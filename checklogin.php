<?php

/*

Author: Subodh Kant

Created On: 13-08-2015 08:00 PM

Purpose: To check login and create the session variables 

$_SESSION['globaluser'] = id of Users Table 

$_SESSION['globaluserId'] =user_id of Users Table

$_SESSION['globalName'] =User First Name

$_SESSION['globalEmail'] = Users Email Address

$_SESSION['role'] = Staff type id FROM staff_type table USED for Restriction of pages

$_SESSION['hpImage'] = Hospital Image 

$_SESSION['hpName'] = Hospital Name 

$_SESSION['image'] = User Image

*/

ob_start();

session_start();

include 'controllers/admin/config.php';

$pagetimeout = 1000*60*10;

$urlun = "";

$urlpw = "";

$cleanpw = "";

$localIP    =  $_SERVER['REMOTE_ADDR'];



//Function to get IP Address of USER 

function getIp() {

    $ip = $_SERVER['REMOTE_ADDR'];

 

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {

        $ip = $_SERVER['HTTP_CLIENT_IP'];

    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {

        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

    }

 

    return $ip;

}



//Function to Get Mac Address of USER DEVICE



function getMACaddress(){

	 ob_start();

	 system('ipconfig /all');

	 $mycom=ob_get_contents(); // Capture the output into a variable

	 ob_clean();

	 $findme = "Physical";

	 $pos = strpos($mycom, $findme);

	 $macp=substr($mycom,($pos+36),17);

	 //echo "The mac id of this system is :".$macp;

	 return $macp;

}



$staticIP = getIp();

$macAddress =getMACaddress();

$urlun = "";

$urlpw = "";

if(isset($_COOKIE["u_id"])&& isset($_COOKIE["u_pass"]))

{

	$urlun = strip_tags(strtolower(substr($_COOKIE["u_id"],0,32)));

	$urlpw = strip_tags(substr($_COOKIE["u_pass"],0,32)); 

}else{

	$urlun = strip_tags(strtolower(substr($_REQUEST['depemail'],0,32)));

	$urlpw = strip_tags(substr($_REQUEST['deppwd'],0,32)); 

}



$sql = "SELECT u.id, u.first_name, u.password,u.email_id,u.user_id,u.staff_type_id,u.status,IFNULL(u.images_path,'default.jpg') as images_path FROM users AS u WHERE u.email_id='".$urlun."'" ;

$record = mysqli_query($conn, $sql);

$count = mysqli_num_rows($record);

if($count>0){

			global $accId;

			$authenticated = "false";

			$userId = "";

			while($row = mysqli_fetch_array($record))

			{

				 $password = $row[2];

				 if (password_verify($urlpw, $password)) {

					  $accId = $row[0];

					  $userName = $row[1];

					  $status = $row[6];

					  $email = $row[3]; 

					  $userId = $row[4]; 

					  $stafftype = $row[5];
					  if($row[7] != ""){
						$imagePath = $row[7];
					  }
					  else{
						  $imagePath = "default.jpg";
					  }

					  if($status == 'I'){

						   $_SESSION['error'] = "2";

						   header("location:login.php");

						   exit();

					  }

					 $user = $accId;					 

					 $_SESSION['globaluser'] = $user ;

					 $_SESSION['globaluserId'] = $userId;

					 $_SESSION['globalName'] = $userName;

					 $_SESSION['globalEmail'] = $email;	

					 $_SESSION["role"] = $stafftype;
					 if (file_exists("./upload_images/staff_dp/" . $imagePath)) 
					 {
						 $_SESSION["image"] = $imagePath;
					 }
					 else{
						$_SESSION["image"] = "default.jpg";
					 }

					 $_SESSION['mainPage'] = "index.php";

					 

					 

					 $authenticated = "true";

					 break;

				 }  

			}  

			 

			if($authenticated == "false") {

				  header("location:login.php");

				  $_SESSION['error'] = "1";

				  exit();

			}

			 $random = substr(number_format(time() * rand(),0,'',''),0,10);

			 $_SESSION['globalNumber'] = $random;   

		   

			 $sqlPrevSessions = "DELETE FROM session WHERE user_id = '".$user."'";

			 mysqli_query($conn,$sqlPrevSessions);

			 $date = new DateTime();

			 //Saving the user session

			 $sqlUserSession = "INSERT INTO session(user_id,mac_address,ip_address,login_timestamp,status,session_id) 

			 VALUES('".$user."','".$macAddress."','".$localIP."',".$date->getTimestamp().",'A','".session_id()."')";   

			 mysqli_query($conn,$sqlUserSession); 

			 

			 //Getting Hospital Profile For User

			 

			 $defaultHospitalName = "New Hospital";		//Default Hospital Name If No Hospital Name is Defined

			 $defaultHospitalImage = "logo.gif";		//Default Hospital Image If No Hospital Image is Defined

			 

			 

			 $sqlGetHospitalProfile = "SELECT (CASE WHEN COUNT(*) > 0 then IFNULL(hospital_profile.hospital_name,'".$defaultHospitalName."') ELSE '".$defaultHospitalName."' END)  as hpName ,

										(CASE WHEN COUNT(*) > 0 then IFNULL(hospital_profile.images_path,'".$defaultHospitalImage."') ELSE '".$defaultHospitalImage."' END)  as hpImage

										FROM hospital_profile";

			 $result = mysqli_query($conn, $sqlGetHospitalProfile);

			 $rows   = array();

			 while ($resultset = mysqli_fetch_array($result)) {					
				if($resultset[0] != "")
				{
					$hpName = $resultset[0];
				}
				else{
					$hpName = $defaultHospitalName;
				}
				if($resultset[1] != "")
				{
					$hpImage = $resultset[1];
				}
				else{
					$hpImage = $defaultHospitalImage;
				}

			 }
			if (file_exists("images/" . $hpImage)) 
			{
				 $_SESSION['hpImage'] = $hpImage;
			}
			else{
				$_SESSION['hpImage'] = "logo.gif";
			}
			

			 $_SESSION['hpName'] = $hpName;

			 

			 $cookie_value = $email; 	

			 

			 /*

				Remember me value check

			 */

			 

			 if(isset($_REQUEST['remember']) && $_REQUEST['remember'] == "on"){				 

				setcookie("u_id",$cookie_value,time() +(3600*24*30),"/");

				setcookie("u_pass",$urlpw,time() +(3600*24*30),"/");

			 }

			 else {

				

				/* setcookie($cookie_name,$cookie_value,time() +(0),"/"); */

			 }

			 header("location:".$_SESSION['mainPage']); 

			 

}

else{

  header("location:login.php");

  $_SESSION['error'] = "1";

  exit();

}

ob_flush();

?>