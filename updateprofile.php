<?php
/*

Included function to update profile parameters
1. Email Id
2. Password
New Updated Password will be sent via mail to the user


*/
session_start();

function crypto_rand_secure($min, $max)
{
	$range = $max - $min;
	if ($range < 1) return $min; // not so random...
	$log = ceil(log($range, 2));
	$bytes = (int) ($log / 8) + 1; // length in bytes
	$bits = (int) $log + 1; // length in bits
	$filter = (int) (1 << $bits) - 1; // set all lower bits to 1
	do {
		$rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
		$rnd = $rnd & $filter; // discard irrelevant bits
	} while ($rnd >= $range);
	return $min + $rnd;
}
function getToken($length)
{
	$token = "";
	$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
	$codeAlphabet.= "0123456789";
	$max = strlen($codeAlphabet) - 1;
	for ($i=0; $i < $length; $i++) {
		$token .= $codeAlphabet[crypto_rand_secure(0, $max)];
	}
	return $token;
}
function updateUserProfile(){
	$newEmail = $_POST['user_settings_email'];
	$oldPassword = strip_tags(substr($_POST['user_settings_old_password'],0,32));
	$password = strip_tags(substr($_POST['user_settings_password'],0,32));
	$newPassword = strip_tags(substr($_POST['user_settings_repassword'],0,32));
	
	if($password != $newPassword ){
		echo "check password";
	}
	else{
		include 'controllers/admin/config.php';
		$headers = "";
		$query1 = "select password from users where email_id = '".$newEmail."' AND id = ".$_SESSION['globaluser']."";
		$resultSet = mysqli_query($conn,$query1);
		while($row = mysqli_fetch_assoc($resultSet)){
			$pwd = $row['password'];
			 if (password_verify($oldPassword, $pwd)) {
					$options = array('password'=>crypto_rand_secure(5,10));
					$decryptedPassword = $newPassword;
					$encryptedpassword = password_hash($decryptedPassword, PASSWORD_BCRYPT, $options);		
					$sql="UPDATE users SET password = '".$encryptedpassword."',email_id='".$newEmail."' WHERE id = ".$_SESSION['globaluser']."";
					$result = mysqli_query($conn,$sql);
					mail($newEmail, "Account Updated on HERP", "USERNAME: ".$newEmail." PASSWORD:".$decryptedPassword."", $headers);
					echo "success";

			 }
			 else{
				 echo "wrong password";
			 }
		}
	}		
}
if(isset($_POST['operation']))
{
  if($_POST['operation'] == "update")
  {
	    updateUserProfile();
  }
}else{

}
?>