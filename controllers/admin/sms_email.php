<?php
	
	
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "saveSmsEmail") {

		$PatientOtRegSms = $_POST['PatientOtRegSms'];
		$appointmentSms = $_POST['appointmentSms'];
		$appointmentEmail = $_POST['appointmentEmail'];
		$opdBookingSms = $_POST['opdBookingSms'];
		$pateintRegSms = $_POST['pateintRegSms'];
		$applyLeaveEmail = $_POST['applyLeaveEmail'];
		$leaveCancelSms = $_POST['leaveCancelSms'];
		$leaveCancelEmail = $_POST['leaveCancelEmail'];
		$leaveApprovedSms = $_POST['leaveApprovedSms'];
		$leaveApprovedEmail = $_POST['leaveApprovedEmail'];
		$rosterManagementEmail = $_POST['rosterManagementEmail'];
		$billingEmail = $_POST['billingEmail'];
		$OTBookingsSms = $_POST['OTBookingsSms'];
		$OTBookingsEmail = $_POST['OTBookingsEmail'];
		$appointmentCancelSms = $_POST['appointmentCancelSms'];
		$appointmentCancelEmail = $_POST['appointmentCancelEmail'];
		$appointmentScheduledSms = $_POST['appointmentScheduledSms'];
		$appointmentScheduledEmail = $_POST['appointmentScheduledEmail'];

		$selSmsEmail = "SELECT id FROM sms_email";
		$checkSmsEmail = mysqli_query($conn,$selSmsEmail);
		$countSmsEmail = mysqli_num_rows($checkSmsEmail);

		//echo $countSmsEmail;
		if ($countSmsEmail == "0") {

			$query = "INSERT INTO sms_email(patient_ot_reg_sms,appointment_sms,app_cancel_sms,app_cancel_email,app_scheduled_sms,app_scheduled_email,appointment_email,opd_booking_sms,patient_reg_sms,apply_leave_email,leave_cancel_sms,leave_cancel_email,leave_approved_sms,leave_approved_email,roster_management_email,billing_email,ot_booking_sms,ot_booking_email,created_by,created_on) 
			VALUES('".$PatientOtRegSms."','".$appointmentSms."','".$appointmentEmail."','".$appointmentCancelSms."','".$appointmentCancelEmail."','".$appointmentScheduledSms."','".$appointmentScheduledEmail."','".$opdBookingSms."','".$pateintRegSms."','".$applyLeaveEmail."','".$leaveCancelSms."','".$leaveCancelEmail."','".$leaveApprovedSms."','".$leaveApprovedEmail."','".$rosterManagementEmail."','".$billingEmail."','".$OTBookingsSms."','".$OTBookingsEmail."','".$userId."',UNIX_TIMESTAMP())";

			$result = mysqli_query($conn, $query);
			echo $result;
		}
		else {
			$query = "UPDATE sms_email SET patient_ot_reg_sms='".$PatientOtRegSms."',appointment_sms='".$appointmentSms."',appointment_email='".$appointmentEmail."',app_cancel_sms='".$appointmentCancelSms."',app_cancel_email='".$appointmentCancelEmail."',app_scheduled_sms='".$appointmentScheduledSms."',app_scheduled_email='".$appointmentScheduledEmail."',opd_booking_sms='".$opdBookingSms."',patient_reg_sms='".$pateintRegSms."',apply_leave_email='".$applyLeaveEmail."',leave_cancel_sms='".$leaveCancelSms."',leave_cancel_email='".$leaveCancelEmail."',leave_approved_sms='".$leaveApprovedSms."',leave_approved_email='".$leaveApprovedEmail."',roster_management_email='".$rosterManagementEmail."',billing_email='".$billingEmail."',ot_booking_sms='".$OTBookingsSms."',ot_booking_email='".$OTBookingsEmail."',created_by='".$userId."',created_on= UNIX_TIMESTAMP() WHERE status ='A'" ;
			$result = mysqli_query($conn, $query);
			echo $result;
		}
	}
	if($operation == "showDefaultValues") {

		$query = "SELECT patient_ot_reg_sms,appointment_sms,appointment_email,opd_booking_sms,patient_reg_sms,apply_leave_email,leave_cancel_sms,leave_cancel_email,leave_approved_sms,leave_approved_email,roster_management_email,billing_email,ot_booking_sms,ot_booking_email FROM sms_email WHERE status = 'A'";
		$result = mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while ($r = mysqli_fetch_assoc($result)) {
			$rows = $r;
		}
		print json_encode($rows);
	}
?>