<?php
	
	session_start();
	if(!session_id()){
		exit();
	}
	$toDate = '';
	$fromDate = '';
	
	include_once('config.php');

	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET['operation'])){
		$operation=$_GET["operation"];
	}

	if ($operation=="showChartData") {
		if (isset($_POST['dataLoad'])) {
			$dataLoad = $_POST["dataLoad"];
		}
		if (isset($_POST['fromDate'])) {
			$fromDate = $_POST["fromDate"];
		}
		if (isset($_POST['toDate'])) {
			$toDate = $_POST["toDate"];
		}

		if ($dataLoad =="all") {

			$sqlConsultantWiseOPDServices = "SELECT COUNT(services_request.id) AS 'Total Services',services.name AS 'Service Name',CONCAT(users.first_name,' ',users.last_name) AS 'Consultant Name' 
				,visits.consultant_id AS 'Consultant Id' FROM services_request
				LEFT JOIN visits ON visits.id = services_request.visit_id
				LEFT JOIN services ON services.id = services_request.service_id
				LEFT JOIN users on users.id = visits.consultant_id
				GROUP BY 'Consultant Id',services_request.service_id";
		}
		
		else if ($dataLoad =="last30days") {	

			$sqlConsultantWiseOPDServices = "SELECT COUNT(services_request.id) AS 'Total Services',services.name AS 'Service Name',CONCAT(users.first_name,' ',users.last_name) AS 'Consultant Name'
				,visits.consultant_id AS 'Consultant Id' FROM services_request
				LEFT JOIN visits ON visits.id = services_request.visit_id
				LEFT JOIN services ON services.id = services_request.service_id
				LEFT JOIN users on users.id = visits.consultant_id
				WHERE services_request.request_date >= unix_timestamp(curdate() - interval 1 month)
				GROUP BY 'Consultant Id',services_request.service_id";
		}


		else if ($dataLoad =="last7days") {

			$sqlConsultantWiseOPDServices = "SELECT COUNT(services_request.id) AS 'Total Services',services.name AS 'Service Name',CONCAT(users.first_name,' ',users.last_name) AS 'Consultant Name'
				,visits.consultant_id AS 'Consultant Id' FROM services_request
				LEFT JOIN visits ON visits.id = services_request.visit_id
				LEFT JOIN services ON services.id = services_request.service_id
				LEFT JOIN users on users.id = visits.consultant_id
				WHERE services_request.request_date >= unix_timestamp(curdate() - interval 7 day)
				GROUP BY 'Consultant Id',services_request.service_id";
		}


		else if ($dataLoad =="today") {	

			$sqlConsultantWiseOPDServices = "SELECT COUNT(services_request.id) AS 'Total Services',services.name AS 'Service Name',CONCAT(users.first_name,' ',users.last_name) AS 'Consultant Name'
				,visits.consultant_id AS 'Consultant Id' FROM services_request
				LEFT JOIN visits ON visits.id = services_request.visit_id
				LEFT JOIN services ON services.id = services_request.service_id
				LEFT JOIN users on users.id = visits.consultant_id
				WHERE services_request.request_date >= unix_timestamp(curdate())
				GROUP BY 'Consultant Id',services_request.service_id";
		}
		else  {

			$sqlConsultantWiseOPDServices = "SELECT COUNT(services_request.id) AS 'Total Services',services.name AS 'Service Name',CONCAT(users.first_name,' ',users.last_name) AS 'Consultant Name' 
				,visits.consultant_id AS 'Consultant Id' FROM services_request
				LEFT JOIN visits ON visits.id = services_request.visit_id
				LEFT JOIN services ON services.id = services_request.service_id
				LEFT JOIN users on users.id = visits.consultant_id
				WHERE services_request.request_date BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59')
				GROUP BY 'Consultant Id',services_request.service_id";			
		}		
		
		$rowsConsultantWiseOPDServices  = array();
	    $resultConsultantWiseOPDServices  = mysqli_query($conn,$sqlConsultantWiseOPDServices);
	    while ($r = mysqli_fetch_assoc($resultConsultantWiseOPDServices)) {
	        $rowsConsultantWiseOPDServices[] = $r;
	    }
	    echo json_encode($rowsConsultantWiseOPDServices);
	}
?>