<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	
	include 'config.php';
	$headers = '';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}	
	//Operation to update the Test Status to 'done' 
	if($operation == "cancelService")   
	{
		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		if (isset($_POST['cost'])) {
			$cost=$_POST["cost"];
		}
		if (isset($_POST['visitId'])) {
			$visitId=$_POST["visitId"];
		}
		
		$query      = "SELECT id+1 AS id FROM patient_bill_payment ORDER BY id DESC LIMIT 1";
		$receiptNo = '';
	    $result     = mysqli_query($conn, $query);
	    $rows_count = mysqli_num_rows($result);
	    if ($rows_count > 0) {
	        while ($row = mysqli_fetch_row($result)) {
	            $receiptNo = $row[0];
	        }
	    }
	    $query="INSERT INTO  patient_bill_payment (debit,receipt_no,payment_mode,visit_id) Values('".$cost."','".$receiptNo."','1','".$visitId."')";  
		$result=mysqli_query($conn,$query);
		
		$query="UPDATE services_request SET status = 'I' WHERE  id = '".$id."'";  
		$result=mysqli_query($conn,$query);
		
		echo $result;

		if($result == '1'){
	    	$selQuery = "select s.name,concat(p.first_name,' ',p.last_name) as patient_name,p.email,sr.pay_status,se.billing_email from sms_email as se,services_request as sr left join patients as p on p.id=sr.patient_id left join services as s on s.id=sr.service_id where sr.id = '".$id."'";
	    	$resultCheck = mysqli_query($conn,$selQuery);
			$totalrecords = mysqli_num_rows($resultCheck);
			$rows = array();
			while ($r = mysqli_fetch_assoc($resultCheck)) {
				$rows = $r;
				//print_r($rows);
				$pay_status = $rows['pay_status'];
				$email = $rows['email'];
				$patient_name = $rows['patient_name'];
				$service_name = $rows['name'];
				$billing_email = $rows['billing_email'];
			}
			if($pay_status == 'paid'){
				$previousMessage  = $billing_email;
				$replace = array("@@patient_name@@", "@@service_name@@");
				$replaced   = array($patient_name, $service_name);
				$message = str_replace($replace, $replaced, $previousMessage);
				//print_r($message);
				mail($billing_email, " Refund Money ".$patient_name."", $message, $headers);
			}

			
	   	}

	}

	// Procedure

	//Operation to update the Test Status to 'done' 
	if($operation == "cancelProcedure")   
	{
		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		if (isset($_POST['cost'])) {
			$cost=$_POST["cost"];
		}
		if (isset($_POST['visitId'])) {
			$visitId=$_POST["visitId"];
		}
		
		$query      = "SELECT id+1 AS id FROM patient_bill_payment ORDER BY id DESC LIMIT 1";
		$receiptNo = '';
	    $result     = mysqli_query($conn, $query);
	    $rows_count = mysqli_num_rows($result);
	    if ($rows_count > 0) {
	        while ($row = mysqli_fetch_row($result)) {
	            $receiptNo = $row[0];
	        }
	    }
	    $query="INSERT INTO  patient_bill_payment (debit,receipt_no,payment_mode,visit_id) Values('".$cost."','".$receiptNo."','1','".$visitId."')";  
		$result=mysqli_query($conn,$query);
		
		$query="UPDATE procedures_requests SET status = 'I' WHERE  id = '".$id."'";  
		$result=mysqli_query($conn,$query);
		echo $result;

		if($result == '1'){
	    	$selQuery = "select pr.pay_status,concat(p.first_name,' ',p.last_name) as patient_name,p.email,pro.name,se.billing_email from sms_email as se,procedures_requests as pr left join patients as p on p.id=pr.patient_id left join procedures as pro on pro.id=pr.procedure_id where pr.id = '".$id."'";
	    	$resultCheck = mysqli_query($conn,$selQuery);
			$totalrecords = mysqli_num_rows($resultCheck);
			$rows = array();
			while ($r = mysqli_fetch_assoc($resultCheck)) {
				$rows = $r;
				//print_r($rows);
				$pay_status = $rows['pay_status'];
				$email = $rows['email'];
				$patient_name = $rows['patient_name'];
				$service_name = $rows['name'];
				$billing_email = $rows['billing_email'];
			}
			if($pay_status == 'paid'){
				$previousMessage  = $billing_email;
				$replace = array("@@patient_name@@", "@@service_name@@");
				$replaced   = array($patient_name, $service_name);
				$message = str_replace($replace, $replaced, $previousMessage);
				//print_r($message);
				mail($billing_email, " Refund Money ".$patient_name."", $message, $headers);
			}
				
	   	}
		
	}

	//Lab Request

	//Operation to update the Test Status to 'done' and Generate a new specimen
	if($operation == "cancelLab")   
	{
		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		if (isset($_POST['cost'])) {
			$cost=$_POST["cost"];
		}
		if (isset($_POST['visitId'])) {
			$visitId=$_POST["visitId"];
		}
		
		$query      = "SELECT id+1 AS id FROM patient_bill_payment ORDER BY id DESC LIMIT 1";
		$receiptNo = '';
	    $result     = mysqli_query($conn, $query);
	    $rows_count = mysqli_num_rows($result);
	    if ($rows_count > 0) {
	        while ($row = mysqli_fetch_row($result)) {
	            $receiptNo = $row[0];
	        }
	    }
	    $query="INSERT INTO  patient_bill_payment (debit,receipt_no,payment_mode,visit_id) Values('".$cost."','".$receiptNo."','1','".$visitId."')";  
		$result=mysqli_query($conn,$query);
		
		$query="UPDATE lab_test_requests SET status = 'I' WHERE  id = '".$id."'";  
		$result=mysqli_query($conn,$query);
		
		echo $result;

		if($result == '1'){
	    	$selQuery = "select ltr.pay_status,concat(p.first_name,' ',p.last_name) as patient_name,p.email,lt.name,se.billing_email from sms_email as se,lab_test_requests as ltr left join patients as p on p.id=ltr.patient_id left join lab_tests as lt on lt.id=ltr.lab_test_id where ltr.id = '".$id."'";
	    	$resultCheck = mysqli_query($conn,$selQuery);
			$totalrecords = mysqli_num_rows($resultCheck);
			$rows = array();
			while ($r = mysqli_fetch_assoc($resultCheck)) {
				$rows = $r;
				//print_r($rows);
				$pay_status = $rows['pay_status'];
				$email = $rows['email'];
				$patient_name = $rows['patient_name'];
				$service_name = $rows['name'];
				$billing_email = $rows['billing_email'];
			}
			if($pay_status == 'paid'){
				$previousMessage  = $billing_email;
				$replace = array("@@patient_name@@", "@@service_name@@");
				$replaced   = array($patient_name, $service_name);
				$message = str_replace($replace, $replaced, $previousMessage);
				//print_r($message);
				mail($billing_email, " Refund Money ".$patient_name."", $message, $headers);
			}
				
	   	}
	}

	//Radiology Request

	if($operation == "cancelRadiology")   
	{
		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		if (isset($_POST['cost'])) {
			$cost=$_POST["cost"];
		}
		if (isset($_POST['visitId'])) {
			$visitId=$_POST["visitId"];
		}
		
		$query      = "SELECT id+1 AS id FROM patient_bill_payment ORDER BY id DESC LIMIT 1";
		$receiptNo = '';
	    $result     = mysqli_query($conn, $query);
	    $rows_count = mysqli_num_rows($result);
	    if ($rows_count > 0) {
	        while ($row = mysqli_fetch_row($result)) {
	            $receiptNo = $row[0];
	        }
	    }
	    $query="INSERT INTO  patient_bill_payment (debit,receipt_no,payment_mode,visit_id) Values('".$cost."','".$receiptNo."','1','".$visitId."')";  
		$result=mysqli_query($conn,$query);
		$query="UPDATE radiology_test_request SET status = 'I' WHERE  id = '".$id."'";  
		$result=mysqli_query($conn,$query);
		echo $result;

		if($result == '1'){
	    	$selQuery = "select rtr.pay_status,concat(p.first_name,' ',p.last_name) as patient_name,p.email,rt.name,se.billing_email from sms_email as se,radiology_test_request as rtr left join patients as p on p.id=rtr.patient_id left join radiology_tests as rt on rt.id=rtr.radiology_test_id where rtr.id = '".$id."'";
	    	$resultCheck = mysqli_query($conn,$selQuery);
			$totalrecords = mysqli_num_rows($resultCheck);
			$rows = array();
			while ($r = mysqli_fetch_assoc($resultCheck)) {
				$rows = $r;
				//print_r($rows);
				$pay_status = $rows['pay_status'];
				$email = $rows['email'];
				$patient_name = $rows['patient_name'];
				$service_name = $rows['name'];
				$billing_email = $rows['billing_email'];
			}
			if($pay_status == 'paid'){
				$previousMessage  = $billing_email;
				$replace = array("@@patient_name@@", "@@service_name@@");
				$replaced   = array($patient_name, $service_name);
				$message = str_replace($replace, $replaced, $previousMessage);
				//print_r($message);
				mail($billing_email, " Refund Money ".$patient_name."", $message, $headers);
			}
				
	   	}
	}

	//Forensic Request

	if($operation == "cancelForensic")   
	{
		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		if (isset($_POST['cost'])) {
			$cost=$_POST["cost"];
		}
		if (isset($_POST['visitId'])) {
			$visitId=$_POST["visitId"];
		}
		
		$query      = "SELECT id+1 AS id FROM patient_bill_payment ORDER BY id DESC LIMIT 1";
		$receiptNo = '';
	    $result     = mysqli_query($conn, $query);
	    $rows_count = mysqli_num_rows($result);
	    if ($rows_count > 0) {
	        while ($row = mysqli_fetch_row($result)) {
	            $receiptNo = $row[0];
	        }
	    }
	    $query="INSERT INTO  patient_bill_payment (debit,receipt_no,payment_mode,visit_id) Values('".$cost."','".$receiptNo."','1','".$visitId."')";  
		$result=mysqli_query($conn,$query);

		$query="UPDATE forensic_test_request SET status = 'I' WHERE  id = '".$id."'";  
		$result=mysqli_query($conn,$query);
		echo $result;

		if($result == '1'){
	    	$selQuery = "select ftr.pay_status,concat(p.first_name,' ',p.last_name) as patient_name,p.email,ft.name,se.billing_email from sms_email as se,forensic_test_request as ftr left join patients as p on p.id=ftr.patient_id left join forensic_tests as ft on ft.id=ftr.forensic_test_id where ftr.id = '".$id."'";
	    	$resultCheck = mysqli_query($conn,$selQuery);
			$totalrecords = mysqli_num_rows($resultCheck);
			$rows = array();
			while ($r = mysqli_fetch_assoc($resultCheck)) {
				$rows = $r;
				//print_r($rows);
				$pay_status = $rows['pay_status'];
				$email = $rows['email'];
				$patient_name = $rows['patient_name'];
				$service_name = $rows['name'];
				$billing_email = $rows['billing_email'];
			}
			if($pay_status == 'paid'){
				$previousMessage  = $billing_email;
				$replace = array("@@patient_name@@", "@@service_name@@");
				$replaced   = array($patient_name, $service_name);
				$message = str_replace($replace, $replaced, $previousMessage);
				//print_r($message);
				mail($billing_email, " Refund Money ".$patient_name."", $message, $headers);
			}
				
	   	}
	}
?>