<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET['operation'])){
		$operation=$_GET["operation"];
	}

	if($operation == "bindTableData"){
		if (isset($_POST['visitId'])) {
			$visitId=$_POST['visitId'];
		}
		else if(isset($_GET['visitId'])){
			$visitId=$_GET['visitId'];
		}
		
		if (isset($_POST['paitentId'])) {
		$paitentId=$_POST['paitentId'];
		}
		else if(isset($_GET['paitentId'])){
			$paitentId=$_GET['paitentId'];
		}
		
		$sql = "SELECT radiology_test_result.*,patients.id as patient_id,concat(patients.salutation,' ',patients.first_name,' ',
				patients.last_name) as patient_name,visits.created_on as visit_date,radiology_tests.name,visits.id as visit_id FROM `radiology_test_result` 		
				LEFT JOIN radiology_test_request ON radiology_test_request.id=radiology_test_result.request_id
				LEFT JOIN radiology_tests ON radiology_tests.id = radiology_test_request.radiology_test_id 
				LEFT JOIN patients ON radiology_test_request.patient_id = patients.id 
				LEFT JOIN visits ON visits.id = radiology_test_request.visit_id 
				WHERE radiology_test_request.visit_id=".$visitId." AND radiology_test_request.patient_id=".$paitentId."
				AND radiology_test_request.test_status='resultsent' AND radiology_test_request.pay_status='settled'";
		
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
	
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);	
	}
?>