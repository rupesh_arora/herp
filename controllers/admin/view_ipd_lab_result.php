<?php
/* ****************************************************************************************************
 * File Name    :   view_ipd_lab_result.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   4th mar, 2016
 * Description  :   This page  manages ipd lab results
 *************************************************************************************************** */	
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET['operation'])){
		$operation=$_GET["operation"];
	}

	if($operation == "bindTableData"){
		if(isset($_GET['ipdId'])){
			$ipdId = $_GET['ipdId'];
		}
		
		if (isset($_POST['patientId'])) {
			$patientId = $_POST['patientId'];
		}
		else if(isset($_GET['patientId'])){
			$patientId = $_GET['patientId'];
		}
		if (isset($_POST['patientName'])) {
			$patientName = $_POST['patientName'];
		}
		else if(isset($_GET['patientName'])){
			$patientName = $_GET['patientName'];
		}
		
		$isFirst = "false";

		$sql = "SELECT ipd_lab_test_requests.lab_test_id,l.id,l.result,l.normal_range,l.report,patients.id as patient_id,concat(patients.salutation,' ',patients.first_name,' ',
			patients.middle_name,' ',patients.last_name) as patient_name,
			DATE_FORMAT(FROM_UNIXTIME(ipd_registration.created_on), '%Y-%m-%d') as visit_date,ipd_registration.id as ipd_id,l_c.name,
			l.lab_test_component_id,l_t.name AS lab_test_name,
			(SELECT value FROM configuration WHERE name = 'ipd_prefix') AS ipd_prefix,
			(select value from configuration where name = 'patient_prefix') as patient_prefix 
			from `ipd_lab_test_result` AS l 
			LEFT JOIN ipd_patient_specimen ON l.ipd_specimen_id =ipd_patient_specimen.id 
			LEFT JOIN lab_test_component AS l_c ON l_c.id = l.lab_test_component_id
			LEFT JOIN lab_tests AS l_t ON l_t.id = l_c.lab_test_id
			LEFT JOIN ipd_lab_test_requests ON ipd_patient_specimen.lab_test_id = ipd_lab_test_requests.id
			LEFT JOIN patients ON patients.id = ipd_lab_test_requests.patient_id 
			LEFT JOIN ipd_registration ON ipd_registration.id = ipd_lab_test_requests.ipd_id 
			WHERE ipd_lab_test_requests.test_status='resultsent' 
			AND ipd_lab_test_requests.pay_status='settled'";		

			if ($patientName != '' || $patientId != '' || $ipdId != '' ) {
		        $sql .= " AND ";
		    }

		    if ($patientName != '') {

		        if ($isFirst == "true") {
		            $sql .= " AND ";
		        }
		        $sql .=  " patients.first_name LIKE '%" . $patientName . "%'";
		        $isFirst = "true";
		    }

		    if ($patientId != '') {
		        $patientPrefix = $_GET['patientPrefix'];
				$query       = "SELECT value FROM configuration WHERE name = 'patient_prefix'";
				$resultQry    = mysqli_query($conn, $query);
				
				while ($r = mysqli_fetch_assoc($resultQry)) {
					$prefix = $r['value'];
				}
				
				if($prefix == $patientPrefix){
					if ($isFirst == "true") {
			            $sql .= " AND ";
			        }
					$sql .= " patients.id = '".$patientId."' ";
					$isFirst = "true";
				}
				else{
					if ($isFirst == "true") {
			            $sql .= " AND ";
			        }
			        $sql .=" patients.id = '0' ";
			        $isFirst = "true";
				}
		    }

		    if ($ipdId != '') {	
		    	 if(isset($_GET['ipdPrefix'])) {
		    		$ipdPrefix = $_GET['ipdPrefix'];
		    	}		    	
				$query       = "SELECT value FROM configuration WHERE name = 'ipd_prefix'";
				$resultQry    = mysqli_query($conn, $query);
				
				while ($r = mysqli_fetch_assoc($resultQry)) {
					$prefix = $r['value'];
				}

		        if($prefix == $ipdPrefix){
		        	if ($isFirst == "true") {
			            $sql .= " AND ";
			        }
					$sql .= " ipd_registration.id = '".$ipdId."' ";
					$isFirst = "true";
				}
				else{
					if ($isFirst == "true") {
			            $sql .= " AND ";
			        }
			        $sql .=" ipd_registration.id = '0' ";
			        $isFirst = "true";
				}
		    }

		$sql .=" GROUP BY ipd_lab_test_requests.lab_test_id";

		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		
		$rows = array();			
		while($r = mysqli_fetch_assoc($result)) {							
			$rows[] = $r;
		}		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}

	if($operation == "bindTableDataByOrder"){
		if (isset($_POST['ipdId'])) {
			$ipdId=$_POST['ipdId'];
		}
		else if(isset($_GET['ipdId'])){
			$ipdId=$_GET['ipdId'];
		}
		
		if (isset($_POST['paitentId'])) {
		$paitentId=$_POST['paitentId'];
		}
		else if(isset($_GET['paitentId'])){
			$paitentId=$_GET['paitentId'];
		}


		if (isset($_POST['labTestId'])) {
			$labTestId=$_POST['labTestId'];

			$sql = "SELECT ipd_lab_test_requests.lab_test_id,l.id,l.result,l.normal_range,l.report,
			patients.id as patient_id,concat(patients.salutation,' ',patients.first_name,
			' ',patients.middle_name,' ',patients.last_name) as patient_name,ipd_registration.created_on 
			as visit_date,ipd_registration.id as ipd_id,l_c.name,l.lab_test_component_id,l_t.name 
			AS lab_test_name from `ipd_lab_test_result` AS l 
			LEFT JOIN ipd_patient_specimen ON l.ipd_specimen_id=ipd_patient_specimen.id 
			LEFT JOIN lab_test_component AS l_c ON l_c.id = l.lab_test_component_id
			LEFT JOIN lab_tests AS l_t ON l_t.id = l_c.lab_test_id
			LEFT JOIN ipd_lab_test_requests ON ipd_patient_specimen.lab_test_id = ipd_lab_test_requests.id
			LEFT JOIN patients ON patients.id = ipd_lab_test_requests.patient_id 
			LEFT JOIN ipd_registration ON ipd_registration.id = ipd_lab_test_requests.ipd_id  
			WHERE ipd_lab_test_requests.ipd_id=".$ipdId." 
			AND ipd_lab_test_requests.patient_id=".$paitentId." 
			AND ipd_lab_test_requests.test_status='resultsent'
			AND ipd_lab_test_requests.pay_status='settled'
			AND ipd_lab_test_requests.lab_test_id = '".$labTestId."'
			ORDER BY ipd_lab_test_requests.lab_test_id";
		}

		else{
			$sql = "SELECT ipd_lab_test_requests.lab_test_id,l.id,l.result,l.normal_range,l.report,
				patients.id as patient_id,concat(patients.salutation,' ',patients.first_name,
				' ',patients.middle_name,' ',patients.last_name) as patient_name,ipd_registration.created_on 
				as visit_date,ipd_registration.id as visit_id,l_c.name,l.lab_test_component_id,l_t.name 
				AS lab_test_name from `lab_test_result` AS l 
				LEFT JOIN patient_specimen ON l.specimen_id=patient_specimen.id 
				LEFT JOIN lab_test_component AS l_c ON l_c.id = l.lab_test_component_id
				LEFT JOIN lab_tests AS l_t ON l_t.id = l_c.lab_test_id
				LEFT JOIN ipd_lab_test_requests ON patient_specimen.lab_test_id = ipd_lab_test_requests.id
				LEFT JOIN patients ON patients.id = ipd_lab_test_requests.patient_id 
				LEFT JOIN ipd_registration ON ipd_registration.id = ipd_lab_test_requests.ipd_id 
				WHERE ipd_lab_test_requests.ipd_id=".$ipdId." 
				AND ipd_lab_test_requests.patient_id=".$paitentId." 
				AND ipd_lab_test_requests.test_status='resultsent'
				AND ipd_lab_test_requests.pay_status='settled'
				ORDER BY ipd_lab_test_requests.lab_test_id";
		}	
		
	$result = mysqli_query($conn, $sql);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);	
			
	}
?>