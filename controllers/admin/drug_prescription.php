<?php
/*File Name  :   drug_prescription.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   31th Dec, 2015
Description  :   This page manages all the drug prescription in consultant module*/

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
/*include config file*/
include 'config.php';

$operation = "";

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
} //nothing

/*operation to serach drug*/
if ($operation == "searchDrug") {
    
    if (isset($_POST['drug'])) {
        $drug = $_POST["drug"];
    }
    
    $query = "	SELECT d.name,d.id,d.alternate_name,d.price,u.unit as unit_measure,
		l_j.name AS alternate_drug_name ,pharmacy_inventory.quantity from drugs AS d
		LEFT JOIN drugs AS l_j ON d.alternate_drug_id = l_j.id 
		LEFT JOIN pharmacy_inventory on pharmacy_inventory.drug_name_id = d.id
		LEFT JOIN units AS u ON d.unit = u.id  
		WHERE d.name = '" . $drug . "' AND d.status = 'A'";    
    $result     = mysqli_query($conn, $query);
    $rows_count = mysqli_num_rows($result);
    
    /*If drug exisit in database*/
    if ($rows_count >= 1) {
        $rows = array();
        while ($r = mysqli_fetch_assoc($result)) {
            $rows[] = $r;
        }
        print json_encode($rows);
    } else {
        echo "0";
    }
}

/*operation to save data*/
if ($operation == "saveData") {
    
    if (isset($_POST['visitId'])) {
        $visitId = $_POST["visitId"];
    }
    if (isset($_POST['patientId'])) {
        $patientId = $_POST["patientId"];
    }
    if (isset($_POST['outputDate'])) {
        $outputDate = $_POST["outputDate"];
    }
    if (isset($_POST['arrayData'])) {
        $arrayData = json_decode($_POST["arrayData"]);
    }
    $totalCurrentReqBill = $_POST['totalCurrentReqBill'];
    $copayValue = $_POST['copayValue'];
    $copayType = $_POST['copayType'];
        
    /*Check if data from data table is null or not*/
    if (!empty($arrayData)) {
        
        $sql      = "INSERT INTO patients_prescription_request (visit_id,patient_id,drug_id,quantity,cost,dispense_status,prescribe_by
                ,issued_on,dosage,duration,notes,insurance_payment,payment_mode) VALUES";
       /* $sqlPharmacyBill = "INSERT INTO patient_pharmacy_bill(visit_id, patient_id, amount,net_amount,discount,status,created_on,updated_on,created_by,updated_by)
                VALUES";*/ 
        $subQuery = '';
        $counter  = 0;
        foreach ($arrayData as $value) {
            $drug_id  = 0;
            $quantity = 0;
            $dosage   = 0;
            $duration = 0;
            $notes    = 0;
            $paymentMode = '';
            foreach ($value as $key => $val) {
                
                if ($key == 0) { //at index one get the drug id
                    $drug_id = $val;
                }
                if ($key == 1) { // at index of array get the dosage
                    $dosage = $val;
                }
                if ($key == 2) { //at index one get the duration
                    $duration = $val;
                }
                if ($key == 3) { // at index of array get the quantity
                    $quantity = $val; 
                }
                if ($key == 4) { //at index one get the notes
                    $notes = $val;
                }
                if ($key == 5) { //at index one get the notes
                    $paymentMode = $val;
                }
                if ($key == 6) { //at index one get the notes
                    $cost = $val;
                }

                //if insurance mode is insurance then check copay 
                if (strtolower($paymentMode) == "insurance") {

                    if (strtolower($copayType) == "none") {
                        $billStatus = 'paid';           
                    }
                    else if (strtolower($copayType) == "fixed"){
                        if ($totalCurrentReqBill <= $copayValue) {
                            $billStatus = 'paid';
                        }
                        else{
                            $billStatus = 'unpaid';
                        }
                    }
                    else if (strtolower($copayType) == "percentage") {
                        if ($copayValue == "0" ) {
                            $billStatus =  "paid";
                        }
                        else{
                            $billStatus = 'unpaid';
                        }
                    }
                }
                else {
                    $billStatus =  "unpaid";
                }
            }
            
            if ($counter > 0) {
                $subQuery .= ",";
            }
            
            $subQuery .= "('" . $visitId . "','" . $patientId . "','" . $drug_id . "','" . $quantity . "','" . $cost . "','Pending','Doctor',
                '" . $outputDate . "','" . $dosage . "','" . $duration . "','" . $notes . "',
                '".$billStatus."','".$paymentMode."')";

            $counter++;
        }
        
        $sqlInsert = $sql . $subQuery.';'; // join both query
        if($billStatus == 'paid'){
            $subBillQuery = "INSERT INTO patient_pharmacy_bill(visit_id, patient_id, amount,net_amount,discount,status,created_on,updated_on,created_by,updated_by)
                VALUES ('".$visitId."', '".$patientId."','".$totalCurrentReqBill."','".$totalCurrentReqBill."','0','paid',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
           //$sqlPharmacyBill .= $sqlPharmacyBill . $subBillQuery . ";";
             $result    = mysqli_query($conn, $subBillQuery);
            
        }
       
        $result    = mysqli_query($conn, $sqlInsert);
		if($result){
			$sqlUpdate = "UPDATE visits AS v SET v.prescription_status = 'Yes' WHERE id = '" . $visitId . "'";			
			$resultUpdate    = mysqli_query($conn, $sqlUpdate);
			echo $resultUpdate;
		}
        else{
			echo $result;
		}
    }
}

/*Operation to get old data in datatable*/
if ($operation == "getOldData") {    
    $visit_id = $_POST['visit_id'];
    $paymentMode = $_POST['paymentMode'];
    $activeTab = $_POST['activeTab'];

    if ($paymentMode == "insurance") {
        $insuranceCompanyId = $_POST['insuranceCompanyId'];
        $insurancePlanId = $_POST['insurancePlanId'];
        $insurancePlanNameid = $_POST['insurancePlanNameid'];

        $sqlSelect = "SELECT patients_prescription_request.id,drugs.name, 
        patients_prescription_request.quantity,patients_prescription_request.dosage,
        patients_prescription_request.duration,patients_prescription_request.notes, drugs.alternate_name,
        units.unit,drugs.price,pharmacy_inventory.quantity AS stock,        
        patients_prescription_request.cost/patients_prescription_request.quantity AS revised_cost         
        FROM patients_prescription_request
        LEFT JOIN drugs ON patients_prescription_request.drug_id = drugs.id
        LEFT JOIN pharmacy_inventory on pharmacy_inventory.drug_name_id = drugs.id        
        LEFT JOIN units ON drugs.unit = units.id        
        where patients_prescription_request.visit_id = '".$visit_id."' AND drugs.`status` = 'A' 
        AND units.`status` = 'A'";

    }
    else {

        $sqlSelect ="SELECT patients_prescription_request.id,drugs.name, 
        patients_prescription_request.quantity,patients_prescription_request.dosage,
        patients_prescription_request.duration,patients_prescription_request.notes, drugs.alternate_name,
        units.unit,drugs.price,pharmacy_inventory.quantity AS stock FROM patients_prescription_request
        LEFT JOIN drugs ON patients_prescription_request.drug_id = drugs.id
        LEFT JOIN pharmacy_inventory on pharmacy_inventory.drug_name_id = drugs.id
        LEFT JOIN units ON drugs.unit = units.id
        where patients_prescription_request.visit_id = '".$visit_id."' AND drugs.`status` = 'A' 
        AND units.`status` = 'A'";
    }

        

    $result=mysqli_query($conn,$sqlSelect);
    $rows = array();
    while($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "checkDiagnosis") {    
    $visit_id = $_POST['visitId'];

    $sqlSelect ="SELECT * FROM patient_diagnosis_request where visit_id = '".$visit_id."'";

    $result=mysqli_query($conn,$sqlSelect);
    $rows_count = mysqli_num_rows($result);

	if($rows_count >= 1){
		echo "1";
	}
	else{
		echo "0";
	}
}
?>