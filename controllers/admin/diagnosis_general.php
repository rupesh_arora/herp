<?php
	session_start();
	
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
		exit();
	}

	if ($operation == "showDiagnosis") {
	    if (isset($_POST['value'])) {
	        $value = $_POST['value'];
	    }
	    $query  = "SELECT id,name,code FROM diagnosis WHERE diagnosis_cat_id = '" . $value . "' AND status = 'A'";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	
	if ($operation == "showDisease") {
	    
	    $query  = "SELECT d.id,d.name FROM diseases AS d WHERE d.status = 'A'";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if($operation == "save"){
		if (isset($_POST['bigdiagnosis'])) {
		   $bigdiagnosis = json_decode($_POST['bigdiagnosis']);
		}
		$patientId = $_POST['patientId'];
		$visitId = $_POST['visitId'];
		$tblName = $_POST['tblName'];
		
		$insertQuery = "INSERT INTO patient_diagnosis_request (patient_id,disease_id,visit_id,created_by,diagnosis_name) ";
		$insertQuery .= "VALUES ";
		$counter = 0;
		foreach($bigdiagnosis as $value) {
			
			$diagnosisId = "";
			$diseaseId = "";
			foreach($value as $key => $val) {
				 if ($key == 0) {
					$diagnosisId = $val;
				}
				if ($key == 1) {
					$diseaseId = $val;
				}
				$counter++;			
				
			}
			if($counter != 2){
				$insertQuery .= ",";
			}			
			$insertQuery .= "(".$patientId.",".$diseaseId.",".$visitId.",'".$userId."','".$diagnosisId."')";			
		}
		$result= mysqli_query($conn,$insertQuery);  
		echo $result;
		
	}
	
	if($operation == "search"){
		$patientId = $_POST['patientId'];
		$visitId = $_POST['visitId'];
		$tblName = $_POST['tblName'];
		
		$searchQuery = "SELECT COUNT(*) as total FROM ".$tblName." WHERE patient_id = ".$patientId." AND visit_id = ".$visitId;
		$searchResult = mysqli_query($conn,$searchQuery);
		
		$data=mysqli_fetch_assoc($searchResult);
		echo $data['total'];
	}

	if ($operation =="searchDisease") {

		if (isset($_POST['diseaseName'])) {
			$diseaseName=$_POST["diseaseName"];
		}
		$query = "SELECT id,code, name from diseases WHERE name LIKE '%".$diseaseName."%'";

		$result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
?>