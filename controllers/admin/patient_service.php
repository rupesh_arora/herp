<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	
	include 'config.php';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}	
	//Operation to update the Test Status to 'done' 
	if($operation == "update")   
	{
		if (isset($_POST['ServiceId'])) {
			$id=$_POST["ServiceId"];
		}
		
		$query="UPDATE services_request SET test_status = 'done' WHERE  id = '".$id."'";  
		$result=mysqli_query($conn,$query);
		echo $result;
	}
	//Operation to Show all Records for Lab Requests which are pending and payment is paid
	if($operation == "show"){
		
		$query= "SELECT services_request.patient_id,
				services_request.visit_id,
				services.code,services.name,
				services_request.cost,
				service_type.type,
				service_type.description,
				services_request.pay_status,
				patients.salutation , patients.first_name , 
				patients.last_name ,
				patients.mobile,
				patients.middle_name,
				patients.dob,
				patients.care_of,
				patients.address,patients.gender,
				patients.zip,patients.images_path,
				patients.care_contact,
				patients.email,
				services_request.id,
				visit_type.`type` AS visitType,
				(SELECT value FROM configuration WHERE name = 'patient_prefix') AS patient_prefix,
				(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visit_prefix			
				FROM `services_request` 
				LEFT JOIN services ON services_request.service_id = services.id 
				LEFT JOIN service_type ON service_type.id = services.service_type_id 
				LEFT JOIN patients On patients.id = services_request.patient_id   
				LEFT JOIN visits On visits.id = services_request.visit_id   
				LEFT JOIN visit_type On visit_type.id = visits.visit_type   
				WHERE services_request.pay_status = 'paid' AND services_request.status = 'A' 
				AND services_request.test_status = 'pending'  ORDER BY ((DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%y-%m-%d')) = CURDATE()) desc,visits.visit_type= '3' desc,visits.id  asc";		
		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}	
	if($operation == "showinactive"){
		
		$query= "SELECT services_request.patient_id,
				services_request.visit_id,
				services.code,services.name,
				services_request.cost,
				service_type.type,
				service_type.description,
				services_request.pay_status,
				patients.salutation , patients.first_name , 
				patients.last_name ,
				patients.mobile,
				patients.middle_name,
				patients.dob,
				patients.care_of,
				patients.address,patients.gender,
				patients.zip,patients.images_path,
				patients.care_contact,
				patients.email,
				services_request.id,
				(SELECT value FROM configuration WHERE name = 'patient_prefix') AS patient_prefix,
				(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visit_prefix			
				FROM `services_request` 
				LEFT JOIN services ON services_request.service_id = services.id 
				LEFT JOIN service_type ON service_type.id = services.service_type_id 
				LEFT JOIN patients On patients.id = services_request.patient_id 
				WHERE services_request.pay_status = 'paid' 
				AND services_request.test_status = 'done'";		
		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
?>