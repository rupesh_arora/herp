<?php
/*
 * File Name    :   cash.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar gupta
 * Created Date :   31th dec, 2015
 * Description  :   This page manage cash details of patient 
 */
$operation = "";

$hrn       = "";
$date      = date("Y/m/d");
$createOn  = new DateTime();
$createdBy = "";

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $createdBy = $_SESSION['globaluser']; // user id through session
}
else{
	exit();
}



include 'config.php'; // import database connection file    
$operation = $_POST["operation"]; // get operation from js file

// fetch value from patient table
if ($operation == "select") {
    $patientPrefix = $_POST['patientPrefix'];
	$sql       = "SELECT value FROM configuration WHERE name = 'patient_prefix'";
    $result    = mysqli_query($conn, $sql);
    
    while ($r = mysqli_fetch_assoc($result)) {
		$prefix = $r['value'];
	}
	if($prefix == $patientPrefix){

		$patientID = $_POST['patientID'];

		$sql       = " SELECT fan_id,first_name FROM patients WHERE id = '" . $patientID . "'";
	    $result    = mysqli_query($conn, $sql);
	    $row = array();
	    while ($r = mysqli_fetch_assoc($result)) {
			$fanId = $r['fan_id'];
			$name = $r['first_name'];
		}



		if($fanId == "" || $fanId == null){
			
			$query = "SELECT first_name, last_name, middle_name, gender, dob, mobile, fan_id FROM patients WHERE id ='" . $patientID . "'";

			$result    = mysqli_query($conn, $query);
			$row = array();
			$rows_count = mysqli_num_rows($result);
			if ($rows_count > 0) {
				
				while ($r = mysqli_fetch_assoc($result)) {
					$row[] = $r;
				}
			} 

			$sql  = "SELECT sum(cash_account.credit)-sum(cash_account.debit) as amount FROM cash_account 
			WHERE cash_account.patient_id ='" . $patientID . "'";
			$result    = mysqli_query($conn, $sql);
	
			$rows_count = mysqli_num_rows($result);
			if ($rows_count > 0) {
				
				while ($r = mysqli_fetch_assoc($result)) {
					$row[] = $r;
				}
				print json_encode($row);
			} 
			else {
				echo "";
			}
		}
		else{

			$query = "SELECT first_name, last_name, middle_name, gender, dob, mobile, fan_id FROM patients WHERE id ='" . $patientID . "'";

			$result    = mysqli_query($conn, $query);
			$row = array();
			$rows_count = mysqli_num_rows($result);
			if ($rows_count > 0) {
				
				while ($r = mysqli_fetch_assoc($result)) {
					$row[] = $r;
				}
			} 

			$sql  = "SELECT sum(cash_account.credit)-sum(cash_account.debit) as amount FROM cash_account 
				WHERE cash_account.patient_id ='" . $fanId . "' OR cash_account.depositor_id ='" . $patientID . "'";
				$result    = mysqli_query($conn, $sql);
		
				$rows_count = mysqli_num_rows($result);
				if ($rows_count > 0) {
					
					while ($r = mysqli_fetch_assoc($result)) {
						$row[] = $r;
					}
					print json_encode($row);
				} 
				else {
					echo "";
				}
		}		
		
	}
	else{
		echo "2";
	}
    
}
// save cash of patient in cash_account table
if ($operation == "save") {
    $patientID     = $_POST["patientID"];
    $fanPatientId     = $_POST["fanPatientId"];
    $depositAmount = $_POST["depositAmount"];
    $credit        = 0;
    $debit         = 0;

    $creditDeposit = $depositAmount;
    $debitDeposit  = 0;

	$patientPrefix = $_POST['prefix'];
	$sql       = "SELECT value FROM configuration WHERE name = 'patient_prefix'";
    $result    = mysqli_query($conn, $sql);
    
    while ($r = mysqli_fetch_assoc($result)) {
		$prefix = $r['value'];
	}
	if($prefix == $patientPrefix){
		$sql           = "insert into cash_account (patient_id,depositor_id,date,credit,debit,created_on,created_by)
        values('" . $fanPatientId . "','" . $patientID . "','" . $date . "','" . $creditDeposit . "','" . $debitDeposit . "','" . $createOn->getTimestamp() . "','" . $createdBy . "')";
		//echo $sql;
		$result        = mysqli_query($conn, $sql);
		//echo $result;
		if ($result == 1) {
			$sql1    = "select sum(credit)-sum(debit) as amount from cash_account where patient_id = '" . $patientID . "' OR depositor_id ='" . $fanPatientId . "'";
			$result1 = mysqli_query($conn, $sql1);
			
			// cash deposite transaction
			/* $insertTransaction = "INSERT INTO transactions (patient_id,transaction_type_id,credit,debit,processed_on,processed_by,details)
					values ('" . $fanPatientId . "',' 8 ','" . $creditDeposit . "','" . $debitDeposit . "',UNIX_TIMESTAMP(),'".$createdBy ."','Cash Deposite')"; 
			mysqli_query($conn,$insertTransaction); */
			
			while ($r = mysqli_fetch_assoc($result1)) {
				$amount = $r['amount'];
			}
			echo $amount;

			if ($result) {
				$selQuery = "SELECT CONCAT(p.first_name,' ',p.last_name) AS patient_name,p.mobile AS mobile_no,p.country_code,ca.date,ca.credit,p.email,
			        sms_email.id,sms_email.billing_email FROM sms_email,
			        cash_account AS ca LEFT JOIN patients AS p on p.id = ca.patient_id
			        where patient_id = '" . $patientID . "' OR depositor_id ='" . $fanPatientId . "'";
			        $resultCheck = mysqli_query($conn,$selQuery);
			        $totalrecords = mysqli_num_rows($resultCheck);
			        $rows = array();
			        $patient_name;
			        $mobile_no;
			        $country_code;
			        $mobile_no;
			        $date;
			        while ($r = mysqli_fetch_assoc($resultCheck)) {
			            $rows = $r;
			            
			            $patient_name = $rows['patient_name'];
			            $mobile_no = $rows['mobile_no'];
			            $country_code = $rows['country_code'];
			            $date = $rows['date'];
			            $credit = $rows['credit'];
			            $email = $rows['email'];
			            $billing_email = $rows['billing_email'];

			            $previousEmailMessage  = $billing_email;
						$replaceEmail = array("@@patient_name@@","@@amount@@","@@date@@");
						$replacedEmail   = array($patient_name,$credit,$date);
						$emailMessage = str_replace($replaceEmail, $replacedEmail, $previousEmailMessage);
						//print_r($emailMessage);
						$headers = '';
						if ($emailMessage) {
							mail($email, " Account Credited", $emailMessage, $headers);
							$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
			                            created_on,updated_on,created_by,updated_by,msg_status) VALUES('Cash Billing','',
			                            '".$email."','','".$emailMessage."','".$date."',UNIX_TIMESTAMP(),
			                            UNIX_TIMESTAMP(),'" . $createdBy . "','" . $createdBy . "','Mail')";

			                $result = mysqli_query($conn, $sqlQuery);
						}
			        }
			}
		} 
		else {
			echo "";
		}
	}else{
		echo "-2";
	}
    
}
?>