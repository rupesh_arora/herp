<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "save") {
		$name = $_POST['name'];
		$cost = $_POST['cost'];
		$desc = $_POST['desc'];

		$selSurgery = "SELECT name FROM surgery WHERE name ='".$name."' AND status = 'A'";
		$checkSurgery = mysqli_query($conn,$selSurgery);
		$countSurgery = mysqli_num_rows($checkSurgery);
		if ($countSurgery == "0") {
			$sql = "INSERT INTO surgery (name,cost,description,created_on,updated_on,`status`,created_by,updated_by) 
				VALUES('".$name."','".$cost."','".$desc."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'A','".$userId."','".$userId."')";
			    
		    $result = mysqli_query($conn, $sql);
	    	echo $result;
		}
		else{
			echo "0";
		}
			
	}

	if ($operation == "show") { // show active data
    
	    $sql = "SELECT id,name,cost,description FROM surgery WHERE `status`= 'A'";
	    $result = mysqli_query($conn, $sql);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	if ($operation == "checked") {
    
	    $query = "SELECT id,name,cost,description FROM surgery WHERE `status`= 'I'";
	    
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	if ($operation == "update") // update data
	{
	    $description = "";
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    if (isset($_POST['name'])) {
	        $name = $_POST['name'];
	    }
	    if (isset($_POST['cost'])) {
	        $cost = $_POST['cost'];
	    }
	    if (isset($_POST['desc'])) {
	        $desc = $_POST['desc'];
	    }	
		
		$selSurgery = "SELECT name FROM surgery WHERE name ='".$name."' AND status = 'A' 
			AND id !='".$id."'";
		$checkSurgery = mysqli_query($conn,$selSurgery);
		$countSurgery = mysqli_num_rows($checkSurgery);
		if ($countSurgery < 1) {
	    
		$sql    = "UPDATE surgery set name = '".$name."',cost ='".$cost."',description= '".$desc."',updated_on=				UNIX_TIMESTAMP(),updated_by = '".$userId."' where id = '".$id."' ";

			$result = mysqli_query($conn, $sql);
			echo $result;
		}
		else{
			echo "0";
		}
	}

	if ($operation == "restore") // for restore    
    {
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    if (isset($_POST['surgeryName'])) {
	        $surgeryName = $_POST['surgeryName'];
	    }

	    $selSurgery = "SELECT name FROM surgery WHERE name ='".$surgeryName."' AND status = 'A' 
		AND id !='".$id."'";
		$checkSurgery = mysqli_query($conn,$selSurgery);
		$countSurgery = mysqli_num_rows($checkSurgery);
		if ($countSurgery < 1) {

		    $sql    = "UPDATE surgery SET status= 'A'  WHERE  id = '" . $id . "'";
		    $result = mysqli_query($conn, $sql);
		    echo $result;
		}
		else {
			echo "0";
		}
	}
	if ($operation == "delete") {
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    
	    $select = "SELECT surgery_id FROM patient_ot_registration WHERE surgery_id = '" . $id . "' AND status= 'A'";
	    
	    $resultCheck = mysqli_query($conn, $select);
	    $countRows = mysqli_num_rows($resultCheck);
	    if($countRows == 0){
		    $sql    = "UPDATE surgery SET status= 'I' where id = '" . $id . "'";
		    $result = mysqli_query($conn, $sql);
		    echo $result;
	    }
	    echo "0";
	}
?>