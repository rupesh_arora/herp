<?php
	
	session_start();
	if(!session_id()){
		exit();
	}
	$toDate = '';
	$fromDate = '';
	
	include_once('config.php');

	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET['operation'])){
		$operation=$_GET["operation"];
	}

	if ($operation=="showChartData") {
		if (isset($_POST['dataLoad'])) {
			$dataLoad = $_POST["dataLoad"];
		}
		if (isset($_POST['fromDate'])) {
			$fromDate = $_POST["fromDate"];
		}
		if (isset($_POST['toDate'])) {
			$toDate = $_POST["toDate"];
		}

		if ($dataLoad =="all") {

			$sqlDepartmentWiseServices = "SELECT COUNT(services_request.id) AS 'Total Services',services.name AS 'Service Name',department.department AS 'Department Name' ,visits.department AS 'Department Id'  
				FROM services_request
				LEFT JOIN visits ON visits.id = services_request.visit_id
				LEFT JOIN services ON services.id = services_request.service_id
				LEFT JOIN department ON department.id = visits.department
				WHERE department.department IS NOT NULL
				GROUP BY visits.department,services_request.service_id ";
		}
		
		else if ($dataLoad =="last30days") {			

			$sqlDepartmentWiseServices = "SELECT COUNT(services_request.id) AS 'Total Services',services.name AS 'Service Name',department.department AS 'Department Name' ,visits.department AS 'Department Id'  
				FROM services_request
				LEFT JOIN visits ON visits.id = services_request.visit_id
				LEFT JOIN services ON services.id = services_request.service_id
				LEFT JOIN department ON department.id = visits.department
				WHERE department.department IS NOT NULL AND
				visits.created_on >= unix_timestamp(curdate() - interval 1 month)
				GROUP BY visits.department,services_request.service_id";
		}


		else if ($dataLoad =="last7days") {

			$sqlDepartmentWiseServices = "SELECT COUNT(services_request.id) AS 'Total Services',services.name AS 'Service Name',department.department AS 'Department Name' ,visits.department AS 'Department Id' 
				FROM services_request
				LEFT JOIN visits ON visits.id = services_request.visit_id
				LEFT JOIN services ON services.id = services_request.service_id
				LEFT JOIN department ON department.id = visits.department
				WHERE department.department IS NOT NULL AND
				visits.created_on >= unix_timestamp(curdate() - interval 7 day)
				GROUP BY visits.department,services_request.service_id";
		}


		else if ($dataLoad =="today") {			

			$sqlDepartmentWiseServices = "SELECT COUNT(services_request.id) AS 'Total Services',services.name AS 'Service Name',department.department AS 'Department Name' ,visits.department AS 'Department Id'  
				FROM services_request
				LEFT JOIN visits ON visits.id = services_request.visit_id
				LEFT JOIN services ON services.id = services_request.service_id
				LEFT JOIN department ON department.id = visits.department
				WHERE department.department IS NOT NULL AND
				visits.created_on >= unix_timestamp(curdate())
				GROUP BY visits.department,services_request.service_id";
		}
		else  {

			$sqlDepartmentWiseServices = "SELECT COUNT(services_request.id) AS 'Total Services',services.name AS 'Service Name',department.department AS 'Department Name' ,visits.department AS 'Department Id'  
				FROM services_request
				LEFT JOIN visits ON visits.id = services_request.visit_id
				LEFT JOIN services ON services.id = services_request.service_id
				LEFT JOIN department ON department.id = visits.department
				WHERE department.department IS NOT NULL AND	visits.created_on BETWEEN 
				UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59')
				GROUP BY visits.department,services_request.service_id";
		}		
		
		$rowsDepartmentWiseServices  = array();
	    $resultDepartmentWiseServices  = mysqli_query($conn,$sqlDepartmentWiseServices);
	    while ($r = mysqli_fetch_assoc($resultDepartmentWiseServices)) {
	        $rowsDepartmentWiseServices[] = $r;
	    }


	    echo json_encode($rowsDepartmentWiseServices);
	}
?>