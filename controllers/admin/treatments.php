<?php
/*****************************************************************************************************
 * File Name    :   treatment.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages  treatment data
 ****************************************************************************************************/
 
	session_start(); // session start
    $operation = "";
	$labName ="";
	$description = "";
	$date = new DateTime();
	include 'config.php';

	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
		exit();
	}

	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}

	//Operation for save the data
	if($operation == "save")			
	{
		if (isset($_POST['price'])) {
		$price=$_POST['price'];
		}
		if (isset($_POST['treatment'])) {
		$treatment=$_POST['treatment'];
		}		
	
			
		$sql = "INSERT INTO treatments(price,treatment,created_on,created_by) VALUES('".$price."','".$treatment."',".$date->getTimestamp().",'".$userId."')";
		$result= mysqli_query($conn,$sql);  
		echo $result;
		
	} 
	
	//Opearation for update the data
	if($operation == "update")			
	{
		if (isset($_POST['price'])) {
			$price=$_POST['price'];
		}
		if (isset($_POST['treatments'])) {
			$treatment=$_POST['treatments'];
		}
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}		
	
		$sql = "UPDATE treatments SET price= '".$price."',treatment='".$treatment."' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//Operation for inactive the data
	if($operation == "delete")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}		
	
		$sql = "UPDATE treatments SET status= 'I'  WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}	
	
	//Operation for restore the data
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		
		$sql = "UPDATE treatments SET status= 'A' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//Operation for show the active data
	if($operation == "show"){
		
		$query= "select * from treatments where status = 'A'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}	
	
	//Operation for show the inactive data
	if($operation == "showInActive"){
		
		$query= "select * from treatments where status = 'I'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
?>