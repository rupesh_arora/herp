<?php
/*
 * File Name    :   items.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   23rd feb, 2016
 * Description  :   This page use for load,save,update,delete,resotre details from database        
 */

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

$operation        = "";
$diseasesCategory = "";
$description      = "";
$createdate       = new DateTime();
include 'config.php';					// import database connection file
if (isset($_POST['operation'])) {		// opeartion come from js 
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
    
}
if ($operation == "showcategory") // show category in category selection box
    {
    $query  = "SELECT id,category FROM item_categories where status = 'A' AND id != '5' ORDER BY category";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "checkdiseases") // check duplicacy
    {
    $code        = $_POST['Code'];
    $name        = $_POST['Name'];
    $category_id = $_POST['category_Id'];
    $id          = $_REQUEST['Id'];
    if ($id != "") {
        $query = "Select code from items where code = '" . $code . "' and id != " . $id . "";
    } else {
        $query = "Select code from items where code = '" . $code . "'";
    }
    $sqlSelect = mysqli_query($conn, $query);
    $numrows   = mysqli_num_rows($sqlSelect);
    if ($numrows > 0) {
        echo "1";
    } else {
        echo "0";
    }
}
if ($operation == "save") // save details
    {
    if (isset($_POST['Code'])) {
        $code = $_POST['Code'];
    }
    if (isset($_POST['Name'])) {
        $name = $_POST['Name'];
    }
    $category_id = $_POST['category_Id'];
    $description = $_POST['description'];
	
    $sql    = "INSERT INTO items(name,code,item_category_id,description,created_on,updated_on,created_by,updated_by,status) 
	VALUES('" . $name . "','" . $code . "','" . $category_id . "','" . $description . "',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."','A')";
	
    $result = mysqli_query($conn, $sql);
    echo $result;
}
if ($operation == "show") { // show active data
    
    $query        = "select items.* ,item_categories.category  
	from items LEFT JOIN item_categories ON items.item_category_id=item_categories.id 
	where items.status='A' and item_categories.status='A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
    
}
// opertaion form update
if ($operation == "update") {
    if (isset($_POST['Code'])) {
        $code = $_POST['Code'];
    }
    if (isset($_POST['Name'])) {
        $name = $_POST['Name'];
    }
    $category_id = $_POST['category_Id'];
    $description = $_POST['description'];
    
    if (isset($_POST['Id'])) {
        $id = $_POST['Id'];
    }
    
    $sql    = "update items set code= '" . $code . "' , name = '" . $name . "', item_category_id='" . $category_id . "',description='" . $description . "',updated_on=UNIX_TIMESTAMP(),updated_by='".$userId."' where id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}
//  operation for delete
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE items SET status= 'I' where id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}
//When checked box is check
if ($operation == "checked") {
    
    $query        = "select items.*,item_categories.category 
	from items LEFT JOIN item_categories ON items.item_category_id=item_categories.id 
	where items.status='I' or item_categories.status='I'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}
if ($operation == "restore") // for restore details 
    {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $category = $_POST['category'];
    $sql      = "UPDATE items SET status= 'A'  WHERE  id = '" . $id . "'";
    $result   = mysqli_query($conn, $sql);
    if ($result == 1) {
        $query  = "SELECT status from item_categories where id='" . $category . "'";
        $result = mysqli_query($conn, $query);
        while ($r = mysqli_fetch_assoc($result)) {
            if ($r['status'] == 'A') {
                echo "1";
            } else {
                echo "0";
            }
        }
    }
}
?>