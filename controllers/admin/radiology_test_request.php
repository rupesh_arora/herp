<?php
/*File Name  :   radiology_test_request.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page manages  all the lab test request*/

session_start();
$visitIdTest ="";
$visitId ="";

/*include config file*/
include 'config.php';

/*Get user id from session table*/
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
}

/*operation to save data*/
if ($operation == "save") {
    $data      = json_decode($_POST['data']); //decode array
    $patientId = $_POST['patientId'];
    $visitId   = $_POST['visitId'];
    $tblName   = $_POST['tblName'];
    $fkColName = $_POST['fkColName'];
    $totalCurrentReqBill = $_POST['totalCurrentReqBill'];
    $copayValue = $_POST['copayValue'];
    $copayType = $_POST['copayType'];
    
    $testResult  = '';
    $testId      = '';
    $insertQuery = '';
    $result      = '';
    
    /*query to isert data*/
    $insertQuery = "INSERT INTO " . $tblName . " (ledger_id,gl_account_id,account_payable_id,patient_id,visit_id," . $fkColName . ",request_date,pay_status,test_status,cost, status,request_type) ";
    $insertQuery .= "VALUES ";

    /*query to insert bill*/
    $insertBill = "INSERT INTO patient_radiology_bill (ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,radiology_test_id,charge,status,
            created_by,updated_by,created_on,updated_on,payment_mode)";
    $insertBill .= "VALUES";

    $counter = 0;
    $j=0;
    $length = count($data);
    for ($i = 0; $i < $length; $i++) { 

        $revisedCost = $data[$i][4];
        $id = $data[$i][6];
        $requestType = $data[$i][7];
        $paymentMethod = $data[$i][8];
        $restrictBill = $data[$i][9];

        //if insurance mode is insurance then check copay 
        if (strtolower($paymentMethod) == "insurance") {

            if (strtolower($copayType) == "none") {
                $billStatus = 'paid';           
            }
            else {
                if ($copayValue == 0) {
                    $billStatus = 'paid';
                }
                else{
                    $billStatus = 'unpaid';
                }
            }
        }
        else {
            $billStatus =  "unpaid";
        }

        $query  = "SELECT ledger_id,gl_account_id,account_payable_id FROM radiology_tests WHERE id = " . $id . "";
        $result = mysqli_query($conn, $query);
        $ledgerId;
        $glAccountId;
        $accountPayable;
        while ($r = mysqli_fetch_assoc($result)) {
            $ledgerId = $r['ledger_id'];
            $glAccountId = $r['gl_account_id'];
            $accountPayable = $r['account_payable_id'];
        }

        
        if ($i > 0) {
            $insertQuery .= ",";
        }
        if ($j > 0 && $requestType == "internal" && $restrictBill == 'notrestrictBill') {
            $insertBill .= ",";
        }

        /*Check size to get if already result sent then show always paid*/
        if ($restrictBill == 'restrictBill'){
            $insertQuery .= "(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . "," . $patientId . "," . $visitId . "," . $id . ",UNIX_TIMESTAMP(),
            'Paid','Pending'," . $revisedCost . ",'A','".$requestType."')";
        }
        else{
            $insertQuery .= "(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . "," . $patientId . "," . $visitId . "," . $id . ",UNIX_TIMESTAMP(),
            '".$billStatus."','Pending'," . $revisedCost . ",'A','".$requestType."')";
        }

        if ($requestType == "internal" && $restrictBill == 'notrestrictBill'){
            
            $insertBill .= "(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . ",'".$patientId."','".$visitId."','".$id."','".$revisedCost."',
                '".$billStatus."','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),
                '".$paymentMethod."')";
                $j++;
        }
    }
    if (strlen($insertBill) > 170) {
        $joinQry = $insertQuery. ";" .$insertBill;
    }
    else{
        $joinQry = $insertQuery;
    }
    
    $result = mysqli_multi_query($conn,rtrim($joinQry,','));
    if ($result == 1) {
        echo $result;
    }
    else{
        echo "0";
    }
}

/*operation to count total no. of request for old test*/
if ($operation == "search") {
    $patientId = $_POST['patientId'];
    $visitId   = $_POST['visitId'];
    $tblName   = $_POST['tblName'];
    
    $searchQuery  = "SELECT COUNT(*) as total FROM " . $tblName . " WHERE patient_id = " . $patientId . " AND visit_id = " . $visitId;
    $searchResult = mysqli_query($conn, $searchQuery);
    
    $data = mysqli_fetch_assoc($searchResult);
    echo $data['total'];
}

/*operation to show only those radiology for which patient test result sent*/
if ($operation == "showRadiologyTest") {
	if (isset($_POST['visitIdTest'])) {
		$visitIdTest = $_POST["visitIdTest"];
	}
    $query   = "SELECT radiology_tests.id,radiology_tests.name,radiology_tests.fee FROM radiology_tests WHERE radiology_tests.id NOT IN 
            (SELECT radiology_test_request.radiology_test_id FROM radiology_test_request WHERE 
                radiology_test_request.visit_id ='" . $visitIdTest . "' AND radiology_test_request.status = 'A' AND radiology_test_request.test_status !='resultsent') 
            AND radiology_tests.status ='A' ORDER BY name";
    $result  = mysqli_query($conn, $query);
    $rows    = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

?>