<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	$operation = "";
	include 'config.php';

	$date      = new DateTime();
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}
	
	if (isset($_FILES["file"]["type"])) // use for image uploading
	{	
	    $id = $_POST['id'];

	     if($id != ""){
	     	$query = "SELECT id FROM asset WHERE status = 'A' ORDER BY id DESC  LIMIT 1";
	     	$result = mysqli_query($conn, $query);
		    $rows   = array();
		    while ($r = mysqli_fetch_assoc($result)) {
		        $id = $r['id'];
		    }
		}
	     	$validextensions = array(
		        "jpeg",
		        "jpg",
		        "png",
		        "gif"
		    );
		    $temporary       = explode(".", $_FILES["file"]["name"]);
		    
		    $extension = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION); // getting extension
		    $imagename = pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME) . $date->getTimestamp(); // getting a file name 
		    $str = 'abcdef';
			$imagename = str_shuffle($str). $date->getTimestamp();
		    $image_name = $imagename . "." . $extension;
		    $name       = str_replace(" ", "", $image_name); // remove space
		    
		    $file_extension = end($temporary);
		    if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")) && ($_FILES["file"]["size"] < 2002000) //Approx. 2mb files can be uploaded.
		        && in_array($file_extension, $validextensions)) {
		        if ($_FILES["file"]["error"] > 0) {
		            echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
		        } else {
		            if (file_exists("../../upload_images/asset_image/" . $name)) {
		                echo "";
		            } else {
		                $sourcePath = $_FILES["file"]["tmp_name"]; // Storing source path of the file in a variable
		                $targetPath = "../../upload_images/asset_image/" . $name; // Target path where file is to be stored
		                move_uploaded_file($sourcePath, $targetPath); // Moving Uploaded file
		                
		                
		                $sql    = "UPDATE asset set image_path = '".$name."' WHERE id = '".$id."' ";
						$result = mysqli_query($conn, $sql);
						echo $result;
		                
		            }
		        }
		    } else {
		        echo "invalid file";
		    }
	     /*}*/

	    
	}

	/*if(isset($_FILES['filename']['name'])) {
		$images_arr = array();
		for ($i = 0; $i < count($_FILES['filename']['name']); $i++) {
			//upload and stored images
			$target_dir = "../../upload_images/asset_image/";
			
			$imageString = explode(".",$_FILES['filename']['name'][$i]);
			$imagename = $imageString[0]. $date->getTimestamp();
			$extension = $imageString[1];
			
			$image_name = $imagename . "." . $extension;
			$name       = str_replace(" ", "", $image_name); // remove space
			$target_file = $target_dir.$name;
			
			if(move_uploaded_file($_FILES['filename']['tmp_name'][$i],$target_file)){
				$images_arr[]= $name;
			}
		}
		echo json_encode($images_arr);
	}*/
	
	if ($operation == "showRoom") { // show insurance company type
	    $query = "SELECT `id`,`number` FROM ward_room WHERE status = 'A'";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "showAssetClass") { // show insurance company type
	    $query = "SELECT id,asset_class_name FROM asset_class WHERE status = 'A'";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "showAssetsubClass") { // show insurance company type
		$subClass = $_POST['subClass'];
	    $query = "SELECT id,sub_class_name FROM asset_subclass WHERE  `class` = '".$subClass."' AND  status = 'A'";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	

	if ($operation == "searchManufacturer") { // show insurance company type
		
		$name = $_POST['name'];

	    $query = "SELECT id,name FROM manufacturer WHERE  `name` LIKE '%".$name."%' AND `type` = '0' AND  status = 'A'";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "searchBrandName") { // show insurance company type

		$name = $_POST['name'];
		if (isset($_POST['parentId'])) {
			$parentId = $_POST["parentId"];
		}
		else{
			$parentId = "";
		}

	    $query = "SELECT id,name FROM manufacturer WHERE  `name` LIKE '%".$name."%' AND parent_id = '".$parentId."' AND `type` = '1' AND  status = 'A'";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "searchModal") { // show insurance company type
		
		$name = $_POST['name'];

		if (isset($_POST['parentId'])) {
			$parentId = $_POST["parentId"];
		}
		else{
			$parentId = "";
		}

	    $query = "SELECT id,name FROM manufacturer WHERE  `name` LIKE '%".$name."%' AND parent_id = '".$parentId."' AND `type` = '2' AND  status = 'A'";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "showAssetLocation") { // show insurance company type
	    $query = "SELECT id,`location` FROM asset_location WHERE status = 'A'";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "save") {
		$manufacturerId = '';
		$brandNameId = '';
		$modalId = '';
		if (isset($_POST['manufacturerId'])) {
			$manufacturerId = $_POST["manufacturerId"];
		}
		if (isset($_POST['manufacturerName'])) {
			$manufacturerName = $_POST["manufacturerName"];
		}
		if (isset($_POST['brandNameId'])) {
			$brandNameId = $_POST["brandNameId"];
		}
		if (isset($_POST['brandName'])) {
			$brandName = $_POST["brandName"];
		}
		if (isset($_POST['modalId'])) {
			$modalId = $_POST["modalId"];
		}
		if (isset($_POST['modalName'])) {
			$modalName = $_POST["modalName"];
		}
		if (isset($_POST['assetNo'])) {
			$assetNo = $_POST["assetNo"];
		}
		if (isset($_POST['barcode'])) {
			$barcode = $_POST["barcode"];
		}
		if (isset($_POST['description'])) {
			$description = $_POST["description"]; 
		}
		if (isset($_POST['advanceDescription'])) {
			$advanceDescription = $_POST["advanceDescription"];
		}
		if (isset($_POST['assetClass'])) {
			$assetClass = $_POST["assetClass"];
		}
		if (isset($_POST['assetSubClass'])) {
			$assetSubClass = $_POST["assetSubClass"];
		}
		if (isset($_POST['assetLocation'])) {
			$assetLocation = $_POST["assetLocation"];
		}
		if (isset($_POST['serialNo'])) {
			$serialNo = $_POST["serialNo"];
		}
		if (isset($_POST['licenceNo'])) {
			$licenceNo = $_POST["licenceNo"];
		}
		if (isset($_POST['assignTo'])) {
			$assignTo = $_POST["assignTo"];
		}
		if (isset($_POST['room'])) {
			$room = $_POST["room"];  
		}
		if (isset($_POST['purchaseDate'])) {
			$purchaseDate = $_POST["purchaseDate"];
		}
		if (isset($_POST['purchaseValue'])) {
			$purchaseValue = $_POST["purchaseValue"];
		}


		$query = "SELECT id FROM asset WHERE asset_no = '".$assetNo."'";
     	$result = mysqli_query($conn, $query);
	    $countAsset   = '';
	    while ($r = mysqli_fetch_assoc($result)) {
	        $countAsset = $r['id'];
	    }
	    $query = "SELECT id FROM asset WHERE asset_barcode = '".$barcode."'";
     	$result = mysqli_query($conn, $query);
	    $countBarcode   = '';
	    while ($r = mysqli_fetch_assoc($result)) {
	        $countBarcode = $r['id'];
	    }
	    if($countAsset == ''){

	    	if($countBarcode == ''){

	    		if($manufacturerId == ''){
					$query = "INSERT INTO manufacturer (name,type,created_on,updated_on,created_by,updated_by) VALUES('".$manufacturerName."','0',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
					$result = mysqli_query($conn, $query);
					$manufacturerId = mysqli_insert_id($conn);
				}
				if($brandNameId == ''){
					$query = "INSERT INTO manufacturer (name,type,parent_id,created_on,updated_on,created_by,updated_by) VALUES('".$brandName."','1','".$manufacturerId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
					$result = mysqli_query($conn, $query);
					$brandNameId = mysqli_insert_id($conn);
				}
				if($modalId == ''){
					$query = "INSERT INTO manufacturer (name,type,parent_id,created_on,updated_on,created_by,updated_by) VALUES('".$modalName."','2','".$brandNameId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
					$result = mysqli_query($conn, $query);
					$modalId = mysqli_insert_id($conn);
				}

	    		$sql = "INSERT INTO `asset`(`asset_no`, `asset_barcode`, `asset_description`, `asset_advance_description`, `asset_class_id`, `asset_sub_class_id`, `asset_location_id`, `manufacturer_id`, `brand_name_id`, `modal_id`, `serial_no`, `licence_no`, `assign_to`, `room_id`, `purchase_date`, `purchase_value`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES('".$assetNo."','".$barcode."','".$description."','".$advanceDescription."','".$assetClass."','".$assetSubClass."','".$assetLocation."','".$manufacturerId."','".$brandNameId."','".$modalId."','".$serialNo."','".$licenceNo."','".$assignTo."','".$room."','".$purchaseDate."','".$purchaseValue."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
		    
				$result = mysqli_query($conn, $sql);
				if($result){
					echo $Lastid = mysqli_insert_id($conn);
				}
	    	}
	    	else{
	    		echo 'Barcode';
	    	}

	    }
	    else{
	    	echo 'Asset';
	    }

		
		
		
	}

	
	if ($operation == "update") // update data
	{
	    
	    $id = $_POST['id'];
	    $manufacturerId = '';
		$brandNameId = '';
		$modalId = '';
		if (isset($_POST['manufacturerId'])) {
			$manufacturerId = $_POST["manufacturerId"];
		}
		if (isset($_POST['manufacturerName'])) {
			$manufacturerName = $_POST["manufacturerName"];
		}
		if (isset($_POST['brandNameId'])) {
			$brandNameId = $_POST["brandNameId"];
		}
		if (isset($_POST['brandName'])) {
			$brandName = $_POST["brandName"];
		}
		if (isset($_POST['modalId'])) {
			$modalId = $_POST["modalId"];
		}
		if (isset($_POST['modalName'])) {
			$modalName = $_POST["modalName"];
		}
		if (isset($_POST['assetNo'])) {
			$assetNo = $_POST["assetNo"];
		}
		if (isset($_POST['barcode'])) {
			$barcode = $_POST["barcode"];
		}
		if (isset($_POST['description'])) {
			$description = $_POST["description"]; 
		}
		if (isset($_POST['advanceDescription'])) {
			$advanceDescription = $_POST["advanceDescription"];
		}
		if (isset($_POST['assetClass'])) {
			$assetClass = $_POST["assetClass"];
		}
		if (isset($_POST['assetSubClass'])) {
			$assetSubClass = $_POST["assetSubClass"];
		}
		if (isset($_POST['assetLocation'])) {
			$assetLocation = $_POST["assetLocation"];
		}
		if (isset($_POST['serialNo'])) {
			$serialNo = $_POST["serialNo"];
		}
		if (isset($_POST['licenceNo'])) {
			$licenceNo = $_POST["licenceNo"];
		}
		if (isset($_POST['assignTo'])) {
			$assignTo = $_POST["assignTo"];
		}
		if (isset($_POST['room'])) {
			$room = $_POST["room"];  
		}
		if (isset($_POST['purchaseDate'])) {
			$purchaseDate = $_POST["purchaseDate"];
		}
		if (isset($_POST['purchaseValue'])) {
			$purchaseValue = $_POST["purchaseValue"];
		}

		if($manufacturerId == ''){
			$query = "INSERT INTO manufacturer (name,type,created_on,updated_on,created_by,updated_by) VALUES('".$manufacturerName."','0',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
			$result = mysqli_query($conn, $query);
			$manufacturerId = mysqli_insert_id($conn);
		}
		if($brandNameId == ''){
			$query = "INSERT INTO manufacturer (name,type,parent_id,created_on,updated_on,created_by,updated_by) VALUES('".$brandName."','1','".$manufacturerId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
			$result = mysqli_query($conn, $query);
			$brandNameId = mysqli_insert_id($conn);
		}
		if($modalId == ''){
			$query = "INSERT INTO manufacturer (name,type,parent_id,created_on,updated_on,created_by,updated_by) VALUES('".$modalName."','2','".$brandNameId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
			$result = mysqli_query($conn, $query);
			$modalId = mysqli_insert_id($conn);
		}
   	
	
		$sql    = "UPDATE `asset` SET `asset_description`='".$description."',`asset_advance_description`='".$advanceDescription."',`asset_class_id`='".$assetClass."',`asset_sub_class_id`='".$assetSubClass."',`asset_location_id`='".$assetLocation."',`manufacturer_id`='".$manufacturerId."',`brand_name_id`='".$brandNameId."',`modal_id`='".$modalId."',`serial_no`='".$serialNo."',`licence_no`='".$licenceNo."',`assign_to`='".$assignTo."',`room_id`='".$room."',`purchase_date`='".$purchaseDate."',`purchase_value`='".$purchaseValue."',`updated_on`=UNIX_TIMESTAMP(),`updated_by`='".$userId."' WHERE  id = '".$id."' ";			

		$result = mysqli_query($conn, $sql);
		echo $result;		

	}

	/*operation to show active data*/
if ($operation == "show") {
    
    $query        = "SELECT a.*,ac.asset_class_name,a_s.sub_class_name,al.location,
 			(SELECT name FROM manufacturer m1 WHERE m1.id = a.manufacturer_id ) AS manufacturer_name,
            (SELECT name FROM manufacturer m1 WHERE m1.id = a.brand_name_id) AS brand_name,
            (SELECT name FROM manufacturer m1 WHERE m1.id = a.modal_id) AS modal
 			FROM asset AS a 
			LEFT JOIN asset_class AS ac ON ac.id = a.asset_class_id  
			LEFT JOIN asset_subclass AS a_s ON a_s.id = a.asset_sub_class_id  
	        LEFT JOIN asset_location AS al ON al.id = a.asset_location_id 
	        LEFT JOIN manufacturer AS m ON m.id = a.manufacturer_id  
			WHERE a.status='A' AND ac.status='A' AND al.status='A' AND a_s.status='A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/*operation to show inactive data*/
if ($operation == "showInActive") {
    $query        = "SELECT a.*,ac.asset_class_name,a_s.sub_class_name,al.location,
 			(SELECT name FROM manufacturer m1 WHERE m1.id = a.manufacturer_id ) AS manufacturer_name,
            (SELECT name FROM manufacturer m1 WHERE m1.id = a.brand_name_id) AS brand_name,
            (SELECT name FROM manufacturer m1 WHERE m1.id = a.modal_id) AS modal
			 FROM asset AS a  
		LEFT JOIN asset_class AS ac ON ac.id = a.asset_class_id  
		LEFT JOIN asset_subclass AS a_s ON a_s.id = a.asset_sub_class_id  
        LEFT JOIN asset_location AS al ON al.id = a.asset_location_id 
        LEFT JOIN manufacturer AS m ON m.id = a.manufacturer_id  
		WHERE a.status='I' OR ac.status='I' OR al.status='I' OR a_s.status='I'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/*operation to delete data*/
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE asset SET status= 'I' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
	if($result){
		echo "1";
	}else{
		echo "0";
	}  
}

/*operation to restore data*/
if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE asset SET status= 'A' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql); 
   
        
    if ($result) {
        echo "Restored Successfully!!!";
    } 
    else {
        echo "It can not be restore!!!";
    }
}

?>