<?php
/*File Name  :   put_orders.php
Company Name :   Qexon Infotech
Created By   :   Kamesh Pathak
Created Date :   16th FEB, 2016
Description  :   This page manages all the orders of inventory*/

session_start(); //start session

if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
	exit();
}
/*include config file*/
include 'config.php';

$operation = "";

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
} //nothing

if ($operation == "showDataTableDetails") {

	
	$supplierId = $_POST['supplierId'];

	/*if ($customerId != '') {
		$sql = "SELECT cpd.quantity AS item_quantity,cpd.unit_price AS item_unitprice,cpd.tax as item_tax,cpd.discount AS item_discount,ci.total_bill AS invoice_net,cpd.id AS customer_product_id,p.name AS item_name,ci.id,cpd.total AS item_total ,(SELECT SUM(quantity*unit_price) FROM customer_product_details AS cpd_1
			LEFT JOIN customer_invoice AS ci_1 ON ci_1.id = cpd_1.customer_invoice_id
			LEFT JOIN customer AS c_1 ON c_1.id = ci_1.customer_id
			WHERE c_1.id = '".$customerId."' and ci_1.id = ci.id
			GROUP BY ci_1.id) AS invoice_total
			FROM customer AS c
			LEFT JOIN customer_invoice AS ci ON ci.customer_id = c.id 
			LEFT JOIN customer_product_details AS cpd ON cpd.customer_invoice_id = ci.id 
			LEFT JOIN products AS p ON p.id = cpd.product_id
			WHERE c.id= '".$customerId."' AND cpd.pay_status='unpaid'  ORDER BY ci.id ";
	}*/

	
	$sql = "SELECT received_orders.invoice_no AS id,chart_of_account.account_name,received_orders.account_payable,received_orders.id AS pk_table,items.name AS item_name,received_orders.unit_cost AS item_unitprice,received_orders.quantity AS item_quantity, items.id AS customer_product_id,'0.00' AS item_tax,received_orders.quantity*((received_orders.unit_cost * received_orders.discount)/100) AS item_discount, (received_orders.unit_cost * received_orders.quantity) - ((received_orders.unit_cost * received_orders.quantity) * received_orders.discount)/100 as item_total ,(SELECT SUM(ro.unit_cost * ro.quantity) from received_orders AS ro
			LEFT JOIN orders  AS ords ON ords.id  = ro.order_id
			WHERE ro.invoice_no = received_orders.invoice_no AND ords.seller_id = '".$supplierId."'
			GROUP BY ro.invoice_no) As invoice_total ,'' AS extra,			
			(SELECT SUM(ro_1.quantity*((ro_1.unit_cost * ro_1.discount)/100)) FROM received_orders as ro_1
			LEFT JOIN orders  AS ords_1 ON ords_1.id  = ro_1.order_id
			WHERE ro_1.invoice_no = received_orders.invoice_no AND ords_1.seller_id = '".$supplierId."'
         GROUP BY ro_1.invoice_no) AS invoice_discount,received_orders.gl_account ,received_orders.discount 
			FROM received_orders
			LEFT JOIN orders ON orders.id = received_orders.order_id
			LEFT JOIN items ON items.id = received_orders.item_id
			LEFT JOIN chart_of_account ON chart_of_account.id = received_orders.gl_account
			WHERE  orders.seller_id = '".$supplierId."' ";
	if (isset($_POST['saveSupplierInv'])) {
		$sql.=" AND received_orders.payment_voucher_status = 'unpaid'";
	}	
	$sql.="	ORDER BY id";
	
	$result = mysqli_query($conn,$sql);
	$rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
	echo json_encode($rows);
}

if ($operation == "showCOA") {

	$accPayable = $_POST['accPayable'];

	$query  = "SELECT id,account_name,is_parent,parent_account_no FROM chart_of_account WHERE status='A'";

	if ($accPayable != '') {
		$query.= " AND class = '3'";
	}

	$query.=" ORDER BY parent_account_no,is_parent ASC";


    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "showAccReceivable") {


	$query  = "SELECT id,account_name FROM chart_of_account WHERE status='A' AND class = '4'";

    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "showPaymentMode") {
	$ledgerId = $_POST['ledgerId'];

	$query  = "SELECT id, name FROM payment_mode WHERE ledger_type = '".$ledgerId."' AND status = 'A'	ORDER BY name";

    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "showPayeeName") {
	$ledgerId = $_POST['ledgerId'];
	$paymentMode = $_POST['paymentMode'];

	$query  = "SELECT payee_name FROM payment_mode 
		WHERE ledger_type = '".$ledgerId."' AND id = '".$paymentMode."' AND status = 'A'";

    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "saveCustomer_NonRegisterData") {
	$registerdPayeeId = $_POST['registerdPayeeId'];
	$registeredPayeeType = $_POST['registeredPayeeType'];
	$ledgerId = $_POST['ledgerId'];
	$paymentMode = $_POST['paymentMode'];
	$cheque_no = $_POST['cheque_no'];
	$invoiceNo = $_POST['invoiceNo'];
	$saveNetAmount = $_POST['saveNetAmount'];
	$payeeName = $_POST['payeeName'];
	$description = $_POST['description'];
	$accountPayableId = $_POST['accountPayableId'];
	$itemDetails = json_decode($_POST['itemDetails']);

	$insertMainDetails = "INSERT INTO payment_voucher (registered_payee_id,registered_payee_type,ledger_id,payment_mode,cheque_no,amount,payee_name,description,created_by,updated_by,created_on,updated_on,voucher_disbursed_date,disbursed_by) VALUES ('".$registerdPayeeId."','".$registeredPayeeType."','".$ledgerId."','".$paymentMode."','".$cheque_no."','".$saveNetAmount."','".$payeeName."','".$description."','".$userId."','".$userId."',UNIX_TIMESTAMP(), UNIX_TIMESTAMP(),'',NULL)";

	$resultMain = mysqli_query($conn,$insertMainDetails);
	$last_id = mysqli_insert_id($conn);

	if (!empty($itemDetails)) {
		$insertSubDetails = "INSERT INTO payment_voucher_item (gl_account_id,net_amount,discount,tax_id,payment_voucher_id,remarks,created_by,updated_by,created_on,updated_on,invoice_no,ap_id) VALUES ";

		$subInsertSubDetails = '';
		$v = '';
		$subItemCounter = 0;
		foreach ($itemDetails as $key => $value) {
			
			$GLAccountId    = $value->GLAccountId;
	        $netAmount      = $value->netAmount;
	        $remarks        = $value->remarks;
	        $taxArray       = $value->taxArray;
	        $discount       = $value->discount;
	        
	        if (!empty($taxArray )) {
		        foreach ($taxArray as $k => $v) {
		        	if ($subItemCounter > 0) {
		        		$subInsertSubDetails.= ',';
		        	}
		        	$subInsertSubDetails.= "('".$GLAccountId."','".$netAmount."','".$discount."','".$v."','".$last_id."','".$remarks."','".$userId."','".$userId."',UNIX_TIMESTAMP(), UNIX_TIMESTAMP(),'','".$accountPayableId."')";
		        	$subItemCounter++;
		        }
		    }

		    else{

		    	if ($subItemCounter > 0) {
	        		$subInsertSubDetails.= ',';
	        	}

		    	$subInsertSubDetails.= "('".$GLAccountId."','".$netAmount."','".$discount."','NULL','".$last_id."','".$remarks."','".$userId."','".$userId."',UNIX_TIMESTAMP(), UNIX_TIMESTAMP(),'','".$accountPayableId."')";

		        $subItemCounter++;
		    }
		}

		$subCompleteQry = $insertSubDetails.$subInsertSubDetails;
		$resultMain = mysqli_query($conn,$subCompleteQry);
		echo $last_id;
	}

}


if ($operation == "updateCustomer_NonRegisterData") {
	$registerdPayeeId = $_POST['registerdPayeeId'];
	$registeredPayeeType = $_POST['registeredPayeeType'];
	$ledgerId = $_POST['ledgerId'];
	$paymentMode = $_POST['paymentMode'];
	$cheque_no = $_POST['cheque_no'];
	$invoiceNo = $_POST['invoiceNo'];
	$saveNetAmount = $_POST['saveNetAmount'];
	$payeeName = $_POST['payeeName'];
	$description = $_POST['description'];
	$itemDetails = json_decode($_POST['itemDetails']);
	$id = $_POST['id'];

	//fisrtly delete data from payment voucher item
	$deleteVoucherItems = "DELETE FROM payment_voucher_item WHERE payment_voucher_id = '".$id."'";

	$updateMainDetails = "UPDATE payment_voucher SET ledger_id = '".$ledgerId."', payment_mode ='".$paymentMode."',cheque_no='".$cheque_no."',amount = '".$saveNetAmount."', payee_name = '".$payeeName."', description = '".$description."',updated_by = '".$userId."',updated_on = UNIX_TIMESTAMP() WHERE id = '".$id."'";

	$joinQry = $deleteVoucherItems.';'.$updateMainDetails;
	mysqli_multi_query($conn,$joinQry);	
	
	if (!empty($itemDetails)) {
		$insertSubDetails = "INSERT INTO payment_voucher_item (gl_account_id,net_amount,discount,tax_id,payment_voucher_id,remarks,created_by,updated_by,created_on,updated_on,invoice_no) VALUES ";

		$subInsertSubDetails = '';
		$v = '';
		$subItemCounter = 0;
		foreach ($itemDetails as $key => $value) {
			
			$GLAccountId    = $value->GLAccountId;
	        $netAmount      = $value->netAmount;
	        $remarks        = $value->remarks;
	        $taxArray       = $value->taxArray;
	        $discount       = $value->discount;

	        if (!empty($taxArray)) {
		        foreach ($taxArray as $k => $v) {
		        	if ($subItemCounter > 0) {
		        		$subInsertSubDetails.= ',';
		        	}
		        	$subInsertSubDetails.= "('".$GLAccountId."','".$netAmount."','".$discount."','".$v."','".$id."','".$remarks."','".$userId."','".$userId."',UNIX_TIMESTAMP(), UNIX_TIMESTAMP(),'')";
		        	$subItemCounter++;
		        }
		    }
		    else{

		    	if ($subItemCounter > 0) {
	        		$subInsertSubDetails.= ',';
	        	}

		    	$subInsertSubDetails.= "('".$GLAccountId."','".$netAmount."','".$discount."','".$v."','".$id."','".$remarks."','".$userId."','".$userId."',UNIX_TIMESTAMP(), UNIX_TIMESTAMP())";

		        $subItemCounter++;
		    }
		}

		$subCompleteQry = $insertSubDetails.$subInsertSubDetails;
		if (!(mysqli_ping($conn))) {
	        include 'config.php';
	    }
		$resultMain = mysqli_query($conn,$subCompleteQry);
		echo $resultMain;
	}

}

if ($operation == "saveRegisteredData") {
	
	$ledgerId = $_POST['ledgerId'];
	$chequeNo = $_POST['chequeNo'];
	$payeeName = $_POST['payeeName'];
	$paymentMode = $_POST['paymentMode'];
	$registerdPayeeId = $_POST['registerdPayeeId'];
	$description = $_POST['description'];
	$registeredPayeeType = $_POST['registeredPayeeType'];
	$productDetail = json_decode($_POST['productDetail']);
	$receiptDetail = json_decode($_POST['receiptDetail']);
	$saveNetAmount = $_POST['saveNetAmount'];
	$glAccountId = $_POST['glAccountId'];
	$accountPayableId = $_POST['account_payable_id'];

	$insertMainDetails = "INSERT INTO payment_voucher (registered_payee_id,registered_payee_type,ledger_id,payment_mode,cheque_no,amount,payee_name,description,created_by,updated_by,created_on,updated_on,voucher_disbursed_date,disbursed_by) VALUES ('".$registerdPayeeId."','".$registeredPayeeType."','".$ledgerId."','".$paymentMode."',
	'".$chequeNo."','".$saveNetAmount."','".$payeeName."','".$description."','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'',NULL)";
	$resultMain = mysqli_query($conn,$insertMainDetails);
	$last_id = mysqli_insert_id($conn);

	if (!empty($productDetail)) {

		$insertSubDetails = "INSERT INTO payment_voucher_item (gl_account_id,net_amount,discount,tax_id,payment_voucher_id,invoice_no,ap_id,remarks,created_by,updated_by,created_on,updated_on) VALUES ";

		$voucherCounter = 0;
		$subInsertVoucherQry = '';
		foreach ($productDetail as $value) {
			$aPId    = $value->aPId;
	        $discount        = $value->discount;
	        $glAccountId     = $value->glAccountId;
	        $invoiceId     = $value->invoiceId;
	        $netAmount     = $value->netAmount;

	        if ($voucherCounter > 0) {
	        	$subInsertVoucherQry.=',';
	        }

	        $subInsertVoucherQry.="('".$glAccountId."','".$netAmount."','".$discount."',NULL,'".$last_id."','".$invoiceId."','".$aPId."','','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";

	        $voucherCounter++;
		}
		$completeVoucherQry = $insertSubDetails.$subInsertVoucherQry;
	}

	if ($completeVoucherQry) {
		$updateReceiveOrder = "UPDATE received_orders SET payment_voucher_status = 'paid' WHERE ";
		$productCounter = 0;
		$subUpdateReceiveQry = '';

		foreach ($productDetail as $value) {
			$mainId    = $value->mainId;

			if ($productCounter == 0) {
				$subUpdateReceiveQry.= "id = '".$mainId."'";
			}
			else {
				$subUpdateReceiveQry.= " OR id = '".$mainId."'";
			}
			$productCounter++;
		}
		$completeProductQry = $updateReceiveOrder.$subUpdateReceiveQry;
	}

	 $combinedQry = $completeVoucherQry.';'.$completeProductQry;

	$resultQry = mysqli_multi_query($conn,$combinedQry);
	echo $last_id;
}

if ($operation == "updateRegisteredData") {
	
	$ledgerId = $_POST['ledgerId'];
	$chequeNo = $_POST['chequeNo'];
	$payeeName = $_POST['payeeName'];
	$paymentMode = $_POST['paymentMode'];
	$registerdPayeeId = $_POST['registerdPayeeId'];
	$description = $_POST['description'];
	$registeredPayeeType = $_POST['registeredPayeeType'];
	$productDetail = json_decode($_POST['productDetail']);
	$receiptDetail = json_decode($_POST['receiptDetail']);
	$sellerId = $_POST['sellerId'];
	$id = $_POST['id'];
	$saveNetAmount = $_POST['saveNetAmount'];
	$glAccountId = $_POST['glAccountId'];


	//fisrtly delete data from payment voucher item
	$deleteVoucherItems = "DELETE FROM payment_voucher_item WHERE payment_voucher_id = '".$id."'";
	
	$resetReceiveOrderStatus = "UPDATE received_orders SET payment_voucher_status = 'unpaid' WHERE received_orders.order_id IN(SELECT id FROM orders WHERE orders.seller_id = '".$sellerId."' )";

	$updateMainDetails = "UPDATE payment_voucher SET ledger_id = '".$ledgerId."', payment_mode ='".$paymentMode."',cheque_no='".$chequeNo."',amount = '".$saveNetAmount."', payee_name = '".$payeeName."', description = '".$description."',updated_by = '".$userId."',updated_on = UNIX_TIMESTAMP() WHERE id = '".$id."'";
	
	$joinQry = $deleteVoucherItems.';'.$resetReceiveOrderStatus.';'.$updateMainDetails;
	mysqli_multi_query($conn,$joinQry);


	if (!empty($receiptDetail)) {

		$insertSubDetails = "INSERT INTO payment_voucher_item (gl_account_id,net_amount,discount,tax_id,payment_voucher_id,invoice_no,ap_id,remarks,created_by,updated_by,created_on,updated_on) VALUES ";

		$voucherCounter = 0;
		$subInsertVoucherQry = '';
		foreach ($productDetail as $value) {
			$aPId    = $value->aPId;
	        $discount        = $value->discount;
	        $glAccountId     = $value->glAccountId;
	        $invoiceId     = $value->invoiceId;
	        $netAmount     = $value->netAmount;

	        if ($voucherCounter > 0) {
	        	$subInsertVoucherQry.=',';
	        }

	        $subInsertVoucherQry.="('".$glAccountId."','".$netAmount."','".$discount."',NULL,'".$id."','".$invoiceId."','".$aPId."','','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";

	        $voucherCounter++;
		}
		$completeVoucherQry = $insertSubDetails.$subInsertVoucherQry;
	}

	if ($completeVoucherQry) {
		$updateReceiveOrder = "UPDATE received_orders SET payment_voucher_status = 'paid' WHERE ";
		$productCounter = 0;
		$subUpdateReceiveQry = '';

		foreach ($productDetail as $value) {
			$mainId    = $value->mainId;

			if ($productCounter == 0) {
				$subUpdateReceiveQry.= "id = '".$mainId."'";
			}
			else {
				$subUpdateReceiveQry.= " OR id = '".$mainId."'";
			}
			$productCounter++;
		}
		$completeProductQry = $updateReceiveOrder.$subUpdateReceiveQry;
	}

	$combinedQry = $completeVoucherQry.';'.$completeProductQry;
	if (!(mysqli_ping($conn))) {
        include 'config.php';
    }
	$resultQry = mysqli_multi_query($conn,$combinedQry);
	echo $resultQry;
}


if ($operation == "show") {
	$sql = "SELECT payment_voucher.id,payment_voucher.payee_name,ledger.name AS ledger,payment_voucher.amount,payment_mode.name AS payment_mode,payment_voucher.cheque_no,payment_voucher.ledger_id ,payment_mode.id AS payment_mode_id,
		payment_voucher.registered_payee_id,payment_voucher.registered_payee_type,DATE_FORMAT(FROM_UNIXTIME(payment_voucher.updated_on), '%Y-%m-%d') AS voucher_date,payment_voucher.voucher_disbursement
		FROM payment_voucher
		LEFT JOIN ledger ON ledger.id = payment_voucher.ledger_id
		LEFT JOIN payment_mode ON payment_mode.id = payment_voucher.payment_mode
		WHERE payment_voucher.`status` = 'A' ORDER BY voucher_disbursement DESC,id";

		$result       = mysqli_query($conn, $sql);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    /*JSON encode*/
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
}

if ($operation == "showInactive") {
	$sql = "SELECT payment_voucher.id,payment_voucher.payee_name,ledger.name AS ledger,payment_voucher.amount,payment_mode.name AS payment_mode,payment_voucher.cheque_no,payment_voucher.ledger_id ,payment_mode.id AS payment_mode_id,
		payment_voucher.registered_payee_id,payment_voucher.registered_payee_type,DATE_FORMAT(FROM_UNIXTIME(payment_voucher.updated_on), '%Y-%m-%d') AS voucher_date,payment_voucher.voucher_disbursement
		FROM payment_voucher
		LEFT JOIN ledger ON ledger.id = payment_voucher.ledger_id
		LEFT JOIN payment_mode ON payment_mode.id = payment_voucher.payment_mode
		WHERE payment_voucher.`status` = 'I'";

		$result       = mysqli_query($conn, $sql);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    /*JSON encode*/
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
}

if ($operation =="showSupplierDetail") {

	$supplierId = $_POST['supplierId'];

	$query = "SELECT name,firm_name,email FROM seller WHERE id = '".$supplierId."' AND status = 'A'";

	$result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation =="showCustomerDetail") {

	$customerId = $_POST['customerId'];

	$query = "SELECT CONCAT(first_name,' ',last_name) AS name,email FROM customer WHERE id = '".$customerId."' AND status = 'A'";

	$result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "showPaymentVoucherItem") {

	$paymentVoucherId = $_POST['paymentVoucherId'];

	$query = "SELECT payment_voucher_item.gl_account_id,payment_voucher_item.net_amount,payment_voucher_item.discount,payment_voucher_item.remarks,chart_of_account.account_name,GROUP_CONCAT(payment_voucher_item.tax_id) AS tax_id,
		GROUP_CONCAT(tax_type.tax_name) AS tax_name,GROUP_CONCAT(tax_type.tax_rate) AS tax_rate FROM payment_voucher_item
		LEFT JOIN chart_of_account ON chart_of_account.id = payment_voucher_item.gl_account_id
		LEFT JOIN tax_type ON tax_type.id = payment_voucher_item.tax_id
		LEFT JOIN payment_voucher on payment_voucher.id = payment_voucher_item.payment_voucher_id
		WHERE payment_voucher_item.payment_voucher_id = '".$paymentVoucherId."' AND payment_voucher_item.status = 'A' AND payment_voucher.`status` = 'A'
		GROUP BY gl_account_id,net_amount";

	$result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "delete") {

	$id = $_POST['id'];

	$updateMainTable = "UPDATE payment_voucher SET status = 'I' WHERE id = '".$id."'";
	$updateSubTable = "UPDATE payment_voucher_item SET status = 'I' WHERE payment_voucher_id = '".$id."'";

	$combinedQry = $updateMainTable.';'.$updateSubTable;
	$result = mysqli_multi_query($conn,$combinedQry);
	echo $result;
}

if ($operation == "restore") {

	$id = $_POST['id'];

	$updateMainTable = "UPDATE payment_voucher SET status = 'A' WHERE id = '".$id."'";
	$updateSubTable = "UPDATE payment_voucher_item SET status = 'A' WHERE payment_voucher_id = '".$id."'";

	$combinedQry = $updateMainTable.';'.$updateSubTable;
	$result = mysqli_multi_query($conn,$combinedQry);
	echo $result;
}

if ($operation == "voucherDisbursement") {
	$id = $_POST['id'];
	$chequeDate = $_POST['chequeDate'];

	$updateMainTable = "UPDATE payment_voucher SET voucher_disbursement = 'yes',voucher_disbursed_date = '".$chequeDate."',disbursed_by = '".$userId."' WHERE id = '".$id."'";
	$result = mysqli_query($conn,$updateMainTable);
	echo $result;
}

if ($operation == "getChequeDescription") {
	$id = $_POST['id'];


	$query = "SELECT cheque_no,description FROM payment_voucher WHERE id = '".$id."'";

	$result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

?>