<?php

	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	$createdate          = new DateTime();
    $operation = "";
	$specimenType ="";
	$description = "";
	$date = new DateTime();
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	
	//update excel file
	if (isset($_FILES["file"]["type"])) // use for import data from csv
	{
		$temporary       = explode(".", $_FILES["file"]["name"]);
		$file_extension = end($temporary);
		/*if ($file_extension == "csv") {
			if ($_FILES["file"]["error"] > 0) {
				echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
			} 

			else {
				if($_FILES["file"]["size"] > 0) {
				
					$filename = $_FILES["file"]["tmp_name"];
					$file = fopen($filename, "r");
					$countCSV = 0;
					$subInsertValues = '';
					$sqlInsert = "INSERT IGNORE INTO specimens_types(type,description,created_on,updated_on) VALUES ";
					while (($emapData = fgetcsv($file, 10000, ",")) != FALSE) {
					
						//if there are more than 2 column then restrict it.	
						if (sizeof($emapData) != 2) {
						 	echo "You must have two column in your file i.e specimen type and description";
						 	exit();
						}

						//check whether first column name should be room_type and second should be description
						if ($countCSV == 0) {
							if(strtolower(reset($emapData)) !="specimen type") {
								echo "First column should be specimen type";
								exit();
							}
							if(strtolower(end($emapData)) !="description") {
								echo "Second column should be description";
								exit();
							}						
						}

						//data can't be null in first row else row it may be null
						if ($countCSV == 1) {
							if (empty($emapData[0])) {
								echo "Data can't be null in specimen type column";
								exit();
							}
						}
						reset($emapData);
						$firstIndex = key($emapData);
						end($emapData);
						$lastIndex = key($emapData);


						if ($countCSV > 0) {

							//check whether data not be null in the file
							if(($emapData[$firstIndex]) != "") {

								if ($countCSV >1) {
									$subInsertValues.= ',';
								}

								$subInsertValues.= "('".$emapData[$firstIndex]."','".$emapData[$lastIndex]."',	UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";
							}
						}				

						$countCSV++;						
					}
					$sqlInsert = $sqlInsert.$subInsertValues;
					$result = mysqli_query($conn,$sqlInsert);
					if($result)
					{
						echo $result;
					}
					else{
						echo "";
					}
					fclose($file);
				}
			} 	
		}
		else if($file_extension == "xls"){
			ini_set("display_errors",1);
			$filename = $_FILES['file']['tmp_name'];
			require_once 'excel_reader2.php';
			$data = new Spreadsheet_Excel_Reader($filename);
			$countSheets = count($data->sheets);
			for($i=0;$i<$countSheets;$i++) { // Loop to get all sheets in a file.
			
				$countSheetsCells =0;
				if(isset($data->sheets[$i]['cells'])){
					$countSheetsCells = count($data->sheets[$i]['cells']);
				}
				if($countSheetsCells > 0 && $countSheetsCells) {// checking sheet not empty
				
					$sql = "INSERT IGNORE INTO specimens_types(type,description,created_on,updated_on) VALUES ";
					$countXls = 0;				
					$subInsert = '';
					for($j=1;$j<=$countSheetsCells;$j++) {// loop used to get each row of the sheet
					 
						
					  	if (sizeof($data->sheets[$i]['cells'][$j]) !=2) {
					  		echo "You must have two column in your file i.e specimen type and description";
						 	exit();
					  	}
					  	
					  		
					  	$indexOfExcelFile = array_keys($data->sheets[$i]['cells'][$j]);	

						$name = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[0]];
						$description = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[1]];
						

						
						if ($countXls == 0) {
							if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[0]]) !="specimen type"){
								echo "First column should be specimen type";
								exit();
							}
							if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[1]]) !="description"){
								echo "Second column should be description";
								exit();
							}
						}

						//data can't be null in the very first row of file
						if ($countXls == 1) {
							if (empty($name)) {
								echo "Data can't be null in specimen type column";
								exit();
							}
						}
						 
						if ($countXls>0) {

							//check whether data not be null in file
							if($name != '' ) {

								if ($countXls >1) {
									$subInsert .=',';
								}			

							 	$subInsert .=" ('$name','$description',UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";
							}
						}
						$countXls++;		
					}
					$sql = $sql.$subInsert;
					$result = mysqli_query($conn,$sql);

					if($result) {
					
						echo $result;
					}
					else{
						echo "";
					}
				}
			}		
		}*/
		if($file_extension == "xlsx" || $file_extension == "csv" ||$file_extension == "xls"){
			// get file name
			$filename = $_FILES['file']['tmp_name'];
			
			//import php script
			require_once 'PHPExcel.php';
			
			// 
			try {
				$inputFileType = PHPExcel_IOFactory::identify($filename);  // get type of file name
				$objReader = PHPExcel_IOFactory::createReader($inputFileType); //create object of read file
				$objPHPExcel = $objReader->load($filename);  //load excel into the object
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($filename, PATHINFO_BASENAME) 
				. '": ' . $e->getMessage());
			}

			//  Get worksheet dimensions
			$sheet = $objPHPExcel->getSheet(0); // get sheet 1 of excel file
			$highestRow = $sheet->getHighestRow(); // get heighest row
			$highestColumn = $sheet->getHighestColumn(); // get heighest column
			
			$headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE); // get heading in first row
			
			if (sizeof($headings[0]) < 2) {
				echo "You must have two column in your file i.e specimen type and description";
				exit();
			}
			//reset($headings[0]) it is the first column
			if(trim(strtolower(reset($headings[0]))) !="specimen type"){
				echo "First column should be specimen type";
				exit();
			}
			//end($headings[0]) it is the last column
			if(trim(strtolower(end($headings[0]))) !="description"){
				echo "Second column should be description";
				exit();
			}
			$sql = "INSERT IGNORE INTO specimens_types(type,description,created_on,updated_on) VALUES ";
			$countXlsx = 0;
			$subInsertXlsx = '';

			for ($row = 2; $row <= $highestRow; $row++) {
				//  Read a row of data into an array
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL, TRUE, FALSE);
				
				// combine data in array accoding to heading
				$rowData[0] = array_combine($headings[0], $rowData[0]);
				//var_dump($rowData[0]);
				
				$key  = array_keys($rowData[0]);//getting the ondex name

				$name = trim(mysql_real_escape_string($rowData[0][$key[0]]));
				$description = trim(mysql_real_escape_string($rowData[0][$key[1]]));
				
				// data shouldn't be null in the very first row of file
				if ($countXlsx == 0) {

					if (empty($name)) {
						echo "Data can't be null in specimen type column";
						exit();
					}
				}
				
				//not save null data in category column
				if ($name != "") {		
					// insert data into table
					if ($countXlsx > 0) {
						$subInsertXlsx.=',';
					}
					$subInsertXlsx.="('$name','$description',UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";
				}
						
				$countXlsx++;		
			}

			$sql = $sql.$subInsertXlsx;
			$result = mysqli_query($conn,$sql);
			if($result)
			{
				echo $result;
			}
			else{
				echo "";
			}
		}
		else{
			echo "invalid file";
		}
	}

	//Operation for save the data
	if($operation == "save")			
	{
		if (isset($_POST['specimenType'])) {
			$specimenType=$_POST['specimenType'];
		}
		if (isset($_POST['description'])) {
			$description=$_POST['description'];
		}		
	
		$sql1 = "SELECT type from specimens_types where type='".$specimenType."'";
		$resultSelect=mysqli_query($conn,$sql1);
		$rows_count= mysqli_num_rows($resultSelect);
		
		if($rows_count <= 0){
			
			$sql = "INSERT INTO specimens_types(type,description,created_on,updated_on) VALUES('".$specimenType."','".$description."',".$date->getTimestamp().",".$date->getTimestamp().")";
			$result= mysqli_query($conn,$sql);  
			echo $result;
		}
		else{
			echo "0";
		}
	} 	
	
	//Operation for update the data
	if($operation == "update")			
	{
		if (isset($_POST['specimenType'])) {
			$specimenType=$_POST['specimenType'];
		}
		if (isset($_POST['description'])) {
			$description=$_POST['description'];
		}
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}		
		$sql1 = "SELECT type from specimens_types where type='".$specimenType."' AND id !='".$id."'";
		$resultSelect=mysqli_query($conn,$sql1);
		$rows_count= mysqli_num_rows($resultSelect);
		
		if($rows_count <= 0){
			
			$sql = "UPDATE specimens_types SET type= '".$specimenType."',description='".$description."',
			updated_on=".$date->getTimestamp()." WHERE  id = '".$id."'";
			$result= mysqli_query($conn,$sql);  
			echo $result;
		}
		else{
			echo "0";
		}		
	}
	
	//Operation for update the status
	if($operation == "delete")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}		
	
		$sql = "UPDATE specimens_types SET status= 'I'  WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}	
	
	//Operation for show the active data
	if($operation == "show"){
		
		$query= "SELECT * from specimens_types WHERE status = 'A'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}	
	
	//Operation for show the inactive data
	if($operation == "checked"){
		
		$query= "SELECT * from specimens_types WHERE status = 'I'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}	

	//to restore data back to data table
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		$sql = "UPDATE specimens_types SET status= 'A'  WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
?>