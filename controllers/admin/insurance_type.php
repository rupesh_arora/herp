<?php
/*
 * File Name    :   insurance_type.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   22 feb, 2016
 * Description  :   This page is use for add Insurance Type
 */
session_start(); // session start

$operation      = "";
$insuranceName = "";
$description    = "";
$date 			= new DateTime();

if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

include 'config.php'; // include database connection file

if (isset($_POST['operation'])) { // define operation value from js file
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

/* save insurance type information into database*/
if ($operation == "save") {
    if (isset($_POST['insuranceName'])) {
        $insuranceName = $_POST['insuranceName'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    
    
    $sql1         = "SELECT * FROM insurance_type WHERE name='" . $insuranceName . "'";
    $resultSelect = mysqli_query($conn, $sql1);
    $rows_count   = mysqli_num_rows($resultSelect);
    
    if ($rows_count <= 0) {
        
        $sql    = "INSERT INTO insurance_type(name,description,created_on,updated_on) VALUES('" . $insuranceName . "','" . $description . "'," . $date->getTimestamp() . "," . $date->getTimestamp() . ")";
        $result = mysqli_query($conn, $sql);
        echo $result;
    } else {
        echo "0";
    }
}

/* update insurance type information into database*/
if ($operation == "update") {
    if (isset($_POST['insuranceName'])) {
        $insuranceName = $_POST['insuranceName'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    
    $sql    = "UPDATE insurance_type SET name= '" . $insuranceName . "',description='" . $description . "',updated_on=" . $date->getTimestamp() . " WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}
/* set status Inactive for delete insurance type */
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    
    $sql    = "UPDATE insurance_type SET status= 'I'  WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}

/*show active data into the datatable  */
if ($operation == "show") {
    
    $query        = "SELECT * from insurance_type WHERE status = 'A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/* show Inactive data into datatable on checked case*/

if ($operation == "checked") {
    
    $query        = "SELECT * from insurance_type WHERE status = 'I'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/* set status active for restore insurance type */

if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $sql    = "UPDATE insurance_type SET status= 'A'  WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}
?>