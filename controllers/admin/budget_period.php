<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}

	$operation       = "";

	include 'config.php'; // include database connection file

	if (isset($_POST['operation'])) { // define operation value from js file
	    $operation = $_POST["operation"];
	} else if (isset($_GET["operation"])) {
	    $operation = $_GET["operation"];
	}

	if ($operation == "showFiscalYear") {

	    $sqlSelect = "SELECT id,CONCAT(leave_period.from_date,' - ',leave_period.to_date) AS period FROM leave_period WHERE period_status = 1";

	    $result         = mysqli_query($conn, $sqlSelect);
	    $rows           = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	if($operation == "saveBudgetPeriod") {
		$name = $_POST["name"];
		$fiscalYear = $_POST["fiscalYear"];
		$beginDate = $_POST["beginDate"];
		$endDate = $_POST["endDate"];
		$description = $_POST["description"];

		$selBudgetPeriod = "SELECT name FROM budget_period WHERE name ='".$name."' AND status = 'A'";
		$checkBudgetPeriod = mysqli_query($conn,$selBudgetPeriod);
		$countBudgetPeriod = mysqli_num_rows($checkBudgetPeriod);

		if ($countBudgetPeriod == "0") {

			$insertColumn = "ALTER TABLE `budget_matrix` ADD COLUMN `".$name."` FLOAT(20) NULL ";

			$insertResult = mysqli_query($conn,$insertColumn);
			if($insertResult){
				$query = "INSERT INTO budget_period(name,fiscal_year,begin_date,end_date,description,created_on,updated_on,created_by,updated_by)VALUES('".$name."','".$fiscalYear."','".$beginDate."','".$endDate."','".$description."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";

				$result = mysqli_query($conn,$query);
				echo $result;
			}else{
				echo "0";
			}
			
		 }
		else {
			echo "0";
		}
	}
	if ($operation == "show") { // show active data
    
    	$query = "SELECT budget_period.id,budget_period.name,budget_period.fiscal_year,budget_period.begin_date,budget_period.end_date,budget_period.description,CONCAT(leave_period.from_date,' - ',leave_period.to_date) AS fiscal_year_period FROM budget_period 
			LEFT JOIN leave_period on leave_period.id = budget_period.fiscal_year
			WHERE budget_period.`status` = 'A'";
	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}
	if ($operation == "checked") { // show active data
    	
    	$query = "SELECT budget_period.id,budget_period.name,budget_period.fiscal_year,budget_period.begin_date,budget_period.end_date,budget_period.description,CONCAT(leave_period.from_date,' - ',leave_period.to_date) AS fiscal_year_period FROM budget_period 
			LEFT JOIN leave_period on leave_period.id = budget_period.fiscal_year
			WHERE budget_period.`status` = 'I'";
	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}
	if ($operation == "update") // update data
	{
	    $name = $_POST["name"];
		$fiscalYear = $_POST["fiscalYear"];
		$beginDate = $_POST["beginDate"];
		$endDate = $_POST["endDate"];
		$description = $_POST["description"];
	    $id = $_POST['id'];

	    $selBudgetPeriod = "SELECT name FROM budget_period WHERE name ='".$name."' AND status = 'A' AND id !='".$id."'";
	    
		$checkBudgetPeriod = mysqli_query($conn,$selBudgetPeriod);

		$countBudgetPeriod = mysqli_num_rows($checkBudgetPeriod);
		$lastColumnName  ='';
		if ($countBudgetPeriod == "0") {
			if(((int)$id)-1 == 0){
				$selBudget = "SELECT name FROM budget_period WHERE status = 'A' AND id ='".$id."'";
			}
			else{
				$selBudget = "SELECT name FROM budget_period WHERE status = 'A' AND id ='".$id."' OR id =".$id." -1";
			}
			
			$checkBudget= mysqli_query($conn,$selBudget);
		    $column = Array();
		    while ($r = mysqli_fetch_assoc($checkBudget)) {
		        $column[] = $r;
		    }
			$sql    = "UPDATE budget_period set name = '".$name."',fiscal_year = '".$fiscalYear."',begin_date = '".$beginDate."',end_date = '".$endDate."',description= '".$description."',updated_on = UNIX_TIMESTAMP() ,updated_by = '".$userId."' where id = '".$id."' ";

			$result = mysqli_query($conn, $sql);

			if(((int)$id)-1 == 0){
				$updateColumn = "ALTER TABLE `budget_matrix`
				ALTER `".$column[0]['name']."` DROP DEFAULT;
				ALTER TABLE `budget_matrix`
				CHANGE COLUMN `".$column[0]['name']."` `".$name."` FLOAT NULL DEFAULT NULL AFTER `amount`";
			}
			else{
				$updateColumn = "ALTER TABLE `budget_matrix`
				ALTER `".$column[1]['name']."` DROP DEFAULT;
				ALTER TABLE `budget_matrix`
				CHANGE COLUMN `".$column[1]['name']."` `".$name."` FLOAT NULL DEFAULT NULL AFTER `".$column[0]['name']."`";
			}

			
			$result = mysqli_multi_query($conn, $updateColumn);
			echo $result;
		}
		else {
			echo "0";
		}
	}
	if ($operation == "delete") {
        $id = $_POST['id'];

		$sql    = "UPDATE budget_period SET status= 'I' where id = '" . $id . "'";
	    $result = mysqli_query($conn, $sql);
	    echo $result;	    
	}

	if ($operation == "restore") {// for restore    
        $id = $_POST['id'];
        $name = $_POST['name'];

        $selLedger = "SELECT name FROM budget_period WHERE name ='".$name."' AND status = 'A' AND id !='".$id."'";
		$checkLedger = mysqli_query($conn,$selLedger);
		$countLedger = mysqli_num_rows($checkLedger);

		if ($countLedger == "0") {

			echo $sql    = "UPDATE budget_period SET status= 'A'  WHERE  id = '" . $id . "'";
			$result = mysqli_query($conn, $sql);
		    echo $result;
		}
		else {
			echo "0";
		}
	}
?>