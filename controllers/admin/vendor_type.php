<?php

/*File Name    :   room_type.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page  manages room type */
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
$operation   = "";
$roomType    = "";
$description = "";
$createdate          = new DateTime();
/*include config file*/
include 'config.php';

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
} //nothing


/*operation to save*/
if ($operation == "save") {
    if (isset($_POST['roomType'])) {
        $roomType = $_POST['roomType'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    
    /*check that room type for that type exist or not*/
    $sqlSelect    = "SELECT type FROM vendor_type WHERE type='" . $roomType . "'";
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $rows_count   = mysqli_num_rows($resultSelect);
    
    if ($rows_count <= 0) {
        $sql    = "INSERT INTO vendor_type(type,description,created_on,updated_on,created_by,updated_by) VALUES('" . $roomType . "','" . $description . "',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
        $result = mysqli_query($conn, $sql);
        echo $result;
    } else {
        echo "0";
    }
}

/*Operation to update data*/
if ($operation == "update") {
    if (isset($_POST['roomType'])) {
        $roomType = $_POST['roomType'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $sql1         = "SELECT type FROM vendor_type WHERE type='" . $roomType . "' and id != '$id'";
    $resultSelect = mysqli_query($conn, $sql1);
    $rows_count   = mysqli_num_rows($resultSelect);
    if ($rows_count == 0) {
        $sql    = "UPDATE vendor_type SET type= '" . $roomType . "',description='" . $description . "',updated_on=UNIX_TIMESTAMP(),updated_by='".$userId."' WHERE  id = '" . $id . "'";
        $result = mysqli_query($conn, $sql);
        echo $sql;
    } else {
        echo "0";
    }
}

/*Operation to delete data*/
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE vendor_type SET status= 'I' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}

/*Operation to restore data*/
if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE vendor_type SET status= 'A' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}

if ($operation == "show") {
    $query        = "SELECT * FROM vendor_type WHERE status= 'A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result); //count total records
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

if ($operation == "showInActive") {
    $query        = "SELECT * FROM vendor_type WHERE status= 'I'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}
?>