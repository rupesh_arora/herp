<?php
	// Mysql connection
	
	$mysql_server = "localhost"; // mysql server
	$mysql_user = "root"; // mysql user
	$mysql_pass = ""; //mysql password for user
	$mysql_db = "herp"; // mysql database to use
	
	$conn = mysqli_connect($mysql_server,$mysql_user,$mysql_pass,$mysql_db);
	
	//This is added to increase the memory size to handle big size data
	ini_set('memory_limit', '-1');
	
	$software_name = "Health Link";
	$version_name ="Health Link 0.3";
	
	
?>