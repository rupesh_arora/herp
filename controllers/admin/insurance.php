<?php
/*
 * File Name    :   insuance_plan.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   21th feb, 2016
 * Description  :   This page use for load,save,update,delete,resotre details form database
 */
$operation           = "";
$description         = "";
include 'config.php'; //import databse connection file
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

if (isset($_POST['operation'])) { // perform operation form js file
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
}

if($operation == "showPlan") {

    if (isset($_POST['companyId'])) {
        $companyId = $_POST['companyId'];
    }

	$query = "SELECT id,plan_name FROM insurance_plan where status = 'A' AND company_name_id = '" . $companyId . "'";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}


if ($operation == "save") // save details
{	
	
    if (isset($_POST['patientId'])) {
        $patientId = $_POST['patientId'];
    }
	if (isset($_POST['expirationDate'])) {
        $expirationDate = $_POST['expirationDate'];
    }
    if (isset($_POST['memberSince'])) {
        $memberSince = $_POST['memberSince'];
    }
    if (isset($_POST['insurancePlan'])) {
        $insurancePlan = $_POST['insurancePlan'];
    }
    $planName = $_POST['planName'];
    if (isset($_POST['insuranceCompany'])) {
        $insuranceCompany = $_POST['insuranceCompany'];
    }
    if (isset($_POST['extraInfo'])) {
        $extraInfo = $_POST['extraInfo'];
    }
    $insuranceNumber = $_POST['insuranceNumber'];
   
	
	
    $patientPrefix = $_POST['patientPrefix'];
    $sql         = "SELECT value FROM configuration WHERE name = 'patient_prefix'";
    $result      = mysqli_query($conn, $sql);
    
    while ($r = mysqli_fetch_assoc($result)) {
        $prefix = $r['value'];
    }
    if ($prefix == $patientPrefix) {       
        
        $checkPatient = "SELECT id FROM patients WHERE id = '".$patientId."'";
        $resultCheck = mysqli_query($conn,$checkPatient);
        $countRows = mysqli_num_rows($resultCheck);
        if ($countRows == 0) {
            echo "Patient not exist";
        }
        $checkSameInsurance = "SELECT patient_id FROM insurance WHERE patient_id = '".$patientId."'
            AND insurance_company_id = '".$insuranceCompany."' 
            AND insurance_plan_name_id = '".$planName."'  AND insurance_plan_id = '".$insurancePlan."'
            AND status = 'A'";

        $resultCheckInsurance = mysqli_query($conn,$checkSameInsurance);
        $countInsuranceDetails = mysqli_num_rows($resultCheckInsurance);
        if ($countInsuranceDetails > 0) {
            echo "Insurance details already exist";
            exit();
        }

        $countInsurancePerIndividual = "SELECT patient_id FROM insurance 
            WHERE patient_id = '".$patientId."' AND status = 'A'";

        $resultCount = mysqli_query($conn, $countInsurancePerIndividual);
        $rowsCount = mysqli_num_rows($resultCount);
        if ($rowsCount >=3) {
            echo "Limit Crossed";
            exit();
        }


        //check scheme plan expiry date and member since
        $checkMemberSince = "SELECT start_date,end_date FROM scheme_plans WHERE insurance_company_id ='".$insuranceCompany."' AND insurance_plan_id='".$insurancePlan."' AND id ='".$planName."'
            AND status ='A'";

        $resultMemberSince       = mysqli_query($conn, $checkMemberSince);
        $date         = array();
        while ($rows = mysqli_fetch_assoc($resultMemberSince)) {
            $startDate = $rows['start_date'];
            $endDate = $rows['end_date'];
        } 
        
        if($startDate > $memberSince){
            echo "Member since can't be smaller than plan start date i.e ".$startDate;
            exit();
        }

        if($endDate < $expirationDate){
            echo "Expiry date can't be greater than plan expire date i.e ".$endDate;
            exit();
        }
        
        $sql    = "INSERT INTO insurance(patient_id,insurance_number,insurance_company_id,
            insurance_plan_name_id,insurance_plan_id,member_since,expiration_date,extra_info,created_on,
            updated_on,created_by,updated_by)
			VALUES('" . $patientId . "','" . $insuranceNumber . "','" . $insuranceCompany . "',
            '" . $planName . "','" . $insurancePlan . "','" . $memberSince . "','" . $expirationDate . "',
            '" . $extraInfo . "',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),".$userId.",".$userId.")";
        $result = mysqli_query($conn, $sql);
        echo $result;
    }
    else{
        echo "2";
    }
}
if ($operation == "show") { // show active data
    
    $query        = "SELECT i.*,ic.name AS company_name,ip.plan_name,scheme_plans.scheme_plan_name,
        (SELECT value FROM configuration WHERE name = 'patient_prefix') AS patient_prefix    
        FROM  insurance AS i 
        LEFT JOIN insurance_companies AS ic ON ic.id = i.insurance_company_id
        LEFT JOIN insurance_plan AS ip ON ip.id = i.insurance_plan_id 
        LEFT JOIN scheme_plans ON scheme_plans.id = i.insurance_plan_name_id
        WHERE i.`status`='A' AND ic.status = 'A' AND ip.status='A' AND scheme_plans.status = 'A' 
        AND  i.expiration_date >= CURDATE()  AND scheme_plans.end_date >=CURDATE()";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
    
}
// opertaion form update
if ($operation == "update") // update data
{    
    if (isset($_POST['expirationDate'])) {
        $expirationDate = $_POST['expirationDate'];
    }
    if (isset($_POST['memberSince'])) {
        $memberSince = $_POST['memberSince'];
    }
    if (isset($_POST['insurancePlan'])) {
        $insurancePlan = $_POST['insurancePlan'];
    }
    if (isset($_POST['insuranceCompany'])) {
        $insuranceCompany = $_POST['insuranceCompany'];
    }
    if (isset($_POST['extraInfo'])) {
        $extraInfo = $_POST['extraInfo'];
    }
    
    $id = $_POST['id'];
    $insuranceNumber = $_POST['insuranceNumber'];
    $planName = $_POST['planName'];
    
    //check same insurance details alreday exist 
    $checkSameInsurance = "SELECT patient_id FROM insurance WHERE patient_id = '".$patientId."'
        AND insurance_company_id = '".$insuranceCompany."' 
        AND insurance_plan_name_id = '".$planName."'  AND insurance_plan_id = '".$insurancePlan."'
        AND status = 'A' AND id !='".$id."'";

    $resultCheckInsurance = mysqli_query($conn,$checkSameInsurance);
    $countInsuranceDetails = mysqli_num_rows($resultCheckInsurance);
    if ($countInsuranceDetails > 0) {
        echo "Insurance details already exist";
        exit();
    }

    //check scheme plan expiry date and member since
    $checkMemberSince = "SELECT start_date,end_date FROM scheme_plans WHERE insurance_company_id ='".$insuranceCompany."' AND insurance_plan_id='".$insurancePlan."' AND id ='".$planName."'
        AND status ='A'";

    $resultMemberSince       = mysqli_query($conn, $checkMemberSince);
    $start_date         = array();
    while ($rows = mysqli_fetch_assoc($resultMemberSince)) {
        $startDate = $rows['start_date'];
        $endDate = $rows['end_date'];
    } 
    
    if($startDate > $memberSince){
        echo "Member since can't be smaller than plan start date i.e ".$startDate;
        exit();
    }

    if($endDate < $expirationDate){
        echo "Expiry date can't be greater than plan expire date i.e ".$endDate;
        exit();
    }


    $sql    = "UPDATE insurance SET insurance_number= '" . $insuranceNumber . "',
        insurance_company_id= '" . $insuranceCompany . "' ,insurance_plan_name_id = '".$planName."',
        insurance_plan_id = '" . $insurancePlan . "',member_since = '" . $memberSince . "',
        expiration_date = '" . $expirationDate . "',extra_info = '" . $extraInfo . "',
        updated_on = UNIX_TIMESTAMP(),updated_by='" . $userId . "' where id = '" . $id . "' ";
    $result = mysqli_query($conn, $sql);
    echo $result;
}

//  operation for delete
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE insurance SET status= 'I' where id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}
//When checked box is check
if ($operation == "checked") {
    
    $query = "SELECT i.*,ic.name AS company_name,ip.plan_name,scheme_plans.scheme_plan_name,
        (SELECT value FROM configuration WHERE name = 'patient_prefix') AS patient_prefix    
        FROM  insurance AS i 
        LEFT JOIN insurance_companies AS ic ON ic.id = i.insurance_company_id
        LEFT JOIN insurance_plan AS ip ON ip.id = i.insurance_plan_id 
        LEFT JOIN scheme_plans ON scheme_plans.id = i.insurance_plan_name_id
        where i.`status`='I' OR ic.status = 'I' OR ip.status='I' OR scheme_plans.status='I'
        AND  i.expiration_date >= CURDATE()  AND scheme_plans.end_date >=CURDATE()";
    
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}
if ($operation == "restore") // for restore    
    {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    if (isset($_POST['patientId'])) {
        $patientId = $_POST['patientId'];
    }

    $countInsurancePerIndividual = "SELECT patient_id FROM insurance 
        WHERE patient_id = '".$patientId."' AND status = 'A'";

    $resultCount = mysqli_query($conn, $countInsurancePerIndividual);
    $rowsCount = mysqli_num_rows($resultCount);
    if ($rowsCount >=3) {
        echo "Limit Crossed";
        exit();
    }
    
    
    $sql    = "UPDATE insurance SET status= 'A'  WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}
?>