<?php
/*
 * File Name    :   allergies.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre details from database        
 */
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
$operation        = "";
include 'config.php';                   // import database connection file
if (isset($_POST['operation'])) {       // opeartion come from js 
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
    
}


if ($operation == "showOTRooms") {
    
    $AllRows = array();

    $sqlSelect = "SELECT room_no as name,CONCAT('room',id) as id FROM ot_rooms WHERE status = 'A'";
    $result = mysqli_query($conn, $sqlSelect);

    $rowsSelRoom = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rowsSelRoom[] = $r;
    }


    $selOTDetails = "SELECT CONCAT('room',ot_room_availability.ot_room_id) as roomId,
        ot_room_availability.from_time,ot_room_availability.to_time,
        CONCAT(patients.first_name,' ',patients.last_name) AS patient_name,
        patient_ot_registration.operation_date,patients.id as patient_id,
        GROUP_CONCAT(CONCAT(u1.first_name,' ', u1.last_name)) As surgeon_name, 
        GROUP_CONCAT(CONCAT(u2.first_name,' ',u2.last_name)) As anethaesthiest_name,
        GROUP_CONCAT(CONCAT(u3.first_name,' ',u3.last_name)) As nurse_name
        FROM ot_room_availability
        LEFT JOIN patient_ot_registration ON patient_ot_registration.id = ot_room_availability.
        patient_operation_id
        LEFT JOIN patients ON patients.id = patient_ot_registration.patient_id        
        LEFT JOIN ot_staff_details ON ot_staff_details.patient_operation_id = patient_ot_registration.id
        LEFT JOIN users  AS u1 ON u1.id = ot_staff_details.ot_staff_id AND u1.staff_type_id = 13
        LEFT JOIN users AS u2 ON u2.id = ot_staff_details.ot_staff_id AND u2.staff_type_id = 14
        LEFT JOIN users AS u3 on u3.id  = ot_staff_details.ot_staff_id AND u3.staff_type_id = 4
        WHERE ot_room_availability.status = 'A' AND patient_ot_registration.status = 'A' 
        AND ot_staff_details.status = 'A'
        GROUP BY patient_ot_registration.id";

    $resultOTDetails = mysqli_query($conn, $selOTDetails);

    $rowsOTDetails = array();
    while ($r = mysqli_fetch_assoc($resultOTDetails)) {
        $rowsOTDetails[] = $r;
    }

    array_push($AllRows, $rowsSelRoom);
    array_push($AllRows, $rowsOTDetails);
    echo json_encode($AllRows);    
}
?>