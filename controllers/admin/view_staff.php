<?php
/*
 * File Name    :   view_staff.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load and save inforamtion
 */
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
$operation = "";
include 'config.php'; // import databse connection file
if (isset($_POST['operation'])) { // load operation from js file
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

if ($operation == "showDesignation") // show designation    
    {
    
    $query = "Select id,designation FROM designation WHERE status='A' ORDER BY designation";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    print json_encode($rows);
}

if ($operation == "showStaffType") // show staff type
    {
    
    $query = "Select id,type FROM staff_type WHERE status='A' ORDER BY type";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    print json_encode($rows);
}

if ($operation == "staffDetail") { // show staff details
    
    if (isset($_POST['id'])) {
        $id = $_POST["id"];
    }
    $query  = "SELECT users.images_path,users.id, users.phone,staff_type.`type`, concat(users.salutation,' ',users.first_name,' ',users.last_name) as staff_name, users.mobile,CASE WHEN users.country_code = '' OR users.country_code = NULL THEN '0' ELSE users.country_code END AS country_code, users.joining_date,
            designation.`designation`,users.gender, users.dob, users.address, users.martial_status, users.email_id,users.zip,users.phone,users.referred_by
            ,users.degree_held,users.emergency_name,users.emergency_contact,users.remarks,department.department,
             (SELECT name FROM locations l WHERE l.location_id = users.country_id) AS country,
            (SELECT name FROM locations l WHERE l.location_id = users.state_id) AS state,
            (SELECT name FROM locations l WHERE l.location_id = users.city_id) AS city
            from users
            LEFT JOIN department ON department.id = users.department_id
            LEFT JOIN staff_type ON staff_type.id = users.staff_type_id
            LEFT JOIN designation ON designation.id = users.designation
            LEFT JOIN locations ON locations.location_id = users.id
            WHERE users.id= '" . $id . "' AND users.status = 'A' ";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}


if ($operation == "search") { // search functionality
    
    if (isset($_POST['staffId'])) {
        $id = $_POST["staffId"];
    }
    if (isset($_POST['designation'])) {
        $designation = $_POST["designation"];
    }
    if (isset($_POST['stafftype'])) {
        $stafftype = $_POST["stafftype"];
    }
    if (isset($_POST['firstName'])) {
        $firstName = $_POST["firstName"];
    }
    if (isset($_POST['lastName'])) {
        $lastName = $_POST["lastName"];
    }
    if (isset($_POST['mobile'])) {
        $mobile = $_POST["mobile"];
    }
    
    $isFirst = "false";
    
    $query = "SELECT users.id, staff_type.`type`, CONCAT(users.first_name,' ',users.last_name) as name, users.mobile,CASE WHEN users.country_code = '' OR users.country_code = NULL THEN '0' ELSE users.country_code END AS country_code,
            designation.`designation`from users
            LEFT JOIN staff_type ON staff_type.id = users.staff_type_id
            LEFT JOIN designation ON designation.id = users.designation";
    
    if ($firstName != '' || $lastName != '' || $mobile != '' || $id != '' || $designation != '-1' || $stafftype != '-1') {
        $query .= " WHERE users.status = 'A' ";
        $isFirst = "true";
    } else {
        $query .= " WHERE users.status = 'A' ";
        
    }
    if ($firstName != '') {
        if ($isFirst == "true") { //if value true then add and in the last
            $query .= " AND ";
        }
        $query .= "users.first_name LIKE '%" . $firstName . "%'";
        $isFirst = "true";
    }
    if ($lastName != '') {
        if ($isFirst == "true") {
            $query .= " AND ";
        }
        $query .= "users.last_name LIKE '%" . $lastName . "%'";
        $isFirst = "true";
    }
    if ($mobile != '') {
        if ($isFirst == "true") {
            $query .= " AND ";
        }
        $query .= "users.mobile LIKE '%" . $mobile . "%'";
        $isFirst = "true";
    }
    if ($id != '') {
        if ($isFirst == "true") {
            $query .= " AND ";
        }
        $query .= "users.id  = '" . $id . "'";
        $isFirst = "true";
    }
    if ($designation != '-1') {
        if ($isFirst == "true") {
            $query .= " AND ";
        }
        $query .= "users.designation = '" . $designation . "' ";
        $isFirst = "true";
    }
    if ($stafftype != '-1') {
        if ($isFirst == "true") {
            $query .= " AND ";
        }
        $query .= "users.staff_type_id = '" . $stafftype . "' ";
        $isFirst = "true";
    }
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "searchInactiveData") { // search case for inactive data
    
    if (isset($_POST['staffId'])) {
        $id = $_POST["staffId"];
    }
    if (isset($_POST['designation'])) {
        $designation = $_POST["designation"];
    }
    if (isset($_POST['stafftype'])) {
        $stafftype = $_POST["stafftype"];
    }
    if (isset($_POST['firstName'])) {
        $firstName = $_POST["firstName"];
    }
    if (isset($_POST['lastName'])) {
        $lastName = $_POST["lastName"];
    }
    if (isset($_POST['mobile'])) {
        $mobile = $_POST["mobile"];
    }
    
    $isFirst = "false";
    
    $query = "SELECT users.id, staff_type.`type`, CONCAT(users.first_name,' ',users.last_name) as name, users.mobile,CASE WHEN users.country_code = '' OR users.country_code = NULL THEN '0' ELSE users.country_code END AS country_code,
            designation.`designation`from users
            LEFT JOIN staff_type ON staff_type.id = users.staff_type_id
            LEFT JOIN designation ON designation.id = users.designation";
    
    if ($firstName != '' || $lastName != '' || $mobile != '' || $id != '' || $designation != '-1' || $stafftype != '-1') {
        $query .= " WHERE users.status = 'I' ";
        $isFirst = "true";
    } else {
        $query .= " WHERE users.status = 'I' ";
        
    }
    if ($firstName != '') {
        if ($isFirst == "true") { //if value true then add and in the last
            $query .= " AND ";
        }
        $query .= "users.first_name LIKE '%" . $firstName . "%'";
        $isFirst = "true";
    }
    if ($lastName != '') {
        if ($isFirst == "true") {
            $query .= " AND ";
        }
        $query .= "users.last_name LIKE '%" . $lastName . "%'";
        $isFirst = "true";
    }
    if ($mobile != '') {
        if ($isFirst == "true") {
            $query .= " AND ";
        }
        $query .= "users.mobile LIKE '%" . $mobile . "%'";
        $isFirst = "true";
    }
    if ($id != '') {
        if ($isFirst == "true") {
            $query .= " AND ";
        }
        $query .= "users.id  = '" . $id . "' ";
        $isFirst = "true";
    }
    if ($designation != '-1') {
        if ($isFirst == "true") {
            $query .= " AND ";
        }
        $query .= "users.designation = '" . $designation . "' ";
        $isFirst = "true";
    }
    if ($stafftype != '-1') {
        if ($isFirst == "true") {
            $query .= " AND ";
        }
        $query .= "users.staff_type_id = '" . $stafftype . "' ";
        $isFirst = "true";
    }
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "searchCurrentDateVisit") { // show data fromdate to todate 
    
    if (isset($_POST['id'])) {
        $id = $_POST["id"];
    }
    if (isset($_POST['getFromDateTimeStamp'])) {
        $getFromDateTimeStamp = $_POST["getFromDateTimeStamp"];
    }
    if (isset($_POST['getToDateTimeStamp'])) {
        $getToDateTimeStamp = $_POST["getToDateTimeStamp"];
    }
    
    $query = "SELECT visits.id, visit_type.`type` , CONCAT(patients.first_name,'',patients.last_name) as name ,
            CONCAT(users.first_name,'',users.last_name) as created_by, CONCAT(users.first_name,' ',users.last_name) as updated_by
            from visits
            LEFT JOIN visit_type ON visits.visit_type = visit_type.id
            LEFT JOIN patients ON visits.patient_id = patients.id
            LEFT JOIN users ON visits.created_by = users.id AND visits.updated_by = users.id 
            WHERE CAST(FROM_UNIXTIME(visits.created_on) AS DATE) = CAST(FROM_UNIXTIME(UNIX_TIMESTAMP()) AS DATE)
            AND visits.status = 'A'";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "deleteStaffDetail") { // delete staff type set status inactive
    if (isset($_POST['id'])) {
        $id = $_POST["id"];
    }
    $sql    = "UPDATE users set status='I' WHERE  id = '" . $id . "' ";
    $result = mysqli_query($conn, $sql);
    if ($result) {
        echo $result;
    } else {
        echo "";
    }
}
//this will show all inactive data to datatable
if ($operation == "checked") { // show inactive details
    $sqlSelect    = "SELECT users.id, staff_type.`type`, CONCAT(users.first_name,' ', users.last_name) as name,CASE WHEN users.country_code = '' OR users.country_code = NULL THEN '0' ELSE users.country_code END AS country_code, users.mobile,
                    designation.`designation`from users
                    LEFT JOIN staff_type ON staff_type.id = users.staff_type_id
                    LEFT JOIN designation ON designation.id = users.designation where users.status = 'I'";
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $totalrecords = mysqli_num_rows($resultSelect);
    
    $rows = array();
    while ($rUpdate = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $rUpdate;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}
if ($operation == "unChecked") { // show active details
    $sqlSelect    = "SELECT users.id, staff_type.`type`, CONCAT(users.first_name,' ',users.last_name) as name, users.mobile,CASE WHEN users.country_code = '' OR users.country_code = NULL THEN '0' ELSE users.country_code END AS country_code,
                    designation.`designation`from users
                    LEFT JOIN staff_type ON staff_type.id = users.staff_type_id
                    LEFT JOIN designation ON designation.id = users.designation where users.status = 'A'";
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $totalrecords = mysqli_num_rows($resultSelect);
    $rows         = array();
    while ($rUpdate = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $rUpdate;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}
//to restore data back to data 
if ($operation == "restoreStaffDetail") // retore details to set status active
    {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $sql    = "UPDATE users set status='A' WHERE  id = '" . $id . "' ";
    $result = mysqli_query($conn, $sql);
    if ($result) {
        echo $result;
    } else {
        echo "";
    }
}
?>