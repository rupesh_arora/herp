<?php
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
include 'config.php';

$operation   = "";

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

/*checking room type set or not*/
if (isset($_POST['roomType'])) {
    $roomType = $_POST["roomType"];
} else if (isset($_GET["roomType"])) {
    $roomType = $_GET["roomType"];
}


/*operation to show active data*/
if ($operation == "show") {
	$operation = $_GET["operation"];
    $query        = "SELECT beds.id,beds.price,beds.number AS bed_number ,ward_room.number AS ward_number from beds 
					LEFT JOIN ward_room ON beds.room_id = ward_room.id                    
                     WHERE beds.`status` = 'A' AND 
					beds.room_id = ".$roomType." AND beds.bed_availability = '1'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/*operation to show inactive data*/
if ($operation == "showInActive") {
    $query        = "SELECT beds.id,beds.price,beds.number AS bed_number ,ward_room.number AS ward_number from beds 
					LEFT JOIN ward_room ON beds.room_id = ward_room.id
                     WHERE beds.`status` = 'A' AND 
					beds.room_id= ".$roomType." AND beds.bed_availability = '0'";
					
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

?>