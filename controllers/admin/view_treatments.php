<?php
/*File Name  :   view_treatment.php
Company Name :   Qexon Infotech
Created By   :   Tushar
Created Date :   30th Dec, 2015
Description  :   This page search treatments*/

session_start(); //start session

/*include config file*/
include 'config.php';

$operation   = "";

$date      = date("Y-m-d");
$createOn  = new DateTime();
	
/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

// search treatment
if ($operation == "search") {
    
	if (isset($_POST['IPDId'])) {
        $IPDId = $_POST["IPDId"];
    }
    if (isset($_POST['treatmentDate'])) {
        $treatmentDate = $_POST["treatmentDate"];
    }
	
	$isFirst = "false";
    
        $query = "select (select value from configuration where name = 'ipd_prefix') AS ipdPrefix,ipd_treatment.*,treatments.price,treatments.treatment from ipd_treatment 
				LEFT JOIN ipd_treatment_details ON ipd_treatment_details.ipd_treatment_id = ipd_treatment.id
				LEFT JOIN treatments ON treatments.id = ipd_treatment_details.treatment_id";
        
        if ($IPDId != '' || $treatmentDate != '') {
            $query .= " WHERE ";
        }
        if ($IPDId != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " ipd_treatment.ipd_id = '" . $IPDId . "'";
            $isFirst = "true";
        }
        if ($treatmentDate != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " ipd_treatment.treatments_date = '" . $treatmentDate . "'";
            $isFirst = "true";
        }
     
    $result = mysqli_query($conn, $query);
    //$totalrecords = mysqli_num_rows($result);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
    print json_encode($rows);
}
?>