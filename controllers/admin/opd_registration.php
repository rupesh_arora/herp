<?php
/*
 * File Name    :   opd_registration.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load and save data into database.
 */
$operation        = "";
$serviceType      = "";
$serviceId        = "";
$service          = "";
$serviceFee       = "";
$hrn              = "";
$CaseId           = "";
$visitType        = "";
$txtVisitType     = "";
$remarks          = "";
$consultantRoomId = "";
$headers 		= "";
$date             = new DateTime();

session_start(); // session start
$userId = "";
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
	exit();
}
include 'config.php'; // import database connection file
include '../AfricasTalkingGateway.php';
$operation = $_POST['operation']; // load opearion value from js


if ($operation == "showVisitType") { // opeartion to find visit type
    $sqlSelect = "SELECT id,`type` FROM visit_type WHERE type != 'Self Request' ORDER BY id ASC";
    
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "showServiceType") { // opeartion to find service type
    $sqlSelect = "SELECT id,`type` FROM service_type WHERE status='A' ORDER BY `type`";
    
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "showServices") { // opeartion to find services
    if (isset($_POST['serviceType'])) {
        $serviceType = $_POST['serviceType'];
    }
    
    $sqlSelect = "SELECT id,name FROM services WHERE service_type_id ='" . $serviceType . "' And status='A' ORDER BY name";
    
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "showServiceFee") { // opeartion to find service fee
    if (isset($_POST['serviceId'])) {
        $serviceId = $_POST['serviceId'];
    }
    
    $sqlSelect = "SELECT cost AS fee FROM services WHERE id ='" . $serviceId . "' And status='A'";
    
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "showInsuranceNumber") { 
	if (isset($_POST['patientId'])) {
        $patientId = $_POST['patientId'];
    }
    $insuranceCompany = $_POST['insuranceCompany'];
    $insuranceScheme = $_POST['insuranceScheme'];
    $insuranceSchemePlan = $_POST['insuranceSchemePlan'];

    $sqlSelect = "SELECT insurance_number FROM insurance WHERE patient_id ='".$patientId."' 
    AND insurance_company_id ='".$insuranceCompany."' AND insurance_plan_id ='".$insuranceScheme."'
    AND insurance_plan_name_id ='".$insuranceSchemePlan."'  And status='A'";

    $resultSelect = mysqli_query($conn, $sqlSelect);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "insuranceDetails") { // opeartion to find service fee
    if (isset($_POST['hrn'])) {
        $hrn = $_POST['hrn'];
    }
    
    $sqlSelect = "SELECT insurance.insurance_number,insurance.insurance_plan_name_id,insurance.insurance_company_id,insurance.insurance_plan_id FROM insurance 
		LEFT JOIN scheme_plans ON scheme_plans.id = insurance.insurance_plan_name_id
		WHERE insurance.patient_id ='" . $hrn . "' AND insurance.status='A' AND insurance.expiration_date > CURDATE()
		AND scheme_plans.end_date >=CURDATE() AND scheme_plans.status = 'A' ";
    
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "showInsuranceServiceFee") { // opeartion to find service fee
    if (isset($_POST['serviceId'])) {
        $serviceId = $_POST['serviceId'];
    }
    if (isset($_POST['companyId'])) {
        $companyId = $_POST['companyId'];
    }
    if (isset($_POST['planId'])) {
        $planId = $_POST['planId'];
    }
    if (isset($_POST['schemeId'])) {
        $schemeId = $_POST['schemeId'];
    }

    $sqlSelect = "SELECT id FROM scheme_exclusion 
		WHERE item_id ='" . $serviceId . "' AND insurance_company_id ='" . $companyId . "'   AND scheme_plan_id ='" . $planId . "'AND scheme_name_id ='" . $schemeId . "' AND `service_type` = 'Service'  And status='A'";
    
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $rows_count   = mysqli_num_rows($resultSelect);
	if ($rows_count == 0) {

	    $sqlSelect = "SELECT revised_cost AS fee FROM insurance_revised_cost 
			WHERE item_id ='" . $serviceId . "' AND insurance_company_id ='" . $companyId . "'   AND scheme_plan_id ='" . $planId . "'AND scheme_name_id ='" . $schemeId . "' AND `service_type` = 'Service'  AND status='A'";
	    
	    $resultSelect = mysqli_query($conn, $sqlSelect);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($resultSelect)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	else{
		echo '0';
	}
}

if ($operation == "showDepartment") { // opeartion to find department
    $sqlSelect = "SELECT id,`department` FROM department WHERE status='A' ORDER BY `department`";
    
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "hospitalProfile") { 
    
    $sqlSelect = "SELECT * FROM hospital_profile AS h ";
    
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}


if ($operation == "selectHRNInfo") { // code to show data to text on click of hrn button
    $getHRN = $_POST['GetHRN'];
	$patientPrefix = $_POST['patientPrefix'];
	$sql       = "SELECT value FROM configuration WHERE name = 'patient_prefix'";
	$result    = mysqli_query($conn, $sql);
	
	while ($r = mysqli_fetch_assoc($result)) {
		$prefix = $r['value'];
	}
	if($prefix == $patientPrefix){
    
		$sqlSelect    = "SELECT first_name, last_name, mobile, dob from patients where id='$getHRN'";
		$resultSelect = mysqli_query($conn, $sqlSelect);
		$rows_count   = mysqli_num_rows($resultSelect);
		if ($rows_count > 0) {
			$rows = array();
			while ($r = mysqli_fetch_assoc($resultSelect)) {
				$rows[] = $r;
			}
			print json_encode($rows);
		} else {
			echo "0";
		}
	}
	else{
		echo "2";
	}
}

if ($operation == "save") { // save details
    if (isset($_POST['selDepartment'])) {
        $department = $_POST['selDepartment'];
    }
    if (isset($_POST['hrn'])) {
        $hrn = $_POST['hrn'];
    }
    if (isset($_POST['roomId'])) {
        $roomId = $_POST['roomId'];
    }
    if (isset($_POST['remarks'])) {
        $remarks = $_POST['remarks'];
    }
    if (isset($_POST['visitType'])) {
        $visitType = $_POST['visitType'];
    }
    if (isset($_POST['txtVisitType'])) {
        $txtVisitType = $_POST['txtVisitType'];
    }
    if (isset($_POST['service'])) {
        $service = $_POST['service'];
    }
    if (isset($_POST['serviceFee'])) {
        $serviceFee = $_POST['serviceFee'];
    }
    if (isset($_POST['consultantName'])) {
        $consultantName = $_POST['consultantName'];
    }
    if (isset($_POST['paymentMode'])) {
        $paymentMode = $_POST['paymentMode'];
    }
    if (isset($_POST['insuranceNo'])) {
        $insuranceNo = $_POST['insuranceNo'];
    }
    if (isset($_POST['insurancePayment'])) {
        $insurancePayment = $_POST['insurancePayment'];
    }

    $insuranceCompany = $_POST['insuranceCompany'];
    $insuranceScheme = $_POST['insuranceScheme'];
    $schemePlan = $_POST['schemePlan'];
    $remainingAmount = '';

    /*This case is used to insert data when user select insurance*/
    if ($insuranceNo != "-1") {
        $insuranceNo = $_POST['insuranceNo'];
   
	    $insurance = '';
		
		$sqlInsurance   = "SELECT insurance_number FROM insurance WHERE insurance_number = '" . $insuranceNo . "' AND patient_id = '" . $hrn . "'";
		$queryInsurance = mysqli_query($conn, $sqlInsurance);
		while ($row = mysqli_fetch_row($queryInsurance)) {
			$insurance = $row[0];
		}
		if ($insurance == $insuranceNo){

			$sql_timeStamp   = "SELECT * FROM visits WHERE created_on BETWEEN UNIX_TIMESTAMP(curdate()) AND UNIX_TIMESTAMP(curdate() +1) AND patient_id = '" . $hrn . "'";
			$query_timeStamp = mysqli_query($conn, $sql_timeStamp);
			$rows_count   = mysqli_num_rows($query_timeStamp);
			if ($rows_count <= 0){
				if($txtVisitType == 'Follow Up OPD'){ //visit type 2 for follow up opd 
			
					$sql = "SELECT COUNT(case_id) AS case_id FROM visits WHERE visit_type IN (1, 3) AND patient_id = '" . $hrn . "' ORDER BY id DESC LIMIT 1"; //Check visit for this id
					$result = mysqli_query($conn, $sql);
					$rows_count   = mysqli_fetch_row($result);
					if ($rows_count[0] > 0) {
						$sqlCaseId = "SELECT MAX(case_id) FROM visits WHERE patient_id ='".$hrn."'"; //Select maximum case id for this patient
						$queryCaseId = mysqli_query($conn, $sqlCaseId);
						while ($row = mysqli_fetch_row($queryCaseId)) {
							$CaseId = $row[0];
						}
						if($CaseId != ""){
							if($department == -1) {
								$sql    = "INSERT INTO visits(visit_type,payment_mode,insurance_no,patient_id,remark,created_on,updated_on,created_by,updated_by,case_id) 
								VALUES('" . $visitType . "','" . $paymentMode . "','" . $insuranceNo . "','" . $hrn . "','" . $remarks . "'," . $date->getTimestamp() . "," . $date->getTimestamp() . "," . $userId . "," . $userId . ",'" . $CaseId . "')";
								$result = mysqli_query($conn, $sql);
							}
							else {
								$sql    = "INSERT INTO visits(visit_type,payment_mode,insurance_no,patient_id,department,remark,created_on,updated_on,created_by,updated_by,case_id) 
								VALUES('" . $visitType . "','" . $paymentMode . "','" . $insuranceNo . "','" . $hrn . "','" . $department . "','" . $remarks . "'," . $date->getTimestamp() . "," . $date->getTimestamp() . "," . $userId . "," . $userId . ",'" . $CaseId . "')";
								$result = mysqli_query($conn, $sql);
							}
							
							if ($result == '1') {

								$sql_id   = "SELECT LAST_INSERT_ID()";
								$query_id = mysqli_query($conn, $sql_id);
								while ($row = mysqli_fetch_row($query_id)) {
									$visitType = $row[0];
								}
								if ($visitType != '' && $visitType != '0') {

									//check service is excluded or not if not excluded then status should be paid else unpaid

									$checkService = 'SELECT item_id FROM scheme_exclusion WHERE service_type = "Service" AND item_id = "'.$service.'" AND insurance_company_id = "'.$insuranceCompany.'" AND scheme_plan_id  = "'.$insuranceScheme.'" AND scheme_name_id = "'.$schemePlan.'" AND status = "A"';
									$resultCheckService = mysqli_query($conn,$checkService);
									$countService = mysqli_num_rows($resultCheckService);
									 
									if ($countService == "0") {

										//now check it's copay
										$checkCopay = "SELECT copay.copay_type,scheme_plans.value 
										FROM scheme_plans 
										LEFT JOIN copay ON copay.id =  scheme_plans.copay 
										WHERE scheme_plans.insurance_company_id = '".$insuranceCompany."'
										AND scheme_plans.insurance_plan_id = '".$insuranceScheme."'
										AND scheme_plans.id = '".$schemePlan."' 
										AND scheme_plans.status = 'A'";

										$resultCopay = mysqli_query($conn,$checkCopay);
										while ($r = mysqli_fetch_assoc($resultCopay)) {
											$copayType = $r['copay_type'];
											$copayValue = $r['value'];
										}

										if (strtolower($copayType) == "none") {
											$paidStatus =  "paid";
										}
										elseif (strtolower($copayType) == "fixed") {
											if ($serviceFee <= $copayValue) {
											 	$paidStatus =  "paid";
											}
											else {
												//$remainingAmount = $serviceFee-$copayValue;
												$remainingAmount =$copayValue;
												$paidStatus =  "unpaid";
											}
										}
										else{
											if ($copayValue == "0" ) {
												$paidStatus =  "paid";
											}
											else{
												$remainingAmount = ($serviceFee*$copayValue)/100;
												//$remainingAmount = $serviceFee - $remainingAmount;
												$paidStatus =  "unpaid";
											}
										}									
									}
									else {
										$paidStatus =  "unpaid";
									}

									$sql = "INSERT INTO patient_service_bill(patient_id,payment_mode,service_id,charge,visit_id,created_on,updated_on,created_by,updated_by,status) 
										VALUES('" . $hrn . "','" . $insurancePayment . "','" . $service . "','" . $serviceFee . "','" . $visitType . "','" . $date->getTimestamp() . "','" . $date->getTimestamp() . "',
										'" . $userId . "','" . $userId . "','".$paidStatus."')";
									
									$result = mysqli_query($conn, $sql);
									
									if ($result == '1') {
										
										$sqlServiceRequest = "INSERT INTO services_request(patient_id,visit_id,service_id,
											request_date,pay_status,test_status,cost,status)
											VALUES('" . $hrn . "','" . $visitType . "','" . $service . "',UNIX_TIMESTAMP(),'".$paidStatus."','pending',
											'" . $serviceFee . "','A')";
										
										$resultSet = mysqli_query($conn, $sqlServiceRequest);
										
										if ($resultSet == '1') {
											$sql_case_id   = "SELECT visits.id,visits.department,visit_type.`type` as `vst_type` FROM visits 
											left join visit_type on visits.visit_type = visit_type.id ORDER BY visits.id DESC LIMIT 1";
											$query_case_id = mysqli_query($conn, $sql_case_id);
											while ($row = mysqli_fetch_row($query_case_id)) {
												$case_id = $row[0];
												$department = $row[1];
												$vst_type = $row[2];
											}
											
											if ($case_id != '') {
												$sqlSelect = "SELECT id,(SELECT value FROM configuration WHERE name = 'visit_prefix') AS prefix,
												CONCAT(first_name,' ',last_name) AS patient_name ,dob,middle_name, gender,
												 " . $case_id . " AS reg_id , '".$department."' AS department,'".$vst_type."' AS visit_type
												 FROM patients WHERE id = '" . $hrn . "'";
												
												$resultSelect = mysqli_query($conn, $sqlSelect);
												$rows         = array();
												while ($r = mysqli_fetch_assoc($resultSelect)) {
													$rows[] = $r;
												}

												if ($remainingAmount !='') {
													print json_encode($rows);
													echo $remainingAmount;
												}
												else{
													print json_encode($rows);
												}												
											}
										}
										if($resultSet != ''){
											$selQuery = "SELECT v.patient_id,concat(p.first_name,' ',p.last_name) as p_name,p.email,p.mobile,se.opd_booking_sms,p.country_code,
											DATE_FORMAT(FROM_UNIXTIME(v.created_on), '%Y-%m-%d') AS opd_date FROM sms_email AS se,visits AS v 
											LEFT JOIN patients AS p ON p.id = v.patient_id  WHERE patient_id = '" . $hrn . "'";
								        	$resultCheck = mysqli_query($conn,$selQuery);
											$totalrecords = mysqli_num_rows($resultCheck);
											$rows = array();
											$sms_message;
											$p_name;
											$mobile_no;
											$email;
											$date;
											while ($r = mysqli_fetch_assoc($resultCheck)) {
												$rows = $r;
												//print_r($rows);
												$sms_message = $rows['opd_booking_sms'];
												$p_name = $rows['p_name'];
												$mobile_no = $rows['mobile'];
												$email = $rows['email'];
												$date = $rows['opd_date'];
												$country_code = $rows['country_code'];
											}
											$previousMessage  = $sms_message;
											$replace = array("@@user_name@@","@@date@@");
											$replaced   = array($p_name,$date);
											$message = str_replace($replace, $replaced, $previousMessage);
											//print_r($message);

											$headers 		= "";

											
											/*if ($email) {
												mail($email, "OPD Booked '".$p_name."'", $message, $headers);

												$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
						  						created_on,updated_on,created_by,updated_by,msg_status) VALUES('OPD Booking','',
						  						'".$patientEmail."','','".$emailMessage."','".$start_date_time."',UNIX_TIMESTAMP(),
						  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','Mail')";

												$result = mysqli_query($conn, $sqlQuery);
												echo $result;
											}*/	
											
											
											// sms to patient
											$recipients = $country_code.$mobile_no;
											// And of course we want our recipients to know what we really do
											$message    = $message;
											//print_r($message);
											// Create a new instance of our awesome gateway class
											$gateway    = new AfricasTalkingGateway($username, $apikey);
											// Any gateway error will be captured by our custom Exception class below, 
											// so wrap the call in a try-catch block
											try 
											{ 
											  // Thats it, hit send and we'll take care of the rest. 
											  $results = $gateway->sendMessage($recipients, $message);

											  if($results){
											  	$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
											  						created_on,updated_on,created_by,updated_by,msg_status) VALUES('OPD Booking',
											  						'".$recipients."','','".$message."','','".$date."',UNIX_TIMESTAMP(),
											  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS')";
														$result = mysqli_query($conn, $sqlQuery);
														//echo $result;
											  }   
											            
											  foreach($results as $result) {
											    // status is either "Success" or "error message"
											    //echo " Number: " .$result->number;
											    //echo " Status: " .$result->status;
											    // " MessageId: " .$result->messageId;
											    //echo " Cost: "   .$result->cost."\n";
											  }
											}
											catch ( AfricasTalkingGatewayException $e )
											{
												$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
							  						created_on,updated_on,created_by,updated_by,msg_status,status) VALUES('OPD Booking',
							  						'".$recipients."','','".$message."','','".$date."',UNIX_TIMESTAMP(),
							  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS','pending')";
												$result = mysqli_query($conn, $sqlQuery);
												//echo $result;
											  //echo "Encountered an error while sending: ".$e->getMessage();
											}
											// DONE!!! 	
										}
									}
								}
							}
						}
					}
					else{
						echo "Follow up";
					}	
				}
				else{
					$sqlCaseId = "SELECT case_id FROM visits WHERE visit_type IN (1, 3) AND patient_id = '" . $hrn . "' ORDER BY id DESC LIMIT 1"; //Select maximum case id 
					$queryCaseId = mysqli_query($conn, $sqlCaseId);
					$rows_count= mysqli_num_rows($queryCaseId);
					if($rows_count > 0){
						while ($row = mysqli_fetch_row($queryCaseId)) {
							$CaseId = $row[0];
							$CaseId = (int)$CaseId + 1;						
						}
					}
					else{
						$CaseId = "1";
					}
					if($CaseId != ""){
						if($department == -1) {
							$sql    = "INSERT INTO visits(visit_type,payment_mode,insurance_no,patient_id,remark,created_on,updated_on,created_by,updated_by,case_id) 
							VALUES('" . $visitType . "','" . $paymentMode . "','" . $insuranceNo . "','" . $hrn . "','" . $remarks . "'," . $date->getTimestamp() . "," . $date->getTimestamp() . "," . $userId . "," . $userId . ",'" . $CaseId . "')";
							$result = mysqli_query($conn, $sql);
						}
						else {
							$sql    = "INSERT INTO visits(visit_type,payment_mode,insurance_no,patient_id,department,remark,created_on,updated_on,created_by,updated_by,case_id) 
							VALUES('" . $visitType . "','" . $paymentMode . "','" . $insuranceNo . "','" . $hrn . "','" . $department . "','" . $remarks . "'," . $date->getTimestamp() . "," . $date->getTimestamp() . "," . $userId . "," . $userId . ",'" . $CaseId . "')";
							$result = mysqli_query($conn, $sql);
						}
						if ($result == '1') {
							
							$sql_id   = "SELECT LAST_INSERT_ID()";
							$query_id = mysqli_query($conn, $sql_id);
							while ($row = mysqli_fetch_row($query_id)) {
								$visitType = $row[0];
							}
							if ($visitType != '' && $visitType != '0') {


								//check service is excluded or not if not excluded then status should be paid else unpaid
								$checkService = 'SELECT item_id FROM scheme_exclusion WHERE service_type = "Service" AND item_id = "'.$service.'" AND insurance_company_id = "'.$insuranceCompany.'" AND scheme_plan_id  = "'.$insuranceScheme.'" AND scheme_name_id = "'.$schemePlan.'" AND status = "A"';
								$resultCheckService = mysqli_query($conn,$checkService);
								$countService = mysqli_num_rows($resultCheckService);
								if ($countService == "0") {

									//now check it's copay
									$checkCopay = "SELECT copay.copay_type,scheme_plans.value 
									FROM scheme_plans 
									LEFT JOIN copay ON copay.id =  scheme_plans.copay 
									WHERE scheme_plans.insurance_company_id = '".$insuranceCompany."'
									AND scheme_plans.insurance_plan_id = '".$insuranceScheme."'
									AND scheme_plans.id = '".$schemePlan."' 
									AND scheme_plans.status = 'A'";

									$resultCopay = mysqli_query($conn,$checkCopay);
									while ($r = mysqli_fetch_assoc($resultCopay)) {
										$copayType = $r['copay_type'];
										$copayValue = $r['value'];
									}

									if (strtolower($copayType) == "none") {
										$paidStatus =  "paid";
									}
									elseif (strtolower($copayType) == "fixed") {
										if ($serviceFee <= $copayValue) {
										 	$paidStatus =  "paid";
										}
										else {
											//$remainingAmount = $serviceFee-$copayValue;
											$remainingAmount = $copayValue;
											$paidStatus =  "unpaid";
										}
									}
									else{
										if ($copayValue == "100") {
											$paidStatus =  "paid";
										}
										else{
											$remainingAmount = ($serviceFee*$copayValue)/100;
											//$remainingAmount = $serviceFee - $remainingAmount;
											$paidStatus =  "unpaid";
										}
									}									
								}
								else {
									$paidStatus =  "unpaid";
								}

								$sql = "INSERT INTO patient_service_bill(patient_id,payment_mode,service_id,charge,visit_id,created_on,updated_on,created_by,updated_by,status) 
									VALUES('" . $hrn . "','" . $insurancePayment . "','" . $service . "','" . $serviceFee . "','" . $visitType . "','" . $date->getTimestamp() . "','" . $date->getTimestamp() . "',
									'" . $userId . "','" . $userId . "','".$paidStatus."')";
								
								$result = mysqli_query($conn, $sql);
								
								if ($result == '1') {
									
									$sqlServiceRequest = "INSERT INTO services_request(patient_id,visit_id,service_id,
										request_date,pay_status,test_status,cost,status)
										VALUES('" . $hrn . "','" . $visitType . "','" . $service . "',UNIX_TIMESTAMP(),'".$paidStatus."','pending',
										'" . $serviceFee . "','A')";
									
									$resultSet = mysqli_query($conn, $sqlServiceRequest);
									
									if ($resultSet == '1') {
										$sql_case_id   = "SELECT visits.id,visits.department,visit_type.`type` as `vst_type` FROM visits 
											left join visit_type on visits.visit_type = visit_type.id ORDER BY visits.id DESC LIMIT 1";
										$query_case_id = mysqli_query($conn, $sql_case_id);
										while ($row = mysqli_fetch_row($query_case_id)) {
											$case_id = $row[0];
											$department = $row[1];
											$vst_type = $row[2];
										}
										
										if ($case_id != '') {
											$sqlSelect = "SELECT id,(SELECT value FROM configuration WHERE name = 'visit_prefix') AS prefix,
											CONCAT(first_name,' ',middle_name,' ',last_name) AS patient_name ,dob,middle_name, gender,
														 " . $case_id . " AS reg_id ,'".$visitType."' AS visit_id, '".$department."' AS department,'".$vst_type."' AS visit_type
														 FROM patients WHERE id = '" . $hrn . "'";
											
											$resultSelect = mysqli_query($conn, $sqlSelect);
											$rows         = array();
											while ($r = mysqli_fetch_assoc($resultSelect)) {
												$rows[] = $r;
											}
											if ($remainingAmount !='') {
												print json_encode($rows);
												echo $remainingAmount;
											}
											else{
												print json_encode($rows);
											}
											
										}
									}
									if($resultSet != ''){
										$selQuery = "SELECT v.patient_id,concat(p.first_name,' ',p.last_name) as p_name,p.email,p.mobile,se.opd_booking_sms,p.country_code,
										DATE_FORMAT(FROM_UNIXTIME(v.created_on), '%Y-%m-%d') AS opd_date FROM sms_email AS se,visits AS v 
										LEFT JOIN patients AS p ON p.id = v.patient_id  WHERE patient_id = '" . $hrn . "'";
							        	$resultCheck = mysqli_query($conn,$selQuery);
										$totalrecords = mysqli_num_rows($resultCheck);
										$rows = array();
										$sms_message;
										$p_name;
										$mobile_no;
										$email;
										$date;
										while ($r = mysqli_fetch_assoc($resultCheck)) {
											$rows = $r;
											//print_r($rows);
											$sms_message = $rows['opd_booking_sms'];
											$p_name = $rows['p_name'];
											$mobile_no = $rows['mobile'];
											$email = $rows['email'];
											$date = $rows['opd_date'];
											$country_code = $rows['country_code'];
										}
										$previousMessage  = $sms_message;
										$replace = array("@@user_name@@","@@date@@");
										$replaced   = array($p_name,$date);
										$message = str_replace($replace, $replaced, $previousMessage);
										//print_r($message);

										$headers 		= "";

										
										/*if ($email) {
											mail($email, "OPD Booked '".$p_name."'", $message, $headers);

											$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
					  						created_on,updated_on,created_by,updated_by,msg_status) VALUES('OPD Booking','',
					  						'".$patientEmail."','','".$emailMessage."','".$start_date_time."',UNIX_TIMESTAMP(),
					  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','Mail')";

											$result = mysqli_query($conn, $sqlQuery);
											echo $result;
										}*/	
										
										
										// sms to patient
										$recipients = $country_code.$mobile_no;
										// And of course we want our recipients to know what we really do
										$message    = $message;
										//print_r($message);
										// Create a new instance of our awesome gateway class
										$gateway    = new AfricasTalkingGateway($username, $apikey);
										// Any gateway error will be captured by our custom Exception class below, 
										// so wrap the call in a try-catch block
										try 
										{ 
										  // Thats it, hit send and we'll take care of the rest. 
										  $results = $gateway->sendMessage($recipients, $message);

										  if($results){
										  	$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
										  						created_on,updated_on,created_by,updated_by,msg_status) VALUES('OPD Booking',
										  						'".$recipients."','','".$message."','','".$date."',UNIX_TIMESTAMP(),
										  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS')";
													$result = mysqli_query($conn, $sqlQuery);
													//echo $result;
										  }   
										            
										  foreach($results as $result) {
										    // status is either "Success" or "error message"
										    //echo " Number: " .$result->number;
										    //echo " Status: " .$result->status;
										    // " MessageId: " .$result->messageId;
										    //echo " Cost: "   .$result->cost."\n";
										  }
										}
										catch ( AfricasTalkingGatewayException $e )
										{
											$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
						  						created_on,updated_on,created_by,updated_by,msg_status,status) VALUES('OPD Booking',
						  						'".$recipients."','','".$message."','','".$date."',UNIX_TIMESTAMP(),
						  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS','pending')";
											$result = mysqli_query($conn, $sqlQuery);
											//echo $result;
										  //echo "Encountered an error while sending: ".$e->getMessage();
										}
										// DONE!!! 	
									}
								}
							}
						}
					}
					else{
						echo "0";
					}
				}
			}
			else if($rows_count > 0){
				echo "visit";
			}
		}
		else{
			echo 'Insurance';
		}	
	}

	/*This case is used to insert data when user select cash*/
	else{
		$sql_timeStamp   = "SELECT * FROM visits WHERE created_on BETWEEN UNIX_TIMESTAMP(curdate()) AND UNIX_TIMESTAMP(curdate() +1) AND patient_id = '" . $hrn . "'";
		$query_timeStamp = mysqli_query($conn, $sql_timeStamp);
		$rows_count   = mysqli_num_rows($query_timeStamp);
		if ($rows_count <= 0){
			if($txtVisitType == 'Follow Up OPD'){ //visit type 2 for follow up opd 
		
				$sql = "SELECT case_id FROM visits WHERE visit_type IN (1, 3) AND patient_id = '" . $hrn . "' ORDER BY id DESC LIMIT 1"; //Check visit for this id
				$result = mysqli_query($conn, $sql);
				$rows_count   = mysqli_fetch_row($result);
				if ($rows_count[0] > 0) {
					$sqlCaseId = "SELECT MAX(case_id) FROM visits WHERE patient_id ='".$hrn."'"; //Select maximum case id for this patient
					$queryCaseId = mysqli_query($conn, $sqlCaseId);
					while ($row = mysqli_fetch_row($queryCaseId)) {
						$CaseId = $row[0];
					}
					if($CaseId != ""){
						if($department == -1) {
							$sql    = "INSERT INTO visits(visit_type,payment_mode,insurance_no,patient_id,remark,created_on,updated_on,created_by,updated_by,case_id) 
							VALUES('" . $visitType . "','" . $paymentMode . "','" . $insuranceNo . "','" . $hrn . "','" . $remarks . "'," . $date->getTimestamp() . "," . $date->getTimestamp() . "," . $userId . "," . $userId . ",'" . $CaseId . "')";
							$result = mysqli_query($conn, $sql);
						}
						else {
							$sql    = "INSERT INTO visits(visit_type,payment_mode,insurance_no,patient_id,department,remark,created_on,updated_on,created_by,updated_by,case_id) 
							VALUES('" . $visitType . "','" . $paymentMode . "','" . $insuranceNo . "','" . $hrn . "','" . $department . "','" . $remarks . "'," . $date->getTimestamp() . "," . $date->getTimestamp() . "," . $userId . "," . $userId . ",'" . $CaseId . "')";
							$result = mysqli_query($conn, $sql);
						}
						
						if ($result == '1') {
							
							$sql_id   = "SELECT LAST_INSERT_ID()";
							$query_id = mysqli_query($conn, $sql_id);
							while ($row = mysqli_fetch_row($query_id)) {
								$visitType = $row[0];
							}
							if ($visitType != '' && $visitType != '0') {
								$sql = "INSERT INTO patient_service_bill(patient_id,service_id,charge,visit_id,created_on,updated_on,created_by,updated_by) 
									VALUES('" . $hrn . "','" . $service . "','" . $serviceFee . "','" . $visitType . "','" . $date->getTimestamp() . "','" . $date->getTimestamp() . "',
									'" . $userId . "','" . $userId . "')";
								
								$result = mysqli_query($conn, $sql);
								
								if ($result == '1') {
									
									$sqlServiceRequest = "INSERT INTO services_request(patient_id,visit_id,service_id,
										request_date,pay_status,test_status,cost,status)
										VALUES('" . $hrn . "','" . $visitType . "','" . $service . "',UNIX_TIMESTAMP(),'unpaid','pending',
										'" . $serviceFee . "','A')";
									
									$resultSet = mysqli_query($conn, $sqlServiceRequest);
									
									if ($resultSet == '1') {
										$sql_case_id   = "SELECT visits.id,visits.department,visit_type.`type` as `vst_type` FROM visits 
										left join visit_type on visits.visit_type = visit_type.id ORDER BY visits.id DESC LIMIT 1";
										$query_case_id = mysqli_query($conn, $sql_case_id);
										while ($row = mysqli_fetch_row($query_case_id)) {
											$case_id = $row[0];
											$department = $row[1];
											$vst_type = $row[2];
										}
										
										if ($case_id != '') {
											$sqlSelect = "SELECT id,(SELECT value FROM configuration WHERE name = 'visit_prefix') AS prefix,
											CONCAT(first_name,' ',last_name) AS patient_name ,dob,middle_name, gender,
											 " . $case_id . " AS reg_id , '".$department."' AS department,'".$vst_type."' AS visit_type
											 FROM patients WHERE id = '" . $hrn . "'";
											
											$resultSelect = mysqli_query($conn, $sqlSelect);
											$rows         = array();
											while ($r = mysqli_fetch_assoc($resultSelect)) {
												$rows[] = $r;
											}
											print json_encode($rows);
										}
									}
									if($resultSet != ''){
										$selQuery = "SELECT v.patient_id,concat(p.first_name,' ',p.last_name) as p_name,p.email,p.mobile,se.opd_booking_sms,p.country_code,
										DATE_FORMAT(FROM_UNIXTIME(v.created_on), '%Y-%m-%d') AS opd_date FROM sms_email AS se,visits AS v 
										LEFT JOIN patients AS p ON p.id = v.patient_id  WHERE patient_id = '" . $hrn . "'";
							        	$resultCheck = mysqli_query($conn,$selQuery);
										$totalrecords = mysqli_num_rows($resultCheck);
										$rows = array();
										$sms_message;
										$p_name;
										$mobile_no;
										$email;
										$date;
										while ($r = mysqli_fetch_assoc($resultCheck)) {
											$rows = $r;
											//print_r($rows);
											$sms_message = $rows['opd_booking_sms'];
											$p_name = $rows['p_name'];
											$mobile_no = $rows['mobile'];
											$email = $rows['email'];
											$date = $rows['opd_date'];
											$country_code = $rows['country_code'];
										}
										$previousMessage  = $sms_message;
										$replace = array("@@user_name@@","@@date@@");
										$replaced   = array($p_name,$date);
										$message = str_replace($replace, $replaced, $previousMessage);
										//print_r($message);

										$headers 		= "";

										
										/*if ($email) {
											mail($email, "OPD Booked '".$p_name."'", $message, $headers);

											$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
					  						created_on,updated_on,created_by,updated_by,msg_status) VALUES('OPD Booking','',
					  						'".$patientEmail."','','".$emailMessage."','".$start_date_time."',UNIX_TIMESTAMP(),
					  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','Mail')";

											$result = mysqli_query($conn, $sqlQuery);
											echo $result;
										}*/	
										
										
										// sms to patient
										$recipients = $country_code.$mobile_no;
										// And of course we want our recipients to know what we really do
										$message    = $message;
										//print_r($message);
										// Create a new instance of our awesome gateway class
										$gateway    = new AfricasTalkingGateway($username, $apikey);
										// Any gateway error will be captured by our custom Exception class below, 
										// so wrap the call in a try-catch block
										try 
										{ 
										  // Thats it, hit send and we'll take care of the rest. 
										  $results = $gateway->sendMessage($recipients, $message);

										  if($results){
										  	$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
										  						created_on,updated_on,created_by,updated_by,msg_status) VALUES('OPD Booking',
										  						'".$recipients."','','".$message."','','".$date."',UNIX_TIMESTAMP(),
										  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS')";
													$result = mysqli_query($conn, $sqlQuery);
													//echo $result;
										  }   
										            
										  foreach($results as $result) {
										    // status is either "Success" or "error message"
										    //echo " Number: " .$result->number;
										    //echo " Status: " .$result->status;
										    // " MessageId: " .$result->messageId;
										    //echo " Cost: "   .$result->cost."\n";
										  }
										}
										catch ( AfricasTalkingGatewayException $e )
										{
											$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
						  						created_on,updated_on,created_by,updated_by,msg_status,status) VALUES('OPD Booking',
						  						'".$recipients."','','".$message."','','".$date."',UNIX_TIMESTAMP(),
						  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS','pending')";
											$result = mysqli_query($conn, $sqlQuery);
											//echo $result;
										  //echo "Encountered an error while sending: ".$e->getMessage();
										}
										// DONE!!! 	
									}
								}
							}
						}
					}
				}
				else{
					echo "Follow up";
				}	
			}
			else{
				$sqlCaseId = "SELECT case_id FROM visits WHERE visit_type IN (1, 3) AND patient_id = '" . $hrn . "' ORDER BY id DESC LIMIT 1"; //Select maximum case id 
				$queryCaseId = mysqli_query($conn, $sqlCaseId);
				$rows_count= mysqli_num_rows($queryCaseId);
				if($rows_count > 0){
					while ($row = mysqli_fetch_row($queryCaseId)) {
						$CaseId = $row[0];
						$CaseId = (int)$CaseId + 1;						
					}
				}
				else{
					$CaseId = "1";
				}
				if($CaseId != ""){
					if($department == -1) {
						$sql    = "INSERT INTO visits(visit_type,payment_mode,insurance_no,patient_id,remark,created_on,updated_on,created_by,updated_by,case_id) 
						VALUES('" . $visitType . "','" . $paymentMode . "','" . $insuranceNo . "','" . $hrn . "','" . $remarks . "'," . $date->getTimestamp() . "," . $date->getTimestamp() . "," . $userId . "," . $userId . ",'" . $CaseId . "')";
						$result = mysqli_query($conn, $sql);
					}
					else {
						$sql    = "INSERT INTO visits(visit_type,payment_mode,insurance_no,patient_id,department,remark,created_on,updated_on,created_by,updated_by,case_id) 
						VALUES('" . $visitType . "','" . $paymentMode . "','" . $insuranceNo . "','" . $hrn . "','" . $department . "','" . $remarks . "'," . $date->getTimestamp() . "," . $date->getTimestamp() . "," . $userId . "," . $userId . ",'" . $CaseId . "')";
						$result = mysqli_query($conn, $sql);
					}
					if ($result == '1') {
						
						$sql_id   = "SELECT LAST_INSERT_ID()";
						$query_id = mysqli_query($conn, $sql_id);
						while ($row = mysqli_fetch_row($query_id)) {
							$visitType = $row[0];
						}
						if ($visitType != '' && $visitType != '0') {
							$sql = "INSERT INTO patient_service_bill(patient_id,service_id,charge,visit_id,created_on,updated_on,created_by,updated_by) 
								VALUES('" . $hrn . "','" . $service . "','" . $serviceFee . "','" . $visitType . "','" . $date->getTimestamp() . "','" . $date->getTimestamp() . "',
								'" . $userId . "','" . $userId . "')";
							
							$result = mysqli_query($conn, $sql);
							
							if ($result == '1') {
								
								$sqlServiceRequest = "INSERT INTO services_request(patient_id,visit_id,service_id,
									request_date,pay_status,test_status,cost,status)
									VALUES('" . $hrn . "','" . $visitType . "','" . $service . "',UNIX_TIMESTAMP(),'unpaid','pending',
									'" . $serviceFee . "','A')";
								
								$resultSet = mysqli_query($conn, $sqlServiceRequest);
								
								if ($resultSet == '1') {
									$sql_case_id   = "SELECT visits.id,visits.department,visit_type.`type` as `vst_type` FROM visits 
										left join visit_type on visits.visit_type = visit_type.id ORDER BY visits.id DESC LIMIT 1";
									$query_case_id = mysqli_query($conn, $sql_case_id);
									while ($row = mysqli_fetch_row($query_case_id)) {
										$case_id = $row[0];
										$department = $row[1];
										$vst_type = $row[2];
									}
									
									if ($case_id != '') {
										$sqlSelect = "SELECT id,(SELECT value FROM configuration WHERE name = 'visit_prefix') AS prefix,
										CONCAT(first_name,' ',middle_name,' ',last_name) AS patient_name ,dob,middle_name, gender,
													 " . $case_id . " AS reg_id ,'".$visitType."' AS visit_id, '".$department."' AS department,'".$vst_type."' AS visit_type
													 FROM patients WHERE id = '" . $hrn . "'";
										
										$resultSelect = mysqli_query($conn, $sqlSelect);
										$rows         = array();
										while ($r = mysqli_fetch_assoc($resultSelect)) {
											$rows[] = $r;
										}
										print json_encode($rows);
									}
								}
								if($resultSet != ''){
									$selQuery = "SELECT v.patient_id,concat(p.first_name,' ',p.last_name) as p_name,p.email,p.mobile,se.opd_booking_sms,p.country_code,
									DATE_FORMAT(FROM_UNIXTIME(v.created_on), '%Y-%m-%d') AS opd_date FROM sms_email AS se,visits AS v 
									LEFT JOIN patients AS p ON p.id = v.patient_id  WHERE patient_id = '" . $hrn . "'";
						        	$resultCheck = mysqli_query($conn,$selQuery);
									$totalrecords = mysqli_num_rows($resultCheck);
									$rows = array();
									$sms_message;
									$p_name;
									$mobile_no;
									$email;
									$date;
									while ($r = mysqli_fetch_assoc($resultCheck)) {
										$rows = $r;
										//print_r($rows);
										$sms_message = $rows['opd_booking_sms'];
										$p_name = $rows['p_name'];
										$mobile_no = $rows['mobile'];
										$email = $rows['email'];
										$date = $rows['opd_date'];
										$country_code = $rows['country_code'];
									}
									$previousMessage  = $sms_message;
									$replace = array("@@user_name@@","@@date@@");
									$replaced   = array($p_name,$date);
									$message = str_replace($replace, $replaced, $previousMessage);
									//print_r($message);

									$headers 		= "";

									
									/*if ($email) {
										mail($email, "OPD Booked '".$p_name."'", $message, $headers);

										$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
				  						created_on,updated_on,created_by,updated_by,msg_status) VALUES('OPD Booking','',
				  						'".$patientEmail."','','".$emailMessage."','".$start_date_time."',UNIX_TIMESTAMP(),
				  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','Mail')";

										$result = mysqli_query($conn, $sqlQuery);
										echo $result;
									}*/	
									
									
									// sms to patient
									$recipients = $country_code.$mobile_no;
									// And of course we want our recipients to know what we really do
									$message    = $message;
									//print_r($message);
									// Create a new instance of our awesome gateway class
									$gateway    = new AfricasTalkingGateway($username, $apikey);
									// Any gateway error will be captured by our custom Exception class below, 
									// so wrap the call in a try-catch block
									try 
									{ 
									  // Thats it, hit send and we'll take care of the rest. 
									  $results = $gateway->sendMessage($recipients, $message);

									  if($results){
									  	$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
									  						created_on,updated_on,created_by,updated_by,msg_status) VALUES('OPD Booking',
									  						'".$recipients."','','".$message."','','".$date."',UNIX_TIMESTAMP(),
									  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS')";
												$result = mysqli_query($conn, $sqlQuery);
												//echo $result;
									  }   
									            
									  foreach($results as $result) {
									    // status is either "Success" or "error message"
									    //echo " Number: " .$result->number;
									    //echo " Status: " .$result->status;
									    // " MessageId: " .$result->messageId;
									    //echo " Cost: "   .$result->cost."\n";
									  }
									}
									catch ( AfricasTalkingGatewayException $e )
									{
										$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
					  						created_on,updated_on,created_by,updated_by,msg_status,status) VALUES('OPD Booking',
					  						'".$recipients."','','".$message."','','".$date."',UNIX_TIMESTAMP(),
					  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS','pending')";
										$result = mysqli_query($conn, $sqlQuery);
										//echo $result;
									  //echo "Encountered an error while sending: ".$e->getMessage();
									}
									// DONE!!! 	
								}
							}
						}
					}
				}
				else{
					echo "0";
				}
			}
		}
		else if($rows_count > 0){
			echo "visit";					
		}
	}
}
?>