<?php
/*File Name  :   bed.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page manages AND add beds*/

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

/*include config file*/
include 'config.php';

$operation   = "";
$description = "";

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

/*operation to show active data*/
if ($operation == "showImprestRequest") {
    
        $query        = "SELECT iw.*,CONCAT(u.first_name,' ',u.last_name) AS name,u.staff_type_id,l.name AS ledger_name,(SELECT value FROM configuration WHERE name ='imprest_prefix') AS imprest_prefix FROM imprest_warrent AS iw 
            LEFT JOIN users AS u ON u.id = iw.staff_id  
            LEFT JOIN ledger AS l ON iw.ledger_id = l.id  
            WHERE iw.status='A' AND u.status='A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

if ($operation == "showPaymentMode") {
    if (isset($_POST['ledgerId'])) {
        $ledgerId = $_POST['ledgerId'];
    }

    $query  = "SELECT id,name FROM payment_mode WHERE status='A' AND ledger_type = '".$ledgerId."' ORDER BY name";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

/*operation to save data*/
if ($operation == "save") {
    if (isset($_POST['imprestId'])) {
        $imprestId = $_POST['imprestId'];
    }
    if (isset($_POST['chequeNo'])) {
        $chequeNo = $_POST['chequeNo'];
    }
    if (isset($_POST['paymentMode'])) {
        $paymentMode = $_POST['paymentMode'];
    }
    if (isset($_POST['amount'])) {
        $amount = $_POST['amount'];
    }
    if (isset($_POST['bankTransfer'])) {
        $bankTransfer = $_POST['bankTransfer'];
    }
    
   
    $sql = "INSERT INTO imprest_disbursement(imprest_request_id,amount,payment_mode,cheque_no,bank_transfer,created_on,created_by,updated_on,updated_by) VALUES('" . $imprestId . "','" . $amount . "','" . $paymentMode . "','" . $chequeNo . "','" . $bankTransfer . "',UNIX_TIMESTAMP(),'".$userId."',UNIX_TIMESTAMP(),'".$userId."')";
    $result = mysqli_query($conn, $sql);
    echo $result;
}

/*operation to show active data*/
if ($operation == "show") {
    
    $query        = "SELECT i_d.amount AS disbursement_amount,i_d.id,i_d.imprest_request_id, i_d.`payment_mode`,i_d.cheque_no,i_d.bank_transfer, p.name,CONCAT(u.first_name,' ',u.last_name) AS applicantName,l.name AS ledger_name,iw.* 
                    ,(SELECT value FROM configuration WHERE name ='imprest_prefix') AS imprest_prefix
                    FROM imprest_disbursement AS i_d 
                    LEFT JOIN payment_mode AS p ON p.id = i_d.`payment_mode`  
                    LEFT JOIN imprest_warrent AS iw ON iw.imprest_request_id = i_d.imprest_request_id 
                    LEFT JOIN users AS u ON u.id = iw.staff_id  
                    LEFT JOIN ledger AS l ON iw.ledger_id = l.id  
                     WHERE i_d.status='A' AND p.status='A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/*operation to show inactive data*/
if ($operation == "showInActive") {
    $query        = "SELECT i_d.*, p.name,CONCAT(u.first_name,' ',u.last_name) AS applicantName,l.name AS ledger_name,iw.* 
                    ,(SELECT value FROM configuration WHERE name ='imprest_prefix') AS imprest_prefix
                    FROM imprest_disbursement AS i_d 
                    LEFT JOIN payment_mode AS p ON p.id = i_d.`payment_mode`  
                    LEFT JOIN imprest_warrent AS iw ON iw.imprest_request_id = i_d.imprest_request_id 
                    LEFT JOIN users AS u ON u.id = iw.staff_id  
                    LEFT JOIN ledger AS l ON iw.ledger_id = l.id  
                    WHERE i_d.status='I' OR p.status='I'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/*operation to delete data*/
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE imprest_disbursement SET status= 'I' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
	if($result){
		echo "1";
	}else{
		echo "0";
	}  
}

/*operation to restore data*/
if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE imprest_disbursement SET status= 'A' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);   
    
        
    if ($result) {
        echo "Restored Successfully!!!";
    } else {
        echo "It can not be restore!!!";
    }
}

/*operation to update data*/
if ($operation == "update") {

    if (isset($_POST['imprestId'])) {
        $imprestId = $_POST['imprestId'];
    }
    if (isset($_POST['chequeNo'])) {
        $chequeNo = $_POST['chequeNo'];
    }
    if (isset($_POST['paymentMode'])) {
        $paymentMode = $_POST['paymentMode'];
    }
    if (isset($_POST['amount'])) {
        $amount = $_POST['amount'];
    }
    if (isset($_POST['bankTransfer'])) {
        $bankTransfer = $_POST['bankTransfer'];
    }
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
   
   $sql    = "UPDATE imprest_disbursement SET imprest_request_id = '" . $imprestId . "',amount= '" . $amount . "',payment_mode= '" . $paymentMode . "',cheque_no= '" . $chequeNo . "',bank_transfer= '" . $bankTransfer . "',updated_on = UNIX_TIMESTAMP(),updated_by='".$userId."' WHERE  id = '" . $id . "'";
        $result = mysqli_query($conn, $sql);
    echo $result;
}
?>