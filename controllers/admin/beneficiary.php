<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "saveBeneficiary") {

		$beneficiaryName = $_POST['beneficiaryName'];
		$companyName = $_POST['companyName'];
		$description = $_POST['description'];

		$selLedger = "SELECT beneficiary_name FROM beneficiary WHERE beneficiary_name ='".$beneficiaryName."' AND company_name ='".$companyName."'AND status = 'A'";
		$checkLedger = mysqli_query($conn,$selLedger);
		$countLedger = mysqli_num_rows($checkLedger);

		if ($countLedger == "0") {

			$query = "INSERT INTO beneficiary(beneficiary_name,company_name,description,created_by,updated_by,created_on,updated_on) VALUES('".$beneficiaryName."','".$companyName."','".$description."','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";

			$result = mysqli_query($conn, $query);
			echo $result;
		}
		else{
			echo "0";
		}
	}

	if ($operation == "show") { // show active data
    
    	$query = "SELECT id,beneficiary_name,company_name,description FROM beneficiary WHERE status = 'A'";
	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}

	if ($operation == "checked") {
	    
	    $query = "SELECT id,beneficiary_name,company_name,description FROM beneficiary WHERE status = 'I'";
	    
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	if ($operation == "update") // update data
	{
	    $beneficiaryName = $_POST['beneficiaryName'];
	    $companyName = $_POST['companyName'];
		$description = $_POST['description'];
	    $id = $_POST['id'];

	    $selLedger = "SELECT beneficiary_name FROM beneficiary WHERE beneficiary_name ='".$beneficiaryName."' AND company_name ='".$companyName."' AND status = 'A' AND id != '".$id."' ";
		$checkLedger = mysqli_query($conn,$selLedger);
		$countLedger = mysqli_num_rows($checkLedger);

		if ($countLedger == "0") {
		
			$sql    = "UPDATE beneficiary set beneficiary_name = '".$beneficiaryName."',company_name = '".$companyName."',description= '".$description."',updated_on = UNIX_TIMESTAMP() ,updated_by = '".$userId."' where id = '".$id."' ";

			$result = mysqli_query($conn, $sql);
			echo $result;
		}
		else{
			echo "0";
		}
	}

	if ($operation == "delete") {
        $id = $_POST['id'];

		$sql    = "UPDATE beneficiary SET status= 'I' where id = '" . $id . "'";
	    $result = mysqli_query($conn, $sql);
	    echo $result;	    
	}

	if ($operation == "restore") {// for restore    
        $id = $_POST['id'];
        $beneficiaryName = $_POST['beneficiaryName'];	
        $companyName = $_POST['companyName'];

        $selLedger = "SELECT beneficiary_name FROM beneficiary WHERE beneficiary_name ='".$beneficiaryName."' AND company_name ='".$companyName."' AND status = 'A' AND id != '".$id."' ";
		$checkLedger = mysqli_query($conn,$selLedger);
		$countLedger = mysqli_num_rows($checkLedger);

		if ($countLedger == "0") {

			echo $sql    = "UPDATE beneficiary SET status= 'A'  WHERE  id = '" . $id . "'";
			$result = mysqli_query($conn, $sql);
		    echo $result;
		}
		else{
			echo "0";
		}
	}
?>