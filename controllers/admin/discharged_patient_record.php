<?php
	
	session_start();
	if(!session_id()){
		exit();
	}
	
	include_once('config.php');

	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET['operation'])){
		$operation=$_GET["operation"];
	}


	if ($operation=="showChartData") {
		if (isset($_POST['dataLoad'])) {
			$dataLoad = $_POST["dataLoad"];
		}
		if (isset($_POST['fromDate'])) {
			$fromDate = $_POST["fromDate"];
		}
		if (isset($_POST['toDate'])) {
			$toDate = $_POST["toDate"];
		}

		if ($dataLoad =="all") {
			$sql = "SELECT (SELECT IFNULL(SUM(charge),0) from patient_lab_bill WHERE status = 'paid') 
			AS 'Lab Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_procedure_bill WHERE status = 'paid') 
			AS 'Procedure Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_radiology_bill WHERE status = 'paid') 
			AS 'Radiology Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_service_bill WHERE status = 'paid') 
			AS 'Service Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_forensic_bill WHERE status = 'paid') 
			AS 'Forensic Bill'";
		}
		
		else if ($dataLoad =="last30days") {
			$sql = "SELECT (SELECT IFNULL(SUM(charge),0) from patient_lab_bill WHERE status = 'paid' 
			AND created_on >= unix_timestamp(CURDATE() - interval 1 month))AS 'Lab Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_procedure_bill WHERE status = 'paid' 
			AND created_on >= unix_timestamp(CURDATE() - interval 1 month)) AS 'Procedure Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_radiology_bill WHERE status = 'paid' 
			AND created_on >= unix_timestamp(CURDATE() - interval 1 month)) AS 'Radiology Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_service_bill WHERE status = 'paid' 
			AND created_on >= unix_timestamp(CURDATE() - interval 1 month)) AS 'Service Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_forensic_bill WHERE status = 'paid'
			AND created_on >= unix_timestamp(CURDATE() - interval 1 month)) AS 'Forensic Bill'";
		}

		else if ($dataLoad =="last7days") {
			$sql = "SELECT (SELECT IFNULL(SUM(charge),0) from patient_lab_bill WHERE status = 'paid' 
			AND created_on >= unix_timestamp(CURDATE() - interval 7 day))AS 'Lab Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_procedure_bill WHERE status = 'paid' 
			AND created_on >= unix_timestamp(CURDATE() - interval 7 day)) AS 'Procedure Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_radiology_bill WHERE status = 'paid' 
			AND created_on >= unix_timestamp(CURDATE() - interval 7 day)) AS 'Radiology Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_service_bill WHERE status = 'paid' 
			AND created_on >= unix_timestamp(CURDATE() - interval 7 day)) AS 'Service Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_forensic_bill WHERE status = 'paid'
			AND created_on >= unix_timestamp(CURDATE() - interval 7 day)) AS 'Forensic Bill'";
		}

		else if ($dataLoad =="today") {
			$sql = "SELECT (SELECT IFNULL(SUM(charge),0) from patient_lab_bill WHERE status = 'paid' 
			AND created_on >= unix_timestamp(CURDATE()))AS 'Lab Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_procedure_bill WHERE status = 'paid' 
			AND created_on >= unix_timestamp(CURDATE())) AS 'Procedure Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_radiology_bill WHERE status = 'paid' 
			AND created_on >= unix_timestamp(CURDATE())) AS 'Radiology Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_service_bill WHERE status = 'paid' 
			AND created_on >= unix_timestamp(CURDATE())) AS 'Service Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_forensic_bill WHERE status = 'paid'
			AND created_on >= unix_timestamp(CURDATE())) AS 'Forensic Bill'";
		}

		else  {
			$sql = "SELECT (SELECT IFNULL(SUM(charge),0) from patient_lab_bill WHERE status = 'paid' 
			AND created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59'))AS 'Lab Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_procedure_bill WHERE status = 'paid' 
			AND created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS 'Procedure Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_radiology_bill WHERE status = 'paid' 
			AND created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS 'Radiology Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_service_bill WHERE status = 'paid' 
			AND created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS 'Service Bill',
			(SELECT IFNULL(SUM(charge),0) from patient_forensic_bill WHERE status = 'paid'
			AND created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS 'Forensic Bill'";
		}

		$result=mysqli_query($conn,$sql);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);
	}
?>	