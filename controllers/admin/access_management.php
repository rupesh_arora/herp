<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	$operation = "";
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}

	if ($operation == "getAllScreens") {

		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		$query= "SELECT pages.id as id,pages.screen_name as title, pages.sort_order ,pages.parent_menu_id as parentid,pages.screen_load_url as link FROM pages ORDER BY pages.sort_order";
		$result=mysqli_query($conn,$query);
		$resultRows = array();
		$menu = array(
			'menus' => array(),
			'parent_menus' => array()
		);
		while ($row = mysqli_fetch_assoc($result)) {
			//creates entry into menus array with current menu id ie. $menus['menus'][1]
			$menu['menus'][$row['id']] = $row;
			//creates entry into parent_menus array. parent_menus array contains a list of all menus with children
			$menu['parent_menus'][$row['parentid']][] = $row['id'];
		}

		//print_r($menu);
		$html = buildMenu(0, $menu,0);;
		print $html;
	}
	if ($operation == "getScreenAccess") {

		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}

		$query= "SELECT users.id, staff_type.`type`, users.first_name, users.last_name, users.mobile, users.joining_date,
			designation.`designation`, ( CASE WHEN users.gender='M' THEN 'Male' ELSE 'Female' END) as gender , users.dob, users.address, users.martial_status, users.email_id,
			po.screen_id AS screenId, po.view AS isViewPermission, po.update AS isEditPermission, po.delete AS isDeletePermission
			from users
			LEFT JOIN staff_type ON staff_type.id = users.staff_type_id
			LEFT JOIN designation ON designation.id = users.designation
            LEFT JOIN pages_options po ON users.id = po.user_id
			WHERE users.id= '".$id."'";
			
		$result=mysqli_query($conn,$query);
		if(mysqli_num_rows($result) > 0) {
			$rows = array();
			while($r = mysqli_fetch_assoc($result)) {
			 $rows[] = $r;
			}
			print json_encode($rows);			
		} else {
			print "0";			
		}
	}
	
	if ($operation == "saveScreensAccess") {
		$id;
		$objData;
		$counter = 0;
		
		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		
		if (isset($_POST['objdata'])) {
			$objData = json_decode($_POST["objdata"]);
		}
		
		$result=mysqli_query($conn, "DELETE FROM pages_options WHERE user_id=" .$id  .";");
		
		$query= "INSERT INTO `pages_options`(`screen_id`, `user_id`, `view`, `update`, `delete`) VALUES ";
		$subQuery = '';
		foreach ($objData as $value)
		{
			if($counter == 0) {
				$counter = $counter + 1;
			} else {
				$subQuery = $subQuery .", ";
			}
			
			/*$item = split("_", $value);
			$pageId = $item[0];
			$isViewChecked = $item[1];
			$isEditChecked = $item[2];
			$isDeleteChecked = $item[3];*/
			$pageId = $value->id;			
			$isViewChecked = $value->isViewChecked;
			if($isViewChecked == ""){
				$isViewChecked = 0;
			}
			$isEditChecked = $value->isEditChecked;
			if($isEditChecked == ""){
				$isEditChecked = 0;
			}
			$isDeleteChecked = $value->isDeleteChecked;
			if($isDeleteChecked == ""){
				$isDeleteChecked = 0;
			}
			$subQuery = $subQuery ."(".$pageId .", " .$id .", " .$isViewChecked .", " .$isEditChecked .", " .$isDeleteChecked .")"; 
		}
		$query = $query .$subQuery;
		//echo $query;
		$result=mysqli_query($conn,$query);
		echo $result;		
	}
function buildMenu($parent,$menu,$counter) {
	$html = "";
	
	if (isset($menu['parent_menus'][$parent])) {
		if($counter == 0){
			$html .= "<ul id='sortable'  class='root'>";
			$counter++;
		}
		else{
			$html .= "<ul  id='sortable'>";
		}
		foreach ($menu['parent_menus'][$parent] as $menu_id) {
			if (!isset($menu['parent_menus'][$menu_id])) {
				$html .= "<li class='sidebar-nav-menu' ><a></a><input type='checkbox' data-id=" . $menu['menus'][$menu_id]['id'] . " class='view' data-parentId=" .$menu['menus'][$menu_id]['parentid']." data-order=" .$menu['menus'][$menu_id]['sort_order']." ><span class='menuText'>" . $menu['menus'][$menu_id]['title'] . "</span></li>";
			}
			if (isset($menu['parent_menus'][$menu_id])) {
				$html .= "<li class='sidebar-nav-submenu' ><a></a><input type='checkbox' data-id=" . $menu['menus'][$menu_id]['id'] . " class='view'  data-parentId=" .$menu['menus'][$menu_id]['parentid']." data-order=" .$menu['menus'][$menu_id]['sort_order']."  ><span class='menuText'>" . $menu['menus'][$menu_id]['title'] . "</span>";
				$html .= buildMenu($menu_id, $menu,1);
				$html .= "</li>";
			}
		}
		$html .= "</ul>";
	}
	return $html;
}

/* on form submission */
if(isset($_POST['do_submit']))  {
	/* split the value of the sortation */
	$ids = explode(',',$_POST['sort_order']);
	/* run the update query for each id */
	foreach($ids as $index=>$id) {
		$id = (int) $id;
		if($id != '') {
			$query = 'UPDATE test_table SET sort_order = '.($index + 1).' WHERE id = '.$id;
			$result = mysql_query($query,$connection) or die(mysql_error().': '.$query);
		}
	}
	
	/* now what? */
	if($_POST['byajax']) { die(); } else { $message = 'Sortation has been saved.'; }
}

if($operation == "changeLocation")  {
	
	if (isset($_POST['id'])) {
		$id=$_POST["id"];
	}
	if (isset($_POST['parentId'])) {
		$parentId=$_POST["parentId"];
	} 
	if (isset($_POST['dragIndex'])) {
		$dragIndex=$_POST["dragIndex"];
	} 
	if (isset($_POST['dragParentId'])) {
		$dragParentId=$_POST["dragParentId"];
	} 
	if (isset($_POST['dropIndex'])) {
		$dropIndex=$_POST["dropIndex"];
	} 
	if (isset($_POST['dropParentId'])) {
		$dropParentId=$_POST["dropParentId"];
	} 
	if (isset($_POST['pageId'])) {
		$pageId=$_POST["pageId"];
	} 								
	
	
	if($id != '') {
		$query = "UPDATE pages SET parent_menu_id = '".$parentId."' WHERE id = '".$id."'";
		$result=mysqli_query($conn,$query);

		if($result){
			 $procedure = "CALL sp_update_sorting($pageId, $dragParentId, $dropParentId, $dragIndex, $dropIndex)";			 
			 $result=mysqli_query($conn,$procedure);
			echo $result;
		}
	}
}
	
?>