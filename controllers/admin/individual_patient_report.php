<?php
	
	session_start();
	if(!session_id()){
		exit();
	}
	$toDate = '';
	$fromDate = '';
	
	include_once('config.php');

	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET['operation'])){
		$operation=$_GET["operation"];
	}

	if ($operation=="showChartData") {
		if (isset($_POST['dataLoad'])) {
			$dataLoad = $_POST["dataLoad"];
		}
		if (isset($_POST['fromDate'])) {
			$fromDate = $_POST["fromDate"];
		}
		if (isset($_POST['toDate'])) {
			$toDate = $_POST["toDate"];
		}

		if ($dataLoad =="all") {

			/*Patient lab bill queries*/
			$queryLabBill = "SELECT SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 1 ELSE 0 END) 
			as paid,SUM(CASE WHEN patient_lab_bill.`status` = 'Unpaid' THEN 1 ELSE 0 END) 
			as unpaid,lab_tests.name FROM patient_lab_bill
			LEFT JOIN lab_tests ON lab_tests.id = patient_lab_bill.lab_test_id
			GROUP by lab_tests.id ORDER by paid,unpaid ";

			$sqlLabBill = "SELECT SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 
			patient_lab_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN patient_lab_bill.`status` = 'Unpaid' 
			THEN patient_lab_bill.charge ELSE 0 END) as unpaid FROM patient_lab_bill";


			/*Patient radiology bill queries*/

			$queryRadiologyBill = "SELECT SUM(CASE WHEN patient_radiology_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_radiology_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,radiology_tests.name FROM patient_radiology_bill
			LEFT JOIN radiology_tests ON radiology_tests.id = patient_radiology_bill.radiology_test_id GROUP BY radiology_tests.id ORDER by paid,unpaid ";

			$sqlRadiologyBill = "SELECT SUM(CASE WHEN patient_radiology_bill.`status` = 'paid'
			THEN patient_radiology_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN
			patient_radiology_bill.`status` = 'Unpaid' THEN patient_radiology_bill.charge 
			ELSE 0 END) as unpaid FROM patient_radiology_bill";


			/*Patient service bill */
			$queryServiceBill = "SELECT SUM(CASE WHEN patient_service_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_service_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,services.name FROM patient_service_bill
			LEFT JOIN services ON services.id = patient_service_bill.service_id 
			GROUP BY services.id ORDER by paid,unpaid ";

			$sqlServiceBill = "SELECT SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END) as unpaid FROM patient_service_bill";


			/*patient procedure bill*/

			$queryProcedureBill = "SELECT SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_procedure_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,procedures.name FROM patient_procedure_bill
			LEFT JOIN procedures ON procedures.id = patient_procedure_bill.procedure_id 
			GROUP BY procedures.id ORDER by paid,unpaid ";

			$sqlProcedureBill = "SELECT SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' 
			THEN patient_procedure_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN 
			patient_procedure_bill.`status` = 'Unpaid' THEN patient_procedure_bill.charge 
			ELSE 0 END) as unpaid FROM patient_procedure_bill";

			/*patint forensic bill*/
			$queryForensicBill = "SELECT SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_forensic_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,forensic_tests.name FROM patient_forensic_bill
			LEFT JOIN forensic_tests ON forensic_tests.id = patient_forensic_bill.forensic_test_id 
			GROUP BY forensic_tests.id ORDER by paid,unpaid  ";

			$sqlForensicBill = "SELECT SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' 
			THEN patient_forensic_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN 
			patient_forensic_bill.`status` = 'Unpaid' THEN patient_forensic_bill.charge ELSE 0 END)
			as unpaid FROM patient_forensic_bill";
		}
		
		else if ($dataLoad =="last30days") {

			/*Patient lab bill queries*/
			$queryLabBill = "SELECT SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 1 ELSE 0 END) 
			as paid,SUM(CASE WHEN patient_lab_bill.`status` = 'Unpaid' THEN 1 ELSE 0 END) 
			as unpaid,lab_tests.name FROM patient_lab_bill
			LEFT JOIN lab_tests ON lab_tests.id = patient_lab_bill.lab_test_id
			WHERE patient_lab_bill.created_on >= unix_timestamp(curdate() - interval 1 month)
			GROUP by lab_tests.id ORDER by paid,unpaid ";

			$sqlLabBill = "SELECT SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 
			patient_lab_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN patient_lab_bill.`status` = 'Unpaid' 
			THEN patient_lab_bill.charge ELSE 0 END) as unpaid FROM patient_lab_bill
			WHERE patient_lab_bill.created_on >= unix_timestamp(curdate() - interval 1 month)";


			/*Patient radiology bill queries*/

			$queryRadiologyBill = "SELECT SUM(CASE WHEN patient_radiology_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_radiology_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,radiology_tests.name FROM patient_radiology_bill
			LEFT JOIN radiology_tests ON radiology_tests.id = patient_radiology_bill.radiology_test_id
			WHERE patient_radiology_bill.created_on >= unix_timestamp(curdate() - interval 1 month)
			GROUP BY radiology_tests.id ORDER by paid,unpaid ";

			$sqlRadiologyBill = "SELECT SUM(CASE WHEN patient_radiology_bill.`status` = 'paid'
			THEN patient_radiology_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN
			patient_radiology_bill.`status` = 'Unpaid' THEN patient_radiology_bill.charge 
			ELSE 0 END) as unpaid FROM patient_radiology_bill
			WHERE patient_radiology_bill.created_on >= unix_timestamp(curdate() - interval 1 month)";


			/*Patient service bill */
			$queryServiceBill = "SELECT SUM(CASE WHEN patient_service_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_service_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,services.name FROM patient_service_bill
			LEFT JOIN services ON services.id = patient_service_bill.service_id
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate() - interval 1 month)
			GROUP BY services.id ORDER by paid,unpaid ";

			$sqlServiceBill = "SELECT SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END) as unpaid FROM patient_service_bill
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate() - interval 1 month)";


			/*patient procedure bill*/

			$queryProcedureBill = "SELECT SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_procedure_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,procedures.name FROM patient_procedure_bill
			LEFT JOIN procedures ON procedures.id = patient_procedure_bill.procedure_id
			WHERE patient_procedure_bill.created_on >= unix_timestamp(curdate() - interval 1 month) 
			GROUP BY procedures.id ORDER by paid,unpaid ";

			$sqlProcedureBill = "SELECT SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' 
			THEN patient_procedure_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN 
			patient_procedure_bill.`status` = 'Unpaid' THEN patient_procedure_bill.charge 
			ELSE 0 END) as unpaid FROM patient_procedure_bill
			WHERE patient_procedure_bill.created_on >= unix_timestamp(curdate() - interval 1 month)";

			/*patint forensic bill*/
			$queryForensicBill = "SELECT SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_forensic_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,forensic_tests.name FROM patient_forensic_bill
			LEFT JOIN forensic_tests ON forensic_tests.id = patient_forensic_bill.forensic_test_id
			WHERE patient_forensic_bill.created_on >= unix_timestamp(curdate() - interval 1 month) 
			GROUP BY forensic_tests.id ORDER by paid,unpaid  ";

			$sqlForensicBill = "SELECT SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' 
			THEN patient_forensic_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN 
			patient_forensic_bill.`status` = 'Unpaid' THEN patient_forensic_bill.charge ELSE 0 END)
			as unpaid FROM patient_forensic_bill
			WHERE patient_forensic_bill.created_on >= unix_timestamp(curdate() - interval 1 month)";
		}


		else if ($dataLoad =="last7days") {

			/*Patient lab bill queries*/
			$queryLabBill = "SELECT SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 1 ELSE 0 END) 
			as paid,SUM(CASE WHEN patient_lab_bill.`status` = 'Unpaid' THEN 1 ELSE 0 END) 
			as unpaid,lab_tests.name FROM patient_lab_bill
			LEFT JOIN lab_tests ON lab_tests.id = patient_lab_bill.lab_test_id
			WHERE patient_lab_bill.created_on >= unix_timestamp(curdate() - interval 7 day)
			GROUP by lab_tests.id ORDER by paid,unpaid ";

			$sqlLabBill = "SELECT SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 
			patient_lab_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN patient_lab_bill.`status` = 'Unpaid' 
			THEN patient_lab_bill.charge ELSE 0 END) as unpaid FROM patient_lab_bill
			WHERE patient_lab_bill.created_on >= unix_timestamp(curdate() - interval 7 day)";


			/*Patient radiology bill queries*/

			$queryRadiologyBill = "SELECT SUM(CASE WHEN patient_radiology_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_radiology_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,radiology_tests.name FROM patient_radiology_bill
			LEFT JOIN radiology_tests ON radiology_tests.id = patient_radiology_bill.radiology_test_id
			WHERE patient_radiology_bill.created_on >= unix_timestamp(curdate() - interval 7 day)
			GROUP BY radiology_tests.id ORDER by paid,unpaid ";

			$sqlRadiologyBill = "SELECT SUM(CASE WHEN patient_radiology_bill.`status` = 'paid'
			THEN patient_radiology_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN
			patient_radiology_bill.`status` = 'Unpaid' THEN patient_radiology_bill.charge 
			ELSE 0 END) as unpaid FROM patient_radiology_bill
			WHERE patient_radiology_bill.created_on >= unix_timestamp(curdate() - interval 7 day)";


			/*Patient service bill */
			$queryServiceBill = "SELECT SUM(CASE WHEN patient_service_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_service_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,services.name FROM patient_service_bill
			LEFT JOIN services ON services.id = patient_service_bill.service_id
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate() - interval 7 day)
			GROUP BY services.id ORDER by paid,unpaid ";

			$sqlServiceBill = "SELECT SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END) as paid, SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END) as unpaid FROM patient_service_bill 
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate() - interval 7 day)";


			/*patient procedure bill*/

			$queryProcedureBill = "SELECT SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_procedure_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,procedures.name FROM patient_procedure_bill
			LEFT JOIN procedures ON procedures.id = patient_procedure_bill.procedure_id
			WHERE patient_procedure_bill.created_on >= unix_timestamp(curdate() - interval 7 day) 
			GROUP BY procedures.id ORDER by paid,unpaid ";

			$sqlProcedureBill = "SELECT SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' 
			THEN patient_procedure_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN 
			patient_procedure_bill.`status` = 'Unpaid' THEN patient_procedure_bill.charge 
			ELSE 0 END) as unpaid FROM patient_procedure_bill
			WHERE patient_procedure_bill.created_on >= unix_timestamp(curdate() - interval 7 day)";

			/*patint forensic bill*/
			$queryForensicBill = "SELECT SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_forensic_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,forensic_tests.name FROM patient_forensic_bill
			LEFT JOIN forensic_tests ON forensic_tests.id = patient_forensic_bill.forensic_test_id
			WHERE patient_forensic_bill.created_on >= unix_timestamp(curdate() - interval 7 day) 
			GROUP BY forensic_tests.id ORDER by paid,unpaid  ";

			$sqlForensicBill = "SELECT SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' 
			THEN patient_forensic_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN 
			patient_forensic_bill.`status` = 'Unpaid' THEN patient_forensic_bill.charge ELSE 0 END)
			as unpaid FROM patient_forensic_bill
			WHERE patient_forensic_bill.created_on >= unix_timestamp(curdate() - interval 7 day)";
		}


		else if ($dataLoad =="today") {

			/*Patient lab bill queries*/
			$queryLabBill = "SELECT SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 1 ELSE 0 END) 
			as paid,SUM(CASE WHEN patient_lab_bill.`status` = 'Unpaid' THEN 1 ELSE 0 END) 
			as unpaid,lab_tests.name FROM patient_lab_bill
			LEFT JOIN lab_tests ON lab_tests.id = patient_lab_bill.lab_test_id
			WHERE patient_lab_bill.created_on >= unix_timestamp(curdate())
			GROUP by lab_tests.id ORDER by paid,unpaid ";

			$sqlLabBill = "SELECT SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 
			patient_lab_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN patient_lab_bill.`status` = 'Unpaid' 
			THEN patient_lab_bill.charge ELSE 0 END) as unpaid FROM patient_lab_bill
			WHERE patient_lab_bill.created_on >= unix_timestamp(curdate())";


			/*Patient radiology bill queries*/

			$queryRadiologyBill = "SELECT SUM(CASE WHEN patient_radiology_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_radiology_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,radiology_tests.name FROM patient_radiology_bill
			LEFT JOIN radiology_tests ON radiology_tests.id = patient_radiology_bill.radiology_test_id
			WHERE patient_radiology_bill.created_on >= unix_timestamp(curdate())
			GROUP BY radiology_tests.id ORDER by paid,unpaid ";

			$sqlRadiologyBill = "SELECT SUM(CASE WHEN patient_radiology_bill.`status` = 'paid'
			THEN patient_radiology_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN
			patient_radiology_bill.`status` = 'Unpaid' THEN patient_radiology_bill.charge 
			ELSE 0 END) as unpaid FROM patient_radiology_bill
			WHERE patient_radiology_bill.created_on >= unix_timestamp(curdate())";


			/*Patient service bill */
			$queryServiceBill = "SELECT SUM(CASE WHEN patient_service_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_service_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,services.name FROM patient_service_bill
			LEFT JOIN services ON services.id = patient_service_bill.service_id
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate())
			GROUP BY services.id ORDER by paid,unpaid ";

			$sqlServiceBill = "SELECT SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END) as unpaid FROM patient_service_bill
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate())";


			/*patient procedure bill*/

			$queryProcedureBill = "SELECT SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_procedure_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,procedures.name FROM patient_procedure_bill
			LEFT JOIN procedures ON procedures.id = patient_procedure_bill.procedure_id
			WHERE patient_procedure_bill.created_on >= unix_timestamp(curdate()) 
			GROUP BY procedures.id ORDER by paid,unpaid ";

			$sqlProcedureBill = "SELECT SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' 
			THEN patient_procedure_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN 
			patient_procedure_bill.`status` = 'Unpaid' THEN patient_procedure_bill.charge 
			ELSE 0 END) as unpaid FROM patient_procedure_bill
			WHERE patient_procedure_bill.created_on >= unix_timestamp(curdate())";

			/*patint forensic bill*/
			$queryForensicBill = "SELECT SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_forensic_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,forensic_tests.name FROM patient_forensic_bill
			LEFT JOIN forensic_tests ON forensic_tests.id = patient_forensic_bill.forensic_test_id
			WHERE patient_forensic_bill.created_on >= unix_timestamp(curdate()) 
			GROUP BY forensic_tests.id ORDER by paid,unpaid  ";

			$sqlForensicBill = "SELECT SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' 
			THEN patient_forensic_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN 
			patient_forensic_bill.`status` = 'Unpaid' THEN patient_forensic_bill.charge ELSE 0 END)
			as unpaid FROM patient_forensic_bill
			WHERE patient_forensic_bill.created_on >= unix_timestamp(curdate())";
		}



		else  {

			/*Patient lab bill queries*/
			$queryLabBill = "SELECT SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 1 ELSE 0 END) 
			as paid,SUM(CASE WHEN patient_lab_bill.`status` = 'Unpaid' THEN 1 ELSE 0 END) 
			as unpaid,lab_tests.name FROM patient_lab_bill
			LEFT JOIN lab_tests ON lab_tests.id = patient_lab_bill.lab_test_id
			WHERE patient_lab_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59')
			GROUP by lab_tests.id ORDER by paid,unpaid ";

			$sqlLabBill = "SELECT SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 
			patient_lab_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN patient_lab_bill.`status` = 'Unpaid' 
			THEN patient_lab_bill.charge ELSE 0 END) as unpaid FROM patient_lab_bill
			WHERE patient_lab_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59')";


			/*Patient radiology bill queries*/

			$queryRadiologyBill = "SELECT SUM(CASE WHEN patient_radiology_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_radiology_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,radiology_tests.name FROM patient_radiology_bill
			LEFT JOIN radiology_tests ON radiology_tests.id = patient_radiology_bill.radiology_test_id
			WHERE patient_radiology_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59')
			GROUP BY radiology_tests.id ORDER by paid,unpaid ";

			$sqlRadiologyBill = "SELECT SUM(CASE WHEN patient_radiology_bill.`status` = 'paid'
			THEN patient_radiology_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN
			patient_radiology_bill.`status` = 'Unpaid' THEN patient_radiology_bill.charge 
			ELSE 0 END) as unpaid FROM patient_radiology_bill
			WHERE patient_radiology_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59')";


			/*Patient service bill */
			$queryServiceBill = "SELECT SUM(CASE WHEN patient_service_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_service_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,services.name FROM patient_service_bill
			LEFT JOIN services ON services.id = patient_service_bill.service_id
			WHERE patient_service_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59')
			GROUP BY services.id ORDER by paid,unpaid ";

			$sqlServiceBill = "SELECT SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END) as unpaid FROM patient_service_bill
			WHERE patient_service_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59')";


			/*patient procedure bill*/

			$queryProcedureBill = "SELECT SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_procedure_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,procedures.name FROM patient_procedure_bill
			LEFT JOIN procedures ON procedures.id = patient_procedure_bill.procedure_id
			WHERE patient_procedure_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59') 
			GROUP BY procedures.id ORDER by paid,unpaid ";

			$sqlProcedureBill = "SELECT SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' 
			THEN patient_procedure_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN 
			patient_procedure_bill.`status` = 'Unpaid' THEN patient_procedure_bill.charge 
			ELSE 0 END) as unpaid FROM patient_procedure_bill
			WHERE patient_procedure_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59')";

			/*patint forensic bill*/
			$queryForensicBill = "SELECT SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' THEN 1 
			ELSE 0 END) as paid,SUM(CASE WHEN patient_forensic_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END) as unpaid,forensic_tests.name FROM patient_forensic_bill
			LEFT JOIN forensic_tests ON forensic_tests.id = patient_forensic_bill.forensic_test_id
			WHERE patient_forensic_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59') 
			GROUP BY forensic_tests.id ORDER by paid,unpaid  ";

			$sqlForensicBill = "SELECT SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' 
			THEN patient_forensic_bill.charge ELSE 0 END) as paid,SUM(CASE WHEN 
			patient_forensic_bill.`status` = 'Unpaid' THEN patient_forensic_bill.charge ELSE 0 END)
			as unpaid FROM patient_forensic_bill
			WHERE patient_forensic_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59')";
		}


		$Allrows   = array();

		/*Connection for patient lab bill query*/
		$queryResultLabBill  = mysqli_query($conn,$queryLabBill);	
		$rows   = array();
	    while ($r = mysqli_fetch_assoc($queryResultLabBill)) {
	        $rowsQueryLabBill[] = $r;
	    }

	    $sqlResultLabBill  = mysqli_query($conn,$sqlLabBill);		
		$rows   = array();
	    while ($r = mysqli_fetch_assoc($sqlResultLabBill)) {
	        $rowsSqlLabBill[] = $r;
	    }

	    /*Connection for patient radiology bill query*/
		$queryResultRadiologyBill  = mysqli_query($conn,$queryRadiologyBill);	
		$rows   = array();
	    while ($r = mysqli_fetch_assoc($queryResultRadiologyBill)) {
	        $rowsQueryRadiologyBill[] = $r;
	    }

	    $sqlResultRadiologyBill  = mysqli_query($conn,$sqlRadiologyBill);		
		$rows   = array();
	    while ($r = mysqli_fetch_assoc($sqlResultRadiologyBill)) {
	        $rowsSqlRadiologyBill[] = $r;
	    }

	    /*Connection for service radiology bill query*/
		$queryResultServiceBill  = mysqli_query($conn,$queryServiceBill);	
		$rows   = array();
	    while ($r = mysqli_fetch_assoc($queryResultServiceBill)) {
	        $rowsQueryServiceBill[] = $r;
	    }

	    $sqlResultServiceBill  = mysqli_query($conn,$sqlServiceBill);		
		$rows   = array();
	    while ($r = mysqli_fetch_assoc($sqlResultServiceBill)) {
	        $rowsSqlServiceBill[] = $r;
	    }

	    /*Connection for patient procedure bill query*/
		$queryResultProcedureBill  = mysqli_query($conn,$queryProcedureBill);	
		$rows   = array();
	    while ($r = mysqli_fetch_assoc($queryResultProcedureBill)) {
	        $rowsQueryProcedureBill[] = $r;
	    }

	    $sqlResultProcedureBill  = mysqli_query($conn,$sqlProcedureBill);		
		$rows   = array();
	    while ($r = mysqli_fetch_assoc($sqlResultProcedureBill)) {
	        $rowsSqlProcedureBill[] = $r;
	    }


	    /*Connection for patient forensic bill query*/
		$queryResultForensicBill  = mysqli_query($conn,$queryForensicBill);	
		$rows   = array();
	    while ($r = mysqli_fetch_assoc($queryResultForensicBill)) {
	        $rowsQueryForensicBill[] = $r;
	    }

	    $sqlResultForensicBill  = mysqli_query($conn,$sqlForensicBill);		
		$rows   = array();
	    while ($r = mysqli_fetch_assoc($sqlResultForensicBill)) {
	        $rowsSqlForensicBill[] = $r;
	    }


	    array_push($Allrows, $rowsQueryLabBill);
	    array_push($Allrows, $rowsSqlLabBill);
	    array_push($Allrows, $rowsQueryRadiologyBill);
	    array_push($Allrows, $rowsSqlRadiologyBill);
	    array_push($Allrows, $rowsQueryServiceBill);
	    array_push($Allrows, $rowsSqlServiceBill);
	    array_push($Allrows, $rowsQueryProcedureBill);
	    array_push($Allrows, $rowsSqlProcedureBill);
	    array_push($Allrows, $rowsQueryForensicBill);
	    array_push($Allrows, $rowsSqlForensicBill);


	    echo json_encode($Allrows);
	}
?>