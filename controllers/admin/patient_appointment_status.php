<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	
	include 'config.php';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}	
	
	if($operation == "showPatientAppointmentStatus"){
		
		$query= "Select appointments.*,Concat(appointments.start_date_time,' - ',appointments.end_date_time) As time_duration,
				Concat(patients.salutation,' ',patients.first_name,' ',patients.last_name) As patient_name,
				Concat(users.salutation,' ',users.first_name,' ',users.last_name) AS consultant_name,
				(SELECT value FROM configuration WHERE name = 'patient_prefix') AS patient_prefix from appointments 
				LEFT JOIN patients ON patients.id = appointments.patient_id
				LEFT JOIN users ON users.id = appointments.consultant_id 
				where appointments.status = 'Hold On'";		
		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
?>