<?php

/*File Name  :   lcation_screen.php
Company Name :   Qexon INfotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page manges city , state and country*/

/*INclude config file*/
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
INclude 'config.php';

$operation = "";
$stateID   = "";
$countryID = "";

/*checkINg operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
} //nothINg	

/*operation to show country*/
if ($operation == "showcountry") {
    $query = "SELECT location_id,name FROM locations WHERE location_type = 0 ORDER BY name";    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    prINt json_encode($rows);//JSON encode
}

/*operation to show state*/
if ($operation == "showstate") {
    $value = $_POST['value'];
    $query = "SELECT location_id,name FROM locations WHERE location_type = 1 and parent_id = '$value' ORDER BY name";    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    prINt json_encode($rows);
}

/*operation to show city*/
if ($operation == "showcity") {
    $value = $_POST['value'];
    $query = "SELECT location_id,name FROM locations WHERE location_type = 2 and parent_id = '$value' ORDER BY name";    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    prINt json_encode($rows);
}

/*operation to update country*/
if ($operation == "updateCountry") {
    if (isset($_POST['newCountry'])) {
        $newCountry = $_POST['newCountry'];
    }
    if (isset($_POST['getCountryId'])) {
        $getCountryId = $_POST['getCountryId'];
    }
     
    $sql    = "UPDATE locations SET name = '" . $newCountry . "' WHERE location_id = '" . $getCountryId . "' ";
    $result = mysqli_query($conn, $sql);
    echo $result;
    
}

/*operation to update state*/
if ($operation == "updateState") {
    if (isset($_POST['newState'])) {
        $newState = $_POST['newState'];
    }
    if (isset($_POST['getStateId'])) {
        $getStateId = $_POST['getStateId'];
    }
    if (isset($_POST['getCountryId'])) {
        $getCountryId = $_POST['getCountryId'];
    }
    
   
    $sql    = "UPDATE locations SET name = '" . $newState . "' WHERE location_id = '" . $getStateId . "' ";
    $result = mysqli_query($conn, $sql);
    echo $result;
    
}

/*operation to update city*/
if ($operation == "updateCity") {
    if (isset($_POST['newCity'])) {
        $newCity = $_POST['newCity'];
    }
    if (isset($_POST['getCountryId'])) {
        $getCountryId = $_POST['getCountryId'];
    }
    if (isset($_POST['getStateId'])) {
        $getStateId = $_POST['getStateId'];
    }
    if (isset($_POST['getCityId'])) {
        $getCityId = $_POST['getCityId'];
    }
    
  
    $sql    = "UPDATE locations SET name = '" . $newCity . "' WHERE location_id = '" . $getCityId . "' ";
    $result = mysqli_query($conn, $sql);
    echo $result;

}

/*operation to save country*/
if ($operation == "saveCountry") {
    if (isset($_POST['country'])) {
        $country = $_POST['country'];
    }
    
    $query_select  = "SELECT name FROM locations WHERE name = '" . $country . "'";
    $result_select = mysqli_query($conn, $query_select);
    $rows_count    = mysqli_num_rows($result_select);
    if ($rows_count >= 1) {
        echo "0";
    } else {
        $sql    = "INSERT INTO  locations (name, location_type,  is_visible, country_code) 
        VALUES ('" . $country . "' ,'0','0', '0') ";
        $result = mysqli_query($conn, $sql);
        echo $result;
    }
}

/*operation to save state*/
if ($operation == "saveState") {
    if (isset($_POST['state'])) {
        $state = $_POST['state'];
    }
    if (isset($_POST['getCountryId'])) {
        $getCountryId = $_POST['getCountryId'];
    }
    
    $query_select  = "SELECT name FROM locations WHERE name = '" . $state . "' and parent_id='" . $getCountryId . "'";
    $result_select = mysqli_query($conn, $query_select);
    $rows_count    = mysqli_num_rows($result_select);
    if ($rows_count >= 1) {
        echo "0";
    } else {
        $sql    = "INSERT INTO  locations (name, location_type, parent_id, is_visible, country_code) 
        VALUES ('" . $state . "' ,'1','" . $getCountryId . "', '0', '0') ";
        $result = mysqli_query($conn, $sql);
        echo $result;
    }
}

/*operation to save city*/
if ($operation == "saveCity") {
    if (isset($_POST['city'])) {
        $city = $_POST['city'];
    }
    if (isset($_POST['getCountryId'])) {
        $getCountryId = $_POST['getCountryId'];
    }
    if (isset($_POST['getStateId'])) {
        $getStateId = $_POST['getStateId'];
    }
    if (isset($_POST['getCityId'])) {
        $getCityId = $_POST['getCityId'];
    }
    
    $query_select  = "SELECT location_id, name FROM locations WHERE parent_id IN(SELECT location_id from locations WHERE parent_id = '" . $getCountryId . "') and name='" . $newCity . "'";
    $result_select = mysqli_query($conn, $query_select);
    $rows_count    = mysqli_num_rows($result_select);
    if ($rows_count >= 1) {
        echo "0";
    } else {
        $sql    = "INSERT INTO  locations (name, location_type, parent_id, is_visible, country_code) 
        VALUES ('" . $city . "' ,'2','" . $getStateId . "', '0', '0') ";
        $result = mysqli_query($conn, $sql);
        echo $result;
    }
}

/*operation to delete locations*/
if ($operation == "deleteLocation") {
    if (isset($_POST['delCountryId'])) {
        $delCountryId = $_POST['delCountryId'];

        $sqlDelete = "DELETE FROM locations WHERE location_id ='".$delCountryId."'";
    }
    if (isset($_POST['delStateId'])) {
        $delStateId = $_POST['delStateId'];

        $sqlDelete = "DELETE from locations WHERE location_id ='".$delStateId."'";
    }   
    if (isset($_POST['delCityId'])) {
        $delCityId = $_POST['delCityId'];

        $sqlDelete = "DELETE from locations WHERE location_id = '".$delCityId."' ";
    } 
    
    $result = mysqli_query($conn, $sqlDelete);
    echo $sqlDelete;   
}
?>