<?php
/*File Name  :   lab_test_request.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page manages  all the lab test request*/

session_start();

/*include config file*/
include 'config.php';

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
}
/*Get user id from session table*/
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
/*operation to save data*/
if ($operation == "save") {
    
    $Price      = json_decode($_POST['Price']); //decode array data
    $bigPrice_l      = json_decode($_POST['bigPrice_l']); //decode array data
    $patientId = $_POST['patientId'];
    $visitId   = $_POST['visitId'];
    $tblName   = $_POST['tblName'];
    $fkColName = $_POST['fkColName'];
    $totalCurrentReqBill = $_POST['totalCurrentReqBill'];
    $copayValue = $_POST['copayValue'];
    $copayType = $_POST['copayType'];
    $insertBill='';  
    $joinQry = '';
    $lastPatientLabBillId = '';
    $componentIdArr = array();
    $lastInsertedId = array();  
   
    
    foreach ($Price as $value) {

        foreach ($value as $values) {

            $labId    = $values->labId;
            $price    = $values->total;
            $restrict    = $values->restrictBill;
            $reqType    = $values->requestType;

            $query  = "SELECT ledger_id,gl_account_id,account_payable_id FROM lab_tests WHERE id = " . $labId . "";
            $result = mysqli_query($conn, $query);
            $ledgerId;
            $glAccountId;
            $accountPayable;
            while ($r = mysqli_fetch_assoc($result)) {
                $ledgerId = $r['ledger_id'];
                $glAccountId = $r['gl_account_id'];
                $accountPayable = $r['account_payable_id'];
            }
        
            if (($restrict == "notRestrictBill") && ($reqType == "internal")){

                $insert_lab_test_bill = "INSERT INTO patient_lab_bill (ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,
                lab_test_id,charge,status,created_by,updated_by,created_on,updated_on)
                VALUES(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . ",'".$patientId."','".$visitId."','".$labId."','".$price."','unpaid','".$userId."',
                '".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP())"; 

                $resultbill = mysqli_query($conn, $insert_lab_test_bill);
                $lastPatientLabBillId = mysqli_insert_id($conn);


                $sql = "INSERT INTO lab_test_requests(ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,lab_test_id
                ,request_date,pay_status,test_status,cost,status,request_type,created_on,created_by)
                 VALUES(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . "," . $patientId . "," . $visitId . "," . $labId . ",
                UNIX_TIMESTAMP(),'Unpaid','Pending'," . $price . ",'A','".$reqType."',UNIX_TIMESTAMP(),'".$userId."')";
            }

            else if (($restrict == "notRestrictBill") && ($reqType == "external")) {

               $sql = "INSERT INTO lab_test_requests(ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,lab_test_id
                ,request_date,pay_status,test_status,cost,status,request_type,created_on,created_by)
                 VALUES(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . "," . $patientId . "," . $visitId . "," . $labId . ",
                UNIX_TIMESTAMP(),'Unpaid','Pending'," . $price . ",'A','".$reqType."',UNIX_TIMESTAMP(),'".$userId."')";
            }

            else {
                $sql = "INSERT INTO lab_test_requests(ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,lab_test_id
                ,request_date,pay_status,test_status,cost,status,request_type,created_on,created_by)
                 VALUES(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . "," . $patientId . "," . $visitId . "," . $labId . ",
                UNIX_TIMESTAMP(),'Paid','Pending'," . $price . ",'A','".$reqType."',UNIX_TIMESTAMP(),'".$userId."')"; 
            } 
            
            $result = mysqli_query($conn, $sql);//run request query
            $labTestId = mysqli_insert_id($conn);

            foreach ($bigPrice_l as $val) {
                
                foreach ($val as $vals) {

                    $id    = $vals->labId;
                    $componentId    = $vals->componentId;
                    $paymentMethod = $vals->savePaymentMethod;
                    $componentCost = $vals->componentCost;
                    if (strtolower($paymentMethod) == "insurance") {

                        if (strtolower($copayType) == "none") {
                            $billStatus = 'paid';           
                        }
                        else {
                            if ($copayValue == 0) {
                                $billStatus = 'paid';
                            }
                            else{
                                $billStatus = 'unpaid';
                            }
                        }
                    }
                    else{
                        $billStatus = 'unpaid';
                    }

                    if($id == $labId){
                        
                       $sqlComponent = "INSERT INTO lab_test_request_component(lab_test_request_id,
                           lab_component_id,payment_mode,cost,created_on,created_by,status,
                           patient_lab_bill_id,pay_status) VALUES('".$labTestId."','".$componentId."',
                           '".$paymentMethod."','".$componentCost."',UNIX_TIMESTAMP(),'".$userId."','A',
                           '".$lastPatientLabBillId."','".$billStatus."')";

                        if ($lastPatientLabBillId == '') {//if the request is internal then no bill generate

                                $sqlComponent = "INSERT INTO lab_test_request_component(
                                lab_test_request_id,lab_component_id,payment_mode,cost,created_on,
                                created_by,status,patient_lab_bill_id,pay_status) VALUES('".$labTestId."',
                                '".$componentId."','".$paymentMethod."','".$componentCost."',
                                UNIX_TIMESTAMP(),'".$userId."','A',NULL,'".$billStatus."')";
                        }

                        $resultcomponent  = mysqli_query($conn, $sqlComponent);

                        //update the bill status of lab test request and patient lab bill
                        if (strtolower($paymentMethod) == "insurance") {

                            if (strtolower($copayType) == "none") {
                                $billStatus = 'paid';           
                            }
                            else {
                                if ($copayValue == 0) {
                                    $billStatus = 'paid';
                                }
                                else{
                                    $billStatus = 'unpaid';
                                }
                            }

                            $updateLabTestReq = "UPDATE lab_test_requests SET pay_status = 
                                '".$billStatus."' WHERE id = '".$labTestId."'";

                            $resultUpdateLabTestReq = mysqli_query($conn,$updateLabTestReq);

                            $updatePatientLabBill = "UPDATE patient_lab_bill SET status = '".$billStatus."' WHERE id = '".$lastPatientLabBillId."' ";

                            $resultUpdatePatientLabBill = mysqli_query($conn,$updatePatientLabBill);
                        }                      
                    }
                }
            }
        }
        echo  $resultcomponent;
    }    
}

/*operation to count total no. of request for old test*/
if ($operation == "search") {
    $patientId = $_POST['patientId'];
    $visitId   = $_POST['visitId'];
    $tblName   = $_POST['tblName'];
    
    $searchQuery  = "SELECT COUNT(*) as total FROM " . $tblName . " WHERE patient_id = " . $patientId . " AND visit_id = " . $visitId;
    $searchResult = mysqli_query($conn, $searchQuery);
    $data         = mysqli_fetch_assoc($searchResult);
    echo $data['total'];
}

/*operation to show only those labtest for which labtest has sent*/
if ($operation == "showLabTest") {
    
    if (isset($_POST['value'])) {
        $value = $_POST['value'];
    }
    $visitId = $_POST['visitId'];
    
    $query  = "SELECT lab_tests.id,lab_tests.name FROM lab_tests WHERE lab_tests.status ='A' 
    AND lab_tests.lab_type_id = '" . $value . "' ORDER BY lab_tests.name";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "showLabTestComponent") {
    if (isset($_POST['labTestId'])) {
        $labTestId = $_POST['labTestId'];
    }
    $query  = "SELECT lab_test_component.id,lab_test_component.name,lab_test_component.fee
    ,lab_test_component.lab_test_id  FROM lab_test_component WHERE 
    lab_test_component.lab_test_id = '".$labTestId."'";

    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "payment_mode") {

    $visitId = $_POST['visitId'];
    $selectPaymentMode = "SELECT payment_mode,insurance_no FROM visits WHERE id = '".$visitId."'";
    $resultPaymentMode = mysqli_query($conn, $selectPaymentMode);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($resultPaymentMode)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation =="insuranceDetail") {

   $visitId = $_POST['visitId'];
    $selectInsuranceDetail = "SELECT insurance.insurance_company_id,insurance.insurance_plan_id,
        insurance.insurance_plan_name_id FROM insurance
        LEFT JOIN visits ON visits.insurance_no = insurance.insurance_number
        WHERE visits.id = '".$visitId."'";
   $resultInsuranceDetail = mysqli_query($conn, $selectInsuranceDetail);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($resultInsuranceDetail)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "revisedCostDetail") {
    $insuranceCompanyId = $_POST['insuranceCompanyId'];
    $insurancePlanId = $_POST['insurancePlanId'];
    $insurancePlanNameid = $_POST['insurancePlanNameid'];
    $componentId = $_POST['componentId'];
    $activeTab = $_POST['activeTab'];

    $selectRevisedCostDetail = "SELECT revised_cost from insurance_revised_cost
        WHERE insurance_revised_cost.insurance_company_id = '".$insuranceCompanyId."' AND
        insurance_revised_cost.scheme_name_id = '".$insurancePlanNameid."' AND
        insurance_revised_cost.scheme_plan_id = '".$insurancePlanId."' AND
        insurance_revised_cost.service_type = '".$activeTab."' AND
        item_id = '".$componentId."'";

    $resultRevisedCostDetail = mysqli_query($conn, $selectRevisedCostDetail);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($resultRevisedCostDetail)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "getCopayValue") {
    $insuranceCompanyId = $_POST['insuranceCompanyId'];
    $insurancePlanId = $_POST['insurancePlanId'];
    $insurancePlanNameid = $_POST['insurancePlanNameid'];

     $selectCopayDetails = "SELECT scheme_plans.value,copay.copay_type FROM scheme_plans
        LEFT JOIN copay ON copay.id = scheme_plans.copay
        WHERE scheme_plans.insurance_company_id = '".$insuranceCompanyId."' AND
        scheme_plans.insurance_plan_id = '".$insurancePlanId."' AND 
        scheme_plans.id = '".$insurancePlanNameid."'";

    $resultCopayDetails = mysqli_query($conn, $selectCopayDetails);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($resultCopayDetails)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

?>