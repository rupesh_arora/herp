<?php
/*
 * File Name    :   vision_assissment.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for save details
 */

$operation = "";
$createOn  = new DateTime();
$createdBy = "";
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $createdBy = $_SESSION['globaluser'];
}
else{
	exit();
}
include 'config.php'; // import database connection file
$operation = $_POST["operation"]; // operation come from js file

// code to show data to text on click of visit button
if ($operation == "selectVisitInfo") {
    $getVisit = $_POST['getVisit'];	
	$visitPrefix = $_POST['visitPrefix'];
	$sql       = "SELECT value FROM configuration WHERE name = 'visit_prefix'";
	$result    = mysqli_query($conn, $sql);
	
	while ($r = mysqli_fetch_assoc($result)) {
		$prefix = $r['value'];
	}
	if($prefix == $visitPrefix){
		$query    = "select sum(cash_account.credit - cash_account.debit) as amount from cash_account 
			Left Join visits on cash_account.patient_id = visits.patient_id where visits.id = '" . $getVisit . "'";
		$result   = mysqli_query($conn, $query);
		while ($row = mysqli_fetch_array($result)) {
			if ($row['0'] >= "0") {
				$sqlSelect = "select visits.patient_id,patients.first_name,patients.last_name,patients.dob,patients.gender,
								patients.mobile,patients.created_on as join_date,patients.images_path ,visits.created_on as visit_date,
								(SELECT value FROM configuration WHERE name = 'patient_prefix') AS patient_prefix from patients
								Left Join visits on patients.id = visits.patient_id  where visits.id='" . $getVisit . "'";
				
				$resultSelect = mysqli_query($conn, $sqlSelect);
				$rows_count   = mysqli_num_rows($resultSelect);
				if ($rows_count > 0) {
					$rows = array();
					while ($r = mysqli_fetch_assoc($resultSelect)) {
						$rows[] = $r;
					}
					print json_encode($rows);
				} else {
					echo "0";
				}
			} else {
				echo "0";
			}
		}
	}
	else{
		echo "2";
	}
}
// for save  vision data
if ($operation == "saveVisionInfo") { 
    $VisitID              = $_POST["VisitID"];
    $patientId            = $_POST["patientId"];
    $EyeStatus            = $_POST["EyeStatus"];
    $VisualAcuityRight    = $_POST["VisualAcuityRight"];
    $VisualAcuityLeft     = $_POST["VisualAcuityLeft"];
    $VisualAcuityExtRight = $_POST["VisualAcuityExtRight"];
    $VisualAcuityExtLeft  = $_POST["VisualAcuityExtLeft"];
    $HandMovementRight    = $_POST["HandMovementRight"];
    $HandMovementLeft     = $_POST["HandMovementLeft"];
    $PreceptionRight      = $_POST["PreceptionRight"];
    $PreceptionLeft       = $_POST["PreceptionLeft"];
    $Comment              = $_POST["Comment"];
    $Notes                = $_POST["Notes"];
    
    $sqlSelect = "SELECT visit_id, patient_id from vision_assissment WHERE visit_id = '".$VisitID."' AND patient_id = '".$patientId."' "; 
    $result     = mysqli_query($conn, $sqlSelect);
    $rows_count = mysqli_num_rows($result);
    if ($rows_count < 1) {
		$sqlUpdateVisit = "update visits set triage_status = 'Taken' where id = '".$VisitID ."'";
		mysqli_query($conn,$sqlUpdateVisit);
      
		$sql    = "Insert into `vision_assissment` (visit_id,patient_id,eye_status,visualacuityright,visualacuityleft,
        visualacuityextright,visualacuityextleft,handmovementright,handmovementleft,preceptionright,preceptionleft,
        Comment,notes,created_on,created_by) values('" . $VisitID . "','" . $patientId . "','" . $EyeStatus . "',
        '" . $VisualAcuityRight . "','" . $VisualAcuityLeft . "','" . $VisualAcuityExtRight . "','" . $VisualAcuityExtLeft . "',
        '" . $HandMovementRight . "','" . $HandMovementLeft . "','" . $PreceptionRight . "','" . $PreceptionLeft . "',
        '" . $Comment . "','" . $Notes . "','" . $createOn->getTimestamp() . "','" . $createdBy . "')";

        $result = mysqli_query($conn, $sql);
        
        if ($result) {
            echo "1";
        } else {
            echo "0";
        }
    }
    else{
		$sqlUpdateVisit = "update visits set triage_status = 'Taken' where id = '".$VisitID ."'";
		mysqli_query($conn,$sqlUpdateVisit);
		
        $sqlUpdate = "UPDATE vision_assissment SET visit_id = '".$VisitID."',patient_id = '".$patientId."',
        eye_status = '".$EyeStatus."',visualacuityright = '".$VisualAcuityRight."',
        visualacuityleft = '".$VisualAcuityLeft."',visualacuityextright = '".$VisualAcuityExtRight."',
        visualacuityextleft = '".$VisualAcuityExtLeft."',handmovementright = '".$HandMovementRight."',
        handmovementleft = '".$HandMovementLeft."', preceptionright = '".$PreceptionRight."',
        preceptionleft = '".$PreceptionLeft."',Comment = '".$Comment."',notes = '".$Notes."',
        created_on = '".$createOn->getTimestamp()."',created_by = '".$createdBy."' WHERE visit_id = '".$VisitID."' 
        AND patient_id = '".$patientId."' ";

        $result = mysqli_query($conn, $sqlUpdate);
        if ($result) {
            echo "1";
        } 
        else {
            echo "0";
        }
    }    
}
if ($operation == "getData") {
	$visitId = $_POST['visitId'];
	$visitPrefix = $_POST['visitPrefix'];
	$sql       = "SELECT value FROM configuration WHERE name = 'visit_prefix'";
	$result    = mysqli_query($conn, $sql);
	
	while ($r = mysqli_fetch_assoc($result)) {
		$prefix = $r['value'];
	}
	if($prefix == $visitPrefix){
		$sqlSelect = "SELECT eye_status, visualacuityright, visualacuityleft, visualacuityextright, visualacuityextleft,
			handmovementright, handmovementleft, preceptionright, preceptionleft, `Comment`, notes from vision_assissment
			WHERE visit_id = '".$visitId."'";
		
		$resultSelect = mysqli_query($conn, $sqlSelect);
		$rows_count   = mysqli_num_rows($resultSelect);
		if ($rows_count > 0) {
			$rows = array();
			while ($r = mysqli_fetch_assoc($resultSelect)) {
				$rows[] = $r;
			}
			print json_encode($rows);
		} else {
			echo "0";
		}
	}
	else{
		echo "0";
	}
}
?>