<?php	

	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	$operation        = "";
	$date             = new DateTime();
	
	include 'config.php';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}	
	
	// search patient opearion
	if ($operation == "search") {
	    
	    if (isset($_POST['firstName'])) {
	        $firstName = $_POST["firstName"];
	    }
	    if (isset($_POST['lastName'])) {
	        $lastName = $_POST["lastName"];
	    }
	    if (isset($_POST['patientId'])) {
	        $patientId = $_POST["patientId"];
	    }
	    if (isset($_POST['appointmentId'])) {
	        $appointmentId = $_POST["appointmentId"];
	    }
		if (isset($_POST['fromDate'])) {
			$fromDate=$_POST["fromDate"];
		}
		if (isset($_POST['toDate'])) {
			$toDate=$_POST["toDate"];
		}			
       
    
        $query = "SELECT p.id,CONCAT(p.first_name,' ',p.middle_name,' ',p.last_name) AS name,a.visit_type_id,
			CONCAT(u.first_name,' ',u.last_name) AS cunsultant_name,a.start_date_time,a.end_date_time,a.id AS appointment_id, 
            (select value from configuration where name = 'patient_prefix') as prefix FROM appointments AS a 
            LEFT JOIN patients AS p ON p.id = a.patient_id
            LEFT JOIN users AS u ON u.id = a.consultant_id  WHERE a.status = 'Booked' ";
        
        if ($firstName != '' || $lastName != '' || $patientId != '' || $appointmentId != '' || $fromDate != '' || $toDate != '') {
        }
        else{

        }
        if ($firstName != '') {

            $query .= " AND p.first_name LIKE '%" . $firstName . "%'";

        }
        if ($lastName != '') {

            $query .= " AND p.last_name LIKE '%" . $lastName . "%'";

        }
		if($patientId !=''){

			$patientPrefix = $_POST['patientPrefix'];
			$sql       = "SELECT value FROM configuration WHERE name = 'patient_prefix'";
			$result    = mysqli_query($conn, $sql);
			
			while ($r = mysqli_fetch_assoc($result)) {
				$prefix = $r['value'];
			}
			
			if($prefix == $patientPrefix){

				$query .= " AND a.patient_id = '".$patientId."' ";

			}
		}
        if ($appointmentId != '') {

            $query .= " AND a.id ='" . $appointmentId . "'";
            $isFirst = "true";
        }
		if($fromDate !='' AND $toDate !=''){

			$query .= " AND a.start_date_time BETWEEN '".$fromDate." 00:00:00' 
						AND '".$toDate." 23:59:59' AND a.end_date_time BETWEEN '".$fromDate." 00:00:00' 
						AND '".$toDate." 23:59:59' ";

		}
		if($fromDate !='' AND $toDate ==''){

			$query .= " AND a.start_date_time BETWEEN '".$fromDate." 00:00:00'
						AND CURRENT_TIMESTAMP() ";

		}
		if($fromDate =='' AND $toDate !=''){

			$query .= " AND a.end_date_time BETWEEN '0000000000' AND 
						'".$toDate." 23:59:59' ";

		}
	    

	    $result = mysqli_query($conn, $query);
	    //$totalrecords = mysqli_num_rows($result);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
	    print json_encode($rows);
	}

	if ($operation == "save") {
	    
	    if (isset($_POST['id'])) {
	        $patientId = $_POST["id"];
	    }
	    if (isset($_POST['appointmentId'])) {
	        $appointmentId = $_POST["appointmentId"];
	    }
	    if (isset($_POST['visitTypeId'])) {
	        $visitType = $_POST["visitTypeId"];
	    }
	   	
	   	$sqlCaseId = "SELECT MAX(case_id) FROM visits WHERE patient_id ='".$patientId."'"; //Select maximum case id for this patient
		$queryCaseId = mysqli_query($conn, $sqlCaseId);
		while ($row = mysqli_fetch_row($queryCaseId)) {
			$CaseId = $row[0];
		}
		if($CaseId != ""){
			$sql    = "INSERT INTO visits(visit_type,patient_id,created_on,updated_on,created_by,updated_by,case_id) 
				VALUES('" . $visitType . "','" . $patientId . "'," . $date->getTimestamp() . "," . $date->getTimestamp() . "," . $userId . "," . $userId . ",'" . $CaseId . "')";
			$result = mysqli_query($conn, $sql);
			if ($result == '1') {
				
				$sql_id   = "SELECT LAST_INSERT_ID()";
				$query_id = mysqli_query($conn, $sql_id);
				while ($row = mysqli_fetch_row($query_id)) {
					$visitId = $row[0];
				}

				$sqlUpdate   = "UPDATE appointments SET status = 'Activated' WHERE id = '" . $appointmentId . "'";
				$resultUpdate = mysqli_query($conn, $sqlUpdate);

				$sql_id   = "SELECT value FROM configuration WHERE name = 'visit_prefix'";
				$query_id = mysqli_query($conn, $sql_id);
				while ($row = mysqli_fetch_row($query_id)) {
					$visitPrefix = $row[0];
				}									 
				
				$visitIdLength = 6-strlen($visitId);
				for($i=0;$i<$visitIdLength;$i++){
					$visitId = "0".$visitId;
				}
				
				$visitId = $visitPrefix.$visitId;
				echo $visitId;

			}
		} 
	   
	}
	
	
	
?>