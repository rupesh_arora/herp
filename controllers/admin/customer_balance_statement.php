<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "showTotalBill") {
		$fromDate = $_POST['fromDate'];
		$toDate = $_POST['toDate'];
		$query = "SELECT customer_invoice.total_bill AS balance,customer_invoice.customer_id,CONCAT(customer.first_name,' ',customer.last_name) AS customer_name FROM customer_invoice
		LEFT JOIN customer ON customer.id = customer_invoice.customer_id
		WHERE customer_invoice.`status` = 'A'";

		if($fromDate !='' AND $toDate !=''){
			$query.= " AND customer_invoice.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59') ";
		}
		if($fromDate !='' AND $toDate ==''){
			$query.= " AND customer_invoice.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP(NOW()) ";
		}
		if($fromDate =='' AND $toDate !=''){
			$query.= " AND customer_invoice.created_on BETWEEN '0000000000' AND UNIX_TIMESTAMP('".$toDate."  23:59:59') ";
		}


		$query.= " GROUP BY customer.id";

		$result = mysqli_query($conn,$query);
		$rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
		echo json_encode($rows);
	}