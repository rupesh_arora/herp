<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "showAccountType") { // show insurance company type
	    $query = "SELECT id,type FROM account_type where status = 'A' ";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	if ($operation == "showClass") { // show insurance company type
	    $query = "SELECT id,class FROM class where status = 'A' ";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "showSubType") { // show insurance company type
	    $query = "SELECT id,name FROM sub_type where status = 'A' ";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	if ($operation == "showChild") { // show insurance company type
	    $query = "SELECT id,account_no,account_name FROM chart_of_account where status = 'A' AND is_parent = 'yes' ";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	
	if ($operation == "saveCashOfAccount") {

		$accountNo = $_POST['accountNo'];
		$accountName = $_POST['accountName'];
		$accountType = $_POST['accountType'];
		$isChild = $_POST['isChild'];
		$subType = $_POST['subType'];
		$accountClass = $_POST['accountClass'];
		$notes = $_POST['notes'];

		$selChartOfAccount = "SELECT account_no FROM chart_of_account WHERE account_no ='".$accountNo."' AND status = 'A'";
		$checkChartOfAccount = mysqli_query($conn,$selChartOfAccount);
		$countChartOfAccount = mysqli_num_rows($checkChartOfAccount);

		if ($countChartOfAccount == "0") {

			$query = "INSERT INTO chart_of_account(account_no,account_name,account_type,parent_account_no,class,subtype,notes,created_by,updated_by,created_on,updated_on) VALUES('".$accountNo."','".$accountName."','".$accountType."','".$isChild."','".$accountClass."','".$subType."','".$notes."','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";

			$result = mysqli_query($conn, $query);
			$last_id = mysqli_insert_id($conn);

			//in case it is parent then
			if ($isChild == '') {
				$updateParent = "UPDATE chart_of_account SET parent_account_no = '".$last_id."' , is_parent = 'yes' WHERE id = '".$last_id."' ";
				$updateResult = mysqli_query($conn, $updateParent);
				echo $updateResult;
			}
			else{
				echo $result;
			}
			
		}
		else{
			echo "0";
		}

	}

	if ($operation == "show") { // show active data

		$query = "SELECT coa.id,coa.account_no,coa.account_name,coa.account_type,(CASE WHEN coa.is_parent = 'yes' THEN '' ELSE ca.account_name  END ) AS parent_account_name  ,coa.parent_account_no,coa.class,coa.subtype,coa.notes,sub_type.name AS sub_type_name,class.class AS class_name,account_type.`type` AS account_type_name,coa.is_parent FROM chart_of_account AS coa 
			LEFT JOIN chart_of_account AS ca ON ca.id = coa.parent_account_no 
			LEFT JOIN class on class.id = coa.class
			LEFT JOIN sub_type ON sub_type.id = coa.subtype
			LEFT JOIN account_type ON account_type.id = coa.account_type
			WHERE coa.status = 'A' ORDER BY parent_account_no";
	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    }

	if ($operation == "checked") {
	    
	    $query = "SELECT coa.id,coa.account_no,coa.account_name,coa.account_type,(CASE WHEN coa.is_parent = 'yes' THEN '' ELSE ca.account_name  END ) AS parent_account_name  ,coa.parent_account_no,coa.class,coa.subtype,coa.notes,sub_type.name AS sub_type_name,class.class AS class_name,account_type.`type` AS account_type_name ,coa.is_parent
	    	FROM chart_of_account AS coa 
			LEFT JOIN chart_of_account AS ca ON ca.id = coa.parent_account_no 
			LEFT JOIN class on class.id = coa.class
			LEFT JOIN sub_type ON sub_type.id = coa.subtype
			LEFT JOIN account_type ON account_type.id = coa.account_type
			WHERE coa.status = 'I'  ORDER BY parent_account_no";
	    
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	if ($operation == "update") // update data
	{
	    $accountNo = $_POST['accountNo'];
	    $accountName = $_POST['accountName'];
	    $accountType = $_POST['accountType'];
	    $isChild = $_POST['isChild'];
	    $subType = $_POST['subType'];
	    $accountClass = $_POST['accountClass'];
		$notes = $_POST['notes'];
	    $id = $_POST['id'];
		
	    if ($isChild == '') {
	    	$isParent = 'yes';
	    }
	    else{
	    	$isParent = 'no';
	    }

		$selChartOfAccount = "SELECT account_no FROM chart_of_account WHERE account_no ='".$accountNo."' AND status = 'A' AND id !='".$id."'";
		$checkChartOfAccount = mysqli_query($conn,$selChartOfAccount);
		$countChartOfAccount = mysqli_num_rows($checkChartOfAccount);

		if ($countChartOfAccount == "0") {

			echo$sql    = "UPDATE chart_of_account set account_no = '".$accountNo."',account_name = '".$accountName."',account_type = '".$accountType."',parent_account_no = '".$isChild."',class = '".$accountClass."',subtype = '".$subType."',notes= '".$notes."',updated_on = UNIX_TIMESTAMP() ,updated_by = '".$userId."' ,is_parent = '".$isParent."' WHERE id = '".$id."' ";

			
			$result = mysqli_query($conn, $sql);
			echo $result;
		}
		else {
			echo "0";
		}
		
	}

	if ($operation == "delete") {
        $id = $_POST['id'];

        $selChartOfAccount = "SELECT parent_account_no FROM chart_of_account WHERE parent_account_no = '".$id."' 
        	AND status = 'A'";
		$checkChartOfAccount = mysqli_query($conn,$selChartOfAccount);
		$countChartOfAccount = mysqli_num_rows($checkChartOfAccount);

		if ($countChartOfAccount == "0") {

		$sql    = "UPDATE chart_of_account SET status= 'I' where id = '" . $id . "'";
	    $result = mysqli_query($conn, $sql);
	    echo $result;	 
	    }
		else {
			echo "0";
		}   
	}

	if ($operation == "restore") {// for restore    
        $id = $_POST['id'];
        $accountNo = $_POST['accountNo'];

        $selChartOfAccount = "SELECT account_no FROM chart_of_account WHERE account_no ='".$accountNo."' AND status = 'A' AND id !='".$id."'";
		$checkChartOfAccount = mysqli_query($conn,$selChartOfAccount);
		$countChartOfAccount = mysqli_num_rows($checkChartOfAccount);

		if ($countChartOfAccount == "0") {

			 $sql    = "UPDATE chart_of_account SET status= 'A'  WHERE  id = '" . $id . "'";
			$result = mysqli_query($conn, $sql);
		    echo $result;
		    }
		else {
			echo "0";
		}
	}
?>