<?php
/*****************************************************************************************************
 * File Name    :   drug.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages drugs data
 ****************************************************************************************************/
   session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
    $operation = "";
    $drugId = "";
    $drugName = "";
    $drugCategory = "";
    $alternateName = "";
    $unitMeasure = "";
    $unitCost = "";
    $unitPrice = "";
    $composition = "";
    $alternateDrugName = "";
	$date = new DateTime();
	
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	
	//This operation used for select the drug category 
	if($operation == "showDrugCategory")			
	{		
		$query="Select id,category FROM drug_categories WHERE status = 'A'  ORDER BY category";		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}		
		 print json_encode($rows);
	} 
	
	//This operation is used for select the unit measure 
	if($operation == "showUnitMeasure")			
	{		
		$query="SELECT id,unit_abbr FROM units WHERE status='A'  ORDER BY unit_abbr";		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}			 
		 print json_encode($rows);
	}
	
	//This operation is used for select the alternateDrugName
	if($operation == "showAlternateDrugName")			
	{
		
		$query="SELECT id,name FROM drugs WHERE status='A'  ORDER BY name";		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}			
		 print json_encode($rows);
	}
	
	//This operation is used for select the drug id
	if($operation == "showDrugId")			
	{	
		$query="SELECT id, (SELECT value FROM configuration WHERE name ='drug_prefix' ) FROM drugs ORDER BY id DESC LIMIT 1";		
		$result=mysqli_query($conn,$query);	
		$rows_count= mysqli_num_rows($result);
		if($rows_count > 0){
			while($r=mysqli_fetch_row($result)) {
			 $rows[] = $r;
		}			
		 print json_encode($rows);
		}
		else{
			$query="SELECT value FROM configuration WHERE name ='drug_prefix'";
			$result=mysqli_query($conn,$query);	
			$rows_count= mysqli_num_rows($result);
			if($rows_count > 0){
				while($r=mysqli_fetch_row($result)) {
				 $rows[] = $r;
			}			
			 print json_encode($rows);
			}
		}
	}
	
	//This operation is used for save the data 
	if($operation == "save")			
	{
		if (isset($_POST['drugCode'])) {
			$drugCode=$_POST['drugCode'];
		}
		if (isset($_POST['drugName'])) {
			$drugName=$_POST['drugName'];
		}
		if (isset($_POST['drugCategory'])) {
			$drugCategory=$_POST['drugCategory'];
		}
		if (isset($_POST['alternateName'])) {
			$alternateName=$_POST['alternateName'];
		}
		if (isset($_POST['unitMeasure'])) {
			$unitMeasure=$_POST['unitMeasure'];
		}
		if (isset($_POST['unitCost'])) {
			$unitCost=$_POST['unitCost'];
		}
		if (isset($_POST['unitPrice'])) {
			$unitPrice=$_POST['unitPrice'];
		}
		if (isset($_POST['alternateDrugName'])) {
			$alternateDrugName=$_POST['alternateDrugName'];
		}
		if (isset($_POST['composition'])) {
			$composition=$_POST['composition'];
		}
		
		$sql = "INSERT INTO drugs(drug_code,name,alternate_name,drug_category_id,unit,cost,price,alternate_drug_id,composition,created_by,created_on,updated_by,updated_on) 
		VALUES('".$drugCode."','".$drugName."','".$alternateName."','".$drugCategory."','".$unitMeasure."','".$unitCost."','".$unitPrice."','".$alternateDrugName."','".$composition."',".$userId.",".$date->getTimestamp().",".$userId.",".$date->getTimestamp().")";

		$result= mysqli_query($conn,$sql);
		$id = mysqli_insert_id($conn);

		$query = "INSERT INTO `insurance_revised_cost`(`insurance_company_id`, `scheme_plan_id`, `scheme_name_id`,`item_name`, `service_type`, `item_id`, 
        `actual_cost`,`revised_cost`, `created_on`, `created_by`, `status`)
         SELECT  
        insurance_company_id ,scheme_plan_id , scheme_name_id
        ,'".$drugName."' AS item_name ,'pharmacy' AS service_type , '".$id."' As item_id, '".$unitPrice."' AS
        actual_cots, '".$unitPrice."' AS revised_cost,UNIX_TIMESTAMP() AS created_on ,'".$userId."' 
        AS created_by, 'A' AS `status`   FROM insurance_revised_cost GROUP BY scheme_name_id ";

        $sqlItem    = "INSERT INTO items(name,code,item_category_id,description,created_on,updated_on,created_by,updated_by,status) 
		VALUES('" . $drugName . "','" . $drugCode . "','5','".$composition."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."','A')";

        $result = mysqli_multi_query($conn, $query.';'.$sqlItem);
    	echo $result;			
	}	
	
	//This operartion is used for show the active data 
	if($operation == "show"){
		
		$query= "SELECT d.id,(SELECT value FROM configuration WHERE name ='drug_prefix' ) AS drug_prefix ,d.composition,d.name,d.alternate_name,d.drug_category_id,d.unit,
		d.cost,d.price,d.alternate_drug_id,c_d.category,u.unit_abbr,
		l_j.name AS alternate_drug_name from drugs AS d 
		LEFT JOIN units AS u ON d.unit = u.id 
		LEFT JOIN drug_categories AS c_d ON d.drug_category_id = c_d.id 
		LEFT JOIN drugs AS l_j ON d.alternate_drug_id = l_j.id 
		WHERE d.status = 'A' and c_d.status = 'A'" ;
		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 //print json_encode($rows);
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
	//This operation is used for show the inactive data 
	if($operation == "checked"){
		$query= "SELECT d.id,(SELECT value FROM configuration WHERE name ='drug_prefix' ) AS drug_prefix,d.drug_code,d.composition,d.name,d.alternate_name,d.drug_category_id,d.unit,
		d.cost,d.price,d.alternate_drug_id,c_d.category,u.unit_abbr,
		l_j.name AS alternate_drug_name from drugs AS d 
		LEFT JOIN units AS u ON d.unit = u.id 
		LEFT JOIN drug_categories AS c_d ON d.drug_category_id = c_d.id 
		LEFT JOIN drugs AS l_j ON d.alternate_drug_id = l_j.id 
		WHERE d.status = 'I' or c_d.status='I'" ;
		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
	//This operation is used for update the data 
	if($operation == "update")			
	{
		if (isset($_POST['drugCode'])) {
			$drugCode=$_POST['drugCode'];
		}
		if (isset($_POST['drugName'])) {
			$drugName=$_POST['drugName'];
		}
		if (isset($_POST['drugCategory'])) {
			$drugCategory=$_POST['drugCategory'];
		}
		if (isset($_POST['alternateName'])) {
			$alternateName=$_POST['alternateName'];
		}
		if (isset($_POST['unitMeasure'])) {
			$unitMeasure=$_POST['unitMeasure'];
		}
		if (isset($_POST['unitCost'])) {
			$unitCost=$_POST['unitCost'];
		}
		if (isset($_POST['unitPrice'])) {
			$unitPrice=$_POST['unitPrice'];
		}
		if (isset($_POST['alternateDrugName'])) {
			$alternateDrugName=$_POST['alternateDrugName'];
		}
		if (isset($_POST['composition'])) {
			$composition=$_POST['composition'];
		}
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}	
		$oldCode = '';
		$selOldCode = "SELECT drug_code FROM drugs WHERE id = '".$id."'";
		$resultOldCode = mysqli_query($conn,$selOldCode);
		while ($r = mysqli_fetch_assoc($resultOldCode)) {
			$oldCode = $r['drug_code'];
		}
		
		$sql = "UPDATE drugs SET drug_code= '".$drugCode."',composition= '".$composition."',name= '".$drugName."',alternate_name='".$alternateName."',drug_category_id= '".$drugCategory."',unit= '".$unitMeasure."',cost= '".$unitCost."',price= '".$unitPrice."',alternate_drug_id= '".$alternateDrugName."',updated_by= '".$userId."',updated_on= ".$date->getTimestamp()." WHERE  id = '".$id."'";


		echo $sqlUpdateItem    = "UPDATE items SET code= '" . $drugCode . "' , name = '" . $drugName . "',description='" . $composition . "',updated_on=UNIX_TIMESTAMP(),updated_by='".$userId."' WHERE code = '" . $oldCode . "'";

		$result= mysqli_multi_query($conn,$sql.';'.$sqlUpdateItem);
		echo $result;		
	}
	
	//This operation is used for update the status 
	if($operation == "delete")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}		
		
		$oldCode = '';
		$selOldCode = "SELECT drug_code FROM drugs WHERE id = '".$id."'";
		$resultOldCode = mysqli_query($conn,$selOldCode);
		while ($r = mysqli_fetch_assoc($resultOldCode)) {
			$oldCode = $r['drug_code'];
		}

		$sql = "UPDATE drugs SET status= 'I'  WHERE  id = '".$id."'";

		$delItem = "UPDATE items SET status= 'I'  WHERE  code = '".$oldCode."'";
		$result= mysqli_multi_query($conn,$sql.';'.$delItem);  
		echo $result;
	}
	
	//to restore data back to data 
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}

		$oldCode = '';
		$selOldCode = "SELECT drug_code FROM drugs WHERE id = '".$id."'";
		$resultOldCode = mysqli_query($conn,$selOldCode);
		while ($r = mysqli_fetch_assoc($resultOldCode)) {
			$oldCode = $r['drug_code'];
		}

		$sql = "UPDATE drugs SET status= 'A'  WHERE  id = '".$id."'";

		$itemRestore = "UPDATE items SET status= 'A'  WHERE  code = '".$oldCode."'";
		$result= mysqli_multi_query($conn,$sql.';'.$itemRestore);  
		echo $result;
	}
?>