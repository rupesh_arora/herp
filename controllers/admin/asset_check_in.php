<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "showRoom") {
    
	    $query  = "SELECT id,`number` FROM ward_room WHERE status='A' ORDER BY `number`";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    
	    print json_encode($rows);
	}

	if ($operation == "showLocation") {
    
	    $query  = "SELECT id,location FROM asset_location WHERE `status`='A' ORDER BY location";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    
	    print json_encode($rows);
	}
	if ($operation == "showAssetClass") {
    
	    $query  = "SELECT id,asset_class_name FROM asset_class WHERE `status`='A' ORDER BY asset_class_name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    
	    print json_encode($rows);
	}
	if ($operation == "showAssetSubClass") {
    
	    $query  = "SELECT id,sub_class_name FROM asset_subclass WHERE `status`='A' ORDER BY sub_class_name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    
	    print json_encode($rows);
	}
	if ($operation == "show") { // show active data	

		if (isset($_GET['staffId'])) {
			$staffId = $_GET["staffId"];
		}
		    
    	echo $query = "SELECT a.id,a.asset_no,a.asset_barcode FROM asset_check_out AS aco  
			LEFT JOIN asset AS a ON a.id = aco.asset_id 
			WHERE a.status = 'A' AND aco.staff_id = '".$staffId."'";   	

	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}

	if ($operation == "showTableData") { // show active data	
		    
    	$query = "SELECT aco.id,aco.staff_id,aco.staff_name,aco.set_room_to,aco.set_location_to,al.location,r.number,
				(SELECT value FROM configuration WHERE name = 'staff_prefix') AS staff_prefix  
				FROM asset_check_in AS aco 
				LEFT JOIN room AS r ON r.id = aco.set_room_to 
				LEFT JOIN asset_location AS al ON al.id = aco.set_location_to 
				WHERE aco.status = 'A'";   	

	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows); 
	   
    
	}

	if ($operation == "search") { // show active data
		$manufacturerId = '';
		$brandNameId = '';
		$modalId = '';

		if (isset($_POST['staffId'])) {
			$staffId = $_POST["staffId"];
		}

		if (isset($_POST['manufacturerId'])) {
			$manufacturerId = $_POST["manufacturerId"];
		}
		if (isset($_POST['brandNameId'])) {
			$brandNameId = $_POST["brandNameId"];
		}
		if (isset($_POST['modalId'])) {
			$modalId = $_POST["modalId"];
		}
		if (isset($_POST['assetNo'])) {
			$assetNo = $_POST["assetNo"];
		}
		if (isset($_POST['barcode'])) {
			$barcode = $_POST["barcode"];
		}
		if (isset($_POST['assetDescription'])) {
			$assetDescription = $_POST["assetDescription"];
		}
		if (isset($_POST['assetAdvanceDescription'])) {
			$assetAdvanceDescription = $_POST["assetAdvanceDescription"];
		}
		if (isset($_POST['assetClass'])) {
			$assetClass = $_POST["assetClass"];
		}
		if (isset($_POST['assetSubClass'])) {
			$assetSubClass = $_POST["assetSubClass"];
		}
		if (isset($_POST['assetLocation'])) {
			$assetLocation = $_POST["assetLocation"];
		}
    
    	$query = "SELECT a.id,a.asset_no,a.asset_barcode FROM asset_check_out AS aco  
				LEFT JOIN asset AS a ON a.id = aco.asset_id 
				WHERE a.status = 'A' AND aco.staff_id = '".$staffId."' AND a.is_checked_out ='YES' ";

    	if($manufacturerId != '' || $brandNameId != '' || $modalId != '' || $assetNo != '' || $barcode != '' || $assetClass != '-1' || $assetSubClass != '-1' || $assetLocation != '-1' || $assetDescription != '' || $assetAdvanceDescription != ''){
			
			if($manufacturerId !=''){
				$query .= " AND a.manufacturer_id = '".$manufacturerId."' ";
				$isFirst = "true";
			}
			if($assetDescription !=''){
				$query .= " AND a.asset_description = '".$assetDescription."' ";
				$isFirst = "true";
			}
			if($assetAdvanceDescription !=''){
				$query .= " AND a.asset_advance_description = '".$assetAdvanceDescription."' ";
				$isFirst = "true";
			}
			if($brandNameId !=''){
				$query .= " AND a.brand_name_id = '".$brandNameId."' ";
				$isFirst = "true";
			}
			if($modalId !=''){
				$query .= " AND a.modal_id = '".$modalId."' ";
				$isFirst = "true";
			}
			if($assetNo !=''){
				$query .= " AND a.asset_no LIKE '%".$assetNo."%' ";
				$isFirst = "true";
			}
			if($barcode !=''){
				$query .= " AND a.asset_barcode  LIKE '%".$barcode."%' ";
				$isFirst = "true";
			}
			if($assetClass !='-1'){
					$query .= " AND a.asset_class_id = '".$assetClass."' ";
					$isFirst = "true";
			}
			if($assetSubClass !='-1'){
				$query .= " AND a.asset_sub_class_id = '".$assetSubClass."' ";
				$isFirst = "true";
			}
			if($assetLocation !='-1'){
				$query .= " AND a.asset_location_id = '".$assetLocation."' ";
				$isFirst = "true";
			}
		}

	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	    
	    
    
	}

	if ($operation == "saveData") {
		$staffId = $_POST['staffId'];
		$staffName = $_POST['staffName'];
		$setRoomTo = $_POST['setRoomTo'];
		$setLocation = $_POST['setLocation'];
		$saveArray = json_decode($_POST['saveArray']);
		
		

		$insertSubDetails = "INSERT INTO `asset_check_in`( `staff_id`, `staff_name`, `asset_id`, `set_room_to`, `set_location_to`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES ";
		$counter = 0;
		$subQry = '';
		foreach ($saveArray as  $value) {
			$assetId = $value->assetId;

			if ($counter > 0) {
				$subQry.= ',';
			}
			$subQry.= "('".$staffId."','".$staffName."','".$assetId."','".$setRoomTo."','".$setLocation."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
			$counter++;
			$sql = "UPDATE asset SET is_checked_out = 'NO' WHERE id = '".$assetId."'";
			$rs = mysqli_query($conn,$sql);
		}

		$completeQry = $insertSubDetails.$subQry;
		$result = mysqli_query($conn,$completeQry);
		echo $result;
	}
?>