<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "showPeriod") {
    
	    $query  = "SELECT id,`period` FROM asset_count_period WHERE status='A' ORDER BY `period`";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    
	    print json_encode($rows);
	}
	if ($operation == "showBarcode") {
    
	    $query  = "SELECT id,`asset_barcode` FROM asset WHERE status='A'";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    
	    print json_encode($rows);
	}
	if ($operation == "showRoom") {
    
	    $query  = "SELECT id,`number` FROM room WHERE status='A' ORDER BY `number`";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    
	    print json_encode($rows);
	}

	if ($operation == "showLocation") {
    
	    $query  = "SELECT id,location FROM asset_location WHERE `status`='A' ORDER BY location";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    
	    print json_encode($rows);
	}
	

	if ($operation == "show") { // show active data	
		    
    	$query = "SELECT id,asset_no,asset_barcode FROM asset WHERE status = 'A'";   	

	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}

	
	
	
	if ($operation == "search") { // show active data
		$manufacturerId = '';
		$brandNameId = '';
		$modalId = '';

		if (isset($_POST['manufacturerId'])) {
			$manufacturerId = $_POST["manufacturerId"];
		}
		if (isset($_POST['brandNameId'])) {
			$brandNameId = $_POST["brandNameId"];
		}
		if (isset($_POST['modalId'])) {
			$modalId = $_POST["modalId"];
		}
		if (isset($_POST['assetNo'])) {
			$assetNo = $_POST["assetNo"];
		}
		if (isset($_POST['barcode'])) {
			$barcode = $_POST["barcode"];
		}
		if (isset($_POST['description'])) {
			$description = $_POST["description"]; 
		}
		if (isset($_POST['advanceDescription'])) {
			$advanceDescription = $_POST["advanceDescription"];
		}
		if (isset($_POST['assetClass'])) {
			$assetClass = $_POST["assetClass"];
		}
		if (isset($_POST['assetSubClass'])) {
			$assetSubClass = $_POST["assetSubClass"];
		}
		if (isset($_POST['assetLocation'])) {
			$assetLocation = $_POST["assetLocation"];
		}
    
    	$query = "SELECT id,asset_no,asset_barcode FROM asset WHERE status = 'A'";

    	if($manufacturerId != '' || $brandNameId != '' || $modalId != '' || $assetNo != '' || $barcode != '' || $assetClass != '-1' || $assetSubClass != '-1' || $assetLocation != '-1'){
			
			if($manufacturerId !=''){
				$query .= " AND manufacturer_id = '".$manufacturerId."' ";
				$isFirst = "true";
			}
			if($brandNameId !=''){
				$query .= " AND brand_name_id = '".$brandNameId."' ";
				$isFirst = "true";
			}
			if($modalId !=''){
				$query .= " AND modal_id = '".$modalId."' ";
				$isFirst = "true";
			}
			if($assetNo !=''){
				$query .= " AND asset_no LIKE '%".$assetNo."%' ";
				$isFirst = "true";
			}
			if($barcode !=''){
				$query .= " AND asset_barcode  LIKE '%".$barcode."%' ";
				$isFirst = "true";
			}
			if($assetClass !='-1'){
					$query .= " AND asset_class_id = '".$assetClass."' ";
					$isFirst = "true";
			}
			if($assetSubClass !='-1'){
				$query .= " AND asset_sub_class_id = '".$assetSubClass."' ";
				$isFirst = "true";
			}
			if($assetLocation !='-1'){
				$query .= " AND asset_location_id = '".$assetLocation."' ";
				$isFirst = "true";
			}
		}

	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	    
	    
    
	}

	if ($operation == "showCount") {

		
		if (isset($_POST['period'])) {
			$period = $_POST["period"];
		}
		if (isset($_POST['room'])) {
			$room = $_POST["room"];
		}
		if (isset($_POST['barcode'])) {
			$barcode = $_POST["barcode"];
		}
		if (isset($_POST['location'])) {
			$location = $_POST["location"]; 
		}
		
		

		$query = "SELECT a.id,a.is_checked_out,a.asset_no,a.asset_barcode,a.asset_description,a.asset_advance_description,r.number AS expected_room FROM asset AS a 
			LEFT JOIN room AS r ON r.id = a.room_id 
			WHERE a.status = 'A'";

    	if($period != '-1' || $room != '-1' || $barcode != '-1' || $location != '-1' ){
			
			/*if($period !=''){
				$query .= " AND a.manufacturer_id = '".$period."' ";
				$isFirst = "true";
			}*/
			if($room !='-1'){
				$query .= " AND a.room_id = '".$room."' ";
				$isFirst = "true";
			}
			if($barcode !='-1'){
				$query .= " AND a.id = '".$barcode."' ";
				$isFirst = "true";
			}
			if($location !='-1'){
				$query .= " AND a.asset_location_id = '".$location."' ";
				$isFirst = "true";
			}
		}
		$rows         = array();
		$actualRoom = array();
		$countRow = array();
		
		$result = mysqli_query($conn, $query);
	    $length =0;
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;	        
	        $id = $r['id'];
	        $actual_room = $r['expected_room'];
	        $length++;
	        $status = $r['is_checked_out'];
	        if($status == "YES"){
		    	$assetOutQuery = "SELECT ro.number AS actual_room  FROM asset_check_out AS aco
				LEFT JOIN room AS ro ON ro.id = aco.set_room_to  
				WHERE aco.status = 'A' AND aco.asset_id ='".$id."' ORDER BY aco.id DESC LIMIT 1";

		    	$assetOutResult = mysqli_query($conn, $assetOutQuery);	    
		    	while ($r = mysqli_fetch_assoc($assetOutResult)) {
		        	$actualRoom[] = $r;
		    	}
		    }
		    else{
		    	$assetInQuery = "SELECT ro.number AS actual_room  FROM asset_check_in AS aci
				LEFT JOIN room AS ro ON ro.id = aci.set_room_to  
				WHERE aci.status = 'A' AND aci.asset_id ='".$id."' ORDER BY aci.id DESC LIMIT 1";

		    	$assetInResult = mysqli_query($conn, $assetInQuery);
		    	$numRows = mysqli_num_rows($assetInResult);
		    	if($numRows != "0")	{
		    		while ($r = mysqli_fetch_assoc($assetInResult)) {
				        $actualRoom[] = $r;
				    }
		    	}    
		    	else{
		    		//echo $actual_room." hello";
		    		$actualRoom[] = $actual_room;
		    	}
			    
		    }

	    }
	    $rows[] = $length;
	    print json_encode(array_merge($rows,$actualRoom));
	}
?>