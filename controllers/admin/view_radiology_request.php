<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	
	include 'config.php';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}	
	
	if($operation == "update")   
	{
		if (isset($_POST['lab_test_id'])) {
			$id=$_POST["lab_test_id"];
		}
		$query="UPDATE radiology_test_request SET test_status = 'done',request_date=UNIX_TIMESTAMP() WHERE  id = '".$id."'";  
		$result=mysqli_query($conn,$query);

		if($result)
		{
			$query="INSERT INTO radiology_test_result(request_id,created_on,created_by) VALUES (".$id.",UNIX_TIMESTAMP(),".$_SESSION["globaluser"].")"; 
			$result=mysqli_query($conn,$query);
			echo $conn->insert_id;
		} 
	}
	
	if($operation == "show"){
		
		$query= "SELECT radiology_test_request.patient_id,
		radiology_test_request.visit_id,
		radiology_tests.test_code,radiology_tests.name,
		radiology_test_request.cost,
		radiology_test_request.pay_status,
		patients.salutation , patients.first_name , 
		patients.last_name ,
		patients.middle_name,
		patients.dob,
		patients.mobile,
		patients.care_of,
		patients.address,patients.gender,
		patients.zip,patients.images_path,
		patients.care_contact,
		patients.email,
		radiology_test_request.id,
		visit_type.`type` AS visitType,
		(SELECT  locations.name as state FROM locations WHERE locations.location_id = patients.country_id) as country,
		(SELECT  locations.name as state FROM locations WHERE locations.location_id = patients.city_id) as city,
		(SELECT  locations.name as state FROM locations WHERE locations.location_id = patients.state_id) as state,
		(SELECT value FROM configuration WHERE name = 'patient_prefix') AS patient_prefix,
		(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visit_prefix
		FROM `radiology_test_request` 
		LEFT JOIN radiology_tests ON radiology_test_request.radiology_test_id = radiology_tests.id 
		LEFT JOIN patients On patients.id = radiology_test_request.patient_id 
		LEFT JOIN visits On visits.id = radiology_test_request.visit_id   
		LEFT JOIN visit_type On visit_type.id = visits.visit_type   
		WHERE radiology_test_request.pay_status = 'paid' AND radiology_test_request.test_status = 'pending'  AND radiology_test_request.request_type = 'internal'  AND radiology_test_request.status = 'A' 
		ORDER BY ((DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%y-%m-%d')) = CURDATE()) desc,visits.visit_type= '3' desc,visits.id  asc";		
		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
	
	/* if($operation == "loadSpecimen")			
	{
		$sql = "SELECT id as value,type as text FROM specimens_types WHERE  status = 'A'";
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		echo json_encode($rows);	
	} */
	
	if($operation == "loadLabTests")			
	{
		$sql = "SELECT id as value,name as text FROM radiology_tests WHERE  status = 'A'";
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		echo json_encode($rows);
	}
?>