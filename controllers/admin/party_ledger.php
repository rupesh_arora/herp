<?php
/*File Name  :   party_ledger.php
Company Name :   Qexon Infotech
Created By   :   Tushar Gupta
Created Date :   30th Dec, 2015
Description  :   This page manages*/

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

/*include config file*/
include 'config.php';


/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

/*operation to show leave type*/
if ($operation == "showBindSeller") {
    
    $query  = "SELECT id,name FROM seller WHERE STATUS = 'A' ORDER BY name ASC";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

// search patient opearion
if ($operation == "SearchLedger") {
   
    if (isset($_POST['sellerId'])) {
        $sellerId = $_POST["sellerId"];
    }
	
	$query = "SELECT SUM(received_orders.quantity * received_orders.unit_cost) as credit_amount ,
		DATE_FORMAT(FROM_UNIXTIME(received_orders.received_date), '%Y-%m-%d') AS receive_date,orders.id,
		inventory_payment.total_amount AS debit_total,inventory_payment.date
		FROM received_orders
		LEFT JOIN orders ON orders.id = received_orders.order_id
		LEFT JOIN inventory_payment ON inventory_payment.order_id = orders.id
		WHERE orders.seller_id = '".$sellerId."' GROUP BY orders.id";
        
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }   
    print json_encode($rows);
}
?>