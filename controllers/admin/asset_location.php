<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "saveAssetLocation") {

		$location = $_POST['location'];
		$description = $_POST['description'];

		$selAssetLocation = "SELECT location FROM asset_location WHERE location ='".$location."' AND status = 'A'";
		$checkAssetLocation = mysqli_query($conn,$selAssetLocation);
		$countAssetLocation = mysqli_num_rows($checkAssetLocation);

		if ($countAssetLocation == "0") {

			 $query = "INSERT INTO asset_location(location,description,created_by,updated_by,created_on,updated_on) VALUES('".$location."','".$description."','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";

			$result = mysqli_query($conn, $query);
			echo $result;
		}
		else{
			echo "0";
		}
	}

	if ($operation == "show") { // show active data
    
    	$query = "SELECT id,location,description FROM asset_location WHERE status = 'A'";
	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}

	if ($operation == "checked") {
	    
	    $query = "SELECT id,location,description FROM asset_location WHERE status = 'I'";
	    
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	if ($operation == "update") // update data
	{
	    $location = $_POST['location'];
		$description = $_POST['description'];
	    $id = $_POST['id'];

	    $selAssetLocation = "SELECT location FROM asset_location WHERE location ='".$location."' AND status = 'A' AND id !='".$id."'";
		$checkcountAssetLocation = mysqli_query($conn,$selAssetLocation);
		$countcountAssetLocation = mysqli_num_rows($checkcountAssetLocation);

		if ($countcountAssetLocation == "0") {
		
			$sql    = "UPDATE asset_location set location = '".$location."',description= '".$description."',updated_on = UNIX_TIMESTAMP() ,updated_by = '".$userId."' where id = '".$id."' ";

			$result = mysqli_query($conn, $sql);
			echo $result;
		}
		else{
			echo "0";
		}
	}

	if ($operation == "delete") {
        $id = $_POST['id'];

		$sql    = "UPDATE asset_location SET status= 'I' where id = '" . $id . "'";
	    $result = mysqli_query($conn, $sql);
	    echo $result;	    
	}

	if ($operation == "restore") {// for restore    
		$location = $_POST['location'];
        $id = $_POST['id'];

        $selAssetLocation = "SELECT location FROM asset_location WHERE location ='".$location."' AND status = 'A' AND id !='".$id."'";
		$checkcountAssetLocation = mysqli_query($conn,$selAssetLocation);
		$countcountAssetLocation = mysqli_num_rows($checkcountAssetLocation);

		if ($countcountAssetLocation == "0") {

			$sql    = "UPDATE asset_location SET status= 'A'  WHERE  id = '" . $id . "'";
			$result = mysqli_query($conn, $sql);
		    echo $result;
		}
		else{
			echo "0";
		}

	}
?>