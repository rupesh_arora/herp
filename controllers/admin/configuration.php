<?php
/*File Name  :   configuration.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   31th Dec, 2015
Description  :   This page manages all the prefix*/

	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}

	/*include config file*/
	include 'config.php';

	$operation = "";

	/*checking operation set or not*/
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}
	if($operation == "update"){	

		$flag ="flase";
		
		if (isset($_POST['errorColor'])) {
			$errorColor = $_POST["errorColor"];
			
			if($errorColor != ""){
				$sqlUpdate = "UPDATE configuration SET value = '" . $errorColor . "',updated_on = UNIX_TIMESTAMP(),updated_by = '" . $userId . "' WHERE name = 'error_color'";			
				$resultUpdate    = mysqli_query($conn, $sqlUpdate);
				$flag = "true";
			}
			
		}
		if (isset($_POST['patientId'])) {
			$patientId = $_POST["patientId"];
			
			if($patientId != ""){
				$sqlUpdate = "UPDATE configuration SET value = '" . $patientId . "',updated_on = UNIX_TIMESTAMP(),updated_by = '" . $userId . "' WHERE name = 'patient_prefix'";			
				$resultUpdate    = mysqli_query($conn, $sqlUpdate);
				$flag = "true";
			}
			
		}
		if (isset($_POST['VisitId'])) {
			$VisitId = $_POST["VisitId"];
			
			if($VisitId != ""){
				$sqlUpdate = "UPDATE configuration SET value = '" . $VisitId . "',updated_on = UNIX_TIMESTAMP(),updated_by = '" . $userId . "' WHERE name = 'visit_prefix'";			
				$resultUpdate    = mysqli_query($conn, $sqlUpdate);
				$flag = "true";
			}
			
		}
		if (isset($_POST['ipdPrefix'])) {
			$ipdPrefix = $_POST["ipdPrefix"];
			
			if($ipdPrefix != ""){
				$sqlUpdate = "UPDATE configuration SET value = '" . $ipdPrefix . "',updated_on = UNIX_TIMESTAMP(),updated_by = '" . $userId . "' WHERE name = 'ipd_prefix'";			
				$resultUpdate    = mysqli_query($conn, $sqlUpdate);
				$flag = "true";
			}
			
		}
		if (isset($_POST['drugCode'])) {
			$drugCode = $_POST["drugCode"];
			if($drugCode != ""){
				$sqlUpdate = "UPDATE configuration SET value = '" . $drugCode . "',updated_on = UNIX_TIMESTAMP(),updated_by = '" . $userId . "' WHERE name = 'drug_prefix'";			
				$resultUpdate    = mysqli_query($conn, $sqlUpdate);
				$flag = "true";
			}
			
		}
		if (isset($_POST['familyAccountPrefix'])) {
			$familyAccountPrefix = $_POST["familyAccountPrefix"];
			if($familyAccountPrefix != ""){
				$sqlUpdate = "UPDATE configuration SET value = '" . $familyAccountPrefix . "',updated_on = UNIX_TIMESTAMP(),updated_by = '" . $userId . "' WHERE name = 'account_prefix'";			
				$resultUpdate    = mysqli_query($conn, $sqlUpdate);
				$flag = "true";
			}
			
		}
		if (isset($_POST['amountPrefix'])) {
			$amountPrefix = $_POST["amountPrefix"];
			if($amountPrefix != ""){
				$sqlUpdate = "UPDATE configuration SET value = '" . $amountPrefix . "',updated_on = UNIX_TIMESTAMP(),updated_by = '" . $userId . "' WHERE name = 'amount_prefix'";			
				$resultUpdate    = mysqli_query($conn, $sqlUpdate);
				$flag = "true";
			}			
		}

		if (isset($_POST['themeBgColor'])) {			
			$themeBgColor = $_POST["themeBgColor"];

			if($themeBgColor != ""){
				$sqlUpdate = "UPDATE configuration SET value = '" . $themeBgColor . "',updated_on = UNIX_TIMESTAMP(),updated_by = '" . $userId . "'
				WHERE name = 'theme_color'";
				$resultUpdate    = mysqli_query($conn, $sqlUpdate);
				$flag = "true";
			}			
		}
		if (isset($_POST['staffPrefix'])) {			
			$staffPrefix = $_POST["staffPrefix"];

			if($staffPrefix != ""){
				$sqlUpdate = "UPDATE configuration SET value = '" . $staffPrefix . "',updated_on = UNIX_TIMESTAMP(),updated_by = '" . $userId . "'
				WHERE name = 'staff_prefix'";
				$resultUpdate    = mysqli_query($conn, $sqlUpdate);
				$flag = "true";
			}			
		}
		if (isset($_POST['invoicePrefix'])) {	

			$invoicePrefix = $_POST["invoicePrefix"];
			
			$sqlUpdate = "UPDATE configuration SET value = '" . $invoicePrefix . "',updated_on = UNIX_TIMESTAMP(),updated_by = '" . $userId . "'
			WHERE name = 'invoice_prefix'";
			$resultUpdate    = mysqli_query($conn, $sqlUpdate);
			$flag = "true";
		}
		if (isset($_POST['productPrefix'])) {	

			$productPrefix = $_POST["productPrefix"];
			
			$sqlUpdate = "UPDATE configuration SET value = '" . $productPrefix . "',updated_on = UNIX_TIMESTAMP(),updated_by = '" . $userId . "'
			WHERE name = 'product_prefix'";
			$resultUpdate    = mysqli_query($conn, $sqlUpdate);
			$flag = "true";
		}
		if (isset($_POST['customerPrefix'])) {	

			$customerPrefix = $_POST["customerPrefix"];
			
			$sqlUpdate = "UPDATE configuration SET value = '" . $customerPrefix . "',updated_on = UNIX_TIMESTAMP(),updated_by = '" . $userId . "'
			WHERE name = 'customer_prefix'";
			$resultUpdate    = mysqli_query($conn, $sqlUpdate);
			$flag = "true";
		}
		if (isset($_POST['receiptPrefix'])) {	

			$receiptPrefix = $_POST["receiptPrefix"];
			
			$sqlUpdate = "UPDATE configuration SET value = '" . $receiptPrefix . "',updated_on = UNIX_TIMESTAMP(),updated_by = '" . $userId . "'
			WHERE name = 'receipt_prefix'";
			$resultUpdate    = mysqli_query($conn, $sqlUpdate);
			$flag = "true";
		}
		if (isset($_POST['voucherPrefix'])) {	

			$voucherPrefix = $_POST["voucherPrefix"];
			
			$sqlUpdate = "UPDATE configuration SET value = '" . $voucherPrefix . "',updated_on = UNIX_TIMESTAMP(),updated_by = '" . $userId . "'
			WHERE name = 'voucher_prefix'";
			$resultUpdate    = mysqli_query($conn, $sqlUpdate);
			$flag = "true";
		}
		if (isset($_POST['imprestPrefix'])) {	

			$imprestPrefix = $_POST["imprestPrefix"];
			
			$sqlUpdate = "UPDATE configuration SET value = '" . $imprestPrefix . "',updated_on = UNIX_TIMESTAMP(),updated_by = '" . $userId . "'
			WHERE name = 'imprest_prefix'";
			$resultUpdate    = mysqli_query($conn, $sqlUpdate);
			$flag = "true";
		}
		if (isset($_POST['pettyPrefix'])) {	

			$pettyPrefix = $_POST["pettyPrefix"];
			
			$sqlUpdate = "UPDATE configuration SET value = '" . $pettyPrefix . "',updated_on = UNIX_TIMESTAMP(),updated_by = '" . $userId . "'
			WHERE name = 'petty_prefix'";
			$resultUpdate    = mysqli_query($conn, $sqlUpdate);
			$flag = "true";
		}
		if($flag == "true"){
			echo "1";
		}
		else{
			echo "0";
		}
	}
	
	if($operation == "show"){
		$sqlSelect="SELECT value FROM configuration";
		$resultSelect=mysqli_query($conn,$sqlSelect);
		$rows = array();
		while($r = mysqli_fetch_assoc($resultSelect)) {
			$rows[] = $r;
		}
	   print json_encode($rows);		
	}
	
	if ($operation == "getConfigurationDetails") {

	    $sqlSelect = "SELECT * FROM configuration";

	    $result = mysqli_query($conn, $sqlSelect);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "getChequeDetails") {

	    $sqlSelect = "SELECT * FROM cheque_setting WHERE status = 'A'";

	    $result = mysqli_query($conn, $sqlSelect);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
?>