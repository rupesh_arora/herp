<?php
/*File Name  :   bed.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page manages AND add beds*/

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

/*include config file*/
include 'config.php';

$operation   = "";
$wardId      = "";
$roomId      = "";
$bedNumber   = "";
$cost        = "";
$price       = "";
$description = "";
$ward        = "";

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

/*operation to show wards*/
if ($operation == "showWard") {
    if (isset($_POST['departmentId'])) {
        $departmentId = $_POST['departmentId'];
    }

    $query  = "SELECT id,CONCAT(name, '  ','(',ward_type,')') AS name FROM wards WHERE status='A' AND department = '".$departmentId."' ORDER BY name";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

/*operation to show rooms*/
if ($operation == "showRoom") {
    if (isset($_POST['ward'])) {
        $ward = $_POST['ward'];
    }
    $query  = "SELECT ward_room.id, ward_room.number AS room_number FROM ward_room
        WHERE ward_room.ward_id = '" . $ward . "' AND ward_room.status='A' ORDER BY ward_room.number ";

    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

/*operation to save data*/
if ($operation == "save") {
    if (isset($_POST['wardId'])) {
        $wardId = $_POST['wardId'];
    }
    if (isset($_POST['roomId'])) {
        $roomId = $_POST['roomId'];
    }
    if (isset($_POST['bedNumber'])) {
        $bedNumber = $_POST['bedNumber'];
    }
    if (isset($_POST['cost'])) {
        $cost = $_POST['cost'];
    }
    if (isset($_POST['price'])) {
        $price = $_POST['price'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    if (isset($_POST['departmentId'])) {
        $departmentId = $_POST['departmentId'];
    }
    
    /*Check that bed already exist for that particular bed number, room AND ward*/
    $sqlSELECT    = "SELECT number FROM beds WHERE number='" . $bedNumber . "' AND room_id='" . $roomId . "' AND ward_id='" . $wardId . "'";
    $resultSELECT = mysqli_query($conn, $sqlSELECT);
    $rows_count   = mysqli_num_rows($resultSELECT);
    
    if ($rows_count <= 0) { //if no
        $sql    = "INSERT INTO beds(department_id,ward_id,room_id,number,cost,price,description,created_on,created_by,updated_on,updated_by) VALUES('" . $departmentId . "','" . $wardId . "','" . $roomId . "','" . $bedNumber . "','" . $cost . "','" . $price . "','" . $description . "',UNIX_TIMESTAMP(),'".$userId."',UNIX_TIMESTAMP(),'".$userId."')";
        $result = mysqli_query($conn, $sql);
        echo $result;
    } else {
        echo "0";
    }
}

/*operation to show active data*/
if ($operation == "show") {
    
    $query        = "SELECT b.id,b.department_id,d.department, r.number AS roomName, b.ward_id, CONCAT(w.name, '  ','(',w.ward_type,')') AS name,b.room_id, b.number,b.cost,b.price, b.description FROM beds AS b 
        LEFT JOIN wards AS w ON b.ward_id=w.id 
        LEFT JOIN ward_room AS r ON b.room_id=r.id  
        LEFT JOIN department AS d ON d.id = b.department_id 
        WHERE b.status='A' AND w.status='A' AND r.status='A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/*operation to show inactive data*/
if ($operation == "showInActive") {
    $query        = "SELECT b.id,b.department_id,d.department, r.number AS roomName, b.ward_id, CONCAT(w.name, '  ','(',w.ward_type,')') AS name,b.room_id, b.number,b.cost,b.price, b.description FROM beds AS b 
        LEFT JOIN wards AS w ON b.ward_id=w.id 
        LEFT JOIN ward_room AS r ON b.room_id=r.id  
        LEFT JOIN department AS d ON d.id = b.department_id   
		WHERE b.status='I' Or w.status='I' Or r.status='I'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/*operation to delete data*/
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE beds SET status= 'I' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
	if($result){
		echo "1";
	}else{
		echo "0";
	}  
}

/*operation to restore data*/
if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    if (isset($_POST['wardId'])) {
        $wardId = $_POST['wardId'];
    }
    if (isset($_POST['roomId'])) {
        $roomId = $_POST['roomId'];
    }
    
    $sql    = "UPDATE beds SET status= 'A' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    
    /*Fetch status from ward room and wards for that particular room id and ward id if status is active then only restore*/
    if ($result == 1) {
        $query      = "SELECT status FROM ward_room WHERE id='" . $roomId . "' AND status='A'";
        $result     = mysqli_query($conn, $query);
        $count_room = mysqli_num_rows($result);
        
        $query_ward  = "SELECT status FROM wards WHERE id='" . $wardId . "' AND status='A'";
        $result_ward = mysqli_query($conn, $query_ward);
        $count_ward  = mysqli_num_rows($result_ward);
        
        if ($count_room == '1' && $count_ward == '1') {
            echo "Restored Successfully!!!";
        } else {
            echo "It can not be restore!!!";
        }
    }
}

/*operation to update data*/
if ($operation == "update") {
    if (isset($_POST['wardId'])) {
        $wardId = $_POST['wardId'];
    }
    if (isset($_POST['roomId'])) {
        $roomId = $_POST['roomId'];
    }
    if (isset($_POST['bedNumber'])) {
        $bedNumber = $_POST['bedNumber'];
    }
    if (isset($_POST['cost'])) {
        $cost = $_POST['cost'];
    }
    if (isset($_POST['price'])) {
        $price = $_POST['price'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    if (isset($_POST['departmentId'])) {
        $departmentId = $_POST['departmentId'];
    }
    
    /*Check that bed already exist for that particular bed number, roomid AND wardid*/
    $sqlSELECT    = "SELECT number FROM beds WHERE number='" . $bedNumber . "'AND room_id='" . $roomId . "' AND ward_id='" . $wardId . "' AND id != '$id'";
    $resultSELECT = mysqli_query($conn, $sqlSELECT);
    $rows_count   = mysqli_num_rows($resultSELECT);
    
    if ($rows_count == 0) {
        $sql    = "UPDATE beds SET department_id = '" . $departmentId . "',ward_id= '" . $wardId . "',room_id= '" . $roomId . "',number= '" . $bedNumber . "',cost= '" . $cost . "',price= '" . $price . "',description='" . $description . "',created_by='".$userId."',updated_by='".$userId."' WHERE  id = '" . $id . "'";
        $result = mysqli_query($conn, $sql);
        echo $result;
    } else {
        echo "0";
    }
}
?>