<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "saveChequeSetting") {

		$paperWidth = $_POST['paperWidth'];
		$paperHeight = $_POST['paperHeight'];
		$dateTop = $_POST['dateTop'];
		$dateLeft = $_POST['dateLeft'];
		$payeeTop = $_POST['payeeTop'];
		$payeeLeft = $_POST['payeeLeft'];
		$payeeWidth = $_POST['payeeWidth'];
		$payeePrefix = $_POST['payeePrefix'];
		$payeeSuffix = $_POST['payeeSuffix'];
		$amountWordWidth = $_POST['amountWordWidth'];
		$amountWordHeight = $_POST['amountWordHeight'];
		$amountWordTop = $_POST['amountWordTop'];
		$amountWordLeft = $_POST['amountWordLeft'];
		$maxDigits = $_POST['maxDigits'];
		$NullPrefix = $_POST['NullPrefix'];
		$amountFigureTop = $_POST['amountFigureTop'];
		$amountFigureLeft = $_POST['amountFigureLeft'];
		$amountFigureWidth = $_POST['amountFigureWidth'];
		$amountFigurePrefix = $_POST['amountFigurePrefix'];
		$amountFigureSuffix = $_POST['amountFigureSuffix'];
		$fontSize = $_POST['fontSize'];

		$selChequeSetting = "SELECT id FROM cheque_setting";
		$checkChequeSetting = mysqli_query($conn,$selChequeSetting);
		$countChequeSetting = mysqli_num_rows($checkChequeSetting);

		if ($countChequeSetting == "1") {

			$query = "INSERT INTO cheque_setting(paper_width,paper_height,date_top,date_left,payee_top,payee_left,payee_width,payee_prefix,payee_suffix,amount_word_width,amount_word_height,amount_word_top,amount_word_left,max_no_of_digit,prefix_for_null_value,amount_figure_top,amount_figure_left,amount_figure_width,amount_figure_prefix,amount_figure_suffix,font_size,created_by,updated_by,created_on,updated_on) VALUES('".$paperWidth."','".$paperHeight."','".$dateTop."','".$dateLeft."','".$payeeTop."','".$payeeLeft."','".$payeeWidth."','".$payeePrefix."','".$payeeSuffix."','".$amountWordWidth."','".$amountWordHeight."','".$amountWordTop."','".$amountWordLeft."','".$maxDigits."','".$NullPrefix."','".$amountFigureTop."','".$amountFigureLeft."','".$amountFigureWidth."','".$amountFigurePrefix."','".$amountFigureSuffix."','".$fontSize."','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";


			$result = mysqli_query($conn, $query);
			echo $result;
		}
		else {
			$query = "UPDATE cheque_setting SET paper_width='".$paperWidth."',paper_height='".$paperHeight."',date_top='".$dateTop."',date_left='".$dateLeft."',payee_top='".$payeeTop."',payee_left='".$payeeLeft."',payee_width='".$payeeWidth."',payee_prefix='".$payeePrefix."',payee_suffix='".$payeeSuffix."',amount_word_width='".$amountWordWidth."',amount_word_height='".$amountWordHeight."',amount_word_top='".$amountWordTop."',amount_word_left='".$amountWordLeft."',max_no_of_digit='".$maxDigits."',prefix_for_null_value='".$NullPrefix."',amount_figure_top='".$amountFigureTop."',amount_figure_left='".$amountFigureLeft."',amount_figure_width='".$amountFigureWidth."',amount_figure_prefix='".$amountFigurePrefix."',amount_figure_suffix='".$amountFigureSuffix."',font_size='".$fontSize."',updated_by='".$userId."',updated_on= UNIX_TIMESTAMP() WHERE restore_default ='0'" ;
			$result = mysqli_query($conn, $query);
			echo $result;
		}

		$selResetDefault = "SELECT count(c.id) AS count,c.id FROM cheque_setting AS c
							WHERE exists
							(SELECT null FROM cheque_setting AS c1
							 WHERE c.paper_width = c1.paper_width AND c.id != c1.id)";
		$checkResetDefault = mysqli_query($conn,$selResetDefault);

		
		$rowsCount = array();
		while ($r = mysqli_fetch_assoc($checkResetDefault)) {
			$rowsCount = $r['count'];
		}
		/*echo $rowsCount;*/

		if ($rowsCount == "2") {
			$resetDefaultQuery = "UPDATE cheque_setting
								   SET status = CASE restore_default 
								                      WHEN '1' THEN 'A' 
								                      WHEN '0' THEN 'I' 
								                      ELSE status
								                      END
								 WHERE restore_default IN('1', '0');";
			$result = mysqli_query($conn, $resetDefaultQuery);
			echo $result;
		}
		else{
			$resetDefaultQuery = "UPDATE cheque_setting
								   SET status = CASE restore_default 
								                      WHEN '1' THEN 'I' 
								                      WHEN '0' THEN 'A' 
								                      ELSE status
								                      END
								 WHERE restore_default IN('1', '0');";
			$result = mysqli_query($conn, $resetDefaultQuery);
			echo $result;
		}


	}
	if($operation == "showDefaultValues") {

		$query = "SELECT paper_width,paper_height,date_top,date_left,payee_top,payee_left,payee_width,payee_prefix,payee_suffix,amount_word_width,amount_word_height,amount_word_top,amount_word_left,max_no_of_digit,prefix_for_null_value,amount_figure_top,amount_figure_left,amount_figure_width,amount_figure_prefix,amount_figure_suffix,font_size FROM cheque_setting WHERE restore_default = '1'";
		$result = mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while ($r = mysqli_fetch_assoc($result)) {
			$rows = $r;
		}
		print json_encode($rows);

		

	}
?>