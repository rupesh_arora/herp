<?php
/*File Name  :   leave_apply.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page manages AND add beds*/

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
} else {
    exit();
}

$entitlements = 0;
$count_taken = 0;
$count_schedule = 0;
$count_approval = 0;

/*include config file*/
include 'config.php';

$headers = '';
/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

/*operation to show leave type*/
if ($operation == "showBindType") {
    
    $query  = "SELECT leave_type.* FROM leave_type WHERE leave_type.status='A' ORDER BY leave_type.name";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
  
}

if ($operation == "showHoliday") {
    
    $query  = "SELECT date FROM holidays WHERE status = 'A'";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

/*operation to show leave type*/
if ($operation == "showLeaveBalance") {
    $selectQuery  = "SELECT staff_type_id from users where id = '" . $userId . "'";
    $executeQuery = mysqli_query($conn, $selectQuery);
    while ($r = mysqli_fetch_assoc($executeQuery)) {
        $staffId = $r['staff_type_id'];
    }
    
    $value = $_POST['value'];
    
    $query = "SELECT COUNT(CASE WHEN staff_leaves.leave_status = 'taken' 
        AND staff_leaves.leave_type_id = '" . $value . "' AND staff_leaves.staff_id = '" . $userId . "'  THEN staff_leaves.staff_id END) as count_taken,
        COUNT(CASE WHEN staff_leaves.leave_status = 'schedule' AND
        staff_leaves.leave_type_id = '" . $value . "'  AND staff_leaves.staff_id = '" . $userId . "' THEN staff_leaves.staff_id END) as count_schedule,
        COUNT(CASE WHEN staff_leaves.leave_status = 'approval' AND
        staff_leaves.leave_type_id = '" . $value . "' AND staff_leaves.staff_id = '" . $userId . "'  THEN staff_leaves.staff_id END) as count_approval,
        entitlements.entitlements FROM entitlements 
        LEFT JOIN leave_period ON leave_period.id = entitlements.leave_period_id AND leave_period.period_status = '1'
        LEFT JOIN staff_leaves on entitlements.staff_type_id  = '" . $staffId . "' AND 
        entitlements.leave_type_id = '" . $value . "' AND entitlements.leave_period_id = leave_period.id
        
        WHERE (DATE_FORMAT(FROM_UNIXTIME(staff_leaves.leave_date), '%Y-%m-%d') between
        (select leave_period.from_date FROM leave_period where leave_period.period_status = '1') 
        AND (select leave_period.to_date FROM leave_period where leave_period.period_status = '1')
        AND staff_leaves.leave_type_id = '" . $value . "') OR
        (entitlements.leave_type_id = ".$value." AND entitlements.staff_type_id  = '" . $staffId . "' AND
         entitlements.leave_period_id in (select leave_period.id FROM leave_period where leave_period.period_status = '1'))";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

/*operation to show leave type*/
if ($operation == "checkAppointmentDate") {
    
    $fromDate = $_POST['fromDate'];
    $toDate   = $_POST['toDate'];
    $staffId  = $userId;
    
    $datetime1 = new DateTime($fromDate);
    $datetime2 = new DateTime($toDate);
    $interval  = $datetime1->diff($datetime2);
    
    $dateArray = array();
    
    $timeDifference = $interval->format('%a');
    for ($i = 0; $i < $timeDifference; $i++) {
        
        $selectQuery = "select * from appointments where consultant_id = '" . $userId . "' 
         and start_date_time BETWEEN '" . $fromDate . " 00:00:00' AND '" . $fromDate . " 23:59:59' and
         end_date_time BETWEEN '" . $fromDate . " 00:00:00' AND '" . $fromDate . " 23:59:59'";
        $excuteQuery = mysqli_query($conn, $selectQuery);
        $rowcount    = mysqli_num_rows($excuteQuery);
        if ($rowcount > 0) {
            array_push($dateArray, $fromDate);
        }
        $date     = date('Y-m-d', strtotime("+1 day", strtotime($fromDate)));
        $fromDate = $date;
    }
    echo json_encode($dateArray);
}

/*operation to show leave type*/
if ($operation == "save") {
    
    $leaveType    = $_POST['leaveType'];
    //$leaveBalance = $_POST['leaveBalance'];
    $fromDate     = $_POST['fromDate'];
    $toDate       = $_POST['toDate'];
    $report       = $_POST['report'];
    //$staffId      = $userId;
    $checked      = $_POST['checked'];
    $diff      = $_POST['diff'];
    $value = $_POST['value'];


    //Check for they have booked on OT or not

    $selectOTDATE = "SELECT patient_ot_registration.operation_date FROM patient_ot_registration 
    LEFT JOIN ot_staff_details ON ot_staff_details.patient_operation_id = 
    patient_ot_registration.id
    WHERE patient_ot_registration.status = 'A' AND ot_staff_details.ot_staff_id ='".$userId."'
    AND (patient_ot_registration.operation_date  BETWEEN '".$fromDate."' AND  '".$toDate."')";

    $resultOTDATE       = mysqli_query($conn, $selectOTDATE);
    $countOTDate = mysqli_num_rows($resultOTDATE);
    if ($countOTDate > 0) {
        
        while ($rows = mysqli_fetch_assoc($resultOTDATE)) {
            $OTDate = $rows['operation_date'];
        }
        echo "You have scheduled a operation on ".$OTDate;
        exit();
    }
        

    $selectQuery  = "SELECT staff_type_id from users where id = '" . $userId . "'";
    $executeQuery = mysqli_query($conn, $selectQuery);
    while ($r = mysqli_fetch_assoc($executeQuery)) {
        $staffId = $r['staff_type_id'];
    }    

    $query = "SELECT COUNT(CASE WHEN staff_leaves.leave_status = 'taken' 
        AND staff_leaves.leave_type_id = '" . $value . "'  AND staff_leaves.staff_id = '" . $userId . "'  THEN staff_leaves.staff_id END) as count_taken,
        COUNT(CASE WHEN staff_leaves.leave_status = 'schedule' AND
        staff_leaves.leave_type_id = '" . $value . "'   AND staff_leaves.staff_id = '" . $userId . "'  THEN staff_leaves.staff_id END) as count_schedule,
        COUNT(CASE WHEN staff_leaves.leave_status = 'approval' AND
        staff_leaves.leave_type_id = '" . $value . "' AND staff_leaves.staff_id = '" . $userId . "'  THEN staff_leaves.staff_id END) as count_approval,
        entitlements.entitlements FROM entitlements 
        LEFT JOIN leave_period ON leave_period.id = entitlements.leave_period_id AND leave_period.period_status = '1'
        LEFT JOIN staff_leaves on entitlements.staff_type_id  = '" . $staffId . "' AND 
        entitlements.leave_type_id = '" . $value . "' AND entitlements.leave_period_id = leave_period.id
        
        WHERE (DATE_FORMAT(FROM_UNIXTIME(staff_leaves.leave_date), '%Y-%m-%d') between
        (select leave_period.from_date FROM leave_period where leave_period.period_status = '1') 
        AND (select leave_period.to_date FROM leave_period where leave_period.period_status = '1')
        AND staff_leaves.leave_type_id = '" . $value . "') OR
        (entitlements.leave_type_id = ".$value." AND entitlements.staff_type_id  = '" . $staffId . "' AND
         entitlements.leave_period_id in (select leave_period.id FROM leave_period where leave_period.period_status = '1'))";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $entitlements = $r['entitlements'];
        $count_taken = $r['count_taken'];
        $count_schedule = $r['count_schedule'];
        $count_approval = $r['count_approval'];
    }

    $leaveBalance = $entitlements - ($count_taken + $count_schedule + $count_approval);
    
    $selectCheck = "SELECT * from staff_leaves where staff_id = '" . $userId . "'  AND 
    DATE_FORMAT(FROM_UNIXTIME(leave_date), '%Y-%m-%d') BETWEEN '" . $fromDate . "' AND '" . $toDate . "'";
    $excuteQuery = mysqli_query($conn, $selectCheck);
    $rowCount    = mysqli_num_rows($excuteQuery);
    if ($rowCount == 0) {
        // get date from holidays
        $selectHolidayDate = "SELECT date from holidays WHERE status = 'A'";
        $excuteHolidayDate = mysqli_query($conn, $selectHolidayDate);
        $rowsHolidayDate   = array();
        while ($r = mysqli_fetch_row($excuteHolidayDate)) {
            array_push($rowsHolidayDate, $r[0]);
        }

        $start = new DateTime($fromDate);
        $end = new DateTime($toDate);
        $days = $start->diff($end, true)->days;

        // find week end day of perticular staff
        $sqlSelectDays = "SELECT (CASE WHEN (`day` = '0') THEN 'Monday' 
            WHEN (`day` = '1') THEN 'Tuesday' 
            WHEN (`day` = '2') THEN 'Wednesday' 
            WHEN (`day` = '3') THEN 'Thursday' 
            WHEN (`day` = '4') THEN 'Friday' 
            WHEN (`day` = '5') THEN 'Saturday' 
            WHEN (`day` = '6') THEN 'Sunday' 
            END) AS staff_days ,day from staff_working_days_schedule
            WHERE is_available = 'notAvailable' AND staff_id = '" . $userId . "'";
        
        $excuteQueryDays = mysqli_query($conn, $sqlSelectDays);
        $rows            = array();
        $weekend = 0; 
        $j = 0;
        while ($r = mysqli_fetch_row($excuteQueryDays)) {
            array_push($rows, $r[0]);
            $day =  $r[1]+1;
            $weekend = $weekend + intval($days / $day) + ($start->format('N') + $days % $day >= $day);
            //echo $j++;
        }
        $totalLeave =($diff+1)-$weekend;
        $balanceLeave = (int)$leaveBalance;
        if($totalLeave > $balanceLeave){
            echo 'you have not sufficient leave';
            exit();
        }
        
        //print_r($rowsHolidayDate);
        //exit;
        $datetime1 = new DateTime($fromDate);
        $datetime2 = new DateTime($toDate);
        $interval  = $datetime1->diff($datetime2); // calculate day interval
        
        $timeDifference = $interval->format('%a');
        for ($i = 0; $i <= $timeDifference; $i++) {
            if ($i == 0) {
                $timestamp = strtotime($fromDate);
                $day       = date('l', $timestamp);
                
                
                if (((in_array($day, $rows) || in_array($fromDate, $rowsHolidayDate)) && ($checked == '0')) || (in_array($fromDate, $rowsHolidayDate) && $checked == '1')) {
                } else {
                    // update satus of appointments
                    $updateQuery        = "UPDATE appointments SET status = 'Hold On' where 
                    start_date_time BETWEEN '" . $fromDate . " 00:00:00' AND '" . $fromDate . " 23:59:59' and
                    end_date_time BETWEEN '" . $fromDate . " 00:00:00' AND '" . $fromDate . " 23:59:59'";
                    $executeUpdateQuery = mysqli_query($conn, $updateQuery);
                    
                    
                    // save staff leave 
                    $insertQuery = "INSERT INTO staff_leaves(staff_id,leave_type_id,leave_reason,leave_date,leave_status,
                    created_on,updated_on,created_by,updated_by) VALUES ('" . $userId . "','" . $leaveType . "','" . $report . "','" . strtotime($fromDate) . "'
                    ,'schedule',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'" . $userId . "','" . $userId . "')";
            

                    $excuteQuery = mysqli_query($conn, $insertQuery);

                    $last_id="SELECT id as last_id FROM `staff_leaves` ORDER BY id DESC LIMIT 0 , 1";
                    $resultlast_id = mysqli_query($conn,$last_id);
						$totalrecords = mysqli_num_rows($resultlast_id);
						$rowslast_id = array();
						while ($rlast_id = mysqli_fetch_assoc($resultlast_id)) {
							$rowslast_id = $rlast_id;
							$lastId = $rowslast_id['last_id'];
						}

                    if($excuteQuery == '1'){
				    	$selQuery = "SELECT lt.name,u.email_id,concat(u.first_name,' ',u.last_name) AS staff_name,
                        DATE_FORMAT(FROM_UNIXTIME(sl.created_on), '%Y-%m-%d') AS staff_leave_date,se.apply_leave_email,
                        sl.staff_id,sl.leave_type_id,DATE_FORMAT(FROM_UNIXTIME(sl.leave_date), '%Y-%m-%d') AS leave_date FROM sms_email AS se,
                        staff_leaves AS sl LEFT JOIN users AS u ON u.id=sl.staff_id
                        LEFT JOIN leave_type AS lt ON lt.id=sl.leave_type_id WHERE sl.id='" . $lastId . "'";
				    	$resultCheck = mysqli_query($conn,$selQuery);
						$totalrecords = mysqli_num_rows($resultCheck);
						$rows = array();
                        $leave_type;
                        $staff_name;
                        $applyMessage;
                        $leave_date;
                        $date;
                        $adminEmail;
                        $admin_name;
						while ($r = mysqli_fetch_assoc($resultCheck)) {
							$rows = $r;
							//print_r($rows);
							$leave_type = $rows['name'];
							$staff_name = $rows['staff_name'];
							$applyMessage = $rows['apply_leave_email'];
                            $leave_date = $rows['leave_date'];
							$date = $rows['staff_leave_date'];
						}
						$admin_mail = "SELECT CONCAT(u.first_name,' ',u.last_name) AS admin_name,u.email_id FROM users AS u 
                                        WHERE u.id = 1";
						$resultAdminEmail = mysqli_query($conn,$admin_mail);
						$totalrecordsAdminEmail = mysqli_num_rows($resultAdminEmail);
						$rowsAdminEmail = array();
						while ($rAdminEmail = mysqli_fetch_assoc($resultAdminEmail)) {
							$rowsAdminEmail = $rAdminEmail;
							//print_r($rowsAdminEmail);
							$adminEmail = $rowsAdminEmail['email_id'];
							$admin_name = $rowsAdminEmail['admin_name'];
						}
						$previousMessage  = $applyMessage;
						$replace = array("@@staff_name@@", "@@admin_name@@", "@@date@@",);
						$replaced   = array($staff_name, $admin_name, $leave_date);
						$message = str_replace($replace, $replaced, $previousMessage);
						//print_r($message);
                        $headers = '';
                        if ($adminEmail) {
                            mail($adminEmail, " Leave Approval for ".$staff_name."", $message, $headers);
                            $sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
                                        created_on,updated_on,created_by,updated_by,msg_status) VALUES('Apply Leave','',
                                        '".$adminEmail."','','".$message."','".$date."',UNIX_TIMESTAMP(),
                                        UNIX_TIMESTAMP(),'".$userId."','".$userId."','Mail')";

                            $result = mysqli_query($conn, $sqlQuery);
                            //echo $result;
                        }
						
				   	}
                    
                }
            } else {
                $date      = date('Y-m-d', strtotime("+1 day", strtotime($fromDate)));
                $fromDate  = $date;
                $timestamp = strtotime($fromDate);
                $day       = date('l', $timestamp); // find day on the given date
                
                // this will check whether that day is exit in array or not.
                if (((in_array($day, $rows) || in_array($fromDate, $rowsHolidayDate)) && ($checked == '0')) || 
                    (in_array($fromDate, $rowsHolidayDate) && $checked == '1')){
                } else {
                    // update satus of appointments
                    $updateQuery        = "UPDATE appointments SET status = 'Hold On' where 
                    start_date_time BETWEEN '" . $fromDate . " 00:00:00' AND '" . $fromDate . " 23:59:59' and
                    end_date_time BETWEEN '" . $fromDate . " 00:00:00' AND '" . $fromDate . " 23:59:59'";
                    $executeUpdateQuery = mysqli_query($conn, $updateQuery);
                    
                    
                    // save staff leave
                    $insertQuery = "INSERT INTO staff_leaves(staff_id,leave_type_id,leave_reason,leave_date,leave_status,
                    created_on,updated_on,created_by,updated_by) VALUES ('" . $userId . "','" . $leaveType . "','" . $report . "','" . strtotime($date) . "'
                    ,'schedule',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'" . $userId . "','" . $userId . "')";
                    $excuteQuery = mysqli_query($conn, $insertQuery);

                    $last_id="SELECT id as last_id FROM `staff_leaves` ORDER BY id DESC LIMIT 0 , 1";
                    $resultlast_id = mysqli_query($conn,$last_id);
						$totalrecords = mysqli_num_rows($resultlast_id);
						$rowslast_id = array();
						while ($rlast_id = mysqli_fetch_assoc($resultlast_id)) {
							$rowslast_id = $rlast_id;
							$lastId = $rowslast_id['last_id'];
						}

                    if($excuteQuery == '1'){
                        $selQuery = "SELECT lt.name,u.email_id,concat(u.first_name,' ',u.last_name) AS staff_name,
                        DATE_FORMAT(FROM_UNIXTIME(sl.created_on), '%Y-%m-%d') AS staff_leave_date,se.apply_leave_email,
                        sl.staff_id,sl.leave_type_id,from_unixtime(sl.leave_date) AS leave_date FROM sms_email AS se,
                        staff_leaves AS sl LEFT JOIN users AS u ON u.id=sl.staff_id
                        LEFT JOIN leave_type AS lt ON lt.id=sl.leave_type_id WHERE sl.id='" . $lastId . "'";
                        $resultCheck = mysqli_query($conn,$selQuery);
                        $totalrecords = mysqli_num_rows($resultCheck);
                        $rows = array();
                        $leave_type;
                        $staff_name;
                        $applyMessage;
                        $leave_date;
                        $date;
                        $adminEmail;
                        $admin_name;
                        while ($r = mysqli_fetch_assoc($resultCheck)) {
                            $rows = $r;
                            //print_r($rows);
                            $leave_type = $rows['name'];
                            $staff_name = $rows['staff_name'];
                            $applyMessage = $rows['apply_leave_email'];
                            $leave_date = $rows['leave_date'];
                            $date = $rows['staff_leave_date'];
                        }
                        $admin_mail = "SELECT CONCAT(u.first_name,' ',u.last_name) AS admin_name,u.email_id FROM users AS u 
                                        WHERE u.id = 1";
                        $resultAdminEmail = mysqli_query($conn,$admin_mail);
                        $totalrecordsAdminEmail = mysqli_num_rows($resultAdminEmail);
                        $rowsAdminEmail = array();
                        while ($rAdminEmail = mysqli_fetch_assoc($resultAdminEmail)) {
                            $rowsAdminEmail = $rAdminEmail;
                            //print_r($rowsAdminEmail);
                            $adminEmail = $rowsAdminEmail['email_id'];
                            $admin_name = $rowsAdminEmail['admin_name'];
                        }
                        $previousMessage  = $applyMessage;
                        $replace = array("@@staff_name@@", "@@admin_name@@", "@@date@@",);
                        $replaced   = array($staff_name, $admin_name, $leave_date);
                        $message = str_replace($replace, $replaced, $previousMessage);
                        //print_r($message);
                        $headers = '';
                        if ($adminEmail) {
                            mail($adminEmail, " Leave Approval for ".$staff_name."", $message, $headers);
                            $sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
                                        created_on,updated_on,created_by,updated_by,msg_status) VALUES('Apply Leave','',
                                        '".$adminEmail."','','".$message."','".$date."',UNIX_TIMESTAMP(),
                                        UNIX_TIMESTAMP(),'".$userId."','".$userId."','Mail')";

                            $result = mysqli_query($conn, $sqlQuery);
                            //echo $result;
                        }
                        
                    }
                }
            }
        }
        if ($excuteQuery) {
            echo "1";
        }
    } else {
        echo "Leave already taken";
    }
}
?>