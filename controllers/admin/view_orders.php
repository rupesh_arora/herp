<?php
/*File Name  :   lab_test_request.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page manages  all the lab test request*/

session_start();

/*include config file*/
include 'config.php';

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
}
/*Get user id from session table*/
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

/*Operation to serach order result*/
if ($operation == "searchOrderDetails") {
	
	if (isset($_POST['sellerId'])) {
        $sellerId = $_POST["sellerId"];
    }
    if (isset($_POST['date'])) {
        $date = $_POST["date"];
    }
    if (isset($_POST['orderId'])) {
        $orderId = $_POST["orderId"];
    }
    if (isset($_POST['item'])) {
        $item = $_POST["item"];
    }

    $isFirst = "false";

    /*Check if isset for receive order pop up if isset then show only pending order*/
    if (isset($_POST['showPendingOrderOnly'])) {

        $query = "SELECT orders.id,seller.name as seller_name, DATE_FORMAT(FROM_UNIXTIME(orders.date), '%Y-%m-%d') as order_date, orders.`status` as order_status,`order-item`.order_id ,
            items.name as item_name FROM orders
            LEFT JOIN seller ON seller.id = orders.seller_id
            LEFT JOIN `order-item` ON `order-item`.order_id = orders.id
            LEFT JOIN items ON items.id = `order-item`.item_id WHERE orders.status ='pending' ";

        if ($sellerId != '') {
            $query .= "  AND orders.seller_id  = '" . $sellerId . "'";
        }

        if ($date != '') {
            $query .= "  AND orders.date  BETWEEN UNIX_TIMESTAMP('".$date." 00:00:00') AND UNIX_TIMESTAMP('".$date." 23:59:59')";
        }

        if ($orderId != '') {
            $query .= " AND orders.id  =  '" . $orderId . "'";
        }

        if ($item != '') {
            $query .= " AND items.name LIKE '%" . $item . "%'";
        }
    }

    else if (isset($_POST['inventoryPaymentHdnId'])) {

        $query = "SELECT orders.id,seller.name as seller_name, DATE_FORMAT(FROM_UNIXTIME(orders.date), '%Y-%m-%d') as order_date, orders.`status` as order_status,`order-item`.order_id ,
            items.name as item_name FROM orders
            LEFT JOIN seller ON seller.id = orders.seller_id
            LEFT JOIN `order-item` ON `order-item`.order_id = orders.id
            LEFT JOIN items ON items.id = `order-item`.item_id
            WHERE orders.id NOT IN (SELECT order_id FROM inventory_payment) ";

        if ($sellerId != '') {
            $query .= "  AND orders.seller_id  = '" . $sellerId . "'";
        }

        if ($date != '') {
            $query .= "  AND orders.date  BETWEEN UNIX_TIMESTAMP('".$date." 00:00:00') AND UNIX_TIMESTAMP('".$date." 23:59:59')";
        }

        if ($orderId != '') {
            $query .= " AND orders.id  =  '" . $orderId . "'";
        }

        if ($item != '') {
            $query .= " AND items.name LIKE '%" . $item . "%'";
        }
    }

    else if(isset($_POST['showReceivedOrderOnly'])){

        $query = "SELECT orders.id,seller.name as seller_name, DATE_FORMAT(FROM_UNIXTIME(orders.date), '%Y-%m-%d') as order_date, orders.`status` as order_status,`order-item`.order_id ,
            items.name as item_name FROM orders
            LEFT JOIN seller ON seller.id = orders.seller_id
            LEFT JOIN `order-item` ON `order-item`.order_id = orders.id
            LEFT JOIN items ON items.id = `order-item`.item_id 
            WHERE orders.id IN(SELECT received_orders.order_id FROM received_orders) ";

        if ($sellerId != '') {
            $query .= "  AND orders.seller_id  = '" . $sellerId . "'";
        }

        if ($date != '') {
            $query .= "  AND orders.date  BETWEEN UNIX_TIMESTAMP('".$date." 00:00:00') AND UNIX_TIMESTAMP('".$date." 23:59:59')";
        }

        if ($orderId != '') {
            $query .= " AND orders.id  =  '" . $orderId . "'";
        }

        if ($item != '') {
            $query .= " AND items.name LIKE '%" . $item . "%'";
        }       
    }

    else{
        $query = "SELECT orders.id,seller.name as seller_name, DATE_FORMAT(FROM_UNIXTIME(orders.date), '%Y-%m-%d') as order_date, orders.`status` as order_status,`order-item`.order_id ,
        	items.name as item_name FROM orders
    		LEFT JOIN seller ON seller.id = orders.seller_id
    		LEFT JOIN `order-item` ON `order-item`.order_id = orders.id
    		LEFT JOIN items ON items.id = `order-item`.item_id ";

    	if ($item != '' || $orderId != '' || $date != '' || $sellerId != '') {
            $query .= " WHERE ";
        }

    	if ($sellerId != '') {
            if ($isFirst == "true") {
                $query .= " AND ";
            }
            $query .= " orders.seller_id  = '" . $sellerId . "'";
            $isFirst = "true";
        }

        if ($date != '') {
            if ($isFirst == "true") {
                $query .= " AND ";
            }
            $query .= "orders.date  BETWEEN UNIX_TIMESTAMP('".$date." 00:00:00') AND UNIX_TIMESTAMP('".$date." 23:59:59')";
            $isFirst = "true";
        }

        if ($orderId != '') {
            if ($isFirst == "true") {
                $query .= " AND ";
            }
            $query .= "orders.id  =  '" . $orderId . "'";
            $isFirst = "true";
        }

        if ($item != '') {
            if ($isFirst == "true") {
                $query .= " AND ";
            }
            $query .= "items.name LIKE '%" . $item . "%'";
            $isFirst = "true";
        }
    }
	   
    $query .="GROUP BY orders.id";
	//echo $query;
    $result = mysqli_query($conn, $query);
    $rowCount = mysqli_num_rows($result);
    if ($rowCount > 0) {
       $rows   = array();
        while ($r = mysqli_fetch_assoc($result)) {
            $rows[] = $r;
        }
        print json_encode($rows); 
    }
    else{
        echo "0";
    }
        
}

if ($operation == "particularOrderDetail") {

	if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }

    $query = "SELECT seller.name as seller_name, DATE_FORMAT(FROM_UNIXTIME(orders.date), '%Y-%m-%d') as order_date, `order-item`.`status` as order_status,`order-item`.order_id ,items.name as item_name,
    	`order-item`.ordered_quantity AS quantity  FROM orders
		LEFT JOIN seller ON seller.id = orders.seller_id
		LEFT JOIN `order-item` ON `order-item`.order_id = orders.id
		LEFT JOIN items ON items.id = `order-item`.item_id 
		WHERE orders.id = '".$id."' ";

	//echo $query;
	$result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "showSeller") {

	$query = "SELECT * FROM seller WHERE status = 'A'";
	$result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
?>