<?php
	
	session_start();
	if(!session_id()){
		exit();
	}
	$toDate = '';
	$fromDate = '';
	
	include_once('config.php');

	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET['operation'])){
		$operation=$_GET["operation"];
	}

	if ($operation=="showChartData") {
		if (isset($_POST['dataLoad'])) {
			$dataLoad = $_POST["dataLoad"];
		}
		if (isset($_POST['fromDate'])) {
			$fromDate = $_POST["fromDate"];
		}
		if (isset($_POST['toDate'])) {
			$toDate = $_POST["toDate"];
		}

		if ($dataLoad =="all") {

			$sqlBatchWiseItem = "SELECT COUNT(id) `Item Stock`,batch_no As Batch from item_stock 
				WHERE item_stock.quantity > 0 GROUP BY item_stock.batch_no";
		}
		
		else if ($dataLoad =="last30days") {

			$sqlBatchWiseItem = "SELECT COUNT(id) `Item Stock`,batch_no As Batch from item_stock 
				WHERE item_stock.quantity > 0 AND created_on >= unix_timestamp(curdate() -
				interval 1 month) GROUP BY item_stock.batch_no";
		}


		else if ($dataLoad =="last7days") {

			$sqlBatchWiseItem = "SELECT COUNT(id) `Item Stock`,batch_no As Batch from item_stock 
				WHERE item_stock.quantity > 0 AND created_on >= unix_timestamp(curdate() -
				interval 7 day) GROUP BY item_stock.batch_no";
		}


		else if ($dataLoad =="today") {	
			
			$sqlBatchWiseItem = "SELECT COUNT(id) `Item Stock`,batch_no As Batch from item_stock 
				WHERE item_stock.quantity > 0 AND created_on >= unix_timestamp(curdate()) 
				GROUP BY item_stock.batch_no";
		}
		else  {

			$sqlBatchWiseItem = "SELECT COUNT(id) `Item Stock`,batch_no As Batch from item_stock 
				WHERE item_stock.quantity > 0 AND created_on BETWEEN UNIX_TIMESTAMP('".$fromDate."
				00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59') GROUP BY item_stock.batch_no";
		}		
		
		$rowsBatchWiseItem  = array();
	    $resultBatchWiseItem  = mysqli_query($conn,$sqlBatchWiseItem);
	    while ($r = mysqli_fetch_assoc($resultBatchWiseItem)) {
	        $rowsBatchWiseItem[] = $r;
	    }


	    echo json_encode($rowsBatchWiseItem);
	}
?>