<?php
/*****************************************************************************************************
 * File Name    :   view_patient_history.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page manages patient history data
 ****************************************************************************************************/
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	$operation = "";	
	$patientId = "";
	
	include 'config.php';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	
	//This operation is used for select the unit 
	if($operation == "show")   
	{
		if (isset($_POST['patientId'])) {
			$patientId=$_POST['patientId'];
		}
		 $patientPrefix = $_POST['patientPrefix'];
		$sql       = "SELECT value FROM configuration WHERE name = 'patient_prefix'";
		$result    = mysqli_query($conn, $sql);
		
		while ($r = mysqli_fetch_assoc($result)) {
			$prefix = $r['value'];
		}
		if($prefix == $patientPrefix){
			
			 $query="SELECT p.first_name, p.last_name, p.mobile, p.dob,v.id,v.created_on 
				,v.id AS visit_id,v_t.`type`,CONCAT(u.first_name,' ',u.last_name) AS consultant_name,
				(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visit_prefix  from  visits AS  v
				LEFT JOIN patients AS p ON v.patient_id = p.id
				LEFT JOIN visit_type AS v_t ON v_t.id = v.visit_type 
				LEFT JOIN users AS u ON u.id = v.consultant_id  
				where p.id='".$patientId."' AND v_t.`type` != 'Self Request'";
	  
			$result=mysqli_query($conn,$query);
			$rows = array();
			while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
			}
			print json_encode($rows);
		}
	}
	
	
?>