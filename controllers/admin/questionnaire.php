<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';

	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}

	if($operation == "save"){
		if (isset($_POST['bigquestionnaire'])) {
		   $bigquestionnaire = json_decode($_POST['bigquestionnaire']);
		}
		$patientId = $_POST['patientId'];
		$visitId = $_POST['visitId'];
		$tblName = $_POST['tblName'];
		
		$insertQuery = "INSERT INTO patients_questionnaire (patient_id,visit_id,question,answer,created_by) ";
		$insertQuery .= "VALUES ";
		$counter = 0;
		foreach($bigquestionnaire as $value) {
			
			$question = "";
		    $answer = "";
			foreach($value as $key => $val) {
				 if ($key == 0) {
					if($val == "Please click for add Question"){
						$question="";
					}else{
						$question = $val;
					}
				}
				if ($key == 1) {
					if($val == "Please click for add Answer"){
						$answer="";
					}else{
						$answer = $val;
					}
				}
				$counter++;			
			}
			if($counter != 2){
				$insertQuery .= ",";
			}
			if($question =="" || $answer == ""){
				echo "";
			}	
			else{
				$insertQuery .= "(".$patientId.",".$visitId.",'".$question."','".$answer."','".$_SESSION['globaluser']."')";			
			}
		}		
		$result= mysqli_query($conn,$insertQuery); 
		if($result){
			echo $result;
		}
		else{
			echo "";
		}
	}
?>