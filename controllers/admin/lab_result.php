<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}

	if($operation == "loadSpecimen"){
		if (isset($_POST['specimenId'])) {
			$id=$_POST["specimenId"];
		}
		//$requestId = $_POST["requestId"];
		$sql = "SELECT lab_test_requests.patient_id,lab_test_requests.visit_id,lab_tests.test_code,
			lab_tests.name, lab_tests.id, l.name AS component_name,l.normal_range,lab_test_requests.cost,
			specimens_types.type,patient_specimen.description,patient_specimen.created_on,lab_test_result.result_flag,
			lab_test_result.report,l.id AS component_id  
  
  		FROM patient_specimen
		LEFT JOIN `lab_test_requests`  ON lab_test_requests.id = patient_specimen.lab_test_id
		LEFT JOIN lab_tests ON lab_test_requests.lab_test_id = lab_tests.id 
		LEFT JOIN lab_test_request_component ON lab_test_request_component.lab_test_request_id = lab_test_requests.id 
		LEFT JOIN lab_test_component AS l ON l.id = lab_test_request_component.lab_component_id
		LEFT JOIN specimens_types ON specimens_types.id = lab_tests.specimen_id LEFT JOIN patients On 
		patients.id = lab_test_requests.patient_id 
		LEFT JOIN lab_test_result ON lab_test_result.specimen_id = patient_specimen.id
		WHERE patient_specimen.status = 'A' AND patient_specimen.id = '".$id."' AND lab_test_request_component.pay_status = 'paid' AND
		lab_test_requests.test_status = 'done'";
		
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		echo json_encode($rows);			
	}
	if($operation == "save"){
		
		if (isset($_POST['specimenId'])) {
			$specimenId=$_POST['specimenId'];
		}
		if (isset($_POST['report'])) {
			$report=$_POST['report'];
		}
		
		if (isset($_POST['tableData'])) {
			$tableData=json_decode($_POST["tableData"]);
			$first = "false";
			foreach($tableData as $value) {			
				
				$result = $value-> result;
				$range = $value-> normalRange;
				$componentId = $value-> componentId;
				
				$sql_select = "SELECT COUNT(*) from lab_test_result where specimen_id =".$specimenId."";  
				$result_select = mysqli_query($conn,$sql_select);  
				while ($row=mysqli_fetch_row($result_select))
				{
					$count = $row[0];
				}
				
				if($count == 0 || $first == "true")
				{			
					$sql = "INSERT INTO `lab_test_result`(`specimen_id`,`lab_test_component_id`,`report`,`normal_range`,`result`,`created_on`, `updated_on`, `created_by`, `updated_by`)
					 VALUES('".$specimenId."','".$componentId."','".$report."','".$range."', '".$result."',UNIX_TIMESTAMP(), UNIX_TIMESTAMP(), '".$_SESSION['globaluser']."', '".$_SESSION['globaluser']."')";		
					
					$result= mysqli_query($conn,$sql);	
					 if($result){
						$sqlupdate = "update `lab_test_requests` SET `pay_status`='settled',`test_status`='resultsent' where id = 
						(select patient_specimen.lab_test_id from patient_specimen 
						Left Join lab_test_result on patient_specimen.id = lab_test_result.specimen_id 
						where lab_test_result.specimen_id = '".$specimenId."')";
						mysqli_query($conn,$sqlupdate);
					} 					
					$first = "true";
					echo $result;
				}
				else{

					$sql = "UPDATE `lab_test_result` SET 
					`lab_test_component_id` = '".$componentId."', 
					`report`='".$report."',`normal_range`='".$range."',
					`result`='".$result."',`updated_on`= UNIX_TIMESTAMP(),
					`updated_by`='".$_SESSION['globaluser']."' WHERE specimen_id=".$specimenId."";			
					$result= mysqli_query($conn,$sql);			
					echo $result;
				}
			}				
		}			
	}
?>