<?php

    /*File Name  :   scheme_exclusion.php
    Company Name :   Qexon Infotech
    Created By   :   Tushar Gupta
    Created Date :   23th march, 2016
    Description  :   This page manges Insurance details*/
	session_start();	

    if(isset($_SESSION['globaluser'])){
        $userId = $_SESSION['globaluser'];//store global user value in variable
    }
    else{
    	exit();
    }

    /*include config file*/
	include 'config.php';
    $operation = "";

    /*checking operation set or not*/
	if (isset($_POST['operation'])) {
	    $operation = $_POST["operation"];
	} else if (isset($_GET["operation"])) {
	    $operation = $_GET["operation"];
	}

	/*operation to show scheme name*/
	if ($operation == "showSchemeName") {
	    if (isset($_POST['companyId'])) {
	        $companyId = $_POST['companyId'];
	    }

	    if (isset($_POST['insuranceCompany'])) {

	        $insuranceCompany = $_POST["insuranceCompany"];
	        $patientId = $_POST['patientId'];
	        $query = "SELECT DISTINCT(insurance_plan.id),insurance_plan.plan_name FROM insurance
	        LEFT JOIN insurance_plan ON insurance_plan.id = insurance.insurance_plan_id
	        WHERE insurance.status= 'A' AND insurance.expiration_date >=CURDATE()
	        AND insurance.insurance_company_id = '".$insuranceCompany."' 
	        AND insurance.patient_id ='".$patientId."'";
	    }
	    else{
	    	$query  = "SELECT id,plan_name FROM insurance_plan WHERE company_name_id = '" . $companyId . "' AND status = 'A' ORDER BY plan_name";
	    }	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	
	/*operation to show Plan name*/
	if ($operation == "showPlanName") {
	    if (isset($_POST['schemeId'])) {
	        $schemeId = $_POST['schemeId'];
	    }

	    if (isset($_POST['insuranceScheme'])) {

	        $insuranceScheme = $_POST["insuranceScheme"];
	        $patientId = $_POST['patientId'];

	        $query = "SELECT DISTINCT(scheme_plans.id),scheme_plans.scheme_plan_name FROM insurance
	        LEFT JOIN scheme_plans ON scheme_plans.id = insurance.insurance_plan_name_id
	        WHERE insurance.status= 'A' AND insurance.insurance_plan_id = '".$insuranceScheme."' 
	        AND insurance.patient_id ='".$patientId."' AND scheme_plans.end_date >= CURDATE() AND insurance.expiration_date >=CURDATE()";
	    }
	    else{
	    	$query  = "SELECT id,scheme_plan_name FROM scheme_plans WHERE 
	    	insurance_plan_id = '" . $schemeId . "' AND status = 'A' AND scheme_plans.end_date >= CURDATE()
	    	ORDER BY scheme_plan_name";
	    }
	    	
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	
	
    /*operation to show service type*/
	if ($operation == "showServiceDetails") {
		if (isset($_POST['insuranceCompnayId'])) {
	        $companyId = $_POST['insuranceCompnayId'];
	    }
	    if (isset($_POST['serviceNameId'])) {
	       $schemeId  = $_POST['serviceNameId'];
	    }
	    if (isset($_POST['planNameId'])) {
			$planId = $_POST['planNameId'];
	    }
		$type = $_POST['type'];
		$finalArray = array();
		$insQuery = "SELECT item_id FROM scheme_exclusion 
					WHERE status = 'A'  AND insurance_company_id ='" . $companyId . "'   
					AND scheme_plan_id ='" . $schemeId . "'AND scheme_name_id ='" . $planId . "' 
					AND `service_type` = '".$type."'";
		$insResult = mysqli_query($conn, $insQuery);
		$rows1   = array();
	    while ($r = mysqli_fetch_assoc($insResult)) {
	        $rows1[] = $r;
	    }
		
	    $query  = "SELECT id,name FROM services WHERE status = 'A'  ORDER BY name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
		
		array_push($finalArray,$rows1,$rows);
		//PRINT_R($finalArray);
	    print json_encode($finalArray);
	}

    /*operation to show lab type*/
	if ($operation == "showLabDetails") {
		if (isset($_POST['insuranceCompnayId'])) {
	        $companyId = $_POST['insuranceCompnayId'];
	    }
	    if (isset($_POST['serviceNameId'])) {
	       $schemeId  = $_POST['serviceNameId'];
	    }
	    if (isset($_POST['planNameId'])) {
			$planId = $_POST['planNameId'];
	    }
		$type = $_POST['type'];
		$finalArray = array();
		$insQuery = "SELECT item_id FROM scheme_exclusion 
					WHERE status = 'A'  AND insurance_company_id ='" . $companyId . "'   
					AND scheme_plan_id ='" . $schemeId . "'AND scheme_name_id ='" . $planId . "' 
					AND `service_type` = '".$type."'";
		$insResult = mysqli_query($conn, $insQuery);
		$rows1   = array();
	    while ($r = mysqli_fetch_assoc($insResult)) {
	        $rows1[] = $r;
	    }
		
	    $query  = "SELECT id,name FROM lab_test_component ORDER BY name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    array_push($finalArray,$rows1,$rows);
		//PRINT_R($finalArray);
	    print json_encode($finalArray);
	}

    /*operation to show procedure*/
	if ($operation == "showProcedureDetails") {
		if (isset($_POST['insuranceCompnayId'])) {
	        $companyId = $_POST['insuranceCompnayId'];
	    }
	    if (isset($_POST['serviceNameId'])) {
	       $schemeId  = $_POST['serviceNameId'];
	    }
	    if (isset($_POST['planNameId'])) {
			$planId = $_POST['planNameId'];
	    }
		$type = $_POST['type'];
		$finalArray = array();
		$insQuery = "SELECT item_id FROM scheme_exclusion 
					WHERE status = 'A'  AND insurance_company_id ='" . $companyId . "'   
					AND scheme_plan_id ='" . $schemeId . "'AND scheme_name_id ='" . $planId . "' 
					AND `service_type` = '".$type."'";
		$insResult = mysqli_query($conn, $insQuery);
		$rows1   = array();
	    while ($r = mysqli_fetch_assoc($insResult)) {
	        $rows1[] = $r;
	    }
		
	    $query  = "SELECT id,CONCAT(name, '  ','(',code,')') AS name, price FROM procedures WHERE status='A' ORDER BY name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    array_push($finalArray,$rows1,$rows);
		//PRINT_R($finalArray);
	    print json_encode($finalArray);
	}

    /*operation to show radiology test*/
	if ($operation == "showRadiologyDetails") {
		if (isset($_POST['insuranceCompnayId'])) {
	        $companyId = $_POST['insuranceCompnayId'];
	    }
	    if (isset($_POST['serviceNameId'])) {
	       $schemeId  = $_POST['serviceNameId'];
	    }
	    if (isset($_POST['planNameId'])) {
			$planId = $_POST['planNameId'];
	    }
		$type = $_POST['type'];
		$finalArray = array();
		$insQuery = "SELECT item_id FROM scheme_exclusion 
					WHERE status = 'A'  AND insurance_company_id ='" . $companyId . "'   
					AND scheme_plan_id ='" . $schemeId . "'AND scheme_name_id ='" . $planId . "' 
					AND `service_type` = '".$type."'";
		$insResult = mysqli_query($conn, $insQuery);
		$rows1   = array();
	    while ($r = mysqli_fetch_assoc($insResult)) {
	        $rows1[] = $r;
	    }
	    $query  = "SELECT id,name,fee FROM radiology_tests WHERE status = 'A' ORDER BY name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    array_push($finalArray,$rows1,$rows);
		//PRINT_R($finalArray);
	    print json_encode($finalArray);
	}

    /*operation to show forensic test*/
	if ($operation == "showForensicDetails") {
		if (isset($_POST['insuranceCompnayId'])) {
	        $companyId = $_POST['insuranceCompnayId'];
	    }
	    if (isset($_POST['serviceNameId'])) {
	       $schemeId  = $_POST['serviceNameId'];
	    }
	    if (isset($_POST['planNameId'])) {
			$planId = $_POST['planNameId'];
	    }
		$type = $_POST['type'];
		$finalArray = array();
		$insQuery = "SELECT item_id FROM scheme_exclusion 
					WHERE status = 'A'  AND insurance_company_id ='" . $companyId . "'   
					AND scheme_plan_id ='" . $schemeId . "'AND scheme_name_id ='" . $planId . "' 
					AND `service_type` = '".$type."'";
		$insResult = mysqli_query($conn, $insQuery);
		$rows1   = array();
	    while ($r = mysqli_fetch_assoc($insResult)) {
	        $rows1[] = $r;
	    }
	    $query  = "SELECT id,name FROM forensic_tests WHERE status = 'A' ORDER BY name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    array_push($finalArray,$rows1,$rows);
		//PRINT_R($finalArray);
	    print json_encode($finalArray);
	}

	/*operation to show drug*/
	if ($operation == "showPharmacyDetails") {
		if (isset($_POST['insuranceCompnayId'])) {
	        $companyId = $_POST['insuranceCompnayId'];
	    }
	    if (isset($_POST['serviceNameId'])) {
	       $schemeId  = $_POST['serviceNameId'];
	    }
	    if (isset($_POST['planNameId'])) {
			$planId = $_POST['planNameId'];
	    }
		$type = $_POST['type'];
		$finalArray = array();
		$insQuery = "SELECT item_id FROM scheme_exclusion 
					WHERE status = 'A'  AND insurance_company_id ='" . $companyId . "'   
					AND scheme_plan_id ='" . $schemeId . "'AND scheme_name_id ='" . $planId . "' 
					AND `service_type` = '".$type."'";
		$insResult = mysqli_query($conn, $insQuery);
		$rows1   = array();
	    while ($r = mysqli_fetch_assoc($insResult)) {
	        $rows1[] = $r;
	    }
	    $query  = "SELECT id,name FROM drugs WHERE status = 'A' ORDER BY name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    array_push($finalArray,$rows1,$rows);
		//PRINT_R($finalArray);
	    print json_encode($finalArray);
	}

    /*operation to save whole data*/
	if ($operation == "saveDataTableData") {		
	    if (isset($_POST['data'])) {
	        $data = json_decode($_POST['data']);
	    }
		if (isset($_POST['insuranceCompnayId'])) {
	        $insuranceCompnayId = $_POST['insuranceCompnayId'];
	    }
	    if (isset($_POST['serviceNameId'])) {
	        $serviceNameId = $_POST['serviceNameId'];
	    }
	    if (isset($_POST['planNameId'])) {
	        $planNameId = $_POST['planNameId'];
	    }
	    if (isset($_POST['saveServiceTypeName'])) {
	        $saveServiceTypeName = $_POST['saveServiceTypeName'];
	    }
		
		$queryDelete = "DELETE FROM scheme_exclusion WHERE insurance_company_id = '".$insuranceCompnayId."' AND scheme_plan_id = '".$serviceNameId."' AND
		scheme_name_id = '".$planNameId."' AND service_type = '".$saveServiceTypeName."'";
		$delResult = mysqli_query($conn,$queryDelete);
		
	    $insertQry = "INSERT INTO scheme_exclusion(insurance_company_id,scheme_plan_id,scheme_name_id,item_name,item_id,service_type,created_on,created_by) VALUES";

	    $subQuery         = '';
	    $counter          = 0;

	    foreach ($data as $value) {
	    	$itemName = '';
	    	$itemId = '';

	    	foreach ($value as $key => $val) {

	    		if ($key == 0) { //at index zero get the item name
					$itemName = $val;
				}
				if ($key == 2) { //at index three get the item id
					$itemId = $val;
				}								
	    	}
	    	if ($counter > 0) {
				$subQuery .= ",";
			}
	    	$subQuery .= "('".$insuranceCompnayId."','".$serviceNameId."','".$planNameId."',
				'".$itemName."','".$itemId."','".$saveServiceTypeName."',UNIX_TIMESTAMP(),
				'".$userId."')";

			$counter++;
	    }
	    $completeInsertQry = $insertQry . $subQuery . ";";
	    $result = mysqli_query($conn,$completeInsertQry);
	    if ($result == '') {
	    	echo $delResult;
	    }
	    else{
	    	echo $result;
	    }
	}
?>