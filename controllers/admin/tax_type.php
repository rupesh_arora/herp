<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "save") {
		$taxName = $_POST['taxName'];
		$taxRate = $_POST['taxRate'];

		$checkTaxName = "SELECT tax_name FROM tax_type WHERE status = 'A' AND tax_name = '".$taxName."'";
		$resultCheckTaxName = mysqli_query($conn,$checkTaxName);
		$countTaxName = mysqli_num_rows($resultCheckTaxName);
		if ($countTaxName == "0") {

			$sql = "INSERT INTO tax_type (tax_name,tax_rate,created_on,updated_on,created_by,updated_by) VALUES('".$taxName."','".$taxRate."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."',
				'".$userId."')";
			    
		    $result = mysqli_query($conn, $sql);
	    	echo $result;
		}
		else{
			echo "0";
		}
	}

	if ($operation == "show") { // show active data
    
	    $sql = "SELECT id,tax_name,tax_rate FROM tax_type WHERE `status`= 'A'";
	    $result = mysqli_query($conn, $sql);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	if ($operation == "checked") {
    
	    $query = "SELECT id,tax_name,tax_rate FROM tax_type WHERE `status`= 'I'";
	    
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	if ($operation == "update") {// update data
	
	    
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    if (isset($_POST['taxName'])) {
	        $taxName = $_POST['taxName'];
	    }
	    if (isset($_POST['taxRate'])) {
	        $taxRate = $_POST['taxRate'];
	    }	
		
	    $id = $_POST['id'];
		
		$selTaxName = "SELECT tax_name FROM tax_type WHERE tax_name ='".$taxName."' AND status = 'A' 
		AND id !='".$id."'";
		$checktaxName = mysqli_query($conn,$selTaxName);
		$countTaxName = mysqli_num_rows($checktaxName);
		if ($countTaxName < 1) {
		$sql    = "UPDATE tax_type set tax_name = '".$taxName."',tax_rate= '".$taxRate."',updated_on=				UNIX_TIMESTAMP(),updated_by = '".$userId."' where id = '".$id."' ";

			$result = mysqli_query($conn, $sql);
			echo $result;
		}
		else {
			echo "0";
		}	
	}

	if ($operation == "restore") // for restore    
    {
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    $taxName = $_POST['taxName'];

	    $selTaxName = "SELECT tax_name FROM tax_type WHERE tax_name ='".$taxName."' AND status = 'A' 
		AND id !='".$id."'";
		$checktaxName = mysqli_query($conn,$selTaxName);
		$countTaxName = mysqli_num_rows($checktaxName);
		if ($countTaxName < 1) {
			$sql    = "UPDATE tax_type SET status= 'A'  WHERE  id = '" . $id . "'";
		    $result = mysqli_query($conn, $sql);
		    echo $result;
		}
		else{
			echo "0";
		} 
	}
	if ($operation == "delete") {
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    
	    $select = "SELECT tax_id FROM product_taxes WHERE tax_id = '" . $id . "' AND status= 'A'";
	    $resultCheck = mysqli_query($conn, $select);
	    $countRows = mysqli_num_rows($resultCheck);
	    if($countRows == 0){
	    	$sql    = "UPDATE tax_type SET status= 'I' where id = '" . $id . "'";
		    $result = mysqli_query($conn, $sql);
		    echo $result;
	    }
	    else{
	    	echo "0";
	    }
	}
?>