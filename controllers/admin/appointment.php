<?php
/*
 * File Name    :   appointment.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   19-02-2016
 * Description  :   This page use for load and save data into database.
 */

$operation        = "";
$date             = new DateTime();

$userId = "";
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
    //$email = $_SESSION['globalEmail'];
}
else{
    exit();
}
include 'config.php'; // import database connection file
include '../AfricasTalkingGateway.php';
$headers = "";

$operation = $_POST['operation']; // load opearion value from js

	if ($operation == "save" || $operation == "update") { // save details 		
	   
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }

	    if (isset($_POST['patientId'])) {
	        $patientId = $_POST['patientId'];
	    }
	   
	    if (isset($_POST['notes'])) {
	        $notes = $_POST['notes'];
	    }
	    if (isset($_POST['visitType'])) {
	        $visitType = $_POST['visitType'];
	    }
	   
	    if (isset($_POST['date'])) {
	        $date = $_POST['date'];
			$leaveDate = $date;
	    }
	    if (isset($_POST['fromTime'])) {
	        $fromTime = $_POST['fromTime'];
	    }
	    if (isset($_POST['toTime'])) {
	        $toTime = $_POST['toTime'];
	    }

	    if (isset($_POST['consultantId'])) {
	        $consultantId = $_POST['consultantId'];
	        if($consultantId == "-2"){
	        	$consultantId = $_SESSION['globaluser'];
	        }
	    }
		$patient_id = $_POST['patient_id'];
		$hospitalName = $_POST['hospitalName'];
		$hospitalPhoneNo = $_POST['hospitalPhoneNo'];
		
	    $date = date_create($date);
		$newDate= date_format($date, 'Y-m-d');
	    $startTimeDate = $newDate.' '.$fromTime;
	    $endTimeDate = $newDate.' '.$toTime;

	    $newDate = explode('-', $newDate);
		$month = $newDate[1];
		$day   = $newDate[2];
		$year  = $newDate[0];

		$dateSchedule = $year.'-'.$month.'-'.$day;

	    $jd = cal_to_jd(CAL_GREGORIAN,date($month),date($day),date($year));
		$day = (jddayofweek($jd,1));
		if($day == "Monday"){
			$day = "0";
		}
		else if($day == "Tuesday"){
			$day = "1";
		}
		else if($day == "Wednesday"){
			$day = "2";
		}
		else if($day == "Thursday"){
			$day = "3";
		}
		else if($day == "Friday"){
			$day = "4";
		}
		else if($day == "Saturday"){
			$day = "5";
		}
		else if($day == "Sunday"){
			$day = "6";
		}
		$dayRows ="0";
		$email ="";
		$staffName ="";
		$patientEmail ="";
		$patientName ="";
		
		$selectStaffEmail = "SELECT email_id,CONCAT(salutation,' ',first_name,' ',last_name) AS Name FROM users WHERE id = '".$consultantId ."'";
		$resultStaffEmail = mysqli_query($conn,$selectStaffEmail);
		while($row = mysqli_fetch_row($resultStaffEmail)) {
			$email = $row[0];
			$staffName = $row[1];
		}
		
		$selectPatientEmail = "SELECT email,CONCAT(salutation,' ',first_name,' ',last_name) AS Name FROM patients WHERE id = '".$patientId ."'";
		$resultPatientEmail = mysqli_query($conn,$selectPatientEmail);
		while($row = mysqli_fetch_row($resultPatientEmail)) {
			$patientEmail = $row[0];
			$patientName = $row[1];
		}
		
		if($operation == "save"){	
			$selectCheck = "SELECT * from staff_leaves where staff_id = '".$consultantId."' AND
			DATE_FORMAT(FROM_UNIXTIME(leave_date), '%Y-%m-%d') = '".$leaveDate."' AND leave_status = 'approval'";
			$excuteQuery = mysqli_query($conn,$selectCheck);
			$rowCount = mysqli_num_rows($excuteQuery);
			if($rowCount == 0) {
				$sqlPatientCheck = "select * from appointments where patient_id = '". $patientId."'
				AND start_date_time BETWEEN '".$leaveDate." 00:00:00' AND '".$leaveDate." 23:59:59' AND
				end_date_time BETWEEN '".$leaveDate." 00:00:00' AND '".$leaveDate." 23:59:59'";
				$excutePatientQuery = mysqli_query($conn,$sqlPatientCheck);
				$rowPatientCount = mysqli_num_rows($excutePatientQuery);
				
				if($rowPatientCount == 0) {
					$sql = "INSERT INTO appointments(consultant_id,patient_id,visit_type_id,notes,start_date_time,end_date_time) 
						VALUES('" . $consultantId . "','". $patientId."','" . $visitType . "','".$notes."','".$startTimeDate . "',
						'" . $endTimeDate . "')";
					
					// mail to staff
					
					 
					/*if(isset($patientEmail)) {
						// mail to patient
						$message = "Dear ".$patientName.",

						Your appointment with ".$staffName." has been scheduled on date at time ".$startTimeDate.". 
						Please visit the hospital atleast 15 mins before your scheduled time. 

						Thank you! 

						".$hospitalName." 
						".$hospitalPhoneNo."";
						
						mail($patientEmail, "Appointment With ".$staffName."", $message, $headers);
					}*/
					
					
					$result = mysqli_query($conn, $sql);
					if($result == '1'){
                    	$selQuery = "SELECT CONCAT(p.first_name,' ',p.last_name) AS patient_name,p.mobile AS mobile_no,p.country_code,CONCAT(u.first_name,' ',u.last_name) AS consultant_name,sms_email.id,sms_email.appointment_sms,sms_email.appointment_email,sl.id,sl.patient_id,sl.start_date_time FROM sms_email,appointments AS sl
							LEFT JOIN patients AS p ON p.id = sl.patient_id
							LEFT JOIN users AS u ON u.id = sl.consultant_id WHERE consultant_id = '" . $consultantId . "' AND patient_id = '" . $patientId . "'";
                    	$resultCheck = mysqli_query($conn,$selQuery);
						$totalrecords = mysqli_num_rows($resultCheck);
						$rows = array();
						while ($r = mysqli_fetch_assoc($resultCheck)) {
							$rows = $r;
							//print_r($rows);
							$sms_message = $rows['appointment_sms'];
							$email_message = $rows['appointment_email'];
							$patient_name = $rows['patient_name'];
							$consultant_name = $rows['consultant_name'];
							$start_date_time = $rows['start_date_time'];
							$mobile_no = $rows['mobile_no'];
							$countryCode = $rows['country_code'];
						}

						$previousSmsMessage  = $sms_message;
						$replaceSms = array("@@patient_name@@", "@@date@@","@@user_name@@");
						$replacedSms   = array($patient_name, $start_date_time, $consultant_name);
						$smsMessage = str_replace($replaceSms, $replacedSms, $previousSmsMessage);
						//print_r($smsMessage);

						$previousEmailMessage  = $email_message;
						$replaceEmail = array("@@patient_name@@", "@@date@@","@@user_name@@");
						$replacedEmail   = array($patient_name, $start_date_time, $consultant_name);
						$emailMessage = str_replace($replaceEmail, $replacedEmail, $previousEmailMessage);

						if ($patientEmail) {
							mail($patientEmail, "Appointment With ".$staffName."", $emailMessage, $headers);
							$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
			  						created_on,updated_on,created_by,updated_by,msg_status) VALUES('Scheduled Appointment','',
			  						'".$patientEmail."','',
			  						'".$emailMessage."','".$start_date_time."',UNIX_TIMESTAMP(),
			  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','Mail')";
									$result = mysqli_query($conn, $sqlQuery);
									echo $result;
						}
						// sms to patient
						$recipients = $countryCode.$mobile_no;

						// And of course we want our recipients to know what we really do
						$message    = $smsMessage;
						// Create a new instance of our awesome gateway class
						$gateway    = new AfricasTalkingGateway($username, $apikey);
						// Any gateway error will be captured by our custom Exception class below, 
						// so wrap the call in a try-catch block
						try 
						{ 
						  // Thats it, hit send and we'll take care of the rest. 
						  $results = $gateway->sendMessage($recipients, $message);
						          
						   if($results){
						  	$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
						  						created_on,updated_on,created_by,updated_by,msg_status) VALUES('Scheduled Appointment',
						  						'".$mobile_no."','','".$message."','','".$start_date_time."',UNIX_TIMESTAMP(),
						  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS')";
								$result = mysqli_query($conn, $sqlQuery);
								echo $result;
						  }     
						  /*foreach($results as $result) {
						    // status is either "Success" or "error message"
						    //echo " Number: " .$result->number;
						    //echo " Status: " .$result->status;
						    //echo " MessageId: " .$result->messageId;
						    //echo " Cost: "   .$result->cost."\n";
						  }*/
						}
						catch ( AfricasTalkingGatewayException $e )
						{
							$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
						  						created_on,updated_on,created_by,updated_by,msg_status,status) VALUES('Scheduled Appointment',
						  						'".$mobile_no."','','".$smsMessage."','','".$start_date_time."',UNIX_TIMESTAMP(),
						  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS','pending')";
							$result = mysqli_query($conn, $sqlQuery);
							echo $result;
						  //echo "Encountered an error while sending: ".$e->getMessage();
						}
						// DONE!!! 
				   	}
					
					//save notification details
					$sqlNotification = "INSERT INTO notification(notification_staff_id,message,created_on,created_by) 
					VALUES('" . $consultantId . "','Appointment booked of this patient Id: $patientId',UNIX_TIMESTAMP(),'".$userId."')";
					$resultNotification = mysqli_query($conn, $sqlNotification);
					
					if($result){

						echo  mysqli_insert_id($conn);
					}
				}
				else {
					echo "Already booked";
				}
			}
			else{
				echo "";
			}
		}
		else{
			$selectCheck = "SELECT * from staff_leaves where staff_id = '".$consultantId."' AND
			DATE_FORMAT(FROM_UNIXTIME(leave_date), '%Y-%m-%d') = '".$leaveDate."' AND leave_status = 'approval'";
			$excuteQuery = mysqli_query($conn,$selectCheck);
			$rowCount = mysqli_num_rows($excuteQuery);
			if($rowCount == 0) {
				$sqlPatientCheck = "select * from appointments where patient_id = '". $patientId."'
				AND start_date_time BETWEEN '".$leaveDate." 00:00:00' AND '".$leaveDate." 23:59:59' AND
				end_date_time BETWEEN '".$leaveDate." 00:00:00' AND '".$leaveDate." 23:59:59'   WHERE  id != '" . $id . "'";
				$excutePatientQuery = mysqli_query($conn,$sqlPatientCheck);
				$rowPatientCount = mysqli_num_rows($excutePatientQuery);
				
				if($rowPatientCount == 0) {
					$sql = "UPDATE appointments SET consultant_id ='" . $consultantId . "',visit_type_id = '". $visitType."',notes = '".$notes."',start_date_time = '".$startTimeDate . "',end_date_time = '" . $endTimeDate . "'  WHERE  id = '" . $id . "'";			
					// mail to staff
					mail($email, "Appointment Booked", "Your appointment with patient ".$patientName." has been scheduled on date at time. " . $startTimeDate . "", $headers);
					
					$result = mysqli_query($conn, $sql);

					if($result != 0){
						$sql ="SELECT concat(p.first_name,' ',p.last_name) AS patient_name,concat(u.first_name,' ',u.last_name) AS consultant_name,p.country_code,p.mobile AS mobile_no,p.email AS patient_email,se.app_scheduled_sms,se.app_scheduled_email,app.consultant_id,app.patient_id,app.start_date_time from sms_email AS se,appointments AS app
							LEFT JOIN users AS u on u.id = app.consultant_id
							LEFT JOIN patients AS p on p.id = app.patient_id WHERE  id = '" . $id . "'";
							$result = mysqli_query($conn,$sql);
							$totalrecords = mysqli_num_rows($result);
							$rows = array();
							$patientEmail;
							$sms_message;
							$email_message;
							$patient_name;
							$consultant_name;
							$start_date_time;
							$mobile_no;
							while ($r = mysqli_fetch_assoc($result)) {
								$rows = $r;
								$sms_message = $rows['app_scheduled_sms'];
								$email_message = $rows['app_scheduled_email'];
								$patient_name = $rows['patient_name'];
								$consultant_name = $rows['consultant_name'];
								$start_date_time = $rows['start_date_time'];
								$mobile_no = $rows['mobile_no'];
								$patientEmail = $rows['patient_email'];
								$country_code = $rows['country_code'];
							}

						// mail to patient
						$previousSmsMessage  = $sms_message;
						$replaceSms = array("@@user_name@@", "@@date@@","@@consultant_name@@");
						$replacedSms   = array($patient_name, $start_date_time, $consultant_name);
						$smsMessage = str_replace($replaceSms, $replacedSms, $previousSmsMessage);

						$previousEmailMessage  = $email_message;
						$replaceEmail = array("@@user_name@@", "@@date@@","@@consultant_name@@");
						$replacedEmail   = array($patient_name, $start_date_time, $consultant_name);
						$emailMessage = str_replace($replaceEmail, $replacedEmail, $previousEmailMessage);
						
						if ($patientEmail) {
							mail($patientEmail, "Appointment With ".$staffName."", $emailMessage, $headers);

							$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
			  						created_on,updated_on,created_by,updated_by,msg_status) VALUES('Scheduled Appointment','',
			  						'".$patientEmail."','','".$emailMessage."','".$start_date_time."',UNIX_TIMESTAMP(),
			  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','Mail')";
									$result = mysqli_query($conn, $sqlQuery);
									echo $result;
						}
						

						try 
						{ 
								// sms to patient
							$recipients = $country_code.$mobile_no;
							// And of course we want our recipients to know what we really do
							$message    = $smsMessage;
							// Create a new instance of our awesome gateway class
							$gateway    = new AfricasTalkingGateway($username, $apikey);
							// Any gateway error will be captured by our custom Exception class below, 
							// so wrap the call in a try-catch block

						  // Thats it, hit send and we'll take care of the rest. 
						  $results = $gateway->sendMessage($recipients, $message);

						  if($results){
						  	$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
						  						created_on,updated_on,created_by,updated_by,msg_status) VALUES('Scheduled Appointment',
						  						'".$mobile_no."','','".$message."','','".$start_date_time."',UNIX_TIMESTAMP(),
						  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS')";
									$result = mysqli_query($conn, $sqlQuery);
									echo $result;
						  }   
						            
						  /*foreach($results as $result) {
						    // status is either "Success" or "error message"
						    //echo " Number: " .$result->number;
						    //echo " Status: " .$result->status;
						    //echo " MessageId: " .$result->messageId;
						    //echo " Cost: "   .$result->cost."\n";
						  }*/
						}
						catch ( AfricasTalkingGatewayException $e )
						{
							$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
					  						created_on,updated_on,created_by,updated_by,msg_status,status) VALUES('Scheduled Appointment',
					  						'".$mobile_no."','','".$smsMessage."','','".$start_date_time."',UNIX_TIMESTAMP(),
					  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS','pending')";
								$result = mysqli_query($conn, $sqlQuery);
								echo $result;
						  //echo "Encountered an error while sending: ".$e->getMessage();
						}
						// DONE!!! 
					}
					
					$sqlNotification = "INSERT INTO notification(notification_staff_id,message,created_on,created_by) 
					VALUES('" . $consultantId . "','Appointment booked of this patient Id: $patientId',UNIX_TIMESTAMP(),'".$userId."')";
					$resultNotification = mysqli_query($conn, $sqlNotification);
					if($result){
						echo "1";
					}
					else{
						echo "0";
					}
				}
				else {
					echo "Already booked";
				}
			}
			else{
				echo "";
			}
		}
	}
	
	if ($operation == "selectData") { // save details
   
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }

	    $sql = "SELECT a.*,(SELECT value FROM configuration WHERE name = 'patient_prefix') AS prefix,users.staff_type_id
				FROM appointments AS a LEFT JOIN users ON users.id = a.consultant_id  
				WHERE a.id = '" . $id . "'";
	    $result = mysqli_query($conn, $sql);
		$rows = array();
		while ($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		print json_encode($rows); 
    }

    if ($operation == "delete") { // save details
   
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }

	    $sql    = "UPDATE appointments SET status = 'Cancel' WHERE  id = '" . $id . "'";
	    $result = mysqli_query($conn, $sql);
		if($result){
			echo "1";
			
			$sql ="SELECT concat(p.first_name,' ',p.last_name) AS patient_name,concat(u.first_name,' ',u.last_name) AS consultant_name,p.country_code,
			p.mobile AS mobile_no,se.app_cancel_sms,p.email AS patient_email,se.app_cancel_email,app.consultant_id,
			app.patient_id,app.start_date_time from sms_email AS se,appointments AS app 
			LEFT JOIN users AS u on u.id = app.consultant_id LEFT JOIN patients AS p on p.id = app.patient_id 
			WHERE  app.id = '" . $id . "'";
				$result = mysqli_query($conn,$sql);
				$totalrecords = mysqli_num_rows($result);
				$rows = array();
				$sms_message;
				$email_message;
				$patient_name;
				$consultant_name;
				$start_date_time;
				$mobile_no;
				$patientEmail;
				$country_code;
				while ($r = mysqli_fetch_assoc($result)) {
					$rows = $r;
					$sms_message = $rows['app_cancel_sms'];
					$email_message = $rows['app_cancel_email'];
					$patient_name = $rows['patient_name'];
					$consultant_name = $rows['consultant_name'];
					$start_date_time = $rows['start_date_time'];
					$mobile_no = $rows['mobile_no'];
					$patientEmail = $rows['patient_email'];
					$country_code = $rows['country_code'];
				}

			// mail to patient
			$previousSmsMessage  = $sms_message;
			$replaceSms = array("@@user_name@@", "@@date@@","@@consultant_name@@");
			$replacedSms   = array($patient_name, $start_date_time, $consultant_name);
			$smsMessage = str_replace($replaceSms, $replacedSms, $previousSmsMessage);
			

			$previousEmailMessage  = $email_message;
			$replaceEmail = array("@@user_name@@", "@@date@@","@@consultant_name@@");
			$replacedEmail   = array($patient_name, $start_date_time, $consultant_name);
			$emailMessage = str_replace($replaceEmail, $replacedEmail, $previousEmailMessage);
			
			
			if ($patientEmail != '' || $patientEmail != NULL) {
				mail($patientEmail, "Appointment With ".$staffName."", $emailMessage, $headers);

				$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
			  						created_on,updated_on,created_by,updated_by,msg_status) VALUES('Cancel Appointment','',
			  						'".$patientEmail."','','','".$emailMessage."','".$start_date_time."',UNIX_TIMESTAMP(),
			  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','Mail')";
						$result = mysqli_query($conn, $sqlQuery);
						echo $result;
			}
			

			try 
			{ 
				// sms to patient
				$recipients = $country_code.$mobile_no;
				// And of course we want our recipients to know what we really do
				$message    = $smsMessage;
				// Create a new instance of our awesome gateway class
				$gateway    = new AfricasTalkingGateway($username, $apikey);
				// Any gateway error will be captured by our custom Exception class below, 
				// so wrap the call in a try-catch block

			  // Thats it, hit send and we'll take care of the rest. 
			  $results = $gateway->sendMessage($recipients, $message);
			   
			  if($results){
			  	$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
			  						created_on,updated_on,created_by,updated_by,msg_status) VALUES('Cancel Appointment',
			  						'".$mobile_no."','','".$smsMessage."','','".$start_date_time."',UNIX_TIMESTAMP(),
			  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS')";
						$result = mysqli_query($conn, $sqlQuery);
						echo $result;
			  }         
			  /*foreach($results as $result) {
			    // status is either "Success" or "error message"
			    //echo " Number: " .$result->number;
			    //echo " Status: " .$result->status;
			    //echo " MessageId: " .$result->messageId;
			    //echo " Cost: "   .$result->cost."\n";
			  }*/
			}
			catch ( AfricasTalkingGatewayException $e )
			{
				$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
			  						created_on,updated_on,created_by,updated_by,msg_status,status) VALUES('Cancel Appointment',
			  						'".$mobile_no."','','".$smsMessage."','','".$start_date_time."',UNIX_TIMESTAMP(),
			  						UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS','pending')";
						$result = mysqli_query($conn, $sqlQuery);
						echo $result;
			  //echo "Encountered an error while sending: ".$e->getMessage();
			}
			// DONE!!! 
		}
		
		else {
			echo "0";
		}  
    }
	
	if($operation == "showSlots") {
		
		if (isset($_POST['consultantId'])) {
	        $consultantId = $_POST['consultantId'];
	        if($consultantId == "-2"){
	        	$consultantId = $_SESSION['globaluser'];
	        }
	    }
		
		$scheduleDate = $_POST['scheduleDate'];
		
		$newDate = explode('-', $scheduleDate);
		$month = $newDate[1];
		$day   = $newDate[2];
		$year  = $newDate[0];

		$dateSchedule = $year.'-'.$month.'-'.$day;

	    $jd = cal_to_jd(CAL_GREGORIAN,date($month),date($day),date($year));
		$day = (jddayofweek($jd,1));
		if($day == "Monday"){
			$day = "0";
		}
		else if($day == "Tuesday"){
			$day = "1";
		}
		else if($day == "Wednesday"){
			$day = "2";
		}
		else if($day == "Thursday"){
			$day = "3";
		}
		else if($day == "Friday"){
			$day = "4";
		}
		else if($day == "Saturday"){
			$day = "5";
		}
		else if($day == "Sunday"){
			$day = "6";
		}
		$dayRows ="0";
		
		$datequery = "SELECT date_schedule_slots.*,staff_date_schedule.time_duration,appointments.start_date_time,appointments.end_date_time FROM date_schedule_slots 
				LEFT JOIN staff_date_schedule ON staff_date_schedule.id = date_schedule_slots.schedule_id 			  
				LEFT JOIN appointments ON appointments.consultant_id = staff_date_schedule.staff_id  
				WHERE date_schedule_slots.schedule_id 
				IN(SELECT id FROM staff_date_schedule WHERE staff_id = '".$consultantId."' AND date = '".$dateSchedule."') order by appointments.start_date_time asc"; //Select maximum case id for this patient
			 
		$dateResult = mysqli_query($conn, $datequery);
		$dateRows = mysqli_num_rows($dateResult);	
		if($dateRows == "0"){
			$dayquery = "SELECT weekly_schedule_slots.*,staff_weekly_schedule.time_duration,appointments.start_date_time,appointments.end_date_time FROM weekly_schedule_slots 
				LEFT JOIN staff_weekly_schedule ON staff_weekly_schedule.id = weekly_schedule_slots.schedule_id 
				LEFT JOIN appointments ON appointments.consultant_id = staff_weekly_schedule.staff_id  
				WHERE schedule_id IN(SELECT id FROM staff_weekly_schedule WHERE staff_id = '".$consultantId."' AND day = '".$day."') order by appointments.start_date_time asc"; //Select maximum case id for this patient
				 
			$dayResult = mysqli_query($conn, $dayquery);
			$dayRows = mysqli_num_rows($dayResult);	
			$row = array();
			while($r = mysqli_fetch_assoc($dayResult)){
				$row[] = $r;
			}
			echo json_encode($row);
		}
		else{
			$row = array();
			while($r = mysqli_fetch_assoc($dateResult)){
				$row[] = $r;
			}
			echo json_encode($row);
		}
	}
?>