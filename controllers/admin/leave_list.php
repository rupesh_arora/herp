<?php
/*File Name  :   bed.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page manages AND add beds*/

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

/*include config file*/
include 'config.php';
include '../AfricasTalkingGateway.php';
$headers = '';

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

/*operation to show leave type*/
if ($operation == "showLeaveList") {
    
	$fromDate = $_POST['fromDate'];
	$toDate = $_POST['toDate'];
	$checkRejected = $_POST['checkRejected'];
	$checkCancelled = $_POST['checkCancelled'];
	$checkApproval = $_POST['checkApproval'];
	$checkScheduled = $_POST['checkScheduled'];
	$checkTaken = $_POST['checkTaken'];
	$isFirst = "false";


	/*If staff type is set then data is coming and going for admin leave list*/
	if (isset($_POST['staffTypeid'])) {
		$staffTypeId = $_POST['staffTypeid'];

		$query = "SELECT staff_leaves.id,
		DATE_FORMAT(FROM_UNIXTIME(staff_leaves.leave_date), '%Y-%m-%d') AS date,entitlements.entitlements,
		leave_type.name,staff_leaves.leave_status,staff_leaves.leave_reason ,entitlements.staff_type_id,staff_attendance.punch_in
		from staff_leaves
		LEFT JOIN leave_type ON leave_type.id = staff_leaves.leave_type_id
		LEFT JOIN users ON users.id = staff_leaves.staff_id
		LEFT JOIN entitlements ON entitlements.staff_type_id = users.staff_type_id AND entitlements.leave_type_id = staff_leaves.leave_type_id
		LEFT JOIN staff_attendance ON staff_attendance.staff_id = users.id
		WHERE entitlements.staff_type_id = '".$staffTypeId."' AND 
		staff_leaves.leave_date BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') 
		AND UNIX_TIMESTAMP('".$toDate." 23:59:59')";
	}

	/*If staff type is not set then data is coming and going for leave list*/
	else {
		$selectQuery = "SELECT staff_type_id from users where id = '".$userId."'";
		$executeQuery = mysqli_query($conn,$selectQuery);
		while($r = mysqli_fetch_assoc($executeQuery)) {
			$staffId = $r['staff_type_id'];
		}	
		
		$query = "SELECT staff_leaves.id,DATE_FORMAT(FROM_UNIXTIME(staff_leaves.leave_date), '%Y-%m-%d') AS date,entitlements.entitlements,leave_type.name,staff_leaves.leave_status,staff_leaves.leave_reason from staff_leaves
			LEFT JOIN leave_type ON leave_type.id = staff_leaves.leave_type_id
			LEFT JOIN entitlements ON entitlements.staff_type_id = '".$staffId."' AND entitlements.leave_type_id = staff_leaves.leave_type_id
			WHERE staff_leaves.staff_id = '".$userId."' AND 
			staff_leaves.leave_date BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') 
			AND UNIX_TIMESTAMP('".$toDate." 23:59:59')";
	}	
	
	if($checkRejected != '' || $checkCancelled != '' || $checkApproval != '' || $checkScheduled != '' || $checkTaken !='') {
		$query .= " AND (";
	}
	if($checkRejected !=''){
		if ($isFirst != "false") {
			$query .= " OR ";
		}
		$query .= " staff_leaves.leave_status = '".$checkRejected."'";
		$isFirst = "true";
	}
	if($checkCancelled !=''){
		if ($isFirst != "false") {
			$query .= " OR ";
		}
		$query .= " staff_leaves.leave_status = '".$checkCancelled."'";
		$isFirst = "true";
	}
	if($checkApproval !=''){
		if ($isFirst != "false") {
			$query .= " OR ";
		}
		$query .= " staff_leaves.leave_status = '".$checkApproval."'";
		$isFirst = "true";
	}
	if($checkScheduled !=''){
		if ($isFirst != "false") {
			$query .= " OR ";
		}
		$query .= " staff_leaves.leave_status = '".$checkScheduled."'";
		$isFirst = "true";
	}
	if($checkTaken !=''){
		if ($isFirst != "false") {
			$query .= " OR ";
		}
		$query .= " staff_leaves.leave_status = '".$checkTaken."'";
		$isFirst = "true";
	}
	if($isFirst == "true"){
		$query .= ")  GROUP BY staff_leaves.id  ORDER BY staff_leaves.leave_date DESC";
	}
	else {
		$query .= ")  GROUP BY staff_leaves.id  ORDER BY staff_leaves.leave_date DESC";
	}

	//echo $query;
	$result=mysqli_query($conn,$query);
	$rows = array();
	while($r = mysqli_fetch_assoc($result)) {
	 $rows[] = $r;
	}
	print json_encode($rows);

}
/*operation to show leave type*/
if ($operation == "saveActionDetails") {
	if (isset($_POST['selArray'])) {
	   $selArray = json_decode($_POST['selArray']);
	}
	foreach($selArray as $value) {
		$val = $value->val;
		$id =  $value->id;
		$updateQuery = "UPDATE staff_leaves SET leave_status = '".$val."' WHERE id = '".$id."'";
		$queryExcute = mysqli_query($conn,$updateQuery);
	}
	if($queryExcute) {
		echo "1";
	}
	if ($conn->query($queryExcute) === TRUE) {
	    $last_id = $conn->insert_id;
	}
	if($queryExcute == '1'){
    	$selQuery = "SELECT DATE_FORMAT(FROM_UNIXTIME(sl.leave_date), '%Y-%m-%d') AS leave_date,CONCAT(u.first_name,' ',u.last_name) AS staff_name,u.country_code,
    	u.email_id,us.mobile,sl.leave_status,lt.name,sms_email.id,DATE_FORMAT(FROM_UNIXTIME(sl.updated_on), '%Y-%m-%d') AS staff_leave_date,
    	sms_email.leave_cancel_sms,sms_email.leave_approved_sms,sms_email.leave_cancel_email,sms_email.leave_approved_email FROM sms_email,staff_leaves AS sl  
    	LEFT JOIN users AS u ON u.id = sl.staff_id 
				LEFT JOIN leave_type AS lt ON lt.id = sl.staff_id
				LEFT JOIN users AS us on us.id = sl.staff_id WHERE sl.id = '".$id."'";
    	$resultCheck = mysqli_query($conn,$selQuery);
		$totalrecords = mysqli_num_rows($resultCheck);
		$rows = array();
		$staff_name;
		$mobile;
		$name;
		$id;
		$leave_cancel_sms;
		$leave_approved_sms;
		$leave_date;
		$email_id;
		$date;
		$country_code;
		while ($r = mysqli_fetch_assoc($resultCheck)) {
			$rows = $r;	
			//print_r($rows);
			$staff_name = $rows['staff_name'];
			$mobile = $rows['mobile'];
			$leave_status = $rows['leave_status'];
			$name = $rows['name'];
			$id = $rows['id'];
			$leave_cancel_sms = $rows['leave_cancel_sms'];
			$leave_approved_sms = $rows['leave_approved_sms'];
			$leave_date = $rows['leave_date'];
			$email_id = $rows['email_id'];
			$date = $rows['staff_leave_date'];
			$leave_cancel_email = $rows['leave_cancel_email'];
			$leave_approved_email = $rows['leave_approved_email'];
			$country_code = $rows['country_code'];
		}
		if($leave_status == 'cancel'){
			$previousSmsMessage  = $leave_cancel_sms;
			$replaceSms = array("@@user_name@@","@@date@@");
			$replacedSms   = array($staff_name,$leave_date);
			$smsMessage = str_replace($replaceSms, $replacedSms, $previousSmsMessage);

			$previousEmailMessage  = $leave_cancel_email;
			$replaceEmail = array("@@user_name@@","@@date@@");
			$replacedEmail   = array($staff_name,$leave_date);
			$emailMessage = str_replace($replaceEmail, $replacedEmail, $previousEmailMessage);
			//print_r($emailMessage);
			if ($email_id) {
				mail($email_id, " Leave cacelled for ".$staff_name."", $emailMessage, $headers);
				$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
                            created_on,updated_on,created_by,updated_by,msg_status) VALUES('Leave Cancel','',
                            '".$email_id."','','".$emailMessage."','".$date."',UNIX_TIMESTAMP(),
                            UNIX_TIMESTAMP(),'".$userId."','".$userId."','Mail')";

                $result = mysqli_query($conn, $sqlQuery);
			}
			
			//print_r($message);
			// sms to patient
			$recipients = $country_code.$mobile	;
			// And of course we want our recipients to know what we really do
			$message    = $smsMessage;
			// Create a new instance of our awesome gateway class
			$gateway    = new AfricasTalkingGateway($username, $apikey);
			// Any gateway error will be captured by our custom Exception class below, 
			// so wrap the call in a try-catch block
			try 
			{ 
			  // Thats it, hit send and we'll take care of the rest. 
			  $results = $gateway->sendMessage($recipients, $message);
			  
			  if($results){
                $sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
                            created_on,updated_on,created_by,updated_by,msg_status) VALUES('Leave Cancel',
                            '".$recipients."','','".$smsMessage."','','".$date."',UNIX_TIMESTAMP(),
                            UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS')";
                    $result = mysqli_query($conn, $sqlQuery);
                    //echo $result;
	            }    
			  foreach($results as $result) {
			    // status is either "Success" or "error message"
			   // echo " Number: " .$result->number;
			    //echo " Status: " .$result->status;
			    //echo " MessageId: " .$result->messageId;
			    //echo " Cost: "   .$result->cost."\n";
			  }
			}
			catch ( AfricasTalkingGatewayException $e )
			{
				$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
                            created_on,updated_on,created_by,updated_by,msg_status,status) VALUES('Leave Cancel',
                            '".$recipients."','','".$smsMessage."','','".$date."',UNIX_TIMESTAMP(),
                            UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS','pending')";
                $result = mysqli_query($conn, $sqlQuery);
			  //echo "Encountered an error while sending: ".$e->getMessage();
			}
			// DONE!!! 
	   	}
	   	else{
	   		$previousSmsMessage  = $leave_approved_sms;
			$replaceSms = array("@@user_name@@","@@date@@");
			$replacedSms   = array($staff_name,$leave_date);
			$smsMessage = str_replace($replaceSms, $replacedSms, $previousSmsMessage);

			$previousEmailMessage  = $leave_approved_email;
			$replaceEmail = array("@@user_name@@","@@date@@");
			$replacedEmail   = array($staff_name,$leave_date);
			$emailMessage = str_replace($replaceEmail, $replacedEmail, $previousEmailMessage);
			//print_r($emailMessage);
			
			
			//print_r($message);
			if ($email_id) {
				mail($email_id, " Leave approved for ".$staff_name."", $emailMessage, $headers);		// mail to staff
				$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
                            created_on,updated_on,created_by,updated_by,msg_status) VALUES('Leave Approval','',
                            '".$email_id."','','".$emailMessage."','".$date."',UNIX_TIMESTAMP(),
                            UNIX_TIMESTAMP(),'".$userId."','".$userId."','Mail')";
                	$result = mysqli_query($conn, $sqlQuery);
			}
			

			// sms to staff
			$recipients = $country_code.$mobile	;
			$message = $smsMessage	;
			// And of course we want our recipients to know what we really do
			// Create a new instance of our awesome gateway class
			$gateway    = new AfricasTalkingGateway($username, $apikey);
			// Any gateway error will be captured by our custom Exception class below, 
			// so wrap the call in a try-catch block
			try 
			{ 
			  // Thats it, hit send and we'll take care of the rest. 
			  $results = $gateway->sendMessage($recipients, $message);

			  if($results){
                $sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
                            created_on,updated_on,created_by,updated_by,msg_status) VALUES('Leave Approval',
                            '".$recipients."','','".$message."','','".$date."',UNIX_TIMESTAMP(),
                            UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS')";
	                $result = mysqli_query($conn, $sqlQuery);
	                //echo $result;
	           }   
			            
			  foreach($results as $result) {
			    // status is either "Success" or "error message"
			   // echo " Number: " .$result->number;
			    //echo " Status: " .$result->status;
			    //echo " MessageId: " .$result->messageId;
			    //echo " Cost: "   .$result->cost."\n";
			  }
			}
			catch ( AfricasTalkingGatewayException $e )
			{
				$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
                            created_on,updated_on,created_by,updated_by,msg_status,status) VALUES('Leave Approval',
                            '".$recipients."','','".$message."','','".$date."',UNIX_TIMESTAMP(),
                            UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS','pending')";
	            $result = mysqli_query($conn, $sqlQuery);
			  //echo "Encountered an error while sending: ".$e->getMessage();
			}
			// DONE!!! 
	   	}

	}
		
}

?>
