<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';

	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "showData") {
		
		$toDate = $_POST['toDate'];
		$fromDate = $_POST['fromDate'];

		if ($toDate !='' && $fromDate !='') {

			$expandDate = explode("-", $toDate);
			$toDatePrev = ($expandDate[0]-1).'-'.($expandDate[1]).'-'.($expandDate[2]);

			$expandDate = explode("-", $fromDate);
			$fromDatePrev = ($expandDate[0]-1).'-'.($expandDate[1]).'-'.($expandDate[2]);	
			
			$salesQry = "SELECT(SELECT SUM(charge) FROM patient_radiology_bill WHERE created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS `Radiology Cash`,(SELECT SUM(charge) FROM patient_procedure_bill WHERE created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS `Procedure Cash`, (SELECT SUM(charge) FROM patient_lab_bill WHERE created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS `Lab Cash`, (SELECT SUM(charge) FROM patient_service_bill WHERE created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS `Service Cash`,(SELECT SUM(charge) FROM patient_forensic_bill WHERE created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS `Forensic Cash`,(SELECT SUM(charge) FROM patient_radiology_bill WHERE created_on BETWEEN UNIX_TIMESTAMP('".$fromDatePrev." 00:00:00') AND UNIX_TIMESTAMP('".$toDatePrev." 23:59:59')) AS `Radiology Cash_1`,(SELECT SUM(charge) FROM patient_procedure_bill WHERE created_on BETWEEN UNIX_TIMESTAMP('".$fromDatePrev." 00:00:00') AND UNIX_TIMESTAMP('".$toDatePrev." 23:59:59')) AS `Procedure Cash_1`, (SELECT SUM(charge) FROM patient_lab_bill WHERE created_on BETWEEN UNIX_TIMESTAMP('".$fromDatePrev." 00:00:00') AND UNIX_TIMESTAMP('".$toDatePrev." 23:59:59')) AS `Lab Cash_1`, (SELECT SUM(charge) FROM patient_service_bill WHERE created_on BETWEEN UNIX_TIMESTAMP('".$fromDatePrev." 00:00:00') AND UNIX_TIMESTAMP('".$toDatePrev." 23:59:59')) AS `Service Cash_1`,(SELECT SUM(charge) FROM patient_forensic_bill WHERE created_on BETWEEN UNIX_TIMESTAMP('".$fromDatePrev." 00:00:00') AND UNIX_TIMESTAMP('".$toDatePrev." 23:59:59')) AS `Forensic Cash_1`";

			$expenseQry = "SELECT (SELECT SUM(unit_cost) FROM received_orders WHERE received_date BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS 'Receive Order',(SELECT SUM(amount) FROM payment_voucher WHERE status='A' AND created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS 'Payment Voucher',(SELECT SUM(amount) FROM petty_cash WHERE status='A' AND created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS 'Petty Cash',(SELECT SUM(amount) FROM imprest_disbursement WHERE status='A' AND created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS 'Imprest Disbursment',(SELECT SUM(unit_cost) FROM received_orders WHERE received_date BETWEEN UNIX_TIMESTAMP('".$fromDatePrev." 00:00:00') AND UNIX_TIMESTAMP('".$toDatePrev." 23:59:59')) AS 'Receive Order_1',(SELECT SUM(amount) FROM payment_voucher WHERE status='A' AND created_on BETWEEN UNIX_TIMESTAMP('".$fromDatePrev." 00:00:00') AND UNIX_TIMESTAMP('".$toDatePrev." 23:59:59')) AS 'Payment Voucher_1',(SELECT SUM(amount) FROM petty_cash WHERE status='A' AND created_on BETWEEN UNIX_TIMESTAMP('".$fromDatePrev." 00:00:00') AND UNIX_TIMESTAMP('".$toDatePrev." 23:59:59')) AS 'Petty Cash_1',(SELECT SUM(amount) FROM imprest_disbursement WHERE status='A' AND created_on BETWEEN UNIX_TIMESTAMP('".$fromDatePrev." 00:00:00') AND UNIX_TIMESTAMP('".$toDatePrev." 23:59:59')) AS 'Imprest Disbursment_1'";
		}
		else{
			$salesQry = "SELECT(SELECT SUM(charge) FROM patient_radiology_bill WHERE YEAR(FROM_UNIXTIME(created_on)) = YEAR(CURDATE())) AS `Radiology Cash`, (SELECT SUM(charge) FROM patient_procedure_bill WHERE YEAR(FROM_UNIXTIME(created_on)) = YEAR(CURDATE())) AS `Procedure Cash`, (SELECT SUM(charge) FROM patient_lab_bill WHERE YEAR(FROM_UNIXTIME(created_on)) = YEAR(CURDATE())) AS `Lab Cash`,(SELECT SUM(charge) FROM patient_service_bill WHERE YEAR(FROM_UNIXTIME(created_on)) = YEAR(CURDATE())) AS `Service Cash`, (SELECT SUM(charge) FROM patient_forensic_bill WHERE YEAR(FROM_UNIXTIME(created_on)) = YEAR(CURDATE())) AS `Forensic Cash`,(SELECT SUM(charge) FROM patient_radiology_bill WHERE YEAR(FROM_UNIXTIME(created_on)) = YEAR(CURDATE())-1) AS `Radiology Cash_1`, (SELECT SUM(charge) FROM patient_procedure_bill WHERE YEAR(FROM_UNIXTIME(created_on)) = YEAR(CURDATE())-1) AS `Procedure Cash_1`, (SELECT SUM(charge) FROM patient_lab_bill WHERE YEAR(FROM_UNIXTIME(created_on)) = YEAR(CURDATE())-1) AS `Lab Cash_1`,(SELECT SUM(charge) FROM patient_service_bill WHERE YEAR(FROM_UNIXTIME(created_on)) = YEAR(CURDATE())-1) AS `Service Cash_1`, (SELECT SUM(charge) FROM patient_forensic_bill WHERE YEAR(FROM_UNIXTIME(created_on)) = YEAR(CURDATE())-1) AS `Forensic Cash_1`";

			$expenseQry = "SELECT (SELECT SUM(unit_cost) FROM received_orders WHERE YEAR(FROM_UNIXTIME(received_date)) = YEAR(CURDATE())) AS 'Receive Order',(SELECT SUM(amount) FROM payment_voucher WHERE status='A' AND YEAR(FROM_UNIXTIME(created_on)) = YEAR(CURDATE())) AS 'Payment Voucher',(SELECT SUM(amount) FROM petty_cash WHERE status='A' AND YEAR(FROM_UNIXTIME(created_on)) = YEAR(CURDATE())) AS 'Petty Cash',(SELECT SUM(amount) FROM imprest_disbursement WHERE status='A'  AND YEAR(FROM_UNIXTIME(created_on)) = YEAR(CURDATE())) AS 'Imprest Disbursment',(SELECT SUM(unit_cost) FROM received_orders WHERE YEAR(FROM_UNIXTIME(received_date)) = YEAR(CURDATE())-1) AS 'Receive Order_1',(SELECT SUM(amount) FROM payment_voucher WHERE status='A' AND YEAR(FROM_UNIXTIME(created_on)) = YEAR(CURDATE())-1) AS 'Payment Voucher_1',(SELECT SUM(amount) FROM petty_cash WHERE status='A' AND YEAR(FROM_UNIXTIME(created_on)) = YEAR(CURDATE())-1) AS 'Petty Cash_1',(SELECT SUM(amount) FROM imprest_disbursement WHERE status='A'  AND YEAR(FROM_UNIXTIME(created_on)) = YEAR(CURDATE())-1) AS 'Imprest Disbursment_1'";
		}

		$qryTotalSales  = mysqli_query($conn,$salesQry);	
		$rowsSale   = array();
	    while ($r = mysqli_fetch_assoc($qryTotalSales)) {
	        $rowsSale[] = $r;
	    }

	    $qryTotalExpense  = mysqli_query($conn,$expenseQry);	
		$rowsExpense   = array();
	    while ($r = mysqli_fetch_assoc($qryTotalExpense)) {
	        $rowsExpense[] = $r;
	    }

		$Allrows   = array();

		array_push($Allrows, $rowsSale);
		array_push($Allrows, $rowsExpense);

		echo json_encode($Allrows);
	}
?>		


