<?php
/*****************************************************************************************************
 * File Name    :   drug_category.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages drug category data
 ****************************************************************************************************/
 
 	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	$createdate          = new DateTime();
    $operation = "";
	$categoriesName ="";
	$description = "";
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	
	// import excel file feature
	if (isset($_FILES["file"]["type"])) // use for import data from csv
	 {
		$temporary       = explode(".", $_FILES["file"]["name"]);
		$file_extension = end($temporary);
		/*if ($file_extension == "csv") {
			if ($_FILES["file"]["error"] > 0) {
				echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
			} else {
				if($_FILES["file"]["size"] > 0)
				{
					$filename = $_FILES["file"]["tmp_name"];
					$file = fopen($filename, "r");
					$countCSV = 0;
					$subInsertValues = '';
					$sqlInsert = "INSERT IGNORE INTO drug_categories(category,description,created_on,updated_on,created_by,updated_by) VALUES " ;
					while (($emapData = fgetcsv($file, 10000, ",")) != FALSE)
					{
						//if there are more than 2 column then restrict it.	
						if (sizeof($emapData) != 2) {
						 	echo "You must have two column in your file i.e category and description";
						 	exit();
						}

						//check whether first column name should be category and second should be description
						if ($countCSV == 0) {
							if(strtolower(reset($emapData)) !="category"){
								echo "First column should be category";
								exit();
							}
							if(strtolower(end($emapData)) !="description"){
								echo "Second column should be description";
								exit();
							}						
						}
						reset($emapData);
						$firstIndex = key($emapData);
						end($emapData);
						$lastIndex = key($emapData);
						//data can't be null in first row else row it may be null
						if ($countCSV == 1) {
							if (empty($emapData[$firstIndex]) ) {
								echo "Data can't be null in category column";
								exit();
							}
						}
						if ($countCSV > 0) {
							//check whether data not be null in the file
							if(($emapData[$firstIndex]) != "" ) {
								if ($countCSV >1) {
									$subInsertValues.= ',';
								}						
								$subInsertValues.= "('".$emapData[$firstIndex]."','".$emapData[$lastIndex]."',	UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
							}
						}
						//It wiil insert a row to our subject table from our csv file`					

						$countCSV++;						
					}
					$sqlInsert = $sqlInsert.$subInsertValues;
					$result = mysqli_query($conn,$sqlInsert);
					if($result)
					{
						echo $result;
					}
					else{
						echo "";
					}
					fclose($file);
				}
			} 	
		}
		else if($file_extension == "xls"){
			ini_set("display_errors",1);
			$filename = $_FILES['file']['tmp_name'];
			require_once 'excel_reader2.php';
			$data = new Spreadsheet_Excel_Reader($filename);
			$countSheets = count($data->sheets);
			for($i=0;$i<$countSheets;$i++) // Loop to get all sheets in a file.
			{ 
				$countSheetsCells =0;
				if(isset($data->sheets[$i]['cells'])){
					$countSheetsCells = count($data->sheets[$i]['cells']);
				}
				if($countSheetsCells > 0 && $countSheetsCells) // checking sheet not empty
				{
					$sql = "INSERT IGNORE INTO drug_categories(category,description,created_on,updated_on,created_by,updated_by) VALUES ";
					$countXls = 0;				
					$subInsert = '';
					for($j=1;$j<=$countSheetsCells;$j++) // loop used to get each row of the sheet
					{ 
						
					  	if (sizeof($data->sheets[$i]['cells'][$j]) != 2) {
					  		echo "You must have two column in your file i.e category and description";
						 	exit();
					  	}
					  	
					  		
					  	$indexOfExcelFile = array_keys($data->sheets[$i]['cells'][$j]);	

						
						$name = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[0]];
						
						$description = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[1]];
						

						
						if ($countXls == 0) {
							if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[0]]) !="category"){
								echo "First column should be category";
								exit();
							}
							if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[1]]) !="description"){
								echo "Second column should be description";
								exit();
							}
						}

						//data can't be null in the very first row of file
						if ($countXls == 1) {
							if (empty($name)) {
								echo "Data can't be null in category column";
								exit();
							}
						}
						 
						if ($countXls>0) {

							//check whether data not be null in file
							if($name != '' ){
								if ($countXls >1) {
									$subInsert .=',';
								}					 	
							 	$subInsert .=" ('$name','$description',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
							}
						}
						$countXls++;						 		
					}
					$sql = $sql.$subInsert;
					$result = mysqli_query($conn,$sql);
					if($result)
					{
						echo $result;
					}
					else{
						echo "";
					}
				}
			}		
		}*/
		if($file_extension == "xlsx" || $file_extension == "csv" ||$file_extension == "xls"){
			// get file name
			$filename = $_FILES['file']['tmp_name'];
			
			//import php script
			require_once 'PHPExcel.php';
			
			// 
			try {
				$inputFileType = PHPExcel_IOFactory::identify($filename);  // get type of file name
				$objReader = PHPExcel_IOFactory::createReader($inputFileType); //create object of read file
				$objPHPExcel = $objReader->load($filename);  //load excel into the object
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($filename, PATHINFO_BASENAME) 
				. '": ' . $e->getMessage());
			}

			//  Get worksheet dimensions
			$sheet = $objPHPExcel->getSheet(0); // get sheet 1 of excel file
			$highestRow = $sheet->getHighestRow(); // get heighest row
			$highestColumn = $sheet->getHighestColumn(); // get heighest column
			
			$headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE); // get heading in first row
				
			//check there should be two column
			if (sizeof($headings[0]) != 2) {
				echo "You must have two column in your file i.e category and description";
				exit();
			}
			//reset($headings[0]) it is the first column
			if(trim(strtolower(reset($headings[0]))) !="category"){
				echo "First column should be category";
				exit();
			}
			//end($headings[0]) it is the last column
			if(trim(strtolower(end($headings[0]))) !="description"){
				echo "Second column should be description";
				exit();
			}
			$sql = "INSERT IGNORE INTO drug_categories(category,description,created_on,updated_on,created_by,updated_by) VALUES ";
			$countXlsx = 0;
			$subInsertXlsx = '';	

			for ($row = 2; $row <= $highestRow; $row++) {
				//  Read a row of data into an array
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL, TRUE, FALSE);
				
				// combine data in array accoding to heading
				$rowData[0] = array_combine($headings[0], $rowData[0]);
				//var_dump($rowData[0]);
				
				// fetch value from row

				$key  = array_keys($rowData[0]);//getting the ondex name
				
				$name =  mysql_real_escape_string(trim($rowData[0][$key[0]]));
			
				$description = mysql_real_escape_string(trim($rowData[0][$key[1]]));
				
				// data shouldn't be null in the very first row of file
				if ($countXlsx == 0) {

					if (empty($name)) {
						echo "Data can't be null in category column";
						exit();
					}
				}
					
				// insert data into table
				if ($countXlsx>0) {
					$subInsertXlsx.=',';
				}
				$subInsertXlsx.="('$name','$description',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
						
				$countXlsx++;			
			}

			$sql = $sql.$subInsertXlsx;
			$result = mysqli_query($conn,$sql);

			if($result)	{
				echo $result;
			}
			else{
				echo "";
			}
		}
		else{
			echo "invalid file";
		}
	}

	//This operation is used for save the data 
	if($operation == "save")			
	{
		if (isset($_POST['categoriesName'])) {
		$categoriesName=$_POST['categoriesName'];
		}
		if (isset($_POST['description'])) {
		$description=$_POST['description'];
		}
		
		$sql = "INSERT INTO drug_categories(category,description,created_on,updated_on,created_by,updated_by) 
				VALUES('".$categoriesName."','".$description."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	} 

	//This function is used for update the data 
	if($operation == "update")			
	{
		if (isset($_POST['categoriesName'])) {
			$categoriesName=$_POST['categoriesName'];
		}
		if (isset($_POST['description'])) {
			$description=$_POST['description'];
		}
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}		
		
		$sql = "UPDATE drug_categories SET category= '".$categoriesName."',description= '".$description."',updated_on=UNIX_TIMESTAMP(),updated_by='".$userId."' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//This operation is used for update the status 
	if($operation == "delete")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}		
	
		$sql = "UPDATE drug_categories SET status= 'I' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//This operation is used for restore the data 
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		
		$sql = "UPDATE drug_categories SET status= 'A' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//This operation is used for show the active data 
	if($operation == "show"){
		
		$query= "select * from drug_categories WHERE status='A'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
	//This operation is used for show the inactive data 
	if($operation == "showInActive"){
		
		$query= "select * from drug_categories WHERE status='I'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
?>