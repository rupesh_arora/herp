<?php
/*File Name  :   department_budget.php
Company Name :   Qexon Infotech
Created By   :   Kamesh Pathak
Created Date :   30th May, 2016
Description  :   This page manages AND add beds*/

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

/*include config file*/
include 'config.php';

$operation   = "";
$description = "";

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

if ($operation == "showBudget") {
    
    $query        = "SELECT id,CONCAT(name,'  ','( ',begin_date,'-',end_date,' )') AS name FROM budget_period   WHERE status='A'";
    $result       = mysqli_query($conn, $query);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
   
}

/*operation to save data*/
if ($operation == "save") {
    if (isset($_POST['departmentId'])) {
        $departmentId = json_decode( $_POST['departmentId']);
    }
    if (isset($_POST['budgetId'])) {
        $budgetId = $_POST['budgetId'];
    }    
   
   foreach ($departmentId as $value) {
         $sql = "INSERT INTO department_budget(budget_period_id,department_id,created_on,created_by,updated_on,updated_by) VALUES('" . $budgetId . "','" . $value . "',UNIX_TIMESTAMP(),'".$userId."',UNIX_TIMESTAMP(),'".$userId."')";
        $result = mysqli_query($conn, $sql);
   }
   
    echo $result;
}

/*operation to show active data*/
if ($operation == "show") {
    
    $query        = "SELECT db.id,db.budget_period_id,db.department_id,d.department,CONCAT(bp.name,'  ','( ',bp.begin_date,'-',bp.end_date,' )') AS   name FROM department_budget AS db 
                LEFT JOIN department AS d ON d.id = db.department_id  
                LEFT JOIN budget_period AS bp ON bp.id = db.budget_period_id 
                WHERE db.status='A' AND bp.status='A' ";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/*operation to show inactive data*/
if ($operation == "showInActive") {
    $query        = "SELECT db.id,db.budget_period_id,db.department_id,d.department,CONCAT(bp.name,'  ','( ',bp.begin_date,'-',bp.end_date,' )')
                     AS name FROM department_budget AS db 
                    LEFT JOIN department AS d ON d.id = db.department_id  
                    LEFT JOIN budget_period AS bp ON bp.id = db.budget_period_id 
                    WHERE db.status='I' OR bp.status='I'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/*operation to delete data*/
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE department_budget SET status= 'I' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
	if($result){
		echo "1";
	}else{
		echo "0";
	}  
}

/*operation to restore data*/
if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE department_budget SET status= 'A' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);   
    
        
    if ($result) {
        echo "Restored Successfully!!!";
    } else {
        echo "It can not be restore!!!";
    }
}

/*operation to update data*/
if ($operation == "saveBdgetMatrix") {

     if (isset($_POST['aTable'])) {
        $aTable = json_decode($_POST['aTable']);
    }

    if (isset($_POST['tableData'])) {
        $tableData = json_decode($_POST["tableData"]);
    }

    if (isset($_POST['glLedgerarray'])) {
        $glLedgerarray = json_decode($_POST["glLedgerarray"]);
    }

    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    } 
    $count =0;
    $result = '0';
    
    $a4         = array();
    foreach ($tableData as $value) {
        $a3         = array();
        foreach($value as $key => $val) { 

              $a3[] =$val;
        }
        $a1=array( $glLedgerarray[$count],$id);
        $a2=array($userId,$userId,'A');
        $a4 = array_merge($a1,$a2,$a3);
        $sql='INSERT INTO `budget_matrix` (`'.implode('`, `', $aTable).'`) VALUES("'.implode('", "', $a4).'")';
        $result = mysqli_query($conn, $sql);

        $lastId = mysqli_insert_id($conn);
        $query = "UPDATE budget_matrix SET created_on =UNIX_TIMESTAMP(),updated_on = UNIX_TIMESTAMP() WHERE id = '" . $lastId . "'";
        $result = mysqli_query($conn, $query);
        $count++;
   }    
  
    echo $result;
}

if ($operation == "updateBdgetMatrix") {

     if (isset($_POST['updateTable'])) {
        $updateTable = json_decode($_POST['updateTable']);
    }

    if (isset($_POST['tableData'])) {
        $tableData = json_decode($_POST["tableData"]);
    }

    if (isset($_POST['whereData'])) {
        $whereData = json_decode($_POST["whereData"]);
    }
   
    $result = '0';
    $count = 0;

    foreach ($tableData as $value) {
        $args         = array();
        $counter = 0;
        foreach($value as $key => $val) {             
            $args[] = '`'.$updateTable[$counter].'`="'.$val.'"';
            $counter++;
        }
        
        $sql= "UPDATE `budget_matrix` SET updated_on = UNIX_TIMESTAMP(),updated_by = '" . $userId ."', ".implode(',',$args)." WHERE id = '" .$whereData[$count]."'";
        $result = mysqli_query($conn, $sql);
        $count++;
       
   }    
  
    echo $result;
}

if ($operation == "showColumnName") {
    
    $query        = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '".$mysql_db."' AND TABLE_NAME = 'budget_matrix'";
    $result       = mysqli_query($conn, $query);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    print json_encode($rows);
}

if ($operation == "showGLAccount") {

     if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $query        = "SELECT id,account_no,account_name FROM chart_of_account";
    $result       = mysqli_query($conn, $query);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    print json_encode($rows);
}

if ($operation == "showBudgetMatrix") {

    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }

    if (isset($_POST['aTable'])) {
        $aTable = json_decode($_POST['aTable']);
    }
    $len =  sizeof($aTable);
    //print_r($aTable);
     $query        = "SELECT ";
    for($i=5; $i<$len; $i++){
        $query        .= "`".$aTable[$i]."`,";
    }
    $query        .= "`id` FROM budget_matrix WHERE department_budget_id = '". $id."' ";
    /*$query        = "SELECT id,account_no,account_name FROM chart_of_account";*/
    $result       = mysqli_query($conn, $query);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    print json_encode($rows);
}

?>