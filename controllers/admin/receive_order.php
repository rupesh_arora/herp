<?php
/*File Name  :   lab_test_request.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page manages  all the lab test request*/

session_start();

/*include config file*/
include 'config.php';

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
}
/*Get user id from session table*/
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
} else {
    exit();
}

if ($operation == "showOrderDetails") {
    
    if (isset($_POST['orderId'])) {
        $orderId = $_POST["orderId"];
    }
    
    $query = "SELECT `order-item`.quantity,`order-item`.quantity , items.name as item_name, 
		items.id as item_id ,`order-item`.cost ,`order-item`.`status` from `order-item`
		LEFT JOIN items ON items.id = `order-item`.item_id
		WHERE  `order-item`.order_id = '" . $orderId . "'";
    
    $result   = mysqli_query($conn, $query);
    $rowCount = mysqli_num_rows($result);
    if ($rowCount > 0) {
        $rows = array();
        while ($r = mysqli_fetch_assoc($result)) {
            $rows[] = $r;
        }
        print json_encode($rows);
    } else {
        echo "0";
    }
}
if ($operation =="itemQuantity") {

    $orderId = $_POST['orderId'] ;
    $itemId = $_POST['itemId'];

    $sqlSelect = "SELECT ordered_quantity from `order-item` 
    WHERE order_id = '" . $orderId . "' AND item_id = '".$itemId."'";

    $result = mysqli_query($conn, $sqlSelect);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "searchItem") {
    
    if (isset($_POST['itenName'])) {
        $itenName = $_POST["itenName"];
    }
    
    /*This orderId will be get from receive order but not from return item screen*/
    if (isset($_POST['orderId'])) {
        $orderId = $_POST["orderId"];
        
        $query = "SELECT items.id, items.name,`order-item`.ordered_quantity from items 
       LEFT JOIN `order-item` ON `order-item`.item_id = items.id
       WHERE `order-item`.order_id = '".$orderId."' AND items.status  = 'A' 
       AND `order-item`.`status` = 'pending'  ORDER BY name";
    } 
    else {
        $query = "SELECT id, name from items WHERE status  = 'A'";
    }
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "serachOrderId") {
    
    if (isset($_POST['orderId'])) {
        $orderId = $_POST["orderId"];
    }
    $query = "SELECT id from `order-item` WHERE order_id  = '".$orderId."'  AND `status` ='pending'";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "saveReceivedDetails") {
    if (isset($_POST['data'])) {
        $data = json_decode($_POST['data']);
    }
    if (isset($_POST['orderId'])) {
        $orderId = json_decode($_POST['orderId']);
    }
    if (isset($_POST['invoiceNumber'])) {
        $invoiceNo = $_POST['invoiceNumber'];
    }
    if (isset($_POST['ledgerId'])) {
        $ledgerId = $_POST['ledgerId'];
    }
    if (isset($_POST['accountPayable'])) {
        $accountPayable = $_POST['accountPayable'];
    }
    
    $arrayLength = sizeof($data);
    if (!empty($data)) {
        
        //query to insert value
        
        $insertQry = "INSERT INTO received_orders(order_id,item_id,quantity,unit_cost,received_by,received_date,batch_no,invoice_no,mfg_date,exp_date,discount,ledger,gl_account,account_payable) VALUES";
        
        $insertStockQry = "INSERT INTO item_stock(item_id,mfg_date,exp_date,batch_no,quantity,
    	unit_cost,created_on,created_by,updated_on,updated_by) VALUES";
        
        $updateQry = "UPDATE `order-item` SET `status` ='received' 
    		WHERE ";
        
        $subQuery          = '';
        $subUpdateQry      = '';
        $subInsertStockQry = "";
        
        $counter = 0;
        $stockInsertCounter = 0;
        
        foreach ($data as $value) {
            
            $item_id       = 0;
            $cost          = 0;
            $quantity      = 0;
            $batchNo       = 0;            
            $mfgDate       = 0;
            $expDate       = 0;
            $discount      = 0;
            $totalCost     = 0;
            $gl_account_id = '';
            
            foreach ($value as $key => $val) {
                if ($key == 1) {
                    $quantity = $val;
                }
                if ($key == 2) {
                    $cost = $val;
                }
                if ($key == 3) {
                    $batchNo = $val;
                }
                if ($key == 4) {
                    $mfgDate = $val;
                }
                if ($key == 5) {
                    $expDate = $val;
                }
                if ($key == 6) {
                    $discount = $val;
                }
                if ($key == 10) {
                    $item_id = $val;
                }
                if ($key == 11) {
                    $glAccountId = $val;
                }
            }
            
            //query to check whether that item id with order id is already there or not
            $sqlSelect = "SELECT COUNT(id) AS count FROM received_orders WHERE order_id = '" . $orderId . "' AND item_id = '" . $item_id . "'";
            $result    = mysqli_query($conn, $sqlSelect);
            $r         = mysqli_fetch_assoc($result);
            $count     = $r['count'];
            
            //query to check whther item id present in item stock
            $sqlSelectItem = "SELECT COUNT(item_id) AS countItemId FROM item_stock WHERE 
				item_id = '" . $item_id . "'";
            $resultItemId  = mysqli_query($conn, $sqlSelectItem);
            $rItemId       = mysqli_fetch_assoc($resultItemId);
            $countItemId   = $rItemId['countItemId'];
            
            if ($count == 0) {
                
                if ($counter > 0) {
                    $subQuery .= ",";
                    $subUpdateQry .= " OR ";
                }                
                
                $subQuery .= "('" . $orderId . "','" . $item_id . "','" . $quantity . "',
					'" . $cost . "','" . $userId . "',UNIX_TIMESTAMP(),'" . $batchNo . "','" . $invoiceNo . "','" . $mfgDate . "','" . $expDate . "','" . $discount . "','".$ledgerId."','".$glAccountId."','".$accountPayable."')";
                
                //if count item id not present then insert data
                if ($countItemId == 0) {

                    //if qry repeats add a comma
                    if ($stockInsertCounter > 0) {
                        $subInsertStockQry .= ",";
                    }

                    //values for insert in to stock table
                    $subInsertStockQry .= "('".$item_id."','".$mfgDate."','".$expDate."','".$batchNo."',
                    '".$quantity."','".$cost."',UNIX_TIMESTAMP(),'".$userId."',UNIX_TIMESTAMP(),
                    '".$userId."')";

                    $stockInsertCounter++;
                }
                /*If item id is present then update stock*/
                else {
                    $updateItemIdQry = "UPDATE item_stock SET mfg_date ='".$mfgDate."', 
                    exp_date ='".$expDate."', batch_no = '".$batchNo."', 
                    quantity = quantity +'".$quantity ."', unit_cost ='".$cost."', 
                    updated_on =UNIX_TIMESTAMP(),updated_by = '".$userId."' WHERE item_id ='".$item_id."'";
                    
                    $result = mysqli_query($conn, $updateItemIdQry);
                }
                
                
                if ($counter == 0) {
                    
                    $subUpdateQry .= "order_id='" . $orderId . "' AND ";
                    $subUpdateQry .= "(";
                }
                $subUpdateQry .= "item_id ='" . $item_id . "'";
                
                if ($counter == $arrayLength - 1) {
                    $subUpdateQry .= ")";
                }
                $counter++;
            }
            
            //if item id already exsit not run any of the query
            else {
                echo "Already exist";
                exit();
            }
        }
        
        $compelteInsertQry = $insertQry . $subQuery . ";";
        $compelteUpdateQry = $updateQry . $subUpdateQry . ";";
        
        /*If string of insert query is greater than 130 then only execute it*/
       
        $compelteItemInsertQry = $insertStockQry . $subInsertStockQry . ";";
        
        
        $resultIns = mysqli_query($conn, $compelteInsertQry);
        if ($resultIns) {
            
            $resultUpdate = mysqli_query($conn, $compelteUpdateQry);
            
            if (strlen($compelteItemInsertQry) > 130) {
                $resultItemInsert = mysqli_query($conn, $compelteItemInsertQry);
            }
            
            
            if ($resultUpdate) {
                
                $statusQry = "SELECT *  from `order-item` where `status` = 'pending' AND 
				order_id = '" . $orderId . "'";
                
                $countRows = '';
                if (!(mysqli_ping($conn))) {
                    include 'config.php';
                }
                $statusResult = mysqli_query($conn, $statusQry);
                
                $countRows = mysqli_num_rows($statusResult);
                if ($countRows == 0) {
                    
                    $updateStatus = "UPDATE orders SET orders.`status` = 'received' 
					WHERE orders.id ='" . $orderId . "'";
                } else {
                    $updateStatus = "UPDATE orders SET orders.`status` = 'pending' 
					WHERE orders.id ='" . $orderId . "'";
                }
                
                if (!(mysqli_ping($conn))) {
                    include 'config.php';
                }
                $UpdateStatusResult = mysqli_query($conn, $updateStatus);
                
                if ($UpdateStatusResult) {
                    echo $UpdateStatusResult;
                } else {
                    echo "2";
                }
            }
        } else {
            echo "0";
        }
    }
}
?>