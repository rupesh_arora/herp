<?php
/*****************************************************************************************************
 * File Name    :   pharmacy_inventory.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages pharmacy inventory data
 ****************************************************************************************************/
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	$operation = "";
	$drugId = "";
	$quantity = "";
	$manufactureName = "";
	$unitVol = "";
	$unit = "";
	$id = "";
	$manufactureDate = "";
	$expiryDate = "";	
	
	include 'config.php';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	
	//This operation is used for select the unit 
	if($operation == "showUnit")   
	{
		$query="SELECT id,unit_abbr FROM units ORDER BY unit_abbr";
  
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		$rows[] = $r;
		}
		print json_encode($rows);
	}
	
	//This operation is used for save the data 
	if($operation == "save")   
	{
		if (isset($_POST['drugId'])) {
			$drugId=$_POST["drugId"];
		}
		if (isset($_POST['quantity'])) {
			$quantity=$_POST["quantity"];
		}
		if (isset($_POST['manufactureName'])) {
			$manufactureName=$_POST["manufactureName"];
		}
		if (isset($_POST['unitVol'])) {
			$unitVol=$_POST["unitVol"];
		}
		if (isset($_POST['unit'])) {
			$unit=$_POST["unit"];
		}
		if (isset($_POST['manufactureDate'])) {
			$manufactureDate=$_POST["manufactureDate"];
			$mdate=date_create($manufactureDate);
			$manDate = date_format($mdate,"Y/m/d");
		}
		if (isset($_POST['expiryDate'])) {
			$expiryDate=$_POST["expiryDate"];
			$exdate=date_create($expiryDate);
			$expDate = date_format($exdate,"Y/m/d");
		}		
		
		$query="INSERT INTO pharmacy_inventory(drug_name_id, quantity, manufacture_name, unit_vol, unit_id, manufacture_date, expiry_date,created_on,updated_on,created_by,updated_by)
		VALUES('".$drugId."', '".$quantity."','".$manufactureName."', '".$unitVol."', '".$unit."', '".$manDate."', '".$expDate."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";  
		$result=mysqli_query($conn,$query);
		echo $result;
	}
	
	//This operation is used for update the data 
	if($operation == "Update")   
	{
		if (isset($_POST['drugId'])) {
			$drugId=$_POST["drugId"];
		}
		if (isset($_POST['quantity'])) {
			$quantity=$_POST["quantity"];
		}
		if (isset($_POST['manufactureName'])) {
			$manufactureName=$_POST["manufactureName"];
		}
		if (isset($_POST['unitVol'])) {
			$unitVol=$_POST["unitVol"];
		}
		if (isset($_POST['unit'])) {
			$unit=$_POST["unit"];
		}
		if (isset($_POST['manufactureDate'])) {
			$manufactureDate=$_POST["manufactureDate"];
			$mdate=date_create($manufactureDate);
			$manDate = date_format($mdate,"Y/m/d");
		}
		if (isset($_POST['expiryDate'])) {
			$expiryDate=$_POST["expiryDate"];
			$exdate=date_create($expiryDate);
			$expDate = date_format($exdate,"Y/m/d");
		}
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		
		$query="UPDATE pharmacy_inventory SET drug_name_id= '".$drugId."',quantity= '".$quantity."',manufacture_name= '".$manufactureName."',unit_vol='".$unitVol."',unit_id= '".$unit."',manufacture_date= '".$manDate."',expiry_date= '".$expDate."',updated_on=UNIX_TIMESTAMP(),updated_by='".$userId."'  WHERE  id = '".$id."'";  
		$result=mysqli_query($conn,$query);
		echo $result;
	}
	
	//This operation is used for show the active data 
	if($operation == "show"){
		
		$query= "SELECT p.id,p.drug_name_id,p.quantity,p.manufacture_name,CONCAT(p.unit_vol,' ',u.unit_abbr) AS unit_vol,d.name,
			p.unit_id,p.manufacture_date,p.expiry_date,p.unit_vol AS unit_v,u.unit,
			d.composition,d.drug_code FROM 	pharmacy_inventory p
			LEFT JOIN drugs AS d ON d.id = p.drug_name_id 
			LEFT JOIN units AS u ON u.id = p.unit_id 
			WHERE d.`status` = 'A' And p.`status` = 'A'";		
		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
/* 	//This operation is used for show the inactive data 
	if($operation == "showInActive"){
		
		$query= "SELECT p.id,p.drug_name_id,p.quantity,p.manufacture_name,CONCAT(p.unit_vol,' ',u.unit_abbr) AS unit_vol,d.name,
			p.unit_id,p.manufacture_date,p.expiry_date,p.unit_vol AS unit_v,u.unit,
			d.composition,d.drug_code FROM 	pharmacy_inventory p
			LEFT JOIN drugs AS d ON d.id = p.drug_name_id 
			LEFT JOIN units AS u ON u.id = p.unit_id 
			WHERE d.`status` = 'I' OR p.`status` = 'I'";		
		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	} */
	
	//This operation is used for update the status
	if($operation == "delete")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		
		$sql = "DELETE FROM pharmacy_inventory WHERE id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo "Deleted Successfully!!!";
	}
	
	//This operation is used for restore the data 
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		$sql = "UPDATE pharmacy_inventory SET status= 'A'  WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
?>