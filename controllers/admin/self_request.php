<?php

    /*File Name  :   self_request.php
    Company Name :   Qexon Infotech
    Created By   :   Rupesh Arora
    Created Date :   30th Dec, 2015
    Description  :   This page manges self request tests*/
	session_start();	

    if(isset($_SESSION['globaluser'])){
        $userId = $_SESSION['globaluser'];//store global user value in variable
    }
    else{
    	exit();
    }

    /*include config file*/
	include 'config.php';
    $operation = "";

    /*checking operation set or not*/
	if (isset($_POST['operation'])) {
	    $operation = $_POST["operation"];
	} else if (isset($_GET["operation"])) {
	    $operation = $_GET["operation"];
	}

    /*operation to show service type*/
	if ($operation == "showServiceType") {
	    $query  = "SELECT id,type FROM service_type WHERE status = 'A'  ORDER BY type";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

    /*operation to show services*/
	if ($operation == "showServices") {
	    if (isset($_POST['value'])) {
	        $value = $_POST['value'];
	    }
	    $query  = "SELECT id,cost,name FROM services WHERE service_type_id = '" . $value . "' AND status = 'A' ORDER BY name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

    /*operation to show lab type*/
	if ($operation == "showLabType") {
	    $query  = "SELECT id,type FROM lab_types WHERE status = 'A' ORDER BY type";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

    /*operation to show lab test*/
	if ($operation == "showLabTest") {
	    if (isset($_POST['value'])) {
	        $value = $_POST['value'];
	    }
	    $query  = "SELECT id,name FROM lab_tests WHERE lab_type_id = '" . $value . "' AND status = 'A' ORDER BY name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	/*operation to show lab test*/
	if ($operation == "showLabTestComponent") {
	    if (isset($_POST['labTestId'])) {
	        $labTestId = $_POST['labTestId'];
	    }
	    $query  = "SELECT id,name,fee,lab_test_id  FROM lab_test_component WHERE lab_test_id = '" . $labTestId . "' ORDER BY name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

    /*operation to show procedure*/
	if ($operation == "showProcedure") {
	    $query  = "SELECT id,CONCAT(name, '  ','(',code,')') AS name, price FROM procedures WHERE status='A' ORDER BY name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

    /*operation to show radiology test*/
	if ($operation == "showRadiologyTest") {
	    $query  = "SELECT id,name,fee FROM radiology_tests WHERE status = 'A' ORDER BY name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

    /*operation to show forensic test*/
	if ($operation == "showForensicTest") {
	    $query  = "SELECT id,name,fee FROM forensic_tests WHERE status = 'A' ORDER BY name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	/*operation to show drug*/
	if ($operation == "showDrug") {
	    $query  = "SELECT id,name,price FROM drugs WHERE status = 'A' ORDER BY name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

    /*operation to show patient details*/
	if ($operation == "patientDetail") {
		$patientPrefix = $_POST['patientPrefix'];
		$sql           = "SELECT value FROM configuration WHERE name = 'patient_prefix'";
		$result        = mysqli_query($conn, $sql);
		while ($r = mysqli_fetch_assoc($result)) {
			$prefix = $r['value'];
		}
		if($prefix == $patientPrefix){
			 $r='';
			 if (isset($_POST['patientId'])) {
				$patientId = json_decode($_POST['patientId']);
			}
			/*Check wheteher patient id exisit in patient table or not*/
			$sqlSelectPatient        = "SELECT first_name, last_name, mobile, email,salutation,
				middle_name FROM patients
			WHERE id='" . $patientId . "'";
			$resultSelectPatient     = mysqli_query($conn, $sqlSelectPatient);
			$rows_count = mysqli_num_rows($resultSelectPatient);
			if ($rows_count < 1) {
				echo "0";
			}
			else{
				$rows = array();
				while ($r = mysqli_fetch_row($resultSelectPatient)) {
					$rows['first_name'] = $r[0];
					$rows['last_name'] = $r[1];
					$rows['mobile'] = $r[2];
					$rows['email'] = $r[3];
					$rows['salutation'] = $r[4];
					$rows['middle_name'] = $r[5];
				}
				print json_encode($rows);
			}
		}
		else{
			echo "";
		}
	}

    /*operation to save whole data*/
	if ($operation == "saveDataTableData") {

		$copay = '';
		$copayValue = '';
		$billStatus = '';
		$status ='';

	    if (isset($_POST['bigPrice_p'])) {
	        $bigPrice_p = json_decode($_POST['bigPrice_p']);
	    }
	    if (isset($_POST['bigPrice_r'])) {
	        $bigPrice_r = json_decode($_POST['bigPrice_r']);
	    }
	    if (isset($_POST['bigPrice_l'])) {
	        $bigPrice_l = json_decode($_POST['bigPrice_l']);
	    }
	    if (isset($_POST['Price'])) {
	        $Price = json_decode($_POST['Price']);
	    }
	    if (isset($_POST['bigPrice_s'])) {
	        $bigPrice_s = json_decode($_POST['bigPrice_s']);
			//print_r($bigPrice_s);
	    }
	    if (isset($_POST['bigPrice_f'])) {
	        $bigPrice_f = json_decode($_POST['bigPrice_f']);
	    }
	    if (isset($_POST['bigPrice_d'])) {
	        $bigPrice_d = json_decode($_POST['bigPrice_d']);
	    }
	    if (isset($_POST['patientId'])) {
	        $patientId = $_POST['patientId'];
	    }
		if (isset($_POST['prefix'])) {
	        $patientPrefix = $_POST['prefix'];
	    }
        if (isset($_POST['remarks'])) {
            $remarks = $_POST['remarks'];
        }
		if (isset($_POST['paymentMode'])) {
            $paymentMode = $_POST['paymentMode'];
        }
		if($_POST['insuranceNumber'] == "") {
			$insuranceNumber = "";
		}
		else{
			$insuranceNumber = $_POST['insuranceNumber'];
			$sql           = "SELECT i.*,c.copay_type,sp.value FROM insurance AS i 
								LEFT JOIN scheme_plans AS sp ON sp.id = i.insurance_plan_name_id 
								LEFT JOIN copay AS c ON c.id = sp.copay 
								WHERE patient_id = '".$patientId."' and insurance_number = '".$insuranceNumber."'";
			$result        = mysqli_query($conn, $sql);
			while ($row = mysqli_fetch_row($result)) {
				$copay = $row[14];
				$copayValue = $row[15];
			}
			
			$r = mysqli_num_rows($result);
			if($r == 0) {
				echo "check insuranc number";
				exit();
			}
			else{
				$insuranceNumber = $_POST['insuranceNumber'];
				$insuranceTotal = $_POST['insuranceTotal'];
				 
                if ($copay == "None") {
                    $billStatus = 'paid';           
                }
                else if ($copay == "fixed"){
                    if ($insuranceTotal <= $copayValue) {
                        $billStatus = 'paid';
                    }
                    else{
                        $billStatus = 'unpaid';
                    }
                }
                else if ($copay == "Percentage") {
                    if ($copayValue == "0" ) {
                        $billStatus =  "paid";
                    }
                    else{
                    	$billStatus =  "unpaid";
                    }
                }				
			}		
		}
		
		
		$visitPrefix ="";        
	    $sql           = "SELECT value,name FROM configuration";
		$result        = mysqli_query($conn, $sql);
		while ($r = mysqli_fetch_assoc($result)) {
			
			if($r['name'] == "visit_prefix"){
				$visitPrefix = $r['value'];
			}
			if($r['name'] == "patient_prefix"){
				$prefix = $r['value'];
			}			
		}
		if($prefix == $patientPrefix){
			$insert_procedure      = "";
			$insert_radiology_test = "";
			$insert_lab_test       = "";
			$insert_forensic_test  = "";
			$insert_services       = "";
			$insertQuery           = "";
			$visitId               = "";
			
			/*Insert data in to visit table first to get last visit id*/
			$sqlInsertVisit = "INSERT INTO visits(visit_type,insurance_no,payment_mode,patient_id,remark,created_on,updated_on,
				created_by,updated_by,status,triage_status)
				VALUES('4','".$insuranceNumber."','".$paymentMode."','".$patientId."', '".$remarks."', UNIX_TIMESTAMP(), UNIX_TIMESTAMP(),
				'".$_SESSION['globaluser']."','".$_SESSION['globaluser']."','A','A')";
			$resultInsertVisit= mysqli_query($conn,$sqlInsertVisit);

			/*select last visit id of patient*/
			$sqlSelect = "  SELECT id FROM visits  WHERE patient_id = '" . $patientId . "' ORDER BY id DESC LIMIT 1";
			$query     = mysqli_query($conn, $sqlSelect);
			while ($row = mysqli_fetch_row($query)) {
				$visitId = $row[0];
			} 
			/*Check if data FROM data table is null or not*/
			if (!empty($bigPrice_p)) {
				
				//query to insert value
				$insert_procedure = "INSERT INTO procedures_requests (ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,procedure_id,request_date
					,pay_status,test_status,cost,status) VALUES";
				
				//query to insert bill
				$insert_procedure_bill = "INSERT INTO patient_procedure_bill (ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,procedure_id,
					charge,status,created_by,updated_by,created_on,updated_on,payment_mode) VALUES";

				$subQuery         = '';
				$subQueryBill ='';
				$counter          = 0;
				
				
				foreach ($bigPrice_p as $value) {
				
					$id    = 0;
					$price = 0;
									
					foreach ($value as $key => $val) {
						if ($key == 0) { //at index one get the id
							$id = $val;
						}
						if ($key == 1) { // at index of array get the price
							$price = $val;
						}
						if ($key == 2) { // at index of array get the price
							$testName = $val;
						}
						if ($key == 3) {
							$paymentType = $val;

							if($paymentType == "insurance"){
								$status = $billStatus;
							}
							else{
								$status = 'unpaid';
							}
						}
					}
					$query  = "SELECT ledger_id,gl_account_id,account_payable_id FROM procedures WHERE id = " . $id . "";
			        $result = mysqli_query($conn, $query);
			        $ledgerId;
			        $glAccountId;
			        $accountPayable;
			        while ($r = mysqli_fetch_assoc($result)) {
			            $ledgerId = $r['ledger_id'];
			            $glAccountId = $r['gl_account_id'];
			            $accountPayable = $r['account_payable_id'];
			        }
					if ($counter > 0) {
						$subQuery .= ",";
						$subQueryBill .= ",";
					}
					$subQuery .= "(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . ",'" . $patientId . "','" . $visitId . "','" . $id . "',UNIX_TIMESTAMP()   ,
						'" . $status . "','pending','" . $price . "','A')";

					$subQueryBill .= "(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . ",'" . $patientId . "','" . $visitId . "','" . $id . "',
						'" . $price . "','" . $status . "','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$paymentType."')";
					$counter++;
					//$desc = $desc.' Service:'.$testName.', Price:'.$price.',';
				}
				$insertQuery .= $insert_procedure . $subQuery . ";";
				$insertQuery .= $insert_procedure_bill . $subQueryBill . ";";
				/*$insertTransaction = "INSERT INTO transactions (patient_id,transaction_type_id,credit,debit,processed_on,processed_by,details)
				values ('" . $patientId . "',' 5 ','0','0',UNIX_TIMESTAMP(),'".$userId ."','".$desc."')";
				mysqli_query($conn,$insertTransaction); */
			}
			
			if (!empty($bigPrice_r)) {

				//query to insert value
				$insert_radiology_test = "INSERT INTO radiology_test_request(ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,radiology_test_id
					,request_date,pay_status,test_status,cost,status) VALUES";
				
				//query to insert bill
				$insert_radiology_test_bill = "INSERT INTO patient_radiology_bill (ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,
					radiology_test_id,charge,status,created_by,updated_by,created_on,updated_on,payment_mode) VALUES";
				$counter               = 0;
				$subQuery              = '';
				$subQueryBill ='';

				foreach ($bigPrice_r as $value) {
					$id    = 0;
					$price = 0;
					
					foreach ($value as $key => $val) {
						if ($key == 0) {
							$id = $val;
						}
						if ($key == 1) {
							$price = $val;
						}
						if ($key == 3) {
							$paymentType = $val;

							if($paymentType == "insurance"){
								$status = $billStatus;
							}
							else{
								$status = 'unpaid';
							}
						}
					}
					 $query  = "SELECT ledger_id,gl_account_id,account_payable_id FROM radiology_tests WHERE id = " . $id . "";
			        $result = mysqli_query($conn, $query);
			        $ledgerId;
			        $glAccountId;
			        $accountPayable;
			        while ($r = mysqli_fetch_assoc($result)) {
			            $ledgerId = $r['ledger_id'];
			            $glAccountId = $r['gl_account_id'];
			            $accountPayable = $r['account_payable_id'];
			        }

					if ($counter > 0) {
						$subQuery .= ",";
						$subQueryBill .= ",";
					}
					$subQuery .= " (" . $ledgerId . "," . $glAccountId . "," . $accountPayable . ",'" . $patientId . "','" . $visitId . "','" . $id . "',UNIX_TIMESTAMP(),
						'" . $status . "','pending','" . $price . "','A')";
					
					$subQueryBill .= "(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . ",'" . $patientId . "','" . $visitId . "','" . $id . "',
						'" . $price . "','" . $status . "','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$paymentType."')";
					$counter++;
				}
				$insertQuery .= $insert_radiology_test . $subQuery . ";";
				$insertQuery .= $insert_radiology_test_bill . $subQueryBill . ";";
			}
			
			if (!empty($bigPrice_s)) {

				//query to insert value
				$insert_services = "INSERT INTO services_request(ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,service_id
					,request_date,pay_status,test_status,cost,status) VALUES";
				
				//query to insert bill
				$insert_services_test_bill = "INSERT INTO patient_service_bill (ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,
					service_id,charge,status,created_by,updated_by,created_on,updated_on,payment_mode) VALUES";
				$counter         = 0;
				$subQuery        = '';
				$subQueryBill    = '';

				foreach ($bigPrice_s as $value) {
					$id          = 0;
					$price       = 0;
					$price       = "";

					foreach ($value as $key => $val) {
						if ($key == 0) {
							$id = $val;
						}
						if ($key == 1) {
							$price = $val;
						}
						if ($key == 3) {
							$paymentType = $val;

							if($paymentType == "insurance"){
								$status = $billStatus;
							}
							else{
								$status = 'unpaid';
							}
						}
					}
					$query  = "SELECT ledger_id,gl_account_id,account_payable_id FROM services WHERE id = " . $id . "";
			        $result = mysqli_query($conn, $query);
			        $ledgerId;
			        $glAccountId;
			        $accountPayable;
			        while ($r = mysqli_fetch_assoc($result)) {
			            $ledgerId = $r['ledger_id'];
			            $glAccountId = $r['gl_account_id'];
			            $accountPayable = $r['account_payable_id'];
			        }
					if ($counter > 0) {
						$subQuery .= ",";
						$subQueryBill .= ",";
					}
					$subQuery .= " (" . $ledgerId . "," . $glAccountId . "," . $accountPayable . ",'" . $patientId . "','" . $visitId . "','" . $id . "',UNIX_TIMESTAMP(),
							'" . $status . "','pending','" . $price . "','A') ";
					
					$subQueryBill .= "(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . ",'" . $patientId . "','" . $visitId . "','" . $id . "',
						'" . $price . "','" . $status . "','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$paymentType."')";

					$counter++;
					
				}
				$insertQuery .= $insert_services . $subQuery . ";";
				$insertQuery .= $insert_services_test_bill . $subQueryBill . ";";
			}
			
			if (!empty($bigPrice_f)) {

				//query to insert value
				$insert_forensic_test = "INSERT INTO forensic_test_request(ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,forensic_test_id
					,request_date,pay_status,test_status,cost,created_by,created_on,status) VALUES";
				
				//query to insert bill
				$insert_forensic_test_bill = "INSERT INTO patient_forensic_bill (ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,
					forensic_test_id,charge,status,created_by,updated_by,created_on,updated_on,payment_mode) VALUES";
				$counter              = 0;
				$subQuery             = '';
				$subQueryBill         = '';

				foreach ($bigPrice_f as $value) {
					$id    = 0;
					$price = 0;
					
					foreach ($value as $key => $val) {
						if ($key == 0) {
							$id = $val;
						}
						if ($key == 1) {
							$price = $val;
						}
						if ($key == 3) {
							$paymentType = $val;

							if($paymentType == "insurance"){
								$status = $billStatus;
							}
							else{
								$status = 'unpaid';
							}
						}
					}
					$query  = "SELECT ledger_id,gl_account_id,account_payable_id FROM forensic_tests WHERE id = " . $id . "";
			        $result = mysqli_query($conn, $query);
			        $ledgerId;
			        $glAccountId;
			        $accountPayable;
			        while ($r = mysqli_fetch_assoc($result)) {
			            $ledgerId = $r['ledger_id'];
			            $glAccountId = $r['gl_account_id'];
			            $accountPayable = $r['account_payable_id'];
			        }
					if ($counter > 0) {
						$subQuery .= ",";
						$subQueryBill .= ",";
					}
					$subQuery .= " (" . $ledgerId . "," . $glAccountId . "," . $accountPayable . ",'" . $patientId . "','" . $visitId . "','" . $id . "',UNIX_TIMESTAMP(),
						'" . $status . "','pending','" . $price . "','".$userId."',UNIX_TIMESTAMP(),'A')";
					
					$subQueryBill .= "(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . ",'" . $patientId . "','" . $visitId . "','" . $id . "',
						'" . $price . "','" . $status . "','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$paymentType."')";

					$counter++;
				}
				$insertQuery .= $insert_forensic_test . $subQuery . ";";
				$insertQuery .= $insert_forensic_test_bill . $subQueryBill . ";";
			}
			
			if (!empty($bigPrice_d)) {
				$queryUpdate = "UPDATE visits SET prescription_status ='Yes' WHERE id = '".$visitId."'";
				//query to insert value
				 $sqlPharmacyBill = "INSERT INTO patient_pharmacy_bill(visit_id, patient_id, amount,net_amount,discount,status,created_on,updated_on,created_by,updated_by)
                VALUES"; 
				$insert_drug = "INSERT INTO patients_prescription_request(patient_id,visit_id,  drug_id,quantity,cost,prescribe_by,dispense_status,payment_mode,insurance_payment) VALUES";
								
				$counter              = 0;
				$subQuery             = '';
				$totalAmount 		  = 0;

				foreach ($bigPrice_d as $value) {
					$id    = 0;
					$price = 0;
					$quantity = 0;
					
					foreach ($value as $key => $val) {
						if ($key == 0) {
							$id = $val;
						}
						if ($key == 1) {
							$price = $val;
							$totalAmount =+ $price;
						}
						if ($key == 3) {
							$quantity = $val;
						}
						if ($key == 4) {
							$paymentType = $val;

							if($paymentType == "insurance"){
								$status = $billStatus;
							}
							else{
								$status = 'unpaid';
							}
						}
					}
					if ($counter > 0) {
						$subQuery .= ",";
					}
					$subQuery .= " ('" . $patientId . "','" . $visitId . "','" . $id . "',
						'" . $quantity . "','" . $price . "','Self','Pending','" . $paymentType . "','" . $status . "')";					
					
					$counter++;
				}
				if($billStatus == 'paid'){
					$subBillQuery = " ('".$visitId."', '".$patientId."','".$totalAmount."','".$totalAmount."','0','paid',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
					$insertQuery .= $sqlPharmacyBill . $subBillQuery . ";";
				}
				
				$insertQuery .= $insert_drug . $subQuery . ";".$queryUpdate. ";";
				
			}

			
			if (!empty($bigPrice_l)) {				
				$status ='';

				foreach ($Price as $value) {

					foreach ($value as $values) {

						$labId    = $values->labId;
						$price    = $values->total;
						//$paymentType    = $values->paymentType;
						 $query  = "SELECT ledger_id,gl_account_id,account_payable_id FROM lab_tests WHERE id = " . $labId . "";
			            $result = mysqli_query($conn, $query);
			            $ledgerId;
			            $glAccountId;
			            $accountPayable;
			            while ($r = mysqli_fetch_assoc($result)) {
			                $ledgerId = $r['ledger_id'];
			                $glAccountId = $r['gl_account_id'];
			                $accountPayable = $r['account_payable_id'];
			            }

						//query to insert bill
						$insert_lab_test_bill = "INSERT INTO patient_lab_bill (ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,
						lab_test_id,charge,status,created_by,updated_by,created_on,updated_on,payment_mode) VALUES(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . ",'" . $patientId . "','" . $visitId . "','" . $labId . "',
							'" . $price . "','unpaid','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'')";
						$resultbill             = mysqli_query($conn, $insert_lab_test_bill);
						$patientLabBillId =  mysqli_insert_id($conn);

						$sql = "INSERT INTO lab_test_requests(ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,lab_test_id
							,request_date,pay_status,test_status,cost,created_by,created_on,status)
							 VALUES(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . ",'" . $patientId . "','" . $visitId . "','" . $labId . "',UNIX_TIMESTAMP(),
								'unpaid','pending','" . $price . "','".$userId."',UNIX_TIMESTAMP(),'A')";
						$resultlab             = mysqli_query($conn, $sql);
						$labTestId = mysqli_insert_id($conn);

						foreach ($bigPrice_l as $val) {
							
							foreach ($val as $vals) {

								$id    = $vals->labId;
								$componentId    = $vals->componentId;
								$paymentType    = $vals->paymentType;
								if($paymentType == "insurance"){
									$status = $billStatus;
								}
								else{
									$status = 'unpaid';
								}
								$cost    = $vals->labBill;
								if($id == $labId){
									$sqlComponent = "INSERT INTO lab_test_request_component(lab_test_request_id,patient_lab_bill_id,cost,payment_mode,created_by,created_on,lab_component_id,pay_status)
									 VALUES('" . $labTestId . "','" . $patientLabBillId . "','" . $cost . "','" . $paymentType . "','".$userId."',UNIX_TIMESTAMP(),'" . $componentId . "','" . $status . "')";
									$resultcomponent             = mysqli_query($conn, $sqlComponent);
									
								}
							}
						}
					}
				}
			}

			$result = mysqli_multi_query($conn,$insertQuery);
			if($result == 1){
				$visitData = array($visitId,$visitPrefix);
				echo json_encode($visitData);
			}
			else{
				$visitData = array($visitId,$visitPrefix);
				echo json_encode($visitData);
			}
		}
		else{
			echo "0";
		}  
	}

// get revised cost
if ($operation == "revisedCostDetail") {
    $insuranceCompanyId = $_POST['insuranceCompanyId'];
    $insurancePlanId = $_POST['insurancePlanId'];
    $insurancePlanNameid = $_POST['insurancePlanNameid'];
    $componentId = $_POST['componentId'];
    $activeTab = $_POST['activeTab'];
	
	$sqlSelect = "SELECT id FROM scheme_exclusion 
		WHERE item_id ='" . $componentId . "' AND insurance_company_id ='" . $insuranceCompanyId . "'  
		AND scheme_plan_id ='" . $insurancePlanId . "'AND scheme_name_id ='" . $insurancePlanNameid . "' 
		And status='A' AND service_type = '".$activeTab."'";
    
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $rows_count   = mysqli_num_rows($resultSelect);
	if ($rows_count == 0) {
	
		$selectRevisedCostDetail = "SELECT revised_cost from insurance_revised_cost
			WHERE insurance_revised_cost.insurance_company_id = '".$insuranceCompanyId."' AND
			insurance_revised_cost.scheme_name_id = '".$insurancePlanNameid."' AND
			insurance_revised_cost.scheme_plan_id = '".$insurancePlanId."' AND
			insurance_revised_cost.service_type = '".$activeTab."' AND
			item_id = '".$componentId."'";

		$resultRevisedCostDetail = mysqli_query($conn, $selectRevisedCostDetail);
		$rows   = array();
		while ($r = mysqli_fetch_assoc($resultRevisedCostDetail)) {
			$rows[] = $r;
		}
		print json_encode($rows);
	}
	else{
		echo '0';
	}
}
?>