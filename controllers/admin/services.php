<?php

	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	
    $operation = "";
	$diseasesCategory ="";
	$description = "";
	$createdate = date("Y/m/d") ;
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	else{}
	
	if($operation == "showtype")   // show category in category selection box
	{
		$query="SELECT id,type FROM service_type where status = 'A' ORDER BY type";
  		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		$rows[] = $r;
		}
		print json_encode($rows);
	}
	
	if($operation == "checkservices") // check duplicacy
	{		
		$code=$_POST['Code'];
		$id=$_REQUEST['Id'];
		if($id != "")
		{
			$query = "Select code from services where  code = '".$code."' and id != ".$id."";
		}
		else
		{
			$query = "Select code from services where code = '".$code."'";
		}
		$sqlSelect = mysqli_query($conn,$query);
		$numrows = mysqli_num_rows($sqlSelect);
		if($numrows > 0)
		{
			echo "1";
		}
		else{
			echo "0";
		}   
	}
	
	//operation for save the data
	if($operation == "save")			
	{
		$servicetype=$_POST['Servicetype'];
		$code=$_POST['Code'];
		$name=$_POST['name'];
		$unitCost=$_POST['UnitCost'];		
		$desc=$_POST['Desc'];
		if (isset($_POST['ledger'])) {
			$ledger=$_POST['ledger'];
		}
		if (isset($_POST['glAccount'])) {
			$glAccount=$_POST['glAccount'];
		}
		if (isset($_POST['accountPayable'])) {
			$accountPayable=$_POST['accountPayable'];
		}

		$sql = "INSERT INTO services(service_type_id,ledger_id,gl_account_id,account_payable_id,name,code,cost,description) 
		VALUES('".$servicetype."','".$ledger."','".$glAccount."','".$accountPayable."','".$name."','".$code."','".$unitCost."','".$desc."')";
		$result= mysqli_query($conn,$sql);  
		$id = mysqli_insert_id($conn);

		$query = "INSERT INTO `insurance_revised_cost`(`insurance_company_id`, `scheme_plan_id`, `scheme_name_id`,`item_name`, `service_type`, `item_id`, 
        `actual_cost`,`revised_cost`, `created_on`, `created_by`, `status`)
         SELECT  
        insurance_company_id ,scheme_plan_id , scheme_name_id
        ,'".$name."' AS item_name ,'service' AS service_type , '".$id."' As item_id, '".$unitCost."' AS
        actual_cots, '".$unitCost."' AS revised_cost,UNIX_TIMESTAMP() AS created_on ,'".$userId."' 
        AS created_by, 'A' AS `status`   FROM insurance_revised_cost GROUP BY scheme_name_id ";
        $result = mysqli_query($conn, $query);
    	echo $result;
	} 	
	
	//operation for show the active data
	if ($operation == "show"){
		
		$query= "select services.*,service_type.type from services 
		LEFT JOIN service_type ON services.service_type_id=service_type.id 
		where services.status='A' and  service_type.status='A'";
		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);	
	
	}
	
	// opertaion form update
	if ($operation == "update")
	{
		if (isset($_POST['Id'])) {
		$id = $_POST['Id'];
		}
		if (isset($_POST['Code'])) {
		$code=$_POST['Code'];
		}
		if (isset($_POST['name'])) {
		$name=$_POST['name'];
		}
		$unitCost=$_POST['UnitCost'];
		$desc=$_POST['desc'];
		if (isset($_POST['ledger'])) {
			$ledger=$_POST['ledger'];
		}
		if (isset($_POST['glAccount'])) {
			$glAccount=$_POST['glAccount'];
		}
		if (isset($_POST['accountPayable'])) {
			$accountPayable=$_POST['accountPayable'];
		}

		
		$servicetype=$_POST['ServiceType'];
		$sql = "update services set service_type_id='".$servicetype."',gl_account_id = '".$glAccount."', ledger_id= '".$ledger."',account_payable_id= '".$accountPayable."',code= '".$code."' , name = '".$name."', cost='".$unitCost."',description='".$desc."' where id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//  operation for delete
	if ($operation == "delete")
	{
		if (isset($_POST['id'])) {
		$id = $_POST['id'];
		}
		
		$sql = "UPDATE services SET status= 'I' where id = '".$id."' ";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//When checked box is check
	if ($operation == "checked") {
		
		$query= "select services.*,service_type.type from services LEFT JOIN service_type ON services.service_type_id=service_type.id where services.status='I' or  service_type.status='I'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 //print json_encode($rows);
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);	
	}
	
	//operation for restore thew data
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		$serviceType=$_POST['ServiceType'];
		$sql = "UPDATE services SET status= 'A'  WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		if($result == 1){
			$query = "SELECT status from service_type where id='".$serviceType."'";
			$result= mysqli_query($conn,$query);
			while($r = mysqli_fetch_array($result)) {
				if($r['status'] == 'A'){
					echo "1";	
				}
				else{
					echo "0";
				}
			}
		}
	}
?>