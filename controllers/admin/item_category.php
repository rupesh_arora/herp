<?php
/*****************************************************************************************************
 * File Name    :   item_category.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   24th feb, 2016
 * Description  :   This page  manages item category data
 ****************************************************************************************************/
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}

    $operation = "";
	$categoriesName ="";
	$description = "";
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	
	//This operation is used for save the data 
	if($operation == "save")			
	{
		if (isset($_POST['categoriesName'])) {
			$categoriesName=$_POST['categoriesName'];
		}
		if (isset($_POST['description'])) {
			$description=$_POST['description'];
		}
		if (isset($_POST['inventoryTypeId'])) {
			$inventoryTypeId=$_POST['inventoryTypeId'];
		}
		
		$sql = "INSERT INTO item_categories(category,description,inventory_type_id,created_on,updated_on,created_by,updated_by,status) 
		VALUES('".$categoriesName."','".$description."','".$inventoryTypeId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."','A')";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	} 

	//This function is used for update the data 
	if($operation == "update")			
	{
		if (isset($_POST['categoriesName'])) {
			$categoriesName=$_POST['categoriesName'];
		}
		if (isset($_POST['description'])) {
			$description=$_POST['description'];
		}
		if (isset($_POST['inventoryTypeId'])) {
			$inventoryTypeId=$_POST['inventoryTypeId'];
		}
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}		
		
		$sql = "UPDATE item_categories SET category= '".$categoriesName."',
			description= '".$description."', inventory_type_id = '".$inventoryTypeId."',updated_on=UNIX_TIMESTAMP(),updated_by='".$userId."' 
			WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//This operation is used for update the status 
	if($operation == "delete")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}		
	
		$sql = "UPDATE item_categories SET status= 'I' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//This operation is used for restore the data 
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		
		$sql = "UPDATE item_categories SET status= 'A' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//This operation is used for show the active data 
	if($operation == "show"){
		
		$query= "SELECT  item_categories.*,inventory_type.`type`  from item_categories 
			LEFT JOIN inventory_type ON inventory_type.id = item_categories.inventory_type_id
			WHERE item_categories.status='A'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
	//This operation is used for show the inactive data 
	if($operation == "showInActive"){
		
		$query= "SELECT  item_categories.*,inventory_type.`type`  from item_categories 
			LEFT JOIN inventory_type ON inventory_type.id = item_categories.inventory_type_id
			WHERE item_categories.status='I'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	if ($operation == "showInventoryType") {

	    $query  = "SELECT id,type FROM inventory_type WHERE status='A' ORDER BY type";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	
?>