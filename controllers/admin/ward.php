<?php
	/*	File Name    :   ward.php
	Company Name :   Qexon Infotech
	Created By   :   Rupesh Arora
    Created Date :   30th Dec, 2015
	Description  :   This page  manages  all the wards*/
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
    $operation = "";
	$wardName ="";
	$description = "";
	$date = new DateTime();//get today date
	/*include config file*/
	include 'config.php';
	/*checking operation set or not*/
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}

	/*save operation*/
	if($operation == "save")			
	{
		if (isset($_POST['wardName'])) {
		$wardName=$_POST['wardName'];
		}
		if (isset($_POST['wardType'])) {
		$wardType=$_POST['wardType'];
		}
		if (isset($_POST['description'])) {
		$description=$_POST['description'];
		}
		if (isset($_POST['department'])) {
		$department=$_POST['department'];
		}
		/*check that ward for particular name and type exist or not*/
		$sqlSelect = "SELECT name from wards where name='".$wardName."' AND ward_type='".$wardType."'";
		$resultSelect=mysqli_query($conn,$sqlSelect);
		$rows_count= mysqli_num_rows($resultSelect);
		if($rows_count <= 0){//if count is smaller then zero then insert ward
			
			$sql = "INSERT INTO wards(name,ward_type,department,description,created_on,updated_on,created_by,updated_by) VALUES('".$wardName."','".$wardType."',
			'".$department."','".$description."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
			$result= mysqli_query($conn,$sql);  
			echo $result;
		}
		else{
			echo "0";
		}
	}
	
	/*update operation*/
	if($operation == "update")			
	{
		if (isset($_POST['wardName'])) {
			$wardName=$_POST['wardName'];
		}
		if (isset($_POST['description'])) {
			$description=$_POST['description'];
		}
		if (isset($_POST['wardType'])) {
		$wardType=$_POST['wardType'];
		}
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		if (isset($_POST['department'])) {
		$department=$_POST['department'];
		}
		/*check that ward for particular name ,type and id exist or not*/
		$sqlSelect = "SELECT name from wards where name='".$wardName."' AND ward_type='".$wardType."' and id != '$id'";
		$resultSelect=mysqli_query($conn,$sqlSelect);
		$rows_count= mysqli_num_rows($resultSelect);
		if($rows_count == 0){//if count is equlas to zero then insert ward
			$sql = "UPDATE wards SET name= '".$wardName."',ward_type ='".$wardType."',department ='".$department."',description='".$description."',updated_on=UNIX_TIMESTAMP(),updated_by='".$userId."' WHERE  id = '".$id."'";
			$result= mysqli_query($conn,$sql);  
			echo $result;
		}
		else{
			echo "0";
		}	
	}
	
	/*delete operation*/
	if($operation == "delete")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		
		$sql = "UPDATE wards SET status= 'I' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	/*operation to show department*/
	if ($operation == "showDepartment") {
		$query  = "Select id,department FROM department WHERE status='A' ORDER BY department";
		$result = mysqli_query($conn, $query);
		$rows   = array();
		while ($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		print json_encode($rows);
	}

	/*restore operation*/
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		
		$sql = "UPDATE wards SET status= 'A' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	/*show operation to display data in datatable*/
	if($operation == "show"){
		$query= "select wards.*,department.department AS departmentName from wards 
				LEFT JOIN department on department.id = wards.department WHERE wards.status= 'A'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}

		//print json_encode($rows);		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
	/*showInActive operation to display inactive data in datatable*/
	if($operation == "showInActive"){
		$query= "select wards.*,department.department AS departmentName from wards 
		LEFT JOIN department on department.id = wards.department WHERE wards.status= 'I'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 //print json_encode($rows);
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
	
?>