<?php
/* ****************************************************************************************************
 * File Name    :   lab_test.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages lab test data
 *************************************************************************************************** */
    session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
    $operation = "";
	$labType = "";
	$specimenType = "";
	$unitMeasure = "";
	$testName = "";
	$testFee = "";
	$description = "";
	$date = new DateTime();
	
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	
	// It query use for SELECT the wards
	if($operation == "showLabType")			
	{		
		$query="SELECT id,type FROM lab_types WHERE status='A' ORDER BY type";		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 print json_encode($rows);
	} 
	
	//It is use for SELECT the units 
	if($operation == "showUnitMeasure")			
	{		
		$query="SELECT id,unit_abbr FROM units WHERE status='A' ORDER BY unit_abbr";		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 print json_encode($rows);
	}
	
	if($operation == "showSpecimenType")			
	{		
		$query="SELECT id,type FROM specimens_types WHERE status='A' ORDER BY type";		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 print json_encode($rows);
	}
	
	// It is use for save the data
	if($operation == "save")			
	{
		if (isset($_POST['labType'])) {
			$labType=$_POST['labType'];
		}
		if (isset($_POST['ledger'])) {
			$ledger=$_POST['ledger'];
		}
		if (isset($_POST['glAccount'])) {
			$glAccount=$_POST['glAccount'];
		}
		if (isset($_POST['accountPayable'])) {
			$accountPayable=$_POST['accountPayable'];
		}
		if (isset($_POST['specimenType'])) {
			$specimenType=$_POST['specimenType'];
		}
		if (isset($_POST['testName'])) {
			$testName=$_POST['testName'];
		}
		if (isset($_POST['description'])) {
			$description=$_POST['description'];
		}
		if (isset($_POST['testCode'])) {
			$testCode=$_POST['testCode'];
		}
		$sql1 = "SELECT name from lab_tests where name='".$testName."' And lab_type_id='".$labType."'";
		$resultSELECT=mysqli_query($conn,$sql1);
		$rows_count= mysqli_num_rows($resultSELECT);
		
		if($rows_count <= 0){

		$sqlTestCode =  "SELECT test_code from lab_tests where test_code='".$testCode."'";
		$resultCode=mysqli_query($conn,$sqlTestCode);
		$rows_count_code= mysqli_num_rows($resultCode);

			if($rows_count_code > 0){
				echo "Code already exist";
				exit();
			}

			 $sql = "INSERT INTO lab_tests(name,lab_type_id,ledger_id,gl_account_id,account_payable_id,specimen_id,
			 description,created_on,updated_on,test_code,created_by,updated_by,status)	VALUES('".$testName."','".$labType."','".$ledger."','".$glAccount."','".$accountPayable."','".$specimenType."','".$description."',
				UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$testCode."','".$userId."','".$userId."','A')";

			$result= mysqli_query($conn,$sql);  
			
			$sql_id   = "SELECT LAST_INSERT_ID()";
			$query_id = mysqli_query($conn, $sql_id);
			while ($row = mysqli_fetch_row($query_id)) {
				$labTestId = $row[0];
			}
			$result;
			if (isset($_POST['labTestComponent'])) {
				$labTestComponent=json_decode($_POST["labTestComponent"]);
				
				foreach($labTestComponent as $value) {
					
					$unitId = $value-> unitId;
					$componentName = $value-> componentName;
					$componentRange = $value-> componentRange;
					$testFee = $value-> testFee;
					
					$sql = "INSERT INTO lab_test_component(lab_test_id, unit_id, name,fee,normal_range,created_on,updated_on,created_by,updated_by,status)
					VALUES('".$labTestId."', '".$unitId."','".$componentName."','".$testFee."','".$componentRange."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."','A')";		
					$result= mysqli_query($conn,$sql);

					$id = mysqli_insert_id($conn);

					$query = "INSERT INTO `insurance_revised_cost`(`insurance_company_id`, `scheme_plan_id`, `scheme_name_id`,`item_name`, `service_type`, `item_id`, 
			        `actual_cost`,`revised_cost`, `created_on`, `created_by`, `status`)
			         SELECT  
			        insurance_company_id ,scheme_plan_id , scheme_name_id
			        ,'".$componentName."' AS item_name ,'lab' AS service_type , '".$id."' As item_id, '".$testFee."' AS
			        actual_cots, '".$testFee."' AS revised_cost,UNIX_TIMESTAMP() AS created_on ,'".$userId."' 
			        AS created_by, 'A' AS `status`   FROM insurance_revised_cost GROUP BY scheme_name_id ";
			        $result = mysqli_query($conn, $query);
					
				}				
			}
			echo $result;
		}
		else{
			echo "0";
		}
	} 
	
	
	if($operation == "tblSowData"){
		
		if (isset($_POST['labTestId'])) {
			$labTestId=$_POST['labTestId'];
		}
		
		$query= "SELECT l.id,l.lab_test_id,l.unit_id,l.fee,l.name,l.normal_range,u.unit from lab_test_component AS l 
			LEFT JOIN units AS u ON l.unit_id=u.id 
			WHERE l.lab_test_id = '".$labTestId."'";		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		echo json_encode($rows);
	}
	
	//Operation for show the active data
	if($operation == "show"){
		
		$query= "SELECT l.id,l.ledger_id AS ledger,l.gl_account_id AS gl_account,l.account_payable_id AS account_payable,l.name,l.lab_type_id,l.specimen_id, l_t.type as lab_type,
		s.type as specimen_type,l.description ,l.test_code  from lab_tests AS l 
		LEFT JOIN lab_types AS l_t ON l.lab_type_id=l_t.id 
		LEFT JOIN specimens_types AS s ON l.specimen_id=s.id  
		WHERE l_t.status='A' And l.status='A'";		
		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);
	}
	
	//Operation for show the inactive data
	if($operation == "showInActive"){
		
		$query= "SELECT l.id,l.ledger_id AS ledger,l.gl_account_id AS gl_account,l.account_payable_id  AS account_payable,l.name,l.lab_type_id,l.specimen_id, l_t.type as lab_type,
		s.type as specimen_type,l.description,l.test_code from lab_tests AS l 
		LEFT JOIN lab_types AS l_t ON l.lab_type_id=l_t.id 
		LEFT JOIN specimens_types AS s ON l.specimen_id=s.id   
		WHERE l_t.status='I' Or l.status='I'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);
	}
	
	//Operation for update the status
	if($operation == "delete")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		
		$sql = "UPDATE lab_tests SET status= 'I'  WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//Operation for update the status
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		if (isset($_POST['labType'])) {
			$labType=$_POST['labType'];
		}
		
		$sql = "UPDATE lab_tests SET status= 'A' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);
		
		if($result == 1){
			$query_lab_type = "SELECT status from lab_types where id='".$labType."' And status='A'";
			$result_lab_type= mysqli_query($conn,$query_lab_type);
			$count_lab_type= mysqli_num_rows($result_lab_type);
			if($count_lab_type == '1'){
			echo "Restored Successfully!!!";	
			}
			else{
				echo "It can not be restore!!!";				
			}
		}
	}
	
	//operation for update the data
	if($operation == "update")			
	{
		if (isset($_POST['labType'])) {
			$labType=$_POST['labType'];
		}
		if (isset($_POST['ledger'])) {
			$ledger=$_POST['ledger'];
		}
		if (isset($_POST['glAccount'])) {
			$glAccount=$_POST['glAccount'];
		}
		if (isset($_POST['accountPayable'])) {
			$accountPayable=$_POST['accountPayable'];
		}
		if (isset($_POST['specimenType'])) {
			$specimenType=$_POST['specimenType'];
		}
		if (isset($_POST['testName'])) {
			$testName=$_POST['testName'];
		}
		if (isset($_POST['description'])) {
			$description=$_POST['description'];
		}
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		if (isset($_POST['testCode'])) {
			$testCode=$_POST['testCode'];
		}
		 $sql1 = "SELECT name from lab_tests where name='".$testName."' And lab_type_id='".$labType."' and id != '".$id."'";
	    $resultSELECT=mysqli_query($conn,$sql1);
		$rows_count= mysqli_num_rows($resultSELECT);

		if($rows_count == 0){

			 $sqlTestCode =  "SELECT test_code from lab_tests where test_code='".$testCode."' AND id != '$id'";
			$resultCode=mysqli_query($conn,$sqlTestCode);
			$rows_count_code= mysqli_num_rows($resultCode);

			if($rows_count_code > 0){
				echo "Code already exist";
				exit();
			}

			$sql = "UPDATE lab_tests SET name= '".$testName."',gl_account_id = '".$glAccount."', ledger_id= '".$ledger."',account_payable_id= '".$accountPayable."',lab_type_id= '".$labType."',specimen_id= '".$specimenType."',description='".$description."',updated_on=UNIX_TIMESTAMP(),
			test_code = '".$testCode."',updated_by='".$userId."' WHERE  id = '".$id."'";
			$result= mysqli_query($conn,$sql); 
			
			$sqlDelete = "DELETE  FROM lab_test_component WHERE lab_test_id = '".$id."'";		
			$resultDelete= mysqli_query($conn,$sqlDelete);
			if($resultDelete != "0" && $resultDelete != ""){
				if (isset($_POST['updateLabComponent'])) {
				$updateLabComponent=json_decode($_POST["updateLabComponent"]);
					
					foreach($updateLabComponent as $value) {
						
						$unitId = $value-> unitId;
						$componentName = $value-> componentName;
						$componentRange = $value-> componentRange;
						$testFee = $value-> testFee;
						
						echo $sql = "INSERT INTO lab_test_component(lab_test_id, unit_id, name,fee,normal_range,created_on,updated_on,created_by,updated_by)
						VALUES('".$id."', '".$unitId."','".$componentName."','".$testFee."','".$componentRange."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";		
						$result= mysqli_query($conn,$sql);
						
					}
					echo $result;				
				}
			}
			else{
				echo "Component is used";
			}
		}
		else{
			echo "0";
		}				
	}
?>
