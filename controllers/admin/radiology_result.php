<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	$date      = new DateTime();
	$operation="";
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	if(isset($_FILES['filename']['name'])) {
		$images_arr = array();
		for ($i = 0; $i < count($_FILES['filename']['name']); $i++) {
			//upload and stored images
			$target_dir = "../../upload_images/radio_test_dp/";
			
			$imageString = explode(".",$_FILES['filename']['name'][$i]);
			$imagename = $imageString[0]. $date->getTimestamp();
			$extension = $imageString[1];
			
			$image_name = $imagename . "." . $extension;
			$name       = str_replace(" ", "", $image_name); // remove space
			$target_file = $target_dir.$name;
			
			if(move_uploaded_file($_FILES['filename']['tmp_name'][$i],$target_file)){
				$images_arr[]= $name;
			}
		}
		echo json_encode($images_arr);
	}
	

	if($operation == "loadSpecimen"){
		if (isset($_POST['specimenId'])) {
			$id=$_POST["specimenId"];
		}
		$sql = "SELECT radiology_test_request.patient_id,
		radiology_test_request.visit_id,
		radiology_test_request.request_date,
		radiology_tests.test_code,
		radiology_test_result.report,
		radiology_tests.name,
		radiology_test_request.cost
		FROM radiology_test_request
		LEFT JOIN radiology_tests ON radiology_test_request.radiology_test_id = radiology_tests.id 
		LEFT JOIN patients On patients.id = radiology_test_request.patient_id
		LEFT JOIN radiology_test_result On radiology_test_result.request_id = radiology_test_request.id
		WHERE  radiology_test_result.id = '".$id."' AND radiology_test_request.pay_status = 'paid' AND
		radiology_test_request.test_status = 'done'";
		
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		echo json_encode($rows);			
	}
	if($operation == "saveResult"){
		if (isset($_POST['txtSpecimenId'])) {
			$specimenId=$_POST['txtSpecimenId'];
		}
		/* if (isset($_POST['txtNormalRange'])) {
			$range=$_POST['txtNormalRange'];
		}
		if (isset($_POST['txtResult'])) {
			$result=$_POST['txtResult'];
		} */
		
		if (isset($_POST['txtReport'])) {
			$report=$_POST['txtReport'];
		}
		$imageName = $_POST['radioImage']; 
		

		$sql_select = "SELECT COUNT(*) from radiology_test_result where id =".$specimenId."";  
		$result_select = mysqli_query($conn,$sql_select);  
		while ($row=mysqli_fetch_row($result_select))
		{
			$count = $row[0];
		}
		
		if($count == 0)
		{			
			$sql = "INSERT INTO `radiology_test_result`(`request_id`,`report`, images_path,`created_on`, `updated_on`, `created_by`, `updated_by`)
			 VALUES('".$specimenId."','".$report."','".$imageName."', UNIX_TIMESTAMP(), UNIX_TIMESTAMP(), '".$_SESSION['globaluser']."', '".$_SESSION['globaluser']."')";		
			
			$result= mysqli_query($conn,$sql);
		}
		else{

			$sql = "UPDATE `radiology_test_result` SET 
			`report`='".$report."',`updated_on`= UNIX_TIMESTAMP(),`images_path`='".$imageName."',
			`updated_by`='".$_SESSION['globaluser']."' WHERE id=".$specimenId."";			
			$result= mysqli_query($conn,$sql);			
			if($result){
				$sqlupdate = "update `radiology_test_request` SET `pay_status`='settled',`test_status`='resultsent' 
							  where radiology_test_request.id = (SELECT radiology_test_result.request_id from 
							  radiology_test_result where id ='".$specimenId."')";
				$result1= mysqli_query($conn,$sqlupdate);
			} 
			echo $result;
		}				
	}
?>