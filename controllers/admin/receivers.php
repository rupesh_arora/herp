<?php
/*****************************************************************************************************
 * File Name    :   receivers.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :  	16 feb, 2016
 * Description  :   This page  manages receiver name data
 ****************************************************************************************************/
 	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
    $operation = "";
	$receiverName ="";
	$description = "";
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	
	if (isset($_FILES["file"]["type"])) // use for import data from csv
	 {
		$temporary       = explode(".", $_FILES["file"]["name"]);
		$file_extension = end($temporary);
		if ($file_extension == "csv") {
			if ($_FILES["file"]["error"] > 0) {
				echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
			} else {
				if($_FILES["file"]["size"] > 0)
				{
					$filename = $_FILES["file"]["tmp_name"];
					$file = fopen($filename, "r");
					while (($emapData = fgetcsv($file, 10000, ",")) != FALSE)
					{
						//It wiil insert a row to our subject table from our csv file`
						$sqlInsert = "insert into receiver(name,description) 
							values('$emapData[0]','$emapData[1]')";
				
						//we are using mysql_query function. it returns a resource on true else False on error
						$result = mysqli_query($conn,$sqlInsert);
						
					}
					if($result)
					{
						echo $result;
					}
					else{
						echo "";
					}
					fclose($file);
				}
			} 	
		}
		else if($file_extension == "xls"){
			ini_set("display_errors",1);
			$filename = $_FILES['file']['tmp_name'];
			require_once 'excel_reader2.php';
			$data = new Spreadsheet_Excel_Reader($filename);
			$countSheets = count($data->sheets);
			for($i=0;$i<$countSheets;$i++) // Loop to get all sheets in a file.
			{ 
				$countSheetsCells =0;
				if(isset($data->sheets[$i]['cells'])){
					$countSheetsCells = count($data->sheets[$i]['cells']);
				}
				if($countSheetsCells > 0 && $countSheetsCells) // checking sheet not empty
				{
					 for($j=1;$j<=$countSheetsCells;$j++) // loop used to get each row of the sheet
					 { 
						 
						 $name = "";
						 if(isset($data->sheets[$i]['cells'][$j][1])){
							 $name = $data->sheets[$i]['cells'][$j][1];
						 }
						 
						 $description = "";
						 if(isset($data->sheets[$i]['cells'][$j][2])){
							 $description = $data->sheets[$i]['cells'][$j][2];
						 }
						 
						 $sql = "insert into receiver(name,description)
								VALUES ('$name','$description')";
						
						$result = mysqli_query($conn,$sql);		
					 }
					 if($result)
					{
						echo $result;
					}
					else{
						echo "";
					}
				}
			}		
		}
		else if($file_extension == "xlsx"){
			// get file name
			$filename = $_FILES['file']['tmp_name'];
			
			//import php script
			require_once 'PHPExcel.php';
			
			// 
			try {
				$inputFileType = PHPExcel_IOFactory::identify($filename);  // get type of file name
				$objReader = PHPExcel_IOFactory::createReader($inputFileType); //create object of read file
				$objPHPExcel = $objReader->load($filename);  //load excel into the object
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($filename, PATHINFO_BASENAME) 
				. '": ' . $e->getMessage());
			}

			//  Get worksheet dimensions
			$sheet = $objPHPExcel->getSheet(0); // get sheet 1 of excel file
			$highestRow = $sheet->getHighestRow(); // get heighest row
			$highestColumn = $sheet->getHighestColumn(); // get heighest column
			
			$headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE); // get heading in first row
											
			for ($row = 2; $row <= $highestRow; $row++) {
				//  Read a row of data into an array
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL, TRUE, FALSE);
				
				// combine data in array accoding to heading
				$rowData[0] = array_combine($headings[0], $rowData[0]);
				//var_dump($rowData[0]);
				
				// fetch value from row
				$name =  $rowData[0]['name'];
				$description = $rowData[0]['description'];
				
				// insert data into table
				$sql = "insert into receiver(name,description)
								VALUES ('$name','$description')";
						
				$result = mysqli_query($conn,$sql);			
			 }
			if($result)
			{
				echo $result;
			}
			else{
				echo "";
			}
		}
		else{
			echo "invalid file";
		}
	}

	//This operation is used for save the data 
	if($operation == "save")			
	{
		if (isset($_POST['receiverName'])) {
		$receiverName=$_POST['receiverName'];
		}
		if (isset($_POST['description'])) {
		$description=$_POST['description'];
		}
		
		$sql = "INSERT INTO receiver(name,description,created_on,updated_on,created_by,updated_by) VALUES('".$receiverName."','".$description."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	} 

	//This function is used for update the data 
	if($operation == "update")			
	{
		if (isset($_POST['receiverName'])) {
			$receiverName=$_POST['receiverName'];
		}
		if (isset($_POST['description'])) {
			$description=$_POST['description'];
		}
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}		
		
		$sql = "UPDATE receiver SET name= '".$receiverName."',description= '".$description."',updated_on=UNIX_TIMESTAMP(),updated_by='".$userId."' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//This operation is used for update the status 
	if($operation == "delete")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}		
	
		$sql = "UPDATE receiver SET status= 'I' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//This operation is used for restore the data 
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		
		$sql = "UPDATE receiver SET status= 'A' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//This operation is used for show the active data 
	if($operation == "show"){
		
		$query= "select * from receiver WHERE status='A'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
	//This operation is used for show the inactive data 
	if($operation == "showInActive"){
		
		$query= "select * from receiver WHERE status='I'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
?>