<?php
	
	session_start();
	if(!session_id()){
		exit();
	}
	$toDate = '';
	$fromDate = '';
	
	include_once('config.php');

	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET['operation'])){
		$operation=$_GET["operation"];
	}

	if ($operation=="showChartData") {
		if (isset($_POST['dataLoad'])) {
			$dataLoad = $_POST["dataLoad"];
		}
		if (isset($_POST['fromDate'])) {
			$fromDate = $_POST["fromDate"];
		}
		if (isset($_POST['toDate'])) {
			$toDate = $_POST["toDate"];
		}

		if ($dataLoad =="all") {

			/*Patient lab bill queries*/
			$queryLabBill = "SELECT IFNULL(SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 
			patient_lab_bill.charge ELSE 0 END),'') as 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_lab_bill.`status` = 'Unpaid' 
			THEN patient_lab_bill.charge ELSE 0 END),'') as 'Unpaid Amount'
			FROM patient_lab_bill";

			$sqlLabBill = "SELECT IFNULL(SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 
			patient_lab_bill.charge ELSE 0 END),'') as 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_lab_bill.`status` = 'Unpaid' 
			THEN patient_lab_bill.charge ELSE 0 END),'') as 'Unpaid Amount',lab_tests.name AS 'Lab Test'
			FROM patient_lab_bill
			LEFT JOIN lab_tests ON lab_tests.id = patient_lab_bill.lab_test_id
			GROUP BY lab_tests.id";


			/*Patient radiology bill queries*/

			$queryRadiologyBill = "SELECT IFNULL(SUM(CASE WHEN patient_radiology_bill.`status` = 'paid'
			THEN patient_radiology_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN
			patient_radiology_bill.`status` = 'Unpaid' THEN patient_radiology_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' 
			FROM patient_radiology_bill";

			$sqlRadiologyBill = "SELECT IFNULL(SUM(CASE WHEN patient_radiology_bill.`status` = 'paid'
			THEN patient_radiology_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN
			patient_radiology_bill.`status` = 'Unpaid' THEN patient_radiology_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount',radiology_tests.name AS 'Radiology Test' 
			FROM patient_radiology_bill
			LEFT JOIN radiology_tests ON radiology_tests.id = patient_radiology_bill.radiology_test_id
			GROUP BY radiology_tests.id";


			/*Patient service bill */
			$queryServiceBill = "SELECT IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_service_bill";

			$sqlServiceBill = "SELECT IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' ,services.name 'Service Name' FROM patient_service_bill
			LEFT JOIN services ON services.id = patient_service_bill.service_id 
			GROUP BY services.id";


			/*patient procedure bill*/

			$queryProcedureBill = "SELECT IFNULL(SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' 
			THEN patient_procedure_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_procedure_bill.`status` = 'Unpaid' THEN patient_procedure_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_procedure_bill";

			$sqlProcedureBill = "SELECT IFNULL(SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' 
			THEN patient_procedure_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_procedure_bill.`status` = 'Unpaid' THEN patient_procedure_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount',procedures.name AS 'Procedue Name' 
			FROM patient_procedure_bill
			LEFT JOIN procedures ON procedures.id  = patient_procedure_bill.procedure_id
			GROUP BY procedures.id";

			/*patint forensic bill*/
			$queryForensicBill = "SELECT IFNULL(SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' 
			THEN patient_forensic_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_forensic_bill.`status` = 'Unpaid' THEN patient_forensic_bill.charge ELSE 0 END),'')
			AS 'Unpaid Amount' FROM patient_forensic_bill";

			$sqlForensicBill = "SELECT IFNULL(SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' 
			THEN patient_forensic_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_forensic_bill.`status` = 'Unpaid' THEN patient_forensic_bill.charge ELSE 0 END),'')
			AS 'Unpaid Amount' ,forensic_tests.name AS 'Forensic Test' FROM patient_forensic_bill
			LEFT JOIN forensic_tests ON forensic_tests.id = patient_forensic_bill.forensic_test_id
		 	GROUP BY forensic_tests.id";
		}
		
		else if ($dataLoad =="last30days") {

			/*Patient lab bill queries*/
			$queryLabBill = "SELECT IFNULL(SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 
			patient_lab_bill.charge ELSE 0 END),'') as 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_lab_bill.`status` = 'Unpaid' 
			THEN patient_lab_bill.charge ELSE 0 END),'') as 'Unpaid Amount'
			FROM patient_lab_bill
			WHERE patient_lab_bill.created_on >= unix_timestamp(curdate() - interval 1 month)";

			$sqlLabBill = "SELECT IFNULL(SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 
			patient_lab_bill.charge ELSE 0 END),'') as 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_lab_bill.`status` = 'Unpaid' 
			THEN patient_lab_bill.charge ELSE 0 END),'') as 'Unpaid Amount',lab_tests.name AS 'Lab Test'
			FROM patient_lab_bill
			LEFT JOIN lab_tests ON lab_tests.id = patient_lab_bill.lab_test_id
			WHERE patient_lab_bill.created_on >= unix_timestamp(curdate() - interval 1 month)
			GROUP BY lab_tests.id";


			/*Patient radiology bill queries*/

			$queryRadiologyBill = "SELECT IFNULL(SUM(CASE WHEN patient_radiology_bill.`status` = 'paid'
			THEN patient_radiology_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN
			patient_radiology_bill.`status` = 'Unpaid' THEN patient_radiology_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_radiology_bill
			WHERE patient_radiology_bill.created_on >= unix_timestamp(curdate() - interval 1 month)";

			$sqlRadiologyBill = "SELECT IFNULL(SUM(CASE WHEN patient_radiology_bill.`status` = 'paid'
			THEN patient_radiology_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN
			patient_radiology_bill.`status` = 'Unpaid' THEN patient_radiology_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount',radiology_tests.name AS 'Radiology Test' 
			FROM patient_radiology_bill
			LEFT JOIN radiology_tests ON radiology_tests.id = patient_radiology_bill.radiology_test_id
			WHERE patient_radiology_bill.created_on >= unix_timestamp(curdate() - interval 1 month)
			GROUP BY radiology_tests.id";


			/*Patient service bill */
			$queryServiceBill = "SELECT IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_service_bill
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate() - interval 1 month)";

			$sqlServiceBill = "SELECT IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' ,services.name 'Service Name' FROM patient_service_bill
			LEFT JOIN services ON services.id = patient_service_bill.service_id 			
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate() - interval 1 month)
			GROUP BY services.id";


			/*patient procedure bill*/

			$queryProcedureBill = "SELECT IFNULL(SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' 
			THEN patient_procedure_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_procedure_bill.`status` = 'Unpaid' THEN patient_procedure_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_procedure_bill
			WHERE patient_procedure_bill.created_on >= unix_timestamp(curdate() - interval 1 month)";

			$sqlProcedureBill = "SELECT IFNULL(SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' 
			THEN patient_procedure_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_procedure_bill.`status` = 'Unpaid' THEN patient_procedure_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount',procedures.name AS 'Procedue Name' 
			FROM patient_procedure_bill
			LEFT JOIN procedures ON procedures.id  = patient_procedure_bill.procedure_id
			WHERE patient_procedure_bill.created_on >= unix_timestamp(curdate() - interval 1 month)
			GROUP BY procedures.id";

			/*patint forensic bill*/
			$queryForensicBill = "SELECT IFNULL(SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' 
			THEN patient_forensic_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_forensic_bill.`status` = 'Unpaid' THEN patient_forensic_bill.charge ELSE 0 END),'')
			AS 'Unpaid Amount' FROM patient_forensic_bill
			WHERE patient_forensic_bill.created_on >= unix_timestamp(curdate() - interval 1 month)";

			$sqlForensicBill = "SELECT IFNULL(SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' 
			THEN patient_forensic_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_forensic_bill.`status` = 'Unpaid' THEN patient_forensic_bill.charge ELSE 0 END),'')
			AS 'Unpaid Amount' ,forensic_tests.name AS 'Forensic Test' FROM patient_forensic_bill
			LEFT JOIN forensic_tests ON forensic_tests.id = patient_forensic_bill.forensic_test_id
			WHERE patient_forensic_bill.created_on >= unix_timestamp(curdate() - interval 1 month)
			GROUP BY forensic_tests.id";
		}


		else if ($dataLoad =="last7days") {

			/*Patient lab bill queries*/
			$queryLabBill = "SELECT IFNULL(SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 
			patient_lab_bill.charge ELSE 0 END),'') as 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_lab_bill.`status` = 'Unpaid' 
			THEN patient_lab_bill.charge ELSE 0 END),'') as 'Unpaid Amount'
			FROM patient_lab_bill
			WHERE patient_lab_bill.created_on >= unix_timestamp(curdate() - interval 7 day)";

			$sqlLabBill = "SELECT IFNULL(SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 
			patient_lab_bill.charge ELSE 0 END),'') as 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_lab_bill.`status` = 'Unpaid' 
			THEN patient_lab_bill.charge ELSE 0 END),'') as 'Unpaid Amount',lab_tests.name AS 'Lab Test'
			FROM patient_lab_bill
			LEFT JOIN lab_tests ON lab_tests.id = patient_lab_bill.lab_test_id
			WHERE patient_lab_bill.created_on >= unix_timestamp(curdate() - interval 7 day)
			GROUP BY lab_tests.id";


			/*Patient radiology bill queries*/

			$queryRadiologyBill = "SELECT IFNULL(SUM(CASE WHEN patient_radiology_bill.`status` = 'paid'
			THEN patient_radiology_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN
			patient_radiology_bill.`status` = 'Unpaid' THEN patient_radiology_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_radiology_bill";

			$sqlRadiologyBill = "SELECT IFNULL(SUM(CASE WHEN patient_radiology_bill.`status` = 'paid'
			THEN patient_radiology_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN
			patient_radiology_bill.`status` = 'Unpaid' THEN patient_radiology_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount',radiology_tests.name AS 'Radiology Test' 
			FROM patient_radiology_bill
			LEFT JOIN radiology_tests ON radiology_tests.id = patient_radiology_bill.radiology_test_id
			WHERE patient_radiology_bill.created_on >= unix_timestamp(curdate() - interval 7 day)
			GROUP BY radiology_tests.id";


			/*Patient service bill */
			$queryServiceBill = "SELECT IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_service_bill
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate() - interval 7 day)";

			$sqlServiceBill = "SELECT IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' ,services.name 'Service Name' FROM patient_service_bill
			LEFT JOIN services ON services.id = patient_service_bill.service_id
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate() - interval 7 day)
			GROUP BY services.id";


			/*patient procedure bill*/

			$queryProcedureBill = "SELECT IFNULL(SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' 
			THEN patient_procedure_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_procedure_bill.`status` = 'Unpaid' THEN patient_procedure_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_procedure_bill
			WHERE patient_procedure_bill.created_on >= unix_timestamp(curdate() - interval 7 day)";

			$sqlProcedureBill = "SELECT IFNULL(SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' 
			THEN patient_procedure_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_procedure_bill.`status` = 'Unpaid' THEN patient_procedure_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount',procedures.name AS 'Procedue Name' 
			FROM patient_procedure_bill
			LEFT JOIN procedures ON procedures.id  = patient_procedure_bill.procedure_id
			WHERE patient_procedure_bill.created_on >= unix_timestamp(curdate() - interval 7 day) 
			GROUP BY procedures.id  ";

			/*patint forensic bill*/
			$queryForensicBill = "SELECT IFNULL(SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' 
			THEN patient_forensic_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_forensic_bill.`status` = 'Unpaid' THEN patient_forensic_bill.charge ELSE 0 END),'')
			AS 'Unpaid Amount' FROM patient_forensic_bill
			WHERE patient_forensic_bill.created_on >= unix_timestamp(curdate() - interval 7 day)   ";

			$sqlForensicBill = "SELECT IFNULL(SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' 
			THEN patient_forensic_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_forensic_bill.`status` = 'Unpaid' THEN patient_forensic_bill.charge ELSE 0 END),'')
			AS 'Unpaid Amount' ,forensic_tests.name AS 'Forensic Test' FROM patient_forensic_bill
			LEFT JOIN forensic_tests ON forensic_tests.id = patient_forensic_bill.forensic_test_id
			WHERE patient_forensic_bill.created_on >= unix_timestamp(curdate() - interval 7 day) 
			GROUP BY forensic_tests.id";
		}


		else if ($dataLoad =="today") {

			/*Patient lab bill queries*/
			$queryLabBill = "SELECT IFNULL(SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 
			patient_lab_bill.charge ELSE 0 END),'') as 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_lab_bill.`status` = 'Unpaid' 
			THEN patient_lab_bill.charge ELSE 0 END),'') as 'Unpaid Amount'
			FROM patient_lab_bill
			WHERE patient_lab_bill.created_on >= unix_timestamp(curdate())";

			$sqlLabBill = "SELECT IFNULL(SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 
			patient_lab_bill.charge ELSE 0 END),'') as 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_lab_bill.`status` = 'Unpaid' 
			THEN patient_lab_bill.charge ELSE 0 END),'') as 'Unpaid Amount',lab_tests.name AS 'Lab Test'
			FROM patient_lab_bill
			LEFT JOIN lab_tests ON lab_tests.id = patient_lab_bill.lab_test_id
			WHERE patient_lab_bill.created_on >= unix_timestamp(curdate()) GROUP BY lab_tests.id";


			/*Patient radiology bill queries*/

			$queryRadiologyBill = "SELECT IFNULL(SUM(CASE WHEN patient_radiology_bill.`status` = 'paid'
			THEN patient_radiology_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN
			patient_radiology_bill.`status` = 'Unpaid' THEN patient_radiology_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_radiology_bill
			WHERE patient_radiology_bill.created_on >= unix_timestamp(curdate())";

			$sqlRadiologyBill = "SELECT IFNULL(SUM(CASE WHEN patient_radiology_bill.`status` = 'paid'
			THEN patient_radiology_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN
			patient_radiology_bill.`status` = 'Unpaid' THEN patient_radiology_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount',radiology_tests.name AS 'Radiology Test' 
			FROM patient_radiology_bill
			LEFT JOIN radiology_tests ON radiology_tests.id = patient_radiology_bill.radiology_test_id
			WHERE patient_radiology_bill.created_on >= unix_timestamp(curdate())
			GROUP BY radiology_tests.id ";


			/*Patient service bill */
			$queryServiceBill = "SELECT IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_service_bill
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate())";

			$sqlServiceBill = "SELECT IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' ,services.name 'Service Name' FROM patient_service_bill
			LEFT JOIN services ON services.id = patient_service_bill.service_id
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate()) GROUP BY services.id";


			/*patient procedure bill*/

			$queryProcedureBill = "SELECT IFNULL(SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' 
			THEN patient_procedure_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_procedure_bill.`status` = 'Unpaid' THEN patient_procedure_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_procedure_bill WHERE created_on >= 
			unix_timestamp(curdate())";

			$sqlProcedureBill = "SELECT IFNULL(SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' 
			THEN patient_procedure_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_procedure_bill.`status` = 'Unpaid' THEN patient_procedure_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount',procedures.name AS 'Procedue Name' 
			FROM patient_procedure_bill
			LEFT JOIN procedures ON procedures.id  = patient_procedure_bill.procedure_id
			WHERE patient_procedure_bill.created_on >= unix_timestamp(curdate()) GROUP BY procedures.id";

			/*patint forensic bill*/
			$queryForensicBill = "SELECT IFNULL(SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' 
			THEN patient_forensic_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_forensic_bill.`status` = 'Unpaid' THEN patient_forensic_bill.charge ELSE 0 END),'')
			AS 'Unpaid Amount' FROM patient_forensic_bill
			WHERE patient_forensic_bill.created_on >= unix_timestamp(curdate())";

			$sqlForensicBill = "SELECT IFNULL(SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' 
			THEN patient_forensic_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_forensic_bill.`status` = 'Unpaid' THEN patient_forensic_bill.charge ELSE 0 END),'')
			AS 'Unpaid Amount' ,forensic_tests.name AS 'Forensic Test' FROM patient_forensic_bill
			LEFT JOIN forensic_tests ON forensic_tests.id = patient_forensic_bill.forensic_test_id
			WHERE patient_forensic_bill.created_on >= unix_timestamp(curdate()) 
			GROUP BY forensic_tests.id  ";
		}



		else  {

			/*Patient lab bill queries*/
			$queryLabBill = "SELECT IFNULL(SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 
			patient_lab_bill.charge ELSE 0 END),'') as 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_lab_bill.`status` = 'Unpaid' 
			THEN patient_lab_bill.charge ELSE 0 END),'') as 'Unpaid Amount'
			FROM patient_lab_bill
			WHERE patient_lab_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
			UNIX_TIMESTAMP('".$toDate." 23:59:59')";

			$sqlLabBill = "SELECT IFNULL(SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 
			patient_lab_bill.charge ELSE 0 END),'') as 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_lab_bill.`status` = 'Unpaid' 
			THEN patient_lab_bill.charge ELSE 0 END),'') as 'Unpaid Amount',lab_tests.name AS 'Lab Test'
			FROM patient_lab_bill
			LEFT JOIN lab_tests ON lab_tests.id = patient_lab_bill.lab_test_id			
			WHERE patient_lab_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
			UNIX_TIMESTAMP('".$toDate." 23:59:59') GROUP BY patient_lab_bill.lab_test_id";


			/*Patient radiology bill queries*/

			$queryRadiologyBill = "SELECT IFNULL(SUM(CASE WHEN patient_radiology_bill.`status` = 'paid'
			THEN patient_radiology_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN
			patient_radiology_bill.`status` = 'Unpaid' THEN patient_radiology_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' 
			FROM patient_radiology_bill
			WHERE patient_radiology_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
			UNIX_TIMESTAMP('".$toDate." 23:59:59')";

			$sqlRadiologyBill = "SELECT IFNULL(SUM(CASE WHEN patient_radiology_bill.`status` = 'paid'
			THEN patient_radiology_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN
			patient_radiology_bill.`status` = 'Unpaid' THEN patient_radiology_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount',radiology_tests.name AS 'Radiology Test' 
			FROM patient_radiology_bill
			LEFT JOIN radiology_tests ON radiology_tests.id = patient_radiology_bill.radiology_test_id
			WHERE patient_radiology_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') 
			AND UNIX_TIMESTAMP('".$toDate." 23:59:59') GROUP BY radiology_tests.id";


			/*Patient service bill */
			$queryServiceBill = "SELECT IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_service_bill
			WHERE patient_service_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
			UNIX_TIMESTAMP('".$toDate." 23:59:59')";

			$sqlServiceBill = "SELECT IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' ,services.name 'Service Name' FROM patient_service_bill
			LEFT JOIN services ON services.id = patient_service_bill.service_id
			WHERE patient_service_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
			UNIX_TIMESTAMP('".$toDate." 23:59:59') GROUP BY services.id";


			/*patient procedure bill*/

			$queryProcedureBill = "SELECT IFNULL(SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' 
			THEN patient_procedure_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_procedure_bill.`status` = 'Unpaid' THEN patient_procedure_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_procedure_bill
			WHERE patient_procedure_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
			UNIX_TIMESTAMP('".$toDate." 23:59:59')";

			$sqlProcedureBill = "SELECT IFNULL(SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' 
			THEN patient_procedure_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_procedure_bill.`status` = 'Unpaid' THEN patient_procedure_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount',procedures.name AS 'Procedue Name' 
			FROM patient_procedure_bill
			LEFT JOIN procedures ON procedures.id  = patient_procedure_bill.procedure_id
			WHERE patient_procedure_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
			UNIX_TIMESTAMP('".$toDate." 23:59:59') GROUP BY procedures.id";

			/*patint forensic bill*/
			$queryForensicBill = "SELECT IFNULL(SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' 
			THEN patient_forensic_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_forensic_bill.`status` = 'Unpaid' THEN patient_forensic_bill.charge ELSE 0 END),'')
			AS 'Unpaid Amount' FROM patient_forensic_bill
			WHERE patient_forensic_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') 
			AND UNIX_TIMESTAMP('".$toDate." 23:59:59')";

			$sqlForensicBill = "SELECT IFNULL(SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' 
			THEN patient_forensic_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_forensic_bill.`status` = 'Unpaid' THEN patient_forensic_bill.charge ELSE 0 END),'')
			AS 'Unpaid Amount' FROM patient_forensic_bill
			LEFT JOIN forensic_tests ON forensic_tests.id = patient_forensic_bill.forensic_test_id
			WHERE patient_forensic_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
			UNIX_TIMESTAMP('".$toDate." 23:59:59') GROUP BY forensic_tests.id";
		}


		$Allrows   = array();

		/*Connection for patient lab bill query*/
		$queryResultLabBill  = mysqli_query($conn,$queryLabBill);	
		$rowsQueryLabBill   = array();
	    while ($r = mysqli_fetch_assoc($queryResultLabBill)) {
	        $rowsQueryLabBill[] = $r;
	    }

	    $sqlResultLabBill  = mysqli_query($conn,$sqlLabBill);		
		$rowsSqlLabBill   = array();
	    while ($r = mysqli_fetch_assoc($sqlResultLabBill)) {
	        $rowsSqlLabBill[] = $r;
	    }

	    /*Connection for patient radiology bill query*/
		$queryResultRadiologyBill  = mysqli_query($conn,$queryRadiologyBill);	
		$rowsQueryRadiologyBill   = array();
	    while ($r = mysqli_fetch_assoc($queryResultRadiologyBill)) {
	        $rowsQueryRadiologyBill[] = $r;
	    }

	    $sqlResultRadiologyBill  = mysqli_query($conn,$sqlRadiologyBill);		
		$rowsSqlRadiologyBill   = array();
	    while ($r = mysqli_fetch_assoc($sqlResultRadiologyBill)) {
	        $rowsSqlRadiologyBill[] = $r;
	    }

	    /*Connection for service radiology bill query*/
		$queryResultServiceBill  = mysqli_query($conn,$queryServiceBill);	
		$rowsQueryServiceBill   = array();
	    while ($r = mysqli_fetch_assoc($queryResultServiceBill)) {
	        $rowsQueryServiceBill[] = $r;
	    }

	    $sqlResultServiceBill  = mysqli_query($conn,$sqlServiceBill);		
		$rowsSqlServiceBill   = array();
	    while ($r = mysqli_fetch_assoc($sqlResultServiceBill)) {
	        $rowsSqlServiceBill[] = $r;
	    }

	    /*Connection for patient procedure bill query*/
		$queryResultProcedureBill  = mysqli_query($conn,$queryProcedureBill);	
		$rowsQueryProcedureBill   = array();
	    while ($r = mysqli_fetch_assoc($queryResultProcedureBill)) {
	        $rowsQueryProcedureBill[] = $r;
	    }

	    $sqlResultProcedureBill  = mysqli_query($conn,$sqlProcedureBill);		
		$rowsSqlProcedureBill   = array();
	    while ($r = mysqli_fetch_assoc($sqlResultProcedureBill)) {
	        $rowsSqlProcedureBill[] = $r;
	    }


	    /*Connection for patient forensic bill query*/
		$queryResultForensicBill  = mysqli_query($conn,$queryForensicBill);	
		$rowsQueryForensicBill   = array();
	    while ($r = mysqli_fetch_assoc($queryResultForensicBill)) {
	        $rowsQueryForensicBill[] = $r;
	    }

	    $sqlResultForensicBill  = mysqli_query($conn,$sqlForensicBill);		
		$rowsSqlForensicBill   = array();
	    while ($r = mysqli_fetch_assoc($sqlResultForensicBill)) {
	        $rowsSqlForensicBill[] = $r;
	    }


	    array_push($Allrows, $rowsQueryLabBill);
	    array_push($Allrows, $rowsSqlLabBill);
	    array_push($Allrows, $rowsQueryRadiologyBill);
	    array_push($Allrows, $rowsSqlRadiologyBill);
	    array_push($Allrows, $rowsQueryServiceBill);
	    array_push($Allrows, $rowsSqlServiceBill);
	    array_push($Allrows, $rowsQueryProcedureBill);
	    array_push($Allrows, $rowsSqlProcedureBill);
	    array_push($Allrows, $rowsQueryForensicBill);
	    array_push($Allrows, $rowsSqlForensicBill);


	    echo json_encode($Allrows);
	}
?>