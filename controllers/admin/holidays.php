<?php
/*
 * File Name    :   holidays.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   15 March,2016
 * Description  :   This page is use for add/update/show/delete holidays
 */
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

$operation       = "";

include 'config.php'; // include database connection file

if (isset($_POST['operation'])) { // define operation value from js file
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}


/* save designation informationinto database*/
if ($operation == "save") {
    
    if (isset($_POST['holidayName'])) {
        $holidayName = $_POST['holidayName'];
    }
    if (isset($_POST['holidayDate'])) {
        $holidayDate = $_POST['holidayDate'];
    }
    if (isset($_POST['availability'])) {
        $availability = $_POST['availability'];
    }
    if (isset($_POST['hoildayDescription'])) {
        $hoildayDescription = $_POST['hoildayDescription'];
    }
    
    $sqlSelect= "SELECT name from holidays where name='" . $holidayName . "' AND
     date = '" . $holidayDate . "' AND status = 'A'";
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $rows_count   = mysqli_num_rows($resultSelect);
    
    if ($rows_count <= 0) {
        echo $sqlInsert    = "INSERT INTO holidays (name,`date`,repeat_annually,description,created_on,updated_on,
        created_by,updated_by) VALUES('".$holidayName."','" . $holidayDate . "','" . $availability . "','".$hoildayDescription."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."',
        '".$userId."')";

        $resultInsert = mysqli_query($conn, $sqlInsert);
        echo $resultInsert;
    } else {
        echo "0";
    }
}

/* update designation information into database*/
if ($operation == "update") {
    if (isset($_POST['holidayName'])) {
        $holidayName = $_POST['holidayName'];
    }
    if (isset($_POST['holidayDate'])) {
        $holidayDate = $_POST['holidayDate'];
    }
    if (isset($_POST['availability'])) {
        $availability = $_POST['availability'];
    }
    if (isset($_POST['hoildayDescription'])) {
        $hoildayDescription = $_POST['hoildayDescription'];
    }
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
        
    $sqlSelect= "SELECT name from holidays where name='" . $holidayName . "' AND
     date = '" . $holidayDate . "' AND status = 'A' AND id !='".$id."'";
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $rows_count   = mysqli_num_rows($resultSelect);    
    if ($rows_count <= 0) {
        $sql    = "UPDATE holidays SET name= '" . $holidayName . "', repeat_annually = '" . $availability . "',date = '" . $holidayDate . "',
            description='".$hoildayDescription."',updated_on = UNIX_TIMESTAMP(),
            updated_by ='".$userId."' WHERE id = '" . $id . "'";
        $result = mysqli_query($conn, $sql);
        echo $result;
    }
    else{
        echo "0";
    }
        
}
/* set status Inactive for delete designation */
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $sql    = "UPDATE holidays SET status= 'I'  where  id = " . $id . "";
    $result = mysqli_query($conn, $sql);
    echo $result;
}
/*show active data into the datatable  */

if ($operation == "show") {
    $sqlSelect    = "SELECT id,name,description,date,repeat_annually FROM holidays where status = 'A'";
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $totalrecords = mysqli_num_rows($resultSelect);
    
    $rows = array();
    while ($rUpdate = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $rUpdate;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/* show Inactive data into datatable on checked case*/
if ($operation == "checked") {
    $sqlSelect    = "SELECT id,name,description,date FROM holidays where status = 'I'";
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $totalrecords = mysqli_num_rows($resultSelect);
    
    $rows = array();
    while ($rUpdate = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $rUpdate;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/* set status active for restore designation */
if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $checkholidayName = $_POST['checkholidayName'];
    $checkholidayDate = $_POST['checkholidayDate'];

    //check holiday alreday exist
    $selectQry = "SELECT name FROM holidays WHERE status='A' AND name = '".$checkholidayName."' 
    AND date = '".$checkholidayDate."'";
    $resultSel = mysqli_query($conn,$selectQry);
    $conutHolidays = mysqli_num_rows($resultSel);

    if ($conutHolidays =="0") {
        $sql    = "UPDATE holidays SET status= 'A'  where  id = " . $id . "";
        $result = mysqli_query($conn, $sql);
        echo $result;
    }
    else {
        echo "0";
    }  
}

if ($operation =="saveLeavePeriod") {
    if (isset($_POST['startDate'])) {
        $startDate = $_POST['startDate'];
    }
    if (isset($_POST['endDate'])) {
        $endDate = $_POST['endDate'];
    }
    if (isset($_POST['availability'])) {
        $availability = $_POST['availability'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    if($availability == "1"){
        $sql    = "UPDATE leave_period SET period_status= '0' WHERE period_status = '1'";
        $result = mysqli_query($conn, $sql);
    }
    $sql    = "INSERT INTO leave_period(from_date,to_date,description,period_status,created_on,updated_on,created_by
        ,updated_by) VALUES('".$startDate."','".$endDate."','".$description."','".$availability."',UNIX_TIMESTAMP(),
        UNIX_TIMESTAMP(),$userId ,$userId )";

    $resultInsert = mysqli_query($conn, $sql);
    echo $resultInsert;
}
if ($operation == "showLeavePeriod") {
    $sqlSelect    = "SELECT * FROM leave_period where status = 'A'";
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $totalrecords = mysqli_num_rows($resultSelect);
    
    $rows = array();
    while ($rUpdate = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $rUpdate;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}
if ($operation == "checkedLeavePeriod") {
    $sqlSelect    = "SELECT * FROM leave_period where status = 'I'";
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $totalrecords = mysqli_num_rows($resultSelect);
    
    $rows = array();
    while ($rUpdate = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $rUpdate;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/* update designation information into database*/
if ($operation == "updateLeavePeriod") {
    if (isset($_POST['startDate'])) {
        $startDate = $_POST['startDate'];
    }
    if (isset($_POST['endDate'])) {
        $endDate = $_POST['endDate'];
    }
    if (isset($_POST['availability'])) {
        $availability = $_POST['availability'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    if($availability == "1"){
        $sql    = "UPDATE leave_period SET period_status= '0' WHERE period_status = '1'";
        $result = mysqli_query($conn, $sql);
    }
    
    $sql    = "UPDATE leave_period SET from_date= '" . $startDate . "',period_status= '" . $availability . "',to_date= '" . $endDate . "',
    description='".$description."',updated_on = UNIX_TIMESTAMP(),updated_by ='".$userId."'
    WHERE id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}
/* set status Inactive for delete designation */
if ($operation == "deleteLeavePeriod") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $sql    = "UPDATE leave_period SET status= 'I'  where  id = " . $id . "";
    $result = mysqli_query($conn, $sql);
    echo $result;
}
/* set status active for restore designation */
if ($operation == "restoreLeavePeriod") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $sql    = "UPDATE leave_period SET status= 'A'  where  id = " . $id . "";
    $result = mysqli_query($conn, $sql);
    echo "1";
}

?>