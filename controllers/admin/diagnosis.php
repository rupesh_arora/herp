<?php
	
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	
    $operation = "";
	$diseasesCategory ="";
	$description = "";
	$createdate = new DateTime();
	include 'config.php';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	else{
		
	}
	if($operation == "showcategory")   // show category in category selection box
	{
		$query="SELECT id,category FROM diagnosis_categories where status = 'A'";
  		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		$rows[] = $r;
		}
		print json_encode($rows);
	}
	if($operation == "checkdiseases") // check duplicacy
	{		
			$code=$_POST['Code'];
			$name=$_POST['Name'];
			$category_id=$_POST['category_Id'];
			$id=$_REQUEST['Id'];
			if($id != "")
			{
				$query = "Select code from diagnosis where code = '".$code."' and id != ".$id."";
			}
			else
			{
				$query = "Select code from diagnosis where code = '".$code."'";
			}
			$sqlSelect = mysqli_query($conn,$query);
			$numrows = mysqli_num_rows($sqlSelect);
			if($numrows > 0)
			{	
				echo "1";
			}
			else{
				echo "0";
			}   
	}
	if($operation == "save")			
	{
		if (isset($_POST['Code'])) {
		$code=$_POST['Code'];
		}
		if (isset($_POST['Name'])) {
		$name=$_POST['Name'];
		}
		$category_id=$_POST['category_Id'];

		$sql = "INSERT INTO diagnosis(name,code,diagnosis_cat_id,created_on,updated_on) VALUES('".$name."','".$code."','".$category_id."','".$createdate->getTimestamp()."','".$createdate->getTimestamp()."')";
		$result= mysqli_query($conn,$sql);  
		echo $sql;
	} 	
	if ($operation == "show"){
		
		$query= "select diagnosis.*,diagnosis_categories.category from diagnosis LEFT JOIN diagnosis_categories ON diagnosis.diagnosis_cat_id=diagnosis_categories.id where diagnosis.status='A' and diagnosis_categories.status='A'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 //print json_encode($rows);
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);	
	
	}
	// opertaion form update
	if ($operation == "update")
	{
		if (isset($_POST['Code'])) {
		$code=$_POST['Code'];
		}
		if (isset($_POST['Name'])) {
		$name=$_POST['Name'];
		}
		$category_id=$_POST['category_Id'];

		if (isset($_POST['Id'])) {
		$id = $_POST['Id'];
		}
		
		$sql = "update diagnosis set code= '".$code."' , name = '".$name."', diagnosis_cat_id='".$category_id."', updated_on='".$createdate->getTimestamp()."' where id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $sql;
	}
	//  operation for delete
	if ($operation == "delete")
	{
		if (isset($_POST['id'])) {
		$id = $_POST['id'];
		}
		
		$sql = "UPDATE diagnosis SET status= 'I' where id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $sql;
	}
	//When checked box is check
	if ($operation == "checked") {
		
		$query= "select diagnosis.*,diagnosis_categories.category from diagnosis LEFT JOIN diagnosis_categories ON diagnosis.diagnosis_cat_id=diagnosis_categories.id where diagnosis.status='I' or diagnosis_categories.status='I'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 //print json_encode($rows);
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);					
	}
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		$category=$_POST['category'];
		$sql = "UPDATE diagnosis SET status= 'A'  WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		if($result == 1){
			$query = "SELECT status from diagnosis_categories where id='".$category."'";
			$result= mysqli_query($conn,$query);
			while($r = mysqli_fetch_assoc($result)) {
				if($r['status'] == 'A'){
					echo "1";	
				}
				else{
					echo "0";
				}
			}
		}
	}
?>