<?php
/*
 * File Name    :   bed_transfer.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   28th feb, 2016
 * Description  :   This page use for load,save,update,delete,resotre details form database
 */
$operation           = "";
$createdate          = new DateTime();
$date      = date("Y-m-d");
$createOn  = new DateTime();
  
include 'config.php'; //import databse connection file
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

if (isset($_POST['operation'])) { // perform operation form js file
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
}


// opertaion form update
if ($operation == "update") // update data
{    
    if (isset($_POST['transferReason'])) {
        $transferReason = $_POST['transferReason'];
    }
    if (isset($_POST['bedId'])) {
        $bedId = $_POST['bedId'];
    }
    if (isset($_POST['ipdId'])) {
        $ipdId = $_POST['ipdId'];
    }
    if (isset($_POST['ICU'])) {
        $ICU = $_POST['ICU'];
    }
    if (isset($_POST['price'])) {
        $price = $_POST['price'];
    }
    if (isset($_POST['oldPrice'])) {
        $oldPrice = $_POST['oldPrice'];
    }
    if (isset($_POST['patientId'])) {
        $patientId = $_POST['patientId'];
    }
    //$transactionData = "";
    $id = $_POST['id'];

    $queryCashAccount = "select sum(cash_account.credit - cash_account.debit) as amount from cash_account
                        WHERE patient_id = ".$patientId."";
    $resultCashAccount = mysqli_query($conn,$queryCashAccount);
    while($row = mysqli_fetch_row($resultCashAccount)) {
        $r = $row['0'];
    }
    if($r >= $price) {
    
        $sql    = "update beds_ipd_history set old_bed_id= '" . $bedId . "' ,ic_icu= '" . $ICU . "' ,is_transfer= '1' ,transfer_reason = '" . $transferReason . "', updated_on='" . $createdate->getTimestamp() . "',updated_by='" . $userId . "' where id = '" . $id . "' ";
        $result = mysqli_query($conn, $sql);

        if($result) {

            $query = "Select old_bed_id from beds_ipd_history
                    WHERE id = ".$id."";
            $resultQuery = mysqli_query($conn,$query);
            while($row = mysqli_fetch_row($resultQuery)) {
                $bed_id = $row['0'];
            }

            // fetch bed price
            $updateOldBedStatus = "UPDATE beds SET bed_availability = '1' where id = ".$bed_id."";
            $resultOldBedStatus = mysqli_query($conn,$updateOldBedStatus);

            $updateBedStatus = "UPDATE beds SET bed_availability = '0' where id = ".$bedId."";
            $resultBedStatus = mysqli_query($conn,$updateBedStatus);
            
            $selectBedPrice = "SELECT price from beds WHERE id = ".$bed_id."";
            $resultBedPrice = mysqli_query($conn,$selectBedPrice);
            while ($r = mysqli_fetch_assoc($resultBedPrice)) {
                $price = $r['price'];
            }

           

            if($price == $price) {

                $queryGetAccountNo = "SELECT fan_id from patients WHERE id = ".$patientId."";
                $resultAccount = mysqli_query($conn,$queryGetAccountNo);
                while ($r = mysqli_fetch_assoc($resultAccount)) {
                    $familyAccountNo = $r['fan_id'];
                }
                if($familyAccountNo != ""){
                    $AccountNo = $familyAccountNo;
                }
                else{
                    $AccountNo = $patientId;
                }
                $insertDebit = "INSERT INTO cash_account (patient_id,depositor_id,credit,debit,date,created_on,created_by) VALUES 
                        ('".$AccountNo."','".$patientId."',0,".$price.",'".$date."','" . $createOn->getTimestamp() . "','" . $userId . "')";
			    $resultDebit = mysqli_query($conn,$insertDebit);
				
				/* $transactionData = $transactionData.'IPD Id: '.$ipdId.',Hospitalization Charge: '.$price.'.';
				// cash deposite transaction
				$insertTransaction = "INSERT INTO transactions (patient_id,transaction_type_id,credit,debit,processed_on,processed_by,details)
						values ('" . $patientId . "',' 7 ','0','" .$price. "',UNIX_TIMESTAMP(),'".$createdBy ."','".$transactionData."')"; 
				mysqli_query($conn,$insertTransaction);	 */		
               
                if($resultDebit) {
                    echo $resultDebit;
                }
            }
        }
    }
    else{
        echo "2";
    }
}

/*operation to show inactive data*/
if ($operation == "showData") {

    if (isset($_POST['ipdId'])) {
        $ipdId = $_POST['ipdId'];
    }

    $query        = "SELECT bipd.id AS bed_history_id, ipd.*,(CASE WHEN bipd.ic_icu = '0' THEN 'NO' ELSE 'YES' END) AS is_icu, w.name,d.department,wr.number,b.number AS bedName,CONCAT(p.first_name,' ',p.middle_name,' ',p.last_name) AS patient_name 
        ,b.ward_id,b.room_id,b.department_id,b.id AS bed_id,b.price FROM ipd_registration AS ipd 
        LEFT JOIN beds_ipd_history AS bipd ON bipd.ipd_id = ipd.id
        LEFT JOIN beds AS b ON b.id = bipd.bed_id  
        LEFT JOIN ward_room AS wr ON wr.id = b.room_id
        LEFT JOIN wards AS w ON w.id = b.ward_id
        LEFT JOIN patients AS p ON p.id = ipd.patient_id 
        LEFT JOIN department AS d ON d.id = b.department_id 
        WHERE ipd.id =".$ipdId."";
                    
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    echo json_encode($rows);
}
?>