<?php
/* ****************************************************************************************************
 * File Name    :   transaction_statement.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   5th mar, 2016
 * Description  :   This page print Transaction Statement
 *************************************************************************************************** */
	session_start();	

    if(isset($_SESSION['globaluser'])){
        $userId = $_SESSION['globaluser'];//store global user value in variable
    }
    else{
    	exit();
    }
	$date      = date("Y-m-d");	
	$createOn  = new DateTime();

	include 'config.php';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}	
	
	// search data
	if($operation == "showTransactionStatement") {
		
		$patientId = $_POST['patientId'];
		$fromDate = $_POST['fromDate'];
		$toDate = $_POST['toDate'];
		
		$isFirst = "false";   
		$patientPrefix = $_POST['patientPrefix'];
		$sql       = "SELECT value FROM configuration WHERE name = 'patient_prefix'";
		$result    = mysqli_query($conn, $sql);
		
		while ($r = mysqli_fetch_assoc($result)) {
			$prefix = $r['value'];
		}
		
		if($prefix == $patientPrefix){

			 $query = "SELECT (SELECT value from configuration where name = 'patient_prefix') as patient_prefix,transaction.type_description,transactions.*,DATE_FORMAT(FROM_UNIXTIME(transactions.processed_on), '%Y-%m-%d') AS 'transactionsDate',invoice_details.invoice_details  from transactions
						LEFT JOIN invoice_details ON invoice_details.id = transactions.invoice_id
						LEFT JOIN `transaction` ON transaction.id = transactions.transaction_type_id
						 where transactions.patient_id='".$patientId."'";
				 
			if($fromDate !='' AND $toDate !=''){
				$query .= " AND transactions.processed_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') 
							AND UNIX_TIMESTAMP('".$toDate." 23:59:59') ";				
			}
			if($fromDate !='' AND $toDate ==''){
				$query .= " AND transactions.processed_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') 
							AND CURRENT_TIMESTAMP() ";				
			}
			if($fromDate =='' AND $toDate !=''){
				$query .= " AND transactions.processed_on BETWEEN '0000000000' AND 
							UNIX_TIMESTAMP('".$toDate." 23:59:59') ";
			}
	       
			$result = mysqli_query($conn, $query);
			//$totalrecords = mysqli_num_rows($result);
			$rows   = array();
			while ($r = mysqli_fetch_assoc($result)) {
				$rows[] = $r;
			}
			//$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
			print json_encode($rows);	
		}	
	}
?>