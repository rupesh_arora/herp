<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	
	include 'config.php';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}	
	//Operation to update the Test Status to 'done' and Generate a new specimen
	if($operation == "update")   
	{
		if (isset($_POST['lab_test_id'])) {
			$id=$_POST["lab_test_id"];
		}
		if (isset($_POST['specimen_description'])) {
			$specimenDescription=$_POST["specimen_description"];
		}
		$labRequestid = $_POST["labRequestId"];
		
		$query="UPDATE lab_test_requests SET test_status = 'done',request_date=UNIX_TIMESTAMP() WHERE  id = '".$id."'";  
		$result=mysqli_query($conn,$query);
		
		if($result)
		{
			$query="INSERT INTO patient_specimen(lab_test_id,lab_test_requested_id,description,created_on,created_by)
			VALUES (".$id.",".$labRequestid.",'".$specimenDescription."',UNIX_TIMESTAMP(),".$_SESSION['globaluser'].")"; 
			$result=mysqli_query($conn,$query);
			echo $conn->insert_id;
		}
	}
	//Operation to Show all Records for Lab Requests which are pending and payment is paid
	if($operation == "show"){
		
		$query= "SELECT lab_test_requests.patient_id,
				lab_test_requests.visit_id,
				lab_tests.test_code,lab_tests.name,
				lab_test_requests.cost,
				specimens_types.type,
				specimens_types.description,
				ltrc.pay_status,
				patients.salutation , patients.first_name , 
				patients.last_name ,
				patients.mobile,
				patients.middle_name,
				patients.dob,
				patients.care_of,
				patients.address,patients.gender,
				patients.zip,patients.images_path,
				patients.care_contact,
				patients.email,
				lab_test_requests.id,lab_tests.id as lab_test_id,
				(SELECT value FROM configuration WHERE name = 'patient_prefix') AS patient_prefix,
				(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visit_prefix
				FROM `lab_test_requests` 
				LEFT JOIN lab_tests ON lab_test_requests.lab_test_id = lab_tests.id 
				LEFT JOIN lab_test_request_component AS ltrc ON ltrc.lab_test_request_id = lab_test_requests.id  
				LEFT JOIN specimens_types ON specimens_types.id = lab_tests.specimen_id LEFT JOIN patients On 
				patients.id = lab_test_requests.patient_id 
				WHERE ltrc.pay_status = 'paid' AND lab_test_requests.status = 'A'
				AND lab_test_requests.test_status = 'pending'  AND lab_test_requests.request_type = 'internal' 
				GROUP BY lab_test_requests.visit_id,lab_test_requests.patient_id,lab_tests.id";		
		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
	//Operation to load Specimen Types
	if($operation == "loadSpecimen")			
	{
		$sql = "SELECT id as value,type as text FROM specimens_types WHERE  status = 'A'";
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		echo json_encode($rows);	
	}
	//Operation to load Lab tests 
	if($operation == "loadLabTests")			
	{
		$sql = "SELECT id as value,name as text FROM lab_tests WHERE  status = 'A'";
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		echo json_encode($rows);
	}
	
	// get component name
	if($operation == "getComponentName") {
		
		if (isset($_POST['visitId'])) {
			$visitId = $_POST["visitId"];
		}
		if (isset($_POST['patientId'])) {
			$patientId = $_POST["patientId"];
		}
		if (isset($_POST['labTestId'])) {
			$labTestId = $_POST["labTestId"];
		}
		
		$sql = "SELECT  lab_test_component.name As component_name from lab_test_requests 
		LEFT JOIN lab_test_request_component ON lab_test_request_component.lab_test_request_id 
		= lab_test_requests.id 
		LEFT JOIN lab_test_component ON lab_test_request_component.lab_component_id = lab_test_component.id
		WHERE lab_test_requests.patient_id = '".$patientId."'  AND
		lab_test_requests.visit_id = '".$visitId."' AND lab_test_requests.lab_test_id = '".$labTestId."'";
		
		
		$result=mysqli_query($conn,$sql);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		echo json_encode($rows);
	}
?>