<?php
	
	session_start();
	if(!session_id()){
		exit();
	}
	$toDate = '';
	$fromDate = '';
	
	include_once('config.php');

	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET['operation'])){
		$operation=$_GET["operation"];
	}

	if ($operation=="showChartData") {
		if (isset($_POST['dataLoad'])) {
			$dataLoad = $_POST["dataLoad"];
		}
		if (isset($_POST['fromDate'])) {
			$fromDate = $_POST["fromDate"];
		}
		if (isset($_POST['toDate'])) {
			$toDate = $_POST["toDate"];
		}

		if ($dataLoad =="all") {

			$sqlConsultantVisits = "SELECT COUNT(visits.id) AS Visits,users.first_name As `Consultant Name` FROM visits LEFT JOIN users ON users.id = visits.id
				WHERE consultant_id IS NOT NULL GROUP BY consultant_id ";
		}
		
		else if ($dataLoad =="last30days") {

			
			$sqlConsultantVisits = "SELECT COUNT(visits.id) AS Visits,users.first_name As `Consultant Name` FROM visits LEFT JOIN users ON users.id = visits.id
				WHERE consultant_id IS NOT NULL  AND visits.created_on >= unix_timestamp(curdate() -
				interval 1 month) GROUP BY consultant_id";
		}


		else if ($dataLoad =="last7days") {
			
			$sqlConsultantVisits = "SELECT COUNT(visits.id) AS Visits,users.first_name As `Consultant Name` FROM visits LEFT JOIN users ON users.id = visits.id
				WHERE consultant_id IS NOT NULL  AND visits.created_on >= unix_timestamp(curdate() -
				interval 7 day) GROUP BY consultant_id";
		}


		else if ($dataLoad =="today") {			

			$sqlConsultantVisits = "SELECT COUNT(visits.id) AS Visits,users.first_name As `Consultant Name` FROM visits LEFT JOIN users ON users.id = visits.id
				WHERE consultant_id IS NOT NULL  AND visits.created_on >= unix_timestamp(curdate())
				GROUP BY consultant_id";
		}
		else  {

			$sqlConsultantVisits = "SELECT COUNT(visits.id) AS Visits,users.first_name As `Consultant Name` FROM visits LEFT JOIN users ON users.id = visits.id
				WHERE consultant_id IS NOT NULL  AND visits.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate."
				00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59') GROUP BY consultant_id";
		}		
		
		$rowsConsultantVisits  = array();
	    $resultConsultantVisits  = mysqli_query($conn,$sqlConsultantVisits);
	    while ($r = mysqli_fetch_assoc($resultConsultantVisits)) {
	        $rowsConsultantVisits[] = $r;
	    }


	    echo json_encode($rowsConsultantVisits);
	}
?>