<?php
/*
 * File Name    :   diseases.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre details form database
 */

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
    
$operation        = "";
$diseasesCategory = "";
$description      = "";
$createdate       = new DateTime();
include 'config.php';


if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
    
}
/* if ($operation == "showcategory") // show category in category selection box
    {
    $query  = "SELECT id,category FROM diseases_categories where status = 'A'";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
} */
if (isset($_FILES["file"]["type"])) // use for import data from csv
 {
    $temporary       = explode(".", $_FILES["file"]["name"]);
    $file_extension = end($temporary);
    /*if ($file_extension == "csv") {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
        } else {
			if($_FILES["file"]["size"] > 0) {
			
				$filename = $_FILES["file"]["tmp_name"];
				$file = fopen($filename, "r");

				$countCSV = 0;
				$subInsertValues = '';
				$sqlInsert = "INSERT IGNORE INTO diseases(code,name,description,created_on,updated_on,created_by,updated_by) VALUES ";

				while (($emapData = fgetcsv($file, 10000, ",")) != FALSE) {
					
					if (sizeof($emapData) != 3) {
					 	echo "You must have three column in your file i.e disease code, disease name and description";
					 	exit();
					}

					//check whether header name same or not
					if ($countCSV == 0) {
						if(strtolower($emapData[0]) !="disease code") {
							echo "First column should be disease code";
							exit();
						}
						if(strtolower($emapData[1]) !="disease name") {
							echo "Second column should be disease name";
							exit();
						}
						if(strtolower($emapData[2]) !="description") {
							echo "Second column should be description";
							exit();
						}						
					}

					//data can't be null in first row else row it may be null
					if ($countCSV == 1) {
						if (empty($emapData[0]) || empty($emapData[1])) {
							echo "Data can't be null in code and name column";
							exit();
						}
					}

					if ($countCSV > 0) {

						//check whether data not be null in the file
						if(($emapData[0]) != "" && ($emapData[1]) != "" ) {

							$diseasesCategory = mysql_real_escape_string($emapData[0]);
							$diseaseName = mysql_real_escape_string($emapData[1]);
							$diseaseDescription =  mysql_real_escape_string($emapData[2]);
							if ($countCSV >1) {
								$subInsertValues.= ',';
							}						
							$subInsertValues.= "('".$diseasesCategory."','".$diseaseName."','".$diseaseDescription."',	UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
						}
					}			
					$countCSV++;						
				}

				$sqlInsert = $sqlInsert.$subInsertValues;
				$result = mysqli_query($conn,$sqlInsert);

				if($result) {
				
					echo $result;
				}
				else{
					echo "";
				}
				fclose($file);
			}
		} 	
	}
	else if($file_extension == "xls"){
		ini_set("display_errors",1);
		$filename = $_FILES['file']['tmp_name'];
		require_once 'excel_reader2.php';
		$data = new Spreadsheet_Excel_Reader($filename);
		$countSheets = count($data->sheets);
		for($i=0;$i<$countSheets;$i++) // Loop to get all sheets in a file.
		{ 
			$countSheetsCells =0;
			if(isset($data->sheets[$i]['cells'])){
				$countSheetsCells = count($data->sheets[$i]['cells']);
			}
			if($countSheetsCells > 0 && $countSheetsCells) {// checking sheet not empty
			
				$sql = "INSERT IGNORE INTO diseases(code,name,description,created_on,updated_on,created_by,updated_by) VALUES ";
				$countXls = 0;				
				$subInsert = '';
				for($j=1;$j<=$countSheetsCells;$j++) {// loop used to get each row of the sheet
				 
					//Check size of array
				  	if (sizeof($data->sheets[$i]['cells'][$j]) !="3") {
				  		echo "You must have three column in your file i.e disease code, disease name and description";
					 	exit();
				  	}
				  	
				  	//Geeting the index of Excel file data like in which it is A or B or C/	
				  	$indexOfExcelFile = array_keys($data->sheets[$i]['cells'][$j]);

					//Check the first header and second header
					if ($countXls == 0) {
						if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[0]]) !="disease code"){
							echo "First column should be disease code";
							exit();
						}
						if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[1]]) !="disease name"){
							echo "Second column should be disease name";
							exit();
						}
						if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[2]]) !="description"){
							echo "Third column should be description";
							exit();
						}
					}
					
					$code = mysql_real_escape_string($data->sheets[$i]['cells'][$j][$indexOfExcelFile[0]]);
				
					$name = mysql_real_escape_string($data->sheets[$i]['cells'][$j][$indexOfExcelFile[1]]);
				
					$description = mysql_real_escape_string($data->sheets[$i]['cells'][$j][$indexOfExcelFile[2]]);

					//data can't be null in the fisrt column
					if ($countXls == 1) {
						if (empty($code) || empty($name)) {
							echo "Data can't be null in code and name column";
							exit();
						}
					}
					

					if ($countXls>0) {
						
						//check whether data not be null in file
						if($code != "" && $name != ''){

							if ($countXls >1) {
								$subInsert .=',';
							}					 	
						 	$subInsert .=" ('$code','$name','$description',UNIX_TIMESTAMP(),
						 	UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
						}
					}
					$countXls++;		
				}
				$sql = $sql.$subInsert;
				$result = mysqli_query($conn,$sql);
				if($result)
				{
					echo $result;
				}
				else{
					echo "";
				}
			}
		}		
	}*/
	if($file_extension == "xlsx" || $file_extension == "csv" ||$file_extension == "xls"){
		// get file name
		$filename = $_FILES['file']['tmp_name'];
		
		//import php script
		require_once 'PHPExcel.php';
		
		// 
		try {
			$inputFileType = PHPExcel_IOFactory::identify($filename);  // get type of file name
			$objReader = PHPExcel_IOFactory::createReader($inputFileType); //create object of read file
			$objPHPExcel = $objReader->load($filename);  //load excel into the object
		} catch (Exception $e) {
			die('Error loading file "' . pathinfo($filename, PATHINFO_BASENAME) 
			. '": ' . $e->getMessage());
		}

		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); // get sheet 1 of excel file
		$highestRow = $sheet->getHighestRow(); // get heighest row
		$highestColumn = $sheet->getHighestColumn(); // get heighest column
		
		$headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE); // get heading in first row
		
		if (sizeof($headings[0]) < 3) {
			echo "You must have three column in your file i.e disease code, disease name and description";
			exit();
		}
		
		//reset($headings[0]) it is the first column
		if(trim(strtolower($headings[0][0])) !="disease code"){
			echo "First column should be disease code";
			exit();
		}
		//end($headings[0]) it is the last column
		if(trim(strtolower($headings[0][1])) !="disease name"){
			echo "Second column should be disease name";
			exit();
		}
		if(trim(strtolower($headings[0][2])) !="description"){
			echo "Third column should be description";
			exit();
		}
		$sql = "INSERT IGNORE INTO diseases(code,name,description,created_on,updated_on,created_by,updated_by) VALUES ";
		$countXlsx = 0;
		$subInsertXlsx = '';

		for ($row = 2; $row <= $highestRow; $row++) {
			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL, TRUE, FALSE);
			
			// combine data in array accoding to heading
			$rowData[0] = array_combine($headings[0], $rowData[0]);
			
			$key  = array_keys($rowData[0]);//getting the index name

			$code =  mysql_real_escape_string($rowData[0][$key[0]]);
			$name = mysql_real_escape_string($rowData[0][$key[1]]);
			$description = mysql_real_escape_string($rowData[0][$key[2]]);

			$code =  trim($code);
			$name = trim($name);
			$description = trim($description);


			//data can't be null in first row
			if ($countXlsx == 0) {
				if (empty($code) || empty($name)) {
					echo "Data can't be null in code and name column";
					exit();
				}
			}

			if ($code != "" && $name != "") {				

				if ($countXlsx > 0) {
					$subInsertXlsx.=',';
				}
				$subInsertXlsx.="('$code','$name','$description',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),
				'".$userId."','".$userId."')";
			}

			$countXlsx++;		
		}
		$sql = $sql.$subInsertXlsx;
		$result = mysqli_query($conn,$sql);
		if($result)
		{
			echo $result;
		}
		else{
			echo "";
		}
	}
	else{
		echo "invalid file";
	}
}

	
if ($operation == "checkdiseases") // check duplicacy
    {
    $code        = $_POST['Code'];
    $name        = $_POST['Name'];
    $description = $_POST['description'];
    $id          = $_REQUEST['Id'];
    if ($id != "") {
        $query = "Select code from diseases where code = '" . $code . "' and id != " . $id . "";
    } else {
        $query = "Select code from diseases where code = '" . $code . "'";
    }
    $sqlSelect = mysqli_query($conn, $query);
    $numrows   = mysqli_num_rows($sqlSelect);
    if ($numrows > 0) {
        echo "1";
    } else {
        echo "0";
    }
}
// for save
if ($operation == "save") {
    if (isset($_POST['Code'])) {
        $code = $_POST['Code'];
    }
    if (isset($_POST['Name'])) {
        $name = $_POST['Name'];
    }
	$description = $_POST['description'];
    
    $sql    = "INSERT INTO diseases(name,code,description,created_on,updated_on,created_by,updated_by) VALUES('" . $name . "','" . $code . "','" . $description . "',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
    $result = mysqli_query($conn, $sql);
    if ($result) {
        echo $result;
    } else {
        echo "";
    }
}
// for show
if ($operation == "show") {
    
    $query        = "select * from diseases where diseases.status='A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
    
}
// opertaion form update
if ($operation == "update") {
    if (isset($_POST['Code'])) {
        $code = $_POST['Code'];
    }
    if (isset($_POST['Name'])) {
        $name = $_POST['Name'];
    }
    $description = $_POST['description'];
    
    if (isset($_POST['Id'])) {
        $id = $_POST['Id'];
    }
    
    $sql    = "UPDATE diseases SET code = '" . $code . "' , name = '" . $name . "', description='" . $description . "', updated_on=UNIX_TIMESTAMP() ,updated_by='".$userId."'  where id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    if ($result) {
        echo $result;
    } else {
        echo "";
    }
}
//  operation for delete
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE diseases SET status= 'I' where id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $sql;
}
//When checked box is check
if ($operation == "checked") {
    
    $query        = "select * from diseases where diseases.status='I'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}
// for restore
if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $category = $_POST['category'];
    $sql      = "UPDATE diseases SET status= 'A'  WHERE  id = '" . $id . "'";
    $result   = mysqli_query($conn, $sql);
    if ($result == 1) {
        echo "1";
    }
}
?>