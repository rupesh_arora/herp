<?php
	
	session_start();
	if(!session_id()){
		exit();
	}
	$toDate = '';
	$fromDate = '';
	
	include_once('config.php');

	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET['operation'])){
		$operation=$_GET["operation"];
	}

	if ($operation=="showChartData") {

		

			/*Patient lab bill queries*/
			$queryLabBill = "SELECT IFNULL(SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 1 
			ELSE 0 END),1) 	as `paid-patients`,IFNULL(SUM(CASE WHEN patient_lab_bill.`status` = 'Unpaid' 
			THEN 1 ELSE 0 END),1) as `unpaid-patients`,lab_tests.name FROM patient_lab_bill
			LEFT JOIN lab_tests ON lab_tests.id = patient_lab_bill.lab_test_id
			WHERE patient_lab_bill.created_on >= unix_timestamp(curdate())
			GROUP by patient_lab_bill.lab_test_id ORDER by `paid-patients`,`unpaid-patients` ";

			$sqlLabBill = "SELECT IFNULL(SUM(CASE WHEN patient_lab_bill.`status` = 'paid' THEN 
			patient_lab_bill.charge ELSE 0 END),1) as `paid-bill`,IFNULL(SUM(CASE WHEN 
			patient_lab_bill.`status` = 'Unpaid' 
			THEN patient_lab_bill.charge ELSE 0 END),1) as `unpaid-bill` FROM patient_lab_bill
			WHERE patient_lab_bill.created_on >= unix_timestamp(curdate())";


			/*Patient radiology bill queries*/

			$queryRadiologyBill = "SELECT IFNULL(SUM(CASE WHEN 
			patient_radiology_bill.`status` = 'paid' THEN 1 
			ELSE 0 END),1) as `paid-patients`,IFNULL(SUM(CASE WHEN patient_radiology_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END),1) as `unpaid-patients`,radiology_tests.name FROM patient_radiology_bill
			LEFT JOIN radiology_tests ON radiology_tests.id = patient_radiology_bill.radiology_test_id
			WHERE patient_radiology_bill.created_on >= unix_timestamp(curdate())
			GROUP BY radiology_tests.id ORDER by `paid-patients`,`unpaid-patients` ";

			$sqlRadiologyBill = "SELECT IFNULL(SUM(CASE WHEN patient_radiology_bill.`status` = 'paid'
			THEN patient_radiology_bill.charge ELSE 0 END),1) as `paid-bill`,IFNULL(SUM(CASE WHEN
			patient_radiology_bill.`status` = 'Unpaid' THEN patient_radiology_bill.charge 
			ELSE 0 END),1) as `unpaid-bill` FROM patient_radiology_bill
			WHERE patient_radiology_bill.created_on >= unix_timestamp(curdate())";


			/*Patient service bill */
			$queryServiceBill = "SELECT IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'paid' THEN 1 
			ELSE 0 END),1) as `paid-patients`,IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'Unpaid' THEN 1 
			ELSE 0 END),1) as `unpaid-patients`,services.name FROM patient_service_bill
			LEFT JOIN services ON services.id = patient_service_bill.service_id
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate())
			GROUP BY services.service_type_id ORDER by `paid-patients`,`unpaid-patients` ";

			$sqlServiceBill = "SELECT IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END),1) as `paid-bill`,IFNULL(SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END),1) as `unpaid-bill` FROM patient_service_bill
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate())";


			/*patient procedure bill*/

			$queryProcedureBill = "SELECT IFNULL(SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' 
			THEN patient_procedure_bill.charge ELSE 0 END),1) as `paid-patients`,IFNULL(SUM(CASE WHEN 
			patient_procedure_bill.`status` = 'Unpaid' THEN patient_procedure_bill.charge 
			ELSE 0 END),1) as `unpaid-patients` FROM patient_procedure_bill WHERE created_on >= 
			unix_timestamp(curdate()) 
			GROUP BY patient_procedure_bill.procedure_id ORDER by `paid-patients`,`unpaid-patients` ";

			$sqlProcedureBill = "SELECT IFNULL(SUM(CASE WHEN patient_procedure_bill.`status` = 'paid' 
			THEN patient_procedure_bill.charge ELSE 0 END),1) as `paid-bill`,IFNULL(SUM(CASE WHEN 
			patient_procedure_bill.`status` = 'Unpaid' THEN patient_procedure_bill.charge 
			ELSE 0 END),1) as `unpaid-bill` FROM patient_procedure_bill
			WHERE patient_procedure_bill.created_on >= unix_timestamp(curdate())";

			/*patint forensic bill*/
			$queryForensicBill = "SELECT IFNULL(SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' 
			THEN 1 ELSE 0 END),1) as `paid-patients`,IFNULL(SUM(CASE WHEN 
			patient_forensic_bill.`status` = 'Unpaid' THEN 1 ELSE 0 END),1) as 
			`unpaid-patients`,forensic_tests.name FROM patient_forensic_bill
			LEFT JOIN forensic_tests ON forensic_tests.id = patient_forensic_bill.forensic_test_id
			WHERE patient_forensic_bill.created_on >= unix_timestamp(curdate()) 
			GROUP BY forensic_tests.id ORDER by `paid-patients`,`unpaid-patients`  ";

			$sqlForensicBill = "SELECT IFNULL(SUM(CASE WHEN patient_forensic_bill.`status` = 'paid' 
			THEN patient_forensic_bill.charge ELSE 0 END),1) as `paid-bill`,IFNULL(SUM(CASE WHEN 
			patient_forensic_bill.`status` = 'Unpaid' THEN patient_forensic_bill.charge ELSE 0 END),1)
			as `unpaid-bill` FROM patient_forensic_bill
			WHERE patient_forensic_bill.created_on >= unix_timestamp(curdate())";

			$visitQuery = "SELECT COUNT(*) as count,(CASE WHEN ISNULL(department.department) THEN 'Others' ELSE department.department END) AS department FROM `visits` 
				LEFT JOIN department ON department.id = visits.department
				where visits.created_on >= unix_timestamp(CURDATE())
				GROUP BY department.id ORDER BY COUNT(*) desc";
		

		$Allrows   = array();

		/*Connection for patient lab bill query*/
		$queryResultLabBill  = mysqli_query($conn,$queryLabBill);	
		$rowsQueryLabBill   = array();
	    while ($r = mysqli_fetch_assoc($queryResultLabBill)) {
	        $rowsQueryLabBill[] = $r;
	    }

	    $sqlResultLabBill  = mysqli_query($conn,$sqlLabBill);		
		$rowsSqlLabBill   = array();
	    while ($r = mysqli_fetch_assoc($sqlResultLabBill)) {
	        $rowsSqlLabBill[] = $r;
	    }

	    /*Connection for patient radiology bill query*/
		$queryResultRadiologyBill  = mysqli_query($conn,$queryRadiologyBill);	
		$rowsQueryRadiologyBill   = array();
	    while ($r = mysqli_fetch_assoc($queryResultRadiologyBill)) {
	        $rowsQueryRadiologyBill[] = $r;
	    }

	    $sqlResultRadiologyBill  = mysqli_query($conn,$sqlRadiologyBill);		
		$rowsSqlRadiologyBill   = array();
	    while ($r = mysqli_fetch_assoc($sqlResultRadiologyBill)) {
	        $rowsSqlRadiologyBill[] = $r;
	    }

	    /*Connection for service radiology bill query*/
		$queryResultServiceBill  = mysqli_query($conn,$queryServiceBill);	
		$rowsQueryServiceBill   = array();
	    while ($r = mysqli_fetch_assoc($queryResultServiceBill)) {
	        $rowsQueryServiceBill[] = $r;
	    }

	    $sqlResultServiceBill  = mysqli_query($conn,$sqlServiceBill);		
		$rowsSqlServiceBill   = array();
	    while ($r = mysqli_fetch_assoc($sqlResultServiceBill)) {
	        $rowsSqlServiceBill[] = $r;
	    }

	    /*Connection for patient procedure bill query*/
		$queryResultProcedureBill  = mysqli_query($conn,$queryProcedureBill);	
		$rowsQueryProcedureBill   = array();
	    while ($r = mysqli_fetch_assoc($queryResultProcedureBill)) {
	        $rowsQueryProcedureBill[] = $r;
	    }

	    $sqlResultProcedureBill  = mysqli_query($conn,$sqlProcedureBill);		
		$rowsSqlProcedureBill   = array();
	    while ($r = mysqli_fetch_assoc($sqlResultProcedureBill)) {
	        $rowsSqlProcedureBill[] = $r;
	    }


	    /*Connection for patient forensic bill query*/
		$queryResultForensicBill  = mysqli_query($conn,$queryForensicBill);	
		$rowsQueryForensicBill   = array();
	    while ($r = mysqli_fetch_assoc($queryResultForensicBill)) {
	        $rowsQueryForensicBill[] = $r;
	    }

	    $sqlResultForensicBill  = mysqli_query($conn,$sqlForensicBill);		
		$rowsSqlForensicBill   = array();
	    while ($r = mysqli_fetch_assoc($sqlResultForensicBill)) {
	        $rowsSqlForensicBill[] = $r;
	    }

	    /*Connection for visist query*/
	    $visitResultQuery  = mysqli_query($conn,$visitQuery);		
		$rowsVisited   = array();
	    while ($r = mysqli_fetch_assoc($visitResultQuery)) {
	        $rowsVisited[] = $r;
	    }

	    array_push($Allrows, $rowsQueryLabBill);
	    array_push($Allrows, $rowsSqlLabBill);
	    array_push($Allrows, $rowsQueryRadiologyBill);
	    array_push($Allrows, $rowsSqlRadiologyBill);
	    array_push($Allrows, $rowsQueryServiceBill);
	    array_push($Allrows, $rowsSqlServiceBill);
	    array_push($Allrows, $rowsQueryProcedureBill);
	    array_push($Allrows, $rowsSqlProcedureBill);
	    array_push($Allrows, $rowsQueryForensicBill);
	    array_push($Allrows, $rowsSqlForensicBill);
	    array_push($Allrows, $rowsVisited);

	    echo json_encode($Allrows);
	}
?>