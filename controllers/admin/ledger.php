<?php
/*
 * File Name    :   diseases.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre details form database
 */

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
    
$operation        = "";
$diseasesCategory = "";
$description      = "";
$createdate       = new DateTime();
include 'config.php';


if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
    
}
 if ($operation == "showParentGroup") // show category in category selection box
    {
    $query  = "SELECT id,name FROM patient_account";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
} 

// for save
if ($operation == "save") {
    if (isset($_POST['Code'])) {
        $code = $_POST['Code'];
    }
    if (isset($_POST['Name'])) {
        $name = $_POST['Name'];
    }
    if (isset($_POST['parentId'])) {
        $parentId = $_POST['parentId'];
    }
    if (isset($_POST['balance'])) {
        $balance = $_POST['balance'];
    }
    if (isset($_POST['type'])) {
        $type = $_POST['type'];
    }
	$description = $_POST['description'];
    $unit = $_POST['unit'];

    if($parentId == '-1'){
        if($unit == "1"){
            $sql    = "INSERT INTO patient_account(name,code,description,db_balance,type) VALUES('" . $name . "','" . $code . "','" . $description . "','" . $balance . "','" . $type . "')";
            $result = mysqli_query($conn, $sql);
            echo $result;
        }
        else{
            $sql    = "INSERT INTO patient_account(name,code,description,cr_balance,type) VALUES('" . $name . "','" . $code . "','" . $description . "','" . $balance . "','" . $type . "')";
            $result = mysqli_query($conn, $sql);
            echo $result;
        }
    }
    else if($unit == "1"){
        echo $sql    = "INSERT INTO patient_account(name,code,description,db_balance,parent_id,type) VALUES('" . $name . "','" . $code . "','" . $description . "','" . $balance . "','" . $parentId . "','" . $type . "')";
        $result = mysqli_query($conn, $sql);
        echo $result;
    }
    else{
         $sql    = "INSERT INTO patient_account(name,code,description,cr_balance,parent_id,type) VALUES('" . $name . "','" . $code . "','" . $description . "','" . $balance . "','" . $parentId . "','" . $type . "')";
        $result = mysqli_query($conn, $sql);
        echo $result;
    }
    
}
// for show
if ($operation == "show") {
    
    $query        = "select * from patient_account";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
    
}
// opertaion form update
if ($operation == "update") {
    if (isset($_POST['Code'])) {
        $code = $_POST['Code'];
    }
    if (isset($_POST['Name'])) {
        $name = $_POST['Name'];
    }
    if (isset($_POST['parentId'])) {
        $parentId = $_POST['parentId'];
    }
    if (isset($_POST['balance'])) {
        $balance = $_POST['balance'];
    }
    if (isset($_POST['type'])) {
        $type = $_POST['type'];
    }
    $description = $_POST['description'];
    $unit = $_POST['unit'];
    
    if (isset($_POST['Id'])) {
        $id = $_POST['Id'];
    }

    if($parentId == '-1'){
        if($unit == "1"){
            echo $sql    = "update patient_account set code= '" . $code . "' , name = '" . $name . "', description='" . $description . "', db_balance='" . $balance . "', type='" . $type . "' where id = '" . $id . "'";
            $result = mysqli_query($conn, $sql);
            echo $result;
        }
        else{
            echo $sql    = "update patient_account set code= '" . $code . "' , name = '" . $name . "', description='" . $description . "', cr_balance='" . $balance . "', type='" . $type . "' where id = '" . $id . "'";
            $result = mysqli_query($conn, $sql);
            echo $result;
        }
    }
    else if($unit == "1"){
        $sql    = "update patient_account set code= '" . $code . "' , name = '" . $name . "', description='" . $description . "',  db_balance='" . $balance . "', parent_id='" . $parentId . "', type='" . $type . "' where id = '" . $id . "'";
        $result = mysqli_query($conn, $sql);
        echo $result;
    }
    else{
        $sql    = "update patient_account set code= '" . $code . "' , name = '" . $name . "', description='" . $description . "',  cr_balance='" . $balance . "', parent_id='" . $parentId . "', type='" . $type . "' where id = '" . $id . "'";
        $result = mysqli_query($conn, $sql);
        echo $result;
    }    
    
}
//  operation for delete
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "DELETE FROM patient_account where id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $sql;
}
//When checked box is check
if ($operation == "checked") {
    
    $query        = "select * from diseases where diseases.status='I'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}
// for restore
if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $category = $_POST['category'];
    $sql      = "UPDATE diseases SET status= 'A'  WHERE  id = '" . $id . "'";
    $result   = mysqli_query($conn, $sql);
    if ($result == 1) {
        echo "1";
    }
}
?>