<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
    $operation = "";
	$unitMeasure = "";
	$testName = "";
	$normalRange = "";
	$test = "";
	$name="";
	$description = "";
	$date = new DateTime();
	
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	
	//It is use for select the units 
	if($operation == "showUnitMeasure")			
	{
		
		$query="Select id,unit FROM units WHERE status='A'";
		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 print json_encode($rows);
	}
	
	if($operation == "showTest")			
	{
		
		$query="Select id,name FROM forensic_tests WHERE status='A'";
		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 print json_encode($rows);
	}
	// It is use for save the data
	if($operation == "save")			
	{
		if (isset($_POST['unitMeasure'])) {
			$unitMeasure=$_POST['unitMeasure'];
		}
		if (isset($_POST['test'])) {
			$test=$_POST['test'];// add forensic sub test
		}
		if (isset($_POST['testName'])) {
			$testName=$_POST['testName'];//select forensic test 
		}
		if (isset($_POST['normalRange'])) {
			$normalRange=$_POST['normalRange'];
		}
		if (isset($_POST['description'])) {
			$description=$_POST['description'];
		}
		//to check wheteher name exisit already or not
		$sql1 = "SELECT name from forensic_sub_tests where name='".$test."'";
		$resultSelect=mysqli_query($conn,$sql1);
		$rows_count= mysqli_num_rows($resultSelect);
		
		if($rows_count <= 0){
		
			$sql = "INSERT INTO forensic_sub_tests(name,`range`,unit_id,description,forensic_type_id,created_on,updated_on)
			VALUES('".$test."','".$normalRange."','".$unitMeasure."','".$description."','".$testName."',".$date->getTimestamp().",".$date->getTimestamp().")";
			$result= mysqli_query($conn,$sql);  
			echo $result;
		}
		else{
			echo "0";
		}
	} 
	
	if($operation == "show"){
		
		$query= "SELECT forensic_sub_tests.id, forensic_sub_tests.name,forensic_sub_tests.`range`, units.unit,
		forensic_sub_tests.unit_id,	forensic_sub_tests.description, forensic_sub_tests.forensic_type_id, 
		forensic_tests.name as test_name FROM forensic_sub_tests
		LEFT JOIN units ON  forensic_sub_tests.unit_id = units.id
		LEFT JOIN forensic_tests ON forensic_sub_tests.forensic_type_id = forensic_tests.id
		WHERE forensic_sub_tests.status = 'A' AND forensic_tests.status = 'A'";
		/* $query= "select id, ward_id,number,description from rooms"; */
		/* echo $query; */
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 //print json_encode($rows);
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);
	}
	
	if($operation == "checked"){
		
		$query= "SELECT forensic_sub_tests.id, forensic_sub_tests.name,forensic_sub_tests.`range`, units.unit,
		forensic_sub_tests.unit_id,	forensic_sub_tests.description, forensic_sub_tests.forensic_type_id, 
		forensic_tests.name as test_name FROM forensic_sub_tests
		LEFT JOIN units ON  forensic_sub_tests.unit_id = units.id
		LEFT JOIN forensic_tests ON forensic_sub_tests.forensic_type_id = forensic_tests.id
		WHERE forensic_sub_tests.status = 'I' OR forensic_tests.status = 'I'";
		/* $query= "select id, ward_id,number,description from rooms"; */
		/* echo $query; */
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 //print json_encode($rows);
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);
	}
	
	if($operation == "delete")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		
		$sql = "UPDATE forensic_sub_tests SET status= 'I'  WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		if (isset($_POST['test_name'])) {
			$test_name=$_POST['test_name'];
		}
		
		$sql = "UPDATE forensic_sub_tests SET status= 'A' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);
		
		if($result == 1){
			$query_test_name = "SELECT status from forensic_tests where id='".$test_name."' And status='A'";
			$result_test_name= mysqli_query($conn,$query_test_name);
			$count_test_name= mysqli_num_rows($result_test_name);
			if($count_test_name == '1'){
			echo "Restored Successfully!!!";	
			}
			else{
				echo "It can not be restore!!!";				
			}
		}
	}
	
	if($operation == "update")			
	{

		if (isset($_POST['unit_id'])) {
			$unit_id=$_POST['unit_id'];
		}
		if (isset($_POST['test'])) {
			$test=$_POST['test'];
		}
		if (isset($_POST['subTestName'])) {
			$subTestName=$_POST['subTestName'];
		}
		if (isset($_POST['range'])) {
			$range=$_POST['range'];
		}
		if (isset($_POST['description'])) {
			$description=$_POST['description'];
		}
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		$sql1 = "SELECT name from forensic_sub_tests where name='".$test."' And forensic_type_id='".$subTestName."' and id != '$id'";
	    $resultSelect=mysqli_query($conn,$sql1);
		$rows_count= mysqli_num_rows($resultSelect);
		if($rows_count == 0){		
			$sql = "UPDATE forensic_sub_tests SET name= '".$test."',forensic_type_id= '".$subTestName."',`range`= '".$range."',unit_id= '".$unit_id."',description='".$description."',updated_on=".$date->getTimestamp()." WHERE  id = '".$id."'";
			$result= mysqli_query($conn,$sql);  
			echo $result;
		}
		else{
			echo "0";
		}
	}	
?>
