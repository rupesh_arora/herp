<?php
/*
 * File Name    :   stocking.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar gupta
 * Created Date :   16th feb, 2016
 * Description  :   This page manage stocking details 
 */
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
$operation = "";

include 'config.php'; // import database connection file    
$operation = $_POST["operation"]; // get operation from js file

// fetch value from patient table
if ($operation == "save") {
	$item = $_POST['item'];
	$quantity = $_POST['quantity'];
	$shelNo = $_POST['shelNo'];
	$notes = $_POST['notes'];
	$stockingDate = $_POST['stockingDate'];

	$sqlSelect = "SELECT Item from stocking WHERE item = ".$item."";
	$selectResult        = mysqli_query($conn, $sqlSelect);
	$rowCount = mysqli_num_rows($selectResult);
		if($rowCount > 0) {

			if($_POST['expireDate'] == "") {
				$sql = "UPDATE stocking SET item = '".$item."',Quantity = '" . $quantity . "',notes = '" . $notes . "',shelf_no = '" . $shelNo . "',stocking_date = '" . $stockingDate . "' WHERE item = '" . $item . "'";
				$result        = mysqli_query($conn, $sql);
			}

			else {
			   $expireDate = $_POST['expireDate'];
			  
				$sql =  "UPDATE stocking SET item = '" . $item . "',Quantity = '" . $quantity . "',notes = '" . $notes . "'	,shelf_no = '" . $shelNo . "',stocking_date = '" . $stockingDate . "',expire_date = '".$expireDate."' WHERE item = '" . $item . "'";
				$result        = mysqli_query($conn, $sql);
			}
		}

		else {
		   if($_POST['expireDate'] == "") {
			$sql = "INSERT INTO stocking (Item,Quantity,notes,shelf_no,stocking_date)
			VALUES('" . $item . "','" . $quantity . "','" . $notes . "','" . $shelNo . "','" . $stockingDate . "')";
			$result        = mysqli_query($conn, $sql);
		  }
		  else {
			   $expireDate = $_POST['expireDate'];
				$sql = "INSERT INTO stocking (Item,Quantity,expire_date,notes,shelf_no,stocking_date)
				VALUES('" . $item . "','" . $quantity . "','" . $expireDate . "','" . $notes . "','" . $shelNo . "','" . $stockingDate . "')";
				$result        = mysqli_query($conn, $sql);
		  }
		}
	 
	//echo $result;
	if ($result) {
		echo "1";
	} 
	else {
		echo "0";
	}
}

if($operation == "searchDetails") {
	$expireDate ="";
	$chkData = "";
	  $item = $_POST['item'];
	  $quantity = $_POST['quantity'];
	  if(isset($_POST['expireDate'])) {
		  $expireDate = $_POST['expireDate'];
	  }
	  if(isset($_POST['chkData'])) {
		  $chkData = $_POST['chkData'];
	  }	 
	  $shelNo = $_POST['shelNo'];
	  $notes = $_POST['notes'];
	  $stockingDate = $_POST['stockingDate'];
	  
	   $isFirst = "false";
    
       $query = "SELECT stocking.*,items.name AS itemName FROM stocking LEFT JOIN items ON items.id = stocking.item ";
	   
	   if ($item != '' || $quantity != '' || $expireDate != '' || $shelNo != '' || $notes != '' || $stockingDate !='' || $chkData !='') {
            $query .= " WHERE ";
        }
		if ($item != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " item LIKE '%" . $item . "%'";
            $isFirst = "true";
        }
        if ($quantity != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " Quantity LIKE '%" . $quantity . "%'";
            $isFirst = "true";
        }
        if ($expireDate != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " expire_date ='" . $expireDate . "'";
            $isFirst = "true";
        }
		if ($chkData != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " expire_date IS NULL";
            $isFirst = "true";
        }
        if ($shelNo != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " shelf_no LIKE '%" . $shelNo . "%'";
            $isFirst = "true";
        }
        if ($notes != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " notes LIKE '%" . $notes . "%'";
            $isFirst = "true";
        }
		if ($stockingDate != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " stocking_date LIKE '%" . $stockingDate . "%'";
            $isFirst = "true";
        }
		$result = mysqli_query($conn, $query);
		$rows   = array();
		while ($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
	
		print json_encode($rows);
}
?>