<?php
/*****************************************************************************************************
 * File Name    :   triage.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages triage data
 ****************************************************************************************************/
 
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	$operation="";
	include 'config.php';

	$date = new DateTime();
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	else{}
	
	//This operation is used for select data on load button
	if($operation == "showData"){

		if (isset($_POST['visitId'])) {
			$visitId=$_POST['visitId'];
		}	
		$visitPrefix = $_POST['visitPrefix'];
		$sql       = "SELECT value FROM configuration WHERE name = 'visit_prefix'";
		$result    = mysqli_query($conn, $sql);
		
		while ($r = mysqli_fetch_assoc($result)) {
			$prefix = $r['value'];
		}
		if($prefix == $visitPrefix){
			// query check for paid and unpaid value
			$query = "select sum(cash_account.credit - cash_account.debit) as amount from cash_account 
			Left Join visits on cash_account.patient_id = visits.patient_id where visits.id = '".$visitId."'";
			$result=mysqli_query($conn,$query);
			while($row = mysqli_fetch_row($result)){
				
				if($row['0'] >= 0){
					
					$sqlSelect="SELECT DISTINCT(patients.id), patients.first_name, patients.dob, patients.gender, patients.images_path, visits.created_on,triage.weight, triage.temperature, triage.height, triage.pulse, 
					triage.blood_pressure, triage.head_circum, triage.body_surface_area, triage.respiration_rate, 
					triage.oxygen_saturation, triage.heart_rate, triage.bmi, triage.notes,visit_type.`type` , 
					(SELECT value FROM configuration WHERE name = 'patient_prefix') AS patient_prefix from patients 
					LEFT JOIN visits ON visits.patient_id = patients.id 
					LEFT JOIN visit_type ON visits.visit_type = visit_type.id 
					LEFT JOIN patient_service_bill ON visits.id = patient_service_bill.visit_id 
					LEFT JOIN triage ON visits.id = triage.visit_id
					where visits.id= '".$visitId."' AND visit_type.`type` != 'Self Request' AND 	patient_service_bill.`status` = 'paid'";
						
					$resultSelect=mysqli_query($conn,$sqlSelect);
					$rows_count= mysqli_num_rows($resultSelect);
					if($rows_count > 0){
						$rows = array();
						while($r = mysqli_fetch_assoc($resultSelect)) {
							$rows[] = $r;
						}
						
					   print json_encode($rows);
					}
					else{
					   echo "0";
					}   
				}
				else{
					echo "0";
				}				
			}
		}
		else{
			echo "2";
		}
	}
	
	//This operation is used for save and update the data 
	if($operation == "save")			
	{	
		if (isset($_POST['visitId'])) {
			$visitId=$_POST['visitId'];
		}
		if (isset($_POST['patientId'])) {
			$patientId=$_POST['patientId'];
		}
		if (isset($_POST['weight'])) {
			$weight=$_POST['weight'];
		}
		if (isset($_POST['temprature'])) {
			$temprature=$_POST['temprature'];
		}
		if (isset($_POST['height'])) {
			$height=$_POST['height'];
		}
		if (isset($_POST['pulse'])) {
			$pulse=$_POST['pulse'];
		}
		if (isset($_POST['bloodPressure'])) {
			$bloodPressure=$_POST['bloodPressure'];
		}
		if (isset($_POST['headCircumference'])) {
			$headCircumference=$_POST['headCircumference'];
		}
		if (isset($_POST['bodySurfaceArea'])) {
			$bodySurfaceArea=$_POST['bodySurfaceArea'];
		}
		if (isset($_POST['respirationRate'])) {
			$respirationRate=$_POST['respirationRate'];
		}
		if (isset($_POST['oxygenSaturation'])) {
			$oxygenSaturation=$_POST['oxygenSaturation'];
		}	
		if (isset($_POST['heartRate'])) {
			$heartRate=$_POST['heartRate'];
		}
		if (isset($_POST['bmi'])) {
			$bmi=$_POST['bmi'];
		}
		if (isset($_POST['notes'])) {
			$notes=$_POST['notes'];
		}	
		$sql_select = "SELECT COUNT(*) from triage where visit_id=".$visitId."";  
		$result_select = mysqli_query($conn,$sql_select);  
		while ($row=mysqli_fetch_row($result_select))
		{
			$count = $row[0];
		}
		//If visit id not available in triage table than return 0 and save else update 
		if($count == 0)
		{			
			$sql = "INSERT INTO triage(visit_id, patient_id, weight, temperature, height, pulse, blood_pressure, head_circum,
			body_surface_area, respiration_rate, oxygen_saturation, heart_rate, bmi, notes, created_on, created_by)
			VALUES('".$visitId."', '".$patientId."','".$weight."', '".$temprature."', '".$height."', '".$pulse."', '".$bloodPressure."', 
			'".$headCircumference."', '".$bodySurfaceArea."', '".$respirationRate."', '".$oxygenSaturation."', 
			'".$heartRate."', '".$bmi."', '".$notes."',".$date->getTimestamp().", '".$_SESSION['globaluser']."')";		
			$result= mysqli_query($conn,$sql);  
			
			if($result){
				$sqlUpdate = "UPDATE visits set triage_status = 'Taken' WHERE id = '".$visitId."'";
				$resultUpdate= mysqli_query($conn,$sqlUpdate);
				echo $resultUpdate;
			}
		}
		else{

			$sql = "UPDATE `triage` SET `weight`='".$weight."',`temperature`='".$temprature."',`height`='".$height."',`pulse`='".$pulse."',`blood_pressure`='".$bloodPressure."',`head_circum`='".$headCircumference."',
			`body_surface_area`='".$bodySurfaceArea."',`respiration_rate`='".$respirationRate."',`oxygen_saturation`='".$oxygenSaturation."',
			`heart_rate`='".$heartRate."',`bmi`='".$bmi."',`notes`='".$notes."',
			`created_on`=".$date->getTimestamp().",`created_by`='".$_SESSION['globaluser']."' WHERE `visit_id` = ".$visitId."";			
			$result= mysqli_query($conn,$sql);  
			echo $result;
		}					
	} 
?>