<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}
	if ($operation == "showInventory") { // show staff type
	    $query = "SELECT id,type FROM staff_type where status = 'A'";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	if($operation == "showExpireItem") {
		$inventoryType = $_POST['inventoryType'];

		$sql = "SELECT items.name AS item_name,seller.name AS seller_name,received_orders.mfg_date,received_orders.exp_date FROM received_orders
			LEFT JOIN orders ON orders.id = received_orders.order_id
			LEFT JOIN seller ON seller.id = orders.seller_id
			LEFT JOIN items ON items.id = received_orders.item_id 
			LEFT JOIN item_categories ON item_categories.id = items.item_category_id 
			WHERE item_categories.inventory_type_id ='".$inventoryType."' 
			AND received_orders.exp_date < CURDATE()";

		$result = mysqli_query($conn,$sql);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);
	}

?>