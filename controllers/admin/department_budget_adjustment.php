<?php
/*File Name  :   put_orders.php
Company Name :   Qexon Infotech
Created By   :   Kamesh Pathak
Created Date :   16th FEB, 2016
Description  :   This page manages all the orders of inventory*/

session_start(); //start session

if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
	exit();
}
/*include config file*/
include 'config.php';

$operation = "";

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
} //nothing


if ($operation == "show") {

	$departmentId = $_POST['departmentId'] ;
	$query        = "SELECT budget_matrix.id,chart_of_account.id AS account_id, budget_matrix.amount,chart_of_account.account_no,chart_of_account.account_name  FROM budget_matrix
		LEFT JOIN chart_of_account ON chart_of_account.id = budget_matrix.gl_account_id
		LEFT JOIN department_budget ON department_budget.id = budget_matrix.department_budget_id
		WHERE department_budget.department_id = '".$departmentId."' AND budget_matrix.`status` = 'A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    echo json_encode($rows);
}

if ($operation == "saveAmount") {
	$array = json_decode($_POST['array']);
	$departmentId = $_POST['departmentId'];

	$insertQry = "INSERT INTO department_budget_adjustment(account_id,department_id,amount,notes,created_on,updated_on,created_by,updated_by) VALUES ";
	$updateQry = '';
	$subQry = '';
	$counter = 0;

	foreach ($array as $value) {
		$accountId = $value->accountId;
		$amount = $value->amount;
		$remarks = $value->remarks;
		$budgetMatrixId = $value->budgetMatrixId;

		if ($counter > 0) {
			$subQry.= ',';
		}

		$subQry.= "('".$accountId."','".$departmentId."','".$amount."','".$remarks."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";

		$counter++;

		$updateQry.= "UPDATE budget_matrix SET amount = amount +'".$amount ."' WHERE id='".$budgetMatrixId."';";

	}

	$completeQry = $insertQry.$subQry.';'.$updateQry;
	$result = mysqli_multi_query($conn,$completeQry);
	echo $result;
}
?>