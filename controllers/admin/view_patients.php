<?php
/*
	 * File Name    :   view_patients.php
	 * Company Name :   Qexon Infotech
	 * Created By   :   Tushar Gupta
	 * Created Date :   29th dec, 2015
	 * Description  :   use for search patient list, update and show.
*/
$operation = "";
$firstName = "";
$lastName  = "";
$dob       = "";
$mobile    = "";
$email     = "";
$id        = "";
$userId    = "";
$date      = new DateTime();
include 'config.php'; // impoert data base connection file

session_start();						// session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];  // set user_id through session
}
else{
    exit();
}

if (isset($_POST['operation'])) {  // opearion value come from js file to perform functioanlity. 
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

/* use for image uploading */
if (isset($_FILES["file"]["type"])){
   $validextensions = array(
        "jpeg",
        "jpg",
        "png",
        "gif"
    );
    $temporary       = explode(".", $_FILES["file"]["name"]);
    
    $extension = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION); // getting extension
	$str = 'abcdef';
	$imagename = str_shuffle($str). $date->getTimestamp();
    
    $image_name = $imagename . "." . $extension;
    $name       = str_replace(" ", "", $image_name); // remove space
    
    $file_extension = end($temporary);
    if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg")) && ($_FILES["file"]["size"] < 2002000) //Approx. 2mb files can be uploaded.
        && in_array($file_extension, $validextensions)) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
        } else {
            if (file_exists("../../upload_images/patient_dp/" . $name)) {
                echo "";
            } else {
                
                $sourcePath = $_FILES["file"]["tmp_name"]; // Storing source path of the file in a variable
                $targetPath = "../../upload_images/patient_dp/" . $name; // Target path where file is to be stored
                move_uploaded_file($sourcePath, $targetPath); // Moving Uploaded file
                
                echo $name;
                
            }
        }
    } else {
        echo "invalid file";
    }
}

// show country
if ($operation == "showcountry") {
    $query = "SELECT location_id,name FROM locations where location_type = 0";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

// show state
if ($operation == "showstate") {
    $countryID = $_POST['country_ID'];
    $query     = "SELECT location_id,name FROM locations where location_type = 1 and parent_id = '$countryID'";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

// show city
if ($operation == "showcity") {
    $stateID = $_POST['state_ID'];
    $query   = "SELECT location_id,name FROM locations where location_type = 2 and parent_id = '$stateID'";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

// show patient details
if ($operation == "patientDetail") {
    
    if (isset($_POST['id'])) {
        $id = $_POST["id"];
    }
    
    $query  = "SELECT p.id,p.fan_id,CONCAT(p.salutation,' ',p.first_name,' ',p.middle_name,' ',p.last_name ) AS patient_name,p.address,p.dob, p.email, p.mobile, CASE WHEN p.country_code = '' OR p.country_code = NULL THEN '0' ELSE p.country_code END AS country_code,
            p.care_of,p.care_contact,p.phone,p.gender,p.zip,p.images_path,
            (SELECT name FROM locations l WHERE l.location_id = p.country_id) AS country,
            (SELECT name FROM locations l WHERE l.location_id = p.state_id) AS state,
            (SELECT name FROM locations l WHERE l.location_id = p.city_id) AS city,
            (select value from configuration where name = 'patient_prefix') as patient_prefix,
            (select value from configuration where name = 'account_prefix') as account_prefix            
            FROM patients AS p  WHERE id= '" . $id . "' AND status= 'A'";
    $result = mysqli_query($conn, $query);
    //$totalrecords = mysqli_num_rows($result);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
    
    //$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
    //echo json_encode($json);        
}

// show patient deatils for search patient opearion
if ($operation == "searchPatient") {
    
    if (isset($_POST['id'])) {
        $id = $_POST["id"];
    }
    
    $query  = "SELECT p.id,CONCAT(p.first_name,' ',p.middle_name,' ',p.last_name ) AS patient_name,p.address,p.dob, p.email, p.mobile, CASE WHEN p.country_code = '' OR p.country_code = NULL THEN '0' ELSE p.country_code END AS country_code,
            p.care_of,p.care_contact,p.phone,p.gender,p.zip,
            (SELECT name FROM locations l WHERE l.location_id = p.country_id) AS country,
            (SELECT name FROM locations l WHERE l.location_id = p.state_id) AS state,
            (SELECT name FROM locations l WHERE l.location_id = p.city_id) AS city,
            (select value from configuration where name = 'patient_prefix') as prefix           
            FROM patients AS p  WHERE id= '" . $id . "' AND status= 'A'";
    $result = mysqli_query($conn, $query);
    //$totalrecords = mysqli_num_rows($result);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
    
    //$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
    //echo json_encode($json);        
}

// search Today patient opearion
if ($operation == "searchToday") {
	 $query = "SELECT id,CONCAT(first_name,' ',middle_name,' ',last_name) AS name, dob, email, mobile,CASE WHEN country_code = '' OR country_code = NULL THEN '0' ELSE country_code END AS country_code,
        (select value from configuration where name = 'patient_prefix') as prefix FROM patients WHERE 
        ((DATE_FORMAT(FROM_UNIXTIME(patients.created_on), '%y-%m-%d')) = CURDATE()) ";
		
	$result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

// search patient opearion
if ($operation == "search") {
    
    if (isset($_POST['firstName'])) {
        $firstName = $_POST["firstName"];
    }
    if (isset($_POST['lastName'])) {
        $lastName = $_POST["lastName"];
    }
    if (isset($_POST['dob'])) {
        $dob = $_POST["dob"];
    }
    if (isset($_POST['mobile'])) {
        $mobile = $_POST["mobile"];
    }
    if (isset($_POST['email'])) {
        $email = $_POST["email"];
    }


    /*if click data is set then make functional search query*/
    if (isset($_POST['clickData'])) {
        $clickData = $_POST["clickData"];		
		
        $isFirst = "false";
    
        $query = "SELECT id,CONCAT(first_name,' ',middle_name,' ',last_name) AS name, dob, email, mobile, CASE WHEN country_code = '' OR country_code = NULL THEN '0' ELSE country_code END AS country_code,
            (select value from configuration where name = 'patient_prefix') as prefix FROM patients ";

        if (isset($_POST['showVisitPatients'])) {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .=" WHERE id IN(SELECT DISTINCT(patient_id) FROM visits WHERE visit_type !=4)";
            $isFirst = "true";
        }
        else{
            if ($firstName != '' || $lastName != '' || $dob != '' || $mobile != '' || $email != '') {
                $query .= " WHERE ";
            }
            else{

            }
        }        
            
        if ($firstName != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " first_name LIKE '%" . $firstName . "%'";
            $isFirst = "true";
        }
        if ($lastName != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " last_name LIKE '%" . $lastName . "%'";
            $isFirst = "true";
        }
        if ($dob != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " dob ='" . $dob . "'";
            $isFirst = "true";
        }
        if ($mobile != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " mobile LIKE '%" . $mobile . "%'";
            $isFirst = "true";
        }
        if ($email != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " email LIKE '%" . $email . "%'";
            $isFirst = "true";
        }        
    }    
    
    else{
        $query = "SELECT id,CONCAT(first_name,' ',middle_name,' ',last_name) AS name, dob, email, mobile,CASE WHEN country_code = '' OR country_code = NULL THEN '0' ELSE country_code END AS country_code,
            (select value from configuration where name = 'patient_prefix') 
            as prefix FROM patients WHERE DATE_FORMAT(FROM_UNIXTIME(patients.created_on), '%y-%m-%d') = CURDATE()";
        if (isset($_POST['showVisitPatients'])) {
            $query .=" AND id IN(SELECT DISTINCT(patient_id) FROM visits WHERE visit_type !=4)";
        }
    }
    
    $result = mysqli_query($conn, $query);
    //$totalrecords = mysqli_num_rows($result);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
    print json_encode($rows);
}

// search patient opearion
if ($operation == "searchIPD") {
    
    if (isset($_POST['firstName'])) {
        $firstName = $_POST["firstName"];
    }
    if (isset($_POST['lastName'])) {
        $lastName = $_POST["lastName"];
    }
    if (isset($_POST['dob'])) {
        $dob = $_POST["dob"];
    }
    if (isset($_POST['mobile'])) {
        $mobile = $_POST["mobile"];
    }
    if (isset($_POST['email'])) {
        $email = $_POST["email"];
    }


    /*if click data is set then make functional search query*/
    if (isset($_POST['clickData'])) {
        $clickData = $_POST["clickData"];       
        
        $isFirst = "false";
    
        $query = "SELECT p.id,CONCAT(p.first_name,' ',p.middle_name,' ',p.last_name) AS name, p.dob, p.email, p.mobile,
            (select value from configuration where name = 'patient_prefix') as prefix,ipr.operating_consultant_id,ipr.attending_consultant_id,
                ipr.admission_reason FROM patients AS p
            LEFT JOIN ipd_registration AS ipr ON ipr.patient_id = p.id ";
        
        if ($firstName != '' || $lastName != '' || $dob != '' || $mobile != '' || $email != '') {
            $query .= " WHERE ";
        }
        else{

        }
        if ($firstName != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " p.first_name LIKE '%" . $firstName . "%'";
            $isFirst = "true";
        }
        if ($lastName != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " p.last_name LIKE '%" . $lastName . "%'";
            $isFirst = "true";
        }
        if ($dob != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " p.dob ='" . $dob . "'";
            $isFirst = "true";
        }
        if ($mobile != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " p.mobile LIKE '%" . $mobile . "%'";
            $isFirst = "true";
        }
        if ($email != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " p.email LIKE '%" . $email . "%'";
            $isFirst = "true";
        }
    }    
    
    else{
        $query = "SELECT p.id,CONCAT(p.first_name,' ',p.middle_name,' ',p.last_name) AS name, p.dob, p.email, p.mobile,
                 (select value from configuration where name = 'patient_prefix') as prefix,ipr.operating_consultant_id,ipr.attending_consultant_id,
                 ipr.admission_reason FROM patients AS p
                 LEFT JOIN ipd_registration AS ipr ON ipr.patient_id = p.id  WHERE DATE_FORMAT(FROM_UNIXTIME(ipr.created_on), '%y-%m-%d') = CURDATE()";
    }
        
    $result = mysqli_query($conn, $query);
    //$totalrecords = mysqli_num_rows($result);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
    print json_encode($rows);
}

// search patient opearion
if ($operation == "searchIpdPatient") {
    
    if (isset($_POST['firstName'])) {
        $firstName = $_POST["firstName"];
    }
    if (isset($_POST['lastName'])) {
        $lastName = $_POST["lastName"];
    }
    if (isset($_POST['dob'])) {
        $dob = $_POST["dob"];
    }
    if (isset($_POST['mobile'])) {
        $mobile = $_POST["mobile"];
    }
    if (isset($_POST['email'])) {
        $email = $_POST["email"];
    }


    /*if click data is set then make functional search query*/
    if (isset($_POST['clickData'])) {
        $clickData = $_POST["clickData"];       
        
        $isFirst = "false";
    
        $query = "SELECT i.id AS ipd_id, p.id,CONCAT(p.first_name,' ',p.middle_name,' ',p.last_name) AS name, p.dob, p.email, p.mobile,
            (select value from configuration where name = 'ipd_prefix') as prefix FROM ipd_registration AS i
                LEFT JOIN patients AS p ON p.id = i.patient_id ";
        
        if ($firstName != '' || $lastName != '' || $dob != '' || $mobile != '' || $email != '') {
            $query .= " WHERE ";
        }
        else{

        }
        if ($firstName != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " p.first_name LIKE '%" . $firstName . "%'";
            $isFirst = "true";
        }
        if ($lastName != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " p.last_name LIKE '%" . $lastName . "%'";
            $isFirst = "true";
        }
        if ($dob != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " p.dob ='" . $dob . "'";
            $isFirst = "true";
        }
        if ($mobile != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " p.mobile LIKE '%" . $mobile . "%'";
            $isFirst = "true";
        }
        if ($email != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " p.email LIKE '%" . $email . "%'";
            $isFirst = "true";
        }
    }    
    
    else{
        $query = "SELECT i.id AS ipd_id, p.id,CONCAT(p.first_name,' ',p.middle_name,' ',p.last_name) AS name, p.dob, p.email, p.mobile,
            (select value from configuration where name = 'patient_prefix') as prefix FROM ipd_registration AS i
            LEFT JOIN patients AS p ON p.id = i.patient_id WHERE DATE_FORMAT(FROM_UNIXTIME(p.created_on), '%y-%m-%d') = CURDATE()";
    }
        
    $result = mysqli_query($conn, $query);
    //$totalrecords = mysqli_num_rows($result);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
    print json_encode($rows);
}

// search patient opearion
if ($operation == "searchPatientAccount") {
    
    if (isset($_POST['firstName'])) {
        $firstName = $_POST["firstName"];
    }
    if (isset($_POST['lastName'])) {
        $lastName = $_POST["lastName"];
    }
    if (isset($_POST['dob'])) {
        $dob = $_POST["dob"];
    }
    if (isset($_POST['mobile'])) {
        $mobile = $_POST["mobile"];
    }
    if (isset($_POST['email'])) {
        $email = $_POST["email"];
    }


    /*if click data is set then make functional search query*/
    if (isset($_POST['clickData'])) {
        $clickData = $_POST["clickData"];		
		
        $isFirst = "false";       
        
        if ($firstName != '' || $lastName != '' || $dob != '' || $mobile != '' || $email != '') {
			$query = "SELECT DISTINCT(p.id),c.patient_id AS fan_id,CONCAT(p.first_name,' ',p.middle_name,' ',p.last_name) AS name, p.dob, p.email, p.mobile,p.country_code,
               (select value from configuration where name = 'patient_prefix') as prefix,
                (select value from configuration where name = 'account_prefix') as account_prefix FROM patients AS p
                LEFT JOIN cash_account AS c ON c.patient_id = p.id WHERE ";            
        }
        else{
			echo "0";
        }
        if ($firstName != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " p.first_name LIKE '%" . $firstName . "%'";
            $isFirst = "true";
        }
        if ($lastName != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " p.last_name LIKE '%" . $lastName . "%'";
            $isFirst = "true";
        }
        if ($dob != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " p.dob ='" . $dob . "'";
            $isFirst = "true";
        }
        if ($mobile != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " p.mobile LIKE '%" . $mobile . "%'";
            $isFirst = "true";
        }
        if ($email != '') {
            if ($isFirst != "false") {
                $query .= " AND ";
            }
            $query .= " p.email LIKE '%" . $email . "%'";
            $isFirst = "true";
        }
    } 
        
    $result = mysqli_query($conn, $query);
    //$totalrecords = mysqli_num_rows($result);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
    print json_encode($rows);
}

// show patient deatils for search patientshow
if ($operation == "patientShow") {
    if (isset($_POST['id'])) {
        $id = $_POST["id"];
    }
    $sql    = "SELECT patients.*,CASE WHEN country_code = '' OR country_code = NULL THEN '0' ELSE country_code END AS country_mobile_code,(SELECT name FROM locations l WHERE l.location_id = patients.country_id) AS country,
            (SELECT name FROM locations l WHERE l.location_id = patients.state_id) AS state,
            (SELECT name FROM locations l WHERE l.location_id = patients.city_id) AS city,
            (SELECT value FROM configuration WHERE name = 'patient_prefix'  ) AS patient_prefix         
            FROM patients WHERE id= '" . $id . "' AND status= 'A'";
    $result = mysqli_query($conn, $sql);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

// update patient details.
if ($operation == "updateDetails") {
    $id           = $_POST['id'];
    $salutation   = $_POST['salutation'];
    $lname        = $_POST['Lname'];
    $fname        = $_POST['Fname'];
    $middleName   = $_POST['middleName'];
    $dob          = $_POST['Dob'];
    $gender       = $_POST['Gender'];
    $address      = $_POST['Address'];
    $country      = $_POST['Country'];
    $state        = $_POST['State'];
    $city         = $_POST['City'];
    $zipcode      = $_POST['Zipcode'];
    $email        = $_POST['Email'];
    $mobile       = $_POST['Mobile'];
    $mobileCode       = $_POST['mobileCode'];
    $phone        = $_POST['Phone'];
    $careOff      = $_POST['CareOff'];
    $careOffPhone = $_POST['CareOffPhone'];
    $createdate   = new DateTime();
    $imagename    = "";
    $targetPath   = "";
    
    if ($_POST['imageName'] != "") {
        if ($_POST['imagesPath'] != "") {
            
            $filename = $_POST['imagesPath'];
            $file     = basename($filename);
            $path     = "../../upload_images/patient_dp";
            unlink($path . "/" . $file);
        }
        $imagename  = $_POST['imageName'];
        $targetPath = str_replace(" ", "", $imagename); // remove space
        $sql        = "update patients set salutation='" . $salutation . "',first_name='" . $fname . "',last_name='" . $lname . "',
                middle_name='" . $middleName . "',dob='" . $dob . "',gender='" . $gender . "',address='" . $address . "',country_id='" . $country . "',
                state_id='" . $state . "',city_id='" . $city . "',zip='" . $zipcode . "',
                email='" . $email . "',mobile='" . $mobile . "',country_code='" . $mobileCode . "',phone='" . $phone . "',care_of='" . $careOff . "',care_contact='" . $careOffPhone . "',
                updated_on='" . $date->getTimestamp() . "',updated_by='" . $userId . "',images_path='" . $targetPath . "' where id = '" . $id . "'";
    } else {
        $sql = "update patients set salutation='" . $salutation . "',first_name='" . $fname . "',last_name='" . $lname . "',
                middle_name='" . $middleName . "',dob='" . $dob . "',gender='" . $gender . "',address='" . $address . "',country_id='" . $country . "',
                state_id='" . $state . "',city_id='" . $city . "',zip='" . $zipcode . "',
                email='" . $email . "',mobile='" . $mobile . "',country_code='" . $mobileCode . "',phone='" . $phone . "',care_of='" . $careOff . "',care_contact='" . $careOffPhone . "',
                updated_on='" . $date->getTimestamp() . "',updated_by='" . $userId . "' where id = '" . $id . "'";
    }
    
    /* mail($email, "New Account Created On HRP", "USERNAME: ".$email." PASSWORD:".$decryptedPassword."", $headers); */
    
    $result = mysqli_query($conn, $sql);
    echo $result;
}
?>