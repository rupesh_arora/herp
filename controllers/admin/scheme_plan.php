<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "ShowInsuranceCompany") { // show insurance company type
	    $query = "SELECT id,name FROM insurance_companies where status = 'A' ";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "ShowSchemeName") { // show Scheme namw type
		$insuranceCompany = $_POST['insuranceCompany'];
	 	// show Scheme Name
	 	$query = "SELECT insurance_plan.id,insurance_plan.plan_name FROM insurance_plan  LEFT JOIN insurance_companies ON insurance_companies.id = insurance_plan.company_name_id WHERE insurance_companies.id='".$insuranceCompany."'";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "saveSchemePlan") {

		$insuranceCompany = $_POST['insuranceCompany'];
		$schemeName = $_POST['schemeName'];
		$planName = $_POST['planName'];
		$description = $_POST['description'];
		$StartDate = $_POST['StartDate'];
		$EndDate = $_POST['EndDate'];
		$copay = $_POST['copay'];
		$value = $_POST['value'];

		// show Scheme Name
		$queryCheck = "SELECT id FROM scheme_plans WHERE scheme_plan_name = '".$planName."' AND insurance_company_id ='".$insuranceCompany."' AND insurance_plan_id =  '".$schemeName."'
			AND status = 'A'";
	    
	    $resultCheck = mysqli_query($conn, $queryCheck);
	    $countRows = mysqli_num_rows($resultCheck);

	    if($countRows == "0"){
	    	
			$query = "INSERT INTO scheme_plans (scheme_plan_name,insurance_company_id,insurance_plan_id,description,start_date,end_date,copay,value,created_on,updated_on,created_by,updated_by,status) VALUES('".$planName."','".$insuranceCompany."','".$schemeName."','".$description."','".$StartDate."','".$EndDate."','".$copay."','".$value."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."','A')";
		    
		    $result = mysqli_query($conn, $query);
		    $last_id = mysqli_insert_id($conn);

		    if ($result == "1") {

		    	$insertServiceRevisedCost = "INSERT INTO `insurance_revised_cost`(`insurance_company_id`, `scheme_plan_id`, `scheme_name_id`,`item_name`, `service_type`, `item_id`, 
		    		`actual_cost`,`revised_cost`, `created_on`, `created_by`, `status`) SELECT  
		    		'".$insuranceCompany."' AS insuance_company_id ,'".$schemeName."' AS scheme_plan_id , 
		    		'".$last_id."' AS scheme_name_id,name AS item_name ,'service' AS service_type , id As
		    		item_id, services.cost AS actual_cots, cost AS revised_cost,UNIX_TIMESTAMP() AS 
		    		created_on ,'".$userId."' AS created_by, 'A' AS `status`   FROM services";

				$insertForensicRevisedCost = "INSERT INTO `insurance_revised_cost`(`insurance_company_id`, `scheme_plan_id`, `scheme_name_id`,`item_name`, `service_type`, `item_id`, 
		    		`actual_cost`,`revised_cost`, `created_on`, `created_by`, `status`)
		    		 SELECT  '".$insuranceCompany."' AS 
				insuance_company_id ,'".$schemeName."' AS scheme_plan_id , '".$last_id."' AS scheme_name_id
				,name AS item_name ,'forensic' AS service_type , id As item_id, fee AS
				actual_cots, fee AS revised_cost,UNIX_TIMESTAMP() AS created_on ,'".$userId."' 
				AS created_by, 'A' AS `status`   FROM forensic_tests";

				$insertRadiologyRevisedCost = "INSERT INTO `insurance_revised_cost`(`insurance_company_id`, `scheme_plan_id`, `scheme_name_id`,`item_name`, `service_type`, `item_id`, 
		    		`actual_cost`,`revised_cost`, `created_on`, `created_by`, `status`)
		    		SELECT  '".$insuranceCompany."' AS 
				insuance_company_id ,'".$schemeName."' AS scheme_plan_id , '".$last_id."' AS scheme_name_id
				,name AS item_name ,'radiology' AS service_type , id As item_id, fee AS
				actual_cots, fee AS revised_cost,UNIX_TIMESTAMP() AS created_on ,'".$userId."' 
				AS created_by, 'A' AS `status`   FROM radiology_tests";

				$insertProcedureRevisedCost = "INSERT INTO `insurance_revised_cost`(`insurance_company_id`, `scheme_plan_id`, `scheme_name_id`,`item_name`, `service_type`, `item_id`, 
		    		`actual_cost`,`revised_cost`, `created_on`, `created_by`, `status`)
		    		SELECT  '".$insuranceCompany."' AS 
				insuance_company_id ,'".$schemeName."' AS scheme_plan_id , '".$last_id."' AS scheme_name_id
				,name AS item_name ,'procedure' AS service_type , id As item_id, price AS
				actual_cots, price AS revised_cost,UNIX_TIMESTAMP() AS created_on ,'".$userId."' 
				AS created_by, 'A' AS `status`   FROM procedures";

				$insertPharmacyRevisedCost = "INSERT INTO `insurance_revised_cost`(`insurance_company_id`, `scheme_plan_id`, `scheme_name_id`,`item_name`, `service_type`, `item_id`, 
		    		`actual_cost`,`revised_cost`, `created_on`, `created_by`, `status`) 
		    		SELECT  '".$insuranceCompany."' AS 
				insuance_company_id ,'".$schemeName."' AS scheme_plan_id , '".$last_id."' AS scheme_name_id
				,name AS item_name ,'pharmacy' AS service_type , id As item_id, cost AS
				actual_cots, cost AS revised_cost,UNIX_TIMESTAMP() AS created_on ,'".$userId."' 
				AS created_by, 'A' AS `status`   FROM drugs";

				$insertLabRevisedCost = "INSERT INTO `insurance_revised_cost`(`insurance_company_id`, `scheme_plan_id`, `scheme_name_id`,`item_name`, `service_type`, `item_id`, 
		    		`actual_cost`,`revised_cost`, `created_on`, `created_by`, `status`)
		    		SELECT  '".$insuranceCompany."' AS 
				insuance_company_id ,'".$schemeName."' AS scheme_plan_id , '".$last_id."' AS scheme_name_id
				,name AS item_name ,'lab' AS service_type , id As item_id, fee AS
				actual_cots, fee AS revised_cost,UNIX_TIMESTAMP() AS created_on ,'".$userId."' 
				AS created_by, 'A' AS `status`   FROM lab_test_component";

				$combinedRevisedCost = $insertServiceRevisedCost.';'.$insertForensicRevisedCost.';'.$insertRadiologyRevisedCost.';'.$insertProcedureRevisedCost.';'.$insertPharmacyRevisedCost.';'.$insertLabRevisedCost;

				$resultCombinedCost = mysqli_multi_query($conn, $combinedRevisedCost);
		    }
	    	echo $resultCombinedCost;
	    } 
	    else{
	    	echo '0';
	    }
	}


	if ($operation == "show") { // show active data
    
    	$query = "SELECT scheme_plans.id,scheme_plans.scheme_plan_name AS plan_name,
			insurance_companies.name AS company_name,insurance_plan.plan_name AS scheme_name,
			scheme_plans.description ,scheme_plans.insurance_company_id,
			scheme_plans.insurance_plan_id,scheme_plans.start_date,scheme_plans.end_date,copay.value as copay_value,
			scheme_plans.copay,scheme_plans.value FROM scheme_plans LEFT JOIN 
			insurance_companies ON insurance_companies.id = scheme_plans.insurance_company_id LEFT JOIN 
			insurance_plan ON insurance_plan.id = scheme_plans.insurance_plan_id 
			LEFT JOIN copay ON copay.id = scheme_plans.copay
			WHERE scheme_plans.`status`= 'A' AND scheme_plans.end_date >= CURDATE()";
	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}

	if ($operation == "checked") {
	    
	    $query = "SELECT scheme_plans.id,scheme_plans.scheme_plan_name AS plan_name,
				insurance_companies.name AS company_name,
				insurance_plan.plan_name AS scheme_name,scheme_plans.description,scheme_plans.start_date,
				scheme_plans.end_date,scheme_plans.copay,scheme_plans.value,insurance_companies.id AS company_id,
				insurance_plan.id AS insurance_plan_id
				FROM scheme_plans 
				LEFT JOIN insurance_companies ON insurance_companies.id = scheme_plans.insurance_company_id 
				LEFT JOIN insurance_plan ON insurance_plan.id = scheme_plans.insurance_plan_id 
				WHERE scheme_plans.`status`= 'I' AND scheme_plans.end_date >= CURDATE()";
	    
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}
	
	if ($operation == "update") // update data
	{
	    $description = "";
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    if (isset($_POST['insuranceCompany'])) {
	        $insuranceCompany = $_POST['insuranceCompany'];
	    }
	    if (isset($_POST['schemeName'])) {
	        $schemeName = $_POST['schemeName'];
	    }
		if (isset($_POST['planName'])) {
	        $planName = $_POST['planName'];
	    }
	    if (isset($_POST['description'])) {
	        $description = $_POST['description'];
	    }
	    if (isset($_POST['StartDate'])) {
	        $StartDate = $_POST['StartDate'];
	    }
	    if (isset($_POST['EndDate'])) {
	        $EndDate = $_POST['EndDate'];
	    }
	    if (isset($_POST['copay'])) {
	        $copay = $_POST['copay'];
	    }
	    if (isset($_POST['value'])) {
	        $value = $_POST['value'];
	    }
	    $id = $_POST['id'];
		
		$queryCheck = "SELECT id FROM scheme_plans WHERE scheme_plan_name = '".$planName."' AND insurance_company_id ='".$insuranceCompany."' AND insurance_plan_id =  '".$schemeName."' AND status = 'A' AND id != '".$id."' ";
	    
	    $resultCheck = mysqli_query($conn, $queryCheck);
	    $countRows = mysqli_num_rows($resultCheck);
	    if($countRows== "0"){
	    
			$sql    = "UPDATE scheme_plans set scheme_plan_name = '".$planName."',
			insurance_company_id= '".$insuranceCompany."' ,insurance_plan_id= '".$schemeName."' ,description = '".$description."',start_date='".$StartDate."',end_date='".$EndDate."',copay='".$copay."',value='".$value."',updated_on=UNIX_TIMESTAMP(),updated_by='".$userId."' where id = '".$id."' ";

			$result = mysqli_query($conn, $sql);
			echo $result;
		}
	}

	if ($operation == "restore") // for restore    
	    {
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    if (isset($_POST['insuranceCompanyId'])) {
	        $insuranceCompanyId = $_POST['insuranceCompanyId'];
	    }
	    if (isset($_POST['schemeName'])) {
	        $schemeName = $_POST['schemeName'];
	    }
	    if (isset($_POST['planName'])) {
	        $planName = $_POST['planName'];
	    }

	   $checkScheme = "SELECT insurance_company_id,insurance_plan_id,scheme_plan_name FROM scheme_plans WHERE   insurance_company_id = '".$insuranceCompanyId."' AND insurance_plan_id = '".$schemeName."' AND 
	    	scheme_plan_name = '".$planName."' AND `status`= 'A' ";
	    	

		$resultCheck = mysqli_query($conn,$checkScheme);
		$countRows = mysqli_num_rows($resultCheck);

		if ($countRows < 1) {
		    $sql    = "UPDATE scheme_plans SET status= 'A'  WHERE  id = '" . $id . "'";

		    $result = mysqli_query($conn, $sql);
		    echo $result;
		}
		else{
			echo "0";
		}
	}

	if ($operation == "delete") {
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    $selectQry = "SELECT insurance_plan_name_id FROM insurance WHERE insurance_plan_name_id ='".$id."'";
	    $selectResult = mysqli_query($conn,$selectQry);
	    $countRows = mysqli_num_rows($selectResult);
		if ($countRows == 0) {

			$sql    = "UPDATE scheme_plans SET status= 'I' where id = '" . $id . "'";
		    $result = mysqli_query($conn, $sql);
		    echo $result;
		}
		else{
			echo "0";
		}		    
	}

	if ($operation == "showCopay") { // show copay name
		 // show Scheme Name
		 $query = "SELECT id,copay_type,value FROM copay where status = 'A'";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

?>