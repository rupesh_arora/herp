<?php
/*****************************************************************************************************
 * File Name    :   seller.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   22th feb, 2016
 * Description  :   This page  manages seller data
 ****************************************************************************************************/
 
 	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}

    $operation = "";
	$sellerName ="";
	$description = "";
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}

	if ($operation == "showVendorType") {			
		
		$sql = "SELECT id,type FROM vendor_type WHERE status = 'A'";

		$result = mysqli_query($conn,$sql);
		$rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
		echo json_encode($rows);
	}
	
	// import excel file
	if (isset($_FILES["file"]["type"])) // use for import data from csv
	 {
		$temporary       = explode(".", $_FILES["file"]["name"]);
		$file_extension = end($temporary);
		/*if ($file_extension == "csv") {
			if ($_FILES["file"]["error"] > 0) {
				echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
			} 
			else {
				if($_FILES["file"]["size"] > 0){
				
					$filename = $_FILES["file"]["tmp_name"];
					$file = fopen($filename, "r");

					$countCSV = 0;
					$subInsertValues = '';
					$sqlInsert = "INSERT IGNORE INTO seller(name,firm_name,email,description,created_on,updated_on) VALUES ";

					while (($emapData = fgetcsv($file, 10000, ",")) != FALSE) {
						
						if (sizeof($emapData) != 4) {
						 	echo "You must have four column in your file i.e seller name, firm name, seller email and description";
						 	exit();
						}

						//check whether header name same or not
						if ($countCSV == 0) {
							if(strtolower($emapData[0]) !="seller name") {
								echo "First column should be seller name";
								exit();
							}
							if(strtolower($emapData[1]) !="firm name") {
								echo "Second column should be firm name";
								exit();
							}
							if(strtolower($emapData[2]) !="seller email") {
								echo "Third column should be seller email";
								exit();
							}	
							if(strtolower($emapData[3]) !="description") {
								echo "Fourth column should be description";
								exit();
							}						
						}

						//data can't be null in first row else row it may be null
						if ($countCSV == 1) {
							if (empty($emapData[0]) || empty($emapData[1]) || empty($emapData[2]) ) {
								echo "Data can't be null in seller name ,firm name and seller email";
								exit();
							}
						}

						if ($countCSV > 0) {

							//check whether data not be null in the file
							if(($emapData[0]) != "" && ($emapData[1]) != "" && ($emapData[2]) != "") {

								if (!filter_var($emapData[2], FILTER_VALIDATE_EMAIL) === false) {
									if ($countCSV >1) {
										$subInsertValues.= ',';
									}						
									$subInsertValues.= "('".$emapData[0]."','".$emapData[1]."',
										'".$emapData[2]."','".$emapData[3]."',UNIX_TIMESTAMP(),
										UNIX_TIMESTAMP())";
								}
								else{
									echo "Email Id is not valid";
									exit();
								}								
							}
						}			
						$countCSV++;						
					}

					$sqlInsert = $sqlInsert.$subInsertValues;
					$result = mysqli_query($conn,$sqlInsert);
					if($result)
					{
						echo $result;
					}
					else{
						echo "";
					}
					fclose($file);
				}
			} 	
		}
		else if($file_extension == "xls"){
			ini_set("display_errors",1);
			$filename = $_FILES['file']['tmp_name'];
			require_once 'excel_reader2.php';
			$data = new Spreadsheet_Excel_Reader($filename);
			$countSheets = count($data->sheets);
			for($i=0;$i<$countSheets;$i++) // Loop to get all sheets in a file.
			{ 
				$countSheetsCells =0;
				if(isset($data->sheets[$i]['cells'])){
					$countSheetsCells = count($data->sheets[$i]['cells']);
				}
				if($countSheetsCells > 0 && $countSheetsCells) // checking sheet not empty
				{
					$sql = "INSERT IGNORE INTO seller(name,firm_name,email,description,created_on,updated_on) VALUES ";
					$countXls = 0;				
					$subInsert = '';
					for($j=1;$j<=$countSheetsCells;$j++) {// loop used to get each row of the sheet
					 
						
					  	if (sizeof($data->sheets[$i]['cells'][$j]) !="4") {
					  		echo "You must have four column in your file i.e seller name, firm name, seller email and description";
						 	exit();
					  	}
					  	
					  		
					  	$indexOfExcelFile = array_keys($data->sheets[$i]['cells'][$j]);

						
						if ($countXls == 0) {
							if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[0]]) !="seller name"){
								echo "First column should be seller name";
								exit();
							}
							if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[1]]) !="firm name"){
								echo "Second column should be firm name";
								exit();
							}
							if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[2]]) !="seller email"){
								echo "Third column should be seller email";
								exit();
							}
							if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[3]]) !="description"){
								echo "Fourth column should be description";
								exit();
							}
						}
						
						$s_name = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[0]];
					
						$f_name = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[1]];

						$email = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[2]];
					
						$description = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[3]];
						
						//data can't be null in the very first row of file
						if ($countXls == 1) {
							if (empty($s_name) || empty($f_name) || empty($email)) {
								echo "Data can't be null in seller name ,firm name and seller email";
								exit();
							}
						}

						if ($countXls>0) {
							
							//check whether data not be null in file
							if($s_name != "" && $f_name != '' && $email != ''){

								if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
									if ($countXls >1) {
										$subInsert .=',';
									}					 	
								 	$subInsert .=" ('$s_name','$f_name','$email','$description',
								 	UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";
								}
								else{
									echo "Email Id is not valid";
									exit();
								}
							}
						}
						$countXls++;		
					}

					$sql = $sql.$subInsert;
					$result = mysqli_query($conn,$sql);

					if($result) {					
						echo $result;
					}
					else{
						echo "";
					}
				}
			}		
		}*/
		if($file_extension == "xlsx" || $file_extension == "csv" ||$file_extension == "xls"){
			// get file name
			$filename = $_FILES['file']['tmp_name'];
			
			//import php script
			require_once 'PHPExcel.php';
			
			// 
			try {
				$inputFileType = PHPExcel_IOFactory::identify($filename);  // get type of file name
				$objReader = PHPExcel_IOFactory::createReader($inputFileType); //create object of read file
				$objPHPExcel = $objReader->load($filename);  //load excel into the object
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($filename, PATHINFO_BASENAME) 
				. '": ' . $e->getMessage());
			}

			//  Get worksheet dimensions
			$sheet = $objPHPExcel->getSheet(0); // get sheet 1 of excel file
			$highestRow = $sheet->getHighestRow(); // get heighest row
			$highestColumn = $sheet->getHighestColumn(); // get heighest column
			
			$headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE); // get heading in first row
			
			if (sizeof($headings[0]) < 4) {
				echo "You must have four column in your file i.e seller name, firm name, seller email and description";
				exit();
			}
			
			//reset($headings[0]) it is the first column
			if(trim(strtolower($headings[0][0])) !="supplier name"){
				echo "First column should be supplier name";
				exit();
			}
			//end($headings[0]) it is the last column
			if(trim(strtolower($headings[0][1])) !="firm name"){
				echo "Second column should be firm name";
				exit();
			}
			if(trim(strtolower($headings[0][2])) !="supplier email"){
				echo "Third column should be supplier email";
				exit();
			}
			if(trim(strtolower($headings[0][3])) !="description"){
				echo "Fourth column should be description";
				exit();
			}
			$sql = "INSERT IGNORE INTO seller(name,firm_name,email,description,created_on,updated_on,created_by,updated_by) VALUES ";
			$countXlsx = 0;
			$subInsertXlsx = '';

			for ($row = 2; $row <= $highestRow; $row++) {
				//  Read a row of data into an array
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL, TRUE, FALSE);
				
				// combine data in array accoding to heading
				$rowData[0] = array_combine($headings[0], $rowData[0]);
				//var_dump($rowData[0]);
				
				// fetch value from row
				$key  = array_keys($rowData[0]);//getting the ondex name

				$s_name =  trim(mysql_real_escape_string($rowData[0][$key[0]]));
				$f_name = trim(mysql_real_escape_string($rowData[0][$key[1]]));
				$email = trim(mysql_real_escape_string($rowData[0][$key[2]]));
				$description = trim(mysql_real_escape_string($rowData[0][$key[3]]));

				// data shouldn't be null in the very first row of file
				if ($countXlsx == 0) {

					if (empty($s_name) || empty($f_name)|| empty($email)) {
						echo "Data can't be null in seller name ,firm name and seller email";
						exit();
					}
				}
				if ($s_name != "" && $f_name != "" && $email != "") {

					//check email Id 
					if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {

						if ($countXlsx > 0) {
							$subInsertXlsx.=',';
						}
						$subInsertXlsx.="('$s_name','$f_name','$email','$description',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),' $userId',' $userId')";
					}
					else{
						echo "Email Id is not valid";
						exit();
					}						
				}				
					
				$countXlsx++;		
			}

			$sql = $sql.$subInsertXlsx;
			$result = mysqli_query($conn,$sql);

			if($result)
			{
				echo $result;
			}
			else{
				echo "";
			}
		}
		else{
			echo "invalid file";
		}
	}

	//This operation is used for save the data 
	if($operation == "save")			
	{
		if (isset($_POST['sellerName'])) {
			$sellerName=$_POST['sellerName'];
		}
		if (isset($_POST['description'])) {
			$description=$_POST['description'];
		}
		if (isset($_POST['firmName'])) {
			$firmName=$_POST['firmName'];
		}
		if (isset($_POST['sellerEmail'])) {
			$sellerEmail=$_POST['sellerEmail'];
		}

		if (isset($_POST['phone'])) {
			$phone=$_POST['phone'];
		}

		if (isset($_POST['address'])) {
			$address=$_POST['address'];
		}

		if (isset($_POST['vendorType'])) {
			$vendorType=$_POST['vendorType'];
		}
		if (isset($_POST['code'])) {
			$code=$_POST['code'];
		}
		if (isset($_POST['bankName'])) {
			$bankName=$_POST['bankName'];
		}
		if (isset($_POST['accountNo'])) {
			$accountNo=$_POST['accountNo'];
		}
		
		$sql = "INSERT INTO seller(name,description,firm_name,email,phone,vendor_type_id,ifsc_code,bank_name,account_no,address,created_on,updated_on,created_by,updated_by) 
		VALUES('".$sellerName."','".$description."','".$firmName."','".$sellerEmail."','".$phone."','".$vendorType."','".$code."','".$bankName."','".$accountNo."','".$address."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
		
		$result= mysqli_query($conn,$sql);  
		echo $result;
	} 

	//This function is used for update the data 
	if($operation == "update")			
	{
		if (isset($_POST['sellerName'])) {
			$sellerName=$_POST['sellerName'];
		}
		if (isset($_POST['description'])) {
			$description=$_POST['description'];
		}
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		if (isset($_POST['firmName'])) {
			$firmName=$_POST['firmName'];
		}
		if (isset($_POST['sellerEmail'])) {
			$sellerEmail=$_POST['sellerEmail'];
		}	

		if (isset($_POST['phone'])) {
			$phone=$_POST['phone'];
		}

		if (isset($_POST['address'])) {
			$address=$_POST['address'];
		}

		if (isset($_POST['vendorType'])) {
			$vendorType=$_POST['vendorType'];
		}
		if (isset($_POST['code'])) {
			$code=$_POST['code'];
		}
		if (isset($_POST['bankName'])) {
			$bankName=$_POST['bankName'];
		}
		if (isset($_POST['accountNo'])) {
			$accountNo=$_POST['accountNo'];
		}	
		
		$sql = "UPDATE seller SET name= '".$sellerName."',description= '".$description."',phone='".$phone."',ifsc_code='".$code."',bank_name ='".$bankName."',account_no = '".$accountNo."',vendor_type_id ='".$vendorType."',address = '".$address."',firm_name ='".$firmName."' ,email ='".$sellerEmail."',updated_on=UNIX_TIMESTAMP(),updated_by='".$userId."' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//This operation is used for update the status 
	if($operation == "delete")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}		
	
		$sql = "UPDATE seller SET status= 'I' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//This operation is used for restore the data 
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		
		$sql = "UPDATE seller SET status= 'A' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//This operation is used for show the active data 
	if($operation == "show"){
		
		$query= "select * from seller WHERE status='A'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
	//This operation is used for show the inactive data 
	if($operation == "showInActive"){
		
		$query= "select * from seller WHERE status='I'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
?>