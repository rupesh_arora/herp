<?php
/*File Name  :   configure_item_price.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   17th Feb, 2016
Description  :   This page manages  all the configure item data*/

session_start();

/*include config file*/
include 'config.php';

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
}
else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}
/*Get user id from session table*/
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
	exit();
}

if ($operation == "showData") {

	$query ="SELECT item_price.price , items.name,items.description from item_price
		LEFT JOIN items ON items.id = item_price.item_id";

	$result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "save") {
	
	if (isset($_POST["price"])) {
		$price = $_POST["price"];
	}
	if (isset($_POST["itemName"])) {
		$itemName = $_POST["itemName"];
	}

	$selectQry = "SELECT id from items	WHERE name = '".$itemName."'";

	$resultSelect =  mysqli_query($conn, $selectQry);
	while ($row = mysqli_fetch_row($resultSelect)) {
		$itemId = $row[0];
	} 
	$rows_count = mysqli_num_rows($resultSelect);

	if ($rows_count>0) {
		
		$query = "INSERT INTO item_price(item_id,price,updated_by) VALUES ('".$itemId."', 
		'".$price."', '".$userId."')";

		$result = mysqli_query($conn,$query);
		echo $result;
	}
	else{
		echo "0";
	}
}
if ($operation =="serachItemDetails") {
	if (isset($_POST["itemName"])) {
		$itemName = $_POST["itemName"];
	}

	$selectQry = "SELECT id from items	WHERE name = '".$itemName."'";

	$resultSelect =  mysqli_query($conn, $selectQry);
	while ($row = mysqli_fetch_row($resultSelect)) {
		$itemId = $row[0];
	} 
	$rows_count = mysqli_num_rows($resultSelect);

	if ($rows_count>0) {
		$selectQry = "SELECT items.id,items.name,items.description,received_orders.unit_cost from 	items
			LEFT JOIN received_orders ON received_orders.item_id = items.id
			WHERE items.id = '".$itemId."'	GROUP BY items.id ";

		$result = mysqli_query($conn, $selectQry);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	else{
		echo "0";
	}
}
?>