<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	$date      = new DateTime();
	$operation="";
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	if(isset($_FILES['filename']['name'])) {
		$images_arr = array();
		for ($i = 0; $i < count($_FILES['filename']['name']); $i++) {
			//upload and stored images
			$target_dir = "../../upload_images/external_result/";
			
			$imageString = explode(".",$_FILES['filename']['name'][$i]);
			$imagename = $imageString[0]. $date->getTimestamp();
			$extension = $imageString[1];
			
			$image_name = $imagename . "." . $extension;
			$name       = str_replace(" ", "", $image_name); // remove space
			$target_file = $target_dir.$name;
			
			if(move_uploaded_file($_FILES['filename']['tmp_name'][$i],$target_file)){
				$images_arr[]= $name;
			}
		}
		echo json_encode($images_arr);
	}

	if ($operation == "showResult") {
		$visitId = $_POST['visitId'];
		$patientId = $_POST['patientId'];

		$radiologyTest = "SELECT 'radiology' AS tbl_name ,radiology_tests.name,radiology_test_request.id from radiology_tests 
			LEFT JOIN radiology_test_request ON radiology_test_request.radiology_test_id = radiology_tests.id
			WHERE radiology_test_request.visit_id = '".$visitId."' AND radiology_test_request.patient_id 
			= '".$patientId."' AND radiology_test_request.request_type = 'external' UNION ALL ";

		$forensicTest = "SELECT 'forensic' AS tbl_name ,forensic_tests.name,forensic_test_request.id from forensic_tests 
			LEFT JOIN forensic_test_request ON forensic_test_request.forensic_test_id = forensic_tests.id
			WHERE forensic_test_request.visit_id = '".$visitId."' AND forensic_test_request.patient_id = '".$patientId."' AND forensic_test_request.request_type = 'external' UNION ALL ";

		$serviceTest ="SELECT 'service' AS tbl_name ,services.name,services_request.id from services 
			LEFT JOIN services_request ON services_request.service_id = services.id
			WHERE services_request.visit_id = '".$visitId."' AND services_request.patient_id = '".$patientId."' AND services_request.request_type = 'external' UNION ALL " ;

		$procedureTest = "SELECT 'procedure' AS tbl_name ,procedures.name ,procedures_requests.id from procedures 
			LEFT JOIN procedures_requests ON procedures_requests.procedure_id = procedures.id
			WHERE procedures_requests.visit_id = '".$visitId."' AND procedures_requests.patient_id = '".$patientId."' AND procedures_requests.request_type = 'external' UNION ALL ";
			
		$labTest = "SELECT 'lab' AS tbl_name ,lab_test_component.name ,lab_test_request_component.id
        FROM lab_test_requests AS ltr
        LEFT JOIN lab_tests AS lt ON lt.id=ltr.lab_test_id
        LEFT JOIN lab_test_request_component ON lab_test_request_component.lab_test_request_id = ltr.id
        LEFT JOIN lab_test_component ON lab_test_component.id = lab_test_request_component.lab_component_id
        WHERE ltr.status = 'A' AND ltr.visit_id = '".$visitId."' AND ltr.patient_id = '".$patientId."' AND ltr.request_type = 'external'";

        $compeleteQry = $radiologyTest.$forensicTest.$serviceTest.$procedureTest.$labTest;
		$result = mysqli_query($conn,$compeleteQry);
		$rows = array();

		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		echo json_encode($rows);
	}
?>