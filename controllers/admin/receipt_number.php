<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "showLedger") {		

		$sql = "SELECT id,name FROM ledger WHERE status = 'A'";

		$result = mysqli_query($conn,$sql);
		$rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
		echo json_encode($rows);
	}
	
	if ($operation == "showCustomerDetails") {
			
		$customerId = $_POST['customerId'];

		$sql = "SELECT c.first_name,c.last_name,c.email,c.phone,ci.billing_date,ci.total_bill,cpd.id AS customer_product_id,p.name,ci.id,cpd.total,
		(SELECT value FROM configuration WHERE name = 'invoice_prefix') AS invoice_prefix,CONCAT('INVOICE', CASE WHEN ci.recurrence_period = 'does not recur' THEN ' (Normal)' ELSE ' (Recurrence)' END) AS description ,
		receipt_book_credit.balance
		 FROM customer AS c
		LEFT JOIN customer_invoice AS ci ON ci.customer_id = c.id 
		LEFT JOIN customer_product_details AS cpd ON cpd.customer_invoice_id = ci.id 
		LEFT JOIN products AS p ON p.id = cpd.product_id
		LEFT JOIN receipt_book_credit ON receipt_book_credit.customer_id = c.id
		WHERE c.id= '".$customerId."' AND cpd.pay_status='unpaid'  ORDER BY ci.id ";

		$result = mysqli_query($conn,$sql);
		$rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
		echo json_encode($rows);
	}

	if ($operation == "save") {	
			
		$customerId = $_POST['customerId'];

		$paymentMode = $_POST['paymentMode'];

		$reference = $_POST['reference'];

		$userCredit = $_POST['userCredit'];

		$last_id = '';


		$serachUserCredit = "SELECT id FROM receipt_book_credit 
			WHERE customer_id = '".$customerId."'";
		$resultSerachUserCredit = mysqli_query($conn,$serachUserCredit);
		$countUser = mysqli_num_rows($resultSerachUserCredit);

		if ($userCredit == 0) {			

			if ($countUser > 0) {
				$updateUserCredit = "UPDATE receipt_book_credit SET balance = '".$userCredit."' ,updated_by = '".$userId."',updated_on = UNIX_TIMESTAMP() WHERE customer_id ='".$customerId."'";
				mysqli_query($conn,$updateUserCredit);
				$lastUpadtedId = "SELECT id FROM receipt_book_credit ORDER BY updated_on DESC LIMIT 1";
				$resultLastUpdated = mysqli_query($conn,$lastUpadtedId);
				while ($rows = mysqli_fetch_assoc($resultLastUpdated)) {
		            $last_id = $rows['id'];
		        }
			}
		}

		else {
			if ($countUser > 0) {
				$updateUserCredit = "UPDATE receipt_book_credit SET balance = '".$userCredit."' ,updated_by = '".$userId."',updated_on = UNIX_TIMESTAMP() WHERE customer_id ='".$customerId."'";
				mysqli_query($conn,$updateUserCredit);

				$lastUpadtedId = "SELECT id FROM receipt_book_credit ORDER BY updated_on DESC LIMIT 1";
				$resultLastUpdated = mysqli_query($conn,$lastUpadtedId);
				while ($rows = mysqli_fetch_assoc($resultLastUpdated)) {
		            $last_id = $rows['id'];
		        }
			}
			else{

				$creditQry = "INSERT INTO receipt_book_credit (customer_id,balance,created_on,updated_on,created_by,updated_by) VALUES ('".$customerId."','".$userCredit."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";

				$resultCredit = mysqli_query($conn,$creditQry);
				$last_id = mysqli_insert_id($conn);
			}
		}
		if ($last_id == '') {
			$last_id = !empty($last_id) ? "'$last_id'" : "NULL";
		}
		

		if (isset($_POST['ledgerId'])) {
			$ledgerId = $_POST["ledgerId"];
		}		
		
		if (isset($_POST['receiptDetail'])) {
        	$receiptDetail = json_decode($_POST["receiptDetail"]);
        	
	        foreach ($receiptDetail as $value) {
	            
	            $invoiceId    = $value->invoiceId;
	            $total        = $value->total;
				
	           $sql = "INSERT INTO receipt_book (reference,invoice_id,amount,ledger_id,created_on,updated_on,created_by,updated_by,payment_mode_id,receipt_book_credit_id ) values('".$reference."','".$invoiceId."','".$total."','".$ledgerId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."','".$paymentMode."',".$last_id.")";
	           
				$result = mysqli_query($conn,$sql);
	        }
		}
		if (isset($_POST['productDetail'])) {
        	$productDetail = json_decode($_POST["productDetail"]);
        	
	        foreach ($productDetail as $value) {
	            
	            $productId    = $value->productId;
				
	            $sql = "UPDATE customer_product_details SET pay_status ='paid', updated_on = UNIX_TIMESTAMP(),updated_by ='".$userId."' WHERE id = '".$productId."'";

				$result = mysqli_query($conn,$sql);
	        }
		}

		if($result){
			echo $result;
		}
		else{
			echo '0';
		}
	}

	if ($operation == "productDetails") {

		$sql = "SELECT products.id,products.name,products.cost_price,products.unit,IFNULL(SUM(tax_type.tax_rate),0) AS tax,(SELECT value FROM configuration WHERE name = 'product_prefix') AS product_prefix   
			FROM products
			LEFT JOIN product_taxes ON product_taxes.product_id = products.id
			LEFT JOIN tax_type ON tax_type.id = product_taxes.tax_id
			GROUP BY products.id";

		$result = mysqli_query($conn,$sql);
		$rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
		echo json_encode($rows);
	}
?>