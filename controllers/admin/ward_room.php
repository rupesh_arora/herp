<?php

/*File Name    :   ward_room.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page  manages  all the wards room */

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
$operation   = "";
$wardId      = "";
$ward_id     = "";
$roomType    = "";
$roomNumber  = "";
$description = "";

/*include config file*/
include 'config.php';

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
} //else nothing

/*operation to show ward*/
if ($operation == "showWard") {
    
    $query = "Select id,CONCAT(name, '  ','(',ward_type,')') AS name FROM wards WHERE status='A' ORDER BY name";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    print json_encode($rows);
}



/*Operation to save data*/
if ($operation == "save") {
    if (isset($_POST['wardId'])) {
        $wardId = $_POST['wardId'];
    }
    if (isset($_POST['roomNumber'])) {
        $roomNumber = $_POST['roomNumber'];
    }
    if (isset($_POST['roomType'])) {
        $roomType = $_POST['roomType'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
	$departmentId = $_POST['departmentId'];
    /*check that ward room for that room number and ward id exist or not*/
    if($roomType == "N"){
        $sqlSelect    = "SELECT number FROM ward_room WHERE number='" . $roomNumber . "' And ward_id='" . $wardId . "'";
    }
    else{
        $sqlSelect    = "SELECT number FROM ward_room WHERE number='" . $roomNumber . "' And ward_id= null";
    }
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $rows_count   = mysqli_num_rows($resultSelect);
    
    if ($rows_count <= 0) {
        if($roomType == "N"){
            $sql    = "INSERT INTO ward_room(ward_id,department_id,other,number,description,created_on,updated_on,created_by,updated_by)
                 VALUES(" . $wardId . "," . $departmentId . ",'" . $roomType . "','" . $roomNumber . "','" . $description . "',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
        }
        else{
             $sql    = "INSERT INTO ward_room(department_id,other,number,description,created_on,updated_on,created_by,updated_by)
                 VALUES(" . $departmentId . ",'" . $roomType . "','" . $roomNumber . "','" . $description . "',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
        }
       
        $result = mysqli_query($conn, $sql);
        echo $result;
    } else {
        echo "0";
    }
}

/*Operation to update data*/
if ($operation == "update") {
    if (isset($_POST['wardId'])) {
        $wardId = $_POST['wardId'];
    }
    if (isset($_POST['roomNumber'])) {
        $roomNumber = $_POST['roomNumber'];
    }
    if (isset($_POST['roomType'])) {
        $roomType = $_POST['roomType'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
	$departmentId = $_POST['departmentId'];
    /*check that ward room for that room number ,ward id and id exist or not*/
    $sqlSelect    = "SELECT number FROM ward_room WHERE number='" . $roomNumber . "' And ward_id='" . $wardId . "' and id != '$id'";
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $rows_count   = mysqli_num_rows($resultSelect);
    if ($rows_count == 0) {
        
        $sql    = "UPDATE ward_room SET ward_id= '" . $wardId . "',department_id = '" . $departmentId . "',other= '" . $roomType . "',number= '" . $roomNumber . "',description='" . $description . "',updated_on=UNIX_TIMESTAMP(),updated_by='".$userId."' WHERE  id = '" . $id . "'";
        $result = mysqli_query($conn, $sql);
        echo $result;
    } else {
        echo "0";
    }
}

/*Operation to delete data*/
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE ward_room SET status= 'I'  WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}

/*Operation to restore data*/
if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    if (isset($_POST['ward_id'])) {
        $ward_id = $_POST['ward_id'];
    }
    $sql    = "UPDATE ward_room SET status= 'A' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    /*if query sucessful then check ward status if status active then query be sucessful*/
    if ($result) {
        $query  = "SELECT wards.status as wStatus,ward_room_type.`status` as wrStatus FROM wards,ward_room_type 
				   WHERE wards.id='".$ward_id."' and ward_room_type.`id`='".$room_type."'";
        $result1 = mysqli_query($conn, $query);
        while ($r = mysqli_fetch_assoc($result1)) {
            if ($r['wStatus'] == 'A' && $r['wrStatus'] == 'A') {
                echo "1";
            } else {
                echo "0";
            }
        }
    }
}

/*Operation to show active data*/
if ($operation == "show") {
    
    $query        = "SELECT r.id,d.id As department_id,d.department As departmentName,r.other, r.ward_id, CONCAT(w.name, '  ','(',w.ward_type,')') AS name, r.number, r.description FROM ward_room AS r 
        LEFT JOIN wards AS w ON r.ward_id=w.id 
        LEFT JOIN department AS d ON d.id =r.department_id
        WHERE r.status='A'  AND d.`status` = 'A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

if ($operation == "showInActive") {
    $query        = "SELECT r.id, r.ward_id,d.id As department_id,d.department As departmentName, CONCAT(w.name, '  ','(',w.ward_type,')') AS name, r.number, r.description FROM ward_room AS r 
		LEFT JOIN wards AS w ON r.ward_id=w.id    
		LEFT JOIN department AS d ON d.id =r.department_id
		WHERE r.status='I' Or w.status='I' OR d.`status` = 'I'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}
?>