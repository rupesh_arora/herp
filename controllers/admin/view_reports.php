<?php
/*File Name  :   bed.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page manages AND add beds*/

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

/*include config file*/
include 'config.php';


/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

/*operation to show leave type*/
if ($operation == "showBindLeavePeriod") {
    
    $query  = "SELECT leave_period.*,CONCAT(leave_period.from_date,' - ',leave_period.to_date) As period FROM leave_period WHERE leave_period.status='A' ORDER BY leave_period.id ASC";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

// search patient opearion
if ($operation == "SearchReports") {
    $staffId = "";

    if (isset($_POST['leavePeriod'])) {
        $leavePeriod = $_POST["leavePeriod"];
    }
	
	$selectQuery = "SELECT staff_type_id from users where id = '".$userId."'";
	$executeQuery = mysqli_query($conn,$selectQuery);
	while($r = mysqli_fetch_assoc($executeQuery)) {
		$staffId = $r['staff_type_id'];
	}
	
	$isFirst = "false";
	$query = "select COUNT(CASE WHEN staff_leaves.leave_status = 'taken' THEN staff_leaves.staff_id END) as count_taken,
			COUNT(CASE WHEN staff_leaves.leave_status = 'schedule' THEN staff_leaves.staff_id END) as count_schedule,
			COUNT(CASE WHEN staff_leaves.leave_status = 'approval' THEN staff_leaves.staff_id END) as count_approval
			,entitlements.entitlements,leave_type.name from staff_leaves 
			LEFT JOIN entitlements ON entitlements.staff_type_id = '".$staffId."'
			Left Join leave_type on leave_type.id = entitlements.leave_type_id
			where staff_leaves.staff_id = '".$userId."'";
	
	if ($leavePeriod != '') {
		$isFirst = "true";
	}

	if ($leavePeriod != '') {
		if ($isFirst != "false") {
			$query .= " AND ";
		}
		$query .= " entitlements.leave_period_id = '" . $leavePeriod . "'";
		$isFirst = "true";
	}
        
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }   
    print json_encode($rows);
}
?>