<?php
/*
 * File Name    :   patient.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre details from database        
 */

include 'config.php'; // import database connection file
include '../AfricasTalkingGateway.php';
$operation = "";
$userId    = "";
$date      = new DateTime();
/* $headers=""; */
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
if (isset($_POST['operation'])) { // opeartion come from js 
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}


if ($operation == "checkAccount") {
    
    $accountNo     = $_POST['accountNo'];
    $accountPrefix = $_POST['accountPrefix'];
    $sql         = "SELECT value FROM configuration WHERE name = 'account_prefix'";
    $result      = mysqli_query($conn, $sql);
    
    while ($r = mysqli_fetch_assoc($result)) {
        $prefix = $r['value'];
    }
    if ($prefix == $accountPrefix) {
        
        $query      = "SELECT CONCAT(first_name,' ',last_name) AS name  FROM patients where id='" . $accountNo . "'";
        $result     = mysqli_query($conn, $query);
        $rows_count = mysqli_num_rows($result);
		
        if ($rows_count > 0) {            
           while ($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
			}
			print json_encode($rows);
        }
		else{
			echo "0";
		}
	} 
	else {
        echo "2";
    }
}

if (isset($_FILES["file"]["type"])) // use for image uploading
    {
    
    $validextensions = array(
        "jpeg",
        "jpg",
        "png",
        "gif"
    );
    $temporary       = explode(".", $_FILES["file"]["name"]);
    
    $extension = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION); // getting extension
    /* $imagename = pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME) . $date->getTimestamp(); // getting a file name */
	$str = 'abcdef';
	$imagename = str_shuffle($str). $date->getTimestamp();
    $image_name = $imagename . "." . $extension;
    $name       = str_replace(" ", "", $image_name); // remove space
    
    $file_extension = end($temporary);
    if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")) && ($_FILES["file"]["size"] < 2002000) //Approx. 2mb files can be uploaded.
        && in_array($file_extension, $validextensions)) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
        } else {
            if (file_exists("../../upload_images/patient_dp/" . $name)) {
                echo "";
            } else {
                $sourcePath = $_FILES["file"]["tmp_name"]; // Storing source path of the file in a variable
                $targetPath = "../../upload_images/patient_dp/" . $name; // Target path where file is to be stored
                move_uploaded_file($sourcePath, $targetPath); // Moving Uploaded file
                
                echo $name;
                
            }
        }
    } else {
        echo "invalid file";
    }
}
// call for email validation
if ($operation == "checkEmail") {
    $email     = $_POST['Email'];
    $query     = "Select email from patients where email = '" . $email . "'";
    $sqlSelect = mysqli_query($conn, $query);
    $numrows   = mysqli_num_rows($sqlSelect);
    if ($numrows > 0) {
        $rows = array();
        while ($r = mysqli_fetch_assoc($sqlSelect)) {
            $rows[] = $r;
        }
        echo "1";
    } else {
        echo "0";
    }
    
}
// show country
if ($operation == "showcountry") {
    $query = "SELECT location_id,name FROM locations where location_type = 0 ORDER BY name";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
// show state
if ($operation == "showstate") {
    $countryID = $_POST['country_ID'];
    $query     = "SELECT location_id,name FROM locations where location_type = 1 and parent_id = '$countryID' ORDER BY name";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
// show city
if ($operation == "showcity") {
    $stateID = $_POST['state_ID'];
    $query   = "SELECT location_id,name FROM locations where location_type = 2 and parent_id = '$stateID' ORDER BY name";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
// save details
if ($operation == "save") {
    $salutation   = $_POST['salutation'];
    $lname        = $_POST['Lname'];
    $fname        = $_POST['Fname'];
    $middleName   = $_POST['middleName'];
    $dob          = $_POST['Dob'];
    $gender       = $_POST['Gender'];
    $address      = $_POST['Address'];
    $country      = $_POST['Country'];
    $state        = $_POST['State'];
    $city         = $_POST['City'];
    $zipcode      = $_POST['Zipcode'];
    $email        = $_POST['Email'];
    $mobile       = $_POST['Mobile'];
    $mobileCode       = $_POST['mobileCode'];
    $phone        = $_POST['Phone'];
    $careOff      = $_POST['CareOff'];
    $careOffPhone = $_POST['CareOffPhone'];
    $createdate   = new DateTime();
    $imagename    = "";
    $targetPath   = "";
    
    if ($_POST['imageName'] != "") {
        $imagename  = $_POST['imageName'];
        $targetPath = str_replace(" ", "", $imagename); // remove space
    } else {
        $targetPath = "";
    }
    function crypto_rand_secure($min, $max) // generate crypto random value
    {
        $range = $max - $min;
        if ($range < 1)
            return $min; // not so random...
        $log    = ceil(log($range, 2));
        $bytes  = (int) ($log / 8) + 1; // length in bytes
        $bits   = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }
    function getToken($length) // use for generate random value    
    {
        $token        = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        $max = strlen($codeAlphabet) - 1;
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[crypto_rand_secure(0, $max)];
        }
        return $token;
    }
    $options           = array(
        'password' => crypto_rand_secure(5, 10)
    );
    $decryptedPassword = getToken(10); // generated random password
    /* $decryptedPassword = "Admin@123#"; */
    $encryptedpassword = password_hash($decryptedPassword, PASSWORD_BCRYPT, $options); // generated encryption password  
	if (isset($_POST['accountNo'])) {
        $accountNo = $_POST["accountNo"];
		
		$sql               = "INSERT INTO patients(salutation,first_name,last_name,middle_name,fan_id,dob,gender,address,country_id,state_id,city_id,zip,
            email,mobile,country_code,phone,care_of,care_contact,created_on,updated_on,created_by,updated_by,password,images_path)
            VALUES('" . $salutation . "','" . $fname . "','" . $lname . "','" . $middleName . "','" . $accountNo . "','" . $dob . "','" . $gender . "','" . $address . "',
            '" . $country . "','" . $state . "','" . $city . "','" . $zipcode . "',
            '" . $email . "','" . $mobile . "','" . $mobileCode . "','" . $phone . "','" . $careOff . "','" . $careOffPhone . "','" . $date->getTimestamp() . "','" . $date->getTimestamp() . "',
            '" . $userId . "','" . $userId . "','" . $encryptedpassword . "','" . $targetPath . "')";
			$result            = mysqli_query($conn, $sql);
    }
	else{
		$sql               = "INSERT INTO patients(salutation,first_name,last_name,middle_name,fan_id,dob,gender,address,country_id,state_id,city_id,zip,
				email,mobile,country_code,phone,care_of,care_contact,created_on,updated_on,created_by,updated_by,password,images_path)
				VALUES('" . $salutation . "','" . $fname . "','" . $lname . "','" . $middleName . "',NULL,'" . $dob . "','" . $gender . "','" . $address . "',
				'" . $country . "','" . $state . "','" . $city . "','" . $zipcode . "',
				'" . $email . "','" . $mobile . "','" . $mobileCode . "','" . $phone . "','" . $careOff . "','" . $careOffPhone . "','" . $date->getTimestamp() . "','" . $date->getTimestamp() . "',
				'" . $userId . "','" . $userId . "','" . $encryptedpassword . "','" . $targetPath . "')";
		/* mail($email, "New Account Created On HRP", "USERNAME: ".$email." PASSWORD:".$decryptedPassword."", $headers); */
		$result            = mysqli_query($conn, $sql);
        //echo $result;
	}
    $sql_id;
    $query_id;
    $rowID;
	if($result){
		$sql_id   = "SELECT LAST_INSERT_ID()";
		$query_id = mysqli_query($conn, $sql_id);
		while ($row = mysqli_fetch_row($query_id)) {
			$rowID = $row[0];
		}
		if ($rowID != '' && $rowID != '0') {
		  $sql_select   = "SELECT *,(select value from configuration where name = 'patient_prefix') as prefix
							from patients where id='".$rowID."'";
			$query_select = mysqli_query($conn, $sql_select);
			$rowData = array();
			while ($rows = mysqli_fetch_assoc($query_select)) {
				$rowData[] = $rows;
			}
			
			echo json_encode($rowData);
		}
	}  
    /*$last_id;
    if ($conn->query($sql) === TRUE) {
        $last_id = $conn->insert_id;
    }*/
    if($result == '1'){
        $selQuery = "SELECT CONCAT(p.first_name,' ',p.last_name) AS patient_name,p.id AS patient_id,p.mobile AS mobile_no,p.country_code,
        DATE_FORMAT(FROM_UNIXTIME(p.created_on), '%Y-%m-%d') AS patient_date,sms_email.id,sms_email.patient_reg_sms FROM sms_email,
        patients AS p WHERE p.id = '" . $rowID . "'";
        //exit();
        $resultCheck = mysqli_query($conn,$selQuery);
        $totalrecords = mysqli_num_rows($resultCheck);
        $rows = array();
        $patient_name;
        $patient_id;
        $patient_reg_sms;
        $mobile_no;
        $date;
        while ($r = mysqli_fetch_assoc($resultCheck)) {
            $rows = $r;
            
            $patient_name = $rows['patient_name'];
            $patient_id = $rows['patient_id'];
            $patient_reg_sms = $rows['patient_reg_sms'];
            $mobile_no = $rows['mobile_no'];
            $date = $rows['patient_date'];
            $country_code = $rows['country_code'];
        }
        $previousMessage  = $patient_reg_sms;
        $replace = array("@@user_name@@", "@@patient_id@@");
        $replaced   = array($patient_name, $patient_id);
        $message = str_replace($replace, $replaced, $previousMessage);
        
        //exit();

        // sms to patient
        $recipients = $country_code.$mobile_no;
        // And of course we want our recipients to know what we really do
        $message    = $message;
        //print_r($message);
        //exit();
        // Create a new instance of our awesome gateway class
        $gateway    = new AfricasTalkingGateway($username, $apikey);
        // Any gateway error will be captured by our custom Exception class below, 
        // so wrap the call in a try-catch block
        try 
        { 
          // Thats it, hit send and we'll take care of the rest. 
          $results = $gateway->sendMessage($recipients, $message);
                    
            if($results){
                $sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
                            created_on,updated_on,created_by,updated_by,msg_status) VALUES('Patient Registration',
                            '".$recipients."','','".$message."','','".$date."',UNIX_TIMESTAMP(),
                            UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS')";
                    $result = mysqli_query($conn, $sqlQuery);
                    //echo $result;
            }   
          foreach($results as $result) {
            // status is either "Success" or "error message"
            //echo " Number: " .$result->number;
            //echo " Status: " .$result->status;
            //echo " MessageId: " .$result->messageId;
            //echo " Cost: "   .$result->cost."\n";
          }
        }
        catch ( AfricasTalkingGatewayException $e )
        {
            $sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
                        created_on,updated_on,created_by,updated_by,msg_status,status) VALUES('Patient Registration',
                        '".$recipients."','','".$message."','','".$date."',UNIX_TIMESTAMP(),
                        UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS','pending')";
                    $result = mysqli_query($conn, $sqlQuery);
                    echo $result;
          //echo "Encountered an error while sending: ".$e->getMessage();
        }
        // DONE!!! 
    }
}
?>