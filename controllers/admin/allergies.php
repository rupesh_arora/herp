<?php
/*
 * File Name    :   allergies.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre details from database        
 */
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
$operation        = "";
$diseasesCategory = "";
$description      = "";
$createdate       = new DateTime();
include 'config.php';					// import database connection file
if (isset($_POST['operation'])) {		// opeartion come from js 
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
    
}
if ($operation == "showcategory") // show category in category selection box
    {
    $query  = "SELECT id,category FROM allergies_categories where status = 'A' ORDER BY category";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "checkdiseases") // check duplicacy
    {
    $code        = $_POST['Code'];
    $name        = $_POST['Name'];
    $category_id = $_POST['category_Id'];
    $id          = $_REQUEST['Id'];
    if ($id != "") {
        $query = "Select code from allergies where code = '" . $code . "' and id != " . $id . "";
    } else {
        $query = "Select code from allergies where code = '" . $code . "'";
    }
    $sqlSelect = mysqli_query($conn, $query);
    $numrows   = mysqli_num_rows($sqlSelect);
    if ($numrows > 0) {
        echo "1";
    } else {
        echo "0";
    }
}
if ($operation == "save") // save details
    {
    if (isset($_POST['Code'])) {
        $code = $_POST['Code'];
    }
    if (isset($_POST['Name'])) {
        $name = $_POST['Name'];
    }
    $category_id = $_POST['category_Id'];
    
    $sql    = "INSERT INTO allergies(name,code,allergy_cat_id,created_on,updated_on,created_by,updated_by) VALUES('" . $name . "','" . $code . "','" . $category_id . "','" . $createdate->getTimestamp() . "','" . $createdate->getTimestamp() . "','" . $userId . "','" . $userId . "')";$
    $result = mysqli_query($conn, $sql);
    echo $result;
}
if ($operation == "show") { // show active data
    
    $query        = "select allergies.*,allergies_categories.category from allergies LEFT JOIN allergies_categories ON allergies.allergy_cat_id=allergies_categories.id where allergies.status='A' and allergies_categories.status='A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
    
}
// opertaion form update
if ($operation == "update") {
    if (isset($_POST['Code'])) {
        $code = $_POST['Code'];
    }
    if (isset($_POST['Name'])) {
        $name = $_POST['Name'];
    }
    $category_id = $_POST['category_Id'];
    
    if (isset($_POST['Id'])) {
        $id = $_POST['Id'];
    }
    
    $sql    = "update allergies set code= '" . $code . "' , name = '" . $name . "', allergy_cat_id='" . $category_id . "', updated_on='" . $createdate->getTimestamp() . "',updated_by='" . $userId . "' where id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}
//  operation for delete
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE allergies SET status= 'I' where id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}
//When checked box is check
if ($operation == "checked") {
    
    $query        = "select allergies.*,allergies_categories.category from allergies LEFT JOIN allergies_categories ON allergies.allergy_cat_id=allergies_categories.id where allergies.status='I' or allergies_categories.status='I'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}
if ($operation == "restore") // for restore details 
    {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $category = $_POST['category'];
    $sql      = "UPDATE allergies SET status= 'A'  WHERE  id = '" . $id . "'";
    $result   = mysqli_query($conn, $sql);
    if ($result == 1) {
        $query  = "SELECT status from allergies_categories where id='" . $category . "'";
        $result = mysqli_query($conn, $query);
        while ($r = mysqli_fetch_assoc($result)) {
            if ($r['status'] == 'A') {
                echo "1";
            } else {
                echo "0";
            }
        }
    }
}
?>