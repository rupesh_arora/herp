<?php
/*File Name  :   bed.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page manages AND add beds*/

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

/*include config file*/
include 'config.php';

$operation   = "";
$description = "";

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}



/*operation to save data*/
if ($operation == "save") {
    if (isset($_POST['staffNameId'])) {
        $staffNameId = $_POST['staffNameId'];
    }
    if (isset($_POST['outstanding'])) {
        $outstanding = $_POST['outstanding'];
    }
    if (isset($_POST['estimationDay'])) {
        $estimationDay = $_POST['estimationDay'];
    }
    if (isset($_POST['amount'])) {
        $amount = $_POST['amount'];
    }
    if (isset($_POST['startDate'])) {
        $startDate = $_POST['startDate'];
    }
    if (isset($_POST['endDate'])) {
        $endDate = $_POST['endDate'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    
   
    $sql = "INSERT INTO imprest_request(staff_id,imprest_outstanding,nature_of_duty,est_day,amount,assignment_start,assignment_end,created_on,created_by,updated_on,updated_by) VALUES('" . $staffNameId . "','" . $outstanding . "','" . $description . "','" . $estimationDay . "','" . $amount . "','" . $startDate . "','" . $endDate . "',UNIX_TIMESTAMP(),'".$userId."',UNIX_TIMESTAMP(),'".$userId."')";
    $result = mysqli_query($conn, $sql);
    echo $result;
}

/*operation to show active data*/
if ($operation == "show") {
    
    $query        = "SELECT ir.*,(ir.amount- id.amount) AS outstanding,CONCAT(u.first_name,' ',u.last_name)
                    AS name,u.staff_type_id 
                    FROM imprest_request AS ir 
                    LEFT JOIN users AS u ON u.id = ir.staff_id 
                    LEFT JOIN imprest_disbursement AS id ON id.imprest_request_id = ir.id 
                    WHERE ir.status='A' AND u.status='A' ";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/*operation to show inactive data*/
if ($operation == "showInActive") {
    $query        = "SELECT ir.*,CONCAT(u.first_name,' ',u.last_name) AS name FROM imprest_request AS ir 
                    LEFT JOIN users AS u ON u.id = ir.staff_id 
                    WHERE ir.status='I' OR u.status='I' ";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/*operation to delete data*/
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE imprest_request SET status= 'I' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
	if($result){
		echo "1";
	}else{
		echo "0";
	}  
}

/*operation to restore data*/
if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE imprest_request SET status= 'A' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);   
    
        
    if ($result) {
        echo "Restored Successfully!!!";
    } else {
        echo "It can not be restore!!!";
    }
}

/*operation to update data*/
if ($operation == "saveImprestWarrent") {

     if (isset($_POST['staffNameId'])) {
        $staffNameId = $_POST['staffNameId'];
    }
    if (isset($_POST['outstanding'])) {
        $outstanding = $_POST['outstanding'];
    }
    if (isset($_POST['estimationDay'])) {
        $estimationDay = $_POST['estimationDay'];
    }
    if (isset($_POST['amount'])) {
        $amount = $_POST['amount'];
    }
    if (isset($_POST['startDate'])) {
        $startDate = $_POST['startDate'];
    }
    if (isset($_POST['endDate'])) {
        $endDate = $_POST['endDate'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql = "INSERT INTO imprest_warrent(imprest_request_id,staff_id,imprest_outstanding,nature_of_duty,est_day,amount,assignment_start,assignment_end,created_on,created_by,updated_on,updated_by) VALUES('" . $id . "','" . $staffNameId . "','" . $outstanding . "','" . $description . "','" . $estimationDay . "','" . $amount . "','" . $startDate . "','" . $endDate . "',UNIX_TIMESTAMP(),'".$userId."',UNIX_TIMESTAMP(),'".$userId."')";

    $result = mysqli_query($conn, $sql);

    $updateSql = "UPDATE imprest_request SET is_warrant = 'yes' WHERE id = '" . $id . "'";
    $result = mysqli_query($conn, $updateSql);
    echo $result;
}
?>