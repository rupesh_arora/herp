<?php
	$operation="";
	$totalAmount="";
	$PatientId="";
	$VisitId="";
	$bigprescription_id="";
	include 'config.php';

	$date = new DateTime();	
	$currentDate = date("Y-m-d");	
	$createOn = new DateTime();
	session_start();
	
	if(isset($_SESSION['globaluser'])){
		$userId = $_SESSION['globaluser'];
	}
	else{
		exit();
	}
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	
	else{}
	
	if($operation == "showData"){

		if (isset($_POST['visitId'])) {
			$visitId=$_POST['visitId'];
		}	
		$visitPrefix = $_POST['visitPrefix'];
		$sql       = "SELECT value FROM configuration WHERE name = 'visit_prefix'";
		$result    = mysqli_query($conn, $sql);
		
		while ($r = mysqli_fetch_assoc($result)) {
			$prefix = $r['value'];
		}
		if($prefix == $visitPrefix){
			
			/*$sql       = "SELECT id FROM patients_prescription_request WHERE visit_id = '".$visitId."'";
			$result    = mysqli_query($conn, $sql);
			$rows_count= mysqli_num_rows($result);
			if($rows_count > 0){*/
				
				$sqlSelect="SELECT patients.id,patients.fan_id, CONCAT(patients.first_name,' ',patients.middle_name,' ' ,patients.last_name) AS name, IFNULL(p.quantity, 0) AS stock, patients.dob, patients.gender,visits.created_on , p_r.id AS prescription_id,p_r.drug_id,p_r.quantity AS quantity,d.name AS drug_name,d.price, p_r.quantity * d.price AS total,
					(SELECT value FROM configuration WHERE name = 'patient_prefix') AS patient_prefix,
					p.expiry_date,p_r.prescribe_by,p_r.payment_mode,p_r.insurance_payment from patients 
					LEFT JOIN visits ON visits.patient_id = patients.id 
					LEFT JOIN patients_prescription_request AS p_r ON p_r.visit_id = visits.id AND
					p_r.dispense_status = 'Pending' 
					LEFT JOIN drugs AS d ON d.id = p_r.drug_id 
					LEFT JOIN pharmacy_inventory AS p  ON p.drug_name_id = d.id
					where visits.id= '".$visitId."'";
				$resultSelect=mysqli_query($conn,$sqlSelect);
				$rows_count= mysqli_num_rows($resultSelect);
				if($rows_count > 0){
					$rows = array();
					while($r = mysqli_fetch_assoc($resultSelect)) {
						$rows[] = $r;
					}
					$patient_id =  $rows[0]['id'];
					$fan_id = $rows[0]['fan_id'];
					 
					if($fan_id != NULL){
						$id = $fan_id;
					}
					else{
						$id = $patient_id;
					}
					
					$sqlamount=" select sum(c_a.credit )-sum(c_a.debit ) as amount from cash_account AS c_a   where c_a.patient_id ='".$id."'";
					$resultamount=mysqli_query($conn,$sqlamount);
					while($r = mysqli_fetch_assoc($resultamount)) {
						$rows[] = $r;
					}
				   print json_encode($rows);
				}
			/*}
			else{
			   $sqlSelect="SELECT patients.id,patients.fan_id,'self_request' AS visit_type, CONCAT(patients.first_name,' ',patients.middle_name,' ' ,patients.last_name) AS name, IFNULL(p.quantity, 0) AS stock, patients.dob, patients.gender,
				visits.created_on , p_r.id AS prescription_id,p_r.drug_id,p_r.quantity AS quantity,d.name AS drug_name,
				d.price, p_r.quantity * d.price AS total,(SELECT value FROM configuration WHERE name = 'patient_prefix')
				AS patient_prefix from patients 
				LEFT JOIN visits ON visits.patient_id = patients.id 
				LEFT JOIN pharmacy_request AS p_r ON p_r.visit_id = visits.id AND p_r.pay_status ='unpaid' 
				LEFT JOIN drugs AS d ON d.id = p_r.drug_id 
				LEFT JOIN pharmacy_inventory AS p  ON p.drug_name_id = d.id
				where visits.id= '".$visitId."'";
				$resultSelect=mysqli_query($conn,$sqlSelect);
				$rows_count= mysqli_num_rows($resultSelect);
				if($rows_count > 0){
					$rows = array();
					while($r = mysqli_fetch_assoc($resultSelect)) {
						$rows[] = $r;
					}
					$patient_id =  $rows[0]['id'];
					$fan_id = $rows[0]['fan_id'];
					 
					if($fan_id != NULL){
						$id = $fan_id;
					}
					else{
						$id = $patient_id;
					}
					
					$sqlamount=" select sum(c_a.credit )-sum(c_a.debit ) as amount from cash_account AS c_a   where c_a.patient_id ='".$id."'";
					$resultamount=mysqli_query($conn,$sqlamount);
					while($r = mysqli_fetch_assoc($resultamount)) {
						$rows[] = $r;
					}
				   print json_encode($rows);
				}
				else{
					echo "0";
				}
			}  */
		}
		else{
			echo "2";
		}			
	}	
	
	if($operation == "saveDataTableData"){
	    if (isset($_POST['bigprescription_id'])) {
		   $bigprescription_id = json_decode($_POST['bigprescription_id']);
		}	
	    if (isset($_POST['bigFinalStock'])) {
		   $bigFinalStock = json_decode($_POST['bigFinalStock']);
		}	
	    if (isset($_POST['bigSelfRequestId'])) {
		   $bigSelfRequestId = json_decode($_POST['bigSelfRequestId']);
		}	
		if (isset($_POST['totalAmount'])) {
		   $totalAmount = $_POST['totalAmount'];
		}	
		if (isset($_POST['netAmount'])) {
		   $netAmount = $_POST['netAmount'];
		}	
		if (isset($_POST['paymentMode'])) {
			$paymentMode=$_POST["paymentMode"];
		}
		if (isset($_POST['discount'])) {
		   $discount = $_POST['discount'];
		}	
		if (isset($_POST['PatientId'])) {
		   $PatientId = $_POST['PatientId'];
		}
		if (isset($_POST['familyMemberId'])) {
			$familyMemberId = $_POST["familyMemberId"];
		}	
		if (isset($_POST['VisitId'])) {
		   $VisitId = $_POST['VisitId'];
		}
		if (isset($_POST['printDiv'])) {
			$printDiv = $_POST["printDiv"];
		}
		if (isset($_POST['status'])) {
			$status = $_POST["status"];
		}
		$creditDeposit="0";
		//$finalData = "";
		$counter = 0;
		$result = '';
		/*foreach($bigSelfRequestId as $value) {
			  $id = 0;
			   foreach($value as $key => $val) {			
				    $id = $val;	
			
					$updateQuery = "UPDATE pharmacy_request set pay_status = 'paid' WHERE id = '".$id."'";
					$result= mysqli_query($conn,$updateQuery);
					 $counter++;
			   }
		} */
		if($status == 'paid'){
			foreach($bigprescription_id as $value) {
			  $id = 0;
			   foreach($value as $key => $val) {			
				    $id = $val;	
			
					$updateQuery = "UPDATE patients_prescription_request set dispense_status = 'Dispensed' WHERE id = '".$id."'";
					$result= mysqli_query($conn,$updateQuery);
					 $counter++;
			   }
			} 
			foreach($bigFinalStock as $value) { //This is run for update the quantity
				  $id    = 0;
	              $stock = 0;
				   foreach($value as $key => $val) {			
					    if ($key == 0) {
	                        $id = $val;
	                    }
	                    if ($key == 1) {
	                        $stock = $val;
	                    }					
						 $counter++;
				   }
				   $updateStock = "UPDATE pharmacy_inventory set quantity = '".$stock."' WHERE drug_name_id = '".$id."'";
				   $result= mysqli_query($conn,$updateStock);
			}
			if($result){
				echo '1';
			}
			else{
				echo '0';
			}
		}
		
		else{

			foreach($bigprescription_id as $value) {
			  $id = 0;
			  $qty = 0;

			   foreach($value as $key => $val) {			
				    if ($key == 0) {
                        $id = $val;
                    }
                    if ($key == 1) {
                        $qty = $val;
                    }	
			
					$updateQuery = "UPDATE patients_prescription_request set quantity = '".$qty."' WHERE id = '".$id."'";
					$result= mysqli_query($conn,$updateQuery);
					 $counter++;
			   }
			} 
			if (isset($_POST['extraMedicine'])) {
				$extraMedicine=json_decode($_POST["extraMedicine"]);
				
				foreach($extraMedicine as $value) {
					
					/* $PatientId = $value-> patientId; */
					$quantity = $value-> quantity;
					$drugName = $value-> drugName;
					$price = $value-> price;
					/* $visitId = $value-> visitId; */
					$drugId = $value-> drugId;
					
					$sql = "INSERT INTO patients_prescription_request(visit_id, patient_id, drug_id,quantity,cost,prescribe_by,dispense_status,payment_mode,insurance_payment)
					VALUES('".$VisitId."', '".$PatientId."','".$drugId."','".$quantity."','".$price."','Extra','Pending','cash','unpaid')";                      
					$result= mysqli_query($conn,$sql);
				}				
			}
			if($result){
				echo 'Update successfully';
			}
			else{
				echo '0';
			}
		}
		
		
		/*$sqlPharmacyBill = "INSERT INTO patient_pharmacy_bill(visit_id, patient_id, amount,net_amount,discount,status,created_on,updated_on,created_by,updated_by)
		VALUES('".$VisitId."', '".$PatientId."','".$totalAmount."','".$netAmount."','".$discount."','paid',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";		
		$resultPharmacyBill= mysqli_query($conn,$sqlPharmacyBill);
		
		$query="SELECT id+1 AS id FROM patient_bill_payment ORDER BY id DESC LIMIT 1";
		$result=mysqli_query($conn,$query);	
		$rows_count= mysqli_num_rows($result);
		if($rows_count > 0){
			while($row=mysqli_fetch_row($result)) {
			 $receiptNo = $row[0];
			}
		}*/
		
		//$finalData = $finalData.'VisitId: '.$VisitId;
		
		/*$sqlInsertBill = "INSERT INTO patient_bill_payment (visit_id,receipt_no,payment_mode,amount_tendered)
		values('".$VisitId."','".$receiptNo."','".$paymentMode."','".$netAmount."')";
		$resultInsertBill= mysqli_query($conn,$sqlInsertBill);
		
		if($resultInsertBill == "1" && $paymentMode == "2")
		{   
			$sqlInsertInvoice = "INSERT INTO invoice_details (invoice_details) VALUES ('".$printDiv."')";
			$resultInvoice = mysqli_query($conn,$sqlInsertInvoice);
			if($resultInvoice) {
				$lastInsertId = mysqli_insert_id($conn);
				// cash deposite transaction
				$insertTransaction = "INSERT INTO transactions (patient_id,transaction_type_id,credit,debit,processed_on,processed_by,invoice_id)
						values ('" . $PatientId . "','2','0','" .$netAmount. "',UNIX_TIMESTAMP(),'".$userId ."','".$lastInsertId."')"; 
				mysqli_query($conn,$insertTransaction);	
			}
		
			$sql = "INSERT INTO cash_account (patient_id,depositor_id,date,credit,debit,created_on,created_by)
			values('".$familyMemberId."','".$PatientId."','".$currentDate."','".$creditDeposit."','".$netAmount."',UNIX_TIMESTAMP(),'".$userId."')";
			$result= mysqli_query($conn,$sql);
			if($result == "1")
			{
				echo "1";
			}
			else{
				echo "0";
			}
		}
		else{			
			
			$sqlInsertInvoice = "INSERT INTO invoice_details (invoice_details) VALUES ('".$printDiv."')";
			$resultInvoice = mysqli_query($conn,$sqlInsertInvoice);
			if($resultInvoice) {
				$lastInsertId = mysqli_insert_id($conn);
				// cash deposite transaction
				$insertTransaction = "INSERT INTO transactions (patient_id,transaction_type_id,credit,debit,processed_on,processed_by,invoice_id)
						values ('" . $PatientId . "','2','" .$netAmount. "','" .$netAmount. "',UNIX_TIMESTAMP(),'".$userId ."','".$lastInsertId."')"; 
				mysqli_query($conn,$insertTransaction);	
			}
			
			$sqlCredit = "INSERT INTO cash_account (patient_id,depositor_id,date,credit,debit,created_on,created_by)
			values('".$PatientId."','".$PatientId."','".$currentDate."','".$netAmount."','0',UNIX_TIMESTAMP(),'".$userId."')";
			$resultCredit= mysqli_query($conn,$sqlCredit);
			
			$sqlDebit = "INSERT INTO cash_account (patient_id,depositor_id,date,credit,debit,created_on,created_by)
			values('".$PatientId."','".$PatientId."','".$currentDate."','0','".$netAmount."',UNIX_TIMESTAMP(),'".$userId."')";
			$resultDebit= mysqli_query($conn,$sqlDebit);
			
			if($resultCredit == "1" && $resultDebit =="1")
			{
				echo "1";
			}
			else{
				echo "0";
			}
		}*/
	}
	//This operation is used for select last receipt number 
	if ($operation == "receipt") {
		
		$query      = "SELECT id FROM invoice_details ORDER BY id DESC LIMIT 1";
		$result     = mysqli_query($conn, $query);
		$rows_count = mysqli_num_rows($result);
		if ($rows_count > 0) {
			while ($row = mysqli_fetch_row($result)) {
				$rows = $row[0];
			}
			echo $rows;
		} else {
			echo "0";
		}
	}
?>