<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';

	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}


	if ($operation == "showGLAccount") {

		$query = "SELECT id,account_name FROM chart_of_account WHERE status = 'A'";
	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "showChartData") {
		
		$toDate = $_POST['toDate'];
		$fromDate = $_POST['fromDate'];
		$glAccountId = $_POST['glAccountId'];

		if ($toDate !='' && $fromDate !='') {
			
			$qry = "SELECT(SELECT SUM(charge) FROM patient_radiology_bill WHERE gl_account_id = '".$glAccountId."' AND created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS `Radiology Cash`,(SELECT SUM(charge) FROM patient_procedure_bill WHERE gl_account_id = '".$glAccountId."' AND created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS `Procedure Cash`, (SELECT SUM(charge) FROM patient_lab_bill WHERE gl_account_id = '".$glAccountId."' AND created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS `Lab Cash`, (SELECT SUM(charge) FROM patient_service_bill WHERE gl_account_id = '".$glAccountId."' AND created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS `Service Cash`,(SELECT SUM(charge) FROM patient_forensic_bill WHERE gl_account_id = '".$glAccountId."' AND created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59')) AS `Forensic Cash`";
		}
		else{
			$qry = "SELECT(SELECT SUM(charge) FROM patient_radiology_bill WHERE gl_account_id = '".$glAccountId."')  AS `Radiology Cash`, (SELECT SUM(charge) FROM patient_procedure_bill WHERE gl_account_id = '".$glAccountId."')  AS `Procedure Cash`, (SELECT SUM(charge) FROM patient_lab_bill WHERE gl_account_id = '".$glAccountId."')  AS `Lab Cash`,(SELECT SUM(charge) FROM patient_service_bill WHERE gl_account_id = '".$glAccountId."')  AS `Service Cash`, (SELECT SUM(charge) FROM patient_forensic_bill WHERE gl_account_id = '".$glAccountId."')  AS `Forensic Cash`";
		}

		$result = mysqli_query($conn, $qry);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
		echo json_encode($rows);
	}
?>