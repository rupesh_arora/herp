<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}

	if($operation == "loadSpecimen"){
		if (isset($_POST['specimenId'])) {
			$id=$_POST["specimenId"];
		}
		$sql = "SELECT forensic_test_request.patient_id,
		forensic_test_request.visit_id,
		forensic_tests.test_code,
		forensic_tests.name,
		forensic_tests.range as rangeNormal,

		forensic_test_request.cost,
		specimens_types.type,
		patient_forensic_specimen.description,
		patient_forensic_specimen.created_on,

        forensic_test_result.result_flag,
        forensic_test_result.report
		
		FROM patient_forensic_specimen
        LEFT JOIN `forensic_test_request`  ON forensic_test_request.id = patient_forensic_specimen.forensic_test_id
		LEFT JOIN forensic_tests ON forensic_test_request.forensic_test_id = forensic_tests.id 
		LEFT JOIN specimens_types ON specimens_types.id = forensic_tests.specimen_id LEFT JOIN patients On 
		patients.id = forensic_test_request.patient_id 
		LEFT JOIN forensic_test_result ON forensic_test_result.specimen_id = patient_forensic_specimen.id
		WHERE patient_forensic_specimen.status = 'A' AND patient_forensic_specimen.id = ".$id." AND forensic_test_request.pay_status = 'paid' AND
		forensic_test_request.test_status = 'done'";
		
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		echo json_encode($rows);			
	}
	if($operation == "saveResult"){
		if (isset($_POST['txtSpecimenId'])) {
			$specimenId=$_POST['txtSpecimenId'];
		}
		if (isset($_POST['txtNormalRange'])) {
			$range=$_POST['txtNormalRange'];
		}
		if (isset($_POST['txtResult'])) {
			$result=$_POST['txtResult'];
		}
		if (isset($_POST['ddlResultFlag'])) {
			$resultFlag=$_POST['ddlResultFlag'];
		}
		if (isset($_POST['txtReport'])) {
			$report=$_POST['txtReport'];
		}
		

		$sql_select = "SELECT COUNT(*) from forensic_test_result where specimen_id =".$specimenId."";  
		$result_select = mysqli_query($conn,$sql_select);  
		while ($row=mysqli_fetch_row($result_select))
		{
			$count = $row[0];
		}
		
		if($count == 0)
		{			
			$sql = "INSERT INTO `forensic_test_result`(`specimen_id`, `result_flag`,`report`,`normal_range`,`result`,`created_on`, `updated_on`, `created_by`, `updated_by`)
			 VALUES('".$specimenId."','".$resultFlag."','".$report."','".$range."','".$result."', UNIX_TIMESTAMP(), UNIX_TIMESTAMP(), '".$_SESSION['globaluser']."', '".$_SESSION['globaluser']."')";		
			 
			$result= mysqli_query($conn,$sql);
			 if($result){
				$sqlupdate = "update `forensic_test_request` SET `pay_status`='settled',`test_status` ='resultsent' where id = 
				(select patient_forensic_specimen.forensic_test_id from patient_forensic_specimen 
				Left Join forensic_test_result on patient_forensic_specimen.id = forensic_test_result.specimen_id 
				where forensic_test_result.specimen_id = '".$specimenId."')";
				mysqli_query($conn,$sqlupdate);
			} 
			echo $result;
		}
		else{

			$sql = "UPDATE `forensic_test_result` SET 
			`result_flag`='".$resultFlag."',`report`='".$report."',`normal_range`='".$range."',
			`result`='".$result."',`updated_on`= UNIX_TIMESTAMP(),
			`updated_by`='".$_SESSION['globaluser']."' WHERE specimen_id=".$specimenId."";			
			$result= mysqli_query($conn,$sql);			
			echo $result;
		}				
	}
?>