<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
    $operation = "";
	$opdRoom = "";
	$consultant = "";
	$date = new DateTime();
	
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	
	// It query use for select the wards
	if($operation == "showOPDRooms")			
	{		
		$query="SELECT CONCAT(r.number,' ','(',w.name,')') AS number,opr.id from ward_room AS r 
		Inner Join opd_rooms  AS opr On r.id = opr.room_id 
		Left JOIN wards AS w On w.id = r.ward_id
		WHERE r.status='A' And opr.`status`='A' AND w.`status`='A'";
		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 print json_encode($rows);
	} 
	
	
	
	// It query use for select the department
	if($operation == "showConsultants")			
	{		
		$query="SELECT u.id,u.first_name from users AS u 
		Inner Join staff_type  AS s On u.staff_type_id = s.id 
		WHERE s.status='A' And u.`status`='A'";
		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 print json_encode($rows);
	} 
	
	// It is use for save the data
	if($operation == "save")			
	{
		if (isset($_POST['opdRoom'])) {
			$opdRoom=$_POST['opdRoom'];
		}
		if (isset($_POST['consultant'])) {
			$consultant=$_POST['consultant'];
		}

		$sql1 = "SELECT opd_room_id from opd_consultants where consultant_id='".$consultant."'";
		$resultSelect=mysqli_query($conn,$sql1);
		$rows_count= mysqli_num_rows($resultSelect);
		/* echo $rows_count;
		echo $sql1; */
		if($rows_count <= 0){
		
			$sql = "INSERT INTO opd_consultants(opd_room_id,consultant_id,created_on,updated_on,created_by,updated_by,status) VALUES('".$opdRoom."','".$consultant."',,UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."','A')";
			$result= mysqli_query($conn,$sql);  
			echo $result;
		
		}
		else{
			echo "0";
		}
		
	} 
	
	if($operation == "show"){
		
		$query= "SELECT o_c.id,o_c.opd_room_id,o_c.consultant_id,  CONCAT(r.number,'(',w.name,')') AS room_number, CONCAT(u.first_name, ' ', u.last_name, ' (', dep.department,')' )  AS name
		FROM ward_room AS r

		LEFT JOIN opd_rooms AS o_r 
		ON o_r.room_id = r.id

		Left JOIN opd_consultants AS o_c
		ON o_c.opd_room_id = o_r.id 

		Left JOIN users AS u
		ON u.id = o_c.consultant_id

		LEFT JOIN department AS dep
		ON dep.id = o_r.department_id

		LEFT JOIN wards AS w
		ON w.id = r.ward_id

		WHERE o_c.opd_room_id = o_r.id 
		AND O_r.room_id = r.id AND  u.id = o_c.consultant_id AND
		o_r.status='A' AND w.status='A' AND r.status='A' AND o_c.status='A' AND 
		dep.status='A' AND u.status='A'";
		
		
		/* $query= "select id, ward_id,number,description from ward_room"; */
		/* echo $query; */
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 //print json_encode($rows);
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
	if($operation == "showInActive"){
		$query= "SELECT o_c.id,o_c.opd_room_id,o_c.consultant_id,  CONCAT(r.number,'(',w.name,')') AS room_number, CONCAT(u.first_name, ' ', u.last_name, ' (', dep.department,')' )  AS name
		FROM ward_room AS r

		LEFT JOIN opd_rooms AS o_r 
		ON o_r.room_id = r.id

		Left JOIN opd_consultants AS o_c
		ON o_c.opd_room_id = o_r.id 

		Left JOIN users AS u
		ON u.id = o_c.consultant_id

		LEFT JOIN department AS dep
		ON dep.id = o_r.department_id

		LEFT JOIN wards AS w
		ON w.id = r.ward_id

		WHERE (o_c.opd_room_id = o_r.id 
		AND O_r.room_id = r.id AND  u.id = o_c.consultant_id) AND
		(o_r.status='I' OR w.status='I' OR o_c.status='I' OR r.status='I' OR dep.status='I' OR u.status='I')";
		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 //print json_encode($rows);
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
	if($operation == "delete")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		
		$sql = "UPDATE opd_consultants SET status= 'I' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo "Delete Successfully!!!";
	}
	
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		if (isset($_POST['opdRoom'])) {
			$opdRoom=$_POST['opdRoom'];
		}
		if (isset($_POST['consultant'])) {
			$consultant=$_POST['consultant'];
		}
		
		
		$sql = "UPDATE opd_consultants SET status= 'A' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		
		if($result == 1){
			$query = "SELECT  o_c.`status` AS o_c_status,r.`status` AS r_status, u.`status` AS u_status,o_r.`status` AS o_r_status,dep.`status` 
		AS dep_status, w.`status` AS w_status 
		FROM ward_room AS r

		LEFT JOIN opd_rooms AS o_r 
		ON o_r.room_id = r.id

		Left JOIN opd_consultants AS o_c
		ON o_c.opd_room_id = o_r.id 

		Left JOIN users AS u
		ON u.id = o_c.consultant_id

		LEFT JOIN department AS dep
		ON dep.id = o_r.department_id

		LEFT JOIN wards AS w
		ON w.id = r.ward_id

		WHERE  o_c.id ='".$id."' AND o_c.`status` ='A' AND r.`status` ='A' AND u.`status` ='A' 
		AND o_r.`status` ='A' AND dep.`status` ='A' AND w.`status` ='A'";
		
			$result= mysqli_query($conn,$query);
			$count_rows= mysqli_num_rows($result);
			
			
			
			
			if($count_rows == '1'){
				
				echo "Restored Successfully!!!";	
				
			}
			else{
				echo "It can not be restore!!!";				
			}
			/* echo $count_room;
			echo $result_room; */
		}
	}
	
	if($operation == "update")			
	{
		if (isset($_POST['opdRoom'])) {
			$opdRoom=$_POST['opdRoom'];
		}
		if (isset($_POST['consultant'])) {
			$consultant=$_POST['consultant'];
		}
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}

		$sql1 = "SELECT opd_room_id from opd_consultants where consultant_id='".$consultant."' AND id != '$id'";
		$resultSelect=mysqli_query($conn,$sql1);
		$rows_count= mysqli_num_rows($resultSelect);
		
		if($rows_count <= 0){
		
			$sql = "UPDATE opd_consultants SET opd_room_id= '".$opdRoom."',consultant_id= '".$consultant."',updated_on=UNIX_TIMESTAMP(),updated_by='".$userId."' WHERE  id = '".$id."'";
			$result= mysqli_query($conn,$sql);  
			echo $result;
		
		}
		else{
			echo "0";
		}
		
	}
	
?>