<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "showstaffid") { // show staff id/name
	    $staffType = $_POST['staffType'];
	    $query     = "SELECT users.id,CONCAT(users.first_name,' ',users.last_name) AS name FROM users LEFT JOIN 	staff_type ON staff_type.id=users.staff_type_id WHERE staff_type.id= '" . $staffType . "'";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	if ($operation == "showLedger") { // show insurance company type
	    $query = "SELECT id,name FROM ledger where status = 'A' ";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	
	if ($operation == "showCustomerType") { // show staff type
	    $query = "SELECT id,type FROM customer_type where status = 'A'";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	if ($operation == "showCustomerId") { // show staff id/name
	    $customerType = $_POST['customerType'];
	    $query     = "SELECT customer.id,CONCAT(customer.first_name,' ',customer.last_name) AS name FROM customer LEFT JOIN 	customer_type ON customer_type.id=customer.customer_type_id WHERE customer_type.id= '" . $customerType . "'";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if($operation == "savePettyCash") {
		
		$staffCustomerId = $_POST["staffCustomerId"];
		$staffCustomerName = $_POST["staffCustomerName"];
		$staffCustomerNameId = $_POST["staffCustomerNameId"];
		$ledger = $_POST["ledger"];
		$glAccount = $_POST["glAccount"];
		$paymentMode = $_POST['paymentMode'];
		$reference = $_POST['reference'];
		$amount = $_POST["amount"];
		$description = $_POST["description"];  
		$type = $_POST["type"];

		$query = "INSERT INTO petty_cash(payment_mode_id,reference,staff_customer_id,staff_customer_name,staff_customer_name_id,ledger_id,amount,description,created_on,updated_on,created_by,updated_by,gl_account_id)VALUES('".$paymentMode."','".$reference."','".$staffCustomerId."','".$staffCustomerName."','".$staffCustomerNameId."','".$ledger."','".$amount."','".$description."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."','".$glAccount."')";

		mysqli_query($conn,$query);

		$last_id = mysqli_insert_id($conn);

		$sqlInsertBill = "INSERT INTO finance_transaction (gl_account_id,ledger_id,amt_cr,type,type_id,created_on,updated_on,created_by,updated_by) VALUES('".$glAccount."','".$ledger."','".$amount."','petty','".$last_id."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
		$result = mysqli_query($conn,$sqlInsertBill);
		echo $result;
	}

	if ($operation == "show") { // show active data
    
    	$query = "SELECT petty_cash.id AS id,petty_cash.reference,petty_cash.payment_mode_id ,staff_customer_id,staff_customer_name_id,gl_account_id,coa.account_name,staff_customer_name,ledger_id,amount,description,DATE_FORMAT(FROM_UNIXTIME(petty_cash.created_on), '%Y-%m-%d') AS petty_date,ledger.name AS ledger_name
			FROM petty_cash 
			LEFT JOIN ledger ON ledger.id = petty_cash.ledger_id LEFT JOIN chart_of_account AS coa ON coa.id = petty_cash.gl_account_id WHERE petty_cash.status = 'A'";
		//append this to show pending ones	
		if( isset($_GET['showPending']) ){
			$query.=" AND petty_cash.cash_payement_voucher_status = 'unpaid'";
		}

    	if(isset($_GET['fromDate']) && isset($_GET['toDate']) !=''){

			$fromDate = $_GET['fromDate'];
			$toDate = $_GET['toDate'];

			if( $fromDate !='' AND $toDate !='' ){

				$fromDate = $_GET['fromDate'];
				$toDate = $_GET['toDate'];
				$query .= " AND petty_cash.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') 
				AND UNIX_TIMESTAMP('".$toDate." 23:59:59') ";
				$isFirst = "true";
			}
			
			if( $fromDate !='' AND $toDate =='' ){

				$fromDate = $_GET['fromDate'];
				$toDate = $_GET['toDate'];
				$query .= " AND petty_cash.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') 
				AND UNIX_TIMESTAMP(NOW()) ";
				$isFirst = "true";
			}

			if( $fromDate =='' AND $toDate !='' ){

				$fromDate = $_GET['fromDate'];
				$toDate = $_GET['toDate'];
				$query .= " AND petty_cash.created_on BETWEEN '0000000000' AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59') ";
				$isFirst = "true";
			}
		}

	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}
	if ($operation == "checked") { // show active data
    
    	$query = "SELECT petty_cash.id AS id,petty_cash.reference,petty_cash.payment_mode_id ,staff_customer_id,staff_customer_name_id,gl_account_id,coa.account_name,staff_customer_name,ledger_id,amount,description,ledger.name AS ledger_name from petty_cash left join ledger on ledger.id = petty_cash.ledger_id LEFT JOIN chart_of_account AS coa ON coa.id = petty_cash.gl_account_id WHERE petty_cash.status = 'I'";
	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}

	if ($operation == "update") // update data
	{
	    $staffCustomerId = $_POST['staffCustomerId'];
	    $staffCustomerNameId = $_POST['staffCustomerNameId'];
	    $staffCustomerName = $_POST['staffCustomerName'];
	    $ledger = $_POST['ledger'];
		$paymentMode = $_POST['paymentMode'];
		$reference = $_POST['reference'];
	    $amount = $_POST['amount'];
	    $type = $_POST['type'];
		$description = $_POST['description'];
	    $id = $_POST['id'];
	    $gLAccount = $_POST['gLAccount'];
		
		$sql    = "UPDATE petty_cash SET payment_mode_id = '".$paymentMode."',reference= '".$reference."',staff_customer_id = '".$staffCustomerId."',staff_customer_name_id= '".$staffCustomerNameId."',staff_customer_name= '".$staffCustomerName."',ledger_id= '".$ledger."',amount= '".$amount."',description= '".$description."',type= '".$type."',updated_on = UNIX_TIMESTAMP() ,updated_by = '".$userId."', gl_account_id = '".$gLAccount."' WHERE id = '".$id."' ";

		$sqlUpdateFinanceBill = "UPDATE finance_transaction SET gl_account_id = '".$gLAccount."',ledger_id= '".$ledger."',amt_cr= '".$amount."',updated_on = UNIX_TIMESTAMP() ,updated_by = '".$userId."' WHERE type='petty' AND type_id = '".$id."' ";		
		$result = mysqli_multi_query($conn, $sql.';'.$sqlUpdateFinanceBill);
		echo $result;
	}

	if ($operation == "delete") {
        $id = $_POST['id'];

		echo $sql    = "UPDATE petty_cash SET status= 'I' where id = '" . $id . "'";
	    $result = mysqli_query($conn, $sql);
	    echo $result;	    
	}

	if ($operation == "restore") {// for restore    
        $id = $_POST['id'];
        $staffCustomerId = $_POST['staffCustomerId'];
        $staffCustomerNameId = $_POST['staffCustomerNameId'];

        /*$selLedger = "SELECT staff_customer_id,staff_customer_name_id FROM petty_cash WHERE staff_customer_id ='".$staffCustomerId."',staff_customer_name_id ='".$staffCustomerNameId."' AND status = 'A' AND id !='".$id."'";
			$checkLedger = mysqli_query($conn,$selLedger);
			$countLedger = mysqli_num_rows($checkLedger);

		if ($countLedger == "0") {*/

			$sql    = "UPDATE petty_cash SET status= 'A'  WHERE  id = '" . $id . "'";
			$result = mysqli_query($conn, $sql);
		    echo $result;
		    /*}
		else {
			echo "0";
		}*/
	}

	if ($operation == "saveCustomerLineItem") {
		$id = $_POST['id'];
		$updateLineItem = json_decode($_POST['updateLineItems']);
		$glLedger = '';
		$amount = '';
		$remarks = '';
		
		if(!empty($updateLineItem)){
			$insertQry =  "INSERT INTO petty_cash_line_item(petty_cash_id,gl_account_id,remarks,amount,created_on,created_by,updated_on,updated_by) VALUES ";

			$counter = 0;
			$subQuery = '';

			foreach ($updateLineItem as $value) {
				foreach ($value as $key => $values) {
				
					if ($key == 0 ) {
						$glLedger = $values;
						
					}
					if ($key == 1 ) {
						$amount = $values;
					}
					if ($key == 2 ) {
						$remarks = $values;
					}
				}
				if($counter > 0) {
					$subQuery.= ',';
				}

				$subQuery.= "('".$id."','".$glLedger."','".$remarks."','".$amount."',UNIX_TIMESTAMP(),'".$userId."',UNIX_TIMESTAMP(),'".$userId."')";

				$counter++;
			}
			$completePettyCashLineItem = $insertQry.$subQuery;
			$result = mysqli_query($conn,$completePettyCashLineItem);
			echo $result;
		}
	}


	if ($operation == "saveEmployeeLineItem") {
		$id = $_POST['id'];
		$updateLineItem = json_decode($_POST['updateLineItems']);
		$glLedger = '';
		$amount = '';
		$remarks = '';
		
		if(!empty($updateLineItem)){
			$insertQry =  "INSERT INTO petty_cash_line_item(petty_cash_id,gl_account_id,remarks,amount,created_on,created_by,updated_on,updated_by) VALUES ";

			$counter = 0;
			$subQuery = '';

			foreach ($updateLineItem as $value) {
				
					$glLedger = $value->glLedger;
				
					$amount = $value->amount;
				
					$remarks = $value->remarks;
				
				
				if($counter > 0) {
					$subQuery.= ',';
				}

				$subQuery.= "('".$id."','".$glLedger."','".$remarks."','".$amount."',UNIX_TIMESTAMP(),'".$userId."',UNIX_TIMESTAMP(),'".$userId."')";

				$counter++;
			}
			$completePettyCashLineItem = $insertQry.$subQuery;
			$result = mysqli_query($conn,$completePettyCashLineItem);
			echo $result;
		}
	}

	if ($operation == "getLineItems"){
		$id = $_POST['id'];

		$selAmt = "SELECT petty_cash_line_item.amount,petty_cash_line_item.remarks,chart_of_account.account_name  FROM petty_cash_line_item
			LEFT JOIN chart_of_account ON chart_of_account.id = petty_cash_line_item.gl_account_id 
			WHERE petty_cash_line_item.petty_cash_id = '".$id."'";

		$result = mysqli_query($conn, $selAmt);
	    
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
?>