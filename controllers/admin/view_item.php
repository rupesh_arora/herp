<?php
/*
	 * File Name    :   view_item.php
	 * Company Name :   Qexon Infotech
	 * Created By   :   Tushar Gupta
	 * Created Date :   29th dec, 2015
	 * Description  :   use for search item list
*/
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
$operation = "";

include 'config.php'; // impoert data base connection file

if (isset($_POST['operation'])) {  // opearion value come from js file to perform functioanlity. 
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

if ($operation == "showcategory") // show category in category selection box
    {
    $query  = "SELECT id,category FROM item_categories where status = 'A' ORDER BY category";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

// search item opearion
if ($operation == "search") {
    
    if (isset($_POST['category'])) {
        $category = $_POST["category"];
    }
    if (isset($_POST['code'])) {
        $code = $_POST["code"];
    }
    if (isset($_POST['name'])) {
        $name = $_POST["name"];
    }
	
	$isFirst = "false";
    
	$query = "SELECT items.*,item_categories.category AS categoryName ,received_orders.unit_cost 	from items 
			LEFT JOIN item_categories on items.item_category_id = item_categories.id
			LEFT JOIN received_orders ON received_orders.item_id = items.id";
	
	if ($category != '' || $code != '' || $name != '') {
		$query .= " WHERE ";
	}
	if ($category != '') {
		if ($isFirst != "false") {
			$query .= " AND ";
		}
		$query .= " items.item_category_id = '" . $category . "'";
		$isFirst = "true";
	}
	if ($code != '') {
		if ($isFirst != "false") {
			$query .= " AND ";
		}
		$query .= " items.code = '" . $code . "'";
		$isFirst = "true";
	}
	if ($name != '') {
		if ($isFirst != "false") {
			$query .= " AND ";
		}
		$query .= " items.name = '" . $name . "'";
		$isFirst = "true";
	} 
	if($isFirst == "true") {
		$query .= " group by items.id";
	}
	else {
		$query .= " group by items.id";
	}

    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
  
    print json_encode($rows);
}

// search stock item
if ($operation == "searchStockItem") {
    
    if (isset($_POST['category'])) {
        $category = $_POST["category"];
    }
    if (isset($_POST['code'])) {
        $code = $_POST["code"];
    }
    if (isset($_POST['name'])) {
        $name = $_POST["name"];
    }
	
	$isFirst = "false";
    
	$query = "select stocking.stocking_date as receive_date,stocking.expire_date,stocking.Quantity,stocking.Item AS id,items.name,items.item_category_id,
			items.code,items.description,item_categories.category AS categoryName,received_orders.unit_cost,item_price.price
			from stocking LEFT JOIN items ON items.id = stocking.Item
			LEFT JOIN item_categories ON items.item_category_id = item_categories.id
			LEFT JOIN received_orders ON items.id = received_orders.item_id
			LEFT JOIN item_price ON items.id = item_price.item_id";
	
	if ($category != '' || $code != '' || $name != '') {
		$query .= " WHERE ";
	}
	if ($category != '') {
		if ($isFirst != "false") {
			$query .= " AND ";
		}
		$query .= " items.item_category_id = '" . $category . "'";
		$isFirst = "true";
	}
	if ($code != '') {
		if ($isFirst != "false") {
			$query .= " AND ";
		}
		$query .= " items.code = '" . $code . "'";
		$isFirst = "true";
	}
	if ($name != '') {
		if ($isFirst != "false") {
			$query .= " AND ";
		}
		$query .= " items.name = '" . $name . "'";
		$isFirst = "true";
	} 
    if($isFirst == "true") {
		$query .= " group by items.id";
	}
	else {
		$query .= " group by items.id";
	}
	
    $result = mysqli_query($conn, $query);
   
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
  
    print json_encode($rows);
}

// search stock item
if ($operation == "searchReceiveItem") {
    
    if (isset($_POST['category'])) {
        $category = $_POST["category"];
    }
    if (isset($_POST['code'])) {
        $code = $_POST["code"];
    }
    if (isset($_POST['name'])) {
        $name = $_POST["name"];
    }
	
	$isFirst = "false";
    
	$query = "SELECT items.*,item_categories.category AS categoryName ,received_orders.unit_cost,received_orders.quantity,
			DATE_FORMAT(FROM_UNIXTIME(received_orders.mfg_date), '%Y-%m-%d') as receive_date,received_orders.exp_date	from received_orders 
			LEFT JOIN items ON items.id = received_orders.item_id
			LEFT JOIN item_categories ON items.item_category_id = item_categories.id";
	
	if ($category != '' || $code != '' || $name != '') {
		$query .= " WHERE ";
	}
	if ($category != '') {
		if ($isFirst != "false") {
			$query .= " AND ";
		}
		$query .= " items.item_category_id = '" . $category . "'";
		$isFirst = "true";
	}
	if ($code != '') {
		if ($isFirst != "false") {
			$query .= " AND ";
		}
		$query .= " items.code = '" . $code . "'";
		$isFirst = "true";
	}
	if ($name != '') {
		if ($isFirst != "false") {
			$query .= " AND ";
		}
		$query .= " items.name = '" . $name . "'";
		$isFirst = "true";
	} 
    if($isFirst == "true") {
		$query .= " group by items.id";
	}
	else {
		$query .= " group by received_orders.id";
	}
	/*echo $query;*/
    $result = mysqli_query($conn, $query);
   
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
  
    print json_encode($rows);
}
?>