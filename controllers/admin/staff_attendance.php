<?php
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
} else {
    exit();
}
include 'config.php';

if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

if ($operation == "showAttendance") {
    $staffId  = $_POST['staffId'];
    $fromDate = $_POST["fromDate"];
    $toDate   = $_POST["toDate"];
    $staffTypeId = $_POST['staffTypeId'];
    $attendanceType = $_POST['attendanceType'];

    if ($attendanceType == 'present') {

        $sql = "SELECT staff_attendance.staff_id,CONCAT(users.first_name,' ',users.last_name) AS name,staff_attendance.`date`,DATE_FORMAT(FROM_UNIXTIME(punch_in), '%Y-%m-%d %h:%i %p') AS punch_in,
        (CASE WHEN punch_out = 0 THEN '' ELSE DATE_FORMAT(FROM_UNIXTIME(punch_out), '%Y-%m-%d %h:%i %p') 
        END) AS punch_out ,staff_attendance.remarks,users.joining_date  AS created_on FROM staff_attendance 
        LEFT JOIN users ON users.id=staff_attendance.staff_id
        WHERE users.staff_type_id = '".$staffTypeId."' ";

        if ($staffId != '') {
            $sql .= "AND users.id = '".$staffId."'";
        }
        
        if ($fromDate != '' AND $toDate != '') {
            $sql .= " AND `date` BETWEEN ('" . $fromDate . " 00:00:00') AND ('" . $toDate . " 23:59:59') ";
        }
        else if ($fromDate != '' AND $toDate == '') {
            $sql .= " AND `date` BETWEEN ('" . $fromDate . " 00:00:00') AND CURDATE() ";
        }
        else if ($fromDate == '' AND $toDate != '') {
            $sql .= " AND `date` BETWEEN '0000000000' AND ('" . $toDate . " 23:59:59') ";
        }
        else{
            $sql .=" AND `date`>= (curdate() - interval 3 month)" ;
        }    

        //echo $sql;
        $result = mysqli_query($conn,$sql);
        $rows = array();
        while ($r = mysqli_fetch_assoc($result)) {
            $rows[] = $r;
        }
        print json_encode($rows);
    }
    else{
        $sqlAbsent = "SELECT users.id,CONCAT(users.first_name,' ',users.last_name) AS name 
        FROM users WHERE users.staff_type_id = '".$staffTypeId."' AND 
        users.id NOT IN(SELECT staff_id from staff_attendance 
        WHERE `date`";
    
        if ($fromDate != '' AND $toDate != '') {
            $sqlAbsent .= "BETWEEN ('" . $fromDate . " 00:00:00') AND ('" . $toDate . " 23:59:59')) ";
        }
        else if ($fromDate != '' AND $toDate == '') {
            $sqlAbsent .= "BETWEEN ('" . $fromDate . " 00:00:00') AND CURDATE()) ";
        }
        else if ($fromDate == '' AND $toDate != '') {
            $sqlAbsent .= "BETWEEN '0000000000' AND ('" . $toDate . " 23:59:59')) ";
        }
        else{
            $sqlAbsent .=">= (curdate() - interval 3 month))" ;
        }  
        if ($staffId != '') {
            $sqlAbsent .= "AND users.id = '".$staffId."'";
        }
        //echo $sqlAbsent;
        $result = mysqli_query($conn,$sqlAbsent);
        $rows = array();
        while ($r = mysqli_fetch_assoc($result)) {
            $rows[] = $r;
        }
        print json_encode($rows);
    }
     
}

if ($operation == "showAbsent") {
    
    $sql = "SELECT users.id,CONCAT(users.first_name,' ',users.last_name) AS staff_name FROM users WHERE users.id NOT IN(SELECT staff_id from staff_attendance WHERE date = curdate())";
    
    $result = mysqli_query($conn, $sql);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "showstaffid") { // show staff id/name
    $staffType = $_POST['staffType'];
    $query     = "SELECT users.id,CONCAT(users.first_name,' ',users.last_name) AS name FROM users LEFT JOIN 	staff_type ON staff_type.id=users.staff_type_id WHERE staff_type.id= '" . $staffType . "'";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

?>