<?php 
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "showOrderDetails") { // show order details

		$orderId = $_POST['orderId'];

	$query = "SELECT received_orders.id,items.name AS item_name,received_orders.unit_cost,received_orders.quantity,received_orders.gl_account,received_orders.account_payable,received_orders.ledger FROM received_orders LEFT JOIN items ON received_orders.item_id=items.id WHERE order_id= '".$orderId."' 
		AND received_orders.pay_status='unpaid'";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "save") {
	    if (isset($_POST['sellerId'])) {
	        $sellerId = $_POST["sellerId"];
	    }
	    if (isset($_POST['orderId'])) {
	        $orderId = $_POST["orderId"];
	    }
	    if (isset($_POST['paymentMode'])) {
	        $paymentMode = $_POST["paymentMode"];
	    }
	    if (isset($_POST['amount'])) {
	        $amount = $_POST["amount"];
	    }
	    if (isset($_POST['receivedItem'])) {
	        $receivedItem = $_POST["receivedItem"];
	    }
	    $arr = $_POST['arr'];


	    $sql = "INSERT INTO inventory_payment(seller_id,order_id,total_amount,`date`,created_on,created_by,payment_mode,status) VALUES ('".$sellerId."','".$orderId."','".$amount."',CURDATE(),UNIX_TIMESTAMP(),'".$userId."','".$paymentMode."','A')";
	    mysqli_query($conn, $sql);

	    $last_id = mysqli_insert_id($conn);

	    $updateQry = "UPDATE received_orders SET pay_status = 'paid' WHERE ";
	    $sqlInsertFinanceBill = "INSERT INTO finance_transaction(gl_account_id,ledger_id,ap_id,amt_cr,type,type_id,created_on,updated_on,created_by,updated_by) VALUES ";
	    $subUpdateQry = '';
	    $subInsertFinanceBill = '';
	    $counter = 0;
	    $pkId = '';
	    $ledgerId = '';
	    $accountPayable = '';
	    $glAccountId = '';
	    $itemAmount = '';
	    foreach ($arr as $value) {
	    	
	    	$pkId = $value['id'];
	    	$accountPayable = $value['accPayable'];
	    	$glAccountId = $value['glAccount'];
	    	$ledgerId = $value['ledger'];
	    	$itemAmount = $value['itemAmount'];

	    	if ($counter == 0) {
				$subUpdateQry.= "id = '".$pkId."'";
			}
			else {
				$subUpdateQry.= " OR id = '".$pkId."'";
				$subInsertFinanceBill.=',';
			}			

			$subInsertFinanceBill.= "('".$glAccountId."','".$ledgerId."','".$accountPayable."','".$itemAmount."','inventory','".$last_id."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";

			$counter++;
	    }
	    $completeUpdateQry = $updateQry.$subUpdateQry;	   
	    
	    $combinedQry =  $completeUpdateQry.';'.$sqlInsertFinanceBill.$subInsertFinanceBill;	    
	    $result = mysqli_multi_query($conn, $combinedQry);
    	echo $result;
	}

	//This operation is used for select last receipt number 
if ($operation == "showseller") {
    
    $query      = "SELECT id,name FROM seller WHERE status = 'A'";
    $result     = mysqli_query($conn, $query);
    $rows_count = mysqli_num_rows($result);
    $rows   = array();
	while ($r = mysqli_fetch_assoc($result)) {
		$rows[] = $r;
	}
	print json_encode($rows);
}

?>