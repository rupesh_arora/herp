<?php
/*File Name  :   month_wise_collection_comparision.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   Apr 20, 2016
Description  :   This page manages  all the lab test request*/

session_start();

/*include config file*/
include 'config.php';

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
}
/*Get user id from session table*/
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
	exit();
}
if ($operation == "showChartData") {

	$year = $_POST['year'];
	$sqlSelect =  "SELECT 
		IFNULL(SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 1) THEN debit ELSE '0' END),'') AS Jan,
		IFNULL(SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 2) THEN debit ELSE '0' END),'') AS Feb,  
		IFNULL(SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 3) THEN debit ELSE '0' END),'') AS Mar,  
		IFNULL(SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 4) THEN debit ELSE '0' END),'') AS Apr,  
		IFNULL(SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 5) THEN debit ELSE '0' END),'') AS May,  
		IFNULL(SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 6) THEN debit ELSE '0' END),'') AS Jun,  
		IFNULL(SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 7) THEN debit ELSE '0' END),'') AS Jul,  
		IFNULL(SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 8) THEN debit ELSE '0' END),'') AS Aug,  
		IFNULL(SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 9) THEN debit ELSE '0' END),'') AS Sept, 
		IFNULL(SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 10) THEN debit ELSE '0' END),'') AS Oct, 
		IFNULL(SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 11) THEN debit ELSE '0' END),'') AS Nov, 
		IFNULL(SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 12) THEN debit ELSE '0' END),'') AS `Dec`
		FROM cash_account                                            
		WHERE YEAR(FROM_UNIXTIME(created_on)) =  '".$year."'";

	$result = mysqli_query($conn,$sqlSelect);
	
	$rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
?>