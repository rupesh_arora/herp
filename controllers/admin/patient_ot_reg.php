<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	$headers = '';
	include 'config.php';
	include '../AfricasTalkingGateway.php';
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}
	if ($operation == "showSurgery") { // show insurance company type
	     $query = "SELECT id,name FROM surgery where status = 'A' ";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	/*if ($operation == "serachStaff") {
		
		if (isset($_POST['searchSurgeon'])) {
			# code...
			$sql = "SELECT id,CONCAT(first_name,' ',last_name) AS name FROM users WHERE staff_type_id = '13'";
		}
		if (isset($_POST['serach'])) {
			# code...
		}
	}*/

	if ($operation == "showSurgeon") { // show insurance company type
	    $query = "SELECT users.id,concat(users.first_name,' ',users.last_name) AS name FROM users 
			LEFT JOIN staff_type ON  staff_type.id = users.staff_type_id 
			where users.`status` = 'A' AND staff_type.`type` = 'Surgeon' ";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	if ($operation == "showAnesthesian") { // show insurance company type
	    $query = "SELECT users.id,concat(users.first_name,' ',users.last_name) AS name FROM users 
			LEFT JOIN staff_type ON  staff_type.id = users.staff_type_id 
			where users.`status` = 'A' AND staff_type.`type` = 'Anesthesian' ";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "showNurse") { 
	    $query = "SELECT users.id,concat(users.first_name,' ',users.last_name) AS name FROM users 
			LEFT JOIN staff_type ON  staff_type.id = users.staff_type_id 
			where users.`status` = 'A' AND staff_type.`type` = 'Nurse' ";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "showOtRooms") { // show insurance company type
	    $query = "SELECT id,room_no FROM ot_rooms where status = 'A' ";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "savePatientOtReg") {
		$patientId = $_POST['patientId'];
		$operationDate = $_POST['operationDate'];
		$surgeryType = $_POST['surgeryType'];
		$surgery = $_POST['surgery'];
		$otRoom = $_POST['otRoom'];
		$fromTime = $_POST['fromTime'];
		$toTime = $_POST['toTime'];
		$hourDiff = $_POST['hourDiff'];
		$arrNurse = json_decode($_POST['arrNurse']);
		$arrSurgeon = json_decode($_POST['arrSurgeon']);
		$arrAnesthesian = json_decode($_POST['arrAnesthesian']);

		$staffId = (array_merge($arrNurse,$arrSurgeon,$arrAnesthesian));//merge all 3 array
		$changeFromTime =  substr_replace($fromTime ,"1",-1);
		$changeToTime = date("H:i:s", strtotime($toTime) - 1);

		$surgeonLength = sizeof($arrSurgeon);
		$nurseLength = sizeof($arrNurse);
		$anesthesianLength = sizeof($arrAnesthesian);

		$subCheckSurgeon = '';		
		$subCheckAnsthesian = '';
		$subCheckNurse = '';
		$subSurgeonHoliday  = '';
		$subHolidayAnsthesian   = '';
		$subHolidayNurse   = '';


		//check that patinet id is exist or not
		$checkPatient = "SELECT id FROM patients WHERE id = '".$patientId."'";
		$resultCheck = mysqli_query($conn,$checkPatient);
		$countRows = mysqli_num_rows($resultCheck);
		if($countRows > 0){

			//making common for query for multiple nurse, anasthesian and doctor
			$countSurgeon = 0;			
			foreach ($arrSurgeon as $value) {
				if ($countSurgeon == 0) {
					$subCheckSurgeon.= "(";//for booking
					$subSurgeonHoliday.= '(';//for leave
				}
				if ($countSurgeon > 0) {
					$subCheckSurgeon.= " OR ";//for booking
					$subSurgeonHoliday.= " OR ";//for leave
				}

				$subCheckSurgeon.="ot_staff_details.ot_staff_id='".$value."'";//for booking
				$subSurgeonHoliday.="staff_id='".$value."'";//for leave

				if ($countSurgeon == $surgeonLength - 1) {
                	$subCheckSurgeon .= ")";//for booking
                	$subSurgeonHoliday .= ")";//for leave
                }
                $countSurgeon++;
            }


            $countAnsthesian = 0;
			foreach ($arrAnesthesian as $valueAns) {
				if ($countAnsthesian == 0) {
					$subCheckAnsthesian.= "(";//for booking
					$subHolidayAnsthesian.= "(";//for leave
				}
				if ($countAnsthesian > 0) {
					$subCheckAnsthesian.= " OR ";//for booking
					$subHolidayAnsthesian.= " OR ";//for leave
				}

				$subCheckAnsthesian.="ot_staff_details.ot_staff_id='".$valueAns."'";//for booking
				$subHolidayAnsthesian.="staff_id='".$valueAns."'";//for leave

				if ($countAnsthesian == $anesthesianLength - 1) {
                	$subCheckAnsthesian .= ")";//for booking
                	$subHolidayAnsthesian .= ")";//for leave
                }
                $countAnsthesian++;
            }

            $countNurse = 0;
			foreach ($arrNurse as $value) {
				if ($countNurse == 0) {
					$subCheckNurse.= "(";
					$subHolidayNurse.= "(";
				}
				if ($countNurse > 0) {
					$subCheckNurse.= " OR ";
					$subHolidayNurse.= " OR ";
				}

				$subCheckNurse.="ot_staff_details.ot_staff_id='".$value."'";
				$subHolidayNurse.="staff_id='".$value."'";

				if ($countNurse == $nurseLength - 1) {
                	$subCheckNurse .= ")";
                	$subHolidayNurse .= ")";
                }
                $countNurse++;
            }

			//check that ot room already booked or not
			$checkRoom =  "SELECT ot_room_availability.ot_room_id FROM ot_room_availability
				LEFT JOIN patient_ot_registration ON patient_ot_registration.id = ot_room_availability.
				patient_operation_id
				WHERE ('".$changeFromTime."'  BETWEEN ot_room_availability.from_time AND
				ot_room_availability.`to_time` OR '".$toTime."' BETWEEN ot_room_availability.from_time AND
				ot_room_availability.`to_time`) 
				AND patient_ot_registration.operation_date ='".$operationDate."' 
				AND  ot_room_availability.ot_room_id = '".$otRoom."' AND ot_room_availability.`status` =
				'A' AND patient_ot_registration.`status` = 'A'";

			$resultCheckRoom = mysqli_query($conn,$checkRoom);
			$countRoom = mysqli_num_rows($resultCheckRoom);

			//if booked then can't save data
			if($countRoom > 0){
				echo "This room for given time slot is already booked.";
				exit();
			}
			else{

				//check that surgeon booked or not
				$checkSurgeon = "SELECT ot_staff_details.ot_staff_id FROM patient_ot_registration
				LEFT JOIN ot_room_availability ON patient_ot_registration.id = ot_room_availability.
				patient_operation_id
				LEFT JOIN ot_staff_details ON ot_staff_details.patient_operation_id = 
				patient_ot_registration.id
				WHERE ('".$changeFromTime."'  BETWEEN ot_room_availability.from_time AND
				ot_room_availability.`to_time` OR '".$toTime."' BETWEEN ot_room_availability.from_time AND
				ot_room_availability.`to_time`)  
				AND ot_room_availability.`status` = 'A' 
				AND patient_ot_registration.`status` = 'A' AND patient_ot_registration.operation_date ='".$operationDate."' AND ot_staff_details.status = 'A' AND ";				

                $checkSurgeon = $checkSurgeon.$subCheckSurgeon;//sub check coming from upside query
				$resultCheckSurgeon = mysqli_query($conn,$checkSurgeon);
				$countSurgeon = mysqli_num_rows($resultCheckSurgeon);
				if($countSurgeon > 0){
					echo "This surgeon for given time slot is already booked.";
					exit();
				}
				else{

					//check Ansthesian is booked or not
					$checkAnsthesian = "SELECT ot_staff_details.ot_staff_id FROM
					patient_ot_registration
					LEFT JOIN ot_room_availability ON patient_ot_registration.id = ot_room_availability.
					patient_operation_id
					LEFT JOIN ot_staff_details ON ot_staff_details.patient_operation_id = patient_ot_registration.id
					WHERE ('".$changeFromTime."'  BETWEEN ot_room_availability.from_time AND
					ot_room_availability.`to_time` OR '".$toTime."' BETWEEN ot_room_availability.from_time
					AND	ot_room_availability.`to_time`)  
					AND ot_room_availability.`status` = 'A' 
					AND patient_ot_registration.`status` = 'A' AND patient_ot_registration.operation_date ='".$operationDate."' AND ot_staff_details.status = 'A' AND  ";
					
	                $checkAnsthesian = $checkAnsthesian.$subCheckAnsthesian;	                
					$resultCheckAnsthesian = mysqli_query($conn,$checkAnsthesian);
					$countAnsthesian = mysqli_num_rows($resultCheckAnsthesian);			
					if($countAnsthesian > 0){
						echo "This ansthesian for given time slot is already booked.";
						exit();
					}


					//check that nurse is booked or not

					$checkNurse = "SELECT ot_staff_details.ot_staff_id FROM
					patient_ot_registration
					LEFT JOIN ot_room_availability ON patient_ot_registration.id = ot_room_availability.
					patient_operation_id
					LEFT JOIN ot_staff_details ON ot_staff_details.patient_operation_id = patient_ot_registration.id
					WHERE ('".$changeFromTime."'  BETWEEN ot_room_availability.from_time AND
					ot_room_availability.`to_time` OR '".$toTime."' BETWEEN ot_room_availability.from_time
					AND	ot_room_availability.`to_time`)  
					AND ot_room_availability.`status` = 'A' 
					AND patient_ot_registration.`status` = 'A' AND patient_ot_registration.operation_date ='".$operationDate."' AND ot_staff_details.status = 'A' AND  ";
					
	                $checkNurse = $checkNurse.$subCheckNurse;	                
					$resultCheckNurse = mysqli_query($conn,$checkNurse);
					$countNurse = mysqli_num_rows($resultCheckNurse);			
					if($countNurse > 0){
						echo "This nurse for given time slot is already booked.";
						exit();
					}

					//check that ansthesian is on leave or not

					$checkAnsthesianLeave = "SELECT staff_id from staff_leaves 
					WHERE DATE_FORMAT(FROM_UNIXTIME(leave_date), '%Y-%m-%d') = '".$operationDate."'
					AND (leave_status !='not_taken' OR 'cancel') AND ";

	                $checkAnsthesianLeave = $checkAnsthesianLeave.$subHolidayAnsthesian;
					$resultCheckAnsthesianLeave = mysqli_query($conn,$checkAnsthesianLeave);
					$countnsthesianLeave = mysqli_num_rows($resultCheckAnsthesianLeave);
					if($countnsthesianLeave > 0){
						echo "Ansthesian is on leave.";
						exit();
					}

					//check that surgeon is on leave or not
					$checkSurgeonLeave = "SELECT staff_id from staff_leaves 
					WHERE DATE_FORMAT(FROM_UNIXTIME(leave_date), '%Y-%m-%d') = '".$operationDate."'
					AND (leave_status !='not_taken' OR 'cancel') AND ";

	                $checkSurgeonLeave = $checkSurgeonLeave.$subSurgeonHoliday;
					$resultCheckLeave = mysqli_query($conn,$checkSurgeonLeave);
					$countLeave = mysqli_num_rows($resultCheckLeave);
					if($countLeave > 0){
						echo "Surgeon is on leave.";
						exit();
					}


					//check that nurse is on leave or not
					$checkNurseLeave = "SELECT staff_id from staff_leaves 
					WHERE DATE_FORMAT(FROM_UNIXTIME(leave_date), '%Y-%m-%d') = '".$operationDate."'
					AND (leave_status !='not_taken' OR 'cancel') AND ";

	                $checkNurseLeave = $checkNurseLeave.$subHolidayNurse;
					$resultCheckLeave = mysqli_query($conn,$checkNurseLeave);
					$countLeave = mysqli_num_rows($resultCheckLeave);
					if($countLeave > 0){
						echo "Nurse is on leave.";
						exit();
					}
					else{

						//check patient already booked or not

						$checkPatient = "SELECT patient_ot_registration.patient_id FROM patient_ot_registration
							LEFT JOIN ot_room_availability ON patient_ot_registration.id = 
							ot_room_availability.patient_operation_id
							WHERE ('".$changeFromTime."'  BETWEEN ot_room_availability.from_time AND
							ot_room_availability.`to_time` OR '".$toTime."' BETWEEN ot_room_availability.
							from_time AND	ot_room_availability.`to_time`) AND patient_ot_registration.
							operation_date ='".$operationDate."' AND patient_ot_registration.patient_id	='".$patientId."'";

						$resultCheckPatient = mysqli_query($conn,$checkPatient);
						$countBookPatient = mysqli_num_rows($resultCheckPatient);
						if ($countBookPatient > 0) {
							echo "Patient alredy booked for this given time";
							exit();
						}
						else{

							$query = "INSERT INTO patient_ot_registration(patient_id,operation_date,surgery_id,surgery_type,ot_room_id,duration,created_on,updated_on,created_by,updated_by,status) VALUES('".$patientId."','".$operationDate."','".$surgery."','".$surgeryType."','".$otRoom."','".$hourDiff."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."','A')";
						    
						    $result = mysqli_query($conn, $query);
						    $last_id = mysqli_insert_id($conn);

						    if($result == '1'){
						    	$selQuery = "select concat(p.first_name,' ',p.last_name) as patient_name,p.mobile,p.country_code,
						    	se.patient_ot_reg_sms,por.operation_date,DATE_FORMAT(FROM_UNIXTIME(por.created_on), '%Y-%m-%d') AS por_date 
						    	from sms_email as se,patient_ot_registration as por 
						    	left join patients as p on p.id=por.patient_id where por.id='" . $last_id . "'";
						    	$resultCheck = mysqli_query($conn,$selQuery);
								$totalrecords = mysqli_num_rows($resultCheck);
								$rows = array();
								$patient_name;
								$operation_date;
								$mobile;
								$patient_ot_reg_sms;
								$date;
								while ($r = mysqli_fetch_assoc($resultCheck)) {
									$rows = $r;
									//print_r($rows);
									$patient_name = $rows['patient_name'];
									$operation_date = $rows['operation_date'];
									$mobile = $rows['mobile'];
									$patient_ot_reg_sms = $rows['patient_ot_reg_sms'];
									$date = $rows['por_date'];
									$country_code = $rows['country_code'];
								}
								
								$previousMessage  = $patient_ot_reg_sms;
								$replace = array("@@user_name@@","@@date@@");
								$replaced   = array($patient_name,$operation_date);
								$message = str_replace($replace, $replaced, $previousMessage);
								//print_r($message);

								// sms to patient
								$recipients = $country_code.$mobile;
								// And of course we want our recipients to know what we really do
								$message    = $message;
								// Create a new instance of our awesome gateway class
								$gateway    = new AfricasTalkingGateway($username, $apikey);
								// Any gateway error will be captured by our custom Exception class below, 
								// so wrap the call in a try-catch block
								try 
								{ 
								  // Thats it, hit send and we'll take care of the rest. 
								  $results = $gateway->sendMessage($recipients, $message);
								  if ($results) {
								  	$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
				                            created_on,updated_on,created_by,updated_by,msg_status) VALUES('Patient OT Registration',
				                            '".$recipients."','','".$message."','','".$date."',UNIX_TIMESTAMP(),
				                            UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS')";
					                $result = mysqli_query($conn, $sqlQuery);
								  }
								  
								            
								  foreach($results as $result) {
								    // status is either "Success" or "error message"
								    //echo " Number: " .$result->number;
								    //echo " Status: " .$result->status;
								    //echo " MessageId: " .$result->messageId;
								    //echo " Cost: "   .$result->cost."\n";
								  }
								}
								catch ( AfricasTalkingGatewayException $e )
								{
									$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
				                            created_on,updated_on,created_by,updated_by,msg_status,status) VALUES('Patient OT Registration',
				                            '".$recipients."','','".$message."','','".$date."',UNIX_TIMESTAMP(),
				                            UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS','pending')";
					                $result = mysqli_query($conn, $sqlQuery);
								  //echo "Encountered an error while sending: ".$e->getMessage();
								}
								// DONE!!! 
						   	}

						    //query to select room number to save in notification

						    $selRoom = "SELECT room_no FROM ot_rooms WHERE id ='".$otRoom."'";
						    $resultRoom = mysqli_query($conn,$selRoom);
						    while ($row = mysqli_fetch_assoc($resultRoom)) {
						    	$roomNumber  = $row['room_no'];
						    }

						    if ($result) {
						   	
							   	$sqlInsert = "INSERT INTO ot_room_availability(ot_room_id,
							   	patient_operation_id,from_time,to_time,created_on,created_by,updated_on,
							   	updated_by) VALUES('".$otRoom."','".$last_id."','".$fromTime."','".$toTime.
							   	"',UNIX_TIMESTAMP(),'".$userId."',UNIX_TIMESTAMP(),'".$userId."')";
							   	

							   	$insertStaff = "INSERT INTO ot_staff_details(ot_staff_id,
							   	patient_operation_id,created_on,created_by,updated_on,updated_by) VALUES ";


							   	//query to insert notification

							   	$notifyQry = "INSERT INTO notification(notification_staff_id,message,
							   		created_on,created_by) VALUES ";

							   	$subQuery = '';
							   	$subNotifyQry = '';
							   	$countQry = '';
							   	foreach ($staffId as $value) {
							   		if ($countQry > 0) {
							   			$subQuery.=',';
							   			$subNotifyQry.=',';
							   		}
							   		$subQuery.= "('".$value."','".$last_id."',UNIX_TIMESTAMP(),
							   		'".$userId."',UNIX_TIMESTAMP(),'".$userId."')";

							   		$subNotifyQry.="('".$value."','An OT is booked for you on 
							   		".$operationDate." from ".$fromTime." to ".$toTime." at room number :".$roomNumber."',UNIX_TIMESTAMP(),'".$userId."')";	
							   		$countQry++;
							   	}
							   	$insertStaff = $insertStaff.$subQuery.";";
							   	$completeQry = $sqlInsert.';'.$insertStaff.$notifyQry.$subNotifyQry;
							   	$resultSubQuery = mysqli_multi_query($conn,$completeQry);

							   	echo $resultSubQuery;

							   	if($resultSubQuery > '0'){
							   		$staff_name = '';
							   		if (!(mysqli_ping($conn))) {
					                    include 'config.php';
					                }
							    	$query = "SELECT por.id,por.operation_date,por.surgery_type,ot.room_no,se.ot_booking_sms,se.ot_booking_email FROM sms_email AS se,patient_ot_registration AS por LEFT JOIN ot_rooms AS ot ON ot.id = por.ot_room_id WHERE por.id='" . $last_id . "'";
							    	$rslt = mysqli_query($conn,$query);
									$totalrecords = mysqli_num_rows($rslt);
									//print_r($rslt);
									$rowsOt = array();
									
									while ($rOt = mysqli_fetch_assoc($rslt)) {
										$rowsOt = $rOt;
										//print_r($rowsOt);
										$operation_date = $rowsOt['operation_date'];
										$surgery_type = $rowsOt['surgery_type'];
										$room_no = $rowsOt['room_no'];
										$ot_booking_sms = $rowsOt['ot_booking_sms'];
										$ot_booking_email = $rowsOt['ot_booking_email'];
										$id = $rowsOt['id'];
									}

									if($id != ''){
										$sqlQuery = "SELECT por.id,CONCAT(u.first_name,' ',u.last_name) AS staff_name,u.email_id,u.mobile,u.country_code,
										DATE_FORMAT(FROM_UNIXTIME(por.created_on), '%Y-%m-%d') AS por_date FROM ot_staff_details AS osd 
										LEFT JOIN patient_ot_registration AS por ON por.id = osd.patient_operation_id 
										LEFT JOIN users AS u ON u.id = osd.ot_staff_id WHERE osd.patient_operation_id = '" . $id . "'";
										$rsltCheck = mysqli_query($conn,$sqlQuery);
										$totalrecords = mysqli_num_rows($rsltCheck);
										$staff_name;
										$email_id;
										$mobile;
										$date;
										$rowsCheck = array();
										while ($r = mysqli_fetch_assoc($rsltCheck)) {
											$staff_name = $r['staff_name'];
											$email_id = $r['email_id'];
											$date = $r['por_date'];			
											$mobile = $r['mobile'];	
											$country_code = $r['country_code'];	
											
											$previousEmailMessage  = $ot_booking_email;
											$replaceEmail = array("@@staff_name@@", "@@operation_type@@", "@@ot_theater@@","@@date@@");
											$replacedEmail   = array($staff_name, $surgery_type, $room_no, $operation_date);
											$EmailMessage = str_replace($replaceEmail, $replacedEmail, $previousEmailMessage);

											//print_r($EmailMessage);

											if ($email_id) {
												mail($email_id, " OT Book for ".$staff_name."", $EmailMessage, $headers);
												$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
					                                        created_on,updated_on,created_by,updated_by,msg_status) VALUES('Patient OT Registration','',
					                                        '".$email_id."','','".$EmailMessage."','".$date."',UNIX_TIMESTAMP(),
					                                        UNIX_TIMESTAMP(),'".$userId."','".$userId."','Mail')";

					                            $result = mysqli_query($conn, $sqlQuery);
											}
											

											$previousSmsMessage  = $ot_booking_sms;
											$replaceSms = array("@@staff_name@@", "@@operation_type@@", "@@ot_theater@@"," @@date@@");
											$replacedSms   = array($staff_name, $surgery_type, $room_no, $operation_date);
											$SmsMessage = str_replace($replaceSms, $replacedSms, $previousSmsMessage);
											//print_r($SmsMessage);

											// Please ensure you include the country code (+254 for Kenya in this case)
											$recipients = $country_code.$mobile;
											// And of course we want our recipients to know what we really do
											$message    = $SmsMessage;
											// Create a new instance of our awesome gateway class
											$gateway    = new AfricasTalkingGateway($username, $apikey);
											// Any gateway error will be captured by our custom Exception class below, 
											// so wrap the call in a try-catch block
											try 
											{ 
											  // Thats it, hit send and we'll take care of the rest. 
											  $results = $gateway->sendMessage($recipients, $message);
											  if ($results) {
											  	$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
							                            created_on,updated_on,created_by,updated_by,msg_status) VALUES('Patient OT Registration',
							                            '".$recipients."','','".$message."','','".$date."',UNIX_TIMESTAMP(),
							                            UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS')";
								                $result = mysqli_query($conn, $sqlQuery);
											  }
											            
											  foreach($results as $result) {
											    // status is either "Success" or "error message"
											    //echo " Number: " .$result->number;
											    //echo " Status: " .$result->status;
											    //echo " MessageId: " .$result->messageId;
											    //echo " Cost: "   .$result->cost."\n";
											  }
											}
											catch ( AfricasTalkingGatewayException $e )
											{
											  //echo "Encountered an error while sending: ".$e->getMessage();
												$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
							                            created_on,updated_on,created_by,updated_by,msg_status,status) VALUES('Patient OT Registration',
							                            '".$recipients."','','".$message."','','".$date."',UNIX_TIMESTAMP(),
							                            UNIX_TIMESTAMP(),'".$userId."','".$userId."','SMS','pending')";
								                $result = mysqli_query($conn, $sqlQuery);
											}
											// DONE!!! 
										}
										//print_r($rowsCheck);

										//exit();
									}									

									

							   	}
						    }
						    else{
						    	echo "0";
						    }
						}
					}
				}					
			}				
		}
		else{
			echo "Patient not exist.";
		}		 	
	}

	if ($operation == "update") {// update data
	    $description = "";
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    if (isset($_POST['patientId'])) {
	        $patientId = $_POST['patientId'];
	    }
	    if (isset($_POST['operationDate'])) {
	        $operationDate = $_POST['operationDate'];
	    }
	    if (isset($_POST['surgery'])) {
	        $surgery = $_POST['surgery'];
	    }
	    if (isset($_POST['surgeryType'])) {
	        $surgeryType = $_POST['surgeryType'];
	    }	
	    if (isset($_POST['surgeon'])) {
	        $surgeon = $_POST['surgeon'];
	    }
	    if (isset($_POST['anesthesian'])) {
	        $anesthesian = $_POST['anesthesian'];
	    }
	    if (isset($_POST['otRoom'])) {
	        $otRoom = $_POST['otRoom'];
	    }
	    if (isset($_POST['hourDiff'])) {
	        $hourDiff = $_POST['hourDiff'];
	    }
	    $fromTime = $_POST['fromTime'];
		$toTime = $_POST['toTime'];
	    $id = $_POST['id'];
	    $changeFromTime =  substr_replace($fromTime ,"1",-1);
		$changeToTime = date("H:i:s", strtotime($toTime) - 1);
		
		$arrNurse = json_decode($_POST['arrNurse']);
		$arrSurgeon = json_decode($_POST['arrSurgeon']);
		$arrAnesthesian = json_decode($_POST['arrAnesthesian']);

		$staffId = (array_merge($arrNurse,$arrSurgeon,$arrAnesthesian));//merge all 3 array
		
		$surgeonLength = sizeof($arrSurgeon);
		$nurseLength = sizeof($arrNurse);
		$anesthesianLength = sizeof($arrAnesthesian);
		
		$subCheckSurgeon = '';		
		$subCheckAnsthesian = '';
		$subCheckNurse = '';
		$subSurgeonHoliday  = '';
		$subHolidayAnsthesian   = '';
		$subHolidayNurse   = '';


		$checkPatient = "SELECT id FROM patients WHERE id = '".$patientId."'";
		$resultCheck = mysqli_query($conn,$checkPatient);
		$countRows = mysqli_num_rows($resultCheck);
		if($countRows > 0){	    	
				
			//check that ot room already booked or not
			$checkRoom =  "SELECT ot_room_availability.ot_room_id FROM ot_room_availability
				LEFT JOIN patient_ot_registration ON patient_ot_registration.id = ot_room_availability.
				patient_operation_id
				WHERE ('".$changeFromTime."'  BETWEEN ot_room_availability.from_time AND
				ot_room_availability.`to_time` OR '".$changeToTime."' BETWEEN ot_room_availability.
				from_time AND	ot_room_availability.`to_time`) 
				AND patient_ot_registration.operation_date ='".$operationDate."' 
				AND  ot_room_availability.ot_room_id = '".$otRoom."' AND ot_room_availability.`status` =
				'A' AND patient_ot_registration.`status` = 'A' AND patient_ot_registration.id !='".$id."'";

			$resultCheckRoom = mysqli_query($conn,$checkRoom);
			$countRoom = mysqli_num_rows($resultCheckRoom);

			//if booked then can't save data
			if($countRoom > 0){
				echo "This room for given time slot is already booked.";
				exit();
			}
			else{

				//making common for query for multiple nurse, anasthesian and doctor
				$countSurgeon = 0;			
				foreach ($arrSurgeon as $value) {
					if ($countSurgeon == 0) {
						$subCheckSurgeon.= "(";//for booking
						$subSurgeonHoliday.= '(';//for leave
					}
					if ($countSurgeon > 0) {
						$subCheckSurgeon.= " OR ";//for booking
						$subSurgeonHoliday.= " OR ";//for leave
					}

					$subCheckSurgeon.="ot_staff_details.ot_staff_id='".$value."'";//for booking
					$subSurgeonHoliday.="staff_id='".$value."'";//for leave

					if ($countSurgeon == $surgeonLength - 1) {
	                	$subCheckSurgeon .= ")";//for booking
	                	$subSurgeonHoliday .= ")";//for leave
	                }
	                $countSurgeon++;
	            }


	            $countAnsthesian = 0;
				foreach ($arrAnesthesian as $valueAns) {
					if ($countAnsthesian == 0) {
						$subCheckAnsthesian.= "(";//for booking
						$subHolidayAnsthesian.= "(";//for leave
					}
					if ($countAnsthesian > 0) {
						$subCheckAnsthesian.= " OR ";//for booking
						$subHolidayAnsthesian.= " OR ";//for leave
					}

					$subCheckAnsthesian.="ot_staff_details.ot_staff_id='".$valueAns."'";//for booking
					$subHolidayAnsthesian.="staff_id='".$valueAns."'";//for leave

					if ($countAnsthesian == $anesthesianLength - 1) {
	                	$subCheckAnsthesian .= ")";//for booking
	                	$subHolidayAnsthesian .= ")";//for leave
	                }
	                $countAnsthesian++;
	            }

	            $countNurse = 0;
				foreach ($arrNurse as $value) {
					if ($countNurse == 0) {
						$subCheckNurse.= "(";
						$subHolidayNurse.= "(";
					}
					if ($countNurse > 0) {
						$subCheckNurse.= " OR ";
						$subHolidayNurse.= " OR ";
					}

					$subCheckNurse.="ot_staff_details.ot_staff_id='".$value."'";
					$subHolidayNurse.="staff_id='".$value."'";

					if ($countNurse == $nurseLength - 1) {
	                	$subCheckNurse .= ")";
	                	$subHolidayNurse .= ")";
	                }
	                $countNurse++;
	            }


				//check that surgeon booked or not
				$checkSurgeon = "SELECT ot_staff_details.ot_staff_id FROM patient_ot_registration
				LEFT JOIN ot_room_availability ON patient_ot_registration.id = ot_room_availability.
				patient_operation_id
				LEFT JOIN ot_staff_details ON ot_staff_details.patient_operation_id = 
				patient_ot_registration.id
				WHERE ('".$changeFromTime."'  BETWEEN ot_room_availability.from_time AND
				ot_room_availability.`to_time` OR '".$changeToTime."' BETWEEN ot_room_availability.
				from_time AND	ot_room_availability.`to_time`) 
				AND patient_ot_registration.operation_date ='".$operationDate."' AND ot_room_availability.`status` = 'A' 
				AND patient_ot_registration.`status` = 'A' AND patient_ot_registration.id !='".$id."' 
				AND ot_staff_details.status = 'A' AND ";

				$checkSurgeon = $checkSurgeon.$subCheckSurgeon;//sub check coming from upside query
				$resultCheckSurgeon = mysqli_query($conn,$checkSurgeon);
				$countSurgeon = mysqli_num_rows($resultCheckSurgeon);

				if($countSurgeon > 0){
					echo "This surgeon for given time slot is already booked.";
					exit();
				}
				else{

					//check ansthesian is booked  or not
					$checkAnsthesian = "SELECT ot_staff_details.ot_staff_id FROM
					patient_ot_registration
					LEFT JOIN ot_room_availability ON patient_ot_registration.id = ot_room_availability.
					patient_operation_id
					LEFT JOIN ot_staff_details ON ot_staff_details.patient_operation_id = patient_ot_registration.id
					WHERE ('".$changeFromTime."'  BETWEEN ot_room_availability.from_time AND
					ot_room_availability.`to_time` OR '".$toTime."' BETWEEN ot_room_availability.from_time
					AND	ot_room_availability.`to_time`)  
					AND ot_room_availability.`status` = 'A' 
					AND patient_ot_registration.`status` = 'A' AND patient_ot_registration.operation_date ='".$operationDate."' AND ot_staff_details.status = 'A' 
					AND patient_ot_registration.id !='".$id."' AND ";

					$checkAnsthesian = $checkAnsthesian.$subCheckAnsthesian;
					$resultCheckAnsthesian = mysqli_query($conn,$checkAnsthesian);
					$countAnsthesian = mysqli_num_rows($resultCheckAnsthesian);

					if($countAnsthesian > 0){
						echo "This ansthesian for given time slot is already booked.";
						exit();
					}


					//check that nurse is booked or not

					$checkNurse = "SELECT ot_staff_details.ot_staff_id FROM
					patient_ot_registration
					LEFT JOIN ot_room_availability ON patient_ot_registration.id = ot_room_availability.
					patient_operation_id
					LEFT JOIN ot_staff_details ON ot_staff_details.patient_operation_id = patient_ot_registration.id
					WHERE ('".$changeFromTime."'  BETWEEN ot_room_availability.from_time AND
					ot_room_availability.`to_time` OR '".$toTime."' BETWEEN ot_room_availability.from_time
					AND	ot_room_availability.`to_time`)  
					AND ot_room_availability.`status` = 'A' 
					AND patient_ot_registration.`status` = 'A' AND patient_ot_registration.operation_date
					= '".$operationDate."' AND ot_staff_details.status = 'A' 
					AND patient_ot_registration.id !='".$id."' AND  ";
					
	                $checkNurse = $checkNurse.$subCheckNurse;	                
					$resultCheckNurse = mysqli_query($conn,$checkNurse);
					$countNurse = mysqli_num_rows($resultCheckNurse);			
					if($countNurse > 0){
						echo "This nurse for given time slot is already booked.";
						exit();
					}


					//check that ansthesian is on leave or not
					$checkAnsthesianLeave = "SELECT staff_id from staff_leaves 
					WHERE DATE_FORMAT(FROM_UNIXTIME(leave_date), '%Y-%m-%d') = '".$operationDate."'
					AND (leave_status !='not_taken' OR 'cancel') AND ";

	                $checkAnsthesianLeave = $checkAnsthesianLeave.$subHolidayAnsthesian;
					$resultCheckAnsthesianLeave = mysqli_query($conn,$checkAnsthesianLeave);
					$countnsthesianLeave = mysqli_num_rows($resultCheckAnsthesianLeave);
					if($countnsthesianLeave > 0){
						echo "Ansthesian is on leave.";
						exit();
					}

					//check that surgeon is on leave or not
					$checkSurgeonLeave = "SELECT staff_id from staff_leaves 
					WHERE DATE_FORMAT(FROM_UNIXTIME(leave_date), '%Y-%m-%d') = '".$operationDate."'
					AND (leave_status !='not_taken' OR 'cancel') AND ";

	                $checkSurgeonLeave = $checkSurgeonLeave.$subSurgeonHoliday;
					$resultCheckLeave = mysqli_query($conn,$checkSurgeonLeave);
					$countLeave = mysqli_num_rows($resultCheckLeave);
					if($countLeave > 0){
						echo "Surgeon is on leave.";
						exit();
					}

					//check that nurse is on leave or not
					$checkNurseLeave = "SELECT staff_id from staff_leaves 
					WHERE DATE_FORMAT(FROM_UNIXTIME(leave_date), '%Y-%m-%d') = '".$operationDate."'
					AND (leave_status !='not_taken' OR 'cancel') AND ";

	                $checkNurseLeave = $checkNurseLeave.$subHolidayNurse;
					$resultCheckLeave = mysqli_query($conn,$checkNurseLeave);
					$countLeave = mysqli_num_rows($resultCheckLeave);
					if($countLeave > 0){
						echo "Nurse is on leave.";
						exit();
					}
					else{
						/*Check patient booked or not*/
						$checkPatient = "SELECT patient_ot_registration.patient_id FROM patient_ot_registration
							LEFT JOIN ot_room_availability ON patient_ot_registration.id = 
							ot_room_availability.patient_operation_id
							WHERE ('".$changeFromTime."'  BETWEEN ot_room_availability.from_time AND
							ot_room_availability.`to_time` OR '".$changeToTime."' BETWEEN ot_room_availability.
							from_time AND	ot_room_availability.`to_time`) AND patient_ot_registration.
							operation_date ='".$operationDate."' AND patient_ot_registration.patient_id	='".$patientId."' AND patient_ot_registration.id !='".$id."'";

						$resultCheckPatient = mysqli_query($conn,$checkPatient);
						$countBookPatient = mysqli_num_rows($resultCheckPatient);

						if ($countBookPatient > 0) {
							echo "Patient alredy booked for this given time";
							exit();
						}
						else{
							//update patient ot registration
							$sql    = "UPDATE patient_ot_registration set patient_id = 
								'".$patientId."',operation_date= '".$operationDate."',
								surgery_id= '".$surgery."',surgery_type= '".$surgeryType."',
								ot_room_id= '".$otRoom."',duration= '".$hourDiff."',
								updated_on = UNIX_TIMESTAMP(),updated_by = '".$userId."' WHERE id = '".$id."' ";

							$sqlUpdate = "UPDATE ot_room_availability SET ot_room_id = '".$otRoom."',
								from_time = '".$fromTime."',to_time = '".$toTime."',updated_on = UNIX_TIMESTAMP(),
								updated_by = '".$userId."' WHERE patient_operation_id ='".$id."'";
							
							//get the room number to insert in to notification
							$selRoom = "SELECT room_no FROM ot_rooms WHERE id ='".$otRoom."'";
						    $resultRoom = mysqli_query($conn,$selRoom);
						    while ($row = mysqli_fetch_assoc($resultRoom)) {
						    	$roomNumber  = $row['room_no'];
						    }

							//first delete data from both notification and from ot staff details
							$delStaff = "DELETE FROM ot_staff_details WHERE 
								patient_operation_id ='".$id."'";

							$delNotficatin = "DELETE FROM notification";

							//reinsert data in to ot staff
							$insertStaff = "INSERT INTO ot_staff_details(ot_staff_id,
							   	patient_operation_id,created_on,created_by,updated_on,updated_by) VALUES ";
							
							// reinsert data in to notification
							$notifyQry = "INSERT INTO notification(notification_staff_id,message,
						   		created_on,created_by) VALUES ";

						   	$subQuery = '';
						   	$subNotifyQry = '';
						   	$countQry = '';
						   	foreach ($staffId as $value) {
						   		if ($countQry > 0) {
						   			$subQuery.=',';
						   			$subNotifyQry.=',';
						   		}
						   		$subQuery.= "('".$value."','".$id."',UNIX_TIMESTAMP(),
						   		'".$userId."',UNIX_TIMESTAMP(),'".$userId."')";

						   		$subNotifyQry.="('".$value."','An OT is booked for you on 
						   		".$operationDate." from ".$fromTime." to ".$toTime." at room number :".$roomNumber."',UNIX_TIMESTAMP(),'".$userId."')";	
						   		$countQry++;
						   	}
							
						   	$insertStaff = $insertStaff.$subQuery.";";
							$completeQry = $sql.';'.$sqlUpdate.';'.$delStaff.';'.$delNotficatin.';'.$insertStaff.$notifyQry.$subNotifyQry;
							$result = mysqli_query($conn, $sqlUpdate);
							echo $result;
							
						}
					}
				}
			}				
		}
		else{
			echo "Patient not exist.";
		}		
	}


	if ($operation == "show") {
		
		 $query = "SELECT patient_ot_registration.id, 
	    	CONCAT(patients.first_name,' ',patients.last_name) AS patients_name,
	    	patient_ot_registration.operation_date,
	    	GROUP_CONCAT(CONCAT(u1.first_name,' ',u1.last_name)) AS surgeon_name,
	    	GROUP_CONCAT(CONCAT(u2.first_name,' ',u2.last_name)) AS anethaesthest_name,
	    	GROUP_CONCAT(CONCAT(u3.first_name,' ',u3.last_name)) AS nurse_name,
	    	GROUP_CONCAT(u1.id) AS surgeon_id,
	    	GROUP_CONCAT(u2.id) AS anethaesthiest_id,
	    	GROUP_CONCAT(u3.id) AS nurse_id,ot_rooms.room_no,
	    	patient_ot_registration.duration,patient_ot_registration.surgery_type,
	    	surgery.name AS surgery_name,patient_ot_registration.patient_id,
	    	patient_ot_registration.surgery_id,	    	
	    	patient_ot_registration.ot_room_id,ot_room_availability.from_time,
	    	ot_room_availability.to_time
			FROM patient_ot_registration
			LEFT JOIN  patients ON  patients.id = patient_ot_registration.patient_id
			LEFT JOIN ot_staff_details ON ot_staff_details.patient_operation_id = 
			patient_ot_registration.id
			LEFT JOIN users  AS u1 ON u1.id = ot_staff_details.ot_staff_id AND u1.staff_type_id = 13
	        LEFT JOIN users AS u2 ON u2.id = ot_staff_details.ot_staff_id AND u2.staff_type_id = 14
	        LEFT JOIN users AS u3 on u3.id  = ot_staff_details.ot_staff_id AND u3.staff_type_id = 4
			LEFT JOIN ot_rooms ON ot_rooms.id = patient_ot_registration.ot_room_id
			LEFT JOIN surgery ON surgery.id = patient_ot_registration.surgery_id
			LEFT JOIN ot_room_availability ON ot_room_availability.patient_operation_id = 
			patient_ot_registration.id
			WHERE patient_ot_registration.`status` = 'A' AND ot_room_availability.`status` = 'A' 
			AND ot_rooms.`status` = 'A' AND ot_staff_details.`status` = 'A' 
			GROUP BY id";


		$result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    /*JSON encode*/
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}
	if ($operation == "checked") {
    
	    $query = "SELECT patient_ot_registration.id, 
	    	CONCAT(patients.first_name,' ',patients.last_name) AS patients_name,
	    	patient_ot_registration.operation_date,
	    	GROUP_CONCAT(CONCAT(u1.first_name,' ',u1.last_name)) AS surgeon_name,
	    	GROUP_CONCAT(CONCAT(u2.first_name,' ',u2.last_name)) AS anethaesthest_name,
	    	GROUP_CONCAT(CONCAT(u3.first_name,' ',u3.last_name)) AS nurse_name,
	    	GROUP_CONCAT(u1.id) AS surgeon_id,
	    	GROUP_CONCAT(u2.id) AS anethaesthiest_id,
	    	GROUP_CONCAT(u3.id) AS nurse_id,ot_rooms.room_no,
	    	patient_ot_registration.duration,patient_ot_registration.surgery_type,
	    	surgery.name AS surgery_name,patient_ot_registration.patient_id,
	    	patient_ot_registration.surgery_id,	    	
	    	patient_ot_registration.ot_room_id,ot_room_availability.from_time,
	    	ot_room_availability.to_time
			FROM patient_ot_registration
			LEFT JOIN  patients ON  patients.id = patient_ot_registration.patient_id
			LEFT JOIN ot_staff_details ON ot_staff_details.patient_operation_id = 
			patient_ot_registration.id
			LEFT JOIN users  AS u1 ON u1.id = ot_staff_details.ot_staff_id AND u1.staff_type_id = 13
	        LEFT JOIN users AS u2 ON u2.id = ot_staff_details.ot_staff_id AND u2.staff_type_id = 14
	        LEFT JOIN users AS u3 on u3.id  = ot_staff_details.ot_staff_id AND u3.staff_type_id = 4
			LEFT JOIN ot_rooms ON ot_rooms.id = patient_ot_registration.ot_room_id
			LEFT JOIN surgery ON surgery.id = patient_ot_registration.surgery_id
			LEFT JOIN ot_room_availability ON ot_room_availability.patient_operation_id = 
			patient_ot_registration.id
			WHERE patient_ot_registration.status = 'I' GROUP BY id";
	    
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	if ($operation == "restore") // for restore    
    {
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    $checkFromTime = $_POST['checkFromTime'];
	    $checkToTime = $_POST['checkToTime'];
	    $checkOTDate = $_POST['checkOTDate'];

	    //check whether restored lot is alreday booked or not
	     $checkSlotBooked = "SELECT patient_ot_registration.id FROM patient_ot_registration
	    	LEFT JOIN ot_room_availability ON ot_room_availability.patient_operation_id =
	    	patient_ot_registration.id
	    	WHERE ('".$checkFromTime."'  BETWEEN ot_room_availability.from_time AND
			ot_room_availability.`to_time` OR '".$checkToTime."' BETWEEN ot_room_availability.
			from_time AND	ot_room_availability.`to_time`) AND
	    	patient_ot_registration.operation_date ='".$checkOTDate."' 
	    	AND patient_ot_registration.status = 'A' AND ot_room_availability.`status` = 'A'";

	    $resultCheck = mysqli_query($conn, $checkSlotBooked);
	    $countRows = mysqli_num_rows($resultCheck);
	    if ($countRows < 1) {

	    	$sql    = "UPDATE patient_ot_registration SET status= 'A'  WHERE  id = '" . $id . "'";
		    $result = mysqli_query($conn, $sql);

		    if ($result) {

		    	$sqlRestore    = "UPDATE ot_room_availability SET status= 'A'  
		    		WHERE  patient_operation_id = '" . $id . "'";
		    	$result = mysqli_query($conn, $sqlRestore);
		    	echo $result;
		    }
		    else{
		    	echo "0";
		    }
	    }
	    else {
	    	echo "Already Booked";
	    }
	}
	if ($operation == "delete") {
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    
	    $sql    = "UPDATE patient_ot_registration SET status= 'I'  WHERE  id = '" . $id . "'";
	    $result = mysqli_query($conn, $sql);

	    if ($result) {

	    	$sqlRestore    = "UPDATE ot_room_availability SET status= 'I'  
	    		WHERE  patient_operation_id = '" . $id . "'";
	    	$result = mysqli_query($conn, $sqlRestore);
	    	echo $result;
	    }
	    else{
	    	echo "0";
	    }
	}



	if($operation == "searchName"){		
		
		if (isset($_POST['surgeonName'])) {
			$surgeonName=$_POST["surgeonName"];
		}
		if (isset($_POST['nurseName'])) {
			$nurseName=$_POST["nurseName"];
		}
		if (isset($_POST['anesthesianName'])) {
			$anesthesianName=$_POST["anesthesianName"];
		}

		if (isset($_POST['searchSurgeon'])) {
			$query ="SELECT id,CONCAT(first_name,' ',last_name) AS name FROM users
			WHERE users.status = 'A' AND users.first_name LIKE '%".$surgeonName."%' AND users.staff_type_id = '13'";
		}
		if (isset($_POST['searchAnesthesian'])) {
			$query ="SELECT id,CONCAT(first_name,' ',last_name) AS name FROM users
			WHERE users.status = 'A' AND users.first_name LIKE '%".$anesthesianName."%' AND users.staff_type_id = '14'";
		}
		if (isset($_POST['searchNurse'])) {
			$query ="SELECT id,CONCAT(first_name,' ',last_name) AS name FROM users
			WHERE users.status = 'A' AND users.first_name LIKE '%".$nurseName."%' AND users.staff_type_id = '4'";
		}
								
		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		print json_encode($rows);
	}	
?>