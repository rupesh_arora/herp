<?php

	/*File Name  :   insurance_revised_cost.php
    Company Name :   Qexon Infotech
    Created By   :   Tushar Gupta
    Created Date :   23th march, 2016
    Description  :   This page manges revised cost details*/
	session_start();	

    if(isset($_SESSION['globaluser'])){
        $userId = $_SESSION['globaluser'];//store global user value in variable
    }
    else{
    	exit();
    }

     $companyId = '';
     $planId = '';
     $schemeId = '';

    /*include config file*/
	include 'config.php';
    $operation = "";

    /*checking operation set or not*/
	if (isset($_POST['operation'])) {
	    $operation = $_POST["operation"];
	} else if (isset($_GET["operation"])) {
	    $operation = $_GET["operation"];
	}

	/*operation to show scheme name*/
	if ($operation == "showSchemeName") {
	    if (isset($_POST['companyId'])) {
	        $companyId = $_POST['companyId'];
	    }
	    $query  = "SELECT id,plan_name FROM insurance_plan WHERE company_name_id = '" . $companyId . "' AND status = 'A' ORDER BY plan_name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	
	/*operation to show Plan name*/
	if ($operation == "showPlanName") {
	    if (isset($_POST['schemeId'])) {
	        $schemeId = $_POST['schemeId'];
	    }
	    $query  = "SELECT id,scheme_plan_name FROM scheme_plans WHERE insurance_plan_id = '" . $schemeId . "' AND status = 'A' ORDER BY scheme_plan_name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	
	
    /*operation to show service type*/
	if ($operation == "showServiceDetails") {

	    if (isset($_POST['companyId'])) {
	        $companyId = $_POST['companyId'];
	    }
	    if (isset($_POST['planId'])) {
	        $schemeId = $_POST['planId'];
	    }
	    if (isset($_POST['schemeId'])) {
	        $planId = $_POST['schemeId'];
	    }

		$insQuery = "SELECT item_id AS id,item_name AS name,actual_cost AS cost, revised_cost FROM
		insurance_revised_cost 
		WHERE status = 'A' AND insurance_company_id = '" . $companyId . "' AND 
		scheme_plan_id ='" . $planId . "' AND scheme_name_id ='" . $schemeId . "'  AND
		`service_type` = 'service' AND item_id NOT IN (SELECT item_id FROM scheme_exclusion WHERE
		status = 'A' AND insurance_company_id = '" . $companyId . "' AND 
		scheme_plan_id ='" . $planId . "' AND scheme_name_id ='" . $schemeId ."' 
		AND `service_type` = 'service') ORDER BY name";
		$insResult = mysqli_query($conn, $insQuery);
		$rowCount = mysqli_num_rows($insResult);
		if($rowCount > 0){
			 $rows   = array();
		    while ($r = mysqli_fetch_assoc($insResult)) {
		        $rows[] = $r;
		    }
		    print json_encode($rows);
		}
		else{
			$query  = "SELECT id,name,cost,cost AS revised_cost  FROM services WHERE id NOT IN 
			(SELECT item_id FROM scheme_exclusion WHERE status = 'A' AND
		    insurance_company_id = '" . $companyId . "' AND scheme_plan_id ='" . $planId . "' AND
		    scheme_name_id ='" . $schemeId ."' AND `service_type` = 'service') AND status = 'A' ORDER BY name";
		    $result = mysqli_query($conn, $query);
		    $rows   = array();
		    while ($r = mysqli_fetch_assoc($result)) {
		        $rows[] = $r;
		    }
		    print json_encode($rows);
		}	   
	}

    /*operation to show lab type*/
	if ($operation == "showLabDetails") {
		
	    if (isset($_POST['companyId'])) {
	        $companyId = $_POST['companyId'];
	    }
	    if (isset($_POST['planId'])) {
	        $schemeId = $_POST['planId'];
	    }
	    if (isset($_POST['schemeId'])) {
	        $planId = $_POST['schemeId'];
	    }

		$insQuery = "SELECT item_id AS id,item_name AS name,actual_cost AS cost, revised_cost FROM insurance_revised_cost 
			WHERE status = 'A' AND insurance_company_id = '" . $companyId . "' AND 
			scheme_plan_id ='" . $planId . "' AND scheme_name_id ='" . $schemeId . "'  AND
			`service_type` = 'Lab' AND item_id NOT IN (SELECT item_id FROM scheme_exclusion WHERE
			status = 'A' AND insurance_company_id = '" . $companyId . "' AND 
			scheme_plan_id ='" . $planId . "' AND scheme_name_id ='" . $schemeId ."' 
			AND `service_type` = 'Lab') ORDER BY name";
		$insResult = mysqli_query($conn, $insQuery);
		$rowCount = mysqli_num_rows($insResult);
		if($rowCount > 0){
			 $rows   = array();
		    while ($r = mysqli_fetch_assoc($insResult)) {
		        $rows[] = $r;
		    }
		    print json_encode($rows);
		}
		else{
		    $query  = "SELECT id,name,fee AS cost,fee AS revised_cost  FROM lab_test_component 
		    WHERE id NOT IN (SELECT item_id FROM scheme_exclusion WHERE status = 'A' AND
		    insurance_company_id = '" . $companyId . "' AND scheme_plan_id ='" . $planId . "' AND
		    scheme_name_id ='" . $schemeId ."' AND `service_type` = 'Lab') ORDER BY name";
		    $result = mysqli_query($conn, $query);
		    $rows   = array();
		    while ($r = mysqli_fetch_assoc($result)) {
		        $rows[] = $r;
		    }
		    print json_encode($rows);
		}
	}
    /*operation to show procedure*/
	if ($operation == "showProcedureDetails") {
		
	    if (isset($_POST['companyId'])) {
	        $companyId = $_POST['companyId'];
	    }
	    if (isset($_POST['planId'])) {
	        $schemeId = $_POST['planId'];
	    }
	    if (isset($_POST['schemeId'])) {
	        $planId = $_POST['schemeId'];
	    }

		$insQuery = "SELECT item_id AS id,item_name AS name,actual_cost AS cost, revised_cost FROM insurance_revised_cost
		WHERE status = 'A' AND insurance_company_id = '" . $companyId . "' AND 
		scheme_plan_id ='" . $planId . "' AND scheme_name_id ='" . $schemeId . "'  AND
		`service_type` = 'procedure' AND item_id NOT IN (SELECT item_id FROM scheme_exclusion WHERE
		status = 'A' AND insurance_company_id = '" . $companyId . "' AND 
		scheme_plan_id ='" . $planId . "' AND scheme_name_id ='" . $schemeId ."' 
		AND `service_type` = 'procedure') ORDER BY name";
		$insResult = mysqli_query($conn, $insQuery);
		$rowCount = mysqli_num_rows($insResult);
		if($rowCount > 0){
			 $rows   = array();
		    while ($r = mysqli_fetch_assoc($insResult)) {
		        $rows[] = $r;
		    }
		    print json_encode($rows);
		}
		else{
		    $query  = "SELECT id,CONCAT(name, '  ','(',code,')') AS name, price AS cost,price AS revised_cost  FROM procedures WHERE id NOT IN (SELECT item_id FROM scheme_exclusion WHERE
		    status = 'A' AND insurance_company_id = '".$companyId."' AND scheme_plan_id ='".$planId."' AND
		    scheme_name_id ='" . $schemeId ."' AND `service_type` = 'procedure') AND status = 'A'  ORDER BY name";
		    $result = mysqli_query($conn, $query);
		    $rows   = array();
		    while ($r = mysqli_fetch_assoc($result)) {
		        $rows[] = $r;
		    }
		    print json_encode($rows);
		}
	}

    /*operation to show radiology test*/
	if ($operation == "showRadiologyDetails") {
		
	    if (isset($_POST['companyId'])) {
	        $companyId = $_POST['companyId'];
	    }
	    if (isset($_POST['planId'])) {
	        $schemeId = $_POST['planId'];
	    }
	    if (isset($_POST['schemeId'])) {
	        $planId = $_POST['schemeId'];
	    }

		$insQuery = "SELECT item_id AS id,item_name AS name,actual_cost AS cost, revised_cost FROM insurance_revised_cost 
		WHERE status = 'A' AND insurance_company_id = '" . $companyId . "' AND 
		scheme_plan_id ='" . $planId . "' AND scheme_name_id ='" . $schemeId . "'  AND
		`service_type` = 'radiology' AND item_id NOT IN (SELECT item_id FROM scheme_exclusion WHERE
		status = 'A' AND insurance_company_id = '" . $companyId . "' AND 
		scheme_plan_id ='" . $planId . "' AND scheme_name_id ='" . $schemeId ."' 
		AND `service_type` = 'radiology') ORDER BY name";
		$insResult = mysqli_query($conn, $insQuery);
		$rowCount = mysqli_num_rows($insResult);
		if($rowCount > 0){
			 $rows   = array();
		    while ($r = mysqli_fetch_assoc($insResult)) {
		        $rows[] = $r;
		    }
		    print json_encode($rows);
		}
		else{
		    $query  = "SELECT id,name,fee AS cost,fee AS revised_cost  FROM radiology_tests 
		    WHERE id NOT IN (SELECT item_id FROM scheme_exclusion WHERE status = 'A' AND 
		    insurance_company_id = '".$companyId."' AND scheme_plan_id ='".$planId."' AND
		    scheme_name_id ='" . $schemeId ."' AND `service_type` = 'radiology') AND status = 'A'  
		    ORDER BY name";
		    $result = mysqli_query($conn, $query);
		    $rows   = array();
		    while ($r = mysqli_fetch_assoc($result)) {
		        $rows[] = $r;
		    }
		    print json_encode($rows);
		}
	}

    /*operation to show forensic test*/
	if ($operation == "showForensicDetails") {
		
	    if (isset($_POST['companyId'])) {
	        $companyId = $_POST['companyId'];
	    }
	    if (isset($_POST['planId'])) {
	        $schemeId = $_POST['planId'];
	    }
	    if (isset($_POST['schemeId'])) {
	        $planId = $_POST['schemeId'];
	    }

		$insQuery = "SELECT item_id AS id,item_name AS name,actual_cost AS cost, revised_cost FROM insurance_revised_cost 
		WHERE status = 'A' AND insurance_company_id = '" . $companyId . "' AND 
		scheme_plan_id ='" . $planId . "' AND scheme_name_id ='" . $schemeId . "'  AND
		`service_type` = 'forensic' AND item_id NOT IN (SELECT item_id FROM scheme_exclusion WHERE
		status = 'A' AND insurance_company_id = '" . $companyId . "' AND 
		scheme_plan_id ='" . $planId . "' AND scheme_name_id ='" . $schemeId ."' 
		AND `service_type` = 'forensic') ORDER BY name";
		$insResult = mysqli_query($conn, $insQuery);
		$rowCount = mysqli_num_rows($insResult);
		if($rowCount > 0){
			 $rows   = array();
		    while ($r = mysqli_fetch_assoc($insResult)) {
		        $rows[] = $r;
		    }
		    print json_encode($rows);
		}
		else{
		    $query  = "SELECT id,name,fee AS cost,fee AS revised_cost  FROM forensic_tests 
		    WHERE id NOT IN (SELECT item_id FROM scheme_exclusion WHERE status = 'A' AND 
		    insurance_company_id = '".$companyId."' AND scheme_plan_id ='".$planId."' AND
		    scheme_name_id ='" . $schemeId ."' AND `service_type` = 'forensic') AND status = 'A'  
		    ORDER BY name";
		    $result = mysqli_query($conn, $query);
		    $rows   = array();
		    while ($r = mysqli_fetch_assoc($result)) {
		        $rows[] = $r;
		    }
		    print json_encode($rows);
		}
	}

	/*operation to show drug*/
	if ($operation == "showPharmacyDetails") {
		
	    if (isset($_POST['companyId'])) {
	        $companyId = $_POST['companyId'];
	    }
	    if (isset($_POST['planId'])) {
	        $schemeId = $_POST['planId'];
	    }
	    if (isset($_POST['schemeId'])) {
	        $planId = $_POST['schemeId'];
	    }

		$insQuery = "SELECT item_id AS id,item_name AS name,actual_cost AS cost, revised_cost FROM insurance_revised_cost 
		WHERE status = 'A' AND insurance_company_id = '" . $companyId . "' AND 
		scheme_plan_id ='" . $planId . "' AND scheme_name_id ='" . $schemeId . "'  AND
		`service_type` = 'Pharmacy' AND item_id NOT IN (SELECT item_id FROM scheme_exclusion WHERE
		status = 'A' AND insurance_company_id = '" . $companyId . "' AND 
		scheme_plan_id ='" . $planId . "' AND scheme_name_id ='" . $schemeId ."' 
		AND `service_type` = 'Pharmacy') ORDER BY name";
		$insResult = mysqli_query($conn, $insQuery);
		$rowCount = mysqli_num_rows($insResult);
		if($rowCount > 0){
			 $rows   = array();
		    while ($r = mysqli_fetch_assoc($insResult)) {
		        $rows[] = $r;
		    }
		    print json_encode($rows);
		}
		else{
		    $query  = "SELECT id,name,price AS cost,price AS revised_cost  FROM drugs  
		    WHERE id NOT IN (SELECT item_id FROM scheme_exclusion WHERE status = 'A' AND 
		    insurance_company_id = '".$companyId."' AND scheme_plan_id ='".$planId."' AND
		    scheme_name_id ='" . $schemeId ."' AND `service_type` = 'Pharmacy') AND status = 'A'  
		    ORDER BY name";
		    $result = mysqli_query($conn, $query);
		    $rows   = array();
		    while ($r = mysqli_fetch_assoc($result)) {
		        $rows[] = $r;
		    }
		    print json_encode($rows);
		}
	}

    /*operation to save whole data*/
	if ($operation == "saveDataTableData") {		
	    if (isset($_POST['data'])) {
	        $data = json_decode($_POST['data']);
	    }
		if (isset($_POST['insuranceCompnayId'])) {
	        $insuranceCompnayId = $_POST['insuranceCompnayId'];
	    }
	    if (isset($_POST['serviceNameId'])) {
	        $serviceNameId = $_POST['serviceNameId'];
	    }
	    if (isset($_POST['planNameId'])) {
	        $planNameId = $_POST['planNameId'];
	    }
	    if (isset($_POST['saveServiceTypeName'])) {
	        $saveServiceTypeName = $_POST['saveServiceTypeName'];
	    }

	    /*Firstly delete whole data*/
	    $deleteQry = "DELETE FROM insurance_revised_cost WHERE insurance_company_id = 
	    '".$insuranceCompnayId."' AND scheme_plan_id = '".$serviceNameId."'
	    AND scheme_name_id = '".$planNameId."' AND service_type = '".$saveServiceTypeName."'";
	    mysqli_query($conn,$deleteQry);

	    $insertQry = "INSERT INTO insurance_revised_cost(insurance_company_id,scheme_plan_id,scheme_name_id,item_name,item_id,actual_cost,revised_cost,service_type,created_on,created_by) VALUES";

	    $subQuery         = '';
	    $counter          = 0;

	    foreach ($data as $value) {
	    	$itemName = '';
	    	$itemId = '';
	    	$actualCost = '';
	    	$revisedCost = '';

	    	foreach ($value as $key => $val) {

	    		if ($key == 0) { //at index zero get the item name
					$itemName = $val;
				}
				if ($key == 1) { //at index one get the actual cost
					$actualCost = $val;
				}
				if ($key == 2) { //at index two get the revised cost
					$revisedCost = $val;
				}
				if ($key == 3) { //at index three get the item id
					$itemId = $val;
				}								
	    	}
	    	if ($counter > 0) {
				$subQuery .= ",";
			}
	    	$subQuery .= "('".$insuranceCompnayId."','".$serviceNameId."','".$planNameId."',
				'".$itemName."','".$itemId."','".$actualCost."','".$revisedCost."',
				'".$saveServiceTypeName."',UNIX_TIMESTAMP(),'".$userId."')";

			$counter++;
	    }
	    $completeInsertQry = $insertQry . $subQuery . ";";
	    $result = mysqli_query($conn,$completeInsertQry);
	    echo $result;
	}
?>