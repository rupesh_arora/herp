<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "showPpeClass") { // show insurance company type
	    $query = "SELECT id,ppe_class_name FROM ppe_class where status = 'A' ";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "saveAssetClass") {

		$assetClass = $_POST['assetClass'];
		$depriciationRate = $_POST['depriciationRate'];
		$glAccount = $_POST['glAccount'];
		$accumulatedGlAccount = $_POST['accumulatedGlAccount'];
		$ppeClass = $_POST['ppeClass'];
		$description = $_POST['description'];

		$selLedger = "SELECT asset_class_name FROM asset_class WHERE asset_class_name ='".$assetClass."' AND status = 'A'";
		$checkLedger = mysqli_query($conn,$selLedger);
		$countLedger = mysqli_num_rows($checkLedger);

		if ($countLedger == "0") {

			$query = "INSERT INTO asset_class(asset_class_name,depriciation_rate,gl_account_id,accumluated_gl_account_id,ppe_class_id,description,created_by,updated_by,created_on,updated_on) VALUES('".$assetClass."','".$depriciationRate."','".$glAccount."','".$accumulatedGlAccount."','".$ppeClass."','".$description."','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";

			$result = mysqli_query($conn, $query);
			echo $result;
		}
		else{
			echo "0";
		}
	}

	if ($operation == "show") { // show active data
    
	    	 $query = "SELECT asset_class.id,asset_class_name,depriciation_rate,gl_account_id,accumluated_gl_account_id,ppe_class_id,description,gl_account.account_name AS gl_account_name,accumulated_gl_account.account_name AS acc_gl_account_name,ppe_class.ppe_class_name AS ppe_class_name FROM asset_class
				LEFT JOIN chart_of_account AS gl_account ON gl_account.id = asset_class.gl_account_id
				LEFT JOIN chart_of_account AS accumulated_gl_account ON accumulated_gl_account.id = asset_class.accumluated_gl_account_id
				LEFT JOIN ppe_class ON ppe_class.id = asset_class.ppe_class_id WHERE asset_class.`status` = 'A'";
	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}

	if ($operation == "checked") {
	    
		     $query = "SELECT asset_class.id,asset_class_name,depriciation_rate,gl_account_id,accumluated_gl_account_id,ppe_class_id,description,gl_account.account_name AS gl_account_name,accumulated_gl_account.account_name AS acc_gl_account_name,ppe_class.ppe_class_name AS ppe_class_name FROM asset_class
				LEFT JOIN chart_of_account AS gl_account ON gl_account.id = asset_class.gl_account_id
				LEFT JOIN chart_of_account AS accumulated_gl_account ON accumulated_gl_account.id = asset_class.accumluated_gl_account_id
				LEFT JOIN ppe_class ON ppe_class.id = asset_class.ppe_class_id WHERE asset_class.`status` = 'I'";
	    
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	if ($operation == "update") // update data
	{
	    $assetClass = $_POST['assetClass'];
	    $depriciationRate = $_POST['depriciationRate'];
	    $glAccount = $_POST['glAccount'];
	    $accumulatedGlAccount = $_POST['accumulatedGlAccount'];
	    $ppeClass = $_POST['ppeClass'];
		$description = $_POST['description'];
	    $id = $_POST['id'];

	    $selLedger = "SELECT asset_class_name FROM asset_class WHERE asset_class_name ='".$assetClass."' AND status = 'A' AND id != '".$id."' ";
		$checkLedger = mysqli_query($conn,$selLedger);
		$countLedger = mysqli_num_rows($checkLedger);

		if ($countLedger == "0") {

		
			$sql    = "UPDATE asset_class set asset_class_name = '".$assetClass."', depriciation_rate = '".$depriciationRate."', gl_account_id = '".$glAccount."', accumluated_gl_account_id = '".$accumulatedGlAccount."', ppe_class_id = '".$ppeClass."',description= '".$description."',updated_on = UNIX_TIMESTAMP() ,updated_by = '".$userId."' where id = '".$id."' ";

			$result = mysqli_query($conn, $sql);
			echo $result;
		}
		else{
			echo "0";
		}
	}

	if ($operation == "delete") {
        $id = $_POST['id'];
        $name = $_POST['name'];


		$selLedger = "SELECT asset_class.asset_class_name FROM asset_subclass LEFT JOIN asset_class ON asset_class.id = asset_subclass.class where asset_class.asset_class_name ='".$name."' and asset_subclass.`status` = 'A'";
		$checkLedger = mysqli_query($conn,$selLedger);
		$countLedger = mysqli_num_rows($checkLedger);

		if ($countLedger == "0") {
			$sql    = "UPDATE asset_class SET status= 'I' where id = '" . $id . "'";
		    $result = mysqli_query($conn, $sql);
		    echo $result;	    
		}
		else{
			echo "0";
		}
	}

	if ($operation == "restore") {// for restore    
        $id = $_POST['id'];
        $name = $_POST['name'];
        $selLedger = "SELECT asset_class_name FROM asset_class WHERE asset_class_name ='".$name."' AND status = 'A' AND id != '".$id."' ";
		$checkLedger = mysqli_query($conn,$selLedger);
		$countLedger = mysqli_num_rows($checkLedger);

		if ($countLedger == "0") {

			echo $sql    = "UPDATE asset_class SET status= 'A'  WHERE  id = '" . $id . "'";
			$result = mysqli_query($conn, $sql);
		    echo $result;
		}
		else{
			echo "0";
		}
	}
?>