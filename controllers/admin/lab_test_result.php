<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET['operation'])){
		$operation=$_GET["operation"];
	}

	if($operation == "bindTableData"){
		if (isset($_POST['visitId'])) {
			$visitId=$_POST['visitId'];
		}
		else if(isset($_GET['visitId'])){
			$visitId=$_GET['visitId'];
		}
		
		if (isset($_POST['paitentId'])) {
		$paitentId=$_POST['paitentId'];
		}
		else if(isset($_GET['paitentId'])){
			$paitentId=$_GET['paitentId'];
		}
		
		$sql = "SELECT lab_test_requests.lab_test_id,l.id,l.result,l.normal_range,l.report,patients.id as patient_id,concat(patients.salutation,' ',patients.first_name,' ',
			patients.middle_name,' ',patients.last_name) as patient_name,
			visits.created_on as visit_date,visits.id as visit_id,l_c.name,
			l.lab_test_component_id,l_t.name AS lab_test_name   
			from `lab_test_result` AS l 
			LEFT JOIN patient_specimen ON l.specimen_id=patient_specimen.id 
			LEFT JOIN lab_test_component AS l_c ON l_c.id = l.lab_test_component_id
			LEFT JOIN lab_tests AS l_t ON l_t.id = l_c.lab_test_id
			LEFT JOIN lab_test_requests ON patient_specimen.lab_test_id = lab_test_requests.id
			LEFT JOIN patients ON patients.id = lab_test_requests.patient_id 
			LEFT JOIN visits ON visits.id = lab_test_requests.visit_id 
			WHERE lab_test_requests.visit_id=".$visitId." AND 
			lab_test_requests.patient_id=".$paitentId." AND 
			lab_test_requests.test_status='resultsent' AND lab_test_requests.pay_status='settled'
			GROUP BY lab_test_requests.lab_test_id";
		
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		
		$rows = array();			
		while($r = mysqli_fetch_assoc($result)) {							
			$rows[] = $r;
		}		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);	
			
	}

	if($operation == "bindTableDataByOrder"){
		if (isset($_POST['visitId'])) {
			$visitId=$_POST['visitId'];
		}
		else if(isset($_GET['visitId'])){
			$visitId=$_GET['visitId'];
		}
		
		if (isset($_POST['paitentId'])) {
		$paitentId=$_POST['paitentId'];
		}
		else if(isset($_GET['paitentId'])){
			$paitentId=$_GET['paitentId'];
		}


		if (isset($_POST['labTestId'])) {
			$labTestId=$_POST['labTestId'];

			$sql = "SELECT lab_test_requests.lab_test_id,l.id,l.result,l.normal_range,l.report,
			patients.id as patient_id,concat(patients.salutation,' ',patients.first_name,
			' ',patients.middle_name,' ',patients.last_name) as patient_name,visits.created_on 
			as visit_date,visits.id as visit_id,l_c.name,l.lab_test_component_id,l_t.name 
			AS lab_test_name from `lab_test_result` AS l 
			LEFT JOIN patient_specimen ON l.specimen_id=patient_specimen.id 
			LEFT JOIN lab_test_component AS l_c ON l_c.id = l.lab_test_component_id
			LEFT JOIN lab_tests AS l_t ON l_t.id = l_c.lab_test_id
			LEFT JOIN lab_test_requests ON patient_specimen.lab_test_id = lab_test_requests.id
			LEFT JOIN patients ON patients.id = lab_test_requests.patient_id 
			LEFT JOIN visits ON visits.id = lab_test_requests.visit_id 
			WHERE lab_test_requests.visit_id=".$visitId." 
			AND lab_test_requests.patient_id=".$paitentId." 
			AND lab_test_requests.test_status='resultsent'
			AND lab_test_requests.pay_status='settled'
			AND lab_test_requests.lab_test_id = '".$labTestId."'
			ORDER BY lab_test_requests.lab_test_id";
		}

		else{
			$sql = "SELECT lab_test_requests.lab_test_id,l.id,l.result,l.normal_range,l.report,
				patients.id as patient_id,concat(patients.salutation,' ',patients.first_name,
				' ',patients.middle_name,' ',patients.last_name) as patient_name,visits.created_on 
				as visit_date,visits.id as visit_id,l_c.name,l.lab_test_component_id,l_t.name 
				AS lab_test_name from `lab_test_result` AS l 
				LEFT JOIN patient_specimen ON l.specimen_id=patient_specimen.id 
				LEFT JOIN lab_test_component AS l_c ON l_c.id = l.lab_test_component_id
				LEFT JOIN lab_tests AS l_t ON l_t.id = l_c.lab_test_id
				LEFT JOIN lab_test_requests ON patient_specimen.lab_test_id = lab_test_requests.id
				LEFT JOIN patients ON patients.id = lab_test_requests.patient_id 
				LEFT JOIN visits ON visits.id = lab_test_requests.visit_id 
				WHERE lab_test_requests.visit_id=".$visitId." 
				AND lab_test_requests.patient_id=".$paitentId." 
				AND lab_test_requests.test_status='resultsent'
				AND lab_test_requests.pay_status='settled'
				ORDER BY lab_test_requests.lab_test_id";
		}	
		
	$result = mysqli_query($conn, $sql);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);	
			
	}
?>