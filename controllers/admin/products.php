<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}



	if ($operation == "getTaxes") {
		$query  = "SELECT id,tax_name,tax_rate FROM tax_type WHERE status='A' ";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "save") {
		$productName = $_POST['productName'];
		$costprice = $_POST['costprice'];
		$unit = $_POST['unit'];
		$taxes = json_decode($_POST['arrayTaxes']);

		$sql = "INSERT INTO products (name,cost_price,unit,created_on,updated_on,created_by,updated_by) VALUES('".$productName."','".$costprice."','".$unit."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),
			'".$userId."','".$userId."')";
		    
	    $result = mysqli_query($conn, $sql);

	    $last_id = mysqli_insert_id($conn);

	    if (!empty($taxes)) {
	    	$insertTax = "INSERT INTO product_taxes (product_id,tax_id,created_on,updated_on,created_by,updated_by) VALUES ";
		    $subInsert = '';
		    $counter = 0;
		    foreach ($taxes as $key => $value) {
		    	
		    	if ($counter > 0) {
		    		$subInsert.= ',';
		    	}
		    	$subInsert.= "('".$last_id."','".$value."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."',
		    		'".$userId."')";

		    	$counter++;
		    }

		    $completeQry = $insertTax.$subInsert;

		    $result = mysqli_query($conn, $completeQry);
	    	echo $result;
	    }
	    else{
	    	echo $result;
	    }
	}

	if ($operation == "show") { // show active data
    
	    $sql = "SELECT products.id,products.name AS product_name,products.cost_price,products.unit,GROUP_CONCAT(product_taxes.tax_id) AS tax_id,
		 IFNULL(GROUP_CONCAT(tax_type.tax_name),'N/A')  AS tax_name ,units.unit,units.id AS unit_id 
		FROM products

		LEFT JOIN product_taxes ON product_taxes.product_id = products.id
		LEFT JOIN tax_type ON tax_type.id = product_taxes.tax_id
		LEFT JOIN units ON units.id = products.unit
		GROUP BY products.id";
	    $result = mysqli_query($conn, $sql);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	if ($operation == "checked") {
    
	    $query = "SELECT products.id ,products.name AS product_name,products.cost_price,units.unit,GROUP_CONCAT(product_taxes.tax_id) AS tax_id,GROUP_CONCAT(tax_type.tax_name) AS tax_name 
	    	,units.id AS unit_id	FROM products
			LEFT JOIN product_taxes ON product_taxes.product_id = products.id
			LEFT JOIN tax_type ON tax_type.id = product_taxes.tax_id
			LEFT JOIN units ON units.id = products.unit
			WHERE products.`status` = 'I' AND product_taxes.`status` = 'I' GROUP BY products.id";
	    
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	if ($operation == "update") {// update data
	
	    
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    if (isset($_POST['productName'])) {
	        $productName = $_POST['productName'];
	    }
	    /*if (isset($_POST['productName'])) {
	        $productCode = $_POST['productCode'];
	    }*/
	    if (isset($_POST['costprice'])) {
	        $costprice = $_POST['costprice'];
	    }
	    if (isset($_POST['unit'])) {
	        $unit = $_POST['unit'];
	    }	
	    $taxes = json_decode($_POST['arrayTaxes']);		
		
		$sql    = "UPDATE products set name = '".$productName."',cost_price = '".$costprice."',
			unit = '".$unit."', updated_on = UNIX_TIMESTAMP(),updated_by = '".$userId."' 
			WHERE id = '".$id."' ";

		$delPrevoiusData = "DELETE FROM product_taxes WHERE product_id = '".$id."'";

		$insertTax = "INSERT INTO product_taxes (product_id,tax_id,created_on,updated_on,created_by,updated_by) VALUES ";
		    $subInsert = '';
		    $counter = 0;
		    foreach ($taxes as $key => $value) {
		    	
		    	if ($counter > 0) {
		    		$subInsert.= ',';
		    	}
		    	$subInsert.= "('".$id."','".$value."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."',
		    		'".$userId."')";

		    	$counter++;
		    }

		$completeQry = $sql.';'.$delPrevoiusData.';'.$insertTax.$subInsert;

		$result = mysqli_multi_query($conn, $completeQry);
		echo $result;
	
	}

	if ($operation == "restore") // for restore    
    {
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }

		$sql    = "UPDATE products SET status= 'A' where id = '" . $id . "';";
    	$sql.= "UPDATE product_taxes SET status= 'A' where product_id = '" . $id . "'";
	    $result = mysqli_multi_query($conn, $sql);
	    echo $result;
		 
	}
	if ($operation == "delete") {
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    
    	$sql    = "UPDATE products SET status= 'I' where id = '" . $id . "';";
    	$sql.= "UPDATE product_taxes SET status= 'I' where product_id = '" . $id . "'";
	    $result = mysqli_multi_query($conn, $sql);
	    echo $result;
	    
	}
?>