<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "showPettyCash") {

		$query = "SELECT pc.id,pc.staff_customer_name,pc.amount FROM petty_cash AS pc WHERE pc.status = 'A' AND cash_payement_voucher_status='unpaid'";
		 
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	if ($operation == "showPaymentMode") { // show active data
    
    	$query = "SELECT id,name FROM payment_mode WHERE status = 'A'";
	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
    
	}
	if ($operation == "saveData") {
		$cashierDetail = $_POST['cashierDetail'];
		$totalAmount = $_POST['totalAmount'];
		$ledger_id = $_POST['ledger_id'];
		$paymentMode = $_POST['paymentMode'];
		$refNo = $_POST['refNo'];
		$notes = $_POST['notes'];
		$saveArray = json_decode($_POST['saveArray']);
		//print_r($saveArray);
		//exit();
		$insertMainQry = "INSERT INTO cash_payment_voucher (cashier_name,total_amount,
			ledger_id,payment_mode_id,reference_no,notes,created_by,updated_by,created_on,
			updated_on) VALUES ('".$cashierDetail."','".$totalAmount."',
			'".$ledger_id."','".$paymentMode."','".$refNo."','".$notes."','".$userId."','".$userId."',
			UNIX_TIMESTAMP(),UNIX_TIMESTAMP());";

		mysqli_query($conn,$insertMainQry);
		$last_id = mysqli_insert_id($conn);

		$insertSubDetails = "INSERT INTO cash_payment_voucher_item(cash_payment_voucher_id,petty_cash_id,
			amount, created_on, updated_on,created_by,updated_by) VALUES ";
		$updatePetty = "UPDATE petty_cash SET cash_payement_voucher_status = 'paid' WHERE";
		$counter = 0;
		$subQry = '';
		foreach ($saveArray as $key => $value) {
			$pettyCashId = $value->pettyCashId;
			$amount = $value->amount;

			if ($counter > 0) {
				$subQry.= ',';
			}
			$subQry.= "('".$last_id."','".$pettyCashId."','".$amount."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
			
			if ($pettyCashId) {
				$updatePetty = "UPDATE petty_cash SET cash_payement_voucher_status = 'paid' WHERE id = '".$pettyCashId."'";
				$resultUpdate = mysqli_query($conn,$updatePetty);
				//echo $resultUpdate;
			}
			
			$counter++;
		}

		$completeQry = $insertSubDetails.$subQry;
		
		$result = mysqli_query($conn,$completeQry);
		echo $result;
	}

	if ($operation == "show") {
		 $query = "SELECT cpv.id,cpv.cashier_name,cpv.total_amount,pm.name AS payment_mode_name,
		 			l.name AS ledger_name,DATE_FORMAT(FROM_UNIXTIME(cpv.created_on), '%Y-%m-%d') AS cpv_date 
		 			FROM cash_payment_voucher AS cpv LEFT JOIN payment_mode AS pm ON pm.id = cpv.payment_mode_id
					LEFT JOIN ledger AS l ON l.id = cpv.ledger_id WHERE cpv.status = 'A' GROUP BY cpv.id";
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    /*JSON encode*/
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}
?>