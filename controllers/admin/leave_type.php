<?php
/*
 * File Name    :   Designation.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   29 Sept, 2015
 * Description  :   This page is use for add department
 */
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

$operation       = "";

include 'config.php'; // include database connection file

if (isset($_POST['operation'])) { // define operation value from js file
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}


/* save designation informationinto database*/
if ($operation == "save") {
    
    if (isset($_POST['leaveTypeName'])) {
        $leaveTypeName = $_POST['leaveTypeName'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    
    
    $sql1         = "SELECT name from leave_type where name='" . $leaveTypeName . "'";
    $resultSelect = mysqli_query($conn, $sql1);
    $rows_count   = mysqli_num_rows($resultSelect);
    
    if ($rows_count <= 0) {
        $sqlInsert    = "INSERT INTO leave_type (name,description,created_on,updated_on,
            created_by,updated_by) VALUES ('".$leaveTypeName."','".$description."',
            UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";

        $resultInsert = mysqli_query($conn, $sqlInsert);
        echo $resultInsert;
    } else {
        echo "0";
    }
}

/* update designation information into database*/
if ($operation == "update") {
    if (isset($_POST['leaveTypeName'])) {
        $leaveTypeName = $_POST['leaveTypeName'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    /*Check that bed already exist for that particular bed number, roomid AND wardid*/
    $sqlSELECT    = "SELECT name FROM leave_type WHERE name='" . $leaveTypeName . "' 
        AND id != '$id'";
    $resultSELECT = mysqli_query($conn, $sqlSELECT);
    $rows_count   = mysqli_num_rows($resultSELECT);

    if ($rows_count == 0) {
        $sql    = "UPDATE leave_type SET name = '" . $leaveTypeName . "',
            description='" . $description . "',updated_on = UNIX_TIMESTAMP(),
            updated_by = '".$userId."' WHERE id = '" . $id . "'";
        $result = mysqli_query($conn, $sql);
        echo $result;
    }
    else{
        echo "0";
    }
}
/* set status Inactive for delete designation */
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sqlSelectEntitlement = "SELECT leave_type_id FROM entitlements WHERE leave_type_id = '".$id."'AND `status`  = 'A'";
    $resultEntitlement     = mysqli_query($conn, $sqlSelectEntitlement);
    $countEntitlement = mysqli_num_rows($resultEntitlement);

    $sqlSelectDeafaultLeave = "SELECT leave_type_id FROM default_leaves WHERE leave_type_id = '".$id."'";
    $resultDeafaultLeave     = mysqli_query($conn, $sqlSelectDeafaultLeave);
    $countDeafaultLeave = mysqli_num_rows($resultDeafaultLeave);

    $sqlSelectStaffLeave = "SELECT leave_type_id FROM staff_leaves WHERE leave_type_id = '".$id."'";
    $resultStaffLeave     = mysqli_query($conn, $sqlSelectStaffLeave);
    $countStaffLeave = mysqli_num_rows($resultStaffLeave);
    if($countEntitlement == 0 && $countDeafaultLeave == 0 && $countStaffLeave ==0){
        
        $sqlUpdate = "UPDATE `herp`.`leave_type` SET `status`='I' WHERE  `id`='".$id."'"; 
        $resultUpdate = mysqli_query($conn, $sqlUpdate);
        echo $resultUpdate;
    }else{
        echo "0";
    }  
}
/*show active data into the datatable  */

if ($operation == "show") {
    $sqlSelect    = "SELECT * FROM leave_type where status = 'A'";
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $totalrecords = mysqli_num_rows($resultSelect);
    
    $rows = array();
    while ($rUpdate = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $rUpdate;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/* show Inactive data into datatable on checked case*/
if ($operation == "checked") {
    $sqlSelect    = "SELECT * FROM leave_type where status = 'I'";
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $totalrecords = mysqli_num_rows($resultSelect);
    
    $rows = array();
    while ($rUpdate = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $rUpdate;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/* set status active for restore designation */
if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $sql    = "UPDATE leave_type SET status= 'A'  where  id = " . $id . "";
    $result = mysqli_query($conn, $sql);
    echo "1";
}
?>