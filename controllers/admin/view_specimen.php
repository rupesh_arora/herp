<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	$operation = "";
	
	include 'config.php';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	else{
		
	}
	
	// for lab specimen search
	if ($operation == "searchSpecimen") {
		
		$getFromDateTimeStamp="";
		$getToDateTimeStamp ="";

		if (isset($_POST['getFromDateTimeStamp'])) {
			$getFromDateTimeStamp=$_POST["getFromDateTimeStamp"];
		}
		if (isset($_POST['getToDateTimeStamp'])) {
			$getToDateTimeStamp=$_POST["getToDateTimeStamp"];
		}
		if (isset($_POST['firstName'])) {
			$firstName=$_POST["firstName"];
		}
		if (isset($_POST['lastName'])) {
			$lastName=$_POST["lastName"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['patientId'])) {
			$patientId=$_POST["patientId"];
		}

		$isFirst = "false";

		$query= "SELECT lab_test_requests.id,lab_test_requests.patient_id,
	     lab_test_requests.visit_id,
	     lab_tests.test_code,
	     lab_tests.name,
	     lab_test_requests.cost,
	     specimens_types.type,
		 patient_specimen.lab_test_requested_id,
	     patient_specimen.description,
	     patient_specimen.created_on,
	     
	     lab_test_result.result_flag,
	     lab_test_result.report,
	     lab_test_result.id as lab_test_id,
	     patient_specimen.id as specimen_id,
	     CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as patient_name,
	     patients.mobile,
	     CONCAT(users.first_name, ' ', users.last_name) as consultant_name,
	     (SELECT value FROM configuration WHERE name = 'patient_prefix'	) AS patient_prefix
	     FROM patient_specimen
	     LEFT JOIN `lab_test_requests`  ON lab_test_requests.id = patient_specimen.lab_test_id
	     LEFT JOIN lab_tests ON lab_test_requests.lab_test_id = lab_tests.id 
	     LEFT JOIN visits ON lab_test_requests.visit_id = visits.id 
	     LEFT JOIN lab_test_request_component AS ltrc ON ltrc.lab_test_request_id = lab_test_requests.id 
	     LEFT JOIN users ON visits.consultant_id = users.id 
	     LEFT JOIN specimens_types ON specimens_types.id = lab_tests.specimen_id 
	     LEFT JOIN patients On patients.id = lab_test_requests.patient_id 
	     LEFT JOIN lab_test_result ON lab_test_result.specimen_id = patient_specimen.id";

			if($firstName != '' || $lastName != '' || $mobile != '' || $patientId != '' || ($getFromDateTimeStamp != '' && $getToDateTimeStamp != '')){
				$query .=" WHERE lab_test_requests.pay_status='paid' AND lab_test_requests.test_status='done'";
				$isFirst = "true";
			} 
			else{
				$query .=" WHERE ltrc.pay_status='paid' AND lab_test_requests.test_status='done' GROUP BY lab_test_requests.id";
			}

			if($firstName !=''){
				if($isFirst == "true"){//if value true then add and in the last
					$query .=" AND ";
				}
				$query .= " patients.first_name LIKE '%".$firstName."%'";
				$isFirst = "true";
			}
			if($lastName !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patients.last_name LIKE '%".$lastName."%'";
				$isFirst = "true";
			}
			if($mobile !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patients.mobile = ".$mobile."";
				$isFirst = "true";
			}
			if($patientId !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patients.id = ".$patientId."";
				$isFirst = "true";
			}
			
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patient_specimen.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND UNIX_TIMESTAMP('".$getToDateTimeStamp." 23:59:59') ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp ==''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patient_specimen.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND CURRENT_TIMESTAMP() ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp =='' AND $getToDateTimeStamp !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patient_specimen.created_on BETWEEN '0000000000' AND CURRENT_TIMESTAMP() ";
				$isFirst = "true";
			}

			
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);
	}
	
	// for radiology specimen search
	if ($operation == "searchSpecimenRadiology") {
		
		$getFromDateTimeStamp="";
		$getToDateTimeStamp ="";

		if (isset($_POST['getFromDateTimeStamp'])) {
			$getFromDateTimeStamp=$_POST["getFromDateTimeStamp"];
		}
		if (isset($_POST['getToDateTimeStamp'])) {
			$getToDateTimeStamp=$_POST["getToDateTimeStamp"];
		}
		if (isset($_POST['firstName'])) {
			$firstName=$_POST["firstName"];
		}
		if (isset($_POST['lastName'])) {
			$lastName=$_POST["lastName"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['patientId'])) {
			$patientId=$_POST["patientId"];
		}

		$isFirst = "false";

		$query= "SELECT radiology_test_request.id,radiology_test_request.patient_id,
					radiology_test_request.visit_id,
					radiology_tests.test_code,
					radiology_tests.name,
					radiology_test_request.cost,
					radiology_test_result.report,
					radiology_test_result.id as radiology_id,
					radiology_test_request.request_date,
					patients.first_name,
					patients.last_name,
					patients.mobile,
					patients.id,
	    		   (SELECT value FROM configuration WHERE name = 'patient_prefix'	) AS patient_prefix
					FROM radiology_test_request					
					LEFT JOIN radiology_tests ON radiology_test_request.radiology_test_id = radiology_tests.id 
				    LEFT JOIN patients On patients.id = radiology_test_request.patient_id 
					LEFT JOIN radiology_test_result ON radiology_test_result.request_id = radiology_test_request.id";

			if($firstName != '' || $lastName != '' || $mobile != '' || $patientId != '' || ($getFromDateTimeStamp != '' && $getToDateTimeStamp != '')){
				$query .=" WHERE radiology_test_request.pay_status='paid' AND radiology_test_request.test_status='done'";
				$isFirst = "true";
			} 
			else{
				$query .=" WHERE radiology_test_request.pay_status='paid' AND radiology_test_request.test_status='done'";
			}

			if($firstName !=''){
				if($isFirst == "true"){//if value true then add and in the last
					$query .=" AND ";
				}
				$query .= " patients.first_name LIKE '%".$firstName."%'";
				$isFirst = "true";
			}
			if($lastName !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patients.last_name LIKE '%".$lastName."%'";
				$isFirst = "true";
			}
			if($mobile !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patients.mobile = ".$mobile."";
				$isFirst = "true";
			}
			if($patientId !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patients.id = ".$patientId."";
				$isFirst = "true";
			}
			
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patient_specimen.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND UNIX_TIMESTAMP('".$getToDateTimeStamp." 23:59:59') ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp ==''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patient_specimen.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND CURRENT_TIMESTAMP() ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp =='' AND $getToDateTimeStamp !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patient_specimen.created_on BETWEEN '0000000000' AND CURRENT_TIMESTAMP() ";
				$isFirst = "true";
			}
			
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);
	}
	
	// for forensic specimen search
	if ($operation == "searchSpecimenForensic") {
		
		$getFromDateTimeStamp="";
		$getToDateTimeStamp ="";

		if (isset($_POST['getFromDateTimeStamp'])) {
			$getFromDateTimeStamp=$_POST["getFromDateTimeStamp"];
		}
		if (isset($_POST['getToDateTimeStamp'])) {
			$getToDateTimeStamp=$_POST["getToDateTimeStamp"];
		}
		if (isset($_POST['firstName'])) {
			$firstName=$_POST["firstName"];
		}
		if (isset($_POST['lastName'])) {
			$lastName=$_POST["lastName"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['patientId'])) {
			$patientId=$_POST["patientId"];
		}

		$isFirst = "false";

		$query= "SELECT forensic_test_request.id  AS forensic_test_id,forensic_test_request.patient_id,
					forensic_test_request.visit_id,
					forensic_tests.test_code,
					forensic_tests.name,
					forensic_tests.range as rangeNormal,
					
					forensic_test_request.cost,
					specimens_types.type,
					patient_forensic_specimen.description,
					patient_forensic_specimen.created_on,

					forensic_test_result.result_flag,
					forensic_test_result.report,
					forensic_test_result.id,
					patient_forensic_specimen.id as specimen_id,
					patients.first_name,
					patients.last_name,
					patients.mobile,
					patients.id,
	    		   (SELECT value FROM configuration WHERE name = 'patient_prefix'	) AS patient_prefix
					FROM patient_forensic_specimen
					LEFT JOIN `forensic_test_request`  ON forensic_test_request.id = patient_forensic_specimen.forensic_test_id
					LEFT JOIN forensic_tests ON forensic_test_request.forensic_test_id = forensic_tests.id 
					LEFT JOIN specimens_types ON specimens_types.id = forensic_tests.specimen_id LEFT JOIN patients On 
					patients.id = forensic_test_request.patient_id 
					LEFT JOIN forensic_test_result ON forensic_test_result.specimen_id = patient_forensic_specimen.id";

			if($firstName != '' || $lastName != '' || $mobile != '' || $patientId != '' || ($getFromDateTimeStamp != '' && $getToDateTimeStamp != '')){
				$query .=" WHERE forensic_test_request.pay_status='paid' AND forensic_test_request.test_status='done'";
				$isFirst = "true";
			} 
			else{
				$query .=" WHERE forensic_test_request.pay_status='paid' AND forensic_test_request.test_status='done'";
			}

			if($firstName !=''){
				if($isFirst == "true"){//if value true then add and in the last
					$query .=" AND ";
				}
				$query .= " patients.first_name LIKE '%".$firstName."%'";
				$isFirst = "true";
			}
			if($lastName !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patients.last_name LIKE '%".$lastName."%'";
				$isFirst = "true";
			}
			if($mobile !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patients.mobile = ".$mobile."";
				$isFirst = "true";
			}
			if($patientId !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patients.id = ".$patientId."";
				$isFirst = "true";
			}
			
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patient_forensic_specimen.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND UNIX_TIMESTAMP('".$getToDateTimeStamp." 23:59:59') ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp ==''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patient_forensic_specimen.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND CURRENT_TIMESTAMP() ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp =='' AND $getToDateTimeStamp !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patient_forensic_specimen.created_on BETWEEN '0000000000' AND CURRENT_TIMESTAMP() ";
				$isFirst = "true";
			}
			
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		//echo $query;
		print json_encode($rows);
	}

	if ($operation == "visitDetailSpecimen") {
		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		$query= "SELECT lab_test_requests.patient_id,lab_test_requests.visit_id,lab_tests.name as lab_test_name,     
	     specimens_types.type,lab_test_result.result,lab_test_result.result_flag,lab_test_result.report,
	     patient_specimen.id as specimen_id,CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as patient_name,
	     patients.mobile,CONCAT(users.first_name, ' ', users.last_name) as consultant_name,
	     (SELECT value FROM configuration WHERE name = 'visit_prefix') AS prefix,
	     (SELECT value FROM configuration WHERE name = 'patient_prefix'	) AS patient_prefix		   
	     FROM patient_specimen
	     LEFT JOIN `lab_test_requests`  ON lab_test_requests.id = patient_specimen.lab_test_id
	     LEFT JOIN lab_tests ON lab_test_requests.lab_test_id = lab_tests.id 
	     LEFT JOIN visits ON lab_test_requests.visit_id = visits.id 
	     LEFT JOIN users ON visits.consultant_id = users.id 
	     LEFT JOIN specimens_types ON specimens_types.id = lab_tests.specimen_id 
	     LEFT JOIN patients On patients.id = lab_test_requests.patient_id 
	     LEFT JOIN lab_test_result ON lab_test_result.specimen_id = patient_specimen.id
	     WHERE lab_test_requests.id= '".$id."'";

	    $result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		print json_encode($rows);
	}

	if ($operation == "visitDetailRadiology") {
		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		$query= "SELECT radiology_test_request.patient_id,
					radiology_test_request.visit_id,
					radiology_tests.test_code,
					radiology_tests.name as test_name,
					radiology_test_request.cost,
					radiology_test_result.report,
					radiology_test_result.id as radiology_id,
					radiology_test_request.request_date,
					CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as patient_name,
					CONCAT(users.first_name, ' ', users.last_name) as consultant_name,
					patients.mobile,
					patients.id,
	    		   (SELECT value FROM configuration WHERE name = 'patient_prefix'	) AS patient_prefix,
					(SELECT value FROM configuration WHERE name = 'visit_prefix') AS prefix

					FROM radiology_test_request
					
					LEFT JOIN radiology_tests ON radiology_test_request.radiology_test_id = radiology_tests.id 
				    LEFT JOIN patients On patients.id = radiology_test_request.patient_id 
					LEFT JOIN radiology_test_result ON radiology_test_result.request_id = radiology_test_request.id
					LEFT JOIN visits ON radiology_test_request.visit_id = visits.id
	     			LEFT JOIN users ON visits.consultant_id = users.id 
	     			WHERE radiology_test_result.id= '".$id."'";

			
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);
	}


	if ($operation == "visitDetailForensic") {
		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		$query= "SELECT forensic_test_request.patient_id,
					forensic_test_request.visit_id,
					forensic_tests.test_code,
					forensic_tests.name,
					forensic_tests.range as rangeNormal,
					
					forensic_test_request.cost,
					specimens_types.type,
					patient_forensic_specimen.description,
					patient_forensic_specimen.created_on,

					forensic_test_result.result_flag,
					forensic_test_result.report,
					forensic_test_result.id,
					patient_forensic_specimen.id as specimen_id,
					CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as patient_name,
					patients.mobile,
					CONCAT(users.first_name, ' ', users.last_name) as consultant_name,
	    		   (SELECT value FROM configuration WHERE name = 'patient_prefix'	) AS patient_prefix,
					(SELECT value FROM configuration WHERE name = 'visit_prefix') AS prefix
					FROM patient_forensic_specimen
					LEFT JOIN `forensic_test_request`  ON forensic_test_request.id = patient_forensic_specimen.forensic_test_id
					LEFT JOIN forensic_tests ON forensic_test_request.forensic_test_id = forensic_tests.id 
					LEFT JOIN specimens_types ON specimens_types.id = forensic_tests.specimen_id LEFT JOIN patients On 
					patients.id = forensic_test_request.patient_id 
					LEFT JOIN forensic_test_result ON forensic_test_result.specimen_id = patient_forensic_specimen.id

					LEFT JOIN visits ON forensic_test_request.visit_id = visits.id 
	     			LEFT JOIN users ON visits.consultant_id = users.id 
					WHERE forensic_test_request.id = '".$id."' ";

		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);		
	}

	// for lab specimen search
	if ($operation == "searchIPDSpecimen") {
		
		$getFromDateTimeStamp="";
		$getToDateTimeStamp ="";

		if (isset($_POST['getFromDateTimeStamp'])) {
			$getFromDateTimeStamp=$_POST["getFromDateTimeStamp"];
		}
		if (isset($_POST['getToDateTimeStamp'])) {
			$getToDateTimeStamp=$_POST["getToDateTimeStamp"];
		}
		if (isset($_POST['firstName'])) {
			$firstName=$_POST["firstName"];
		}
		if (isset($_POST['lastName'])) {
			$lastName=$_POST["lastName"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['patientId'])) {
			$patientId=$_POST["patientId"];
		}

		$isFirst = "false";

		$query= "SELECT ipd_lab_test_requests.id,ipd_lab_test_requests.patient_id,
		     ipd_lab_test_requests.ipd_id,
		     lab_tests.test_code,
		     lab_tests.name,
		     ipd_lab_test_requests.cost,
		     specimens_types.type,
			  ipd_patient_specimen.ipd_lab_test_requested_id,
		     ipd_patient_specimen.description,
		     ipd_patient_specimen.created_on,
		     
		     ipd_lab_test_result.result_flag,
		     ipd_lab_test_result.report,
		     ipd_lab_test_result.id as lab_test_id,
		     ipd_patient_specimen.id as specimen_id,
		     CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as patient_name,
		     patients.mobile,
		     (SELECT value FROM configuration WHERE name = 'patient_prefix'	) AS patient_prefix
		     FROM ipd_patient_specimen
		     LEFT JOIN `ipd_lab_test_requests`  ON ipd_lab_test_requests.id = ipd_patient_specimen.lab_test_id
		     LEFT JOIN lab_tests ON ipd_lab_test_requests.lab_test_id = lab_tests.id 
		     LEFT JOIN ipd_registration ON ipd_lab_test_requests.ipd_id = ipd_registration.id 
		     LEFT JOIN specimens_types ON specimens_types.id = lab_tests.specimen_id 
		     LEFT JOIN patients On patients.id = ipd_lab_test_requests.patient_id 
		     LEFT JOIN ipd_lab_test_result ON ipd_lab_test_result.ipd_specimen_id = ipd_patient_specimen.id";

			if($firstName != '' || $lastName != '' || $mobile != '' || $patientId != '' || ($getFromDateTimeStamp != '' && $getToDateTimeStamp != '')){
				$query .=" WHERE ipd_lab_test_requests.pay_status='paid' AND ipd_lab_test_requests.test_status='done'";
				$isFirst = "true";
			} 
			else{
				$query .=" WHERE ipd_lab_test_requests.pay_status='paid' AND ipd_lab_test_requests.test_status='done'";
			}

			if($firstName !=''){
				if($isFirst == "true"){//if value true then add and in the last
					$query .=" AND ";
				}
				$query .= " patients.first_name LIKE '%".$firstName."%'";
				$isFirst = "true";
			}
			if($lastName !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patients.last_name LIKE '%".$lastName."%'";
				$isFirst = "true";
			}
			if($mobile !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patients.mobile = ".$mobile."";
				$isFirst = "true";
			}
			if($patientId !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " patients.id = ".$patientId."";
				$isFirst = "true";
			}
			
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " ipd_patient_specimen.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND UNIX_TIMESTAMP('".$getToDateTimeStamp." 23:59:59') ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp ==''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " ipd_patient_specimen.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND CURRENT_TIMESTAMP() ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp =='' AND $getToDateTimeStamp !=''){
				if($isFirst == "true"){
					$query .=" AND ";
				}
				$query .= " ipd_patient_specimen.created_on BETWEEN '0000000000' AND CURRENT_TIMESTAMP() ";
				$isFirst = "true";
			}

		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);
	}

	if ($operation == "visitIPDDetailSpecimen") {
		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		$query= "SELECT ipd_lab_test_requests.patient_id,ipd_lab_test_requests.ipd_id,lab_tests.name as lab_test_name,     
	     specimens_types.type,ipd_lab_test_result.result,ipd_lab_test_result.result_flag,ipd_lab_test_result.report,
	     ipd_patient_specimen.id as specimen_id,CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as patient_name,
	     (SELECT value FROM configuration WHERE name = 'visit_prefix') AS prefix,
	     (SELECT value FROM configuration WHERE name = 'patient_prefix'	) AS patient_prefix		   
	     FROM ipd_patient_specimen
	     LEFT JOIN `ipd_lab_test_requests`  ON ipd_lab_test_requests.id = ipd_patient_specimen.lab_test_id
	     LEFT JOIN lab_tests ON ipd_lab_test_requests.lab_test_id = lab_tests.id 
	     LEFT JOIN ipd_registration ON ipd_lab_test_requests.ipd_id = ipd_registration.id 
	     LEFT JOIN specimens_types ON specimens_types.id = lab_tests.specimen_id 
	     LEFT JOIN patients On patients.id = ipd_lab_test_requests.patient_id 
	     LEFT JOIN ipd_lab_test_result ON ipd_lab_test_result.ipd_specimen_id = ipd_patient_specimen.id
	     WHERE ipd_lab_test_requests.id= '".$id."'";

	    $result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		print json_encode($rows);
	}
?>