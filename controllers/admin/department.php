<?php
/*
 * File Name    :   Department.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   29 Sept, 2015
 * Description  :   This page is use for add department
 */

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

$operation      = "";
$DepartmentName = "";
$description    = "";
$date 			= new DateTime();
$createdate     = new DateTime();
include 'config.php'; // include database connection file

if (isset($_POST['operation'])) { // define operation value from js file
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

// upload excel file
if (isset($_FILES["file"]["type"]))  {// use for import data from csv
	
	$temporary       = explode(".", $_FILES["file"]["name"]);
	$file_extension = end($temporary);
	/*if ($file_extension == "csv") {
		if ($_FILES["file"]["error"] > 0) {
			echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
		} else {
			if($_FILES["file"]["size"] > 0)
			{
				$filename = $_FILES["file"]["tmp_name"];
				$file = fopen($filename, "r");
				$countCSV = 0;
				$subInsertValues = '';
				$sqlInsert ="INSERT IGNORE INTO department(department,description,created_on,updated_on,created_by,updated_by) VALUES ";
				while (($emapData = fgetcsv($file, 10000, ",")) != FALSE)
				{
					//if there are more than 2 column then restrict it.	
					if (sizeof($emapData) != 2) {
					 	echo "You must have two column in your file i.e department and description";
					 	exit();
					}

					//check whether first column name should be designation and second should be description
					if ($countCSV == 0) {
						if(strtolower(reset($emapData)) !="department"){
							echo "First column should be department";
							exit();
						}
						if(strtolower(end($emapData)) !="description"){
							echo "Second column should be description";
							exit();
						}						
					}

					//data can't be null in first row else row it may be null
					if ($countCSV == 1) {
						if (empty($emapData[0])) {
							echo "Data can't be null in department column";
							exit();
						}
					}
					reset($emapData);
					$firstIndex = key($emapData);
					end($emapData);
					$lastIndex = key($emapData);

					//check whether data not be null in the file
					if(($emapData[0]) != "") {

						if ($countCSV > 0) {

							if ($countCSV >1) {
								$subInsertValues.= ',';
							}		

							$subInsertValues.= "('".$emapData[$firstIndex]."','".$emapData[$lastIndex]."',	UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
						}
					}					

					$countCSV++;						
				}
				$sqlInsert = $sqlInsert.$subInsertValues;
				$result = mysqli_query($conn,$sqlInsert);
				if($result)
				{
					echo $result;
				}
				else{
					echo "";
				}
				fclose($file);
			}
		} 	
	}
	else if($file_extension == "xls"){
		ini_set("display_errors",1);
		$filename = $_FILES['file']['tmp_name'];
		require_once 'excel_reader2.php';
		$data = new Spreadsheet_Excel_Reader($filename);
		$countSheets = count($data->sheets);
		for($i=0;$i<$countSheets;$i++) // Loop to get all sheets in a file.
		{ 
			$countSheetsCells =0;
			if(isset($data->sheets[$i]['cells'])){
				$countSheetsCells = count($data->sheets[$i]['cells']);
			}
			if($countSheetsCells > 0 && $countSheetsCells) // checking sheet not empty
			{
				$sql = "INSERT IGNORE INTO department(department,description,created_on,updated_on,created_by,updated_by) VALUES ";
				$countXls = 0;				
				$subInsert = '';
				for($j=1;$j<=$countSheetsCells;$j++) // loop used to get each row of the sheet
				{ 
					Check size of array
				  	if (sizeof($data->sheets[$i]['cells'][$j]) != 2) {
				  		echo "You must have two column in your file i.e department and description";
					 	exit();
				  	}
				  	
				  	Geeting the index of Excel file data like in which it is A or B or C	
				  	$indexOfExcelFile = array_keys($data->sheets[$i]['cells'][$j]);
					
					$name = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[0]];
				
					$description = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[1]];
					

					Check the first header and second header
					if ($countXls == 0) {
						if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[0]]) !="department"){
							echo "First column should be department";
							exit();
						}
						if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[1]]) !="description"){
							echo "Second column should be description";
							exit();
						}
					}

					//data can't be null in the very first row of file
					if ($countXls == 1) {
						if (empty($name)) {
							echo "Data can't be null in department column";
							exit();
						}
					}
					 
					if ($countXls>0) {

						//check whether data not be null in file
						if($name != '' ) {

							if ($countXls >1) {
								$subInsert .=',';
							}

						 	$subInsert .=" ('$name','$description',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
						}
					}
					$countXls++;		
				}

				$sql = $sql.$subInsert;
				$result = mysqli_query($conn,$sql);

				if($result)	{
					echo $result;
				}
				else{
					echo "";
				}
			}
		}		
	}*/
	 if($file_extension == "xlsx" || $file_extension == "xls" || $file_extension == "csv"){
		// get file name
		$filename = $_FILES['file']['tmp_name'];
		
		//import php script
		require_once 'PHPExcel.php';
		
		// 
		try {
			$inputFileType = PHPExcel_IOFactory::identify($filename);  // get type of file name
			$objReader = PHPExcel_IOFactory::createReader($inputFileType); //create object of read file
			$objPHPExcel = $objReader->load($filename);  //load excel into the object
		} catch (Exception $e) {
			die('Error loading file "' . pathinfo($filename, PATHINFO_BASENAME) 
			. '": ' . $e->getMessage());
		}

		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); // get sheet 1 of excel file
		$highestRow = $sheet->getHighestRow(); // get heighest row
		$highestColumn = $sheet->getHighestColumn(); // get heighest column
		
		$headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE); // get heading in first row
		if (sizeof($headings[0]) < 2) {
			echo "You must have two column in your file i.e department and description";
			exit();
		}
		//reset($headings[0]) it is the first column
		if(trim(strtolower(reset($headings[0])) !="department")){
			echo "First column should be department";
			exit();
		}
		//end($headings[0]) it is the last column
		if(trim(strtolower(end($headings[0])) !="description")){
			echo "Second column should be description";
			exit();
		}
		$sql = "INSERT IGNORE INTO department(department,description,created_on,updated_on,created_by,updated_by) VALUES ";
		$countXlsx = 0;
		$subInsertXlsx = '';								
		for ($row = 2; $row <= $highestRow; $row++) {
			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL, TRUE, FALSE);
			
			// combine data in array accoding to heading
			$rowData[0] = array_combine($headings[0], $rowData[0]);
			//var_dump($rowData[0]);
			
			$key  = array_keys($rowData[0]);//getting the ondex name

			$name = trim(mysql_real_escape_string($rowData[0][$key[0]]));
			$description = trim(mysql_real_escape_string($rowData[0][$key[1]]));
			
			// data shouldn't be null in the very first row of file
			if ($countXlsx == 0) {

				if (empty($name)) {
					echo "Data can't be null in department column";
					exit();
				}
			}

			//not save null data in category column
			if ($name != "") {	
				// insert data into table
				if ($countXlsx>0) {

					$subInsertXlsx.=',';
				}
				
				$subInsertXlsx.="('$name','$description',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
			}
					
			$countXlsx++;			
		}

		$sql = $sql.$subInsertXlsx;
		$result = mysqli_query($conn,$sql);

		if($result) {
			echo $result;
		}
		else{
			echo "";
		}
	}
	else{
		echo "invalid file";
	}
}

/* save department information into database*/
if ($operation == "save") {
    if (isset($_POST['DepartmentName'])) {
        $DepartmentName = $_POST['DepartmentName'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    
    
    $sql1         = "SELECT department from department where department='" . $DepartmentName . "'";
    $resultSelect = mysqli_query($conn, $sql1);
    $rows_count   = mysqli_num_rows($resultSelect);
    
    if ($rows_count <= 0) {
        
        $sql    = "INSERT INTO department(department,description,created_on,updated_on,created_by,updated_by) VALUES('" . $DepartmentName . "','" . $description . "',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
        $result = mysqli_query($conn, $sql);
        echo $result;
    } else {
        echo "0";
    }
}

/* update department information into database*/
if ($operation == "update") {
    if (isset($_POST['DepartmentName'])) {
        $DepartmentName = $_POST['DepartmentName'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    
    $sql    = "UPDATE department SET department= '" . $DepartmentName . "',description='" . $description . "',updated_on= UNIX_TIMESTAMP(),updated_by='".$userId."' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}
/* set status Inactive for delete department */
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    
    $sql    = "UPDATE department SET status= 'I'  WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}

/*show active data into the datatable  */
if ($operation == "show") {
    
    $query        = "SELECT * from department WHERE status = 'A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/* show Inactive data into datatable on checked case*/

if ($operation == "checked") {
    
    $query        = "SELECT * from department WHERE status = 'I'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/* set status active for restore department */

if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $sql    = "UPDATE department SET status= 'A'  WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}
?>