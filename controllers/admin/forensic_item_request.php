<?php
	$operation = "";
	$itemName = "";
	$id = "";
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	
	
	include 'config.php';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	
	
	if($operation == "search"){		
		
		if (isset($_POST['itemName'])) {
			$itemName=$_POST["itemName"];
		}
		
		/*This isset for OT requst*/
		if (isset($_POST['serachOT'])) {
			$query ="SELECT i.id,i.name,stc.quantity FROM items AS i 
			LEFT JOIN item_categories AS ic ON ic.id = i.item_category_id 
			LEFT JOIN item_stock AS stc ON stc.item_id = i.id  
			WHERE i.status = 'A' AND ic.inventory_type_id IN(SELECT id FROM inventory_type WHERE type = 'OT') AND i.name LIKE '%".$itemName."%'";
		}
		else{
			$query ="SELECT i.id,i.name,stc.quantity FROM items AS i 
			LEFT JOIN item_categories AS ic ON ic.id = i.item_category_id 
			LEFT JOIN item_stock AS stc ON stc.item_id = i.id  
			WHERE i.status = 'A' AND ic.inventory_type_id IN(SELECT id FROM inventory_type WHERE type = 'forensic') AND i.name LIKE '%".$itemName."%'";
		}
		
		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}

		print json_encode($rows);
	}

	if($operation == "save"){		
		
		if (isset($_POST['requestItem'])) {
			$requestItem=json_decode($_POST["requestItem"]);
		}
		$currentDate = $_POST['todayDate'];

		/*This set condition will be get from OT req*/
		if (isset($_POST['saveOT'])) {
			$query ="SELECT id FROM inventory_type WHERE type = 'OT'";
		}
		else {
			$query ="SELECT id FROM inventory_type WHERE type = 'forensic'";
		}

		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$inventoryType = $r['id'];
		}

		$sql = "INSERT INTO item_request(inventory_type_id, item_id, ordered_quantity,ordered_by,ordered_date) VALUES ";
		$subQry = '';
		$count = 0;
		foreach($requestItem as $value) {
						
			$quantity = $value-> quantity;
			$itemId = $value-> itemId;
			
			if ($count > 0) {
				$subQry.= ',';
			}
			$subQry.="('".$inventoryType."', '".$itemId."','".$quantity."','".$userId."','".$currentDate."')";		
			$count++;
		}
		$compQry = $sql.$subQry;
		$result= mysqli_query($conn,$compQry);
		echo $result;
	}
?>