<?php
/*File Name  :   put_orders.php
Company Name :   Qexon Infotech
Created By   :   Kamesh Pathak
Created Date :   16th FEB, 2016
Description  :   This page manages all the orders of inventory*/

session_start(); //start session

if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
	exit();
}
/*include config file*/
include 'config.php';

$operation = "";

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
} //nothing


if($operation == "search"){		
		
	if (isset($_POST['sellerName'])) {
		$sellerName=$_POST["sellerName"];
	}	
	
	$query ="SELECT * FROM seller AS s 
	WHERE s.status = 'A' AND s.name LIKE '%".$sellerName."%'";	
	
	$result=mysqli_query($conn,$query);
	$rows = array();
	while($r = mysqli_fetch_assoc($result)) {
		$rows[] = $r;
	}

	print json_encode($rows);
}

if($operation == "searchItem"){		
		
	if (isset($_POST['itemName'])) {
		$itemName=$_POST["itemName"];
	}	
	
	/*This will come from stock*/
	if (isset($_POST['receivedItem'])) {
		

		$query ="SELECT items.id, items.name ,received_orders.quantity,received_orders.exp_date
		FROM received_orders
		LEFT JOIN items on items.id = received_orders.item_id
		WHERE items.status = 'A' AND items.name LIKE '%".$itemName."%' ";
	}
	else{
		$query ="SELECT * FROM items AS s 
		WHERE s.status = 'A' AND s.name LIKE '%".$itemName."%'";
	}			
	
	$result=mysqli_query($conn,$query);
	$rows = array();
	while($r = mysqli_fetch_assoc($result)) {
		$rows[] = $r;
	}
	print json_encode($rows);
}	

/*operation to save data*/
if ($operation == "saveData") {
	
	if (isset($_POST['sellerId'])) {
        $sellerId = $_POST["sellerId"];
    }
    if (isset($_POST['date'])) {
        $date = $_POST["date"];
    }
	
    if (isset($_POST['notes'])) {
        $notes = $_POST["notes"];
    }
    if (isset($_POST['bigArray'])) {
        $bigArray = json_decode($_POST["bigArray"]);
    }
    if (isset($_POST['sellerEmail'])) {
    	$sellerEmail = $_POST['sellerEmail'];
    }
    if (isset($_POST['sellerName'])) {
    	$sellerName = $_POST['sellerName'];
    }

    $html = "<html><body><table style='border-collapse: collapse;border: 1px solid black;' width='100%'>
    <thead><tr><th width='10%' style='border: 1px solid black;'>S.No</th>
    <th width='60%' style='border: 1px solid black;'>Item</th>
    <th width='30%' style='border: 1px solid black;'>Quantity</th></tr></thead><tbody>";

    $headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'To: "'.$sellerName.'" <"'.$sellerEmail.'">' . "\r\n";
	$headers .= 'From: "'.$_POST['hospitalName'].'" <"'.$_POST['hospitalEmail'].'">' . "\r\n";	 
    
	$sql      = "INSERT INTO orders (seller_id,`date`,notes,ordered_by)
	VALUES('".$sellerId."',unix_timestamp(CONCAT('".$date." ',CURTIME())),'".$notes."','".$userId."')";	
	$result = mysqli_query($conn, $sql);
	$lastId = mysqli_insert_id($conn);
	$sqlOrder = "INSERT INTO `order-item` (order_id,item_id,ordered_quantity,created_by,created_on) VALUES";
	$count = 0;
	$subQuery = '';				
    /*Check if data from data table is null or not*/
    if (!empty($bigArray)) {
        
        foreach ($bigArray as $value) {
            
            $itemId    = $value->itemId;
            $quantity  = $value->quantity;
            //$cost      = $value->cost;
			$itemName  = $value->itemName;
            if ( $count >0) {
				$subQuery .=",";
			}
            $subQuery .="('".$lastId."','".$itemId."','".$quantity."','".$userId."',UNIX_TIMESTAMP())";            
            $count++;

            $html.="<tr><td style='text-align: center;border: 1px solid black;'>".$count."</td><td style='text-align: center;border: 1px solid black;'>".$itemName."</td><td style='text-align: center;border: 1px solid black;'>".$quantity."</td></tr>";
        }

        $html.="</tbody></table></body></html>";
        
		/*require_once(dirname(__FILE__).'/html2pdf/html2pdf.class.php');
		require_once('class.phpmailer.php');
		$pdf=new HTML2PDF('P','A4','fr');

		if(ini_get('magic_quotes_gpc')=='1')
		$html=stripslashes($html);
		$pdf->WriteHTML($html);

		$mail = new PHPMailer(); // defaults to using php "mail()"
		$body = "Please find the attachment below to get the order details";

		 $mail->AddReplyTo("tushar@qexon.com","Tushar Gupta");
		$mail->SetFrom('tushar@qexon.com', 'Tushar Gupta');

		//$address = "tushar@qexon.com";
		$mail->AddAddress($sellerEmail, $sellerName);       
		$mail->Subject    = "A new order for you from '".$_POST['hospitalName']."'";       
		$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

		$mail->MsgHTML($body);
		//documentation for Output method here: http://www.fpdf.org/en/doc/output.htm       
		$pdf->Output("Test Invoice.pdf","F");
		//$pdf->Output("Test Invoice.pdf","S");
		$path = "Walter Lernt Invoice.pdf";

		$mail->AddAttachment($path, '', $encoding = 'base64', $type = 'application/pdf');

		global $message;
		if(!$mail->Send()) {
		  $message =  "File could not be send. Mailer Error: " . $mail->ErrorInfo;
		} else {
		  $message = "File sent!";
		}*/
		
        mail($sellerEmail, "A new order for you from '".$_POST['hospitalName']."'", $html, $headers);//mail to ,subject,mail body,


        $completeOrderQry = $sqlOrder.$subQuery;
		//echo $completeOrderQry;
        $resultOrder  = mysqli_query($conn, $completeOrderQry);
		echo $lastId;
    }
}
?>