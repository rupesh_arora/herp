<?php
/*File Name  :   triage_general.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   4th Jan, 2016
Description  :   This page manages triage data in consultant module*/
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	$operation="";

/*include config file*/
	include 'config.php';

	$date = new DateTime();

/*checking operation set or not*/
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	else{}//nothing

	/*operation to get data and show it*/
	if($operation == "showData"){

		if (isset($_POST['visitId'])) {
			$visitId=$_POST['visitId'];
		}

		$sqlSelect="SELECT triage.weight, triage.temperature, triage.height, triage.pulse, 
		triage.blood_pressure, triage.head_circum, triage.body_surface_area, triage.respiration_rate, 
		triage.oxygen_saturation, triage.heart_rate, triage.bmi, triage.notes FROM triage
		WHERE triage.visit_id= ".$visitId."";

		$resultSelect=mysqli_query($conn,$sqlSelect);
		$rows_count= mysqli_num_rows($resultSelect);
		if($rows_count > 0){
		    $rows = array();
		    while($r = mysqli_fetch_assoc($resultSelect)) {
			    $rows[] = $r;
		    }
		   print json_encode($rows);
		}
		else{
		   echo "0";
		}       
	}	 

	/*operation to save data*/
	if($operation == "save")			
	{	
		if (isset($_POST['visitId'])) {
			$visitId=$_POST['visitId'];
		}
		if (isset($_POST['patientId'])) {
			$patientId=$_POST['patientId'];
		}
		if (isset($_POST['weight'])) {
			$weight=$_POST['weight'];
		}
		if (isset($_POST['temprature'])) {
			$temprature=$_POST['temprature'];
		}
		if (isset($_POST['height'])) {
			$height=$_POST['height'];
		}
		if (isset($_POST['pulse'])) {
			$pulse=$_POST['pulse'];
		}
		if (isset($_POST['bloodPressure'])) {
			$bloodPressure=$_POST['bloodPressure'];
		}
		if (isset($_POST['headCircumference'])) {
			$headCircumference=$_POST['headCircumference'];
		}
		if (isset($_POST['bodySurfaceArea'])) {
			$bodySurfaceArea=$_POST['bodySurfaceArea'];
		}
		if (isset($_POST['respirationRate'])) {
			$respirationRate=$_POST['respirationRate'];
		}
		if (isset($_POST['oxygenSaturation'])) {
			$oxygenSaturation=$_POST['oxygenSaturation'];
		}	
		if (isset($_POST['heartRate'])) {
			$heartRate=$_POST['heartRate'];
		}
		if (isset($_POST['bmi'])) {
			$bmi=$_POST['bmi'];
		}
		if (isset($_POST['notes'])) {
			$notes=$_POST['notes'];
		}	

		/*Count no. of rows for that particular visit*/
		$sql_select = "SELECT COUNT(*) FROM triage WHERE visit_id=".$visitId."";  
		$result_select = mysqli_query($conn,$sql_select);  
		while ($row=mysqli_fetch_row($result_select))
		{
			$count = $row[0];
		}
		
		if($count == 0)
		{//if count equals to zero save data
			
			$sql = "INSERT INTO triage(visit_id, patient_id, weight, temperature, height, pulse, blood_pressure, head_circum,
			body_surface_area, respiration_rate, oxygen_saturation, heart_rate, bmi, notes, created_on, created_by)
			VALUES('".$visitId."', '".$patientId."','".$weight."', '".$temprature."', '".$height."', '".$pulse."', '".$bloodPressure."', 
			'".$headCircumference."', '".$bodySurfaceArea."', '".$respirationRate."', '".$oxygenSaturation."', 
			'".$heartRate."', '".$bmi."', '".$notes."',".$date->getTimestamp().", '".$_SESSION['globaluser']."')";
			$result= mysqli_query($conn,$sql);

			if ($result==1) {
				$sqlUpdate = "UPDATE visits SET triage_status='Taken' WHERE id='".$visitId."'";
				$resultUpdate = mysqli_query($conn,$sqlUpdate);
				echo $result;
			}
		}
		else{//update data

			$sql = "UPDATE `triage` SET `weight`='".$weight."',`temperature`='".$temprature."',`height`='".$height."',`pulse`='".$pulse."',`blood_pressure`='".$bloodPressure."',`head_circum`='".$headCircumference."',
			`body_surface_area`='".$bodySurfaceArea."',`respiration_rate`='".$respirationRate."',`oxygen_saturation`='".$oxygenSaturation."',
			`heart_rate`='".$heartRate."',`bmi`='".$bmi."',`notes`='".$notes."',
			`created_on`=".$date->getTimestamp().",`created_by`='".$_SESSION['globaluser']."' WHERE `visit_id` = ".$visitId."";			

			$result= mysqli_query($conn,$sql); 
			if ($result==1) {
				$sqlUpdate = "UPDATE visits SET triage_status='Taken' WHERE id='".$visitId."'";
				$resultUpdate = mysqli_query($conn,$sqlUpdate);
				echo $result;
			}
		}				
	} 
?>