<?php
/*
 * File Name    :   insuance_plan.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre details form database
 */
$operation           = "";
$createdate          = new DateTime();
$date      = date("Y-m-d");
$createOn  = new DateTime();
  
include 'config.php'; //import databse connection file
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
	exit();
}

if (isset($_POST['operation'])) { // perform operation form js file
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
}


// opertaion form update
if ($operation == "saveDischargeDetails") // update data
{    
	 $ipdId = $_POST['ipdId'];
	 $dischargeDate = $_POST['dischargeDate'];
	 $dischargeAdvice = $_POST['dischargeAdvice'];
	 
	 $updateQuery = "UPDATE ipd_registration SET discharge_date = '".$dischargeDate."',discharge_advice = '".$dischargeAdvice."',
					updated_on = '".$createOn->getTimestamp()."',updated_by='".$userId."' WHERE id = ".$ipdId."";
	 $result = mysqli_query($conn,$updateQuery);
	 if($result) {
		 echo "1";
	 }
	 else {
		 echo "0";
	 }
}

/*operation to show inactive data*/
if ($operation == "showData") {

    if (isset($_POST['ipdId'])) {
        $ipdId = $_POST['ipdId'];
    }

    $query        = "SELECT ipd_registration.*,CONCAT(patients.salutation,' ',patients.first_name,' ' ,patients.last_name) AS name,patients.dob From ipd_registration
					LEFT JOIN patients ON patients.id = ipd_registration.patient_id
					WHERE ipd_registration.id =".$ipdId."";
                    
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    echo json_encode($rows);
}
?>