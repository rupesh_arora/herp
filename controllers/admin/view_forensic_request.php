<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	
	include 'config.php';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}	
	
	if($operation == "update")   
	{
		if (isset($_POST['lab_test_id'])) {
			$id=$_POST["lab_test_id"];
		}
		if (isset($_POST['specimen_description'])) {
			$specimenDescription=$_POST["specimen_description"];
		}
		$query="UPDATE forensic_test_request SET test_status = 'done',request_date=UNIX_TIMESTAMP() WHERE  id = '".$id."'";  
		$result=mysqli_query($conn,$query);
		
		if($result)
		{
			$query="INSERT INTO patient_forensic_specimen(forensic_test_id,description,created_on,created_by) VALUES (".$id.",'".$specimenDescription."',UNIX_TIMESTAMP(),".$_SESSION["globaluser"].")"; 
			$result=mysqli_query($conn,$query);
			echo $conn->insert_id;
		}
	}
	
	if($operation == "show"){
		
		$query= "SELECT forensic_test_request.patient_id,
		forensic_test_request.visit_id,
		forensic_tests.test_code,forensic_tests.name,
		forensic_test_request.cost,
		specimens_types.type,
		specimens_types.description,
		forensic_test_request.pay_status,
		patients.salutation , patients.first_name , 
		patients.last_name ,
		patients.mobile,
		patients.middle_name,
		patients.dob,
		patients.care_of,
		patients.address,patients.gender,
		patients.zip,patients.images_path,
		patients.care_contact,
		patients.email,
		visit_type.`type` AS visitType,
		forensic_test_request.id,
		(SELECT  locations.name as state FROM locations WHERE locations.location_id = patients.country_id) as country,
		(SELECT  locations.name as state FROM locations WHERE locations.location_id = patients.city_id) as city,
		(SELECT  locations.name as state FROM locations WHERE locations.location_id = patients.state_id) as state,
		(SELECT value FROM configuration WHERE name = 'patient_prefix') AS patient_prefix,
		(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visit_prefix
		FROM `forensic_test_request` 
		LEFT JOIN forensic_tests ON forensic_test_request.forensic_test_id = forensic_tests.id 
		LEFT JOIN specimens_types ON specimens_types.id = forensic_tests.specimen_id 
		LEFT JOIN patients On patients.id = forensic_test_request.patient_id  
		LEFT JOIN visits On visits.id = forensic_test_request.visit_id   
		LEFT JOIN visit_type On visit_type.id = visits.visit_type   
		WHERE forensic_test_request.pay_status = 'paid' AND forensic_test_request.status = 'A'
		AND forensic_test_request.test_status = 'pending'   AND forensic_test_request.request_type = 'internal'  
		ORDER BY ((DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%y-%m-%d')) = CURDATE()) desc,visits.visit_type= '3' desc,visits.id  asc";		
		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
	
	if($operation == "loadSpecimen")			
	{
		$sql = "SELECT id as value,type as text FROM specimens_types WHERE  status = 'A'";
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		echo json_encode($rows);	
	}
	
	if($operation == "loadLabTests")			
	{
		$sql = "SELECT id as value,name as text FROM forensic_tests WHERE  status = 'A'";
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		echo json_encode($rows);
	}
?>