<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
    $operation = "";
	$visitType ="";
	$description = "";
	$date = new DateTime();
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	if($operation == "save")			
	{
		if (isset($_POST['visitType'])) {
		$visitType=$_POST['visitType'];
		}
		if (isset($_POST['description'])) {
		$description=$_POST['description'];
		}
		
	
		$sql ="select user_id from session";
		$query= mysqli_query($conn,$sql);
		 while ($row=mysqli_fetch_row($query))
		{
		$sessionId = $row[0];
		}		
			
		$sql = "INSERT INTO visit_type(type,description,created_on,updated_on,created_by,updated_by) 
		VALUES('".$visitType."','".$description."',".$date->getTimestamp().",".$date->getTimestamp().",".$sessionId.",".$sessionId.")";
		$result= mysqli_query($conn,$sql);  
		echo $result;
		
	} 	
	if($operation == "update")			
	{
		if (isset($_POST['visitType'])) {
			$visitType=$_POST['visitType'];
		}
		if (isset($_POST['description'])) {
			$description=$_POST['description'];
		}
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		
	
		$sql = "UPDATE visit_type SET type= '".$visitType."',description='".$description."',updated_on=".$date->getTimestamp()." WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	if($operation == "delete")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		
	
		$sql = "UPDATE visit_type SET status= 'I'  WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}	
	
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		
		$sql = "UPDATE visit_type SET status= 'A' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	if($operation == "show"){
		
		$query= "select * from visit_type WHERE status = 'A'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 //print json_encode($rows);
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}	
	
	if($operation == "showInActive"){
		
		$query= "select * from visit_type WHERE status = 'I'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 //print json_encode($rows);
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
?>