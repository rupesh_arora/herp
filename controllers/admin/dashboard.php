<?php
/*
	* File Name    :   dashboard.php
	* Company Name :   Qexon Infotech
	* Created By   :   Tushar Gupta
	* Created Date :   30th dec, 2015
	* Description  :   This page show the dashboard details for hospital
	* Last Edited By : Subodh Kant
*/
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
include 'config.php'; // import database connection file

if (isset($_POST['operation'])) {		// getting the opearion value from js	
    $operation = $_POST["operation"];
} 

if ($operation == "showDashboard") {		// show the details of visits
	$currentDate = $_POST["currentTimeStamp"];
    $sql    = "SELECT count(*) as totalvisits,
                 count(CASE WHEN from_unixtime(created_on, '%Y-%m-%e') = from_unixtime(".$currentDate.", '%Y-%m-%e')
                THEN (ifnull((visits.created_on),0)) END) as todayvisit,
                count(CASE WHEN from_unixtime(created_on, '%Y-%m-%e') > DATE_SUB(from_unixtime(".$currentDate.", '%Y-%m-%e'), INTERVAL 1 WEEK)
                THEN (ifnull((visits.created_on),0)) END) as thisweek,
                count(CASE WHEN from_unixtime(created_on, '%Y-%m-%e') > DATE_SUB(from_unixtime(".$currentDate.", '%Y-%m-%e'), INTERVAL 1 MONTH)
                THEN (ifnull((visits.created_on),0)) END) as thismonth,
                count(CASE WHEN from_unixtime(created_on, '%Y-%m-%e') > DATE_SUB(from_unixtime(".$currentDate.", '%Y-%m-%e'), INTERVAL 1 YEAR)
                THEN (ifnull((visits.created_on),0)) END) as thisyear from visits";
	
    $result = mysqli_query($conn, $sql);
    $rows   = array();
    while ($resultset = mysqli_fetch_array($result)) {
        $rows[] = $resultset;
    }
	$sql    = "SELECT IFNULL(SUM(CASE WHEN status='paid' THEN charge ELSE 0 END),0) as totalPaidBill,
			IFNULL(SUM(CASE WHEN status='unpaid' THEN charge ELSE 0 END),0) as  totalUnpaidBill
			FROM
			(
			 SELECT charge,status
			 FROM patient_lab_bill
			 UNION ALL
			 SELECT charge,status
			 FROM patient_radiology_bill
			 UNION ALL
			 SELECT charge,status
			 FROM patient_service_bill
			 UNION ALL
			 SELECT charge,status
			 FROM patient_forensic_bill
			 UNION ALL
			 SELECT amount as charge,status
			 FROM patient_pharmacy_bill
			 UNION ALL
			 SELECT charge ,status
			   FROM patient_procedure_bill
			)x"; 			//Query to Get Total Paid Bill anb Total Unpaid Bill FROM Different Bill Table
			
    $result = mysqli_query($conn, $sql);
    while ($resultset = mysqli_fetch_array($result)) {
        $rows[] = $resultset;
    }
    print json_encode($rows);
}

?>