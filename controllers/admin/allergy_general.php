<?php
/*File Name  :   allergy_general.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   31th Dec, 2015
Description  :   This page manages  all the alergy test request*/

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
/*include config file*/
include 'config.php';

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
}

/*operation to show only those allergy test for which patient not visited*/
if ($operation == "showAllergy") {
    if (isset($_POST['value'])) {
        $value = $_POST['value'];
    }
    if (isset($_POST['visitId'])) {
        $visitId = $_POST['visitId'];
    }
    $query  = "SELECT allergies.id,allergies.name,allergies.code FROM allergies WHERE allergies.id NOT IN 
            (SELECT allergy_request.allergy_id FROM allergy_request WHERE  allergy_request.visit_id ='" . $visitId . "')
            AND allergies.status ='A' AND allergies.allergy_cat_id = '" . $value . "' ORDER BY allergies.name";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

/*operation to save array data*/
if ($operation == "save") {
    $data        = json_decode($_POST['data']);
    $patientId   = $_POST['patientId'];
    $visitId     = $_POST['visitId'];
    $tblName     = $_POST['tblName'];
    $fkColName   = $_POST['fkColName'];
    $insertQuery = "INSERT INTO " . $tblName . " (patient_id,visit_id," . $fkColName . ",allergy_id ,created_on,updated_on,updated_by) ";
    $insertQuery .= "VALUES ";
    $counter = 0;
    foreach ($data as $value) {
        
        $allergyId = 0;
        foreach ($value as $key => $val) {
            
            if ($key == "3") { //at index 3 getting id
                $allergyId = $val;
                if ($counter > 0) {
                    $insertQuery .= ",";
                }
                $insertQuery .= "(" . $patientId . "," . $visitId . ",'" . $_SESSION['globaluser'] . "','" . $allergyId . "',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."')";
                $counter++;
            }
        }
    }
    
    $result = mysqli_query($conn, $insertQuery);
    echo $result;
}

if ($operation == "search") {
    $patientId = $_POST['patientId'];
    $visitId   = $_POST['visitId'];
    $tblName   = $_POST['tblName'];
    
    $searchQuery  = "SELECT COUNT(*) as total FROM " . $tblName . " WHERE patient_id = " . $patientId . " AND visit_id = " . $visitId;
    $searchResult = mysqli_query($conn, $searchQuery);
    $data         = mysqli_fetch_assoc($searchResult);
    echo $data['total'];
}
?>