<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}
	
	if ($operation == "showRecurrencePeriod") {
		
		$sql = "SELECT id,recurrence_period FROM recurrence_period WHERE status = 'A' ";

		$result = mysqli_query($conn,$sql);
		$rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
		echo json_encode($rows);
	}

	if ($operation == "showARAccount") {
		
		$sql = "SELECT coa.id,coa.account_name FROM chart_of_account AS coa 
		LEFT JOIN class AS c ON c.id = coa.class 
		WHERE c.`status` = 'A' AND c.class ='Receivable'";

		$result = mysqli_query($conn,$sql);
		$rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
		echo json_encode($rows);
	}

	if ($operation == "showCustomerDetails") {
			
		$customerId = $_POST['customerId'];

		$sql = "SELECT first_name,last_name,email,phone FROM customer WHERE id= '".$customerId."' ";

		$result = mysqli_query($conn,$sql);
		$rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
		echo json_encode($rows);
	}

	if ($operation == "save") {

		$salesRepresentative='';
		$endDate='';
		$nextDate='';
			
		$customerId = $_POST['customerId'];

		if (isset($_POST['billingDate'])) {
			$billingDate = $_POST["billingDate"];
		}
		if (isset($_POST['dueDate'])) {
			$dueDate = $_POST["dueDate"];
		}
		if (isset($_POST['arAccount'])) {
			$arAccount = $_POST["arAccount"];
		}
		if (isset($_POST['currency'])) {
			$currency = $_POST["currency"];
		}
		if (isset($_POST['salesRepresentative'])) {
			$salesRepresentative = $_POST["salesRepresentative"];
		}
		if (isset($_POST['recurrencePeriod'])) {
			$recurrencePeriod = $_POST["recurrencePeriod"];
		}
		if (isset($_POST['grandTotal'])) {
			$grandTotal = $_POST["grandTotal"];
		}
		if (isset($_POST['nextDate'])) {
			$nextDate = $_POST["nextDate"];
		}
		if (isset($_POST['endDate'])) {
			$endDate = $_POST["endDate"];
		}
		

		$sql = "INSERT INTO customer_invoice (customer_id ,total_bill,currency ,account_recievable ,sales_representative ,billing_date ,due_date,recurrence_period,next_date,end_date,created_on,updated_on,created_by ,updated_by) values('".$customerId."','".$grandTotal."','".$currency."','".$arAccount."','".$userId."','".$billingDate."','".$dueDate."','".$recurrencePeriod."','".$nextDate."','".$endDate."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";

		$result = mysqli_query($conn,$sql);
		$lastInsertId = mysqli_insert_id($conn);

		
		if (isset($_POST['productData'])) {
			$sql = "INSERT INTO customer_product_details (gl_account_id,customer_invoice_id ,quantity,product_id,unit_price,tax ,discount ,total ,created_on,updated_on ,created_by,updated_by ) VALUES ";
			$subQry = '';
			$counter = 0;
        	$productData = json_decode($_POST["productData"]);
        	
	        foreach ($productData as $value) {
	            
	            $productId    = $value->productId;
	            $glAccountId    = $value->glAccountId;
	            $productQuantity        = $value->productQuantity;
	            $productCost        		= $value->productCost;
	            $productTax        		= $value->productTax;
	            $productDiscount        		= $value->productDiscount;
	            $productTotal        		= $value->productTotal;
				
				if ($counter != 0) {
					$subQry.=',';
				}

	            $subQry.= "('".$glAccountId."','".$lastInsertId."','".$productQuantity."','".$productId."','".$productCost."','".$productTax."','".$productDiscount."','".$productTotal."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";

	            $counter++;				
	        }

	        $result = mysqli_query($conn,$sql.$subQry);
		}
		if($result){
			echo $result;
		}
	}

	if ($operation == "productDetails") {

		$sql = "SELECT products.id,products.name,products.cost_price,products.unit,IFNULL(SUM(tax_type.tax_rate),0) AS tax,(SELECT value FROM configuration WHERE name = 'product_prefix') AS product_prefix   
			FROM products
			LEFT JOIN product_taxes ON product_taxes.product_id = products.id
			LEFT JOIN tax_type ON tax_type.id = product_taxes.tax_id
			GROUP BY products.id";

		$result = mysqli_query($conn,$sql);
		$rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
		echo json_encode($rows);
	}
?>