<?php

	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}

	/*include config file*/
	include 'config.php';


	/*checking operation set or not*/
	if (isset($_POST['operation'])) {
	    $operation = $_POST["operation"];
	} else if (isset($_GET["operation"])) {
	    $operation = $_GET["operation"];
	}

	if ($operation == "showData") {
		$fromDate = $_POST['fromDate'];
		$toDate = $_POST['toDate'];
		$query = "SELECT imprest_warrent_line_item.imprest_request_id,imprest_warrent_line_item.initial_amount AS balance,CONCAT(users.first_name,' ',users.last_name) AS payee_name FROM imprest_warrent_line_item
			LEFT JOIN imprest_request ON imprest_request.id = imprest_warrent_line_item.imprest_request_id
			LEFT JOIN users ON users.id = imprest_request.staff_id
			WHERE imprest_warrent_line_item.spent_amount IS NULL AND imprest_warrent_line_item.`status` = 'A'";

			if($fromDate !='' AND $toDate !=''){
				$query.= " AND imprest_warrent_line_item.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59') ";
			}
			if($fromDate !='' AND $toDate ==''){
				$query.= " AND imprest_warrent_line_item.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND UNIX_TIMESTAMP(NOW()) ";
			}
			if($fromDate =='' AND $toDate !=''){
				$query.= " AND imprest_warrent_line_item.created_on BETWEEN '0000000000' AND UNIX_TIMESTAMP('".$toDate." 23:59:59') ";
			}

		$result = mysqli_query($conn, $query);
		$rows         = array();
		while ($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		print json_encode($rows);
	}