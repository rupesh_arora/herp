<?php
/*File Name  :   old_test_request.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   4th Jan, 2015
Description  :   This page manages operations for consultant module*/


session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

/*include config file*/
include 'config.php';

$operation = "";

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
}

/*operation to search old lab request data*/
if ($operation == "lab_search") {
    
    $paymentMode = $_POST['paymentMode'];
    $activeTab = $_POST['activeTab'];

    if ($paymentMode == "insurance") {
        
        $searchLabQuery = "SELECT ltr.lab_test_id as id, CASE WHEN (ltr.request_type ='external')   
        THEN CONCAT(lt.name,' (',ltr.request_type,')') ELSE lt.name END AS name, ltr.request_date ,
        ltr.pay_status,ltr.test_status,lab_test_component.fee ,lab_test_request_component.cost,
        lab_test_component.name AS component_name,lab_test_component.id as component_id 
        FROM lab_test_requests AS ltr
        LEFT JOIN lab_tests AS lt ON lt.id=ltr.lab_test_id
        LEFT JOIN lab_test_request_component ON lab_test_request_component.lab_test_request_id = ltr.id
        LEFT JOIN lab_test_component ON lab_test_component.id = lab_test_request_component.lab_component_id
        WHERE ltr.status = 'A' AND ltr.visit_id = " . $visitId = $_POST["visit_id"];
    }
    else{
        $searchLabQuery = "SELECT ltr.lab_test_id as id,CASE WHEN (ltr.request_type ='external')   
        THEN CONCAT(lt.name,' (',ltr.request_type,')') ELSE lt.name END AS name, ltr.request_date ,
        lab_test_request_component.pay_status,ltr.test_status, lab_test_component.fee AS cost ,lab_test_component.name 
        AS component_name,lab_test_component.id as component_id FROM lab_test_requests AS ltr
        LEFT JOIN lab_tests AS lt ON lt.id=ltr.lab_test_id
        LEFT JOIN lab_test_request_component ON lab_test_request_component.lab_test_request_id = ltr.id
        LEFT JOIN lab_test_component ON lab_test_component.id = lab_test_request_component.lab_component_id
        WHERE ltr.status = 'A' AND ltr.visit_id = " . $visitId = $_POST["visit_id"];
    }

    $result         = mysqli_query($conn, $searchLabQuery);
    $rows           = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}



/*operation to search old radiology request data*/
if ($operation == "rad_search") {

    $paymentMode = $_POST['paymentMode'];
    $activeTab = $_POST['activeTab'];

    if ($paymentMode == "insurance") {

        $serachRadQry = "SELECT rtr.radiology_test_id as id,CASE WHEN (rtr.request_type ='external')   
        THEN CONCAT(rt.name,' (',rtr.request_type,')') ELSE rt.name END AS name, rtr.request_date, rtr.pay_status,
        rtr.test_status, rt.fee,rtr.cost AS revised_cost FROM radiology_test_request AS rtr 
        LEFT JOIN radiology_tests AS rt ON rt.id=rtr.radiology_test_id
        WHERE rtr.status = 'A' AND rtr.visit_id = " . $visitId = $_POST["visit_id"];
    }
    else {
        $serachRadQry = "SELECT rtr.radiology_test_id as id,CASE WHEN (rtr.request_type ='external')   
        THEN CONCAT(rt.name,' (',rtr.request_type,')') ELSE rt.name END AS name, rtr.request_date, rtr.pay_status, rtr.test_status, rt.fee FROM radiology_test_request AS rtr 
            LEFT JOIN radiology_tests AS rt ON rt.id=rtr.radiology_test_id
            WHERE rtr.status = 'A' AND rtr.visit_id = " . $visitId = $_POST["visit_id"];
    }

    $result         = mysqli_query($conn, $serachRadQry);
    $rows           = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

/*operation to search old forensic request data*/
if ($operation == "forensic_search") {

    $paymentMode = $_POST['paymentMode'];
    $activeTab = $_POST['activeTab'];

    if ($paymentMode == "insurance") {

        $serachForensicQry = "SELECT ftr.forensic_test_id as id,CASE WHEN (ftr.request_type ='external')   
        THEN CONCAT(ft.name,' (',ftr.request_type,')') ELSE ft.name END AS name, ftr.request_date, ftr.pay_status, ftr.test_status, ft.fee,ftr.cost AS revised_cost
            FROM forensic_test_request AS ftr 
            LEFT JOIN forensic_tests AS ft ON ft.id=ftr.forensic_test_id
            WHERE  ftr.status = 'A' AND ftr.visit_id = " . $visitId = $_POST["visit_id"];
    }
    else{
        $serachForensicQry = "SELECT ftr.forensic_test_id as id,CASE WHEN (ftr.request_type ='external')   
        THEN CONCAT(ft.name,' (',ftr.request_type,')') ELSE ft.name END AS name, ftr.request_date, ftr.pay_status, ftr.test_status, ft.fee FROM forensic_test_request AS ftr 
            LEFT JOIN forensic_tests AS ft ON ft.id=ftr.forensic_test_id
            WHERE  ftr.status = 'A' AND ftr.visit_id = " . $visitId = $_POST["visit_id"];
    }
    //echo $searchLabQuery;
    $result         = mysqli_query($conn, $serachForensicQry);
    $rows           = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

/*operation to search old service request data*/
if ($operation == "oldServiceSearch") {

    $paymentMode = $_POST['paymentMode'];
    $activeTab = $_POST['activeTab'];

    if ($paymentMode == "insurance") {
        $serachServiceQry = "SELECT services_request.id,  CASE WHEN (services_request.request_type ='external')   
        THEN CONCAT(services.name,' (',services_request.request_type,')') ELSE services.name END AS name, services_request.request_date,
        services_request.pay_status, services_request.test_status,services.cost,
        services_request.cost AS revised_cost FROM services_request
        LEFT JOIN services ON services_request.service_id = services.id
        WHERE services_request.status = 'A' 
        AND services_request.visit_id = ".$visitId = $_POST["visit_id"];
    }

    else{
        $serachServiceQry = "SELECT services_request.id,  CASE WHEN (services_request.request_type ='external')   
        THEN CONCAT(services.name,' (',services_request.request_type,')') ELSE services.name END AS name, services_request.request_date,
        services_request.pay_status, services_request.test_status,services.cost FROM
        services_request
        LEFT JOIN services ON services_request.service_id = services.id
        WHERE services_request.status = 'A' AND 
        services_request.visit_id = ".$visitId = $_POST["visit_id"];
    }
     
    $result         = mysqli_query($conn, $serachServiceQry);
    $rows           = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

/*operation to search old procedure request data*/
if ($operation == "oldProcedureSearch") {

    $paymentMode = $_POST['paymentMode'];
    $activeTab = $_POST['activeTab'];
    if ($paymentMode == "insurance") {
        $searchProcedureQuery = "SELECT procedures_requests.id, CASE WHEN (procedures_requests.request_type
        ='external') THEN CONCAT(procedures.name,' (',procedures_requests.request_type,')') 
        ELSE procedures.name END AS name, procedures_requests.cost AS revised_cost,
        procedures_requests.request_date,procedures_requests.test_status, procedures_requests.pay_status,
        procedures.price FROM procedures_requests
        LEFT JOIN procedures ON procedures_requests.procedure_id = procedures.id
        WHERE procedures_requests.status = 'A' AND 
        procedures_requests.visit_id = ".$visitId = $_POST["visit_id"];
    }
    else{
        $searchProcedureQuery ="SELECT procedures_requests.id, CASE WHEN (procedures_requests.request_type
        ='external') THEN CONCAT(procedures.name,' (',procedures_requests.request_type,')') 
        ELSE procedures.name END AS name, procedures_requests.request_date,procedures_requests.test_status
        , procedures_requests.pay_status,procedures.price FROM procedures_requests
        LEFT JOIN procedures ON procedures_requests.procedure_id = procedures.id
        WHERE procedures_requests.status = 'A' AND
        procedures_requests.visit_id = " . $visitId = $_POST["visit_id"];
    }
       
    
    $result = mysqli_query($conn, $searchProcedureQuery);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

/*operation to search old alergy request data*/
if ($operation == "oldAllergySearch") {
    $searchLabQuery = "SELECT allergies.id ,allergies.code, allergies.name ,  allergies.updated_on as request_date from allergies
			LEFT JOIN allergy_request on allergy_request.allergy_id = allergies.id
			WHERE allergy_request.visit_id = " . $visitId = $_POST["visit_id"];
    
    $result = mysqli_query($conn, $searchLabQuery);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

/*operation to search old diagnosis request data*/
if ($operation == "oldDiagnosisSearch") {
    $searchLabQuery = "SELECT  d.diagnosis_name,d.disease_id,d.id,de.name AS disease_name from patient_diagnosis_request AS d
	LEFT JOIN diseases AS de ON de.id = d.disease_id 
			WHERE d.visit_id = " . $visitId = $_POST["visit_id"];
    //echo $searchLabQuery;
    $result         = mysqli_query($conn, $searchLabQuery);
    $rows           = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

/*operation to search old Questioannaire data*/
if ($operation == "oldQuestionnaireSearch") {
    $searchquesQuery = "SELECT  q.question,q.answer,q.id from patients_questionnaire AS q
			WHERE q.visit_id = " . $visitId = $_POST["visit_id"];
   
    $result         = mysqli_query($conn, $searchquesQuery);
    $rows           = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "checkExclusion") {

    $itemId = $_POST['item_id'];
    $insuranceCompanyId = $_POST['insurance_company_id'];
    $schemePlanId = $_POST['scheme_plan_id'];
    $schemeNameId = $_POST['scheme_name_id'];
    $serviceType = $_POST['service_type'];
    $sqlSelect = "SELECT id FROM scheme_exclusion 
        WHERE item_id ='" . $itemId . "' AND insurance_company_id ='" . $insuranceCompanyId . "'   AND
        scheme_plan_id ='" . $schemePlanId . "' AND scheme_name_id ='" . $schemeNameId . "' 
        AND service_type ='" . $serviceType . "' AND status='A'";

    $resultSelect = mysqli_query($conn, $sqlSelect);
    $rows_count   = mysqli_num_rows($resultSelect);
    if ($rows_count <=0) {
        echo "notexcluded";
    }
    else {
        echo "excluded";
    }
}
?>