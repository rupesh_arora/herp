<?php
/*
 * File Name    :   insuance_plan.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre details form database
 */
$operation           = "";
$description         = "";
$createdate          = new DateTime();
include 'config.php'; //import databse connection file
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

if (isset($_POST['operation'])) { // perform operation form js file
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
}

if($operation == "showCompany") {

    if (isset($_POST['patientId'])) { 
        $patientId = $_POST["patientId"];

        $query = "SELECT DISTINCT(insurance_companies.id),insurance_companies.name FROM insurance
        LEFT JOIN insurance_companies ON insurance_companies.id = insurance.insurance_company_id
        WHERE insurance.status = 'A' AND insurance.patient_id = '".$patientId."'";
    }
    else{
        $query = "SELECT id,name FROM insurance_companies where status = 'A'";
    }

    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "save") // save details
{	
	$description = "";
    if (isset($_POST['companyName'])) {
        $companyName = $_POST['companyName'];
    }
	if (isset($_POST['planName'])) {
        $planName = $_POST['planName'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    
    $sqlSelect = "Select * from insurance_plan WHERE company_name_id = ".$companyName." AND plan_name = '".$planName."'";
	$resultSelect = mysqli_query($conn, $sqlSelect);
    $totalrecords = mysqli_num_rows($resultSelect);
	if ($totalrecords > 0) {
		echo "0";
	}
	else {
		$sqlInsert    = "INSERT INTO insurance_plan(company_name_id,plan_name,description,created_on,updated_on,created_by,updated_by)
				VALUES('" . $companyName . "','" . $planName . "','" . $description . "',
				'" . $createdate->getTimestamp() . "','" . $createdate->getTimestamp() . "',".$userId.",".$userId.")";
		$result = mysqli_query($conn, $sqlInsert);
		echo $result;
	}
}

if ($operation == "show") { // show active data
    
    $query        = "select insurance_plan.*,insurance_companies.name AS CompanyName from  insurance_plan LEFT JOIN insurance_companies ON 
					insurance_companies.id = insurance_plan.company_name_id where insurance_plan.`status`='A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
    
}
// opertaion form update
if ($operation == "update") // update data
{
    $description = "";
    if (isset($_POST['companyName'])) {
        $companyName = $_POST['companyName'];
    }
	if (isset($_POST['planName'])) {
        $planName = $_POST['planName'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    $id = $_POST['id'];
	$sqlSelect = "Select * from insurance_plan WHERE id != ".$id." and plan_name = '".$planName."' and  company_name_id = '" . $companyName . "'";
	$resultSelect = mysqli_query($conn, $sqlSelect);
    $totalrecords = mysqli_num_rows($resultSelect);
	$totalrecords;
	if ($totalrecords == 1) {
		echo "0";
	}
	else {
    
		$sql    = "update insurance_plan set plan_name= '" . $planName . "' ,company_name_id= '" . $companyName . "' ,
					description = '" . $description . "', updated_on='" . $createdate->getTimestamp() . "',
					updated_by='" . $userId . "' where id = '" . $id . "' ";
		$result = mysqli_query($conn, $sql);
		echo $result;
	}
}
//  operation for delete
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $selectQry = "SELECT insurance_plan_id FROM scheme_plans WHERE insurance_plan_id ='".$id."'";
    $selectResult = mysqli_query($conn,$selectQry);
    $countRows = mysqli_num_rows($selectResult);
    if ($countRows == 0) {

        $selectQryIns = "SELECT insurance_plan_id FROM insurance WHERE insurance_plan_id ='".$id."'";
        $selectResultIns = mysqli_query($conn,$selectQryIns);
        $countRows = mysqli_num_rows($selectResultIns);

        if ($countRows == 0) {
           $sql    = "UPDATE insurance_plan SET status= 'I' where id = '" . $id . "'";
            $result = mysqli_query($conn, $sql);
            echo $result; 
        }
        else{
            echo "0";
        }
    }
    else{
        echo "0";
    }            
}
//When checked box is check
if ($operation == "checked") {
    
    $query = "select insurance_plan.*,insurance_companies.name AS CompanyName from  insurance_plan LEFT JOIN insurance_companies ON 
					insurance_companies.id = insurance_plan.company_name_id where insurance_plan.`status`='I'";
    
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}
if ($operation == "restore") // for restore    
    {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $sql    = "UPDATE insurance_plan SET status= 'A'  WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}
?>