<?php
session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
    $operation = "";
    $description = "";
    $unit_id="";
    $id="";
    $subTestName="";
    $labType="";
    $range="";
	$date = new DateTime();


    if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	else{}

	//Selecting the type from labtype table
	if($operation == "showLabType")			
	{		
		$query="SELECT id,type FROM lab_types where status='A'";
		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);
	} 

	if($operation == "showUnitMeasure")			
	{		
		$query="SELECT id,unit FROM units WHERE status='A'";
		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);
	} 

	//Selecting the test from labtests table
	if($operation == "showTest")
	{		
		$query="SELECT id,name FROM lab_tests WHERE status='A'";		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);
	} 


	if($operation == "show"){
		
		$query= "SELECT sub_lab_tests.id, sub_lab_tests.name As test_name, sub_lab_tests.range, units.unit,sub_lab_tests.unit_id,
		sub_lab_tests.description, lab_types.type,sub_lab_tests.lab_type_id,sub_lab_tests.lab_test_id, lab_tests.name FROM sub_lab_tests
		LEFT JOIN units ON  sub_lab_tests.unit_id = units.id
		LEFT JOIN lab_types ON sub_lab_tests.lab_type_id = lab_types.id
		LEFT JOIN lab_tests ON sub_lab_tests.lab_test_id = lab_tests.id
		WHERE sub_lab_tests.status = 'A' AND lab_types.status = 'A' AND lab_tests.status = 'A'";
		/* $query= "select id, ward_id,number,description from rooms"; */
		/* echo $query; */
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 //print json_encode($rows);
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}

	if($operation == "delete")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}		
		$sql = "UPDATE sub_lab_tests SET status= 'I'  WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}

	if($operation == "update")			
	{
		if (isset($_POST['labType'])) {
			$labType=$_POST['labType'];
		}
		if (isset($_POST['subTestName'])) {
			$subTestName=$_POST['subTestName'];
		}
		if (isset($_POST['test'])) {
			$test=$_POST['test'];
		}
		if (isset($_POST['range'])) {
			$range=$_POST['range'];
		}
		if (isset($_POST['unit_id'])) {
			$unit_id=$_POST['unit_id'];
		}
		if (isset($_POST['description'])) {
			$description=$_POST['description'];
		}
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}		
		
		$sql = "UPDATE sub_lab_tests SET lab_type_id= ".$labType.",name= '".$subTestName."',lab_test_id= ".$test.",`range`= '".$range."',unit_id= ".$unit_id.",description='".$description."',updated_on= ".$date->getTimestamp()." WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}

	if($operation == "save")			
	{
		if (isset($_POST['labType'])) {
			$labType=$_POST['labType'];
		}
		if (isset($_POST['subTestName'])) {
			$subTestName=$_POST['subTestName'];
		}
		if (isset($_POST['test'])) {
			$test=$_POST['test'];
		}
		if (isset($_POST['range'])) {
			$range=$_POST['range'];
		}
		if (isset($_POST['unit_id'])) {
			$unit_id=$_POST['unit_id'];
		}
		if (isset($_POST['description'])) {
			$description=$_POST['description'];
		}		
		$sql = "INSERT INTO sub_lab_tests(name,lab_type_id,lab_test_id,`range`,unit_id,description,created_on,updated_on)
		VALUES('".$subTestName."','".$labType."','".$test."','".$range."','".$unit_id."','".$description."',".$date->getTimestamp().",".$date->getTimestamp().")";
		$result= mysqli_query($conn,$sql);  
		echo $result;		
	} 

	//When checked box is check
	if ($operation == "checked") {
		
		$query= "SELECT sub_lab_tests.id, sub_lab_tests.name As test_name, sub_lab_tests.range, units.unit,sub_lab_tests.unit_id,
		sub_lab_tests.description, lab_types.type,sub_lab_tests.lab_type_id,sub_lab_tests.lab_test_id, lab_tests.name FROM sub_lab_tests
		LEFT JOIN units ON  sub_lab_tests.unit_id = units.id
		LEFT JOIN lab_types ON sub_lab_tests.lab_type_id = lab_types.id
		LEFT JOIN lab_tests ON sub_lab_tests.lab_test_id = lab_tests.id
		WHERE sub_lab_tests.status = 'I' OR lab_types.status = 'I' OR lab_tests.status = 'I'";

		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 	$rows[] = $r;
		}
		 //print json_encode($rows);
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);			
	}

	//to restore data back to data 
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		if (isset($_POST['lab_type_id'])) {
			$lab_type_id=$_POST['lab_type_id'];
		}
		if (isset($_POST['lab_test_id'])) {
			$lab_test_id=$_POST['lab_test_id'];
		}
		$sql = "UPDATE sub_lab_tests SET status= 'A'  WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		if($result == 1){
			$query_type = "SELECT status from lab_types where id='".$lab_type_id."'  AND status='A'";
			$result_type= mysqli_query($conn,$query_type);
			$count_type=mysqli_num_rows($result_type);

			$query_test = "SELECT status from lab_tests where id='".$lab_test_id."'  AND status='A'";
			$result_test= mysqli_query($conn,$query_test);
			$count_test=mysqli_num_rows($result_test);
			if($count_type == '1' && $count_test == '1'){
				
				echo "1";	
				
			}
			else{
				echo "0";				
			}
		}
	}
	
?>