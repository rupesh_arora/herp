<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "saveAssetCountPeriod") {

		$period = $_POST['period'];
		$startDate = $_POST['startDate'];
		$endDate = $_POST['endDate'];
		$description = $_POST['description'];

		//$selLedger = "SELECT name FROM ledger WHERE name ='".$name."' AND status = 'A'";
		//$checkLedger = mysqli_query($conn,$selLedger);
		//$countLedger = mysqli_num_rows($checkLedger);

		//if ($countLedger == "0") {

		$query = "INSERT INTO asset_count_period(period,start_date,end_date,description,created_by,updated_by,created_on,updated_on) VALUES('".$period."','".$startDate."','".$endDate."','".$description."','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";

		$result = mysqli_query($conn, $query);
		echo $result;
		//}
		//else {
		//	echo "0";
		//}
	}
	if ($operation == "show") { // show active data
    
    	$query = "SELECT id,period,start_date,end_date,description FROM asset_count_period WHERE status = 'A'";
	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}

	if ($operation == "checked") {
	    
	    $query = "SELECT id,period,start_date,end_date,description FROM asset_count_period WHERE status = 'I'";
	    
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}
	if ($operation == "update") // update data
	{
	    $period = $_POST['period'];
	    $startDate = $_POST['startDate'];
	    $endDate = $_POST['endDate'];
		$description = $_POST['description'];
	    $id = $_POST['id'];

	    /*$selLedger = "SELECT name FROM ledger WHERE name ='".$name."' AND status = 'A' AND id !='".$id."'";
		$checkLedger = mysqli_query($conn,$selLedger);
		$countLedger = mysqli_num_rows($checkLedger);

		if ($countLedger == "0") {*/
		
			$sql    = "UPDATE asset_count_period set period = '".$period."',start_date = '".$startDate."',end_date = '".$endDate."',description= '".$description."',updated_on = UNIX_TIMESTAMP() ,updated_by = '".$userId."' where id = '".$id."' ";

			$result = mysqli_query($conn, $sql);
			echo $result;
		/*}
		else {
			echo "0";
		}*/
	}
	if ($operation == "delete") {
        $id = $_POST['id'];

		$sql    = "UPDATE asset_count_period SET status= 'I' where id = '" . $id . "'";
	    $result = mysqli_query($conn, $sql);
	    echo $result;	    
	}
	if ($operation == "restore") {
        $id = $_POST['id'];

		$sql    = "UPDATE asset_count_period SET status= 'A' where id = '" . $id . "'";
	    $result = mysqli_query($conn, $sql);
	    echo $result;	    
	}
?>