<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}

	if($operation=="showAttendance") {
		$fromDate=$_POST["fromDate"];
		$toDate=$_POST["toDate"];

		$sql = "SELECT `date`,DATE_FORMAT(FROM_UNIXTIME(punch_in), '%Y-%m-%d %h:%i %p') AS punch_in,
			DATE_FORMAT(FROM_UNIXTIME(punch_out), '%Y-%m-%d %h:%i %p') AS punch_out ,remarks,
			TIMEDIFF(DATE_FORMAT(FROM_UNIXTIME(punch_out), '%Y-%m-%d %h:%i %p') ,DATE_FORMAT(FROM_UNIXTIME(punch_in), 
			'%Y-%m-%d %h:%i %p')) AS duration
			FROM staff_attendance WHERE staff_id = '".$userId."' ";
			
			if($fromDate != '' || $toDate != ''){

				if($fromDate !='' AND $toDate !=''){
					$sql .= " AND `date` BETWEEN ('".$fromDate." 00:00:00') AND ('".$toDate." 23:59:59') ";
					$isFirst = "true";
				}
				if($fromDate !='' AND $toDate ==''){
				$sql .= " AND `date` BETWEEN ('".$fromDate." 00:00:00') AND CURDATE() ";
					$isFirst = "true";
				}
				if($fromDate =='' AND $toDate !=''){
					$sql .= " AND `date` BETWEEN '0000000000' AND ('".$toDate." 23:59:59') ";
					$isFirst = "true";
				}
			}
		$result = mysqli_query($conn,$sql);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);
	}
	
?>
