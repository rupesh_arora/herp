<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "showCustomerType") { // show active data
    
    	$query = "SELECT id,`type` FROM customer_type WHERE status = 'A'";
	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
    
	}

	if ($operation == "showPaymentHistory") {
			
		$customerId = $_POST['customerId'];

		$sql = "SELECT rb.amount,rb.id,rb.invoice_id,DATE_FORMAT(FROM_UNIXTIME(rb.created_on), '%Y-%m-%d') AS pay_date,ci.billing_date,
		(SELECT value FROM configuration WHERE name = 'invoice_prefix') AS invoice_prefix,	(SELECT value FROM configuration WHERE name = 'receipt_prefix') AS receipt_prefix,CONCAT('INVOICE', CASE WHEN ci.recurrence_period = 'does not recur' THEN ' (Normal)' ELSE ' (Recurrence)' END) AS description 
		 FROM customer AS c
		LEFT JOIN customer_invoice AS ci ON ci.customer_id = c.id 
		LEFT JOIN receipt_book AS rb ON rb.invoice_id = ci.id 
		WHERE c.id= '".$customerId."' ORDER BY ci.id ";

		$result = mysqli_query($conn,$sql);
		$rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
		echo json_encode($rows);
	}
	

	if ($operation == "save") {
		$firstName = $_POST['firstName'];
		$lastName = $_POST['lastName'];
		$address = $_POST['address'];
		$country = $_POST['country'];
		$state = $_POST['state'];
		$city = $_POST['city'];
		$customerType = $_POST['customerType'];
		$pincode = $_POST['pincode'];
		$email = $_POST['email'];
		$website = $_POST['website'];
		$phone = $_POST['phone'];

		$chkEmail = "SELECT email FROM  customer WHERE status = 'A' AND email = '".$email."'";
		$resultChk = mysqli_query($conn,$chkEmail);
		$rowCount = mysqli_num_rows($resultChk);

		if ($rowCount >= "1") {
			echo "email exist";
			exit();
		}
		$sql = "INSERT INTO customer (first_name,customer_type_id,last_name,address,country_id,state_id,city_id,pin_code,email,website,phone,created_on,updated_on,created_by,updated_by) VALUES('".$firstName."','".$customerType."','".$lastName."','".$address."','".$country."','".$state."','".$city."','".$pincode."',
			'".$email."','".$website."','".$phone."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),
			'".$userId."','".$userId."')";
		    
	    $result = mysqli_query($conn, $sql);
	    echo $result;
	    
	}

	if ($operation == "show") { // show active data
    
	    $sql = "SELECT id,CONCAT(first_name,' ',last_name) AS name,customer_type_id,pin_code AS pincode,first_name,last_name,address,email,
	    	phone,website,country_id, state_id, city_id ,l1.name AS country ,l2.name AS state, 
	    	l3.name AS city   FROM customer
			LEFT JOIN locations AS l1 ON l1.location_id = customer.country_id
			LEFT JOIN locations AS l2 ON l2.location_id = customer.state_id
			LEFT JOIN locations AS l3 ON l3.location_id = customer.city_id
			WHERE customer.`status` = 'A'";
	    $result = mysqli_query($conn, $sql);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	if ($operation == "checked") {
    
	    $query = "SELECT id,CONCAT(first_name,' ',last_name) AS name,customer_type_id,pin_code AS pincode,first_name,last_name,address,email,
	    	phone,website,country_id, state_id, city_id ,l1.name AS country ,l2.name AS state, 
	    	l3.name AS city   FROM customer
			LEFT JOIN locations AS l1 ON l1.location_id = customer.country_id
			LEFT JOIN locations AS l2 ON l2.location_id = customer.state_id
			LEFT JOIN locations AS l3 ON l3.location_id = customer.city_id
			WHERE customer.`status` = 'I'";
	    
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	if ($operation == "update") {// update data
	
	    
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    
		$firstName = $_POST['firstName'];
		$lastName = $_POST['lastName'];
		$address = $_POST['address'];
		$country = $_POST['country'];
		$state = $_POST['state'];
		$city = $_POST['city'];
		$customerType = $_POST['customerType'];
		$pincode = $_POST['pincode'];
		$email = $_POST['email'];
		$website = $_POST['website'];
		$phone = $_POST['phone'];

		$chkEmail = "SELECT email FROM  customer WHERE status = 'A' AND email = '".$email."' AND id != '".$id."'";
		$resultChk = mysqli_query($conn,$chkEmail);
		$rowCount = mysqli_num_rows($resultChk);

		if ($rowCount >= "1") {
			echo "email exist";
			exit();
		}	

		
		$sql    = "UPDATE customer set first_name = '".$firstName."',customer_type_id = '".$customerType."',
			last_name = '".$lastName."',address = '".$address."',country_id = '".$country."',
			state_id = '".$state."',city_id = '".$city."',pin_code = '".$pincode."',
			email = '".$email."',website = '".$website."',
			phone = '".$phone."', updated_on = UNIX_TIMESTAMP(),updated_by = '".$userId."' 
			WHERE id = '".$id."' ";

		$result = mysqli_query($conn, $sql);
		echo $result;
	
	}

	if ($operation == "restore") // for restore    
    {
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }

		$sql    = "UPDATE customer SET status= 'A' where id = '" . $id . "'";
	    $result = mysqli_query($conn, $sql);
	    echo $result;
		 
	}
	if ($operation == "delete") {
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    
    	$sql    = "UPDATE customer SET status= 'I' where id = '" . $id . "'";
	    $result = mysqli_query($conn, $sql);
	    echo $result;
	    
	}
	/*if ($operation == "showCustomerList") { // show active data
    
	    $sql = "SELECT id,CONCAT(first_name,' ',last_name) AS name,customer_type_id,pin_code AS pincode,first_name,last_name,address,email,
	    	phone,website,country_id, state_id, city_id ,l1.name AS country ,l2.name AS state, 
	    	l3.name AS city   FROM customer
			LEFT JOIN locations AS l1 ON l1.location_id = customer.country_id
			LEFT JOIN locations AS l2 ON l2.location_id = customer.state_id
			LEFT JOIN locations AS l3 ON l3.location_id = customer.city_id
			WHERE customer.`status` = 'A'";
	    $result = mysqli_query($conn, $sql);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}*/
?>