<?php
/*****************************************************************************************************
 * File Name    :   forensic_test.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages forensic test data
 ****************************************************************************************************/
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
$operation    = "";
$specimenType = "";
$unitMeasure  = "";
$testName     = "";
$normalRange  = "";
$testFee      = "";
$description  = "";
$date         = new DateTime();

include 'config.php';

if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

//It is use for select the units 
if ($operation == "showUnitMeasure") {
    
    $query  = "Select id,unit FROM units WHERE status='A' ORDER BY unit";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

//This operation is used for select the specimen type
if ($operation == "showSpecimenType") {
    
    $query  = "Select id,type FROM specimens_types WHERE status='A' ORDER BY type";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

// It is use for save the data
if ($operation == "save") {
    if (isset($_POST['specimenType'])) {
        $specimenType = $_POST['specimenType'];
    }
    if (isset($_POST['ledger'])) {
        $ledger=$_POST['ledger'];
    }
    if (isset($_POST['glAccount'])) {
        $glAccount=$_POST['glAccount'];
    }
    if (isset($_POST['accountPayable'])) {
        $accountPayable=$_POST['accountPayable'];
    }
    if (isset($_POST['unitMeasure'])) {
        $unitMeasure = $_POST['unitMeasure'];
    }
    if (isset($_POST['testName'])) {
        $testName = $_POST['testName'];
    }
    if (isset($_POST['normalRange'])) {
        $normalRange = $_POST['normalRange'];
    }
    if (isset($_POST['testFee'])) {
        $testFee = $_POST['testFee'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    //to check whether name already exisit or not
    $sql1         = "SELECT name from forensic_tests where name='" . $testName . "' AND 
        specimen_id = '" . $specimenType . "'";
    $resultSelect = mysqli_query($conn, $sql1);
    $rows_count   = mysqli_num_rows($resultSelect);
    $id ;
    if ($rows_count == 0) { //if not exisit then insert data
        if($unitMeasure == "") {
			$sql    = "INSERT INTO forensic_tests(name,specimen_id,ledger_id,gl_account_id,account_payable_id,`range`,fee,description,created_on,updated_on,created_by,updated_by)
				VALUES('" . $testName . "','" . $specimenType . "','".$ledger."','".$glAccount."','".$accountPayable."','" . $normalRange . "','" . $testFee . "','" . $description . "',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
				$result = mysqli_query($conn, $sql);
				$id = mysqli_insert_id($con);
		}
		else {
			$sql    ="INSERT INTO forensic_tests(name,specimen_id,ledger_id,gl_account_id,account_payable_id,`range` ,unit_id,fee,description,created_on,updated_on,
            created_by,updated_by)
				VALUES('" . $testName . "','" . $specimenType . "','".$ledger."','".$glAccount."','".$accountPayable."','" . $normalRange . "','" . $unitMeasure . "','" . $testFee . "','" . $description . "',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
				$result = mysqli_query($conn, $sql);
				$id = mysqli_insert_id($conn);
		}

       echo $query = "INSERT INTO `insurance_revised_cost`(`insurance_company_id`, `scheme_plan_id`, `scheme_name_id`,`item_name`, `service_type`, `item_id`, 
            `actual_cost`,`revised_cost`, `created_on`, `created_by`, `status`)
             SELECT  
        insurance_company_id ,scheme_plan_id , scheme_name_id
        ,'".$testName."' AS item_name ,'forensic' AS service_type , '".$id."' As item_id, '".$testFee."' AS
        actual_cots, '".$testFee."' AS revised_cost,UNIX_TIMESTAMP() AS created_on ,'".$userId."' 
        AS created_by, 'A' AS `status`   FROM insurance_revised_cost GROUP BY scheme_name_id ";
        $result = mysqli_query($conn, $query);
        echo $result;

    } else {
        echo "0";
    }
}

//This operation is used for show the active data
if ($operation == "show") {
    
    $query = "SELECT forensic_tests.id,forensic_tests.ledger_id AS ledger,forensic_tests.account_payable_id,forensic_tests.gl_account_id, forensic_tests.name, specimens_types.`type`,forensic_tests.specimen_id,forensic_tests.`range`, units.unit,forensic_tests.unit_id,
		forensic_tests.description ,forensic_tests.fee FROM forensic_tests
		LEFT JOIN units ON  forensic_tests.unit_id = units.id
		LEFT JOIN specimens_types ON forensic_tests.specimen_id = specimens_types.id
		WHERE forensic_tests.status = 'A' AND specimens_types.status = 'A'";
    
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

//This operation is used for show the inactive data
if ($operation == "showInActive") {
    
    $query = "SELECT forensic_tests.id,forensic_tests.ledger_id AS ledger,forensic_tests.account_payable_id,forensic_tests.gl_account_id, forensic_tests.name, specimens_types.`type`,
		forensic_tests.`range`,forensic_tests.specimen_id, units.unit,
		forensic_tests.description, forensic_tests.fee FROM forensic_tests
		LEFT JOIN units ON  forensic_tests.unit_id = units.id
		LEFT JOIN specimens_types ON forensic_tests.specimen_id = specimens_types.id
		WHERE forensic_tests.status = 'I' OR specimens_types.status = 'I'";
    
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

//This operation is used for change the status 
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE forensic_tests SET status= 'I'  WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}

//This operation is used for restore the data
if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    if (isset($_POST['specimen_type'])) {
        $specimen_type = $_POST['specimen_type'];
    }
    
    $sql    = "UPDATE forensic_tests SET status= 'A' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    
    if ($result == 1) {
        
        $query_specimen_type  = "SELECT status from specimens_types where id='" . $specimen_type . "' And status='A'";
        $result_specimen_type = mysqli_query($conn, $query_specimen_type);
        $count_specimen_type  = mysqli_num_rows($result_specimen_type);
        
        if ($count_specimen_type == '1') {
            echo "Restored Successfully!!!";
        } else {
            echo "It can not be restore!!!";
        }
    }
}

//This operation is used for update the data 
if ($operation == "update") {
    if (isset($_POST['specimenType'])) {
        $specimenType = $_POST['specimenType'];
    }
    if (isset($_POST['unitMeasure'])) {
        $unitMeasure = $_POST['unitMeasure'];
    }
    if (isset($_POST['ledger'])) {
        $ledger=$_POST['ledger'];
    }
    if (isset($_POST['glAccount'])) {
        $glAccount=$_POST['glAccount'];
    }
    if (isset($_POST['accountPayable'])) {
        $accountPayable=$_POST['accountPayable'];
    }
    if (isset($_POST['testName'])) {
        $testName = $_POST['testName'];
    }
    if (isset($_POST['normalRange'])) {
        $normalRange = $_POST['normalRange'];
    }
    if (isset($_POST['testFee'])) {
        $testFee = $_POST['testFee'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $sql1         = "SELECT name from forensic_tests where name='" . $testName . "' and id != '$id' AND 
        specimen_id = '" . $specimenType . "'";
    $resultSelect = mysqli_query($conn, $sql1);
    $rows_count   = mysqli_num_rows($resultSelect);
    if ($rows_count == 0) {
        $sql    = "UPDATE forensic_tests SET name= '" . $testName . "',gl_account_id = '".$glAccount."', ledger_id= '".$ledger."',account_payable_id= '".$accountPayable."',specimen_id= '" . $specimenType . "',`range`= '" . $normalRange . "',unit_id= '" . $unitMeasure . "',fee= '" . $testFee . "',description='" . $description . "',updated_on=UNIX_TIMESTAMP(),updated_by='".$userId."' WHERE  id = '" . $id . "'";
        $result = mysqli_query($conn, $sql);
        echo $result;
    } else {
        echo "0";
    }
}
?>