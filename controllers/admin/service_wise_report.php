<?php
	
	session_start();
	if(!session_id()){
		exit();
	}
	$toDate = '';
	$fromDate = '';
	
	include_once('config.php');

	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET['operation'])){
		$operation=$_GET["operation"];
	}

	if ($operation=="showChartData") {
		if (isset($_POST['dataLoad'])) {
			$dataLoad = $_POST["dataLoad"];
		}
		if (isset($_POST['fromDate'])) {
			$fromDate = $_POST["fromDate"];
		}
		if (isset($_POST['toDate'])) {
			$toDate = $_POST["toDate"];
		}

		if ($dataLoad =="all") {

			/*Count Service Query*/
			$countServices = "SELECT SUM(CASE WHEN services_request.test_status ='done' THEN 1 ELSE 0 END)
			AS 'Sevices Taken',
			SUM(CASE WHEN services_request.test_status='pending' THEN 1 ELSE 0 END) AS 'Sevices Untaken',
			services.name AS 'Service Name' from services_request
			LEFT JOIN services ON services.id = services_request.service_id GROUP BY service_id";

			/*Revenue Per service*/
			$revenuePerService = "SELECT SUM(CASE WHEN patient_service_bill.`status` = 'paid' THEN charge
			ELSE 0 END) AS 'Paid Amount' ,SUM(CASE WHEN patient_service_bill.status = 'unpaid' THEN charge
			ELSE 0 END) AS 'Unpaid Amount' ,services.name AS 'Service Name' FROM patient_service_bill
			LEFT JOIN services ON services.id = patient_service_bill.service_id	GROUP BY service_id";

			/*Over All revenue*/

			$revenueAll = "SELECT IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_service_bill";
		}
		
		else if ($dataLoad =="last30days") {

			/*Count Service Query*/
			$countServices = "SELECT SUM(CASE WHEN services_request.test_status ='done' THEN 1 ELSE 0 END)
			AS 'Sevices Taken',
			SUM(CASE WHEN services_request.test_status='pending' THEN 1 ELSE 0 END) AS 'Sevices Untaken',
			services.name AS 'Service Name' from services_request
			LEFT JOIN services ON services.id = services_request.service_id 
			WHERE services_request.request_date >= unix_timestamp(curdate() - interval 1 month)
			GROUP BY service_id";

			/*Revenue Per service*/
			$revenuePerService = "SELECT SUM(CASE WHEN patient_service_bill.`status` = 'paid' THEN charge
			ELSE 0 END) AS 'Paid Amount' ,SUM(CASE WHEN patient_service_bill.status = 'unpaid' THEN charge
			ELSE 0 END) AS 'Unpaid Amount' ,services.name AS 'Service Name' FROM patient_service_bill
			LEFT JOIN services ON services.id = patient_service_bill.service_id	
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate()  - interval 1 month)
			GROUP BY service_id";


			/*Over All revenue*/
			$revenueAll = "SELECT IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_service_bill
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate() - interval 1 month)";
		}


		else if ($dataLoad =="last7days") {

			/*Count Service Query*/
			$countServices = "SELECT SUM(CASE WHEN services_request.test_status ='done' THEN 1 ELSE 0 END)
			AS 'Sevices Taken',
			SUM(CASE WHEN services_request.test_status='pending' THEN 1 ELSE 0 END) AS 'Sevices Untaken',
			services.name AS 'Service Name' from services_request			
			LEFT JOIN services ON services.id = services_request.service_id 
			WHERE services_request.request_date >= unix_timestamp(curdate() - interval 7 day)
			GROUP BY service_id";

			/*Revenue Per service*/
			$revenuePerService = "SELECT SUM(CASE WHEN patient_service_bill.`status` = 'paid' THEN charge
			ELSE 0 END) AS 'Paid Amount' ,SUM(CASE WHEN patient_service_bill.status = 'unpaid' THEN charge
			ELSE 0 END) AS 'Unpaid Amount' ,services.name AS 'Service Name' FROM patient_service_bill
			LEFT JOIN services ON services.id = patient_service_bill.service_id	
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate()  - interval 7 day)
			GROUP BY service_id";

			/*Over All revenue*/
			$revenueAll = "SELECT IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_service_bill
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate() - interval 7 day)";
		}


		else if ($dataLoad =="today") {
			/*Count Service Query*/
			$countServices = "SELECT SUM(CASE WHEN services_request.test_status ='done' THEN 1 ELSE 0 END)
			AS 'Sevices Taken',
			SUM(CASE WHEN services_request.test_status='pending' THEN 1 ELSE 0 END) AS 'Sevices Untaken',
			services.name AS 'Service Name' from services_request
			LEFT JOIN services ON services.id = services_request.service_id 
			WHERE services_request.request_date >= unix_timestamp(curdate())
			GROUP BY service_id";

			/*Revenue Per service*/
			$revenuePerService = "SELECT SUM(CASE WHEN patient_service_bill.`status` = 'paid' THEN charge
			ELSE 0 END) AS 'Paid Amount' ,SUM(CASE WHEN patient_service_bill.status = 'unpaid' THEN charge
			ELSE 0 END) AS 'Unpaid Amount' ,services.name AS 'Service Name' FROM patient_service_bill
			LEFT JOIN services ON services.id = patient_service_bill.service_id	
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate())
			GROUP BY service_id";


			/*Over All revenue*/

			$revenueAll = "SELECT IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_service_bill
			WHERE patient_service_bill.created_on >= unix_timestamp(curdate())";
		}



		else  {

			/*Count Service Query*/
			$countServices = "SELECT SUM(CASE WHEN services_request.test_status ='done' THEN 1 ELSE 0 END)
			AS 'Sevices Taken',
			SUM(CASE WHEN services_request.test_status='pending' THEN 1 ELSE 0 END) AS 'Sevices Untaken',
			services.name AS 'Service Name' from services_request
			LEFT JOIN services ON services.id = services_request.service_id 
			WHERE services_request.request_date BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
			UNIX_TIMESTAMP('".$toDate." 23:59:59') GROUP BY service_id";

			/*Revenue Per service*/
			$revenuePerService = "SELECT SUM(CASE WHEN patient_service_bill.`status` = 'paid' THEN charge
			ELSE 0 END) AS 'Paid Amount' ,SUM(CASE WHEN patient_service_bill.status = 'unpaid' THEN charge
			ELSE 0 END) AS 'Unpaid Amount' ,services.name AS 'Service Name' FROM patient_service_bill
			LEFT JOIN services ON services.id = patient_service_bill.service_id	
			WHERE patient_service_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
			UNIX_TIMESTAMP('".$toDate." 23:59:59') GROUP BY service_id";


			/*Over All revenue*/
			$revenueAll = "SELECT IFNULL(SUM(CASE WHEN patient_service_bill.`status` = 'paid' 
			THEN patient_service_bill.charge ELSE 0 END),'') AS 'Paid Amount',IFNULL(SUM(CASE WHEN 
			patient_service_bill.`status` = 'Unpaid' THEN patient_service_bill.charge 
			ELSE 0 END),'') AS 'Unpaid Amount' FROM patient_service_bill
			WHERE patient_service_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
			UNIX_TIMESTAMP('".$toDate." 23:59:59')";
		}


		$Allrows   = array();

		/*Connection for patient lab bill query*/
		$queryResultServices  = mysqli_query($conn,$countServices);	
		$rowsQueryService   = array();
	    while ($r = mysqli_fetch_assoc($queryResultServices)) {
	        $rowsQueryService[] = $r;
	    }

	    $sqlRevenuePerService  = mysqli_query($conn,$revenuePerService);		
		$rowsRevenuePerService   = array();
	    while ($r = mysqli_fetch_assoc($sqlRevenuePerService)) {
	        $rowsRevenuePerService[] = $r;
	    }

	    /*Connection for patient radiology bill query*/
		$queryOverAllRevenue  = mysqli_query($conn,$revenueAll);	
		$rowsOverAllRevenue   = array();
	    while ($r = mysqli_fetch_assoc($queryOverAllRevenue)) {
	        $rowsOverAllRevenue[] = $r;
	    }

	    array_push($Allrows, $rowsQueryService);
	    array_push($Allrows, $rowsRevenuePerService);
	    array_push($Allrows, $rowsOverAllRevenue);

	    echo json_encode($Allrows);
	}
?>