<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	$operation="";
	include 'config.php';
	$operation=$_POST["operation"];
	$ward="";
	$room="";
	$getHRN="";
	//show department name 
	if($operation == "showDepartmentName"){
		$sqlSelect="SELECT id,department FROM department";
		$resultSelect=mysqli_query($conn,"$sqlSelect");
		$rows = array();
	  	while($r = mysqli_fetch_assoc($resultSelect)) {
	  		$rows[] = $r;
	  	}
	   	print json_encode($rows);
	}
	//show ward name
	if($operation == "showWard"){
		$sqlSelect="SELECT id,name FROM wards";
		$resultSelect=mysqli_query($conn,"$sqlSelect");
		$rows = array();
	  	while($r = mysqli_fetch_assoc($resultSelect)) {
	  		$rows[] = $r;
	  	}
	   	print json_encode($rows);
	}
	//show bed name
	if($operation == "showBed"){
		$room = $_POST["room"];
		$sqlSelect="SELECT id,number FROM beds where room_id ='$room'";
		$resultSelect=mysqli_query($conn,"$sqlSelect");
		$rows = array();
	  	while($r = mysqli_fetch_assoc($resultSelect)) {
	  		$rows[] = $r;
	  	}
	   	print json_encode($rows);
	}
	//show rooms numbers
	if($operation == "showRoom"){

		$ward = $_POST["ward"];
		$sqlSelect="SELECT id,number FROM rooms where ward_id='$ward'";
		$resultSelect=mysqli_query($conn,"$sqlSelect");
		$rows = array();
	  	while($r = mysqli_fetch_assoc($resultSelect)) {
	  		$rows[] = $r;
	  	}
	   	print json_encode($rows);
	}

	//to get value from dbon click HRN select
	if($operation == "selectHRNInfo"){
		$getHRN=$_POST['GetHRN'];

		$sqlSelect="SELECT first_name, last_name, mobile, dob from users where id='$getHRN'";
		$resultSelect=mysqli_query($conn,$sqlSelect);
		$rows_count= mysqli_num_rows($resultSelect);
		if($rows_count > 0){
		    $rows = array();
		    while($r = mysqli_fetch_assoc($resultSelect)) {
			    $rows[] = $r;
		    }
		    print json_encode($rows);
		}
		else{
		   echo "0";
		}       
	}
?>