<?php
/*
 * File Name    :   stocking.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar gupta
 * Created Date :   16th feb, 2016
 * Description  :   This page manage stocking details 
 */
$operation = "";
$createdBy = "";

session_start(); // session start

if (isset($_SESSION['globaluser'])) {
    $createdBy = $_SESSION['globaluser']; // user id through session
}

include 'config.php'; // import database connection file    
$operation = $_POST["operation"]; // get operation from js file

if ($operation == "showReceiver") // show receiver selection box
{
   	if (isset($_POST['receiverName'])) {
		$receiverName=$_POST["receiverName"];
	}	
	
	$query = "SELECT * FROM receiver AS r WHERE r.status = 'A' AND r.name LIKE '%".$receiverName."%'";	
	
	$result=mysqli_query($conn,$query);
	$rows = array();
	while($r = mysqli_fetch_assoc($result)) {
		$rows[] = $r;
	}

	print json_encode($rows);
}

// fetch value from patient table
if ($operation == "save") {
	
	$issuanceData = json_decode($_POST["issuanceData"]);
	foreach($issuanceData as $value) {
						
		$itemId = $value-> itemId;
		$itemPrice = $value-> itemPrice;
		$quantity = $value-> quantity;
		$cost = $value-> cost;
		$issueDate = $value-> issueDate;
		$expiryDate = $value-> expiryDate;
		$receiverId = $value-> receiverId;
		$stockQuantity = $value-> stockQuantity;
		
		if($expiryDate == "N/A") {
			 $sql = "INSERT INTO issuance(item_id, item_price, quantity,cost,issued_date,received_by,issued_by,created_on,status)
			VALUES('".$itemId."', '".$itemPrice."','".$quantity."','".$cost."','".$issueDate."',
			'".$receiverId."','".$createdBy."',UNIX_TIMESTAMP(),'A')";		
			$result= mysqli_query($conn,$sql);
			if($result) {
				$sqlUpdate = "UPDATE stocking SET Quantity = '".$stockQuantity."' WHERE Item = '".$itemId."'";
				$resultUpdate= mysqli_query($conn,$sqlUpdate);
			}
		} 
		else {
			$sql = "INSERT INTO issuance(item_id, item_price, quantity,cost,issued_date,expiry_date,received_by,issued_by,created_on,status)
			VALUES('".$itemId."', '".$itemPrice."','".$quantity."','".$cost."','".$issueDate."','".$expiryDate."',
			'".$receiverId."','".$createdBy."',UNIX_TIMESTAMP(),'A')";		
			$result= mysqli_query($conn,$sql);
			if($result) {
				$sqlUpdate = "UPDATE stocking SET Quantity = '".$stockQuantity."' WHERE Item = '".$itemId."'";
				$resultUpdate= mysqli_query($conn,$sqlUpdate);
			}
		}
	}
	//echo $result;
	if ($result) {
		echo "1";
	} 
	else {
		echo "0";
	}
}
?>