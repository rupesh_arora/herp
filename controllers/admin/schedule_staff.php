<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	
	include 'config.php';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}	
	
	//Operation to load staff Types
	if($operation == "showStaffType")			
	{
		$sql = "SELECT id,type FROM staff_type WHERE  status = 'A' ORDER BY type";
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		echo json_encode($rows);	
	}
	
	//Operation to load staff name 
	if($operation == "showStaffName")			
	{
		if (isset($_POST['staffTypeId'])) {		
			$staffTypeId = $_POST["staffTypeId"];
		}
		
		$sql = "SELECT id,CONCAT(first_name,' ',last_name) AS name FROM users WHERE  staff_type_id=".$staffTypeId." ORDER BY first_name";
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		echo json_encode($rows);
	}
	
	//Operation to load staff information 
	if($operation == "showStaffinformation")			
	{
		if (isset($_POST['userId'])) {		
			$userId = $_POST["userId"];
		}
		
		$sql = "SELECT u.address,u.mobile,u.email_id,d.designation FROM users AS u 
			LEFT JOIN designation AS d ON d.id = u.designation WHERE  u.id=".$userId."";
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		echo json_encode($rows);
	}
	
	if ($operation == "saveRepetitive") {
		if (isset($_POST['staffTypeId'])) {
			$staffTypeId = $_POST["staffTypeId"];
		}
		if (isset($_POST['staffId'])) {
			$staffId = $_POST["staffId"];
		}
		$timeDuration = $_POST["timeDuration"];
		 
		 $selectCheckAppointment = "Select * from staff_weekly_schedule where staff_id = '".$staffId."' AND is_available = 'available' AND `day` in(
			SELECT (CASE WHEN DAYNAME(start_date_time) = 'Monday' Then '0'
			WHEN DAYNAME(start_date_time) = 'Tuesday' Then '1'
			WHEN DAYNAME(start_date_time) = 'Wednesday' Then '2'
			WHEN DAYNAME(start_date_time) = 'Thursday' Then '3'
			WHEN DAYNAME(start_date_time) = 'Friday' Then '4'
			WHEN DAYNAME(start_date_time) = 'Saturday' Then '5'
			WHEN DAYNAME(start_date_time) = 'Sunday' Then '6'
			 END) AS `day` from appointments WHERE consultant_id ='".$staffId."')";
			 
		$resultCheckAppointment = mysqli_query($conn,$selectCheckAppointment);
		$rowCount = mysqli_fetch_row($resultCheckAppointment);
		if($rowCount > 1) {
			echo "appointment is booked";
			exit();
		}
		
		
		$sqlDeleteSlots = "DELETE FROM weekly_schedule_slots WHERE schedule_id 
			IN(SELECT id FROM staff_weekly_schedule WHERE staff_id = '" . $staffId . "'  
			AND staff_type_id = '" . $staffTypeId . "' AND is_available = 'available')";
		$resultDeleteslots = mysqli_query($conn, $sqlDeleteSlots);
		
		
		$sqlDeleteSlots = "DELETE FROM staff_weekly_schedule WHERE staff_id = '" . $staffId . "' 
			AND staff_type_id = '" . $staffTypeId . "'";
		$resultDeleteslots = mysqli_query($conn, $sqlDeleteSlots);
		
		if (isset($_POST['repetativeSchedule'])) {
			$repetativeSchedule = json_decode($_POST["repetativeSchedule"]);
			$counter = 0;
			foreach ($repetativeSchedule as $value) {
				
				$isAvailable = "";
				$count    = sizeof($value);
				if($count > 0){
					$isAvailable = "available";
				}
				else{
					$isAvailable = "notAvailable";
				}
				$sql = "INSERT INTO staff_weekly_schedule(staff_type_id,staff_id,time_duration,day,is_available) 
						VALUES('" . $staffTypeId . "','" . $staffId . "','" . $timeDuration . "','" . $counter . "','" . $isAvailable . "')";
				$result             = mysqli_query($conn, $sql);
				$scheduleId = mysqli_insert_id($conn);
				$counter++;
				
				
				foreach ($value as $values) {
					$fromTime    = $values->fromTime;
					$toValue    = $values->toValue;
					$notesValue    = $values->notesValue;
					$sqlDailySchedule = "INSERT INTO weekly_schedule_slots(schedule_id,from_time,to_time,notes) 
					VALUES('" . $scheduleId . "','" . $fromTime . "','" . $toValue . "','" . $notesValue . "')";
					$resultDailyResult = mysqli_query($conn, $sqlDailySchedule);
					
				}
			}
			echo "save successfully";
		}
	}
	
	if ($operation == "saveParticular") {
		if (isset($_POST['staffTypeId'])) {
			$staffTypeId = $_POST["staffTypeId"];
		}
		if (isset($_POST['staffId'])) {
			$staffId = $_POST["staffId"];
		}
		if (isset($_POST['selectedDate'])) {
			$selectedDate = $_POST["selectedDate"];
		}
		$timeDuration = $_POST["timeDuration"];
		
		$sqlDeleteSlots = "DELETE FROM date_schedule_slots WHERE schedule_id 
			IN(SELECT id FROM staff_date_schedule WHERE staff_id = '" . $staffId . "'  
			AND staff_type_id = '" . $staffTypeId . "' AND date =  '" . $selectedDate . "')";
		$resultDeleteslots = mysqli_query($conn, $sqlDeleteSlots);
		
		
		$sqlDelete = "DELETE FROM staff_date_schedule WHERE staff_id = '" . $staffId . "' 
			AND staff_type_id = '" . $staffTypeId . "'";
		$resultDelete = mysqli_query($conn, $sqlDelete);
		
		if (isset($_POST['particularSchedule'])) {
			$particularSchedule = json_decode($_POST["particularSchedule"]);
			$counter = 0;
			foreach ($particularSchedule as $value) {
				
				$isAvailable = "";
				$count    = sizeof($value);
				if($count > 0){
					$isAvailable = "available";
				}
				else{
					$isAvailable = "notAvailable";
				}
				$sql = "INSERT INTO staff_date_schedule(staff_type_id,staff_id,time_duration,date,is_available) 
						VALUES('" . $staffTypeId . "','" . $staffId . "','" . $timeDuration . "','" . $selectedDate . "','" . $isAvailable . "')";
				$result             = mysqli_query($conn, $sql);
				$scheduleId = mysqli_insert_id($conn);
				
				
				foreach ($value as $values) {
					$fromTime    = $values->fromTime;
					$toValue    = $values->toValue;
					$notesValue    = $values->notesValue;
					$sqlDateSchedule = "INSERT INTO date_schedule_slots(schedule_id,from_time,to_time,notes,created_on,created_by) 
					VALUES('" . $scheduleId . "','" . $fromTime . "','" . $toValue . "','" . $notesValue . "',UNIX_TIMESTAMP(),'" . $userId . "')";
					$resultDateResult = mysqli_query($conn, $sqlDateSchedule);
					
				}
			}
			echo "save successfully";
		}
	}
	
	if ($operation == "showRepetitive") {
		if (isset($_POST['staffTypeId'])) {
			$staffTypeId = $_POST["staffTypeId"];
		}
		if (isset($_POST['staffId'])) {
			$staffId = $_POST["staffId"];
		}
		
		$sql = "SELECT sws.id AS staff_weekly_id,sws.time_duration,sws.day,sws.is_available,wss.* 
				FROM staff_weekly_schedule AS sws
				LEFT JOIN weekly_schedule_slots AS wss ON wss.schedule_id = sws.id 
				WHERE sws.staff_type_id = '" . $staffTypeId . "' AND sws.staff_id ='" . $staffId . "'";
		$result             = mysqli_query($conn, $sql);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		echo json_encode($rows);		
	}
	if ($operation == "showParticular") {
		if (isset($_POST['staffTypeId'])) {
			$staffTypeId = $_POST["staffTypeId"];
		}
		if (isset($_POST['staffId'])) {
			$staffId = $_POST["staffId"];
		}
		if (isset($_POST['selectedDate'])) {
			$selectedDate = $_POST["selectedDate"];
		}
		
		$sql = "SELECT dss.id,sds.date,dss.from_time,dss.to_time,sds.time_duration,dss.notes ,sds.is_available 
				FROM staff_date_schedule AS sds
				LEFT JOIN date_schedule_slots AS dss ON dss.schedule_id = sds.id 
				WHERE sds.staff_type_id = '" . $staffTypeId . "' AND sds.staff_id ='" . $staffId . "' 
				AND  sds.date = '" . $selectedDate . "'";
		$result             = mysqli_query($conn, $sql);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		echo json_encode($rows);		
	}
?>