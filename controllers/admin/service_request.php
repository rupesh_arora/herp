<?php
/*File Name  :   service_request.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   31th Dec, 2015
Description  :   This page manages  all the forensic test request in consultant module*/

session_start();

/*include config file*/
include 'config.php';

/*Get user id from session table*/
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
}

/*operation to save data*/
if ($operation == "save") {
    $data      = json_decode($_POST['data']); //decode array data
    $patientId = $_POST['patientId'];
    $visitId   = $_POST['visitId'];
    $tblName   = $_POST['tblName'];
    $fkColName = $_POST['fkColName'];
    $totalCurrentReqBill = $_POST['totalCurrentReqBill'];
    $copayValue = $_POST['copayValue'];
    $copayType = $_POST['copayType'];

    
    /*query to isert data*/
    $insertQuery = "INSERT INTO " . $tblName . " (ledger_id,gl_account_id,account_payable_id,patient_id,visit_id," . $fkColName . ",request_date,pay_status,test_status,cost, status,request_type) ";
    $insertQuery .= "VALUES ";

    /*query to insert bill*/
    $insertBill = "INSERT INTO patient_service_bill (ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,service_id,charge,status,
        created_by,updated_by,created_on,updated_on,payment_mode)";
    $insertBill .= "VALUES";

    $counter = 0;
    $length = count($data);
    for ($i = 0; $i < $length; $i++) {
        
        
        $revisedCost = 0;
        $id   = 0;
        $paymentMethod = 0;
        
        $revisedCost = $data[$i][4];
        $id = $data[$i][6];
        $requestType = $data[$i][7];
        $paymentMethod = $data[$i][8];
        /*if ($key == "4") {
            echo$revisedCost = $value;
        }
        
        if ($key == "6") {
            $id = $value;//at index getting the id
        }*/

        /*$paymentMethod = end($value);*///getting the last value of array that is payment method

        //if insurance mode is insurance then check copay 
        if (strtolower($paymentMethod) == "insurance") {

            if (strtolower($copayType) == "none") {
                $billStatus = 'paid';           
            }
            else {
                if ($copayValue == 0) {
                    $billStatus = 'paid';
                }
                else{
                    $billStatus = 'unpaid';
                }
            }
        }
        else {
            $billStatus =  "unpaid";
        }

        $query  = "SELECT ledger_id,gl_account_id,account_payable_id FROM services WHERE id = " . $id . "";
        $result = mysqli_query($conn, $query);
        $ledgerId;
        $glAccountId;
        $accountPayable;
        while ($r = mysqli_fetch_assoc($result)) {
            $ledgerId = $r['ledger_id'];
            $glAccountId = $r['gl_account_id'];
            $accountPayable = $r['account_payable_id'];
        }
        
        
        if ($counter > 0) {
            $insertQuery .= ",";
            $insertBill .= ",";
        }
        
        $insertQuery .= "(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . "," . $patientId . "," . $visitId . "," . $id . ",UNIX_TIMESTAMP(),
            '".$billStatus."','Pending'," . $revisedCost . ",'A','".$requestType."')";

        $insertBill .= "(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . ",'".$patientId."','".$visitId."','".$id."','".$revisedCost."',
            '".$billStatus."','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),
            '".$paymentMethod."')";

        $counter++;        
    }   

    /*Join both query*/
    $joinQry = $insertQuery. ";" .$insertBill;
    $result = mysqli_multi_query($conn,$joinQry);
    if ($result == 1) {
        echo $result;
    }
    else{
        echo "0";
    }
}

/*operation to show only those service tests for which patient test result sent*/
if ($operation == "showServices") {
    if (isset($_POST['value'])) {
        $value = $_POST['value'];
    }
    if (isset($_POST['visitId'])) {
        $visitId = $_POST['visitId'];
    }
    
    $query  = "SELECT id,cost,name FROM services WHERE service_type_id = '".$value."' AND services.id 
    NOT IN (SELECT services_request.service_id FROM services_request WHERE 
    services_request.visit_id ='".$visitId."' AND services_request.status = 'A' AND 
    services_request.test_status !='done') AND services.status ='A' ORDER BY name";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

/*operation to count total no. of request for old test*/
if ($operation == "search") {
    $patientId = $_POST['patientId'];
    $visitId   = $_POST['visitId'];
    $tblName   = $_POST['tblName'];
    
    $searchQuery  = "SELECT COUNT(*) as total FROM " . $tblName . " WHERE patient_id = " . $patientId . " AND visit_id = " . $visitId;
    $searchResult = mysqli_query($conn, $searchQuery);
    $data         = mysqli_fetch_assoc($searchResult);
    echo $data['total'];
}
?>