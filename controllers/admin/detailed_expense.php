<?php
/*File Name  :   put_orders.php
Company Name :   Qexon Infotech
Created By   :   Kamesh Pathak
Created Date :   16th FEB, 2016
Description  :   This page manages all the orders of inventory*/

session_start(); //start session

if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
	exit();
}
/*include config file*/
include 'config.php';

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
} //nothing
if ($operation == "showData") {
	$fromDate = $_POST['fromDate'];
	$toDate = $_POST['toDate'];
	$sql = "SELECT payment_voucher.id,payment_voucher.payee_name,ledger.name AS ledger,payment_voucher.amount,payment_mode.name AS payment_mode,payment_voucher.cheque_no,payment_voucher.ledger_id ,payment_mode.id AS payment_mode_id,
		payment_voucher.registered_payee_id,payment_voucher.registered_payee_type,payment_voucher.voucher_disbursed_date AS voucher_date,payment_voucher.voucher_disbursement
		FROM payment_voucher
		LEFT JOIN ledger ON ledger.id = payment_voucher.ledger_id
		LEFT JOIN payment_mode ON payment_mode.id = payment_voucher.payment_mode
		WHERE payment_voucher.`status` = 'A' AND payment_voucher.voucher_disbursement = 'yes' ";

		if($fromDate !='' AND $toDate !=''){
			$sql.= " AND payment_voucher.voucher_disbursed_date BETWEEN '".$fromDate."' AND '".$toDate."'";
		}
		if($fromDate !='' AND $toDate ==''){
			$sql.= " AND payment_voucher.voucher_disbursed_date >= '".$fromDate."' ";
		}
		if($fromDate =='' AND $toDate !=''){
			$sql.= " AND payment_voucher.voucher_disbursed_date <= '".$toDate."' ";
		}
		$sql.=" ORDER BY payment_voucher.ledger_id";
		$result       = mysqli_query($conn, $sql);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	echo json_encode($rows);
}

if ($operation == "showSummarizedIncome") {
	
	$fromDate = $_POST['fromDate'];
	$toDate = $_POST['toDate'];

	$Qry = 'SELECT payment_voucher_item.gl_account_id,payment_voucher_item.net_amount AS `amount`,coa.account_name,payment_voucher.`payment_mode`,payment_mode.name AS payment_mode,ledger.name AS ledger,payment_voucher.ledger_id
		FROM payment_voucher_item
		LEFT JOIN chart_of_account AS coa ON coa.id =  payment_voucher_item.gl_account_id
		LEFT JOIN payment_voucher ON payment_voucher.id = payment_voucher_item.payment_voucher_id
		LEFT JOIN payment_mode ON payment_mode.id = payment_voucher.payment_mode
		LEFT JOIN ledger ON ledger.id = payment_voucher.ledger_id
		WHERE payment_voucher_item.`status` = "A"';

		if($fromDate !='' AND $toDate !=''){
			$Qry.= " AND payment_voucher.voucher_disbursed_date BETWEEN '".$fromDate."' AND '".$toDate."'";
		}
		if($fromDate !='' AND $toDate ==''){
			$Qry.= " AND payment_voucher.voucher_disbursed_date >= '".$fromDate."' ";
		}
		if($fromDate =='' AND $toDate !=''){
			$Qry.= " AND payment_voucher.voucher_disbursed_date <= '".$toDate."' ";
		}
		$Qry.=" ORDER BY payment_voucher.ledger_id";
		$result       = mysqli_query($conn, $Qry);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	echo json_encode($rows);
}