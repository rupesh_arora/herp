<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	
	include 'config.php';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}	
	//Operation to update the Test Status to 'done' 
	if($operation == "update")   
	{
		if (isset($_POST['ProcedureId'])) {
			$id=$_POST["ProcedureId"];
		}
		
		$query="UPDATE procedures_requests SET test_status = 'done' WHERE  id = '".$id."'";  
		$result=mysqli_query($conn,$query);
		echo $result;
		
	}
	//Operation to Show all Records for Lab Requests which are pending and payment is paid
	if($operation == "show"){
		
		$query= "SELECT procedures_requests.patient_id,
				procedures_requests.visit_id,
				procedures.code,procedures.name,
				procedures_requests.cost,
				procedures_requests.pay_status,
				patients.salutation , patients.first_name , 
				patients.last_name ,
				patients.mobile,
				patients.middle_name,
				patients.dob,
				patients.care_of,
				patients.address,patients.gender,
				patients.zip,patients.images_path,
				patients.care_contact,
				patients.email,
				procedures_requests.id,
				(SELECT value FROM configuration WHERE name = 'patient_prefix') AS patient_prefix,
				(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visit_prefix
				FROM `procedures_requests` 
				LEFT JOIN procedures ON procedures_requests.procedure_id = procedures.id 
				LEFT JOIN patients On patients.id = procedures_requests.patient_id 
				WHERE procedures_requests.pay_status = 'paid' AND procedures_requests.status = 'A' 
				AND procedures_requests.test_status = 'pending'";		
		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}	
	
	if($operation == "showProcessed"){
		
		$query= "SELECT procedures_requests.patient_id,
				procedures_requests.visit_id,
				procedures.code,procedures.name,
				procedures_requests.cost,
				procedures_requests.pay_status,
				patients.salutation , patients.first_name , 
				patients.last_name ,
				patients.mobile,
				patients.middle_name,
				patients.dob,
				patients.care_of,
				patients.address,patients.gender,
				patients.zip,patients.images_path,
				patients.care_contact,
				patients.email,
				procedures_requests.id,
				(SELECT value FROM configuration WHERE name = 'patient_prefix') AS patient_prefix,
				(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visit_prefix
				FROM `procedures_requests` 
				LEFT JOIN procedures ON procedures_requests.procedure_id = procedures.id 
				LEFT JOIN patients On patients.id = procedures_requests.patient_id 
				WHERE procedures_requests.pay_status = 'paid' 
				AND procedures_requests.test_status = 'done'";		
		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
?>