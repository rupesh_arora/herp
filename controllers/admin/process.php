<?php
session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

	include 'config.php';

$type = $_POST['type'];

if($type == 'new')
{
	$startdate = $_POST['startdate'].'+'.$_POST['zone'];
	$title = $_POST['title'];
	$insert = mysqli_query($conn,"INSERT INTO appointments(`notes`, `start_date_time`, `end_date_time`, `consultant_id`,`visit_id`) VALUES('$title','$startdate','$startdate','$startdate','$startdate')");
	$lastid = mysqli_insert_id($conn);
	echo json_encode(array('status'=>'success','eventid'=>$lastid));
}

if($type == 'changetitle')
{
	$eventid = $_POST['eventid'];
	$title = $_POST['title'];
	$update = mysqli_query($conn,"UPDATE appointments SET notes='$title' where id='$eventid'");
	if($update)
		echo json_encode(array('status'=>'success'));
	else
		echo json_encode(array('status'=>'failed'));
}

if($type == 'resetdate')
{
	$title = $_POST['title'];
	$startdate = $_POST['start'];
	$enddate = $_POST['end'];
	$eventid = $_POST['eventid'];
	$update = mysqli_query($conn,"UPDATE appointments SET notes='$title', start_date_time = '$startdate', end_date_time = '$enddate' where id='$eventid'");
	if($update)
		echo json_encode(array('status'=>'success'));
	else
		echo json_encode(array('status'=>'failed'));
}

if($type == 'remove')
{
	$eventid = $_POST['eventid'];
	$delete = mysqli_query($conn,"DELETE FROM appointments where id='$eventid'");
	if($delete)
		echo json_encode(array('status'=>'success'));
	else
		echo json_encode(array('status'=>'failed'));
}

if($type == 'fetch')
{
	$events = array();
	$query = mysqli_query($conn, "SELECT * FROM appointments");
	//$date = date("Y-m-d");
	//$endDate = "2016-12-30";
	while($fetch = mysqli_fetch_array($query,MYSQLI_ASSOC))
	{ 
		$e = array();
		$e['id'] = $fetch['id'];
		$e['title'] = $fetch['notes'];
		//$e['start'] = $date."T".$fetch['from_time']."-".$fetch['to_time'];
		//$e['end'] = $endDate;
		$e['start'] = $fetch['start_date_time'];
		$e['end'] = $fetch['end_date_time'];

		//$allday = ($fetch['allDay'] == "true") ? true : false;
		$e['allDay'] = false;
		array_push($events, $e);
	}
	echo json_encode($events);
}


?>