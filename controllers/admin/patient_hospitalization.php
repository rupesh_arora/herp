<?php
/*	File Name    :   patient_hospitalization.php
	Company Name :   Qexon Infotech
	Created By   :   Tushar Gupta
    Created Date :   30th Dec, 2015
	Description  :   This page  manages  all details*/

    $operation = "";
	$date      = date("Y-m-d");
	$createOn  = new DateTime();
	$createdBy = "";

	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
		$createdBy = $_SESSION['globaluser']; // user id through session
	}
	else{
		exit();
	}

	/*include config file*/
	include 'config.php';

	/*checking operation set or not*/
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	
	/*operation to show department*/
	if ($operation == "showDepartment") {
		$query  = "Select id,department FROM department WHERE status='A' ORDER BY department";
		$result = mysqli_query($conn, $query);
		$rows   = array();
		while ($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		print json_encode($rows);
	}
	
	/*operation to show ward*/
	if ($operation == "showWard") { // show state
		$departmentID = $_POST['departmentID'];
		$query     = "SELECT wards.id,CONCAT(wards.name,' ','(',wards.ward_type,')') AS name FROM wards where wards.department = ".$departmentID." ORDER BY wards.name";
		
		$result = mysqli_query($conn, $query);
		$rows   = array();
		while ($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		print json_encode($rows);
	}
	
	/*operation to show department*/
	if ($operation == "showWardRoomType") {
		$query  = "Select id,type FROM ward_room_type WHERE status='A' ORDER BY type";
		$result = mysqli_query($conn, $query);
		$rows   = array();
		while ($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		print json_encode($rows);
	}
	
	/*operation to Attending Consultant*/
	if ($operation == "showAttendingConsultant") {
		$query  = "Select id,CONCAT(first_name,' ',last_name) AS name FROM users WHERE status='A' AND staff_type_id = '5' ORDER BY first_name";
		$result = mysqli_query($conn, $query);
		$rows   = array();
		while ($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		print json_encode($rows);
	}
	
	/*operation to Operating Consultant*/
	if ($operation == "showOperatingConsultant") {
		$query  = "Select id,CONCAT(first_name,' ',last_name) AS name FROM users WHERE status='A' AND staff_type_id = '5' ORDER BY first_name";
		$result = mysqli_query($conn, $query);
		$rows   = array();
		while ($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		print json_encode($rows);
	}
	
	// save details
	if ($operation == "save") { // call operation for save information
		$patientId = $_POST['patientId'];
		$admissionDate = $_POST['admissionDate'];
		$department = $_POST['department'];
		$ward = $_POST['ward'];
		$roomType = $_POST['roomType'];
		$roomNo  = $_POST['roomNo'];
		$bedNo = $_POST['bedNo'];
		$price  = $_POST['price'];
		$attendingConsultant  = $_POST['attendingConsultant'];
		$operatingConsultant = $_POST['operatingConsultant'];
		$admissionType = $_POST['admissionType'];
		$reasonAdmission = $_POST['reasonAdmission'];
		$notes = $_POST['notes'];
		$ICU  = $_POST['ICU'];
		$lastInsertId = "";
		if($attendingConsultant == -1) {
			$attendingConsultant = 'NULL';
		}
		if($operatingConsultant == -1) {
			$operatingConsultant = 'NULL';
		}
		$transactionData = "";
		/* $queryCashAccount = "select sum(cash_account.credit - cash_account.debit) as amount from cash_account
							WHERE patient_id = ".$patientId."";
		$resultCashAccount = mysqli_query($conn,$queryCashAccount);
		while($row = mysqli_fetch_row($resultCashAccount)) {
			$bedCost = $row['0'];
		}
		if($bedCost >= 0) { */
			$insertQuery = "INSERT INTO ipd_registration (patient_id,attending_consultant_id,operating_consultant_id,admission_date,admission_type,admission_reason,notes,
				created_on,updated_on,created_by,updated_by) VALUES ('".$patientId."',".$attendingConsultant.",".$operatingConsultant.",'".$admissionDate."','".$admissionType."','".$reasonAdmission."'
				,'".$notes."','" . $createOn->getTimestamp() . "','" . $createOn->getTimestamp() . "','".$createdBy."','" . $createdBy . "')";
	
			$result = mysqli_query($conn,$insertQuery);
			$lastInsertId = mysqli_insert_id($conn);
			if($result) {
				$ipdId =  mysqli_insert_id($conn);
				if(isset($ipdId)) {
					$insertBedQuery = "INSERT INTO beds_ipd_history (ipd_id,bed_id,ic_icu,created_on,updated_on,created_by,updated_by) VALUES 
						('".$ipdId."',".$bedNo.",".$ICU.",'" . $createOn->getTimestamp() . "','" . $createOn->getTimestamp() . "','".$createdBy."','" . $createdBy . "')";
					
					$resultBed = mysqli_query($conn,$insertBedQuery);
					if($resultBed) {
						// fetch bed price
						$updateBedStatus = "UPDATE beds SET bed_availability = '0' where id = ".$bedNo."";
						$resultBedStatus = mysqli_query($conn,$updateBedStatus);
						
						$selectBedPrice = "SELECT price from beds WHERE id = ".$bedNo."";
						$resultBedPrice = mysqli_query($conn,$selectBedPrice);
						while ($r = mysqli_fetch_assoc($resultBedPrice)) {
							$bedPrice = $r['price'];
						}
						if($bedPrice == $price) {
							
							$queryGetAccountNo = "SELECT fan_id from patients WHERE id = ".$patientId."";
							$resultAccount = mysqli_query($conn,$queryGetAccountNo);
							while ($r = mysqli_fetch_assoc($resultAccount)) {
								$familyAccountNo = $r['fan_id'];
							}
							if($familyAccountNo != ""){
								$AccountNo = $familyAccountNo;
							}
							else{
								$AccountNo = $patientId;
							}
							
							$insertCash = "INSERT INTO cash_account (patient_id,depositor_id,credit,debit,date,created_on,created_by) VALUES 
									('".$patientId."','".$AccountNo."',0,".$price.",'".$date."','" . $createOn->getTimestamp() . "','" . $createdBy . "')";
								
							$resultCash = mysqli_query($conn,$insertCash);				
							
							
							if($resultCash) {
								$sql_select   = "select value from configuration where name = 'ipd_prefix'";
								$query_select = mysqli_query($conn, $sql_select);
								$rowData = array();
								$rows = mysqli_fetch_assoc($query_select);
								array_push($rowData,$rows['value'],$lastInsertId);
								echo json_encode($rowData);
								//echo array_push($rowData);
							}
						}
					}
				}
			}
		/* }
		else {
			echo "0";
		} */
	}
?>