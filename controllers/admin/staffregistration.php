<?php
/*
 * File Name    :   staffregistration.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page store user information
 */
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

include 'config.php'; // import database connection file
$operation = "";
$date      = new DateTime();
$headers   = "";
if (isset($_POST['operation'])) { // get operation from js file
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}
if (isset($_FILES["file"]["type"])) // use for image uploading
    {
    
    $validextensions = array(
        "jpeg",
        "jpg",
        "png",
        "gif"
    );
    $temporary       = explode(".", $_FILES["file"]["name"]);
    
    $extension = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION); // getting extension
   /*  $imagename = pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME) . $date->getTimestamp(); // getting a file name */
    $str = 'abcdef';
	$imagename = str_shuffle($str). $date->getTimestamp();
    $image_name = $imagename . "." . $extension;
    $name       = str_replace(" ", "", $image_name); // remove space
    
    $file_extension = end($temporary);
    if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")) && ($_FILES["file"]["size"] < 2002000) //Approx. 2mb files can be uploaded.
        && in_array($file_extension, $validextensions)) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
        } else {
            if (file_exists("../../upload_images/staff_dp/" . $name)) {
                echo "";
            } else {
                $sourcePath = $_FILES["file"]["tmp_name"]; // Storing source path of the file in a variable
                $targetPath = "../../upload_images/staff_dp/" . $name; // Target path where file is to be stored
                move_uploaded_file($sourcePath, $targetPath); // Moving Uploaded file
                
                echo $name;
                
            }
        }
    } else {
        echo "invalid file";
    }
}
// call for email validation
if ($operation == "checkEmail") {
    $email     = $_POST['Email'];
    $query     = "Select email_id from users where email_id = '" . $email . "'";
    $sqlSelect = mysqli_query($conn, $query);
    $numrows   = mysqli_num_rows($sqlSelect);
    if ($numrows > 0) {
        $rows = array();
        while ($r = mysqli_fetch_assoc($sqlSelect)) {
            $rows[] = $r;
        }
        echo "1";
    } else {
        echo "0";
    }
    
}
if ($operation == "save") { // call operation for save information
    $salutation       = $_POST['Salutation'];
    $lname            = $_POST['Lname'];
    $fname            = $_POST['Fname'];
    $dob              = $_POST['Dob'];
    $gender           = $_POST['Gender'];
    $martial          = $_POST['Martial'];
    $joiningdate      = $_POST['Joiningdate'];
    $address          = $_POST['Address'];
    $country          = $_POST['Country'];
    $state            = $_POST['State'];
    $city             = $_POST['City'];
    $zipcode          = $_POST['Zipcode'];
    $mobileCode          = $_POST['mobileCode'];
    $email            = $_POST['Email'];
    $mobile           = $_POST['Mobile'];
    $phone            = $_POST['Phone'];
    $referred         = $_POST['Referred'];
    $stafftype        = $_POST['Stafftype'];
    $designation      = $_POST['Designation'];
    $department       = $_POST['Department'];
    $degree           = $_POST['Degree'];
    $emergencyperson  = $_POST['Emergencyperson'];
    $emergencycontact = $_POST['Emergencycontact'];
    $remark           = $_POST['Remark'];
    $uid              = "herp" . mt_rand();
    $imagename        = "";
    $targetPath       = "";
    
    if ($_POST['ImageName'] != "") {
        $imagename  = $_POST['ImageName'];
        $targetPath = str_replace(" ", "", $imagename); // remove space
    } else {
        $targetPath = "";
    }
    
    function crypto_rand_secure($min, $max) // generate random value with in length into encryption form
    {
        $range = $max - $min;
        if ($range < 1)
            return $min; // not so random...
        $log    = ceil(log($range, 2));
        $bytes  = (int) ($log / 8) + 1; // length in bytes
        $bits   = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }
    function getToken($length) // generate random value with the combination of alpha numeric value 
    {
        $token        = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        $max = strlen($codeAlphabet) - 1;
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[crypto_rand_secure(0, $max)];
        }
        return $token;
    }
    $options           = array(
        'password' => crypto_rand_secure(5, 10)
    );
    $decryptedPassword = getToken(10); // generate password
    /* $decryptedPassword = "Admin@123#"; */
    $encryptedpassword = password_hash($decryptedPassword, PASSWORD_BCRYPT, $options); // generate password into encryption form
    $sql               = "INSERT INTO users(salutation,first_name,last_name,dob,gender,martial_status,joining_date,address,country_id,state_id,city_id,zip,
        email_id,mobile,phone,country_code,referred_by,staff_type_id,designation,department_id,degree_held,emergency_name,emergency_contact,remarks,
        user_id,created_on,updated_on,password,images_path)
        VALUES('" . $salutation . "','" . $fname . "','" . $lname . "','" . $dob . "','" . $gender . "','" . $martial . "','" . $joiningdate . "','" . $address . "',
        '" . $country . "','" . $state . "','" . $city . "','" . $zipcode . "',
        '" . $email . "','" . $mobile . "','" . $phone . "','" . $mobileCode . "','" . $referred . "','" . $stafftype . "','" . $designation . "',
        '" . $department . "','" . $degree . "','" . $emergencyperson . "','" . $emergencycontact . "','" . $remark . "',
        '" . $uid . "'," . $date->getTimestamp() . "," . $date->getTimestamp() . ",'" . $encryptedpassword . "','" . $targetPath . "')";
    //mail($email, "New Account Created On HRP", "USERNAME: " . $email . " PASSWORD:" . $decryptedPassword . "", $headers);//mail to ,subject,mail body,
    $result = mysqli_query($conn, $sql);
    if($result){
		$sql_id   = "SELECT LAST_INSERT_ID()";
		$query_id = mysqli_query($conn, $sql_id);
		while ($row = mysqli_fetch_row($query_id)) {
			$rowID = $row[0];
		}
		if ($rowID != '' && $rowID != '0') {
			$sql_select   = "SELECT id,user_id,salutation,(SELECT value from configuration WHERE name = 'staff_prefix') AS prefix,first_name,last_name,email_id,'".$decryptedPassword."' as pwd from users where id='".$rowID."'";
			$query_select = mysqli_query($conn, $sql_select);
			$rowData = array();
			while ($rows = mysqli_fetch_assoc($query_select)) {
				$rowData[] = $rows;
			}
			
			$user_id = $rowID;
			
			$sql_select   = "SELECT screen_id, view from user_type_access where user_type_id ='".$stafftype."'";
			
			$query_result = mysqli_query($conn, $sql_select);
			$rowSelectData = array();
			$counter = mysqli_num_rows($query_result);			
			$query_insert = "INSERT INTO pages_options (screen_id, user_id, view) VALUES ";
		
			while ($rows = mysqli_fetch_assoc($query_result)) {
				$query_insert .= "(". $rows['screen_id'] .",". $user_id .",". $rows['view'] .")";
				
				if($counter > 1) {
					$query_insert .= ",";
					$counter--;
				}
			}
			
			$query_insert_result = mysqli_query($conn, $query_insert);
			echo json_encode($rowData);
		}
	}
}

if ($operation == "showdepartment") { // show department
    $query = "SELECT id,department FROM department where status = 'A'";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "showdesignation") { // show designation
    $query = "SELECT id,designation FROM designation where status = 'A'";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "showstafftype") { // show staff type
    $query = "SELECT id,type FROM staff_type where status = 'A'";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "showcountry") { // show country
    $query = "SELECT location_id,name FROM locations where location_type = 0";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "showstate") { // show state
    $countryID = $_POST['country_ID'];
    $query     = "SELECT location_id,name FROM locations where location_type = 1 and parent_id = '$countryID'";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "showcity") { // show city
    $stateID = $_POST['state_ID'];
    $query   = "SELECT location_id,name FROM locations where location_type = 2 and parent_id = '$stateID'";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

//this operation used in view staff to load the staff data
if ($operation == "updateStaffDetail") {
    $id = "";
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    
    $query  = "SELECT users.id,users.country_code,users.images_path,users.salutation,users.first_name,users.last_name,users.dob,users.gender,users.martial_status,
            users.joining_date,users.address,users.zip,users.email_id,users.mobile,users.phone,users.referred_by,
            users.staff_type_id,users.department_id,users.designation,users.degree_held,users.emergency_name,
            users.emergency_contact,users.remarks, users.country_id,users.state_id, users.city_id,CASE WHEN users.country_code = '' OR users.country_code = NULL THEN '0' ELSE users.country_code END AS country_code
            from users where users.id = '" . $id . "' AND users.status = 'A' ";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
//this operation also used in view staff for update 
if ($operation == "editStaffDetail") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    if (isset($_POST['salutation'])) {
        $salutation = $_POST['salutation'];
    }
    if (isset($_POST['first_name'])) {
        $first_name = $_POST['first_name'];
    }
    if (isset($_POST['last_name'])) {
        $last_name = $_POST['last_name'];
    }
    if (isset($_POST['dob'])) {
        $dob = $_POST['dob'];
    }
    if (isset($_POST['gender'])) {
        $gender = $_POST['gender'];
    }
    if (isset($_POST['martial_status'])) {
        $martial_status = $_POST['martial_status'];
    }
    if (isset($_POST['joining_date'])) {
        $joining_date = $_POST['joining_date'];
    }
    if (isset($_POST['address'])) {
        $address = $_POST['address'];
    }
    if (isset($_POST['country'])) {
        $country = $_POST['country'];
    }
    if (isset($_POST['state'])) {
        $state = $_POST['state'];
    }
    if (isset($_POST['city'])) {
        $city = $_POST['city'];
    }
    if (isset($_POST['zip'])) {
        $zip = $_POST['zip'];
    }
    if (isset($_POST['email'])) {
        $email = $_POST['email'];
    }
    if (isset($_POST['mobile'])) {
        $mobile = $_POST['mobile'];
    }
    if (isset($_POST['phone'])) {
        $phone = $_POST['phone'];
    }
    if (isset($_POST['referred_by'])) {
        $referred_by = $_POST['referred_by'];
    }
    if (isset($_POST['stafftype'])) {
        $stafftype = $_POST['stafftype'];
    }
    if (isset($_POST['designation'])) {
        $designation = $_POST['designation'];
    }
    if (isset($_POST['department'])) {
        $department = $_POST['department'];
    }
    if (isset($_POST['degree_held'])) {
        $degree_held = $_POST['degree_held'];
    }
    if (isset($_POST['emergency_name'])) {
        $emergency_name = $_POST['emergency_name'];
    }
    if (isset($_POST['emergency_contact'])) {
        $emergency_contact = $_POST['emergency_contact'];
    }
    if (isset($_POST['remarks'])) {
        $remarks = $_POST['remarks'];
    }
    if (isset($_POST['mobileCode'])) {
        $mobileCode = $_POST['mobileCode'];
    }
    $imagename  = "";
    $targetPath = "";
    
    if ($_POST['imageName'] != "") {
        $imagename  = $_POST['imageName'];
        $targetPath = str_replace(" ", "", $imagename); // remove space
        if ($_POST['imagesPath'] != "" && $_POST['imagesPath'] != null) {
            
            $filename = $_POST['imagesPath'];
            $file     = basename($filename);
            $path     = "../../upload_images/staff_dp";
            unlink($path . "/" . $file);
        }
        $sql = "UPDATE users SET salutation = '" . $salutation . "', first_name = '" . $first_name . "', last_name = '" . $last_name . "', dob = '" . $dob . "', 
                gender = '" . $gender . "', martial_status = '" . $martial_status . "', joining_date = '" . $joining_date . "', address = '" . $address . "', 
                country_id = '" . $country . "',  state_id = '" . $state . "', city_id = '" . $city . "', zip = '" . $zip . "', email_id = '" . $email . "', 
                mobile = '" . $mobile . "', country_code = '" . $mobileCode . "', phone = '" . $phone . "', referred_by = '" . $referred_by . "', staff_type_id = '" . $stafftype . "', 
                designation = '" . $designation . "', department_id = '" . $department . "', degree_held = '" . $degree_held . "', 
				emergency_name = '" . $emergency_name . "', emergency_contact = '" . $emergency_contact . "', 
				remarks = '" . $remarks . "', updated_on='". $date->getTimestamp()."', images_path='" . $targetPath . "'
				WHERE id = '" . $id . "' ";
				echo $sql;
				if($_SESSION['globaluser'] == $id)
				{
					$_SESSION['image'] = $targetPath;
				}
    } else {
        $sql = "UPDATE users SET salutation = '" . $salutation . "', first_name = '" . $first_name . "', last_name = '" . $last_name . "', dob = '" . $dob . "', 
            gender = '" . $gender . "', martial_status = '" . $martial_status . "', joining_date = '" . $joining_date . "', address = '" . $address . "', 
            country_id = '" . $country . "',  state_id = '" . $state . "', city_id = '" . $city . "', zip = '" . $zip . "', email_id = '" . $email . "', 
            mobile = '" . $mobile . "', country_code = '" . $mobileCode . "', phone = '" . $phone . "', referred_by = '" . $referred_by . "', staff_type_id = '" . $stafftype . "', 
            designation = '" . $designation . "', department_id = '" . $department . "', degree_held = '" . $degree_held . "', emergency_name = '" . $emergency_name . "', 
            emergency_contact = '" . $emergency_contact . "', updated_on='".$date->getTimestamp()."', remarks = '" . $remarks . "' WHERE id = '" . $id . "' ";
    }
    $result = mysqli_query($conn, $sql);
    echo "SUCCESS";
}

if ($operation == "showStaff") { // show staff type
    if (isset($_POST['value'])) {
        $value = $_POST['value'];
    }
    $query = "SELECT id, CONCAT(first_name,' ',last_name) AS name FROM users 
    WHERE staff_type_id = '".$value."' AND status = 'A'";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
?>