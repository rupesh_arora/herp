<?php
/* ****************************************************************************************************
 * File Name    :   view_ipd_lab_request.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   4th mar, 2016
 * Description  :   This page  manages ipd lab requests
 *************************************************************************************************** */	
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	
	include 'config.php';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}	
	//Operation to update the Test Status to 'done' and Generate a new specimen
	if($operation == "update")   
	{
		if (isset($_POST['lab_test_id'])) {
			$id=$_POST["lab_test_id"];
		}
		if (isset($_POST['specimen_description'])) {
			$specimenDescription=$_POST["specimen_description"];
		}
		$labRequestid = $_POST["labRequestId"];
		
		$query="UPDATE ipd_lab_test_requests SET test_status = 'done',request_date=UNIX_TIMESTAMP() WHERE  id = '".$id."'";  
		$result=mysqli_query($conn,$query);
		
		if($result)
		{
			$query="INSERT INTO ipd_patient_specimen(lab_test_id,ipd_lab_test_requested_id,description,created_on,created_by)
			VALUES (".$id.",".$labRequestid.",'".$specimenDescription."',UNIX_TIMESTAMP(),".$_SESSION['globaluser'].")"; 
			$result=mysqli_query($conn,$query);
			echo $conn->insert_id;
		}
	}
	//Operation to Show all Records for Lab Requests which are pending and payment is paid
	if($operation == "show"){
		
		$query= "SELECT ipd_lab_test_requests.patient_id,
				ipd_lab_test_requests.ipd_id,
				lab_tests.test_code,lab_tests.name,
				ipd_lab_test_requests.cost,
				specimens_types.type,
				specimens_types.description,
				ipd_lab_test_requests.pay_status,
				patients.salutation , patients.first_name , 
				patients.last_name ,
				patients.mobile,
				patients.middle_name,
				patients.dob,
				patients.care_of,
				patients.address,patients.gender,
				patients.zip,patients.images_path,
				patients.care_contact,
				patients.email,
				ipd_lab_test_requests.id,
				(SELECT value FROM configuration WHERE name = 'patient_prefix') AS patient_prefix,
				(SELECT value FROM configuration WHERE name = 'ipd_prefix') AS ipd_prefix
				FROM `ipd_lab_test_requests` 
				LEFT JOIN lab_tests ON ipd_lab_test_requests.lab_test_id = lab_tests.id 
				LEFT JOIN specimens_types ON specimens_types.id = lab_tests.specimen_id 
				LEFT JOIN patients On patients.id = ipd_lab_test_requests.patient_id 
				WHERE ipd_lab_test_requests.pay_status = 'paid' 
				AND ipd_lab_test_requests.test_status = 'pending'";		
		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);		
	}
	
	//Operation to load Specimen Types
	if($operation == "loadSpecimen")			
	{
		$sql = "SELECT id as value,type as text FROM specimens_types WHERE  status = 'A'";
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		echo json_encode($rows);	
	}
	//Operation to load Lab tests 
	if($operation == "loadLabTests")			
	{
		$sql = "SELECT id as value,name as text FROM lab_tests WHERE  status = 'A'";
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		echo json_encode($rows);
	}
	
	// get component name
	if($operation == "getComponentName") {
		
		$labRequestId = $_POST['requestId'];
		$sql = "SELECT lab_test_component.name as componentName FROM lab_test_component
				LEFT JOIN ipd_lab_test_request_component ON ipd_lab_test_request_component.lab_component_id = lab_test_component.id
				WHERE  ipd_lab_test_request_component.ipd_lab_test_request_id = '".$labRequestId."'";
		
		
		$result=mysqli_query($conn,$sql);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		echo json_encode($rows);
	}
?>