<?php
	
	session_start();
	if(!session_id()){
		exit();
	}
	$toDate = '';
	$fromDate = '';
	
	include_once('config.php');

	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET['operation'])){
		$operation=$_GET["operation"];
	}

	if ($operation=="showChartData") {

		if (isset($_POST['dataLoad'])) {
			$dataLoad = $_POST["dataLoad"];
		}
		if (isset($_POST['fromDate'])) {
			$fromDate = $_POST["fromDate"];
		}
		if (isset($_POST['toDate'])) {
			$toDate = $_POST["toDate"];
		}
		if ($dataLoad =="all") {

			$query = "SELECT COUNT(*) as Visits,(CASE WHEN ISNULL(department.department) THEN 'Others' ELSE department.department END) AS Department FROM `visits` 
			LEFT JOIN department ON department.id = visits.department
			GROUP BY department.id ORDER BY COUNT(*) desc";

			$sql = "SELECT COUNT(CASE WHEN patients.gender = 'M' THEN visits.id END) AS Male,
				COUNT(CASE WHEN patients.gender = 'F' THEN visits.id END) AS Female,
				(CASE WHEN ISNULL(department.department) THEN 'Others' ELSE department.department END) AS Department FROM `visits` 
				LEFT JOIN department ON department.id = visits.department
				LEFT JOIN patients ON patients.id = visits.patient_id
				GROUP BY department.id ORDER BY COUNT(*) desc";

			$ageQry = "SELECT IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=0 && 
				TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 11) THEN '1' ELSE '0' END ),0) as '0-10 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=11 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 21) 
				THEN '1' ELSE '0' END ),0) as '11-20 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=21 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 31) 
				THEN '1' ELSE '0' END ),0) as '21-30 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=31 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 41) 
				THEN '1' ELSE '0' END ),0) as '31-40 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=41 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 51) 
				THEN '1' ELSE '0' END ),0) as '41-50 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=51 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 61) 
				THEN '1' ELSE '0' END ),0) as '51-60 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=61 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 71) 
				THEN '1' ELSE '0' END ),0) as '61-70 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=71 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 81) 
				THEN '1' ELSE '0' END ),0) as '71-80 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=81 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 91) 
				THEN '1' ELSE '0' END ),0) as '81-90 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=91) 
				THEN '1' ELSE '0' END ),0) as 'Above 90 Yrs'
				FROM visits 
				LEFT JOIN department ON department.id = visits.department
				LEFT JOIN patients as p ON p.id = visits.patient_id";	
		}

		else if ($dataLoad =="last30days") {
			$query = "SELECT COUNT(*) as Visits,(CASE WHEN ISNULL(department.department) THEN 'Others' ELSE department.department END) AS Department FROM `visits` 
				LEFT JOIN department ON department.id = visits.department
				WHERE visits.created_on >= unix_timestamp(curdate() - interval 1 month)
				GROUP BY department.id ORDER BY COUNT(*) desc";

			$sql = "SELECT COUNT(CASE WHEN patients.gender = 'M' THEN visits.id END) AS Male,
				COUNT(CASE WHEN patients.gender = 'F' THEN visits.id END) AS Female,
				(CASE WHEN ISNULL(department.department) THEN 'Others' ELSE department.department END) AS Department FROM `visits` 
				LEFT JOIN department ON department.id = visits.department
				LEFT JOIN patients ON patients.id = visits.patient_id
				WHERE visits.created_on >= unix_timestamp(curdate() - interval 1 month)
				GROUP BY department.id ORDER BY COUNT(*) desc";

			$ageQry = "SELECT IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=0 && 
				TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 11) THEN '1' ELSE '0' END ),0) as '0-10 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=11 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 21) 
				THEN '1' ELSE '0' END ),0) as '11-20 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=21 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 31) 
				THEN '1' ELSE '0' END ),0) as '21-30 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=31 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 41) 
				THEN '1' ELSE '0' END ),0) as '31-40 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=41 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 51) 
				THEN '1' ELSE '0' END ),0) as '41-50 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=51 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 61) 
				THEN '1' ELSE '0' END ),0) as '51-60 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=61 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 71) 
				THEN '1' ELSE '0' END ),0) as '61-70 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=71 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 81) 
				THEN '1' ELSE '0' END ),0) as '71-80 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=81 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 91) 
				THEN '1' ELSE '0' END ),0) as '81-90 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=91) 
				THEN '1' ELSE '0' END ),0) as 'Above 90 Yrs'
				FROM visits 
				
				LEFT JOIN department ON department.id = visits.department
				LEFT JOIN patients as p ON p.id = visits.patient_id
				WHERE visits.created_on >= unix_timestamp(curdate() - interval 1 month)";
		}

		else if ($dataLoad =="last7days") {

			$query = "SELECT COUNT(*) as Visits,(CASE WHEN ISNULL(department.department) THEN 'Others' ELSE department.department END) AS Department FROM `visits` 
				LEFT JOIN department ON department.id = visits.department
				WHERE visits.created_on >= unix_timestamp(curdate() - interval 7 day)
				GROUP BY department.id ORDER BY COUNT(*) desc";

			$sql = "SELECT COUNT(CASE WHEN patients.gender = 'M' THEN visits.id END) AS Male,
				COUNT(CASE WHEN patients.gender = 'F' THEN visits.id END) AS Female,
				(CASE WHEN ISNULL(department.department) THEN 'Others' ELSE department.department END) AS Department FROM `visits` 
				LEFT JOIN department ON department.id = visits.department
				LEFT JOIN patients ON patients.id = visits.patient_id
				WHERE visits.created_on >= unix_timestamp(curdate() - interval 7 day)
				GROUP BY department.id ORDER BY COUNT(*) desc";

			$ageQry = "SELECT IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=0 && 
				TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 11) THEN '1' ELSE '0' END ),0) as '0-10 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=11 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 21) 
				THEN '1' ELSE '0' END ),0) as '11-20 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=21 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 31) 
				THEN '1' ELSE '0' END ),0) as '21-30 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=31 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 41) 
				THEN '1' ELSE '0' END ),0) as '31-40 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=41 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 51) 
				THEN '1' ELSE '0' END ),0) as '41-50 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=51 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 61) 
				THEN '1' ELSE '0' END ),0) as '51-60 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=61 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 71) 
				THEN '1' ELSE '0' END ),0) as '61-70 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=71 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 81) 
				THEN '1' ELSE '0' END ),0) as '71-80 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=81 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 91) 
				THEN '1' ELSE '0' END ),0) as '81-90 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=91) 
				THEN '1' ELSE '0' END ),0) as 'Above 90 Yrs'
				FROM visits 
				
				LEFT JOIN department ON department.id = visits.department
				LEFT JOIN patients as p ON p.id = visits.patient_id
				WHERE visits.created_on >= unix_timestamp(curdate() - interval 7 day)";	
		}

		else if ($dataLoad =="today") {

			$query = "SELECT COUNT(*) as Visits,(CASE WHEN ISNULL(department.department) THEN 'Others' ELSE department.department END) AS Department FROM `visits` 
				LEFT JOIN department ON department.id = visits.department
				where visits.created_on >= unix_timestamp(CURDATE())
				GROUP BY department.id ORDER BY COUNT(*) desc";

			$sql = "SELECT COUNT(CASE WHEN patients.gender = 'M' THEN visits.id END) AS Male,
				COUNT(CASE WHEN patients.gender = 'F' THEN visits.id END) AS Female,
				(CASE WHEN ISNULL(department.department) THEN 'Others' ELSE department.department END) AS Department FROM `visits` 
				LEFT JOIN department ON department.id = visits.department
				LEFT JOIN patients ON patients.id = visits.patient_id
				WHERE visits.created_on >= unix_timestamp(curdate())
				GROUP BY department.id ORDER BY COUNT(*) desc";

			$ageQry = "SELECT IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=0 && 
				TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 11) THEN '1' ELSE '0' END ),0) as '0-10 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=11 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 21) 
				THEN '1' ELSE '0' END ),0) as '11-20 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=21 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 31) 
				THEN '1' ELSE '0' END ),0) as '21-30 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=31 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 41) 
				THEN '1' ELSE '0' END ),0) as '31-40 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=41 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 51) 
				THEN '1' ELSE '0' END ),0) as '41-50 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=51 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 61) 
				THEN '1' ELSE '0' END ),0) as '51-60 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=61 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 71) 
				THEN '1' ELSE '0' END ),0) as '61-70 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=71 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 81) 
				THEN '1' ELSE '0' END ),0) as '71-80 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=81 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 91) 
				THEN '1' ELSE '0' END ),0) as '81-90 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=91) 
				THEN '1' ELSE '0' END ),0) as 'Above 90 Yrs'
				FROM visits 
				
				LEFT JOIN department ON department.id = visits.department
				LEFT JOIN patients as p ON p.id = visits.patient_id
				WHERE visits.created_on >= unix_timestamp(curdate())";	
		}

		else{

			$query = "SELECT COUNT(*) as Visits,(CASE WHEN ISNULL(department.department) THEN 'Others' ELSE department.department END) AS Department FROM `visits` 
			LEFT JOIN department ON department.id = visits.department
			WHERE visits.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59')
			GROUP BY department.id ORDER BY COUNT(*) desc";

			$sql = "SELECT COUNT(CASE WHEN patients.gender = 'M' THEN visits.id END) AS Male,
				COUNT(CASE WHEN patients.gender = 'F' THEN visits.id END) AS Female,
				(CASE WHEN ISNULL(department.department) THEN 'Others' ELSE department.department END) AS Department FROM `visits` 
				LEFT JOIN department ON department.id = visits.department
				LEFT JOIN patients ON patients.id = visits.patient_id
				WHERE visits.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59')
				GROUP BY department.id ORDER BY COUNT(*) desc";

			$ageQry = "SELECT IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=0 && 
				TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 11) THEN '1' ELSE '0' END ),0) as '0-10 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=11 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 21) 
				THEN '1' ELSE '0' END ),0) as '11-20 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=21 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 31) 
				THEN '1' ELSE '0' END ),0) as '21-30 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=31 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 41) 
				THEN '1' ELSE '0' END ),0) as '31-40 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=41 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 51) 
				THEN '1' ELSE '0' END ),0) as '41-50 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=51 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 61) 
				THEN '1' ELSE '0' END ),0) as '51-60 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=61 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 71) 
				THEN '1' ELSE '0' END ),0) as '61-70 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=71 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 81) 
				THEN '1' ELSE '0' END ),0) as '71-80 Yrs',
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=81 && TIMESTAMPDIFF(YEAR,p.dob,CURDATE())< 91) 
				THEN '1' ELSE '0' END ),0) as '81-90 Yrs' ,
				IFNULL(SUM(CASE WHEN (TIMESTAMPDIFF(YEAR,p.dob,CURDATE()) >=91) 
				THEN '1' ELSE '0' END ),0) as 'Above 90 Yrs'
				FROM visits 
				
				LEFT JOIN department ON department.id = visits.department
				LEFT JOIN patients as p ON p.id = visits.patient_id
				WHERE visits.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') AND 
				UNIX_TIMESTAMP('".$toDate." 23:59:59')";	
		}

		$Allrows   = array();
		
		$ageRows   = array();
    	if (isset($_POST['departmentId'])) {
    		$departmentId =  $_POST['departmentId'];
    		if ($dataLoad =="all"){
    			$ageQry .= " WHERE department.id = '".$departmentId."'";
    		}
    		else {
    			$ageQry .= " AND department.id = '".$departmentId."'";
    		}
    		
    		$result=mysqli_query($conn,$ageQry);
			$rows = array();
			while($r = mysqli_fetch_assoc($result)) {
			 $rows[] = $r;
			}
			print json_encode($rows);
    		exit();
    	}

    	$resultAge  = mysqli_query($conn,$ageQry);


		$result  = mysqli_query($conn,$query);		
		$rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }

	    $resultGender  = mysqli_query($conn,$sql);
	    $newRows   = array();
    	while ($r = mysqli_fetch_assoc($resultGender)) {
        	$newRows[] = $r;
    	}

    	while ($r = mysqli_fetch_assoc($resultAge)) {
        	$ageRows[] = $r;
    	}
	    
	    array_push($Allrows, $rows);
	    array_push($Allrows, $newRows);
	    array_push($Allrows, $ageRows);
	    echo json_encode($Allrows);
	}
?>