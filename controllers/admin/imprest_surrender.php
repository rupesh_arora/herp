<?php
/*File Name  :   roster_management.php
Company Name :   Qexon Infotech
Created By   :   Tushar Gupta
Created Date :   21th March, 2015
Description  :   This page manages entry roster and attendence management*/

	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}

	/*include config file*/
	include 'config.php';


	/*checking operation set or not*/
	if (isset($_POST['operation'])) {
	    $operation = $_POST["operation"];
	} else if (isset($_GET["operation"])) {
	    $operation = $_GET["operation"];
	}

	if ($operation == "show") {
	    $query = "SELECT iw.id AS warrent_id ,i_d.id,i_d.imprest_request_id,iw.staff_id,iw.ledger_id,CONCAT(users.first_name,' ',users.last_name) AS name,nature_of_duty,iw.amount,iw.assignment_start,iw.assignment_end,iw.est_day,ledger.name AS ledger_name,(SELECT value FROM configuration  WHERE name = 'imprest_prefix') AS imprest_prefix FROM imprest_disbursement AS i_d 
			LEFT JOIN imprest_warrent AS iw ON   i_d.imprest_request_id = iw.imprest_request_id 
			LEFT JOIN users ON users.id = iw.staff_id 
			LEFT JOIN ledger ON ledger.id = iw.ledger_id
			WHERE i_d.`status`= 'A'";
		    $result = mysqli_query($conn, $query);
		    $totalrecords = mysqli_num_rows($result);
		    $rows         = array();
		    while ($r = mysqli_fetch_assoc($result)) {
		        $rows[] = $r;
		    }
		    //print json_encode($rows);
		    
		    $json = array(
		        'sEcho' => '1',
		        'iTotalRecords' => $totalrecords,
		        'iTotalDisplayRecords' => $totalrecords,
		        'aaData' => $rows
		    );
		    echo json_encode($json);	
	}

	if ($operation == "showWarrentDetail") {

		$imprestRequestId = $_POST['imprestRequestId'];

	    $query = "SELECT iwl.id,iwl.description,iwl.initial_amount,coa.account_name,iwl.spent_amount,iwl.spent_amount_status FROM 
	    		`imprest_warrent_line_item` AS iwl
				LEFT JOIN chart_of_account AS coa ON iwl.chart_of_account_id = coa.id 
				WHERE iwl.imprest_request_id ='".$imprestRequestId."'";
		    $result = mysqli_query($conn, $query);
		    $totalrecords = mysqli_num_rows($result);
		    $rows         = array();
		    while ($r = mysqli_fetch_assoc($result)) {
		        $rows[] = $r;
		    }
		    print json_encode($rows);
		    
		   /* $json = array(
		        'sEcho' => '1',
		        'iTotalRecords' => $totalrecords,
		        'iTotalDisplayRecords' => $totalrecords,
		        'aaData' => $rows
		    );
		    echo json_encode($rows);*/	
	}

	if ($operation == "spentAmount") { // show insurance company type

		$imprestRequestId = $_POST['imprestRequestId'];
	    $query = "SELECT IFNULL(SUM(spent_amount),0) AS spent_amount FROM imprest_warrent_line_item WHERE imprest_request_id = '".$imprestRequestId."' ";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "updateLineItems") { // show insurance company type
		
		$warrentId = $_POST['warrentId'];

		$warrentQuery = "UPDATE imprest_warrent SET surrender_date = UNIX_TIMESTAMP(),surrender_status ='yes' where id = '".$warrentId."'";
        $warrentResult = mysqli_query($conn, $warrentQuery);


		$updateLineItems = json_decode($_POST['updateLineItems']);
		$counter = 0;
		foreach ($updateLineItems as $value) {
			$id    = $value->id;
	        $amount     = $value->amount;
	       
	        $query = "UPDATE imprest_warrent_line_item SET spent_amount = '".$amount."',spent_amount_status ='paid' where id = '".$id."'";
	         $result = mysqli_query($conn, $query);
		}

	   
	    //$result = mysqli_query($conn, $query);
		echo $result;
	    
	}
?>