<?php

/*File Name  :   consultant.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   31th Dec, 2015
Description  :   This page  manages consultant */
	session_start();

	$operation = "";	
	$patientId = "";	
	
	/*include config file*/
	include 'config.php';

	/*checking operation set or not*/
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	else{}

	/*Get user id from session table*/
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
		exit();
	}

	/*operation to show patient detail for that visit*/
	if ($operation == "visitConsultant") {

		$visitID = $_POST['visitid'];	
		$visitPrefix = $_POST['visitPrefix'];
		$sql       = "SELECT value FROM configuration WHERE name = 'visit_prefix'";
		$result    = mysqli_query($conn, $sql);
		
		while ($r = mysqli_fetch_assoc($result)) {
			$prefix = $r['value'];
		}
		if($prefix == $visitPrefix){

			$sql = "SELECT visits.opd_room_id,visits.consultant_id,visit_type.type,visits.updated_on,visits.patient_id,CONCAT(patients.salutation,' ',patients.first_name,' ',patients.last_name) as Patient_Name
			   ,patients.created_on,patients.dob,patients.gender,(SELECT count(visits.patient_id) from visits
			   where visits.patient_id= patients.id) as totalVisit, services.code as `serviceType`, 
			   (SELECT value FROM configuration WHERE name = 'patient_prefix') AS patient_prefix  from visits
			   LEFT JOIN visit_type on visits.visit_type = visit_type.id
			   LEFT JOIN patients on visits.patient_id = patients.id
			   LEFT JOIN patient_service_bill on visits.id = patient_service_bill.visit_id
			   LEFT JOIN services on  patient_service_bill.service_id = services.id 
			  where patient_service_bill.visit_id=visits.id and visits.id  = '".$visitID."' AND 
			  patient_service_bill.status ='paid' AND visits.visit_type !='4' ";
			
			$result = mysqli_query($conn,$sql);
			$row=array();
			$rowCount = mysqli_num_rows($result);
			if($rowCount != "")
			{
				while($r = mysqli_fetch_assoc($result)){
					$row[]=$r;
				}
				print json_encode($row);
			}
			else{
				echo "";
			}
		}
		else{
			echo "";
		}
	}

	/*Operation to store opd room id and consultant id in visit table*/
	if ($operation == "updateVisitConsultantId") {
		if (isset($_POST['visit_id'])) {
		    $visit_id = $_POST['visit_id'];
		}

		/* $sqlSelect = "SELECT room_id, consultant_id FROM opd_rooms WHERE consultant_id = '".$userId."'";
		$querySelect     = mysqli_query($conn, $sqlSelect);
		$rows_count   = mysqli_num_rows($querySelect);
		if ($rows_count >= 1) {

			while ($row = mysqli_fetch_row($querySelect)) {
	            $room_id = $row[0];
	            $consultant_id = $row[1];
	        } 

	       $sqlUpdate = "UPDATE visits SET consultant_id ='".$consultant_id."' ,opd_room_id = '".$room_id."' 
	        WHERE id ='".$visit_id."' ";
	        $result = mysqli_query($conn, $sqlUpdate);
	        echo $result; */
	        echo "1";
		/* }
		else{
			echo "0";
		} */
	}

	if ($operation == "oldHistoryVisitsSearch") {
		
		if (isset($_POST['patientId'])) {
			$patientId = $_POST['patientId'];
		}

		$sqlSelect ="SELECT visits.id as visit_id, visit_type.`type` AS visit_type , CONCAT(users.first_name,' ',users.last_name) as consultant_name,
		GROUP_CONCAT(services.name) as service_name,visits.updated_on AS visit_date,
		(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visit_prefix from visits
		LEFT JOIN visit_type ON visits.visit_type = visit_type.id
		LEFT JOIN users ON visits.consultant_id = users.id
		LEFT JOIN patient_service_bill ON visits.id = patient_service_bill.visit_id 
		LEFT JOIN services on services.id = patient_service_bill.service_id  
		WHERE visit_type.`type` != 'Self Request' AND patient_service_bill.`status` = 'paid'
		AND visits.patient_id = '".$patientId."' GROUP BY visits.id ";
		
		$result         = mysqli_query($conn, $sqlSelect);
	    $rows           = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "saveIPDRequest") {

		if (isset($_POST['staffTypeId'])) {
			$staffTypeId = $_POST['staffTypeId'];
		}
		if (isset($_POST['operatingConsultantId'])) {
			$operatingConsultantId = $_POST['operatingConsultantId'];
		}
		if (isset($_POST['reason'])) {
			$reason = $_POST['reason'];
		}
		if (isset($_POST['patientId'])) {
			$patientId = $_POST['patientId'];
		}
		if (isset($_POST['visitId'])) {
		    $visitId = $_POST['visitId'];
		}

		$sqlSelect  = "SELECT visit_id FROM ipd_patient_request WHERE visit_id = '".$visitId."'";
		$sqlResult = mysqli_query($conn,$sqlSelect);
		$count = mysqli_num_rows($sqlResult);
		if ($count == 0) {

			$sqlInsert = "INSERT INTO ipd_patient_request (patient_id,operating_consultant_id,attending_consultant_id,visit_id,reason,created_on) VALUES('".$patientId."',
				'".$operatingConsultantId."','".$userId."','".$visitId."','".$reason."',
				UNIX_TIMESTAMP())";

			$resultInsert = mysqli_query($conn,$sqlInsert);
			echo $resultInsert; 
		}
		else{
			echo "0";
		}
	}
?>