<?php
	$operation = "";
	$drugId = "";
	$drugName = "";
	$drugCode = "";
	$id = "";
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	
	include 'config.php';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}	
	if($operation == "searchDrug"){
		
		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		
		$query ="SELECT d.composition,d.name,d.alternate_name,d.unit,u.unit as unit_measure  ,d.cost,d.price,c_d.category,u.unit_abbr,d.drug_code, l_j.name AS alternate_drug_name 
			 from drugs AS d 
			LEFT JOIN units AS u ON d.unit = u.id 
			LEFT JOIN drug_categories AS c_d ON d.drug_category_id = c_d.id 
			LEFT JOIN drugs AS l_j ON d.alternate_drug_id = l_j.id 
			WHERE d.status = 'A' and c_d.status = 'A' and u.status = 'A' and d.id ='".$id."' ";
			
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}

		print json_encode($rows);
	}
	
	
	if($operation == "search"){
		
		if (isset($_POST['drugId'])) {
			$drugId=$_POST["drugId"];
		}
		if (isset($_POST['drugName'])) {
			$drugName=$_POST["drugName"];
		}
		if (isset($_POST['drugCode'])) {
			$drugCode=$_POST["drugCode"];
		}
		
		$isFirst = "false";
		
		$query ="SELECT d.id,(SELECT value FROM configuration WHERE name ='drug_prefix' ) AS drug_prefix,d.composition,d.name,d.alternate_name,d.drug_category_id,d.unit,
		d.cost,d.price,d.alternate_drug_id,c_d.category,u.unit_abbr,d.drug_code,u.unit as unit_measure,
		l_j.name AS alternate_drug_name ,pharmacy_inventory.quantity from drugs AS d 
		LEFT JOIN units AS u ON d.unit = u.id 
		LEFT JOIN drug_categories AS c_d ON d.drug_category_id = c_d.id 
		LEFT JOIN drugs AS l_j ON d.alternate_drug_id = l_j.id 
		LEFT JOIN pharmacy_inventory on pharmacy_inventory.drug_name_id = d.id 
		WHERE d.status = 'A' and c_d.status = 'A' ";
		
		if($drugId != '' || $drugName != '' || $drugCode != ''){
			$query .=" AND ";
		} 
		if($drugName != ''){
			if(!$isFirst){
				$query .=" AND ";
			}
			$query .= " d.name LIKE '%".$drugName."%'";
			$isFirst = "true";
		}
		if($drugCode != ''){
			if(!$isFirst){
				$query .=" AND ";
			}
			$query .= " d.drug_code LIKE '%".$drugCode."%'";
			$isFirst = "true";
		}
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}

		print json_encode($rows);
	}
	
	if($operation == "searchId")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		
		$sql_select= "SELECT p.id,p.drug_name_id,p.quantity,p.manufacture_name,CONCAT(p.unit_vol,' ',u.unit) AS unit_vol,d.name,d.alternate_name,d.cost,
			p.unit_id,p.manufacture_date,p.expiry_date,p.unit_vol AS unit_v,u.unit,u.unit,d.drug_code,d.composition 
			FROM 	pharmacy_inventory p
			LEFT JOIN drugs AS d ON d.id = p.drug_name_id 
			LEFT JOIN units AS u ON u.id = p.unit_id 
			WHERE d.`status` = 'A' And p.`status` = 'A' And p.drug_name_id = '".$id."' ";
		$result_select = mysqli_query($conn,$sql_select);
		$rows_count= mysqli_num_rows($result_select);
		$rows = array();
		if ($rows_count > 0) {
			while($r = mysqli_fetch_assoc($result_select)) {
				$rows[] = $r;
			}
			print json_encode($rows);
		}	
		else{
			echo "0";
		}
	}
?>