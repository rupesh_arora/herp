<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET['operation'])){
		$operation=$_GET["operation"];
	}

	if($operation == "bindTableData"){
		if (isset($_POST['visitId'])) {
			$visitId=$_POST['visitId'];
		}
		else if(isset($_GET['visitId'])){
			$visitId=$_GET['visitId'];
		}
		
		if (isset($_POST['paitentId'])) {
		$paitentId=$_POST['paitentId'];
		}
		else if(isset($_GET['paitentId'])){
			$paitentId=$_GET['paitentId'];
		}
		
		$sql = "SELECT forensic_test_result.*,patients.id as patient_id,concat(patients.salutation,' ',patients.first_name,' ',
				patients.last_name) as patient_name,visits.created_on as visit_date,forensic_tests.name,visits.id as visit_id FROM `forensic_test_result` 
				LEFT JOIN patient_forensic_specimen ON forensic_test_result.specimen_id=patient_forensic_specimen.id 
				LEFT JOIN forensic_test_request ON forensic_test_request.id=patient_forensic_specimen.forensic_test_id 
				LEFT JOIN forensic_tests ON forensic_tests.id = forensic_test_request.forensic_test_id 
				LEFT JOIN patients ON forensic_test_request.patient_id = patients.id 
				LEFT JOIN visits ON visits.id = forensic_test_request.visit_id 
				WHERE forensic_test_request.visit_id=".$visitId." AND forensic_test_request.patient_id=".$paitentId." AND 
				forensic_test_request.test_status='resultsent' AND forensic_test_request.pay_status='settled'";
		
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);	
	}
	
	// search forensic resultsent
	if($operation == "searchFroensicResult") {
		
		$first_name = "";
		$last_name = "";
		if (isset($_POST['visitId'])) {
			$visitId=$_POST['visitId'];
		}
		
		if(isset($_POST['patientId'])) {
			$patientId=$_POST['patientId'];
		}
				
		if (isset($_POST['patinetName'])) {
			$patinetName=$_POST['patinetName'];
			$name = explode(" ", $patinetName);
			
			if(isset($name[0])) {
				$first_name = $name[0];
			}
			if(isset($name[1])) {
				$last_name = $name[1];
			}
		}
		
		$query = "SELECT forensic_test_result.*,patients.id as patient_id,concat(patients.salutation,' ',patients.first_name,' ',
				patients.last_name) as patient_name,visits.created_on as visit_date,forensic_tests.name,visits.id as visit_id,
				(select value from configuration where name = 'patient_prefix') as prefix,
				(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visitPrefix FROM `forensic_test_result` 
				LEFT JOIN patient_forensic_specimen ON forensic_test_result.specimen_id=patient_forensic_specimen.id 
				LEFT JOIN forensic_test_request ON forensic_test_request.id=patient_forensic_specimen.forensic_test_id 
				LEFT JOIN forensic_tests ON forensic_tests.id = forensic_test_request.forensic_test_id 
				LEFT JOIN patients ON forensic_test_request.patient_id = patients.id 
				LEFT JOIN visits ON visits.id = forensic_test_request.visit_id";
		
		$firstName = "false";
		
		if($visitId != '' || $patientId != '' || $patinetName != '') {
			$query .= "	WHERE forensic_test_request.test_status='resultsent' AND forensic_test_request.pay_status='settled'";
			$isFirst = "true";
		}
		else{
			$query .= "	WHERE forensic_test_request.test_status='resultsent' AND forensic_test_request.pay_status='settled'";
		}
		
		if ($visitId != '') {
			if ($isFirst != "false") {
				$query .= " AND ";
			}
			$query .= " forensic_test_request.visit_id ='" . $visitId . "'";
			$isFirst = "true";
		}
		if ($patientId != '') {
			if ($isFirst != "false") {
				$query .= " AND ";
			}
			$query .= " forensic_test_request.patient_id = '" . $patientId . "'";
			$isFirst = "true";
		}
		if ($first_name != '' || $last_name != '') {
			if ($isFirst != "false") {
				$query .= " AND ";
			}
			$query .= " patients.first_name LIKE '%" . $first_name . "%' OR patients.last_name LIKE '%" . $last_name . "%'";
			$isFirst = "true";
		}		
		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}		 
	
		echo json_encode($rows);	
	}
?>