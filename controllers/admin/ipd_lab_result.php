<?php
/* ****************************************************************************************************
 * File Name    :   ipd_lab_result.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   4th mar, 2016
 * Description  :   This page  manages ipd lab results
 *************************************************************************************************** */	
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}

	if($operation == "loadSpecimen"){
		if (isset($_POST['specimenId'])) {
			$id=$_POST["specimenId"];
		}
		
		$sql = "SELECT ipd_lab_test_requests.patient_id,ipd_lab_test_requests.ipd_id,lab_tests.test_code,
			lab_tests.name, lab_tests.id, l.name AS component_name,l.normal_range,ipd_lab_test_requests.cost,
			specimens_types.type,ipd_patient_specimen.description,ipd_patient_specimen.created_on,ipd_lab_test_result.result_flag,
			ipd_lab_test_result.report,l.id AS component_id  
  
  		FROM ipd_patient_specimen
		LEFT JOIN `ipd_lab_test_requests`  ON ipd_lab_test_requests.id = ipd_patient_specimen.lab_test_id
		LEFT JOIN lab_tests ON ipd_lab_test_requests.lab_test_id = lab_tests.id 
		LEFT JOIN ipd_lab_test_request_component ON ipd_lab_test_request_component.ipd_lab_test_request_id = ipd_lab_test_requests.id
		LEFT JOIN lab_test_component AS l ON l.id = ipd_lab_test_request_component.lab_component_id
		LEFT JOIN specimens_types ON specimens_types.id = lab_tests.specimen_id 
		LEFT JOIN patients On patients.id = ipd_lab_test_requests.patient_id 
		LEFT JOIN ipd_lab_test_result ON ipd_lab_test_result.ipd_specimen_id = ipd_patient_specimen.id
		WHERE ipd_patient_specimen.status = 'A' AND ipd_patient_specimen.id = '".$id."' AND ipd_lab_test_requests.pay_status = 'paid' AND
		ipd_lab_test_requests.test_status = 'done'";
		
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		echo json_encode($rows);			
	}
	if($operation == "save"){
		
		if (isset($_POST['specimenId'])) {
			$specimenId=$_POST['specimenId'];
		}
		if (isset($_POST['report'])) {
			$report=$_POST['report'];
		}
		
		if (isset($_POST['tableData'])) {
			$tableData=json_decode($_POST["tableData"]);
			$first = "false";
			foreach($tableData as $value) {			
				
				$result = $value-> result;
				$range = $value-> normalRange;
				$componentId = $value-> componentId;
				
				$sql_select = "SELECT COUNT(*) from ipd_lab_test_result where ipd_specimen_id =".$specimenId."";  
				$result_select = mysqli_query($conn,$sql_select);  
				while ($row=mysqli_fetch_row($result_select))
				{
					$count = $row[0];
				}
				
				if($count == 0 || $first == "true")
				{			
					$sql = "INSERT INTO `ipd_lab_test_result`(`ipd_specimen_id`,`lab_test_component_id`,`report`,`normal_range`,`result`,`created_on`, `updated_on`, `created_by`, `updated_by`)
					 VALUES('".$specimenId."','".$componentId."','".$report."','".$range."', '".$result."',UNIX_TIMESTAMP(), UNIX_TIMESTAMP(), '".$_SESSION['globaluser']."', '".$_SESSION['globaluser']."')";		
					
					$result= mysqli_query($conn,$sql);	
					 if($result){
						$sqlupdate = "update `ipd_lab_test_requests` SET `pay_status`='settled',`test_status`='resultsent' where id = 
						(select ipd_patient_specimen.lab_test_id from ipd_patient_specimen 
						Left Join ipd_lab_test_result on ipd_patient_specimen.id = ipd_lab_test_result.ipd_specimen_id 
						where ipd_lab_test_result.ipd_specimen_id = '".$specimenId."')";
						mysqli_query($conn,$sqlupdate);
					} 					
					$first = "true";
					echo $result;
				}
				else{

					$sql = "UPDATE `ipd_lab_test_result` SET 
					`lab_test_component_id` = '".$componentId."', 
					`report`='".$report."',`normal_range`='".$range."',
					`result`='".$result."',`updated_on`= UNIX_TIMESTAMP(),
					`updated_by`='".$_SESSION['globaluser']."' WHERE ipd_specimen_id=".$specimenId."";			
					$result= mysqli_query($conn,$sql);			
					echo $result;
				}
			}				
		}			
	}
?>