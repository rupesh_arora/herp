<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}
	if ($operation == "save") {
		$roomNo = $_POST['roomNo'];
		$desc = $_POST['desc'];

		$selRoom = "SELECT room_no FROM ot_rooms WHERE room_no ='".$roomNo."' AND status = 'A'";
		$checkRoom = mysqli_query($conn,$selRoom);
		$countRooms = mysqli_num_rows($checkRoom);
		if ($countRooms == "0") {
			$sql = "INSERT INTO ot_rooms (room_no,description,created_on,updated_on,status,created_by,updated_by) VALUES('".$roomNo."','".$desc."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'A',
				'".$userId."','".$userId."')";
			    
		    $result = mysqli_query($conn, $sql);
	    	echo $result;
		}
		else{
			echo "0";
		}

			
	}
	if ($operation == "show") { // show active data
    
	    $sql = "SELECT id,room_no,description FROM ot_rooms WHERE `status`= 'A'";
	    $result = mysqli_query($conn, $sql);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	if ($operation == "checked") {
    
	    $query = "SELECT id,room_no,description FROM ot_rooms WHERE `status`= 'I'";
	    
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	if ($operation == "update") // update data
	{
	    $description = "";
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    if (isset($_POST['roomNo'])) {
	        $roomNo = $_POST['roomNo'];
	    }
	    if (isset($_POST['desc'])) {
	        $desc = $_POST['desc'];
	    }	
		
	    $id = $_POST['id'];
		
		$selRoom = "SELECT room_no FROM ot_rooms WHERE room_no ='".$roomNo."' AND status = 'A' 
		AND id !='".$id."'";
		$checkRoom = mysqli_query($conn,$selRoom);
		$countRooms = mysqli_num_rows($checkRoom);
		if ($countRooms < 1) {
		$sql    = "UPDATE ot_rooms set room_no = '".$roomNo."',description= '".$desc."',updated_on=				UNIX_TIMESTAMP(),updated_by = '".$userId."' where id = '".$id."' ";

			$result = mysqli_query($conn, $sql);
			echo $result;
		}
		else {
			echo "0";
		}
	    
			
	}

	if ($operation == "restore") // for restore    
    {
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    if (isset($_POST['roomNumber'])) {
	        $roomNumber = $_POST['roomNumber'];
	    }

	    $selRoom = "SELECT room_no FROM ot_rooms WHERE room_no ='".$roomNumber."' AND status = 'A' 
		AND id !='".$id."'";
		$checkRoom = mysqli_query($conn,$selRoom);
		$countRooms = mysqli_num_rows($checkRoom);
		if ($countRooms < 1) {
			$sql    = "UPDATE ot_rooms SET status= 'A'  WHERE  id = '" . $id . "'";
		    $result = mysqli_query($conn, $sql);
		    echo $result;
		}
		else{
			echo "0";
		}

		    
	}
	if ($operation == "delete") {
	    if (isset($_POST['id'])) {
	        $id = $_POST['id'];
	    }
	    
	    $select = "SELECT ot_room_id FROM patient_ot_registration WHERE ot_room_id = '" . $id . "' AND status= 'A'";
	    $resultCheck = mysqli_query($conn, $select);
	    $countRows = mysqli_num_rows($resultCheck);
	    if($countRows == 0){
	    	$sql    = "UPDATE ot_rooms SET status= 'I' where id = '" . $id . "'";
		    $result = mysqli_query($conn, $sql);
		    echo $result;
	    }
	    else{
	    	echo "0";
	    }
	}
?>