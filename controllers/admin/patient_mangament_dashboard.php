<?php
/*File Name  :   lab_test_request.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page manages  all the lab test request*/

session_start();

/*include config file*/
include 'config.php';

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
}
/*Get user id from session table*/
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
	exit();
}
if ($operation == "showChartDataForPatientMgt") {

	$sqlSelect =  "SELECT 
		SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 1) THEN '1' ELSE '0' END) AS Jan,
		SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 2) THEN '1' ELSE '0' END) AS Feb,  
		SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 3) THEN '1' ELSE '0' END) AS Mar,  
		SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 4) THEN '1' ELSE '0' END) AS Apr,  
		SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 5) THEN '1' ELSE '0' END) AS May,  
		SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 6) THEN '1' ELSE '0' END) AS Jun,  
		SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 7) THEN '1' ELSE '0' END) AS Jul,  
		SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 8) THEN '1' ELSE '0' END) AS Aug,  
		SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 9) THEN '1' ELSE '0' END) AS Sept, 
		SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 10) THEN '1' ELSE '0' END) AS Oct, 
		SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 11) THEN '1' ELSE '0' END) AS Nov, 
		SUM(CASE WHEN (MONTH(FROM_UNIXTIME(created_on)) = 12) THEN '1' ELSE '0' END) AS `Dec`
		FROM patients                                            
		WHERE YEAR(FROM_UNIXTIME(created_on)) =  YEAR(CURDATE())";

	$result = mysqli_query($conn,$sqlSelect);
	
	$rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "showChartDataForClinicalSupport") {

	$Allrows   = array();

	$sqlGroupByService = "SELECT COUNT(visits.id) AS Visit,services.name As service_name from visits
		LEFT JOIN visit_type ON visits.visit_type = visit_type.id
		LEFT JOIN patient_service_bill ON visits.id = patient_service_bill.visit_id 
		LEFT JOIN services on services.id = patient_service_bill.service_id   
		WHERE visit_type.id != 4 AND patient_service_bill.`status` = 'paid'
		GROUP BY patient_service_bill.service_id";

	$sqlGroupByTriage = "SELECT COUNT(visits.id) AS Visit,visits.triage_status	from visits
		LEFT JOIN visit_type ON visits.visit_type = visit_type.id
		LEFT JOIN patients ON visits.patient_id = patients.id
		LEFT JOIN patient_service_bill ON visits.id = patient_service_bill.visit_id   
		WHERE visit_type.id != 4 AND 	patient_service_bill.`status` = 'paid' 
		GROUP BY visits.triage_status";	

	$selectByConsultantNotReq = "SELECT (SELECT COUNT(radiology_test_request.id) from radiology_test_request
		LEFT JOIN visits ON visits.id = radiology_test_request.visit_id
		WHERE visits.visit_type != 4 AND radiology_test_request.test_status = 'pending') AS
		Radiology_visits,

		(SELECT COUNT(lab_test_requests.id) from lab_test_requests
		LEFT JOIN visits ON visits.id = lab_test_requests.visit_id
		WHERE visits.visit_type != 4 AND lab_test_requests.test_status = 'pending') AS Lab_visits,

		(SELECT COUNT(procedures_requests.id) from procedures_requests
		LEFT JOIN visits ON visits.id = procedures_requests.visit_id
		WHERE visits.visit_type != 4 AND procedures_requests.test_status = 'pending') AS Procedure_visits,

		(SELECT COUNT(services_request.id) from services_request
		LEFT JOIN visits ON visits.id = services_request.visit_id
		WHERE visits.visit_type != 4 AND services_request.test_status = 'pending') AS Service_visits,

		(SELECT COUNT(forensic_test_request.id) from forensic_test_request
		LEFT JOIN visits ON visits.id = forensic_test_request.visit_id
		WHERE visits.visit_type != 4 AND forensic_test_request.test_status = 'pending') AS Forensic_visits"
		;

	$selectByConsultantReq = "SELECT (SELECT COUNT(radiology_test_request.id) from radiology_test_request
		LEFT JOIN visits ON visits.id = radiology_test_request.visit_id
		WHERE visits.visit_type != 4 AND radiology_test_request.test_status != 'pending') AS
		Radiology_visits,

		(SELECT COUNT(lab_test_requests.id) from lab_test_requests
		LEFT JOIN visits ON visits.id = lab_test_requests.visit_id
		WHERE visits.visit_type != 4 AND lab_test_requests.test_status != 'pending') AS Lab_visits,

		(SELECT COUNT(procedures_requests.id) from procedures_requests
		LEFT JOIN visits ON visits.id = procedures_requests.visit_id
		WHERE visits.visit_type != 4 AND procedures_requests.test_status != 'pending') AS Procedure_visits,

		(SELECT COUNT(services_request.id) from services_request
		LEFT JOIN visits ON visits.id = services_request.visit_id
		WHERE visits.visit_type != 4 AND services_request.test_status != 'pending') AS Service_visits,

		(SELECT COUNT(forensic_test_request.id) from forensic_test_request
		LEFT JOIN visits ON visits.id = forensic_test_request.visit_id
		WHERE visits.visit_type != 4 AND forensic_test_request.test_status != 'pending') AS Forensic_visits
		";
	
	$sqlSelectPharmacy = "SELECT COUNT(patients_prescription_request.id) AS Pharmacy,dispense_status from patients_prescription_request GROUP BY dispense_status";

	$queryVisitedToDr = "SELECT SUM(CASE WHEN visits.id NOT IN(SELECT visit_id FROM
		patient_diagnosis_request) THEN '1' ELSE '0' END) AS `Not Queued Patient`,
		(SELECT COUNT(DISTINCT(visit_id)) FROM patient_diagnosis_request) AS `Queued Patients`
		FROM visits";	

	$resultGroupByService = mysqli_query($conn,$sqlGroupByService);
	$resultGroupByTriage = mysqli_query($conn,$sqlGroupByTriage);
	$resultByConsultantNotReq = mysqli_query($conn,$selectByConsultantNotReq);
	$resultByConsultantReq = mysqli_query($conn,$selectByConsultantReq);
	$resultPharmacy = mysqli_query($conn,$sqlSelectPharmacy);
	$resultVisitedToDr = mysqli_query($conn,$queryVisitedToDr);
	
	$rowsGroupByService   = array();
    while ($r = mysqli_fetch_assoc($resultGroupByService)) {
        $rowsGroupByService[] = $r;
    }

    $rowsGroupByTriage   = array();
    while ($r = mysqli_fetch_assoc($resultGroupByTriage)) {
        $rowsGroupByTriage[] = $r;
    }

    $rowsByConsultantNotReq = array();
    while ($r = mysqli_fetch_assoc($resultByConsultantNotReq)) {
        $rowsByConsultantNotReq[] = $r;
    }

    $rowsByConsultantReq = array();
    while ($r = mysqli_fetch_assoc($resultByConsultantReq)) {
        $rowsByConsultantReq[] = $r;
    }

    $rowsPharmacy = array();
    while ($r = mysqli_fetch_assoc($resultPharmacy)) {
        $rowsPharmacy[] = $r;
    }

    $visitedToDrRows = array();    
	while ($r = mysqli_fetch_assoc($resultVisitedToDr)) {
    	$visitedToDrRows[] = $r;
	}

    array_push($Allrows, $rowsGroupByService);
    array_push($Allrows, $rowsGroupByTriage);
    array_push($Allrows, $rowsByConsultantNotReq);
    array_push($Allrows, $rowsByConsultantReq);
    array_push($Allrows, $rowsPharmacy);    
    array_push($Allrows, $visitedToDrRows);    
    print json_encode($Allrows);
}

if ($operation == "showChartDataForOPD") {
	$OPDBookingRows = array();
	$visitedToDrRows = array();
	$Allrows = array();
	$queryOPDBooking = "SELECT COUNT(*) as Visits,(CASE WHEN ISNULL(department.department) THEN 'Others' ELSE department.department END) AS Department FROM `visits` 
		LEFT JOIN department ON department.id = visits.department
		GROUP BY department.id ORDER BY COUNT(*) desc";
	$resultOPDBooking = mysqli_query($conn,$queryOPDBooking);
	while ($r = mysqli_fetch_assoc($resultOPDBooking)) {
    	$OPDBookingRows[] = $r;
	}

	$queryVisitedToDr = "SELECT SUM(CASE WHEN visits.id NOT IN(SELECT visit_id FROM
		patient_diagnosis_request) THEN '1' ELSE '0' END) AS `Visited-Patient`,
		(SELECT COUNT(DISTINCT(visit_id)) FROM patient_diagnosis_request) AS `Not-Visited-Patient`
		FROM visits";
	$resultVisitedToDr = mysqli_query($conn,$queryVisitedToDr);
	while ($r = mysqli_fetch_assoc($resultVisitedToDr)) {
    	$visitedToDrRows[] = $r;
	}


	array_push($Allrows, $OPDBookingRows);
	array_push($Allrows, $visitedToDrRows);
	echo json_encode($Allrows);
}

if ($operation == "showChartDataForAppointment") {
	$scheduledAppointmentRows = array();
	$activatedAppointmentRows = array();
	$Allrows = array();
	$queryScheduledAppointment = "SELECT COUNT(appointments.id) Appointments,CONCAT(users.first_name,' ',
		users.last_name) AS `Consultant Name` FROM appointments
		LEFT JOIN users on users.id = appointments.consultant_id
		WHERE DATE_FORMAT(appointments.start_date_time, '%Y-%m-%d') = curdate()
 		GROUP BY appointments.consultant_id";
	$resultScheduledAppointment = mysqli_query($conn,$queryScheduledAppointment);
	while ($r = mysqli_fetch_assoc($resultScheduledAppointment)) {
    	$scheduledAppointmentRows[] = $r;
	}

	$queryActivatedAppointment = "SELECT SUM(CASE WHEN `status` = 'activated' THEN '1' ELSE '0' END) AS
	`Activated Appointments`,SUM(CASE WHEN `status` != 'activated' THEN '1' ELSE '0' END) AS `NOT Activated Appointments` FROM appointments 
	WHERE DATE_FORMAT(appointments.start_date_time, '%Y-%m-%d') = curdate()";
	$resultActivatedAppointment = mysqli_query($conn,$queryActivatedAppointment);
	while ($r = mysqli_fetch_assoc($resultActivatedAppointment)) {
    	$activatedAppointmentRows[] = $r;
	}


	array_push($Allrows, $scheduledAppointmentRows);
	array_push($Allrows, $activatedAppointmentRows);
	echo json_encode($Allrows);
}

if ($operation == "showChartDataForInpatient") {
	$Allrows = array();
	$labTestRows = array();
	$radiologyTestRows = array();
	$sqlLabTest = "SELECT SUM(CASE WHEN lab_test_requests.`test_status` != 'pending' THEN '1' ELSE '0' 
		END) AS `Result Sent`,SUM(CASE WHEN lab_test_requests.`test_status` = 'pending' THEN '1' ELSE '0'
		END) AS `Result Pending` FROM lab_test_requests
		WHERE DATE_FORMAT(FROM_UNIXTIME(request_date), '%Y-%m-%d') = CURDATE()";

	$resultLabTest = mysqli_query($conn,$sqlLabTest);
	while ($r = mysqli_fetch_assoc($resultLabTest)) {
    	$labTestRows[] = $r;
	}

	$sqlRadiologyTest = "SELECT SUM(CASE WHEN `test_status` != 'pending' THEN '1' ELSE '0' END) AS 
		`Result Sent`,SUM(CASE WHEN `test_status` = 'pending' THEN '1' ELSE '0'	END) AS `Result Pending`
		FROM radiology_test_request
		WHERE DATE_FORMAT(FROM_UNIXTIME(request_date), '%Y-%m-%d') = CURDATE()";

	$resultRadiologyTest = mysqli_query($conn,$sqlRadiologyTest);
	while ($r = mysqli_fetch_assoc($resultRadiologyTest)) {
    	$radiologyTestRows[] = $r;
	}

	array_push($Allrows, $labTestRows);
	array_push($Allrows, $radiologyTestRows);
	echo json_encode($Allrows);
}
?>