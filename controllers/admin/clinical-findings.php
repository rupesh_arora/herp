<?php
/*File Name  :   clinical_findings.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   4th Jan, 2016
Description  :   This page manages  all the alergy test request*/

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
    
$operation = ""; //define opertaion null

/*include config file*/
include 'config.php';

$date = new DateTime();

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

/*operation to get data and show it*/
if ($operation == "getData") {
    
    if (isset($_POST['visitId'])) {
        $visitId = $_POST['visitId'];
    }
    
    if (isset($_POST['patientId'])) {
        $patientId = $_POST['patientId'];
    }
    $sqlSelect = "SELECT presenting_complaint AS presentingComplaint, general_appearance AS generalAppearance, pv_pr AS pvpr, 
			respiratory, psychological_status AS psychologicalStatus, cvs, ent, abdomen, pmh_psh AS pmhpsh, cns, poh, eye, pgh, 
			muscular_skeletal AS muscularSkeletal, fsh, skin, clinical_notes AS clinicalNotes, 
			provisional_diagnosis AS provisionalDiagnosis FROM clinical_findings
			where visit_id = '" . $visitId . "' AND patient_id = '" . $patientId . "' ";
    
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $rows_count   = mysqli_num_rows($resultSelect);
    if ($rows_count > 0) {
        $rows = array();
        while ($r = mysqli_fetch_assoc($resultSelect)) {
            $rows[] = $r;
        }
        print json_encode($rows);
    } else {
        echo "0";
    }
}

/*operation to save data*/
if ($operation == "saveData") {
    if (isset($_POST['visitId'])) {
        $visitId = $_POST['visitId'];
    }
    if (isset($_POST['patientId'])) {
        $patientId = $_POST['patientId'];
    }
    if (isset($_POST['presentingComplaint'])) {
        $presentingComplaint = $_POST['presentingComplaint'];
    }
    if (isset($_POST['generalAppearance'])) {
        $generalAppearance = $_POST['generalAppearance'];
    }
    if (isset($_POST['pvpr'])) {
        $pvpr = $_POST['pvpr'];
    }
    if (isset($_POST['respiratory'])) {
        $respiratory = $_POST['respiratory'];
    }
    if (isset($_POST['psychologicalStatus'])) {
        $psychologicalStatus = $_POST['psychologicalStatus'];
    }
    if (isset($_POST['cvs'])) {
        $cvs = $_POST['cvs'];
    }
    if (isset($_POST['ent'])) {
        $ent = $_POST['ent'];
    }
    if (isset($_POST['abdomen'])) {
        $abdomen = $_POST['abdomen'];
    }
    if (isset($_POST['pmhpsh'])) {
        $pmhpsh = $_POST['pmhpsh'];
    }
    if (isset($_POST['cns'])) {
        $cns = $_POST['cns'];
    }
    if (isset($_POST['poh'])) {
        $poh = $_POST['poh'];
    }
    if (isset($_POST['eye'])) {
        $eye = $_POST['eye'];
    }
    if (isset($_POST['pgh'])) {
        $pgh = $_POST['pgh'];
    }
    if (isset($_POST['muscularSkeletal'])) {
        $muscularSkeletal = $_POST['muscularSkeletal'];
    }
    if (isset($_POST['fsh'])) {
        $fsh = $_POST['fsh'];
    }
    if (isset($_POST['skin'])) {
        $skin = $_POST['skin'];
    }
    if (isset($_POST['clinicalNotes'])) {
        $clinicalNotes = $_POST['clinicalNotes'];
    }
    if (isset($_POST['provisionalDiagnosis'])) {
        $provisionalDiagnosis = $_POST['provisionalDiagnosis'];
    }
    
    /*Firstly delete data that is present in db for particular patient id and visist id then insert data*/
    $result = mysqli_query($conn, "DELETE FROM clinical_findings WHERE visit_id = '" . $visitId . "' AND patient_id = '" . $patientId . "';");
    
    $sqlSaveQry = "INSERT INTO clinical_findings(visit_id, patient_id, presenting_complaint, general_appearance, pv_pr, respiratory, 
		psychological_status, cvs, ent, abdomen, pmh_psh, cns, poh, eye, pgh, muscular_skeletal, fsh, skin, clinical_notes, provisional_diagnosis,created_on,updated_on,created_by,updated_by)
		VALUES('" . $visitId . "', '" . $patientId . "','" . $presentingComplaint . "', '" . $generalAppearance . "', '" . $pvpr . "', '" . $respiratory . "', 
		'" . $psychologicalStatus . "', '" . $cvs . "', '" . $ent . "', '" . $abdomen . "', '" . $pmhpsh . "', '" . $cns . "', '" . $poh . "', 
		'" . $eye . "', '" . $pgh . "', '" . $muscularSkeletal . "','" . $fsh . "', '" . $skin . "','" . $clinicalNotes . "', '" . $provisionalDiagnosis . "',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'" . $userId . "','" . $userId . "')";
    
    $result = mysqli_query($conn, $sqlSaveQry);
    echo $result;
}

// save label details
if($operation == "saveLabelDetails") {
	if(isset($_POST['finalDataObj'])) {
		$finalDataObj = json_decode($_POST['finalDataObj']);
	}
	
	if(isset($_POST['labelText'])) {
		$labelText = $_POST['labelText'];
	}
		
	foreach ($finalDataObj as $value) {
		$insertQuery = "INSERT INTO clinical_notes_templates (name,value,created_on,updated_on,created_by,updated_by,status) VALUES ('".$labelText."','".$value."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."','A')";
		$result= mysqli_query($conn,$insertQuery); 
	}
	if($result) {
		echo "1";
	} 
	else {
		echo "0";
	}
}

// show old details
if($operation == "oldDetailsSearch") {

	if(isset($_POST['labelText'])) {
		$labelText = $_POST['labelText'];
	}
	
	$sqlSelect = "SELECT id,value from clinical_notes_templates WHERE name = '".$labelText."'";
	$result         = mysqli_query($conn, $sqlSelect);
    $rows           = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

// show old details
if($operation == "deleteDetails") {

	if(isset($_POST['id'])) {
		$id = $_POST['id'];
	}
	
	$query = "DELETE from clinical_notes_templates WHERE id = '".$id."'";
	$result = mysqli_query($conn, $query);
	if($result) {
		echo $result;
	}	
}

if ($operation == "showtemplateData")
{
	$lblValue=$_POST["lblValue"];
	$txtValue=$_POST["txtValue"];
	
	$query = "SELECT * FROM clinical_notes_templates WHERE name = '".$lblValue."' AND value LIKE '%".$txtValue."%'";	
	
	$result=mysqli_query($conn,$query);
	$rows = array();
	while($r = mysqli_fetch_assoc($result)) {
		$rows[] = $r;
	}

	print json_encode($rows);
}
?>