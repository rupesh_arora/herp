<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "showLedger") { // show insurance company type
	    $query = "SELECT id,name FROM ledger where status = 'A' ";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	if ($operation == "showAssetAccount") { // show insurance company type
	    $query = "SELECT c.id,c.`account_name` FROM chart_of_account AS c
				LEFT JOIN account_type AS a ON a.id = c.`account_type` 
				WHERE a.`type` = 'Asset' AND c.`status` = 'A' AND c.is_parent = 'no' ";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "savePaymentMode") {

		$name = $_POST['name'];
		$ledger = $_POST['ledger'];
		$accountNumber = $_POST['accountNumber'];
		$startingBalance = $_POST['startingBalance'];
		$assetAccount = $_POST['assetAccount'];
		$payeeName = $_POST['payeeName'];
		$soapUserName = $_POST['soapUserName'];
		$soapPassword = $_POST['soapPassword'];
		$receivedPayment = $_POST['receivedPayment'];
        $makePayment = $_POST['makePayment'];
        $alnumMode = $_POST['alnumMode'];
        $accountActive = $_POST['accountActive'];
        $availableForbanking = $_POST['availableForbanking'];
        $source_bank = $_POST['source_bank'];
        $defaultMode = $_POST['defaultMode'];

		$selSubType = "SELECT account_no FROM payment_mode WHERE account_no ='".$accountNumber."' AND status = 'A'";
		$checkSubType = mysqli_query($conn,$selSubType);
		$countSubType = mysqli_num_rows($checkSubType);

		if ($countSubType == "0") {

			$query = "INSERT INTO payment_mode(name,ledger_type,account_no,starting_balance,
				asset_account_type,payee_name,soap_user_name,soap_password,created_by,updated_by,
				created_on,updated_on,is_receive_payment,is_make_payment,is_alnum_mode,is_account_active,
				is_available_for_banking,is_available_bank_source) 
				VALUES('".$name."','".$ledger."','".$accountNumber."','".$startingBalance."','".$assetAccount."','".$payeeName."','".$soapUserName."','".$soapPassword."','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$receivedPayment."','".$makePayment."','".$alnumMode."','".$accountActive."','".$availableForbanking."','".$source_bank."')";

			$result = mysqli_query($conn, $query);
			$last_id = mysqli_insert_id($conn);

			if ($defaultMode) {
				$unCheckedAllPaymode = "UPDATE payment_mode SET is_default_payment_mode = '0' WHERE ledger_type='".$ledger."'";
	      		$updatePayMode = "UPDATE payment_mode SET is_default_payment_mode = '1' WHERE id='".$last_id."'";
	      		$payModeResult = mysqli_multi_query($conn,$unCheckedAllPaymode.';'.$updatePayMode);
	        }
			echo $result;
		}
		else{
			echo "0";
		}
	}

	if ($operation == "show") { // show active data

		$query = "SELECT payment_mode.id,payment_mode.name,payment_mode.ledger_type,payment_mode.account_no,payment_mode.starting_balance,payment_mode.asset_account_type,chart_of_account.account_name AS account_name,payment_mode.payee_name,payment_mode.soap_user_name,payment_mode.soap_password,ledger.name AS ledger_name,payment_mode.is_receive_payment,payment_mode.is_make_payment,payment_mode.is_alnum_mode,payment_mode.is_account_active,payment_mode.is_available_for_banking,payment_mode.is_available_bank_source,payment_mode.is_default_payment_mode
			FROM payment_mode 
			LEFT JOIN ledger ON ledger.id = payment_mode.ledger_type
			left join chart_of_account on chart_of_account.id = payment_mode.asset_account_type
			WHERE payment_mode.`status` = 'A' ORDER BY ledger_name";
	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);  
    }

    if ($operation == "checked") {
	    
	    $query = "SELECT payment_mode.id,payment_mode.name,payment_mode.ledger_type,payment_mode.account_no,payment_mode.starting_balance,payment_mode.asset_account_type,chart_of_account.account_name AS account_name,payment_mode.payee_name,payment_mode.soap_user_name,payment_mode.soap_password,ledger.name AS ledger_name,payment_mode.is_receive_payment,payment_mode.is_make_payment,payment_mode.is_alnum_mode,payment_mode.is_account_active,payment_mode.is_available_for_banking,payment_mode.is_available_bank_source,payment_mode.is_default_payment_mode
			FROM payment_mode 
			LEFT JOIN ledger ON ledger.id = payment_mode.ledger_type
			left join chart_of_account on chart_of_account.id = payment_mode.asset_account_type
			WHERE payment_mode.`status` = 'I' ORDER BY ledger_name";
	    
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);   
	}

	if ($operation == "update") // update data
	{
	    $name = $_POST['name'];
		$ledger = $_POST['ledger'];
		$accountNumber = $_POST['accountNumber'];
		$startingBalance = $_POST['startingBalance'];
		$assetAccount = $_POST['assetAccount'];
		$payeeName = $_POST['payeeName'];
		$soapUserName = $_POST['soapUserName'];
		$soapPassword = $_POST['soapPassword'];
	    $id = $_POST['id'];
	    $receivedPayment = $_POST['receivedPayment'];
        $makePayment = $_POST['makePayment'];
        $alnumMode = $_POST['alnumMode'];
        $accountActive = $_POST['accountActive'];
        $availableForbanking = $_POST['availableForbanking'];
        $source_bank = $_POST['source_bank'];
        $defaultMode = $_POST['defaultMode'];
		
		$selSubType = "SELECT account_no FROM payment_mode WHERE account_no ='".$accountNumber."' AND status = 'A' AND id !='".$id."'";
		$checkSubType = mysqli_query($conn,$selSubType);
		$countSubType = mysqli_num_rows($checkSubType);

		if ($countSubType == "0") {

			$sql    = "UPDATE payment_mode SET name = '".$name."',ledger_type = '".$ledger."',account_no = '".$accountNumber."',starting_balance = '".$startingBalance."',asset_account_type = '".$assetAccount."',payee_name = '".$payeeName."',soap_user_name= '".$soapUserName."',soap_password= '".$soapPassword."',updated_on = UNIX_TIMESTAMP() ,updated_by = '".$userId."',is_receive_payment='".$receivedPayment."',is_make_payment='".$makePayment."',is_alnum_mode='".$alnumMode."',is_account_active='".$accountActive."',is_available_for_banking='".$availableForbanking."',is_available_bank_source = '".$source_bank."' WHERE id = '".$id."' ";

			if ($defaultMode) {
				$unCheckedAllPaymode = "UPDATE payment_mode SET is_default_payment_mode = '0' WHERE ledger_type='".$ledger."'";
	      		$updatePayMode = "UPDATE payment_mode SET is_default_payment_mode = '1' WHERE id='".$id."'";
	        }

			$result = mysqli_multi_query($conn, $sql.';'.$unCheckedAllPaymode.';'.$updatePayMode);
			echo $result;
		}
		else {
			echo "0";
		}		
	}

	if ($operation == "delete") {
        $id = $_POST['id'];

		 $sql    = "UPDATE payment_mode SET status= 'I' where id = '" . $id . "'";
	    $result = mysqli_query($conn, $sql);
	    echo $result;	    
	}

	if ($operation == "restore") {// for restore    
        $id = $_POST['id'];
        $accountNumber = $_POST['accountNumber'];

        $selSubType = "SELECT account_no FROM payment_mode WHERE account_no ='".$accountNumber."' AND status = 'A' AND id !='".$id."'";
		$checkSubType = mysqli_query($conn,$selSubType);
		$countSubType = mysqli_num_rows($checkSubType);

		if ($countSubType == "0") {

			  $sql    = "UPDATE payment_mode SET status= 'A'  WHERE  id = '" . $id . "'";
			$result = mysqli_query($conn, $sql);
		    echo $result;
		    }
		else {
			echo "0";
		}
	}

	if ($operation == "checkDeafaultPaymentMode") {
		$ledger = $_POST['ledger'];
		$id = $_POST['id'];

		$checkQry = "SELECT name,id FROM payment_mode WHERE status = 'A' AND ledger_type = '".$ledger."' AND is_default_payment_mode = '1'";
		//for update case
		if ($id != '') {
			$checkQry.= " AND id !='".$id."'";
		}
		$result = mysqli_query($conn,$checkQry);
		$count = mysqli_num_rows($result);
		$oldPaymentMode = '';
		if ($count > 0) {
			while ( $r = mysqli_fetch_assoc($result) ) {
				$oldPaymentMode = $r['name'];
			}
			echo $oldPaymentMode;
		}
	}
?>