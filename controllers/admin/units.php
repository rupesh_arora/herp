<?php
/*
 * File Name    :   units.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar gupta
 * Created Date :   31th dec, 2015
 * Description  :   This page manage update,save,delete,restore details from database
 */
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
$operation        = "";
$unitName         = "";
$unitAbbreviation = "";
$description      = "";

include 'config.php'; // import database connection file

if (isset($_POST['operation'])) { // get operation value from js file
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

/* save details */
if ($operation == "save") {
    if (isset($_POST['unitName'])) {
        $unitName = $_POST['unitName'];
    }
    if (isset($_POST['unitAbbreviation'])) {
        $unitAbbreviation = $_POST['unitAbbreviation'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    
    
    $sql    = "INSERT INTO units(unit,unit_abbr,description) VALUES('" . $unitName . "','" . $unitAbbreviation . "','" . $description . "')";
    $result = mysqli_query($conn, $sql);
    echo $result;
}

/* update details */
if ($operation == "update") {
    if (isset($_POST['unitName'])) {
        $unitName = $_POST['unitName'];
    }
    if (isset($_POST['unitAbbreviation'])) {
        $unitAbbreviation = $_POST['unitAbbreviation'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    
    $sql    = "UPDATE units SET unit= '" . $unitName . "',unit_abbr= '" . $unitAbbreviation . "',description='" . $description . "' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    if ($result) {
        echo "$result";
    } else {
        echo "";
    }
}

/* for delete details to set status Inactive*/
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE units SET status= 'I' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}

/* show active data*/
if ($operation == "show") {
    
    $query        = "select * from units WHERE status= 'A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

//this will show all inactive data to datatable
if ($operation == "checked") {
    $sqlSelect    = "SELECT * FROM units WHERE status = 'I'";
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $totalrecords = mysqli_num_rows($resultSelect);
    
    $rows = array();
    while ($rUpdate = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $rUpdate;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

//to restore for set status acitve 
if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $sql    = "UPDATE units SET status= 'A'  WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}
?>