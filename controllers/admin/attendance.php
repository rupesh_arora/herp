<?php
	include 'config.php';
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
		$userId = $_SESSION['globaluser'];
	}
	else{
		exit();
	}
	if (isset($_POST['operation'])) { 
		$operation = $_POST["operation"];
	} 
	else if (isset($_GET["operation"])) {
	    $operation = $_GET["operation"];
	}
	if($operation == "punchIn"){
		
		$date = $_POST['date'];

		/*Check that whether that staff's leave status in approved or scedule for the day he punched in*/
		$selectCheck = "SELECT * from staff_leaves where staff_id = '".$userId."'  AND 
		 	DATE_FORMAT(FROM_UNIXTIME(leave_date), '%Y-%m-%d') = CURDATE()
			AND (leave_status = 'approval' OR leave_status =  'schedule')";

		$excuteQuery = mysqli_query($conn,$selectCheck);
		$rowCount = mysqli_num_rows($excuteQuery);

		/*if rows are grater than 0 then update staff leave column to cancel*/
		if ($rowCount >0) {
			$updateLeaveStatus = "UPDATE staff_leaves SET leave_status = 'cancel' WHERE 
			staff_id = '".$userId."' AND  DATE_FORMAT(FROM_UNIXTIME(leave_date), '%Y-%m-%d') = CURDATE()";
			mysqli_query($conn,$updateLeaveStatus);
		}
		
		$sql = "INSERT INTO staff_attendance (staff_id,`date`,punch_in,punch_out,remarks,created_on,updated_on,created_by,updated_by) VALUES ('".$userId."','".$date."',UNIX_TIMESTAMP(),'','',UNIX_TIMESTAMP(),
			UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
		$result = mysqli_query($conn, $sql);
    	echo $result;
	}
	if($operation == "todayAttendanceDetails"){

		$sql = "SELECT DATE_FORMAT(FROM_UNIXTIME(punch_in), '%Y-%m-%d %h:%i:%s %p') 
	 	AS punch_in,punch_out FROM staff_attendance WHERE staff_id=\"$userId\" AND date=curdate()
	 	ORDER BY id desc LIMIT 1";

	 	$result = mysqli_query($conn, $sql);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	if($operation == "punchOut"){
		$remarks = $_POST['remarks'];
		$sql = "UPDATE staff_attendance SET punch_out=UNIX_TIMESTAMP(),updated_on=UNIX_TIMESTAMP(),remarks='".$remarks."',updated_by='".$userId."' WHERE staff_id='".$userId."' ORDER BY id DESC LIMIT 1";
		$result=mysqli_query($conn,$sql);
		echo $result;
	}
?>