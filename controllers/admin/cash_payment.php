<?php
/*****************************************************************************************************
 * File Name    :   cash_payment.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages forensic test data
 ****************************************************************************************************/

$operation       = "";
$visitId         = "";
$medicineBillId  = "";
$radiologyBillId = "";
$radiologyId     = "";
$procedureBillId = "";
$procedureId     = "";
$totalBill       = "";
$labBillId       = "";
$labId           = "";
$forensicBillId  = "";
$forensicId      = "";
$serviceBillId   = "";
$serviceId       = "";
$receiptNo       = "";
$payDate         = "";
$paymentMode     = "";
$amountTendered  = "";
$patientID       = "";
$userId          = "";
$date            = date_create($payDate);
$newDate         = date_format($date, "Y/m/d");
$createOn        = new DateTime();

$date = new DateTime();
session_start();

if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

include 'config.php';
$operation = $_POST["operation"];

// code to show data to text on click of select button
if ($operation == "showData") {
    
    $visitId     = $_POST['visitId'];
    $visitPrefix = $_POST['visitPrefix'];
    $sql         = "SELECT value FROM configuration WHERE name = 'visit_prefix'";
    $result      = mysqli_query($conn, $sql);
    
    while ($r = mysqli_fetch_assoc($result)) {
        $prefix = $r['value'];
    }
    if ($prefix == $visitPrefix) {
        
        $query      = "SELECT id FROM visits where id='" . $visitId . "'";
        $result     = mysqli_query($conn, $query);
        $rows_count = mysqli_num_rows($result);
        if ($rows_count > 0) {
            
            $sqlSelect    = "SELECT v.payment_mode,v.patient_id,CONCAT(p.first_name,' ',p.middle_name,' ' ,p.last_name) AS name, v.created_on, p.fan_id,
					 (SELECT value FROM configuration WHERE name='patient_prefix') AS patient_prefix FROM visits AS v 
					LEFT JOIN patients AS p ON p.id = v.patient_id 
					where v.id='" . $visitId . "'";
            $resultSelect = mysqli_query($conn, $sqlSelect);
            $rows         = array();
            while ($r = mysqli_fetch_assoc($resultSelect)) {
                $rows[] = $r;
            }
            $patient_id = $rows[0]['patient_id'];
            $fan_id = $rows[0]['fan_id'];
			if($fan_id != NULL){
				$id = $fan_id;
			}
			else{
				$id = $patient_id;
			}
            
            $sqlamount    = " select sum(c_a.credit )-sum(c_a.debit ) as amount from cash_account AS c_a   where c_a.patient_id ='" . $id . "'";
            $resultamount = mysqli_query($conn, $sqlamount);
            while ($r = mysqli_fetch_assoc($resultamount)) {
                $rows[] = $r;
            }
            print json_encode($rows);
        }
    } else {
        echo "2";
    }
}

// code to show data to text on click of select button
if ($operation == "showTableData") {
    $visitId     = $_POST['visitId'];
    $visitPrefix = $_POST['visitPrefix'];
    $sql         = "SELECT value FROM configuration WHERE name = 'visit_prefix'";
    $result      = mysqli_query($conn, $sql);
    $rowsLab = array();
    
    while ($r = mysqli_fetch_assoc($result)) {
        $prefix = $r['value'];
    }
    if ($prefix == $visitPrefix) {
        $rows       = array();
        $query      = "SELECT id FROM visits where id='" . $visitId . "'";
        $result     = mysqli_query($conn, $query);
        $rows_count = mysqli_num_rows($result);
        if ($rows_count > 0) {
            
           /* $sqlMedicine    = "Select 'Medicine' as tblname, 'Medicine' as name,p.id AS bill_id ,'0' AS id, 'Medicine' as description,p.amount AS cost, p.amount AS amount
				,'1' AS quantity,p.payment_mode from patient_pharmacy_bill As p
				where p.visit_id='" . $visitId . "' AND p.`status` ='unpaid'";
            $resultMedicine = mysqli_query($conn, $sqlMedicine);
            while ($r = mysqli_fetch_assoc($resultMedicine)) {
                $rows[] = $r;
            }*/
            
            $sqlForensic    = "Select 'Forensic' as tblname,f_b.charge AS amount,f_b.id AS bill_id ,f_b.forensic_test_id AS id , f_t.name AS name,f_t.description AS description,
				f_t.fee AS cost ,'1' AS quantity,f_b.payment_mode from patient_forensic_bill As f_b
				LEFT JOIN forensic_tests AS f_t ON f_t.id = f_b.forensic_test_id
					where f_b.visit_id='" . $visitId . "' AND f_b.`status` ='unpaid'";
            $resultForensic = mysqli_query($conn, $sqlForensic);
            while ($r = mysqli_fetch_assoc($resultForensic)) {
                $rows[] = $r;
            }
            
            $sqlLab    = "SELECT 'Lab' AS tblname,ltrc.id AS id,ltrc.cost AS amount,plb.id AS bill_id,ltc.name AS name,'' AS description, ltrc.cost AS cost,'1' AS quantity,
                ltrc.payment_mode,ltc.id AS lab_test_id  
                FROM patient_lab_bill AS plb
                LEFT JOIN lab_test_request_component AS ltrc ON ltrc.patient_lab_bill_id = plb.id 
                LEFT JOIN lab_test_component AS ltc ON ltc.id = ltrc.lab_component_id 
                LEFT JOIN lab_test_requests ON lab_test_requests.id = ltrc.lab_test_request_id 
                WHERE plb.visit_id ='" . $visitId . "' AND ltrc.pay_status = 'unpaid'
                AND lab_test_requests.request_type = 'internal'";  

            $resultLab = mysqli_query($conn, $sqlLab);          
            while ($r = mysqli_fetch_assoc($resultLab)) {
                $rows[] = $r;
            }
            
            $sqlProcedure    = "SELECT 'Procedure' as tblname,p_b.charge AS amount,p_b.id AS bill_id ,p_b.procedure_id AS id, p.name AS name,p.description AS description,
					p.price AS cost ,'1' AS quantity,p_b.payment_mode from patient_procedure_bill As p_b
					LEFT JOIN procedures AS p ON p.id = p_b.procedure_id
					where p_b.visit_id='" . $visitId . "' AND p_b.`status` ='unpaid'";
            $resultProcedure = mysqli_query($conn, $sqlProcedure);
            while ($r = mysqli_fetch_assoc($resultProcedure)) {
                $rows[] = $r;
            }
            
            $sqlRadiology    = "Select 'Radiology' as tblname,r_b.charge AS amount,r_b.id AS bill_id ,r_b.radiology_test_id AS id , r_t.name AS name,r_t.description AS description,
					r_t.fee AS cost ,'1' AS quantity,r_b.payment_mode from patient_radiology_bill As r_b
					LEFT JOIN radiology_tests AS r_t ON r_t.id = r_b.radiology_test_id
					where r_b.visit_id='" . $visitId . "' AND r_b.`status` ='unpaid'";
            $resultRadiology = mysqli_query($conn, $sqlRadiology);
            while ($r = mysqli_fetch_assoc($resultRadiology)) {
                $rows[] = $r;
            }
            
            $sqlService    = "Select 'Service' as tblname,s_b.charge AS amount,s_b.id AS bill_id ,s_b.service_id AS id ,
				s.name AS name,s.description AS description,
					s.cost AS cost ,'1' AS quantity,s_b.payment_mode from patient_service_bill As s_b
					LEFT JOIN services AS s ON s.id = s_b.service_id
					where s_b.visit_id='" . $visitId . "' AND s_b.`status` ='unpaid'";
            $resultService = mysqli_query($conn, $sqlService);
            while ($r = mysqli_fetch_assoc($resultService)) {
                $rows[] = $r;
            }

             $sqlMedicine    = "SELECT  'Medicine' as tblname, p_r.cost AS amount,p_r.id AS bill_id,p_r.drug_id AS 
                                id,d.name AS name ,'Medicine' AS description,p_r.cost/p_r.quantity AS cost,p_r.quantity AS quantity, 
                                p_r.payment_mode from patients_prescription_request AS p_r
                
                            LEFT JOIN drugs AS d ON d.id = p_r.drug_id 
                            LEFT JOIN pharmacy_inventory AS p  ON p.drug_name_id = d.id
                            WHERE p_r.visit_id= '" . $visitId . "' AND p_r.dispense_status = 'Pending'  AND p.quantity !=  0 AND p_r.insurance_payment = 'unpaid' AND p.expiry_date > CURDATE()";
            $resultMedicine = mysqli_query($conn, $sqlMedicine);
            while ($r = mysqli_fetch_assoc($resultMedicine)) {
                $rows[] = $r;
            }
        }
        print json_encode($rows);
    }
    
}

//This operation is used for select last receipt number 
if ($operation == "receipt") {
    
    $query      = "SELECT id,(SELECT value FROM configuration WHERE name = 'receipt_prefix') AS prefix FROM invoice_details ORDER BY id DESC LIMIT 1";
    $result     = mysqli_query($conn, $query);
    $rows_count = mysqli_num_rows($result);
    if ($rows_count > 0) {
        $rows       = array();
        while ($r = mysqli_fetch_assoc($result)) {
            $rows[] = $r;
        }
        print json_encode($rows);
    } else {
        echo "0";
    }
}

//This operation is used for save the data and also change the status in all bill tables 
if ($operation == "save") {
    if (isset($_POST['receiptNo'])) {
        $receiptNo = $_POST["receiptNo"];
    }
    if (isset($_POST['paymentMode'])) {
        $paymentMode = $_POST["paymentMode"];
    }
    if (isset($_POST['amountTendered'])) {
        $amountTendered = $_POST["amountTendered"];
    }
    if (isset($_POST['visitId'])) {
        $visitId = $_POST["visitId"];
    }
    if (isset($_POST['patientID'])) {
        $patientID = $_POST["patientID"];
    }
    if (isset($_POST['familyMemberId'])) {
        $familyMemberId = $_POST["familyMemberId"];
    }
    if (isset($_POST['printDiv'])) {
        $printDiv = $_POST["printDiv"];
    }
    if (isset($_POST['newDate'])) {
        $newDate = $_POST["newDate"];
    }
    if (isset($_POST['balance'])) {
        $balance = $_POST["balance"];
    }
    $creditDeposit = "0";
	
	
    $sqlInsertBill = "insert into patient_bill_payment (balance,visit_id,receipt_no,payment_mode,credit,created_on,updated_on,created_by,
   updated_by)
          values('" . $balance . "','" . $visitId . "','" . $receiptNo . "','" . $paymentMode . "','" . $amountTendered . "',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
    
    $resultInsertBill = mysqli_query($conn, $sqlInsertBill);
    
    if ($resultInsertBill == "1" && $paymentMode == "2") {
        
        
        $sqlInsertInvoice = "INSERT INTO invoice_details (invoice_details) VALUES ('".$printDiv."')";
        $resultInvoice = mysqli_query($conn,$sqlInsertInvoice);
        if($resultInvoice) {
            $lastInsertId = mysqli_insert_id($conn);
            // cash deposite transaction
            $insertTransaction = "INSERT INTO transactions (patient_id,transaction_type_id,credit,debit,processed_on,processed_by,invoice_id)
                    values ('" . $patientID . "','1','0','" .$amountTendered. "',UNIX_TIMESTAMP(),'".$userId ."','".$lastInsertId."')"; 
            mysqli_query($conn,$insertTransaction); 
        }
        
                
        $sql    = "insert into cash_account (patient_id,depositor_id,date,credit,debit,created_on,created_by)
            values('" . $familyMemberId . "','" . $patientID . "','" . $newDate . "','" . $creditDeposit . "','" . $amountTendered . "','" . $createOn->getTimestamp() . "','" . $userId . "')";
        $result = mysqli_query($conn, $sql);
        if ($result == "1") {
            echo "1";
        } 
        else {
            echo "0";
        }
    } 
    else {
        if (isset($_POST['totalBill'])) {
            $totalBill = $_POST["totalBill"];
        }
        if($_POST['table']  != $_POST['totalBill']){
            echo "Don't show your smartness";
            exit();
        }
        $sqlInsertInvoice = "INSERT INTO invoice_details (invoice_details) VALUES ('".$printDiv."')";
        $resultInvoice = mysqli_query($conn,$sqlInsertInvoice);
        if($resultInvoice) {
            $lastInsertId = mysqli_insert_id($conn);
            
            // cash deposite transaction
            $insertTransaction = "INSERT INTO transactions (patient_id,transaction_type_id,credit,debit,processed_on,processed_by,invoice_id)
            values ('" . $patientID . "','1','" .$amountTendered. "','" .$amountTendered. "',UNIX_TIMESTAMP(),'".$userId ."','".$lastInsertId."')"; 
            mysqli_query($conn,$insertTransaction);
        }
        
        $sqlCredit    = "insert into cash_account (patient_id,depositor_id,date,credit,debit,created_on,created_by)
            values('" . $patientID . "','" . $patientID . "','" . $newDate . "','" . $amountTendered . "','0','" . $createOn->getTimestamp() . "','" . $userId . "')";
        $resultCredit = mysqli_query($conn, $sqlCredit);
        
        $sqlDebit    = "insert into cash_account (patient_id,depositor_id,date,credit,debit,created_on,created_by)
            values('" . $patientID . "','" . $patientID . "','" . $newDate . "','0','" . $totalBill . "','" . $createOn->getTimestamp() . "','" . $userId . "')";
        $resultDebit = mysqli_query($conn, $sqlDebit);
        if ($resultCredit == "1" && $resultDebit == "1") {
            echo "1";
        } 
        else {
            echo "0";
        }
    }
    
    if (isset($_POST['radiologyBill'])) {
        $radiologyBill = json_decode($_POST["radiologyBill"]);
        $finalData = "";
		$totalAmount = 0;
        foreach ($radiologyBill as $value) {
            
            $radiologyBillId    = $value->radiologyBillId;
            $radiologyId        = $value->radiologyId;
            $amount        		= $value->amount;
			$totalAmount = $totalAmount + $amount;
			
            $sqlRadiologyBillId = "Update patient_radiology_bill SET status = 'Paid' ,updated_on ='" . $date->getTimestamp() . "',
					  updated_by = '" . $userId . "' WHERE id ='" . $radiologyBillId . "'";
            $result             = mysqli_query($conn, $sqlRadiologyBillId);
            
            $sqlRadiologyId = "Update radiology_test_request SET pay_status = 'Paid' 
				 WHERE radiology_test_id ='" . $radiologyId . "' AND visit_id = '" . $visitId . "' and patient_id='" . $patientID . "'";
            $result         = mysqli_query($conn, $sqlRadiologyId);

            $query  = "SELECT ledger_id,gl_account_id,account_payable_id FROM radiology_tests WHERE id = " . $radiologyId . "";
            $result = mysqli_query($conn, $query);
            $ledgerId;
            $glAccountId;
            $accountPayable;
            while ($r = mysqli_fetch_assoc($result)) {
                $ledgerId = $r['ledger_id'];
                $glAccountId = $r['gl_account_id'];
                $accountPayable = $r['account_payable_id'];
            }

            $sqlInsertBill = "INSERT INTO finance_transaction (gl_account_id,ledger_id,ap_id,amt_db,
             type,type_id,created_on,updated_on,created_by,updated_by)
                      values('" . $glAccountId . "','" . $ledgerId . "','" . $accountPayable . "','" . $amount . "','radiology'," . $radiologyId . ",UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
                
                $resultInsertBill = mysqli_query($conn, $sqlInsertBill);
        }
    }
	
    if (isset($_POST['procedureBill'])) {
        $procedureBill = json_decode($_POST["procedureBill"]);
        $finalData = "";
		$totalAmount = 0;
        foreach ($procedureBill as $value) {
            
            $procedureBillId    = $value->procedureBillId;
            $procedureId        = $value->procedureId;
            $amount      	    = $value->amount;
			$totalAmount = $totalAmount + $amount;
			
            $sqlProcedureBillId = "Update patient_procedure_bill SET status = 'Paid' ,updated_on ='" . $date->getTimestamp() . "',
					  updated_by = '" . $userId . "' WHERE id ='" . $procedureBillId . "'";
            $result             = mysqli_query($conn, $sqlProcedureBillId);
            
            $sqlProcedureId = "Update procedures_requests SET pay_status = 'Paid' 
				 WHERE procedure_id ='" . $procedureId . "' AND visit_id = '" . $visitId . "' 
				 and patient_id='" . $patientID . "'";
            $result         = mysqli_query($conn, $sqlProcedureId);

            $query  = "SELECT ledger_id,gl_account_id,account_payable_id FROM procedures WHERE id = " . $procedureId . "";
            $result = mysqli_query($conn, $query);
            $ledgerId;
            $glAccountId;
            $accountPayable;
            while ($r = mysqli_fetch_assoc($result)) {
                $ledgerId = $r['ledger_id'];
                $glAccountId = $r['gl_account_id'];
                $accountPayable = $r['account_payable_id'];
            }

            $sqlInsertBill = "INSERT INTO finance_transaction (gl_account_id,ledger_id,ap_id,amt_db,
             type,type_id,created_on,updated_on,created_by,updated_by)
                      values('" . $glAccountId . "','" . $ledgerId . "','" . $accountPayable . "','" . $amount . "','procedure'," . $procedureId . ",UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
                
                $resultInsertBill = mysqli_query($conn, $sqlInsertBill);
        }	
		
    }
	
    if (isset($_POST['labBill'])) {
        $labBill = json_decode($_POST["labBill"]);
        $finalData = "";
		$totalAmount = 0;
        $temp = 0;
        foreach ($labBill as $value) {
            
            $labBillId    = $value->labBillId;
            $labId        = $value->labId;
            $labTestId        = $value->labTestId;
            $amount        = $value->amount;
			$totalAmount = $totalAmount + $amount;
			
            $sqlLabBillId = "Update patient_lab_bill SET status = 'Paid' ,updated_on ='" . $date->getTimestamp() . "',
					  updated_by = '" . $userId . "' WHERE id ='" . $labBillId . "'";
            $result       = mysqli_query($conn, $sqlLabBillId);
            
            $sqlLabId = "Update lab_test_request_component SET pay_status = 'paid' 
				 WHERE id ='" . $labId . "'";
            $result   = mysqli_query($conn, $sqlLabId);

            //if($labTestId != $temp){
                //Fetch Ledger ,GL And Account Payable Id.
                $query  = "SELECT ledger_id,gl_account_id,account_payable_id FROM lab_tests WHERE id = " . $labTestId . "";
                $result = mysqli_query($conn, $query);
                $ledgerId;
                $glAccountId;
                $accountPayable;
                while ($r = mysqli_fetch_assoc($result)) {
                    $ledgerId = $r['ledger_id'];
                    $glAccountId = $r['gl_account_id'];
                    $accountPayable = $r['account_payable_id'];
                }

                 $sqlInsertBill = "INSERT INTO finance_transaction (gl_account_id,ledger_id,ap_id,amt_db,
                 type,type_id,created_on,updated_on,created_by,updated_by)
                          values('" . $glAccountId . "','" . $ledgerId . "','" . $accountPayable . "','" . $amount . "','lab'," . $labTestId . ",UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
                    
                    $resultInsertBill = mysqli_query($conn, $sqlInsertBill);
                   // $temp = $labTestId;
           // }

            
        }
		
		
    }
	
    if (isset($_POST['forensicBill'])) {
        $forensicBill = json_decode($_POST["forensicBill"]);
        $finalData = "";
		$totalAmount = 0;
        foreach ($forensicBill as $value) {
            
            $forensicBillId    = $value->forensicBillId;
            $forensicId        = $value->forensicId;
            $amount        	   = $value->amount;
			$totalAmount = $totalAmount + $amount;
			
            $sqlForensicBillId = "Update patient_forensic_bill SET status = 'Paid' ,updated_on ='" . $date->getTimestamp() . "',
					  updated_by = '" . $userId . "' WHERE id ='" . $forensicBillId . "'";
            $result            = mysqli_query($conn, $sqlForensicBillId);
            
            $sqlForensicId = "Update forensic_test_request SET pay_status = 'Paid' 
				 WHERE forensic_test_id ='" . $forensicId . "' AND visit_id = '" . $visitId . "' 
				 and patient_id='" . $patientID . "'";
            $result        = mysqli_query($conn, $sqlForensicId);
           

            //Fetch Ledger ,GL And Account Payable Id.
            $query  = "SELECT ledger_id,gl_account_id,account_payable_id FROM forensic_tests WHERE id = " . $forensicId . "";
            $result = mysqli_query($conn, $query);
            $ledgerId;
            $glAccountId;
            $accountPayable;
            while ($r = mysqli_fetch_assoc($result)) {
                $ledgerId = $r['ledger_id'];
                $glAccountId = $r['gl_account_id'];
                $accountPayable = $r['account_payable_id'];
            }

             $sqlInsertBill = "INSERT INTO finance_transaction (gl_account_id,ledger_id,ap_id,amt_db,
             type,type_id,created_on,updated_on,created_by,updated_by)
                      values('" . $glAccountId . "','" . $ledgerId . "','" . $accountPayable . "','" . $amount . "','forensic'," . $forensicId . ",UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
                
                $resultInsertBill = mysqli_query($conn, $sqlInsertBill);
        }	
    }
	
    if (isset($_POST['serviceBill'])) {
        $serviceBill = json_decode($_POST["serviceBill"]);
        $finalData = "";
		$totalAmount = 0;
        foreach ($serviceBill as $value) {
            
            $serviceBillId = $value->serviceBillId;
            $serviceId     = $value->serviceId;
            $amount        = $value->amount;
            $totalAmount = $totalAmount + $amount;
			
            $sqlservicebillid = "update patient_service_bill set status = 'paid' ,updated_on ='" . $date->gettimestamp() . "',
					  updated_by = '" . $userId . "' where id ='" . $serviceBillId . "'";
            $result           = mysqli_query($conn, $sqlservicebillid);
            
            $sqlServiceId = "Update services_request SET pay_status = 'Paid' 
				WHERE service_id ='" . $serviceId . "' AND visit_id = '" . $visitId . "'
				and patient_id='" . $patientID . "'";
            $result       = mysqli_query($conn, $sqlServiceId);

             //Fetch Ledger ,GL And Account Payable Id.
            $query  = "SELECT ledger_id,gl_account_id,account_payable_id FROM services WHERE id = " . $serviceId . "";
            $result = mysqli_query($conn, $query);
            $ledgerId;
            $glAccountId;
            $accountPayable;
            while ($r = mysqli_fetch_assoc($result)) {
                $ledgerId = $r['ledger_id'];
                $glAccountId = $r['gl_account_id'];
                $accountPayable = $r['account_payable_id'];
            }

             $sqlInsertBill = "INSERT INTO finance_transaction (gl_account_id,ledger_id,ap_id,amt_db,
             type,type_id,created_on,updated_on,created_by,updated_by)
                      values('" . $glAccountId . "','" . $ledgerId . "','" . $accountPayable . "','" . $amount . "','service'," . $serviceId . ",UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
                
                $resultInsertBill = mysqli_query($conn, $sqlInsertBill);

        }		
    }
	
    if (isset($_POST['medicineBillId'])) {
        $medicineBillId = json_decode($_POST["medicineBillId"]);
		$finalData = "";
		$totalAmount = 0;
        foreach ($medicineBillId as $value) {
            $BillId            = "";
            $BillId            = $value -> billId;
            $amount            = $value -> amount;
			$totalAmount = $totalAmount + $amount;
			
            $sqlMedicineBillId = "Update patients_prescription_request SET insurance_payment = 'Paid' WHERE id ='" . $BillId . "'";
            $result            = mysqli_query($conn, $sqlMedicineBillId);
        }
        if($result){
            $sqlPharmacyBill = "INSERT INTO patient_pharmacy_bill(visit_id, patient_id, amount,net_amount,discount,status,created_on,updated_on,created_by,updated_by)
                VALUES('".$visitId."', '".$patientID."','".$totalAmount."','".$totalAmount."','0','paid',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";     
            $resultPharmacyBill= mysqli_query($conn,$sqlPharmacyBill);
        }
	
    }
	
    $query      = "SELECT id+1 AS id FROM patient_bill_payment ORDER BY id DESC LIMIT 1";
    $result     = mysqli_query($conn, $query);
    $rows_count = mysqli_num_rows($result);
    if ($rows_count > 0) {
        while ($row = mysqli_fetch_row($result)) {
            $receiptNo = $row[0];
        }
    }
	

}

/*operation to get copay value*/
if ($operation == "insuranceDetails") {
	
    if (isset($_POST['patientID'])) {
        $patientID = $_POST["patientID"];
    }

    if (isset($_POST['visitId'])) {
        $visitId = $_POST["visitId"];
    }
    
	$query  = "SELECT i.insurance_number,i.insurance_plan_name_id,i.insurance_company_id,i.insurance_plan_id,c.copay_type ,s.value FROM insurance AS i
        LEFT JOIN scheme_plans AS s ON s.id = i.insurance_plan_name_id 
        LEFT JOIN copay AS c ON c.id = s.copay  
        WHERE i.patient_id = '" . $patientID . "' AND i.status='A' AND i.expiration_date > CURDATE() AND i.insurance_number = (SELECT insurance_no FROM visits WHERE id = '" . $visitId . "')";
	$result = mysqli_query($conn, $query);
	$rows   = array();
	while ($r = mysqli_fetch_assoc($result)) {
		$rows[] = $r;
	}
	print json_encode($rows);
}
?>