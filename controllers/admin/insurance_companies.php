<?php
/*
 * File Name    :   insuance_companies.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre details form database
 */
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

$operation           = "";
$description         = "";
$createdate          = new DateTime();
include 'config.php'; //import databse connection file

if (isset($_POST['operation'])) { // perform operation form js file
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
}

if ($operation == "save") // save details
{
	$description = "";
	$email="";
    if (isset($_POST['companyName'])) {
        $companyName = $_POST['companyName'];
    }
	if (isset($_POST['address'])) {
        $address = $_POST['address'];
    }
	if (isset($_POST['phone'])) {
        $phone = $_POST['phone'];
    }
	if (isset($_POST['email'])) {
        $email = $_POST['email'];
    }
	if (isset($_POST['country'])) {
        $country = $_POST['country'];
    }
	if (isset($_POST['state'])) {
        $state = $_POST['state'];
    }
	if (isset($_POST['city'])) {
        $city = $_POST['city'];
    }
    if (isset($_POST['mobileCode'])) {
        $mobileCode = $_POST['mobileCode'];
    }
	if (isset($_POST['zipcode'])) {
        $zipcode = $_POST['zipcode'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }    
	$fax = $_POST['fax'];
	$website = $_POST['website'];
	$contactPerson = $_POST['contactPerson'];
	$telephone = $_POST['telephone'];
	$preAuthorization = $_POST['preAuthorization'];
	$postalAddress = $_POST['postalAddress'];
	$preauthorizationCkeck = $_POST['preauthorizationCkeck'];
		 
    $sql    = "INSERT INTO insurance_companies(name,country_code,address,country_id,state_id,city_id,zip,email_id,phone,description,created_on,updated_on,
				fax,website,contact_person,telephone,preauthorization_ip,preauthorization_period,postal_address,created_by,updated_by)
				VALUES('" . $companyName . "','" . $mobileCode . "','" . $address . "','" . $country . "','" . $state . "','" . $city . "',
				'" . $zipcode . "','" . $email . "','" . $phone . "','" . $description . "',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),
				'".$fax."','".$website."','".$contactPerson."','".$telephone."','".$preauthorizationCkeck."','".$preAuthorization."',
				'".$postalAddress."','".$userId."','".$userId."')";
				
    $result = mysqli_query($conn, $sql);
	if($result) {
		echo "1";
	}
	else {
		echo "0";
	}    
}
if ($operation == "show") { // show active data
    
    $query        = "select * from insurance_companies where status='A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
    
}
// opertaion form update
if ($operation == "update") // update data
{
	$description = "";
	$email="";
    if (isset($_POST['companyName'])) {
        $companyName = $_POST['companyName'];
    }
	if (isset($_POST['address'])) {
        $address = $_POST['address'];
    }
	if (isset($_POST['phone'])) {
        $phone = $_POST['phone'];
    }
	if (isset($_POST['email'])) {
        $email = $_POST['email'];
    }
	if (isset($_POST['country'])) {
        $country = $_POST['country'];
    }
	if (isset($_POST['state'])) {
        $state = $_POST['state'];
    }
	if (isset($_POST['city'])) {
        $city = $_POST['city'];
    }
	if (isset($_POST['zipcode'])) {
        $zipcode = $_POST['zipcode'];
    }
    if (isset($_POST['mobileCode'])) {
        $mobileCode = $_POST['mobileCode'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }  
	$fax = $_POST['fax'];
	$website = $_POST['website'];
	$contactPerson = $_POST['contactPerson'];
	$telephone = $_POST['telephone'];
	$preAuthorization = $_POST['preAuthorization'];
	$postalAddress = $_POST['postalAddress'];
	$preauthorizationCkeck = $_POST['preauthorizationCkeck'];
	
    $id = $_POST['id'];
    
    $sql    = "update insurance_companies set name= '" . $companyName . "',country_code= '" . $mobileCode . "',address= '" . $address . "' ,
				phone= '" . $phone . "',email_id= '" . $email . "' ,country_id= '" . $country . "' ,
				state_id= '" . $state . "',city_id= '" . $city . "' ,zip= '" . $zipcode . "' ,
			    description = '" . $description . "', updated_on=UNIX_TIMESTAMP(),
				fax= '" . $fax . "',website= '" . $website . "',contact_person= '" . $contactPerson . "',
				telephone= '" . $telephone . "',preauthorization_ip= '" . $preauthorizationCkeck . "',preauthorization_period= '" . $preAuthorization . "',
				postal_address = '".$postalAddress."',updated_by='".$userId."' where id = '" . $id . "'";
   $result = mysqli_query($conn, $sql);
	if($result) {
		echo "1";
	}
	else {
		echo "0";
	} 
}
//  operation for delete
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $selectQry = "SELECT insurance_company_id FROM scheme_plans WHERE insurance_company_id ='".$id."'";
    $selectResult = mysqli_query($conn,$selectQry);
    $countRows = mysqli_num_rows($selectResult);
    if ($countRows == 0) {

        $selectQryPlan = "SELECT company_name_id FROM insurance_plan WHERE company_name_id ='".$id."'";
        $selectResultPlan = mysqli_query($conn,$selectQryPlan);
        $countRows = mysqli_num_rows($selectResultPlan);

        if ($countRows == 0) {

            $selectQryIns = "SELECT insurance_company_id FROM insurance WHERE insurance_company_id ='".$id."'";
            $selectResultIns = mysqli_query($conn,$selectQryIns);
            $countRows = mysqli_num_rows($selectResultIns);

            if ($countRows == 0) {
                $sql    = "UPDATE insurance_companies SET status= 'I' where id = '" . $id . "'";
                $result = mysqli_query($conn, $sql);
                echo $result;
            }
            else{
                echo "0";
            }            
        }
        else{
            echo "0";
        }
    }
    else{
        echo "0";
    }               
}
//When checked box is check
if ($operation == "checked") {
    
    $query = "select * from insurance_companies where status='I'";
    
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}
if ($operation == "restore") // for restore    
    {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $sql    = "UPDATE insurance_companies SET status= 'A'  WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}
?>