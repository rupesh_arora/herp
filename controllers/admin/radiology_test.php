<?php
/*****************************************************************************************************
 * File Name    :   radiology_test.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages radiology test data
 ****************************************************************************************************/
    $operation = "";
	/* $specimenType = "";
	$unitMeasure = ""; */
	$testName = "";
	/* $normalRange = ""; */
	$testFee = "";
	$description = "";
	$date = new DateTime();

	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	
	if (isset($_FILES["file"]["type"])) // use for import data from csv
	 {
		$temporary       = explode(".", $_FILES["file"]["name"]);
		$file_extension = end($temporary);
		/*if ($file_extension == "csv") {
			if ($_FILES["file"]["error"] > 0) {
				echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
			} 
			else {
				if($_FILES["file"]["size"] > 0)
				{
					$filename = $_FILES["file"]["tmp_name"];
					$file = fopen($filename, "r");

					$countCSV = 0;
					$subInsertValues = '';
					$sqlInsert = "INSERT IGNORE INTO radiology_tests(name,fee,description,created_on,updated_on) VALUES ";

					while (($emapData = fgetcsv($file, 10000, ",")) != FALSE) {
						
						if (sizeof($emapData) != 3) {
						 	echo "You must have three column in your file i.e radiology test name, radiology test fee and description";
						 	exit();
						}

						//check whether header name same or not
						if ($countCSV == 0) {
							if(strtolower($emapData[0]) !="radiology test name") {
								echo "First column should be radiology test name";
								exit();
							}
							if(strtolower($emapData[1]) !="radiology test fee") {
								echo "Second column should be radiology test fee";
								exit();
							}
							if(strtolower($emapData[2]) !="description") {
								echo "Second column should be description";
								exit();
							}						
						}

						//data can't be null in first row else row it may be null
						if ($countCSV == 1) {
							if (empty($emapData[0]) || empty($emapData[1]) ) {
								echo "Data can't be null in radiology test name and radiology test fee column";
								exit();
							}
						}

						if ($countCSV > 0) {

							//check whether data not be null in the file
							if(($emapData[0]) != "" && ($emapData[1]) != "") {

								if (is_float($emapData[1]) || is_numeric($emapData[1])){
									if ($countCSV >1) {
										$subInsertValues.= ',';
									}						
									$subInsertValues.= "('".$emapData[0]."','".$emapData[1]."',
										'".$emapData[2]."',	UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";
								}
								else{
									echo "Radiology test fee should be number";
									exit();
								}								
							}
						}			
						$countCSV++;						
					}

					$sqlInsert = $sqlInsert.$subInsertValues;
					$result = mysqli_query($conn,$sqlInsert);
					if($result)
					{
						echo $result;
					}
					else{
						echo "";
					}
					fclose($file);
				}
			} 	
		}
		else if($file_extension == "xls"){
			ini_set("display_errors",1);
			$filename = $_FILES['file']['tmp_name'];
			require_once 'excel_reader2.php';
			$data = new Spreadsheet_Excel_Reader($filename);
			$countSheets = count($data->sheets);
			for($i=0;$i<$countSheets;$i++) // Loop to get all sheets in a file.
			{ 
				$countSheetsCells =0;
				if(isset($data->sheets[$i]['cells'])){
					$countSheetsCells = count($data->sheets[$i]['cells']);
				}
				if($countSheetsCells > 0 && $countSheetsCells) // checking sheet not empty
				{
					$sql = "INSERT IGNORE INTO radiology_tests(name,fee,description,created_on,updated_on) VALUES ";
					$countXls = 0;				
					$subInsert = '';
					for($j=1;$j<=$countSheetsCells;$j++) {// loop used to get each row of the sheet
					 
						
					  	if (sizeof($data->sheets[$i]['cells'][$j]) !="3") {
					  		echo "You must have three column in your file i.e radiology test name, radiology test fee and description";
						 	exit();
					  	}
					  	
					  		
					  	$indexOfExcelFile = array_keys($data->sheets[$i]['cells'][$j]);

						
						if ($countXls == 0) {
							if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[0]]) !="radiology test name"){
								echo "First column should be radiology test name";
								exit();
							}
							if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[1]]) !="radiology test fee"){
								echo "Second column should be radiology test fee";
								exit();
							}
							if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[2]]) !="description"){
								echo "Third column should be description";
								exit();
							}
						}
						
						$name = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[0]];
					
						$fee = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[1]];
					
						$description = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[4]];
						
						//data can't be null in the very first row of file
						if ($countXls == 1) {
							if (empty($fee) || empty($name)) {
								echo "Data can't be null in radiology test name and radiology test fee column";
								exit();
							}
						}

						if ($countXls>0) {
							
							//check whether data not be null in file
							if($fee != "" && $name != ''){

								if (is_float($fee) || is_numeric($fee)){
									if ($countXls >1) {
										$subInsert .=',';
									}					 	
								 	$subInsert .=" ('$name','$fee','$description',UNIX_TIMESTAMP(),
								 		UNIX_TIMESTAMP())";
								}
								else{
									echo "Radiology test fee should be number";
									exit();
								}
							}
						}
						$countXls++;		
					}

					$sql = $sql.$subInsert;
					$result = mysqli_query($conn,$sql);

					if($result) {
						echo $result;
					}
					else{
						echo "";
					}
				}
			}		
		}*/
		if($file_extension == "xlsx" || $file_extension == "csv" ||$file_extension == "xls"){
			// get file name
			$filename = $_FILES['file']['tmp_name'];
			
			//import php script
			require_once 'PHPExcel.php';
			
			// 
			try {
				$inputFileType = PHPExcel_IOFactory::identify($filename);  // get type of file name
				$objReader = PHPExcel_IOFactory::createReader($inputFileType); //create object of read file
				$objPHPExcel = $objReader->load($filename);  //load excel into the object
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($filename, PATHINFO_BASENAME) 
				. '": ' . $e->getMessage());
			}

			//  Get worksheet dimensions
			$sheet = $objPHPExcel->getSheet(0); // get sheet 1 of excel file
			$highestRow = $sheet->getHighestRow(); // get heighest row
			$highestColumn = $sheet->getHighestColumn(); // get heighest column
			
			$headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE); // get heading in first row
			
			if (sizeof($headings[0]) < 3) {
				echo "You must have three column in your file i.e radiology test name, radiology test fee and description";
				exit();
			}
			
			//reset($headings[0]) it is the first column
			if(trim(strtolower($headings[0][0])) !="radiology test name"){
				echo "First column should be radiology test name";
				exit();
			}
			//end($headings[0]) it is the last column
			if(trim(strtolower($headings[0][1])) !="radiology test fee"){
				echo "Second column should be radiology test fee";
				exit();
			}
			if(trim(strtolower($headings[0][2])) !="description"){
				echo "Third column should be description";
				exit();
			}
			$sql = "INSERT IGNORE INTO radiology_tests(name,fee,description,created_on,updated_on,created_by,updated_by) VALUES ";
			$countXlsx = 0;
			$subInsertXlsx = '';

			for ($row = 2; $row <= $highestRow; $row++) {
				//  Read a row of data into an array
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL, TRUE, FALSE);
				
				// combine data in array accoding to heading
				$rowData[0] = array_combine($headings[0], $rowData[0]);
				//var_dump($rowData[0]);
				
				// fetch value from row
				$key  = array_keys($rowData[0]);//getting the ondex name

				$name = trim(mysql_real_escape_string($rowData[0][$key[0]]));
				$fee = trim(mysql_real_escape_string($rowData[0][$key[1]]));
				$description = trim(mysql_real_escape_string($rowData[0][$key[2]]));

				// data shouldn't be null in the very first row of file
				if ($countXlsx == 0) {

					if (empty($fee) || empty($name)) {
						echo "Data can't be null in radiology test name and radiology test fee column";
						exit();
					}
				}
				if ($fee != "" && $name != "") {

					//check fee should be float or integer 
					if (is_float($fee) || is_numeric($fee)){

						if ($countXlsx > 0) {
							$subInsertXlsx.=',';
						}
						$subInsertXlsx.="('$name','$fee','$description',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
					}
					else{
						echo "Radiology test fee should be number";
						exit();
					}						
				}				
					
				$countXlsx++;		
			}
			$sql = $sql.$subInsertXlsx;
			$result = mysqli_query($conn,$sql);
			if($result)
			{
				echo $result;
			}
			else{
				echo "";
			}
		}
		else{
			echo "invalid file";
		}
	}
	
	//It is use for select the units 
	if($operation == "showUnitMeasure")			
	{		
		$query="Select id,unit FROM units WHERE status='A'";		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 print json_encode($rows);
	}
	
	//Operation for binding the specimen type 
	if($operation == "showSpecimenType")			
	{
		
		$query="Select id,type FROM specimens_types WHERE status='A'";		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 print json_encode($rows);
	}
	
	// It is use for save the data
	if($operation == "save")			
	{
		/* if (isset($_POST['specimenType'])) {
			$specimenType=$_POST['specimenType'];
		}
		if (isset($_POST['unitMeasure'])) {
			$unitMeasure=$_POST['unitMeasure'];
		} */
		if (isset($_POST['testName'])) {
			$testName=$_POST['testName'];
		}
		/* if (isset($_POST['normalRange'])) {
			$normalRange=$_POST['normalRange'];
		} */
		if (isset($_POST['ledger'])) {
			$ledger=$_POST['ledger'];
		}
		if (isset($_POST['glAccount'])) {
			$glAccount=$_POST['glAccount'];
		}
		if (isset($_POST['accountPayable'])) {
			$accountPayable=$_POST['accountPayable'];
		}
		if (isset($_POST['testFee'])) {
			$testFee=$_POST['testFee'];
		}
		if (isset($_POST['description'])) {
			$description=stripslashes($_POST['description']);
			
		}
		$sql1 = "SELECT name from radiology_tests where name='".$testName."'";
		$resultSelect=mysqli_query($conn,$sql1);
		$rows_count= mysqli_num_rows($resultSelect);
		
		if($rows_count <= 0){
		
			$sql = "INSERT INTO radiology_tests(name,ledger_id,gl_account_id,account_payable_id,fee,
			description,created_on,updated_on,created_by,updated_by)
			VALUES('".$testName."','".$ledger."','".$glAccount."','".$accountPayable."','".$testFee."','".$description."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
			$result= mysqli_query($conn,$sql);  
			$id = mysqli_insert_id($conn);

			$query = "INSERT INTO `insurance_revised_cost`(`insurance_company_id`, `scheme_plan_id`, `scheme_name_id`,`item_name`, `service_type`, `item_id`, 
            `actual_cost`,`revised_cost`, `created_on`, `created_by`, `status`)
             SELECT  
	        insurance_company_id ,scheme_plan_id , scheme_name_id
	        ,'".$testName."' AS item_name ,'radiology' AS service_type , '".$id."' As item_id, '".$testFee."' AS
	        actual_cots, '".$testFee."' AS revised_cost,UNIX_TIMESTAMP() AS created_on ,'".$userId."' 
	        AS created_by, 'A' AS `status`   FROM insurance_revised_cost GROUP BY scheme_name_id ";
	        $result = mysqli_query($conn, $query);
        	echo $result;
		}
		else{
			echo "0";
		}
	} 
	
	//Operation is used for show the active data	
	if($operation == "show"){
		
		$query= "SELECT radiology_tests.id, radiology_tests.name,radiology_tests.ledger_id,radiology_tests.gl_account_id,radiology_tests.account_payable_id, 
		radiology_tests.description ,radiology_tests.fee FROM radiology_tests
		WHERE radiology_tests.status = 'A' order by radiology_tests.created_on DESC";		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);
	}
	
	//Operation is used for show the inactive data
	if($operation == "showInActive"){
		
		$query= "SELECT radiology_tests.id, radiology_tests.name, radiology_tests.ledger_id,radiology_tests.gl_account_id,radiology_tests.account_payable_id, 
		radiology_tests.description ,radiology_tests.fee FROM radiology_tests
		WHERE radiology_tests.status = 'I' order by radiology_tests.created_on DESC";		
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);
	}
	
	//Operation is used for update the status
	if($operation == "delete")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		
		$sql = "UPDATE radiology_tests SET status= 'I'  WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//Operation is used for update the status
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
				
		$sql = "UPDATE radiology_tests SET status= 'A' WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);
		if($result == 1){
		echo "Restored Successfully!!!";	
		}
		else{
			echo "It can not be restore!!!";				
		}
	}
	
	//Operation for update the data
	if($operation == "update")			
	{
		/* if (isset($_POST['specimenType'])) {
			$specimenType=$_POST['specimenType'];
		}
		if (isset($_POST['unitMeasure'])) {
			$unitMeasure=$_POST['unitMeasure'];
		} */
		if (isset($_POST['testName'])) {
			$testName=$_POST['testName'];
		}
		/* if (isset($_POST['normalRange'])) {
			$normalRange=$_POST['normalRange'];
		} */
		if (isset($_POST['ledger'])) {
			$ledger=$_POST['ledger'];
		}
		if (isset($_POST['glAccount'])) {
			$glAccount=$_POST['glAccount'];
		}
		if (isset($_POST['accountPayable'])) {
			$accountPayable=$_POST['accountPayable'];
		}
		if (isset($_POST['testFee'])) {
			$testFee=$_POST['testFee'];
		}
		if (isset($_POST['description'])) {
			$description=stripslashes($_POST['description']);
		}
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		$sql1 = "SELECT name from radiology_tests where name='".$testName."' and id != '$id'";
	    $resultSelect=mysqli_query($conn,$sql1);
		$rows_count= mysqli_num_rows($resultSelect);
		if($rows_count){	
			echo "0";
		}
		else{
			$sql = "UPDATE radiology_tests SET name= '".$testName."',gl_account_id = '".$glAccount."', ledger_id= '".$ledger."',account_payable_id= '".$accountPayable."',
			fee= '".$testFee."',description='".$description."',updated_on=UNIX_TIMESTAMP(),updated_by='".$userId."' WHERE  id = '".$id."'";
			$result= mysqli_query($conn,$sql);  
			echo $result;
		}
	}	
?>
