<?php
/*****************************************************************************************************
 * File Name    :   view_visit.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages visits data
 ****************************************************************************************************/
 
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	$operation = "";	
	
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	
	//This operation used for select details for view visit 
	if ($operation == "visitDetail") {

		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		$query= "SELECT visits.id, visit_type.`type` , CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as name,
			visits.`status`,visits.triage_status,patient_service_bill.`status` as payment_status,
			services.name as service_name,DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%Y-%m-%d') AS visit_date,
			(SELECT value FROM configuration WHERE name = 'visit_prefix') AS prefix from visits
			LEFT JOIN visit_type ON visits.visit_type = visit_type.id
			LEFT JOIN patients ON visits.patient_id = patients.id
			LEFT JOIN patient_service_bill ON visits.id = patient_service_bill.visit_id 
			LEFT JOIN services on services.id = patient_service_bill.service_id 
			WHERE visits.id= '".$id."'";
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);
	}

	//This operation used for select details for triage 
	if ($operation == "visitDetailModal") {

		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		$query= "SELECT visits.id, visit_type.`type` , CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as name,visits.`status`,visits.triage_status,patient_service_bill.`status` as service_status,patient_forensic_bill.`status` as forensic_status,patient_lab_bill.`status` as lab_status,patient_pharmacy_bill.`status` as pharmacy_status,
			patient_procedure_bill.`status` as procedure_status,patient_radiology_bill.`status` as
			radiology_status,services.name as service_name,visits.payment_mode as payment_mode,
			DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%Y-%m-%d %h:%i %p') AS visit_date,
			(SELECT value FROM configuration WHERE name = 'visit_prefix') AS prefix  from visits
			LEFT JOIN visit_type ON visits.visit_type = visit_type.id
			LEFT JOIN patients ON visits.patient_id = patients.id
			LEFT JOIN patient_service_bill ON visits.id = patient_service_bill.visit_id 
			LEFT JOIN services on services.id = patient_service_bill.service_id
			LEFT JOIN patient_forensic_bill ON visits.id = patient_forensic_bill.visit_id 
			LEFT JOIN patient_lab_bill ON visits.id = patient_lab_bill.visit_id 
			LEFT JOIN patient_pharmacy_bill ON visits.id = patient_pharmacy_bill.visit_id 
			LEFT JOIN patient_procedure_bill ON visits.id = patient_procedure_bill.visit_id 
			LEFT JOIN patient_radiology_bill ON visits.id = patient_radiology_bill.visit_id   
			WHERE visits.id= '".$id."'";
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);
	}

	//This operation used for select details for triage 
	if ($operation == "visitDetailPharmacyModal") {

		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		$query= "SELECT DISTINCT(visits.id), visit_type.`type` , CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as name,
			visits.`status`,visits.triage_status,pharmacy_request.pay_status,
			patient_pharmacy_bill.`status` as pharmacy_status,
			DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%Y-%m-%d %h:%i %p') AS visit_date,
			(SELECT value FROM configuration WHERE name = 'visit_prefix') AS prefix  from visits
			LEFT JOIN visit_type ON visits.visit_type = visit_type.id
			LEFT JOIN patients ON visits.patient_id = patients.id
			LEFT JOIN pharmacy_request ON visits.id = pharmacy_request.visit_id
			LEFT JOIN patient_pharmacy_bill ON visits.id = patient_pharmacy_bill.visit_id    
			WHERE visits.id= '".$id."'";
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);
	}
	
	//This operation used for select details for cash modal
	if ($operation == "visitDetailCashModal") {

		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		$query= "SELECT visits.id, visit_type.`type` , CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as name,
			visits.`status`,visits.triage_status,patient_service_bill.`status` as payment_status,
			services.name as service_name,DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%Y-%m-%d') AS visit_date from visits
			LEFT JOIN visit_type ON visits.visit_type = visit_type.id
			LEFT JOIN patients ON visits.patient_id = patients.id
			LEFT JOIN patient_service_bill ON visits.id = patient_service_bill.visit_id 
			LEFT JOIN services on services.id = patient_service_bill.service_id  
			WHERE visits.id= '".$id."'";
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);
	}
	
	//This operation used for search patient for view visit
	if ($operation == "search") {
		
		$getFromDateTimeStamp="";
		$getToDateTimeStamp ="";

		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		if (isset($_POST['getFromDateTimeStamp'])) {
			$getFromDateTimeStamp=$_POST["getFromDateTimeStamp"];
		}
		if (isset($_POST['getToDateTimeStamp'])) {
			$getToDateTimeStamp=$_POST["getToDateTimeStamp"];
		}
		if (isset($_POST['firstName'])) {
			$firstName=$_POST["firstName"];
		}
		if (isset($_POST['lastName'])) {
			$lastName=$_POST["lastName"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['patientId'])) {
			$patientId=$_POST["patientId"];
		}
		if (isset($_POST['paidStatus'])) {
			$paidStatus=$_POST["paidStatus"];
		}
		if (isset($_POST['triAssStatus'])) {
			$triAssStatus=$_POST["triAssStatus"];
		}
		

		$isFirst = "false";

		$query= "SELECT visits.id, visit_type.`type` , CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as name,
			visits.`status`,visits.triage_status,patient_service_bill.`status` as payment_status,visits.payment_mode,
			services.name as service_name,DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%Y-%m-%d') AS visit_date ,
			(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visit_prefix
			from visits
			LEFT JOIN visit_type ON visits.visit_type = visit_type.id
			LEFT JOIN patients ON visits.patient_id = patients.id
			LEFT JOIN patient_service_bill ON visits.id = patient_service_bill.visit_id 
			LEFT JOIN services on services.id = patient_service_bill.service_id  
			WHERE visit_type.`type` != 'Self Request' 
			";

		if($firstName != '' || $lastName != '' || $mobile != '' || $patientId != '' || $getFromDateTimeStamp != '' || $getToDateTimeStamp != '' || $paidStatus != '-1' || $triAssStatus != '-1'){
			
			if($firstName !=''){
				$query .= " AND patients.first_name LIKE '%".$firstName."%' ";
				$isFirst = "true";
			}
			if($lastName !=''){
				$query .= " AND patients.last_name LIKE '%".$lastName."%' ";
				$isFirst = "true";
			}
			if($mobile !=''){
				$query .= " AND patients.mobile LIKE '%".$mobile."%' ";
				$isFirst = "true";
			}
			if($paidStatus !='-1'){
				$query .= " AND patient_service_bill.`status` = '".$paidStatus."' ";
				$isFirst = "true";
			}
			if($triAssStatus !='-1'){
				$query .= " AND visits.`triage_status` = '".$triAssStatus."' ";
				$isFirst = "true";
			}
			if($patientId !=''){
				$patientPrefix = $_POST['patientPrefix'];
				$sql       = "SELECT value FROM configuration WHERE name = 'patient_prefix'";
				$result    = mysqli_query($conn, $sql);
				
				while ($r = mysqli_fetch_assoc($result)) {
					$prefix = $r['value'];
				}
				
				if($prefix == $patientPrefix){
					$query .= " AND patients.id = '".$patientId."' ";
					$isFirst = "true";
				}
			}
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp !=''){
				$query .= " AND visits.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') 
							AND UNIX_TIMESTAMP('".$getToDateTimeStamp." 23:59:59') ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp ==''){
				$query .= " AND visits.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') 
							AND UNIX_TIMESTAMP(NOW()) ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp =='' AND $getToDateTimeStamp !=''){
				$query .= " AND visits.created_on BETWEEN '0000000000' AND 
							UNIX_TIMESTAMP('".$getToDateTimeStamp." 23:59:59') ";
				$isFirst = "true";
			}
		}

		$query .= "  ORDER BY ((DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%y-%m-%d')) = CURDATE()) desc,
						   visits.visit_type= '3' desc,visits.id  asc";

		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);
			
	}

	/*This operation used for search patient for view visit in consultant screen*/
	if ($operation == "searchForConsultantScreen") {
		
		$getFromDateTimeStamp="";
		$getToDateTimeStamp ="";

		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		if (isset($_POST['getFromDateTimeStamp'])) {
			$getFromDateTimeStamp=$_POST["getFromDateTimeStamp"];
		}
		if (isset($_POST['getToDateTimeStamp'])) {
			$getToDateTimeStamp=$_POST["getToDateTimeStamp"];
		}
		if (isset($_POST['firstName'])) {
			$firstName=$_POST["firstName"];
		}
		if (isset($_POST['lastName'])) {
			$lastName=$_POST["lastName"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['patientId'])) {
			$patientId=$_POST["patientId"];
		}

		$isFirst = "false";

		$query= "SELECT visits.id, visit_type.`type` , CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as name,
			visits.`status`,visits.triage_status,patient_service_bill.`status` as payment_status,
			GROUP_CONCAT(services.name) as service_name,DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%Y-%m-%d %h:%i %p') AS visit_date,visits.payment_mode,
			(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visit_prefix from visits
			LEFT JOIN visit_type ON visits.visit_type = visit_type.id
			LEFT JOIN patients ON visits.patient_id = patients.id
			LEFT JOIN patient_service_bill ON visits.id = patient_service_bill.visit_id 
			LEFT JOIN services on services.id = patient_service_bill.service_id   
			WHERE visit_type.id != 4 AND 
			patient_service_bill.`status` = 'paid' AND visit_type.`type` != 'Self Request'";

		if($firstName != '' || $lastName != '' || $mobile != '' || $patientId != '' || $getFromDateTimeStamp != '' || $getToDateTimeStamp != ''){
			
			if($firstName !=''){
				$query .= " AND patients.first_name LIKE '%".$firstName."%'";
				$isFirst = "true";
			}
			if($lastName !=''){
				$query .= " AND patients.last_name LIKE '%".$lastName."%'";
				$isFirst = "true";
			}
			if($mobile !=''){
				$query .= " AND patients.mobile LIKE '%".$mobile."%'";
				$isFirst = "true";
			}
			if($patientId !=''){
				$patientPrefix = $_POST['patientPrefix'];
				$sql       = "SELECT value FROM configuration WHERE name = 'patient_prefix'";
				$result    = mysqli_query($conn, $sql);
				
				while ($r = mysqli_fetch_assoc($result)) {
					$prefix = $r['value'];
				}
				
				if($prefix == $patientPrefix){
					$query .= " AND patients.id = '%".$patientId."' ";
					$isFirst = "true";
				}
			}
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp !=''){
				$query .= " AND visits.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND UNIX_TIMESTAMP('".$getToDateTimeStamp." 23:59:59') ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp ==''){
				$query .= " AND visits.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND UNIX_TIMESTAMP(NOW()) ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp =='' AND $getToDateTimeStamp !=''){
				$query .= " AND visits.created_on BETWEEN '0000000000' AND UNIX_TIMESTAMP('".$getToDateTimeStamp." 23:59:59') ";
				$isFirst = "true";
			}

			$query.=" GROUP BY id ";

			if($isFirst == "true"){
				$query .= "ORDER BY ((DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%y-%m-%d')) = CURDATE()) desc,
						   visits.visit_type= '3' desc,visits.id  asc";
			}
		}
		else{
			$query.= " GROUP BY id ORDER BY ((DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%y-%m-%d')) = CURDATE()) desc,visits.visit_type= '3' desc,visits.id asc";
		}
		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);
	}

	/*This operation used for load patients data for today's date visit in consultant screen*/	
	if ($operation == "searchForConsultantScreenAtLoad") {

		$query= "SELECT visits.id, visit_type.`type` , CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as name,
			visits.`status`,visits.triage_status,patient_service_bill.`status` as payment_status,
			GROUP_CONCAT(services.name) as service_name,DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%Y-%m-%d') AS visit_date,visits.payment_mode,
			(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visit_prefix from visits
			LEFT JOIN visit_type ON visits.visit_type = visit_type.id
			LEFT JOIN patients ON visits.patient_id = patients.id
			LEFT JOIN patient_service_bill ON visits.id = patient_service_bill.visit_id 
			LEFT JOIN services on services.id = patient_service_bill.service_id   
			WHERE visit_type.id != 4 AND patient_service_bill.`status` = 'paid' 
			AND visit_type.`type` != 'Self Request' 
			AND  DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%y-%m-%d') = CURDATE() GROUP BY id
			ORDER BY visits.visit_type= '3' desc,visits.id asc";

		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		print json_encode($rows);
	}

	//This operation used for search patient for triage 
	if ($operation == "searchModal") {

		$getFromDateTimeStamp="";
		$getToDateTimeStamp ="";

		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		if (isset($_POST['getFromDateTimeStamp'])) {
			$getFromDateTimeStamp=$_POST["getFromDateTimeStamp"];
		}
		if (isset($_POST['getToDateTimeStamp'])) {
			$getToDateTimeStamp=$_POST["getToDateTimeStamp"];
		}
		if (isset($_POST['firstName'])) {
			$firstName=$_POST["firstName"];
		}
		if (isset($_POST['lastName'])) {
			$lastName=$_POST["lastName"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['patientId'])) {
			$patientId=$_POST["patientId"];
		}
		if (isset($_POST['paidStatus'])) {
			$paidStatus=$_POST["paidStatus"];
		}
		if (isset($_POST['triAssStatus'])) {
			$triAssStatus=$_POST["triAssStatus"];
		}
		$isFirst = "false";

		$query= "SELECT visits.id, visit_type.`type` , CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as name,
			visits.`status`,visits.triage_status,patient_service_bill.`status` as payment_status,visits.payment_mode AS payment_mode,
			services.name as service_name,DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%Y-%m-%d') AS visit_date,
			(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visit_prefix from visits
			LEFT JOIN visit_type ON visits.visit_type = visit_type.id
			LEFT JOIN patients ON visits.patient_id = patients.id
			LEFT JOIN patient_service_bill ON visits.id = patient_service_bill.visit_id 
			LEFT JOIN services on services.id = patient_service_bill.service_id   
			WHERE visits.`triage_status` ='".$triAssStatus."' AND 
			patient_service_bill.`status` = '".$paidStatus."' AND visit_type.`type` != 'Self Request'";
			
		if($firstName != '' || $lastName != '' || $mobile != '' || $patientId != '' || $getFromDateTimeStamp != '' || $getToDateTimeStamp != ''){
			if($firstName !=''){
				$query .= " AND patients.first_name LIKE '%".$firstName."%'";
				$isFirst = "true";
			}
			if($lastName !=''){
				$query .= " AND patients.last_name LIKE '%".$lastName."%'";
				$isFirst = "true";
			}
			if($mobile !=''){
				$query .= " AND patients.mobile LIKE '%".$mobile."%'";
				$isFirst = "true";
			}
			if($patientId !=''){
				$patientPrefix = $_POST['patientPrefix'];
				$sql       = "SELECT value FROM configuration WHERE name = 'patient_prefix'";
				$result    = mysqli_query($conn, $sql);
				
				while ($r = mysqli_fetch_assoc($result)) {
					$prefix = $r['value'];
				}
				
				if($prefix == $patientPrefix){
					$query .= " AND patients.id = '".$patientId."' ";
					$isFirst = "true";
				}
			}
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp !=''){
				$query .= " AND visits.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND UNIX_TIMESTAMP('".$getToDateTimeStamp." 23:59:59') ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp ==''){
				$query .= " AND visits.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND UNIX_TIMESTAMP(NOW()) ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp =='' AND $getToDateTimeStamp !=''){
				$query .= " AND visits.created_on BETWEEN '0000000000' AND UNIX_TIMESTAMP('".$getToDateTimeStamp." 23:59:59') ";
				$isFirst = "true";
			}
			if($isFirst == "true"){
				$query .= "ORDER BY ((DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%y-%m-%d')) = CURDATE()) desc,
						   visits.visit_type= '3' desc,visits.id  asc";
			}
		}else{
			$query .= " ORDER BY ((DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%y-%m-%d')) = CURDATE()) desc,
						visits.visit_type= '3' desc,visits.id  asc ";
		}
		
		$result= mysqli_query($conn,$query);
		$rows = array();
		if($result)
		{
			while($r = mysqli_fetch_assoc($result)) {
			 $rows[] = $r;
			}
			print json_encode($rows);
			exit();
		}		
	}
	
	//This operation used for search patient for Pharmacy 
	if ($operation == "searchPharmacyModal") {

		$getFromDateTimeStamp="";
		$getToDateTimeStamp ="";

		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		if (isset($_POST['getFromDateTimeStamp'])) {
			$getFromDateTimeStamp=$_POST["getFromDateTimeStamp"];
		}
		if (isset($_POST['getToDateTimeStamp'])) {
			$getToDateTimeStamp=$_POST["getToDateTimeStamp"];
		}
		if (isset($_POST['firstName'])) {
			$firstName=$_POST["firstName"];
		}
		if (isset($_POST['lastName'])) {
			$lastName=$_POST["lastName"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['patientId'])) {
			$patientId=$_POST["patientId"];
		}

		$isFirst = "false";

		$query= "SELECT distinct(visits.id), visit_type.`type` , CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as name,		
			DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%Y-%m-%d') AS visit_date,
			(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visit_prefix from visits
			LEFT JOIN visit_type ON visits.visit_type = visit_type.id       
			LEFT JOIN patients ON visits.patient_id = patients.id      
			LEFT JOIN patients_prescription_request ON visits.id = patients_prescription_request.visit_id
			LEFT JOIN pharmacy_request ON visits.id = pharmacy_request.visit_id
			WHERE (visits.`prescription_status` ='Yes' AND  patients_prescription_request.dispense_status = 'pending') OR 
			  (visits.`prescription_status` ='Yes' AND  pharmacy_request.pay_status = 'unpaid')";
			
		if($firstName != '' || $lastName != '' || $mobile != '' || $patientId != '' || $getFromDateTimeStamp != '' || $getToDateTimeStamp != ''){
			if($firstName !=''){
				$query .= " AND patients.first_name LIKE '%".$firstName."%'";
				$isFirst = "true";
			}
			if($lastName !=''){
				$query .= " AND patients.last_name LIKE '%".$lastName."%'";
				$isFirst = "true";
			}
			if($mobile !=''){
				$query .= " AND patients.mobile LIKE '%".$mobile."%'";
				$isFirst = "true";
			}
			if($patientId !=''){
				$patientPrefix = $_POST['patientPrefix'];
				$sql       = "SELECT value FROM configuration WHERE name = 'patient_prefix'";
				$result    = mysqli_query($conn, $sql);
				
				while ($r = mysqli_fetch_assoc($result)) {
					$prefix = $r['value'];
				}
				
				if($prefix == $patientPrefix){
					$query .= " AND patients.id = '".$patientId."' ";
					$isFirst = "true";
				}
			}
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp !=''){
				$query .= " AND visits.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND UNIX_TIMESTAMP('".$getToDateTimeStamp." 23:59:59') ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp ==''){
				$query .= " AND visits.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND UNIX_TIMESTAMP(NOW()) ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp =='' AND $getToDateTimeStamp !=''){
				$query .= " AND visits.created_on BETWEEN '0000000000' AND UNIX_TIMESTAMP('".$getToDateTimeStamp." 23:59:59') ";
				$isFirst = "true";
			}
			if($isFirst == "true"){
				$query .= "ORDER BY ((DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%y-%m-%d')) = CURDATE()) desc,
						   visits.visit_type= '3' desc,visits.id  asc";
			}
		}
		else{
			$query .= "ORDER BY ((DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%y-%m-%d')) = CURDATE()) desc,
					   visits.visit_type= '3' desc,visits.id  asc";
		}
		
		//echo $query;
		$result= mysqli_query($conn,$query);
		$rows = array();
		if($result)
		{
			while($r = mysqli_fetch_assoc($result)) {
			 $rows[] = $r;
			}
			print json_encode($rows);
			exit();
		}		
	}
	
	//This operation used for search patient for vision assissment 
	if ($operation == "searchVisionModal") {

		$getFromDateTimeStamp="";
		$getToDateTimeStamp ="";

		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		if (isset($_POST['getFromDateTimeStamp'])) {
			$getFromDateTimeStamp=$_POST["getFromDateTimeStamp"];
		}
		if (isset($_POST['getToDateTimeStamp'])) {
			$getToDateTimeStamp=$_POST["getToDateTimeStamp"];
		}
		if (isset($_POST['firstName'])) {
			$firstName=$_POST["firstName"];
		}
		if (isset($_POST['lastName'])) {
			$lastName=$_POST["lastName"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['patientId'])) {
			$patientId=$_POST["patientId"];
		}
		if (isset($_POST['paidStatus'])) {
			$paidStatus=$_POST["paidStatus"];
		}
		if (isset($_POST['triAssStatus'])) {
			$triAssStatus=$_POST["triAssStatus"];
		}
		$isFirst = "false";

		$query= "SELECT visits.id, visit_type.`type` , CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as name,
			visits.`status`,visits.triage_status,patient_service_bill.`status` as payment_status,visits.payment_mode AS payment_mode,
			services.name as service_name,DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%Y-%m-%d') AS visit_date,
			(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visit_prefix from visits
			LEFT JOIN visit_type ON visits.visit_type = visit_type.id
			LEFT JOIN patients ON visits.patient_id = patients.id
			LEFT JOIN patient_service_bill ON visits.id = patient_service_bill.visit_id 
			LEFT JOIN services on services.id = patient_service_bill.service_id   
			WHERE visit_type.id != 4 AND visits.triage_status = '".$triAssStatus."' AND 
			patient_service_bill.`status` = '".$paidStatus."'";

		if (isset($_POST['clickButton'])) {
			
			if($firstName != '' || $lastName != '' || $mobile != '' || $patientId != '' || $getFromDateTimeStamp != '' || $getToDateTimeStamp != ''){
				if($firstName !=''){
					$query .= " AND patients.first_name LIKE '%".$firstName."%'";
					$isFirst = "true";
				}
				if($lastName !=''){
					$query .= " AND patients.last_name LIKE '%".$lastName."%'";
					$isFirst = "true";
				}
				if($mobile !=''){
					$query .= " AND patients.mobile LIKE '%".$mobile."%'";
					$isFirst = "true";
				}
				if($patientId !=''){
					$patientPrefix = $_POST['patientPrefix'];
					$sql       = "SELECT value FROM configuration WHERE name = 'patient_prefix'";
					$result    = mysqli_query($conn, $sql);
					
					while ($r = mysqli_fetch_assoc($result)) {
						$prefix = $r['value'];
					}
					
					if($prefix == $patientPrefix){
						$query .= " AND patients.id = '".$patientId."' ";
						$isFirst = "true";
					}
				}
				if($getFromDateTimeStamp !='' AND $getToDateTimeStamp !=''){
					$query .= " AND visits.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND UNIX_TIMESTAMP('".$getToDateTimeStamp." 23:59:59') ";
					$isFirst = "true";
				}
				if($getFromDateTimeStamp !='' AND $getToDateTimeStamp ==''){
					$query .= " AND visits.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND UNIX_TIMESTAMP(NOW()) ";
					$isFirst = "true";
				}
				if($getFromDateTimeStamp =='' AND $getToDateTimeStamp !=''){
					$query .= " AND visits.created_on BETWEEN '0000000000' AND UNIX_TIMESTAMP('".$getToDateTimeStamp." 23:59:59') ";
					$isFirst = "true";
				}
			} 
		}
		else{
			$query .= "AND ((DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%y-%m-%d')) = CURDATE())  ";
		}

		$query .= " ORDER BY ((DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%y-%m-%d')) = CURDATE()) desc,
					   visits.visit_type= '3' desc,visits.id  asc"; 
			
		$result= mysqli_query($conn,$query);
		$rows = array();
		if($result)
		{
			while($r = mysqli_fetch_assoc($result)) {
			 $rows[] = $r;
			}
			print json_encode($rows);
			exit();
		}		
	}
	
	
	//This operation used for search patient for cash payment 
	if ($operation == "searchCashModal") {

		$getFromDateTimeStamp="";
		$getToDateTimeStamp ="";

		if (isset($_POST['id'])) {
			$id=$_POST["id"];
		}
		if (isset($_POST['getFromDateTimeStamp'])) {
			$getFromDateTimeStamp=$_POST["getFromDateTimeStamp"];
		}
		if (isset($_POST['getToDateTimeStamp'])) {
			$getToDateTimeStamp=$_POST["getToDateTimeStamp"];
		}
		if (isset($_POST['firstName'])) {
			$firstName=$_POST["firstName"];
		}
		if (isset($_POST['lastName'])) {
			$lastName=$_POST["lastName"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['mobile'])) {
			$mobile=$_POST["mobile"];
		}
		if (isset($_POST['patientId'])) {
			$patientId=$_POST["patientId"];
		}

		$isFirst = "false";

		/*This isset coing from insurace payment screen*/
		if (isset($_POST['inurancePayment'])) {
			$query = "SELECT visits.id, visit_type.`type` , CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as name,
			DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%Y-%m-%d') AS visit_date,
			(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visit_prefix
			from visits			 
		    LEFT JOIN visit_type ON visits.visit_type = visit_type.id
		    LEFT JOIN patients ON visits.patient_id = patients.id
		    WHERE visits.payment_mode = 'insurance'";
		}
		else{
			$query= "SELECT DISTINCT(visits.id), visit_type.`type` , CONCAT(patients.first_name,' ',patients.middle_name,' ',patients.last_name) as name,
				DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%Y-%m-%d') AS visit_date,
				(SELECT value FROM configuration WHERE name = 'visit_prefix') AS visit_prefix
				from visits			 
			    LEFT JOIN visit_type ON visits.visit_type = visit_type.id
			    LEFT JOIN patients ON visits.patient_id = patients.id
			    LEFT JOIN patient_service_bill ON visits.id = patient_service_bill.visit_id 
			    LEFT JOIN patient_procedure_bill ON visits.id = patient_procedure_bill.visit_id 
			    LEFT JOIN patient_radiology_bill ON visits.id = patient_radiology_bill.visit_id
			    LEFT JOIN patients_prescription_request ON visits.id = patients_prescription_request.visit_id 
			    LEFT JOIN patient_lab_bill ON visits.id = patient_lab_bill.visit_id 
			    LEFT JOIN lab_test_request_component AS ltrc ON ltrc.patient_lab_bill_id = patient_lab_bill.id     
			    LEFT JOIN patient_forensic_bill ON visits.id = patient_forensic_bill.visit_id 
			    where (patient_service_bill.`status` = 'unpaid' or patient_procedure_bill.`status` = 'unpaid' 
				or patient_radiology_bill.`status` = 'unpaid' or patients_prescription_request.insurance_payment ='unpaid' 
				or ltrc.pay_status = 'unpaid'  or  patient_forensic_bill.`status` = 'unpaid')";
		}

		

		if($firstName != '' || $lastName != '' || $mobile != '' || $patientId != '' || $getFromDateTimeStamp != '' || $getToDateTimeStamp != ''){
			if($firstName !=''){
				$query .= " AND patients.first_name LIKE '%".$firstName."%'";
				$isFirst = "true";
			}
			if($lastName !=''){
				$query .= " AND patients.last_name LIKE '%".$lastName."%'";
				$isFirst = "true";
			}
			if($mobile !=''){
				$query .= " AND patients.mobile LIKE '%".$mobile."%'";
				$isFirst = "true";
			}
			if($patientId !=''){
				$patientPrefix = $_POST['patientPrefix'];
				$sql       = "SELECT value FROM configuration WHERE name = 'patient_prefix'";
				$result    = mysqli_query($conn, $sql);
				
				while ($r = mysqli_fetch_assoc($result)) {
					$prefix = $r['value'];
				}
				
				if($prefix == $patientPrefix){
					$query .= " AND patients.id = '".$patientId."' ";
					$isFirst = "true";
				}
			}
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp !=''){
				$query .= " AND visits.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND UNIX_TIMESTAMP('".$getToDateTimeStamp." 23:59:59') ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp !='' AND $getToDateTimeStamp ==''){
				$query .= " AND visits.created_on BETWEEN UNIX_TIMESTAMP('".$getFromDateTimeStamp." 00:00:00') AND UNIX_TIMESTAMP(NOW()) ";
				$isFirst = "true";
			}
			if($getFromDateTimeStamp =='' AND $getToDateTimeStamp !=''){
				$query .= " AND visits.created_on BETWEEN '0000000000' AND UNIX_TIMESTAMP('".$getToDateTimeStamp." 23:59:59') ";
				$isFirst = "true";
			}
		}

		$query .= "  ORDER BY ((DATE_FORMAT(FROM_UNIXTIME(visits.created_on), '%y-%m-%d')) = CURDATE()) desc,
						   visits.visit_type= '3' desc,visits.id  asc";
		
		$result=mysqli_query($conn,$query);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		print json_encode($rows);
	}	
	
?>