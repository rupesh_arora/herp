
<?php
	
	session_start();
	if(!session_id()){
		exit();
	}
	$toDate = '';
	$fromDate = '';
	
	include_once('config.php');

	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET['operation'])){
		$operation=$_GET["operation"];
	}

	if ($operation=="showChartData") {
		if (isset($_POST['dataLoad'])) {
			$dataLoad = $_POST["dataLoad"];
		}
		if (isset($_POST['fromDate'])) {
			$fromDate = $_POST["fromDate"];
		}
		if (isset($_POST['toDate'])) {
			$toDate = $_POST["toDate"];
		}

		if ($dataLoad =="all") {

			$countAmountCollected = "SELECT SUM(patient_service_bill.charge) AS 'Amount Collected',(CASE WHEN visits.department IS NULL THEN 'Others' ELSE department.department END) AS 
				'Department Name' FROM patient_service_bill
				LEFT JOIN visits ON visits.id = patient_service_bill.visit_id
				LEFT JOIN department ON department.id  = visits.department
				GROUP BY visits.department";
		}
		
		else if ($dataLoad =="last30days") {

			
			$countAmountCollected = "SELECT SUM(patient_service_bill.charge) AS 'Amount Collected',(CASE WHEN visits.department IS NULL THEN 'Others' ELSE department.department END) AS 
				'Department Name' FROM patient_service_bill
				LEFT JOIN visits ON visits.id = patient_service_bill.visit_id
				LEFT JOIN department ON department.id  = visits.department
				WHERE patient_service_bill.created_on >= unix_timestamp(curdate() - interval 1 month)
				GROUP BY visits.department";
		}


		else if ($dataLoad =="last7days") {
			
			$countAmountCollected = "SELECT SUM(patient_service_bill.charge) AS 'Amount Collected',(CASE WHEN visits.department IS NULL THEN 'Others' ELSE department.department END) AS 
				'Department Name' FROM patient_service_bill
				LEFT JOIN visits ON visits.id = patient_service_bill.visit_id
				LEFT JOIN department ON department.id  = visits.department
				WHERE patient_service_bill.created_on >= unix_timestamp(curdate() - interval 7 day)
				GROUP BY visits.department";
		}


		else if ($dataLoad =="today") {			

			$countAmountCollected = "SELECT SUM(patient_service_bill.charge) AS 'Amount Collected',(CASE WHEN visits.department IS NULL THEN 'Others' ELSE department.department END) AS 
				'Department Name' FROM patient_service_bill
				LEFT JOIN visits ON visits.id = patient_service_bill.visit_id
				LEFT JOIN department ON department.id  = visits.department
				WHERE patient_service_bill.created_on >= unix_timestamp(curdate())
				GROUP BY visits.department";
		}
		else  {

			$countAmountCollected = "SELECT SUM(patient_service_bill.charge) AS 'Amount Collected',(CASE WHEN visits.department IS NULL THEN 'Others' ELSE department.department END) AS 
				'Department Name' FROM patient_service_bill
				LEFT JOIN visits ON visits.id = patient_service_bill.visit_id
				LEFT JOIN department ON department.id  = visits.department
				WHERE  patient_service_bill.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate."
				00:00:00') AND UNIX_TIMESTAMP('".$toDate." 23:59:59')
				GROUP BY visits.department";
		}		
		
		$rowsAmountCollected  = array();
	    $resultAmountCollected  = mysqli_query($conn,$countAmountCollected);
	    while ($r = mysqli_fetch_assoc($resultAmountCollected)) {
	        $rowsAmountCollected[] = $r;
	    }


	    echo json_encode($rowsAmountCollected);
	}
?>