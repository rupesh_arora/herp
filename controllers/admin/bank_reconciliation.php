<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "ShowPaymentMode") {

    	$ledgerId = $_POST['ledgerId'];
	 	// show Scheme Name
	 	 $query = "SELECT id,name from payment_mode where ledger_type='".$ledgerId."'";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
    
	}
	if ($operation == "saveBankReconciliation") {

		$ledger = $_POST['ledger'];
		$paymentMode = $_POST['paymentMode'];
		$name = $_POST['name'];
		$statementBalance = $_POST['statementBalance'];
		$reconciliationDate = $_POST['reconciliationDate'];
		$reconcilationClosedDate = $_POST['reconcilationClosedDate'];
		$lastReconcilationDate = $_POST['lastReconcilationDate'];

		 $query = "INSERT INTO bank_reconciliation(ledger_id,payment_mode_id,name,statement_balance,reconciliation_date,last_reconciliation_date,reconciliation_close_date,created_by,updated_by,created_on,updated_on) VALUES('".$ledger."','".$paymentMode."','".$name."','".$statementBalance."','".$reconciliationDate."','".$reconcilationClosedDate."','".$lastReconcilationDate."','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";

		$result = mysqli_query($conn, $query);
		echo $result;
	}

	if ($operation == "show") { // show active data
    
	    	$query = "SELECT br.id,ledger_id,payment_mode_id,br.name AS name,statement_balance,reconciliation_date,last_reconciliation_date,reconciliation_close_date,ledger.name AS ledger_name,payment_mode.name AS payement_mode_name FROM bank_reconciliation AS br
			LEFT JOIN ledger on ledger.id = br.ledger_id
			LEFT JOIN payment_mode ON payment_mode.id = br.payment_mode_id
			 WHERE br.`status` = 'A'";
	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}

	if ($operation == "showBankingItems") { // show active data
    
    	$ledger = $_GET['ledger'];

    	$query = "SELECT pm.name,rb.reference,bsrn.amount,bsrn.receipt_id,DATE( FROM_UNIXTIME( bsrn.created_on ) ) as date,
			(SELECT value FROM configuration WHERE name = 'receipt_prefix') AS receipt_prefix 
			 FROM banking_schedule AS bs 
			LEFT JOIN banking_schedule_receipt_number AS bsrn ON bsrn.banking_schedule_id = bs.id 
			LEFT JOIN receipt_book AS rb ON rb.id = bsrn.receipt_id 
			LEFT JOIN payment_mode AS pm ON pm.id = rb.payment_mode_id 
			WHERE rb.ledger_id ='".$ledger."' AND rb.`status` = 'A'";

	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}

	if ($operation == "showDepositIntem") { // show active data
    
    	$ledger = $_GET['ledger'];

    	$query = "SELECT rb.id,rb.reference ,rb.amount,DATE( FROM_UNIXTIME( pm.created_on) ) AS transaction_date ,pm.name,DATE( FROM_UNIXTIME( rb.created_on ) ) AS document_date,
    	(SELECT value FROM configuration WHERE name = 'receipt_prefix') AS receipt_prefix 
    	 FROM receipt_book AS rb 
		LEFT JOIN payment_mode AS pm ON pm.id = rb.payment_mode_id 
		WHERE rb.ledger_id ='".$ledger."' AND pm.is_available_bank_source = 'bank'";

	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}

	if ($operation == "showPayment") { // show active data
    
    	$ledger = $_GET['ledger'];

    	$query = "SELECT pv.id,pv.amount,pv.payee_name ,pm.name,pv.cheque_no,l.name AS ledger_name,(SELECT value FROM configuration WHERE name = 'voucher_prefix') AS voucher_prefix 
    		 FROM payment_voucher AS pv 
			LEFT JOIN payment_mode AS pm ON pm.id = pv.`payment_mode` 
			LEFT JOIN ledger AS l ON l.id = pv.ledger_id 
			WHERE l.id ='".$ledger."'";

	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}
	
	if ($operation == "showJournalVoucher") { // show active data
    
    	$ledger = $_GET['ledger'];

    	$query = "SELECT pv.id,pv.amount,pv.payee_name ,pm.name,pv.cheque_no,l.name AS ledger_name,DATE( FROM_UNIXTIME( pv.created_on) ) AS `date`,(SELECT value FROM configuration WHERE name = 'voucher_prefix') AS voucher_prefix 
    		 FROM payment_voucher AS pv 
			LEFT JOIN payment_mode AS pm ON pm.id = pv.`payment_mode` 
			LEFT JOIN ledger AS l ON l.id = pv.ledger_id 
			WHERE l.id ='".$ledger."'";

	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }

	    $impQuery = "SELECT pv.id,pv.amount,pm.payee_name ,pm.name,pv.cheque_no,l.name AS ledger_name,DATE( FROM_UNIXTIME( pv.created_on) ) AS `date`,(SELECT value FROM configuration WHERE name = 'imprest_prefix') AS voucher_prefix 
    		 FROM imprest_disbursement AS pv 
			LEFT JOIN payment_mode AS pm ON pm.id = pv.`payment_mode` 
			LEFT JOIN ledger AS l ON l.id = pm.ledger_type 
			WHERE l.id ='".$ledger."'";
	    //print json_encode($rows);
	    $result = mysqli_query($conn, $impQuery);
	    $totalrecords = mysqli_num_rows($result);
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}

	if ($operation == "checked") {
	    
	    $query = "SELECT br.id,ledger_id,payment_mode_id,br.name AS name,statement_balance,reconciliation_date,last_reconciliation_date,reconciliation_close_date,ledger.name AS ledger_name,payment_mode.name AS payement_mode_name FROM bank_reconciliation AS br
			LEFT JOIN ledger on ledger.id = br.ledger_id
			LEFT JOIN payment_mode ON payment_mode.id = br.payment_mode_id
			 WHERE br.`status` = 'I'";
	    
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}
	if ($operation == "delete") {
        $id = $_POST['id'];

		$sql    = "UPDATE bank_reconciliation SET status= 'I' where id = '" . $id . "'";
	    $result = mysqli_query($conn, $sql);
	    echo $result;	    
	}

	if ($operation == "restore") {// for restore    
        $id = $_POST['id'];

		echo $sql    = "UPDATE bank_reconciliation SET status= 'A'  WHERE  id = '" . $id . "'";
		$result = mysqli_query($conn, $sql);
	    echo $result;
	}
?>