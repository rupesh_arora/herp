<?php
/*File Name  :   roster_management.php
Company Name :   Qexon Infotech
Created By   :   Tushar Gupta
Created Date :   21th March, 2015
Description  :   This page manages entry roster and attendence management*/

	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}

	/*include config file*/
	include 'config.php';


	/*checking operation set or not*/
	if (isset($_POST['operation'])) {
	    $operation = $_POST["operation"];
	} else if (isset($_GET["operation"])) {
	    $operation = $_GET["operation"];
	}

	if ($operation == "show") {
		
	    $query = "SELECT imprest_warrent.id,imprest_warrent.surrender_status, imprest_request_id,staff_id,ledger_id,CONCAT(users.first_name,' ',users.last_name) AS name,nature_of_duty,amount,assignment_start,assignment_end,est_day,ledger.name AS ledger_name,DATE_FORMAT(FROM_UNIXTIME(imprest_warrent.created_on), '%Y-%m-%d') AS imprest_date FROM imprest_warrent 
			LEFT JOIN users ON users.id = imprest_warrent.staff_id 
			LEFT JOIN ledger ON ledger.id = imprest_warrent.ledger_id
			WHERE imprest_warrent.`status`= 'A'";

			if (isset($_GET['notDisbursed'])) {
				$query.=" AND imprest_warrent.imprest_request_id NOT IN(SELECT imprest_request_id FROM imprest_disbursement) ";
			}
			if(isset($_GET['fromDate']) && isset($_GET['toDate']) !=''){

				$fromDate = $_GET['fromDate'];
				$toDate = $_GET['toDate'];

				if( $fromDate !='' AND $toDate !='' ){

					$fromDate = $_GET['fromDate'];
					$toDate = $_GET['toDate'];
					$query .= " AND imprest_warrent.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') 
								AND UNIX_TIMESTAMP('".$toDate." 23:59:59') ";
					$isFirst = "true";
				}
				if( $fromDate !='' AND $toDate =='' ){

					$fromDate = $_GET['fromDate'];
					$toDate = $_GET['toDate'];
					$query .= " AND imprest_warrent.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') 
								AND UNIX_TIMESTAMP(NOW()) ";
					$isFirst = "true";
				}
				if( $fromDate =='' AND $toDate !='' ){

					$fromDate = $_GET['fromDate'];
					$toDate = $_GET['toDate'];
					$query .= " AND imprest_warrent.created_on BETWEEN '0000000000' AND 
								UNIX_TIMESTAMP('".$toDate." 23:59:59') ";
					$isFirst = "true";
				}
			}
		    $result = mysqli_query($conn, $query);
		    $totalrecords = mysqli_num_rows($result);
		    $rows         = array();
		    while ($r = mysqli_fetch_assoc($result)) {
		        $rows[] = $r;
		    }
		    //print json_encode($rows);
		    
		    $json = array(
		        'sEcho' => '1',
		        'iTotalRecords' => $totalrecords,
		        'iTotalDisplayRecords' => $totalrecords,
		        'aaData' => $rows
		    );
		    echo json_encode($json);	
	}

	if ($operation == "showNotDisbursed") {
		$fromDate = $_GET['fromDate'];
		$toDate = $_GET['toDate'];
	    $query = "SELECT imprest_warrent.id,imprest_warrent.imprest_request_id,staff_id,ledger_id,CONCAT(users.first_name,' ',users.last_name) AS name,nature_of_duty,imprest_warrent.amount,assignment_start,assignment_end,est_day,ledger.name AS ledger_name,DATE_FORMAT(FROM_UNIXTIME(imprest_warrent.created_on), '%Y-%m-%d') AS imprest_date FROM imprest_warrent 
			LEFT JOIN users ON users.id = imprest_warrent.staff_id 
			LEFT JOIN ledger ON ledger.id = imprest_warrent.ledger_id
			WHERE imprest_warrent.`status`= 'A' AND imprest_warrent.imprest_request_id NOT IN(SELECT imprest_request_id FROM imprest_disbursement) ";		

		if( $fromDate !='' AND $toDate !='' ){

			$fromDate = $_GET['fromDate'];
			$toDate = $_GET['toDate'];
			$query .= " AND imprest_warrent.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') 
						AND UNIX_TIMESTAMP('".$toDate." 23:59:59') ";
			$isFirst = "true";
		}
		if( $fromDate !='' AND $toDate =='' ){

			$fromDate = $_GET['fromDate'];
			$toDate = $_GET['toDate'];
			$query .= " AND imprest_warrent.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') 
						AND UNIX_TIMESTAMP(NOW()) ";
			$isFirst = "true";
		}
		if( $fromDate =='' AND $toDate !='' ){

			$fromDate = $_GET['fromDate'];
			$toDate = $_GET['toDate'];
			$query .= " AND imprest_warrent.created_on BETWEEN '0000000000' AND 
						UNIX_TIMESTAMP('".$toDate." 23:59:59') ";
			$isFirst = "true";
		}

	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);	
	}


	if ($operation == "showSurrender") {
		$fromDate = $_GET['fromDate'];
		$toDate = $_GET['toDate'];
		$query = "SELECT i_d.id,i_d.imprest_request_id,iw.staff_id,iw.ledger_id,CONCAT(users.first_name,' ',users.last_name) AS name,nature_of_duty,iw.amount,iw.assignment_start,iw.assignment_end,iw.est_day,ledger.name AS ledger_name,DATE_FORMAT(FROM_UNIXTIME(iw.created_on), '%Y-%m-%d') AS imprest_date FROM imprest_disbursement AS i_d 
			LEFT JOIN imprest_warrent AS iw ON   i_d.imprest_request_id = iw.imprest_request_id 
			LEFT JOIN users ON users.id = iw.staff_id 
			LEFT JOIN ledger ON ledger.id = iw.ledger_id
			WHERE i_d.`status`= 'A'";

			if( $fromDate !='' AND $toDate !='' ){

				$fromDate = $_GET['fromDate'];
				$toDate = $_GET['toDate'];
				$query .= " AND iw.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') 
							AND UNIX_TIMESTAMP('".$toDate." 23:59:59') ";
				$isFirst = "true";
			}
			if( $fromDate !='' AND $toDate =='' ){

				$fromDate = $_GET['fromDate'];
				$toDate = $_GET['toDate'];
				$query .= " AND iw.created_on BETWEEN UNIX_TIMESTAMP('".$fromDate." 00:00:00') 
							AND UNIX_TIMESTAMP(NOW()) ";
				$isFirst = "true";
			}
			if( $fromDate =='' AND $toDate !='' ){

				$fromDate = $_GET['fromDate'];
				$toDate = $_GET['toDate'];
				$query .= " AND iw.created_on BETWEEN '0000000000' AND 
							UNIX_TIMESTAMP('".$toDate." 23:59:59') ";
				$isFirst = "true";
			}

		    $result = mysqli_query($conn, $query);
		    $totalrecords = mysqli_num_rows($result);
		    $rows         = array();
		    while ($r = mysqli_fetch_assoc($result)) {
		        $rows[] = $r;
		    }
		    //print json_encode($rows);
		    
		    $json = array(
		        'sEcho' => '1',
		        'iTotalRecords' => $totalrecords,
		        'iTotalDisplayRecords' => $totalrecords,
		        'aaData' => $rows
		    );
		    echo json_encode($json);
	}

	if ($operation == "showWarrentDetail") {

		$warrantId = $_POST['warrantId'];

	    $query = "SELECT iwl.id,iwl.description,iwl.initial_amount,coa.account_name,iwl.spent_amount,iwl.spent_amount_status FROM 
	    		`imprest_warrent_line_item` AS iwl
				LEFT JOIN chart_of_account AS coa ON iwl.chart_of_account_id = coa.id 
				WHERE iwl.imprest_warrent_id ='".$warrantId."'";
		    $result = mysqli_query($conn, $query);
		    $totalrecords = mysqli_num_rows($result);
		    $rows         = array();
		    while ($r = mysqli_fetch_assoc($result)) {
		        $rows[] = $r;
		    }
		    print json_encode($rows);
		    
		   /* $json = array(
		        'sEcho' => '1',
		        'iTotalRecords' => $totalrecords,
		        'iTotalDisplayRecords' => $totalrecords,
		        'aaData' => $rows
		    );
		    echo json_encode($rows);*/	
	}

	if ($operation == "showLedger") { // show insurance company type
	    $query = "SELECT id,name FROM ledger where status = 'A' ";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}
	if ($operation == "update") { // show insurance company type
		$id = $_POST['id'];
		$ledger = $_POST['ledger'];

	    $query = "UPDATE imprest_warrent SET ledger_id = '".$ledger."' where id = '".$id."' AND status= 'A'";
	    $result = mysqli_query($conn, $query);
		echo $result;
	    
	}
	if ($operation == "saveLineItems") { // show insurance company type
		$imprestRequestId = $_POST['imprestRequestId'];
		$imprestWarrantId = $_POST['imprestWarrantId'];
		$updateLineItems = json_decode($_POST['updateLineItems']);
		$ledger = $_POST['ledger'];

		if(!empty($updateLineItems)){
			$queryLineItems = "INSERT INTO `imprest_warrent_line_item`(imprest_request_id,imprest_warrent_id,chart_of_account_id,
	    		description,initial_amount,created_on,updated_on,created_by,updated_by) VALUES ";
	    	$subquery = '';
	    	$lineItemcounter = 0;

	    	foreach ($updateLineItems as $value) {
				$description    = $value->description;
		        $amount     = $value->amount;
		        $glLedger      = $value->glLedger;

		        if ($lineItemcounter > 0) {
		        	$subquery.=',';
		        }
		         $subquery.="('".$imprestRequestId."','".$imprestWarrantId."','".$glLedger."','".$description."','".$amount."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
		        $lineItemcounter++;
			}
			$completeLineItemQry = $queryLineItems.$subquery;

			$queryImprestwarrant = "UPDATE imprest_warrent SET ledger_id = '".$ledger."' WHERE status = 'A'";

			$completeQry = $completeLineItemQry.";".$queryImprestwarrant;

			$result = mysqli_multi_query($conn,$completeQry);
			echo $result;
		}
	}
?>