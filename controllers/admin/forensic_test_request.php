<?php
/*File Name  :   forensic_test_request.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   31th Dec, 2015
Description  :   This page manages  all the forensic test request*/

session_start();
$visitIdTest ="";
$visitId ="";

/*include config file*/
include 'config.php';

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
}

/*Get user id from session table*/
if(isset($_SESSION['globaluser'])){
    $userId = $_SESSION['globaluser'];
}
else{
	exit();
}

/*operation to save data*/
if ($operation == "save") {
    $data      = json_decode($_POST['data']);
    $patientId = $_POST['patientId'];
    $visitId   = $_POST['visitId'];
    $tblName   = $_POST['tblName'];
    $fkColName = $_POST['fkColName'];
    $testResult ='';    
	$paymentMethod = '';
	$totalCurrentReqBill = $_POST['totalCurrentReqBill'];
    $copayValue = $_POST['copayValue'];
    $copayType = $_POST['copayType'];
    $j=0;

	if(!empty($data)){
		/*query to isert data*/
		$insertQuery = "INSERT INTO " . $tblName . " (ledger_id,gl_account_id,account_payable_id,patient_id,visit_id," . $fkColName . ",request_date,pay_status,test_status,cost,created_on,created_by, status,request_type) ";
		$insertQuery .= "VALUES ";

		/*query to insert bill*/
		$insertBill = "INSERT INTO patient_forensic_bill (ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,forensic_test_id,charge,status,created_by,updated_by,created_on,updated_on,payment_mode)";
		$insertBill .= "VALUES";
		
		$counter = 0;
		$length = count($data);
    	for ($i = 0; $i < $length; $i++) { 
			
			$revisedCost = $data[$i][4];
	        $id = $data[$i][6];
	        $requestType = $data[$i][7];
	        $paymentMethod = $data[$i][8];
	        $restrictBill = $data[$i][9];

			//if insurance mode is insurance then check copay 
			if (strtolower($paymentMethod) == "insurance") {

                if (strtolower($copayType) == "none") {
	                $billStatus = 'paid';           
	            }
	            else {
	                if ($copayValue == 0) {
	                    $billStatus = 'paid';
	                }
	                else{
	                    $billStatus = 'unpaid';
	                }
	            }
            }
            else {
                $billStatus =  "unpaid";
            }

            $query  = "SELECT ledger_id,gl_account_id,account_payable_id FROM forensic_tests WHERE id = " . $id . "";
	        $result = mysqli_query($conn, $query);
	        $ledgerId;
	        $glAccountId;
	        $accountPayable;
	        while ($r = mysqli_fetch_assoc($result)) {
	            $ledgerId = $r['ledger_id'];
	            $glAccountId = $r['gl_account_id'];
	            $accountPayable = $r['account_payable_id'];
	        }

			if ($i > 0) {
				$insertQuery .= ",";
			}
			if ($j > 0 && $requestType == "internal" && $restrictBill == 'notrestrictBill') {
				$insertBill .= ",";
			}

			/*Check size to get if already result sent then show always paid*/
			//if length of incoming data is equal to 9 only at that condition add data
			if ($restrictBill == 'restrictBill'){
				$insertQuery .= "(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . ",".$patientId.",".$visitId.",".$id.",UNIX_TIMESTAMP(),
				'Paid','Pending',".$revisedCost.",UNIX_TIMESTAMP(),'".$userId."','A','".$requestType."')";
			}
			else{
				$insertQuery .= "(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . "," . $patientId . "," . $visitId . "," . $id . ",UNIX_TIMESTAMP(),
				'".$billStatus."','Pending'," . $revisedCost . ",UNIX_TIMESTAMP(),'".$userId."' 
				,'A','".$requestType."')";
			}
			
			//if length of incoming data is equal to 8 only at that condition add data
			if ($requestType == "internal" && $restrictBill == 'notrestrictBill'){
				$insertBill .= "(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . ",'" . $patientId . "','" . $visitId . "','" . $id . "',
				'" . $revisedCost . "','".$billStatus."','" . $userId . "','" . $userId . "',
				UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$paymentMethod."')";
				$j++;
			}			        
		}
		
		if (strlen($insertBill) > 170) {
        	$joinQry = $insertQuery. ";" .$insertBill;
	    }
	    else{
	        $joinQry = $insertQuery;
	    }
		$result = mysqli_multi_query($conn,rtrim($joinQry,','));
		if ($result == 1) {
			echo $result;
		}
		else{
			echo "0";
		}
	}
}

/*operation to count total no. of request for old test*/
if ($operation == "search") {
    $patientId = $_POST['patientId'];
    $visitId   = $_POST['visitId'];
    $tblName   = $_POST['tblName'];
    
    $searchQuery  = "SELECT COUNT(*) as total FROM " . $tblName . " WHERE patient_id = " . $patientId . " AND visit_id = " . $visitId;
    $searchResult = mysqli_query($conn, $searchQuery);
    
    $data = mysqli_fetch_assoc($searchResult);
    echo $data['total'];
}

/*operation to show only those forensic tests for which patient test result sent*/
if ($operation == "showForensicTest") {
	
	if (isset($_POST['visitIdTest'])) {
		$visitIdTest = $_POST["visitIdTest"];
	}
    $query  = "SELECT forensic_tests.id,forensic_tests.name,forensic_tests.fee FROM forensic_tests WHERE forensic_tests.id NOT IN 
        (SELECT forensic_test_request.forensic_test_id FROM forensic_test_request WHERE forensic_test_request.visit_id ='".$visitIdTest."' 
        AND forensic_test_request.status = 'A' AND forensic_test_request.test_status !='resultsent') AND forensic_tests.status ='A' ORDER BY name";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
?>