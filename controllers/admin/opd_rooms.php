<?php

/*File Name  :   opd_rooms.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page  manages OPD Rooms */
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
$operation  = "";
$department = "";
$date       = new DateTime();

/*include config file*/
include 'config.php';

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
} //nothing

/*operation to show OPD rooms*/
if ($operation == "showOPDRooms") {
        /*here room availablity means that is for opd room*/
		$query  = "SELECT ward_room.number as opd_room, ward_room.id from ward_room
            where ward_room.other = 'Y' AND  ward_room.`status` = 'A' ORDER BY ward_room.number";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

/*operation to show consultant*/
if ($operation == "showConsultants") {
    $query  = "SELECT u.id,u.first_name from users AS u 
		Inner Join staff_type  AS s On u.staff_type_id = s.id 
		WHERE s.status='A' And u.`status`='A' AND s.`type` = 'Consultant' ORDER BY first_name";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

/*operation to show department*/
if ($operation == "showDepartment") {
    $query  = "Select id,department FROM department WHERE status='A' ORDER BY department";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

/*operation to save data*/
if ($operation == "save") {
    if (isset($_POST['OPDRooms'])) {
        $OPDRooms = $_POST['OPDRooms'];
    }
    if (isset($_POST['consultant'])) {
        $consultant = $_POST['consultant'];
    }
    if (isset($_POST['department'])) {
        $department = $_POST['department'];
    }
	
	/*check that consultant is booked or not.*/
	$sqlCount    = "SELECT * from opd_rooms where (room_id != '".$OPDRooms."' AND consultant_id='" . $consultant . "' AND status='A')";
    $resultCount = mysqli_query($conn, $sqlCount);
    $rowCount  = mysqli_num_rows($resultCount);
	if($rowCount == 0){
		/*check that room id for that opd room and dept. id exist or not*/
		$sqlSelect    = "SELECT room_id,consultant_id,department_id from opd_rooms where (room_id = '".$OPDRooms."' AND consultant_id='" . $consultant . "' AND status='A')  AND department_id = '" . $department . "' ";
		$resultSelect = mysqli_query($conn, $sqlSelect);
		$rows_count   = mysqli_num_rows($resultSelect);
		
		if ($rows_count < 1) {
			
			$sql    = "INSERT INTO opd_rooms(room_id,department_id,consultant_id,created_on,updated_on,created_by,updated_by,status) VALUES('" . $OPDRooms . "','" . $department . "','" . $consultant . "',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."','A')";
			$result = mysqli_query($conn, $sql);
			echo $result;
		} else {
			echo "0";
		}
	}
	else{
		echo "";
	}
}

/*operation to show active data*/
if ($operation == "show") {    
    $query        = "SELECT opd_rooms.id,ward_room.id as room_id,ward_room.number as number,  users.id as users_id,users.first_name ,department.id as department_id,  department.department from opd_rooms
        LEFT JOIN users ON opd_rooms.consultant_id = users.id
        LEFT JOIN department ON opd_rooms.department_id = department.id
        LEFT JOIN ward_room ON opd_rooms.room_id = ward_room.id
        WHERE opd_rooms.status = 'A' And department.status = 'A' 
        And users.status = 'A' AND ward_room.`status` = 'A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result); //count total no. of records
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/*operation to show inactive data*/
if ($operation == "showInActive") {
    $query        = "SELECT opd_rooms.id,ward_room.id as room_id,ward_room.number as number,  users.id as users_id,users.first_name ,department.id as department_id,  department.department from opd_rooms
        LEFT JOIN users ON opd_rooms.consultant_id = users.id
        LEFT JOIN department ON opd_rooms.department_id = department.id
        LEFT JOIN ward_room ON opd_rooms.room_id = ward_room.id
        WHERE opd_rooms.status = 'I' OR department.status = 'I' 
        OR users.status = 'I' OR ward_room.`status` = 'I'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/*operation to delete data*/
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE opd_rooms SET status= 'I' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo "Delete Successfully!!!";
}

/*operation to restore data*/
if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $sql    = "UPDATE opd_rooms SET status= 'A' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    
    if ($result == 1) {
        
        //check status of room type where type = opd room
        $sqlSelect    = "SELECT status from room_type WHERE type = 'OPD Rooms' AND status ='A'";
        $resultSelect = mysqli_query($conn, $sqlSelect);
        $rows_count   = mysqli_num_rows($resultSelect);
        if ($rows_count < 1) {
            echo "It can not be restore!!!";
        } else {
            echo "Restored Successfully!!!";
        }
    } else {
        echo "It can not be restore!!!";
    }
}

/*operation to update data*/
if ($operation == "update") {

    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $OPDRooms = $_POST['OPDRooms'];
    $consultant = $_POST['consultant'];
    $department = $_POST['department'];

	/*check that consultant is booked or not.*/
	$sqlCount    = "SELECT * from opd_rooms where (room_id != '".$OPDRooms."' AND consultant_id='" . $consultant . "') ";
    $resultCount = mysqli_query($conn, $sqlCount);
    $rowCount  = mysqli_num_rows($resultCount);
	if($rowCount == 0){
		/*check that room id for that opd rooms, consultant id and id exist or not*/
		$sqlSelect    = "SELECT room_id,consultant_id,department_id from opd_rooms 
			where (room_id = '".$OPDRooms."' AND consultant_id='" . $consultant . "')  
			AND department_id = '" . $department . "' AND id !='".$id."'";
		$resultSelect = mysqli_query($conn, $sqlSelect);
		$rows_count   = mysqli_num_rows($resultSelect);
		if ($rows_count < 1) {
			
			$sql    = "UPDATE opd_rooms SET room_id= '" . $OPDRooms . "',consultant_id= '" . $consultant . "',department_id= '" . $department . "',updated_on=UNIX_TIMESTAMP(),updated_by='".$userId."' WHERE  id = '" . $id . "'";
			$result = mysqli_query($conn, $sql);
			echo $result;
		} else {
			echo "0";
		}
	}
	else{
		echo "";
	}
}
?>