<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	
	include 'config.php';
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}	
	
	//Operation to load staff Types
	if($operation == "showStaffType")			
	{
		$sql = "SELECT id,type FROM staff_type WHERE  status = 'A' ORDER BY type";
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		echo json_encode($rows);	
	}
	
	//Operation to load staff name 
	if($operation == "showStaffName")			
	{
		if (isset($_POST['staffTypeId'])) {		
			$staffTypeId = $_POST["staffTypeId"];
		}
		
		$sql = "SELECT id,CONCAT(first_name,' ',last_name) AS name FROM users WHERE  staff_type_id=".$staffTypeId." ORDER BY first_name";
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		echo json_encode($rows);
	}
	
	//Operation to load staff information 
	if($operation == "showStaffinformation")			
	{
		if (isset($_POST['userId'])) {		
			$userId = $_POST["userId"];
		}
		
		$sql = "SELECT u.address,u.mobile,u.email_id,d.designation FROM users AS u 
			LEFT JOIN designation AS d ON d.id = u.designation WHERE  u.id=".$userId."";
		$result=mysqli_query($conn,$sql);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		echo json_encode($rows);
	}
	
	if ($operation == "saveRepetitive") {
		if (isset($_POST['staffTypeId'])) {
			$staffTypeId = $_POST["staffTypeId"];
		}
		if (isset($_POST['staffId'])) {
			$staffId = $_POST["staffId"];
		}
		 
		if($staffId == "-1") {
			$sqlSelect = "SELECT id from users WHERE staff_type_id = '".$staffTypeId."' AND status = 'A'";
			$resultQuery = mysqli_query($conn,$sqlSelect);
			while ($r = mysqli_fetch_assoc($resultQuery)) {
				//echo $r['id'];
				$sqlDeleteSlots = "DELETE FROM staff_days_schedule_slots WHERE schedule_id 
				IN(SELECT id FROM staff_working_days_schedule WHERE staff_id = '" . $r['id'] . "'  
				AND staff_type_id = '" . $staffTypeId . "' AND is_available = 'available')";
				$resultDeleteslots = mysqli_query($conn, $sqlDeleteSlots);
				
				
				$sqlDeleteSlots = "DELETE FROM staff_working_days_schedule WHERE staff_id = '" . $r['id'] . "' 
					AND staff_type_id = '" . $staffTypeId . "'";
				$resultDeleteslots = mysqli_query($conn, $sqlDeleteSlots);
				
				if (isset($_POST['repetativeSchedule'])) {
					$repetativeSchedule = json_decode($_POST["repetativeSchedule"]);
					$counter = 0;
					foreach ($repetativeSchedule as $value) {
						
						$isAvailable = "";
						$count    = sizeof($value);
						if($count > 0){
							$isAvailable = "available";
						}
						else{
							$isAvailable = "notAvailable";
						}
						$sql = "INSERT INTO staff_working_days_schedule(staff_type_id,staff_id,day,is_available) 
								VALUES('" . $staffTypeId . "','" . $r['id'] . "','" . $counter . "','" . $isAvailable . "')";
						$result             = mysqli_query($conn, $sql);
						$scheduleId = mysqli_insert_id($conn);
						$counter++;
						
						
						foreach ($value as $values) {
							$fromTime    = $values->fromTime;
							$toValue    = $values->toValue;
							$notesValue    = $values->notesValue;
							$sqlDailySchedule = "INSERT INTO staff_days_schedule_slots(schedule_id,from_time,to_time,notes) 
							VALUES('" . $scheduleId . "','" . $fromTime . "','" . $toValue . "','" . $notesValue . "')";
							$resultDailyResult = mysqli_query($conn, $sqlDailySchedule);
							
						}
					}
					echo $result;
				}
			}			
		}
		else {
			$sqlDeleteSlots = "DELETE FROM staff_days_schedule_slots WHERE schedule_id 
				IN(SELECT id FROM staff_working_days_schedule WHERE staff_id = '" . $staffId . "'  
				AND staff_type_id = '" . $staffTypeId . "' AND is_available = 'available')";
			$resultDeleteslots = mysqli_query($conn, $sqlDeleteSlots);
			
			
			$sqlDeleteSlots = "DELETE FROM staff_working_days_schedule WHERE staff_id = '" . $staffId . "' 
				AND staff_type_id = '" . $staffTypeId . "'";
			$resultDeleteslots = mysqli_query($conn, $sqlDeleteSlots);
			
			if (isset($_POST['repetativeSchedule'])) {
				$repetativeSchedule = json_decode($_POST["repetativeSchedule"]);
				$counter = 0;
				foreach ($repetativeSchedule as $value) {
					
					$isAvailable = "";
					$count    = sizeof($value);
					if($count > 0){
						$isAvailable = "available";
					}
					else{
						$isAvailable = "notAvailable";
					}
					$sql = "INSERT INTO staff_working_days_schedule(staff_type_id,staff_id,day,is_available) 
							VALUES('" . $staffTypeId . "','" . $staffId . "','" . $counter . "','" . $isAvailable . "')";
					$result             = mysqli_query($conn, $sql);
					$scheduleId = mysqli_insert_id($conn);
					$counter++;
					
					
					foreach ($value as $values) {
						$fromTime    = $values->fromTime;
						$toValue    = $values->toValue;
						$notesValue    = $values->notesValue;
						$sqlDailySchedule = "INSERT INTO staff_days_schedule_slots(schedule_id,from_time,to_time,notes) 
						VALUES('" . $scheduleId . "','" . $fromTime . "','" . $toValue . "','" . $notesValue . "')";
						$resultDailyResult = mysqli_query($conn, $sqlDailySchedule);
						
					}
				}
				echo $result;
			}
		}
	}
	
	if ($operation == "showRepetitive") {
		if (isset($_POST['staffTypeId'])) {
			$staffTypeId = $_POST["staffTypeId"];
		}
		if (isset($_POST['staffId'])) {
			$staffId = $_POST["staffId"];
		}
		
		$sql = "SELECT sws.id AS staff_weekly_id,sws.day,sws.is_available,wss.* 
				FROM staff_working_days_schedule AS sws
				LEFT JOIN staff_days_schedule_slots AS wss ON wss.schedule_id = sws.id 
				WHERE sws.staff_type_id = '" . $staffTypeId . "' AND sws.staff_id ='" . $staffId . "'";
		$result             = mysqli_query($conn, $sql);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
			$rows[] = $r;
		}
		echo json_encode($rows);		
	}
?>