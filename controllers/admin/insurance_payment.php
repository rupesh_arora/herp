<?php
/*File Name  :   insurance_payment.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   April,2,2016
Description  :   This page manages  all the insurance payment*/

session_start();
$visitIdTest ="";
$visitId ="";

/*include config file*/
include 'config.php';

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
}

/*Get user id from session table*/
if(isset($_SESSION['globaluser'])){
    $userId = $_SESSION['globaluser'];
}
else{
	exit();
}

if ($operation == "checkVisitId") {
	
	$visitId = $_POST['visitId'];
	$sqlSelect = "SELECT visits.id FROM visits 
	    WHERE visits.payment_mode = 'insurance' AND visits.id = '".$visitId."'";

	$result = mysqli_query($conn,$sqlSelect);
	$countVisit = mysqli_num_rows($result);
	if ($countVisit > 0) {
	 	echo "1";
	} 
	else{
		echo "0";
	}

}

if ($operation == "insuranceDetails") {

	$insuranceNumber = $_POST['insuranceNumber'];
	$sqlInsurance = "SELECT insurance_plan.id AS insurance_id,insurance_plan.plan_name AS
	insurance_paln_name,insurance_companies.id AS insurance_company_id,insurance_companies.name AS
	insurance_company_name ,scheme_plans.id AS insurance_scheme_id,scheme_plans.scheme_plan_name,scheme_plans.value,
	copay.copay_type,insurance.patient_id,(SELECT value FROM configuration WHERE
	name = 'patient_prefix') AS patient_prefix,
	CONCAT (patients.first_name,' ',patients.middle_name,' ',patients.last_name) AS patient_name
	FROM insurance
	LEFT JOIN insurance_plan ON insurance_plan.id = insurance.insurance_plan_id
	LEFT JOIN insurance_companies ON insurance_companies.id = insurance.insurance_company_id
	LEFT JOIN scheme_plans ON scheme_plans.id = insurance.insurance_plan_name_id
	LEFT JOIN copay ON copay.id = scheme_plans.copay
	LEFT JOIN patients on patients.id = insurance.patient_id
	WHERE insurance.status= 'A' AND insurance.insurance_number = '".$insuranceNumber."'";

	$result = mysqli_query($conn, $sqlInsurance);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "ShowTableData") {
	$visitId = $_POST['visitId'];

	$forensicBill = "SELECT patient_forensic_bill.id,'forensic' AS tbl_name,patient_forensic_bill.charge AS `revised_cost` ,
	forensic_tests.fee AS actual_cost ,forensic_tests.name FROM patient_forensic_bill 
	LEFT JOIN forensic_tests ON forensic_tests.id = patient_forensic_bill.forensic_test_id
	WHERE patient_forensic_bill.payment_mode ='insurance' AND 
	patient_forensic_bill.visit_id ='".$visitId."' AND patient_forensic_bill.insurance_payment = 'unpaid'";           

	$radiologyBill = "SELECT patient_radiology_bill.id,'radiology' AS tbl_name,patient_radiology_bill.charge AS `revised_cost` ,
	radiology_tests.fee AS 	actual_cost,radiology_tests.name FROM patient_radiology_bill 
	LEFT JOIN radiology_tests ON radiology_tests.id = patient_radiology_bill.radiology_test_id
	WHERE patient_radiology_bill.payment_mode ='insurance' AND 
	patient_radiology_bill.visit_id ='".$visitId."' AND patient_radiology_bill.insurance_payment='unpaid'";

	$serviceBill = "SELECT patient_service_bill.id,'service' AS tbl_name,patient_service_bill.charge AS `revised_cost` ,
	services.cost AS actual_cost,services.name FROM patient_service_bill 
	LEFT JOIN services ON services.id = patient_service_bill.service_id
	WHERE patient_service_bill.payment_mode ='insurance' AND 
	patient_service_bill.visit_id ='".$visitId."' AND patient_service_bill.insurance_payment='unpaid'";

	$procedureBill = "SELECT patient_procedure_bill.id,'procedure' AS tbl_name,patient_procedure_bill.charge AS `revised_cost` ,
	procedures.price AS actual_cost,procedures.name FROM patient_procedure_bill 
	LEFT JOIN procedures ON procedures.id = patient_procedure_bill.procedure_id
	WHERE patient_procedure_bill.payment_mode ='insurance' AND 
	patient_procedure_bill.visit_id ='".$visitId."' AND patient_procedure_bill.insurance_payment='unpaid'";

	$labComponentBill = "SELECT lab_test_request_component.id,'Lab' AS tbl_name,lab_test_request_component.cost AS `revised_cost` ,
	lab_test_component.fee AS actual_cost,lab_test_component.name 
	
	FROM lab_tests
	
	LEFT JOIN lab_test_requests ON lab_test_requests.lab_test_id = lab_tests.id
	LEFT JOIN lab_test_component ON lab_test_component.lab_test_id = lab_tests.id
	LEFT JOIN lab_test_request_component ON lab_test_request_component.lab_component_id = lab_test_component.id  AND lab_test_request_component.lab_test_request_id = lab_test_requests.id 
	WHERE  lab_test_request_component.payment_mode = 'insurance' AND 
	lab_test_request_component.insurance_payment = 'unpaid' AND
	lab_test_requests.visit_id ='".$visitId."'  GROUP BY lab_test_request_component.id ";

	$sqlMedicine    = "SELECT  'Medicine' as tbl_name, p_r.cost AS `revised_cost` ,ppb.id AS id,d.name AS name ,p_r.quantity * d.price AS actual_cost,p_r.quantity AS quantity, 
	p_r.payment_mode from patients_prescription_request AS p_r

	LEFT JOIN drugs AS d ON d.id = p_r.drug_id 
	LEFT JOIN patient_pharmacy_bill AS ppb ON ppb.visit_id = p_r.visit_id  
	LEFT JOIN pharmacy_inventory AS p  ON p.drug_name_id = d.id 
	WHERE p_r.visit_id= '".$visitId."'  AND p.quantity !=  0 AND ppb.insurance_payment = 
	'unpaid' AND p.expiry_date > CURDATE() AND p_r.payment_mode='insurance'
	GROUP BY p_r.id";

	$resultForensicBill = mysqli_query($conn,$forensicBill);
	$resultRadiologyBill = mysqli_query($conn,$radiologyBill);
	$resultServiceBill = mysqli_query($conn,$serviceBill);
	$resultProcedureBill = mysqli_query($conn,$procedureBill);
	$resultLabComponentBill = mysqli_query($conn,$labComponentBill);
	$resultMedicine = mysqli_query($conn, $sqlMedicine);

	$Allrows = array();
	$rowsForensicBill   = array();
    while ($r = mysqli_fetch_assoc($resultForensicBill)) {
        $rowsForensicBill[] = $r;
    }

    $rowsRadiologyBill   = array();
    while ($r = mysqli_fetch_assoc($resultRadiologyBill)) {
        $rowsRadiologyBill[] = $r;
    }

    $rowsServiceBill   = array();
    while ($r = mysqli_fetch_assoc($resultServiceBill)) {
        $rowsServiceBill[] = $r;
    }

    $rowsProcedureBill   = array();
    while ($r = mysqli_fetch_assoc($resultProcedureBill)) {
        $rowsProcedureBill[] = $r;
    }

    $rowsLabComponentBill   = array();
    while ($r = mysqli_fetch_assoc($resultLabComponentBill)) {
        $rowsLabComponentBill[] = $r;
    }
    $rowsMedicineBill   = array();
    while ($r = mysqli_fetch_assoc($resultMedicine)) {
        $rowsMedicineBill[] = $r;
    }


    array_push($Allrows, $rowsForensicBill);
    array_push($Allrows, $rowsRadiologyBill);
    array_push($Allrows, $rowsServiceBill);
    array_push($Allrows, $rowsProcedureBill);
    array_push($Allrows, $rowsLabComponentBill);
    array_push($Allrows, $rowsMedicineBill);

    print json_encode($Allrows);
}

if ($operation =="saveInsurancePaymentdetails") {
	
	$visitId = $_POST['visitId'];
	$patientId = $_POST['patientId'];
	$insuranceNo = $_POST['insuranceNo'];
	$insuranceCompanyId = $_POST['insuranceCompanyId'];
	$insuranceSchemeId = $_POST['insuranceSchemeId'];
	$insurancePlanId = $_POST['insurancePlanId'];
	$date = $_POST['date'];
	$bill = $_POST['bill'];
	$radiologyArray = json_decode($_POST['radiologyArray']);
	$forensicArray = json_decode($_POST['forensicArray']);
	$medicineArray = json_decode($_POST['medicineArray']);
	$serviceArray = json_decode($_POST['serviceArray']);
	$procedureArray = json_decode($_POST['procedureArray']);
	$labArray = json_decode($_POST['labArray']);
	$completeQry = '';

	if (!empty($radiologyArray)) {

		$updateRadiologyBill = "UPDATE patient_radiology_bill SET insurance_payment = 'paid',
		updated_by ='".$userId."',updated_on = UNIX_TIMESTAMP() WHERE ";
		$subUpdateRadiologyBill = '';
		$countRadiology = 0;
		foreach ($radiologyArray as $key => $value) {			
			
			if ($countRadiology == 0) {
				$subUpdateRadiologyBill.= "id = '".$value."'";
			}
			else {
				$subUpdateRadiologyBill.= "OR id = '".$value."'";
			}
			$countRadiology++;
		}

		$completeRadiologyBill =  $updateRadiologyBill.$subUpdateRadiologyBill;

		$completeQry.= $completeRadiologyBill.';';
	}

	if (!empty($forensicArray)) {
		

		$updateForensicBill = "UPDATE patient_forensic_bill SET insurance_payment = 'paid',
		updated_by ='".$userId."',updated_on = UNIX_TIMESTAMP() WHERE ";
		$subUpdateForensicBill = '';
		$countForensic = 0;
		foreach ($forensicArray as $key => $value) {			
			
			if ($countForensic == 0) {
				$subUpdateForensicBill.= "id = '".$value."'";
			}
			else {
				$subUpdateForensicBill.= "OR id = '".$value."'";
			}
			$countForensic++;
		}

		$completeForensicBill =  $updateForensicBill.$subUpdateForensicBill;

		$completeQry.= $completeForensicBill.';';
	}

	if (!empty($serviceArray)) {
		
		$updateServiceBill = "UPDATE patient_service_bill SET insurance_payment = 'paid',
		updated_by ='".$userId."',updated_on = UNIX_TIMESTAMP() WHERE ";
		$subUpdateServiceBill = '';
		$countService = 0;
		foreach ($serviceArray as $key => $value) {			
			
			if ($countService == 0) {
				$subUpdateServiceBill.= "id = '".$value."'";
			}
			else {
				$subUpdateServiceBill.= "OR id = '".$value."'";
			}
			$countService++;
		}

		$completeServiceBill =  $updateServiceBill.$subUpdateServiceBill;

		$completeQry.= $completeServiceBill.';';
	}

	if (!empty($medicineArray)) {
		
		$updateMedicineBill = "UPDATE patient_pharmacy_bill SET insurance_payment = 'paid',
		updated_by ='".$userId."',updated_on = UNIX_TIMESTAMP() WHERE ";
		$subUpdateMedicineBill = '';
		$countService = 0;
		foreach ($medicineArray as $key => $value) {			
			
			if ($countService == 0) {
				$subUpdateMedicineBill.= "id = '".$value."'";
			}
			$countService++;
		}

		$completeMedicineBill =  $updateMedicineBill.$subUpdateMedicineBill;

		$completeQry.= $completeMedicineBill.';';
	}

	if (!empty($procedureArray)) {
		
		$updateProcedureBill = "UPDATE patient_procedure_bill SET insurance_payment = 'paid',
		updated_by ='".$userId."',updated_on = UNIX_TIMESTAMP() WHERE ";
		$subUpdateProcedureBill = '';
		$countprocedure = 0;
		foreach ($procedureArray as $key => $value) {			
			
			if ($countprocedure == 0) {
				$subUpdateProcedureBill.= "id = '".$value."'";
			}
			else {
				$subUpdateProcedureBill.= "OR id = '".$value."'";
			}
			$countprocedure++;
		}

		$completeProcedureBill =  $updateProcedureBill.$subUpdateProcedureBill;

		$completeQry.= $completeProcedureBill.';';
	}

	if (!empty($labArray)) {
		
		$updateLabComponent = "UPDATE lab_test_request_component SET insurance_payment = 'paid',
		created_by ='".$userId."',created_on = UNIX_TIMESTAMP() WHERE ";
		$subUpdateLabComponent = '';
		$countLabComponent = 0;
		foreach ($labArray as $key => $value) {			
			
			if ($countLabComponent == 0) {
				$subUpdateLabComponent.= "id = '".$value."'";
			}
			else {
				$subUpdateLabComponent.= "OR id = '".$value."'";
			}
			$countLabComponent++;
		}

		$completeLabComponent =  $updateLabComponent.$subUpdateLabComponent;

		$completeQry.= $completeLabComponent.';';
	}	

	$insert = "INSERT INTO insurance_payment (insurance_no,patient_id,visit_id,insurance_company,insurance_plan,insurance_scheme_plan,total_bill,created_on,created_by,date)
		VALUES('".$insuranceNo."','".$patientId."','".$visitId."','".$insuranceCompanyId."',
		'".$insurancePlanId."','".$insuranceSchemeId."','".$bill."',UNIX_TIMESTAMP(),'".$userId."','".$date."')";

	$completeQry.= $insert.';';
	$result = mysqli_multi_query($conn,$completeQry);
	echo $result;
}
if ($operation =="showNotification") {
	$select  = "SELECT message,DATE_FORMAT(FROM_UNIXTIME(created_on), '%Y-%m-%d %h:%i %p') AS `date` from notification WHERE notification_staff_id ='".$userId."'";

	$result = mysqli_query($conn,$select);

	$rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }

    print json_encode($rows);
}
?>