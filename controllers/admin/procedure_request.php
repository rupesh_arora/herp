<?php
/*File Name  :   procedure_request.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   31th Dec, 2015
Description  :   This page manages all the procedure request in consultant module*/

session_start(); //start session
$visitIdProcedure="";
$visitId="";

/*include config file*/
include 'config.php';

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
}

/*Get user id from session table*/
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

/*operation to save data*/
if ($operation == "save") {
    
    $data      = json_decode($_POST['data']);
    $patientId = $_POST['patientId'];
    $visitId   = $_POST['visitId'];
    $tblName   = $_POST['tblName'];
    $fkColName = $_POST['fkColName'];
    $totalCurrentReqBill = $_POST['totalCurrentReqBill'];
    $copayValue = $_POST['copayValue'];
    $copayType = $_POST['copayType'];
    
    /*query to isert data*/
    $insertQuery = "INSERT INTO " . $tblName . " (ledger_id,gl_account_id,account_payable_id,patient_id,visit_id," . $fkColName . ",request_date,pay_status,test_status,cost, status,request_type) ";
    $insertQuery .= "VALUES ";
    
    /*query to insert bill*/
    $insertBill = "INSERT INTO patient_procedure_bill (ledger_id,gl_account_id,account_payable_id,patient_id,visit_id,procedure_id, charge,status,
				created_by,updated_by,created_on,updated_on,payment_mode)";
    $insertBill .= "VALUES";
    
    $counter = 0;
    $length = count($data);
    for ($i = 0; $i < $length; $i++) {

        $revisedCost = $data[$i][4];
        $id = $data[$i][6];
        $requestType = $data[$i][7];
        $paymentMethod = $data[$i][8];
       

        //if insurance mode is insurance then check copay 
        if (strtolower($paymentMethod) == "insurance") {

            if (strtolower($copayType) == "none") {
                $billStatus = 'paid';           
            }
            else {
                if ($copayValue == 0) {
                    $billStatus = 'paid';
                }
                else{
                    $billStatus = 'unpaid';
                }
            }
        }
        else {
            $billStatus =  "unpaid";
        }

        $query  = "SELECT ledger_id,gl_account_id,account_payable_id FROM procedures WHERE id = " . $id . "";
        $result = mysqli_query($conn, $query);
        $ledgerId;
        $glAccountId;
        $accountPayable;
        while ($r = mysqli_fetch_assoc($result)) {
            $ledgerId = $r['ledger_id'];
            $glAccountId = $r['gl_account_id'];
            $accountPayable = $r['account_payable_id'];
        }
        
        if ($i > 0) {
            $insertQuery .= ",";
            $insertBill .= ",";
        }
        
        $insertQuery .= "(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . "," . $patientId . "," . $visitId . "," . $id . ",UNIX_TIMESTAMP(),
            '".$billStatus."','Pending'," . $revisedCost . ",'A','". $requestType ."')";
        
        $insertBill .= "(" . $ledgerId . "," . $glAccountId . "," . $accountPayable . ",'".$patientId."','".$visitId."','".$id."','".$revisedCost."',
            '".$billStatus."','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),
            '".$paymentMethod."')";        
    }
    
    $joinQry = $insertQuery . ";" . $insertBill;
    $result  = mysqli_multi_query($conn, $joinQry);
    if ($result == 1) {
        echo $result;
    } else {
        echo "0";
    }
}

/*operation to show only those procedure for which patient test result sent*/
if ($operation == "showProcedure") {
    
    if (isset($_POST['visitIdProcedure'])) {
		$visitIdProcedure = $_POST["visitIdProcedure"];
	}
    $query   = "SELECT id,CONCAT(name, '  ','(',code,')') AS name, price FROM procedures WHERE
    procedures.id NOT IN (SELECT procedures_requests.procedure_id FROM procedures_requests 
    WHERE procedures_requests.visit_id ='" . $visitIdProcedure . "' AND procedures_requests.status = 'A' AND procedures_requests.test_status !='done') AND procedures.status ='A'   ORDER BY name";
    $result  = mysqli_query($conn, $query);
    $rows    = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

/*operation to count total no. of request for old test*/
if ($operation == "search") {
    
    $patientId = $_POST['patientId'];
    $visitId   = $_POST['visitId'];
    $tblName   = $_POST['tblName'];
    
    $searchQuery  = "SELECT COUNT(*) as total FROM " . $tblName . " WHERE patient_id = " . $patientId . " AND visit_id = " . $visitId;
    $searchResult = mysqli_query($conn, $searchQuery);
    $data         = mysqli_fetch_assoc($searchResult);
    echo $data['total'];
}
?>