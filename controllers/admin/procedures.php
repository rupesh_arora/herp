<?php
	session_start(); // session start
 	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
    $operation = "";
    $id = "";
    $name = "";
	$code ="";
	$unitPrice ="";
	$description = "";
	$shortName ="";
	$createdate = date("Y/m/d") ;
	$date = new DateTime();
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation=$_POST["operation"];
	}
	else if(isset($_GET["operation"])){
		$operation=$_GET["operation"];
	}
	else{}
	
	if (isset($_FILES["file"]["type"])) // use for import data from csv
	 {
		$temporary       = explode(".", $_FILES["file"]["name"]);
		$file_extension = end($temporary);
		/*if ($file_extension == "csv") {
			if ($_FILES["file"]["error"] > 0) {
				echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
			} 
			else {
				if($_FILES["file"]["size"] > 0) {
				
					$filename = $_FILES["file"]["tmp_name"];
					$file = fopen($filename, "r");
					$countCSV = 0;
					$subInsertValues = '';
					$sqlInsert = "INSERT IGNORE INTO procedures(code,name,short_name,price,description,created_on,updated_on) VALUES ";

					while (($emapData = fgetcsv($file, 10000, ",")) != FALSE) {
						
						if (sizeof($emapData) != 5) {
						 	echo "You must have five column in your file i.e procedure code, procedure name, short name, price and description";
						 	exit();
						}

						//check whether header name same or not
						if ($countCSV == 0) {
							if(strtolower($emapData[0]) !="procedure code") {
								echo "First column should be procedure code";
								exit();
							}
							if(strtolower($emapData[1]) !="procedure name") {
								echo "Second column should be procedure name";
								exit();
							}
							if(strtolower($emapData[2]) !="short name") {
								echo "Second column should be short name";
								exit();
							}	
							if(strtolower($emapData[3]) !="price") {
								echo "Second column should be price";
								exit();
							}	
							if(strtolower($emapData[4]) !="description") {
								echo "Second column should be description";
								exit();
							}						
						}

						//data can't be null in first row else row it may be null
						if ($countCSV == 1) {
							if (empty($emapData[0]) || empty($emapData[1]) || empty($emapData[3]) ) {
								echo "Data can't be null in code ,name and price column";
								exit();
							}
						}

						if ($countCSV > 0) {

							//check whether data not be null in the file
							if(($emapData[0]) != "" && ($emapData[1]) != "" && ($emapData[3]) != "") {

								if (is_float($emapData[3]) || is_numeric($emapData[3])){
									if ($countCSV >1) {
										$subInsertValues.= ',';
									}						
									$subInsertValues.= "('".$emapData[0]."','".$emapData[1]."','".$emapData[2]."','".$emapData[3]."','".$emapData[4]."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";
								}
								else{
									echo "Price should be number";
									exit();
								}								
							}
						}			
						$countCSV++;						
					}

					$sqlInsert = $sqlInsert.$subInsertValues;
					$result = mysqli_query($conn,$sqlInsert);

				
					if($result) {
						echo $result;
					}
					else{
						echo "";
					}
					fclose($file);
				}
			} 	
		}
		else if($file_extension == "xls"){
			ini_set("display_errors",1);
			$filename = $_FILES['file']['tmp_name'];
			require_once 'excel_reader2.php';
			$data = new Spreadsheet_Excel_Reader($filename);
			$countSheets = count($data->sheets);
			for($i=0;$i<$countSheets;$i++) // Loop to get all sheets in a file.
			{ 
				$countSheetsCells =0;
				if(isset($data->sheets[$i]['cells'])){
					$countSheetsCells = count($data->sheets[$i]['cells']);
				}
				if($countSheetsCells > 0 && $countSheetsCells) // checking sheet not empty
				{
					$sql = "INSERT IGNORE INTO procedures(code,name,short_name,price,description,created_on,updated_on) VALUES ";
					$countXls = 0;				
					$subInsert = '';
					for($j=1;$j<=$countSheetsCells;$j++) {// loop used to get each row of the sheet
					 
						
					  	if (sizeof($data->sheets[$i]['cells'][$j]) !="5") {
					  		echo "You must have five column in your file i.e procedure code, procedure name, short name, price and description";
						 	exit();
					  	}
					  	
					  	
					  	$indexOfExcelFile = array_keys($data->sheets[$i]['cells'][$j]);

						
						if ($countXls == 0) {
							if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[0]]) !="procedure code"){
								echo "First column should be procedure code";
								exit();
							}
							if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[1]]) !="procedure name"){
								echo "Second column should be procedure name";
								exit();
							}
							if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[2]]) !="short name"){
								echo "Third column should be short name";
								exit();
							}
							if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[3]]) !="price"){
								echo "Fourth column should be price";
								exit();
							}
							if(strtolower($data->sheets[$i]['cells'][$j][$indexOfExcelFile[4]]) !="description"){
								echo "Fifth column should be description";
								exit();
							}
						}
						
						$code = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[0]];
					
						$name = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[1]];

						$s_name = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[2]];

						$price = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[3]];
					
						$description = $data->sheets[$i]['cells'][$j][$indexOfExcelFile[4]];
						
						//data can't be null in the very first row of file
						if ($countXls == 1) {
							if (empty($code) || empty($name) || empty($price)) {
								echo "Data can't be null in code ,name and price column";
								exit();
							}
						}

						if ($countXls>0) {
							
							//check whether data not be null in file
							if($code != "" && $name != '' && $price != ''){

								if (is_float($price) || is_numeric($price)){
									if ($countXls >1) {
										$subInsert .=',';
									}					 	
								 	$subInsert .=" ('$code','$name','$s_name','$price','$description',
								 	UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";
								}
								else{
									echo "Price should be number";
									exit();
								}
							}
						}
						$countXls++;		
					}

					$sql = $sql.$subInsert;
					$result = mysqli_query($conn,$sql);

					if($result)
					{
						echo $result;
					}
					else{
						echo "";
					}
				}
			}		
		}*/
		if($file_extension == "xlsx" || $file_extension == "csv" ||$file_extension == "xls"){
			// get file name
			$filename = $_FILES['file']['tmp_name'];
			
			//import php script
			require_once 'PHPExcel.php';
			
			// 
			try {
				$inputFileType = PHPExcel_IOFactory::identify($filename);  // get type of file name
				$objReader = PHPExcel_IOFactory::createReader($inputFileType); //create object of read file
				$objPHPExcel = $objReader->load($filename);  //load excel into the object
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($filename, PATHINFO_BASENAME) 
				. '": ' . $e->getMessage());
			}

			//  Get worksheet dimensions
			$sheet = $objPHPExcel->getSheet(0); // get sheet 1 of excel file
			$highestRow = $sheet->getHighestRow(); // get heighest row
			$highestColumn = $sheet->getHighestColumn(); // get heighest column
			
			$headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE); // get heading 

			if (sizeof($headings[0]) < 5) {
				echo "You must have five column in your file i.e procedure code, procedure name, short name, price and description";
				exit();
			}
			
			//reset($headings[0]) it is the first column
			if(trim(strtolower($headings[0][0])) !="procedure code"){
				echo "First column should be procedure code";
				exit();
			}
			//end($headings[0]) it is the last column
			if(trim(strtolower($headings[0][1])) !="procedure name"){
				echo "Second column should be procedure name";
				exit();
			}
			if(trim(strtolower($headings[0][2])) !="short name"){
				echo "Third column should be short name";
				exit();
			}
			if(trim(strtolower($headings[0][3]))!="price"){
				echo "Fourth column should be price";
				exit();
			}
			if(trim(strtolower($headings[0][4])) !="description"){
				echo "Fifth column should be description";
				exit();
			}
			$sql = "INSERT IGNORE INTO procedures(code,name,short_name,price,description,created_on,updated_on,created_by,updated_by) VALUES ";
			$countXlsx = 0;
			$subInsertXlsx = '';
											
			for ($row = 2; $row <= $highestRow; $row++) {
				//  Read a row of data into an array
				$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL, TRUE, FALSE);
				
				// combine data in array accoding to heading
				$rowData[0] = array_combine($headings[0], $rowData[0]);
				//var_dump($rowData[0]);
				
				// fetch value from row
				$key  = array_keys($rowData[0]);//getting the ondex name

				$code =  trim(mysql_real_escape_string($rowData[0][$key[0]]));
				$name = trim(mysql_real_escape_string($rowData[0][$key[1]]));
				$s_name = trim(mysql_real_escape_string($rowData[0][$key[2]]));
				$price = trim(mysql_real_escape_string($rowData[0][$key[3]]));
				$description = trim(mysql_real_escape_string($rowData[0][$key[4]]));

				// data shouldn't be null in the very first row of file
				if ($countXlsx == 0) {

					if (empty($code) || empty($name)|| empty($price)) {
						echo "Data can't be null in code ,name and price column";
						exit();
					}
				}
				if ($code != "" && $name != "" && $price != "") {

					//check price should be float or integer 
					if (is_float($price) || is_numeric($price)){

						if ($countXlsx > 0) {
							$subInsertXlsx.=',';
						}
						$subInsertXlsx.="('$code','$name','$s_name','$price','$description',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
					}
					else{
						echo "Price should be number";
						exit();
					}						
				}				
					
				$countXlsx++;		
			}
			$sql = $sql.$subInsertXlsx;
			$result = mysqli_query($conn,$sql);
			if($result)
			{
				echo $result;
			}
			else{
				echo "";
			}
		}
		else{
			echo "invalid file";
		}
	}
	//operation for save the data
	if($operation == "save")			
	{
		if (isset($_POST['name'])) {
		$name=$_POST['name'];
		}
		if (isset($_POST['code'])) {
		$code=$_POST['code'];
		}
		if (isset($_POST['ledger'])) {
			$ledger=$_POST['ledger'];
		}
		if (isset($_POST['glAccount'])) {
			$glAccount=$_POST['glAccount'];
		}
		if (isset($_POST['accountPayable'])) {
			$accountPayable=$_POST['accountPayable'];
		}
		if (isset($_POST['shortName'])) {
		$shortName=$_POST['shortName'];
		}
		if (isset($_POST['unitPrice'])) {
		$unitPrice=$_POST['unitPrice'];
		}		
		$description=$_POST['description'];

		$sql = "INSERT INTO procedures(code,ledger_id,gl_account_id,account_payable_id,name,short_name,price,description,created_on,updated_on,created_by,updated_by) 
		VALUES('".$code."','".$ledger."','".$glAccount."','".$accountPayable."','".$name."','".$shortName."','".$unitPrice."','".$description."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
		$result= mysqli_query($conn,$sql);  
		$id = mysqli_insert_id($conn);

		$query = "INSERT INTO `insurance_revised_cost`(`insurance_company_id`, `scheme_plan_id`, `scheme_name_id`,`item_name`, `service_type`, `item_id`, 
        `actual_cost`,`revised_cost`, `created_on`, `created_by`, `status`)
         SELECT  
        insurance_company_id ,scheme_plan_id , scheme_name_id
        ,'".$name."' AS item_name ,'procedure' AS service_type , '".$id."' As item_id, '".$unitPrice."' AS
        actual_cots, '".$unitPrice."' AS revised_cost,UNIX_TIMESTAMP() AS created_on ,'".$userId."' 
        AS created_by, 'A' AS `status`   FROM insurance_revised_cost GROUP BY scheme_name_id ";
        $result = mysqli_query($conn, $query);
    	echo $result;
	} 
	
    //operation for show active data	
	if ($operation == "show"){
		
		$query= "select * from procedures where status='A'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);	
	}
	
	// opertaion form update
	if ($operation == "update")
	{		
		if (isset($_POST['name'])) {
		$name=$_POST['name'];
		}
		if (isset($_POST['code'])) {
		$code=$_POST['code'];
		}
		if (isset($_POST['ledger'])) {
			$ledger=$_POST['ledger'];
		}
		if (isset($_POST['glAccount'])) {
			$glAccount=$_POST['glAccount'];
		}
		if (isset($_POST['accountPayable'])) {
			$accountPayable=$_POST['accountPayable'];
		}
		if (isset($_POST['shortName'])) {
		$shortName=$_POST['shortName'];
		}
		if (isset($_POST['unitPrice'])) {
		$unitPrice=$_POST['unitPrice'];
		}		
		$description=$_POST['description'];

		if (isset($_POST['id'])) {
		$id = $_POST['id'];
		}
		
		$sql = "update procedures set code= '".$code."',gl_account_id = '".$glAccount."', ledger_id= '".$ledger."',account_payable_id= '".$accountPayable."' , name = '".$name."',short_name='".$shortName."', price=".$unitPrice.",description='".$description."',updated_on=UNIX_TIMESTAMP(),updated_by='".$userId."' where id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//  operation for delete
	if ($operation == "delete")
	{
		if (isset($_POST['id'])) {
		$id = $_POST['id'];
		}
		
		$sql = "UPDATE procedures SET status= 'I' where id = '".$id."' ";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
	
	//When checked box is check
	if ($operation == "checked") {
		
		$query= "select * from procedures where status='I'";
		$result=mysqli_query($conn,$query);
		$totalrecords = mysqli_num_rows($result);
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		 $rows[] = $r;
		}
		 
		$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
		echo json_encode($json);	
	}
	
	if($operation == "restore")			
	{
		if (isset($_POST['id'])) {
			$id=$_POST['id'];
		}
		$sql = "UPDATE procedures SET status= 'A'  WHERE  id = '".$id."'";
		$result= mysqli_query($conn,$sql);  
		echo $result;
	}
?>