<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "showAssetClass") { // show insurance company type
	    $query = "SELECT id,asset_class_name FROM asset_class WHERE status = 'A'";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "saveAssetSubClass") {

		$subClass = $_POST['subClass'];
		$className = $_POST['className'];
		$description = $_POST['description'];

		$selLedger = "SELECT sub_class_name FROM asset_subclass WHERE sub_class_name ='".$subClass."' AND class = '".$className."' AND status = 'A'";
		$checkLedger = mysqli_query($conn,$selLedger);
		$countLedger = mysqli_num_rows($checkLedger);

			if ($countLedger == "0") {

			 $query = "INSERT INTO asset_subclass(sub_class_name,class,description,created_by,updated_by,created_on,updated_on) VALUES('".$subClass."','".$className."','".$description."','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";

			$result = mysqli_query($conn, $query);
			echo $result;
		}
		else {
			echo "0";
		}
	}

	if ($operation == "show") { // show active data
    
    	$query = "SELECT asset_subclass.id AS id,sub_class_name,class,asset_subclass.description AS description,asset_class.asset_class_name AS asset_class_name FROM asset_subclass
			LEFT JOIN asset_class ON asset_class.id = asset_subclass.class WHERE asset_subclass.`status` = 'A'";
	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}

	if ($operation == "checked") {
	    
	    $query = "SELECT asset_subclass.id AS id,sub_class_name,class,asset_class.description AS description,asset_class.asset_class_name AS asset_class_name FROM asset_subclass
			LEFT JOIN asset_class ON asset_class.id = asset_subclass.class WHERE asset_subclass.`status` = 'I'";
	    
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	if ($operation == "update") // update data
	{
	    $subClass = $_POST['subClass'];
	    $className = $_POST['className'];
		$description = $_POST['description'];
	    $id = $_POST['id'];

    	$selLedger = "SELECT sub_class_name FROM asset_subclass WHERE sub_class_name ='".$subClass."' AND class = '".$className."' AND status = 'A' AND id != '".$id."'";
		$checkLedger = mysqli_query($conn,$selLedger);
		$countLedger = mysqli_num_rows($checkLedger);

		if ($countLedger == "0") {
	
			$sql    = "UPDATE asset_subclass set sub_class_name = '".$subClass."',class = '".$className."',description= '".$description."',updated_on = UNIX_TIMESTAMP() ,updated_by = '".$userId."' WHERE id = '".$id."' ";

			$result = mysqli_query($conn, $sql);
			echo $result;
		}
		else{
			echo "0";
		}

	}

	if ($operation == "delete") {
        $id = $_POST['id'];

		$sql    = "UPDATE asset_subclass SET status= 'I' where id = '" . $id . "'";
	    $result = mysqli_query($conn, $sql);
	    echo $result;	    
	}

	if ($operation == "restore") {// for restore    
        $id = $_POST['id'];
        $subClassName = $_POST['subClassName'];
        $ClassNameId = $_POST['ClassNameId'];
        $selLedger = "SELECT sub_class_name FROM asset_subclass WHERE sub_class_name ='".$subClassName."' AND class = '".$ClassNameId."' AND status = 'A' AND id != '".$id."'";
		$checkLedger = mysqli_query($conn,$selLedger);
		$countLedger = mysqli_num_rows($checkLedger);

		if ($countLedger == "0") {

			 $sql    = "UPDATE asset_subclass SET status= 'A'  WHERE  id = '" . $id . "'";
			$result = mysqli_query($conn, $sql);
		    echo $result;
		}
		else{
			echo "0";
		}
	}
?>