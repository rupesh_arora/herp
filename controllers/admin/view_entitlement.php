<?php
/*File Name  :   bed.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page manages AND add beds*/

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

/*include config file*/
include 'config.php';


/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

/*operation to show leave type*/
if ($operation == "showBindLeavePeriod") {
    
    $query  = "SELECT leave_period.*,CONCAT(leave_period.from_date,' - ',leave_period.to_date) As period FROM leave_period WHERE leave_period.status='A' ORDER BY leave_period.id ASC";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

// search patient opearion
if ($operation == "SearchEntitlements") {
    $staffId = "";
    if (isset($_POST['leaveType'])) {
        $leaveType = $_POST["leaveType"];
    }
    if (isset($_POST['leavePeriod'])) {
        $leavePeriod = $_POST["leavePeriod"];
    }
	
	$selectQuery = "SELECT staff_type_id from users where id = '".$userId."'";
	$executeQuery = mysqli_query($conn,$selectQuery);
	while($r = mysqli_fetch_assoc($executeQuery)) {
		$staffId = $r['staff_type_id'];
	}
	
	$isFirst = "false";
	$query = "SELECT entitlements.*,leave_type.name,leave_period.from_date,leave_period.to_date from entitlements 
			Left Join leave_type on leave_type.id = entitlements.leave_type_id
			Left Join leave_period on leave_period.id = entitlements.leave_period_id
			where entitlements.staff_type_id = '".$staffId."' and leave_period.period_status = '1'";
	
	if ($leaveType != '' || $leavePeriod != '') {
		$isFirst = "true";
	}
	if ($leaveType != '') {
		if ($isFirst != "false") {
			$query .= " AND ";
		}
		$query .= " entitlements.leave_type_id = '" . $leaveType . "'";
		$isFirst = "true";
	}
	if ($leavePeriod != '') {
		if ($isFirst != "false") {
			$query .= " AND ";
		}
		$query .= " entitlements.leave_period_id = '" . $leavePeriod . "'";
		$isFirst = "true";
	} 
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }   
    print json_encode($rows);
}
?>