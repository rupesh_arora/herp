<?php
/*
 * File Name    :   insuance_plan.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre details form database
 */
$operation           = "";
$description         = "";
$createdate          = new DateTime();
include 'config.php'; //import databse connection file
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

if (isset($_POST['operation'])) { // perform operation form js file
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
}


if ($operation == "save") // save details
{	
	$description = "";
	if (isset($_POST['copayType'])) {
        $copayType = $_POST['copayType'];
    }
    if (isset($_POST['value'])) {
        $value = $_POST['value'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    
    $queryCheck = "SELECT id FROM copay WHERE copay_type = '".$copayType."' 
                 AND status = 'A'";

    $resultCheck = mysqli_query($conn,$queryCheck);
    $countRows = mysqli_num_rows($resultCheck);
   
   if($countRows == "0"){

    	$sqlInsert    = "INSERT INTO copay(copay_type,value,description,created_on,updated_on,created_by,updated_by) VALUES('" . $copayType . "','" . $value . "','" . $description . "',
    			'" . $createdate->getTimestamp() . "','" . $createdate->getTimestamp() . "',".$userId.",".$userId.")";
    	$result = mysqli_query($conn, $sqlInsert);
    	echo $result;
    }

    else{
        echo "0";
    }
	
}

if ($operation == "show") { // show active data
    
    $query        = "SELECT * FROM  copay WHERE `status`='A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
    
}
// opertaion form update
if ($operation == "update") // update data
{
    $description = "";
	if (isset($_POST['copayType'])) {
        $copayType = $_POST['copayType'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    if (isset($_POST['value'])) {
        $value = $_POST['value'];
    }
    $id = $_POST['id'];
    
    $queryCheck = "SELECT id FROM copay WHERE copay_type = '".$copayType."' AND status = 'A' AND id !='".$id."' ";
    $resultCheck = mysqli_query($conn,$queryCheck);
    $countRows = mysqli_num_rows($resultCheck);

    if($countRows == 0){
        $sql    = "update copay set copay_type= '" . $copayType . "' ,value= '" . $value . "' ,description = '" . $description . "', updated_on='" . $createdate->getTimestamp() . "',
                updated_by='" . $userId . "' where id = '" . $id . "' ";
    $result = mysqli_query($conn, $sql);
    echo $result;
    }
    else{
        echo "0";
    }
	
	
}
//  operation for delete
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE copay SET status= 'I' where id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    echo $result;
}
//When checked box is check
if ($operation == "checked") {
    
    $query = "SELECT * FROM  copay WHERE `status`='I'";
    
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}
if ($operation == "restore") // for restore    
    {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    if (isset($_POST['copay'])) {
        $copay = $_POST['copay'];
    }
    if (isset($_POST['value'])) {
        $value = $_POST['value'];
    }

    $queryCheck = "SELECT id,copay_type,value FROM copay WHERE copay_type = '".$copay."' AND status = 'A'";

    $resultCheck = mysqli_query($conn,$queryCheck);
    $countRows = mysqli_num_rows($resultCheck);

    if($countRows == "0"){
        $sql    = "UPDATE copay SET status= 'A'  WHERE  id = '" . $id . "'";
        $result = mysqli_query($conn, $sql);
        echo $result;
    }
    else{
        echo "0";
    }
}
?>