<?php
/*File Name  :   roster_management.php
Company Name :   Qexon Infotech
Created By   :   Tushar Gupta
Created Date :   21th March, 2015
Description  :   This page manages entry roster and attendence management*/

session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

/*include config file*/
include 'config.php';
$headers = '';

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

// save details
if ($operation == "saveDetails") {
    $staffName = $_POST["staffName"];
    $date = $_POST["date"];
    $fieldValue = $_POST["fieldValue"];
    $currentDateTime = $_POST["currentDateTime"];
	$nextDateTime = date( "Y-m-d H:i:s", strtotime( $currentDateTime ) + 8 * 3600 );
	
	if($_POST["assignStaffName"] == "") {
		$assignStaffName = "NULL";
	}
	else{
		$assignStaffName = $_POST["assignStaffName"];
	}
	$selectCheck = "SELECT * from staff_leaves where staff_id = '".$staffName."'  AND 
	DATE_FORMAT(FROM_UNIXTIME(leave_date), '%Y-%m-%d') = '".$date."'";
	$excuteQuery = mysqli_query($conn,$selectCheck);
	$rowCount = mysqli_num_rows($excuteQuery);
	if($rowCount == 0) {
		
		 $insertQuery = "INSERT INTO roaster_management (roaster_staff_id,assigned_staff_id,`date`,created_on,created_by,available) VALUES
						('".$staffName."',".$assignStaffName.",UNIX_TIMESTAMP('$date'),UNIX_TIMESTAMP(),'".$userId."','".$fieldValue."')";
		$executeQuery = mysqli_query($conn,$insertQuery);
		  $last_id="SELECT id as last_id FROM `roaster_management` ORDER BY id DESC LIMIT 0 , 1";
	        $resultlast_id = mysqli_query($conn,$last_id);
			$totalrecords = mysqli_num_rows($resultlast_id);
			$rowslast_id = array();
			while ($rlast_id = mysqli_fetch_assoc($resultlast_id)) {
				$rowslast_id = $rlast_id;
				//print_r($rowslast_id);
				$lastId = $rowslast_id['last_id'];
			}
			if($executeQuery == '1'){
		    	$selQuery = "SELECT CONCAT(uas.first_name,' ',uas.last_name) AS assigned_staff,uas.email_id,
		    	DATE_FORMAT(FROM_UNIXTIME(rm.created_on), '%Y-%m-%d') AS roster_date,
		    	concat(urm.first_name,' ',urm.last_name) AS roster_staff,from_unixtime(rm.date) as assigned_date,
		    	se.roster_management_email FROM sms_email AS se,roaster_management AS rm 
		    	LEFT JOIN users AS urm ON urm.id = rm.roaster_staff_id
				LEFT JOIN users AS uas ON uas.id = rm.assigned_staff_id WHERE rm.id='" . $lastId . "'";
		    	$resultCheck = mysqli_query($conn,$selQuery);
				$totalrecords = mysqli_num_rows($resultCheck);
				$rows = array();
				$assigned_staff;
				$roster_staff;
				$assigned_date;
				$email_id;
				$roster_management_email;
				$date;
				while ($r = mysqli_fetch_assoc($resultCheck)) {
					$rows = $r;
					//print_r($rows);
					$assigned_staff = $rows['assigned_staff'];
					$roster_staff = $rows['roster_staff'];
					$assigned_date = $rows['assigned_date'];
					$email_id = $rows['email_id'];
					$roster_management_email = $rows['roster_management_email'];
					$date = $rows['roster_date'];
				}
				
				$previousMessage  = $roster_management_email;
				$replace = array("@@assigned_staff@@", "@@roster_staff@@", "@@date@@",);
				$replaced   = array($assigned_staff, $roster_staff, $assigned_date);
				$message = str_replace($replace, $replaced, $previousMessage);
				//print_r($message);
				if ($email_id) {
					mail($email_id, " Assigned Work by ".$roster_staff."", $message, $headers);
					$sqlQuery = "INSERT INTO sms_email_log(screen_name,mobile_no,email,phone_message,email_message,date,
                                created_on,updated_on,created_by,updated_by,msg_status) VALUES('Roster Management','',
                                '".$email_id."','','".$message."','".$date."',UNIX_TIMESTAMP(),
                                UNIX_TIMESTAMP(),'".$userId."','".$userId."','Mail')";

                    $result = mysqli_query($conn, $sqlQuery);
				}
				
		   	}
		
		if($fieldValue == 'P') {
			$insertQueryAttendence = "INSERT INTO staff_attendance (staff_id,`date`,punch_in,punch_out,created_on,updated_on,
			created_by,updated_by) VALUES ('".$staffName."','".$date."',UNIX_TIMESTAMP('$currentDateTime'),
			UNIX_TIMESTAMP('$nextDateTime'),UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
			
			$executeQueryAttendence = mysqli_query($conn,$insertQueryAttendence);
		}
		if($executeQuery) {
			echo $executeQuery;
		}
	}
	else{
		echo "He is on leave";
	}
}
?>