<?php
/*
 * File Name    :   allergies.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre details from database        
 */
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
$operation        = "";
$createdate       = new DateTime();

include 'config.php';		// import database connection file

if (isset($_POST['operation'])) {		// opeartion come from js 
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
    
}

if ($operation == "showInventoryType") { // show department
    $query = "SELECT id,type FROM inventory_type where status = 'A'";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

// show detals 
if ($operation == "showDetails") { // show department
	$inventoryType = $_POST['inventoryType'];
    $query = "SELECT item_request.item_id,items.name,item_request.received_quantity,item_request.ordered_quantity  AS quantity,
				item_request.ordered_by,item_request.ordered_date,item_stock.quantity AS stock,item_stock.unit_cost,
				CONCAT(users.salutation,' ',users.first_name,' ',users.last_name) As user_name
				FROM item_request
				LEFT JOIN item_stock ON item_request.item_id  = item_stock.item_id
				LEFT JOIN items ON items.id  = item_request.item_id
				LEFT JOIN users ON users.id  = item_request.ordered_by WHERE item_request.inventory_type_id = '".$inventoryType."'
				AND items.status='A'  AND item_request.`status`='Pending'";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

// save details
if ($operation == "saveDetails") {
    $inventoryType = $_POST["inventoryType"];
    
    $itemId = $_POST["itemId"];
    echo $selectDrugId = "SELECT id FROM drugs WHERE drug_code IN(SELECT code FROM items WHERE id IN(".$itemId."))";
	$executeQuery = mysqli_query($conn,$selectDrugId);
	$drugId   = array();
    while ($r = mysqli_fetch_row($executeQuery)) {
        array_push($drugId, $r[0]);
    }
   // $quantity = $_POST["quantity"];
    //$stock = $_POST["stock"];
    //$requestQuantity = $_POST["requestQuantity"];
    $todayDate = date("Y-m-d");
	$printDiv = $_POST["printDiv"];
	$receiptNo = $_POST["receiptNo"];
	//$stockQuantity = $stock  - $quantity;
	//$reqQuantity = $requestQuantity  + $quantity;
	$counter =0;
	if (isset($_POST['dataArray'])) {
		$dataArray=json_decode($_POST["dataArray"]);
		
		foreach($dataArray as $value) {

			$itemQuantity = $value-> itemQuantity;
			$itemId = $value-> itemId;
			$requestQuantity = $value-> requestQuantity;
			$stockQuantity = $value-> stockQuantity;

			$insertQuery = "INSERT INTO item_response (inventory_type_id,item_id,quantity,dispatched_by,dispatched_date,invoice_id) VALUES
					('".$inventoryType."',".$itemId.",'".$itemQuantity."','".$userId."','".$todayDate."','".$receiptNo."')";
			$executeQuery = mysqli_query($conn,$insertQuery);
			if($executeQuery) {
				$updateQuery = "UPDATE item_request SET received_quantity = '".$itemQuantity."',status='Received' WHERE item_id = '".$itemId."'";
				$executeUpdateQuery = mysqli_query($conn,$updateQuery);
				
				$updateStockQuery = "UPDATE item_stock SET quantity = '".$stockQuantity."',updated_on=UNIX_TIMESTAMP(),updated_by = '".$userId."' WHERE item_id = '".$itemId."'";
				$executeUpdateStockQuery = mysqli_query($conn,$updateStockQuery);
				
			}	

			print_r($drugId);
			exit();
			$newQuantity =0;
		    echo $query = "SELECT quantity FROM pharmacy_inventory WHERE drug_name_id = ".$drugId[$counter]."";
			$result = mysqli_query($conn,$query);
			//exit();
			while ($r = mysqli_fetch_row($result)) {            
		    	$newQuantity =  $r[0] +$itemQuantity;
			}

			$updateQuery = "UPDATE pharmacy_inventory SET quantity = '".$newQuantity."' WHERE drug_name_id = ".$drugId[$counter]."";
			$executeUpdateQuery = mysqli_query($conn,$updateQuery);
			$counter++;

			if ($result == 0) {
				echo $insertQuery = "INSERT INTO pharmacy_inventory(drug_name_id,quantity,created_on,updated_on,created_by,updated_by)
								VALUES('".$drugId[$counter]."','".$newQuantity."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP,'".$userId."','".$userId."')";
				$executeQuery = mysqli_query($conn,$insertQuery);
				$counter++;

			}
		}
	}	

	if($executeUpdateStockQuery) {
		//echo $executeUpdateStockQuery;
		$sqlInsertInvoice = "INSERT INTO invoice_details (invoice_details) VALUES ('".$printDiv."')";
		$resultInvoice = mysqli_query($conn,$sqlInsertInvoice);
		if($resultInvoice) {
			$lastInsertId = mysqli_insert_id($conn);
			
			// cash deposite transaction
			$insertTransaction = "INSERT INTO inventory_transaction (transaction_type_id,processed_on,processed_by,invoice_id)
			values ('3',UNIX_TIMESTAMP(),'".$userId ."','".$lastInsertId."')"; 
			mysqli_query($conn,$insertTransaction);
		}
	}
}
?>