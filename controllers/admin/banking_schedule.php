<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "showPaymentmode") { // show insurance company type

		$type = $_POST['type'];
		$ledgerId = $_POST['ledgerId'];

	    $query = "SELECT payment_mode.id,payment_mode.name FROM payment_mode			
			WHERE payment_mode.`status` = 'A' AND payment_mode.is_available_bank_source = '".$type."' AND 
			payment_mode.ledger_type = '".$ledgerId."'";
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "showReceiptNumber") {
		$source = $_POST['source'];
		$query = "SELECT receipt_book.id,receipt_book.amount ,receipt_book.ledger_id,receipt_book.payment_mode_id,
		DATE_FORMAT(FROM_UNIXTIME(receipt_book.updated_on), '%Y-%m-%d') AS date,'' As refernce,
		(SELECT name FROM payment_mode WHERE id=receipt_book.payment_mode_id) AS payee_name
		 FROM receipt_book
		 WHERE receipt_book.status = 'A'  AND receipt_book.payment_mode_id = '".$source."' AND receipt_book.id NOT IN (SELECT receipt_id FROM banking_schedule_receipt_number WHERE banking_schedule_id IN (SELECT id FROM banking_schedule WHERE source_id = '".$source."'))";
		 
	    
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	if ($operation == "saveData") {
		$ledgerId = $_POST['ledgerId'];
		$sourceId = $_POST['sourceId'];
		$bankId = $_POST['bankId'];
		$totalAmount = $_POST['totalAmount'];
		$saveArray = json_decode($_POST['saveArray']);
		
		$insertMainQry = "INSERT INTO banking_schedule (ledger_id,payment_mode_id,source_id,amount,created_by,updated_by,created_on,updated_on) VALUES ('".$ledgerId."','".$bankId."','".$sourceId."','".$totalAmount."','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP());";

		mysqli_query($conn,$insertMainQry);
		$last_id = mysqli_insert_id($conn);

		$insertSubDetails = "INSERT INTO banking_schedule_receipt_number(banking_schedule_id,receipt_id,amount, created_on, updated_on,created_by,updated_by) VALUES ";
		$counter = 0;
		$subQry = '';
		foreach ($saveArray as $key => $value) {
			$receiptId = $value->receiptId;
			$amount = $value->amount;

			if ($counter > 0) {
				$subQry.= ',';
			}
			$subQry.= "('".$last_id."','".$receiptId."','".$amount."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
			$counter++;
		}

		$completeQry = $insertSubDetails.$subQry;
		$result = mysqli_query($conn,$completeQry);
		echo $result;
	}

	if ($operation == "show") {
		 $query        = "SELECT banking_schedule.id,banking_schedule.ledger_id,banking_schedule.payment_mode_id,banking_schedule.source_id,DATE_FORMAT(FROM_UNIXTIME(banking_schedule.created_on), '%Y-%m-%d') as bank_date,GROUP_CONCAT(bsrn.receipt_id) AS receipt_id,GROUP_CONCAT(bsrn.amount) AS amount,
		ledger.name As ledger_name,(SELECT name FROM payment_mode WHERE id=banking_schedule.payment_mode_id) AS payee_name
		FROM banking_schedule
		LEFT JOIN banking_schedule_receipt_number AS bsrn ON bsrn.banking_schedule_id = banking_schedule.id
		LEFT JOIN ledger ON ledger.id = banking_schedule.ledger_id
		WHERE banking_schedule.status = 'A' AND bsrn.`status` = 'A'
		GROUP BY banking_schedule.id";
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    /*JSON encode*/
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	/*if ($operation == "showInActive") {
		 $query        = "SELECT banking_schedule.id,banking_schedule.ledger_id,banking_schedule.payment_mode_id,banking_schedule.source_id ,GROUP_CONCAT(bsrn.receipt_id) AS receipt_id,GROUP_CONCAT(bsrn.amount) AS amount,
		ledger.name As ledger_name,(SELECT name FROM payment_mode WHERE id=banking_schedule.payment_mode_id) AS payee_name
		FROM banking_schedule
		LEFT JOIN banking_schedule_receipt_number AS bsrn ON bsrn.banking_schedule_id = banking_schedule.id
		LEFT JOIN ledger ON ledger.id = banking_schedule.ledger_id
		WHERE banking_schedule.status = 'I' AND bsrn.`status` = 'I'
		GROUP BY banking_schedule.id";
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //JSON encode
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}*/
?>