<?php
/*
 * File Name    :   Designation.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   29 Sept, 2015
 * Description  :   This page is use for add department
 */
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

$operation       = "";

include 'config.php'; // include database connection file

if (isset($_POST['operation'])) { // define operation value from js file
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}


/* save designation informationinto database*/
if ($operation == "save") {
    
    if (isset($_POST['staffId'])) {
        $staffId = $_POST['staffId'];
    }
    if (isset($_POST['staffName'])) {
        $staffName = $_POST['staffName'];
    }
    if (isset($_POST['comments'])) {
        $comments = $_POST['comments'];
    }
    if (isset($_POST['countStarDependablity'])) {
        $countStarDependablity = $_POST['countStarDependablity'];
    }
    if (isset($_POST['countStarCooperativeness'])) {
        $countStarCooperativeness = $_POST['countStarCooperativeness'];
    }
    if (isset($_POST['countStarAdaptability'])) {
        $countStarAdaptability = $_POST['countStarAdaptability'];
    }
    if (isset($_POST['countStarSercice'])) {
        $countStarSercice = $_POST['countStarSercice'];
    }
    if (isset($_POST['countStarAttedance'])) {
        $countStarAttedance = $_POST['countStarAttedance'];
    }
    if (isset($_POST['countStarCommunication'])) {
        $countStarCommunication = $_POST['countStarCommunication'];
    }
    

    $sqlInsert    = "INSERT INTO review_staff (staff_id,dependability_rating,cooperativeness_rating,adaptability_rating,sercice_rating,attedance_rating,communication_rating,comments,review_by,review_date,created_by,created_on) 
        VALUES ('".$staffId."','".$countStarDependablity."','".$countStarCooperativeness."',
        '".$countStarAdaptability."','".$countStarSercice."','".$countStarAttedance."',
        '".$countStarCommunication."','".$comments."','".$userId."',UNIX_TIMESTAMP(),
        '".$userId."',UNIX_TIMESTAMP())";

    $resultInsert = mysqli_query($conn, $sqlInsert);
    echo $resultInsert;
    
}

if ($operation == "searchStaff") {
    if (isset($_POST['staffId'])) {
        $staffId = $_POST['staffId'];
    }

    $sql = "SELECT first_name FROM users WHERE id ='".$staffId."'";

    $result         = mysqli_query($conn, $sql);
    $rows           = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}

if ($operation == "viewStaffReviewDetails") {

    $sql ="SELECT review_staff.id,review_staff.staff_id,review_staff.dependability_rating,
        review_staff.cooperativeness_rating,review_staff.adaptability_rating,
        review_staff.sercice_rating,review_staff.attedance_rating,
        review_staff.communication_rating,review_staff.comments,
        DATE_FORMAT(FROM_UNIXTIME(review_staff.review_date), '%Y-%m-%d') AS review_date,
        users.first_name AS review_by , users_staff.first_name AS staff_name FROM review_staff
        LEFT JOIN users ON users.id = review_staff.review_by 
        LEFT JOIN users as users_staff ON users_staff.id = review_staff.staff_id";

    $result         = mysqli_query($conn, $sql);
    $rows           = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
?>