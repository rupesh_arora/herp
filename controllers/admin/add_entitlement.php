<?php
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

$operation       = "";

include 'config.php'; // include database connection file

if (isset($_POST['operation'])) { // define operation value from js file
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

if ($operation == "showLeaveType") {

    $sqlSelect = "SELECT id,name FROM leave_type WHERE status = 'A'";

    $result         = mysqli_query($conn, $sqlSelect);
    $rows           = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "showLeavePeriod") {

    $sqlSelect = "SELECT id,CONCAT(leave_period.from_date,' - ',leave_period.to_date) AS period FROM leave_period WHERE period_status = 1";

    $result         = mysqli_query($conn, $sqlSelect);
    $rows           = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "showStaffType") {

    $sqlSelect = "SELECT id,type FROM staff_type WHERE status = 'A'";

    $result         = mysqli_query($conn, $sqlSelect);
    $rows           = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
/* save designation informationinto database*/
if ($operation == "save") {
    
    if (isset($_POST['staffType'])) {
        $staffType = $_POST['staffType'];
    }
    if (isset($_POST['leaveType'])) {
        $leaveType = $_POST['leaveType'];
    }
    if (isset($_POST['entitlement'])) {
        $entitlement = $_POST['entitlement'];
    }
    if (isset($_POST['leavePeriod'])) {
        $leavePeriod = $_POST['leavePeriod'];
    }
    
    $sql1 = "SELECT entitlements FROM entitlements WHERE staff_type_id = '".$staffType."' AND leave_type_id = '".$leaveType."' AND leave_period_id = '".$leavePeriod."'";
    $resultSelect = mysqli_query($conn, $sql1);
    $rows_count   = mysqli_num_rows($resultSelect);
    
    if ($rows_count <= 0) {
        $sqlInsert    = "INSERT INTO entitlements (staff_type_id,leave_type_id,leave_period_id,
        entitlements,created_on,updated_on,created_by,updated_by) VALUES ('".$staffType."',
        '".$leaveType."','".$leavePeriod."','".$entitlement."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),
        '".$userId."','".$userId."')";

        $resultInsert = mysqli_query($conn, $sqlInsert);
        echo $resultInsert;
    } else {
        echo "0";
    }
}

if ($operation == "show") {
    $sqlSelect    = "SELECT entitlements.id,entitlements.`entitlements`,entitlements.staff_type_id,entitlements.leave_type_id,entitlements.leave_period_id,  staff_type.`type` AS `staff_type`,leave_type.name AS leave_name,CONCAT(leave_period.from_date, ' -',leave_period.to_date) AS leave_period 
        FROM entitlements 
        LEFT JOIN staff_type ON staff_type.id = entitlements.staff_type_id
        LEFT JOIN leave_type ON leave_type.id = entitlements.leave_type_id
        LEFT JOIN leave_period ON leave_period.id = entitlements.leave_period_id
        WHERE  entitlements.`status` = 'A'";

    $resultSelect = mysqli_query($conn, $sqlSelect);
    $totalrecords = mysqli_num_rows($resultSelect);
    
    $rows = array();
    while ($r = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $r;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/* update designation information into database
if ($operation == "update") {
    if (isset($_POST['leaveTypeName'])) {
        $leaveTypeName = $_POST['leaveTypeName'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    Check that bed already exist for that particular bed number, roomid AND wardid
    $sqlSELECT    = "SELECT name FROM leave_type WHERE name='" . $leaveTypeName . "' 
        AND id != '$id'";
    $resultSELECT = mysqli_query($conn, $sqlSELECT);
    $rows_count   = mysqli_num_rows($resultSELECT);

    if ($rows_count == 0) {
        $sql    = "UPDATE leave_type SET name = '" . $leaveTypeName . "',
            description='" . $description . "',updated_on = UNIX_TIMESTAMP(),
            updated_by = '".$userId."' WHERE id = '" . $id . "'";
        $result = mysqli_query($conn, $sql);
        echo $result;
    }
    else{
        echo "0";
    }
}
 set status Inactive for delete designation 
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sqlSelectEntitlement = "SELECT leave_type_id FROM entitlements WHERE leave_type_id = '".$id."'AND `status`  = 'A'";
    $resultEntitlement     = mysqli_query($conn, $sqlSelectEntitlement);
    $countEntitlement = mysqli_num_rows($resultEntitlement);

    $sqlSelectDeafaultLeave = "SELECT leave_type_id FROM default_leaves WHERE leave_type_id = '".$id."' AND `status`  = 'A'";
    $resultDeafaultLeave     = mysqli_query($conn, $sqlSelectDeafaultLeave);
    $countDeafaultLeave = mysqli_num_rows($resultDeafaultLeave);

    $sqlSelectStaffLeave = "SELECT leave_type_id FROM staff_leaves WHERE leave_type_id = '".$id."'";
    $resultStaffLeave     = mysqli_query($conn, $sqlSelectStaffLeave);
    $countStaffLeave = mysqli_num_rows($resultStaffLeave);
    if($countEntitlement == 0 && $countDeafaultLeave == 0 && $countStaffLeave ==0){
        
        $sqlUpdate = "UPDATE `herp`.`leave_type` SET `status`='I' WHERE  `id`='".$id."'"; 
        $resultUpdate = mysqli_query($conn, $sqlUpdate);
        echo $resultUpdate;
    }else{
        echo "0";
    }  
}
show active data into the datatable  



 show Inactive data into datatable on checked case
if ($operation == "checked") {
    $sqlSelect    = "SELECT * FROM leave_type where status = 'I'";
    $resultSelect = mysqli_query($conn, $sqlSelect);
    $totalrecords = mysqli_num_rows($resultSelect);
    
    $rows = array();
    while ($rUpdate = mysqli_fetch_assoc($resultSelect)) {
        $rows[] = $rUpdate;
    }
    //print json_encode($rows);
    
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

 set status active for restore designation 
if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $sql    = "UPDATE leave_type SET status= 'A'  where  id = " . $id . "";
    $result = mysqli_query($conn, $sql);
    echo "1";
}*/
?>