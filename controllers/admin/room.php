<?php
/*File Name  :   room.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page  manages rooms */
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
$operation   = "";
$roomType    = "";
$roomNumber  = "";
$description = "";

/*include config file*/
include 'config.php';

/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
} //nothing

/*operation to show room type*/
if ($operation == "showRoomType") {
    
    $query  = "SELECT id,`type` FROM room_type WHERE status='A' ORDER BY `type`";
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    print json_encode($rows);
}

/*operation to save*/
if ($operation == "save") {
    
    if (isset($_POST['roomNumber'])) {
        $roomNumber = $_POST['roomNumber'];
    }
    if (isset($_POST['roomType'])) {
        $roomType = $_POST['roomType'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    if (isset($_POST['availability'])) {
        $availability = $_POST['availability'];
    }
    /*check that room for that room number exist or not*/
    $sqlSELECT    = "SELECT number, room_type_id FROM room WHERE number='" . $roomNumber . "' 
        AND room_type_id = '".$roomType."' ";
    $resultSELECT = mysqli_query($conn, $sqlSELECT);
    $rows_count   = mysqli_num_rows($resultSELECT);
    if ($rows_count == 0) {
        $sql    = "INSERT INTO room(room_type_id,number,description,availability,created_on,updated_on,created_by,updated_by) VALUES(" . $roomType . ",'" . $roomNumber . "','" . $description . "','" . $availability . "',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."')";
        $result = mysqli_query($conn, $sql);
        
        echo $result;
    } else {
        echo "0";
    }
}

/*operation to update*/
if ($operation == "update") {
    if (isset($_POST['roomNumber'])) {
        $roomNumber = $_POST['roomNumber'];
    }
    if (isset($_POST['roomType'])) {
        $roomType = $_POST['roomType'];
    }
    if (isset($_POST['description'])) {
        $description = $_POST['description'];
    }
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    if (isset($_POST['availability'])) {
        $availability = $_POST['availability'];
    }
    /*check that room for that room number and id exist or not*/
    $sqlSELECT    = "SELECT number, room_type_id FROM room WHERE number='" . $roomNumber . "'  
    AND id != '$id' AND room_type_id = '".$roomType."'";
    $resultSELECT = mysqli_query($conn, $sqlSELECT);
    $rows_count   = mysqli_num_rows($resultSELECT);
    if ($rows_count == 0) {
        
        $sql    = "UPDATE room SET room_type_id= '" . $roomType . "',number= '" . $roomNumber . "',
            description='" . $description . "' , availability = '".$availability."',updated_on=UNIX_TIMESTAMP(),updated_by='".$userId."' WHERE  id = '" . $id . "'";
        $result = mysqli_query($conn, $sql);
        echo $result;
    } else {
        echo "0";
    }
}

/*operation to delete*/
if ($operation == "delete") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    
    $sql    = "UPDATE room SET status= 'I'  WHERE  id = '" . $id . "' and availability = '0'";
    $result = mysqli_query($conn, $sql);
	if($result == "1")
	{
		$query  = "SELECT status FROM room WHERE id = '" . $id . "'";
        $result = mysqli_query($conn, $query);
        while ($r = mysqli_fetch_assoc($result)) {
            if ($r['status'] == 'I') {
                echo "1";
            } else {
                echo "0";
            }
        }
	}
}

/*operation to restore*/
if ($operation == "restore") {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    $room_id = $_POST['room_id'];
    
    $sql    = "UPDATE room SET status= 'A' WHERE  id = '" . $id . "'";
    $result = mysqli_query($conn, $sql);
    /*IF above query run sucessfully then check status from room type table if it is active then restore*/
    if ($result == 1) {
        $query  = "SELECT status FROM room_type WHERE id='" . $room_id . "'";
        $result = mysqli_query($conn, $query);
        while ($r = mysqli_fetch_assoc($result)) {
            if ($r['status'] == 'A') {
                echo "1";
            } else {
                echo "0";
            }
        }
    }
}

/*Operation to show active data*/
if ($operation == "show") {
    
    $query        = "SELECT r.id,r.room_type_id,r_t.type, r.number, r.description ,r.availability FROM room AS r 
        LEFT JOIN room_type AS r_t ON r.room_type_id=r_t.id  
        WHERE r.status='A' And r_t.status='A'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result); //count total no. of records
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

/*Operation to show inactive data*/
if ($operation == "showInActive") {
    $query        = "SELECT r.id,r.room_type_id,r_t.type, r.number, r.description, r.availability FROM room AS r 
        LEFT JOIN room_type AS r_t ON r.room_type_id=r_t.id  
        WHERE r.status='I' OR r_t.status='I'";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}
?>