<?php
	session_start(); // session start
	if (isset($_SESSION['globaluser'])) {
	    $userId = $_SESSION['globaluser'];
	}
	else{
	    exit();
	}
	include 'config.php';
	
	if (isset($_POST['operation'])) {
		$operation = $_POST["operation"];
	}

	else if(isset($_GET["operation"])){
		$operation = $_GET["operation"];
	}

	if ($operation == "saveRecurrencePeriod") {

		$recurrencePeriod = $_POST['recurrencePeriod'];
		$recurrencePeriod = mysql_real_escape_string($recurrencePeriod);

		$selSubType = "SELECT recurrence_period FROM recurrence_period WHERE recurrence_period ='".$recurrencePeriod."' AND status = 'A'";
		$checkSubType = mysqli_query($conn,$selSubType);
		$countSubType = mysqli_num_rows($checkSubType);

		if ($countSubType == "0") {

			$query = "INSERT INTO recurrence_period(recurrence_period,created_by,updated_by,created_on,updated_on) VALUES('".$recurrencePeriod."','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";

			$result = mysqli_query($conn, $query);
			echo $result;
		}
		else {
			echo "0";
		}
	}

	if ($operation == "show") { // show active data
    
    	$query = "SELECT id,recurrence_period FROM recurrence_period WHERE status = 'A'";
	    $result = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
    
	}

	if ($operation == "checked") {
	    
	    $query = "SELECT id,recurrence_period FROM recurrence_period WHERE status = 'I'";
	    
	    $result       = mysqli_query($conn, $query);
	    $totalrecords = mysqli_num_rows($result);
	    $rows         = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    //print json_encode($rows);
	    
	    $json = array(
	        'sEcho' => '1',
	        'iTotalRecords' => $totalrecords,
	        'iTotalDisplayRecords' => $totalrecords,
	        'aaData' => $rows
	    );
	    echo json_encode($json);
	}

	if ($operation == "update") // update data
	{
	    $recurrencePeriod = $_POST['recurrencePeriod'];
	    $id = $_POST['id'];

	    $selSubType = "SELECT recurrence_period FROM recurrence_period WHERE recurrence_period ='".$recurrencePeriod."' AND status = 'A' AND id !='".$id."'";
		$checkSubType = mysqli_query($conn,$selSubType);
		$countSubType = mysqli_num_rows($checkSubType);

		if ($countSubType == "0") {
		
			$sql    = "UPDATE recurrence_period set recurrence_period = '".$recurrencePeriod."',updated_on = UNIX_TIMESTAMP() ,updated_by = '".$userId."' where id = '".$id."' ";

			$result = mysqli_query($conn, $sql);
			echo $result;
		}
		else {
			echo "0";
		}

	}

	if ($operation == "delete") {
        $id = $_POST['id'];

        
		$sql    = "UPDATE recurrence_period SET status= 'I' where id = '" . $id . "'";
	    $result = mysqli_query($conn, $sql);
	    echo $result;	    
	}

	if ($operation == "restore") {// for restore    
        $id = $_POST['id'];
        $recurrencePeriod = $_POST['recurrencePeriod'];

       	$selSubType = "SELECT recurrence_period FROM recurrence_period WHERE recurrence_period ='".$recurrencePeriod."' AND status = 'A' AND id !='".$id."'";
		$checkSubType = mysqli_query($conn,$selSubType);
		$countSubType = mysqli_num_rows($checkSubType);

		if ($countSubType == "0") {

		    $sql    = "UPDATE recurrence_period SET status= 'A'  WHERE  id = '" . $id . "'";

		    $result = mysqli_query($conn, $sql);
		    echo $result;
		}
		else {
			echo "0";
		}
	}