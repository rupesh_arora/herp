<?php
/* ****************************************************************************************************
 * File Name    :   investigations.php
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   4th mar, 2016
 * Description  :   This page  manages ipd ivestigations
 *************************************************************************************************** */
	session_start();	

    if(isset($_SESSION['globaluser'])){
        $userId = $_SESSION['globaluser'];//store global user value in variable
    }
    else{
    	exit();
    }
$date      = date("Y-m-d");
$createOn  = new DateTime();

	

    /*include config file*/
	include 'config.php';
    $operation = "";

    /*checking operation set or not*/
	if (isset($_POST['operation'])) {
	    $operation = $_POST["operation"];
	} else if (isset($_GET["operation"])) {
	    $operation = $_GET["operation"];
	}

    /*operation to show lab type*/
	if ($operation == "showLabType") {
	    $query  = "SELECT id,type FROM lab_types WHERE status = 'A' ORDER BY type";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

    /*operation to show lab test*/
	if ($operation == "showLabTest") {
	    if (isset($_POST['value'])) {
	        $value = $_POST['value'];
	    }
	    $query  = "SELECT id,name FROM lab_tests WHERE lab_type_id = '" . $value . "' AND status = 'A' ORDER BY name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

	/*operation to show lab test*/
	if ($operation == "showLabTestComponent") {
	    if (isset($_POST['labTestId'])) {
	        $labTestId = $_POST['labTestId'];
	    }
	    $query  = "SELECT id,name,fee,lab_test_id  FROM lab_test_component WHERE lab_test_id = '" . $labTestId . "' ORDER BY name";
	    $result = mysqli_query($conn, $query);
	    $rows   = array();
	    while ($r = mysqli_fetch_assoc($result)) {
	        $rows[] = $r;
	    }
	    print json_encode($rows);
	}

    

    /*operation to show patient details*/
	if ($operation == "patientDetail") {
		$ipdPrefix = $_POST['ipdPrefix'];
		$sql           = "SELECT value FROM configuration WHERE name = 'ipd_prefix'";
		$result        = mysqli_query($conn, $sql);
		while ($r = mysqli_fetch_assoc($result)) {
			$prefix = $r['value'];
		}
		if($prefix == $ipdPrefix){
			 $r='';
			 if (isset($_POST['ipdId'])) {
				$ipdId = json_decode($_POST['ipdId']);
			}
			/*Check wheteher patient id exisit in patient table or not*/
			$sqlSelectPatient = "SELECT i.patient_id,p.first_name, p.last_name, p.mobile, p.email,p.salutation,
				p.last_name,i.id,(select value from configuration where name = 'patient_prefix') 
            	as prefix FROM ipd_registration AS i
				LEFT JOIN patients AS p ON p.id = i.patient_id
				WHERE i.id='" . $ipdId . "'";
			$resultSelectPatient     = mysqli_query($conn, $sqlSelectPatient);
			$rows_count = mysqli_num_rows($resultSelectPatient);
			if ($rows_count < 1) {
				echo "0";
			}
			else{
				$rows = array();
				while ($r = mysqli_fetch_assoc($resultSelectPatient)) {
					$rows[] = $r;
				}
				print json_encode($rows);
			}
		}
		else{
			echo "";
		}
	}

    /*operation to save whole data*/
	if ($operation == "saveDataTableData") {
	    if (isset($_POST['bigPrice_l'])) {
	        $bigPrice_l = json_decode($_POST['bigPrice_l']);
	    }
	    if (isset($_POST['Price'])) {
	        $Price = json_decode($_POST['Price']);
	    }
	    if (isset($_POST['patientId'])) {
	        $patientId = $_POST['patientId'];
	    }
		if (isset($_POST['prefix'])) {
	        $patientPrefix = $_POST['prefix'];
	    }
        if (isset($_POST['ipdId'])) {
            $ipdId = $_POST['ipdId'];
        }
        if (isset($_POST['grandTotal'])) {
            $grandTotal = $_POST['grandTotal'];
        }
		/* $finalData  = "";
		$IPD = ""; */
		$queryCashAccount = "select sum(cash_account.credit - cash_account.debit) as amount from cash_account WHERE patient_id = ".$patientId."";
		$resultCashAccount = mysqli_query($conn,$queryCashAccount);
		while($row = mysqli_fetch_row($resultCashAccount)) {
			$balance = $row['0'];
		}
		if($balance >= $grandTotal){

			$queryGetAccountNo = "SELECT fan_id from patients WHERE id = ".$patientId."";
            $resultAccount = mysqli_query($conn,$queryGetAccountNo);
            while ($r = mysqli_fetch_assoc($resultAccount)) {
                $familyAccountNo = $r['fan_id'];
            }
            if($familyAccountNo != ""){
                $AccountNo = $familyAccountNo;
            }
            else{
                $AccountNo = $patientId;
            }
			$IPD = $IPD.'IPD Id: '.$ipdId;
			$sqlDebit    = "insert into cash_account (patient_id,depositor_id,date,credit,debit,created_on,created_by)
				values('" . $AccountNo . "','" . $patientId . "','" . $date . "','0','" . $grandTotal . "','" . $createOn->getTimestamp() . "','" . $userId . "')";
	        $resultDebit = mysqli_query($conn, $sqlDebit);
	        if ($resultDebit == "1") {
	        
				if (!empty($bigPrice_l)) {					

					foreach ($Price as $value) {

						foreach ($value as $values) {

							$labId    = $values->labId;
							$price    = $values->total;

							//query to insert bill
							/*$insert_lab_test_bill = "INSERT INTO patient_lab_bill (patient_id,visit_id,
							lab_test_id,charge,status,created_by,updated_by,created_on,updated_on) VALUES('" . $patientId . "','" . $ipdId . "','" . $labId . "',
								'" . $price . "','unpaid','".$userId."','".$userId."',UNIX_TIMESTAMP(),UNIX_TIMESTAMP())";*/
							//$resultbill             = mysqli_query($conn, $insert_lab_test_bill);

							$sql = "INSERT INTO ipd_lab_test_requests(patient_id,ipd_id,lab_test_id
								,request_date,pay_status,test_status,cost,status,requested_by)
								 VALUES('" . $patientId . "','" . $ipdId . "','" . $labId . "',UNIX_TIMESTAMP(),
									'paid','pending','" . $price . "','A','" . $userId . "')";
							$resultlab             = mysqli_query($conn, $sql);
							$labTestId = mysqli_insert_id($conn);
						
							
							foreach ($bigPrice_l as $val) {
								
								foreach ($val as $vals) {

									$id    = $vals->labId;
									$componentId    = $vals->componentId;
									$componentName   = $vals->componentName;
									$componentPrice    = $vals->price;									
									
									if($id == $labId){
										//$finalData = $finalData.'Test Name: '.$componentName.',Cost: '.$componentPrice.',';
										
										$sqlComponent = "INSERT INTO ipd_lab_test_request_component(ipd_lab_test_request_id,lab_component_id)
										 VALUES('" . $labTestId . "','" . $componentId . "')";
										$resultcomponent             = mysqli_query($conn, $sqlComponent);										
									}
								}
							}
						}
					}
					/*$data = $IPD.','.$finalData;
					 // save transaction details
					$insertTransaction = "INSERT INTO transactions (patient_id,transaction_type_id,credit,debit,processed_on,processed_by,details)
					values ('" . $patientId . "',' 9 ','0','".$grandTotal."',UNIX_TIMESTAMP(),'".$userId ."','".$data."')"; 
					mysqli_query($conn,$insertTransaction); */
					
					echo $resultcomponent;
				}    
	        }
		}
		else{
			echo "2";
		}  
	}
?>