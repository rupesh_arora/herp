<?php
/*
 * File Name    :   hospital_profile.php
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   29th dec, 2015
 * Description  :   This page is use for save and update a hospital profile
 */
session_start(); // session start
if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}
include 'config.php'; // database connection file 

$operation    = "";
$hospitalName = "";
$address      = "";
$phone        = "";
$email        = "";
$country      = "";
$state        = "";
$city         = "";

/* operation value take from js file*/

if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
} else {
}

/* use for image uploading */

if (isset($_FILES["file"]["type"])) {
    $date            = new DateTime();
    $validextensions = array(
        "jpeg",
        "jpg",
        "png",
        "gif"
    );
    $temporary       = explode(".", $_FILES["file"]["name"]);
    
    $extension = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION); // getting extension
	$str = 'abcdef';
	$imagename = str_shuffle($str). $date->getTimestamp();
    
    $image_name = $imagename . "." . $extension;
    $name       = str_replace(" ", "", $image_name); // remove space
    
    $file_extension = end($temporary);
    if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")) && ($_FILES["file"]["size"] < 2002000) //Approx. 100kb files can be uploaded.
        && in_array($file_extension, $validextensions)) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
        } else {
            if (file_exists("../../images/" . $name)) {
                echo "";
            } else {
                $sourcePath = $_FILES["file"]["tmp_name"]; // Storing source path of the file in a variable
                $targetPath = "../../images/" . $name; // Target path where file is to be stored
                move_uploaded_file($sourcePath, $targetPath); // Moving Uploaded file
                
                echo $name;
                
            }
        }
    } else {
        echo "invalid file";
    }
}

/* for show country */
if ($operation == "showcountry") {
    $query = "SELECT location_id,name FROM locations where location_type = 0 ORDER BY name";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
/* for show state */
if ($operation == "showstate") {
    $countryID = $_POST['country_ID'];
    $query     = "SELECT location_id,name FROM locations where location_type = 1 and parent_id = '$countryID' ORDER BY name";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
/* for show city */
if ($operation == "showcity") {
    $stateID = $_POST['state_ID'];
    $query   = "SELECT location_id,name FROM locations where location_type = 2 and parent_id = '$stateID' ORDER BY name";
    
    $result = mysqli_query($conn, $query);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    print json_encode($rows);
}
if ($operation == "showCountryCode") {
    $countryID = $_POST['countryID'];
    $query   = "SELECT location_id,country_code FROM locations WHERE location_id = '".$countryID."'";
    
    $result = mysqli_query($conn,$query);
    $totalrecords = mysqli_num_rows($result);
    $rows = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows = $r;
    }
    print json_encode($rows);
}

/* save information into database */
if ($operation == "save") {
    
    if (isset($_POST['hospitalName'])) {
        $hospitalName = $_POST['hospitalName'];
    }
    if (isset($_POST['address'])) {
        $address = $_POST['address'];
    }
    if (isset($_POST['phone'])) {
        $phone = $_POST['phone'];
    }
    if (isset($_POST['email'])) {
        $email = $_POST['email'];
    }
    if (isset($_POST['country'])) {
        $country = $_POST['country'];
    }
    if (isset($_POST['state'])) {
        $state = $_POST['state'];
    }
    if (isset($_POST['city'])) {
        $city = $_POST['city'];
    }
    if (isset($_POST['zipCode'])) {
        $zipCode = $_POST['zipCode'];
    }
    $imagename  = "";
    $targetPath = "";
    
    
    if ($_POST['imageName'] != "") {
        $imagename  = $_POST['imageName'];
        $targetPath = str_replace(" ", "", $imagename); // remove space
    } else {
        $targetPath = "";
    }
    
    //insert
    $sql    = "INSERT INTO hospital_profile(hospital_name, address, country_id, state_id, city_id,
            phone_number, email,images_path,zipcode,created_on,updated_on,created_by,updated_by,status) 
            VALUES ('" . $hospitalName . "','" . $address . "', '" . $country . "', '" . $state . "','" . $city . "','" . $phone . "', '" . $email . "','" . $targetPath . "',
            '" . $zipCode . "',UNIX_TIMESTAMP(),UNIX_TIMESTAMP(),'".$userId."','".$userId."','A')";
    $result = mysqli_query($conn, $sql);
    if ($result) {
		if($targetPath != "")
		{
			$_SESSION['hpImage'] = $targetPath;
		}
        echo $result;
    } else {
        echo "";
    }
}

if ($operation == "show") {
    $sql      = "select * from hospital_profile";
    $result   = mysqli_query($conn, $sql);
    $rowcount = mysqli_num_rows($result);
    if ($rowcount > 0) {
        $rows = array();
        while ($r = mysqli_fetch_assoc($result)) {
            $rows[] = $r;
        }
        print json_encode($rows);
    } else {
        echo "";
    }
}
if ($operation == "update") {
    
    if (isset($_POST['hospitalName'])) {
        $hospitalName = $_POST['hospitalName'];
    }
    if (isset($_POST['address'])) {
        $address = $_POST['address'];
    }
    if (isset($_POST['phone'])) {
        $phone = $_POST['phone'];
    }
    if (isset($_POST['email'])) {
        $email = $_POST['email'];
    }
    if (isset($_POST['country'])) {
        $country = $_POST['country'];
    }
    if (isset($_POST['state'])) {
        $state = $_POST['state'];
    }
    if (isset($_POST['city'])) {
        $city = $_POST['city'];
    }
    if (isset($_POST['zipCode'])) {
        $zipCode = $_POST['zipCode'];
    }
    $imageName  = "";
    $targetPath = "";
    
    if ($_POST['imagename'] != "") {
        $sqlselect    = "select images_path from hospital_profile";
        $resultselect = mysqli_query($conn, $sqlselect);
        while ($row = mysqli_fetch_array($resultselect)) {
            
            if ($row[0] == "null" || $row[0] == "") {
				$imageName  = $_POST['imagename'];
				$targetPath = str_replace(" ", "", $imageName); // remove space
            } else {
                $path = "../../images";
                unlink($path . "/" . $row[0]); // delete image file 
				$imageName  = $_POST['imagename'];
				$targetPath = str_replace(" ", "", $imageName); // remove space
            }
        }
        $sql = "UPDATE hospital_profile SET hospital_name ='" . $hospitalName . "',   
                address = '" . $address . "', country_id = '" . $country . "', 
                state_id ='" . $state . "',city_id='" . $city . "',phone_number='" . $phone . "', email='" . $email . "', images_path='" . $targetPath . "' ,
                zipcode = '".$zipCode."',updated_on = UNIX_TIMESTAMP(),updated_by = '".$userId."',status = 'A'";
			if($targetPath != "")
			{
				$_SESSION['hpImage'] = $targetPath;
			}
    } else {
        
        $sql = "UPDATE hospital_profile SET hospital_name ='" . $hospitalName . "',  
            address = '" . $address . "', country_id = '" . $country . "',
            state_id ='" . $state . "', city_id='" . $city . "',phone_number='" . $phone . "', email='" . $email . "' , zipcode = '".$zipCode."',updated_on = UNIX_TIMESTAMP(),updated_by = '".$userId."',status = 'A' ";
    }
    $result = mysqli_query($conn, $sql);
    if ($result) {
        echo "$result";
    } else {
        echo "";
    }
}
?>