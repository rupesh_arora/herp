<?php
/*File Name  :   bed.php
Company Name :   Qexon Infotech
Created By   :   Rupesh Arora
Created Date :   30th Dec, 2015
Description  :   This page manages AND add beds*/

session_start(); //start session

/*include config file*/
include 'config.php';

$operation   = "";

$date      = date("Y-m-d");
$createOn  = new DateTime();
	
/*checking operation set or not*/
if (isset($_POST['operation'])) {
    $operation = $_POST["operation"];
} else if (isset($_GET["operation"])) {
    $operation = $_GET["operation"];
}

if (isset($_SESSION['globaluser'])) {
    $userId = $_SESSION['globaluser'];
}
else{
    exit();
}

/*operation to show active data*/
if ($operation == "show") {
    
    $query        = "SELECT (select value from configuration where name = 'ipd_prefix') AS ipdPrefix,bipd.id AS bed_history_id, ipd.*,(CASE WHEN bipd.ic_icu = '0' THEN 'NO' ELSE 'YES' END) AS is_icu, w.name,d.department,wr.number,b.number AS bedName,CONCAT(p.salutation,' ',p.first_name,' ',p.middle_name,' ',p.last_name) AS patient_name 
        ,p.dob,b.ward_id,b.room_id,b.department_id,b.id AS bed_id,b.price,(select value from configuration where name = 'patient_prefix') as prefix FROM ipd_registration AS ipd 
        LEFT JOIN beds_ipd_history AS bipd ON bipd.ipd_id = ipd.id
        LEFT JOIN beds AS b ON b.id = bipd.bed_id  
        LEFT JOIN ward_room AS wr ON wr.id = b.room_id
        LEFT JOIN wards AS w ON w.id = b.ward_id
        LEFT JOIN patients AS p ON p.id = ipd.patient_id 
        LEFT JOIN department AS d ON d.id = b.department_id";
    $result       = mysqli_query($conn, $query);
    $totalrecords = mysqli_num_rows($result);
    $rows         = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    /*JSON encode*/
    $json = array(
        'sEcho' => '1',
        'iTotalRecords' => $totalrecords,
        'iTotalDisplayRecords' => $totalrecords,
        'aaData' => $rows
    );
    echo json_encode($json);
}

if ($operation == "update") {
	$ICU = $_POST['ICU'];
	$bedId = $_POST['bedId'];
	$ipdId = $_POST['ipdId'];
	$patientId = $_POST['patientId'];
	$price = $_POST['price'];
	$oldBedId  = $_POST['oldBedId'];
	$bedHistoryId  = $_POST['bedHistoryId'];
	$oldbedPrice  = $_POST['oldbedPrice'];
	$transactionData = "";
	
	$queryCashAccount = "select sum(cash_account.credit - cash_account.debit) as amount from cash_account
						WHERE patient_id = ".$patientId."";
	$resultCashAccount = mysqli_query($conn,$queryCashAccount);
	while($row = mysqli_fetch_row($resultCashAccount)) {
		$bedCost = $row['0'];
	}
	if($bedCost >= 0) {
		$updateBedQuery = "UPDATE beds_ipd_history SET ipd_id= '" . $ipdId . "',ic_icu= '" . $ICU . "',bed_id= '" . $bedId . "',
							updated_on='" . $createOn->getTimestamp() . "',updated_by='" . $userId . "' 
							where id = '" . $bedHistoryId . "'";
		
		$resultBed = mysqli_query($conn,$updateBedQuery);
		if($resultBed) {
			// fetch bed price
			$updateOldBedStatus = "UPDATE beds SET bed_availability = '1' where id = ".$oldBedId."";
			$resultOldBedStatus = mysqli_query($conn,$updateOldBedStatus);
			
			$updateBedStatus = "UPDATE beds SET bed_availability = '0' where id = ".$bedId."";
			$resultBedStatus = mysqli_query($conn,$updateBedStatus);
			
			$selectBedPrice = "SELECT price from beds WHERE id = ".$bedId."";
			$resultBedPrice = mysqli_query($conn,$selectBedPrice);
			while ($r = mysqli_fetch_assoc($resultBedPrice)) {
				$bedPrice = $r['price'];
			}
			if($bedPrice == $price) {
				
				$queryGetAccountNo = "SELECT fan_id from patients WHERE id = ".$patientId."";
                $resultAccount = mysqli_query($conn,$queryGetAccountNo);
                while ($r = mysqli_fetch_assoc($resultAccount)) {
                    $familyAccountNo = $r['fan_id'];
                }
                if($familyAccountNo != ""){
                    $AccountNo = $familyAccountNo;
                }
                else{
                    $AccountNo = $patientId;
                }
				
				$insertCashCredit = "INSERT INTO cash_account (patient_id,depositor_id,credit,debit,date,created_on,created_by) VALUES 
						('".$patientId."','".$AccountNo."',".$oldbedPrice.",0,'".$date."','" . $createOn->getTimestamp() . "','" . $userId . "')";
				 
				$resultCredit = mysqli_query($conn,$insertCashCredit);
				
				// return cash deposite transaction
				/* $insertTransactionCredit = "INSERT INTO transactions (patient_id,transaction_type_id,credit,debit,processed_on,processed_by,details)
						values ('" . $patientId . "',' 7 ',".$oldbedPrice.",'0',UNIX_TIMESTAMP(),'".$userId ."','Refund Cash.')"; 
				mysqli_query($conn,$insertTransactionCredit) */;	
				
				$insertCash = "INSERT INTO cash_account (patient_id,depositor_id,credit,debit,date,created_on,created_by) VALUES 
						('".$patientId."','".$AccountNo."',0,".$price.",'".$date."','" . $createOn->getTimestamp() . "','" . $userId . "')";
					
				$resultCash = mysqli_query($conn,$insertCash);
				
				//$transactionData = $transactionData.'IPD Id: '.$ipdId.',Hospitalization Charge: '.$price.'.';
				// cash deposite transaction
				/* $insertTransaction = "INSERT INTO transactions (patient_id,transaction_type_id,credit,debit,processed_on,processed_by,details)
						values ('" . $patientId . "',' 7 ','0','" .$price. "',UNIX_TIMESTAMP(),'".$userId ."','".$transactionData."')"; 
				mysqli_query($conn,$insertTransaction);	 */
				
				if($resultCash) {
					echo $resultCash;
				}
			}
		}
	}
	else {
		echo "0";
	}
}

// search
// search patient opearion
if ($operation == "search") {
    
	if (isset($_POST['IPDId'])) {
        $IPDId = $_POST["IPDId"];
    }
    if (isset($_POST['firstName'])) {
        $firstName = $_POST["firstName"];
    }
    if (isset($_POST['lastName'])) {
        $lastName = $_POST["lastName"];
    }
    if (isset($_POST['mobile'])) {
        $mobile = $_POST["mobile"];
    }
	
	        $isFirst = "false";
    
        $query = "SELECT (select value from configuration where name = 'ipd_prefix') AS ipdPrefix,bipd.id AS bed_history_id, ipd.*,(CASE WHEN bipd.ic_icu = '0' THEN 'NO' ELSE 'YES' END) AS is_icu, w.name,d.department,wr.number,b.number AS bedName,CONCAT(p.salutation,' ',p.first_name,' ',p.middle_name,' ',p.last_name) AS patient_name 
					,p.dob,b.ward_id,b.room_id,b.department_id,b.id AS bed_id,b.price,(select value from configuration where name = 'patient_prefix') as prefix FROM ipd_registration AS ipd 
					LEFT JOIN beds_ipd_history AS bipd ON bipd.ipd_id = ipd.id
					LEFT JOIN beds AS b ON b.id = bipd.bed_id  
					LEFT JOIN ward_room AS wr ON wr.id = b.room_id
					LEFT JOIN wards AS w ON w.id = b.ward_id
					LEFT JOIN patients AS p ON p.id = ipd.patient_id 
					LEFT JOIN department AS d ON d.id = b.department_id";
        
        if ($IPDId != '' || $firstName != '' || $lastName != '' || $mobile != '') {
            
        
            if ($IPDId != '') {
                $ipdPrefix = $_POST['ipdPrefix'];
                $sql       = "SELECT value FROM configuration WHERE name = 'ipd_prefix'";
                $result    = mysqli_query($conn, $sql);
                
                while ($r = mysqli_fetch_assoc($result)) {
                    $prefix = $r['value'];
                }
                
                if($prefix == $ipdPrefix){

                    if ($isFirst != "false") {
                        $query .= " AND ";
                    }
                    else{
                        $query .= " WHERE ";
                    }
                    $query .= " ipd.id = '" . $IPDId . "'";
                    $isFirst = "true";
                }
            }
            if ($firstName != '') {
                if ($isFirst != "false") {
                    $query .= " AND ";
                }
                else{
                    $query .= " WHERE ";
                }
                $query .= " p.first_name LIKE '%" . $firstName . "%'";
                $isFirst = "true";
            }
            if ($lastName != '') {
                if ($isFirst != "false") {
                    $query .= " AND ";
                }
                else{
                    $query .= " WHERE ";
                }
                $query .= " p.last_name LIKE '%" . $lastName . "%'";
                $isFirst = "true";
            }
            if ($mobile != '') {
                if ($isFirst != "false") {
                    $query .= " AND ";
                }
                else{
                    $query .= " WHERE ";
                }
                $query .= " p.mobile = '" . $mobile . "'";
                $isFirst = "true";
            }
        }
     
    $result = mysqli_query($conn, $query);
    //$totalrecords = mysqli_num_rows($result);
    $rows   = array();
    while ($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
    }
    //$json = array('sEcho' => '1', 'iTotalRecords' => $totalrecords, 'iTotalDisplayRecords' => $totalrecords, 'aaData' => $rows);
    print json_encode($rows);
}
?>