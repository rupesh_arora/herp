<?php 
session_start();
require('./controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('checklogin.php');
}
else{
	header('Location: login.php');
}
?>
<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title><?php echo $software_name;?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="./img/favicon.ico" />
        <link rel="stylesheet" href="./css/bootstrap.min.css" />
		<link href="css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="./css/plugins.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
        <!-- <link rel="stylesheet" href="./css/multiselect.css" /> -->
        <link rel="stylesheet" href="./css/main.css" />
        <link rel="stylesheet" href="./css/themes.css" />
        <script src="./script/vendor/jquery-1.11.3.min.js"></script>
  		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="./script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
        <script src="./script/vendor/bootstrap.min.js"></script>
        <script src="./script/plugins.js"></script>
        <script src="./script/google-analytics.js" async="true"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
        <script src="./script/jquerysession.js"></script>
        <script src="./script/app.js"></script>
        <script src="./script/pages/index.js"></script>
        <script src="./script/main.js"></script>
		<script src="script/admin/common.js"></script>
        <script type="text/javascript" src="./script/chart-loader.js"></script>
        <!-- script for export to excel file-->
        <script type="text/javascript" src="./script/excel-import_1.js"></script>
        <script type="text/javascript" src="./script/excel-import_2.js"></script>

		<script type="text/javascript">
			google.charts.load("current", {packages:["corechart"]});		      
		</script>
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
				<?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>
				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="row">
								<div class="page-header">
									<h1>Dashboard</h1>
								</div>
							</div>								
							<div class="row">
								<div class="dashBoardContainer">
									<div class="dashBoardTab" data-url="patient_registration.html" id="patientRegistrationDashboard">
										<p class="tabName customise480">Patient Registration</p>
										<img src="images/dashboard/patient_reg.png" class="" id="regPatientIcon">
									</div>
									<div class="dashBoardTab" data-url="appointment.php" id="appointmentDashboard">
										<p class="tabName customise480">Appointment Management</p>
										<img src="images/dashboard/appointment.gif" class="" id="margBot">
									</div>
									<div class="dashBoardTab" data-url="opd_reg.html" id="OPDRegDashboard">
										<p class="tabName">OPD Booking</p>
										<img src="images/dashboard/opd_booking.gif" class="" id="margBot">
									</div>
									<div class="dashBoardTab" data-url="triage.html" id="NurseDashboard">
										<p class="tabName customise480">Triage/Nursing Station</p>
										<img src="images/dashboard/nurse.gif" class="">
									</div>
									<div class="dashBoardTab" data-url="consultant.html" id="consultantDashboard">
										<p class="tabName">Doctor Station</p>
										<img src="images/dashboard/doctor_icon.png" class="">
									</div>
									<div class="dashBoardTab" data-url="lab_test.html" id="laboratoryDashboard">
										<p class="tabName">Laboratory</p>
										<img src="images/dashboard/lab.gif" class="">
									</div>
									<div class="dashBoardTab" data-url="radiology_test.html" id="radiologyDashboard">
										<p class="tabName">Radiology</p>
										<img src="images/dashboard/Radiology.png" class="" id="radiologyBooking">
									</div>
									<div class="dashBoardTab" data-url="pharmacy_dispense.html" id="pharmacyDashboard">
										<p class="tabName">Pharmacy</p>
										<img src="images/dashboard/pharmacy.gif" class="" >
									</div>
									<div class="dashBoardTab" data-url="patient_ot_reg.html" id="OTDashboard">
										<p class="tabName">Theatre</p>
										<img src="images/dashboard/OT.gif" class="">
									</div>
									<div class="dashBoardTab" data-url="cash.html" id="billingDashboard">
										<p class="tabName">Patient Billing</p>
										<img src="images/dashboard/patient_billing.png" class="">
									</div>
									<div class="dashBoardTab" data-url="patient_hospitalization.html" id="IPDDashboard">
										<p class="tabName customise480">Inpatient Booking</p>
										<img src="images/dashboard/in_patient_booking.gif" class="" id="patientBooking">
									</div>
									<div class="dashBoardTab" data-url="view_attendance.html" id="HRMDashboard">
										<p class="tabName">HRM</p>
										<img src="images/dashboard/HR_payroll.gif" class="" id="patientBooking">
									</div>
									<div class="dashBoardTab" data-url="cash_payment.html" id="accountsDashboard">
										<p class="tabName">Account/Finance</p>
										<img src="images/dashboard/account_finance.png" class="">
									</div>
									<div class="dashBoardTab" data-url="department_wise_report.html" id="MISDashboard">
										<p class="tabName">MIS(Reports)</p>
										<img src="images/dashboard/MIS-reprots.gif" class="">
									</div>									
									<div class="dashBoardTab" data-url="access_management.html" id="setUpDashboard">
										<p class="tabName customise480">Setup & Configuration</p>
										<img src="images/dashboard/set_configuration.png" class="">
									</div>
								</div>
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>

                <!-- Check access of dashboard -->
                <?php
				include 'controllers/admin/config.php';
				$query = "SELECT pages.screen_load_url as link FROM `pages_options` LEFT JOIN pages ON pages.id=pages_options.screen_id WHERE user_id = ".$_SESSION['globaluser']." AND view = 0  ORDER BY pages.sort_order";
				$result       = mysqli_query($conn, $query);
				$link         = array();
		        while ($rows = mysqli_fetch_assoc($result)) {
		            $link[] = $rows['link'];
		        }
		        ?>
		        <script type="text/javascript">
		        	var notAccessibleLink = '';
		        	var dashBoardTiles = [];
		        	var differnceArray = '';
		        	$.each($('.dashBoardTab'),function (i,v) {
		        		dashBoardTiles.push($($('.dashBoardTab')[i]).attr('data-url'));
		        	});		        	
				    notAccessibleLink = <?php echo json_encode($link); ?>;				    
				    
				    //This will give the array that match in both
					$.arrayIntersect = function(a, b)
					{
					    return $.grep(b, function(i)
					    {
					        return $.inArray(i, a) > -1;
					    });
					};
					
					differnceArray =  $.arrayIntersect(notAccessibleLink, dashBoardTiles);

					//After that remove that tiles that are same 
					$.each(differnceArray,function(i,v){
						$("div[data-url='"+v+"']").remove();
					});
				</script>
				
                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="./img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        <div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style="width:100% !important;">
                    <div class="modal-header text-center">
                        <h4 class="modal-title"><i class="fa fa-pencil"></i>Change Password</h2>
                    </div>
                    <div class="modal-body-frame">
                        <form  method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" action="" />
                            <fieldset>
                                <legend>Personal Information</legend>
								<div class= "row" style="text-align:center;">
									<div  class="" >
										<div style="margin-top:20px;" >
											<div id="image-box" style="width: 100px; height:100px; border: 1px solid #C0C0C0;background-position: center; background-size: 100% 100%;display: inline-block; cursor: default;">
												<?php if($_SESSION["image"] != null){
													?>
													<img id="user_reg_image" src="./upload_images/staff_dp/<?php echo $_SESSION["image"];?>" alt="Default" style="position: relative;height: 100%;width:100%;">
												<?php
												}
												else{
													?>
													<img id="user_reg_image" src="./img/default.jpg" alt="Default" style="position: relative;height: 100%; width:100%;">
												<?php
												}
												?>
												<!-- <div id="imagetext" style="text-align: center;POSITION: relative; top: -37%;" >
													Click on Upload Image
												</div>  -->                         
											</div>  
											<input type="file" id="upload" name="upload" class="hidden" accept="image/*"> 
										</div>
									</div>
								</div>
								<div class="row">
									<div  class="col-md-6" style="width:50% !important;">
										<div class="form-group">
											<label class="col-md-6 control-label" style="width:50% !important;">Username</label>
											<div class="col-md-6" style="width:50% !important;">
												<p class="form-control-static"><?php echo $_SESSION['globalName']; ?></p>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-6 control-label" style="width:50% !important;" for="user-settings-email">Email</label>
											<div class="col-md-6" style="width:50% !important;">
												<label class="control-label" for="val_email" id="setting-email" ><?php echo $_SESSION['globalEmail']; ?></label>
											</div>
										</div>
									</div>
								</div>
                                
                            </fieldset>
                            <fieldset>
                                <legend>Password Update</legend>
								<div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-password">Old Password<span class="text-danger">*</span></label>
                                    <div class="col-md-8">
                                        <input type="password" id="user-settings-old-password" name="user-settings-old-password" class="form-control" placeholder="Please enter old password.." />
                                    </div>
									<span id="errorOldPwd" style="color:#a94442;margin-left: 36%;margin-top:8px;display:none;"></span>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-password">New Password<span class="text-danger">*</span></label>
                                    <div class="col-md-8">
                                        <input type="password" id="user-settings-password" name="user-settings-password" class="form-control" maxlength="10" placeholder="Please choose a complex one.." />
                                    </div>
									<span id="errorNewPwd" style="color:#a94442;margin-top:8px;margin-left: 36%;display:none;"></span>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-repassword">Confirm New Password<span class="text-danger">*</span></label>
                                    <div class="col-md-8">
                                        <input type="password" id="user-settings-repassword" name="user-settings-repassword" class="form-control" maxlength="10" placeholder="..and confirm it!" />
                                    </div>
									<span id="errorConfirmPwd" style="color:#a94442;margin-top:8px;margin-left: 36%;display:none;"></span>
                                </div>
								<span id="errorMessage" style="color:#a94442;margin-top:8px;margin-left: 36%;display:none;"></span>
                            </fieldset>
                            <div class="form-group form-actions">
                                <div class="col-xs-12 text-right">
                                    <button type="button" id="close" class="btn btn-sm btn-info" style="background: #0C71C8;" data-dismiss="modal">Close</button>
                                    <button type="button" id="submit" class="btn btn-sm btn-primary" style="background: #0C71C8;">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
		
		<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        <div id="modal-user-profile" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style="width:100% !important;">
                    <div class="modal-header text-center">
                        <h4 class="modal-title"><i class="fa fa-pencil"></i>My Profile </h2>
                    </div>
                    <div class="modal-body-frame">
                        <form  method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" action="" />
                            <fieldset>
                                <legend>Personal Information</legend>
								<div class= "row" style="text-align: center;">
									<div  class="">
										<div style="margin-top:20px;" >
											<div id="image-box" style="width: 100px; height:100px; border: 1px solid #C0C0C0;background-position: center; background-size: 100% 100%;display: inline-block; cursor: default;">
												<?php if($_SESSION["image"] != null){
													?>
													<img id="user_reg_image" src="./upload_images/staff_dp/<?php echo $_SESSION["image"];?>" alt="Default" style="position: relative;height: 100%;width:100%;">
												<?php
												}
												else{
													?>
													<img id="user_reg_image" src="./img/default.jpg" alt="Default" style="position: relative;height: 100%;width:100%;">
												<?php
												}
												?>
												<!-- <div id="imagetext" style="text-align: center;POSITION: relative; top: -37%;" >
													Click on Upload Image
												</div>  -->                         
											</div>  
											<input type="file" id="upload" name="upload" class="hidden" accept="image/*"> 
										</div>
									</div>
								</div>
								<div class="row">
									<div  class="col-md-6" style="width:50% !important;">
										<div class="form-group">
											<label class="col-md-6 control-label" style="width:50% !important;">Username</label>
											<div class="col-md-6" style="width:50% !important;">
												<p class="form-control-static"><?php echo $_SESSION['globalName']; ?></p>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-6 control-label" style="width:50% !important;" for="user-settings-email">Email</label>
											<div class="col-md-6" style="width:50% !important;">
												<label class="control-label" for="val_email" id="setting-email" ><?php echo $_SESSION['globalEmail']; ?></label>
											</div>
										</div>
									</div>
									
								</div>
                                
                            </fieldset>
                           
                            <div class="form-group form-actions">
                                <div class="col-xs-12 text-right">
                                    <button type="button" id="close" class="btn btn-sm btn-info" style="background: #0C71C8;" data-dismiss="modal">Close</button>
                                   
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
		
		
		 <!--for update content Modal -->
		<div class="modal fade" id="myModal" role="dialog">
		    <a href="#myModal" id="modal" role="button" class="hidden" data-toggle="modal"></a>
			<div class="modal-dialog" id="modal-dialog-style-2">			
			  <!-- Modal content-->
			    <div class="modal-content modal-confirm">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><div id="pop_up_close" class="close-modal">X</div></button>
						<h4 id="myModalLabel"></h4>
					</div>
					<div class="modal-body" id="myModalBody">					
					</div>
					<div class="modal-footer">
						
					</div> 
			  </div>			  
			</div>
		</div>	

		<!--Confirm  Modal -->
		<div class="modal fade" id="confirmmyModal" role="dialog">
			<div id="confirm_popup">
				<a href="#confirmmyModal" id="myConfirmModal" role="button" class="hidden" data-toggle="modal"></a>
				<div class="modal-dialog confirm-modal-style">			
				  <!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close close-confirm" id="pop_up_close" data-dismiss="modal">×</button>
							<h4 id="confirmMyModalLabel"></h4>
						</div>
						<div class="modal-body selfRequestConfirm-body" id="myConfirmModalBody"><!-- Add one extra class to clear div for another modal -->					
						</div>
						<div class="modal-footer">
							<input type="button" class="btn" data-dismiss="modal" aria-hidden="true" value="Confirm" id="confirm" />
							<input type="button" class="btn" style="margin-bottom: 2px !important;" data-dismiss="modal" aria-hidden="true" value="Cancel" id="cancel" />
						</div>
				  </div>			  
				</div>
			</div>    
		</div>	
		<!-- message pop up -->
		<div class="modal fade" id="messagemyModal" role="dialog">
			<div id="message_popup">
				<a href="#messagemyModal" id="mymessageModal" role="button" class="hidden" data-toggle="modal"></a>
				<div class="modal-dialog confirm-modal-style">			
				  <!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" id="pop_up_close" data-dismiss="modal">×</button>
							<h4 id="messageMyModalLabel"></h4>
						</div>
						<div class="modal-body" id="messageBody">					
						</div>
						<div class="modal-footer" style="">
							<input type="button" class="btn" data-dismiss="modal" aria-hidden="true" value="OK" id="ok"  />
						</div>
				  </div>			  
				</div>
			</div>    
		</div>	
		
		<!--Confirm  Modal -->
		<div class="modal fade" id="confirmUpdateModal" role="dialog">
			<div id="confirm_popup">
				<a href="#confirmUpdateModal" id="updateConfirmModal" role="button" class="hidden" data-toggle="modal"></a>
				<div class="modal-dialog confirm-modal-style">			
				  <!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close close-confirm" id="pop_up_close" data-dismiss="modal">×</button>
							<h4 id="confirmUpdateModalLabel">Confirmation</h4>
						</div>
						<div class="" id="updateBody">					
						</div>
						<div class="modal-footer">
							<input type="button" class="btn" data-dismiss="modal" aria-hidden="true" value="Confirm" id="btnConfirm" />
							<input type="button" class="btn" style="margin-bottom: 2px !important;" data-dismiss="modal" aria-hidden="true" value="Cancel" id="cancel" />
						</div>
				  </div>			  
				</div>
			</div>    
		</div>	
		
		<div class="modal fade" id="myNotificationModal" role="dialog">
		    <a href="#myNotificationModal" id="modal" role="button" class="hidden" data-toggle="modal"></a>
			<div class="modal-dialog" id="modal-dialog-style-2">			
			  <!-- Modal content-->
			    <div class="modal-content modal-confirm">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><div id="pop_up_close" class="close-modal">X</div></button>
						<h4 id="myNotificationModalLabel"></h4>
					</div>
					<div class="modal-body" id="notificationModalBody"></div>
			  	</div>			  
			</div>
		</div>	
    </body>
</html>