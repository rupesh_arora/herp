<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>
<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpGettingStarted').addClass("open");
				$('#helpGettingStartedUl').css("display","block");
				$('#helpServicesSetup').addClass("active");
			});
			
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>
				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Services Setup</h3>
								<h6 align="center" id="helpServiceType"><a href="#helpServiceType">Services Types</a> | <a href="#helpServicesList">Services List</a> | <a href="#helpProceduresList">Procedures List</a></h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11">
								<p>
								In Services Setup section, we setup some data in this system related to the services and procedures offered by the hospital before using the software. 
								</p>
								<p class="heading1">1. Services Type</p>
								<p>In this section, we define service types offered by the hospital.</p>
								<p>
									<span class="heading2">
										Add Service Type
									</span><br/>
									To add any service type, you go to the Service Types page and click on <strong> +Add Service Type </strong> tab. <br/>There you can fill in the form following fields:
									<ol>
										<li>
											Service Type - Name of type of Service. [mandatory field]
										</li>
										<li>
											Description - About the service type. [optional disease]
										</li>
									</ol>									
								</p>
								<p class="image">
									<img src="images/034.Services.Type.Save.png" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
								<span class="heading2">View Service Types</span><br/>
								This page shows whole list of Service Types defined in system.
								</p>
								<p class="image">
									<img src="images/033.Services.Type.List.png" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpServicesList">
								</p>
								<p class="heading1">2. Service List</p>
								<p>
								In here you can maintain different types of Services offered by the hospital.
								</p>
								<p>
								<span class="heading2">Add Service</span><br/>
								Here you can add a Service on the system. On the save form you can enter following fields:
								<ol>
									<li>
									Service Type - Select the service type in which this service lies. [mandatory fields]
									</li>
									<li>
									<li>
									Code - A unique code for this service. [mandatory fields]
									</li>
									<li>
									Name - Name of the service. [mandatory field]
									</li>
									<li>
									Unit Cost/Price - It is the price of that service. [mandatory fields]
									</li>
									Description - Info about this service. [optional field]
									</li>
								</ol>
								</p>
								<p class="image">
								<img src="images/036.Services.Save.png" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
								<span class="heading2">View Services</span><br/>
								Here you can see all defined Services present in system.
								</p>
								<p class="image">
								<img src="images/035.Services.List.png" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpProceduresList">
								</p>
								<p class="heading1">
								3. Procedures List
								</p>
								<p>
								This section maintains different Procedures offered by the hospital in the system.
								</p>	
								<p>
								<span class="heading2">Add Procedure</span><br/>
								To add a particular procedure in the system, you can access the save form by clicking the <strong>+Add Procedures</strong> tab. There you can fill the following data:
								<ol>
									<li>
										CPT Code - Unique code assigned to this particular procedure. [mandatory field]
									</li>
									<li>
										Name - Procedure name. [mandatory field]
									</li>
									<li>
										Short Name - Short procedure name. [optional field]
									</li>
									<li>
										Unit Price - Price for this particular procedure. [mandatory field]
									</li>
									<li>
										Description - About the procedure. [optional field]
									</li>
								</ol>
								</p>
								<p class="image">
									<img src="images/038.Procedures.Save.png" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
								<span class="heading2">View Procedures</span><br/>
								Here you see all the procedures saved in the system.
								</p>
								<p class="image">
									<img src="images/037.Procedures.List.png" class="img-responsive" />
								</p>
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>

                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>