<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>
<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	<title><?php echo $software_name;?> Help Manual</title>
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title><?php echo $software_name;?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/modernizer-loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>
        <script src="../script/plugins.js"></script>
        <script src="scripts/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			
			
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center"><?php echo $software_name;?> Manual</h3>
							</div>
							<div class="row">
								<div class="col-md-1 col-lg-1">
								</div>
								<div class="col-md-10 col-lg-10">
									<p><i>This is the manual for the <?php echo $software_name?> software. Here you can find all the flow, procedures and functionality of the software.</i></p>
									
									<h4><?php echo $software_name;?></h4>
									<ul>
										<li>
											<?php echo $software_name?> streamlines and integrate the operation processes and information flow in the
hospital to synergize the resources namely men, material, money and equipments through
information.<br><br>
										</li>
										<li>
											This ERP facilitates hospital-wide Integrated Information System covering all functional
areas like Patients Billing &amp; Management, OPD &amp; IPD Management, Inventory Management, Appointment Booking, Staff Scheduling, Pharmacy, Pathology Laboratories, Radiology Laboratories etc.<br><br>
										</li>
										<li>
											It performs core hospital activities and increases customer service thereby augmenting the
overall Hospital Image. ERP bridges the information gap across the hospital.<br><br>
										</li>
										<li>
										<?php echo $software_name?> eliminates the most of the business problems like Material shortages,
Productivity enhancements, Customer service, Cash Management, Inventory problems,
Quality problems, Prompt delivery, Billing etc.<br><br>
										</li>
										<li>
										The data is stored in a single database providing real time data across applications
throughout the hospital and its affiliate clinics. Since all the data is stored centrally, it can
be viewed simultaneously from multiple terminals giving all departments’ access to timely,
up-to-date patient information.<br><br>
										</li>
										<li>
										As an area of Medical Informatics the aim of HealthLink is to achieve the best possible
support of patient care and administration by electronic data processing.
										</li>
									</ul>
								</div>
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>

                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        <div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style="width:100% !important;">
                    <div class="modal-header text-center">
                        <h4 class="modal-title"><i class="fa fa-pencil"></i>Change Password</h2>
                    </div>
                    <div class="modal-body-frame">
                        <form  method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" action="" />
                            <fieldset>
                                <legend>Personal Information</legend>
								<div class="row">
									<div  class="col-md-6" style="width:50% !important;">
										<div class="form-group">
											<label class="col-md-6 control-label" style="width:50% !important;">Username</label>
											<div class="col-md-6" style="width:50% !important;">
												<p class="form-control-static"><?php echo $_SESSION['globalName']; ?></p>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-6 control-label" style="width:50% !important;" for="user-settings-email">Email</label>
											<div class="col-md-6" style="width:50% !important;">
												<label class="control-label" for="val_email" id="setting-email" ><?php echo $_SESSION['globalEmail']; ?></label>
											</div>
										</div>
									</div>
									<div  class="col-md-6" style="width:50% !important;">
										<div style="margin-top:20px;" >
											<div id="image-box" style="width: 100px; height:100px; border: 1px solid #C0C0C0;background-position: center; background-size: 100% 100%;display: inline-block; cursor: default;">
												<?php if($_SESSION["image"] != null){
													?>
													<img id="user_reg_image" src="../images/<?php echo $_SESSION["image"];?>" alt="Default" style="position: relative;height: 100%;width:100%;">
												<?php
												}
												else{
													?>
													<img id="user_reg_image" src="../img/default.jpg" alt="Default" style="position: relative;height: 100%; width:100%;">
												<?php
												}
												?>
												<!-- <div id="imagetext" style="text-align: center;POSITION: relative; top: -37%;" >
													Click on Upload Image
												</div>  -->                         
											</div>  
											<input type="file" id="upload" name="upload" class="hidden" accept="image/*"> 
										</div>
									</div>
								</div>
                                
                            </fieldset>
                            <fieldset>
                                <legend>Password Update</legend>
								<div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-password">Old Password<span class="text-danger">*</span></label>
                                    <div class="col-md-8">
                                        <input type="password" id="user-settings-old-password" name="user-settings-old-password" class="form-control" placeholder="Please enter old password.." />
                                    </div>
									<span id="errorOldPwd" style="color:#a94442;margin-left: 36%;margin-top:8px;display:none;"></span>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-password">New Password<span class="text-danger">*</span></label>
                                    <div class="col-md-8">
                                        <input type="password" id="user-settings-password" name="user-settings-password" class="form-control" maxlength="10" placeholder="Please choose a complex one.." />
                                    </div>
									<span id="errorNewPwd" style="color:#a94442;margin-top:8px;margin-left: 36%;display:none;"></span>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-repassword">Confirm New Password<span class="text-danger">*</span></label>
                                    <div class="col-md-8">
                                        <input type="password" id="user-settings-repassword" name="user-settings-repassword" class="form-control" maxlength="10" placeholder="..and confirm it!" />
                                    </div>
									<span id="errorConfirmPwd" style="color:#a94442;margin-top:8px;margin-left: 36%;display:none;"></span>
                                </div>
								<span id="errorMessage" style="color:#a94442;margin-top:8px;margin-left: 36%;display:none;"></span>
                            </fieldset>
                            <div class="form-group form-actions">
                                <div class="col-xs-12 text-right">
                                    <button type="button" id="close" class="btn btn-sm btn-info" style="background: #0C71C8;" data-dismiss="modal">Close</button>
                                    <button type="button" id="submit" class="btn btn-sm btn-primary" style="background: #0C71C8;">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
		
		<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        <div id="modal-user-profile" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style="width:100% !important;">
                    <div class="modal-header text-center">
                        <h4 class="modal-title"><i class="fa fa-pencil"></i>My Profile </h2>
                    </div>
                    <div class="modal-body-frame">
                        <form  method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" action="" />
                            <fieldset>
                                <legend>Personal Information</legend>
								<div class="row">
									<div  class="col-md-6" style="width:50% !important;">
										<div class="form-group">
											<label class="col-md-6 control-label" style="width:50% !important;">Username</label>
											<div class="col-md-6" style="width:50% !important;">
												<p class="form-control-static"><?php echo $_SESSION['globalName']; ?></p>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-6 control-label" style="width:50% !important;" for="user-settings-email">Email</label>
											<div class="col-md-6" style="width:50% !important;">
												<label class="control-label" for="val_email" id="setting-email" ><?php echo $_SESSION['globalEmail']; ?></label>
											</div>
										</div>
									</div>
									<div  class="col-md-6" style="width:50% !important;">
										<div style="margin-top:20px;" >
											<div id="image-box" style="width: 100px; height:100px; border: 1px solid #C0C0C0;background-position: center; background-size: 100% 100%;display: inline-block; cursor: default;">
												<?php if($_SESSION["image"] != null){
													?>
													<img id="user_reg_image" src="../images/<?php echo $_SESSION["image"];?>" alt="Default" style="position: relative;height: 100%;width:100%;">
												<?php
												}
												else{
													?>
													<img id="user_reg_image" src="../img/default.jpg" alt="Default" style="position: relative;height: 100%;width:100%;">
												<?php
												}
												?>
												<!-- <div id="imagetext" style="text-align: center;POSITION: relative; top: -37%;" >
													Click on Upload Image
												</div>  -->                         
											</div>  
											<input type="file" id="upload" name="upload" class="hidden" accept="image/*"> 
										</div>
									</div>
								</div>
                                
                            </fieldset>
                           
                            <div class="form-group form-actions">
                                <div class="col-xs-12 text-right">
                                    <button type="button" id="close" class="btn btn-sm btn-info" style="background: #0C71C8;" data-dismiss="modal">Close</button>
                                   
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
		
		
		 <!--for update content Modal -->
		<div class="modal fade" id="myModal" role="dialog">
		    <a href="#myModal" id="modal" role="button" class="hidden" data-toggle="modal"></a>
			<div class="modal-dialog" style="width:750px;">			
			  <!-- Modal content-->
			    <div class="modal-content modal-confirm" style="width:100%;">
					<div class="modal-header" style="background-color: #6fcceb; color: darkgreen;">
						<button type="button" class="close" data-dismiss="modal"><div id="pop_up_close" class="close-modal">X</div></button>
						<h4 id="myModalLabel"></h4>
					</div>
					<div class="modal-body">					
					</div>
					<div class="modal-footer" style="background-color: #6fcceb; color: darkgreen;">
						
					</div> 
			  </div>			  
			</div>
		</div>	

		<!--Confirm  Modal -->
		<div class="modal fade" id="confirmmyModal" role="dialog">
			<div id="confirm_popup">
				<a href="#confirmmyModal" id="myConfirmModal" role="button" class="hidden" data-toggle="modal"></a>
				<div class="modal-dialog">			
				  <!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header" style="background-color: #6fcceb; color: darkgreen;">
							<button type="button" class="close close-confirm" id="pop_up_close" data-dismiss="modal">×</button>
							<h4 id="confirmMyModalLabel"></h4>
						</div>
						<div class="modal-body selfRequestConfirm-body"><!-- Add one extra class to clear div for another modal -->					
						</div>
						<div class="modal-footer" style="background-color: #6fcceb; color: darkgreen;">
							<input type="button" class="btn" data-dismiss="modal" aria-hidden="true" value="Confirm" id="confirm" />
							<input type="button" class="btn" style="margin-bottom: 2px !important;" data-dismiss="modal" aria-hidden="true" value="Cancel" id="cancel" />
						</div>
				  </div>			  
				</div>
			</div>    
		</div>	
		<!-- message pop up -->
		<div class="modal fade" id="messagemyModal" role="dialog">
			<div id="message_popup">
				<a href="#messagemyModal" id="mymessageModal" role="button" class="hidden" data-toggle="modal"></a>
				<div class="modal-dialog">			
				  <!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" id="pop_up_close" data-dismiss="modal">×</button>
							<h4 id="messageMyModalLabel"></h4>
						</div>
						<div class="modal-body">					
						</div>
						<div class="modal-footer" style="">
							<input type="button" class="btn" data-dismiss="modal" aria-hidden="true" value="OK" id="ok"  />
						</div>
				  </div>			  
				</div>
			</div>    
		</div>	
		
		<!--Confirm  Modal -->
		<div class="modal fade" id="confirmUpdateModal" role="dialog">
			<div id="confirm_popup">
				<a href="#confirmUpdateModal" id="updateConfirmModal" role="button" class="hidden" data-toggle="modal"></a>
				<div class="modal-dialog">			
				  <!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close close-confirm" id="pop_up_close" data-dismiss="modal">×</button>
							<h4 id="confirmUpdateModalLabel">Confirmation</h4>
						</div>
						<div class="" id="updateBody">					
						</div>
						<div class="modal-footer">
							<input type="button" class="btn" data-dismiss="modal" aria-hidden="true" value="Confirm" id="btnConfirm" />
							<input type="button" class="btn" style="margin-bottom: 2px !important;" data-dismiss="modal" aria-hidden="true" value="Cancel" id="cancel" />
						</div>
				  </div>			  
				</div>
			</div>    
		</div>	
		
    </body>
</html>