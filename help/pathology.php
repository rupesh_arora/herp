<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpPatient').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Diagnosis Center</h3>
								<h6 align="center">
									<a href="#helpViewLabRequest">View Lab Request</a> |
									<a href="#helpViewRadiologyRequest">View Radiology Request</a> |
									<a href="#helpViewForensicRequest">View Forensic Request</a> |									 
									<a href="#helpLabResult">Lab Result</a> |									 
									<a href="#helpRadiologyResult">Radiology Result</a> |									 
									<a href="#helpForensicResult">Forensic Result</a> |
									<a href="#helpViewLabResult">View Lab Result</a> |
									<a href="#helpViewRadiologyResult">View Radiology Result</a>  |
									<a href="#helpViewForensicResult">View Forensic Result</a>									
																	
								</h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11">
							
								<p>
									In the pathology section, User can do follwing operations.
									<ul>
										<li>View lab request of patient for proceed</li>
										<li>View radiology request of patient for proceed</li>
										<li>View forensic request of patient for proceed</li>											
										<li>Put the lab result of patient specimen</li>
										<li>Put the radiology result of patient request</li>
										<li>Put the forensic result of patient specimen</li>
										<li>View lab result status and print</li>
										<li>View radiology result status and print</li>
										<li>View forensic result status and print</li>
									</ul>
								</p>
								<p class="small-gap"  id="helpViewLabRequest">
								</p>
								<p class="heading1">
								1. View Lab Request
								</p>
								<p>
									In this section user can proceed of lab test request.
								</p>
								<span class="heading2"></span><br/>
									To load lab request details of patient into table.</br>
															
								<p class="image">
								<img src="images/pathology001.png" class="img-responsive" />
								</p>
								<p>
									Then click on <strong>Process</strong> button to show popup with details of patient and then click <strong>Proceed</strong> to generate specimen id for the progress result.</br>	
								</p>
								<p class="small-gap"  id="helpViewRadiologyRequest">
								</p>
								<p class="small-gap">
								</p>
								<p class="heading1">
								2. View Radiology Request
								</p>
								<p>
									In this section user can proceed of radiology test request.
								</p>
								<span class="heading2"></span><br/>
									To load radiology request details of patient into table.</br>
															
								<p class="image">
								<img src="images/pathology002.png" class="img-responsive" />
								</p>
								<p>
									Then click on <strong>Process</strong> button to show popup with details of patient and then click <strong>Proceed</strong> to generate request id for the progress result.</br>	
								</p>
								<p class="small-gap"  id="helpViewForensicRequest">
								</p>
								<p class="small-gap">
								</p>
								<p class="heading1">
								3. View Forensic Request
								</p>
								<p>
									In this section user can proceed of forensic test request.
								</p>
								<span class="heading2"></span><br/>
									To load forensic request details of patient into table.</br>
															
								<p class="image">
								<img src="images/pathology003.png" class="img-responsive" />
								</p>
								<p>
									Then click on <strong>Process</strong> button to show popup with details of patient and then click <strong>Proceed</strong> to generate specimen id for the progress result.</br>	
								</p>
								<p class="small-gap"  id="helpLabResult">
								</p>
								<p class="small-gap">
								</p>
								<p class="heading1">
								4. Lab Result
								</p>
								<p>
									In this section user can put the result of lab test of patient.
								</p>
								<span class="heading2"></span><br/>
								To click on search icon to select specimen which you want to give the result then click <strong>load</strong> button to load test component into the table.																
								<p>	
									<ol>
										<li>Specimen No  - Specimen No of the patient. [Mandatory field]</li>
										<li>Specimen Description  - Specimen Description of the patient. [View field]</li>
										<li>Drawn Date & Time - Drawn Date & Time of the patient. [View field]</li>
										<li>Lab Test - Lab Test of the patient. [View field]</li>
										<li>Report - Report of the patient. [Optional field]</li>																			
									</ol>
									<p><strong>Note:</strong> Put the result of test component in table. 
									</p>
								</p>
								<p class="image">
								<img src="images/pathology004.png" class="img-responsive" />
								</p>
								<p>
								Then click on <strong>Save</strong> button to save result of lab test. 	
								</p>
								<p class="small-gap"  id="helpRadiologyResult">
								</p>
								<p class="small-gap">
								</p>
								<p class="heading1">
								5. Radiology Result
								</p>
								<p>
									In this section user can put the result of Radiology test of patient and upload multiple test images.
								</p>
								<span class="heading2"></span><br/>
								To click on search icon to select request id which you want to give the result then click <strong>load</strong> button to load detais.																
								<p>	
									<ol>
										<li>Request Id  - Request Id of the patient. [Mandatory field]</li>
										<li>Taken Date & Time - Drawn Date & Time of the patient. [View field]</li>
										<li>Test Name - Test name of the patient. [View field]</li>
										<li>Report - Report of the patient. [Optional field]</li>																			
										<li>Upload Images - Upload Images of the patient. [Optional field]</li>																			
									</ol>
									<p><strong>Note:</strong> Upload multiple images of test and put details in report field. 
									</p>
								</p>
								<p class="image">
								<img src="images/pathology005.png" class="img-responsive" />
								</p>
								<p>
								Then click on <strong>Save</strong> button to save result of radiology test. 	
								</p>
								<p class="small-gap"  id="helpForensicResult">
								</p>
								<p class="small-gap">
								</p>
								<p class="heading1">
								6. Forensic Result
								</p>
								<p>
									In this section user can put the result of forensic test of patient.
								</p>
								<span class="heading2"></span><br/>
								To click on search icon to select specimen which you want to give the result then click <strong>load</strong> button to load details in field.																
								<p>	
									<ol>
										<li>Specimen No  - Specimen No of the patient. [Mandatory field]</li>
										<li>Specimen Description  - Specimen Description of the patient. [View field]</li>
										<li>Drawn Date & Time - Drawn Date & Time of the patient. [View field]</li>
										<li>Forensic Test - Forensic Test of the patient. [View field]</li>
										<li>Normal Range - Normal Range of the patient test. [Optional field]</li>
										<li>Result - Result of the patient test. [Optional field]</li>
										<li>Result Flag - Result Flag of the patient test. [Optional field]</li>
										<li>Report - Report of the patient test. [Optional field]</li>																			
									</ol>
									<p><strong>Note:</strong> Put the result of test in optional field. 
									</p>
								</p>
								<p class="image">
								<img src="images/pathology006.png" class="img-responsive" />
								</p>
								<p>
								Then click on <strong>Save</strong> button to save result of lab test. 	
								</p>
								<p class="small-gap"  id="helpViewLabResult">
								</p>
								<p class="small-gap">
								</p>
								<p class="heading1">
								7. View Lab Result
								</p>
								<p>
									In this section user can filter the patient lab test result on click search button.
								</p>
								<span class="heading2"></span><br/>
								
								<p>	
									<ol>
										<li>Patient Id  - Patient Id of the patient. [Optional field]</li>
										<li>Visit Id  - Visit Id of the patient. [Optional field]</li>
										<li>Patient Name - Patient Name of the patient. [Optional field]</li>
																												
									</ol>
								</p>
								<p class="image">
								<img src="images/pathology007.png" class="img-responsive" />
								</p>
								<p>
								Then click on <strong>view</strong> icon to view details on pop up then click <strong>print</strong> icon to print the result of lab test. 	
								</p>
								<p class="small-gap"  id="helpViewRadiologyResult">
								</p>
								<p class="small-gap">
								</p>
								<p class="heading1">
								8. View Radiology Result
								</p>
								<p>
									In this section user can filter the patient radiology test result on click search button.
								</p>
								<span class="heading2"></span><br/>
								
								<p>	
									<ol>
										<li>Patient Id  - Patient Id of the patient. [Optional field]</li>
										<li>Visit Id  - Visit Id of the patient. [Optional field]</li>
										<li>Patient Name - Patient Name of the patient. [Optional field]</li>
																												
									</ol>
								</p>
								<p class="image">
								<img src="images/pathology008.png" class="img-responsive" />
								</p>
								<p>
								Then click on <strong>print</strong> icon to print the result of radiology test. 	
								</p>
								<p class="small-gap"  id="helpViewForensicResult">
								</p>
								<p class="small-gap">
								</p>
								<p class="heading1">
								9. View Forensic Result
								</p>
								<p>
									In this section user can filter the patient forensic test result on click search button.
								</p>
								<span class="heading2"></span><br/>
								
								<p>	
									<ol>
										<li>Patient Id  - Patient Id of the patient. [Optional field]</li>
										<li>Visit Id  - Visit Id of the patient. [Optional field]</li>
										<li>Patient Name - Patient Name of the patient. [Optional field]</li>
																												
									</ol>
								</p>
								<p class="image">
								<img src="images/pathology009.png" class="img-responsive" />
								</p>
								<p>
								Then click on <strong>print</strong> icon to print the result of forensic test. 	
								</p>
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>
                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>