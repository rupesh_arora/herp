<header class="navbar navbar-default" id="header" style="position: fixed;  z-index: 999; left:0; right:0;">
   <ul class="nav navbar-nav-custom slidebar_ul">
        <!-- Main Sidebar Toggle Button -->
        <li>
            <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');" id="slide_sidebar">
                <i class="fa fa-bars fa-fw"></i>
            </a>
        </li>
    </ul>    
	<ul class="nav navbar-nav-custom pull-right">
        <li style="padding: 6px;">
			<button type="button" class="btn btn-large btn-danger" onclick="window.location.href= '../index.php';" ><i class="fa fa-caret-left fa-fw"></i>Back to <?php echo $software_name;?></button>
		</li>
		<li class="dropdown">
            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
            <img src="../images/<?php echo $_SESSION['image'] ?>" alt="Profile Pic" /> <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                <li class="dropdown-header text-center">Account</li>
                <li class="divider"></li>
                <li><a href="../logout.php"><i class="fa fa-ban fa-fw pull-right"></i>Logout</a></li>
            </ul>
        </li>
    </ul>
</header>
<div style="height: 50px;">
</div>