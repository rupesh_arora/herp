<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>
<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
       <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpGettingStarted').addClass("open");
				$('#helpGettingStartedUl').css("display","block");
				$('#helpHospitalSetup').addClass("active");
			});
			
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>
				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Hospital Setup</h3>
								<h6 align="center" id="helpHospitalProfile"><a href="#helpHospitalProfile">Hospital Profile</a> | <a href="#helpDepartments">Departments</a> | <a href="#helpWards">Wards</a> | <a href="#helpWardRoomTypes">Ward Room Types</a> | <a href="#helpWardRooms">Ward Rooms</a> | <a href="#helpBeds">Beds</a> | <a href="#helpRoomType">Room Type</a> | <a href="#helpRooms">Rooms</a></h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11">
								<p>
								In Hospital Setup section, we setup some data in this system related to hospital before using/releasing the software. 
								</p>
								<p class="heading1">1. Hospital Profile</p>
								<p>In hospital profile, you can setup the general info of the hospital that are using the Health ERP. Here you can set the following:</p>
								<p>
									<ol>
										<li>
											<i>Hospital Logo</i> [If it's not uploaded it will take the default logo]
										</li>
										<li>
											<i>Hospital Name</i>
										</li>
										<li>
											<i>Address of Hospital</i>
										</li>
										<li>
											<i>Country</i> [Hospital country]
										</li>
										<li>
											<i>State</i> [Hospital state]
										</li>
										<li>
											<i>City</i> [Hospital city]
										</li>
										<li>
											<i>Zipcode</i> [Hospital zipcode or pincode]
										</li>
										<li>
											<i>Phone</i> [Currently supports only 10-digit][One number only]
										</li>
										<li>
											<i>Email</i> [Contact email for the hospital]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/005.Hospital.Profile.PNG" class="img-responsive"/>
								</p>
								<p>
									Note: Whatever you save here, will be reflected on the whole software (for e.g. logo will be changed and every printout will bear the info of the hospital). It is highly recommended that you save hospital info before using the software.
								</p>
								
								<p class="para-gap" id="helpDepartments">
								
								</p>
								
								<p class="heading1">
									2. Departments
								</p>
								<p>
									In the departments, you can add, update or delete any departments of the hospital. These departments will be later reflected on many screens.
								</p>
								<p>
									<span class="heading2">
										Adding Department
									</span><br/>
									To add any department, you go to the department screen and click on <strong> +Add Department </strong> tab. <br/>There you can fill the <strong>Department Name</strong> [mandatory field] and <strong> Description</strong> [optional field].
								</p>
								<p class="image">
									<img src="images/007.Department.Save.png" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">View Departments</span><br/>
									You can see the list of saved departments by clicking the <strong>Department List</strong>.
								</p>
								<p class="image">
									<img src="images/006.Department.List.png" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Updating Department</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved department info.
								</p>
								<p class="image">
									<img src="images/008.Department.Update.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Deleting Department</span><br/>
									By Clicking the <strong>Trash/Delete Icon</strong>, you can delete a saved department. Before deleting the department a prompt will ask to confirm the delete.
								</p>
								<p class="para-gap" id="helpWards"></p>
								<p class="heading1">
									3. Wards
								</p>
								<p>
									Every hospital have wards where patients are admitted and given treatments.
								</p>
								<p>
									<span class="heading2">Adding Ward</span><br/>
									To add ward in the system, please click on the <strong>+Add Ward</strong> tab. Here in the form you have to fill:
									<ol>
										<li>
										Department [mandatory field]
										</li>
										<li>
										Ward name [mandatory field]
										</li>
										<li>
										Ward type - Male/Female [mandatory field]
										</li>
										<li>
										Description about the ward [optional field]
										</li>
									</ol>									
								</p>
								
								<p class="image">
									<img src="images/010.Ward.Save.PNG" class="img-responsive" />
								</p>
								<p class="small-gap"></p>
								<p><span class="heading2">Viewing Wards</span><br/>To view all saved departments click on the <strong>Wards List</strong>.</p>
								<p class="image">
									<img src="images/009.Wards.List.PNG" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpWardRoomTypes">
								</p>
								<p class="heading1">
								4. Ward Room Type
								</p>
								<p>
								In ward room type, you can define the types of room present inside wards. There are different types of ward rooms in different hospitals. So by using this page hospital admin can maintain the ward room types they have at their hospital. 
								</p>
								<p>
								<span class="heading2">Adding Ward Room Type</span><br/>
								To add the Ward room Type, you can access the form clicking the <strong>+Add Room Type</strong>. There are two fields in the from:
								<ol>
									<li>
									Room Type [mandatory Field]
									</li>
									<li>
									Description about room type [optional field]
									</li>
								</ol>
								</p>
								<p class="image">
									<img src="images/012.Ward.Room.Types.Save.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Viewing Ward Room Types</span><br/>
									This list shows all Ward Room Types saved in the system.
								</p>
								<p class="image">
									<img src="images/011.Ward.Room.Type.List.PNG" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpWardRooms">
								</p>
								<p class="heading1">
								5. Ward Rooms
								</p>
								<p>
									In Ward Rooms section, you can maintain the info of rooms inside different wards. Every room is labeled with room number, we can save room number with additionla info.
								</p>
								<p>
								<span>Add Ward Room</span><br/>
								To add a ward room you have to access the Add Ward Room form by clicking the <strong>+Add Ward Room</strong> tab. There you can fill the following fields:
								<ol>
									<li>
										Ward - Select ward to which this room is related [mandatory field]
									</li>
									<li>
									Room Type - Select Room Type of the room that you are adding [mandatory field]
									</li>
									<li>
									Room Number - This is a unique number of Room, an identity of room [mandatory field]
									</li>
									<li>
									Description - You can fill additional information about that particular room here [optional room]
									</li>
								</ol>
								</p>
								<p class="image">
									<img src="images/014.Ward.Room.Save.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								<p>
								<p>
								<span class="heading2">View Ward Rooms</span><br/>
								In here you can see the list of Ward rooms.
								</p>
								<p class="image">
								<img src="images/013.Ward.Room.List.PNG" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpBeds" >
								</p>
								<p class="heading1">
								6. Beds
								</p>
								<p>
								In this section you can mainatin the data about beds in particular wards/rooms.
								</p>
								<p>
								<span class="heading2">Add Bed</span><br/>
								To add the bed, please click on the <strong>+Add Bed</strong> tab. In the add bed form you can fill the following data:
								<ol>
									<li>
										Ward - Select the ward to which bed is related. [mandatory field]
									</li>
									<li>
										Room - Select the room inside which the bed is related. This dropdown will load rooms of selected ward. [mandatory field]
									</li>
									<li>
										Bed Number - Put the number assignedto bed. [mandatory field]
									</li>
									<li>
										Cost - The cost of bed to hospital during the patient occupancy like maintainance bed sheet/mattress, cleaning etc. (0 is accepted) [mandatory field]  
									</li>
									<li>
										Price - The price of bed that is charged to patients during occupancy. [mandatory field]
									</li>
									<li>
										Description - Additional info about bed. [optional field]
									</li>
								</ol>
								</p>
								<p class="image">
									<img src="images/016.Beds.Save.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
								<span class="heading2">View Beds</span><br/>
								Here you can see the list of all beds present in the hospital.
								</p>
								<p class="image">
									<img src="images/015.Beds.List.png" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpRoomType">
								</p>
								<p class="heading1">
								7. Room Type
								</p>
								<p>
								In this section you can define the room types present in hospital other than ward rooms like OPD cosultant rooms, cabins, store rooms, record rooms etc.
								</p>
								<p>
								<span class="heading2">Adding Room Type</span><br/>
								To add Room Type you can access the form by clicking the <strong>+Add Room type</strong> tab. Here you can fill the folowing fields:
								<ol>
									<li>
										Room Type - The type of room which may define its purpose. [mandatory field]
									</li>
									<li>
										Description - About the room type. [optional field]
									</li>
								</ol>
								</p>
								<p class="image">
									<img src="images/018.Room.Types.Save.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
								<span class="heading2">View Room Types</span><br/>
								Here you can see the list of all Room types.
								</p>
								<p class="image">
									<img src="images/017.Room.Type.Save.PNG" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpRooms"></p>
								<p class="heading1" >
								8. Rooms
								</p>
								<p>
								In this section you can maintain info about all rooms (except ward rooms) of hospital.
								</p>
								<p>
								<span class="heading2" >Add Room</span><br/>
								To add the room you can find the form after clicking the <strong>+Add Room</strong> tab. Here you can fill the form entering following fields:
								<ol>
									<li>
									Room type - Select which type of room is this. [mandatory field]
									</li>
									<li>
									Room Number - A unique number assigned to the room. [mandatory field]
									</li>
									<li>
									Description - Additional info about that particular room. [optional field]
									</li>
									<li>
									Assign to OPD Room - Please check this checkbox if you want to mark this room as OPD room. OPD Rooms are going to be listed in the process of OPD while using the software [Checked - OPD room, Unchecked - Not OPD Room].
									</li>
								</ol>
								</p>
								<p class="image">
									<img src="images/020.Room.Save.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
								<span class="heading2">View Rooms</span><br/>
								Here you can see all rooms present in the hospital (except ward rooms).
								</p>
								<p class="image">
									<img src="images/019.Room.List.PNG" class="img-responsive" />
								</p>
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>

                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>