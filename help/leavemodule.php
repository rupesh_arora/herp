<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpleavemodule').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>
				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Configure</h3>
								<h6 align="center" id="helpAddEntitlements"><a href="#helpAddEntitlements">Add Entitlements</a> | <a href="#helpViewEntitlements">View Entitlements</a> | <a href="#helpAdminLeaveList">Admin Leave List</a>| <a href="#helpproduct">Product</a>| <a href="#helpCustomer">Customer</a></h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11 ordering">
								<p class="heading1">
									1. Add Entitlements
								</p>
								<p>
									In the Add Entitlements, you can add, update or delete any Entitlements of the year. These Entitlements will be later reflected on many screens.
								</p>
								<p>
									<span class="heading2">
										Adding Entitlements
									</span><br/>
									To add any Entitlements, you go to the Entitlements screen and click on <strong> +Add Entitlements </strong> tab. <br/>There you can fill the following fields.
									<ol>
										<li>
										<strong> Staff Type </strong>- Select from here staff type [mandatory field]
										</li>
										<li>
										<strong> Leave Type </strong> - Select from here Leave type [mandatory field]
										</li>
										<li>
										<strong> Leave Period </strong> - Select from here Leave Period [mandatory field]
										</li>
										<li>
										<strong> Entitlements </strong> - User add entitlement. [mandatory field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/001_save_entitlement.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">View Entitlements</span><br/>
									You can see the list of saved entitlements by clicking the <strong>Entitlements List</strong>.
								</p>
								<p class="image">
									<img src="images/0112_view_entitlement.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Deleting Entitlements</span><br/>
									By Clicking the <strong>Trash/Delete Icon</strong>, you can delete a saved entitlements. Before deleting the sub types a prompt will ask to confirm the delete.
								</p>
							<!--------- Entitlements screen end from here ------------>		
								
							<!--------- Tax Type Request screen start from here ------------>	
								<p class="para-gap" id="helpViewEntitlements"></p>
								<p class="heading1">
								2. View Entitlements
								</p>
								<p>
									In the View Entitlements, user can view there all entitlements.
								</p>
								<p>
									<span class="heading2">
										View Entitlements
									</span><br/>
									To View Entitlements, you go to the View Entitlements screen and <strong> Filter </strong> tab. <br/>There you can filter with the following fields.
									<ol>
										<li>
										<strong> Leave Type </strong>- [optional field]
										</li>
										<li>
										<strong> Leave Period </strong> -  [optional field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/0001_view_entitlement_user.PNG" class="img-responsive" />
								</p>
								
							<!--------- Leave Type screen end from here ------------>
							
							<!--------- Leave Apply screen start from here ------------>	
								<p class="para-gap" id="helpLeaveApply"></p>
								<p class="heading1">
								3. Leave Apply
								</p>
								<p>
									In the Apply Leave, User take there leave with there leave type. 
								</p>
								<p>
									<span class="heading2">
										To Apply Leave
									</span><br/>
									To Apply any leave, you can go to apply leave screen and you can apply your leave after filling following fields.
									<ol>
										<li>
										<strong> Leave Type </strong>-Slect your leave type [mandatory field]
										</li>
										<li>
										<strong> Leave Balance </strong> -on select  [mandatory field]
										</li>
										<li>
										<strong> From Date </strong> - [mandatory field]
										</li>
										<li>
										<strong> To Date </strong> - [mandatory field]
										</li>
										<li>
										<strong> Report </strong> - [optional field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/0223_save_admin_leave_list.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Approval</strong> on select of approval user approve leave.<br/>
									<strong>Rejected</strong> on select of rejected user reject leave.<br/>
									<strong>Save</strong> will save all the data.
								</p>
								<p class="small-gap">
								</p>
								<p><strong> Show Leave with Status </strong> from this field user filtered it with different check box.<br/>
								<strong> ALL , Rejected, Cancelled, Pending Approval, Scheduled, Taken </strong>
								</p>
							<!--------- Tax Types screen end from here ------------>

							<!--------- Admin Leave List screen start from here ------------>	
								<p class="para-gap" id="helpAdminLeaveList"></p>
								<p class="heading1">
								3. Admin Leave List
								</p>
								<p>
									In the Admin Leave List, you can see list of leaves applied by staffs of the hospital. 
								</p>
								<p>
									<span class="heading2">
										To approval
									</span><br/>
									To approval any leave, you can go to admin leave list screen and you can filter with the following fields.
									<ol>
										<li>
										<strong> From Date </strong>- [mandatory field]
										</li>
										<li>
										<strong> To Date </strong> - [mandatory field]
										</li>
										<li>
										<strong> Staff Type </strong> - [mandatory field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/0223_save_admin_leave_list.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Approval</strong> on select of approval user approve leave.<br/>
									<strong>Rejected</strong> on select of rejected user reject leave.<br/>
									<strong>Save</strong> will save all the data.
								</p>
								<p class="small-gap">
								</p>
								<p><strong> Show Leave with Status </strong> from this field user filtered it with different check box.<br/>
								<strong> ALL , Rejected, Cancelled, Pending Approval, Scheduled, Taken </strong>
								</p>
							<!--------- Tax Types screen end from here ------------>	
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>

                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>