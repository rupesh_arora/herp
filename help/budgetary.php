<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>
<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
       <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpGettingStarted').addClass("open");
				$('#helpGettingStartedUl').css("display","block");
				$('#helpHospitalSetup').addClass("active");
			});
			
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>
				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Budgetary Setup</h3>
								<h6 align="center" id="helpBudgetPeriod"><a href="#helpmainbudget">Main Budget</a> | <a href="#helpDepartmentBudget">Department Budget</a> | <a href="#helpBudgetAdjustment">Department Budget Adjustment</a> | <a href="#helpmainbudget">Main Budget</a></h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11">
								<p>
								In Budgetary Setup section, we setup some data in this system related to Budget before using/releasing the software. 
								</p>
								<p class="heading1">1. Budget Period</p>
								<p>In Budget Period, you can setup the Budget Period that are using the Health ERP. Here you can set the following:</p>
								
								
			
									<span class="heading2">
										Adding Budget Period
									</span><br/>
									To add any Budget Period, you go to the Budget Period screen and click on <strong> +Add Budget Period </strong> tab. <br/>There you can fill the <strong>Budget Period Name</strong> [mandatory field], <strong> Fiscal Year</strong>, [mandatory field],<strong> Begin Date</strong> [mandatory field],<strong> End Date</strong> [mandatory field] and<strong> Description</strong> [optional field]
								</p>
								</p>
								</p>
								</p>
								<p class="image">
									<img src="images/011_budget_period_save.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">View Budget Period</span><br/>
									You can see the list of saved Budget Period by clicking the <strong>Budget Period List</strong>.
								</p>
								<p class="image">
									<img src="images/0021_budget_period_update.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Updating Budget Period</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved Budget Period info.
								</p>
								<p class="image">
									<img src="images/008.Department.Update.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Deleting Budget Period</span><br/>
									By Clicking the <strong>Trash/Delete Icon</strong>, you can delete a saved Budget Period. Before deleting the Budget Period a prompt will ask to confirm the delete.
								</p>
								<p class="para-gap" id="helpBudgetPeriod"></p>
								<p class="heading1">
									2. Department Budget
								</p>
								<p>
									Every hospital have wards where patients are admitted and given treatments.
								</p>
								<p>
									<span class="heading2">Adding Department Budget</span><br/>
									To add Department Budget in the system, please click on the <strong>+Department Budget</strong> tab. Here in the form you have to fill:
									<ol>
										<li>
										Budget [mandatory field]
										</li>
										<li>
										Department [mandatory field]
										</li>
									</ol>									
								</p>
								
								<p class="image">
									<img src="images/save_department budget.PNG" class="img-responsive" />
								</p>
								<p class="small-gap"></p>
								<p><span class="heading2">Viewing Department Budget</span><br/>To view all saved department budget click on the <strong>Department Budget List</strong>.</p>
								<p class="image">
									<img src="images/0022viewbudgetdepartment.PNG" class="img-responsive" />
								<p class="para-gap" id="helpDepartmentBudget">
								</p>
								<p class="small-gap"></p>
								<p><span class="heading2">Updating Department Budget</span><br/>To view all saved department budget click on the <strong>Department Budget List</strong>.</p>
								<p class="image">
									<img src="images/0022viewbudgetdepartment.PNG" class="img-responsive" />
								<p class="para-gap" id="helpDepartmentBudget">
								</p>
								<p class="heading1">
								3. Department Budget Adjustment
								</p>
								<p>
								In Department Budget Adjustment, you will adjust the budget amount of different Department. There are different types of department in different hospitals. So by using this page hospital admin can maintain the department budget they have at their hospital. 
								</p>
								<p>
								<span class="heading2">Adjusting Department Budget</span><br/>
								To update the Department Budget, you can access the form clicking the <strong>Department Budget Adjustment</strong>. There are one fields in the from:
								<ol>
									<li>
									Department [mandatory Field]
									</li>
								</ol>
								</p>
								<p><strong>Note<strong/> On select of department different account name is come out in datatable so user add the amount into it by simply double click on any account row a blank space is come out show user add amount and notes into it.</p>
								<p class="image">
									<img src="images/adjusting_department_budget.png" class="img-responsive" />
								
								<p class="para-gap" id="helpBudgetAdjustment">
								</p>
								<p class="heading1">
								4. Main Budget
								</p>
								<p>
									In Main Budget section, you can maintain the info of rooms inside different wards. Every room is labeled with room number, we can save room number with additionla info.
								</p>
								<p class="image">
									<img src="images/0011main_budget.PNG" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpmainbudget" >
								</p>
								<p class="heading1">
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>

                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>