<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpPatient').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Patient Administration</h3>
								<h6 align="center">
									<a href="#helpPatientHospitilization">Patient Hospitilization</a> |
									<a href="#helpBedTransfer">Bed Transfer</a> |
									<a href="#helpViewInpatient">View Inpatient</a> |									 
									<a href="#helpDischargeForm">Discharge Form</a> |									 
									<a href="#helpAddTreatment">Add Treatment</a> |									 
									<a href="#helpInvestigations">Investigations</a> |
									<a href="#helpViewIPDLabRequest">View IPD Lab Request</a> |
									<a href="#helpViewIPDLabResult">View IPD Lab Result</a>	|								
									<a href="#helpIPDLabTestResult">IPD Lab Result</a>	|								
								</h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11">
							
									<p>
										In the patient administration section, User can do follwing operations.
										<ul>
											<li>Add details of patient hospitalization</li>
											<li>Add details of bed transfer of patient</li>
											<li>View/Update hospitalization patient</li>											
											<li>Discharge patient details</li>
											<li>Lab test of hospitalization patient </li>
											<li>Process of lab request</li>
											<li>Add details of lab request result</li>
											<li>View lab result of hospitalization patient</li>
										</ul>
									</p>
									<p class="small-gap"  id="helpPatientHospitilization">
									</p>
									<p class="heading1">
									1. Patient Hospitalization
									</p>
									<p>
										In this section user can fill the details of patient for admit.
									</p>
									<p>
									<span class="heading2"></span><br/>
									To hospitalization a patient you need to fill/select following fields.
									<p>	
										<ol>
											<li>Patient Id  - Id of the patient. [Mandatory field]</li>
											<li>Admission Date  - Admission Date of the patient. [Mandatory field]</li>
											<li>Department - Department of the patient. [Mandatory field]</li>
											<li>Ward - Ward of the patient. [Mandatory field]</li>
											<li>Room Type - Room Type of the patient. [Mandatory field]</li>
											<li>Room No - Room No of the patient. [View field]</li>
											<li>Bed No - Price of the patient. [View field]</li>
											<li>Price - Bed No of the patient. [View field]</li>
											<li>ICU - ICU of the patient. [Optional field]</li>
											<li>Attending Consultant - Attending Consultant of the patient. [Optional field]</li>
											<li>Operating Consultant - Operating Consultant of the patient. [Optional field]</li>
											<li>Admission Type  - Admission Type  of the patient. [Mandatory field]</li>
											<li>Reason of Admission - Reason of Admission of the patient. [Optional field]</li>
											<li>Notes - Notes of the patient. [Optional field]</li>											
										</ol>
										<strong>Note: Patient Id load when you click on search icon.</strong></br>
										<strong>Note: Select room type to fill room no, bed no, price of bed.</strong></br>
									</p>
									<p>
									  Then Click on <strong>Save</strong> to admit the patient.
									</p>
									<p class="image">
									<img src="images/patienthospitalization001.png" class="img-responsive" />
									</p>
									<p class="small-gap"  id="helpBedTransfer">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									2. Bed Transfer
									</p>
									<p>
										In this section user can add the details of bed transfer of patient.
									</p>
									
									<p>	
										<ol>
											<li>IPD Id - IPD Id of the patient. [Mandatory field]</li>
											<li>Admission Date  - Admission Date of the patient. [Mandatory field]</li>
											<li>Department - Department of the patient. [Mandatory field]</li>
											<li>Ward - Ward of the patient. [Mandatory field]</li>
											<li>Room Type - Room Type of the patient. [Mandatory field]</li>
											<li>Room No - Room No of the patient. [View field]</li>
											<li>Bed No - Price of the patient. [View field]</li>
											<li>Price - Bed No of the patient. [View field]</li>
											<li>ICU - ICU of the patient. [Optional field]</li>
											<li>Reason of Transfer - Reason of Transfer of the patient. [Mandatory field]</li>											
										</ol>
									</p>
									<p class="image">
									<img src="images/patienthospitalization002.png" class="img-responsive" />
									</p>
									<p>
									Then Click on <strong>Transfer</strong> to save details.
									</p>
									<p class="small-gap">
									</p>
									<p class="small-gap"  id="helpViewInpatient">
									</p>
									<p class="heading1">
									3. View Inpatient
									</p>
									<p>
										In this section user can View/Update the patient hospitalization details.
									</p>
									<p>
									<span class="heading2"></span><br/>
									To search a IPD patient you can use the following filters to show filtered patients or directly click on search button to show all the IPD patients.
									<p>	
										<ol>
											<li>IPD Id - IPD Id of the patient. [optional field]</li>
											<li>First Name - First Name of the patient. [optional field]</li>
											<li>Last Name - First Name of the patient. [optional field]</li>											
											<li>Mobile - Mobile Number of the patient. [optional field]</li>
										
										</ol>
									</p>
									<p class="image">
									<img src="images/patienthospitalization003.png" class="img-responsive" />
									</p>
									<p>
									You can also click on the view icons to change the IPD patient details.<br/>
									After clicking on <strong>View</strong> icon a pop will show all the details of selected patient and click on update button to update details .<br/>		
									</p>
									<p class="small-gap"  id="helpDischargeForm">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									4. Discharge Form
									</p>
									<p>
										In this section user can add the dischrge details of patient.
									</p>
									<p>
									<span class="heading2"></span><br/>
									To search a IPD patient click on search icon to show all the IPD patients and load the patient details.
									<p>	
										<ol>
											<li>IPD Id - IPD Id of the patient. [Mandatory field]</li>
											<li>Patient Name - Patient Name of the patient. [View field]</li>
											<li>Age - Age of the patient. [View field]</li>											
											<li>Reason of Hospitalization - Reason of Hospitalization of the patient. [View field]</li>
											<li>Hospitalization Date - Hospitalization Date of the patient. [View field]</li>
											<li>Discharge Date - Discharge Date of the patient. [Mandatory field]</li>
											<li>Discharge Advice - Discharge Advice of the patient. [optional field]</li>
										
										</ol>
									</p>
									<p class="image">
									<img src="images/patienthospitalization004.png" class="img-responsive" />
									</p>
									<p>
									Then click on <strong>Save</strong> to save discharge details of patient.		
									</p>
									<p class="small-gap"  id="helpInvestigations">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									5. Investigations
									</p>
									<p>
										In this section user can request for lab test.
									</p>
									<span class="heading2"></span><br/>
									To search a IPD patient click on search icon to show all the IPD patients and load the patient details.
									<p>	
										<ol>
											<li>IPD Id - IPD Id of the patient. [Mandatory field]</li>
											<li>First Name - First Name of the patient. [View field]</li>
											<li>Last Name - Last Name of the patient. [View field]</li>
											<li>Mobile - Mobile of the patient. [View field]</li>											
											<li>Email - Email of the patient. [View field]</li>
											<li>Patient Id - Patient Id of the patient. [View field]</li>										
										</ol>
									</p>
									<p>
										Click on <strong>Lab</strong> Tab to select lab component for test of patient after then click <strong>Add</strong> button to add componenet in table.
									<p>
									<p class="image">
									<img src="images/patienthospitalization006.png" class="img-responsive" />
									</p>
									<p>
									Then click on <strong>Save</strong> to save lab request of patient for the process.		
									</p>
									</p>
									<p class="small-gap"  id="helpViewIPDLabRequest">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									6. IPD Lab Request
									</p>
									<p>
										In this section user can proceed of lab test request.
									</p>
									<span class="heading2"></span><br/>
									To load lab request details of IPD patient into table.</br>
																
									<p class="image">
									<img src="images/patienthospitalization005.png" class="img-responsive" />
									</p>
									<p>
									Then click on <strong>Process</strong> button to show popup with details of patient and then click <strong>Proceed</strong> to generate specimen id for the progress result.</br>	
									</p>
									<p class="small-gap"  id="helpIPDLabTestResult">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									7. IPD Lab Result
									</p>
									<p>
										In this section user can put the result of lab test of patient.
									</p>
									<span class="heading2"></span><br/>
									To click on search icon to select specimen which you want to give the result then click <strong>load</strong> button to load test component into the table.																
									<p>	
										<ol>
											<li>Specimen No  - Specimen No of the patient. [Mandatory field]</li>
											<li>Specimen Description  - Specimen Description of the patient. [View field]</li>
											<li>Drawn Date & Time - Drawn Date & Time of the patient. [View field]</li>
											<li>Lab Test - Lab Test of the patient. [View field]</li>
											<li>Report - Report of the patient. [Optional field]</li>																			
										</ol>
										<p><strong>Note:</strong> Put the result of test component in table. 
										</p>
									</p>
									<p class="image">
									<img src="images/patienthospitalization007.png" class="img-responsive" />
									</p>
									<p>
									Then click on <strong>Save</strong> button to save result of lab test. 	
									</p>
									<p class="small-gap"  id="helpViewIPDLabResult">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									8. View IPD Lab Test Result
									</p>
									<p>
										In this section user can filter the patient lab test result on click search button.
									</p>
									<span class="heading2"></span><br/>
									
									<p>	
										<ol>
											<li>Patient Id  - Patient Id of the patient. [Optional field]</li>
											<li>IPD Id  - IPD Id of the patient. [Optional field]</li>
											<li>Patient Name - Patient Name of the patient. [Optional field]</li>
																													
										</ol>
									</p>
									<p class="image">
									<img src="images/patienthospitalization008.png" class="img-responsive" />
									</p>
									<p>
									Then click on <strong>view</strong> icon to view details on pop up then click <strong>print</strong> icon to print the result of lab test. 	
									</p>
									<p class="small-gap"  id="helpAddTreatment">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									9. Add Treatment
									</p>
									<p>
										In this section user can add treatment of particular patient in day and night shift.
									</p>
									<span class="heading2"></span><br/>									
									<p>	
										<ol>
											<li>IPD Id  - IPD Id of the patient. [Mandatory field]</li>
											<li>Date  - Date of the patient. [Mandatory field]</li>
											<li>Shift  - Shift of the patient. [Mandatory field]</li>																													
										</ol>
										<p>
											<strong>Note: </strong>When user click on add treatment button then show pop to list of treatment and can also add new treatment name and then click on check for select multiple treatment and add into table on click <strong>Add</strong> button.  
										</p>
									</p>
									<p class="image">
									<img src="images/patienthospitalization009.png" class="img-responsive" />
									</p>
									<p>
									Then click on <strong>save</strong> button to save the treatment details. 	
									</p>
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>
                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>