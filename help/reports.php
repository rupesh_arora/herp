<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpReport').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Reports</h3>
								<h6 align="center">
									<a href="#helpDailySalesReport">Daily Sales Report</a> |
									<a href="#helpDepartementWiseReport">Department Wise Report</a> |
									<a href="#helpDischargePatientReport">Discharge Patient Report</a> | 
									<a href="#helpSalesReport">Sales Report</a>	
								</h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11">
							
									<p>
										In the report section, we can do follwing operations.
										<ul>
											<li>View report in the form of graphs.</li>
											<li>Download data of report in to excel file.</li>
										</ul>
									</p>
									<p class="heading1">
									1. Daily Sales Report
									</p>
									<p>
										In this section we can view hospital daily sales data in form of chart.
									</p>
									<p>
									<span class="heading2"></span><br/>
									To view the daily sales we just need to click on daily sales report.
									</p>

									<p>
										<span class="heading2"></span><br/>
										Click on <strong>Download Excel File</strong> to save the excel file.
										<br><br>
										Click on <strong>Download Pdf</strong> to save the pdf file.
									</p>
									<p class="image">
									<img src="images/01.Reports.DailySales.png" class="img-responsive" />
									</p>
									
									<p class="small-gap"  id="helpDepartementWiseReport">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									2. Department Wise Report
									</p>
									<p>
										In this section we can view three graph. One for visitors analysis. Second for visitors gender analysis. Third for visitors age analysis.
									</p>
									<p>
									<span class="heading2"></span><br/>
									To view the graph you can use the following filters to show the graph.
									<p>	
										<ol>
											<li>Today - Toady will show you today's data of visitors. [optional field]</li>
											<li>Last 7 days - Last 7 days will show you last seven days data of visitors. [optional field]</li>
											<li>From Date to To Date - From Date to To Date will show you  data of visitors according to the date that you have selected. [optional field]</li>
											<li>Last 30 days - Last 30 days will show you last seven days data of visitors. [optional field]</li>
											<li>All time - All time will show you complete data of visitors. [optional field]</li>
										</ol>
									</p>
									<p>
									<span class="heading2"></span><br/>
									Visitors age analysis graph can further be filterd by department. To do this we need to select any department from the list given in the dropdown.
									<p>									
									<p>
										<span class="heading2"></span><br/>
										Click on <strong>Download Excel File</strong> to save the excel file.<br><br>
										Click on <strong>Download Pdf</strong> to save the pdf file.
									</p>
									<p class="image">
									<img src="images/02.Reports.DepartmentWiseReport.png" class="img-responsive" />
									</p>
									<p class="small-gap">
									</p>
									
									<p class="small-gap"  id="helpDischargePatientReport">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									3. Discharge Patient Report
									</p>
									<p>
										In this section we can view graph for bill analysis.
									</p>
									<p>
									<span class="heading2"></span><br/>
									To view the graph you can use the following filters to show the graph.
									<p>	
										<ol>
											<li>Today - Toady will show you today's bill data. [optional field]</li>
											<li>Last 7 days - Last 7 days will show you last seven days bill data. [optional field]</li>
											<li>From Date to To Date - From Date to To Date will show you  bill data according to the date that you have selected. [optional field]</li>
											<li>Last 30 days - Last 30 days will show you last seven days bill data. [optional field]</li>
											<li>All time - All time will show you complete bill data. [optional field]</li>
										</ol>
									</p>
									<p>
										<span class="heading2"></span><br/>
										Click on <strong>Download Excel File</strong> to save the excel file.<br><br>
										Click on <strong>Download Pdf</strong> to save the pdf file.
									</p>
									<p class="small-gap">
									</p>

									<p class="small-gap"  id="helpSalesReport">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									4. Sales Report
									</p>
									<p>
										In this section we can view it's daily sales data in form of chart.
									</p>
									<p>
									<span class="heading2"></span><br/>
									To view the graph you can use the following filters to show the graph.
									<p>	
										<ol>
											<li>Today - Toady will show you today's sales report . [optional field]</li>
											<li>Last 7 days - Last 7 days will show you last seven days sales report. [optional field]</li>
											<li>From Date to To Date - From Date to To Date will show you  sales report according to the date that you have selected. [optional field]</li>
											<li>Last 30 days - Last 30 days will show you last seven days sales report. [optional field]</li>
											<li>All time - All time will show you complete report of sales. [optional field]</li>
										</ol>
									</p>									
									<p>
										<span class="heading2"></span><br/>
										Click on <strong>Download Excel File</strong> to save the excel file.
									</p>
									<p class="small-gap">
									</p>
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>
                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>