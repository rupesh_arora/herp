<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>
<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpGettingStarted').addClass("open");
				$('#helpGettingStartedUl').css("display","block");
				$('#helpDrugSetup').addClass("active");
			});
			
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>
				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Drug Setup</h3>
								<h6 align="center" id="helpDrugCategories"><a href="#helpDrugCategories">Drug Categories</a> | <a href="#helpDrugsList">Drugs List</a> </h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11">
								<p>
								In the Drug Setup, we defined the medicine that hospital is giving/prescribing/stocking at the hospital for the patients.
								</p>
								<p class="heading1">
								1. Drug Categories
								</p>
								<p>
								In this section we define the categories of drugs so that drus can be classified into groups in the system.
								</p>
								<p>
								<span class="heading2">Add Drug Category</span><br/>
								To add a particular drug category in the system, you can access the save form by clicking the <strong>+Add Drug Category</strong> tab. There you can fill the following data:
									<ol>
										<li>Category - The name of category of drug. [mandatory field]</li>
										<li>Description - Description of the drug category. [optional field]</li>
									</ol>
								</p>
								<p class="image">
								<img src="images/049.Drug.Categories.Save.png" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<span  class="heading2">Drug Category List</span><br/>
								Here you see all the drug category List saved in the system.
								</p>
								<p class="image">
									<img src="images/048.Drug.Categories.List.png" class="img-responsive" />
								</p>
								
								<!-- Drug List Starts here -->
								<p class="para-gap" id="helpDrugsList">
								</p>
								<p class="heading1">
								2. Drug List
								</p>
								<p>
								In this section we maintain the drug list in the system.
								</p>
								<p>
								<span class="heading2">Add Drug</span><br/>
								To add a particular drug in the system, you can access the save form by clicking the <strong>+Add Drug</strong> tab. There you can fill the following data:
									<ol>
										<li>Drug Name - Name of the drug. [mandatory field]</li>
										<li>Alternate Name - Specify an alternate name to drug. [mandatory field]</li>
										<li>Drug Category - Select drug category from the list of drug category. [mandatory field]</li>
										<li>Unit Measure - Select unit measure of the drug. [mandatory field]</li>
										<li>Unit Cost - Unit cost of the drug. [mandatory field]</li>
										<li>Unit price - Unit Price of the drug. [mandatory field]</li>
										<li>Alternate Name - Select alternate drug (In case if this particular drug is not available). [optional field]</li>										
										<li>Composition - Describe the composition of drug. [optional field]</li>
									</ol>
								</p>
								<p class="image">
								<img src="images/051.Drugs.Save.png" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<span  class="heading2">Drug List</span><br/>
								Here you see all the drugs saved in the system.
								</p>
								<p class="image">
									<img src="images/050.Drugs.List.png" class="img-responsive" />
								</p>
								<p class="small-gap"></p>
								</p>
								<p>
									<span class="heading2">Updating Drug List</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved Drug List info.
								</p>
								<p class="image">
									<img src="images/008.drug.Update.PNG" class="img-responsive" />
								</p>
								<!-- Drug List Ends here -->
								
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>

                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>