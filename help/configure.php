<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpconfigure').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>
				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Configure</h3>
								<h6 align="center" id="helpsubtype"><a href="#helpsubtype">Holidays</a> | <a href="#helpleavetype">Leave Types</a> | <a href="#helpCustomerType">Customer Types</a>| <a href="#helpproduct">Product</a>| <a href="#helpCustomer">Customer</a></h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11 ordering">
								<p class="heading1">
									1. Holidays
								</p>
								<p>
									In the Holidays, you can add, update or delete any holidays of the year. These holidays will be later reflected on many screens.
								</p>
								<p>
									<span class="heading2">
										Adding Holidays
									</span><br/>
									To add any Holidays, you go to the holidays screen and click on <strong> +Add Holidays </strong> tab. <br/>There you can fill the following fields.
									<ol>
										<li>
										<strong>Holiday Name </strong>- Name of holidays [mandatory field]
										</li>
										<li>
										<strong>Date </strong> - Description of holidays [mandatory field]
										</li>
										<li>
										<strong>Description </strong> - Description of holidays [optional field]
										</li>
										<li>
										<strong>Repeat Annually </strong> - User click on this if that holidays comes repeatedily in same date. [optional field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/0111_save_holidays.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">View Holidays</span><br/>
									You can see the list of saved holidays by clicking the <strong>Holidays List</strong>.
								</p>
								<p class="image">
									<img src="images/0112_view_holidays.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Updating Holidays</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved Holidays info.
								</p>
								<p class="image">
									<img src="images/0113_update_holidays.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Deleting Holidays</span><br/>
									By Clicking the <strong>Trash/Delete Icon</strong>, you can delete a saved Holidays. Before deleting the sub types a prompt will ask to confirm the delete.
								</p>
							<!--------- Holidays screen end from here ------------>		
								
							<!--------- Tax Type Request screen start from here ------------>	
								<p class="para-gap" id="helpleavetype"></p>
								<p class="heading1">
								2. Leave Type
								</p>
								<p>
									In the Leave Type, you can add, update or delete any Leave Type of the hospital. These Leave Type will be later reflected on many screens.
								</p>
								<p>
									<span class="heading2">
										Adding Leave Type
									</span><br/>
									To add any leave type, you go to the leave type screen and click on <strong> +Add Leave Type </strong> tab. <br/>There you can fill the following fields.
									<ol>
										<li>
										<strong> Name </strong>- Name of Tax [mandatory field]
										</li>
										<li>
										<strong> Description </strong> - Description of leave type [optional field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/0011_SAVE_LEAVE_TYPE.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">View Leave Type</span><br/>
									You can see the list of saved leave type by clicking the <strong>Leave Type List</strong>.
								</p>
								<p class="image">
									<img src="images/0012_VIEW_LEAVE_TYPE.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Updating Leave Type</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved leave type info.
								</p>
								<p class="image">
									<img src="images/0113_UPDATE_LEAVE_TYPE.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Deleting Leave Type</span><br/>
									By Clicking the <strong>Trash/Delete Icon</strong>, you can delete a saved leave type. Before deleting the tax types a prompt will ask to confirm the delete.
								</p>
							<!--------- Leave Type screen end from here ------------>	
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>

                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>