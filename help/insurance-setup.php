<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpInsuranceSetup').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>
				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Insurance Setup</h3>
								<h6 align="center" id="helpInsuranceCompanies"><a href="#helpInsuranceCompanies">Insurance Companies</a> | <a href="#helpInsurancePlan">Insurance Plan</a> | <a href="#helpInsuranceType">Insurance Type</a> | <a href="#helpInsurance">Insurance</a></h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11 ordering">
								<p class="heading1">1. Insurance Companies</p>
								<p>In Insurance Companies, user can add the insurance companies. These insurance companies is used in insurance plan and insurance.</p>
								<p>
									<span class="heading2">
										Add Insurance Companies
									</span><br/>
									To add any insurance company, you go to the insurance company screen and click on <strong> +Add Insurance Company </strong> tab. <br/>There you can fill in the form following fields:
									<ol>
										<li>
										Name [mandatory field]
										</li>
										<li>
										Phone - Phone or contact number fo company [mandatory field]
										</li>
										<li>
										Email - Personal email of insurance company . [optional field]
										</li>
										<li>
										Address - Address of company [mandatory field]
										</li>
										<li>
										Country - Country where company resides [mandatory field]
										</li>
										<li>
										State - State where company resides [mandatory field]
										</li>
										<li>
										City - City where company resides [mandatory field]
										</li>
										<li>
										Zipcode - Zipcode/Pincode of company residence's area [mandatory field]
										</li>
										<li>
											Description about the company [optional field]
										</li>
									</ol>									
								</p>
								<p class="image">
									<img src="images/save-insurancecompany.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap"></p>
								<p><span class="heading2">Viewing Insurance Companies</span><br/>To view all saved insurance companies click on the <strong>Insurance Companies List</strong>.</p>
								<p class="image">
									<img src="images/view-insurancecompany.PNG" class="img-responsive" />
								</p>
								<p class="small-gap"></p>
								</p>
								<p>
									<span class="heading2">Updating Insurance companies</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved Insurance companies info.
								</p>
								<p class="image">
									<img src="images/008.Department.Update.PNG" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpInsurancePlan">
								<p class="heading1">
								2. Insurance Plan
								</p>
								<p>
								In Insurance Plan, user can add insurance plan according to the insurance company. 
								</p>
								<p>
									<span class="heading2">
										Add Insurance Plan
									</span><br/>
									To add any insurance plan, you go to the insurance plan screen and click on <strong> +Add Insurance Plan </strong> tab. <br/>There you can fill in the form following fields:
									<ol>
										<li>
										Company Name [mandatory field]
										</li>
										<li>
										Plan Name  [mandatory field]
										</li>
										<li>
											Description about the company [optional field]
										</li>
									</ol>									
								</p>
								<p class="image">
									<img src="images/save-insurancePlan.PNG" class="img-responsive" />
								</p>
								<p class="small-gap"></p>
								<p><span class="heading2">Viewing Insurance Plan</span><br/>To view all saved insurance plan click on the <strong>Insurance Plan List</strong>.</p>
								<p class="image">
									<img src="images/view-insurancePlan.PNG" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpInsuranceType">
								</p>
								<p class="heading1">
								3. Insurance Type
								</p>
								<p>
								In Insurance Type, user can add the insurance type and these type is used in insurance. 
								</p>
								<p>
									<span class="heading2">
										Add Insurance Type
									</span><br/>
									To add any insurance plan, you go to the insurance type screen and click on <strong> +Add Insurance Type </strong> tab. <br/>There you can fill in the form following fields:
									<ol>
										<li>
										Name  [mandatory field]
										</li>
										<li>
											Description about the insurance type [optional field]
										</li>
									</ol>									
								</p>
								<p class="image">
									<img src="images/save-insuranceType.PNG" class="img-responsive" />
								</p>
								<p class="small-gap"></p>
								<p><span class="heading2">Viewing Insurance Type</span><br/>To view all saved insurance type click on the <strong>Insurance Type List</strong>.</p>
								<p class="image">
									<img src="images/view-insuranceType.PNG" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpInsurance">
								</p>
								<p class="heading1">
								4. Insurance 
								</p>
								<p>In Insurance, user can add the insurance of patient.</p>
								<p>
									<span class="heading2">
										Add Insurance
									</span><br/>
									To add any insurance, you go to the insurance screen and click on <strong> +Add Insurance </strong> tab. <br/>There you can fill in the form following fields:
									<ol>
										<li>
										Patient Id  [mandatory field]
										</li>
										<li>
										Insurance Type  [mandatory field]
										</li>
										<li>
										Insurance Company  [mandatory field]
										</li>
										<li>
										Insurance Plan  [mandatory field]
										</li>
										<li>
										Member Since - Start date of insurance  [mandatory field]
										</li>
										<li>
										Expiration Date - End date of insurance  [mandatory field]
										</li>
										<li>
											Extra Info. about the insurance [optional field]
										</li>
									</ol>									
								</p>
								<p class="image">
									<img src="images/save-insurance.PNG" class="img-responsive" />
								</p>
								<p class="small-gap"></p>
								<p><span class="heading2">Viewing Insurance</span><br/>To view all saved insurance click on the <strong>Insurance List</strong>.</p>
								<p class="image">
									<img src="images/view-insurance.PNG" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpInsurance">
								</p>
								<p class="heading1">
								
								
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>

                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>