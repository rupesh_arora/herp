<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpinventoryrequest').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>
				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Inventory Request</h3>
								<h6 align="center" id="helplabitemrequest"><a href="#helplabitemrequest">Lab Item Request</a> | <a href="#helpforensicitemrequest">Forensic Item Request</a> | <a href="#helpradiologyitemrequest">Radiology Item Request</a></h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11 ordering">
								<p class="heading1">1. Lab Item Request</p>
								<p>In Lab Item Request, user can request the item to main inventory . These Item request is reflected to inventory response screen</p>
								<p>
									<span class="heading2">
										To Request Lab Item
									</span><br/>
									To Reaquest any lab item, you go to the Lab Item Request screen and you can fill the following form  fields:
									<ol>
										<li>
										Item - Name of lab item to request   [mandatory field]
										</li>
										<li>
										Stock - Selected item stock is come out automatically in disable form.
										</li>
										<li>
										Quantity - Quantity of item to request  [optional field]
										</li>
									</ol>									
								</p>
								<p>
									<strong>Add</strong> will add item to the table, so user request multiple items at once.<br/>
								</p>	
								<p class="image">
									<img src="images/lab_item_request.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Request</strong> will request the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap"></p>
								
							<!--------- Lab item request screen end from here ------------>		
								
							<!--------- Forensic Item Request screen start from here ------------>	
								<p class="para-gap" id="helpforensicitemrequest">
								<p class="heading1">
								2. Forensic Item Request</p>
								<p>In Forensic Item Request, user can request the item to main inventory . These Item request is reflected to inventory response screen</p>
								<p>
									<span class="heading2">
										To Request Forensic Item
									</span><br/>
									To Reaquest any Forensic item, you go to the Forensic Item Request screen and you can fill the following form  fields:
									<ol>
										<li>
										Item - Name of Forensic item to request[mandatory field]
										</li>
										<li>
										Stock - Selected item stock is come out automatically in disable form.
										</li>
										<li>
										Quantity - Quantity of item to request [optional field]
										</li>
									</ol>									
								</p>
								<p>
									<strong>Add</strong> will add item to the table, so user request multiple items at once.<br/>
								</p>	
								<p class="image">
									<img src="images/forensic_item_request.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Request</strong> will request the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap"></p>
							
							<!--------- Forensic Item Request screen end from here ------------>
							
							<!--------- Radiology Item Request screen start from here ------------>
								<p class="para-gap" id="helpradiologyitemrequest">
								</p>
								<p class="heading1">
								3. Radiology Item Request</p>
								<p>In Radiology Item Request, user can request the item to main inventory . These Item request is reflected to inventory response screen</p>
								<p>
									<span class="heading2">
										To Request Radiology Item
									</span><br/>
									To Reaquest any Radiology item, you go to the Radiology Item Request screen and you can fill the following form  fields:
									<ol>
										<li>
										Item - Name of radiology item to request[mandatory field]
										</li>
										<li>
										Stock - Selected item stock is come out automatically in disable form.
										</li>
										<li>
										Quantity - Quantity of item to request [optional field]
										</li>
									</ol>									
								</p>
								<p>
									<strong>Add</strong> will add item to the table, so user request multiple items at once.<br/>
								</p>	
								<p class="image">
									<img src="images/radiology_item_request.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Request</strong> will request the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap"></p>
								
							<!--------- Radiology Item Request screen end from here ------------>
							
							
							
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>

                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>