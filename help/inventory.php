<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpPatient').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Inventory</h3>
								<h6 align="center">
									<a href="#helpItemCategory">Item Category</a> |
									<a href="#helpItems">Items</a> |
									<a href="#helpViewOrder">View Order</a> |									 
									<a href="#helpSeller">Seller</a> |									 
									<a href="#helpReceivers">Receivers</a> |									 
									<a href="#helpPutOrders">Put Orders</a> |
									<a href="#helpStocking">Stocking</a> |
									<a href="#helpReceiveOrder">Receive Order</a>	|								
									<a href="#helpIssuance">Issuance</a>	|								
									<a href="#helpConfigureItemPrice">Configure Item Price</a>	|								
								</h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11">
							
									<p>
										In the inventory section, User can do follwing operations.
										<ul>
											<li>Add item category</li>
											<li>Add item into to category</li>
											<li>Search order list</li>											
											<li>Add seller name</li>
											<li>Add receiver name</li>
											<li>Put order of item to a seller</li>
											<li>Add item into stock</li>
											<li>Search receive order list</li>
											<li>Issue item from stock</li>
											<li>Configure the price of item</li>
										</ul>
									</p>
									<p class="small-gap"  id="helpItemCategory">
									</p>
									<p class="heading1">
									1. Item Category
									</p>
									<p>
										In this section user can define the category of item.
									</p>
									<p>
									<span class="heading2">Add Item Category</span><br/>
									To add category to click on +Add Item Category tab.
									<p>	
										<ol>
											<li>Categories  - Categories of item. [Mandatory field]</li>
											<li>Description  - Description of item. [Optional field]</li>																				
										</ol>
									</p>
									<p>
									  Then Click on <strong>Save</strong> to save item category.
									</p>
									<p class="image">
									<img src="images/inventory001.png" class="img-responsive" />
									</p>
									<p class="small-gap">
									</p>
									<p>
										<span class="heading2">Item Category List</span><br/>
										To list item category to click on Item Category List tab.
									</p>
									<p class="image">
									<img src="images/inventory002.png" class="img-responsive" />
									</p>
									<p class="small-gap"  id="helpItems">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									2. Items
									</p>
									<p>
										In this section user can add item in category.
									</p>
									<p>
									<span class="heading2">Add Items</span><br/>
									To add items to click on +Add Items tab.
									<p>	
										<ol>
											<li>Category  - Category of item. [Mandatory field]</li>
											<li>Code  - Code of item. [Mandatory field]</li>
											<li>Name  - Name of item. [Mandatory field]</li>
											<li>Description  - Description of item. [Optional field]</li>																				
										</ol>
									</p>
									<p>
									  Then Click on <strong>Save</strong> to save item.
									</p>
									<p class="image">
									<img src="images/inventory003.png" class="img-responsive" />
									</p>
									<p class="small-gap">
									</p>
									<p>
										<span class="heading2">Items List</span><br/>
										To list item to click on Items List tab.
									</p>
									<p class="image">
									<img src="images/inventory004.png" class="img-responsive" />
									</p>
									<p class="small-gap">
									</p>
									<p class="small-gap"  id="helpViewOrder">
									</p>
									<p class="heading1">
									3. View Order
									</p>
									<p>
										In this section user can view order liast of item.
									</p>
									<p>
									<span class="heading2">	</span><br/>
									To search order according to the filters.	
									<p>	
										<ol>
											<li>Order Id  - Order Id of item. [Optional field]</li>
											<li>Seller Id  - Seller Id of item. [Optional field]</li>
											<li>Date  - Date of order item. [Optional field]</li>
											<li>Item  - Item name. [Optional field]</li>																				
										</ol>
									</p>
									<p>
									  Then Click on <strong>search</strong> to search oreder list.
									</p>
									<p class="image">
									<img src="images/inventory005.png" class="img-responsive" />
									</p>
									<p class="small-gap"  id="helpSeller">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									4. Seller
									</p>
									<p>
										In this section user can add seller list for record.
									</p>
									<p>
									<span class="heading2">Add Seller</span><br/>
									To add seller to click on +Add Seller tab.
									<p>	
										<ol>
											<li>Seller Name  - Seller Name. [Mandatory field]</li>
											<li>Description  - Description of item. [Optional field]</li>																				
										</ol>
									</p>
									<p>
									  Then Click on <strong>Save</strong> to save seller name.
									</p>
									<p class="image">
									<img src="images/inventory006.png" class="img-responsive" />
									</p>
									<p class="small-gap">
									</p>
									<p>
										<span class="heading2">Seller List</span><br/>
										To list seller to click on Seller List tab.
									</p>
									<p class="image">
									<img src="images/inventory007.png" class="img-responsive" />
									</p>
									<p class="small-gap"  id="helpReceivers">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									5. Receivers
									</p>
										<p>
										In this section user can add receiver list for record.
									</p>
									<p>
									<span class="heading2">Add Receiver</span><br/>
									To add receiver to click on +Add Receiver tab.
									<p>	
										<ol>
											<li>Receiver Name  - Receiver Name. [Mandatory field]</li>
											<li>Description  - Description of item. [Optional field]</li>																				
										</ol>
									</p>
									<p>
									  Then Click on <strong>Save</strong> to save receiver name.
									</p>
									<p class="image">
									<img src="images/inventory008.png" class="img-responsive" />
									</p>
									<p class="small-gap">
									</p>
									<p>
										<span class="heading2">Receivers List</span><br/>
										To list receiver to click on Receivers List tab.
									</p>
									<p class="image">
									<img src="images/inventory009.png" class="img-responsive" />
									</p>
									</p>
									<p class="small-gap"  id="helpPutOrders">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									6. Put Orders
									</p>
									<p>
										In this section user can put orders of item.
									</p>
									<span class="heading2"></span><br/>
									<strong>Note: </strong>To click on search icon then show the list of seller and item for use to double click on it.</br>
									<p>	
										<ol>
											<li>Seller  - Seller of items. [Mandatory field]</li>
											<li>Date  - Date of order item. [Mandatory field]</li>
											<li>Item  - Item name. [Mandatory field]</li>
											<li>Quantity  - Quantity of item. [Mandatory field]</li>
											<li>Unit Cost  - Unit Cost of item. [Mandatory field]</li>																				
											<li>Notes  - Notes of item. [Optional field]</li>																				
										</ol>
									</p>							
									<p class="image">
									<img src="images/inventory010.png" class="img-responsive" />
									</p>
									<p>
									Then click on <strong>Add</strong> button to add details in table then click <strong>Save</strong> to save details.</br>	
									</p>
									<p class="small-gap"  id="helpStocking">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									7. Stocking
									</p>
									<p>
										In this section user can store stock of item and also search details by using following filters.
									</p>
									<span class="heading2"></span><br/>
									<strong>Note: </strong>To click on search icon then show the list of item for use to double click on it.</br>
									<p>	
										<ol>
											<li>Item  - Item name. [Mandatory field]</li>
											<li>Quantity  - Quantity of item. [Mandatory field]</li>
											<li>Expire Date - Expire Date of item. [Mandatory field]</li>
											<li>Shelf No - Shelf No of item. [Mandatory field]</li>
											<li>Stocking Date - Stocking Date of the patient. [Mandatory field]</li>																			
											<li>Notes - Notes of item. [Opentional field]</li>																			
										</ol>
										<p><strong>Note:</strong> When you click on N/A check fiels this mean no expiry date select of item. 
										</p>
									</p>
									<p class="image">
									<img src="images/inventory011.png" class="img-responsive" />
									</p>
									<p>
									Then click on <strong>Save</strong> button to save details. 	
									</p>
									<p>
									<strong>Note: </strong>If you want to search item stock details then click on <strong>search</strong> button to show list in table. 	
									</p>
									<p class="small-gap"  id="helpReceiveOrder">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									8. Receive Order
									</p>
									<p>
										In this section user can save receive order details.
									</p>
									<span class="heading2"></span><br/>
									<p>
										<strong>Note: </strong>When you click on search icon then show list of order click on <strong>search</strong> button for use double click on it.
									</p>
									<p>	
										<ol>
											<li>Order Id  - Order Id  of item. [Mandatory field]</li>																													
										</ol>
									</p>
									<p>
									Then click on <strong>load</strong> button show order list in table.
									</p>
									<p class="image">
									<img src="images/inventory012.png" class="img-responsive" />
									</p>
									<p>
									After then click on<strong>receive</strong> button for save details. 	
									</p>
									<p class="small-gap"  id="helpIssuance">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									9. Issuance
									</p>
									<p>
										In this section user can issue item to receiver.
									</p>
									<span class="heading2"></span><br/>
									<strong>Note: </strong>To input three character of receiver name then show full receiver name list in dropdown to select and use.</br>
									<strong>Note: </strong>To click on search icon then show the list of item for use to double click on it.</br>
									<p>	
										<ol>
											<li>Receiver  - Receiver name. [Mandatory field]</li>
											<li>Date of Issue  - Date of Issue of item. [Mandatory field]</li>
											<li>Item - Item name. [Mandatory field]</li>
											<li>Quantity - Quantity of item. [Mandatory field]</li>
											<li>Expiry Date - Stocking Date of the patient. [view field]</li>															
										</ol>
										<p><strong>Note:</strong> N/A means no expiry date of item. 
										</p>
									</p>
									<p>
									Then click on <strong>Add Items</strong> button to add issue item details in table and you can also issue more item of receiver. 	
									</p>
									<p class="image">
									<img src="images/inventory013.png" class="img-responsive" />
									</p>
									<p>
									Then click on <strong>Save</strong> button to save details. 	
									</p>
									
									<p class="small-gap"  id="helpConfigureItemPrice">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									10. Configure Item Price
									</p>
									<p>
										In this section user can save item price in record.
									</p>
									<span class="heading2"></span><br/>									
									<strong>Note: </strong>To click on search icon then show the list of item for use to double click on it and load details to click on <strong>search</strong> button.</br>
									<p>	
										<ol>
											<li>Item  - Item name. [Mandatory field]</li>
											<li>Item Description  - Item Description of item. [View field]</li>
											<li>Item Cost - Item Cost of name. [View field]</li>
											<li>Item Price - Item Price of item. [Mandatory field]</li>															
										</ol>
									</p>									
									<p class="image">
									<img src="images/inventory014.png" class="img-responsive" />
									</p>
									<p>
									Then click on <strong>Save</strong> button to save details and then show record in table. 	
									</p>
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>
                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>