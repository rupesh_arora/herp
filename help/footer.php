<footer class="clearfix" id="footer">
	<div class="pull-right">
		Developed by <a href="http://www.qexon.com/" target="_blank">Qexon Infotech Pvt. Ltd.</a>
	</div>
	<div class="pull-left">
		<span id="year-copy"></span> &copy; <a href="./index.php" target="_blank"><?php echo $version_name;?></a>
	</div>
</footer>