/*
 * File Name    :   main.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page  manages menu,dashboard,change password screen
 */
var hospitalName;
var hospitalPhoneNo;
var hospitalEmail;
var hospitalAddress;
var hospitalLogo;
var amountPrefix;
$(document).ready(function() {
	loader();

    /*New Loader Code*/
    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");;
    });
 
   $('.sidebar-nav a').on('click',function(){
		if($(this).attr("href") != ""){
			window.location.href = $(this).attr("href");
		}
	});
   
   
    $(".sidebar-nav a").click(function() {setTimeout(function() {
        var sideBarHeight = $("#sidebar").height();
        $(".block").attr('style', 'min-height:'+sideBarHeight+' !important');//set the height of page to resolve footer issue
    },200)});
	
	$(function(){
	var sideBarHeight = $("#sidebar").height();
	$(".block").attr('style', 'min-height:'+sideBarHeight+' !important');
	});
});

function loader() { // define loader functionality into that function
    $.ajax({
        // your ajax code
        beforeSend: function() {
            $('.loader').show();
        },
        complete: function() {
            $('.loader').hide();
        }
    });
}





