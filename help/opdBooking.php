<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpOpd').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Opd Booking</h3>
								<h6 align="center">
									<a href="#helpOpdBooking">Opd Booking</a> |
									<a href="#helpViewOpdBooking">View Opd Booking</a> |
									<a href="#helpTriage">Triage</a> |	
									<a href="#helpVisionAssessment">Vision Assessment</a> 									
								</h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11">
							
									<p>
										In the opd booking section, User can do follwing operations.
										<ul>
											<li>Book a new OPD in the system</li>
											<li>View the OPD Information</li>
											<li>Add/Update a triage in the system</li>
											<li>Add/Update a vision assessment in the system</li>
										</ul>
									</p>
									<p class="heading1">
									1. Opd Booking
									</p>
									<p>
										In this section user can add/update Opd in the system.
									</p>
									<p>
									<span class="heading2"></span><br/>
									To add/update Opd you need to fill/select following fields.
									</p>
									<p>	
										<ol>
											<li>Patient Id  -Patient Id of the patient. [Mandatory field]</li>
											<li>Service Type - Service Type for the patient. [Mandatory field]</li>
											<li>Services - Services for the patient. [Mandatory field]</li>
											<li>Service Fee - Service Fee of the service. [Mandatory field]</li>
											<li>Visit Type - Visit Type of the patient. [Mandatory field]</li>
											<li>Department - Department of the patient. [Optional field]</li>
											<li>Remarks - Remarks of the patient. [Optional field]</li>
										</ol>										
									</p>
									<p>
										To fill the patient id <strong>two</strong> options are available.<br>
									</p>
									<ul>
										<li>Either you can fill the paient id.<br>
											After that click on <strong>Select</strong> button.Then patients's first name, last name, age and mobile will be auto filled.<br><br>
										</li>
										<li>
											Another way is to click on the <strong>Search</strong>icon. A pop up will open where you can serach patients.<br>
											This pop also provides facility to narrow your search by using following filters:
											<p></p>
											<ol>
												<li>First Name</li>
												<li>Last Name</li>
												<li>DOB</li>
												<li>Mobile</li>
												<li>Email</li><br>							
											</ol>
											<p>These filters can be used as single or you can use the combination of filters. The blank or not selected filters will not be counted in search. For e.g. If you want to search all medical officers with the last name Kent, also enter the Kent word in the Last Name filter. 
												First Name, Last Name & Mobile filters also supports partial word search.
											</p>
										</li>
									</ul>	
									<p class="image">
									<img src="images/01.OpdBooking.opdbook.popup.png" class="img-responsive" />
									</p>
									<p>
										As you <strong>double click</strong> on the list of patients the selected patient on which you have clicked two times will insert record in the screen.<br>
										<strong>Eye Icon</strong> will show that particular patients's detail.
									</p><br>
									<p>After filling all the manadotry fields,please click <strong>Save</strong> button to save the data.</p>
									<p class="image">
									<img src="images/02.OpdBooking.opdbook.png" class="img-responsive" />
									</p>


									<p class="small-gap"  id="helpViewPatients">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									2. View Opd Booking
									</p>
									<p>
										In this section user can view all the registered opd booking.
									</p>
									<p>
									<span class="heading2"></span><br/>
									To search a patient you can use the following filters to show filtered patients or directly click on search button to show all the patients.
									<p>	
										<ol>
											<li>First Name - First Name of the patient. [optional field]</li>
											<li>Last Name - Last Name of the patient. [optional field]</li>
											<li>Patient Id - Patient Id of the patient. [optional field]</li>
											<li>Mobile - Mobile Number of the patient. [optional field]</li>
											<li>From Date - From Date of the visit. [optional field]</li>
											<li>To Date - To Date of the visit. [optional field]</li>
											<li>Paid Status - Paid Status of the patient. [optional field]</li>
											<li>Triage/Assessment Status - Triage/Assessment Status of the patient. [optional field]</li>
										</ol>
										<p>These filters can be used as single or you can use the 		combination of filters. The blank or not selected filters will 	not be counted in search. For e.g. If you want to search all 	medical officers with the last name Kent, also enter the 		Kent word in the Last Name filter. First Name, Last Name & 		Mobile filters also supports partial word search.
										</p>
									</p>
									
									<p class="image">
									<img src="images/03.OpdBooking.ViewOpdBooking.png" class="img-responsive" />
									</p>
									<p>
									You can also click on the view icons to view the visit Information.<br/>
									After clicking on <strong>View</strong> icon a pop will show all the details of selected visit.<br/>
									</p>

									<p class="small-gap">
									</p>
									
									<p class="heading1">
									3. Triage
									</p>
									To register a triage in the system, you have to fill the registration form entering the fields:
									<p>
									<ol>
									<li>
									Visit Id - Visit Id of the patient [mandatory Field]
									</li>
									<li>
									Weight [optional field]
									</li>
									<li>
									Temprature [optional field]
									</li>
									<li>
									Height [optional field]
									</li>
									<li>
									Pulse [optional field]
									</li>
									<li>
									Blood Pressure [optional field]
									</li>
									<li>
									Head Circumference [optional field]
									</li>
									<li>
									Body Surface Area [optional field]
									</li>
									<li>
									Respiration Rate [optional field]
									</li>
									<li>
									Oxygen Saturation [optional field]
									</li>
									<li>
									Heart Rate [optional field]
									</li>
									<li>
									BMI [optional field]
									</li>
									<li>
									Notes [optional field]
									</li>									
								</ol>
								<p>
									To fill the visit id <strong>two</strong> options are available.<br>
								</p>
								<ul>
									<li>Either you can fill the visit id.<br>
										After that click on <strong>Select</strong> button.Then patients's id, image, name,age ,gender and visit date will be auto filled.<br><br>
									</li>
									<li>
										Another way is to click on the <strong>Search</strong>icon. A pop up will open where you can serach patients.<br>
										This pop also provides facility to narrow your search by using following filters:
										<p></p>
										<ol>
											<li>First Name</li>
											<li>Last Name</li>
											<li>Patient Id</li>
											<li>Mobile</li>
											<li>From Date</li>
											<li>To Date</li>
											<li>Paid Status</li>
											<li>Triage/Assessment Status</li>
											<br>							
										</ol>
										<p>These filters can be used as single or you can use the combination of filters. The blank or not selected filters will not be counted in search. For e.g. If you want to search all medical officers with the last name Kent, also enter the Kent word in the Last Name filter. First Name, Last Name & Mobile filters also supports partial word search.
										</p>
									</li>
								</ul>
								<p class="image">
								<img src="images/04.OpdBooking.Triage.popup.png" class="img-responsive" />
								</p>
								<p>
									As you <strong>double click</strong> on the list of visits the selected visit on which you have clicked two times will insert record in the screen.
								</p><br>
								<p>
								You can also click on the view icons to view the visit Information.<br/>
								After clicking on <strong>View</strong> icon a pop will show all the details of selected visit.<br/>
								</p>
								<br>
								<p>After filling all the manadotry fields,please click <strong>Update</strong> button to save the data.</p>
								<p class="image">
								<img src="images/05.OpdBooking.Triage.png" class="img-responsive" />
								</p>

								<p class="small-gap">
									</p>
									
									<p class="heading1">
									4. Vision Assessment
									</p>
									To register a vision assessment in the system, you have to fill the registration form entering the fields:
									<p>
									<ol>
									<li>
									Visit Id - Visit Id of the patient [mandatory Field]
									</li>
									<li>
									Eye Status [mandatory field]
									</li>
									<li>
									Visual Acuity [mandatory field]
									</li>
									<li>
									Visual Acuity Ext [mandatory field]
									</li>
									<li>
									Hand Movement [mandatory field]
									</li>
									<li>
									Preception Of Light [mandatory field]
									</li>
									<li>
									Clinical Comment [optional field]
									</li>
									<li>
									Notes [optional field]
									</li>								
								</ol>
								<p>
									To fill the visit id <strong>two</strong> options are available.<br>
								</p>
								<ul>
									<li>Either you can fill the visit id.<br>
										After that click on <strong>Select</strong> button.Then patients's id, image, name,age ,gender and visit date will be auto filled.<br><br>
									</li>
									<li>
										Another way is to click on the <strong>Load</strong>icon. A pop up will open where you can serach patients.<br>
										This pop also provides facility to narrow your search by using following filters:
										<p></p>
										<ol>
											<li>First Name</li>
											<li>Last Name</li>
											<li>Patient Id</li>
											<li>Mobile</li>
											<li>From Date</li>
											<li>To Date</li>
											<li>Paid Status</li>
											<li>Triage/Assessment Status</li>
											<br>							
										</ol>
										<p>These filters can be used as single or you can use the combination of filters. The blank or not selected filters will not be counted in search. For e.g. If you want to search all medical officers with the last name Kent, also enter the Kent word in the Last Name filter. First Name, Last Name & Mobile filters also supports partial word search.
										</p>
									</li>
								</ul>
								<p class="image">
								<img src="images/04.OpdBooking.Triage.popup.png" class="img-responsive" />
								</p>
								<p>
									As you <strong>double click</strong> on the list of visits the selected visit on which you have clicked two times will insert record in the screen.
								</p><br>
								<p>
								You can also click on the view icons to view the visit Information.<br/>
								After clicking on <strong>View</strong> icon a pop will show all the details of selected visit.<br/>
								</p>
								<br>
								<p>After filling all the manadotry fields,please click <strong>Update</strong> button to save the data.</p>
								<p class="image">
								<img src="images/06.OpdBooking.VisionAssessment.png" class="img-responsive" />
								</p>
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>
                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>