<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>
<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpGettingStarted').addClass("open");
				$('#helpGettingStartedUl').css("display","block");
				$('#helpDiseaseSetup').addClass("active");
			});
			
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>
				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Diseases Setup</h3>
								<h6 align="center" id="helpDiseasesList"><a href="#helpDiseasesList">Diseases List</a> | <a href="#helpAllergyCat">Allergy Categories</a> | <a href="#helpAllergyList">Allergies List</a> | <a href="#helpTreatment">Treatment</a></h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11">
								<p>
								In Diseases Setup section, we setup some data in this system related diseases, allergies and treatment before using the software. 
								</p>
								<p class="heading1">1. Diseases List</p>
								<p>In this section, we define standard diseases names and their ICD Codes.</p>
								<p>
									<span class="heading2">
										Add Disease
									</span><br/>
									To add any disease, you go to the disease page and click on <strong> +Add Disease </strong> tab. <br/>There you can fill in the form following fields:
									<ol>
										<li>
											Code - Standard code for that disease. Its unique in nature. [mandatory field]
										</li>
										<li>
											Name - Name of thr disease. [mandatory field]
										</li>
										<li>
											Description - About the disease. [optional disease]
										</li>
									</ol>									
								</p>
								<p class="image">
									<img src="images/028.Diseases.Save.png" class="img-responsive" />
								</p>
								<p>
									Note: Here we also have an option to upload CSV/XLS sheet to upload multiple diseases at a time. In sheet first column, second column and third column must represent code, name, description respectively.
								</p>
								<p class="small-gap">
								</p>
								<p>
								<span class="heading2">View Diseases</span><br/>
								This page shows whole list of diseases defined in system.
								</p>
								<p class="image">
									<img src="images/027.Diseases.List.png" class="img-responsive" />
								</p>
								<p class="small-gap"></p>
								</p>
								<p>
									<span class="heading2">Updating Diseases</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved Diseases info.
								</p>
								<p class="image">
									<img src="images/026.Diseases.update.PNG" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpAllergyCat">
								</p>
								<p class="heading1">2. Allergy Categories</p>
								<p>
								In here you can maintain the categories/types of Allergies.
								</p>
								<p>
								<span class="heading2">Add Allergy Category</span><br/>
								Here you can add allergy categories. On the save form you can enter following fields:
								<ol>
									<li>
									Category - Name of the allergy category. [mandatory field]
									</li>
									<li>
									Description - About this category of allergy. [optional field]
									</li>
								</ol>
								</p>
								<p class="image">
								<img src="images/030.Allergy.Category.Save.png" class="img-responsive" />
								</p>
								<p>
									Note: Here we also have an option to upload CSV/XLS sheet to upload multiple Allergy Categories at a time. In sheet first column and second column  must represent name, description respectively.
								</p>
								<p class="small-gap">
								</p>
								<p>
								<span class="heading2">View Allergy Categories</span><br/>
								Here you can see all defined Allergy Categories.
								</p>
								<p class="image">
								<img src="images/029.Allergy.Category.List.png" class="img-responsive" />
								</p>
								<p class="small-gap"></p>
								</p>
								<p>
									<span class="heading2">Updating Allergy Categories</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved Allergy Categories info.
								</p>
								<p class="image">
									<img src="images/025.Allergy Categories.update.PNG" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpAllergyList">
								</p>
								<p class="heading1">
								3. Allergies List
								</p>
								<p>
								This section maintains the info about different Allergies in the system.
								</p>	
								<p>
								<span class="heading2">Add Allergy</span><br/>
								To add a particular allergy in the system, you can access the save form by clicking the <strong>+Add Allergy</strong> tab. There you can fill the following data:
								<ol>
									<li>
										Code - Unique code assigned to this particular allergy. [mandatory field]
									</li>
									<li>
										Name - Allergy name. [mandatory field]
									</li>
									<li>
										Category - Choose the Allergy category in which this allergy lies.
									</li>
								</ol>
								</p>
								<p class="image">
									<img src="images/032.Allergy.Save.png" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
								<span class="heading2">View Allergies</span><br/>
								Here you see all the allergies saved in the system.
								</p>
								<p class="image">
									<img src="images/031.Allergy.List.png" class="img-responsive" />
								</p>
								<p class="small-gap"></p>
								</p>
								<p>
									<span class="heading2">Updating Allergies</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved allergies info.
								</p>
								<p class="image">
									<img src="images/025.allergies.update.PNG" class="img-responsive" />
								</p>
								</p>
								<p class="para-gap" id="helpTreatment">
								</p>
								<p class="heading1">4.Treatment</p>
								<p>
								This section maintains the info about different Treatments in the system.
								</p>	
								<p>
								<span class="heading2">Add Treatments</span><br/>
								To add a particular Treatments in the system, you can access the save form by clicking the <strong>+Add Treatments</strong> tab. There you can fill the following data:
								<ol>
									<li>
										Price - Fee of the Treatment [mandatory field]
									</li>
									<li>
										Treatments - Name of the Treatment. [mandatory field]
									</li>
	
								</ol>
								</p>
								<p class="image">
									<img src="images/024.treatment.save.png" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
								<span class="heading2">View Treatments</span><br/>
								Here you see all the treatments saved in the system.
								</p>
								<p class="image">
									<img src="images/023.treatment.List.png" class="img-responsive" />
								</p>
								<p class="small-gap"></p>
								</p>
								<p>
									<span class="heading2">Updating Treatment</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved treatment info.
								</p>
								<p class="image">
									<img src="images/022.treatment.update.PNG" class="img-responsive" />
								</p>
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>

                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>