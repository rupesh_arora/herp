<div id="sidebar">
	
	<!-- div to make it scrollable -->
	<div class="sidebar-scroll">
		
		<!-- Starting Sidebar Content -->
		<div class="sidebar-content">
			<a href="./index.php" class="sidebar-brand" style="position:fixed; width: 200px;z-index:1000; background-color:#23557F">
				<strong><?php echo $software_name?> Manual</strong>
			</a>
			<div class="sidebar-section sidebar-user clearfix" style="padding-top:55px;">
				<div class="sidebar-user-avatar">
				<a href="index.php">
					<img src="../images/<?php echo $_SESSION['hpImage']; ?>" alt="avatar" />
				</a>
				</div>
				<div class="sidebar-user-name"><?php echo $_SESSION['globalName']; ?></div>
				<div class="sidebar-user-links">
					<!--<a href="" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user" id="profile"></i></a>-->
					<!-- <a href="" data-toggle="tooltip" data-placement="bottom" title="Messages"><i class="gi gi-envelope"></i></a>
					<a href="" data-toggle="modal" class="enable-tooltip" data-placement="bottom" title="Settings"><i class="gi gi-cogwheel"></i></a> -->
					<a href="logout.php" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
				</div>
			</div>
			
			<!-- Side bar pages -->
			<ul class="sidebar-nav">
				<li>
					<a href="" data-url="index.php" class="sidebar-nav-submenu" id="helpGettingStarted">
						<i class="fa fa-angle-left sidebar-nav-indicator"></i>
						Getting Started
					</a>
					<ul id="helpGettingStartedUl">
						<li>
							<a href="hospital-setup.php" data-url="" class="sidebar-nav-submenu" id="helpHospitalSetup">
								Hospital Setup
							</a>
						</li>
						<li>
							<a href="staff-setup.php" data-url="" class="sidebar-nav-submenu" id="helpStaffSetup">
								Staff Setup
							</a>
						</li>
						<li>
							<a href="opd-setup.php" data-url="" class="sidebar-nav-submenu" id="helpOpdSetup">
								OPD Setup
							</a>
						</li>
						<li>
							<a href="disease-setup.php" data-url="" class="sidebar-nav-submenu" id="helpDiseaseSetup">
								Diseases/Allergies Setup
							</a>
						</li>
						<li>
							<a href="services-setup.php" data-url="" class="sidebar-nav-submenu" id="helpServicesSetup">
								Services Setup
							</a>
						</li>
						<li>
							<a href="pathological-setup.php" data-url="" class="sidebar-nav-submenu" id="helpPathologicalSetup">
								Pathological Setup
							</a>
						</li>
						<li>
							<a href="drug-setup.php" data-url="" class="sidebar-nav-submenu" id="helpDrugSetup">
								Drug Setup
							</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="patient.php" class="sidebar-nav-menu" id="helpPatient">
						Patient
					</a>
				</li>
					<a href="account.php" class="sidebar-nav-menu" id="helpAccount">
						Billing
					</a>
				</li>
				<li>
					<a href="pharmacy.php" class="sidebar-nav-menu" id="helpPharmacy">
						Pharmacy
					</a>
				</li>
				<li>
					<a href="insurance-setup.php" class="sidebar-nav-menu" id="helpInsuranceSetup">
						Insurance Setup
					</a>
				</li>
				<li>
					<a href="patient-administration.php" class="sidebar-nav-menu" id="helpPatientAdministration">
						Patient Administration
					</a>
				</li>
				<li>
					<a href="reports.php" class="sidebar-nav-menu" id="helpReport">
						Report
					</a>
				</li>
				<li>
					<a href="consultant.php" class="sidebar-nav-menu" id="helpConsultant">
						Consultant
					</a>
				</li>
				<li>
					<a href="opdBooking.php" class="sidebar-nav-menu" id="helpOpd">
						Opd Booking
					</a>
				</li>
				<li>
					<a href="setting.php" class="sidebar-nav-menu" id="helpSetting">
						Setting
					</a>
				</li>
				<li>
					<a href="pathology.php" class="sidebar-nav-menu" id="helpSetting">
						Diagnosis Center
					</a>
				</li>
				<li>
					<a href="budgetary.php" class="sidebar-nav-menu" id="helpSetting">
						Budget Setup
					</a>
				</li>
				<li>
					<a href="clinical_support.php" class="sidebar-nav-menu" id="helpclinicalsupport">
						Clinical Support
					</a>
				</li>
				<li>
					<a href="supply_chain_management.php" class="sidebar-nav-menu" id="helpsupplychainmanagement">
						Supply Chain Management
					</a>
				</li>
				<li>
					<a href="inventory_request.php" class="sidebar-nav-menu" id="helpinventoryrequest">
						Inventory Request
					</a>
				</li>
				<li>
					<a href="account_module.php" class="sidebar-nav-menu" id="helpaccountmodule">
						Account Module
					</a>
				</li>
				<li>
					<a href="assets_module.php" class="sidebar-nav-menu" id="helpassetsmodule">
						Assets Module
					</a>
				</li>
				<li>
					<a href="configure.php" class="sidebar-nav-menu" id="helpconfigure">
						Configure
					</a>
				</li>
				<li>
					<a href="leavemodule.php" class="sidebar-nav-menu" id="helpleavemodule">
						Leave Module
					</a>
				</li>
		</div><!--end sidebar-content class -->
	</div><!--end sidebar-scroll class -->
</div><!--end sidebar -->