<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpPatient').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Patient</h3>
								<h6 align="center">
									<a href="#helpPatientRegistration">Patient Registration</a> |
									<a href="#helpViewPatients">View Patients</a> |
									<a href="#helpViewPatientHistory">View Patient History</a> |									 
									<a href="#helpSelfRequest">Self Request</a> |
									<a href="#helpPatientService">Patient Service</a> |
									<a href="#helpPatientProcedure">Patient Procedure</a>									
								</h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11">
							
									<p>
										In the patient section, User can do follwing operations.
										<ul>
											<li>Register a new patient in the system</li>
											<li>View/Update the patient Information</li>
											<li>View patient history</li>											
											<li>Assign a self request for any patient</li>
											<li>Assign a service for any patient</li>
											<li>Assign a procedure for any patient</li>
										</ul>
									</p>
									<p class="heading1">
									1. Patient Registration
									</p>
									<p>
										In this section user can register a new patient.
									</p>
									<p>
									<span class="heading2"></span><br/>
									To register a patient you need to fill/select following fields.
									<p>	
										<ol>
											<li>Last Name - First Name of the patient. [Mandatory field]</li>
											<li>Salutation  - Salutation of the patient. [Mandatory field]</li>
											<li>First Name - First Name of the patient. [Mandatory field]</li>
											<li>Middle Name - First Name of the patient. [Mandatory field]</li>
											<li>Gender - Gender of the patient. [Mandatory field]</li>
											<li>Date of Birth - Date of Birth of the patient. [Mandatory field]</li>
											<li>Email - Email ID of the patient. [Optional field]</li>
											<li>Mobile - Mobile No. of the patient. [Mandatory field]</li>
											<li>Address - Address of the patient. [Optional field]</li>
											<li>Country - Country of the patient address. [Mandatory field]</li>
											<li>State - State of the patient address. [Mandatory field]</li>
											<li>City - City of the patient address. [Mandatory field]</li>
											<li>Zipcode - Zipcode of the patient address. [Mandatory field]</li>
											<li>Phone - Alternate Phone No. of the patient. [Optional field]</li>
											<li>Next of Kin - Next of Kin of the patient. [Optional field]</li>
											<li>Kin Phone No. - NOK's phone no. of the patient. [Optional field]</li>
											<li>Family Account No. - Family Account Number of the patient. [Optional field]</li>	
											
										</ol>
										<strong>Note: Family Account No. denotes the earlier account no. associated with patient/relative.</strong>
									</p>
									<p>
									  Then Click on <strong>Save</strong> to register the patient.
									</p>
										<p class="image">
											<img src="images/052.Patient.Registration.png" class="img-responsive" />
										</p>
										
									<!-- View Patients Starts from here -->	
									
										<p class="small-gap"  id="helpViewPatients"></p>
										<p class="small-gap"></p>
									<p class="heading1">
									2. View Patients
									</p>
									<p>
										In this section user can View/Update the patient personal information.
									</p>
									<p>
										<span class="heading2"></span><br/>
										To search a patient you can use the following filters to show filtered patients or directly click on search button to show all the patients.
										<p>	
											<ol>
												<li>First Name - First Name of the patient. [optional field]</li>
												<li>Last Name - First Name of the patient. [optional field]</li>
												<li>DOB - First Name of the patient. [optional field]</li>
												<li>Mobile - Mobile Number of the patient. [optional field]</li>
												<li>Email Id - Email Id of the patient. [optional field]</li>
											</ol>
										</p>
										<p class="image">
										<img src="images/053.View.Patients.png" class="img-responsive" />
										</p>
										<p>
										You can also click on the view/edit icons to change the patient personal Information.<br/>
										After clicking on <strong>View</strong> icon a pop will show all the details of selected patient.<br/>
										After clicking on <strong>Edit</strong> icon a pop will show a form where you can edit details of selected patient.<br/>
										</p>
										
									<!-- View Patients Ends from here -->	
									
									<!-- Self Request Starts from here -->	
									
										<p class="small-gap"  id="helpSelfRequest"></p>
										<p class="small-gap"></p>
									<p class="heading1">
									3. Self Request
									</p>
									<p>
										In this section user can request there service or medicine from there own hand.
									</p>
									<p>
										<span class="heading2"></span><br/>
										To request a service you need to fill/select following fields.
										<p>	
											<ol>
												<li>Patient Id - Select patient Id. [mandatory field]</li>
												<li>Payment Mode - Select there payment mode. [mandatory field]</li>
											</ol>
										</p>
										<p class="image">
										<img src="images/selfrequest_service.png" class="img-responsive" />
										</p>
										<p>
											You can also select there payment mode as a Insurance and resister from here.<br/>
											After selecting on <strong>insurance</strong> a pop will show all the insurance details of selected patient.<br/>
										</p>
										<p class="image">
										<img src="images/selfrequest_insurance.png" class="img-responsive" />
										</p>
									<!-- Self Request Ends from here -->	
									
									<!-- View Patients History start here -->
									
									<p class="small-gap"  id="helpViewPatientHistory"></p>
										<p class="small-gap"></p>
										<p class="heading1">
											4. View Patients History
										</p>
										<p>
											In this section user can View the patient history.
										</p>
										<p>
										<span class="heading2"></span><br/>
										To view a patient history you can either enter the </b>Patient Id</b> or from .
										<p>	
											<ol>
												<li>First Name - First Name of the patient. [optional field]</li>
												<li>Last Name - First Name of the patient. [optional field]</li>
												<li>DOB - First Name of the patient. [optional field]</li>
												<li>Mobile - Mobile Number of the patient. [optional field]</li>
												<li>Last Name - Email Id of the patient. [optional field]</li>
											</ol>
										</p>
										<p class="image">
											<img src="images/053.View.Patients.png" class="img-responsive" />
										</p>
										<p>
											You can also click on the view/edit icons to change the patient personal Information.<br/>
											After clicking on <strong>View</strong> icon a pop will show all the details of selected patient.<br/>
											After clicking on <strong>Edit</strong> icon a pop will show a form where you can edit details of selected patient.<br/>
										</p>
										<p class="small-gap"></p>
									</p>
							<!-- View Patients History ends here -->
							
							<!-- Patient Service Starts here -->
							
								<p class="para-gap" id="helpPatientService"></p>
								<p class="heading1">
								5. Patient Service
								</p>
								
								<p>
									<strong>In this screen all patient service request come out in there day priority so when a patient take there service user marked them processed by simply click of process button</strong>
								</p>
								<p class="image">
									<img src="images/patient service save.png" class="img-responsive" />
								</p>
									<p class="small-gap">
								</p>
								<p>
									<strong>On click of Process button a new pop up come out so user just click on mark processd button</strong>
								</p>
								<p class="image">
									<img src="images/service processed.PNG" class="img-responsive" />
								</p>
								
								<p class="small-gap"></p>
							</p>
								
						<!-- Patient Service Ends here -->
						
						<!-- Patient Procedure Starts here -->
							
								<p class="para-gap" id="helpPatientProcedure">
								</p>
								<p class="heading1">
								6. Patient Procedure
								</p>
								
								<p>
									<strong>In this screen all patient procedure request come out in there day priority so when a patient take there procedure user marked them processed by simply click of process button</strong>
								</p>
								<p class="image">
									<img src="images/patient procedure save.PNG" class="img-responsive" />
								</p>
								<p class="small-gap"></p>
								<p>
									<strong>On click of Process button a new pop up come out so user just click on mark processd button</strong>
								</p>
								<p class="image">
									<img src="images/procedure processed.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								
						<!-- Patient Procedure Ends here -->
						
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>
                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>