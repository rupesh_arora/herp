<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpaccountmodule').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>
				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Account Module</h3>
								<h6 align="center" id="helpsubtype"><a href="#helpsubtype">Sub Types</a> | <a href="#helpTaxtype">Tax Types</a> | <a href="#helpCustomerType">Customer Types</a>| <a href="#helpproduct">Product</a>| <a href="#helpCustomer">Customer</a></h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11 ordering">
								<p class="heading1">
									1. Sub Types
								</p>
								<p>
									In the Sub Types, you can add, update or delete any sub types of the hospital. These sub types will be later reflected on many screens.
								</p>
								<p>
									<span class="heading2">
										Adding Sub Types
									</span><br/>
									To add any sub Types, you go to the sub types screen and click on <strong> +Add Sub Types </strong> tab. <br/>There you can fill the following fields.
									<ol>
										<li>
										<strong>Name </strong>- Name of Sub Types [mandatory field]
										</li>
										<li>
										<strong>Description </strong> - Description of Sub Types [optional field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/0011_save_sub_type.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">View Sub Types</span><br/>
									You can see the list of saved sub types by clicking the <strong>Sub Types List</strong>.
								</p>
								<p class="image">
									<img src="images/0012_view_sub_type.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Updating Sub Types</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved sub types info.
								</p>
								<p class="image">
									<img src="images/022_update_sub_types.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Deleting Sub Types</span><br/>
									By Clicking the <strong>Trash/Delete Icon</strong>, you can delete a saved sub types. Before deleting the sub types a prompt will ask to confirm the delete.
								</p>
							<!--------- Sub Types screen end from here ------------>		
								
							<!--------- Tax Type Request screen start from here ------------>	
								<p class="para-gap" id="helpTaxtype"></p>
								<p class="heading1">
								2. Tax Type
								</p>
								<p>
									In the Tax Type, you can add, update or delete any Tax Type of the hospital. These Tax Type will be later reflected on many screens.
								</p>
								<p>
									<span class="heading2">
										Adding Tax Type
									</span><br/>
									To add any Tax Type, you go to the tax type screen and click on <strong> +Add Tax Type </strong> tab. <br/>There you can fill the following fields.
									<ol>
										<li>
										<strong>Tax Name </strong>- Name of Tax [mandatory field]
										</li>
										<li>
										<strong>Tax Rate (in %) </strong> - rate of tax [mandatory field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/022_save_tax_type.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">View Tax Types</span><br/>
									You can see the list of saved tax types by clicking the <strong>Tax Types List</strong>.
								</p>
								<p class="image">
									<img src="images/023_view_tax_type.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Updating Tax Types</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved sub types info.
								</p>
								<p class="image">
									<img src="images/032_update_tax_type.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Deleting Tax Types</span><br/>
									By Clicking the <strong>Trash/Delete Icon</strong>, you can delete a saved tax types. Before deleting the tax types a prompt will ask to confirm the delete.
								</p>
							<!--------- Tax Types screen end from here ------------>	
							
							<!--------- Customer Type Request screen start from here ------------>	
								<p class="para-gap" id="helpCustomerType"></p>
								<p class="heading1">
									3. Customer Type
								</p>
								<p>
									In the Customer Type, you can add, update or delete any customer type of the hospital. These customer type will be later reflected on many screens.
								</p>
								<p>
									<span class="heading2">
										Adding Customer Type
									</span><br/>
									To add any customer type, you go to the customer type screen and click on <strong> +Add Customer Type </strong> tab. <br/>There you can fill the following fields.
									<ol>
										<li>
										<strong>Customer Type </strong>-  [mandatory field]
										</li>
										<li>
										<strong>Description </strong> - Description of that customer type [optional field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/0111_customer_type.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">View Customer Types</span><br/>
									You can see the list of saved customer types by clicking the <strong>Customer Types List</strong>.
								</p>
								<p class="image">
									<img src="images/0112_view_customer_type.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Updating Customer Types</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved customer types info.
								</p>
								<p class="image">
									<img src="images/0044_updating_customer_types.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Deleting Customer Types</span><br/>
									By Clicking the <strong>Trash/Delete Icon</strong>, you can delete a saved customer types. Before deleting the customer types a prompt will ask to confirm the delete.
								</p>
							<!--------- Customer Types screen end from here ------------>
							
							<!--------- Product screen start from here ------------>	
								<p class="para-gap" id="helpproduct"></p>
								<p class="heading1">
									4. Product
								</p>
								<p>
									In the Product, you can add, update or delete any Product of the hospital. These product will be later reflected on many screens.
								</p>
								<p>
									<span class="heading2">
										Adding Product
									</span><br/>
									To add any product type, you go to the product screen and click on <strong> +Add Product </strong> tab. <br/>There you can fill the following fields.
									<ol>
										<li>
										<strong>Product Name </strong>-  [mandatory field]
										</li>
										<li>
										<strong>Cost Price </strong> - cost of product [mandatory field]
										</li>
										<li>
										<strong>Unit </strong> - Select unit abbreviation of product [mandatory field]
										</li>
										<li>
										<strong>Tax </strong> - Select tax of product [optional field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/0222_save_product.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">View Product</span><br/>
									You can see the list of saved product by clicking the <strong>Product List</strong>.
								</p>
								<p class="image">
									<img src="images/0223_view_product.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Updating Product</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved product info.
								</p>
								<p class="image">
									<img src="images/033_update_product.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Deleting Product</span><br/>
									By Clicking the <strong>Trash/Delete Icon</strong>, you can delete a saved product. Before deleting the product a prompt will ask to confirm the delete.
								</p>
							<!--------- Product screen end from here ------------>
							
							<!--------- Customer screen start from here ------------>	
								<p class="para-gap" id="helpCustomer"></p>
								<p class="heading1">
									5. Customer 
								</p>
								<p>
									In the Customer , you can add, update or delete any Customer of the hospital. These customer  will be later reflected on many screens.
								</p>
								<p>
									<span class="heading2">
										Adding Customer 
									</span><br/>
									To add any customer , you go to the Customer screen and click on <strong> +Add Customer  </strong> tab. <br/>There you can fill the following fields.
									<ol>
										<li>
										<strong>First Name </strong>-  First Name of the customer. [Mandatory field]
										</li>
										<li>
										<strong>Last Name </strong> - Last Name of the customer. [Mandatory field]
										</li>
										<li>
										<strong>Address </strong> - Address of the customer. [Mandatory field]
										</li>
										<li>
										<strong>Country </strong> - Country of the customer address. [Mandatory field] 
										</li>
										<li>
										<strong>State </strong> - State of the customer address. [Mandatory field]
										</li>
										<li>
										<strong>City </strong> - City of the customer address. [Mandatory field]
										</li>
										<li>
										<strong>Pincode </strong> - Zipcode of the customer address. [Mandatory field]
										</li>
										<li>
										<strong>Customer Type </strong> - Select Customer Type [optional field]
										</li>
										<li>
										<strong>Email </strong> - Email address of the customer. [Mandatory field]
										</li>
										<li>
										<strong>Website </strong> - Website address of customer [optional field]
										</li>
										<li>
										<strong>Phone </strong> - Phone no. of customer [optional field]
										</li>
										
									</ol>
								</p>
								<p class="image">
									<img src="images/0111_save_customer.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">View Customer</span><br/>
									You can see the list of saved customer by clicking the <strong>Customer List</strong>.
								</p>
								<p class="image">
									<img src="images/0112_view_customer.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Updating Customer</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved customer info.
								</p>
								<p class="image">
									<img src="images/008.Department.Update.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Deleting Customer</span><br/>
									By Clicking the <strong>Trash/Delete Icon</strong>, you can delete a saved customer. Before deleting the customer a prompt will ask to confirm the delete.
								</p>
							<!--------- Product screen end from here ------------>
							
							
							
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>

                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>