<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpassetsmodule').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>
				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Assets Module</h3>
								<h6 align="center" id="helpassetclass"><a href="#helpassetclass">Asset Class</a> | <a href="#helpassetsubclass">Asset Sub Class</a> | <a href="#helpassetlocation">Asset Location</a>| <a href="#helpproduct">Product</a>| <a href="#helpCustomer">Customer</a></h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11 ordering">
								<p class="heading1">
									1. Asset Class
								</p>
								<p>
									In the Asset Class, you can add, update or delete any asset class of the hospital. These asset class will be later reflected on many screens.
								</p>
								<p>
									<span class="heading2">
										Adding Asset Class
									</span><br/>
									To add any asset class, you go to the asset class screen and click on <strong> +Add Asset Class </strong> tab. <br/>There you can fill the following fields.
									<ol>
										<li>
										<strong> Asset Class </strong>- Name of asset class [mandatory field]
										</li>
										<li>
										<strong> Depriciation Rate </strong>- Depriciation rate of asset class [mandatory field]
										</li>
										<li>
										<strong> GL Account </strong>- select GL account of asset class [mandatory field]
										</li>
										<li>
										<strong> Accumulated GL Acccount </strong> - elect accumulated GL account of asset class [optional field]
										</li>
										<li>
										<strong> PPE Class </strong> - Description of asset class [optional field]
										</li>
										<li>
										<strong> Description </strong> - Description of asset class [optional field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/0220_save_asset_class.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">View Asset Class</span><br/>
									You can see the list of saved asset class by clicking the <strong>Asset Class List</strong>.
								</p>
								<p class="image">
									<img src="images/0221_view_asset_class.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Updating Asset Class</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved asset class info.
								</p>
								<p class="image">
									<img src="images/02222_update_asset_class.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Deleting Asset Class</span><br/>
									By Clicking the <strong>Trash/Delete Icon</strong>, you can delete a saved asset class. Before deleting the sub types a prompt will ask to confirm the delete.
								</p>
							<!--------- Sub Types screen end from here ------------>		
								
							<!--------- Assets Sub Class screen start from here ------------>	
								<p class="para-gap" id="helpassetsubclass"></p>
								<p class="heading1">
								2. Assets Sub Class
								</p>
								<p>
									In the Assets Sub Class, you can add, update or delete any assets sub class of the hospital. These assets sub class will be later reflected on many screens.
								</p>
								<p>
									<span class="heading2">
										Adding Assets Sub Class
									</span><br/>
									To add any Assets Sub Class, you go to the assets sub class screen and click on <strong> +Add Assets Sub Class </strong> tab. <br/>There you can fill the following fields.
									<ol>
										<li>
										<strong> Sub Class </strong>- Name of sub class [mandatory field]
										</li>
										<li>
										<strong> Class </strong> - Select class for that sub class [mandatory field]
										</li>
										<li>
										<strong> Description </strong> - Description of that sub class [mandatory field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/0222_save_sub_class.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">View Assets Sub Class</span><br/>
									You can see the list of saved assets sub class by clicking the <strong>Assets Sub Class List</strong>.
								</p>
								<p class="image">
									<img src="images/0223_view_sub_class.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Updating Assets Sub Class</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved assets sub class info.
								</p>
								<p class="image">
									<img src="images/update_asset_sub_class.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Deleting Assets Sub Class</span><br/>
									By Clicking the <strong>Trash/Delete Icon</strong>, you can delete a saved assets sub class. Before deleting the tax types a prompt will ask to confirm the delete.
								</p>
							<!--------- Assets Sub Class screen end from here ------------>	
							
							<!--------- Asset Location Request screen start from here ------------>	
								<p class="para-gap" id="helpassetlocation"></p>
								<p class="heading1">
									3. Asset Location
								</p>
								<p>
									In the Asset Location, you can add, update or delete any Asset Location of the hospital. These asset location will be later reflected on many screens.
								</p>
								<p>
									<span class="heading2">
										Adding Asset Location
									</span><br/>
									To add any asset location, you go to the Asset Location screen and click on <strong> +Add Asset Location </strong> tab. <br/>There you can fill the following fields.
									<ol>
										<li>
										<strong> Location </strong>-  [mandatory field]
										</li>
										<li>
										<strong> Description </strong> - Description of that Location [optional field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/0224_save_asset_location.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">View Asset Location</span><br/>
									You can see the list of saved asset location by clicking the <strong>Asset Location List</strong>.
								</p>
								<p class="image">
									<img src="images/0225_view_asset_location.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Updating Asset Location</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved asset location info.
								</p>
								<p class="image">
									<img src="images/02226_update_asset_location.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Deleting Asset Location</span><br/>
									By Clicking the <strong>Trash/Delete Icon</strong>, you can delete a saved asset location. Before deleting the customer types a prompt will ask to confirm the delete.
								</p>
							<!--------- Asset Location screen end from here ------------>
							
							<!--------- Asset Location start from here ------------>
							
								<p class="para-gap" id="helpproduct"></p>
								<p class="heading1">
									4. Asset Count Period
								</p>
								<p>
									In the Product, you can add, update or delete any Product of the hospital. These product will be later reflected on many screens.
								</p>
								<p>
									<span class="heading2">
										Asset Count Period
									</span><br/>
									To add any asset count period, you go to the Asset Count Period screen and click on <strong> +Add Asset Count Period </strong> tab. <br/>There you can fill the following fields.
									<ol>
										<li>
										<strong> Period </strong> Name of Period [mandatory field]
										</li>
										<li>
										<strong> Start Date </strong> - Start date of asset count period [mandatory field]
										</li>
										<li>
										<strong> End Date </strong> - End date of asset count period [mandatory field]
										</li>
										<li>
										<strong> Description </strong> - Description of asset count period [optional field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/0222_save_asset_count_period.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">View Asset Count Period</span><br/>
									You can see the list of saved asset count period by clicking the <strong>Asset Count Period</strong>.
								</p>
								<p class="image">
									<img src="images/02224_view_asset_count_period.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Updating Asset Count Period</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved asset count period info.
								</p>
								<p class="image">
									<img src="images/0221_update_asset_count_period.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Deleting Product</span><br/>
									By Clicking the <strong>Trash/Delete Icon</strong>, you can delete a saved product. Before deleting the product a prompt will ask to confirm the delete.
								</p>
							<!--------- Asset Location end from here ------------>
							
							<!--------- Asset Location start from here ------------ >
								<p class="para-gap" id="helpCustomer"></p>
								<p class="heading1">
									5. Customer 
								</p>
								<p>
									In the Customer , you can add, update or delete any Customer of the hospital. These customer  will be later reflected on many screens.
								</p>
								<p>
									<span class="heading2">
										Adding Customer 
									</span><br/>
									To add any customer , you go to the Customer screen and click on <strong> +Add Customer  </strong> tab. <br/>There you can fill the following fields.
									<ol>
										<li>
										<strong>First Name </strong>-  First Name of the customer. [Mandatory field]
										</li>
										<li>
										<strong>Last Name </strong> - Last Name of the customer. [Mandatory field]
										</li>
										<li>
										<strong>Address </strong> - Address of the customer. [Mandatory field]
										</li>
										<li>
										<strong>Country </strong> - Country of the customer address. [Mandatory field] 
										</li>
										<li>
										<strong>State </strong> - State of the customer address. [Mandatory field]
										</li>
										<li>
										<strong>City </strong> - City of the customer address. [Mandatory field]
										</li>
										<li>
										<strong>Pincode </strong> - Zipcode of the customer address. [Mandatory field]
										</li>
										<li>
										<strong>Customer Type </strong> - Select Customer Type [optional field]
										</li>
										<li>
										<strong>Email </strong> - Email address of the customer. [Mandatory field]
										</li>
										<li>
										<strong>Website </strong> - Website address of customer [optional field]
										</li>
										<li>
										<strong>Phone </strong> - Phone no. of customer [optional field]
										</li>
										
									</ol>
								</p>
								<p class="image">
									<img src="images/0111_save_customer.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">View Customer</span><br/>
									You can see the list of saved customer by clicking the <strong>Customer List</strong>.
								</p>
								<p class="image">
									<img src="images/0112_view_customer.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Updating Customer</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved customer info.
								</p>
								<p class="image">
									<img src="images/008.Department.Update.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Deleting Customer</span><br/>
									By Clicking the <strong>Trash/Delete Icon</strong>, you can delete a saved customer. Before deleting the customer a prompt will ask to confirm the delete.
								</p>
							<!--------- Product screen end from here ------------>
							
							
							
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>

                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>