<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpConsultant').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Consultant</h3>
								<h6 align="center">
									<!-- <a href="#helpDailySalesReport"></a> |
									<a href="#helpDepartementWiseReport"></a> |
									<a href="#helpDischargePatientReport"></a> | 
									<a href="#helpSalesReport"></a> -->	
								</h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11">
							
									<p>
										In the consultanat section consultant, can do follwing operations.
										<ul>
											<li>Diagonse the patients.</li>
											<li>View old history of patients.</li>
										</ul>
									</p>
									<p class="heading1">
									
									</p>
									<p>
										Firstly consultant need to select the visit id for the patient to start his/her diagnosis or to view his/her old history.
									</p>
									<p>
									<span class="heading2"></span><br/>
									</p>
									<p>
									<span class="heading2"></span><br/>
									To search a visit you can use the following filters to show filtered visits or directly click on search button to show all the visits.
									</p>
									<p>	
										<ol>
											<li>First Name - First Name of the patient. [optional field]</li>
											<li>Last Name - First Name of the patient. [optional field]</li>											
											<li>Mobile - Mobile Number of the patient. [optional field]</li>
											<li>Patient Id - Patient Id of the patient. [optional field]</li>
											<li>From Date - From Date of the patient. [optional field]</li>
											<li>To Date - To Date of the patient. [optional field]</li>
										</ol>
									</p>
									<p class="image">
									<img src="images/consultantViewPatients.png" class="img-responsive" />
									</p>
									<p>
									You can also click on the view icons to view the visit Information.<br/>
									After clicking on <strong>View</strong> icon a screen will show all the details of that particular visit.<br/>
									</p>
									<p class="small-gap"></p>
									<p>When you double click on the list given there in the table you will be auto redirected to new screnn whre all the details for that selected visit in the text boxes will be filled. Like his/her visit id,patient Id, registered services, visit date, visit type, patient name, age , total visits.</p>
									<p class="image">
									<img src="images/01.ConsultantFilledDetails.png" class="img-responsive" />
									</p>
									<p>
									You can also change the visist id.<br/>
									After clicking on <strong>Load</strong> button new visist that you have enterd show it's detail in the text boxes.<br/>
									</p>

									</p>
									<p>
									You can start diagnosis of the patient.<br/>
									After clicking on <strong>Start diagnosis</strong> button . You will see further new tabs to start the diagnosis.<br/>
									</p>
									<p class="image">
									<img src="images/02.consultantViewtabs.png" class="img-responsive" />
									</p>
									<p>
									<p class="heading1">
									1.1 Triage  
									</p>
									By default in general tab will be selected and it's first sub tab(Triage) will be shown.
									</p>
									<p>
									In triage screen you will be get the record of the patient that you have selected. As in the above image you can see.<br>
									</p>
									<p>
									You can also update the patient record.<br/>
									After clicking on <strong>Update</strong> button .You can update the record of the selected partient for the selected visit id.<br/>
									</p>
									<p class="small-gap"  id="helpDepartementWiseReport">
									</p>
									<p class="small-gap">
									</p>
									<p>
									After clicking on <strong>Clinical Finding</strong> tab .You can see the new screen that is for clinical finding.<br/>
									</p>
									<p class="heading1">
									1.2 Clinical Finding
									</p>
									<p>
										In clinical finding screen you will be get the record of the patient that you have selected. Also you can update it's record too.
									</p>									
									<p class="image">
									<img src="images/03.Consultant.Clinical.Finding.png" class="img-responsive" />
									</p>
									<p>
									For the convience of consultant there is plus icon.<br/>
									After clicking on <strong>plus</strong>icon. a pop will show a table where Consultant can add list for it's convience so that next he/she need not to write sentence again.<br/>
									</p>
									<p class="image">
									<img src="images/04.Consultant.ClinicalFinding.Popup.png" class="img-responsive" />
									<p>
									<p>
									When you double click on the list that text will be entered in to text box. So you do'nt need to write the complete sentences agian and again.
									Just make a list once add according as per your wish.
									</p>
									<p>
									After clicking on <strong>Update</strong> button  all the information will be updated for that selected patient.
									</p>
									<p class="small-gap"  id="helpDepartementWiseReport">
									</p>
									<p class="small-gap">
									</p>
									<p class="heading1">
									1.3 Diagnosis
									</p>
									<p>
										In diagnosis screen you will be get the record of the patient that you have selected. Also you can update it's record too.
									</p>
									<p>
									After clicking on <strong>Add</strong> button a new row will be inserted in the table in that row you can add disease name and diagnosis name for that disease.<br>
									<strong>Note:</strong>Disease name is auto searched. You just to need to write more than 3 character you will be get complete lsit for that disease that you have typed. You can't enter any other disease that will not be shown in the list.
									</p>
									<p class="image">
									<img src="images/04.Consultant.Disease.png" class="img-responsive" />
									</p>
									
									<p class="small-gap"  id="helpDepartementWiseReport">
									</p>
									<p class="small-gap">
									</p>
									<p>
									After clicking on <strong>Allergy</strong> tab. You can see the new screen that is for allergy.<br/>
									</p>
									<p class="heading1">
									1.4 Allergy
									</p>
									<p>
										In clinical finding screen you will be get the record of the patient that you have selected. Also you can update it's record too.
									</p>
									<p>
										To add the new record you need to choose allergy category and allergy from dropdowns list present there.
										After clicking on <strong>Add</strong> button. You can add the new record.<br/>
									</p>
									<p class="image">
									<img src="images/05.Consultant.Allergy.png" class="img-responsive" />
									</p>
									<p>
									After clicking on <strong>Update</strong> button  all the information will be updated for that selected patient.
									</p>
									<p class="small-gap"  id="helpDepartementWiseReport">
									</p>
									<p class="small-gap">
									</p>
									<p>
									After clicking on <strong>Vision Assessment</strong> tab. You can see the new screen that is for allergy.<br/>
									</p>
									<p class="heading1">
									1.5 Vision Assessment
									</p>
									<p>Functionality of this screen is excatly similar to Triage screen</p>
									<p class="image">
									<img src="images/06.Consultant.VisionAssessment.png" class="img-responsive" />
									</p>

									<p class="small-gap"  id="helpDepartementWiseReport">
									</p>
									<p class="small-gap">
									</p>
									<p>
									After clicking on <strong>Questionnaire</strong> tab. You can see the new screen that is for allergy.<br/>
									</p>
									<p class="heading1">
									1.6 Questionnaire
									</p>
									<p>Functionality of this screen is excatly similar to Triage screen</p>
									<p class="image">
									<img src="images/07.Consultant.Questionnaire.png" class="img-responsive" />
									</p>

									<p class="small-gap"  id="helpDepartementWiseReport">
									</p>
									<p class="small-gap">
									</p>									
									<p class="heading1">
									2. Test Request
									</p>
									<p>
									After clicking on <strong>Test Request</strong> tab. You can see further three new tabs are availbale.<br/>
									</p>									
									<p class="image">
									<img src="images/08.Consultant.TestRequest.png" class="img-responsive" />
									</p>


									<p class="small-gap"  id="helpDepartementWiseReport">
									</p>
									<p class="small-gap">
									</p>
									
									<p class="heading1">
									2.1 Lab Test Request
									</p>
									<p>
									After clicking on <strong>Lab Test Request</strong> tab. You can see the new screen.<br/>
									</p>									
									<p>
										In lab test request screen you will be get the record of the patient that you have selected. Also you can update it's record too.
									</p>
									<p>
										To add the new record you need to choose lab type, lab test, lab component and request type from dropdowns list present there.
										After clicking on <strong>Add</strong> button. You can add the new record.<br>
									</p>
									<p>
										In this screen consultant can't request for same lab component that he/she has alraedy added in the table for more than one time untill and unless it's amount is paid.<br>
										If the amount has been paid for a particular component then it will be added again moreover it's paid status remain paid.<br>
										But the test status remain pending untill it will not be proceed further. 
									</p>
									<p>
										<strong>Note:</strong>If the request type is external then it's bill will not be generated. But record will be created.
									</p>
									<p class="image">
									<img src="images/09.Consultant.LabTestRequest.png" class="img-responsive" />
									</p>

									<p class="heading1">
									2.2 Radiology Test Request
									</p>
									<p>
									After clicking on <strong>Radiology Test Request</strong> tab. You can see the new screen.<br/>
									</p>									
									<p>
										In lab test request screen you will be get the record of the patient that you have selected. Also you can update it's record too.
									</p>
									<p>
										To add the new record you need to choose Radiology Test from dropdowns list present there.
										After clicking on <strong>Add</strong> button. You can add the new record.<br>
									</p>
									<p>
										In this screen consultant can't request for same radiology test that he/she has alraedy added in the table for more than one time untill and unless it's amount is paid.<br>
										If the amount has been paid for particular radiology test then only it will be added again moreover it's paid status remain paid.<br>
										But the test status remain pending untill it will not be proceed further. 
									</p>
									
									<p class="image">
									<img src="images/10.Consultant.RadiologyTestRequest.png" class="img-responsive" />
									</p>

									<p class="heading1">
									2.3 Forensic Test Request
									</p>
									<p>
									After clicking on <strong>Radiology Test Request</strong> tab. You can see the new screen.<br/>
									</p>									
									<p>
										Functionality of this screen is excatly similar to Radiology Test Request screen.
									</p>									
									<p class="image">
									<img src="images/10.Consultant.RadiologyTestRequest.png" class="img-responsive" />
									</p>

									<p class="small-gap"  id="helpDepartementWiseReport">
									</p>
									<p class="small-gap">
									</p>									
									<p class="heading1">
									3. Service Request
									</p>
									<p>
									After clicking on <strong>Service Request</strong> tab. You can see further three new tabs are availbale.<br/>
									</p>									
									<p class="image">
									<img src="images/11.Consultant.ServiceRequest.png" class="img-responsive" />
									</p>

									<p class="small-gap"  id="helpDepartementWiseReport">
									</p>									
									<p class="heading1 small-gap">
									3.1 Service
									</p>
									<p>
									After clicking on <strong>Service</strong> tab. You can see the new screen.<br/>
									</p>									
									<p>
										Functionality of this screen is exactly similar to forensic test request screen.
									</p>
									
									<p class="image">
									<img src="images/12.Consultant.Service.png" class="img-responsive" />
									</p>

									<p class="small-gap"  id="helpDepartementWiseReport">
									</p>									
									<p class="heading1 small-gap">
									3.2 Procedure
									</p>
									<p>
									After clicking on <strong>Procedure</strong> tab. You can see the new screen.<br/>
									</p>									
									<p>
										Functionality of this screen is exactly similar to service screen.
									</p>
									
									<p class="image">
									<img src="images/13.Consultant.Procedure.png" class="img-responsive" />
									</p>

									<p class="small-gap"  id="helpDepartementWiseReport">
									</p>
									<p class="small-gap">
									</p>									
									<p class="heading1">
									4. Prescriptipon
									</p>
									<p>
									After clicking on <strong>Prescriptipon</strong> tab. You can see the new screen.<br/>
									</p>								
									<p>
										In prescriptipon screen you will be get the drug record of the patient that you have selected. Also you can add it's new drug record too.
									</p>
									<p>
										To add the new drug record you need to enter the drug name for that <strong>two</strong> options are available.<br>
										<ul>
											<li>Either you can search the drug name by auto serach option 	for that you need to enter more than 3 charcater.
												After click on <strong>Select</strong> button. It's alternate name, available stocks, unit measure and unit price will be auto filled.<br><br>
											</li>
											<li>
												Another option is that. Click on <strong>Search </strong>icon. After click on it a pop up will come out.
												<p>
													<span class="heading2"></span><br>
													To search a drug you can use the following filters to show filtered drug or directly click on search button to show all the drug.
												</p>
												<p></p>
												<ol>
													<li>Drug Code - Drug Code of the drug. [optional field]</li>
													<li>Drug Name - Drug Name of the drug. [optional field]</li>												
												</ol>
											</li>
											<p class="image">
											<img src="images/14.Consultant.Prescription.png" class="img-responsive" />
											</p>
											<p>
												You can also click on the view icons to get the complete detail of the drug.<br>
												After clicking on <strong>View</strong> icon a new screen will be shown where you can see the complete drug details.<br>
											</p>
										</ul>
										<p>
											Now you need the fill the dosage, duration , quantity and optional notes for that drug.
										</p>
										<p>
										After fill the complete details you need to click on <strong>Add</strong> to enter the drug details.<br><br>
										Now you can click on <strong>Save</strong>button to save all the drug detail.
										</p>
									</p>
									<p><strong>Note:</strong>Before adding this drug detail consultant need to diagnose that patiuent. If that patient is not diagonsed consultant can't give the drug.</p>
									<p class="image">
									<img src="images/15.Consultant.Prescription.png" class="img-responsive" />
									</p>

									<p class="small-gap"  id="helpDepartementWiseReport">
									</p>
									<p class="small-gap">
									</p>									
									<p class="heading1">
									5. Test Result
									</p>
									<p>
									After clicking on <strong>Test Result</strong> tab. You can see further three new tabs are availbale.<br/>
									</p>									
									<p class="image">
									<img src="images/16.Consultant.TestResult.png" class="img-responsive" />
									</p>

									<p class="small-gap"  id="helpDepartementWiseReport">
									</p>									
									<p class="heading1 small-gap">
									5.1 Lab Test Result
									</p>
									<p>
									After clicking on <strong>Lab Test Result</strong> tab. You can see the new screen.<br/>
									</p>									
									<p>
										In lab test request screen you will be get the record of the lab test for the patient that you have selected.
									</p>
									<br>
									<p>You can also print the lab test record with their component. To do this you need to click on <strong>Print</strong> icon.</p>

									<p class="image">
									<img src="images/18.Consultant.TestResult.png" class="img-responsive" />
									</p>

									<p>After clicking on <strong>View</strong> icon a pop will show all the details lab test with thier component for selected patient.</p>	
									<p class="image">
									<img src="images/17.Consultant.TestResult.PopUp.png" class="img-responsive" />
									</p>

									<p class="small-gap"  id="helpDepartementWiseReport">
									</p>									
									<p class="heading1 small-gap">
									5.2 Radiology Test Result
									</p>
									<p>
									After clicking on <strong>Radiology Test Result</strong> tab. You can see the new screen.<br/>
									</p>									
									<p>
										In radiology test request screen you will be get the record of the radiology test for the patient that you have selected.
									</p>
									<br>
									<p>
										You can also view the image for the particular test. To do this you need to click on <strong>Click here for view image</strong> icon.</p>. A pop will show image of the test for the selected patient. 
									</p>
									<p>You can also print the radiology test record. To do this you need to click on <strong>Print</strong> icon.</p>
									
									<p class="image">
									<img src="images/19.Consultant.RadiologyTestResult.png" class="img-responsive" />
									</p>

									<p class="small-gap"  id="helpDepartementWiseReport">
									</p>									
									<p class="heading1 small-gap">
									5.3 Forensic Test Result
									</p>
									<p>
									After clicking on <strong>Forensic Test Result</strong> tab. You can see the new screen.<br/>
									</p>									
									<p>
										Functionality of this screen is exactly similar to radiology test result except you can't view the image of the test.
									</p>	
									<p class="image">
									<img src="images/20.Consultant.ForensicTestResult.png" class="img-responsive" />
									</p>










									
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>
                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
    </body>
</html>