<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpsupplychainmanagement').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>
				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Supply Chain Management</h3>
								<h6 align="center" id="helpItemcategories"><a href="#helpItemcategories">Item Categories</a> | <a href="#helpItemname">Item</a> | <a href="#helpVendorTypes">Vendor Types </a>| <a href="#helpVendor">Vendor</a> | <a href="#helpPlaceOrders">Place Order</a>| <a href="#helpViewOrders">View Orders</a>| <a href="#helpReceiveorder">Receive Orders</a>| <a href="#helpReturnOrders">Return Orders</a>| <a href="#helpstocking">Stocking</a>| <a href="#helpIssuance">Issuance</a>| <a href="#helpShowexpiretime">Show Expire Time</a></h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11 ordering">
								<p class="heading1">1. Item Categories</p>
								<p>In Item Categories, user can add the Item Categories. These Item Categories is used to categories the Item</p>
								<p>
									<span class="heading2">
										Add Item Categories
									</span><br/>
									To add any Item Categories, you go to the Item Categories screen and click on <strong> +Add Item Categories </strong> tab. <br/>There you can fill in the form following fields:
									<ol>
										<li>
										Inventory Type [mandatory field]
										</li>
										<li>
										Categories - Name of Categories [mandatory field]
										</li>
										<li>
										Description - Description of Categories [optional field]
										</li>
									</ol>									
								</p>
								<p class="image">
									<img src="images/save_laboratory_product.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap"></p>
								<p><span class="heading2">Viewing Item Categories</span><br/>To view all saved Item Categories click on the <strong>Item Categories List</strong>.</p>
								<p class="image">
									<img src="images/view_item_categories.png" class="img-responsive" />
								</p>
								<p class="small-gap"></p>
								</p>
								<p>
									<span class="heading2">Updating Item Categories</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved Item Categories info.
								</p>
								<p class="image">
									<img src="images/008.Department.Update.PNG" class="img-responsive" />
								</p>
							<!--------- Item category screen end from here ------------>		
								
							<!--------- Item screen Start from here ------------>		
								<p class="para-gap" id="helpItemname">
								<p class="heading1">
								2. Items
								</p>
								<p>
								In Items, user can add Items according to the Item Categories. 
								</p>
								<p>
									<span class="heading2">
										Add Items
									</span><br/>
									To add any Item, you go to the Item screen and click on <strong> +Add Item </strong> tab. <br/>There you can fill in the form following fields:
									<ol>
										<li>
										Category [mandatory field]
										</li>
										<li>
										Code - code of the item [mandatory field]
										</li>
										<li>
										Name- Name of the Item used in the company [mandatory field]
										</li>
										<li>
										Description- Description of the Item [Optional field]
										</li>
									</ol>									
								</p>
								<p class="image">
									<img src="images/save_item_name.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap"></p>
								<p><span class="heading2">Viewing Item</span><br/>To view all saved Item click on the <strong>Item List</strong>.</p>
								<p class="image">
									<img src="images/view_item.png" class="img-responsive" />
								</p>
								</p>
								<p class="small-gap"></p>
								</p>
								<p>
									<span class="heading2">Updating Item</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved Item Categories info.
								</p>
								<p class="image">
									<img src="images/008.Department.Update.PNG" class="img-responsive" />
								</p>
								
								
							<!--------- Item screen end from here ------------>	
							
							<!--------- Vendor Types screen start from here ------------>	
								<p class="para-gap" id="helpVendorTypes">
								</p>
								<p class="heading1">
								3. Vendor Types
								</p>
								<p>
								In Vendor Types, user can add the Vendor Types and these type is used to categories vendor. 
								</p>
								<p>
									<span class="heading2">
										Add Vendor Types
									</span><br/>
									To add any Vendor Types, you go to the vendor types screen and click on <strong> +Add Vendor Types </strong> tab. <br/>There you can fill in the form following fields:
									<ol>
										<li>
										Vendor Type  [mandatory field]
										</li>
										<li>
										Description -	Description about the vendor type [optional field]
										</li>
									</ol>									
								</p>
								<p class="image">
									<img src="images/save_vendor_type.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap"></p>
								<p><span class="heading2">Viewing Vendor Type</span><br/>To view all saved Vendor Type click on the <strong>Vendor Type List</strong>.</p>
								<p class="image">
									<img src="images/view_vendor_type.PNG" class="img-responsive" />
								</p>
								<p class="small-gap"></p>
								</p>
								<p>
									<span class="heading2">Updating Vendor Type</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved Vendor Type info.
								</p>
								<p class="image">
									<img src="images/008.Department.Update.PNG" class="img-responsive" />
								</p>
							
							<!--------- Vendor Types screen start from here ------------>
							
							<!--------- Vendor screen start from here ------------>
								<p class="para-gap" id="helpVendor">
								</p>
								<p class="heading1">
								4. Vendor 
								</p>
								<p>In Vendor, user can add the Vendor name from here.</p>
								<p>
									<span class="heading2">
										Add Vendor
									</span><br/>
									To add any Vendor, you go to the Vendor screen and click on <strong> +Add Vendor </strong> tab. <br/>There you can fill in the form following fields:
									<ol>
										<li>
										Vendor Name  [mandatory field]
										</li>
										<li>
										Vendor Type  [mandatory field]
										</li>
										<li>
										Firm Name - Firm name where this vendor is works [mandatory field]
										</li>
										<li>
										Vendor Email - Email address of vendor [mandatory field]
										</li>
										<li>
										Phone - phone no of vendor  [mandatory field]
										</li>
										<li>
										Address - address of the vendor  [mandatory field]
										</li>
										<li>
										Description - Description of the vendor [optional field]
										</li>
										<li>
										IFSC Code - IFSC Code of the bank [mandatory field]
										</li>
										<li>
										Bank Name - Name of the bank [mandatory field]
										</li>
										<li>
										A/C No - A/C No of the vendor [mandatory field]
										</li>
									</ol>									
								</p>
								<p class="image">
									<img src="images/save_vendor.png" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap"></p>
								<p><span class="heading2">Viewing Vendor</span><br/>To view all saved Vendor click on the <strong>Vendor List</strong>.</p>
								<p class="image">
									<img src="images/view_vendor.PNG" class="img-responsive" />
								</p>
							<!--------- Vendor screen start from here ------------>

							<!--------- Place order screen start from here ------------>							
								<p class="para-gap" id="helpPlaceOrders">
								</p>
								<p class="heading1">
								5. Place Orders 
								</p>
								<p>
									<span class="heading2"></span><br/>
									To placed a Order you can fill the following fields.
								</p>
									<p>	
										<ol>
											<li>Seller- Seller name to the placed order. [mandatory field]</li>
											<li>Item - Name of Item to order. [mandatory field]</li>											
											<li>Date - Date of the Order. [mandatory field]</li>
											<li>Quantity - Quantity of item to order. [mandatory field]</li>
											<li>Notes - Notes for the order. [mandatory field]</li>
										</ol>
									</p>
								    <span class="heading2">
										Add Button
									</span><br/>
									To add multiple Item, you just click on <strong> +Add button </strong> tab. <br/>There you can add multiple number of item.
								<p class="image">
									<img src="images/place_order.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
							<!--------- Place order screen end from here ------------>	
							
							<!--------- View order screen start from here ------------>
								<p class="small-gap"  id="helpViewOrders">
									</p>
									<p class="heading1">
									6. View Orders
									</p>
									<p>
										<span class="heading2"></span><br/>
										To search a Order you can use the following filters to show filtered visits or directly click on search button to show all the visits.
									</p>
										<p>	
											<ol>
												<li>Order Id - Order Id of the placed order. [optional field]</li>
												<li>Vendor Id - Vendor Id of the vendor. [optional field]</li>											
												<li>Date - Date of the Order. [optional field]</li>
												<li>Item - Name of item. [optional field]</li>
											</ol>
										</p>
										<p class="image">
										<img src="images/view_order.PNG" class="img-responsive" />
										</p>
										<p>
										You can also click on the view icons to view the Orders Information.<br/>
										After clicking on <strong>View</strong> icon a screen will show all the details of that particular order.<br/>
										</p>
										<p class="image">
										<img src="images/view_order_details.PNG" class="img-responsive" />
										</p>
								<!--------- View order screen end from here ------------>
								
								<!--------- Receive order screen start from here ------------>
								<p class="small-gap"  id="helpReceiveorder">
									</p>
									<p class="heading1">
									7. Receive order
									</p>
									<p>
										<span class="heading2"></span><br/>
										To search a order you can use the following filters to show filtered orderes or directly click on search button to show all the orders.
									</p>
										<p>	
											<ol>
												<li>Order Id - Order Id of the placed order. [optional field]</li>
												<li>Vendor Id - Vendor Id of the vendor. [optional field]</li>											
												<li>Date - Date of the Order. [optional field]</li>
												<li>Item - Name of item. [optional field]</li>
											</ol>
										</p>
										<p class="image">
										<img src="images/order_details.PNG" class="img-responsive" />
										</p>
										
									<p>In <strong>Receive order</strong>, you can received the orders, Here you can set the following:</p>
									<p>
										<ol>
											
											<li>
												<i>Item name</i> [mandatory field]
											</li>
											<li>
												<i>Address of Hospital</i> [mandatory field]
											</li>
											<li>
												<i>Quantity</i> [mandatory field]
											</li>
											<li>
												<i>Discount (%)</i> [optional field]
											</li>
											<li>
												<i>Unit Cost</i> [mandatory field]
											</li>
											<li>
												<i>Batch No.</i> [mandatory field]
											</li>
											<li>
												<i>Invoice No.</i> [mandatory field]
											</li>
											<li>
												<i>Mfg. Date</i> [optional field]
											</li><li>
												<i>Exp. Date</i> [optional field]
											</li><li>
												<i>GL Account</i> [mandatory field]
											</li>
										</ol>
									</p>
									<p class="image">
										<img src="images/receive_order.png" class="img-responsive" />
										</p>	
									<p>
									<strong>Add</strong> will add the ordered item in datatable.<br/>
									<strong>Receive</strong> will receive the order in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>	
							<!--------- Receive order screen end from here ------------>

							<!--------- Return order screen start from here ------------>							
								<p class="para-gap" id="helpReturnOrders">
								</p>
								<p class="heading1">
								8. Return Orders 
								</p>
								<p>
									<span class="heading2"></span><br/>
									To Return a Order you can fill the following fields.
								</p>
									<p>	
										<ol>
											<li>Item Name- name of Item to return. [mandatory field]</li>
											<li>Batch No. - Batch No of Item. [mandatory field]</li>											
											<li>Seller Name - Seller name come out on select of batch number.</li>
											<li>Quantity - Quantity of item ordered. [mandatory field]</li>
											<li>Mfg Date - Mfg Date of item. [mandatory field]</li>
											<li>Exp Date - Exp Date of item. [mandatory field]</li>
											<li>Recieved By - receiver name who received particular order. [mandatory field]</li>
											<li>Date - Notes for the order. [mandatory field]</li>
										</ol>
									</p>
									<p class="image">
									<img src="images/return_item.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
							<!--------- Return order screen end from here ------------>
							
							<!--------- Stocking screen start from here ------------>
								<p class="para-gap" id="helpstocking"></p>
								
								<p class="heading1">
									9. Stocking
								</p>
								<p>
									In the Stocking, you can stock items in Warehouse of the hospital.
								</p>
								<p>
									<span class="heading2">
										To Stock any item, you go to the Stocking screen and fill the following form.
									</span><br/>
									<ol>
										<li>
										Item - Name of item it come out as autofilled functionality also by search [mandatory field]
										</li>
										<li>
										Quantity - Quantity of item come out if available to stock [mandatory field]
										</li>
										<li>
										Expire Date - Expiry date of item [optional field]
										</li>
										<li>
										N/A - Expiry date If not available you simply checked it [optional field]
										</li>
										<li>
										Shelf No - Quantity of item to issue. [mandatory field]
										</li>
										<li>
										Notes [optional field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/save_stocking.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save or dispensed the data(item) entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.<br/>
									<strong>Search</strong> will show all the stocked item in datatable
								</p>
								<p class="small-gap"></p>
								
							<!--------- Issuance screen end from here ------------>
							
							<!--------- Receiver screen start from here ------------>

							<p class="para-gap" id="helpReceiver"></p>
								
								<p class="heading1">
									10. Receivers
								</p>
								<p>
									In the Receiver, you can add, update or delete any Receiver of the hospital. These Receiver will be later reflected on many screens.
								</p>
								<p>
									<span class="heading2">
										Adding Receiver
									</span><br/>
									To add any Receiver, you go to the Receiver screen and click on <strong> +Add Receiver </strong> tab. <br/>There you can fill the <strong>Receiver Name</strong> [mandatory field] and <strong> Description</strong> [optional field].
								</p>
								<p class="image">
									<img src="images/011_receiver_save.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">View Receiver</span><br/>
									You can see the list of saved Receivers by clicking the <strong>Receiver List</strong>.
								</p>
								<p class="image">
									<img src="images/002_view_receiver.PNG" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Updating Receiver</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved Receiver info.
								</p>
								<p class="image">
									<img src="images/008.Department.Update.PNG" class="img-responsive" />
								</p>
									<p class="small-gap">
								</p>
								<p>
									<span class="heading2">Deleting Receiver</span><br/>
									By Clicking the <strong>Trash/Delete Icon</strong>, you can delete a saved Receiver. Before deleting the Receiver a prompt will ask to confirm the delete.
								</p>
								<p>
								<strong>Note:</strong> Here we also have an option to upload CSV/XLS sheet to upload multiple receiver at a time. In sheet first column and second column  must represent name, description respectively.
								
								</p>
							<!--------- Receiver screen end from here ------------>	
							
							<!--------- Issuance screen start from here ------------>
								<p class="para-gap" id="helpIssuance"></p>
								
								<p class="heading1">
									11. Issuance
								</p>
								<p>
									In the Issuance, you can issue items to receiver of the hospital.
								</p>
								<p>
									<span class="heading2">
										To Issue any item, you go to the Issuance screen and fill the following form.
									</span><br/>
									<ol>
										<li>
										Receiver - Name of receiver it come out as autofilled functionality  [mandatory field]
										</li>
										<li>
										Date of Issue - Issueing date [mandatory field]
										</li>
										<li>
										Item - name of item to issue [mandatory field]
										</li>
										<li>
										Quantity - Quantity of item to issue. [mandatory field]
										</li>
										<li>
										Expiry Date - Expiry date of item [optional field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/00011_save_issuance.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save or dispensed the data(item) entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap"></p>
								
							<!--------- Issuance screen end from here ------------>
							
							<!--------- Show expire time screen start from here ------------>
								<p class="para-gap" id="helpShowexpiretime"></p>
								
								<p class="heading1">
									12. Show expire time
								</p>
								<p>
									In the Show expire time, In this screen we show all the items with there maufacturing and expired time.
								</p>
								<p>
									<span class="heading2">
										To Show expire time of any item, you go to the show expire time screen and fill the following form.
									</span><br/>
									<ol>
										<li>
										Inventory Type [mandatory field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/show_expire_time.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Show</strong> will Show all the item presented in selected Inventory Type<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap"></p>
								
							<!--------- Show expire time screen end from here ------------>
							
							<!--------- Inventory response screen start from here ------------>
								<p class="para-gap" id="helpInventoryResponse"></p>
								
								<p class="heading1">
									12. Inventory Response
								</p>
								<p>
									In Inventory Response screen we show all the requested items request by the user in different department.
								</p>
								<p>
									<span class="heading2">
										To Show Inventory Response of any item, you go to the inventory response screen and fill the following form.
									</span><br/>
									<ol>
										<li>
										Inventory Type [mandatory field]
										</li>
									</ol>
								</p>
								<p>
									<strong>Show</strong> will Show all the requested item in selected Inventory Type.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="image">
									<img src="images/Inventory_Response.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Proceed</strong> will proceed the request.
								</p>
								<p class="small-gap"></p>
								
							<!--------- Show expire time screen end from here ------------>
							
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>

                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>