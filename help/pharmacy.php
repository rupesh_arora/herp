<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpPharmacy').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Pharmacy</h3>
								<h6 align="center">
									<a href="#helpPharmacyDispense">Pharmacy Dispense</a> |
									<a href="#helpPharmacyInventory">Pharmacy Inventory</a> 								
								</h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11">
							
									<p>
										In the Pharmacy section, User can do follwing operations.
										<ul>
											<li>Dispense the drugs</li>
											<li>Save/View/Update the pharmacy stock</li>
										</ul>
									</p>
									<p class="small-gap"  id="helpPharmacyDispense">
									</p>
									<p class="heading1">
									1. Pharmacy Dispense
									</p>
									<p>
										In this section user can dispense the drug.
									</p>
									<p>
									<span class="heading2"></span><br/>
									To dispense the drug you need to fill/select following fields.
									<p>	
										<ol>
											<li>Visit Id  - Visit id of the patient. [Mandatory field]</li>
											<li>Mode Of Payment - Payment mode. [Mandatory field]</li>
											<li>Discount - Discount on total bill. [Optional field]</li>											
										</ol>
									</p>
									<p>
									  Then Click on <strong>Add Extra</strong> to dispense the extra drug except the prescription.
									</p>
									<p>	
										<ol>
											<li>Drug Name - You can write and search drug name. [Optional field]</li>											
										</ol>
									</p>
									<p class="image">
									<img src="images/addExtra-Pharmacy.PNG" class="img-responsive" />
									</p>
									<p class="small-gap">
									</p>
									<p>
									  Then Click on <strong>Add</strong> to add drug in table.
									</p>
									<p>
									You can also click on checkbox to exclude the medicine.<br/>
									</p>
									<p class="image">
									<img src="images/save-pharmacy-dispense.PNG" class="img-responsive" />
									</p>
									<p class="small-gap">
									</p>
									<p class="small-gap"  id="helpPharmacyInventory">
									</p>
									<p>
									  Then Click on <strong>Save</strong> to dispense the drug.
									</p>
									
									<p class="heading1">
									2. Pharmacy Inventory
									</p>
									<p>
										In this section user can Save/View/Update the drug stock.
									</p>
									<p>
									<span class="heading2"></span><br/>
									To search a drug you can use the following filters to show filtered drugs or directly click on search button to show all the drugs.
									<p>	
										<ol>
											<li>Drug - Name of the drug. [Mandatory field]</li>
											<li>Quantity - Quantity of the drug. [Mandatory field]</li>
											<li>Manufacture Name - First Name of the patient. [optional field]</li>
											<li>Unit Vol - Unit Vol of the drug. [Mandatory field]</li>
											<li>Manufacture Date - Manufacture Date of the drug. [optional field]</li>
											<li>Expiry Date - Expiry Date of the drug. [optional field]</li>
										</ol>
									</p>
									<p class="image">
									<img src="images/update-pharmacy-inventory.PNG" class="img-responsive" />
									</p>
									<p>
									You can also click on the delete/edit icons to delete/change the drug stock.<br/>
									</p>
									<p class="small-gap">
									</p>
									<p>
									  Then Click on <strong>Save</strong> to save the stock of drug.
									</p>
									<p class="image">
									<img src="images/save-pharmacy-inventory.PNG" class="img-responsive" />
									</p>
									
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>
                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>