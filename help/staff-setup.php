<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpGettingStarted').addClass("open");
				$('#helpGettingStartedUl').css("display","block");
				$('#helpStaffSetup').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>
				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Staff Setup</h3>
								<h6 align="center" id="helpDesignations"><a href="#helpDesignations">Designations</a> | <a href="#helpStaffReg">Staff Registration</a> | <a href="#helpViewStaff">View Staff</a> | <a href="#helpAccessManagement">Access Management</a></h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11 ordering">
								<p>
								In Staff Setup section, we setup some data in this system related to the staffs before using the software to manage staff. 
								</p>
								<p class="heading1">1. Designations</p>
								<p>In Designations, admin can define the designations of different staffs present in the hopsital. These designations are used at the time of staff registration, where a staff is deignated to some designation/post.</p>
								<p>
									<span class="heading2">
										Add Designation
									</span><br/>
									To add any designation, you go to the designation screen and click on <strong> +Add Designation </strong> tab. <br/>There you can fill in the form following fields:
									<ol>http://localhost:1111/herp/
										<li>
											Designation Name - Staff designation like Medical Officer, Senior Resident etc.
										</li>
										<li>
											Description about the designation [optional field]
										</li>
									</ol>									
								</p>
								<p class="image">
									<img src="images/022.Designation.Save.PNG" class="img-responsive" />
								</p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br/>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap"></p>
								<p><span class="heading2">Viewing Designations</span><br/>To view all saved designations click on the <strong>Designations List</strong>.</p>
								<p class="image">
									<img src="images/021.Designation.List.PNG" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpStaffReg">
								</p>
								<p>
									<span class="heading2">Updating Designation</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved Designation info.
								</p>
								<p class="image">
									<img src="images/020.Designation.Update.PNG" class="img-responsive" />
								</p>
								<p class="heading1">
								2. Staff Registration
								</p>
								<p>
								In Staff Registration, an official hospital authority can register staffs on their joining at the hospital. Successful registration will send an email with login credntials to the given email id of staff during registration. 
								</p>
								<p>
								<span class="heading2">Registration of Staff/Employee</span><br/>
								To register a staff in the system, you have to fill the registration form entering the fields:
								<ol>
									<li>
									Salutation - Please select as applied [mandatory Field]
									</li>
									<li>
									First Name [mandatory field]
									</li>
									<li>
									Last Name [mandatory field]
									</li>
									<li>
									Date of Birth [mandatory field]
									</li>
									<li>
									Gender [mandatory field]
									</li>
									<li>
									Marital Status [optional field]
									</li>
									<li>
									Joining Date - Date  when staff/employee officially joined the hospital [mandatory field]
									</li>
									<li>
									Address - Residential address of Staff/Employee [mandatory field]
									</li>
									<li>
									Country - Country where staff resides [mandatory field]
									</li>
									<li>
									State - State where staff resides [mandatory field]
									</li>
									<li>
									City - City where staff resides [mandatory field]
									</li>
									<li>
									Zipcode - Zipcode/Pincode of staff residence's area [mandatory field]
									</li>
									<li>
									Email - Personal email id of staff. On this email id staff will receive his login credentials of <?php echo $software_name; ?>. [mandatory field]
									</li>
									<li>
									Mobile - Personal mobile of staff [mandatory field]
									</li>
									<li>
									Phone - Other Phone or contact number fo staff [optional field]
									</li>
									<li>
									Referred By - The person's name who referred this staff to hospital at the time of recruitment. [optional field]
									</li>
								</ol>
								
								After filling all the mandatory fields, please press <strong>Next</strong> to proceed to the next part of form. Beware, pressing the <strong>Reset</strong> button will clear all the entered data in the form.
								</p>
								<p class="image">
									<img src="images/023.1.Staff.Registration.png" class="img-responsive" />
								</p>
								<p>
									<ol start="17">
										<li>
										Staff Type - Whether staff is Doctor, Nurse, Sister etc. [mandatory field]
										</li>
										<li>
										Designation - Designation assigned to the staff. [mandatory field]
										</li>
										<li>
										Department - Department to which staff is posted within. [mandatory field]
										</li>
										<li>
										Degree Held - Degrees held by the staff. [optional field]
										</li>
										<li>
										Emergency Contact Person - Some contact to whom we can contact in case of emergency related to staff. [optional field]
										</li>
										<li>
										Emergency Contact No. - Emergency contact person's contact number. [optional field]
										</li>
										<li>
										Remarks - Any additional info or notes. [optional field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/023.2.Staff.Registration.png" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpViewStaff">
								</p>
								<p class="heading1">
								3. View Staff
								</p>
								<p>
									In View Staff page you can see all registered staffs of the hospital. This page also provides facility to narrow your search by using following filters:
									<ol>
										<li>
											Staff Id - This is a unique ID assigned to Staff after registration.
										</li>
										<li>
										First Name 
										</li>
										<li>
										Last Name
										</li>
										<li>
										Designation
										</li>
										<li>
										Staff Type
										</li>
										<li>
										Mobile
										</li>
									</ol>
									These filters can be used as single or you can use the combination of filters. The blank or not selected  filters will not be counted in search. For e.g. If you want to search all Medical Officers of hospital, you can select <strong>Medical Officer</strong> in the <strong>Designation</strong> field, leaving other fields untouched. It will give you the desired result. If you want to search all medical officers with the last name <strong>Kent</strong>, also enter the <strong>Kent</strong> word in the <strong>Last Name</strong> filter.
									<br/>
									First Name, Last Name &amp; Mobile filters also supports partial word search.
								</p>
								
								<p class="image">
									<img src="images/024.Staff.List.png" class="img-responsive" />
								</p>
								<p>
								<strong>Eye Icon</strong> will show that particular staff's detail.<br/>
								<strong>Pencil Icon</strong> is for editing the staff's details.<br/>
								<strong>Trash/Delete Icon</strong> is for deleting the staff from system.
								</p>
								<!-- Access Management Starts -->
								<p class="para-gap" id="helpAccessManagement">
								</p>
								<p class="heading1">
								4. Access Management 
								</p>
								<p>
									In Access Management page you can assign screen of pages which you need to allow/restrict for a particular user.<br/></br>
									Following are the steps to add/restrict pages for a particular user:
									<ol>
										<li>
											<strong>Case 1: If you know Staff Id of particular staff for which you need to allow/restrict pages.</strong><br/><br/>
												
											<p>Enter the staff Id and click <strong>Select</strong> button on the screen. Then you will see all the allowed/restricted pages as like following image.</p>
											<strong>Case 2: If you don't know Staff Id of particular staff for which you need to allow/restrict pages.</strong><br/><br/>
											<p>
												Click on <strong>Search Icon</strong> located before select button.<br/>
												Then you will see a popup for searching a staff as like discussed in <a href="#helpViewStaff"><strong>View Staff Screen</strong></a>
												
											</p>
											<p>
												After your search completion double click on particular row in table to select that staff.
												Then you will see all the allowed/restricted pages as like following image.
											</p>
										</li>
										<li>
											Check/Uncheck the check boxes to assign/unassign pages.<br/>
											<strong>Note:</strong> If checked then it will be visible by particular staff else it won't.
										</li>
										<li>
											If your selection is complete then click on <strong>Save</strong> button which is located at bottom else click on <strong>Reset</strong> button which is also located at bottom to start with another user.
										</li>
										<li>
											You will see a success message that confirms the operation that your desired changes have been saved successfully.
										</li>
									</ol>
									<br/>
								</p>
								<p class="image">
									<img src="images/066.AccessManagement.png" class="img-responsive" />
								</p>
								<!-- Access Management Ends -->
								
								
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>

                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>