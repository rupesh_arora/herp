<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>
<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpGettingStarted').addClass("open");
				$('#helpGettingStartedUl').css("display","block");
				$('#helpPathologicalSetup').addClass("active");
			});
			
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>
				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Patholgical Setup</h3>
								<h6 align="center" id="helpSpecimenType"><a href="#helpSpecimenType">Specimen Type</a> | <a href="#helpLabTypes">Lab Types</a> | <a href="#helpLabTests">Lab Tests</a> | <a href="#helpRadiologyTests">Radiology Tests</a> | <a href="#helpForensicTests">Forensic Tests</a></h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11">
								<p>
									This section setups the initial data needed for Labs, Radiology and Forensic departments.
								</p>
								<p class="heading1">
								1. Specimen Type
								</p>
								<p>
								In various labs, some sample and specimens are taken from patient to analysis and test. So here in this page we maintain the types of Specimen/Samples with which Labs are dealing.
								</p>
								<p>
								<span class="heading2">Add Specimen Type</span><br/>
								In this page you can add different types of specimen and sample types. To save a specimen type, click on the <strong>+Add Specimen Type</strong> tab. There you can fill the data related to specimen type:
									<ol>
										<li>
										Specimen Type - Name of the specimen type. [mandatory field]
										</li>
										<li>
										Description - About this specimen type. [optional field]
										</li>
									</ol>
								</p>
								<p class="image">
									<img src="images/040.Specimen.Type.Save.png" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
								<span class="heading2">View Specimen Type</span><br/>
								Here on this page you can see all the specimen types.
								</p>
								<p class="image">
									<img src="images/039.Speciemen.Type.List.png" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpLabTypes">
								</p>
								<p class="heading1">
								2. Lab Types
								</p>
								<p>
								In this section we maintain the divisions of pathology labs.
								</p>
								<p>
								<span class="heading2">Add Lab Type</span><br/>
								To add a lab type, please click on the <strong>+Add Lab Type</strong> tab. Then you can fill teh fields in the form:
								<ol>
									<li>
										Lab Type - Name of the lab type. [mandatory field]
									</li>
									<li>
										Description - Info about the lab type. [otional field]
									</li>
								</ol>
								</p>
								<p class="image">
									<img src="images/042LabTypesSave.PNG" class="img-responsive" />
								</p>
								<p>
									Note: Here we also have an option to upload CSV/XLS sheet to upload multiple Lab Types at a time. In sheet first column and second column  must represent name, description respectively.
								</p>
								<p class="small-gap">
								</p>
								<p>
								<span class="heading2">View Lab Types</span><br/>
								Here you can see the whole list of lab types.
								</p>
								<p class="image" >
									<img src="images/041.Lab.Types.List.png" class="img-responsive" />
								</p>
								<p class="small-gap"></p>
								</p>
								<p>
									<span class="heading2">Updating Lab Types</span><br/>
									By clicking the <strong>Pencil Icon</strong>, you can edit a saved Lab Types info.
								</p>
								<p class="image">
									<img src="images/041.Lab.Types.update.PNG" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpLabTests" >
								</p>
								<p class="heading1"> 
								3. Lab Tests
								</p>
								<p>
								In this section, you can define the tests and there sub/components tests done by the hospital for patients.
								</p>
								<p>
								<span class="heading2">Add Lab Test</span><br/>
								In here you can define the lab test and its template. Here you have to save data in 2 parts:<br/><br/>
								<strong>Ist Part (About the Lab Test):</strong><br/>
								<ol>
									<li>
										Test Name - The name of the test. [mandatory field]
									</li>
									<li>
										Lab Type - Select the Lab type to which this test is related. [mandatory field]
									</li>
									<li>
									Specimen Type - Select the type of specimen taken from the patient to test. Not Available should be an option. [mandatory field]
									</li>
									<li>
									Test Fee - The fees charged to patients. [mandatory field]
									</li>
									<li>
									Description - Any info about the test. [optional field]
									</li>
								</ol>
								</p>
								<p class="image">
									<img src="images/044.Lab.Test.Save.No.Sub.Test.png" class="img-responsive" />
								</p>
								<p class="small-gap"></p>
								<p>
								<strong>IInd Part (Adding the sub components of test):</strong><br/>
								After entering the data of test, click on the <strong>Add Component</strong> button. This will add a row in the below table. Fill the data in them:
								<ul>
									<li>
										Sub Component Name - like In CBC test, RBC is a sub-component. [mandatory field]
									</li>
									<li>
										Normal Range - Normal values range for this sub-component test. [mandatory field]
									</li>
									<li>
										Unit - The unit of measuring the value of result. [mandatory field]
									</li>
								</ul>
								<strong>Note:</strong> Atleast one sub component is mandatory with a lab test.
								</p>
								<p class="images">
									<img src="images/044.Lab.Test.Save.Add.Sub.Test.png" class="img-responsive" />
									After adding all components just press <strong>Save</strong> to save the test with thier sub-components.<br/>Beware, clicking <strong> Reset</strong> will clear the whole form, even sub components will be get cleared. 
								</p>
								<p class="small-gap">
								</p>
								<p>
								<span class="heading2">View Lab Tests</span><br/>
								Here we can see all the lab tests. On clicking the edit icon you can also see the sub-components saved in them.
								</p>
								<p class="img">
									<img src="images/043.Lab.Test.List.png" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpRadiologyTests">
								</p>
								<p class="heading1">
									4. Radiology Tests
								</p>
								<p>
								In this section we maintain the radiology tests for patients.
								</p>
								<p>
								<span class="heading2">Add Radiology Test</span><br/>
								In this page, you can add radiology test by entering the following details:
								<ol>
									<li>
										Test Name - The name of radiology test. [mandatory field]
									</li>
									<li>
										Test Fee - The fee for patients taking this test. [mandatory field]
									</li>
									<li>
										Description - Info about the radiology test. [optional field]
									</li>
								</ol>
								</p>
								<p class="image">
								<img src="images/045.Radiology.Test.Save.PNG" class="img-responsive" />
								</p>
								<p>
									Note: Here we also have an option to upload CSV/XLS sheet to upload multiple Radiology Test at a time. In sheet first column, second column and third column must represent name, fee, description respectively.
								</p>
								<p class="small-gap">
								</p>
								<p>
								<span class="heading2">View Radiology Test</span><br/>
								Here you can see all the radiology tests.
								</p>
								<p class="image">
								<img src="images/045.Radiology.Test.List.png" class="img-responsive" />
								</p>
								<p class="para-gap" id="helpForensicTests">
								</p>
								<p class="heading1">
									5. Forensic Tests
								</p>
								<p>
								In this section we maintain the forensic tests for patients.
								</p>
								<p>
								<span class="heading2">Add Forensic Test</span><br/>
								In this page, you can add forensic test by entering the following details:
								<ol>
									<li>
										Test Name - The name of forensic test. [mandatory field]
									</li>
									<li>
										Specimen Type - The name of forensic test. [mandatory field]
									</li>
									<li>
									Normal Range - The normal value range for the result. [optional field]
									</li>
									<li>
									Unit Measure - The unit of analysed result value. [optional field]
									</li>
									<li>
										Test Fee - The fee for patients taking this test. [mandatory field]
									</li>
									<li>
										Description - Info about the forensic test. [optional field]
									</li>
								</ol>
								</p>
								<p class="image">
								<img src="images/047.Forensic.Test.Save.png" class="img-responsive" />
								</p>
								<p class="small-gap">
								</p>
								<p>
								<span class="heading2">View Forensic Test</span><br/>
								Here you can see all the forensic tests.
								</p>
								<p class="image">
								<img src="images/046.Forensic.Test.List.png" class="img-responsive" />
								</p>
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>

                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>