<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpSetting').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Setting</h3>
								<h6 align="center">
									<a href="#helpDailySalesReport">Geolocation</a> |
									<a href="#helpDepartementWiseReport">Units</a> |
									<a href="#helpDischargePatientReport">Configuration</a> 
								</h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11">
							
								<p>
									In the setting section, we can do follwing operations.
									<ul>
										<li>Add,update and delete country,sate,city</li>
										<li>Add the unit for unit measurement</li>
										<li>Configure the software color and prefix</li>
									</ul>
								</p>
								<p class="heading1">
								1. Geolocation
								</p>
								<p>
									In this section we can add,delete and update country,state and city.
								</p>
								<p>
								<span class="heading2"></span><br/>
								To add a new country we need to simply write the name of that country. If a country already exist then you can't add that country.
								</p>

								<p>
									<span class="heading2"></span><br/>
									Click on <strong>Save</strong>button to add new country.
								</p>
								<p class="image">
								<img src="images/01.Setting.Gelocation.png" class="img-responsive" />
								</p>
								

								<span class="heading2"></span><br/>
								To update/delete a country we need to firstly click on the 
								<strong> edit</strong> tab.<br>
								After that we need to choose a country from the dropdown list.
								When you choose any of the country then automatically it's name will be appear in the very next text box from the dropdown list.
								</p>

								<p>
									<span class="heading2"></span><br/>
									After that if we need to update the country then click on the <strong>Update </strong>button to update country.<br>
									<br>
									If we need to delete the country the click on the <strong>Delete</strong> button to delete the country.
								</p>
								<p class="image">
								<img src="images/02.Setting.Geolocation.png" class="img-responsive" />
								</p>
								<p class="small-gap"></p>
								<span class="heading2 "></span><br/>
								To add a new state we need to firstly choose the country from the dropdown list in which we want to add a new state. If a state already exist then you can't add that state.
								</p>

								<p>
									<span class="heading2"></span><br/>
									Click on <strong>Save</strong>button to add new state.
								</p>
								<p class="image">
								<img src="images/03.Setting.Geolocation.png" class="img-responsive" />
								</p>
								<span class="heading2"></span><br/>
								To update/delete a state we need to firstly click on the 
								<strong> edit</strong> tab.<br>
								After that we need to choose a country from the dropdown list.
								Then we need to choose a state from the dropdown list
								When you choose any of the state then automatically it's name will be appear in the very next text box from the dropdown list.
								</p>

								<p>
									<span class="heading2"></span><br/>
									After that if we need to update the state then click on the <strong>Update </strong>button to update state.<br>
									<br>
									If we need to delete the state the click on the <strong>Delete</strong> button to delete the state.
								</p>
								<p class="image">
								<img src="images/04.Setting.Geolocation.png" class="img-responsive" />
								</p>

								<p class="heading2">
									In the similar way you can <strong>Add, delete and Update</strong> city.
								</p>

								<p class="small-gap"></p>
								<p class="heading1">2. Units</p>
								<p>In this section, we define standard unit names and their unit abbreviation with their discription.</p>

								<p>
								<span class="heading2">
									Add unit
								</span><br>
								To add any unit, you go to the unit page and click on <strong> +Add Unit </strong> tab. <br>There you can fill in the form following fields:
								</p>
								<ol>
									<li>
										Unit Name - Standard name for that unit. [mandatory field]
									</li>
									<li>
										Unit Abbreviation - Unit abbreviation of the unit. [mandatory field]
									</li>
									<li>
										Description - About the unit. [optional field]
									</li>
								</ol>
								<p></p>
								<p class="image">
									<img src="images/05.Setting.Unit.png" class="img-responsive">
								</p>
								<p></p>
								<p>
									<strong>Save</strong> will save the data entered in form.<br>
									<strong>Reset</strong> will reset the form as new form by clearing all the data.
								</p>
								<p class="small-gap"></p>
								<p>
									<span class="heading2">View Unit</span><br>
									This page shows whole list of unit defined in system with it's unit abbreviationand description.
								</p>
								<p class="image">
									<img src="images/06.Setting.Unit.png" class="img-responsive">
								</p>

								<p class="para-gap" id="helpDepartments"></p>
								<p class="heading1">
									3. Configuration
								</p>
								<p>
									In the configuration, we can update software configuration setting like we error color, background color, amount prefix, visit id prefix etc.
								</p>
								<p>
									<span class="heading2">
										Update Configuration
									</span><br>
									To add any department, you go to the department screen and click on <strong> +Add Department </strong> tab. <br>There you can fill the <strong>Department Name</strong> [mandatory field] and <strong> Description</strong> [optional field].
								</p>
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>
                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
    </body>
</html>