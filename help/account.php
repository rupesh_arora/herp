<?php 
session_start();
require('../controllers/admin/config.php');
if(isset($_SESSION['globaluser'])){	
}
else if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass'])){
	include_once('../checklogin.php');
	require('../controllers/admin/config.php');
}
else{
	header('Location: ../login.php');
}
?>

<!--[if IE 8]>         
<html class="no-js lt-ie9"> </html>
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
	
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HERP Help Manual</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
		<link href="../css/jquery.dataTables.css" rel="stylesheet"> 
		<link href="../css/dataTables.jqueryui.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/plugins.css" />
        <link rel="stylesheet" href="../css/main.css" />
        <link rel="stylesheet" href="../css/themes.css" />
        <link rel="stylesheet" href="css/help.css" />
        <script src="../script/vendor/jquery-1.11.3.min.js"></script>
        <script src="../script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
		<script src="../script/loader.js"></script>
        <script src="../script/vendor/bootstrap.min.js"></script>		
        <script src="../script/plugins.js"></script>
        <script src="../script/app.js"></script>
        <script src="../script/pages/index.js"></script>
        <script src="scripts/main.js"></script>
        <script>
			$(document).ready(function(){
				$('#helpAccount').addClass("active");
			});
		</script>
		
	</head>
    <body class="bodyOverflow">
    	<!-- pre con is loader div -->
    	<div class="se-pre-con"></div>
        <div id="page-container" class="sidebar-full">
            <!-- Add side bar -->
			<div id="sidebarDiv">
            <?php require_once("sidebar.php");?>
			</div>
            <div id="main-container">
                <!-- Add header -->
                <?php require_once("header.php");?>				
                <div id="page-content">
                    <div class="display_html_pages">
						<div class="block">
							<div class="page-header">
								<h3 align="center">Billing</h3>
								<h6 align="center">
									<a href="#helpCashAccount">Cash Billing</a> |
									<a href="#helpCashPayment">Cash Payment</a> |
									<a href="#helpinsurancePayment">Insurance Payment</a>
								</h6>
							</div>
							<div class="col-md-1 col-lg-1">
							</div>
							<div class="row col-md-11 col-lg-11">
							
									<p>
										In the account section, User can do follwing operations.
										<ul>
											<li>Deposit Amount in our account</li>
											<li>Deposit payment</li>
										</ul>
									</p>
									<p class="small-gap"  id="helpCashAccount">
									</p>
									<p class="heading1">
									1. Cash Billing 
									</p>
									<p>
										In this section user can deposit amount.
									</p>
									<p>
									<span class="heading2"></span><br/>
									To deposit amount you need to fill/select following fields.
									<p>	
										<ol>
											<li>Patient Id  - Patient id of the patient. [Mandatory field]</li>
											<li>Amount To Deposit - Amount to deposit. [Mandatory field]</li>									
										</ol>
									</p>
									<p>
									  Then Click on <strong>Save</strong> to deposit amount.
									</p>
									<p class="image">
									<img src="images/save-cashAccount.PNG" class="img-responsive" />
									</p>
									<p class="small-gap">
									</p>
									<p class="small-gap"  id="helpCashPayment">
									</p>
									
									<p class="heading1">
									2. Cash Payment
									</p>
									<p>
										In this section user can deposit the payment.
									</p>
									<p>
									<span class="heading2"></span><br/>
									To search a visit you can use the following filters to show filtered First Name or directly click on search button to show all the visits.
									<p>	
										<ol>
											<li>Visit Id - Visit id of the patient. [Mandatory field]</li>
											<li>Pay Date - Pay date of payment. [Mandatory field]</li>
											<li>Mode of Payment - Payment mode. [Mandatory field]</li>
											<li>Amount Tendered -Total amount . [Mandatory field]</li>
										</ol>
									</p>
									<p>
									You can also click on checkbox to exclude the bill.<br/>
									</p>
									<p class="small-gap">
									</p>
									<p class="image">
									<img src="images/save-cashPayment.PNG" class="img-responsive" />
									</p>
									<p class="small-gap">
									</p>
									<p><strong>Image OF Cash payment for those user who come out with Insurance </strong> </p>
									</p>
									<p class="small-gap">
									</p>
									<p class="image">
									<img src="images/save_insurance_cash_payment.png" class="img-responsive" />
									</p>
									</p>
									<p class="small-gap">
									</p>
									<p>
									  Then Click on <strong>Submit</strong> to submit the payment.
									</p>
									<p class="image">
									<img src="" class="img-responsive" />
									</p>
									<p class="small-gap"></p>
									<p class="small-gap"  id="helpinsurancePayment">
									</p>
									<p class="heading1">
									3. Insurance Payment
									</p>
									<p>
										In this section user can deposit the Insurance payment.
									</p>
									<p>
									<span class="heading2"></span><br/>
									To search a visit you can use the following filters to show filtered First Name or directly click on search button to show all the visits which come under insurance. 
									<p>	
										<ol>
											<li>Visit Id - Visit id of the patient. [Mandatory field]</li>
										</ol>
									</p>
									
									<p class="small-gap"></p>
									<p class="image">
									<img src="images/save insurance payment.png" class="img-responsive" />
									</p>
									<p class="small-gap">
									</p>
							</div>
							<div style="clear:both;"></div>
						</div>
					</div>
                </div>
                <!-- Add footer -->
                <?php require_once("footer.php");?>
            </div>
        </div>
		<div class="loader">
		   <center>
			   <img class="loading-image" src="../img/loader.gif" alt="loading..">
		   </center>
		</div>
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
        	
		
    </body>
</html>