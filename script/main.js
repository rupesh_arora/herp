/*
 * File Name    :   main.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page  manages menu,dashboard,change password screen
 */
var hospitalName;
var hospitalPhoneNo;
var hospitalEmail;
var hospitalAddress;
var hospitalLogo;
var amountPrefix;
var staffPrefix;
var invoicePrefix;
var visitPrefix;
var productPrefix;
var receiptprefix;
var customerPrefix;
var patientPrefix ='';
var voucherPrefix;
var imprestPrefix = ''
var pettyPrefix = '';
$(document).ready(function() {
	loader();
     $(".gi-exit").click(function(){
        $.session.set('Url','');
    });
    $(".sidebar-brand").click(function(){
        $.session.set('Url','');
    });
    $("#btnLogOut").click(function(){
        $.session.set('Url','');
    });

    $("#profile").click(function(){
        $.session.set('Url','');
    });

    configurationDeatils();
    
    var reloadUrl =  $.session.get('Url');
    if(reloadUrl != undefined){
        if(reloadUrl != "appointment.php"){
            $("#page-content").empty();
            $("#page-content").load(reloadUrl);
            var block = $.session.get('ul_block');
            $("ul").find("[data-url='"+block+"']");
            $("ul").find("[data-url='"+block+"']").addClass('open');
            $("ul").find("[data-url='"+block+"']").parent().find('ul').css("display","block");
            $("ul").find("[data-url='"+block+"']").closest('ul').closest('li');
            $("ul").find("[data-url='"+block+"']").closest('ul').css("display","block");
            $($("ul").find("[data-url='"+block+"']").closest('ul').closest('li').find('a')[0]).addClass('open');
            $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li');
            $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').css("display","block");
            $($("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').find('a')[0]).addClass('open');            
            $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li');
            $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').css("display","block");
            $($("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').find('a')[0]).addClass('open');          
            $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li');
            $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').closest('ul').css("display","block");
            $($("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').find('a')[0]).addClass('open');
        }
        else{
            $("#page-content").empty();
            //$("#page-content").load('scheduler/'+dataUrl);
            $("#page-content").html('<iframe style="width:100%; height:100%;" class="iframe" id="iFrame1" scrolling="no" frameborder="0" src="appointment.php"></iframe>');
             var block = $.session.get('ul_block');
            $("ul").find("[data-url='"+block+"']");
            $("ul").find("[data-url='"+block+"']").addClass('open');
            $("ul").find("[data-url='"+block+"']").parent().find('ul').css("display","block");
            $("ul").find("[data-url='"+block+"']").closest('ul').closest('li');
            $("ul").find("[data-url='"+block+"']").closest('ul').css("display","block");
            $($("ul").find("[data-url='"+block+"']").closest('ul').closest('li').find('a')[0]).addClass('open');
            $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li');
            $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').css("display","block");
            $($("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').find('a')[0]).addClass('open');            
            $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li');
            $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').css("display","block");
            $($("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').find('a')[0]).addClass('open');          
            $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li');
            $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').closest('ul').css("display","block");
            $($("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').find('a')[0]).addClass('open');
        }
    }
    


    /*New Loader Code*/
    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");;
    });
    /*New Loader Code End*/

	// call for get amount prefix
	
	
    sidebarClick = 0;
	loadDashbord();
    //for scroll to top to any menu click
    $(".sidebar-nav li >a").click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, 500);
        return false;
    });    

    /*Getting hospital information*/
    hospitalInformation();
    
    dynamicTimeClock();//function call to get current running time
    // To open side bar on certain resolution
    $("#slide_sidebar").click(function() {
        if (sidebarClick == 0) {
            sidebarClick = 1;
            $("#main-container").css({
                "margin-left": "200px"
            });
            $("#sidebar").css({
                "width": "200px",
            });
        } else {
            sidebarClick = 0;
            $("#main-container").css({
                "margin-left": "0px"
            });
            $("#sidebar").css({
                "width": "0px"
            });
        };
    });

    //loader(); // loader load on the project call that function

	$('.sidebar-nav a').on('click',function(){
		var dataUrl = $(this).attr("data-url");
        //thisblock =  $(this);
		if(dataUrl != undefined && dataUrl != ''){
			if(dataUrl != "appointment.php"){
			     loader();
				$("#page-content").empty();
                loader();
				//removejscssfile("../scheduler/src/jquery.js", "js");
				$("#page-content").load('views/admin/'+dataUrl);
                $.session.set('Url', 'views/admin/'+dataUrl);
                $.session.set('ul_block', dataUrl);
                $("#BBIT_DP_CONTAINER").remove();
			}else{
                loader();
				$("#page-content").empty();
                loader();
				//$("#page-content").load('scheduler/'+dataUrl);
				$("#page-content").html('<iframe style="width:100%; height:100%;" class="iframe" id="iFrame1" scrolling="no" frameborder="0" src="appointment.php"></iframe>');
			 
                $.session.set('Url', dataUrl);
                $.session.set('ul_block', dataUrl);
            }
		}
	});
   
    $(".dashBoardTab").click(function(){
        var dataUrl = $(this).attr("data-url");
        if(dataUrl != ""){
            if (dataUrl != "appointment.php") {
                $("#page-content").empty();
                $("#page-content").load('views/admin/'+dataUrl);
                $.session.set('Url', 'views/admin/'+dataUrl);
                 $.session.set('ul_block', dataUrl);
                var block = dataUrl;
                $("ul").find("[data-url='"+block+"']");
                $("ul").find("[data-url='"+block+"']").addClass('open');
                $("ul").find("[data-url='"+block+"']").parent().find('ul').css("display","block");
                $("ul").find("[data-url='"+block+"']").closest('ul').closest('li');
                $("ul").find("[data-url='"+block+"']").closest('ul').css("display","block");
                $($("ul").find("[data-url='"+block+"']").closest('ul').closest('li').find('a')[0]).addClass('open');
                $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li');
                $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').css("display","block");
                $($("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').find('a')[0]).addClass('open');            
                $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li');
                $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').css("display","block");
                $($("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').find('a')[0]).addClass('open');          
                $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li');
                $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').closest('ul').css("display","block");
                $($("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').find('a')[0]).addClass('open');
            }
            else {
                $("#page-content").empty();
                $("#page-content").html('<iframe style="width:100%; height:100%;" class="iframe" id="iFrame1" scrolling="no" frameborder="0" src="appointment.php"></iframe>');
                $.session.set('Url', dataUrl);
                 $.session.set('ul_block', dataUrl);
                var block =dataUrl;
                $("ul").find("[data-url='"+block+"']");
                $("ul").find("[data-url='"+block+"']").addClass('open');
                $("ul").find("[data-url='"+block+"']").parent().find('ul').css("display","block");
                $("ul").find("[data-url='"+block+"']").closest('ul').closest('li');
                $("ul").find("[data-url='"+block+"']").closest('ul').css("display","block");
                $($("ul").find("[data-url='"+block+"']").closest('ul').closest('li').find('a')[0]).addClass('open');
                $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li');
                $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').css("display","block");
                $($("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').find('a')[0]).addClass('open');            
                $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li');
                $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').css("display","block");
                $($("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').find('a')[0]).addClass('open');          
                $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li');
                $("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').closest('ul').css("display","block");
                $($("ul").find("[data-url='"+block+"']").closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').closest('ul').closest('li').find('a')[0]).addClass('open');
            }                
        }            
    });
    // clear popup setting fields
    clear();
    $("#close").click(function() {
        clear();
    });
    $("#modal-user-settings").blur(function() {
        clear();
    });

    /* click perform for change password with validation */
    $("#submit").click(function() {
        var flag = false;
		
        if ($("#modal-user-settings #user-settings-password").val() == "") {
            $("#modal-user-settings #errorNewPwd").show();
            $("#modal-user-settings #errorNewPwd").text("Please enter valid password");
            $("#modal-user-settings #user-settings-password").focus();
            $("#modal-user-settings #user-settings-password").css("border-Color", "#a94442");
            flag = true;
        }
		if ($("#modal-user-settings #user-settings-password").val().length < "8") {
            $("#modal-user-settings #errorNewPwd").show();
            $("#modal-user-settings #errorNewPwd").text("Password must be at least 8 characters");
            $("#modal-user-settings #user-settings-password").focus();
            $("#modal-user-settings #user-settings-password").css("border-Color", "#a94442");
            flag = true;
        }
        if ($("#modal-user-settings #user-settings-old-password").val() == "") {
            $("#modal-user-settings #errorOldPwd").show();
            $("#modal-user-settings #errorOldPwd").text("Please enter valid password");
            $("#modal-user-settings #user-settings-old-password").focus();
            $("#modal-user-settings #user-settings-old-password").css("border-Color", "#a94442");
            flag = true;
        }
        if (flag == true) {

            return false;

        }
        // call ajax  for change password
        var email = $("#modal-user-settings #setting-email").text();
        var oldPwd = $("#modal-user-settings #user-settings-old-password").val();
        var newPwd = $("#modal-user-settings #user-settings-password").val();
        var newConfirmPwd = $("#modal-user-settings #user-settings-repassword").val();
        $.ajax({ // ajax call for change password
            type: "POST",
            cache: false,
            url: "./updateprofile.php",
            data: {
                "operation": "update",
                user_settings_email: email,
                user_settings_old_password: oldPwd,
                user_settings_password: newPwd,
                user_settings_repassword: newConfirmPwd
            },
            success: function(data) {
                if (data == "check password") {
                    $("#modal-user-settings #errorMessage").show();
                    $("#modal-user-settings #errorMessage").text("Password not matched");
                    $("#modal-user-settings #user-settings-password").val("");
                    $("#modal-user-settings #user-settings-repassword").val("");
                    $("#modal-user-settings #user-settings-password").focus();
                    $("#modal-user-settings #user-settings-repassword").css("border-Color", "#a94442");
                    $("#modal-user-settings #user-settings-password").css("border-Color", "#a94442");

                }
                if (data == "wrong password") {
                    $("#modal-user-settings #errorMessage").show();
                    $("#modal-user-settings #errorMessage").text("Wrong password");
                    $("#modal-user-settings #user-settings-old-password").val("");
                    $("#modal-user-settings #user-settings-password").val("");
                    $("#modal-user-settings #user-settings-repassword").val("");
                    $("#modal-user-settings #user-settings-old-password").focus();
                    $("#modal-user-settings #user-settings-repassword").css("border-Color", "#a94442");
                    $("#modal-user-settings #user-settings-password").css("border-Color", "#a94442");
                    $("#modal-user-settings #user-settings-old-password").css("border-Color", "#a94442");


                }
                if (data == "success") {
                    $('#modal-user-settings').modal('hide');
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Please check your email, the password has been changed.");
                    $('#messagemyModal').modal();
                    /* $("#modal-user-settings #errorMessage").show();
                    $("#modal-user-settings #errorMessage").css({"color":"#6AD2EB"});
                    $("#modal-user-settings #errorMessage").text("Please check your email id password should be change."); */
                    $("#modal-user-settings #user-settings-old-password").val("");
                    $("#modal-user-settings #user-settings-password").val("");
                    $("#modal-user-settings #user-settings-repassword").val("");
                }

            },
            error: function() {}
        });
    });
    // for remove style with validation
    $("#modal-user-settings #user-settings-password").keyup(function() {

        if ($(this).val() != "") {
            $("#modal-user-settings #errorNewPwd").hide();
            $("#modal-user-settings #errorMessage").hide();
            $(this).removeAttr("style");
            //	$("label[for='"+$(this).attr("id")+"']").removeAttr("style");
        }
    });
    $("#modal-user-settings #user-settings-old-password").keyup(function() {

        if ($(this).val() != "") {
            $("#modal-user-settings #errorOldPwd").hide();
            $("#modal-user-settings #errorMessage").hide();
            $(this).removeAttr("style");
            //	$("label[for='"+$(this).attr("id")+"']").removeAttr("style");
        }
    });
    $("#modal-user-settings #user-settings-repassword").keyup(function() {

        if ($(this).val() != "") {
            $("#modal-user-settings #errorConfirmPwd").hide();
            $("#modal-user-settings #errorMessage").hide();
            $(this).removeAttr("style");
            //	$("label[for='"+$(this).attr("id")+"']").removeAttr("style");
        }
    });

    // load DashBoard Deatils to calling function
    var mainContainerHeight = $("#main-container").height();
   $('.sidebar-scroll').css({
        "max-height":mainContainerHeight,
        "min-height":mainContainerHeight
    });


    /*Notification poup*/
    $("#notficatonClick").click(function(){
        var postData ={
            operation : "showNotification"
        };
        var html = '';
        dataCall("controllers/admin/insurance_payment.php", postData, function (result){
            if (result.length > 2) {
                var parseData = JSON.parse(result);
                $.each(parseData,function(i,v){
                    html+='<div class="NotificationMsgContainer">';
                    html+='<div class="notificationMsg"><h6>'+parseData[i].message+'</h6></div>';
                    html+='<div class="notificationMsgDate"><p>'+parseData[i].date+'</p></div>';
                    html+='</div>';
                });
            }

            $('#myNotificationModal').modal();
            $("#myNotificationModalLabel").text("Notification");
            $("#notificationModalBody").html(html);
        });        
    });

   


});

function loader() { // define loader functionality into that function
    $.ajax({
        // your ajax code
        beforeSend: function() {
            $('.loader').show();
        },
        complete: function() {
            $('.loader').hide();
        }
    });
}

function clear() { //cleat function use for change password
    $("#modal-user-settings #user-settings-old-password").val("");
    $("#modal-user-settings #user-settings-password").val("");
    $("#modal-user-settings #user-settings-repassword").val("");
    $("#modal-user-settings #errorConfirmPwd").hide();
    $("#modal-user-settings #errorOldPwd").hide();
    $("#modal-user-settings #errorNewPwd").hide();
    $("#modal-user-settings #user-settings-old-password").removeAttr("style");
    $("#modal-user-settings #user-settings-password").removeAttr("style");
    $("#modal-user-settings #user-settings-repassword").removeAttr("style");
    $("#modal-user-settings #errorMessage").hide();
}
function loadDashbord(){	
	var currentTimeStampUTC = Date.parse(new Date())/1000;
	$.ajax({     
		type: "POST",
		cache: false,
		url: "./controllers/admin/dashboard.php",
		data: {
			"operation":"showDashboard",
			"currentTimeStamp":currentTimeStampUTC			
		},
		success: function(data) {
			if(data != ""){
				var parseData= jQuery.parseJSON(data); 
				$("#lblTotalVisits").text(parseData[0].totalvisits);
				$("#lblTodayVisits").text(parseData[0].todayvisit);
				$("#lblWeekVisits").text(parseData[0].thisweek);
				$("#lblMonthVisits").text(parseData[0].thismonth);
				$("#lblYearVisits").text(parseData[0].thisyear);
				$("#lblTotalUnpaidBill").text("$ "+parseData[1].totalUnpaidBill);
				$("#lblTotalBillPaid").text("$ "+parseData[1].totalPaidBill);
			}
			if(data == ""){
				
			}
		},
		error:function(){
		}
	});
} 
function hospitalInformation(){

    var postData = {
        "operation": "hospitalProfile"
    }

    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/opd_registration.php",
        datatype: "json",
        data: postData,

        success: function(data) {
            if (data != "0" && data != "") {          
                var parseData = jQuery.parseJSON(data);
                
                hospitalName = parseData[0].hospital_name;
                hospitalPhoneNo = parseData[0].phone_number;
                hospitalEmail = parseData[0].email;
                hospitalAddress = parseData[0].address;
                hospitalLogo = parseData[0].images_path;
                //print hospital name in header
                $(".hospitalName").text(hospitalName);                
            }           
        },
        error: function() {             
            $('.modal-body').text("");
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();
        }            
    });
}

// getAmountPrefix function
/*function getAmountPrefix(){
    var postData = {
        "operation": "show"
    }
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/configuration.php",
        datatype: "json",
        data: postData,

        success: function(data) {
            if(data != "0" && data != ""){
				var parseData = jQuery.parseJSON(data);				
				amountPrefix = parseData[5].value;
			}
        },
        error: function() {             
            $('.modal-body').text("");
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();
        }            
    });
}*/

function todayDate(){
    var fullDate = new Date();  
    var twoDigitDate = '';   
    //convert month to 2 digits
    var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
    if (fullDate.getDate().toString().length < 2) {
       twoDigitDate = "0"+fullDate.getDate()
       var currentDate = fullDate.getFullYear() + "-" + twoDigitMonth + "-" + twoDigitDate;
    }
    else{
        var currentDate = fullDate.getFullYear() + "-" + twoDigitMonth + "-" + fullDate.getDate();
    }
    
    return currentDate;
}

//this function is for removing the loaded js file from page
function removejscssfile(filename, filetype){
    //loader();
	var targetelement=(filetype=="js")? "script" : (filetype=="css")? "link" : "none" //determine element type to create nodelist from
    var targetattr=(filetype=="js")? "src" : (filetype=="css")? "href" : "none" //determine corresponding attribute to test for
    var allsuspects=document.getElementsByTagName(targetelement)
    for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
    if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1)
        allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
    }
}

/*function for running clock*/
function dynamicTimeClock() {
    var todayDate = new Date();
    var countHours = todayDate.getHours();
    var countMonth = todayDate.getMinutes();
    var countSecond = todayDate.getSeconds();
    countMonth = checkTime(countMonth);
    countSecond = checkTime(countSecond);
    window.parent.$('#currentTime').html(countHours + ":" + countMonth + ":" + countSecond);
    var time = setTimeout(dynamicTimeClock, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
function configurationDeatils(){
    var postData = {
        "operation": "getConfigurationDetails"
    }
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/configuration.php",
        datatype: "json",
        data: postData,

        success: function(result) {
            if (result.length > 2) {
                var parseData = JSON.parse(result);
                $.each(parseData,function(i,v){

                    if (i==0) {
                        var style = document.createElement('style');
                        style.type = 'text/css';
                        style.innerHTML = '.errorStyle,errorStyle:focus { border-color: #'+v.value +" !important"+'}'+'.error,.text-error,.text-danger { color: #'+v.value+'}';
                        document.getElementsByTagName('head')[0].appendChild(style);
                    }
                    else if(i == 1){
                        patientPrefix = v.value;
                    }
                    else if(i == 2){
                        visitPrefix = v.value;
                    }
                    else if(i == 5){
                        amountPrefix = v.value;
                    }
                    else if(i == 7){
                        $("#page-content").css('background-color','#'+v.value)
                    }
                    else if(i == 8){
                        staffPrefix = v.value;
                    }
                    else if(i == 9){
                        invoicePrefix = v.value;
                    }
                    else if (i== 10) {
                        productPrefix = v.value;
                    }
                    else if (i== 11) {
                        receiptprefix = v.value;
                    }
                    else if (i== 12) {
                        customerPrefix = v.value;
                    }
                    else if( i == 13){
                        imprestPrefix  = v.value;
                    }
                    else if( i == 14){
                        voucherPrefix  = v.value;
                    }
                    else if( i == 15){
                        pettyPrefix  = v.value;
                    }
                });
            }
        },
        error: function() {             
            $('.modal-body').text("");
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();
        } 
    });
}