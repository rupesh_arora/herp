var btable;
$(document).ready(function(){
    loader();
    debugger;    
	$("#txtStaffId").focus();			
	getAllScreens();

	$("#txtStaffId").keyup(function(){
		if ($("#txtStaffId").val().trim() != '') {
			$("#txtStaffIdnError").text("");
			$("#txtStaffId").removeClass("errorStyle");         
		} else {
			$("#txtStaffIdnError").text("Please enter Staff Id");
			$("#txtStaffId").addClass("errorStyle");
		}
	});

	$("#txtStaffId").change(function(){
		if ($("#txtStaffId").val().trim() != '') {
			$("#txtStaffIdnError").text("");
			$("#txtStaffId").removeClass("errorStyle");          
		} else {
			$("#txtStaffIdnError").text("Please enter Staff Id");
			$("#txtStaffId").addClass("errorStyle");
		}
	}); 

	$("#btnStaffReset").click(function(){
		$("#viewScreenTable tr:not(':first') td").find("input:checkbox").prop("checked", false);
		$("#txtStaffId").removeClass("errorStyle");
		$("#txtStaffId").val("");
		$("#txtStaffIdnError").text("");
		$("#txtStaffName").val("");
		$("#txtStaffType").val("");
		$("#txtStaffDes").val("");
		$("#txtJoiningDate").val("");
		$("#txtStaffGender").val("");
		$("#txtStaffMob").val("");
	});

	$("#iconStaffSearch").click(function(){
		$('#searchModalLabel').text("Search Staff");
        $('#searchModalBody').load('views/admin/view_staff.html');
		$('#mySearchModal').modal();
	});
	
	
	
	//getting data of text box on click of button
	$("#btnStaffSelect").click(function(){
		
		if ($("#txtStaffId").val() != '-1') {
			$("#txtStaffIdnError").text("");
			$("#txtStaffId").removeClass("errorStyle");         
		}
		
		var flag = "false";
		if($("#txtStaffId").val() == ""){
			$("#txtStaffIdnError").text("Please enter Staff Id");
			$("#txtStaffId").addClass("errorStyle");
			flag = "true";
		}
		if(flag == "true"){
			return false;
		}
		
		var staffId = $("#txtStaffId").val().trim();            
        if (staffId !='') {
            if (staffId.length != 9) {
            	$("#txtStaffId").addClass("errorStyle");
            	$("#txtStaffId").focus();
            	$("#txtStaffIdnError").text("Staff id is not valid");
                return false;
            }
            var getStaffPrefix = staffId.substring(0, 3);
            if (getStaffPrefix.toLowerCase() != staffPrefix.toLowerCase()) {
                return false;
            }
            staffId = staffId.replace ( /[^\d.]/g, '' ); 
            staffId = parseInt(staffId);
        }
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/access_management.php",
			data :{            
				"operation" : "getScreenAccess",
				"id" : staffId
			},
			success: function(data) {
				if(data != "0" && data != "") {
					$('#staffModelLabel').text("Staff Details");
					$('.modal-body').text();
					var parseData = jQuery.parseJSON(data);
									
					if(parseData.length > 0) {
						$(".lineHtml").show();
						$("#txtStaffName").val(parseData[0].first_name + " " + parseData[0].last_name);
						$("#txtStaffType").val(parseData[0].type);
						$("#txtStaffDes").val(parseData[0].designation);
						$("#txtJoiningDate").val(parseData[0].joining_date);
						$("#txtStaffGender").val(parseData[0].gender);
						$("#txtStaffMob").val(parseData[0].mobile);
						
						bindScreenAccessData(parseData);
					}
				} else {					
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("Staff Id does not exist!!!");
					$('#messagemyModal').modal();
				}
			},
			error:function() {
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Error occurred while loading the data!!!");
					$('#messagemyModal').modal();
			}
		});
	});
	
	$(btnStaffSave).on('click',function(){
		var flag = "false";
		if($("#txtStaffId").val() == ""){
			$("#txtStaffIdnError").text("Please enter Staff Id");
			$("#txtStaffId").addClass("errorStyle");
			flag = "true";
		}
		if(flag == "true"){
			return false;
		}
		var staffId = $("#txtStaffId").val().trim(); 

        if (staffId !='') {
	        if (staffId.length != 9) {
	        	$("#txtStaffId").addClass("errorStyle");
	        	$("#txtStaffId").focus();
	        	$("#txtStaffIdnError").text("Staff id is not valid");
	            return false;
	        }
	        var getStaffPrefix = staffId.substring(0, 3);
	        if (getStaffPrefix.toLowerCase() != staffPrefix.toLowerCase()) {
	            return false;
	        }
	        staffId = staffId.replace ( /[^\d.]/g, '' ); 
	        staffId = parseInt(staffId);
	    }
		
		$.ajax({					
			type: "POST",
			cache: false,
			url: "controllers/admin/view_staff.php",
			data: {"id": staffId, "operation": "staffDetail"},
			
			success: function(dataSet) {
				if(jQuery.parseJSON(dataSet).length) {
					var arrData = [];
					var arrDatas = $(".lineHtml input");
					arrDatas.each(function() {
						var obj = {};
						obj.id = $(this).attr("data-id");
						obj.isViewChecked = $(this).is( ':checked' );
						obj.isEditChecked = false;
						obj.isDeleteChecked = false;
						//var rowVal = id + "_" + isViewChecked + "_" + isEditChecked + "_" + isDeleteChecked;
						arrData.push(obj);
					});
					
					 $.ajax({					
						 type: "post",
						 cache: false,
						 url: "controllers/admin/access_management.php",
						 data :{            
							 "operation" : "saveScreensAccess",
							 "id" : jQuery.parseJSON(dataSet)[0].id,
							 "objdata" : JSON.stringify(arrData)
						 },						
						 success: function(data) {
							if(data != "0" && data != ""){
								$('#messageMyModalLabel').text("Success");
								$('.modal-body').text("Saved Successfully!!!");
								$('#mymessageModal').click();
								clearFormDetails('#advanced-first');
								$('input[type=checkbox]').prop("checked",false);
								$('.lineHtml ul').attr('style','');
							}
						 },
						 error:function() {
							$('#messageMyModalLabel').text("Error");
							$('.modal-body').text("Error occurred while saving data!!!");
							$('#mymessageModal').click();
						 }
					 });
				}
				else{
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("This user is not active.Please activate this user to save screen access!!!");
					$('#mymessageModal').click();
				}					
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Error occurred while saving data!!!");
				$('#mymessageModal').click();
			}
		});
	});

	$('#txtStaffId').keypress(function (e) {
        if (e.which == 13) {
            $("#btnStaffSelect").click();
        }
    });

	
    
	/*/* grab important elements 
		var sortInput = $('#sort_order');
	var submit = $('#autoSubmit');
	var messageBox = $('#message-box');
	var list = $('#sortable-list');
	/* create requesting function to avoid duplicate code 
	var request = function() {
		$.ajax({
			beforeSend: function() {
				messageBox.text('Updating the sort order in the database.');
			},
			complete: function() {
				messageBox.text('Database has been updated.');
			},
			data: 'sort_order=' + sortInput[0].value + '&ajax=' + submit[0].checked + '&do_submit=1&byajax=1', //need [0]?
			type: 'post',
			url: 'controllers/admin/access_management.php'
		});
	};
	 worker function 
	var fnSubmit = function(save) {
		var sortOrder = [];
		list.children('li').each(function(){
			sortOrder.push($(this).data('id'));
		});
		sortInput.val(sortOrder.join(','));
		console.log(sortInput.val());
		if(save) {
			request();
		}
	};
	/* store values
	list.children('li').each(function() {
		var li = $(this);
		li.data('id',li.attr('title')).attr('title','');
	});
	/* sortables 
	list.sortable({
		opacity: 0.7,
		update: function() {
			fnSubmit(submit[0].checked);
		}
	});
	list.disableSelection();
	/* ajax form submission 
		$('#dd-form').bind('submit',function(e) {
		if(e) e.preventDefault();
		fnSubmit(true);
	});*/
});
function getAllScreens() {
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/access_management.php",
		data: {"operation": "getAllScreens"},
		
		success: function(dataSet) {			
			$('.lineHtml').append(dataSet);
			$('.lineHtml input').unbind();
			$( '.lineHtml input' ).change( function() {
				/* if($(this).prop("checked")){					
					if($(this).parent().hasClass("parent")){
						if($( this ).parent().find('input:checked').length == 1){
							$(this).parent().find("input").prop("checked",true);							
						}
						else{
							$(this).parent().parent().find("input:first").prop("checked",true);
						}
						
					}
					else{
						if(( $( this ).parent().parent().find('input:checked').length  == $( this ).parent().parent().find('input').length)){
							$(this).parent().parent().find("input:first").prop("checked",true);
						}
					}
				}
				else{
					
				} */
				
			});
			$('input[type=checkbox]').unbind();
			$('input[type=checkbox]').change(function(){
				// children checkboxes depend on current checkbox
				if($(this).is(':checked')){											
					$(this).next().next().find('input[type=checkbox]').prop('checked',this.checked);
					// go up the hierarchy - and check/uncheck depending on number of children checked/unchecked
					$(this).parents('ul').prev().siblings('input[type=checkbox]').prop('checked',this.checked);
				}
				else{
					if($(this).next().next().find('input[type=checkbox]').is(':checked')){						
						$(this).next().next().find('input[type=checkbox]').prop('checked',false);						
					}
					else{
						$(this).next().next().find('input[type=checkbox]').prop('checked',this.checked);
						// go up the hierarchy - and check/uncheck depending on number of children checked/unchecked
						if(!$(this).parent().parent().find('input[type=checkbox]').is(':checked')){
							//unckeck check box when all child is unchecked
							$(this).parent().parent().siblings('input[type=checkbox]').prop('checked',this.checked);
							//find top menu checkbox
							if(!$(this).parent().parent().parent().siblings().find('input[type=checkbox]').is(':checked')){
								//unckeck check box when all child is unchecked
								$(this).parent().parent().parent().parent().siblings('input[type=checkbox]').prop('checked',this.checked);
							}
						}						
					}				
				}						
			});
			
			$( '.lineHtml li' ).each( function() {
				if( $( this ).children( 'ul' ).length > 0 ) {
					$( this ).addClass( 'parent' );     
				}
			});
			
			$( '.lineHtml li.parent > a' ).click( function( ) {
				$( this ).parent().toggleClass( 'active' );
				$( this ).parent().children( 'ul' ).slideToggle( 'slow' );
				
			});
			/*$("#sortable" ).sortable();
		    $("#sortable" ).draggable();
		   	$("#sortable" ).droppable({
		      drop: function( event, ui ) {
		      	debugger;
		        $( this ).find('input').attr('data-id');
		      }
		    });*/
		    	var dragIndex;
	            var dragParentId; 
	            var dropIndex;
	            var pageId;
	            var dropParentId;
	            debugger; 
		        $("#sortable").sortable({
		        	axis: 'y',
		        	opacity: 0.6,
        			cursor: 'pointer',
        			helper: function(event, ui){
					    var $clone =  $(ui).clone();
					    $clone .css('position','absolute');
					    return $clone.get(0);
					},
		        	start: function(event, ui) {
			            dragIndex = ui.item.index();
			            dragParentId = $($(ui['item']).find('input')[0]).attr('data-parentid');
			            pageId =  $($(ui['item']).find('input')[0]).attr('data-id');
			        },
			        update: function(event, ui) { 
			        	dropIndex = ui.item.index();
			        	if($(ui['item']).parent().parent().find('.root').html() == undefined){
			        		dropParentId = $($(ui['item']).parent().parent().find('input')[0]).attr('data-id');
			        	}
			            else{
			            	dropParentId = "0";
			            }
			        },
		            stop : function(event, ui){	      
		        	var data = $(this).sortable('serialize');
		        	if($(ui['item']).parent().parent().find('.root').html() == undefined){
			        		var parentId = $($(ui['item']).parent().parent().find('input')[0]).attr('data-id');
			        	}
			            else{
			            	parentId = "0";
			            }
		        	
		        	var id = $($(ui['item']).find('input')[0]).attr('data-id');
		        	var itemParentId = $($(ui['item']).find('input')[0]).attr('data-parentid');
		        	//if(itemParentId != "0"){
		        		$($(ui['item']).find('input')[0]).attr('data-parentid',parentId);

			        	$.ajax({					
							type: "POST",
							cache: false,
							url: "controllers/admin/access_management.php",
							data: {
								"id": id, 
								"parentId": parentId, 
								"dragIndex": dragIndex, 
								"pageId": pageId, 
								"dragParentId": dragParentId, 
								"dropIndex": dropIndex, 
								"dropParentId": dropParentId, 
								"operation": "changeLocation"
							},
							
							success: function(dataSet) {

							},
							error:function() {
								$('#messageMyModalLabel').text("Error");
								$('.modal-body').text("Error occurred while saving data!!!");
								$('#mymessageModal').click();
							}

						});
		        	//}	

		        }
		    });
		  $("#sortable").disableSelection();

		},
		error:function() {
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Error occurred while saving data!!!");
			$('#mymessageModal').click();
		}
	});
}


function bindScreenAccessData(data) {
	$(".lineHtml input:checkbox").prop("checked", false);
	
	if(data.length > 0) {
		for(i = 0; i< data.length; i++) {
			var arrDatas = $(".lineHtml");
			var screenId = data[i].screenId;
			var isViewPermission = data[i].isViewPermission;
			arrDatas.find("input:checkbox[data-id=" + screenId +"][class='view']").
			prop('checked', parseInt(isViewPermission));
		}
	}
}