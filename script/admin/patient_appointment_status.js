$(document).ready(function() {	
	loader();	
    var patientAppointmentStatusTbl;
	patientAppointmentStatusTbl = $('#patientAppointmentStatusTbl').dataTable({
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",			
		"sAjaxSource":"controllers/admin/patient_appointment_status.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "showPatientAppointmentStatus" });
		},
		"aoColumns": [
			{  
				"mData": function (o) { 	
				var PatientId = o["patient_id"];
				var patientPrefix = o["patient_prefix"];
					
				var PatientIdLength = PatientId.length;
				for (var i=0;i<6-PatientIdLength;i++) {
					PatientId = "0"+PatientId;
				}
				PatientId = patientPrefix+PatientId;
				return PatientId; 
				}
			},
			{  
				"mData": "patient_name"
			},
			{ "mData": "consultant_name" },
			{ "mData": "time_duration" },
			{ "mData": "status" }
		],
		
	   aoColumnDefs: [{
			aTargets: [4],
			bSortable: false
		}]
	});
});

//# sourceURL=filename.js