/*
 * File Name    :   insurance
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   22feb feb, 2016
 * Description  :   This page use for load,save,update,delete,resotre operation
 */
var insuranceTypeTable; //defining a global varibale for data table
$(document).ready(function() {
    loader();

    $("#form_dept").hide();
    $("#department_list").css({
        "background": "#fff"
    });
    $("#tab_dept").css({
        "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
        "color": "#fff"
    });

    //Hide table record 
    $("#tab_add_dept").click(function() { // click on add insurance tab
        $("#form_dept").show();
        $(".blackborder").hide();
        clear();
        $("#tab_add_dept").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tab_dept").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#department_list").css({
            "background": "#fff"
        });
        $('#txtName').focus();
    });

    //Show table record
    $("#tab_dept").click(function() { // click on list insurance tab
        $('#inactive-checkbox-tick').prop('checked', false).change();
        $("#form_dept").hide();
        $(".blackborder").show();
        $('#tab_add_dept').html("+Add Insurance Type");
        $("#tab_add_dept").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#tab_dept").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#department_list").css({
            "background": "#fff"
        });
        $("#txtNameError").text("");
        $("#txtName").removeClass("errorStyle");
    });

    /* click on button for save  inforamtion */
    $("#btnAddInsurance").click(function() {
        // validation
        var flag = "false";
        $("#txtName").val($("#txtName").val().trim());

        if ($("#txtName").val() == '') {
            $('#txtName').focus();
            $("#txtNameError").text("Please enter insurance type");
            $("#txtName").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtName").val().length > 50) {
            $('#txtName').focus();
            $("#txtNameError").text("Please enter type less than 50");
            $("#txtName").addClass("errorStyle");
            flag = "true";
        }
        if (flag == "true") {
            return false;
        }

        //ajax call for save operation
        var insuranceName = $("#txtName").val();
        var description = $("#txtDescription").val();
        description = description.replace(/'/g, "&#39");
        var postData = {
            "operation": "save",
            "insuranceName": insuranceName,
            "description": description
        }

        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/insurance_type.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Insurance type saved successfully!!!");
                    $('#messagemyModal').modal();
                    clear();
                    //after sucessful data sent to database sent to datatable list
                    $("#form_dept").hide();
                    $(".blackborder").show();
                    $("#tab_add_dept").css({
                        "background": "#fff",
                        "color": "#000"
                    });
                    $("#tab_dept").css({
                        "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
                        "color": "#fff"
                    });
                    $('#inactive-checkbox-tick').prop('checked', false).change();

                } else {
                    $("#txtNameError").text("Insurance type is already exists");
                    $("#txtName").addClass("errorStyle");
                    $("#txtName").focus();
                }

            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });

    });

    //keyup functionality for remove validation style
    $("#txtName").keyup(function() {
        if ($("#txtName").val() != '') {
            $("#txtNameError").text("");
            $("#txtName").removeClass("errorStyle");
        }
    });

    //load Datatable data on page load
    if ($('.inactive-checkbox').not(':checked')) {
        insuranceTypeTable = $('#tblInsuranceType').dataTable({
            "bFilter": true,
            "processing": true,
            "bAutoWidth":false,
            "sPaginationType": "full_numbers",
            "fnDrawCallback": function(oSettings) {
                // update click event   
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = insuranceTypeTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var insurance = mData["name"];
                        var description = mData["description"];
                        editClick(id, insurance, description);
                    }
                });
                // delete click event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = insuranceTypeTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },

            "sAjaxSource": "controllers/admin/insurance_type.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [{
                "mData": "name"
            }, {
                "mData": "description"
            }, {
                "mData": function(o) {
                    return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                        "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                        " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                        "  style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                        "  data-original-title='Delete'></i>";
                }
            }, ],
            aoColumnDefs: [{
                aTargets: [2],
                bSortable: false
            }, {
                aTargets: [1],
                bSortable: false
            }]
        });
    }

    //checked or not checked functionality to show active or inactive data
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) {
            insuranceTypeTable.fnClearTable();
            insuranceTypeTable.fnDestroy();
            insuranceTypeTable = "";
            insuranceTypeTable = $('#tblInsuranceType').dataTable({
                "bFilter": true,
                "processing": true,
                 "bAutoWidth":false,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // restor click event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = insuranceTypeTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            restoreClick(id);
                        }   
                    });
                },

                "sAjaxSource": "controllers/admin/insurance_type.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [{
                    "mData": "name"
                }, {
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                    }
                }, ],
                aoColumnDefs: [{
                    aTargets: [2],
                    bSortable: false
                }, {
                    aTargets: [1],
                    bSortable: false
                }]
            });
        } else {
            insuranceTypeTable.fnClearTable();
            insuranceTypeTable.fnDestroy();
            insuranceTypeTable = "";
            insuranceTypeTable = $('#tblInsuranceType').dataTable({
                "bFilter": true,
                "processing": true,
                "bAutoWidth":false,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // for update click event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = insuranceTypeTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var insurance = mData["name"];
                            var description = mData["description"];
                            editClick(id, insurance, description);
                        }
                    });
                    // for delete click event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = insuranceTypeTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/insurance_type.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [{
                    "mData": "name"
                }, {
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            "  style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            "  data-original-title='Delete'></i>";
                    }
                }, ],
                aoColumnDefs: [{
                    aTargets: [2],
                    bSortable: false
                }, {
                    aTargets: [1],
                    bSortable: false
                }]
            });
        }
    });

    // for reset functionality
    $("#btnReset").click(function() {
        $("#txtName").focus();
        $("#txtName").removeClass("errorStyle");
        clear();
    });

}); //close document.ready

// use fucntion for edit insurance for update 
function editClick(id, insurance, description) {
    $('#myModalLabel').text("Update Insurance Type");
    $('.modal-body').html($("#popUPForm").html()).show();
    $('#myModal').modal('show');
    $('#myModal').on('shown.bs.modal', function() {
        $("#myModal #txtName").focus();
    });
    $("#myModal #btnUpdateInsurance").removeAttr("style");
    $("#myModal #txtNameError").text("");
    $("#myModal #txtName").removeClass("errorStyle");
    $('#myModal #txtName').val(insurance);
    $('#myModal #txtDescription').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);

    $("#myModal #btnUpdateInsurance").click(function() {
        //$("#myModal").hide();
        var flag = "false";

        $("#myModal #txtName").val($("#myModal #txtName").val().trim());

        if ($("#myModal #txtName").val() == '') {
            $('#myModal #txtName').focus();
            $("#myModal #txtNameError").text("Please enter insurance type");
            $("#myModal #txtName").addClass("errorStyle");
            flag = "true";
        }
        if ($("#myModal #txtName").val().length > 50) {
            $('#myModal #txtName').focus();
            $("#myModal #txtNameError").text("Please enter type less than 50");
            $("#myModal #txtName").addClass("errorStyle");
            flag = "true";
        }
        if (flag == "true") {
            return false;
        }
        else{
            var insuranceName = $("#myModal #txtName").val();
            var description = $(" #myModal #txtDescription").val();
            description = description.replace(/'/g, "&#39");
            var id = $('#selectedRow').val();
            
            $('#confirmUpdateModalLabel').text();
            $('#updateBody').text("Are you sure that you want to update this?");
            $('#confirmUpdateModal').modal();
            $("#btnConfirm").unbind();
            $("#btnConfirm").click(function(){
                $.ajax({
                    type: "POST",
                    cache: "false",
                    url: "controllers/admin/insurance_type.php",
                    data: {
                        "operation": "update",
                        "id": id,
                        "insuranceName": insuranceName,
                        "description": description
                    },
                    success: function(data) {
                        if (data != "0" && data != "") {
                            $('#myModal').modal("hide");
                            $('.close-confirm').click();
                            $('.modal-body').text('');
                            $('#messageMyModalLabel').text("Success");
                            $('.modal-body').text("Insurance type updated successfully!!!");
                            $('#messagemyModal').modal();
                            insuranceTypeTable.fnReloadAjax();
                            clear();
                        } else {
                            $('.close-confirm').click();
                            $("#myModal #txtNameError").text("Insurance type is already exists");
                            $("#myModal #txtName").addClass("errorStyle");
                            $('#myModal #txtName').focus();

                        }
                    },
                    error: function() {
                        //$('#myModal').modal('hide');
                        $('.close-confirm').click();
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Error");
                        $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                        $('#messagemyModal').modal();
                    }
                });
            });
        }
    });

    $('#myModal #txtName').keyup(function() {
        if ($("#myModal #txtName").val() != '') {
            $("#myModal #txtNameError").text("");
            $("#myModal #txtName").removeClass("errorStyle");
        }
    });
}
// click function to delete insurance type
function deleteClick(id) {

    $('#selectedRow').val(id);
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Insurance Type");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();

    var type = "delete";
    $('#confirm').attr('onclick', 'deleteInsurance("' + type + '");');
}

//Click function for restore the insurance type
function restoreClick(id) {
    $('#selectedRow').val(id);
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Restore Insurance Type");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();

    var type = "restore";
    $('#confirm').attr('onclick', 'deleteInsurance("' + type + '");');
}

//function for delete and restore the insurance type list
function deleteInsurance(type) {
    if (type == "delete") {
        var id = $('#selectedRow').val();

        //Ajax call for delete the insurance type  
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/insurance_type.php",
            data: {
                "operation": "delete",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Insurance Type deleted successfully!!!");
                    $('#messagemyModal').modal();
                    insuranceTypeTable.fnReloadAjax();
                } else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This insurance is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    } else {
        var id = $('#selectedRow').val();

        //Ajax call for restore the insurance type 
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/insurance_type.php",
            data: {
                "operation": "restore",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Insurance Type restored successfully!!!");
                    insuranceTypeTable.fnReloadAjax();
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}
// clear function
function clear() {
    $('#txtName').val("");
    $('#txtNameError').text("");
    $('#txtDescription').val("");
    $('#txtName').removeClass("errorStyle");
    $("#txtName").removeClass("errorStyle");
    $("#myModal #txtName").removeClass("errorStyle");
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        $(".close").click();
    }
});