var assetSubClassTable;
$(document).ready(function(){
    loader();
    debugger;
	bindAssetClass();
	/*Hide add ward by default functionality*/
	$("#advanced-wizard").hide();
    $("#ledgerList").addClass('list');    
    $("#tabledgerList").addClass('tab-list-add');
	

    $("#tabAddledger").click(function() {
         clear();
       showAddTab();
    });
	
	//Click function for show the ledger lists
    $("#tabledgerList").click(function() {
        clear();
        tabledgerList();
    });

    removeErrorMessage();

    $('#btnSubmit').click(function() {
        var flag = false;

        if($('#selClass').val() == '') {
            $('#selClassError').text('Please select class');
            $('#selClass').focus();
            $('#selClass').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtSubClass').val() == '') {
            $('#txtSubClassError').text('Please enter sub class');
            $('#txtSubClass').focus();
            $('#txtSubClass').addClass("errorStyle"); 
            flag = true;
        }
        if(flag == true) {
            return false;
        }

        var subClass = $('#txtSubClass').val();
        subClass =  subClass.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });
        var className = $('#selClass').val().trim();
        var description = $('#txtDescription').val().trim();

        var postData = {
            subClass : subClass,
            className : className,
            description : description,
            operation : "saveAssetSubClass"
        }


        $.ajax(
            {                   
            type: "POST",
            cache: false,
            url: "controllers/admin/asset_subclass.php",
            datatype:"json",
            data: postData,
            
            success: function(data) {
                if(data != "0" && data != ""){
                    callSuccessPopUp('Success','Saved successfully!!!');
                     tabledgerList();
                    clear();
                }
                else{
                    tabledgerList();
                    callSuccessPopUp("Sorry","This asset subclass is already exist please check subclass name or class name!!!");

                }
            },
            error: function(){

            }
        });
    });
    $('#btnReset').click(function(){
        clear();
    });


    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
    //Datatable code 
    assetSubClassTable = $('#assetSubClassTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = assetSubClassTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var subClassName = mData["sub_class_name"];  
                        var ClassNameId = mData["class"];                    
                        var description = mData["description"];
                        editClick(id,subClassName,ClassNameId,description);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = assetSubClassTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/asset_subclass.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "sub_class_name"
                }, {
                    "mData": "asset_class_name"
                }, {
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [2,3]
            }]

        });
    }

    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            assetSubClassTable.fnClearTable();
            assetSubClassTable.fnDestroy();
            assetSubClassTable = "";
            assetSubClassTable = $('#assetSubClassTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = assetSubClassTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var subClassName = mData["sub_class_name"]; 
                            var ClassNameId = mData["class"];
                            restoreClick(id,subClassName,ClassNameId);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/asset_subclass.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "sub_class_name"
                    }, {
                        "mData": "asset_class_name"
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [2,3]
                }]
            });
        } else { // show active data on unchecked   
            assetSubClassTable.fnClearTable();
            assetSubClassTable.fnDestroy();
            assetSubClassTable = "";
            assetSubClassTable = $('#assetSubClassTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = assetSubClassTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var subClassName = mData["sub_class_name"];  
                            var ClassNameId = mData["class"];                    
                            var description = mData["description"];
                            editClick(id,subClassName,ClassNameId,description);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = assetSubClassTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/asset_subclass.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "sub_class_name"
                    }, {
                        "mData": "asset_class_name"
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [2,3]
                }]
            });
        }
    });
});

function editClick(id,subClassName,ClassNameId,description) {
    
   
     showAddTab()  
    $("#btnReset").hide();
    $("#btnSubmit").hide();
    $('#btnUpdate').show();
    $('#tabAddledger').html("+Update Asset Sub Class");

    $('#txtSubClass').focus();
   
    
    $("#btnUpdate").removeAttr("style");
    
    $("#txtSubClass").removeClass("errorStyle");
    $("#txtSubClassError").text("");
    $("#selClass").removeClass("errorStyle");
    $("#selClassError").text("");

   
    $('#txtSubClass').val(subClassName);
    $('#selClass').val(ClassNameId);
    $('#txtDescription').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
    //validation
    //remove validation style
    
    removeErrorMessage();
    
    $("#btnUpdate").click(function() { // click update button
        var flag = false;
        if($('#selClass').val() == '') {
            $('#selClassError').text('Please select class');
            $('#selClass').focus();
            $('#selClass').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtSubClass').val() == '') {
            $('#txtSubClassError').text('Please enter sub class');
            $('#txtSubClass').focus();
            $('#txtSubClass').addClass("errorStyle"); 
            flag = true;
        }
        
        if(flag == true) {
            return false;
        }

        var subClass = $('#txtSubClass').val();
        var className = $('#selClass').val().trim();
        var description = $("#txtDescription").val().trim();
        description = description.replace(/'/g, "&#39");
        var id = $('#selectedRow').val();
        
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
        var postData = {
            "operation": "update",
            "subClass": subClass,
            "className": className,
            "description": description,
            "id": id
        }
        $.ajax( //ajax call for update data
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/asset_subclass.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('#myModal').modal('hide');
                        $('.close-confirm').click();
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Asset sub class updated successfully!!!");
                        $('#messagemyModal').modal();
                        assetSubClassTable.fnReloadAjax();
                        tabledgerList();
                        clear();
                    }
                    else{
                        callSuccessPopUp("Sorry","This asset subclass is already exist please check subclass name or class name!!!");
                    }
                },
                error: function() {
                    $('.close-confirm').click();
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                    $('#messagemyModal').modal();
                }
            }); // end of ajax
        });
    });
} // end update button

function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Asset Sub Class");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteAssetSubClass("' + type + '","'+id+'","","");');
} // end click fucntion

function restoreClick(id,subClassName,ClassNameId) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Asset Sub Class");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteAssetSubClass("' + type + '","'+id+'","'+subClassName+'","'+ClassNameId+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteAssetSubClass(type,id,subClassName,ClassNameId) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/asset_subclass.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Asset sub class deleted successfully!!!");
                    $('#messagemyModal').modal();
                    assetSubClassTable.fnReloadAjax();
                } 
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/asset_subclass.php",
            data: {
                "operation": "restore",
                "subClassName":subClassName,
                "ClassNameId":ClassNameId,
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Asset sub class restored successfully!!!");
                    $('#messagemyModal').modal();
                    assetSubClassTable.fnReloadAjax();
                }
                else{
                    callSuccessPopUp("Sorry","This Sub class is already exist so you can't restore it");
                }
            },
            error: function() {             
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary unavailable to respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}

function bindAssetClass() {     // function for loading account number of asset type
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/asset_subclass.php",
        data: {
            "operation": "showAssetClass"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                $.each(parseData,function(i,v){
                    option += "<option value='" +v.id+ "'>" + v.asset_class_name + "</option>";
                });
                
                $('#selClass').html(option);
            }
        },
        error: function() {}
    });
}
function tabledgerList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
   
    $("#tabAddledger").removeClass('tab-detail-add');
    $("#tabAddledger").addClass('tab-detail-remove');
    $("#tabledgerList").removeClass('tab-list-remove');    
    $("#tabledgerList").addClass('tab-list-add');
    $("#ledgerList").addClass('list');
   
    $("#btnReset").show();
    $("#btnSubmit").show();
    $('#btnUpdate').hide();
    $('#tabAddledger').html("+Add Asset Sub Class");
    $('#inactive-checkbox-tick').prop('checked', false).change();
    clear();
}
function showAddTab(){
    $("#advanced-wizard").show();
    $(".blackborder").hide();

    $("#tabAddledger").addClass('tab-detail-add');
    $("#tabAddledger").removeClass('tab-detail-remove');
    $("#tabledgerList").removeClass('tab-list-add');
    $("#tabledgerList").addClass('tab-list-remove');
    $("#ledgerList").addClass('list');
    $("#txtName").focus();
}

function clear() {
    removeErrorMessage();
    clearFormDetails(".clearForm"); 
    $('#txtSubClass').focus();
}