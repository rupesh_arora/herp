var departmentBudgetTable;
$(document).ready(function(){
    loader();
    debugger;
    $("#selDepartment").focus();
    departmentBudgetTable = $('#departmentBudgetTable').dataTable({
        "sPaginationType": "full_numbers",});
    bindDepartment();

    $("#selDepartment").on('change',function(){
        var departmentId = $(this).val();
        if (departmentId != '') {
            loadDataTable(departmentId);
        }
    });
    removeErrorMessage();
    $("#btnSave").on('click',function(){

        if($("#selDepartment").val() == ""){
            $("#selDepartment").focus();
            $("#selDepartmentError").text("Please select department");
            $("#selDepartment").addClass("errorStyle");
            return false;
        }

        var arr= [];
        $.grep(departmentBudgetTable.fnGetNodes(), function( n, i ){
            if($(n).find('td:eq(2)').text() != ""){
                var obj = {};
                obj.accountId = $(n).find('td:eq(1) span').attr('account-id');
                obj.amount = $(n).find('td:eq(2)').text();
                obj.remarks = $(n).find('td:eq(3)').text();
                obj.budgetMatrixId = $(n).find('td:eq(1) span').attr('budget-matrix-id');
                arr.push(obj);
            }
        });
        if (arr.length == 0) {
            callSuccessPopUp("Alert","Please insert new data in table");
            flag = true;
            return false;
        }

        var departmentId = $("#selDepartment").val();
        var postData = {
            operation : "saveAmount",
            departmentId : departmentId,
            array : JSON.stringify(arr)
        }

        dataCall("controllers/admin/department_budget_adjustment.php", postData, function (result){
            if (result == 1) {
                callSuccessPopUp("Success","Amount saved successfully.");
                clearFormDetails("#deptBudgetAdjustment");
                departmentBudgetTable.fnClearTable();
            }
        });
    });

    $("#btnReset").click(function(){
        clearFormDetails("#deptBudgetAdjustment");
        $("#selDepartment").focus();
        departmentBudgetTable.fnClearTable();
    });
   	
});
function bindDepartment() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/opd_registration.php",
        data: {
            "operation": "showDepartment"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value=''>-- Select --</option>";
                $.each(parseData,function(i,v){
                    option += "<option value='" + v.id + "'>" + v.department + "</option>";
                });
                    
                $('#selDepartment').html(option);
            }
        },
        error: function() {}
    });
}

function loadDataTable(departmentId){
    departmentBudgetTable.fnClearTable();
    departmentBudgetTable.fnDestroy();
    departmentBudgetTable = $('#departmentBudgetTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "order": [[ 2, 'asc' ]],
        "aaSorting": [],
        aLengthMenu: ["All"],//declare so that show all rows
        iDisplayLength: -1,
    });
    var postData = {
        operation : "show",
        departmentId : departmentId
    }

    dataCall("controllers/admin/department_budget_adjustment.php", postData, function (result){
        if (result.length > 2) {
            var parseData = JSON.parse(result);
            $.grep(parseData,function(v,i){
                var accountInfo = '<span account-id ='+v.account_id+' budget-matrix-id='+v.id+'>'+v.account_name+'</span>';
                var currRow = departmentBudgetTable.fnAddData([v.account_no,accountInfo,'','',v.id]);
                var getCurrRow = departmentBudgetTable.fnSettings().aoData[ currRow ].nTr;
                $(getCurrRow).find('td:eq(2)').addClass('acceptNumber');
            });

            $('#departmentBudgetTable td:nth-child(3),#departmentBudgetTable td:nth-child(4)').unbind();
            $('#departmentBudgetTable td:nth-child(3),#departmentBudgetTable td:nth-child(4)').on('dblclick', function() {
                var previousData = $(this).text();
                var parentObj = $(this);
                $(this).addClass("cellEditing");                  
                $(this).html("<input type='text'  value='" + previousData + "'/>");
                $(this).children().first().keypress(function (e) { 
                    if (e.which == 13) {
                        var newContent = $(this).val();
                        if (newContent !='') {
                            $(this).parent().removeClass("cellEditing");    
                            $(this).parent().text(newContent);
                        }
                        if ($(this).closest('td').hasClass('acceptNumber')) {
                            var floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
                            var value = floatRegex.test(newContent);
                            if(value == false){
                                callSuccessPopUp('Alert',"Enter number only");
                                return false;
                            }   
                        }                                    
                    }
                    // for disable alphabates keys
                    if ($(this).closest('td').hasClass('acceptNumber')) {
                        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { 
                            return false;
                        }
                    }
                });
                $(this).children().first().blur(function (e) {
                    var newContent = $(this).val();
                    if ($(this).closest('td').hasClass('acceptNumber')) {
                        var floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
                        var value = floatRegex.test(newContent);
                        if(value == false){
                            callSuccessPopUp('Alert',"Enter number only");
                            return false;
                        }
                    }
                    if (newContent !='') {
                        $(this).parent().removeClass("cellEditing");    
                        $(this).parent().text(newContent);
                    }
                });
                /*Code to focus on last charcter*/
                var SearchInput = $(this).children();
                SearchInput.val(SearchInput.val());
                var strLength= SearchInput.val().length;
                SearchInput.focus();
            }); 
        }
        else{
            callSuccessPopUp('Alert',"No data exist for choosen department.");
        }
    });
}