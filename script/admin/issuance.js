/*
 * File Name    :   stocking.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   16th feb, 2016
 * Description  :   This page use for load and send data to back hand.
 */
var tableIssuance; // define otable variable for datatable
var totalCost = 0;
$(document).ready(function() {
	debugger;
loader();   
	$(".amountPrefix").text(amountPrefix);
	// inilize data table
	tableIssuance = $('#tblIssuance').dataTable({
		"sPaginationType": "full_numbers",
		"bAutoWidth" : false,
		"fnDrawCallback": function(oSettings) {},
		aoColumnDefs: [{
			aTargets: [5],
			bSortable: false
		}]
	});
	$("#txtReceiver").focus();
	//Autocomplete functionality  for receiver
	$("#txtReceiver").on('keyup', function(e) {  
		if(e.keyCode !=13){
			$("#txtReceiver").removeAttr('receiver-id');
		}
		var currentObj = $(this);
		$("#txtReceiver").autocomplete({
			source: function(request, response) {
				var postData = {
					"operation":"showReceiver",
					"receiverName": currentObj.val()
				}
				
				$.ajax({     
					type: "POST",
					cache: false,
					url: "controllers/admin/issuance.php",
					datatype:"json",
					data: postData,
				 
					success: function(dataSet) {
						if(dataSet.length > 2) {
							response($.map(JSON.parse(dataSet), function (item) {
								return {
									id: item.id,
									value: item.name
								}
							}));
						}
						else {
							$("#txtReceiverError").text("Receiver name is not exists.");
						}
						
					},
					error: function(){

					}
				});
			},
			select: function (e, i) {
				var receiverId = i.item.id;
				currentObj.attr("receiver-id", receiverId);
				$("#txtReceiver").val(i.item.value);    
			},
			minLength: 3
		});  
	});

	// click to load  item search pop updateCommands
	$("#iconStaffSearch").on('click',function(){
		$('#itemModalLabel').text("Search Items");
        $('#itemModalBody').load('views/admin/view_item.html', function() {
            $("#itemValue").val("2");
        });
        $('#myItemissuanceModal').modal();
		$("#txtItemError").text("");
		$("#txtItem").removeClass("errorStyle");
	});
				
	// call date picker
	$("#txtDateIssue").datepicker();
	
	// keyup functionalty for remove error messae
	    // for remove 	
    $("input[type=text]").keyup(function() {

        if ($(this).val() != "") {
            $(this).next("span").text("");
			$(this).next().next("span").text("");
            $(this).removeClass("errorStyle");
        }
		
    });
	$("input[type=number]").keyup(function() {

        if ($(this).val() != "") {
            $(this).next("span").text("");
			 $(this).next().next("span").text("");
            $(this).removeClass("errorStyle");
        }
		
    });
    $("select").on('change', function() {

        if ($(this).val() != "") {
            $(this).next("span").text("");
            $(this).removeClass("errorStyle");    
        }
		
    });
    $("#txtDateIssue").change(function() {
        if ($("#txtDateIssue").val() != "") {
            $("#txtDateIssueError").hide();
            $("#txtDateIssue").removeClass("errorStyle");
            $("#txtDateIssue").datepicker("hide");
        }
    });
	
	// add item data with validation
	$("#btnAddItem").on('click',function() {
		$("input[type=number]").next("span").text("");
		$("input[type=number]").removeClass("errorStyle");
		var flag = false;
		var flagCheck = false;
		var stockDate = $("#txtItem").attr('item-receive-date');
		//check validation
		if ($("#txtQuantity").val() == "" || $("#txtQuantity").val() < 0 ) {
	        $("#txtQuantityError").show();
            $("#txtQuantityError").text("Please enter quantity.");
            $("#txtQuantity").focus();
            $("#txtQuantity").addClass("errorStyle");
            flag = true;
        } 
		else if(parseInt($("#txtQuantity").val()) > parseInt($("#itemPrice").attr("item-quantity"))) {
			$("#txtQuantity").focus();
			$("#txtQuantityError").show();
            $("#txtQuantityError").text("Stock not available.");
            $("#txtQuantity").focus();
            $("#txtQuantity").addClass("errorStyle");
            flag = true;
			flagSave = true;
		} 
		if ($("#txtItem").val().trim() == "") {
			$("#txtItem").focus();
            $("#txtItemError").show();
            $("#txtItemError").text("Please select item.");
            $("#txtItem").focus();
            $("#txtItem").addClass("errorStyle");
            flag = true;
        }
        if ($("#itemID").val().trim() == "") {
			$("#txtItem").focus();
            $("#txtItemError").show();
            $("#txtItemError").text("Please select correct item.");
            $("#txtItem").focus();
            $("#txtItem").addClass("errorStyle");
            flag = true;
        }
		if (stockDate > $("#txtDateIssue").val()) {
			
            $("#txtDateIssueError").show();
            $("#txtDateIssueError").text("Please select correct issue date.");
            $("#txtDateIssue").addClass("errorStyle");
            flag = true;
        }
		if ($("#txtDateIssue").val() == "") {
			//$("#txtDateIssue").focus();
            $("#txtDateIssueError").show();
            $("#txtDateIssueError").text("Please select issue date.");
            $("#txtDateIssue").addClass("errorStyle");
            flag = true;
        }
		if ($("#txtReceiver").val() == "") {			
            $("#txtReceiverError").show();
            $("#txtReceiverError").text("Please select receiver");
            $("#txtReceiver").addClass("errorStyle");
			$("#txtReceiver").focus();
            flag = true;			
        }	
        if ($("#txtReceiver").attr('receiver-id') == undefined) {
			$('#txtReceiver').focus();
            $("#txtReceiverError").text("Please select valid receiver name that exist.");
            $('#txtReceiver').addClass("errorStyle");
            flag=true;
		}	
		if(flag == true) {
			return false;
		}
		
		// define variable to get value
        var item = $("#txtItem").val();
        var quantity = $("#txtQuantity").val();
        var receiver = $("#txtReceiver").attr("receiver-id");
        var issueDate = $("#txtDateIssue").val();
        var expiryDate = $("#txtExpiryDate").text();
        var itemCost = $("#itemCost").val();
        var itemId = $("#itemID").val();
		debugger;
        var itemPrice = $("#itemPrice").val();
		var stockItemQuantity = parseInt($("#itemPrice").attr("item-quantity"));
		var stockQuantity = stockItemQuantity - parseInt(quantity);
		var itemQuantity = 0;
		var rowCount = tableIssuance.fnGetData(); 
		
		for (i=0;i<rowCount.length;i++) {
			
			var itemTableId = rowCount[i][7];			
			if((itemTableId == itemId)) {
				itemQuantity = itemQuantity + rowCount[i][9];
				if(itemQuantity == '0') {
					$("#txtQuantityError").show();
					$("#txtQuantityError").text("Stock not available.");
					$("#txtQuantity").focus();
					$("#txtQuantity").addClass("errorStyle");
					flagCheck = true;
				}
				else if(itemQuantity <= stockItemQuantity) {
					var stoItemQuantity = stockItemQuantity - itemQuantity;
					stockQuantity = stoItemQuantity - itemQuantity;
					if(quantity >= stoItemQuantity) {
						$("#txtQuantityError").show();
						$("#txtQuantityError").text("Stock not available.");
						$("#txtQuantity").focus();
						$("#txtQuantity").addClass("errorStyle");
						flagCheck = true;
					}
				}				
			}
		}
		
		if(flagCheck == true) {
			return false;
		}
		
		var cost = itemPrice * quantity;
		totalCost = totalCost + cost;
		$("#totalCost").text(totalCost);
		//Add data to data table
		tableIssuance.fnAddData([item,quantity,expiryDate,itemCost,cost,'<input type="button" class="remove" onclick=deleteRow(this); value="Remove"/>',receiver,itemId,issueDate,stockQuantity]);
		
		$("#txtItem").val("");
		$("#txtQuantity").val("");
		$("#itemPrice").val("");
		$("#txtExpiryDate").text("N/A");
		$("#itemCost").val("");
		$("#itemID").val("");
		/* $(".remove").on('click',function(){
			
		}); */
		
	});
	
	
	// save table data 
	$("#btnSave").on('click',function() {
		// select total row
		var flag =  false;
		if(tableIssuance.fnGetData().length == 0) {
			$('.modal-body').text("");
			$('#messageMyModalLabel').text("Alert");
			$('.modal-body').text("Please filled the all field !!!");
			$('#messagemyModal').modal();
			flag =  true;
		}
		if(flag == true){
			return false;
		}
		var rowCount = tableIssuance.fnGetData(); 
		var itemData = [];
		
		for (i=0;i<rowCount.length;i++) {
			var ObjIssuance = {};
			ObjIssuance.itemId = rowCount[i][7];
			ObjIssuance.quantity = rowCount[i][1];
			ObjIssuance.cost = rowCount[i][3];
			ObjIssuance.issueDate = rowCount[i][8];
			ObjIssuance.expiryDate = rowCount[i][2];
			ObjIssuance.receiverId = rowCount[i][6];
			ObjIssuance.itemPrice = rowCount[i][4];
			ObjIssuance.stockQuantity = rowCount[i][9];
			itemData.push(ObjIssuance);
		}
		
				
		$.ajax({ // ajax call for save content
			type: "POST",
			cache: "false",
			url: "controllers/admin/issuance.php",
			datatype: "json",
			data: {
				"operation": "save",
				"issuanceData":JSON.stringify(itemData),
			},
			success: function(data) {
				if (data == "1") {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Issuance is saved successfully!!!");
					$('#messagemyModal').modal();
					clearDetails();
				}
			},
			error: function() {
				$('.close-confirm').click();					
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
				clearDetails();
			}
		});
	});
	
	// reset functionality	
	$("#btnReset").on('click',function(){ 
		clearDetails();
	});

});

// clear function
function clearDetails() {
	 $("input[type=text]").val("");
	 $("input[type=number]").val("");
	 $("input[type=text]").next("span").text("");
	 $("input[type=text]").next().next("span").text("");
	 $("input[type=text]").removeClass("errorStyle");
	 $("input[type=number]").next("span").text("");
	 $("input[type=number]").removeClass("errorStyle");
	 $("#txtReceiver").focus();
	 $("#txtReceiver").val("");
	 $("#txtReceiver").removeClass("errorStyle");  
	 $("#txtReceiver").next("span").text("");	 
	 $("#categoryID").val("");
	 $("#txtExpiryDate").text("N/A");
   	 $("#itemCost").val("");
	 $("#itemID").val("");
	 tableIssuance.fnClearTable();
	 $("#totalCost").text("0");
	 totalCost = 0;
}

function clearValues() {
	$("#txtItem").val("");
	$("#txtQuantity").val("");
	$("#itemPrice").val("");
	$("#txtReceiver").val("");
	$("#txtDateIssue").val("");
	$("#txtExpiryDate").text("N/A");
	$("#itemCost").val("");
	$("#itemID").val("");
}

function deleteRow(that){
	var row = $(that).closest("tr").get(0);
	tableIssuance.fnDeleteRow(row);
	var totCost = 0; 
	var rowCount = tableIssuance.fnGetData(); 
	for (i=0;i<rowCount.length;i++) {
		totCost = totCost + rowCount[i][4];				
	}
	$("#totalCost").text(totCost);
	totalCost = totCost;
}