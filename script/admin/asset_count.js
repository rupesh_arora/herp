var assetCheckInTable;
var assetCountTable;
$(document).ready(function(){
    var table = true;
    loader();
    debugger;

    bindRoom();	// bind all rooms
    bindLocation();	// bind all location
    bindBarcode();
    bindPeriod();
    bindAssetClass(); 
    loadAssetData();

    removeErrorMessage();
	
	/*Hide add ward by default functionality*/
	$("#advanced-wizard").hide();
	$("#ledgerList").addClass('list');
	$("#tabledgerList").addClass('tab-list-add');

    $("#selAssetClass").change(function(){
        var htmlRoom ="<option value='-1'>Select</option>";
        
        if ($('#selAssetClass :selected').val() != -1) {
            var value = $('#selAssetClass :selected').val();
            bindAssetSubClass(value);
        }
        else {
            $('#selAssetSubClass').html(htmlRoom);
        }
    });

	assetCheckInTable = $('#assetCheckInTable').dataTable();
    
	assetCountTable = $('#tblAssetCount').dataTable({
		 "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "autoWidth": false,
        "fnDrawCallback": function(oSettings) {
        	
        },
	});
	

	/*Click for add the ledger*/
    $("#tabAddledger").click(function() {
        $("#advanced-wizard").show();
        $(".blackborder").hide();
		clear();
        $("#tabAddledger").addClass('tab-detail-add');
        $("#tabAddledger").removeClass('tab-detail-remove');
        $("#tabledgerList").removeClass('tab-list-add');
        $("#tabledgerList").addClass('tab-list-remove');
        $("#ledgerList").addClass('list');
        $('#txtName').focus();
        $('#txtPeriod').focus();
        $('#btnSubmit').show();
		$('#btnReset').show();
		$('#btnUpdate').hide();
    });
	
	//Click function for show the ledger lists
    $("#tabledgerList").click(function() {
        clear();
        tabledgerList();
    });
   

    $('#btnCount').click(function(){
    	
    	

    	var period = $("#selPeriod").val();
    	var room = $('#selRoom').val();
    	var location = $('#selLocation').val();
    	var barcode = $('#selBarcode').val();

    	var postData = {
    		operation : "showCount",
    		period : period,
    		room : room ,
    		location : location,
    		barcode : barcode
    	}
    	dataCall("controllers/admin/asset_count.php", postData, function (result){
    		if (result !='') {
                var parseData = jQuery.parseJSON(result);
                var a = parseData.join('-');
                var match = parseInt(a.match(/\d+/));
                var m = match +1;
                assetCountTable.fnClearTable();
                for(i=0; i<match; i++){
                    if(parseData[m].actual_room == undefined){
                       var actualRoom = parseData[m];
                    }
                    else{
                        var actualRoom = parseData[m].actual_room;
                    }
                    assetCountTable.fnAddData([parseData[i].asset_no,parseData[i].asset_barcode,parseData[i].asset_description,
                        parseData[i].asset_advance_description,parseData[i].expected_room,actualRoom]);
                    m++;
                }
    			
    		}
    	});
    });

    $('#btnReset').click(function(){
    	clear();
    }); 

    $("#btnApply").click(function(){

        loadAssetData();
    });

   
    $("#txtManufacturer").on('keyup', function(){   
        
        var postData = {
            "operation":"searchManufacturer",
            "name": $("#txtManufacturer").val()
        } 
        $("#txtBrandName").val('');
        $("#txtModal").val('');
        autoComplete("#txtManufacturer",postData);
    });

    $("#txtBrandName").on('keyup', function(){   
        if($("#txtManufacturer").attr('data-id') != ""){
            var postData = {
                "operation":"searchBrandName",
                "name": $("#txtBrandName").val(),
                "parentId" : $("#txtManufacturer").attr('data-id')
            } 
             $("#txtModal").val('');
            autoComplete("#txtBrandName",postData);
        }
        
    });

    $("#txtModal").on('keyup', function(){ 
        if($("#txtBrandName").attr('data-id') != ""){
            var postData = {
                "operation":"searchModal",
                "name":  $("#txtModal").val(),
                "parentId" : $("#txtBrandName").attr('data-id')
            }  
            
            autoComplete("#txtModal",postData);
        }
        
    });

   
    
});
function tabledgerList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
    $("#tabAddledger").html('+Asset Count');
    $("#tabAddledger").removeClass('tab-detail-add');
    $("#tabAddledger").addClass('tab-detail-remove');
    $("#tabledgerList").addClass('tab-list-add');
    $("#tabledgerList").removeClass('tab-list-remove');
    $("#ledgerList").addClass('list');
    clear();
}
function bindRoom() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/asset_count.php",
        data: {
            "operation": "showRoom"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].number + "</option>";
                }
                $('#selRoom').html(option);
            }
        },
       error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
    });
}
function bindPeriod() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/asset_count.php",
        data: {
            "operation": "showPeriod"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].period + "</option>";
                }
                $('#selPeriod').html(option);
            }
        },
       error:function() {
            $('#messagemyModal').modal();
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
        }
    });
}
function bindBarcode() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/asset_count.php",
        data: {
            "operation": "showBarcode"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].asset_barcode + "</option>";
                }
                $('#selBarcode').html(option);
            }
        },
       error:function() {
            $('#messagemyModal').modal();
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
        }
    });
}
function bindLocation() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/asset_count.php",
        data: {
            "operation": "showLocation"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].location + "</option>";
                }
                $('#selLocation').html(option);
                 $('#txtSetLocation').html(option);
                $(' #selAssetLocation').html(option);
            }
        },
       error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
    });
}

function bindAssetClass() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/asset_check_out.php",
        data: {
            "operation": "showAssetClass"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].asset_class_name + "</option>";
                }
               $('#selAssetClass').html(option);
            }
        },
       error:function() {
            $('#messagemyModal').modal();
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
        }
    });
}
function bindAssetSubClass(value) { 
    $.ajax({                    
        type: "POST",
        cache: false,
        url: "controllers/admin/asset_register.php",
        data: {
            "operation":"showAssetsubClass",
            "subClass" : value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].sub_class_name + "</option>";
                }
               $('#selAssetSubClass').html(option);
            }
        },
       error:function() {
            $('#messagemyModal').modal();
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
        }
    });
}

function autoComplete(id,postData){
    //Autocomplete functionality 
   /* $(id).val("");*/
    var currentObj = $(id);
    jQuery(id).autocomplete({
        source: function(request, response) {      
            
            $.ajax(
                {                   
                type: "POST",
                cache: false,
                url: "controllers/admin/asset_register.php",
                datatype:"json",
                data: postData,
                
                success: function(dataSet) {
                    loader();
                    response($.map(JSON.parse(dataSet), function (item) {
                        return {
                            id: item.id,
                            value: item.name
                        }
                    }));
                },
                error: function(){
                    
                }
            });
        },
        select: function (e, i) {
            var id = i.item.id;
            currentObj.attr("data-id", id);                
            
        },
        minLength: 2
    });
}

function validation(){
	var flag = false;
   if ($("#txtSetLocation").val() == "-1") {
        $('#txtSetLocation').focus();
        $("#txtSetLocationError").text("Please select location");
        $("#txtSetLocation").addClass("errorStyle");
        flag = true;
    }
    if ($("#txtSetRoom").val() == "-1") {
        $('#txtSetRoom').focus();
        $("#txtSetRoomError").text("Please select room");
        $("#txtSetRoom").addClass("errorStyle");
        flag = true;
    }
    if ($("#txtStaffName").val() == "") {
        $('#txtStaffName').focus();
        $("#txtStaffNameError").text("Please enter staff name");
        $("#txtStaffName").addClass("errorStyle");
        flag = true;
    }
    if ($("#txtStaffId").val() == "") {
        $('#txtStaffId').focus();
        $("#txtStaffIdError").text("Please select staff Id");
        $("#txtStaffId").addClass("errorStyle");
        flag = true;
    }
    return flag;
}
function clear(){
	$('input[type=text]').val("");
    $('input[type=text]').next().text(""); 
    $('input[type=text]').removeClass("errorStyle");
    $('select').val("-1");
    $('select').next().text("");
    $('select').removeClass("errorStyle");
    $('textarea').val("");
    $('textarea').next().text(""); 
    $('textarea').removeClass("errorStyle");
    $('#txtStaffId').focus();
    assetCountTable.fnClearTable();
}

function LoadDataTable(){
    var postData = {
        "operation" : "showTableData"
    }

    dataCall("controllers/admin/asset_check_in.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (parseData != '') {           
            assetCheckInTable.fnClearTable();
            $.each(parseData,function(i,v){
                staffId = v.staff_id;
                var prefix = v.staff_prefix;
                var idLength = 6-staffId.length;
                for(j=0; j<idLength; j++){
                    staffId="0"+staffId;
                }
                staffId=prefix+staffId;
                    
                assetCheckInTable.fnAddData([staffId,v.staff_name,v.number,v.location,""]);

                    
            }); 
        }
    });
}

function loadAssetData(){
    var manufacturerId = $("#txtManufacturer").attr('data-id');            
    var brandNameId = $("#txtBrandName").attr('data-id');            
    var modalId = $("#txtModal").attr('data-id');  

    var assetNo = $("#txtAssetNo").val();
    var assetDescription = $("#txtAssetDescription").val();
    var assetAdvanceDescription = $("#txtAssetAdvanceDescription").val();
    var barcode = $("#txtAssetBarcode").val();
    var description = $("#txtAssetDescription").val();
    var advanceDescription = $("#txtAssetAdvanceDescription").val();
    var assetClass = $("#selAssetClass").val();
    var assetSubClass = $("#selAssetSubClass").val();
    var assetLocation = $("#selAssetLocation").val();
    
    var postData = {
        "operation" : "search",
        "assetAdvanceDescription" : assetAdvanceDescription,
        "assetDescription" : assetDescription,
        "manufacturerId" : manufacturerId,
        "brandNameId" : brandNameId,
        "modalId" : modalId,
        "assetNo" : assetNo,
        "barcode" : barcode,
        "description" : description,
        "advanceDescription" : advanceDescription,
        "assetClass" : assetClass,
        "assetSubClass" : assetSubClass,
        "assetLocation" : assetLocation
    }

    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/asset_check_out.php",
        datatype:"json",
        data: postData,
        
        success: function(data) {
            assetCheckInTable.fnClearTable();
            assetCheckInTable.fnDestroy();
            assetCheckInTable = $('#assetCheckInTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "autoWidth": false,
                "fnDrawCallback": function(oSettings) {
                   
                                
                    
                },
            });
            if(data != "0" && data != "" ){
                var parseData = jQuery.parseJSON(data);
                for(i=0; i<parseData.length;i++){
                    var assetNo = parseData[i].asset_no;
                    var barcode = parseData[i].asset_barcode;
                    var location = parseData[i].location;
                    var manufacturer = parseData[i].manufacturer_name;
                    var brand = parseData[i].brand_name;
                    var modal = parseData[i].modal;

                    assetCheckInTable.fnAddData([assetNo,barcode,location,manufacturer,brand,modal]);
                }
               
                 
            }
        },
        error: function(){

        }
    });
}

