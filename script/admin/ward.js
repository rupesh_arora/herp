var otable;//define otable globaly
$(document).ready(function(){
	/* ****************************************************************************************************
 * File Name    :   ward.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Arora
 * Created Date :   30th dec, 2015
 * Description  :   This page add and mange ward
 *************************************************************************************************** */
loader();
debugger;
	bindDepartment();
	removeErrorMessage();
	
	/*Hide add ward by default functionality*/
	$("#advanced-wizard").hide();
    $("#wardList").addClass('list');
	$("#tabWardList").addClass('tab-list-add');
		
	/*Click for add the ward*/
    $("#tabAddWard").click(function() {
        $("#advanced-wizard").show();
        $(".blackborder").hide();
        $('#btnSubmit').show();
		$('#btnReset').show();
		$('#btnUpdateWard').hide();
		clear();
        $("#tabAddWard").addClass('tab-detail-add');
        $("#tabAddWard").removeClass('tab-detail-remove');
        $("#tabWardList").removeClass('tab-list-add');
        $("#tabWardList").addClass('tab-list-remove');
        $("#wardList").addClass('list');
		$('#selDepartment').focus();
		$('#selDepartment').val('-1');
    });
	
	//Click function for show the ward lists
    $("#tabWardList").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
		// Call the function for show the ward lists
        tabWardList();
    });
	
	//Ajax call for loading the data table for active ward 
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		loadDataTable();
    }
	/* Ajax call for loading the data table for inactive ward*/
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	otable.fnClearTable();
	    	otable.fnDestroy();
	    	//Datatable code
			otable = $('#wardTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					/*On click of restore icon call this function*/
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  otable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/ward.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},

				/*defining datatables column*/
				"aoColumns": [
					{ "mData": "name" },
					{ "mData": "ward_type" },
					{ "mData": "departmentName" },
					{ "mData": "description" },
					{  
						"mData": function (o) { 
						/*Return values that we get on click of restore*/
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},
				],
				/*False sort for following columns*/
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 3 ] },
				   { 'bSortable': false, 'aTargets': [ 4 ] }
				]
			} );
		}
		else{
			otable.fnClearTable();//clear the datatable
	    	otable.fnDestroy();//destroy datatable
	    	//Datatable code
			loadDataTable();
		}
    });
	
	//Click function for add the ward to database
	$("#btnSubmit").click(function(){
		/*perform validation*/
		var flag="false";
		$("#txtWardName").val($("#txtWardName").val().trim());
		if($("#txtWardName").val()==""){
			$('#txtWardName').focus();
			$("#txtWardNameError").text("Please enter ward name");
			$("#txtWardName").addClass("errorStyle");   
			flag="true";
		}
		
		if($("#selWardType").val()=="-1"){
			$('#selWardType').focus();
			$("#selWardTypeError").text("Please select ward type");
			$("#selWardType").addClass("errorStyle");    
			flag="true";
		}
		
		if ($("#selDepartment").val() == "-1") {
			$('#selDepartment').focus();
            $("#selDepartmentError").text("Please select department");
			$("#selDepartment").addClass("errorStyle");      
            flag = "true";
        }
		if(flag =="true"){
		return false;
		}
		
		var wardName = $("#txtWardName").val();
		var wardType = $("#selWardType").val();
		var description = $("#txtDescription").val();
		var department = $("#selDepartment").val();
		description = description.replace(/'/g, "&#39");// so that we can handle ' (commas)
		var postData = {
			"operation":"save",
			"wardName":wardName,
			"wardType":wardType,
			"department":department,
			"description":description
		}
		
		// Ajax call for save ward
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/ward.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Ward saved successfully!!!");
					$('#messagemyModal').modal();
					$('#inactive-checkbox-tick').prop('checked', false).change();
					tabWardList();//call function to update list
				}
				else{
					$("#txtWardNameError").text("Ward name with this type is already exists");
					$('#txtWardName').focus();
					$("#txtWardName").addClass("errorStyle"); 
				}
				
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	});
	
});

/*Define Click function for edit ward */
function editClick(id,name,ward_type,department,description){	
	$('#tabAddWard').html("+Update Ward");
	$('#tabAddWard').click();
	$('#selDepartment').focus();
	$("#btnUpdateWard").removeAttr("style");
	$('#btnSubmit').hide();
	$('#btnReset').hide();
	$("#txtWardName").removeClass("errorStyle");
	$("#selWardType").removeClass("errorStyle");
	$("#selDepartment").removeClass("errorStyle");
	
	$('#txtWardName').val(name);
	$('#selWardType').val(ward_type);
	$('#selDepartment').val(department);
	$('#txtDescription').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id);
	
	removeErrorMessage();
	
	//Click function for update ward 
	$("#btnUpdateWard").click(function(){
		/*perform validation*/
		var flag = validation();
		
		if(flag == "true"){			
			return false;
		}
		
		var wardName = $("#txtWardName").val();
		var wardType = $("#selWardType").val();
		var description = $("#txtDescription").val();
		var department = $("#selDepartment").val();
		description = description.replace(/'/g, "&#39");//handing commas ,enter etc.
		var id = $('#selectedRow').val();			
		
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to update this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			// Ajax call for update ward
			$.ajax({
				type: "POST",
				cache: "false",
				url: "controllers/admin/ward.php",
				data :{            
					"operation" : "update",
					"id" : id,
					"wardName":wardName,
					"wardType":wardType,
					"department":department,
					"description":description
				},
				success: function(data) {	
					if(data != "0" && data != ""){
						$('#myModal').modal('hide');
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Ward updated successfully!!!");
						$('#messagemyModal').modal();
						otable.fnReloadAjax();
						clear();
						$('#tabWardList').click();
					}
					if(data == "0"){
						$('.close-confirm').click();
						$("#myModal #txtWardNameError").text("Ward name already exists");
						$('#myModal #txtWardName').focus();
						$("#myModal #txtWardName").addClass("errorStyle");
						
					}
				},
				error:function(){
					$('.close-confirm').click();
					$('.modal-body').text("");
					$('#messagemyModal').modal();
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");				  
				}
			});
		});
				
	});		
}


/*Define Click function for delete the ward */
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete ward");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteWard("'+type+'");');//pass attribute in which sent opertion and function with it's type 
}

//Click function for restore the ward
function restoreClick(id){
	$('#selectedRow').val(id);
	$('.modal-body').text("");	
	$('#confirmMyModalLabel').text("Restore ward");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal(); 
	
	var type="restore";
	$('#confirm').attr('onclick','deleteWard("'+type+'");');//pass attribute in which sent opertion and function with it's type
}
//function for delete the wardList
function deleteWard(type){
	/*Checking which type of opertion sent*/
	if(type=="delete"){
		var id = $('#selectedRow').val();
			
		//Ajax call for delete the ward 	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/ward.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Ward deleted successfully!!!");
					$('#messagemyModal').modal();//showing modal
					otable.fnReloadAjax();//reload datatable
				}
				else{
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("This ward is used , so you can not delete!!!");
					$('#messagemyModal').modal();
				}
			},
			error: function()
			{
				alert('error');
				  
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
	
		//Ajax call for restore the ward 
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/ward.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){	
					$('.modal-body').text("");	
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Ward restored successfully!!!");			
					$('#messagemyModal').modal();//show modal
					otable.fnReloadAjax();//reload datatable
				}			
			},
			error:function(){
				$('.modal-body').text("");
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");				  
			}
		});
	}
}

//function for show the list of wards
function tabWardList(){
	$("#advanced-wizard").hide();
	$(".blackborder").show();
	$("#tabAddWard").removeClass('tab-detail-add');
	$("#tabAddWard").addClass('tab-detail-remove');
	$("#tabWardList").addClass('tab-list-add');
	$("#tabWardList").removeClass('tab-list-remove');
	$("#wardList").addClass('list');
	$("#txtWardName").removeClass("errorStyle");
	bindDepartment();
	clear();//clear all the style of previous screen
}

// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) {  
		 $(".close").click();
    }
});

/*reset button functionality*/
$("#btnReset").click(function(){
	$("#txtWardName").removeClass("errorStyle");
	$("#selWardType").removeClass("errorStyle");
	$("#selDepartment").removeClass("errorStyle");
	clear();
	$('#selDepartment').focus();
	bindDepartment();
});

function bindDepartment(){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/ward.php",
		data: {
			"operation":"showDepartment"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>--Select--</option>";
				for (var i=0;i<parseData.length;i++)
				{
				option+="<option value='"+parseData[i].id+"'>"+parseData[i].department+"</option>";
				}
				$('#selDepartment').html(option);				
				
			}
		},
		error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
	});
}

//function for clear the data
function clear(){
	$('#txtWardName').val("");
	$('#txtWardNameError').text("");
	$('#selWardType').val("-1");
	$('#selWardTypeError').text("");
	$('#txtDescription').val("");
	$('#selDepartment').val("");
	$('#selDepartmentError').text("");
}

function loadDataTable(){
otable = $('#wardTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {
			
			/*On click of update icon call this function*/
			$('.update').unbind();
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = otable.fnGetData(data);
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					var name = mData["name"];
					var ward_type = mData["ward_type"];
					var description = mData["description"];
					var department = mData["department"];
					editClick(id,name,ward_type,department,description);//pass parameter on call
   
				}
			});
			/*On click of delete icon call this function*/
			$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  otable.fnGetData(data);
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);
				}
			});
		},
		
		"sAjaxSource":"controllers/admin/ward.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},

		/*defining datatables column*/
		"aoColumns": [
			{ "mData": "name" },
			{ "mData": "ward_type" },
			{ "mData": "departmentName" },
			{ "mData": "description" },	
			{  
				"mData": function (o) { 
				/*Return values that we get on click of update and delete*/
				return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
				   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
				   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
				   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
				   "  data-original-title='Delete'></i>"; 
				}
			},	
		],
		/*False sort for following columns*/
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 4 ] },
		   { 'bSortable': false, 'aTargets': [ 3 ] }
		]
	} );
}
function validation(){
	var flag = "false";
		$("#txtWardName").val($("#txtWardName").val().trim());
		if ($("#txtWardName").val() == '') {
			$("#txtWardName").focus();
			$("#txtWardNameError").text("Please enter ward name");
			$("#txtWardName").addClass("errorStyle");
			flag = "true";
		}
				
		if ($("#selWardType").val() == '-1') {
			$("#selWardType").focus();
			$("#selWardTypeError").text("Please select ward type");
			$("#selWardType").addClass("errorStyle");
			flag = "true";
		}
		
		if ($("#selDepartment").val() == "-1") {
			$('#selDepartment').focus();
            $("#selDepartmentError").text("Please select department");
			$("#selDepartment").addClass("errorStyle");      
            flag = "true";
        }
		
		if ($("#txtWardNameError").text() != '') {
			flag = "true";
		}

		return flag;
}
//# sourceURL = ward.js