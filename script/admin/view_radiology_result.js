/* ****************************************************************************************************
 * File Name    :   seller.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   24th feb, 2016
 * Description  :   This page  manages seller name data
 *************************************************************************************************** */
$(document).ready(function() {
	loader();
	debugger;
	$('.modal-imagebox').hide();	
	
	$("#PrintRadiologyTest").addClass("hide");
	var redResultTable = $('#radiologyResultsTbl').dataTable({aoColumnDefs: [{
			'bSortable': false,
			'aTargets': [2]
		},{
			'bSortable': false,
			'aTargets': [3]
		},{
			'bSortable': false,
			'aTargets': [4]
		}]
	});

	$("#btnSearch").on("click",function(){

		var flag = "false";		
		var patientId = $("#txtPatientId").val().trim();
		var visitId =  $("#txtVisitid").val().trim();
		var patientName = $("#txtPatientName").val().trim();

		if(visitId != ""){
			if(visitId.length != 9){					
				flag = "true";
			}
			else{
				var visitPrefix = visitId.substring(0, 3);
				visitId = visitId.replace ( /[^\d.]/g, '' ); 
				visitId = parseInt(visitId);
			}
		}
		else{
			visitPrefix = "";
		}		

		if($("#txtPatientId").val() != ""){
			if(patientId.length != 9){					
				flag = "true";
			}
			else{
				var patientId = $("#txtPatientId").val();
				var patientPrefix = patientId.substring(0, 3);
				patientId = patientId.replace ( /[^\d.]/g, '' ); 
				patientId = parseInt(patientId);
			}
		}
		else{
			patientPrefix = "";
		}
		if (flag == "true") {
			return false;
		}
		
		redResultTable.fnClearTable();
		redResultTable.fnDestroy();
		redResultTable = $('#radiologyResultsTbl').dataTable( {
			"bFilter": true,
			"processing": true,
			"sPaginationType":"full_numbers",
			"sAjaxSource":"controllers/admin/view_radiology_result.php",
			"fnDrawCallback": function ( oSettings ) {
				$('.Print').unbind();
				$('.Print').click(function(){
					var data=$(this).parents('tr')[0];
					var mData = $('#radiologyResultsTbl').dataTable().fnGetData(data);
					if (null != mData)  // null if we clicked on title row
					{
						var visitId=mData["visit_id"];
						var visitPrefix = mData["visit_prefix"]
						var visitIdLength = visitId.length;
						for (var k=0;k<6-visitIdLength;k++) {
							visitId = "0"+visitId;
						}
						visitid = visitPrefix+visitId;

						var patientId=mData["patient_id"];
						var patientPrefix = mData["patient_prefix"]
						var patientIdLength = patientId.length;
						for (var k=0;k<6-patientIdLength;k++) {
							patientId = "0"+patientId;
						}
						patientid = patientPrefix+patientId;
						$('#lblPrintVisitId').text(visitid);								
						$('#lblPrintPatientId').text(patientid);
						$('#lblPrintPatient').text(mData["patient_name"]);
						$('#printTestname').text(mData["name"]);
						$("#printRemark").text(mData["report"]);	 
						var visitDate = convertDate(mData["visit_date"]);
						$('#lblPrintVisitDate').text(visitDate);

						/*hospital information through global varibale in main.js*/
						$("#lblPrintEmail").text(hospitalEmail);
						$("#lblPrintPhone").text(hospitalPhoneNo);
						$("#lblPrintAddress").text(hospitalAddress);
						if (hospitalLogo != "null") {
							imagePath = "./images/" + hospitalLogo;
							$("#printLogo").attr("src",imagePath);
						}
						$("#printHospitalName").text(hospitalName);

						//$.print("#PrintRadiologyTest");
						window.print();
					}
				});
				$('.viewImage').unbind();
				$('.viewImage').click(function(){
					$('.modal-imagebox').hide();
					var imageName =$(this).attr('data-images');
					if(imageName == "" || imageName == "null" ){
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("There are no image here..");
						$('#messagemyModal').modal();
					}else{
						$(".modalbody").text("");
						var imageList = imageName.split(';');
						$('#imageMyModalLabel').text("Images");
						$('#myimageModal').click();
						for (i = 0;i< imageList.length-1;i++){
							$(".modalbody").append("<img class='img-thumbnail pickImage' data-action='zoom' style='width:auto;height:25%;margin:1%;"+
														"cursor:pointer' src=./upload_images/radio_test_dp/"+imageList[i]+">");						
						}
						$(".pickImage").unbind();
						$(".pickImage").on('click', function(){	
							/* $('.img-header').hide();
							$('.imageContent').css({"background":"transparent"});	
							$('.modalbody').hide(); */
							$("#imageModal").hide();
							$('.modal-imagebox').show();
							var imagesrc = $(this).attr('src');
							$(".modal-imagebox").html("<input type = 'button' value='X' id = 'imgClose' class='imageClose' onclick=imageClose();"+
														"><img class='img-thumbnail imgBlock'"+
														"style='cursor:pointer' src="+imagesrc+">");
							if($(".imgBlock").height() < 600)
							{
								$('.modal-imagebox').css({"overflow-y": "hidden","height": "auto","left":"30%"});
							}else{
								$('.modal-imagebox').css({"overflow-y": "scroll", "height": "600px","left":"10%"});
							}
						});
					}
				});
			}, 
			"fnServerParams": function ( aoData ) {
			  aoData.push( {"name": "operation", "value": "bindTableData" },
			  {"name": "visitId", "value": visitId },
			  {"name": "patientId", "value": patientId },
			  {"name": "patientName", "value": patientName},
			  {"name": "patientPrefix", "value": patientPrefix},
			  {"name": "visitPrefix", "value": visitPrefix});
			},
			"aoColumns": [
					{  "mData":  function(o) {
	                      	var data = o;
							var visitId=data.visit_id;
							var visitPrefix = data.visit_prefix
							var visitIdLength = visitId.length;
							for (var k=0;k<6-visitIdLength;k++) {
								visitId = "0"+visitId;
							}
							visitid = visitPrefix+visitId;
	                    	return visitid;
	                    }
                    },
					{  "mData": "name" },
					{  "mData": function(o) {
	                    var data = o;
						var imagesPath;
						if(data.images_path == ""){
							imagesPath="null";
						}else{
							imagesPath=data.images_path; 
						}
						
	                    return "<span title='View Image'><a href='#imageModal' class = 'viewImage' data-images="+imagesPath+" style='text-decoration: none;color: #000;'>Click here for view Image</a></span>";
	                   } 
					},
					{  "mData": "report" },
					{  "mData": function(o) {
	                    var data = o;
						var id=data.id;
	                    return "<i class='fa fa-print Print' title='Print'" +
	                        " data-id="+id+" data-original-title='Print'/>";
	                   }
					}
					],
					aoColumnDefs: [{
	                'bSortable': false,
	                'aTargets': [2]
	            },{
	                'bSortable': false,
	                'aTargets': [3]
	            },{
	                'bSortable': false,
	                'aTargets': [4]
	            }]
		});
	});

	$("#btnReset").click(function(){
		$('input[type=text]').val("");
		redResultTable.fnClearTable();
	});

});
function imageClose(){
	/* $('.imageContent').css({"background":"#fff"});
	$('.img-header').show();
	$('.modalbody').show(); */
	$("#imageModal").show();
	$('.modal-imagebox').hide();
}
//function to calculate date
function convertDate(o){
        var dbDateTimestamp = o;
        var dateObj = new Date(dbDateTimestamp * 1000);
        var yyyy = dateObj.getFullYear().toString();
        var mm = (dateObj.getMonth()+1).toString(); // getMonth() is zero-based
        var dd  = dateObj.getDate().toString();
        return yyyy +"-"+ (mm[1]?mm:"0"+mm[0]) +"-"+ (dd[1]?dd:"0"+dd[0]);
}