 var oTableBed; 
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   bed.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Arora
 * Created Date :   30th dec, 2015
 * Description  :   This page add and mange bed
 *************************************************************************************************** */
loader();
debugger;
	bindDepartment();

	$("#selDepartment").change(function(){
		var htmlRoom ="<option value='-1'>Select</option>";
		
		if ($('#selDepartment :selected').val() != -1) {
			var value = $('#selDepartment :selected').val();
			bindWard(value,null);
		}
		else {
			$('#selWard').html(htmlRoom);
		}
	});
	
	$("#advanced-wizard").hide();//by default hise first part
	$("#wardList").addClass('list');
	$("#tabWardList").addClass('tab-list-add');
    /*Click for go to add the room type screen part*/

     $("#tabAddWard").click(function() {
     	$('#btnBedSubmit').show();
		$('#btnReset').show();
		$('#btnUpdateBed').hide();

        $("#advanced-wizard").show();
        $(".blackborder").hide();
        $('#btnSubmit').show();
		$('#btnReset').show();
		$('#btnUpdateWard').hide();
		clear();
        $("#tabAddWard").addClass('tab-detail-add');
        $("#tabAddWard").removeClass('tab-detail-remove');
        $("#tabWardList").removeClass('tab-list-add');
        $("#tabWardList").addClass('tab-list-remove');
        $("#wardList").addClass('list');
		$('#selDepartment').focus();
		//bindDepartment();		

		var htmlRoom ="<option value='-1'></option>";
		
		if ($('#selWard :selected').val() != -1) {
			var value = $('#selWard :selected').val();
			bindRoom("value",null);
		}
		else {
			$('#selRoom').val("");
		}
		$('#selDepartment').val('-1');
		$('#selWard').val('-1');
		$('#selDepartment').focus();
    });;

    $("#tabWardList").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
		// Call the function for show the bed lists
        tabBedList();
    });
	
	//
	//On changing the ward calling the bindRoom function 
	//
	$('#selWard').change(function () {
		var htmlRoom ="<option value='-1'>Select</option>";
		
		if ($('#selWard :selected').val() != -1) {
			var value = $('#selWard :selected').val();
			bindRoom(value,null);
		}
		else {
			$('#selRoom').html(htmlRoom);
		}
	});
	
	/*By default when radio button is not checked show all active data*/
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		loadDataTable();
    }

    /*On change of radio button check it is checked or not*/
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	oTableBed.fnClearTable();
	    	oTableBed.fnDestroy();
	    	//Datatable code
			oTableBed = $('#bedTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  oTableBed.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var ward_id = mData["ward_id"];
							var room_id = mData["room_id"];
							restoreClick(id,ward_id,room_id);//call restore click function with certain parameter to restore
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/bed.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					{ "mData": "department" },
					{ "mData": "name" },
					{ "mData": "roomName" },
					{ "mData": "number" },
					{ "mData": "cost" },
					{ "mData": "price" },
					{ "mData": "description" },
					{  
						"mData": function (o) { 
						
							return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; 
						}
					},
				],
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 7 ] },
				   { 'bSortable': false, 'aTargets': [ 6 ] }
			    ]
			} );
		}
		else{
			oTableBed.fnClearTable();
	    	oTableBed.fnDestroy();
	    	//Datatable code
			loadDataTable();
		}
    });
	
	//		
    /*validation ,save and ajax call on submit button*/
	//
    $("#btnBedSubmit").click(function() {
        var flag = "false";
		
		$("#txtPrice").val($("#txtPrice").val().trim());
		$("#txtCost").val($("#txtCost").val().trim());
		$("#txtBedNumber").val($("#txtBedNumber").val().trim());
		
        if ($("#txtPrice").val() == "") {
			$('#txtPrice').focus();
            $("#txtPriceError").text("Please enter price");
			$("#txtPrice").addClass("errorStyle");       
            flag = "true";
        }
		if(!(/^\d*(\.\d{1})?\d{0,9}$/).test($('#txtPrice').val()))
		{			
			$('#txtPrice').focus();
            $("#txtPriceError").text("Please enter number only");
			$("#txtPrice").addClass("errorStyle");     
            flag = "true";
        }
		if($("#txtPrice").val() != ""){
			var cost = parseInt($("#txtCost").val());
			var price = parseInt($("#txtPrice").val());
			if (price < cost) {
				$('#txtPrice').focus();
				$("#txtPriceError").text("Please enter price greater than cost");
				$("#txtPrice").addClass("errorStyle");       
				flag = "true";
			}
		}
		if ($("#txtCost").val() == "") {
			$('#txtCost').focus();
            $("#txtCostError").text("Please enter cost");
			$("#txtCost").addClass("errorStyle");     
            flag = "true";
        }
		if(!(/^\d*(\.\d{1})?\d{0,9}$/).test($('#txtCost').val()))
		{			
			$('#txtCost').focus();
            $("#txtCostError").text("Please enter number only");
			$("#txtCost").addClass("errorStyle");       
            flag = "true";
        }
        if ($("#txtBedNumber").val() == "") {
			$('#txtBedNumber').focus();
            $("#txtBedNumberError").text("Please enter bed number");
			$("#txtBedNumber").addClass("errorStyle");    
            flag = "true";
        }
		if ($("#selRoom").val() == "-1") {
			$('#selRoom').focus();
            $("#selRoomError").text("Please select room");
			$("#selRoom").addClass("errorStyle");    
            flag = "true";
        }		
        if ($("#selWard").val() == "-1") {
			$('#selWard').focus();
            $("#selWardError").text("Please select ward");
			$("#selWard").addClass("errorStyle");      
            flag = "true";
        }		
        if ($("#selDepartment").val() == "-1") {
			$('#selDepartment').focus();
            $("#selDepartmentError").text("Please select department");
			$("#selDepartment").addClass("errorStyle");      
            flag = "true";
        }
		
        if (flag == "true") {
            return false;
        }
		
		var wardId = $("#selWard").val();
		var departmentId = $("#selDepartment").val();
		var roomId = $("#selRoom").val();
		var bedNumber = $("#txtBedNumber").val();
		var cost = $("#txtCost").val();
		var price = $("#txtPrice").val();
		var description = $("#txtDescription").val();
		description = description.replace(/'/g, "&#39");
		var postData = {
			"operation":"save",
			"wardId":wardId,
			"roomId":roomId,
			"bedNumber":bedNumber,
			"cost":cost,
			"price":price,
			"departmentId" : departmentId,
			"description":description
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/bed.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Bed saved successfully!!!");
					$('#messagemyModal').modal();
					$('#inactive-checkbox-tick').prop('checked', false).change();
					bindDepartment();
					tabBedList();
				}
				else{
					$("#txtBedNumberError").text("Bed number already exists");
					$('#txtBedNumber').focus();
					$("#txtBedNumber").addClass("errorStyle");
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	});	
	
	//
    //keyup functionality
	//
    $("#selWard").change(function() {
        if ($("#selWard").val() != "-1") {
            $("#selWardError").text("");
            $("#txtBedNumberError").text("");
            $("#selWard").removeClass("errorStyle");
        }
    });
	
	$("#selDepartment").change(function() {
        if ($("#selDepartment").val() != "-1") {
            $("#selDepartmentError").text("");
			$("#txtBedNumberError").text("");
            $("#selDepartment").removeClass("errorStyle");
        }
    });
	
	$("#selRoom").change(function() {
        if ($("#selRoom").val() != "-1") {
            $("#selRoomError").text("");
            $("#txtBedNumberError").text("");
            $("#selRoom").removeClass("errorStyle");
        }
    });
	
    $("#txtBedNumber").keyup(function() {
        if ($("#txtBedNumber").val() != "") {
            $("#txtBedNumberError").text("");
            $("#txtBedNumber").removeClass("errorStyle");
        }
    });
	
	$("#txtCost").keyup(function() {
        if ($("#txtCost").val() != "") {
            $("#txtCostError").text("");
            $("#txtCost").removeClass("errorStyle");
        } 
    });
	
	$("#txtPrice").keyup(function() {
        if ($("#txtPrice").val() != "") {
            $("#txtPriceError").text("");
            $("#txtPrice").removeClass("errorStyle");
        }
    });	
});

//		
//function for edit the bed 
//
function editClick(id,department_id,ward_id,room_id,number,cost,price,description){	
    $("#tabAddWard").click();
    $("#selDepartment").focus();
	$("#tabAddWard").html("+Update Bed");
	$('#btnBedSubmit').hide();
	$('#btnReset').hide();
	$("#btnUpdateBed").removeAttr("style");
	$('#selDepartment').val(department_id);
	bindWard(department_id,ward_id);
	//loadWard(ward_id);
	$('#selWard').val(ward_id);
	bindRoom(ward_id,room_id);
	//loadRoom(room_id);
	$("#txtBedNumber").val(number);
	$("#selRoom").val(room_id);
	$("#txtCost").val(cost);
	$("#txtPrice").val(price);
	$('#txtDescription').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id);
	
	/*on click of update button update data*/
	$("#btnUpdateBed").click(function(){
		var flag = validation();
		
		if(flag == "true"){			
			return false;
		}
		else{
			var wardId = $("#selWard").val();
			var departmentId = $("#selDepartment").val();
			var roomId = $("#selRoom").val();
			var bedNumber = $("#txtBedNumber").val();
			var cost = $("#txtCost").val();
			var price = $("#txtPrice").val();
			var description = $("#txtDescription").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();		
			var postData = {
				"operation" : "update",
				"id" : id,
				"wardId":wardId,
				"roomId":roomId,
				"bedNumber":bedNumber,
				"cost":cost,
				"price":price,
				"departmentId":departmentId,
				"description":description
			}
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/bed.php",
					data : postData,
					
					success: function(data) {	
						if(data != "0" && data != ""){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Bed updated successfully!!!");
							oTableBed.fnReloadAjax();
							$('#messagemyModal').modal();
							clear();
							$('#tabWardList').click();
						}
						if(data == "0"){
							$('.close-confirm').click();
							$("#myModal #txtBedNumberError").text("Bed number already exists");
							$('#myModal #txtBedNumber').focus();
							$("#myModal #txtBedNumber").addClass("errorStyle");
						}
					},
					error:function() {
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messagemyModal').modal();
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					}
				});
			});
		}
	});
	
	/*Change functionality to remove style*/
	$("selWard").change(function() {
        if ($("#selWard").val() != "-1") {
            $("#selWardError").text("");
			$("#txtBedNumberError").text("");
            $("#selWard").removeClass("errorStyle");
        }
    });
	
	$("#selDepartment").change(function() {
        if ($("#selDepartment").val() != "-1") {
            $("#selDepartmentError").text("");
			$("#txtBedNumberError").text("");
            $("#selDepartment").removeClass("errorStyle");
        }
    });

	removeErrorMessage();
}


//		
/*function for delete the bed*/
//
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete Bed");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteBed("'+type+'");');	//pass attribute with sent opertion and function with it's type
}

function restoreClick(id,ward_id,room_id){
	$('#selectedRow').val(id);
	$('#selWard').val(ward_id);
	Room(room_id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Restore Bed");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();
	
	/*Room function that pass selected ward value to bindroom function during restore*/
	function Room(room_id){
		var htmlRoom ="<option value='-1'>Select</option>";
		
		if ($('#selWard :selected').val() != -1) {
			var value = $('#selWard :selected').val();
			bindRoom(value,room_id);			
		}
		else {
			$('#selRoom').html(htmlRoom);
		}
	}	
	var type="restore";
	$('#confirm').attr('onclick','deleteBed("'+type+'");');
}

//
/*Function for clear the data*/
//
function deleteBed(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();			
			
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/bed.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data == "1"){					
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Bed deleted successfully!!!");
					$("#bedTable").dataTable().fnReloadAjax();
					$('#messagemyModal').modal();
					 
				}
				if(data == ""){					
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("This bed is used , so you can not delete!!!");
					oTableBed.fnReloadAjax();
					$('#messagemyModal').modal();
				}
			},
			error:function() {
				$('.modal-body').text("");
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
		var ward_id = $('#selWard').val();
		var room_id = $('#selRoom').val();
	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/bed.php",
			data :{            
				"operation" : "restore",
				"id" : id,
				"wardId" : ward_id,
				"roomId" : room_id
			},
			success: function(data) {	
				if(data == "Restored Successfully!!!" && data != ""){
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Bed restored successfully!!!");
					oTableBed.fnReloadAjax();
					$('#messagemyModal').modal();
				}		
				if(data == "It can not be restore!!!"){					
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text(data);
					oTableBed.fnReloadAjax();
					$('#messagemyModal').modal();
				}				
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}
}

//
//Function for clear the data
//
function clear(){
	$('#selWard').val("");
	$('#selDepartment').val("");
	$('#selDepartmentError').text("");
	$('#selWardError').text("");
	$('#selRoom').val("");
	$('#selRoomError').text("");
	$('#txtBedNumber').val("");
	$('#txtBedNumberError').text("");
	$('#txtCost').val("");
	$('#txtCostError').text("");
	$('#txtPrice').val("");
	$('#txtPriceError').text("");
	$('#txtDescription').val("");
}

//
//function binding ward
//
function bindWard(value,ward_id){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/bed.php",
		data: {
			"operation":"showWard",
			departmentId : value
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>--Select--</option>";
				for (var i=0;i<parseData.length;i++)
				{
				option+="<option value='"+parseData[i].id+"'>"+parseData[i].name+"</option>";
				}
				$('#selWard').html(option);	
				$('#myModal #selWard').html(option);
				if(ward_id != null){
					$('#myModal #selWard').val(ward_id);
					$('#selWard').val(ward_id);
				}			
			}
		},
		error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
	});
}

//
//function for binding the room
//
function bindRoom(value,room_id){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/bed.php",
		data: {
			"operation":"showRoom",
			"ward" : value
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var htmlRoom ="<option value='-1'>--Select--</option>";
				for (var i=0;i<parseData.length;i++)
				{
				htmlRoom+="<option value='"+parseData[i].id+"'>"+parseData[i].room_number+"</option>";
				}
				$('#selRoom').html(htmlRoom);
				if(room_id != null){
					$('#selRoom').val(room_id);
				}
				else{				
				}				
			}
		},
		error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
	});	
}

// define function for bind department
function bindDepartment() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/opd_registration.php",
        data: {
            "operation": "showDepartment"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].department + "</option>";
                }
                $('#selDepartment').html(option);
            }
        },
        error: function() {}
    });
}

function tabBedList(){
	$("#advanced-wizard").hide();
	$(".blackborder").show();
	$("#tabAddWard").html('+Add Bed');
	$("#tabAddWard").removeClass('tab-detail-add');
	$("#tabAddWard").addClass('tab-detail-remove');
	$("#tabWardList").addClass('tab-list-add');
	$("#tabWardList").removeClass('tab-list-remove');
	$("#wardList").addClass('list');
	$("#txtWardName").removeClass("errorStyle");
	clear();
    $("#selWard").removeClass("errorStyle");
    $("#selRoom").removeClass("errorStyle");
    $("#txtBedNumber").removeClass("errorStyle");
    $("#txtCost").removeClass("errorStyle");
    $("#txtPrice").removeClass("errorStyle");
	$("#selDepartment").removeClass("errorStyle");
}

//
//function for reset button 
//
$("#btnReset").click(function(){
	$("#selDepartment").removeClass("errorStyle");
	$("#selWard").removeClass("errorStyle");
	$("#selRoom").removeClass("errorStyle");
	$("#txtBedNumber").removeClass("errorStyle");
	$("#txtCost").removeClass("errorStyle");
	$("#txtPrice").removeClass("errorStyle");
	bindDepartment();
	$('#selDepartment').focus();
	clear();
	$('#selWard').val('-1');
	$('#selRoom').val('-1');
});


// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		$(".close").click();
    }
});

function loadDataTable(){
oTableBed = $('#bedTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {

			/*On click of update icon call this function*/
			$('.update').unbind();
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = oTableBed.fnGetData(data);//get datatable data
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					var department_id = mData["department_id"];
					var ward_id = mData["ward_id"];
					var room_id = mData["room_id"];
					var number = mData["number"];
					var cost = mData["cost"];
					var price = mData["price"];
					var description = mData["description"];
					editClick(id,department_id,ward_id,room_id,number,cost,price,description);//call edit click function with certain parameters to update		   	   
				}
			});
			/*On click of delete icon call this function*/
			$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  oTableBed.fnGetData(data);
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);//call delete click function with certain parameter to delete
				}
			});
		},
		
		"sAjaxSource":"controllers/admin/bed.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{ "mData": "department" },
			{ "mData": "name" },
			{ "mData": "roomName" },
			{ "mData": "number" },
			{ "mData": "cost" },
			{ "mData": "price" },
			{ "mData": "description" },
			{  
				"mData": function (o) { 
					/*Return update and delete data id*/
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
				   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
				   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
				   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
				   "  data-original-title='Delete'></i>"; 
				}
			},	
		],
		/*Disable sort for following columns*/
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 6 ] },
		   { 'bSortable': false, 'aTargets': [ 7 ] }
	   ]
	} );
}
function validation(){
	 var flag = "false";
		
		$("#txtPrice").val($("#txtPrice").val().trim());
		$("#txtCost").val($("#txtCost").val().trim());
		$("#txtBedNumber").val($("#txtBedNumber").val().trim());
		
        if ($("#txtPrice").val() == "") {
			$('#txtPrice').focus();
            $("#txtPriceError").text("Please enter price");
			$("#txtPrice").addClass("errorStyle");       
            flag = "true";
        }
		if(!(/^\d*(\.\d{1})?\d{0,9}$/).test($('#txtPrice').val()))
		{			
			$('#txtPrice').focus();
            $("#txtPriceError").text("Please enter number only");
			$("#txtPrice").addClass("errorStyle");     
            flag = "true";
        }
		if($("#txtPrice").val() != ""){
			var cost = parseInt($("#txtCost").val());
			var price = parseInt($("#txtPrice").val());
			if (price < cost) {
				$('#txtPrice').focus();
				$("#txtPriceError").text("Please enter price greater than cost");
				$("#txtPrice").addClass("errorStyle");       
				flag = "true";
			}
		}
		if ($("#txtCost").val() == "") {
			$('#txtCost').focus();
            $("#txtCostError").text("Please enter cost");
			$("#txtCost").addClass("errorStyle");     
            flag = "true";
        }
		if(!(/^\d*(\.\d{1})?\d{0,9}$/).test($('#txtCost').val()))
		{			
			$('#txtCost').focus();
            $("#txtCostError").text("Please enter number only");
			$("#txtCost").addClass("errorStyle");       
            flag = "true";
        }
        if ($("#txtBedNumber").val() == "") {
			$('#txtBedNumber').focus();
            $("#txtBedNumberError").text("Please enter bed number");
			$("#txtBedNumber").addClass("errorStyle");    
            flag = "true";
        }
		if ($("#selRoom").val() == "-1") {
			$('#selRoom').focus();
            $("#selRoomError").text("Please select room");
			$("#selRoom").addClass("errorStyle");    
            flag = "true";
        }		
        if ($("#selWard").val() == "-1") {
			$('#selWard').focus();
            $("#selWardError").text("Please select ward");
			$("#selWard").addClass("errorStyle");      
            flag = "true";
        }		
        if ($("#selDepartment").val() == "-1") {
			$('#selDepartment').focus();
            $("#selDepartmentError").text("Please select department");
			$("#selDepartment").addClass("errorStyle");      
            flag = "true";
        }
        return flag;
}
//# sourceURL = bed.js