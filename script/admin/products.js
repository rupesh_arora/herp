var productTable;
$(document).ready(function(){
	debugger;
	$("#formProducts").hide();
    $("#divProducts").addClass('list');    
    $("#tabProductsList").addClass('tab-list-add');
    

    $("#tabProduct").click(function() { // show the add allergies categories tab
       showAddTab();
        clearFormDetails(".clearForm");
       
    });

    $("#tabProductsList").click(function() { // show allergies list tab
		showTabList();  
       
    });
    bindUnitMeasure();
    appendTaxes();
    removeErrorMessage();// remove error messaages

    $("#btnSave").click(function(){
    	var flag = false;

        if($('#selUnit').val() == ''){
            $('#selUnitError').text('Please select unit');
            $('#selUnit').focus();
            $('#selUnit').addClass('errorStyle');
            flag == true;
        }
        if(validTextField('#txtCostPrice','#txtCostPriceError','Please enter cost price') == true)
        {
            flag = true;
        }
        if(validTextField('#txtProductName','#txtProductNameError','Please enter product name') == true)
        {
            flag = true;
        }
        
        
        if(flag == true) {
			return false;
		}

		var productName = $("#txtProductName").val().trim();
		var costprice = $("#txtCostPrice").val().trim();
        var unit = $("#selUnit").val().trim();
        var arrayTaxes = [];
        $.each($('.chkTaxes'),function(i,v){
            if ($(v).prop("checked") == true) {
                arrayTaxes.push(v.value);
            }
        });

		var postData = {
			productName : productName,
			costprice : costprice,
            arrayTaxes : JSON.stringify(arrayTaxes),
            unit : unit,
			"operation" : "save"
		}

		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/products.php",
			datatype: "json",
			data: postData,
			 success: function(data){
                if(data !='' && data!=null && data !='0'){
                    callSuccessPopUp("Success","Saved  successfully!!!");
                    clearFormDetails(".clearForm");
                    showTabList();
                    $("#txtProductName").focus();
                }
                else if (data == "0") {
                    $("#txtProductName").focus();
                    $("#txtProductName").addClass("errorStyle");
                    $("#txtProductNameError").text("This tax name alreday exist.");                    
                }
            },
            error:function(){

            }
        });

    });

    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code
        productTable = $('#productTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = productTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var productName = mData["product_name"];
                        var costprice = mData["cost_price"];
                        var unit = mData["unit_id"];
                        var taxId = mData["tax_id"];
                        editClick(id,productName,costprice,unit,taxId);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = productTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/products.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "product_name"
                },  {
                    "mData": "cost_price"
                }, {
                    "mData": "tax_name"
                }, {
                    "mData": "unit"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [2,4]
            }]
        });
    }
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            productTable.fnClearTable();
            productTable.fnDestroy();
            productTable = "";
            productTable = $('#productTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = productTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var tax_name = mData['tax_name'];
                            restoreClick(id,tax_name);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/products.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                {
                    "mData": "product_name"
                },  {
                    "mData": "cost_price"
                }, {
                    "mData": "tax_name"
                }, {
                    "mData": "unit"
                }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [2,4]
                }]
            });
        } else { // show active data on unchecked   
            productTable.fnClearTable();
            productTable.fnDestroy();
            productTable = "";
            productTable = $('#productTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = productTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var productName = mData["product_name"];
                            var costprice = mData["cost_price"];
                            var unit = mData["unit_id"];
                            var taxId = mData["tax_id"];
                            editClick(id,productName,costprice,unit,taxId);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = productTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/products.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
                    "mData": "product_name"
                }, {
                    "mData": "cost_price"
                }, {
                    "mData": "tax_name"
                }, {
                    "mData": "unit"
                }, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [2,4]
                }]
            });
        }
    });

    $("#btnReset").click(function() {
       clearFormDetails(".clearForm");
      // $('#selUnit').val('-1');
       removeErrorMessage();
       $("#txtProductName").focus();
    });

});
function editClick(id,productName,costprice,unit,taxId) {
        
    showAddTab();
    $("#uploadFile").hide();
    $("#btnReset").hide();
    $("#btnSave").hide();
    $('#btnUpdate').show();
    $('#tabProductsList').html("+Update Product");
    $("#txtProductName").focus();
 
  
    $("#txtProductName").removeClass("errorStyle");
    $("#txtProductNameError").text("");   
    
    $('#txtProductName').val(productName);
    $('#txtCostPrice').val(costprice);
    $("#selUnit").val(unit);

    if(taxId != undefined){
        var splitedTaxId = taxId.split(',');
        $.each(splitedTaxId,function(i,v){
            $("#chkTax"+v).prop("checked",true);
        });
    }
    

    $('#selectedRow').val(id);
    //validation
    //remove validation style
    // edit data function for update 
    
    removeErrorMessage();
    
    
    $("#btnUpdate").click(function() { // click update button
        var flag = "false";
        if ($("#selUnit").val().trim()== '') {
            $("#selUnitError").text("Please select unit");
            $("#selUnit").focus();
            $("#selUnit").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtCostPrice").val().trim()== '') {
            $("#txtCostPriceError").text("Please enter cost price");
            $("#txtCostPrice").focus();
            $("#txtCostPrice").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtProductName").val().trim()== '') {
            $("#txtProductNameError").text("Please enter product name");
            $("#txtProductName").focus();
            $("#txtProductName").addClass("errorStyle");
            flag = "true";
        }
        
        if(flag == "true") {
            return false;
        }
        
        
        var productName = $("#txtProductName").val().trim();
        var costprice = $("#txtCostPrice").val().trim();
        var unit = $("#selUnit").val().trim();
        var arrayTaxes = [];
        $.each($('.chkTaxes'),function(i,v){
            if ($(v).prop("checked") == true) {
                arrayTaxes.push(v.value);
            }
        });      
        
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();

        $("#btnConfirm").click(function(){
            var postData = {
                "operation": "update",
                "productName": productName,
                "costprice": costprice,
                "id": id,
                "unit" : unit,
                "arrayTaxes" : JSON.stringify(arrayTaxes)
            }
            $.ajax({ //ajax call for update data
                
                type: "POST",
                cache: false,
                url: "controllers/admin/products.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data == "1") {
                        callSuccessPopUp("Success","Product updated Successfully!!!");
                        $('#myModal .close').click();
                        productTable.fnReloadAjax();
                        clearFormDetails(".clearForm");
                        showTabList();
                    }
                    else{
                        $("#txtProductName").focus();
                        $("#txtProductName").addClass('errorStyle');
                        $("#txtProductNameError").text("This name alreday exist.");
                    }
                },
                error: function() {
                    callSuccessPopUp("Error","Temporary Unavailable to Respond.Try again later!!!");
                }
            }); // end of ajax
        });
    });
} // end update button
function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete product");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    var productName = '';
    $('#confirm').attr('onclick', 'deleteproductName("'+type+'","'+productName+'");');
} // end click fucntion

function restoreClick(id,tax_name) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore tax name");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    var productName = tax_name;
    $('#confirm').attr('onclick', 'deleteproductName("'+type+'","'+productName+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteproductName(type,productName) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/products.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data == "1") {
                    callSuccessPopUp("Success","Product deleted Successfully!!!");
                    productTable.fnReloadAjax();
                } 
            },
            error: function() {
                callSuccessPopUp("Error","Temporary Unavailable to Respond.Try again later!!!");
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/products.php",
            data: {
                "operation": "restore",
                "id": id,
                "productName" : productName
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    callSuccessPopUp("Success","Tax name restored Successfully!!!");
                    productTable.fnReloadAjax();
                }
                else{
                    callSuccessPopUp('Alert','This tax name already exist. So you can\'t restore it.!!!');
                }
            },
            error: function() {
                callSuccessPopUp("Error","Temporary Unavailable to Respond.Try again later!!!"); 
            }
        });
    }
}

function bindUnitMeasure(){
    $.ajax({                    
        type: "POST",
        cache: false,
        url: "controllers/admin/drugs.php",
        data: {
            "operation":"showUnitMeasure"
        },
        success: function(data) {   
            if(data != null && data != ""){
                var parseData= jQuery.parseJSON(data);
            
                var option ="<option value=''>-- Select --</option>";
                for (var i=0;i<parseData.length;i++)
                {
                    option+="<option value='"+parseData[i].id+"'>"+parseData[i].unit_abbr+"</option>";
                }
                $('#selUnit').html(option);              
                
            }
        },
        error:function(){
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();           
        }
    });
}
function appendTaxes(){
    var postData = {
        operation : "getTaxes"
    }
    dataCall("controllers/admin/products.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (postData !='') {
            var html = '';
            $.each(parseData,function(i,v){
                html+='<div class="taxContainer"><input type="checkbox" class="chkTaxes" id="chkTax'+v.id+'" value="'+v.id+'"><label class="productName" for="chkTax'+v.id+'">'+v.tax_name+'</label></div>';
            });
            $("#appendTaxes").html(html);
        }
    });
}

function showAddTab(){
    $("#formProducts").show();
    $(".blackborder").hide();

    $("#tabProduct").addClass('tab-detail-add');
    $("#tabProduct").removeClass('tab-detail-remove');
    $("#tabProductsList").removeClass('tab-list-add');
    $("#tabProductsList").addClass('tab-list-remove');
    $("#divProducts").addClass('list');
    $("#txtProductName").focus();
}

function showTabList(){

    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#formProducts").hide();
    $(".blackborder").show();

    $("#tabProduct").removeClass('tab-detail-add');
    $("#tabProduct").addClass('tab-detail-remove');
    $("#tabProductsList").removeClass('tab-list-remove');    
    $("#tabProductsList").addClass('tab-list-add');
    $("#divProducts").addClass('list');


    $("#uploadFile").show();
    $("#btnReset").show();
    $("#btnSave").show();
    $('#btnUpdate').hide();
    $('#tabProductsList').html("+Add Product");
     clearFormDetails(".clearForm");
     $('.chkTaxes').prop('checked',false);
}