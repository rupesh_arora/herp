$(document).ready(function() {
	debugger;	
	for (i = new Date().getFullYear(); i > 2014; i--){
   		$('#selYear').append($('<option />').val(i).html(i));
	}
	google.charts.setOnLoadCallback(drawChart);
	$("#clear").click(function(){
		clear();
	});
});
function drawChart() {
	$("#viewReport").on("click",function(){
		var chartWidth  = $("#page-content").width() -25;
		var collectionRecord = [['Record-Group', 'Count']];
		var year = $('#selYear').val();
		var postData = {
			"operation" : "showChartData",
			"year" : year
		}
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/month_wise_collection_comparision.php",
			data: postData,
			success: function(data) { 
				if(data != null && data != ""){
					var parseData =  JSON.parse(data);

					if (parseData[0]['Jan'] =='' &&parseData[0]['Feb'] =='' &&parseData[0]['Mar'] =='' &&parseData[0]['Apr'] =='' &&
						parseData[0]['May'] =='' &&parseData[0]['Jun'] =='' &&parseData[0]['Jul'] =='' &&parseData[0]['Aug'] =='' &&
						parseData[0]['Sept'] =='' &&parseData[0]['Oct'] =='' &&parseData[0]['Nov'] =='' &&parseData[0]['Dec'] =='' ) {
						callSuccessPopUp("Alert","No data exist");
						$("#chart_div,#btnSavePdf,#btnSaveExcel").hide();
						return false;
					}	
					
					collectionRecord = [['Month', 'Amount']];

					$.each(parseData,function(index,value){
						$.each(parseData[index],function(i,v){

							collectionRecord.push([i,parseInt(v)]);
						});	
					});

		            
		            var datapatientRecordGroup = google.visualization.arrayToDataTable(collectionRecord);

				    var options = {
				      title: 'Month Amount Wise Collection','width':1200,'height':1000,is3D:true
				    };

	        		var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
	        		chart.draw(datapatientRecordGroup, options);        		
				}
				$("#chart_div,#btnSavePdf,#btnSaveExcel").show();

				/*function to save in to excel file*/
				window.saveFile = function saveFile () {
					var opts = [{sheetid:'One',header:true}];
					var res = alasql('SELECT INTO XLSX("Month-Wise-Collection.xlsx",?) FROM ?',[opts,[parseData]]);
				}
			}			
		});
	});

	$("#btnSavePdf").click(function(){
		$.print("#printableArea");
	});
}
function clear() {
	$("#chart_div,#btnSavePdf,#btnSaveExcel").hide();
}