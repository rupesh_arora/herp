 var oTableBed; 
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   bed.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Arora
 * Created Date :   30th dec, 2015
 * Description  :   This page add and mange bed
 *************************************************************************************************** */
	loader();	
	removeErrorMessage();
	debugger;
	
	$("#advanced-wizard").hide();//by default hise first part
	$("#bedList").css({
        "background-color": "#fff"
    });
	$("#tabBedList").css({
        "background-color": "#0c71c8",
        "color": "#fff"
    });

    /*Click for go to add the room type screen part*/
    $("#tabAddBed").click(function() {
        $("#advanced-wizard").show();
        $(".blackborder").hide();
		clear();
        $("#tabAddBed").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tabBedList").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#bedList").css({
            "background": "#fff"
        });
    });
    /*Click function for show the bed lists*/
    $("#tabBedList").click(function() {
    	oTableBed.fnReloadAjax();
		tabBedList();
    });
	
	/*By default when radio button is not checked show all active data*/
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		loadDataTable();
    }

    //click on search icon load view visit screen in modal 
	$("#iconSearch").click(function(){
		$("#txtImprestRefError").text("");
		$("#txtImprestRef").removeClass("errorStyle");		
		$('#imprestModalLabel').text("Search Visit");
        $('#imprestModalBody').load('views/admin/search_imprest_request.html');
		$('#myImprestModal').modal();
	});

    /*On change of radio button check it is checked or not*/
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	oTableBed.fnClearTable();
	    	oTableBed.fnDestroy();
	    	//Datatable code
			oTableBed = $('#tblImprestDisbursement').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					/*$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  oTableBed.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);//call restore click function with certain parameter to restore
						}
						
					});*/
				},
				
				"sAjaxSource":"controllers/admin/imprest_disbursement.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					{ "mData": function(o)
						{
							var imprestId = o["imprest_request_id"];
							var imprestPrefix = o["imprest_prefix"];
								
							var visitIdLength = imprestId.length;
							for (var i=0;i<6-visitIdLength;i++) {
								imprestId = "0"+imprestId;
							}
							imprestId = imprestPrefix+imprestId;
							return imprestId;  

						} 
					},
					{ "mData": "name" },
					{ "mData": "ledger_name" },
					{ "mData": "cheque_no" },
					{ "mData": "amount" },
					/*{  
						"mData": function (o) { 
						
							return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; 
						}
					},*/
				],
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 4 ] }
			    ]
			} );
		}
		else{
			oTableBed.fnClearTable();
	    	oTableBed.fnDestroy();
	    	//Datatable code
			loadDataTable();
		}
    });
	
	//		
    /*validation ,save and ajax call on submit button*/
	//
    $("#btnBedSubmit").click(function() {
        var flag = "false";
		
		$("#txtPaymentAmount").val($("#txtPaymentAmount").val().trim());
		$("#txtChequeNo").val($("#txtChequeNo").val().trim());		
        
		if ($("#selPaymentMode").val() == "-1") {
			$('#selPaymentMode').focus();
            $("#selPaymentModeError").text("Please payment mode");
			$("#selPaymentMode").addClass("errorStyle");    
            flag = "true";
        }		
        if ($("#txtChequeNo").val() == "") {
			$('#txtChequeNo').focus();
            $("#txtChequeNoError").text("Please enter cheque no.");
			$("#txtChequeNo").addClass("errorStyle");      
            flag = "true";
        }	
        if(parseInt($("#txtPaymentAmount").val()) > parseInt(grandTotal)){
			$('#txtPaymentAmount').focus();
            $("#txtPaymentAmountError").text("Please enter valid amount");
			$("#txtPaymentAmount").addClass("errorStyle");      
            flag = "true";
		}	
        if ($("#txtPaymentAmount").val() == "") {
			$('#txtPaymentAmount').focus();
            $("#txtPaymentAmountError").text("Please enter amount");
			$("#txtPaymentAmount").addClass("errorStyle");      
            flag = "true";
        }

        var visitId = $("#txtImprestRef").val();
		if($("#txtImprestRef").val()==""){
			$("#txtImprestRef").focus();
			$("#txtImprestRefError").text("Please enter imprest ref.");
			$("#txtImprestRef").addClass("errorStyle");       
			flag = "true";
		}
		
		if(visitId.length != 9){
			$("#txtImprestRef").focus();
			$("#txtImprestRefError").text("Please enter valid imprest ref.");
			$("#txtImprestRef").addClass("errorStyle"); 
			flag = "true";
		}

        if (flag == "true") {
            return false;
        }
        var imprestId = $("#txtImprestRef").val();
		var imprestPrefix = imprestId.substring(0, 3);
		imprestId = imprestId.replace ( /[^\d.]/g, '' ); 
		imprestId = parseInt(imprestId);
		
        if (flag == "true") {
            return false;
        }
        if ($('#chkBankTransfer').is(":checked")==false) {
            $('#chkBankTransfer').val(0);
        }
        else{
            $('#chkBankTransfer').val(1);
        }
		var chequeDate = $("#txtImprestDate").val();
		var payeeName = $("#txtApplicantName").val();
		var amount = $("#txtPaymentAmount").val();
		var chequeNo = $("#txtChequeNo").val();
		var paymentMode = $("#selPaymentMode").val();
		var bankTransfer = $("#chkBankTransfer").val();
		var postData = {
			"operation":"save",
			"imprestId":imprestId,
			"imprestPrefix":imprestPrefix,
			"chequeNo":chequeNo,
			"paymentMode":paymentMode,
			"amount":amount,
			"bankTransfer":bankTransfer
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/imprest_disbursement.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#disbursementModalLabel').text("Success");
					$('#disbursementModalBody').text("Imprest disbursement saved successfully!!!");
					$('#printDisbursementModal').modal();
    				oTableBed.fnReloadAjax();
					tabBedList();
					$("#printDisbursementModal .modal-footer").html('<input type="button" class="btn_save" value="Print" onclick= "chequeDetails(\''+chequeDate+'\',\''+payeeName+'\',\''+amount+'\');" id="print">');
				}
				else{
					$("#txtImprestRefError").text("Please enter valid imprest ref.");
					$('#txtImprestRef').focus();
					$("#txtImprestRef").addClass("errorStyle");
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	});	
	//
	//function for reset button 
	//
	$("#btnDisbursementReset").click(function(){	
		$('#txtImprestRef').focus();
		clear();
	});

	
});

//		
//function for edit the bed 
//

/*function editClick(id,bank_transfer,disbursement_amount,ledger_id,applicantName,nature_of_duty,assignment_start,assignment_end,imprestId,ledger_name,payment_mode,amount,cheque_no){	
    
	$('#myModalLabel').text("Update Imprest Disbursement");
	$('.modal-body').html($("#bedModel").html()).show();
	$("#myModal #btnUpdateBed").removeAttr("style");
	$('#myModal #iconSearch').hide();
	$('#myModal #txtImprestRef').val(imprestId);
	$('#myModal #txtLedger').val(ledger_name);
	$('#myModal #txtApplicantName').val(ledger_name);
	bindPaymentMode(ledger_id,payment_mode);
	$("#myModal #txtImprestDate").val(assignment_start);
	if(bank_transfer == "1"){
        $('#myModal #chkBankTransfer').prop("checked",true);
	}
	else{
		 $('#myModal #chkBankTransfer').prop("checked",false);
	}
	$("#myModal #txtSurrenderDate").val(assignment_end);
	$("#myModal #txtDescription").val(nature_of_duty);
	$("#myModal #txtPaymentAmount").val(disbursement_amount);
	$("#myModal #txtAmount").val(amount);
	$("#myModal #txtChequeNo").val(cheque_no);
	$('#selectedRow').val(id);
    $('#myModal').modal('show');
	 $('#myModal').on('shown.bs.modal', function() {
        $("#myModal #txtPaymentAmount").focus();
    });		
	
	//on click of update button update data
	$("#myModal #btnUpdateBed").click(function(){
		var flag = "false";
		
		$("#myModal #txtAmount").val($("#myModal #txtAmount").val().trim());
		$("#myModal #txtChequeNo").val($("#myModal #txtChequeNo").val().trim());		
        
		if ($("#myModal #selPaymentMode").val() == "-1") {
			$('#myModal #selPaymentMode').focus();
            $("#myModal #selPaymentModeError").text("Please payment mode");
			$("#myModal #selPaymentMode").addClass("errorStyle");    
            flag = "true";
        }		
        if ($("#myModal #txtChequeNo").val() == "-1") {
			$('#myModal #txtChequeNo').focus();
            $("#myModal #txtChequeNoError").text("Please enter cheque no.");
			$("#myModal #txtChequeNo").addClass("errorStyle");      
            flag = "true";
        }	
        if(parseInt($("#myModal #txtPaymentAmount").val()) > parseInt($("#myModal #txtAmount").val())){
			$('#myModal #txtPaymentAmount').focus();
            $("#myModal #txtPaymentAmountError").text("Please enter valid amount");
			$("#myModal #txtPaymentAmount").addClass("errorStyle");      
            flag = "true";
		}		
        if ($("#myModal #txtAmount").val() == "") {
			$('#myModal #txtAmount').focus();
            $("#myModal #txtAmountError").text("Please enter amount");
			$("#myModal #txtAmount").addClass("errorStyle");      
            flag = "true";
        }

        var visitId = $("#myModal #txtImprestRef").val();
		if($("#myModal #txtImprestRef").val()==""){
			$("#myModal #txtImprestRef").focus();
			$("#myModal #txtImprestRefError").text("Please enter imprest ref.");
			$("#myModal #txtImprestRef").addClass("errorStyle");       
			flag = "true";
		}
		
		if(visitId.length != 9){
			$("#myModal #txtImprestRef").focus();
			$("#myModal #txtImprestRefError").text("Please enter valid imprest ref.");
			$("#myModal #txtImprestRef").addClass("errorStyle"); 
			flag = "true";
		}
        if (flag == "true") {
            return false;
        }
        var imprestId = $("#myModal #txtImprestRef").val();
		var imprestPrefix = imprestId.substring(0, 3);
		imprestId = imprestId.replace ( /[^\d.]/g, '' ); 
		imprestId = parseInt(imprestId);
		
        if (flag == "true") {
            return false;
        }
        if ($('#myModal #chkBankTransfer').is(":checked")==false) {
            $('#myModal #chkBankTransfer').val(0);
        }
        else{
            $('#myModal #chkBankTransfer').val(1);
        }
		
		var amount = $("#myModal #txtPaymentAmount").val();
		var chequeNo = $("#myModal #txtChequeNo").val();
		var paymentMode = $("#myModal #selPaymentMode").val();
		var bankTransfer = $("#myModal #chkBankTransfer").val();
		var id = $('#selectedRow').val();
		var postData = {
			"operation":"update",
			"id" : id,
			"imprestId":imprestId,
			"imprestPrefix":imprestPrefix,
			"chequeNo":chequeNo,
			"paymentMode":paymentMode,
			"amount":amount,
			"bankTransfer":bankTransfer
		}
		
			
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to update this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			$.ajax({
				type: "POST",
				cache: "false",
				url: "controllers/admin/imprest_disbursement.php",
				data : postData,
				
				success: function(data) {	
					if(data != "0" && data != ""){
						$('#myModal').modal('hide');
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Imprest disbursement updated successfully!!!");
						oTableBed.fnReloadAjax();
						$('#messagemyModal').modal();
						clear();
					}
				},
				error:function() {
					$('.close-confirm').click();
					$('.modal-body').text("");
					$('#messagemyModal').modal();
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				}
			});
		});
	});	
}*/


//		
/*function for delete the bed*/
//
/*function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete Imprest Disbursement");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteBed("'+type+'");');	//pass attribute with sent opertion and function with it's type
}*/

/*function restoreClick(id){
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Restore Imprest disbursement");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();
	
	//Room function that pass selected ward value to bindroom function during restore
	
	var type="restore";
	$('#confirm').attr('onclick','deleteBed("'+type+'");');
}*/

//
/*Function for clear the data*/
//
/*function deleteBed(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();			
			
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/imprest_disbursement.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data == "1"){					
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Imprest disbursement deleted successfully!!!");
					oTableBed.fnReloadAjax();
					$('#messagemyModal').modal();
					 
				}
				if(data == ""){					
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("This imprest disbursement is used , so you can not delete!!!");
					oTableBed.fnReloadAjax();
					$('#messagemyModal').modal();
				}
			},
			error:function() {
				$('.modal-body').text("");
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/imprest_disbursement.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data == "Restored Successfully!!!" && data != ""){
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Imprest disbursement restored successfully!!!");
					oTableBed.fnReloadAjax();
					$('#messagemyModal').modal();
				}		
				if(data == "It can not be restore!!!"){					
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text(data);
					oTableBed.fnReloadAjax();
					$('#messagemyModal').modal();
				}				
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}
}*/

//
//Function for clear the data
//
function clear(){
	$("input[type=text]").val("");
	$("input[type=text]").next("span").text("");
	$("input[type=text]").parent().next("span").text("");  
	$("input[type=text]").removeClass('errorStyle'); 
	$("input[type=number]").val("");
	$("input[type=number]").next("span").text("");  
	$("input[type=number]").parent().next("span").text("");
	$("input[type=number]").removeClass('errorStyle');  
	$("textarea").val("");
	$("textarea").next("span").text("");  
	$("textarea").removeClass('errorStyle');  
	$("select").val("");
	$("select").next("span").text(""); 
	$("select").parent().next("span").text(""); 
	$("#selPaymentMode").html("<option value='-1'>-- Select --</option>");
	$("select").next("span").text("");  
	$("#txtImprestRefError").text("");  
	$("select").removeClass('errorStyle');
}

function bindPaymentMode(ledgerId,payment_mode) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/imprest_disbursement.php",
        data: {
            "operation": "showPaymentMode",
            "ledgerId": ledgerId
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selPaymentMode').html(option);
            }
            if(payment_mode != null){
				$("#myModal #selPaymentMode").html(option);
				$("#myModal #selPaymentMode").val(payment_mode);
			}
        },
        error: function() {}
    });
}



function tabBedList(){
	$("#advanced-wizard").hide();
    $(".blackborder").show();
	$('#btnAddDepartment').val("Add Imprest Disbursement");
	$('#tabAddBed').html("+Add Imprest Disbursement");

    $("#tabAddBed").css({
        "background": "#fff",
        "color": "#000"
    });
    $("#tabBedList").css({
        "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
        "color": "#fff"
    });
    $("#bedList").css({"background": "#fff"});
	clear();
    $("#selWard").removeClass("errorStyle");
    $("#selRoom").removeClass("errorStyle");
    $("#txtBedNumber").removeClass("errorStyle");
    $("#txtCost").removeClass("errorStyle");
    $("#txtPrice").removeClass("errorStyle");
	$("#selDepartment").removeClass("errorStyle");
}

// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		$(".close").click();
    }
});

function loadDataTable(){
	oTableBed = $('#tblImprestDisbursement').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {

			/*On click of update icon call this function*/
			/*$('.update').unbind();
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = oTableBed.fnGetData(data);//get datatable data
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];

					var imprestId = mData["imprest_request_id"];
					var imprestPrefix = mData["imprest_prefix"];
					var visitIdLength = imprestId.length;
					for (var i=0;i<6-visitIdLength;i++) {
						imprestId = "0"+imprestId;
					}
					imprestId = imprestPrefix+imprestId;

					var ledger_name = mData["ledger_name"];
					var applicantName = mData["applicantName"];
					var assignment_start = mData["assignment_start"];
					var assignment_end = mData["assignment_end"];
					var payment_mode = mData["payment_mode"];
					var nature_of_duty = mData["nature_of_duty"];
					var disbursement_amount = mData["disbursement_amount"];
					var amount = mData["amount"];
					var cheque_no = mData["cheque_no"];
					var bank_transfer = mData["bank_transfer"];
					var ledger_id = mData["ledger_id"];
					editClick(id,bank_transfer,disbursement_amount,ledger_id,applicantName,nature_of_duty,assignment_start,assignment_end,imprestId,ledger_name,payment_mode,amount,cheque_no);//call edit click function with certain parameters to update		   	   
				}
			});*/
			/*On click of delete icon call this function*/
			/*$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  oTableBed.fnGetData(data);
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);//call delete click function with certain parameter to delete
				}
			});*/
		},
		
		"sAjaxSource":"controllers/admin/imprest_disbursement.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{ "mData": function(o){
					var imprestId = o["imprest_request_id"];
					var imprestPrefix = o["imprest_prefix"];
						
					var visitIdLength = imprestId.length;
					for (var i=0;i<6-visitIdLength;i++) {
						imprestId = "0"+imprestId;
					}
					imprestId = imprestPrefix+imprestId;
					return imprestId;  

				} 
			},
			{ "mData": "name" },
			{ "mData": "ledger_name" },
			{ "mData": "cheque_no" },
			{ "mData": "disbursement_amount" },
			/*{  
				"mData": function (o) { 
					//Return update and delete data id
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
				   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
				   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
				   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
				   "  data-original-title='Delete'></i>"; 
				}
			},	*/
		],
		/*Disable sort for following columns*/
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 4 ] }
	   ]
	} );
}

//# sourceURL = bed.js