/*
 * File Name    :   cash.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar gupta
 * Created Date :   31th dec, 2015
 * Description  :   This page manage cash deposite of patient
 */
$(document).ready(function() {
	loader();
    debugger;
    $("#txtPatientID").focus();

    $("#btnReset").click(function() { // reset button
        clear();
        $("#txtPatientID").focus();
    });
    $("#btnSelect").click(function() { // click for fetch patient information
        $("#txtIdError").text("");
		$("#txtPatientID").removeClass("errorStyle");
		var patientID = $("#txtPatientID").val();
        var flag = "false";
        if ($("#txtPatientID").val().trim() == "") {
            $("#txtPatientID").focus();
            $("#txtIdError").text("Please enter patient id");
            $("#txtPatientID").addClass("errorStyle");
            $("#simpleError").text("");
            flag = "true";
        }
		if(patientID.length != 9){
			$("#txtPatientID").focus();
            $("#txtIdError").text("Please enter patient id");
            $("#txtPatientID").addClass("errorStyle");
            $("#simpleError").text("");
            flag = "true";
		}
        if (flag == "true") {
            return false;
        }
        var patientID = $("#txtPatientID").val();
		var patientPrefix = patientID.substring(0, 3);
		patientID = patientID.replace ( /[^\d.]/g, '' ); 
		patientID = parseInt(patientID);
		
        $("#simpleError").text("");
        $.ajax({ // ajax call for show content
            type: "POST",
            cache: "false",
            url: "controllers/admin/cash.php",
            datatype: "json",
            data: {
                patientID: patientID,
                patientPrefix: patientPrefix,
                "operation": "select"
            },
            success: function(data) {
                if (data != null && data != "2") {
                    var parseData = jQuery.parseJSON(data);
                    /*for (var i = 1; i < parseData.length; i++) {*/
                        if (parseData[0].first_name != null) {

                            $("#txtFirstName").val(parseData[0].first_name);
                            $("#txtLastName").val(parseData[0].last_name);
                            $("#txtFatherName").val(parseData[0].middle_name);
                            $("#hdnFanId").val(parseData[0].fan_id);
							if(parseData[0].gender == "M"){
								 $("#txtGender").val("Male");
							}else{
								$("#txtGender").val("Female");
							}
                           
                            $("#val_DOB").val(parseData[0].dob);
                            $("#txtMobile").val(parseData[0].mobile);
                            var amount = 0;
                            if(parseData.length == 2){
                                amount = parseData[1].amount;
                            }
                            else{
                                 amount = parseData[0].amount;
                            }
							if(amount == null)
							{
								$("#txtAmount").val('0');
							}
							else{
								$("#txtAmount").val(amount);
							}
                            
                        } 
                        else {
                            $("#txtPatientID").focus();
                            $("#txtIdError").text("Please enter valid patient id");
                            $("#txtPatientID").addClass("errorStyle");
                            $("#txtFirstName").val("");
                            $("#txtLastName").val("");
                            $("#txtFatherName").val("");
                            $("#txtGender").val("");
                            $("#val_DOB").val("");
                            $("#txtMobile").val("");
                            $("#txtAmount").val("");
                            $("#txtDeposite").val("");
                            $("#txtDepositError").text("");
                            $("#txtDeposite").removeClass("errorStyle");
                        }
                    /*}*/
                }
				if (data == "2") {
					$("#txtPatientID").focus();
					$("#txtIdError").text("Please enter valid patient id");
					$("#txtPatientID").addClass("errorStyle");
					$("#txtFirstName").val("");
					$("#txtLastName").val("");
					$("#txtFatherName").val("");
					$("#txtGender").val("");
					$("#val_DOB").val("");
					$("#txtMobile").val("");
					$("#txtAmount").val("");
					$("#txtDeposite").val("");
					$("#txtDepositError").text("");
					$("#txtDeposite").removeClass("errorStyle");
				}
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    });

    $("#btnSubmit").click(function() { // click on submit for save amount of patient in  cash account
        var flag = "false";
		/* if (!validnumber($("#txtDeposite").val())) {
		   $('#txtDeposite').focus();
		   $("#txtDepositError").text("Please enter only number");
		   $('#txtDeposite').addClass("errorStyle");
		  flag = "true";  
	  } */
        if ($("#txtDeposite").val().trim() == "") {
            $("#txtDepositError").text("Please enter deposit fee.");
            $("#txtDeposite").focus();
            $("#txtDeposite").addClass("errorStyle");
            flag = "true";
        } 
        if ($("#txtPatientID").val().trim() == "") {
            $("#txtPatientID").focus();
            $("#txtIdError").text("Please enter patient id.");
            $("#txtPatientID").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtFirstName").val() == "" && $("#txtDeposite").val().trim() != "" && $("#txtPatientID").val().trim() != "") {
            $("#txtIdError").text("Please click on select button");
            flag = "true";
        }
        if (flag == "true") {
            return false;
        }
		
		var patientID = $("#txtPatientID").val();		
		var patientPrefix = patientID.substring(0, 3);
		patientID = patientID.replace ( /[^\d.]/g, '' ); 
		patientid = parseInt(patientID);
        var depositAmount = $("#txtDeposite").val();
		
		if($("#hdnFanId").val() != ""){
			var fanPatientId = $("#hdnFanId").val();
		}
		else{
			fanPatientId = patientid;
		}
        
		
		$(".close-modal").click();
		$('.modal-body').text("");
		$('#confirmCashModalLabel').text("deposite Payment");
		$('.modal-body').text("Are you sure that you want to deposite this?");
		$('#confirmCashModal').modal();
		$("#btnconfirm").unbind();
		$("#btnconfirm").on('click',function(){			
			$.ajax({ // ajax call for save content
				type: "POST",
				cache: "false",
				url: "controllers/admin/cash.php",
				datatype: "json",
				data: {
					patientID: patientid,
					prefix:patientPrefix,
					depositAmount,depositAmount,
					fanPatientId,fanPatientId,
					"operation": "save"
				},
				success: function(data) {
					if (data > "-1") {
						$("#txtAmount").val("");
						$("#txtAmount").val(data);
						$("#txtDeposite").val("");
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Your cash is deposited successfully!!!");
						$('#messagemyModal').modal();
                        clear();
						$("#txtPatientID").focus();


					}
					if (data == "") {
						$('.close-confirm').click();
						$('.modal-body').text("");
						$("#txtIdError").text("Please check your patient id");
						$("#txtPatientID").addClass("errorStyle");
						$("#txtPatientID").focus();
					}
					if (data == "-2") {
						$('.close-confirm').click();
						$('.modal-body').text("");
						$("#txtIdError").text("Please enter valid patient id");
						$("#txtPatientID").addClass("errorStyle");
						$("#txtPatientID").focus();
					}
				},
				error: function() {
					$('.close-confirm').click();					
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
					clear();
				}
			});
		});
    });
	$("#txtDeposite").keypress(function(e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { // for disable alphabates keys

            return false;
        }
    });
    // for remove validation style	
    $("input[type=text]").keyup(function() {

        if ($(this).val() != "") {
            $(this).next("span").text("");
            $(this).removeClass("errorStyle");
            //	$("label[for='"+$(this).attr("id")+"']").removeAttr("style");
        }
    });
	
	$("#iconSearch").click(function() {			// show popup for search patient
		$('.modal-body').text("");
        $('#cashAccountModalLabel').text("Search Patient");
		$("#hdnCash").val("1");           
        $('#cashAccountModalBody').load('views/admin/view_patients.html');
        $('#myCashAccountModal').modal();
    });

    $('#txtPatientID').keypress(function (e) {
        if (e.which == 13) {
            $("#btnSelect").click();
        }
    });
    getAmountPrefix();
    
    
});
/*Function to validate number*/
/* function validnumber(number) {
    floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
    return floatRegex.test(number);
} */

// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        window.location.href = "http://localhost/herp/";
    }
});

// clear functionality
function clear() {
    $("#txtPatientID").val("");
    $("#txtFirstName").val("");
    $("#txtLastName").val("");
    $("#txtFatherName").val("");
    $("#txtGender").val("");
    $("#val_DOB").val("");
    $("#txtMobile").val("");
    $("#txtIdError").text("");
    $("#txtPatientID").removeClass("errorStyle");
    $("#txtAmount").val("");
    $("#txtDeposite").val("");
    $("#txtDepositError").text("");
    $("#txtDeposite").removeClass("errorStyle");
    $("#simpleError").text("");
}
// getAmountPrefix function
function getAmountPrefix(){
    var postData = {
        "operation": "show"
    }
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/configuration.php",
        datatype: "json",
        data: postData,

        success: function(data) {
            if(data != "0" && data != ""){
                var parseData = jQuery.parseJSON(data);             
                amountPrefix = parseData[5].value;
                 $(".amountPrefix").text(amountPrefix);
            }
        },
        error: function() {             
            $('.modal-body').text("");
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();
        }            
    });
}
//# sourceURL = bed.js