/*
 * File Name    :   vision_assissment.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for save opearion	
 */
 var printArray = [];
$(document).ready(function() {
    loader();    

    // hide detail in consultant screen
    $(".tab-content .block-title").hide();
    $(".tab-content #consultantblock").hide();
    $(".tab-content #txtJoiningDate").hide();
    $(".tab-content #consultantDate").hide();
	
	$('#oldHistoryModalDetails select').attr("disabled", true); 
    $('#oldHistoryModalDetails input[type=text]').attr("disabled", true);
    $('#oldHistoryModalDetails textarea').attr("disabled","disabled");
    $('#oldHistoryModalDetails input[type="button"]').addClass("btnHide");
	$('#oldHistoryModalDetails span').hide()
	
	/* $('#oldHistoryModalDetails select').addClass("isSelect");

	var length = $('#oldHistoryModalDetails select.isSelect').length;
	if(length != ""){
		for(i=0;i<length;i++){
			var value =$('select option:selected').text();
			$('#oldHistoryModalDetails select').replaceWith("<label>"+value+"</label>")
		}
	} */
	
	 if ($('#thing').val() == 3) {
		var getVisitValue = $("#textvisitId").val();
		var visitPrefix = getVisitValue.substring(0, 3);
		getVisitValue = getVisitValue.replace ( /[^\d.]/g, '' ); 
		getVisitValue = parseInt(getVisitValue);
		getVisionAssessmentForPaitentVisit(getVisitValue,visitPrefix);
	} 
	if ($('#thing').val() == 4) {
		var getVisitValue = $("#oldHistoryVisitId").text();
		var visitPrefix = getVisitValue.substring(0, 3);
		getVisitValue = getVisitValue.replace ( /[^\d.]/g, '' ); 
		getVisitValue = parseInt(getVisitValue);
		getVisionAssessmentForPaitentVisit(getVisitValue,visitPrefix);
	} 
	if ($('#thing').val() == 5) {
		var getVisitValue = $("#oldHistoryVisitId").text();
		var visitPrefix = getVisitValue.substring(0, 3);
		getVisitValue = getVisitValue.replace ( /[^\d.]/g, '' ); 
		getVisitValue = parseInt(getVisitValue);
		getVisionAssessmentForPaitentVisit(getVisitValue,visitPrefix);
	}
	
    $("#btnReset").click(function() {
		if ($('#thing').val() == 3) {
			consultantClear();
		} 
		else{
			clearVision();
		}
        $("#textvisitId").focus();
    });
    $("#textvisitId").focus();

    //getting data of text box on click of button
    $("#btnSelect").click(function() {
		consultantClear();
		$("#textvisitId").removeAttr("style");
		$("#txtVisitError").text("");
		var getVisitValue = $("#textvisitId").val();
        var flag = "false";
        if ($("#textvisitId").val().trim() == "") {
            $("#txtVisitError").text("Please enter visit id");
            $("#textvisitId").addClass("errorStyle");
            $("#textvisitId").focus();
            flag = "true";
        }
        if(getVisitValue.length != 9){
			$("#textvisitId").focus();
			$("#txtVisitError").text("Please enter valid visit id");
			$("#textvisitId").addClass("errorStyle");
			flag = "true";
		}
        if (flag == "true") {
            return false;
        }
       
		var visitPrefix = getVisitValue.substring(0, 3);
		getVisitValue = getVisitValue.replace ( /[^\d.]/g, '' ); 
		getVisitValue = parseInt(getVisitValue);
		
		getVisionAssessmentForPaitentVisit(getVisitValue,visitPrefix);// load data

        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/vision_assessment.php",
            data: {
                "operation": "selectVisitInfo",
                "getVisit": getVisitValue,
                "visitPrefix": visitPrefix
            },
            success: function(data) {
                if (data != "0" && data != "2") {

                    var parseData = jQuery.parseJSON(data);
                    for (var i = 0; i < parseData.length; i++) {
						var patientId = parseData[i].patient_id;
						var patientPrefix = parseData[i].patient_prefix;
                        $("#user_reg_image").hide();
                        $("#imagetext").hide();
                        if (parseData[i].images_path != null) {
                            imagePath = "./upload_images/patient_dp/" + parseData[i].images_path;
                            $("#image-box").css({
                                "background-image": "url(" + imagePath + ")"
                            });
                        }
                        if (parseData[i].images_path == null || parseData[i].images_path == "") {
                            $("#image-box").css({
                                "background-image": "url(./img/default.jpg)"
                            });
                        }
                        $("#txtFirstName").val(parseData[i].first_name);
                        $("#txtLastName").val(parseData[i].last_name);
                        $("#txtMobile").val(parseData[i].mobile);
						if(parseData[i].gender == "M"){
							 $("#txtGender").val("Male");
						}
						else{
							$("#txtGender").val("female");
						}
                       

                        var getDOB = parseData[i].dob;
                        //convert time string to date
                        visitTime = parseData[i].visit_date;
                        joinTime = parseData[i].join_date;

                        var date1 = new Date(visitTime * 1000); // date for visit date
                        var date = new Date(joinTime * 1000); // date for joining date
                    } 
					var patientIdLength = patientId.length;
					for (var i=0;i<6-patientIdLength;i++) {
						patientId = "0"+patientId;
					}
					patientId = patientPrefix+patientId;
					$("#txtPatientID").val(patientId);
                    var get_date1 = date1.getDate();
                    var year1 = date1.getFullYear();
                    var month1 = date1.getMonth();
                    $("#txtVisitDate").val(get_date1 + "/" + month1 + "/" + year1);

                    var get_date = date.getDate();
                    var year = date.getFullYear();
                    var month = date.getMonth();
                    $("#txtJoiningDate").val(get_date + "/" + month + "/" + year);

                    var new_age = getAge(getDOB);
                    var split = new_age.split(' ');
                    var age_years = split[0];
                    var age_month = split[1];
                    var age_day = split[2];
                    $("#txt_year").val(age_years);
                    $("#txt_month").val(age_month);
                    $("#txt_day").val(age_day);
                    /* $("#txtVisit").attr("disabled", "disabled"); */
                }
                if (data == "0" || data == "2") {
                    
                    $("#textvisitId").focus();
					clearVision();
					$("#txtVisitError").text("Please enter valid visit id");
					$("#textvisitId").addClass("errorStyle");
                }
            },
            error: function() {
                $('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
            }
        });
    });
	
	/*Function to get data from db*/
	function getVisionAssessmentForPaitentVisit(getVisitValue,visitPrefix) {
		var postData = {
			"operation":"getData",
			"visitId" : getVisitValue,
			"visitPrefix" : visitPrefix
		}
		
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/vision_assessment.php",
			data: postData,
			success: function(data) { 
				if(data != "0"){         
					var parseData = jQuery.parseJSON(data);//parse JSON data
			
					for (var i=0;i<parseData.length;i++) {
						/*Setting value to text box*/
						$("#selEyeStatus").val(parseData[i].eye_status);
						$("#selVisualAcuityRight").val(parseData[i].visualacuityright);
						$("#selVisualAcuityLeft").val(parseData[i].visualacuityleft);
						$("#selVisualAcuityExtRight").val(parseData[i].visualacuityextright);
						$("#selVisualAcuityExtLeft").val(parseData[i].visualacuityextleft);
						$("#selHandMovementRight").val(parseData[i].handmovementright);
						$("#selHandMovementLeft").val(parseData[i].handmovementleft);
						$("#selPreceptionRight").val(parseData[i].preceptionright);
						$("#selPreceptionLeft").val(parseData[i].preceptionleft);
						$("#txtComment").val(parseData[i].Comment);
						$("#txtNotes").val(parseData[i].notes);
					}
				}
			},
			error:function(){
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	}
    // for search icon for load popup and view visit screen
    $("#iconSearch").click(function() {
        $('#assessmentModalLabel').text("Search Visit");
        $('#assessmentModalBody').load('views/admin/view_visit.html', function() {
            $("#myAssessmentModal #txtFirstName").focus();
            $("#thing").val("10");
        });
        $('#myAssessmentModal').modal();

    });

    //print functionality
    debugger;
        

    // save details with validation
    $("#btnSave").click(function() {

        var flag = "false";

        if ($("#selPreceptionLeft").val() == "") {
            $("#selPreceptionLeftError").text("Please select perception left");
            $("#selPreceptionLeft").addClass("errorStyle");
            $("#selPreceptionLeft").focus();
            flag = "true";
        }
        if ($("#selPreceptionRight").val() == "") {
            $("#selPreceptionRightError").text("Please select perception right");
            $("#selPreceptionRight").addClass("errorStyle");
            $("#selPreceptionRight").focus();
            flag = "true";
        }
        if ($("#selHandMovementLeft").val() == "") {
            $("#selHandMovementLeftError").text("Please select hand movement left");
            $("#selHandMovementLeft").addClass("errorStyle");
            $("#selHandMovementLeft").focus();
            flag = "true";
        }
        if ($("#selHandMovementRight").val() == "") {
            $("#selHandMovementRightError").text("Please select hand movement right");
            $("#selHandMovementRight").addClass("errorStyle");
            $("#selHandMovementRight").focus();
            flag = "true";
        }
        if ($("#selVisualAcuityExtLeft").val() == "") {
            $("#selVisualAcuityExtLeftError").text("Please select visual acuity ext left");
            $("#selVisualAcuityExtLeft").addClass("errorStyle");
            $("#selVisualAcuityExtLeft").focus();
            flag = "true";
        }
        if ($("#selVisualAcuityExtRight").val() == "") {
            $("#selVisualAcuityExtRightError").text("Please select visual acuity ext right");
            $("#selVisualAcuityExtRight").addClass("errorStyle");
            $("#selVisualAcuityExtRight").focus();
            flag = "true";
        }
        if ($("#selVisualAcuityLeft").val() == "") {
            $("#selVisualAcuityLeftError").text("Please select visual acuity left");
            $("#selVisualAcuityLeft").addClass("errorStyle");
            $("#selVisualAcuityLeft").focus();
            flag = "true";
        }
        if ($("#selVisualAcuityRight").val() == "") {
            $("#selVisualAcuityRightError").text("Please select visual acuity right");
            $("#selVisualAcuityRight").addClass("errorStyle");
            $("#selVisualAcuityRight").focus();
            flag = "true";
        }
        if ($("#selEyeStatus").val() == "") {
            $("#selEyeStatusError").text("Please select eye status");
            $("#selEyeStatus").addClass("errorStyle");
            $("#selEyeStatus").focus();
            flag = "true";
        }
        if ($("#textvisitId").val().trim() == "") {

            $("#txtVisitError").text("Please enter visit id");
            $("#textvisitId").addClass("errorStyle");
            $("#textvisitId").focus();
            flag = "true";
        }

        if (flag == "true") {
            return false;
        }

		var patientID = parseInt($("#txtPatientID").val().trim().replace ( /[^\d.]/g, '' ));
		var getVisitValue = parseInt($("#textvisitId").val().trim().replace ( /[^\d.]/g, '' ));
        var EyeStatus = $("#selEyeStatus").val();
        var VisualAcuityRight = $("#selVisualAcuityRight").val();
        var VisualAcuityLeft = $("#selVisualAcuityLeft").val();
        var VisualAcuityExtRight = $("#selVisualAcuityExtRight").val();
        var VisualAcuityExtLeft = $("#selVisualAcuityExtLeft").val();
        var HandMovementRight = $("#selHandMovementRight").val();
        var HandMovementLeft = $("#selHandMovementLeft").val();
        var PreceptionRight = $("#selPreceptionRight").val();
        var PreceptionLeft = $("#selPreceptionLeft").val();
        var Comment = $("#txtComment").val();
        var Notes = $("#txtNotes").val();

        printArray.push($("#textvisitId").val().trim(),$("#txtPatientID").val().trim(),todayDate(),$("#txtFirstName").val()+' '+$("#txtLastName").val(),$("#selEyeStatus option:selected").text(),$("#selVisualAcuityRight option:selected").text(),$("#selVisualAcuityLeft option:selected").text(),$("#selVisualAcuityExtRight option:selected").text(),$("#selVisualAcuityExtLeft option:selected").text(),$("#selHandMovementRight option:selected").text(),$("#selHandMovementLeft option:selected").text(),$("#selPreceptionRight option:selected").text(),$("#selPreceptionLeft option:selected").text(),Comment,Notes);

        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
            $.ajax({
                type: "POST",
                cache: "false",
                url: "controllers/admin/vision_assessment.php",

                data: {
                    "operation": "saveVisionInfo",
                    "VisitID": getVisitValue,
                    "patientId": patientID,
                    "EyeStatus": EyeStatus,
                    "VisualAcuityRight": VisualAcuityRight,
                    "VisualAcuityLeft": VisualAcuityLeft,
                    "VisualAcuityExtRight": VisualAcuityExtRight,
                    "VisualAcuityExtLeft": VisualAcuityExtLeft,
                    "HandMovementRight": HandMovementRight,
                    "HandMovementLeft": HandMovementLeft,
                    "PreceptionRight": PreceptionRight,
                    "PreceptionLeft": PreceptionLeft,
                    "Comment": Comment,
                    "Notes": Notes
                },
                success: function(data) {
                    if (data == "1") {
    					if($("#btnSave").val() == "Update"){
    						$('#printMessageBody').text("Vision assessment updated successfully!!!");
    					}
    					else{
    						$('.modal-body').text("Vision assessment saved successfully!!!");
    					}
                        $('#printMyModal').modal();
    					if ($('#thing').val() != 3) {
    						clearVision();
    					}                       
                    }
                    if (data == "0") {
                        if ($('#thing').val() == 3) {
    						consultantClear();
    					} 
    					else{
    						clearVision();
    					}
                        $("#txtVisit").focus();
                    }
                },
                error: function() {
                    $('#messageMyModalLabel').text("Error");
    				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
    				$('#messagemyModal').modal();
                }
            });
        });   
    });

    // keyup functionality
    $("#textvisitId").keyup(function() {
        if ($("#textvisitId").val().trim() != "") {
            $("#txtVisitError").text("");
            $("#textvisitId").removeClass("errorStyle");
        }
    });
    $("#selEyeStatus").change(function() {
        if ($("#selEyeStatus").val() != "") {
            $("#selEyeStatusError").text("");
            $("#selEyeStatus").removeClass("errorStyle");
        }
    });
    $("#selVisualAcuityRight").change(function() {
        if ($("#selVisualAcuityRight").val() != "") {
            $("#selVisualAcuityRightError").text("");
            $("#selVisualAcuityRight").removeClass("errorStyle");
        }
    });
    $("#selVisualAcuityLeft").change(function() {
        if ($("#selVisualAcuityLeft").val() != "") {
            $("#selVisualAcuityLeftError").text("");
            $("#selVisualAcuityLeft").removeClass("errorStyle");
        }
    });
    $("#selVisualAcuityExtRight").change(function() {
        if ($("#selVisualAcuityExtRight").val() != "") {
            $("#selVisualAcuityExtRightError").text("");
            $("#selVisualAcuityExtRight").removeClass("errorStyle");
        }
    });
    $("#selVisualAcuityExtLeft").change(function() {
        if ($("#selVisualAcuityExtLeft").val() != "") {
            $("#selVisualAcuityExtLeftError").text("");
            $("#selVisualAcuityExtLeft").removeClass("errorStyle");
        }
    });
    $("#selHandMovementRight").change(function() {
        if ($("#selHandMovementRight").val() != "") {
            $("#selHandMovementRightError").text("");
            $("#selHandMovementRight").removeClass("errorStyle");
        }
    });
    $("#selHandMovementLeft").change(function() {
        if ($("#selHandMovementLeft").val() != "") {
            $("#selHandMovementLeftError").text("");
            $("#selHandMovementLeft").removeClass("errorStyle");
        }
    });
    $("#selPreceptionRight").change(function() {
        if ($("#selPreceptionRight").val() != "") {
            $("#selPreceptionRightError").text("");
            $("#selPreceptionRight").removeClass("errorStyle");
        }
    });
    $("#selPreceptionLeft").change(function() {
        if ($("#selPreceptionLeft").val() != "") {
            $("#selPreceptionLeftError").text("");
            $("#selPreceptionLeft").removeClass("errorStyle");
        }
    });

    $('#textvisitId').keypress(function (e) {
        if (e.which == 13) {
            $("#btnSelect").click();
        }
    });

    if ($('#consultantblock').attr('style') != undefined) {
        $(".btnHolder").append('<input type="button" value="Print" class="btn_reset" id="btnPrint">');
        $("#printModalFooter").html('<input type="button" class="btn" data-dismiss="modal" aria-hidden="true" value="OK" id="ok">');
        printDetails();
    }
    else{
        $("#printModalFooter").html('<input type="button" value="Print" class="btn_reset" id="btnPrint">');
        printDetails();
    }

});

//function to calculate age
function getAge(dateString) {
    var now = new Date();
    var today = new Date(now.getYear(), now.getMonth(), now.getDate());

    var yearNow = now.getYear();
    var monthNow = now.getMonth();
    var dateNow = now.getDate();

    var dob = new Date(dateString.substring(0, 4), dateString.substring(5, 7) - 1, dateString.substring(8, 10));

    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();
    var age = {};
    var ageString = "";
    var yearString = "";
    var monthString = "";
    var dayString = "";


    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    age = {
        years: yearAge,
        months: monthAge,
        days: dateAge
    };

    if (age.years > 1) yearString = " years";
    else yearString = " year";
    if (age.months > 1) monthString = " months";
    else monthString = " month";
    if (age.days > 1) dayString = " days";
    else dayString = " day";


    if ((age.years > 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else ageString = "Oops! Could not calculate age!";

    return ageString;
}

// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        $('.close').click();
    }
});

// clear function
function clearVision() {
    $("#textvisitId").val("");
    $("#txtPatientID").val("");
    $("#txtFirstName").val("");
    $("#txtLastName").val("");
    $("#txtMobile").val("");
    $("#txt_year").val("");
    $("#txt_month").val("");
    $("#txt_day").val("");
    $("#txtGender").val("");
    $("#txtVisitDate").val("");
    $("#txtJoiningDate").val("");
    $("#selEyeStatus").val("");
    $("#selVisualAcuityRight").val("");
    $("#selVisualAcuityLeft").val("");
    $("#selVisualAcuityExtRight").val("");
    $("#selVisualAcuityExtLeft").val("");
    $("#selHandMovementRight").val("");
    $("#selHandMovementLeft").val("");
    $("#selPreceptionRight").val("");
    $("#selPreceptionLeft").val("");
    $("#txtComment").val("");
    $("#txtNotes").val("");

    $("#textvisitId").removeClass("errorStyle");
    $("#selEyeStatus").removeClass("errorStyle");
    $("#selVisualAcuityRight").removeClass("errorStyle");
    $("#selVisualAcuityLeft").removeClass("errorStyle");
    $("#selVisualAcuityExtRight").removeClass("errorStyle");
    $("#selVisualAcuityExtLeft").removeClass("errorStyle");
    $("#selHandMovementRight").removeClass("errorStyle");
    $("#selHandMovementLeft").removeClass("errorStyle");
    $("#selPreceptionRight").removeClass("errorStyle");
    $("#selPreceptionLeft").removeClass("errorStyle");

    $("#txtVisitError").text("");
    $("#selEyeStatusError").text("");
    $("#selVisualAcuityRightError").text("");
    $("#selVisualAcuityLeftError").text("");
    $("#selVisualAcuityExtRightError").text("");
    $("#selVisualAcuityExtLeftError").text("");
    $("#selHandMovementRightError").text("");
    $("#selHandMovementLeftError").text("");
    $("#selPreceptionRightError").text("");
    $("#selPreceptionLeftError").text("");

    $("#image-box").attr("src", './img/default.jpg');
};

function consultantClear(){
    $("#selEyeStatus").val("");
    $("#selVisualAcuityRight").val("");
    $("#selVisualAcuityLeft").val("");
    $("#selVisualAcuityExtRight").val("");
    $("#selVisualAcuityExtLeft").val("");
    $("#selHandMovementRight").val("");
    $("#selHandMovementLeft").val("");
    $("#selPreceptionRight").val("");
    $("#selPreceptionLeft").val("");
    $("#txtComment").val("");
    $("#txtNotes").val("");


    $("#selEyeStatus").removeClass("errorStyle");
    $("#selVisualAcuityRight").removeClass("errorStyle");
    $("#selVisualAcuityLeft").removeClass("errorStyle");
    $("#selVisualAcuityExtRight").removeClass("errorStyle");
    $("#selVisualAcuityExtLeft").removeClass("errorStyle");
    $("#selHandMovementRight").removeClass("errorStyle");
    $("#selHandMovementLeft").removeClass("errorStyle");
    $("#selPreceptionRight").removeClass("errorStyle");
    $("#selPreceptionLeft").removeClass("errorStyle");

    $("#selEyeStatusError").text("");
    $("#selVisualAcuityRightError").text("");
    $("#selVisualAcuityLeftError").text("");
    $("#selVisualAcuityExtRightError").text("");
    $("#selVisualAcuityExtLeftError").text("");
    $("#selHandMovementRightError").text("");
    $("#selHandMovementLeftError").text("");
    $("#selPreceptionRightError").text("");
    $("#selPreceptionLeftError").text("");
}
function printDetails(){
    $("#btnPrint").on('click',function(){
        /*hospital information through global varibale in main.js*/
        $("#lblPrintEmail").text(hospitalEmail);
        $("#lblPrintPhone").text(hospitalPhoneNo);
        $("#lblPrintAddress").text(hospitalAddress);
        if (hospitalLogo != "null") {
            imagePath = "./images/" + hospitalLogo;
            $("#printLogo").attr("src",imagePath);
        }
        $("#printHospitalName").text(hospitalName);
        if ($('#consultantblock').attr('style') != undefined) {
            $("#lblPrintVisitId").text($("#textvisitId").val());
            $("#lblPrintPatientId").text($("#txtPatientID").val());
            $("#lblPrintDate").text(todayDate());
            $("#lblPrintPatientName").text($("#txtpatientName").val());
            $("#lblPrintEyeStaus").text($("#selEyeStatus option:selected").text());
            $("#lblPrintRightVisualAcuity").text($("#selVisualAcuityRight option:selected").text());
            $("#lblPrintLeftVisualAcuity").text($("#selVisualAcuityLeft option:selected").text());
            $("#lblPrintRightVisualAcuityExt").text($("#selVisualAcuityExtRight option:selected").text());
            $("#lblPrintLeftVisualAcuityExt").text($("#selVisualAcuityExtLeft option:selected").text());
            $("#lblPrintRightHandMovement").text($("#selHandMovementRight option:selected").text());
            $("#lblPrintLeftHandMovement").text($("#selHandMovementLeft option:selected").text());
            $("#lblPrintRightPOL").text($("#selPreceptionRight option:selected").text());
            $("#lblPrintLeftPol").text($("#selPreceptionLeft option:selected").text());
            $("#lblPrintClinicalComment").text($("#txtComment").val());
            $("#lblPrintNotes").text($("#txtNotes").val());
        }
        else{
            $("#lblPrintVisitId").text(printArray[0]);
            $("#lblPrintPatientId").text(printArray[1]);
            $("#lblPrintDate").text(printArray[2]);
            $("#lblPrintPatientName").text(printArray[3]);
            $("#lblPrintEyeStaus").text(printArray[4]);
            $("#lblPrintRightVisualAcuity").text(printArray[5]);
            $("#lblPrintLeftVisualAcuity").text(printArray[6]);
            $("#lblPrintRightVisualAcuityExt").text(printArray[7]);
            $("#lblPrintLeftVisualAcuityExt").text(printArray[8]);
            $("#lblPrintRightHandMovement").text(printArray[9]);
            $("#lblPrintLeftHandMovement").text(printArray[10]);
            $("#lblPrintRightPOL").text(printArray[11]);
            $("#lblPrintLeftPol").text(printArray[12]);
            $("#lblPrintClinicalComment").text(printArray[13]);
            $("#lblPrintNotes").text(printArray[14]);
        }
            

        $.each($('label[id^="lblPrint"]'),function(i,v){
            if ($(v).text() == '' || $(v).text() == '--select--') {
                $(v).text('N/A');
            }
        });

        window.print();
    });
}