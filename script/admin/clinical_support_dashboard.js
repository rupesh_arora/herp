$(document).ready(function() {
	debugger;	
	google.charts.setOnLoadCallback(drawChart);
});
function drawChart() {
	var chartWidth  = $("#page-content").width() -25;
	var serviceRecord = [['Task', 'Hours per Day']];
	var triageRecord = [['Record-Group', 'Count']];
	var consultantNotReq = [['Record-Group', 'Count']];
	var consultantReq = [['Record-Group', 'Count']];
	var pharmacyRecord = [['Record-Group', 'Count']];
	var visitsToDrRecord = [['Record-Group', 'Count']];

	var postData = {
		"operation" : "showChartDataForClinicalSupport"
	}
	$.ajax({     
		type: "POST",
		cache: false,
		url: "controllers/admin/patient_mangament_dashboard.php",
		data: postData,
		success: function(data) { 
			if(data != null && data != ""){
				var parseData =  JSON.parse(data);		
				if (parseData[0] ==''){
					$('#messagemyModal').modal();
					$('#messageMyModalLabel').text("Alert");
					$('.modal-body').text("No data exist!!!");
					return false;
				}
				serviceRecord = [['Task', 'Hours per Day']];
				triageRecord = [['Record-Group', 'Count']];
				consultantNotReq = [['Record-Group', 'Count']];
				consultantReq = [['Record-Group', 'Count']];
				pharmacyRecord = [['Record-Group', 'Count']];
				visitsToDrRecord = [['Record-Group', 'Count']];

				$.each(parseData,function(index,value){

					/*Check for null data*/
					if (parseData[index].length > 0) {

						for(var i=0;i<parseData[index].length;i++){
							if (index == 0) {
								smallArrayService = [];
								smallArrayService.push(parseData[index][i].service_name);
								smallArrayService.push(parseInt(parseData[index][i].Visit));
								serviceRecord.push(smallArrayService);	
							}
							else if(index == 1) {
								smallArrayTriage = [];
								smallArrayTriage.push(parseData[index][i].triage_status);
								smallArrayTriage.push(parseInt(parseData[index][i].Visit));
								triageRecord.push(smallArrayTriage);	
							}
							else if (index == 2) {
								$.each(parseData[index][i],function(i,v){
									consultantNotReq.push([i,parseInt(v)]);
								});
							}
							else if (index == 3) {
								$.each(parseData[index][i],function(i,v){
									consultantReq.push([i,parseInt(v)]);
								});
							}
							else if (index == 4) {
								smallArrayPharmacy = [];
								smallArrayPharmacy.push(parseData[index][i].dispense_status);
								smallArrayPharmacy.push(parseInt(parseData[index][i].Pharmacy));
								pharmacyRecord.push(smallArrayPharmacy);
							}
							else{
								$.each(parseData[index][i],function(i,v){
									visitsToDrRecord.push([i,parseInt(v)]);
								});
							}
						}
					}												
				});

	            /*google chart intialization for patientRecord group visit*/
	            var serviceRecordGroup = google.visualization.arrayToDataTable(serviceRecord);

	            var triageRecordGroup = google.visualization.arrayToDataTable(triageRecord);

	            var consultantNotReqData = google.visualization.arrayToDataTable(consultantNotReq);

	            var consultantReqData = google.visualization.arrayToDataTable(consultantReq);

	            var pharmacyRecordGroup = google.visualization.arrayToDataTable(pharmacyRecord);

	            var visitsToDrRecordGroup = google.visualization.arrayToDataTable(visitsToDrRecord);

			    var options = {
			      title: 'Visits analysis by service','width':1080,'height':600,is3D:true
			    };

			    var options2 = {
			      title: 'Patient with triage record','width':1080,'height':600,is3D:true
			    };

			    var options3 = {
			      title: 'Request not reviewd by consultant ','width':1080,'height':600,is3D:true
			    };

			    var options4 = {
			      title: 'Request reviewd by consultant','width':1080,'height':600,is3D:true
			    };

			    var options5 = {
			      title: 'Prescription sent by consultant','width':1080,'height':600,is3D:true
			    };

			    var options6 = {
			      title: 'Patient in Queued from OPD Booking','width':1080,'height':600,is3D:true
			    };

			    if (parseData[0].length <= 0) {
					$("#chart_div").hide();
				}
				else{
					var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
	        		chart.draw(serviceRecordGroup, options); 
				}
	        		 
				if (parseData[1].length <= 0) {
					$("#chart_div2").hide();
				}
				else{
					var chart2 = new google.visualization.PieChart(document.getElementById('chart_div_2'));
	        		chart2.draw(triageRecordGroup, options2);
				}

				var chart3 = new google.visualization.PieChart(document.getElementById('chart_div_3'));
	        	chart3.draw(consultantNotReqData, options3);

	        	var chart4 = new google.visualization.PieChart(document.getElementById('chart_div_4'));
	        	chart4.draw(consultantReqData, options4);

	        	if (parseData[4].length <= 0) {
					$("#chart_div4").hide();
				}
				else{
					var chart5 = new google.visualization.PieChart(document.getElementById('chart_div_5'));
	        		chart5.draw(pharmacyRecordGroup, options5);
				}

				if (parseData[4].length <= 0) {
					$("#chart_div4").hide();
				}
				else{
					var chart6 = new google.visualization.PieChart(document.getElementById('chart_div_6'));
	        		chart6.draw(visitsToDrRecordGroup, options6);
				}       		      		
			}
		}			
	});
}	