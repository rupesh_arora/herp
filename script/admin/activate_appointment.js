var activateTable;
$("document").ready(function(){
	debugger;
	activateTable = $('#tblActivateAppointment').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",  
		"fnDrawCallback" : function() {
		},
	});

	 /* search functioanlty for patient details on search button*/
    $("#btnSearch").click(function() {

		var flag = "false";
		var firstName = $("#txtFirstName").val().trim();
		var lastName = $("#txtLastName").val().trim();
		var fromDate = $("#txtFromDate").val().trim();
		var toDate = $("#txtToDate").val().trim();
		var patientId = $("#txtPatientId").val().trim();
		var appointmentId = $("#txtAppointmentId").val().trim();
		if($("#txtPatientId").val() != ""){
			if(patientId.length != 9){					
				flag = "true";
			}
			else{
				var patientId = $("#txtPatientId").val();
				var patientPrefix = patientId.substring(0, 3);
				patientId = patientId.replace ( /[^\d.]/g, '' ); 
				patientId = parseInt(patientId);
			}
		}
		else{
			patientPrefix = "";
		}
		if (flag == "true") {
			return false;
		}	

		var postData = {
			"operation": "search",
			"firstName": firstName,
			"lastName": lastName,
			"fromDate": fromDate,
			"toDate": toDate,
			"patientId": patientId,
			"patientPrefix": patientPrefix,
			"appointmentId": appointmentId
		}

		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/activate_appointment.php",
			datatype: "json",
			data: postData,

			success: function(dataSet) {
				dataSet = JSON.parse(dataSet);
				//var dataSet = jQuery.parseJSON(data);
				activateTable.fnClearTable();
				activateTable.fnDestroy();
				activateTable = $('#tblActivateAppointment').DataTable({
					"sPaginationType": "full_numbers",
					"bAutoWidth" : false,
					"fnDrawCallback": function(oSettings) {

						$(".save").click(function(){
							var data=$(this).parents('tr')[0];
							var mData = activateTable.fnGetData(data);
							if (null != mData) // null if we clicked on title row
		                    {
		                        var id = mData["id"];
		                        var appointmentId = mData["appointment_id"];
		                        var visitTypeId = mData["visit_type_id"];

		                        var postData = {
									"operation": "save",
									"appointmentId": appointmentId,
									"id": id,
									"visitTypeId": visitTypeId
								}

		                        $.ajax({
									type: "POST",
									cache: false,
									url: "controllers/admin/activate_appointment.php",
									datatype: "json",
									data: postData,

									success: function(data){										
				                        $('#messageMyModalLabel').text("Success");
				                        $('#messageBody').html("Appointment booked successfully!!!" + "<br>" + "Visit Id = " + data);
				                        $('#messagemyModal').modal();
				                        clear();
									},
									error:function(){

									}
								});

		                    }
						});
					
					},
					"aaData": dataSet,
					"aoColumns": [{
					"mData":  function (o) { 	
						var patientId = o["id"];
						var patientPrefix = o["prefix"];
							
						var patientIdLength = patientId.length;
						for (var i=0;i<6-patientIdLength;i++) {
							patientId = "0"+patientId;
						}
						patientid = patientPrefix+patientId;
						return patientid;	}
					}, {
						"mData": "name"
					}, {
						"mData": "cunsultant_name"
					}, {
						"mData": "start_date_time"
					}, {
						"mData": "end_date_time"
					}, {
						"mData": function(o) {
							var data = o;
							var id = data["id"];
							return "<input type='button' class='btn_save save' value='Activate'>";
						}
					}, ],
					aoColumnDefs: [{
						'bSortable': false,
						'aTargets': [5]
					}]
				});

			},
			error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
    });
    //$('#mySearchModal #viewPatientTable').hide();
    //Code of reset on search modal
    $("#btnReset").click(function() {
        clear();
    });

	 //Datepicker function 
    $("#txtFromDate").datepicker({ changeYear: true,changeMonth: true,autoclose: true}) .on('changeDate', function(selected){
        startDate = new Date(selected.date.valueOf());
        $('#txtToDate').datepicker('setStartDate', startDate);
    }); 
    $('#txtToDate').datepicker({
        changeYear: true,
        changeMonth: true,
        autoclose: true
    });

});
function clear(){
	$("#txtFirstName").val("");
    $("#txtLastName").val("");
    $("#txtFromDate").val("");
    $("#txtToDate").val("");
    $("#txtPatientId").val("");
    $("#txtAppointmentId").val("");
    activateTable.fnClearTable();
    $("#txtFirstName").focus();
}

