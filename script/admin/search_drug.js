$(document).ready(function(){
loader();
debugger;
	var otable;
	otable = $('#searchDrugTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [6]
        }]
	} );

	$(".amountPrefix").text(amountPrefix);

	$("#myDrugModal #btnReset").click(function(){			
		otable.fnClearTable();
		$("#myDrugModal #txtDrugId").val("");
		$("#myDrugModal #txtDrugName").val("");
		$("#myDrugModal #txtDrugCode").val("");
	});
	
	$("#myDrugModal #btnSearch").click(function(){		
		
		var drugName = $("#myDrugModal #txtDrugName").val();
		var drugCode = $("#myDrugModal #txtDrugCode").val();
		
		var postData = {
			"operation":"search",
			"drugName":drugName,
			"drugCode":drugCode
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/search_drug.php",
			datatype:"json",
			data: postData,
			
			success: function(dataSet) {
				dataSet = JSON.parse(dataSet);
				//var dataSet = jQuery.parseJSON(data);
				var paymentMode;
				otable.fnClearTable();
				otable.fnDestroy();
				otable = $('#myDrugModal #searchDrugTable').DataTable( {
					"sPaginationType":"full_numbers",
					"fnDrawCallback": function ( oSettings ) {
						
						$('#myDrugModal #searchDrugTable tbody tr').on( 'dblclick', function () {
	
							if ( $(this).hasClass('selected') ) {
							  $(this).removeClass('selected');
							}
							else {
							  otable.$('tr.selected').removeClass('selected');
							   $(this).addClass('selected');
							}
							
							var mData = otable.fnGetData(this); // get datarow
							if (null != mData)  // null if we clicked on title row
							{
								//now aData[0] - 1st column(count_id), aData[1] -2nd, etc. 								
								
								$('#hidDrugId').val(mData["id"]);

								/*Now describing some var that used in  drug dosage screen*/
								$('#txtDrugId').val(mData["id"]);
								$('#txtAlternateName').val(mData["alternate_name"]);
								$('#txtDrugName').val(mData["name"]);
								$('#txtAvailableStocks').val(mData["quantity"]);
								$('#txtUnitMeasure').val(mData["unit_measure"]);
								$('#txtUnitPrice').val(mData["price"]);
								if($('#thing').val() != ""){
									$("#btnSelect").click();
								}
								
								if (paymentMode != undefined) {

									if (paymentMode.toLowerCase() == "insurance") {
		                				getRevisedCost(drugId);
						                checkExclusion(drugId,insuranceCompanyId,insurancePlanId,insurancePlanNameid,"Pharmacy");
						            }
						            else{
						                checkTestExcluded = '';
						            }
								}
								
								var id = $('#hidDrugId').val();
								$.ajax({
									type: "POST",
									cache: "false",
									url: "controllers/admin/search_drug.php",
									data :{            
										"operation" : "searchId",
										"id" : id
									},
									success: function(data) {	
										if(data != "0" && data != ""){
												
											var parseData = jQuery.parseJSON(data);
		
											for (var i=0;i<parseData.length;i++) {
												$("#hidDrugId").val(parseData[i].drug_name_id);
												$("#txtDrug").val(parseData[i].name);    					
												$("#txtQuantity").val(parseData[i].quantity);								
												$("#txtUnitVol").val(parseData[i].unit_v);								
												$("#selUnit").val(parseData[i].unit_id);								
												$("#txtManufactureName").val(parseData[i].manufacture_name);								
												$("#txtManufactureDate").val(parseData[i].manufacture_date);								
												$("#txtExpiryDate").val(parseData[i].expiry_date);
												$("#txtUnitMeasure").val(parseData[i].unit_measure);
												if(parseData[i].composition == "") {
													$("#lblComposition").text("N/A");
												}
												else {
													$("#lblComposition").text(parseData[i].composition);
												}
												if(parseData[i].drug_code == "") {
													$("#lblDrugCode").text("N/A");
												}
												else {
													$("#lblDrugCode").text(parseData[i].drug_code);
												}
																					
												$("#selectedRow").val(parseData[i].id);
												$("#btnSave").val("Update");
											}
											
										}		
										else{																				
											$('#txtDrug').val(mData["name"]);								
											$('#lblDrugCode').text(mData["drug_code"]);								
											$('#lblComposition').text(mData["composition"]);
											$('#txtAvailableStocks').text(mData["quantity"]);
											$("#txtUnitMeasure").text(mData["unit_measure"]);
											$("#txtDrugError").text("");
											$("#txtDrug").removeAttr("style");
										}				
									},
									error:function()
									{
										alert('error');
										  
									}
								});							
							}
							$(".close").click();
										
						} );
						
						$("#myDrugModal .drugDetails").on('click',function(){
							var id = $(this).attr('data-id');
							var postData = {
								"operation":"searchDrug",
								"id":id
							}
							
							$.ajax({					
								type: "POST",
								cache: false,
								url: "controllers/admin/search_drug.php",
								datatype:"json",
								data: postData,
								
								success: function(data) {
									if(data != "0" && data != ""){
										var parseData = jQuery.parseJSON(data);
														
										for (var i=0;i<parseData.length;i++) {
											$("#myDrugModal #lblDrugName").text(parseData[i].name);
											$("#myDrugModal #lblDrugCode").text(parseData[i].drug_code);											
											if (parseData[i].alternate_name =='') {
												$("#myDrugModal #lblAlterNateName").text("N/A");
											}
											else{
												$("#myDrugModal #lblAlterNateName").text(parseData[i].alternate_name);
											}
											$("#myDrugModal #lblCategory").text(parseData[i].category);
											$("#myDrugModal #lblUnit").text(parseData[i].unit_abbr);
											$("#myDrugModal #lblCost").text(parseData[i].cost);
											$("#myDrugModal #lblPrice").text(parseData[i].price);											
											$("#myDrugModal #lblAltDrugName").text(parseData[i].alternate_drug_name);
											if (parseData[i].alternate_drug_name =='' || parseData[i].alternate_drug_name ==null) {
												$("#myDrugModal #lblAltDrugName").text("N/A");
											}
											if (parseData[i].composition=='') {
												$("#myDrugModal #lblComposition").text("N/A");
											}
											else {
												$("#myDrugModal #lblComposition").text(parseData[i].composition);																				
											}											
										} 
										$("#myDrugModal #searchDrug").hide();
										$("#myDrugModal #viewDrugDetail").show();										
									}
								},
								error:function() {
									alert('error');
								}
							});
							
						}); 
					},
					"aaData": dataSet,					
					"aoColumns": [
						/* { "mData": "drug_id" },  */						
						{ "mData": "name" }, 
						{ "mData": "drug_code" },
						{ "mData": "category" },
						{ "mData": "unit_abbr" },
						{ "mData": "cost" },
						{ "mData": "price" },
						{
						  "mData": function (o) { 				
							var data = o;
							var id = data["id"];	
							return "<i class='fa fa-eye drugDetails' id='btnView' data-id="+id+" style='font-size: 16px; margin-left: 20px; cursor:pointer;' title='view' value='view'"+					 
							   " style='font-size: 16px; cursor:pointer;'></i>";								
							}
						},						
					],
					"aoColumnDefs": [{
			            'bSortable': false,
			            'aTargets': [6]
			        }]
				} );				
				
			},
			error:function() {
				alert('error');
			}
		});
		$("#myDrugModal #btnBackToList").click(function(){
			$("#myDrugModal #searchDrug").show();
			$("#myDrugModal #viewDrugDetail").hide();
		});
	});
	
});

//# sourceURL = xyz.js