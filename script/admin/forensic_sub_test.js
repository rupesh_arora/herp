$(document).ready(function() {
	var otable;		
	loader();

	//
	//Calling the function for loading the lab type and test 
	//	
	bindTest();
	bindUnitMeasure();	
	
	$("#advanced-wizard").hide();
	 $("#forensicSubTestList").css({
            "background": "#fff"
        });
		$("#tabForensicSubTestList").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
    $("#tabAddForensicSubTest").click(function() {
        $("#advanced-wizard").show();
        $(".blackborder").hide();
        //so focus on this text box when open this div
        $("#txtSubForensicTestName").focus();
		clear();
        $("#tabAddForensicSubTest").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tabForensicSubTestList").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#forensicSubTestList").css({
            "background": "#fff"
        });		
		bindTest();
		bindUnitMeasure();
    });
    $("#tabForensicSubTestList").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
        $("#advanced-wizard").hide();
        $(".blackborder").show();
		$("#forensicSubTable").dataTable().fnReloadAjax();
        $("#tabAddForensicSubTest").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#tabForensicSubTestList").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#forensicSubTestList").css({
            "background": "#fff"
        });
    });	


    ///
    //     check box is check then show data

    if($('.inactive-checkbox').not(':checked')){
    	otable = $('#forensicSubTable').dataTable( {
			"bFilter": true,
			"processing": true,
			"sPaginationType":"full_numbers",
			"fnDrawCallback": function ( oSettings ) {
				$('.update').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData = $('#forensicSubTable').dataTable().fnGetData(data);
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						var name = mData["name"];//this test name is off radiologysubtestname
						var test_name = mData["forensic_type_id"];//this test name is off radiologytestname
						var range = mData["range"];
						var unit_id = mData["unit_id"];
						var description = mData["description"];
						editClick(id,name,test_name,range,unit_id,description);
						
       
					}
				});
				$('.delete').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData =  $('#forensicSubTable').dataTable().fnGetData(data);
					
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						deleteClick(id);
					}
				});
			},
			
			"sAjaxSource":"controllers/admin/forensic_sub_test.php",
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "operation", "value": "show" });
			},
			"aoColumns": [
				{ "mData": "name"},
				{ "mData": "test_name" },
				{ "mData": "range" },
				{ "mData": "unit" },
				{ "mData": "description" },
				{
				  "mData": function (o) { 
					var data = o;
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
						   " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
						   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
						   " style='font-size: 22px; color:red; cursor:pointer;' "+
						   " data-original-title='Delete'></i>"; 
				  }
					
				},
			],
			aoColumnDefs: [
		        { 'bSortable': false, 'aTargets': [ 4 ] },
				{ 'bSortable': false, 'aTargets': [ 5 ] }
		    ]
		});
    }

    //show data active or inactive on check functionality in data table
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	otable.fnClearTable();
	    	otable.fnDestroy();
	    	otable="";
	    	otable = $('#forensicSubTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  $('#forensicSubTable').dataTable().fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var forensic_type_id = mData["forensic_type_id"];
							restoreClick(id,forensic_type_id);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/forensic_sub_test.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "checked" });
				},
				"aoColumns": [
					{ "mData": "name" },
					{ "mData": "test_name" },
					{ "mData": "range" },
					{ "mData": "unit" },
					{ "mData": "description" },
					{
					  "mData": function (o) { 
						var data = o;
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},
				],
				aoColumnDefs: [
			        { 'bSortable': false, 'aTargets': [ 4 ] },
					{ 'bSortable': false, 'aTargets': [ 5 ] }
			    ]
			});
		}
		else{
			otable.fnClearTable();
	    	otable.fnDestroy();
	    	otable="";
			otable = $('#forensicSubTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.update').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData = $('#forensicSubTable').dataTable().fnGetData(data);
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var name = mData["name"];//this test name is off radiologysubtestname
							var test_name = mData["forensic_type_id"];//this test name is off radiologytestname
							var range = mData["range"];
							var unit_id = mData["unit_id"];
							var description = mData["description"];
							editClick(id,name,test_name,range,unit_id,description);
							
		   
						}
					});
					$('.delete').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  $('#forensicSubTable').dataTable().fnGetData(data);
						
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							deleteClick(id);
						}
					});
				},
				
				"sAjaxSource":"controllers/admin/forensic_sub_test.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "show" });
				},
				"aoColumns": [
					{ "mData": "name"},
					{ "mData": "test_name" },
					{ "mData": "range" },
					{ "mData": "unit" },
					{ "mData": "description" },
					{
					  "mData": function (o) { 
						var data = o;
						return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
						   " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
						   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
						   " style='font-size: 22px; color:red; cursor:pointer;' "+
						   " data-original-title='Delete'></i>";

					  }
					},
				],
				aoColumnDefs: [
			        { 'bSortable': false, 'aTargets': [ 4 ] },
					{ 'bSortable': false, 'aTargets': [ 5 ] }
			    ]
			});
		}
    });
	
    //validation and ajax call on submit button
	//
    $("#btnSubmit").click(function() {

        var flag = "false";
		$("#txtNormalRange").val($("#txtNormalRange").val().trim());
		$("#txtSubForensicTestName").val($("#txtSubForensicTestName").val().trim());		
		
		if ($("#txtTestfee").val() == "") {
			$('#txtTestfee').focus();
            $("#txtTestfeeError").text("Please enter test fee");
			$("#txtTestfee").css({"border-color": "red"});                
            flag = "true";
        }
		if ($("#selUnitMeasure").val() == "-1") {
			$('#selUnitMeasure').focus();
            $("#selUnitMeasureError").text("Please select unit measure");
			$("#selUnitMeasure").css({"border-color": "red"});
            flag = "true";
        }
		if ($("#selTest").val() == "-1") {
            $("#selTestError").text("Please select test");
            $("#selTest").css({"border-color": "red"});
            flag = "true";
        }
        if ($("#txtSubForensicTestName").val() == "") {
			$('#txtSubForensicTestName').focus();
            $("#txtSubForensicTestNameError").text("Please enter forensic sub test name");
			$("#txtSubForensicTestName").css({"border-color": "red"});
            flag = "true";
        }
        
        if (flag == "true") {
            return false;
        }
		var unitMeasure = $("#selUnitMeasure").val();
		var test = $("#txtSubForensicTestName").val();
		var testName = $("#selTest").val();
		var normalRange = $("#txtNormalRange").val();
		var description = $("#txtDescription").val();
		description = description.replace(/'/g, "&#39");
		var postData = {
			"operation":"save",
			"unitMeasure":unitMeasure,
			"test" : test,
			"testName":testName,
			"normalRange":normalRange,
			"description":description
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/forensic_sub_test.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Forensic sub test saved successfully!!!");
					$('#mymessageModal').click();
					$('#inactive-checkbox-tick').prop('checked', false).change();
					clear();
					bindUnitMeasure();
					bindTest();
					//after sucessful data sent to database redirect to datatable list
					$("#advanced-wizard").hide();
					$(".blackborder").show();
       				$("#tabAddForensicSubTest").css({
			            "background": "#fff",
			            "color": "#000"
			        });
			        $("#tabForensicSubTestList").css({
			            "background":"linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
			            "color": "#fff"
			        });
        			$("#forensicSubTable").dataTable().fnReloadAjax();
				}
				else{
					$("#txtSubForensicTestNameError").text("Test name already exists");
				}

				bindUnitMeasure();
				bindTest();
			},
			error:function() {
				alert('error');
			}
		});
	});	
	
	//
    //keyup functionality
	//
	$("#selTest").change(function() {
        if ($("#selTest").val() != "-1") {
            $("#selTestError").text("");
            $("#selTest").removeAttr("style");
        }
        else{
			$("#selTestError").text("Please select test");
			$("#selTest").css({"border-color": "red"});
        }
    });

    $("#selUnitMeasure").change(function() {
        if ($("#selUnitMeasure").val() != "-1") {
            $("#selUnitMeasureError").text("");
            $("#selUnitMeasure").removeAttr("style");
        }
        else{
			$("#selUnitMeasureError").text("Please select unit measure");
			$("#selUnitMeasure").css({"border-color": "red"});
        }
    });
	
    $("#txtSubForensicTestName").keyup(function() {
        if ($("#txtSubForensicTestName").val() != "") {
            $("#txtSubForensicTestNameError").text("");
            $("#txtSubForensicTestName").removeAttr("style");
        }
        else{
        	$("#txtSubForensicTestNameError").text("Please enter forensic sub test name");
            $("#txtSubForensicTestName").css({"border-color": "red"});
        }
    });
     //on reset click focus on first textbox
    $("#btnReset").click(function(){
    	$("#txtSubForensicTestName").focus();
    	$("#selTest").removeAttr("style");
    	$("#txtSubForensicTestName").removeAttr("style");
    	$("#selUnitMeasure").removeAttr("style");
    	$("#selTest").removeAttr("style");
    	clear();
    });
});

//		
//function for edit the bed 
//
function editClick(id,name,test_name,range,unit_id,description){	
    
	$('#myModalLabel').text("Update forensic sub test");
	$('.modal-body').html($("#subForensicTestModel").html()).show();
	$('#modal').click();
	$("#myModal #btnUpdateForensic").removeAttr("style");
	$('#myModal #txtSubForensicTestName').val(name);
	$("#myModal #selTest").val(test_name);
	$("#myModal #txtNormalRange").val(range);
	$("#myModal #selUnitMeasure").val(unit_id);
	$('#myModal #txtDescription').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id);	
	
	
	$("#myModal #btnUpdateForensic").click(function(){
		var flag = "false";
		
		if ($("#myModal #selTest").val() == "-1") {
            $("#myModal #selTestError").text("Please select forensic test");
            flag = "true";
        }

        if ($("#myModal #txtSubForensicTestName").val() == "") {
            $("#myModal #txtSubForensicTestNameError").text("Please enter forensic sub test");
            flag = "true";
        }


        if ($("#myModal #selTest").val() == "-1") {
            $("#myModal #selTestError").text("Please select test");
            flag = "true";
        }
        if ($("#myModal #selUnitMeasure").val().trim() == "-1") {
            $("#myModal #selUnitMeasureError").text("Please select unit");
            flag = "true";
        }
		
		if(flag == "true"){			
		return false;
		}
		
		var test = $("#myModal #txtSubForensicTestName").val();
		var subTestName = $("#myModal #selTest").val();
		var range = $("#myModal #txtNormalRange").val();
		var unit_id = $("#myModal #selUnitMeasure").val();
		var description = $("#myModal #txtDescription").val();
		description = description.replace(/'/g, "&#39");
		var id = $('#selectedRow').val();
		
		
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/forensic_sub_test.php",
			data :{            
				"operation" : "update",
				"id" : id,
				"test":test,
				"subTestName":subTestName,
				"range":range,
				"unit_id":unit_id,
				"description":description
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('#myModal').modal('toggle');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Forensic sub test update successfully!!!");
					$("#forensicSubTable").dataTable().fnReloadAjax();
					$('#mymessageModal').click();					
					clear();
				}
			},
			error:function() {
				alert('error');
			}
		});
	});
	
	$("#myModal #selUnitMeasure").change(function() {
        if ($("#myModal #selUnitMeasure").val() != "-1") {
            $("#myModal #selUnitMeasureError").text("");
            $("#myModal #selLabType").removeAttr("style");
        }
    });
	
	$("#myModal #selTest").change(function() {
        if ($("#myModal #selTest").val() != "-1") {
            $("#myModal #selTestError").text("");
            $("#myModal #selTest").removeAttr("style");
        }
    });
	
    $("#myModal #txtSubForensicTestName").keyup(function() {
        if ($("#myModal #txtSubForensicTestName").val() != "") {
            $("#myModal #txtSubForensicTestNameError").text("");
            $("#myModal #txtSubForensicTestName").removeAttr("style");
        }
    });	
}

function deleteClick(id){ // delete click function
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete Diseases Category");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#myConfirmModal').click(); 
	$('#selectedRow').val(id); 
	var type="delete";
	$('#confirm').attr('onclick','deleteForensicSubTest("'+type+'");');
}// end click fucntion


function restoreClick(id,forensic_type_id){
	$('.modal-body').text("");
	$('#selectedRow').val(id);
	$('#selTest').val(forensic_type_id);
	
	$('#confirmMyModalLabel').text("Restore Diseases Category");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#myConfirmModal').click(); 
	var type="restore";
	$('#confirm').attr('onclick','deleteForensicSubTest("'+type+'");');
}

//function to restore and delete forensic sub test record
function deleteForensicSubTest(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();		
			
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/forensic_sub_test.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Forensic sub test delete successfully!!!");
					$('#mymessageModal').click();
					$("#forensicSubTable").dataTable().fnReloadAjax();
					 
				}
				else{
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("This test is used , so you can not delete!!!");
					$('#mymessageModal').click();
				}
			},
			error: function()
			{
				alert('error');
				  
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
		var test_name = $("#selTest").val();
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/forensic_sub_test.php",
			data :{            
				"operation" : "restore",
				"test_name" : test_name,
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Forensic sub test restored successfully!!!");
					$('#mymessageModal').click();
					$("#forensicSubTable").dataTable().fnReloadAjax();
					
				}	
				if(data == "It can not be restore!!!"){
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("It can not be restore!!!");
					$('#mymessageModal').click();
					$("#forensicSubTable").dataTable().fnReloadAjax();
				}				
			},
			error:function()
			{
				alert('error');				  
			}
		});
	}
}

//Function for clear the data
function clear(){
	$('#selLabType').val("");
	$('#selLabTypeError').text("");
	$('#selTest').val("");
	$('#selTestError').text("");
	$('#txtSubForensicTestName').val("");
	$('#txtSubForensicTestNameError').text("");
	$('#txtNormalRange').val("");
	$('#txtunit_id').val("");
	$('#selUnitMeasureError').text("");
	$('#txtDescription').val("");
} 
function bindTest(){
	$.ajax({     
        type: "POST",
        cache: false,
        url: "controllers/admin/forensic_sub_test.php",
        data: {
        "operation":"showTest"
        },
        success : function(data) { 
        if(data != null && data != ""){
            var parseData = jQuery.parseJSON(data);

            var option ="<option value='-1'>Please select</option>";
            for (var i=0;i<parseData.length;i++)
            {
            option+="<option value='"+parseData[i].id+"'>"+parseData[i].name+"</option>";
            }
            $('#selTest').html(option);          
            }
        },
        error:function(){
        }
    }); 
}
function bindUnitMeasure(){

	$.ajax({     
        type: "POST",
        cache: false,
        url: "controllers/admin/forensic_sub_test.php",
        data: {
        "operation":"showUnitMeasure"
        },
        success : function(data) { 
        if(data != null && data != ""){
            var parseData = jQuery.parseJSON(data);

            var option ="<option value='-1'>Please select</option>";
            for (var i=0;i<parseData.length;i++)
            {
            option+="<option value='"+parseData[i].id+"'>"+parseData[i].unit+"</option>";
            }
            $('#selUnitMeasure').html(option);          
            }
        },
        error:function(){
        }
    }); 
}



//# sourceURL = sub_lab_test.js