var oForensicTable = null;
var paymentMode = '';
var insuranceCompanyId = "";
var insurancePlanId = "";
var insurancePlanNameid = "";
var revisedCost = '';
var paymentMethod ='';
var copayType = '';
var copayValue = '';
$(document).ready(function() {

/* ****************************************************************************************************
 * File Name    :   forensic_test_request.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Arora
 * Created Date :   30th dec, 2015
 * Description  :   This page  manages lab test in consultant module
 *************************************************************************************************** */
    loader();
    debugger;
    
	$(".amountPrefix").text(amountPrefix);
	$('#oldHistoryModalDetails input[type=text]').addClass("as_label");
    $('#oldHistoryModalDetails input[type=text]').attr('readonly', true);
    $('#oldHistoryModalDetails input[type="button"]').addClass("btnHide");
    $('#oldHistoryModalDetails #divDropDownForensicRequest').hide();

    /*Focus on first element*/
    $("#selForensicTest").focus();

    /**DataTable Initialization**/
    oForensicTable = $('#forensicRequestsTbl').DataTable({
        "bPaginate": false,
        aoColumnDefs: [{'bSortable': false,'aTargets': [4] }]
    });
	
	if ($('#thing').val() ==3) {
        var visitId =  parseInt($("#textvisitId").val().replace( /[^\d.]/g, '' ));
		getPaymentMode(visitId);
    }
    if ($('#thing').val() ==4) {
        var visitId =  parseInt($("#oldHistoryVisitId").text().replace( /[^\d.]/g, '' ));
		getPaymentMode(visitId);
    }
    if ($('#thing').val() ==5) {
        var visitId =  parseInt($("#oldHistoryVisitId").text().replace( /[^\d.]/g, '' ));
		getPaymentMode(visitId);
    }

	bindForensicTest(visitId);//Loading the forensic dropdown at the start

    /*Add Lab Test click event*/
    $('#addForensicLabRequest').click(function() {
        var flag = false;
        $('#addForensicLabRequest').css('border', ''); //remove the red error border from add test button
        $('#noForensicDataError').html(""); //remove the error message after the add button

        if ($('#selForensicTest').val() == "-1" || $('#selForensicTest').val() == "") {
            $('#selForensicTestError').text("Please select forensic test");
            $('#selForensicTest').addClass("errorStyle");
            return false;
        }
        if($("#selRequestType").val() == -1){
            $("#selRequestType").focus();
            $("#selRequestType").addClass("errorStyle");
            $("#selRequestTypeError").text("Please select request type");
            return false;
        }

        var id = $('#selForensicTest').val();//getting value of selected element
        var name = $('#selForensicTest  option:selected').attr('data-name');//getting attribute name of selected element
        var cost = $('#selForensicTest  option:selected').attr('data-cost');
        var requestType = $('#selRequestType').val();
        var newRevisedCost = '';
        var paidStatus = "";
        var testStatus = "";
        var restrictBill = "";
        if (revisedCost == "") {
            newRevisedCost = cost;
            paymentMethod = "cash";
        }
        else {
            newRevisedCost = revisedCost;
            paymentMethod = "insurance";
        }

        if (checkTestExcluded =="yes") {
            $('#confirmUpdateModalLabel').text();
            $('#updateBody').text("Are you sure you want to add this still it is not included in your insurance?");
            $('#confirmUpdateModal').modal();
            $("#btnConfirm").unbind();
            $("#btnConfirm").click(function(){
                var newPaymentMethod = "cash"; 
                newRevisedCost = cost;//if it is excluded then revised cost is equal to orignal cost

                if (requestType == "internal") {
                    paidStatus = "Unpaid";
                    testStatus = "Pending";
                    restrictBill = "notrestrictBill";
                }
                else {
                    paidStatus = "N/A";
                    testStatus = "N/A";
                    cost  = 0; 
                    newRevisedCost = 0;
                    restrictBill = "restrictBill";
                }

                var addData = ["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", paidStatus, testStatus, cost, newRevisedCost,"<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowForensicDelete($(this));' value='Remove'>", id,requestType,newPaymentMethod,restrictBill];
                /*Check that test already exist*/
                for (var i = 0; i < oForensicTable.fnGetNodes().length; i++) {
                    var iRow = oForensicTable.fnGetNodes()[i];   // assign current row to iRow variable
                    var aData = oForensicTable.fnGetData(iRow); // Pull the row

                    testId = aData[6];//getting test id
                    var flag = false;
                    if (id == testId && aData[2]!="resultsent") {
                        $('#selForensicTest').focus();
                        $('#selForensicTestError').text("Test already exist");
                        $('#selForensicTest').addClass("errorStyle");
                        return true;
                    };
                    if (flag == true) {
                        return false;
                    }
                    if (id == testId && aData[2] == "resultsent") {
                        addData = ["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", paidStatus, testStatus, cost,newRevisedCost, "<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowForensicDelete($(this));' value='Remove'>", id,requestType,newPaymentMethod,"restrictBill"];
                    }
                }

                /*Add data in table*/
                var currData = oForensicTable.fnAddData(addData);
                /*Get current row to add color in that*/
                var getCurrDataRow = oForensicTable.fnSettings().aoData[ currData ].nTr;
                $(getCurrDataRow).find('td').css({"background-color":"#fff","color":"#000"});

                $('#totalForensicBill').text(parseInt($('#totalForensicBill').text()) + parseInt(newRevisedCost));//add total bill
            });
        }
        else{

            if (requestType == "internal") {
                paidStatus = "Unpaid";
                testStatus = "Pending";
                restrictBill = "notrestrictBill";
            }
            else {
                paidStatus = "N/A";
                testStatus = "N/A";
                cost  = 0; 
                newRevisedCost = 0;
                restrictBill = "restrictBill";
            }

            var addData = ["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", paidStatus, testStatus, cost, newRevisedCost,"<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowForensicDelete($(this));' value='Remove'>", id,requestType,paymentMethod,restrictBill];
            /*Check that test already exist*/
            for (var i = 0; i < oForensicTable.fnGetNodes().length; i++) {
                var iRow = oForensicTable.fnGetNodes()[i];   // assign current row to iRow variable
                var aData = oForensicTable.fnGetData(iRow); // Pull the row

    			testId = aData[6];//getting test id
                var flag = false;
    			if (id == testId && aData[2]!="resultsent") {
    				$('#selForensicTest').focus();
                    $('#selForensicTestError').text("Test already exist");
                    $('#selForensicTest').addClass("errorStyle");
                    return true;
    			};
                if (flag == true) {
                    return false;
                }
                if (id == testId && aData[2]=="resultsent") {
                    addData = ["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", paidStatus, testStatus, cost,newRevisedCost, "<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowForensicDelete($(this));' value='Remove'>", id,requestType,paymentMethod,"restrictBill"];
                }
            }
            /*Add data in table*/
            var currData = oForensicTable.fnAddData(addData);
            /*Get current row to add color in that*/
            var getCurrDataRow = oForensicTable.fnSettings().aoData[ currData ].nTr;
            $(getCurrDataRow).find('td').css({"background-color":"#fff","color":"#000"});

            $('#totalForensicBill').text(parseInt($('#totalForensicBill').text()) + parseInt(newRevisedCost));//add total bill
        } 
    });    

    //Save button functionality
    $('#btnSaveForensicTestRequest').click(function() {

        var tableData = oForensicTable.fnGetData(); //fetching the datatable data

        /*Code to compare previpous and current data based on attribute so on click not to get previous data coming from database*/
        var arr= [];
        jQuery.grep(tableData, function( n, i ){
            if($(n[0]).attr("data-previous") == "false"){
                arr.push(n);
            }
        });

        /*Checking whether datatable is empty or not*/
        if (tableData.length == 0) {
            $('#addForensicLabRequest').addClass("errorStyle"); //Put the red border around the add lab button
            $('#noForensicDataError').html("&nbsp;&nbsp;Please add test(s)."); //Put the error message after the add lab test button
            return false;
        } 
        else { //Saving the data

            var patientId = parseInt($('#txtPatientID').val().replace( /[^\d.]/g, '' ));
            var visitId = parseInt($('#textvisitId').val().replace( /[^\d.]/g, '' ));

            /*AJAX call to save data*/
			if(arr.length == 0){
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Sorry");
				$('.modal-body').text("Your request already in pending!!!");
			}
			else{
                $('#confirmUpdateModalLabel').text();
                $('#updateBody').text("Are you sure that you want to save this?");
                $('#confirmUpdateModal').modal();
                $("#btnConfirm").unbind();
                $("#btnConfirm").click(function(){

                    var totalCurrentReqBill = 0;
                    //get the total cost of all currently added request
                    $.each(arr ,function(i,v){
                        if (arr[i][8] == 'insurance') {
                            totalCurrentReqBill= totalCurrentReqBill + parseInt(arr[i][4]);
                        }                            
                    });

    				$.ajax({
                        type: "POST",
                        cache: false,
                        url: "controllers/admin/forensic_test_request.php",
                        datatype: "json",
                        data: {
                            'operation': 'save',
                            'data': JSON.stringify(arr),//send only current data
                            'patientId': patientId,
                            'visitId': visitId,
                            'tblName': 'forensic_test_request',
                            'fkColName': 'forensic_test_id',
                            'copayType' : copayType,
                            'copayValue' : copayValue,
                            'totalCurrentReqBill' : totalCurrentReqBill
                        },

                        success: function(data) {
                            if (data != "0" && data == "1") {
                                $('#messagemyModal').modal();
                                $('#messageMyModalLabel').text("Success");
                                $('.modal-body').text("Forensic test requested successfully!!!");
                                bindForensicTest(visitId);
        						/* var visitId = $("#textvisitId").val(); */
        						ForensicTest(visitId,paymentMode);
                            } 
                            if(data == "0") {
                                $('#messagemyModal').modal();
                                $('#messageMyModalLabel').text("Sorry");
                                $('.modal-body').text("Your previous test result is still not sent!!!");
        						/* var visitId = $("#textvisitId").val(); */
        						ForensicTest(visitId,paymentMode);
                            }	
                        },
                        error: function() {
                            $('#messageMyModalLabel').text("Error");
                            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                            $('#messagemyModal').modal();
                        }
                    });
                });
			}            
        }
    });

    /*Reset button functionality*/
    $('#btnResetForensicTestRequest').click(function() {
        clear();
    });

    /*Change functionality*/
    $('#selForensicTest').change(function() {
        var forensicTestId = $('#selForensicTest').val();
        if (forensicTestId != "") {

            $('#selForensicTest').removeClass("errorStyle");
            $('#selForensicTestError').text("");

            if (paymentMode.toLowerCase() == "insurance") {
                getRevisedCost(forensicTestId);
                checkExclusion(forensicTestId,insuranceCompanyId,insurancePlanId,insurancePlanNameid,"forensic");
            }
            else{
                checkTestExcluded = '';
            }
        }
    });
});

/*Binding Forensic Test*/
function bindForensicTest(visitId) {
    /*AJAX call to show forensic tests*/
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/forensic_test_request.php",
        data: {
            "operation": "showForensicTest",
            "visitIdTest" : visitId
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "' data-cost='" + parseData[i].fee + "' data-name='" + parseData[i].name + "'>" + parseData[i].name + "</option>";
                }
                $('#selForensicTest').html(option);
            }
        },
        error: function() {
            $('#messagemyModal').modal();
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
        }
    });
}

/**Row Delete functionality**/
function rowForensicDelete(currInst) {
    var row = currInst.closest("tr").get(0);
    var oForensicTbl = $('#forensicRequestsTbl').dataTable();
    $('#totalForensicBill').text(parseInt($('#totalForensicBill').text()) - parseInt($(row).find('td:eq(4)').text()));
    oForensicTbl.fnDeleteRow(row);
}

function clear() {
    $('#selForensicTest').val("");
    $('#addForensicLabRequest').removeClass("errorStyle");
    $('#noForensicDataError').text("");
    $("#selForensicTest").focus();
    var lengthDeleteRow = $(".btnEnabled").closest("tr").length;
    for(var i=0;i<lengthDeleteRow;i++){
        var myDeleteRow = $(".btnEnabled").closest("tr")[0];            
        $('#totalForensicBill').text(parseInt($('#totalForensicBill').text()) - parseInt($(myDeleteRow).find('td:eq(4)').text()));
        oForensicTable.fnDeleteRow(myDeleteRow);
    }
}

function ForensicTest(visitId,paymentMode){
    /*Fetch data from db and insert in to table during page load*/
    var postData = {
        "operation": "forensic_search",
        "visit_id": visitId,
        "paymentMode" : paymentMode,
        "activeTab" : "forensic"
    }
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/old_test_requests.php",
        datatype: "json",
        data: postData,

        success: function(data) {
            dataSet = JSON.parse(data);
            oForensicTable.fnClearTable();
            $('#totalForensicBill').text("0");
            
            for(var i=0; i< dataSet.length; i++){
                var id = dataSet[i].id;
                var pay_status = dataSet[i].pay_status;
                var name = dataSet[i].name;
                var test_status = dataSet[i].test_status;
                var cost = dataSet[i].fee;
                var oldRevisedCost =''; 
                if (paymentMode != null && paymentMode.toLowerCase() == "insurance") {
                    oldRevisedCost = dataSet[i].revised_cost;
                }
                else {
                    oldRevisedCost = cost;
                }
                //check wheteher in name external is coming or not
                if (name.indexOf("(external)") > 1) {
                    pay_status = "N/A";
                    test_status = "N/A";
                    cost = 0;
                    oldRevisedCost = 0;
                }

                /*Apply here span to recognise previous or current data*/
                oForensicTable.fnAddData(["<span data-previous='true'>"+name+"</span>",pay_status, test_status,cost,oldRevisedCost,"<input type='button' class ='btnDisabled' value='Remove' disabled>",id]);//send one column null so user can't remove previous data
                $('#totalForensicBill').text(parseInt($('#totalForensicBill').text()) + parseInt(oldRevisedCost));//add total bill                
            }
            oForensicTable.find('tbody').find('tr').find('td').css({"background-color":"#ccc"});
        }
    });
}

//get payment mode insurance or other
function getPaymentMode(visitId){
    var postData = {
        "operation": "payment_mode",
        "visitId": visitId
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (result.trim() != '') {
            paymentMode = parseData[0].payment_mode;

            if (paymentMode.toLowerCase() == "insurance") {
                insuranceDetails(visitId);
                paymentMethod = "insurance";
            }
            else{
                paymentMethod = "cash";
            }         
        } 
        ForensicTest(visitId,paymentMode);
    });
}
function insuranceDetails(visitId){
    var postData = {
        "operation": "insuranceDetail",
        "visitId": visitId
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (parseData != '') {
            insuranceCompanyId = parseData[0].insurance_company_id;
            insurancePlanId = parseData[0].insurance_plan_id;
            insurancePlanNameid = parseData[0].insurance_plan_name_id;

            getCopayValue();//get the copay details
        }
    });
}
function getRevisedCost(forensicTestId){
    var postData = {
        "operation": "revisedCostDetail",
        "insuranceCompanyId": insuranceCompanyId,
        "insurancePlanId" : insurancePlanId,
        "insurancePlanNameid" : insurancePlanNameid,
        "componentId" : forensicTestId,
        "activeTab" : "Forensic"
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (result.length > 2) {
            revisedCost = parseData[0].revised_cost;
        }
        else{
            revisedCost ='';
        }
    });
}
function getCopayValue(){
    var postData = {
        "operation": "getCopayValue",
        "insuranceCompanyId": insuranceCompanyId,
        "insurancePlanId" : insurancePlanId,
        "insurancePlanNameid" : insurancePlanNameid
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (parseData != '' && parseData != null) {
            copayType = parseData[0].copay_type;
            copayValue = parseData[0].value;
        }
    });
}