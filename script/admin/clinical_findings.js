var tableLabelValueTemplate;
var labelText = "";
var textBoxId;
var textAreaId ="";
$(document).ready(function() {
/* ****************************************************************************************************
* File Name    :   clinical_findings.js
* Company Name :   Qexon Infotech
* Created By   :   Rupesh Arora
* Created Date :   4th Jan, 2016
* Description  :   This page manages clinical data in consultant module
*************************************************************************************************** */
	loader();
	debugger;
	$('.title-clinical-findings').hide();//hide the title bar
    var visitId = $("#textvisitId").val();
    var patientId = $("#txtPatientID").val();

    $('#oldHistoryModalDetails input[type=text]').attr('readonly', true);
    $('#oldHistoryModalDetails input[type="button"]').addClass("btnHide");
    $('#oldHistoryModalDetails textarea').attr("disabled","disabled");
	$('#textPresentingComplaint').focus();
	/*if visit & patient id not null then call this function to get data from db*/
	if(visitId != '' && patientId != '') {

		if ($('#thing').val() ==3) {
        	var visitId = $("#textvisitId").val();
			var visitPrefix = visitId.substring(0, 3);
			visitId = visitId.replace ( /[^\d.]/g, '' ); 
			visitId = parseInt(visitId);
			var patientId = parseInt($("#txtPatientID").val().replace ( /[^\d.]/g, '' ));
	    }
	    if ($('#thing').val() ==4) {
	        var visitId = $("#oldHistoryVisitId").text();
			var visitPrefix = visitId.substring(0, 3);
			visitId = visitId.replace ( /[^\d.]/g, '' ); 
			visitId = parseInt(visitId);
			var patientId = parseInt($("#txtPatientID").val().replace ( /[^\d.]/g, '' ));
	    }
	    if ($('#thing').val() ==5) {
	        var visitId = $("#oldHistoryVisitId").text();
			var visitPrefix = visitId.substring(0, 3);
			visitId = visitId.replace ( /[^\d.]/g, '' ); 
			visitId = parseInt(visitId);
			patientId = $("#txtPatientIdHistory").val();
			var visitPrefix = patientId.substring(0, 3);
			patientId = patientId.replace ( /[^\d.]/g, '' ); 
			patientId = parseInt(patientId);
	    }
		getClinicalFindingsForPaitentVisit(visitId, patientId);
	}

	/*Save functionality*/
    $("#btnSubmit").click(function() {
		loader();
		/* var flag = false;
    	if ($("#textPresentingComplaint").val() =="" ){	
			$("#textPresentingComplaintError").text("Please enter presenting complain");
			$("#textPresentingComplaint").focus();
			$("#textPresentingComplaint").addClass("errorStyle");
            flag = true;      
		}
		if (flag ==  true) {
			return false;
		} */
		var visitId = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
		var patientId = parseInt($("#txtPatientID").val().replace ( /[^\d.]/g, '' ));
		$('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
        	saveClinicalFindingsForPaitentVisit(visitId, patientId);//call function to save data to database

		});
    });
	
    /*on reset button click*/
    $("#btn_Reset").click(function() {
        $("#textVisitId").focus();
        clearClinicalFindingsData();
        clearFormDetails("#clinicalFindingsForm");
    });

    $("#textPresentingComplaint").keyup(function() {
    	if ($("#textPresentingComplaint").val() !="") {
    		$("#textPresentingComplaintError").text("");
    		$("#textPresentingComplaint").removeClass("errorStyle");
    	}
    });
	
		// inilize data table in popup
	tableLabelValueTemplate = $("#myLabelTemplateModal #tblLabelValueTemplate").dataTable({
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		 "bAutoWidth": false,
		aoColumnDefs: [{'bSortable': false,'aTargets': [2] }],
		"fnDrawCallback" : function() {
			$('#myLabelTemplateModal #tblLabelValueTemplate tbody tr').on( 'dblclick', function () {
							
				if ( $(this).hasClass('selected') ) {
				  $(this).removeClass('selected');
				}
				else {
				  tableLabelValueTemplate.$('tr.selected').removeClass('selected');
				   $(this).addClass('selected');
				}
				
				var mData = tableLabelValueTemplate.fnGetData(this); // get datarow
				if (null != mData)  // null if we clicked on title row
				{				
					var textValue = mData[1].split(">")[1].split("<")[0];
					$('#'+textBoxId).val(textValue);
					if(textAreaId != '' && textAreaId != undefined){
						$('#'+textAreaId).val(textValue);
					}
					$(".close").unbind();
					$(".close").click();
				}					
			});
			
			// delete event
			$('.deleteData').unbind();
			$('.deleteData').on('click',function() {
				var id = $(this).attr('data-id');
				 deleteClick(id);
			});
		}
	});
			
	// click on + button for add  label value
	$(".addTemplate").unbind();
	$(".addTemplate").on('click',function() {
			
			labelText = $(this).parent().siblings('label').text();
			textBoxId = $(this).parent().siblings().find('input[type=text]').attr('id');
			textAreaId = $(this).parent().siblings().find('textarea').attr('id');;
			oldDataRequest();
			$('#myLabelTemplateModal').modal();
			$('#myLabelTemplateModal #labelTemplateModalHeader').text(labelText);
			$('#modal-body').text("");		
	});
	
	// add click for add row and save data.
	$(".addValues").unbind();
	$(".addValues").on('click',function() {
		$("#btnSave").removeAttr('style');
		$("#btnResetValue").removeAttr('style');
		tblLength = tableLabelValueTemplate.fnGetData().length;
		
		tableLabelValueTemplate.fnAddData([tblLength+1,"<input Type = 'text' class= 'textValue' style='width:100%;'>","<input type = 'button' value='Remove' class='removeValue'>"]);
		$(".textValue").focus();
		// remove row
		$(".removeValue").unbind();
		$(".removeValue").on('click',function(){
			var row = $(this).closest("tr").get(0);
			tableLabelValueTemplate.fnDeleteRow(row);		
			var rowData = tableLabelValueTemplate.fnGetData();
			for(j = 0;j < rowData.length;j++) {
				$($('.sorting_1')[j]).text(j+1);
			}
		});
	});
	
	// save data table details
	$("#btnSave").unbind();
	$("#btnSave").on('click',function() {
		var rowData = tableLabelValueTemplate.fnGetData();
		var objData = [];
		for(j = 0;j < rowData.length;j++) {	
			if($($(".textValue")[j]).attr('data-previous') != "true") {
				if($($('.textValue')[j]).val() != "") {
					objData.push($($('.textValue')[j]).val());
				}else{
					$('#messageMyModalLabel').text("Alert");
					$('#messagemyModal .modal-body').text("Please enter value.");
					$('#messagemyModal').modal();
				}
			}			
		}
		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/clinical-findings.php",
			datatype: "json",
			data: {
				'operation': 'saveLabelDetails',
				'labelText':labelText,
				'finalDataObj': JSON.stringify(objData)
			},

			success: function(data) {
				if (data == 1) {
				
					$('#messageMyModalLabel').text("Success");
					$('#messagemyModal .modal-body').text("Saved successfully!!!");
					$('#messagemyModal').modal();
					$("#btnSave").hide();
					$("#btnResetValue").hide();
					oldDataRequest();
				}
				if (data == 0) {
					$(".close-modal").click();
					$('#messageMyModalLabel').text("Alert");
					$('#messagemyModal .modal-body').text("Please enter value.");
					$('#messagemyModal').modal();
				}
			},
			error: function() {
				$(".close-modal").click();
				$('#messageMyModalLabel').text("Error");
				$('#messagemyModal .modal-body').text("Something awful happened!! Please try to contact admin.");
				$('#messagemyModal').modal();
			}
		});
	});
	
	// reset data table fiels
	$("#btnResetValue").unbind();
	$("#btnResetValue").on('click',function(){
		clearDataTbaleDetails();
	});
});

/*Function to get data from db*/
function getClinicalFindingsForPaitentVisit(visitId, patientId) {
	var postData = {
		"operation":"getData",
		"visitId" : visitId,
		"patientId" : patientId
	}
	
	$.ajax({     
		type: "POST",
		cache: false,
		url: "controllers/admin/clinical-findings.php",
		data: postData,
		success: function(data) { 
			if(data != "0"){		 
				var parseData = jQuery.parseJSON(data);//parse JSON data
		
				for (var i=0;i<parseData.length;i++) {
					/*Setting value to text box*/
					$("#textPresentingComplaint").val(parseData[i].presentingComplaint);
					$("#txtGeneralAppearance").val(parseData[i].generalAppearance);
					$("#txtPVPR").val(parseData[i].pvpr);
					$("#txtRespiratory").val(parseData[i].respiratory);
					$(txtPsychologicalStatus).val(parseData[i].psychologicalStatus);
					$("#txtCVS").val(parseData[i].cvs);
					$("#txtENT").val(parseData[i].ent);
					$("#txtAbdomen").val(parseData[i].abdomen);
					$("#txtPMHPSH").val(parseData[i].pmhpsh);
					$("#txtCNS").val(parseData[i].cns);
					$("#txtPOH").val(parseData[i].poh);
					$("#txtEYE").val(parseData[i].eye);
					$("#txtPGH").val(parseData[i].pgh);
					$("#txtMuscularSkeletal").val(parseData[i].muscularSkeletal);
					$("#txtFSH").val(parseData[i].fsh);
					$("#txtSkin").val(parseData[i].skin);
					$("#txtClinicalNotes").val(parseData[i].clinicalNotes);
					$("#txtProvisionalDiagnosis").val(parseData[i].provisionalDiagnosis);
				}

				/*Change save button value if data is present there*/
				if ($("#textPresentingComplaint").val() !="" ){
					$("#btnSubmit").val("Update");
				}
			}
		},
		error:function(){
			$('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();
		}
	});
}

// get value on autoCompletedValue
$("input[type=text]").keyup(function() {
	autoCompletedValue($(this).val(),$(this).attr('id'),$(this).parent().parent().find('label').text());
});
$("textarea").keyup(function() {
	autoCompletedValue($(this).val(),$(this).attr('id'),$(this).parent().parent().find('label').text());
});

/*Function to save data*/
function saveClinicalFindingsForPaitentVisit(visitId, patientId) {
	var presentingComplaint = $("#textPresentingComplaint").val();
	var generalAppearance = $("#txtGeneralAppearance").val() ;
	var pvpr = $("#txtPVPR").val();
	var respiratory = $("#txtRespiratory").val();
	var psychologicalStatus = $("#txtPsychologicalStatus").val();
	var cvs = $("#txtCVS").val();
	var ent = $("#txtENT").val();
	var abdomen = $("#txtAbdomen").val();
	var pmhpsh = $("#txtPMHPSH").val();
	var cns = $("#txtCNS").val();
	var poh = $("#txtPOH").val();
	var eye = $("#txtEYE").val();
	var pgh = $("#txtPGH").val();
	var muscularSkeletal = $("#txtMuscularSkeletal").val();
	var fsh = $("#txtFSH").val();
	var skin = $("#txtSkin").val();
	var clinicalNotes = $("#txtClinicalNotes").val();
	var provisionalDiagnosis = $("#txtProvisionalDiagnosis").val();
	
	var postData = {
		"operation": "saveData",
		"visitId": visitId,
		"patientId": patientId,
		"presentingComplaint": presentingComplaint,
		"generalAppearance": generalAppearance,
		"pvpr": pvpr,
		"respiratory": respiratory,
		"psychologicalStatus": psychologicalStatus,
		"cvs": cvs,
		"ent": ent,
		"abdomen": abdomen,
		"pmhpsh": pmhpsh,
		"cns": cns,
		"poh": poh,
		"eye": eye,
		"pgh": pgh,
		"muscularSkeletal": muscularSkeletal,
		"fsh": fsh,
		"skin": skin,
		"clinicalNotes": clinicalNotes,
		"provisionalDiagnosis": provisionalDiagnosis
	}
	
	$.ajax(
		{                   
		type: "POST",
		cache: false,
		url: "controllers/admin/clinical-findings.php",
		datatype:"json",
		data: postData,
		
		success: function(data) {
			if(data != "0" && data != ""){
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Success");
				$('.modal-body').text("");
				$('.modal-body').text("Clinical finding Updated successfully!!!");
			
			}
		},
		error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
	});
}
	
/*Function to clear data*/
function clearClinicalFindingsData(){
	 $("#textPresentingComplaint").val("");
	 $("#txtGeneralAppearance").val("");
	 $("#txtPVPR").val("");
	 $("#txtRespiratory").val("");
	 $("#txtPsychologicalStatus").val("");
	 $("#txtCVS").val("");
	 $("#txtENT").val("");
	 $("#txtAbdomen").val("");
	 $("#txtPMHPSH").val("");
	 $("#txtCNS").val("");
	 $("#txtPOH").val("");
	 $("#txtEYE").val("");
	 $("#txtPGH").val("");
	 $("#txtMuscularSkeletal").val("");
	 $("#txtFSH").val("");
	 $("#txtSkin").val("");
	 $("#txtClinicalNotes").val("");
	 $("#txtProvisionalDiagnosis").val("");
}
// clear data table details
function clearDataTbaleDetails() {
	tableLabelValueTemplate.fnClearTable();
	$("#btnSave").css({'display':'none'});
	$("#btnResetValue").hide();
}

// show old data on load ad button
function oldDataRequest() {
    var postData = {
        "operation": "oldDetailsSearch",
        "labelText": labelText
    }
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/clinical-findings.php",
        datatype: "json",
        data: postData,

        success: function(data) {
            dataSet = JSON.parse(data);
            tableLabelValueTemplate.fnClearTable();
            
            for(var i=0; i< dataSet.length; i++){
				var value = dataSet[i].value;
				var id = dataSet[i].id;
                var rowCount = tableLabelValueTemplate.fnGetData().length;
                /*Apply here span to recognise previous or current data*/
                tableLabelValueTemplate.fnAddData([rowCount+1,'<span class="textValue" style = "width:100%;" data-previous = "true">'+value+'</span>','<i class="fa fa-trash-o deleteData" data-id ='+id+' title="Delete" data-original-title="Delete" style="font-size:20px;cursor: pointer;"></i>']);
            }
        }
    });
}

// delete details
function deleteClick(id) {
	$('#confirmMyModalLabel').text("Delete");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
	 $('#confirm').on('click',function(){
		  $.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/clinical-findings.php",
			datatype: "json",
			data: {
				"operation": "deleteDetails",
				"id": id
			},
			success: function(data) {
				if(data == 1) {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Deleted Successfully!!!");
					$('#messagemyModal').modal();
						oldDataRequest();
				}
			},
			 error: function() {							
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	});
}
function autoCompletedValue(value,textId,lblValue) {
	//Autocomplete functionality  for clining finding
	var txtValue = value;
	var lblValue = lblValue;
	$("#"+textId).autocomplete({
		source: function(request, response) {
			var postData = {
				"operation":"showtemplateData",
				"lblValue": lblValue,
				"txtValue": txtValue
			}			
			$.ajax({     
				type: "POST",
				cache: false,
				url: "controllers/admin/clinical-findings.php",
				datatype:"json",
				data: postData,
			 
				success: function(dataSet) {
					if(dataSet.length > 2) {
						response($.map(JSON.parse(dataSet), function (item) {
							return {
								id: item.id,
								value: item.value
							}
						}));
					}
				},
				error: function(){

				}
			});
		},
		select: function (e, i) {
			$("#"+textId).val(i.item.value);    
		},
		minLength: 3
	});  

}