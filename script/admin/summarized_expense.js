var summarizedExpenseTable = '';
$(document).ready(function() {
	debugger;
	$(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});
	var arr=[];
	removeErrorMessage();//call from common.js

	summarizedExpenseTable = $('#summarizedExpenseTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "aaSorting": []
    });

    $("#viewReport").on('click',function(){
		var toDate = $("#toDate").val().trim();
		var fromDate = $("#fromDate").val().trim();
    	var postData = {
			"operation" : "showSummarizedIncome",
			'toDate' : toDate,
			'fromDate' : fromDate
		}
		summarizedExpenseTable.fnClearTable();
		dataCall("./controllers/admin/detailed_expense.php", postData, function (result) {
			if (result.length>2) {
				var parseData = JSON.parse(result);
				var lastLedgerId = '';				
				var overAllTotal = 0;
				arr=[];
				var counter = 1;
				$.each(parseData,function(i,v){
					var ledgerId = v.ledger_id;
					var html = '';
					if (lastLedgerId != ledgerId) {
						html+='<table class="main-anlysis"><thead><tr><th>'+counter+'. '+v.ledger+'</th><th></th>';
						html+'</tr></thead><tbody>';
						var subTotal = 0;
						var filteredData = parseData.filter(function (el) {//Function to return all the same data i.e filter the data
							return el.ledger_id == ledgerId;
						});
						$.each(filteredData,function(ind,val){
							html+='<tr><td>'+val.payment_mode+'</td><td>'+val.amount+'</td></tr>';
							subTotal+=parseFloat(val.amount);
							overAllTotal+=parseFloat(val.amount);
						})
						html+='</tbody><tfoot><tr><td>Sub Total</td><td>'+subTotal+'</td></tr></tfoot></table>';
						arr.push(html);
						counter++;
					}
					summarizedExpenseTable.fnAddData([v.account_name,v.amount]);
					lastLedgerId = ledgerId
				});
				var html = '';
				html+='<table class="analysis-total"><thead><th>Over All Total Amount</th><th>';
				html+=overAllTotal+'</th></thead></table>';
				arr.push(html);
				$('.cashAnalysis').html(arr);
				$('.anlayseTransaction').show();
			}
			else{
				$('.anlayseTransaction').hide();
			}
		});
	});

	$("#clear").on('click',function(){
		clear();
	})
});

function clear(){
	$("#fromDate,#toDate").val('');
	summarizedExpenseTable.fnClearTable();
	$('.anlayseTransaction').hide();
}
