$(document).ready(function() {
	loader();
	debugger;
	if ($('#thing').val() == 3) {
		var getVisitValue = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
		var labPatientId = parseInt($("#txtPatientID").val().replace ( /[^\d.]/g, '' ));
	}
	else if ($('#thing').val() == 4) {
		var getVisitValue = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
		var labPatientId = parseInt($("#txtPatientID").val().replace ( /[^\d.]/g, '' ));
	}
	else {
		var getVisitValue = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
		var labPatientId = parseInt($("#txtPatientIdHistory").val().replace ( /[^\d.]/g, '' ));
	}
	$("#PrintLabTest").addClass("hide");	
	var otable = $('#labResultsTbl').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"sAjaxSource":"controllers/admin/lab_test_result.php",
		"fnDrawCallback": function ( oSettings ) {
			
		}, 
		"fnServerParams": function ( aoData ) {
		  aoData.push( {"name": "operation", "value": "bindTableData" },
		  {"name": "visitId", "value": getVisitValue },
		  {"name": "paitentId", "value": labPatientId });
		},
		"aoColumns": [
			{  "mData": "lab_test_name" },
			{  "mData": "report" },
			{  "mData": function(o){
				var data = o;
				return "<i class='fa fa-eye viewLabDetails' style='font-size: 17px; margin-left: 20px; cursor:pointer;' onclick = 'view($(this))' title='view' value='view'></i>";
			} }			
		],
		aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [1],
            'aTargets':[2]
        }]
	});

	$("#printLabTestDetails").click(function(){
		if ($('#labResultsTbl').dataTable().fnGetData().length > 0) {
			$("#printContainerTable").html('');
			var postData ={
				"operation" : "bindTableDataByOrder",
				"paitentId" : labPatientId,
				"visitId" : getVisitValue
			}
			$.ajax({     
				type: "POST",
				cache: false,
				url: "controllers/admin/lab_test_result.php",
				data: postData,
				success: function(data) {
					if (data !='' && data !=null) {

						parseData = JSON.parse(data);
						visit_id = parseData[0].visit_id;
						var visitIdLength = visit_id.length;
					    for (var j=0;j<6-visitIdLength;j++) {
					        visit_id = "0"+visit_id;
					    }
					    visit_id = visitPrefix+visit_id;
						$('#lblPrintVisitId').text(visit_id);

						var patient_id = parseData[0].patient_id;
						var paitentIdLength = patient_id.length;
						for(var i =0;i<6-paitentIdLength;i++){
							patient_id = "0" + patient_id;
						}								
						patient_id = patientPrefix + patient_id;
						$('#lblPrintPatientId').text(patient_id);
						$('#lblPrintPatient').text(parseData[0].patient_name);						 
						var visitDate = convertDate(parseData[0].visit_date);
						$('#lblPrintVisitDate').text(visitDate);
						//hospital information through global varibale in main.js
						$("#lblPrintEmail").text(hospitalEmail);
						$("#lblPrintPhone").text(hospitalPhoneNo);
						$("#lblPrintAddress").text(hospitalAddress);
						if (hospitalLogo != "null") {
							imagePath = "./images/" + hospitalLogo;
							$("#printLogo").attr("src",imagePath);
						}
						$("#printHospitalName").text(hospitalName);	

						//Here all data from db is coming from group by order
						var dataPrevious =0;//varibale to match with last id
						var previousReport ;
						$.each(parseData,function(index,value){
							var labTestName = value['lab_test_name'];
							var labTestId = value['lab_test_id'];
							var html = '';
							//Check whether last id and comingg is same or not
							if (dataPrevious !=labTestId) {
								html ='<table id="codexpl" width="100%; height: 450px;">';
								html +=	'<label>Test Name: '+labTestName+'</label>';
								html +=	'<thead><tr><td width=25%>Component Name</td><td width=20%>Normal range</td><td width=15%>Result</td><td width=40%>Report</td></tr></thead>';
								html +=	'<tbody id="codexplBody'+index+'">';
								
								//This function returns all the data from db for that particular lab id		
								var data = parseData.filter(function (el) {
								  return el.lab_test_id == labTestId;
								});

								//Now data varibale has all the data of particular lab id 
								$.each(data,function(i,v){
									html += '<tr><td>'+v['name']+'</td><td>'+v['normal_range']+'</td><td>'+v['result']+'</td>';
									if (i ==0) {
										html += '<td rowspan='+data.length+'>'+v['report']+'</td>';
									}								
										
									html += '</tr>';
								});
								html +=	'</tbody>';
								html +=	'</table>';
							}
							dataPrevious = labTestId;
							$("#printContainerTable").append(html);											
						});
						//$.print("#PrintLabTest");
						window.print();
					}
				}
			});
		}			
	});

	$("#closemyLabTestModal").click(function(){
		$("#myLabTestModal").modal('hide');
		$('#labResultsViewTable').DataTable().fnDestroy();
	});

	$(".bodyOverflow").unbind();
	$(".bodyOverflow").click(function(){
		if ($("#myLabTestModal").attr('style') == "display: block;") {
			$('#labResultsViewTable').DataTable().fnDestroy();
		}			
	});

});
//function to calculate date
function convertDate(o){
    var dbDateTimestamp = o;
    var dateObj = new Date(dbDateTimestamp * 1000);
    var yyyy = dateObj.getFullYear().toString();
    var mm = (dateObj.getMonth()+1).toString(); // getMonth() is zero-based
    var dd  = dateObj.getDate().toString();
    return yyyy +"-"+ (mm[1]?mm:"0"+mm[0]) +"-"+ (dd[1]?dd:"0"+dd[0]);
}
function view(myThis){
	$('#myLabTestModal').modal();
	$('#myLabTestModalBodyLabel').text("Your Result");

	var row = myThis.closest("tr")[0];
	var currData = $('#labResultsTbl').dataTable().fnGetData(row);
	var labTestId = $(currData)[0]['lab_test_id'];
	var paitentId = $(currData)[0]['patient_id'];
	var visitId = $(currData)[0]['visit_id'];

	var labViewtable = $('#labResultsViewTable').DataTable({
        "bPaginate": false,
        'bSortable': false,
        "bAutoWidth": false      
    });
	var postData ={
		"operation" : "bindTableDataByOrder",
		"paitentId" : paitentId,
		"visitId" : visitId,
		"labTestId" : labTestId
	}
	$.ajax({     
		type: "POST",
		cache: false,
		url: "controllers/admin/lab_test_result.php",
		data: postData,
		success: function(data) {
			if (data !='' && data !=null) {
				labViewtable.fnClearTable();
				parseData = JSON.parse(data);

				for(var i=0;i<parseData.length;i++){
					var testname = parseData[i].lab_test_name;
					var componentName = parseData[i].name;
					var result = parseData[i].result;
					var normalRange = parseData[i].normal_range;
					labViewtable.fnAddData([testname,componentName, result,normalRange]);//send one column null so user can't remove previous data
				}
			}
		}
	});
	if ($(currData)[0]['report'] !='') {
		$("#individualReport").text($(currData)[0]['report']);
	}
	else {
		$("#individualReport").text("N/A");
	}
	
}