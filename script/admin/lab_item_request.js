var pharmacyRequestTable;
var i = 1;
$(document).ready(function(){
	debugger;
	
	var searchedDrug = '';
	$("#txtItem").on('keyup', function(){	
		//Autocomplete functionality 
		var currentObj = $(this);
		jQuery("#txtItem").autocomplete({
			source: function(request, response) {
				var postData = {
					"operation":"search",
					"itemName": currentObj.val()
				}
				
				$.ajax(
					{					
					type: "POST",
					cache: false,
					url: "controllers/admin/lab_item_request.php",
					datatype:"json",
					data: postData,
					
					success: function(dataSet) {
						loader();
						//searchedDrug = JSON.parse(dataSet);
						response($.map(JSON.parse(dataSet), function (item) {
							return {
								id: item.id,
								value: item.name,
								stock: item.quantity
							}
						}));
					},
					error: function(){
						
					}
				});
			},
            select: function (e, i) {
				var Id = i.item.id;
                currentObj.attr("item-id", Id);
                currentObj.val(i.item.name);
                var quantity = $("#txtQuantity").val();
                if(quantity == ''){
                	quantity = '0';
                }
                var row =  pharmacyRequestTable.fnGetNodes();
	 	
			 	var newStockTotal = Math.abs(parseFloat(quantity));
			 	var k = 0;
			 	var m =0;
			 	for(j=0; j<row.length; j++){
			 		var checkId = $(row[j]).find('td:last').find('input').attr('data-id');
			 		if(Id == checkId){
			 			newStockTotal += parseFloat($($(row[j]).find('td')[2]).text());
			 			k =j;
			 		}
			 		m++;
			 	}
			 	
			 	i.item.stock = i.item.stock - newStockTotal;
                $("#txtStock").val(i.item.stock);
            },
			minLength: 3
		});		
	});

	 pharmacyRequestTable = $('#tblLabItemRequest').dataTable({ // inilize datatable on load time.
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
		"bAutoWidth" : false,
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [3]
        }]
    });

	 $("#btnAdd").click(function(){
	 	var flag = false;

	 	if($("#txtStock").val() == ""){
	 		$("#txtStock").focus();
	 		$("#txtStockError").text("Stock is not available");
	 		$("#txtStock").addClass("errorStyle");
	 		flag = true;
	 	}

	 	if($("#txtQuantity").val() == ""){
	 		$("#txtQuantity").focus();
	 		$("#txtQuantityError").text("Please enter quantity");
	 		$("#txtQuantity").addClass("errorStyle");
	 		flag = true;
	 	}

	 	if($("#txtItem").val() == ""){
	 		$("#txtItem").focus();
	 		$("#txtItemError").text("Please enter item");
	 		$("#txtItem").addClass("errorStyle");
	 		flag = true;
	 	}

	 	if(flag == true){
	 		return false;
	 	}

	 	var item = $("#txtItem").val();
	 	var quantity = $("#txtQuantity").val();
	 	var id =  $("#txtItem").attr("item-id");
		var row =  pharmacyRequestTable.fnGetNodes();
	 	
	 	var newTotal = Math.abs(parseFloat(quantity));
	 	var k = 0;
	 	var m =0;
	 	for(j=0; j<row.length; j++){
	 		var checkId = $(row[j]).find('td:last').find('input').attr('data-id');
	 		if(id == checkId){
	 			newTotal += parseFloat($($(row[j]).find('td')[2]).text());
	 			k =j;
	 		}
	 		m++;
	 	}

	 	if($("#txtQuantity").val() > parseFloat($("#txtStock").val())){
	 		$('#messageMyModalLabel').text("Sorry");
			$('.modal-body').text("Stock is not available");
			$('#messagemyModal').modal();
	 	}

	 	else if(parseFloat(newTotal) == parseFloat(quantity) || m == 0){
	 		pharmacyRequestTable.fnAddData([i,item,newTotal,"<input type='button' id ='btnRemove' data-id="+id+" class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>"]);
	 		i++;
	 	}
	 	else{
	 		$($(row[k]).find('td')[2]).text(newTotal);
	 	}


	 	$("#txtItem").val("");
	 	$("#txtQuantity").val("");
	 	$("#txtStock").val("");
	 	$("#txtItem").attr("item-id","");

	 });

	 $("#txtItem").keyup(function(){
        if ($("#txtItem").val() != "") {
            $("#txtItemError").text("");
            $("#txtItem").removeClass("errorStyle");
            $("#txtStockError").text("");
            $("#txtStock").removeClass("errorStyle");
        }
	});


	$("#txtQuantity").keyup(function(){
        if ($("#txtQuantity").val() != "") {
            $("#txtQuantityError").text("");
            $("#txtQuantity").removeClass("errorStyle");
        }
	});

	 $("#btnSaveRequest").click(function(){
	 	var requestItem = [];
	 	var aiRows = pharmacyRequestTable.fnGetNodes(); //Get all rows of data table

	 	if(aiRows.length == 0){
	 		return false;
	 	}
	 	for (var j=0,c=aiRows.length; j<c; j++) {			
			var obj = {};
			obj.quantity = $($(aiRows[j]).find('td')[2]).text();
			obj.itemId = $(aiRows[j]).find('input').attr('data-id');
			requestItem.push(obj);
		}
		var postData ={
			"operation":"save",				
			"requestItem":JSON.stringify(requestItem)
		}			

		$.ajax({					
			type: "POST",
			cache: false,
			url: "controllers/admin/lab_item_request.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "" && data != "0"){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Saved Successfully!!!");
					$('#messagemyModal').modal();
					clear();
				}
				
			},
			error: function(){
				
			}
		});

	 });

	 $("#btnReset").click(function(){
	 	clear();
	 	i = 1;
	 });
});

/**Row Delete functionality**/
function rowDelete(currInst) {
    var row = currInst.closest("tr").get(0);
    pharmacyRequestTable.fnDeleteRow(row);
}

function clear() {
    $("#txtQuantity").val("");
    $("#txtItem").val("");
    $("#txtStock").val("");
    $("#txtQuantityError").text("");
    $("#txtItem").val("");
    $("#txtStockError").text("");
    $("#txtItem").removeClass("errorStyle");
    $("#txtItem").focus();
    $("#txtQuantity").removeClass("errorStyle");
    $("#txtStock").removeClass("errorStyle");
    pharmacyRequestTable.fnClearTable();
    i = 1;
}