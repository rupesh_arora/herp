$(document).ready(function() {
	debugger;
	$(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});

	$("#toDate").attr("disabled",true);
	$("#fromDate").attr("disabled",true);	

	$('.changeRadio').change(function() {
		if ($('#radToFromDate').prop( "checked") == true) {
	        $("#toDate").attr("disabled",false);
			$("#fromDate").attr("disabled",false);
	    }
	    else{
	    	$("#toDate").attr("disabled",true);
			$("#fromDate").attr("disabled",true);
			$("#toDate").val("");
			$("#fromDate").val("");
	    }
	});	
	$("#fromDate").change(function() {
		if ($("#fromDate").val() !='') {
			$("#fromDate").removeClass("errorStyle");
			$("#txtFromDateError").text("");
		}
	});
	$("#toDate").change(function() {
		if ($("#toDate").val() !='') {
			$("#toDate").removeClass("errorStyle");
			$("#txtToDateError").text("");
		}
	});	

	$("#clear").click(function(){
		clear();
	});
	google.charts.setOnLoadCallback(drawChart);
});

function drawChart() {
	var dataLoad;

	var countServicesArray = [['Service Name','Paid','Unpaid']];
	var revenuePerServiceArray = [['Service Name','Paid','Unpaid']];
	var overAllRevenueArray = [['Paid','Unpaid']];

	$("#viewReport").on("click",function(){
		var fromDate ;
		var toDate ;
		var arr = new Array();
		var opts = new Array();
		if ($('#radToday').prop( "checked") == true) {
			dataLoad = "today";
		}
		else if($('#radLastSevenDays').prop( "checked") == true) {
			dataLoad = "last7days";
		}
		else if($('#radToFromDate').prop( "checked") == true) {
			dataLoad = "betweenDate";
			fromDate = $("#fromDate").val();
			toDate = $("#toDate").val();
			if (fromDate =='') {
				$("#txtFromDateError").text("Please choose from date");
				$("#fromDate").addClass("errorStyle");
				$("#fromDate").focus();
				return false;
			}
			if (toDate =='') {
				$("#txtToDateError").text("Please choose from date");
				$("#toDate").addClass("errorStyle");
				$("#toDate").focus();
				return false;
			}
		}
		else if($('#radLastThirtyDays').prop( "checked") == true) {
			dataLoad = "last30days";
		}
		else if($('#radAllTime').prop( "checked") == true) {
			dataLoad = "all";
		}

		if (fromDate == undefined) {
			fromDate ="";
		}
		if (toDate == undefined) {
			toDate = "";
		}
		if (dataLoad==undefined) {
			return false;
		}
		var postData = {
			"operation" : "showChartData",
			"dataLoad" : dataLoad,
			"fromDate" : fromDate,
			"toDate" : toDate
		}
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/service_wise_report.php",
			data: postData,
			success: function(data) {
				if(data != null && data != ""){
					var parseData =  JSON.parse(data);
					if (parseData[1] =='' &&parseData[3] =='' && parseData[5] =='' && parseData[5] =='' &&
						parseData[7] =='' && parseData[0][0]['Paid Amount'] =='' && parseData[2][0]['Unpaid Amount'] ==''
						&& parseData[2][0]['Paid Amount'] =='' && parseData[2][0]['Unpaid Amount'] ==''
						&& parseData[4][0]['Paid Amount'] =='' && parseData[4][0]['Unpaid Amount'] ==''
						&& parseData[6][0]['Paid Amount'] =='' && parseData[6][0]['Unpaid Amount'] ==''
						&& parseData[8][0]['Paid Amount'] =='' && parseData[8][0]['Unpaid Amount'] =='') {
						clear();
						$('#messagemyModal').modal();
						$('#messageMyModalLabel').text("Alert");
						$('.modal-body').text("No data exist!!!");
						$("#btnSaveExcel,#btnSavePdf").hide();
						return false;
					}
					$(".chartDiv,#btnSaveExcel,#btnSavePdf").show();

					countServicesArray = [['Service Name','Paid','Unpaid']];
					revenuePerServiceArray = [['Service Name','Paid','Unpaid']];
					overAllRevenueArray = [['Paid','Unpaid']];

					$.each(parseData,function(index,v){
						//code for excel file
						if (parseData[index] !='') {
							if (parseData[index][0]['Paid Amount'] !='' || parseData[index][0]['Unpaid Amount'] !='' ) {
								arr.push(parseData[index]);

								if (index == 0) {
									opts.push({sheetid:'Services wise Service taken',header:true});
								}
								else if (index == 1) {
									opts.push({sheetid:'Services wise Service Bill',header:true});
								}
								else if (index == 2) {
									opts.push({sheetid:'Over all Service Bill',header:true});
								}								
							}//code for excel file end
						}
							

						for(var i=0;i<parseData[index].length;i++){
							if(index==0){

								smallServicesArray = [];
								smallServicesArray.push(parseData[index][i]['Service Name']);
								smallServicesArray.push(parseInt(parseData[index][i]['Sevices Taken']));
								smallServicesArray.push(parseInt(parseData[index][i]['Sevices Untaken']));
								countServicesArray.push(smallServicesArray);
							}
							else if (index == 1) {

								smallrevenuePerServiceArray = [];
								smallrevenuePerServiceArray.push(parseData[index][i]['Service Name']);
								smallrevenuePerServiceArray.push(parseInt(parseData[index][i]['Paid Amount']));
								smallrevenuePerServiceArray.push(parseInt(parseData[index][i]['Unpaid Amount']));
								revenuePerServiceArray.push(smallrevenuePerServiceArray);
							}

							else if (index == 2) {

								$.each(parseData[index][i],function(i,v){
									overAllRevenueArray.push([i,parseInt(v)]);
								});
							}
						}
					});
					
					if (countServicesArray.length > 1) {
						var dataGpLabBill = google.visualization.arrayToDataTable(countServicesArray);
						var optionsGpLabBill = {
					      title: 'Services wise Service Analysis','width':1200,'height':1000,isStacked: true
					    };
					    var chartGpLabBill = new google.visualization.BarChart(document.getElementById('chart_div'));
					    chartGpLabBill.draw(dataGpLabBill, optionsGpLabBill);
					    $('#chart_div').show();
					}
					else{
						$('#chart_div6').hide();
					}

					if (revenuePerServiceArray.length > 1){
						var dataGpRadiologyBill = google.visualization.arrayToDataTable(revenuePerServiceArray);
						var optionsGpRadiologyBill = {
					      title: 'Services wise Amount Analysis','width':1200,'height':1000,isStacked: true
					    };
					    var chartGpRadiologyBill = new google.visualization.BarChart(document.getElementById('chart_div7'));
					    chartGpRadiologyBill.draw(dataGpRadiologyBill, optionsGpRadiologyBill);
					    $('#chart_div7').show();
					}
					else{						 
						$('#chart_div7').hide();
					}

					if (overAllRevenueArray.length > 1) {
						var dataLabBill = google.visualization.arrayToDataTable([['','']]);
						var dataLabBill = google.visualization.arrayToDataTable(overAllRevenueArray);
						var optionsLabBill = {
					      title: 'Over all Service Analysis','width':1200,'height':600,is3D:true
					    };
					    var chartLabBill = new google.visualization.PieChart(document.getElementById('chart_div8'));
					    chartLabBill.draw(dataLabBill, optionsLabBill);
					}
					else{
						$('#chart_div8').hide();
					}				
				}

				$('svg').css("max-width",$("#page-content").width()-50);//max-width for graph
				
				/*Scroll down code*/
				$('html, body').animate({
			        scrollTop: $("#chart_div").offset().top
			    }, 1000);

				$("#btnSaveExcel").show();

				/*function to save in to excel file*/
				window.saveFile = function saveFile () {

					var res = alasql('SELECT INTO XLSX("Service-Report.xlsx",?) FROM ?',[opts,arr]);
				}
			}
		});
	});

	$("#btnSavePdf").click(function(){
		$.print("#printableArea");
	});
}
function clear(){
	$(".chartDiv,#btnSaveExcel,#btnSavePdf").hide();
	$("#toDate,#fromDate").val("");
	$("#txtFromDateError,#txtToDateError").text("");
	$("#fromDate,#toDate").removeClass("errorStyle");
}