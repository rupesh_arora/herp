var labTypeTable;
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   lab_type.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages  lab type data
 *************************************************************************************************** */
  loader();

    $("#form_dept").hide();
	$("#lab_type_list").addClass('list');
	$("#tab_lab_type").addClass('tab-list-add');
	
	//Click function for change the tab
    $("#tab_add_lab_type").click(function() {
        $("#form_dept").show();
        $(".blackborder").hide();
        $('#btnSave').show();
		$('#btnReset').show();
		$('#btnUpdate').hide();
		clear();
        $("#tab_add_lab_type").addClass('tab-detail-add');
        $("#tab_add_lab_type").removeClass('tab-detail-remove');
        $("#tab_lab_type").removeClass('tab-list-add');
        $("#tab_lab_type").addClass('tab-list-remove');
        $("#lab_type_list").addClass('list');
		$("#txtLabTypeError").text("");
        $("#txtLabType").removeClass("errorStyle");
		$('#txtLabType').focus();
    });
	
	//Click function for show the lab list
    $("#tab_lab_type").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
		tabLabType();    //Calling the function for show the lab type list    
    });
	
	// on change of select file type
	$("#file").on("change", function() {
		$('#txtFileError').hide();
	});
	
	// import excel on submit click
	$('#importDataFile').click(function(){
		var flag = false;
		if($('#file').prop('files')[0] == undefined || $('#file').val() == "") {
			$('#txtFileError').show();
			$('#txtFileError').text("Please choose file.");
			flag  = true;
		}
		if(flag == true){
			return false;
		}
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to upload this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			var file_data = $('#file').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: 'controllers/admin/lab_type.php', // point to server-side PHP script 
				dataType: 'text', // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					if(data != "" && data != "invalid file" && data == "1"){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Your file has been successfully imported!!!");
						$('#messagemyModal').modal();
						$('#file').val("");
					}
					else if(data == "invalid file"){
						$('#txtFileError').show();
						$('#txtFileError').text("Please import only CSV/XLS file.");
						$('#file').val("");
					}
					else if (data =="You must have two column in your file i.e lab type and description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="First column should be lab type" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Data can't be null in lab type column" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Second column should be description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Please insert data in first column and second column only i.e A & B") {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
					else if(data == ""){
						$('#txtFileError').show();
						$('#txtFileError').text("Data already exist.");
						$('#file').val("");
					}
				},
				error: function() {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Data Not Found!!!");
					$('#messagemyModal').modal();

				}
			});
		});
	});
	
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		LoadDataTable();
    }
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	labTypeTable.fnClearTable();
	    	labTypeTable.fnDestroy();
	    	//Datatable code
			labTypeTable = $('#labTypedatatable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  labTypeTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/lab_type.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					{ "mData": "type" },
					{ "mData": "description" },	
					{  
						"mData": function (o) { 
						
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},	
				],
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 1 ] },
				   { 'bSortable': false, 'aTargets': [ 2 ] }
			   ]
			} );
		}
		else{
			labTypeTable.fnClearTable();
	    	labTypeTable.fnDestroy();
	    	LoadDataTable();
		}
    });
	
	//Click function for validate and save the data
    $("#btnSave").click(function() {
		
		var flag = "false";
		
		$("#txtLabType").val($("#txtLabType").val().trim());
		
        if ($("#txtLabType").val() == '') {
			$('#txtLabType').focus();
            $("#txtLabTypeError").text("Please enter lab type");
            $("#txtLabType").addClass("errorStyle");
            flag = "true";
        }
		
		if(flag == "true"){			
			return false;
		}
		
		//ajax call
		
		var labName = $("#txtLabType").val();
		var description = $("#txtDescription").val();
		description = description.replace(/'/g, "&#39");
		var postData = {
			"operation":"save",
			"labName":labName,
			"description":description
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/lab_type.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Lab type saved successfully!!!");
					$('#messagemyModal').modal();
					$('#inactive-checkbox-tick').prop('checked', false).change();
					tabLabType();
					clear();
				}
				else{
					$("#txtLabTypeError").text("Lab is already exists");
					$('#txtLabType').focus();
					$("#txtLabType").addClass("errorStyle");
				}				
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
		
    });

    //keyup functionality
    $("#txtLabType").keyup(function() {
        if ($("#txtLabType").val() != '') {
            $("#txtLabTypeError").text("");
            $("#txtLabType").removeClass("errorStyle");
        } 
    });   
	
});//Close document.ready function 

//Function for edit button
function editClick(id,type,description){	
	$('#tab_add_lab_type').html("+Update Lab Type");
	$('#tab_add_lab_type').click();
	$('#btnSave').hide();
	$('#btnReset').hide();
	
	$("#btnUpdate").removeAttr("style");
	$("#txtLabTypeError").text("");
    $("#txtLabType").removeClass("errorStyle");
	$('#txtLabType').val(type);
	$('#txtDescription').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id);
	$("#uploadFile").hide();
	//Click function for update the data
	$("#btnUpdate").click(function(){
		var flag = validation();
		if(flag == "true"){			
			return false;
		}
		else{
			var labName = $("#txtLabType").val();
			var description = $("#txtDescription").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();			
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){	
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/lab_type.php",
					data :{            
						"operation" : "update",
						"id" : id,
						"labName":labName,
						"description":description
					},
					success: function(data) {	
						if(data != "0" && data != ""){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Lab type updated successfully!!!");
							labTypeTable.fnReloadAjax();
							$('#messagemyModal').modal();
							clear();
							$('#tab_lab_type').click();
						}
						else{
							$('.close-confirm').click();
							$("#myModal #txtLabTypeError").text("Lab is already exists");
							$('#myModal #txtLabType').focus();
							$("#myModal #txtLabType").addClass("errorStyle");
						}
					},
					error:function()
					{
						$('.close-confirm').click();
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();			  
					}
				});
			});
		}
	});
	
	$('#myModal #txtLabType').keyup(function() {
        if ($("#myModal #txtLabType").val() != '') {
            $("#myModal #txtLabTypeError").text("");
            $("#myModal #txtLabType").removeClass("errorStyle");
        }
    });
}

//Click function for delete button
function deleteClick(id){	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete Lab Type");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteLabType("'+type+'");');	//Calling the deleteLabType function for delete the data 
}

function restoreClick(id){
	$('#selectedRow').val(id);	
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Restore Lab Type");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal(); 
	
	var type="restore";
	$('#confirm').attr('onclick','deleteLabType("'+type+'");'); //Calling the deleteLabType function for restore the data 
}

//Function for delete and restore the data
function deleteLabType(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();		
			
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/lab_type.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Lab type  deleted successfully!!!");
					labTypeTable.fnReloadAjax();
					$('#messagemyModal').modal();
				}
				else{
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("This lab is used , so you can not delete!!!");
					$('#messagemyModal').modal();
				}
			},
			error:function()
			{
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
		
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/lab_type.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Lab type restored successfully!!!");
					labTypeTable.fnReloadAjax();
					$('#messagemyModal').modal();
				}			
			},
			error:function()
			{
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();			  
			}
		});
	}
}

//Function for show the list 
function tabLabType(){
	$("#form_dept").hide();
	$(".blackborder").show();
	$('#tab_add_lab_type').html("+Add Lab Type");
	$("#tab_add_lab_type").removeClass('tab-detail-add');
	$("#tab_add_lab_type").addClass('tab-detail-remove');
	$("#tab_lab_type").addClass('tab-list-add');
	$("#tab_lab_type").removeClass('tab-list-remove');
	$("#lab_type_list").addClass('list');
}

// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		 $(".close").click();
    }
});

//Click function for reset the button 
$("#btnReset").click(function(){
	$("#txtLabType").removeClass("errorStyle");
	clear();
	$("#txtLabType").focus();
});


function clear(){
	$('#txtLabType').val("");
	$('#txtLabTypeError').text("");
	$('#txtFileError').text('');
	$('#txtDescription').val("");
}

function LoadDataTable(){
	labTypeTable = $('#labTypedatatable').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {
			$('.update').unbind();
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = labTypeTable.fnGetData(data);
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					var type = mData["type"];
					var description = mData["description"];
					editClick(id,type,description);
   
				}
			});
			$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  labTypeTable.fnGetData(data);
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);
				}
			});
		},
		
		"sAjaxSource":"controllers/admin/lab_type.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{ "mData": "type" },
			{ "mData": "description" },	
			{  
				"mData": function (o) { 
				var data = o;
				return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
				   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
				   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
				   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
				   "  data-original-title='Delete'></i>"; 
				}
			},	
		],
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 1 ] },
		   { 'bSortable': false, 'aTargets': [ 2 ] }
	   ]
	} );
}
function validation(){
	var flag = "false";
		
		$("#txtLabType").val($("#txtLabType").val().trim());
		
		if ($("#txtLabType").val() == '') {
			$('#txtLabType').focus();
			$("#txtLabTypeError").text("Please enter lab type");
			$("#txtLabType").addClass("errorStyle");
			flag = "true";
		}
		return flag;
}
//# sourceURL=filename.js