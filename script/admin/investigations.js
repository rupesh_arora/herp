var investigationTable ;//global variable investigationTable
$(document).ready(function() {
	/* ****************************************************************************************************
 * File Name    :   investigations.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   4th mar, 2016
 * Description  :   This page  manages ipd ivestigations
 *************************************************************************************************** */	
	loader();
	debugger;
	var patientMiddleName;
	$('#txtIPDId').focus();

	$(".amountPrefix").text(amountPrefix);//for amount prefix

	/*reset button functionality*/
	$("#btnReset").click(function(){
		clear();
		$('#txtIPDId').focus();
	});

	/**DataTable Initialization**/
	investigationTable = $('#viewPrice').DataTable({
		aLengthMenu: [
	        [25, 50, 100, 200, -1],
	        [25, 50, 100, 200, "All"]
	    ],//declare so that show all rows
	    iDisplayLength: -1,
		aoColumnDefs: [
		   { aTargets: [ 5 ], bSortable: false },
		]
	});
	
	/**Binding data in drop downs**/
	bindLabType();

	$('#txtIPDId').focus();//focus on patient id

	

	/*Binding Lab Type*/
	function bindLabType(){
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/investigations.php",
			data: {
			"operation":"showLabType"
			},
			success: function(data) { 
				if(data != null && data != ""){
					var parseData= jQuery.parseJSON(data); // parse the value in Array string  jquery

					var option ="<option value='-1'>--Select--</option>";
					for (var i=0;i<parseData.length;i++)
					{
						option+="<option value='"+parseData[i].id+"'>"+parseData[i].type+"</option>";
					}
					$('#selLabType').html(option);          
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}

	/*Binidng Lab Test*/
	function bindLabTest(value){
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/investigations.php",
			data: {
			"operation":"showLabTest",
			"value" : value
			},
			success: function(data) { 
				if(data != null && data != ""){

					var parseData= jQuery.parseJSON(data); // parse the value in Array string  jquery

					var option ="<option value='-1'>--Select--</option>";
					for (var i=0;i<parseData.length;i++)
					{
						option+="<option value='"+parseData[i].id+"' data-name='"+parseData[i].name+"'>"+parseData[i].name+"</option>";
					}
					$('#selLabTest').html(option);          
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}

	/*Binidng Lab Test*/
	function bindLabTestComponent(value){
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/investigations.php",
			data: {
			"operation":"showLabTestComponent",
			"labTestId" : value
			},
			success: function(data) { 
				if(data != null && data != ""){

					var parseData= jQuery.parseJSON(data); // parse the value in Array string  jquery

					var option ="<option value='-1'>--Select--</option>";
					for (var i=0;i<parseData.length;i++)
					{
						option+="<option value='"+parseData[i].id+"' data-cost='"+parseData[i].fee+"' lab_test_id='"+parseData[i].lab_test_id+"' data-name='"+parseData[i].name+"'>"+parseData[i].name+"</option>";
					}
					$('#selLabTestComponent').html(option);          
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}
	
	
	
	/*On changing the lab type calling the bindLabTest function*/
	$("#selLabType").change(function(){
		var option = "<option value='-1'>Select</option>";
		if($("#selLabType :selected") != -1){
			var value = $("#selLabType :selected").val();
		}
		else{
			$("#selLabTest").html(option);
		}
		bindLabTest(value);//on change of lab type call this function
	});

	/*On changing the lab type calling the bindLabTest function*/
	$("#selLabTest").change(function(){
		var option = "<option value='-1'>Select</option>";
		if($("#selLabTest :selected") != -1){
			var value = $("#selLabTest :selected").val();
		}
		else{
			$("#selLabTestComponent").html(option);
		}
		bindLabTestComponent(value);//on change of lab type call this function
	});

	
	/*Function to add to bill*/
	function addToBill(elemId,type,quantity,alertMsg){
		
		var cost = $("#"+elemId+" option:selected").attr('data-cost');//getting cost of selected element
		var id = $("#"+elemId).val();
		var name = $("#"+elemId+" option:selected").attr('data-name');
		if(alertMsg == "lab"){
			var labTestId = $("#"+elemId+" option:selected").attr('lab_test_id');
		}
		
		var totalPrice = cost * quantity;	
		
		var aiRows = investigationTable.fnGetNodes();
		var previousCount = 0;	
		var arrTests = [];//creating array

		/*Check data already exist in datatable i.e remove duplicasy*/
		if($(aiRows).length > 0) {
			arrTests = $(aiRows).find('td:eq(1)').map(function(index, td) {
		        return $(this).parent().attr('data-id') + "_" + $(this).text();
		    });
		}

		var testType = id + "_" + name;

		/*Compare data exist already*/
	    if($.inArray(testType, arrTests) != -1)
	    {
			/*$('#mySelfRequestErrorModal').modal();
			$('#selfRequestErrorModalLabel').text("Error");*/
			$("#testStatus").text('');
			$('#addedStatus').text("This data already exist!!!");
			return true;
	    }
		/*Add new row in datatable*/
		if(alertMsg == "lab"){
			var newRow = investigationTable.dataTable().fnAddData([type,name,cost,quantity,totalPrice,"<input type='button' class='lab' onclick='rowDelete($(this));' lab-id='"+labTestId+"' value='Remove'>",id]);
		}
		else{
			var newRow = investigationTable.dataTable().fnAddData([type,name,cost,quantity,totalPrice,"<input type='button' onclick='rowDelete($(this));' value='Remove'>",id]);
		}
		FadeOut();
		if(alertMsg=="lab") {
			$("#testStatus").text("Lab test added successfully!!!");
		}

		//change cell attributes (add data attribute id with datatable so to comapre data alreday exist)
		var oSettings = investigationTable.fnSettings();
		var nTr = oSettings.aoData[ newRow[0] ].nTr;
		$('td', nTr).parent().attr( 'data-id', id );

		$('#totalBill').text(parseInt($('#totalBill').text())+parseInt(totalPrice));//getting total bill
	}
	
	/**Add click events**/
	
	$("#addLabService").click(function(){
		/*Apply validation on this click event*/
		var flag = false;
		if($("#selLabType").val() == "-1"){
			$("#selLabType").focus();
			$("#selLabTypeError").text("Please choose lab type");
			flag = true;
		}
		if($("#selLabTest").val() == "" || $("#selLabTest").val() == "-1"){
			$("#selLabTest").focus();
			$("#selLabTestError").text("Please choose lab test");
			flag = true;
		}
		if($("#selLabTestComponent").val() == "" || $("#selLabTestComponent").val() == "-1"){
			$("#selLabTestComponent").focus();
			$("#selLabTestComponentError").text("Please choose test component");
			flag = true;
		}
		if (flag == true) {
			return false;
		}//end validation

		addToBill("selLabTestComponent","Lab Test",1,"lab");
		FadeOut();
	});//end click function


	$(".slfBtn").on("click",function(){
		$(".slfBtn").removeClass("activeSelf");
		$(this)	.addClass("activeSelf");

		$('.myHideClass').hide();

		$("#testStatus").text('');
		$('#addedStatus').text('');

		var clickedId = $(this).attr("id");

		if(clickedId == "addLabPopUp"){
			$('.myLabClass').show();			
		}	
	});

	/*Load patient details*/
	$("#btnLoadPatientId").click(function(){

		var flag= false;
		if($("#txtIPDId").val() ==""){
			$("#txtIPDIdError").text("Please enter ipd id");
			$("#txtIPDId").focus();
			$("#txtIPDId").addClass("errorStyle");       
            flag = true;
		}
		if($("#txtIPDId").val().length != 9 ){
			$("#txtIPDIdError").text("Please enter valid ipd id");
			$("#txtIPDId").focus();
			$("#txtIPDId").addClass("errorStyle");       
            flag = true;
		}
		

		if (flag ==true) {
			return false;
		}
		if ($("#txtIPDId").val !="") {
			$("#txtIPDId").removeClass("errorStyle");
			$("#txtIPDIdError").text("");
		}
		var ipdId =  $("#txtIPDId").val();
		var ipdPrefix = ipdId.substring(0, 3);
		ipdId = ipdId.replace ( /[^\d.]/g, '' ); 
		ipdId = parseInt(ipdId);

		var postData = {
			"operation":"patientDetail",
			"ipdId":ipdId,
			"ipdPrefix":ipdPrefix
		}	
		/*AJAX call to get patient details*/
		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/investigations.php",
			datatype:"json",
			data: postData,
			success: function(data) {
				if(data != "0" && data != ""){
					var parseData = jQuery.parseJSON(data);//parse JSON data 
					$("#txtPatientFirstName").val(parseData[0].first_name);//setting in to the text box
					$("#txtPatientLastName").val(parseData[0].last_name);
					$("#txtPatientMobile").val(parseData[0].mobile);
					$("#txtPatientEmail").val(parseData[0].email);
					$("#thing").val(parseData[0].salutation);
					var patientId = parseInt(parseData[0].patient_id);
                    var prefix = parseData[0].prefix;
                    
                    var patientIdLength = patientId.toString().length;
                    for (var i=0;i<6-patientIdLength;i++) {
                        patientId = "0"+patientId;
                    }
                    patientid = prefix+patientId;
                    patientHospitalizationId = patientid;
                    $('#txtPatientId').val(patientid);
					patientMiddleName = parseData[0].last_name;
					$("#txtIPDId").attr("disabled", "disabled"); 
					$(".showOnClickLoad").css({"display":"block"});
				}
				if (data == "0") {
					$('#messagemyModal').modal("show");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Patient id not exist");
					clear();
				}
				if (data == "") {
					$("#txtIPDIdError").text("Please enter ipd id");
					$("#txtIPDId").focus();
					$("#txtIPDId").addClass("errorStyle");  
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	});

	/*Save data to database*/
	$("#btnSave").click(function(){

		/*perform validation*/
		var flag=false;
		if (investigationTable.fnGetData().length < 1) {
			$('#messagemyModal').modal("show");
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Choose any of the one lab test");
			$("#selServiceType").focus();
			flag=true;
		}
		if($("#txtIPDId").val() ==""){
			$("#txtPatientIdError").text("Please enter ipd id");
			$("#txtIPDId").focus();
			$("#txtIPDId").addClass("errorStyle");       
            flag = true;
		}
		if($("#txtPatientFirstName").val() ==""){
			$("#txtIPDIdError").text("Please load ipd id");
			$("#txtIPDId").focus();
			$("#txtIPDId").addClass("errorStyle");       
            flag = true;
		}		
		if (flag==true) {
			return false;
		}

		/*Defining array outside loop so to hold multiple values*/
		

		var Price_l = new Array();
		var Price = new Array();
		var bigPrice_l = new Array();

		var tempArray =  new Array();

		var tempId = "0";
		var componentId = "0";
		var tempTotal = 0;
		var isFirst = "1";
		var j = 1;

		var aiRows = investigationTable.fnGetNodes();
		var labLength = $(aiRows).find('td:last').find('.lab').length;
		
		for (i=0,c=aiRows.length; i<c; i++) {
			iRow = aiRows[i];   // assign current row to iRow variable
			aData = investigationTable.fnGetData(iRow); // Pull the row
			 
			// Getting the first column then compare result and then store i9n diffrent database table
			sType = aData[0];

			/*If column type is equals to service then fetch all service type data*/
			if((sType == "Lab Test")){
				/*Putting array value into a variable*/
				var labId = $(aData[5]).attr('lab-id');
				if(tempId == "0"){
					var Obj = {};
					Obj.labId = labId;
					Obj.componentId = aData[6];
					Obj.componentName = aData[1];
					Obj.price = aData[2];
					tempTotal = parseInt(aData[4]);
					tempArray.push(Obj);
				}
				if(tempId != "0"){
					if(tempId == labId){
						var Obj = {};
						Obj.labId = tempId;
						Obj.componentId = aData[6];
						Obj.componentName = aData[1];
						Obj.price = aData[2];
						tempTotal = tempTotal + parseInt(aData[4]);								
						tempArray.push(Obj);
					}
					else{
						//Price_l = new Array();//so that hold only one value after traverse
						//Price_l.push(tempArray);

						bigPrice_l.push(tempArray);
						var newObj = {};
						newObj.labId = tempId;
						newObj.total = tempTotal;
						Price_l.push(newObj);
						Price.push(Price_l);
						tempArray =  new Array();
						Price_l = new Array()
						tempId = labId;
						var Obj = {};
						Obj.labId = tempId;
						Obj.componentId = aData[6];
						Obj.componentName = aData[1];
						Obj.price = aData[2];
						tempTotal = parseInt(aData[4]);
						tempArray.push(Obj);
					}
				}
				else{
					tempId = labId;
				}
				if(labLength == j){
					
					//Price_l.push(tempTotal,componentId,tempId);
					var newObj = {};
					if(tempId == "0"){
						newObj.labId = labId;
					}	
					else{
						newObj.labId = tempId;
					}
					newObj.total = tempTotal;
					Price_l.push(newObj);
					Price.push(Price_l);

					bigPrice_l.push(tempArray);
					Price_l = new Array();//so that hold only one value after traverse
					
				}
				j++;				
			}
		}

		var ipdId =  $("#txtIPDId").val().replace ( /[^\d.]/g, '' );
		ipdId = parseInt(ipdId);
		
		var patientId =  $("#txtPatientId").val();
		var patientPrefix = patientId.substring(0, 3);
		patientId = patientId.replace ( /[^\d.]/g, '' ); 
		patientid = parseInt(patientId);
		var grandTotal = $("#totalBill").text();
		var remarks   = $("#txtRemarks").val();
		var postData ={
			"operation":"saveDataTableData",
			"bigPrice_l":JSON.stringify(bigPrice_l),
			"Price":JSON.stringify(Price),
			"patientId" : patientid,
			"prefix":patientPrefix,
			"ipdId"   : ipdId,
			"grandTotal" : grandTotal
		}

        $('#confirmMyModalLabel').text("Save Request");
        $('.selfRequestConfirm-body').text("Are you sure that you want to save this?");
        $('#confirmmyModal').modal();
        $('#confirm').unbind();
        $('#confirm').on('click',function(){
		/*AJAX call to save data*/	
			$.ajax({
				type: "POST",
				cache: false,
				url: "controllers/admin/investigations.php",
				datatype:"json",
				data: postData,
				success : function(data){
					if(data !="" && data != "0" && data != "2"){
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Request saved successfully!!!");
						$('#messagemyModal').modal();
						clear();/*
						var patientID =  $("#txtPatientId").val();
						var salu = $("#thing").val();
						var f_name = $("#txtPatientFirstName").val();
						var l_name = $("#txtPatientLastName").val();
						$('#mySelfModal').modal("show");
						$('#selfModalLabel').text("Success");
						$("#success").text("Request saved successfully!!!");
						$("#mySelfModal #lblPatientId").text(patientID);
						var labName = "";
						var testName = "";
						
						if(bigPrice_l.length != 0 )
						{	
							for(j = 0 ;j < bigPrice_l.length ; j++){
								$(bigPrice_l[j]).each(function(){
									labName = labName + this.componentName + ','+ "\n";
								});							   						  
							}
							testName = testName + labName + "\n";						
						}

						
							
						var parseData = jQuery.parseJSON(data);
						var visitId = parseData[0];
						var patientPrefix = parseData[1];
							
						$("#lblName").text(salu +" "+f_name+" "+patientMiddleName+" "+l_name);
					
						clear();*/
					}
					if(data =="2"){
						$('#messageMyModalLabel').text("Sorry");
						$('.modal-body').text("Insufficient balance!!!");
						$('#messagemyModal').modal();
					}
					if(data ==""){
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Something awful happened!! Can't fetch old requests. Please try to contact admin.");
						$('#messagemyModal').modal();
					}
					if(data =="0"){
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Please enter valid patient id");
						$('#messagemyModal').modal();
						clear();
					}
				},
				error:function() {
					$('#messagemyModal').modal();
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				}
			});
		});
	});	

	/*Key up functionality*/	
	$("#txtIPDId").keyup(function(){
		if ($("#txtIPDId").val() !="") {
			$("#txtIPDIdError").text("");
			$("#txtIPDId").removeClass("errorStyle");			
		}
	});
	$("#selLabTest").change(function(){
		if ($("#selLabTest").val() != "") {
			$("#selLabTestError").text("");
			$("#selLabTest").removeClass("errorStyle");
		}	
	});
	$("#selLabType").change(function(){
		if ($("#selLabType").val() != "") {
			$("#selLabTypeError").text("");
			$("#selLabType").removeClass("errorStyle");
		}	
	});
	$("#selLabTestComponent").change(function(){
		if ($("#selLabTestComponent").val() != "") {
			$("#selLabTestComponentError").text("");
			$("#selLabTestComponent").removeClass("errorStyle");
		}	
	});
	//End key up functionality	
	
	$("#searchPatientIcon").click(function() {			// show popup for search patient
		$('#txtIPDId').removeClass('errorStyle');
        $('#InvestigationModalLabel').text("Search IPD Patients");
		$("#hdnInvestigation").val("1");
        $('#InvestigationModalBody').load('views/admin/view_patients.html');
        $('#myInvestigationModal').modal();
    });
	
	$('select ').change(function(){
		$('#testStatus').text('');
		$('#addedStatus').text('');
	});

	$('#txtIPDId').keypress(function (e) {
	    if (e.which == 13) {
		    $("#btnLoadPatientId").click();
    	}
 	});
});//end document ready

/**Row Delete functionality**/
function rowDelete(currInst) {
	var row = currInst.closest("tr").get(0);
	var oTable = $('#viewPrice').dataTable();
	oTable.fnDeleteRow(oTable.fnGetPosition(row));
	$('#totalBill').text(parseInt($('#totalBill').text())-parseInt($(row).find('td:eq(4)').text()));//subtract bill from total amount
}
function numberValidation(number) {
    floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
    return floatRegex.test(number);
}
function clear(){
	$("#txtPatientFirstName").val("");
	$("#txtPatientMobile").val("");
	$("#txtPatientEmail").val("");
	$("#txtPatientLastName").val("");
	$("#selLabType").val("-1");
	$("#selLabTestComponent").val("-1");
	$("#selLabTest").val("-1");
	$("#txtPatientId").val("");
	$("#txtIPDId").val("");

	$("#txtPatientIdError").text("");
	$("#selLabTestComponentError").text("");
	$("#selLabTypeError").text("");
	$("#selLabTestError").text("");
	$("#txtIPDIdError").text("");

	investigationTable.fnClearTable();
	$("#txtPatientId").removeAttr("disabled", "disabled");
	$("#txtPatientId").removeClass("errorStyle");
	$("#totalBill").text("0");
}
function FadeOut(){
    setTimeout(function() {
        $('#addedStatus').text('');
        $('#testStatus').text('');
    }, 3000);
}

/*src url = self_request.js*/