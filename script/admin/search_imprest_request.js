 var imprestRequestTable; 
 var grandTotal;
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   imprest_request.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   21st may 2016
 * Description  :   This page add and mange imprest request
 *************************************************************************************************** */
 	grandTotal=0;
	imprestRequestTable = $('#tblSearchImprestRequest').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"bAutoWidth":false,
		"fnDrawCallback": function ( oSettings ) {
			$('#myImprestModal #tblSearchImprestRequest tbody tr').on( 'dblclick', function () {

				if ( $(this).hasClass('selected') ) {
				  $(this).removeClass('selected');
				}
				else {
				  imprestRequestTable.$('tr.selected').removeClass('selected');
				   $(this).addClass('selected');
				}
				
				var mData = imprestRequestTable.fnGetData(this); // get datarow
				if (null != mData)  // null if we clicked on title row
				{
					//now aData[0] - 1st column(count_id), aData[1] -2nd, etc. 								
					var imprestId = mData["imprest_request_id"];
					var imprestPrefix = mData["imprest_prefix"];
					var visitIdLength = imprestId.length;
					for (var i=0;i<6-visitIdLength;i++) {
						imprestId = "0"+imprestId;
					}
					imprestId = imprestPrefix+imprestId;
					$('#txtImprestRef').val(imprestId);
					$("#txtLedger").val(mData["ledger_name"]);					
					$("#txtAmount").val(mData["amount"]);	
					grandTotal = mData["amount"];			
					$("#txtApplicantName").val(mData["name"]);					
					$("#txtDescription").val(mData["nature_of_duty"]);					
					$("#txtImprestDate").val(mData["assignment_start"]);					
					$("#txtSurrenderDate").val(mData["assignment_end"]);
					bindPaymentMode(mData["ledger_id"],null);				
				}							
				
				$("#btnSelect").click();
				$(".close").click();
							
			} );
			
		},
		
		"sAjaxSource":"controllers/admin/imprest_disbursement.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "showImprestRequest" });
		},
		"aoColumns": [
			{ "mData": "name" },
			{ "mData": "nature_of_duty" },
			{ "mData": "amount" },
			{ "mData": "est_day" },
			{ "mData": "assignment_start" },
			{ "mData": "assignment_end" },
		],
		/*Disable sort for following columns*/
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 5 ] }
	   ]
	} );
} );
