$(document).ready(function(){
/* ****************************************************************************************************
 * File Name    :   view_visit.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages visits data
 *************************************************************************************************** */
	loader(); 
	debugger;
	var otable;
	/*
		Hidden field value 0 for Triage screen  
		Hidden field value 1 for View Visit screen  

		Hidden field value 3 for consultant module screen
	*/
	var value = $("#thing").val();
	if(value == "3"){
		$("#tblPharmacyDispenseVisit").removeAttr("style");
	}
	
	//varibale for time stammps
	var getTimeStamp;
	var getFromDateTimeStamp;
	var getToDateTimeStamp;
	
	//hide details
	$("#myConsultantModal #statusField").hide();
	
	//This code is used for show the date picker in view visit and related with this screen 
	$('#txtFromDate').datepicker();
	$('#txtToDate').datepicker();
	
	$("#myTriageModal #txtFromDate").click(function(){
		$(".datepicker").css({"z-index":"99999"});
	});
	
	$("#myTriageModal #txtToDate").click(function(){
		$(".datepicker").css({"z-index":"99999"});
	});
	$("#myCashModal #txtFromDate").click(function(){
		$(".datepicker").css({"z-index":"99999"});
	});
	
	$("#myCashModal #txtToDate").click(function(){
		$(".datepicker").css({"z-index":"99999"});
	});
	$("#myAssessmentModal #txtFromDate").click(function(){
		$(".datepicker").css({"z-index":"99999"});
	});
	
	$("#myAssessmentModal #txtToDate").click(function(){
		$(".datepicker").css({"z-index":"99999"});
	});

	$("#txtToDate").change(function(){
		$("#txtToDate").datepicker("hide");
	});	
	$("#txtFromDate").change(function(){
		$("#txtFromDate").datepicker("hide");
	});
	//End of the date picker code 
	
	//Click function of reset button for view visit screen 
	$("#btnReset").click(function(){
		$("#txtFromDate").val("");
		$("#txtToDate").val("");	
		otable.fnClearTable();
		clear();
	});	
	
	//Initialize the data table in view visit screen 
	otable = $('#viewVisitTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"bAutoWidth" : false,		
		"sPaginationType":"full_numbers",
		aoColumnDefs: [
			{ 'bSortable': false, 'aTargets': [ 8 ] },
			{ 'bSortable': false, 'aTargets': [ 7 ] },
			{ 'bSortable': false, 'aTargets': [ 6 ] }
		]		
	});
	
	
	//
	//on click of button show data in datatable that serached in view visit screen 
	//
	$("#btnSearch").click(function(){
		var flag = "false";
		var patientId = $("#txtPatientId").val();
		var value = $("#thing").val();// this is hidden field 
		if(value == "1"){
			var firstName = $ ("#txtFirstName").val().trim();
			var lastName = $ ("#txtLastName").val().trim();
			var getFromDateTimeStamp = $ ("#txtFromDate").val();
			var getToDateTimeStamp = $ ("#txtToDate").val();
			var mobile = $ ("#txtMobile").val().trim();
			var paidStatus = $("#selPaidStatus").val();
			var triAssStatus = $("#selTriAssStatus").val();
			
			if($("#txtPatientId").val() != ""){
				if(patientId.length != 9){					
					flag = "true";
				}
				else{
					var patientId = $("#txtPatientId").val();
					var patientPrefix = patientId.substring(0, 3);
					patientId = patientId.replace ( /[^\d.]/g, '' ); 
					patientId = parseInt(patientId);
				}
			}
			else{
				patientPrefix = "";
			}
			if (flag == "true") {
				return false;
			}	
			
			var postData = {
				"operation":"search",
				"getFromDateTimeStamp" : getFromDateTimeStamp,
				"getToDateTimeStamp" : getToDateTimeStamp,
				"firstName" : firstName,
				"lastName" : lastName,
				"mobile" : mobile,
				"patientPrefix" : patientPrefix,
				"patientId" : patientId,
				"paidStatus" : paidStatus,
				"triAssStatus" : triAssStatus
			}

			$.ajax(
				{					
				type: "POST",
				cache: false,
				url: "controllers/admin/view_visit.php",
				datatype:"json",
				data: postData,
				
				success: function(dataSet) {
					if(dataSet != "0" && dataSet != ""){
						dataSet = JSON.parse(dataSet);
						//var dataSet = jQuery.parseJSON(data);
						otable.fnClearTable();
						otable.fnDestroy();
						otable = $('#viewVisitTable').DataTable( {
							"sPaginationType":"full_numbers",
							"bAutoWidth" : false,
							"aaSorting": [],
							"fnDrawCallback": function ( oSettings ) {
								
								var aiRows = otable.fnGetNodes(); //Get all rows of data table

							 	if(aiRows.length == 0){
							 		return false;
							 	}
							 	for (var j=0,c=aiRows.length; j<c; j++) {
							 		var visitType = $(aiRows[j]).find('td:eq(1)').html();
							 		if(visitType == "Emergency Case"){
							 			$(aiRows[j]).css('background-color','#ff6666');
							 		}
							 	}

								$(".visitDetails").on('click',function(){ 
									if(value == "1"){
										$("#myVisitModel").modal("show");
										var id = $(this).attr('data-id');
										var postData = {
											"operation":"visitDetail",
											"id":id
										}
										
										$.ajax({					
											type: "POST",
											cache: false,
											url: "controllers/admin/view_visit.php",
											datatype:"json",
											data: postData,
											
											success: function(data) {
												if(data != "0" && data != ""){
													$("#visitModelLabel").text("Visit Details");
													$('.modal-body').text();
													var parseData = jQuery.parseJSON(data);
																	
													for (var i=0;i<parseData.length;i++) {
														
														$("#myVisitModel #lblDepartementType").text(parseData[i].type);
														$("#myVisitModel #lblName").text(parseData[i].name);
														$("#myVisitModel #lblServiceName").text(parseData[i].service_name);
														$("#myVisitModel #lblVisitDate").text(parseData[i].visit_date);

														if (parseData[i].triage_status =='UnTaken') {
															$("#myVisitModel #lblTriageStatus").text("Not Taken");
														}
														else {
															$("#myVisitModel #lblTriageStatus").text("Taken");

														}

														
														$("#myVisitModel #lblPaymentStatus").text(parseData[i].payment_status);
														var visitId = parseInt(parseData[i].id);
														var patientPrefix = parseData[i].prefix;
															
														var visitIdLength = visitId.toString().length;
														for (var j=0;j<6-visitIdLength;j++) {
															visitId = "0"+visitId;
														}
														visitid = patientPrefix+visitId;
														$("#myVisitModel #lblVisitId").text(visitid);
								
													}  
													$('#viewModel').click();																									
												}
											},
											error:function() {
												$('#messageMyModalLabel').text("Error");
												$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
												$('#messagemyModal').modal();
											}
										});
									}
								});		
							},
							"aaData": dataSet,
							"aoColumns": [
								{  
									"mData": function (o) { 	
									var visitId = o["id"];
									var patientPrefix = o["visit_prefix"];
										
									var visitIdLength = visitId.length;
									for (var i=0;i<6-visitIdLength;i++) {
										visitId = "0"+visitId;
									}
									visitId = patientPrefix+visitId;
									return visitId; 
									}
								},
								/* { "mData": "id"}, */
								{ "mData": "type" },
								{ "mData": "name" },						
								{ "mData": "service_name" },
								{ "mData": "visit_date" },
								{ 
									"mData": "payment_mode"
								},{
								  "mData": function (o) { 				
										var data = o;
										var id = data["id"];
										var visitType = o["type"];
										if(visitType == "Emergency Case"){
											return "<i class='visitDetails fa fa-eye' id='btnView' visitType="+visitType+" title='View' data-id="+id+" "+ "></i>"; 
										}
										else{	
											return "<i class='visitDetails fa fa-eye' id='btnView' title='View' data-id="+id+" "+ "></i>"; 
										}	
									}
								},
							],
							aoColumnDefs: [
								{ 'bSortable': false, 'aTargets': [ 6 ] },
								{ 'bSortable': false, 'aTargets': [ 5 ] }
							]
						} );
					}
					else{
						
					}
				},
				error:function() {
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
			});
		}//Finish the code for view visit screen
	});

	/*Code to get today opd booking data*/
	$ ("#txtFromDate").val(todayDate());
	$ ("#txtToDate").val(todayDate());
	$("#btnSearch").click();
	$ ("#txtFromDate").val("");
	$ ("#txtToDate").val("");		
		
	//This code is used for Triage screen 
	//
	//Code for click on search icon of triage screen
	//
	$("#myTriageModal #btnReset").click(function(){
		$("#myTriageModal #txtFromDate").val("");
		$("#myTriageModal #txtToDate").val("");	
		$("#myTriageModal #txtPatientId").val("");	
		otable.fnClearTable();	
		clear();
	});
	
	//This used for popup goto back to list 	
	$("#myTriageModal #btnBackToList").click(function(){
		$("#myTriageModal #searchVisit").show();
		$("#myTriageModal #viewDetails").hide();
	});
	 
	
	//Click function is used for search data in triage screen 
	$("#myTriageModal #btnSearch").click(function(){
		var flag = "false";
		var value = $("#thing").val();
		if(value == "0"){
			var firstName = $ ("#myTriageModal #txtFirstName").val();
			var lastName = $ ("#myTriageModal #txtLastName").val();
			var getFromDateTimeStamp = $ ("#myTriageModal #txtFromDate").val();
			var getToDateTimeStamp = $ ("#myTriageModal #txtToDate").val();
			var mobile = $ ("#myTriageModal #txtMobile").val();
			var patientId = $ ("#myTriageModal #txtPatientId").val();
			var paidStatus = $("#myTriageModal #selPaidStatus").val();
			var triAssStatus = $("#myTriageModal #selTriAssStatus").val();
			
			if($("#myTriageModal #txtPatientId").val() != ""){
				if(patientId.length != 9){					
					flag = "true";
				}
				else{
					var patientId = $("#myTriageModal #txtPatientId").val();
					var patientPrefix = patientId.substring(0, 3);
					patientId = patientId.replace ( /[^\d.]/g, '' ); 
					patientId = parseInt(patientId);
				}
			}
			else{
				patientPrefix = "";
			}
			if (flag == "true") {
				return false;
			}
			var postData = {
				"operation":"searchModal",
				"getFromDateTimeStamp" : getFromDateTimeStamp,
				"getToDateTimeStamp" : getToDateTimeStamp,
				"firstName" : firstName,
				"lastName" : lastName,
				"mobile" : mobile,
				"patientPrefix" : patientPrefix,
				"patientId" : patientId,
				"paidStatus" : paidStatus,
				"triAssStatus" : triAssStatus
			}

			$.ajax(
				{					
				type: "POST",
				cache: false,
				url: "controllers/admin/view_visit.php",
				datatype:"json",
				data: postData,
				
				success: function(dataSet) {
					dataSet = JSON.parse(dataSet);
					//var dataSet = jQuery.parseJSON(data);
					otable.fnClearTable();
					otable.fnDestroy();
					otable = $('#myTriageModal #viewVisitTable').DataTable( {
						"sPaginationType":"full_numbers",
						"bAutoWidth" : false,
						"responsive": true,
						 "aaSorting": [],
						"bVisible": true,
						"fnDrawCallback": function ( oSettings ) {
							
							var aiRows = otable.fnGetNodes(); //Get all rows of data table

						 	if(aiRows.length == 0){
						 		return false;
						 	}
						 	for (var j=0,c=aiRows.length; j<c; j++) {
						 		var visitType = $(aiRows[j]).find('td:eq(1)').html();
						 		if(visitType == "Emergency Case"){
						 			$(aiRows[j]).css('background-color','#ff6666');
						 		}
						 	}

							$('#myTriageModal #viewVisitTable tbody tr').on( 'dblclick', function () {
		
								if ( $(this).hasClass('selected') ) {
								  $(this).removeClass('selected');
								}
								else {
								  otable.$('tr.selected').removeClass('selected');
								   $(this).addClass('selected');
								}
								
								var mData = otable.fnGetData(this); // get datarow
								if (null != mData)  // null if we clicked on title row
								{
									//now aData[0] - 1st column(count_id), aData[1] -2nd, etc.

									var visitId = mData["id"];
									var patientPrefix = mData["visit_prefix"];
										
									var visitIdLength = visitId.length;
									for (var i=0;i<6-visitIdLength;i++) {
										visitId = "0"+visitId;
									}
									visitId = patientPrefix+visitId;
									$('#textVisitId').val(visitId);	
								}							
								
								$("#btnLoad").click();
								$(".close").click();
											
							});
							//Click function for show the details of patient
							$("#myTriageModal .visitDetails").on('click',function(){
								var id = $(this).attr('data-id');
								var postData = {
									"operation":"visitDetailModal",
									"id":id
								}
								
								$.ajax({					
									type: "POST",
									cache: false,
									url: "controllers/admin/view_visit.php",
									datatype:"json",
									data: postData,
									
									success: function(data) {
										if(data != "0" && data != ""){
											var parseData = jQuery.parseJSON(data);
															
											for (var i=0;i<parseData.length;i++) {
												
												$("#myTriageModal #lblDepartementType").text(parseData[i].type);
												$("#myTriageModal #lblName").text(parseData[i].name);
												$("#myTriageModal #lblServiceName").text(parseData[i].service_name);
												$("#myTriageModal #lblVisitDate").text(parseData[i].visit_date);

												if (parseData[i].triage_status =='UnTaken') {
													$("#myTriageModal #lblTriageStatus").text("Not Taken");
												}
												else {
													$("#myTriageModal #lblTriageStatus").text("Taken");
												}												

												$("#myTriageModal #lblPaymentStatus").text(parseData[i].service_status);
												$("#myTriageModal #lblPaymentMode").text(parseData[i].payment_mode);
												var visitId = parseInt(parseData[i].id);
												var patientPrefix = parseData[i].prefix;
													
												var visitIdLength = visitId.toString().length;
												for (var j=0;j<6-visitIdLength;j++) {
													visitId = "0"+visitId;
												}
												visitid = patientPrefix+visitId;
												$("#myTriageModal #lblVisitId").text(visitid);
								
											} 
											$("#myTriageModal #searchVisit").hide();
											$("#myTriageModal #viewDetails").show();
										}
									},
									error:function() {
										$('#messageMyModalLabel').text("Error");
										$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
										$('#messagemyModal').modal();
									}
								});
							});
						},
						"aaData": dataSet,
						
						"aoColumns": [
							{  
								"mData": function (o) { 	
								var visitId = o["id"];
								var patientPrefix = o["visit_prefix"];
									
								var visitIdLength = visitId.length;
								for (var i=0;i<6-visitIdLength;i++) {
									visitId = "0"+visitId;
								}
								visitId = patientPrefix+visitId;
								return visitId; 
								}
							},
							/* { "mData": "id"}, */
							{ "mData": "type" },
							{ "mData": "name" },						
							{ "mData": "service_name" },
							{ "mData": "visit_date" },
							{ "mData": "payment_mode" },
							{
							  "mData": function (o) { 				
									var data = o;
									var id = data["id"];
									var visitType = o["type"];
									if(visitType == "Emergency Case"){
										return "<i class='visitDetails fa fa-eye' id='btnView' visitType="+visitType+" title='View' data-id="+id+" "+ "></i>"; 
									}
									else{	
										return "<i class='visitDetails fa fa-eye' id='btnView' title='View' data-id="+id+" "+ "></i>"; 
									}	
								}
							},
						],
						aoColumnDefs: [
				          	{ 'bSortable': false, 'aTargets': [ 6 ] },
				          	{ 'bSortable': false, 'aTargets': [ 5 ] }
				       	]
					} );				
					
				},
				error:function() {
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
			});
		}
	});

	/*to get data of today date*/
	$ ("#myTriageModal #txtFromDate").val(todayDate());
	$ ("#myTriageModal #txtToDate").val(todayDate());
	$("#myTriageModal #btnSearch").click();
	$ ("#myTriageModal #txtFromDate").val("");
	$ ("#myTriageModal #txtToDate").val("");	

	$("#myPharmacyModal #btnBackToList").click(function(){
		$("#myPharmacyModal #searchVisit").show();
		$("#myPharmacyModal #viewDetails").hide();
	});
	 
	/*This is used for consultant*/ 
	$("#myConsultantModal #btnSearch").click(function(){
		var flag = "false";
		var value = $("#thing").val();
		if(value == "4"){
			var firstName = $ ("#myConsultantModal #txtFirstName").val();
			var lastName = $ ("#myConsultantModal #txtLastName").val();
			var mobile = $ ("#myConsultantModal #txtMobile").val();
			var patientId = $ ("#myConsultantModal #txtPatientId").val();			
			var getFromDateTimeStamp = $("#myConsultantModal #txtFromDate").val();
			var getToDateTimeStamp = $("#myConsultantModal #txtToDate").val();
			
			if($("#myConsultantModal #txtPatientId").val() != ""){
				if(patientId.length != 9){					
					flag = "true";
				}
				else{
					var patientId = $("#myConsultantModal #txtPatientId").val();
					var patientPrefix = patientId.substring(0, 3);
					patientId = patientId.replace ( /[^\d.]/g, '' ); 
					patientId = parseInt(patientId);
				}
			}
			else{
				patientPrefix = "";
			}
			if (flag == "true") {
				return false;
			}
			
			var postData = {
				"operation":"searchForConsultantScreen",
				"getFromDateTimeStamp" : getFromDateTimeStamp,
				"getToDateTimeStamp" : getToDateTimeStamp,
				"firstName" : firstName,
				"lastName" : lastName,
				"mobile" : mobile,
				"patientPrefix" : patientPrefix,
				"patientId" : patientId
			}
			

			$.ajax(
				{					
				type: "POST",
				cache: false,
				url: "controllers/admin/view_visit.php",
				datatype:"json",
				data: postData,
				
				success: function(dataSet) {
					dataSet = JSON.parse(dataSet);
					//var dataSet = jQuery.parseJSON(data);
					otable.fnClearTable();
					otable.fnDestroy();
					
					otable = $('#myConsultantModal #viewVisitTable').DataTable( {
						"sPaginationType":"full_numbers",
						"bAutoWidth" : false,
						"aaSorting": [],
						"bVisible": true,
						"fnDrawCallback": function ( oSettings ) {

							var aiRows = otable.fnGetNodes(); //Get all rows of data table

						 	if(aiRows.length == 0){
						 		return false;
						 	}
						 	for (var j=0,c=aiRows.length; j<c; j++) {
						 		var visitType = $(aiRows[j]).find('td:eq(1)').html();
						 		if(visitType == "Emergency Case"){
						 			$(aiRows[j]).css('background-color','#ff6666');
						 		}
						 	}
							
							$('#myConsultantModal #viewVisitTable tbody tr').on( 'dblclick', function () {
		
								if ( $(this).hasClass('selected') ) {
								  $(this).removeClass('selected');
								}
								else {
								  otable.$('tr.selected').removeClass('selected');
								   $(this).addClass('selected');
								}
								
								var mData = otable.fnGetData(this); // get datarow
								if (null != mData)  // null if we clicked on title row
								{
									//now aData[0] - 1st column(count_id), aData[1] -2nd, etc. 								
									
									var visitId = mData["id"];
									var patientPrefix = mData["visit_prefix"];
										
									var visitIdLength = visitId.length;
									for (var i=0;i<6-visitIdLength;i++) {
										visitId = "0"+visitId;
									}
									visitId = patientPrefix+visitId;
									$('#textvisitId').val(visitId);								
								}							
								
								$("#btnVisitLoad").click();
								$(".close").click();
											
							} );
							
							$("#myConsultantModal .visitDetails").on('click',function(){
								var id = $(this).attr('data-id');
								var postData = {
									"operation":"visitDetailModal",
									"id":id
								}
								
								$.ajax({					
									type: "POST",
									cache: false,
									url: "controllers/admin/view_visit.php",
									datatype:"json",
									data: postData,
									
									success: function(data) {
										if(data != "0" && data != ""){
											var parseData = jQuery.parseJSON(data);
															
											for (var i=0;i<parseData.length;i++) {
												
												$("#myConsultantModal #lblDepartementType").text(parseData[i].type);
												$("#myConsultantModal #lblName").text(parseData[i].name);
												$("#myConsultantModal #lblServiceName").text(parseData[i].service_name);
												$("#myConsultantModal #lblVisitDate").text(parseData[i].visit_date);

												if (parseData[i].triage_status =='UnTaken') {
													$("#myConsultantModal #lblTriageStatus").text("Not Taken");
												}
												else {
													$("#myConsultantModal #lblTriageStatus").text("Taken");
												}
												
												$("#myConsultantModal #lblPaymentStatus").text(parseData[i].payment_status);
												var visitId = parseInt(parseData[i].id);
												var patientPrefix = parseData[i].prefix;
													
												var visitIdLength = visitId.toString().length;
												for (var j=0;j<6-visitIdLength;j++) {
													visitId = "0"+visitId;
												}
												visitid = patientPrefix+visitId;
												$("#myConsultantModal #lblVisitId").text(visitid);
											} 
											$("#myConsultantModal #searchVisit").hide();
											$("#myConsultantModal #viewDetails").show();
										}
									},
									error:function() {
										$('#messageMyModalLabel').text("Error");
										$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
										$('#messagemyModal').modal();
									}
								});
							});
						},
						"aaData": dataSet,
						"aoColumns": [
							{  
								"mData": function (o) { 	
								var visitId = o["id"];
								var patientPrefix = o["visit_prefix"];
									
								var visitIdLength = visitId.length;
								for (var i=0;i<6-visitIdLength;i++) {
									visitId = "0"+visitId;
								}
								visitId = patientPrefix+visitId;
								return visitId; 
								}
							},
							/* { "mData": "id"}, */
							{ "mData": "type" },
							{ "mData": "name" },						
							{ "mData": "service_name" },
							{ "mData": "visit_date" },
							{ "mData": "payment_mode" },
							{
							  "mData": function (o) { 				
									var data = o;
									var id = data["id"];
									var visitType = o["type"];
									if(visitType == "Emergency Case"){
										return "<i class='visitDetails fa fa-eye' id='btnView' visitType="+visitType+" title='View' data-id="+id+" "+ "></i>"; 
									}
									else{	
										return "<i class='visitDetails fa fa-eye' id='btnView' title='View' data-id="+id+" "+ "></i>"; 
									}	
								}
							},
						],
						aoColumnDefs: [
				          	{ 'bSortable': false, 'aTargets': [ 6 ] },
				          	{ 'bSortable': false, 'aTargets': [ 5 ] }
				       	]
					} );				
					
				},
				error:function() {
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
			});
		}
	});
	
	//This code is used for consultant screen 
	//Code for click on search icon  in consultant screen
	$("#myConsultantModal #btnReset").click(function(){
		$("#myConsultantModal #txtFromDate").val("");
		$("#myConsultantModal #txtToDate").val("");	
		$("#myConsultantModal #txtPatientId").val("");	
		otable.fnClearTable();	
			clear();
	});
	
	$("#myConsultantModal #btnBackToList").click(function(){
		$("#myConsultantModal #searchVisit").show();
		$("#myConsultantModal #viewDetails").hide();
	});	
	

	/*Ajax call to show current date data in consultanat screen*/
	var value = $("#thing").val();
		if(value == "3"){
			$.ajax({					
			type: "POST",
			cache: false,
			url: "controllers/admin/view_visit.php",
			datatype:"json",
			data: {
				"operation":"searchForConsultantScreenAtLoad"			
			},
			
			success: function(data) {
				dataSet = JSON.parse(data);
				//var dataSet = jQuery.parseJSON(data);
				otable.fnClearTable();
				otable.fnDestroy();
				otable = $('#myConsultantModal #viewVisitTable').DataTable( {
					"sPaginationType":"full_numbers",
					"bAutoWidth" : false,
					"aaSorting": [],
					"bVisible": true,
					"fnDrawCallback": function ( oSettings ) {
						
						var aiRows = otable.fnGetNodes(); //Get all rows of data table

					 	if(aiRows.length == 0){
					 		return false;
					 	}
					 	for (var j=0,c=aiRows.length; j<c; j++) {
					 		var visitType = $(aiRows[j]).find('td:eq(1)').html();
					 		if(visitType == "Emergency Case"){
					 			$(aiRows[j]).css('background-color','#ff6666');
					 		}
					 	}

						$('#myConsultantModal #viewVisitTable tbody tr').on( 'dblclick', function () {
							
							if ( $(this).hasClass('selected') ) {
							  $(this).removeClass('selected');
							}
							else {
							  otable.$('tr.selected').removeClass('selected');
							   $(this).addClass('selected');
							}
							
							var mData = otable.fnGetData(this); // get datarow
							if (null != mData)  // null if we clicked on title row
							{									
								var visitId = mData["id"];
								var patientPrefix = mData["visit_prefix"];
									
								var visitIdLength = visitId.length;
								for (var i=0;i<6-visitIdLength;i++) {
									visitId = "0"+visitId;
								}
								visitId = patientPrefix+visitId;
								$('#textvisitId').val(visitId);
							}	

							/*Hide the datatable div and show main page*/
							$('#myConsultantModal').hide();
							$('.advanced-first-consultant').show();
							$("#btnVisitLoad").click();
										
						} );
						
						$("#myConsultantModal .visitDetails").on('click',function(){
							var data=$(this).parents('tr')[0];
							var mData =  otable.fnGetData(data);

							if (null != mData)  {// null if we clicked on title row
		                   
		                        var visitId = mData["id"];
		                        var visitType = mData["type"];
		                        var name = mData["name"];
		                        var serviceName = mData["service_name"];
		                        var visitDate = mData["visit_date"];
		                        var triageStatus = mData["triage_status"];
		                        var paymentStatus = mData["payment_status"];
		                        var paymentMode = mData["payment_mode"];
		                        var visitIdLength = visitId.length;
                                for (var i=0;i<6-visitIdLength;i++) {
                                    visitId = "0"+visitId;
                                }
                                visitId = visitPrefix+visitId;
                                $('#viewDetails #lblVisitId').text(visitId);
		                        $('#viewDetails #lblDepartementType').text(visitType);
		                        $('#viewDetails #lblName').text(name);
		                        $('#viewDetails #lblServiceName').text(serviceName);
		                        $('#viewDetails #lblVisitDate').text(visitDate);
		                        $('#viewDetails #lblTriageStatus').text(triageStatus);
		                        $('#viewDetails #lblPaymentStatus').text(paymentStatus);
		                        $('#viewDetails #lblPaymentMode').text(paymentMode);
		                    }


							$("#myConsultantModal #searchVisit").hide();
							$("#myConsultantModal #viewDetails").show();
						});
					},
					"aaData": dataSet,
					"aoColumns": [
						{  
							"mData": function (o) { 	
							var visitId = o["id"];
							var patientPrefix = o["visit_prefix"];
								
							var visitIdLength = visitId.length;
							for (var i=0;i<6-visitIdLength;i++) {
								visitId = "0"+visitId;
							}
							visitId = visitPrefix+visitId;
							return visitId; 
							}
						},
						/* { "mData": "id"}, */
						{ "mData": "type" },
						{ "mData": "name" },						
						{ "mData": "service_name" },
						{ "mData": "visit_date" },
						{ "mData": "payment_mode" },
						{
						  "mData": function (o) { 				
								var data = o;
								var id = data["id"];
								var visitType = o["type"];
								if(visitType == "Emergency Case"){
									return "<i class='visitDetails fa fa-eye' id='btnView' visitType="+visitType+" title='View' data-id="+id+" "+ "></i>"; 
								}
								else{	
									return "<i class='visitDetails fa fa-eye' id='btnView' title='View' data-id="+id+" "+ "></i>"; 
								}	 
							}
						},
					],
					aoColumnDefs: [
						{ 'bSortable': false, 'aTargets': [ 6 ] },
						{ 'bSortable': false, 'aTargets': [ 5 ] }
					]
				} );					
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
		}
		


	$("#myConsultantModal #btnSearch").click(function(){
		var flag = "false";
		var value = $("#thing").val();
		if(value == "3"){
			var firstName = $ ("#myConsultantModal #txtFirstName").val().trim();
			var lastName = $ ("#myConsultantModal #txtLastName").val().trim();
			var mobile = $ ("#myConsultantModal #txtMobile").val().trim();
			var patientId = $ ("#myConsultantModal #txtPatientId").val().trim();
			var getFromDateTimeStamp = $("#myConsultantModal #txtFromDate").val().trim();
			var getToDateTimeStamp = $("#myConsultantModal #txtToDate").val().trim();
			
			if($("#myConsultantModal #txtPatientId").val() != ""){
				if(patientId.length != 9){					
					flag = "true";
				}
				else{
					var patientId = $("#myConsultantModal #txtPatientId").val();
					var patientPrefix = patientId.substring(0, 3);
					patientId = patientId.replace ( /[^\d.]/g, '' ); 
					patientId = parseInt(patientId);
				}
			}
			else{
				patientPrefix = "";
			}
			if (flag == "true") {
				return false;
			}
			
			var postData = {
				"operation":"searchForConsultantScreen",
				"getFromDateTimeStamp" : getFromDateTimeStamp,
				"getToDateTimeStamp" : getToDateTimeStamp,
				"firstName" : firstName,
				"lastName" : lastName,
				"mobile" : mobile,
				"patientPrefix" : patientPrefix,
				"patientId" : patientId
			}

			$.ajax(
				{					
				type: "POST",
				cache: false,
				url: "controllers/admin/view_visit.php",
				datatype:"json",
				data: postData,
				
				success: function(data) {
					dataSet = JSON.parse(data);
					//var dataSet = jQuery.parseJSON(data);
					otable.fnClearTable();
					otable.fnDestroy();
					otable = $('#myConsultantModal #viewVisitTable').DataTable( {
						"sPaginationType":"full_numbers",
						"bAutoWidth" : false,
						"aaSorting": [],
						"bVisible": true,
						"fnDrawCallback": function ( oSettings ) {
							
							var aiRows = otable.fnGetNodes(); //Get all rows of data table

						 	if(aiRows.length == 0){
						 		return false;
						 	}
						 	for (var j=0,c=aiRows.length; j<c; j++) {
						 		var visitType = $(aiRows[j]).find('td:eq(1)').html();
						 		if(visitType == "Emergency Case"){
						 			$(aiRows[j]).css('background-color','#ff6666');
						 		}
						 	}

							$('#myConsultantModal #viewVisitTable tbody tr').on( 'dblclick', function () {
								
								if ( $(this).hasClass('selected') ) {
								  $(this).removeClass('selected');
								}
								else {
								  otable.$('tr.selected').removeClass('selected');
								   $(this).addClass('selected');
								}
								
								var mData = otable.fnGetData(this); // get datarow
								if (null != mData)  // null if we clicked on title row
								{									
									var visitId = mData["id"];
									var patientPrefix = mData["visit_prefix"];
										
									var visitIdLength = visitId.length;
									for (var i=0;i<6-visitIdLength;i++) {
										visitId = "0"+visitId;
									}
									visitId = patientPrefix+visitId;
									$('#textvisitId').val(visitId);
								}	

								/*Hide the datatable div and show main page*/
								$('#myConsultantModal').hide();
								$('.advanced-first-consultant').show();
								$('html, body').animate({
							        scrollTop: $("#page-content").offset().top
							    }, 500);
								$("#btnVisitLoad").click();
								$('#textvisitId').prop('disabled',true);
											
							} );
							
							$("#myConsultantModal .visitDetails").on('click',function(){
								var data=$(this).parents('tr')[0];
								var mData =  otable.fnGetData(data);

								if (null != mData) // null if we clicked on title row
			                    {
			                        var visitId = mData["id"];
			                        var visitType = mData["type"];
			                        var name = mData["name"];
			                        var serviceName = mData["service_name"];
			                        var visitDate = mData["visit_date"];
			                        var triageStatus = mData["triage_status"];
			                        var paymentStatus = mData["payment_status"];
			                        var paymentMode = mData["payment_mode"];

			                        var visitIdLength = visitId.length;
	                                for (var i=0;i<6-visitIdLength;i++) {
	                                    visitId = "0"+visitId;
	                                }
	                                visitId = visitPrefix+visitId;

			                        $('#viewDetails #lblVisitId').text(visitId);
			                        $('#viewDetails #lblDepartementType').text(visitType);
			                        $('#viewDetails #lblName').text(name);
			                        $('#viewDetails #lblServiceName').text(serviceName);
			                        $('#viewDetails #lblVisitDate').text(visitDate);
			                        $('#viewDetails #lblTriageStatus').text(triageStatus);
			                        $('#viewDetails #lblPaymentStatus').text(paymentStatus);
			                        $('#viewDetails #lblPaymentMode').text(paymentMode);
			                    }


								$("#myConsultantModal #searchVisit").hide();
								$("#myConsultantModal #viewDetails").show();
								$('html, body').animate({
							        scrollTop: $("#page-content").offset().top
							    }, 500);
							});
						},
						"aaData": dataSet,
						"aoColumns": [
							{  
								"mData": function (o) { 	
								var visitId = o["id"];
								var patientPrefix = o["visit_prefix"];
									
								var visitIdLength = visitId.length;
								for (var i=0;i<6-visitIdLength;i++) {
									visitId = "0"+visitId;
								}
								visitId = patientPrefix+visitId;
								return visitId; 
								}
							},
							/* { "mData": "id"}, */
							{ "mData": "type" },
							{ "mData": "name" },						
							{ "mData": "service_name" },
							{ "mData": "visit_date" },
							{ "mData": "payment_mode" },
							{
							  "mData": function (o) { 				
									var data = o;
									var id = data["id"];
									var visitType = o["type"];
									if(visitType == "Emergency Case"){
										return "<i class='visitDetails fa fa-eye' id='btnView' visitType="+visitType+" title='View' data-id="+id+" "+ "></i>"; 
									}
									else{	
										return "<i class='visitDetails fa fa-eye' id='btnView' title='View' data-id="+id+" "+ "></i>"; 
									}	 
								}
							},
						],
						aoColumnDefs: [
				          	{ 'bSortable': false, 'aTargets': [ 6 ] },
				          	{ 'bSortable': false, 'aTargets': [ 5 ] }
				       	]
					} );					
				},
				error:function() {
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
			});
		}
	});
	
	// for vision assessment screen
	$("#myAssessmentModal .block-title").hide();
	
	//This code is used for assessment screen
	//Code for click on search icon  in assessment screen
	$("#myAssessmentModal #btnReset").click(function(){
		$("#myAssessmentModal #txtFromDate").val("");
		$("#myAssessmentModal #txtToDate").val("");	
		$("#myAssessmentModal #txtPatientId").val("");
		$("#myAssessmentModal #txtFirstName").val("");
		$("#myAssessmentModal #txtLastName").val("");
		$("#myAssessmentModal #txtMobile").val("");
		$("#myAssessmentModal #selPaidStatus").val("paid");
		$("#myAssessmentModal #selTriAssStatus").val("UnTaken");
		otable.fnClearTable();
	});
	
	$("#myAssessmentModal #btnBackToList").click(function(){
		$("#myAssessmentModal #searchVisit").show();
		$("#myAssessmentModal #viewDetails").hide();
	});

	$("#myAssessmentModal #btnSearch").click(function(){
		var flag = "false";
		var value = $("#thing").val();
		if(value == "10"){
		
			var firstName = $ ("#myAssessmentModal #txtFirstName").val();
			var lastName = $ ("#myAssessmentModal #txtLastName").val();
			var mobile = $ ("#myAssessmentModal #txtMobile").val();
			var patientId = $ ("#myAssessmentModal #txtPatientId").val();			
			var getFromDateTimeStamp = $("#myAssessmentModal #txtFromDate").val();
			var getToDateTimeStamp = $("#myAssessmentModal #txtToDate").val();
			var paidStatus = $("#myAssessmentModal #selPaidStatus").val();
			var triAssStatus = $("#selTriAssStatus").val();
			
			if($("#myAssessmentModal #txtPatientId").val() != ""){
				if(patientId.length != 9){					
					flag = "true";
				}
				else{
					var patientId = $("#myAssessmentModal #txtPatientId").val();
					var patientPrefix = patientId.substring(0, 3);
					patientId = patientId.replace ( /[^\d.]/g, '' ); 
					patientId = parseInt(patientId);
				}
			}
			else{
				patientPrefix = "";
			}
			if (flag == "true") {
				return false;
			}
			var clickButton = 1;
			var postData = {
				"operation":"searchVisionModal",
				"getFromDateTimeStamp" : getFromDateTimeStamp,
				"getToDateTimeStamp" : getToDateTimeStamp,
				"firstName" : firstName,
				"lastName" : lastName,
				"mobile" : mobile,
				"patientPrefix" : patientPrefix,
				"patientId" : patientId,
				"clickButton" : clickButton,
				"paidStatus" : paidStatus,
				"triAssStatus" : triAssStatus
			}
			viewVissionVisits(postData,otable);
		}		
	});
	$("#myAssessmentModal #txtFromDate").val(todayDate());
	$("#myAssessmentModal #txtToDate").val(todayDate());
	$("#myAssessmentModal #btnSearch").click();
	$("#myAssessmentModal #txtFromDate").val("");
	$("#myAssessmentModal #txtToDate").val("");
});

//function to calculate datestamp
function calculateTimeStamp(date){
	var myDate = date;
	myDate=myDate.split("-");
	var newDate=myDate[1]+"/"+myDate[2]+"/"+myDate[0]; //mm-dd-yyyy
	getTimeStamp=(new Date(newDate).getTime());	
	return getTimeStamp;
}

// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		 $(".close").click();
    }
});

function clear(){
	$("#txtFirstName").val("");
	$("#txtLastName").val("");
	$("#txtPatientId").val("");
	$("#txtMobile").val("");
	$("#txtFromDate").val("");
	$("#txtToDate").val("");
	
}
function viewVissionVisits (myPostData,otable) {
	$.ajax(
		{					
		type: "POST",
		cache: false,
		url: "controllers/admin/view_visit.php",
		datatype:"json",
		data: myPostData,
		
		success: function(dataSet) {
			dataSet = JSON.parse(dataSet);
			//var dataSet = jQuery.parseJSON(data);
			otable.fnClearTable();
			otable.fnDestroy();
			otable = $('#myAssessmentModal #viewVisitTable').DataTable( {
				"sPaginationType":"full_numbers",
				"bAutoWidth" : false,
				"aaSorting": [],
				"fnDrawCallback": function ( oSettings ) {
					
					var aiRows = otable.fnGetNodes(); //Get all rows of data table

				 	if(aiRows.length == 0){
				 		return false;
				 	}
				 	for (var j=0,c=aiRows.length; j<c; j++) {
				 		var visitType = $(aiRows[j]).find('td:eq(1)').html();
				 		if(visitType == "Emergency Case"){
				 			$(aiRows[j]).css('background-color','#ff6666');
				 		}
				 	}
						 	
					$('#myAssessmentModal #viewVisitTable tbody tr').on( 'dblclick', function () {

						if ( $(this).hasClass('selected') ) {
						  $(this).removeClass('selected');
						}
						else {
						  otable.$('tr.selected').removeClass('selected');
						   $(this).addClass('selected');
						}
						
						var mData = otable.fnGetData(this); // get datarow
						if (null != mData)  // null if we clicked on title row
						{
							//now aData[0] - 1st column(count_id), aData[1] -2nd, etc.
								
							var visitId = mData["id"];
							var patientPrefix = mData["visit_prefix"];
								
							var visitIdLength = visitId.length;
							for (var i=0;i<6-visitIdLength;i++) {
								visitId = "0"+visitId;
							}
							visitId = patientPrefix+visitId;
							$('#textvisitId').val(visitId);
						}							
						
						$("#btnSelect").click();
						$(".close").click();
									
					});
					
					$("#myAssessmentModal .visitDetails").on('click',function(){
						var id = $(this).attr('data-id');
						var postData = {
							"operation":"visitDetailModal",
							"id":id
						}
						
						$.ajax({					
							type: "POST",
							cache: false,
							url: "controllers/admin/view_visit.php",
							datatype:"json",
							data: postData,
							
							success: function(data) {
								if(data != "0" && data != ""){
									var parseData = jQuery.parseJSON(data);
													
									for (var i=0;i<parseData.length;i++) {

										$("#myAssessmentModal #lblDepartementType").text(parseData[i].type);
										$("#myAssessmentModal #lblName").text(parseData[i].name);
										$("#myAssessmentModal #lblServiceName").text(parseData[i].service_name);
										$("#myAssessmentModal #lblVisitDate").text(parseData[i].visit_date);

										if (parseData[i].triage_status =='UnTaken') {
											$("#myAssessmentModal #lblTriageStatus").text("Not Taken");
										}
										else {
											$("#myAssessmentModal #lblTriageStatus").text("Taken");
										}
										
										$("#myAssessmentModal #lblPaymentStatus").text(parseData[i].service_status);
										$("#myAssessmentModal #lblPaymentMode").text(parseData[i].payment_mode);
										var visitId = parseInt(parseData[i].id);
										var patientPrefix = parseData[i].prefix;
											
										var visitIdLength = visitId.toString().length;
										for (var j=0;j<6-visitIdLength;j++) {
											visitId = "0"+visitId;
										}
										visitid = patientPrefix+visitId;
										$("#myAssessmentModal #lblVisitId").text(visitid);
									} 
									$("#myAssessmentModal #searchVisit").hide();
									$("#myAssessmentModal #viewDetails").show();
								}
							},
							error:function() {
								$('#messageMyModalLabel').text("Error");
								$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
								$('#messagemyModal').modal();
							}
						});
					});
				},
				"aaData": dataSet,
				"aoColumns": [
					{  
						"mData": function (o) { 	
						var visitId = o["id"];
						var patientPrefix = o["visit_prefix"];
							
						var visitIdLength = visitId.length;
						for (var i=0;i<6-visitIdLength;i++) {
							visitId = "0"+visitId;
						}
						visitId = patientPrefix+visitId;
						return visitId; 
						}
					},
					/* { "mData": "id"}, */
					{ "mData": "type" },
					{ "mData": "name" },						
					{ "mData": "service_name" },
					{ "mData": "visit_date" },
					{ "mData": "payment_mode" },
					{
					  "mData": function (o) { 				
							var data = o;
							var id = data["id"];
							var visitType = o["type"];
							if(visitType == "Emergency Case"){
								return "<i class='visitDetails fa fa-eye' id='btnView' visitType="+visitType+" title='View' data-id="+id+" "+ "></i>"; 
							}
							else{	
								return "<i class='visitDetails fa fa-eye' id='btnView' title='View' data-id="+id+" "+ "></i>"; 
							}	
						}
					},
				],
				aoColumnDefs: [
		          	{ 'bSortable': false, 'aTargets': [ 6 ] },
		          	{ 'bSortable': false, 'aTargets': [ 5 ] }
		       	]
			} );					
		},
		error:function() {
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
	});
}
//# sourceURL = view_visit.js