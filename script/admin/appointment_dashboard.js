$(document).ready(function() {
	debugger;	
	google.charts.setOnLoadCallback(drawChart);
});
function drawChart() {
	var chartWidth  = $("#page-content").width() -25;
	var scheduledAppointmentRecord = [['Record-Group', 'Count']];
	var visitsToDrRecord = [['Record-Group', 'Count']];

	var postData = {
		"operation" : "showChartDataForAppointment"
	}
	$.ajax({     
		type: "POST",
		cache: false,
		url: "controllers/admin/patient_mangament_dashboard.php",
		data: postData,
		success: function(data) { 
			if(data != null && data != ""){
				var parseData =  JSON.parse(data);		
				
				scheduledAppointmentRecord = [['Record-Group', 'Count']];//string,integer
				visitsToDrRecord = [['Record-Group', 'Count']];//string,integer

				$.each(parseData,function(index,value){

					/*Check for null data*/
					if (parseData[index].length > 0) {

						for(var i=0;i<parseData[index].length;i++){
							if (index == 0) {
								smallArrayscheduledAppointment = [];
								smallArrayscheduledAppointment.push(parseData[index][i]['Consultant Name']);
								smallArrayscheduledAppointment.push(parseInt(parseData[index][i]['Appointments']));
								scheduledAppointmentRecord.push(smallArrayscheduledAppointment);	
							}
							else {
								
								$.each(parseData[index][i],function(i,v){
									visitsToDrRecord.push([i,parseInt(v)]);
								});
							}
						}
					}												
				});

	            /*google chart intialization for patientRecord group visit*/
	            var scheduledAppointmentRecordGroup = google.visualization.arrayToDataTable(scheduledAppointmentRecord);

	            var visitsToDrRecordGroup = google.visualization.arrayToDataTable(visitsToDrRecord);

			    var options = {
			      title: 'Scheduled Appointments','width':chartWidth,'height':parseData[0].length * 80
			    };

			    var options2 = {
			      title: 'Activated Appointments','width':chartWidth,'height':600
			    };

			    if (parseData[0].length <= 0) {
					$("#chart_div").hide();
				}
				else{
					var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
	        		chart.draw(scheduledAppointmentRecordGroup, options); 
				}
	        		 
				if (parseData[1]['Activated Appointments'] == undefined && parseData[1]['NOT Activated Appointments'] == undefined) {
					$("#chart_div2").hide();
				}
				else{
					var chart2 = new google.visualization.PieChart(document.getElementById('chart_div_2'));
	        		chart2.draw(visitsToDrRecordGroup, options2);
				}       		      		
			}
		}			
	});
}	