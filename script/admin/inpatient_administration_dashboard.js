$(document).ready(function() {
	debugger;	
	google.charts.setOnLoadCallback(drawChart);
});
function drawChart() {
	var chartWidth  = $("#page-content").width() -25;
	var labTestRecord = [['Task', 'Hours per Day']];
	var radiologyTestRecord = [['Record-Group', 'Count']];

	var postData = {
		"operation" : "showChartDataForInpatient"
	}
	$.ajax({     
		type: "POST",
		cache: false,
		url: "controllers/admin/patient_mangament_dashboard.php",
		data: postData,
		success: function(data) { 
			if(data != null && data != ""){
				var parseData =  JSON.parse(data);		
				if (parseData[0] ==''){
					$('#messagemyModal').modal();
					$('#messageMyModalLabel').text("Alert");
					$('.modal-body').text("No data exist!!!");
					return false;
				}
				labTestRecord = [['Task', 'Hours per Day']];
				radiologyTestRecord = [['Record-Group', 'Count']];

				$.each(parseData,function(index,value){

					/*Check for null data*/
					if (parseData[index].length > 0) {

						for(var i=0;i<parseData[index].length;i++){
							if (index == 0) {
								$.each(parseData[index][i],function(i,v){
									labTestRecord.push([i,parseInt(v)]);
								});	
							}
							else  {
								$.each(parseData[index][i],function(i,v){
									radiologyTestRecord.push([i,parseInt(v)]);
								});	
							}
						}
					}												
				});

	            /*google chart intialization for patientRecord group visit*/
	            var labTestRecordGroup = google.visualization.arrayToDataTable(labTestRecord);

	            var radiologyTestRecordGroup = google.visualization.arrayToDataTable(radiologyTestRecord);

			    var options = {
			      title: 'Lab Test Result','width':chartWidth,'height':parseData[0].length * 400
			    };

			    var options2 = {
			      title: 'Radiology Test Result','width':chartWidth,'height':parseData[0].length * 400
			    };

			    if (parseData[0][0]['Result Sent'] == undefined && parseData[0]['Result Pending'] == undefined) {
					$("#chart_div").hide();
				}
				else{
					var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
	        		chart.draw(labTestRecordGroup, options); 
				}
	        		 
				if (parseData[1][0]['Result Sent'] == undefined && parseData[1][0]['Result Pending'] == undefined) {
					$("#chart_div2").hide();
				}
				else{
					var chart2 = new google.visualization.PieChart(document.getElementById('chart_div_2'));
	        		chart2.draw(radiologyTestRecordGroup, options2);
				} 		
			}
		}			
	});
}	