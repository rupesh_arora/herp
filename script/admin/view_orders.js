/*
 * File Name    :   view_ordert.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Gupta
 * Created Date :   16th Feb, 2016
 * Description  :   This page is use for load and view data of orders
*/
var orderTable;
var subOrderTable;
var subSubOrdertable;
var sellerHiddenFieldValue;
$(document).ready(function() {
	loader();
	debugger;
	$(".amountPrefix").text(amountPrefix);
	$("#txtOrderDate").datepicker({ // date picker function
		autoclose: true
	});
	$("#receiveOrderDetails #txtOrderDate").click(function() {
		$(".datepicker").css({
			"z-index": "99999"
		});
	});
	orderTable = $('#viewOrderTable').dataTable({ // inilize datatable
        "bFilter": true,
        "bAutoWidth" : false,
        "bProcessing": true,
        "sPaginationType": "full_numbers",
        aoColumnDefs: [{'bSortable': false,'aTargets': [4]}]
    });
    subOrderTable = $('#viewSubOrderTable').dataTable({ // inilize datatable
        "bFilter": true,
        "bAutoWidth" : false,
        "bProcessing": true,
        "sPaginationType": "full_numbers",
        aLengthMenu: [	       
	        ["All"]//show all results at once
	    ],
        aoColumnDefs: [{'bSortable': false,'aTargets': [3]}]
    });
    subSubOrdertable = $('#viewSubSubOrderTable').dataTable({ // inilize datatable
        "bFilter": true,
        "bAutoWidth" : false,
        "bProcessing": true,
        "sPaginationType": "full_numbers",
        aLengthMenu: [	       
	        ["All"]//show all results at once
	    ],
        aoColumnDefs: [{'bSortable': false,'aTargets': [3]}]
    });


	/*hide block title when screen open as pop up*/
    $("#receiveOrderDetails .block-title, #receiveOrderDetails #searchsellerIcon").hide();
	
    $('#viewOrderDetails').hide();

    $("#btnSearch,#stockOrderDetails #btnSearch").on("click", function(){ // perform search functioanlity        
		var sellerId = $("#txtSellerId").val().trim();
        var date = $("#txtOrderDate").val().trim();
        var orderId = $("#txtOrderId").val().trim();
        var item = $("#txtItem").val().trim();
        var hdnId = $("#hdnId").val();
        
        var postData = {
            "operation": "searchOrderDetails",
            "sellerId": sellerId,
            "date": date,
            "orderId": orderId,
            "item": item
        }

        //this hidden field value is used when it is open as pop up in receive order to show only pending order
        if ($("#hdnFieldReceiveOrder").val() == "1") {
        	var postData = {
	            "operation": "searchOrderDetails",
	            "sellerId": sellerId,
	            "date": date,
	            "orderId": orderId,
	            "item": item,
	            "showPendingOrderOnly" : "showPendingOrderOnly"
	        }
        }

        // this hidden filed is used for inventory payment screen
        if ($("#inventoryPaymentHdnId").val() == "1") {
        	var postData = {
	            "operation": "searchOrderDetails",
	            "sellerId": sellerId,
	            "date": date,
	            "orderId": orderId,
	            "item": item,
	            "inventoryPaymentHdnId" : "inventoryPaymentHdnId"
	        }
        }
        // this hidden filed is used for stock payment screen
        if ($("#hdnFieldStockOrder").val() == 1) {
        	var postData = {
	            "operation": "searchOrderDetails",
	            "sellerId": sellerId,
	            "date": date,
	            "orderId": orderId,
	            "item": item,
	            "showReceivedOrderOnly" : "showReceivedOrderOnly"
	        }
        }
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/view_orders.php",
            datatype: "json",
            data: postData,

            success: function(data) {
            	if (data !=0 && data !='') {

            		dataSet = JSON.parse(data);

	                orderTable.fnClearTable();
	                orderTable.fnDestroy();
	                orderTable = $('#viewOrderTable').DataTable({
	                    "sPaginationType": "full_numbers",
						"bAutoWidth" : false,
	                    "fnDrawCallback": function(oSettings) {

	                    	$(".orderDetails").on('click', function() {
		                        var mData = orderTable.fnGetData(this);
		                        var id = $(this).attr('data-id');
		                        var postData = {
		                            "operation": "particularOrderDetail",
		                            "id": id
		                        }
		                        if ($("#hdnFieldReceiveOrder").val()==1) {	
		                        	subSubOrdertable.fnClearTable();	                        	                    			
	                    			$('#viewOrderDetails').show();
	    							$('#searchOrders').hide();
	    							$("#backToOrder").show();
	    							$('#receiveOrderDetails .block').addClass('customChange');
	    							$('#viewOrderDetailsReceiveOrder').show();
	    							$('#searchOrders').hide();
	    							$('#viewOrderDetails').hide();
	                    		}
	                    		else{
	                    			subOrderTable.fnClearTable();
	                    			//$("#myViewOrderDetailsModal").modal();
	                        		//$("#backToOrder").hide();
	                        		$('#viewOrderDetailsReceiveOrder').hide();
	                        		$('#viewOrderDetails').show();
	    							$('#searchOrders').hide();
	                    		}
		                        $.ajax({
		                            type: "POST",
		                            cache: false,
		                            url: "controllers/admin/view_orders.php",
		                            datatype: "json",
		                            data: postData,

		                            success: function(data) {
		                            	if (data != "0" && data != "") {

		                            		if ($("#hdnFieldReceiveOrder").val()==1) {			                            		
			                            		var parseData = jQuery.parseJSON(data);
			                            		var myTotalBill = 0;
		                                    	for (var i = 0; i < parseData.length; i++) {
		                                    		$("#lblOrderId").text(parseData[i].order_id);
		                                    		$("#lblSeller").text(parseData[i].seller_name);
		                                    		$("#lblDate").text(parseData[i].order_date);
		                                    		var totalCost = parseData[i].cost * parseData[i].quantity;                                    		
		                                    		 
		                                    		subSubOrdertable.fnAddData([(i+1),parseData[i].item_name,parseData[i].quantity,parseData[i].order_status]);
		                                    		myTotalBill = myTotalBill + parseInt(totalCost);
		                                    		$('#totalCostBill').text(myTotalBill);
		                                    	}
		                            		}

		                            		else{
			                            		var parseData = jQuery.parseJSON(data);
			                            		var myTotalBill = 0;
		                                    	for (var i = 0; i < parseData.length; i++) {
		                                    		$("#lblOrderId").text(parseData[i].order_id);
		                                    		$("#lblSeller").text(parseData[i].seller_name);
		                                    		$("#lblDate").text(parseData[i].order_date);
		                                    		//var totalCost = parseData[i].cost * parseData[i].quantity;                                    		
		                                    		 
		                                    		subOrderTable.fnAddData([(i+1),parseData[i].item_name,parseData[i].quantity,parseData[i].order_status]);
		                                    		myTotalBill = myTotalBill + parseInt(totalCost);
		                                    		$('#totalCostBill').text(myTotalBill);
		                                    	}
		                            		}			                            		
		                            	}
		                            }
		                        });
		                    });

	                    	/*Double click functionality*/
		                    $('#receiveOrderDetails #viewOrderTable tbody tr,#stockOrderDetails #viewOrderTable tbody tr').on('dblclick', function() {

	                            if ($(this).hasClass('selected')) {
	                                $(this).removeClass('selected');
	                            } else {
	                                orderTable.$('tr.selected').removeClass('selected');
	                                $(this).addClass('selected');
	                            }

	                            var mData = orderTable.fnGetData(this); // get datarow
	                            if (null != mData) // null if we clicked on title row
	                            {
	                                $('#txtReceiveOrderId').val(mData["order_id"]);
	                                $('#txtOrder').val(mData["order_id"]);
	                            }

	                            $("#btnShowOrderDetails").click();
	                            $("#btnShow").click();
	                            $(".close").click();
	                        });
	                    },
	                    "aaData": dataSet,
		                "aoColumns": [ 
			                {"mData": "order_id"}, 
			                {"mData": "seller_name"},
			                {"mData": "order_date"},
			                {"mData": "order_status"},
			                {"mData": function(o) {
			                        var data = o;
			                        var id = data["id"];
			                        return "<i class='orderDetails fa fa-eye' id='btnView' title='View' data-id=" + id + " " + "'></i>";
			                    }
			                },
			                {"mData": "id","bVisible": false}
		                ],
		                aoColumnDefs: [{'bSortable': false,'aTargets': [4]}]	                
	                });
            	}
            	else{
            		$("#txtOrderId").focus();            		
            		orderTable.fnClearTable();
            	}	                
            },
            error: function() {
			
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });        
    });

	$("#btnReset, #receiveOrderDetails #btnReset").click(function(){
		clear();
	});

	$('.close').click(function(){
		$('#receiveOrderDetails .block').removeClass('customChange');
	});
	$("#receiveOrderDetails #backToOrder").click(function(){
		$('#receiveOrderDetails #viewOrderDetails').hide();
	    $('#receiveOrderDetails #searchOrders').show();
	    $('#receiveOrderDetails .block').removeClass('customChange');
	    $('#viewOrderDetailsReceiveOrder').hide();
	});
    $("#searchsellerIcon").on('click',function(){
    	sellerHiddenFieldValue = 1;
    	$('#mySellerRequestModal').modal();
        $('#mySellerRequestModalBody').load('views/admin/view_seller.html');       
    });

    $("#txtOrderId, #receiveOrderDetails #txtOrderId").focus();

    $('#txtOrderId,#txtSellerId,#txtOrderDate,#txtItem').keypress(function (e) {
        if (e.which == 13) {
            $("#btnSearch").click();            
        }
    });

    $('#backToOrder').click(function(){
		$('#viewOrderDetails').hide();
	    $('#searchOrders').show();
	});
});
function clear(){
	$("#txtSellerId").val("");
	$("#txtOrderId").val("");
	$("#txtOrderDate").val("");
	$("#txtItem").val("");
	orderTable.fnClearTable();
	$("#txtOrderId").focus();
}