$(document).ready(function(){
/* ****************************************************************************************************
 * File Name    :   search_visit_pharmacy.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages visits data
 *************************************************************************************************** */
	loader(); 
	var otable;
	debugger;
	
	//varibale for time stammps
	var getTimeStamp;
	var getFromDateTimeStamp;
	var getToDateTimeStamp;
	
	otable = $('#tblPharmacyDispenseVisit').dataTable( {
		"bFilter": true,
		"processing": true,
		"bAutoWidth" : false,	
		"aaSorting": [],	
		"sPaginationType":"full_numbers",
		aoColumnDefs: [
			{ 'bSortable': false, 'aTargets': [ 4 ] }
		]		
	});
	
	//This code is used for show the date picker in view visit and related with this screen 
	$('#txtFromDate').datepicker();
	$('#txtToDate').datepicker();
	
	$("#myPharmacyModal #txtFromDate").click(function(){
		$(".datepicker").css({"z-index":"99999"});
	});
	
	$("#myPharmacyModal #txtToDate").click(function(){
		$(".datepicker").css({"z-index":"99999"});
	});
	
	$("#myPharmacyModal #txtToDate").change(function(){
		$("#txtToDate").datepicker("hide");
	});	
	$("#myPharmacyModal #txtFromDate").change(function(){
		$("#txtFromDate").datepicker("hide");
	});
	
	//This code is used for pharmacy dispense 
	//Code for click on search icon of pharmacy dispense
	$("#myPharmacyModal #btnReset").click(function(){
		$("#myPharmacyModal #txtFromDate").val("");
		$("#myPharmacyModal #txtToDate").val("");	
		$("#myPharmacyModal #txtPatientId").val("");	
		otable.fnClearTable();
			clear();
	});
	
	$("#myPharmacyModal #btnBackToList").click(function(){
		$("#myPharmacyModal #searchVisit").show();
		$("#myPharmacyModal #viewDetails").hide();
	});
	 
	$("#myPharmacyModal #btnSearch").click(function(){
		var flag = "false";
		var firstName = $ ("#myPharmacyModal #txtFirstName").val();
		var lastName = $ ("#myPharmacyModal #txtLastName").val();
		var mobile = $ ("#myPharmacyModal #txtMobile").val();
		var patientId = $ ("#myPharmacyModal #txtPatientId").val();
		var getFromDateTimeStamp = $("#myPharmacyModal #txtFromDate").val();
		var getToDateTimeStamp = $("#myPharmacyModal #txtToDate").val();
		
		if($("#myPharmacyModal #txtPatientId").val() != ""){
			if(patientId.length != 9){					
				flag = "true";
			}
			else{
				var patientId = $("#myPharmacyModal #txtPatientId").val();
				var patientPrefix = patientId.substring(0, 3);
				patientId = patientId.replace ( /[^\d.]/g, '' ); 
				patientId = parseInt(patientId);
			}
		}
		else{
			patientPrefix = "";
		}
		if (flag == "true") {
			return false;
		}
			
		var postData = {
			"operation":"searchPharmacyModal",
			"getFromDateTimeStamp" : getFromDateTimeStamp,
			"getToDateTimeStamp" : getToDateTimeStamp,
			"firstName" : firstName,
			"lastName" : lastName,
			"mobile" : mobile,
			"patientPrefix" : patientPrefix,
			"patientId" : patientId
		}

		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/view_visit.php",
			datatype:"json",
			data: postData,
			
			success: function(dataSet) {
				dataSet = JSON.parse(dataSet);
				//var dataSet = jQuery.parseJSON(data);
				otable.fnClearTable();
				otable.fnDestroy();
				otable = $('#myPharmacyModal #tblPharmacyDispenseVisit').DataTable( {
					"sPaginationType":"full_numbers",
					"bAutoWidth" : false,
					"aaSorting": [],	
					"fnDrawCallback": function ( oSettings ) {
						
						var aiRows = otable.fnGetNodes(); //Get all rows of data table

					 	if(aiRows.length == 0){
					 		return false;
					 	}
					 	for (var j=0,c=aiRows.length; j<c; j++) {
					 		var visitType = $(aiRows[j]).find('td:eq(1)').html();
					 		if(visitType == "Emergency Case"){
					 			$(aiRows[j]).css('background-color','#ff6666');
					 		}
					 	}

						$('#myPharmacyModal #tblPharmacyDispenseVisit tbody tr').on( 'dblclick', function () {
	
							if ( $(this).hasClass('selected') ) {
							  $(this).removeClass('selected');
							}
							else {
							  otable.$('tr.selected').removeClass('selected');
							   $(this).addClass('selected');
							}
							
							var mData = otable.fnGetData(this); // get datarow
							if (null != mData)  // null if we clicked on title row
							{
								//now aData[0] - 1st column(count_id), aData[1] -2nd, etc.	
								
								var visitId = mData["id"];
								var patientPrefix = mData["visit_prefix"];
									
								var visitIdLength = visitId.length;
								for (var i=0;i<6-visitIdLength;i++) {
									visitId = "0"+visitId;
								}
								visitId = patientPrefix+visitId;
								$('#txtVisitId').val(visitId);
							}							
							
							$("#btnSelect").click();
							$(".close").click();
										
						} );
						
						$("#myPharmacyModal .visitDetails").on('click',function(){
							var id = $(this).attr('data-id');
							var postData = {
								"operation":"visitDetailPharmacyModal",
								"id":id
							}
							
							$.ajax({					
								type: "POST",
								cache: false,
								url: "controllers/admin/view_visit.php",
								datatype:"json",
								data: postData,
								
								success: function(data) {
									if(data != "0" && data != ""){
										var parseData = jQuery.parseJSON(data);
														
										for (var i=0;i<parseData.length;i++) {

											$("#myPharmacyModal #lblDepartementType").text(parseData[i].type);
											$("#myPharmacyModal #lblName").text(parseData[i].name);
											$("#myPharmacyModal #lblServiceName").text(parseData[i].service_name);
											$("#myPharmacyModal #lblVisitDate").text(parseData[i].visit_date);
											$("#myPharmacyModal #lblTriageStatus").text(parseData[i].triage_status);
											if(parseData[i].payment_status != null){
												$("#myPharmacyModal #lblPaymentStatus").text(parseData[i].payment_status);
											}
											else{
												if(parseData[i].pay_status == 'unpaid'){
													$("#myPharmacyModal #lblPaymentStatus").text(parseData[i].pay_status);
												}
												else{
													$("#myPharmacyModal #lblPaymentStatus").text('unpaid');
												}
												
											}											
											var visitId = parseInt(parseData[i].id);
											var patientPrefix = parseData[i].prefix;
												
											var visitIdLength = visitId.toString().length;
											for (var j=0;j<6-visitIdLength;j++) {
												visitId = "0"+visitId;
											}
											visitid = patientPrefix+visitId;
											$("#myPharmacyModal #lblVisitId").text(visitid);
										} 
										$("#myPharmacyModal #searchVisit").hide();
										$("#myPharmacyModal #viewDetails").show();
									}
								},
								error:function() {
									$('#messageMyModalLabel').text("Error");
									$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
									$('#messagemyModal').modal();
								}
							});
						});
					},
					"aaData": dataSet,
					"aoColumns": [
						{  
							"mData": function (o) { 	
							var visitId = o["id"];
							var patientPrefix = o["visit_prefix"];
								
							var visitIdLength = visitId.length;
							for (var i=0;i<6-visitIdLength;i++) {
								visitId = "0"+visitId;
							}
							visitId = patientPrefix+visitId;
							return visitId; 
							}
						},
						/* { "mData": "id"}, */
						{ "mData": "type" },
						{ "mData": "name" },
						{ "mData": "visit_date" },
						{
						  "mData": function (o) { 				
								var data = o;
								var id = data["id"];
								var visitType = o["type"];
								if(visitType == "Emergency Case"){
									return "<i class='visitDetails fa fa-eye' id='btnView' visitType="+visitType+" title='View' data-id="+id+" "+ "></i>"; 
								}
								else{	
									return "<i class='visitDetails fa fa-eye' id='btnView' title='View' data-id="+id+" "+ "></i>"; 
								}	
							}
						},
					],
					aoColumnDefs: [
						{ 'bSortable': false, 'aTargets': [ 3 ] },
						{ 'bSortable': false, 'aTargets': [ 4 ] }
					]
				} );				
				
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	});

	$("#myPharmacyModal #txtFromDate").val(todayDate());
	$("#myPharmacyModal #txtToDate").val(todayDate());
	$("#myPharmacyModal #btnSearch").click();
	$("#myPharmacyModal #txtFromDate").val();
	$("#myPharmacyModal #txtToDate").val();

	$("#myPharmacyModal #btnBackToList").click(function(){
		$("#myPharmacyModal #searchVisit").show();
		$("#myPharmacyModal #viewDetails").hide();
	});
});