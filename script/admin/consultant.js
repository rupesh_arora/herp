var splitAge = '';
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   consultant.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Arora
 * Created Date :   30th dec, 2015
 * Description  :   This page  manages patient details for consultant
 *************************************************************************************************** */
    debugger;    
    loader();
    var oTableHistoryVisits;
	var first = "false";
	var show = "true";
	/*During page load hide complete page*/
	$('.advanced-first-consultant').hide();

	/*Load table during load of page*/
	$('#myConsultantModal').load('views/admin/view_visit.html', function() {
        $("#thing").val("3");
    });
	
	removeErrorMessage();
    // button load event
    $('#tabsConsultant').hide(); //by default all tabs remain hide 
    $('.startDiagonisViewHistory').hide();//by default hide both button
    $("#oldHistoryOfVisitsTable").hide();

    $("#selStaffType").change(function(){
		var option ="<option value='-1'>Select</option>";
		
		if ($('#selStaffType :selected').val() != -1) {
			var value = $('#selStaffType :selected').val();
			bindStaff(value);
		}
		else {
			$('#selWard').html(option);
		}
	});
    bindStaffType();

    $("#btnVisitLoad").click(function() {
        flag = false;
		var visitID = $("#textvisitId").val();
        if ($("#textvisitId").val().trim() == "") {
            $("#textvisitIdError").show();
            $("#textvisitIdError").text("Please enter visit ID");
            $("#textvisitId").addClass("errorStyle");
            clear();
            flag = true;
        }
		if(visitID.length != 9){
			$('#tabsConsultant').hide();
			$('#btnStartDiagnosis #btnRequestIpd').hide();
			$("#btnViewOldHistory").hide();
			$("#oldHistoryOfVisitsTable").hide();
			$("#textvisitIdError").show();
			$("#textvisitIdError").text("Please enter valid visit ID");
			$("#textvisitId").addClass("errorStyle");
			clear();     
			flag = true;
		}
        if (flag == true) {
            return false;
        }
        
		else {
			var visitID = $("#textvisitId").val();
			var visitPrefix = visitID.substring(0, 3);
			visitID = visitID.replace ( /[^\d.]/g, '' ); 
			visitID = parseInt(visitID);
            
            var postData = {
                    "operation": "visitConsultant",
                    "visitPrefix": visitPrefix,
                    "visitid": visitID
                }
                /*Ajax call to show patient details fro that visit*/
            $.ajax({
                type: "POST",
                cache: false,
                url: "controllers/admin/consultant.php",
                datatype: "json",
                data: postData,

                success: function(data) {
				    generalClick();
				    if (data != "0" && data != "") {
                        var parseData = jQuery.parseJSON(data);

                        var i = 0;
                        $("#txtRegisterService").val(parseData[i].serviceType);
                        $("#txtVisitType").val(parseData[i].type);
						var patientId = parseData[i].patient_id;
						var patientPrefix = parseData[i].patient_prefix;
                        $("#txtpatientName").val(parseData[i].Patient_Name);

                        var gender = parseData[i].gender;
						if(gender == "F"){
							$("#txtgender").val("Female");
						}
						else{
							$("#txtgender").val("Male");
						}
                        $("#txtTotalVisit").val(parseData[i].totalVisit);

                        if (parseData[i].opd_room_id != null && parseData[i].consultant_id != null) {
                            $('#btnStartDiagnosis').hide();
							$("#btnViewOldHistory,#btnRequestIpd").show();
                            $('#tabsConsultant').show();
                        }else{
                            $('#btnStartDiagnosis').show();
							$("#btnViewOldHistory").show();
							$('#tabsConsultant,#btnRequestIpd').hide();
                        }

                        //convert time string to date
                        visitTime = parseData[i].updated_on;
                        joinTime = parseData[i].created_on;

                        var date1 = new Date(visitTime * 1000); // date for visit date
                        var date = new Date(joinTime * 1000); // date for joining date

                        // calculate age
                        var getDOB = parseData[i].dob;
							
                        
						var patientIdLength = patientId.length;
						for (var i=0;i<6-patientIdLength;i++) {
							patientId = "0"+patientId;
						}
						patientId = patientPrefix+patientId;
						$("#txtPatientID").val(patientId);
						
						$.each($(".tab-pane"),function(index,value){
							if($(this).attr("data-url") != undefined){
								$(this).html("");
							}
						});
                        var get_date1 = date1.getDate();
                        var year1 = date1.getFullYear();
                        var month1 = date1.getMonth();
						month1 = month1 +1;
						if(month1 < "10"){
							month1 = "0"+month1;
						}
						 
						if(get_date1 < "10"){
							get_date1 = "0"+get_date1;
						}
                        $("#txtvisitDate").val( year1 + "-" + month1 + "-" + get_date1);						

                        var get_date = date.getDate();
                        var year = date.getFullYear();
                        var month = date.getMonth();
						month = month +1;
						if(month < "10"){
							month = "0"+month;
						}
						 
						if(get_date < "10"){
							get_date = "0"+get_date;
						}
                        $("#txtJoiningDate").val(year + "-" + month + "-" + get_date);

                        var new_age = getAge(getDOB); // call getAge function to calculate age

                        splitAge = new_age.split(' ');
                        $("#txtYear").val(splitAge[0]);
                        $("#txtMonth").val(splitAge[1]);
                        $("#txtDay").val(splitAge[2]);
                                              

                        $('.startDiagonisViewHistory').show();//show button when success
						
						$("#oldHistoryOfVisitsTable").hide();//show table
						$("#btnViewOldHistory").val("View Old History");

						$(".tabConsultantLst").unbind();
                        $(".tabConsultantLst").click(function(e) {
                            var id = $(e.target); //getting the click event tag
                            var contentId = id.attr("href"); //get id 
                            /*if (contentId == "#tabGeneral") {
                                $(".tabConsultantLst").find('li').removeClass('active');
                                $($(".tab-pane").removeClass('active')).addClass('fade');
                                $($($($($(".tabConsultantLst").first('ul').children('li:first').addClass('active').find('a').attr('href')).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active');
                                $(contentId).load('views/admin/triage_general.html');
                            }
                            else if(contentId == "#tabTestRequest"){
                                $(".tabConsultantLst").find('li').removeClass('active');
                                $($(".tab-pane").removeClass('active')).addClass('fade');
                                $($($($($(".tabConsultantLst").first('ul').children('li:first').addClass('active').find('a').attr('href')).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active');
                                $(contentId).load('views/admin/lab_test_request.html');
                            }
                            else if(contentId == "#tabServicesRequest"){
                                $(".tabConsultantLst").find('li').removeClass('active');
                                $($(".tab-pane").removeClass('active')).addClass('fade');
                                $($($($($(".tabConsultantLst").first('ul').children('li:first').addClass('active').find('a').attr('href')).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active');
                                $(contentId).load('views/admin/services_request.html');
                            }
                            else if(contentId == "#tabTestResults"){
                                $(".tabConsultantLst").find('li').removeClass('active');
                                $($(".tab-pane").removeClass('active')).addClass('fade');
                                $($($($($(".tabConsultantLst").first('ul').children('li:first').addClass('active').find('a').attr('href')).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active');
                                $(contentId).load('views/admin/lab_test_result.html');
                            }*/

                            /*on click if we get data url load page */
                            if (($(contentId).attr("data-url") != undefined) && ($("#textvisitId").val() != "" && $("#txtPatientID").val() != "")) {
                                loadUrl = $(contentId).attr("data-url");
                                /*Empty html page*/
                                var prevData = $(".tab-content .tab-pane");
                                $.each(prevData,function(index,value){
                                    if($(this).attr("data-url") != undefined){
                                        $(this).html("");
                                    }
                                });


                                //check this screen whether diagnosis of that screen had done or not
                                if (loadUrl == "procedures_request" || loadUrl == "prescription") {

                                	var visitId =  parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
									var postData ={
										"operation" : "checkDiagnosis",
										"visitId" : visitId,
									}
									$.ajax({
										type: "POST",
										cache: false,
										url: "controllers/admin/drug_prescription.php",
										datatype:"json",
										data: postData,
										success : function(data){
											if(data == "0" ){
												$('#messageMyModalLabel').text("Sorry");
												$('.modal-body').text("No diagnosis available for this patient");
												$('#messagemyModal').modal();
												return false;
											}
											else{
												$(contentId).load('views/admin/' + loadUrl + '.html');
											}
										}
									});
                                } 
                                else{
                                	$(contentId).load('views/admin/' + loadUrl + '.html');
                                }                               
                            }/*
                            else {
                                $("#textvisitId").focus();
                            }*/
                        });
                    }
                    if (data == "") {
                        $('#tabsConsultant').hide();
						$('#btnStartDiagnosis,#btnRequestIpd').hide();
						$("#btnViewOldHistory").hide();
						$("#oldHistoryOfVisitsTable").hide();
                        $("#textvisitIdError").show();
                        $("#textvisitIdError").text("Please enter valid visit ID");
                        $("#textvisitId").addClass("errorStyle");
                        clear();
                    }
                },
                error: function() {
                    $('#tabsConsultant').hide();
                    $('#messagemyModal').modal();
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                }
            });
        }
    });

    /*Back button functionality*/
    $('#btnBackCnsltViewList').click(function(){
    	$('#viewDetails').hide();
    	$('#myConsultantModal').show();
    	$(".advanced-first-consultant").hide();	
    	$("#tabsConsultant").hide();
    }); 

    /*on search icon function*/
    $("#serachVisitIcon").click(function() {
        $('#consultantModalLabel').text("Search Visits");
        $('#consultantModalBody').load('views/admin/view_visit.html', function() {
            $("#thing").val("3");
        });
        $('#myConsultantModal').modal();

        /*remove style*/
        $("#textvisitId").removeAttr("style");
        $("#textvisitId").removeClass("errorStyle");
        $('#textvisitIdError').text("");
    });

    /*on search icon function*/
    $("#btnTriage").click(function() {
        $('#triageModalLabel').text("Search Triage");
        $('#triageModalBody').load('views/admin/triage.html', function() {
            $("#thing").val("4");
        });
        $('#myTriageModal').modal();
    });

    /*On click of IPD request open pop up*/
    $("#btnRequestIpd").click(function() {     	
    	$("#myIPDRequestModal").modal();
    });
    /*On click show tabs*/
    $("#btnStartDiagnosis").click(function() {        
		generalClick();
        var visit_id = $("#textvisitId").val();
		var visitPrefix = visit_id.substring(0, 3);
		visit_id = visit_id.replace ( /[^\d.]/g, '' ); 
		visit_id = parseInt(visit_id);
        var postData = {
            "visit_id" : visit_id,
            "operation": "updateVisitConsultantId"
        }

        /*Ajax call to save opd room id and consultant id in visit table*/
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/consultant.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('#tabsConsultant').show();
					$('#btnStartDiagnosis').hide();
					$('#btnRequestIpd').show();
                }
                else{
                    $('#messagemyModal').modal();
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("You are not registered to any opd rooms");              
                }
            },
            error : function(){
                $('#messagemyModal').modal();
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");                
            }
        });
    });
	
    $("#btnSaveIPDRequest").click(function() {
    	var flag = 'false';
    	if(validTextField('#selStaffType','#selStaffTypeError','Please select staff type') == true)
		{
			flag = true;
		}
		if(validTextField('#selOperatingConsultant','#selOperatingConsultantError','Please select operating consultant') == true)
		{
			flag = true;
		}
		if(validTextField('#txtReason','#txtReasonError','Please enter reason') == true)
		{
			flag = true;
		}
		if (flag == true) {
			return false;
		}
		var staffTypeId = $("#selStaffType").val();
		var operatingConsultantId = $("#selOperatingConsultant").val();
		var reason = $("#txtReason").val().trim();
		var visitId = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
		var patientId = parseInt($("#txtPatientID").val().replace ( /[^\d.]/g, '' ));
		reason = reason.replace(/'/g, "&#39");
		var postData = {
            "operation": "saveIPDRequest",
            "staffTypeId": staffTypeId,
            "visitId" : visitId,
            "patientId" : patientId,
            "operatingConsultantId" : operatingConsultantId,
            "reason" : reason
        }        
        dataCall("controllers/admin/consultant.php", postData, function (result){
        	if (result.trim() == 1) {
        		$(".close").click();//close pop up
        		callSuccessPopUp('Success','Saved sucessfully!!!');
        		clearFormDetails("#IPDRequestModalBody");
        	}
        	else if (result.trim() == 0) {
        		$(".close").click();//close pop up
        		callSuccessPopUp('Alert','Visit already exist!!!');
        		clearFormDetails("#IPDRequestModalBody");
        	}
        });
    });


    $("#btnViewOldHistory").click(function() {
		generalClick();		
		if($("#btnViewOldHistory").val() == "View Old History"){			
			if($('#tabsConsultant').css('display') == 'none'){
				$('#btnStartDiagnosis').hide();
				show = "false";
			}
			$("#oldHistoryOfVisitsTable").show();//show table
			$('#tabsConsultant').hide(); //hide tabs
			$("#btnViewOldHistory").val("Back To List");
			var patientId = $("#txtPatientID").val();
			var visitPrefix = patientId.substring(0, 3);
			patientId = patientId.replace ( /[^\d.]/g, '' ); 
			patientId = parseInt(patientId);
			
			var postData = {
				"patientId" : patientId,
				"operation":"oldHistoryVisitsSearch"
			}
		
			$.ajax({                   
				type: "POST",
				cache: false,
				url: "controllers/admin/consultant.php",
				datatype:"json",
				data: postData,
				
				success: function(dataSet) {
					dataSet = JSON.parse(dataSet);
					if(first != "false"){
						oTableHistoryVisits.fnClearTable();
						oTableHistoryVisits.fnDestroy();
					}
					oTableHistoryVisits = $('#oldHistoryOfVisits').DataTable({
						"bPaginate": false,
						"bFilter": false,
						"aaData": dataSet,
						"bAutoWidth":false,
						aoColumnDefs: [{'bSortable': false,'aTargets': [5] }],
						"fnDrawCallback": function(oSettings) {
							$(".oldHistoryDetails").click(function() {
								generalOldHistoryClick();
								$('#oldHistoryModalDetails').modal();
								$.each($(".tab-pane"),function(index,value){
									if($(this).attr("data-url") != undefined){
										$(this).html("");
									}
								});
								$('#thing').val("4");//set hidden field value =4

								$('#oldHistoryModalDetails input[type=text]').attr('readonly', true);

								$("#thing").val("4");

								var visitId = $(this).attr('data-id');
								var visitDate = $(this).attr('data-visitDate');
								currentDate(visitDate);
								
								$("#oldHistoryVisitId").text(visitId);
								$("#oldHistoryModalDetails .tabViewConsultantLst").unbind();
								$("#oldHistoryModalDetails .tabViewConsultantLst").click(function(e) {
									var id = $(e.target); //getting the click event tag
									var contentId = id.attr("href"); //get id 

									/*on click if we get data url load page */
									if (($(contentId).attr("data-url") != undefined)) {
										loadUrl = $(contentId).attr("data-url");
										/*Empty html page*/
										var prevData = $(".tab-content .tab-pane");
										$.each(prevData,function(index,value){
											if($(this).attr("data-url") != undefined){
												$(this).html("");
											}
										});
										$(contentId).load('views/admin/' + loadUrl + '.html');
                                        //tabConsultantLst();
									} else {
										$(id).removeClass('active');
                                        var tab =  $(e.target).text();
                                        if(tab == 'General'){
                                            $("#oldHistoryModalDetails .tabViewConsultantLst").find('li').removeClass('active');
                                            $($(".tab-pane").removeClass('active')).addClass('fade');
                                            $($($($($("#oldHistoryModalDetails .tabViewConsultantLst").first('ul').children('li:first').addClass('active').find('a').attr('href')).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active')
                                            $('#tabViewTriage').load('views/admin/triage_general.html');
                                        }
                                        if(tab == 'Test Request'){
                                           $("#oldHistoryModalDetails .tabViewConsultantLst").find('li').removeClass('active');
                                            $($(".tab-pane").removeClass('active')).addClass('fade');
                                            $($($($(contentId).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active');
                                            $('#tabViewLabTestRequest').load('views/admin/lab_test_request.html');
                                            //$('#tabLabTestRequest').load('views/admin/lab_test_request.html');
                                        }
                                        if(tab == 'Services Request'){
                                            $("#oldHistoryModalDetails .tabViewConsultantLst").find('li').removeClass('active');
                                            $($(".tab-pane").removeClass('active')).addClass('fade');
                                            $($($($(contentId).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active');
                                            $('#divViewServicesRequest').load('views/admin/services_request.html');
                                            //$('#oldHistoryModalDetails #divServicesRequest').load('views/admin/services_request.html');
                                        }
                                        if(tab == 'Test Results'){
                                            $("#oldHistoryModalDetails .tabViewConsultantLst").find('li').removeClass('active');
                                            $($(".tab-pane").removeClass('active')).addClass('fade');
                                            $($($($(contentId).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active');
                                            $('#tabViewLabTestResult').load('views/admin/lab_test_result.html');
                                            //$('#oldHistoryModalDetails #tabLabTestResult').load('views/admin/lab_test_result.html');
                                        }
									}
								});
							});
						},
						
						"aoColumns": [
							{  
								"mData": function (o) { 	
								var visitId = o["visit_id"];
								var patientPrefix = o["visit_prefix"];
									
								var visitIdLength = visitId.length;
								for (var i=0;i<6-visitIdLength;i++) {
									visitId = "0"+visitId;
								}
								visitId = patientPrefix+visitId;
								return visitId; 
								}
							},
							/* { "mData": "visit_id"}, */
							{ "mData": "visit_type"},                            
							{ "mData": function(o){
								var dbDateTimestamp = o;
								var dateObj = new Date(dbDateTimestamp["visit_date"]*1000);
								var yyyy = dateObj.getFullYear().toString();
								var mm = (dateObj.getMonth()+1).toString(); // getMonth() is zero-based
								var dd  = dateObj.getDate().toString();
								return yyyy +"-"+ (mm[1]?mm:"0"+mm[0]) +"-"+ (dd[1]?dd:"0"+dd[0]); // padding
								} 
							},
							{ "mData": "service_name" },
							{ "mData": "consultant_name" },
							{
								"mData": function(o) {
									var data = o;
									var id = data["visit_id"];
									var patientPrefix = data["visit_prefix"];
									
									var visitIdLength = id.length;
									for (var i=0;i<6-visitIdLength;i++) {
										id = "0"+id;
									}
									id = patientPrefix+id;
									var visitDate = data["visit_date"];
									return "<i class='oldHistoryDetails fa fa-eye' id='eyeViewOldHistory' title='View' data-visitDate = "+ visitDate +" data-id=" + id + " " + "'></i>";
								}
							}
						]
					});
					first = "true";
				}        
			});
		}
        else{
			$("#oldHistoryOfVisitsTable").hide();//show table
			if(show == "false"){
				$('#btnStartDiagnosis').show();
				$('#btnRequestIpd').hide();
				show = "true";
			}
			else if(show == "true"){
				if($('#btnStartDiagnosis').css('display') == 'none'){
					$('#tabsConsultant').show(); //hide tabs
				}	
			}
						
			$("#btnViewOldHistory").val("View Old History");
			$('#thing').val("3");
		}
    });

	$("#oldHistoryModalDetails #pop_up_close").click(function(){
		$.each($(".tab-pane"),function(index,value){
			if($(this).attr("data-url") != undefined){
				$(this).html("");
			}
		});
	});

	// for remove error style	
    $("#textvisitId").keyup(function() {
        if ($("#textvisitId").val().trim() != "") {
            $("#textvisitIdError").hide();
            $("#textvisitIdError").text("");
            $("#textvisitId").removeClass("errorStyle");
            clear();
        } else {
            $("#textvisitIdError").show();
            $("#textvisitIdError").text("Please enter visit ID");
            $("#textvisitId").removeClass("errorStyle");
            clear();
        }
    });

    $('#textvisitId').keypress(function (e) {
        if (e.which == 13) {
            $("#btnVisitLoad").click();
        }
    });

   tabConsultantLst();
	
});
function calculateTimeStamp(date) {
    var myDate = date;
    myDate = myDate.split("-");
    var newDate = myDate[1] + "/" + myDate[2] + "/" + myDate[0]; //mm-dd-yyyy
    getTimeStamp = (new Date(newDate).getTime());
    return getTimeStamp;
}

    
/*key press event on ESC button*/
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        $(".close").click();
    }
});

function tabConsultantLst(){
     $($(".tabConsultantLst")[0]).find('li').click(function(){
        $(this).removeClass('active');
        var tab = $(this).find('a').text();
        if(tab == 'General'){
            $(".tabConsultantLst").find('li').removeClass('active');
            $($(".tab-pane").removeClass('active')).addClass('fade');            
            $($($($($(this).addClass('active').find('a').attr('href')).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active');
            $('#tabTriage').load('views/admin/triage_general.html');
        }
        if(tab == 'Test Request'){
            $(".tabConsultantLst").find('li').removeClass('active');
            $($(".tab-pane").removeClass('active')).addClass('fade');            
            $($($($($(this).addClass('active').find('a').attr('href')).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active');
            $('#tabLabTestRequest').load('views/admin/lab_test_request.html');
        }
        if(tab == 'Services Request'){
            $(".tabConsultantLst").find('li').removeClass('active');
            $($(".tab-pane").removeClass('active')).addClass('fade');            
            $($($($($(this).addClass('active').find('a').attr('href')).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active');
            $('#divServicesRequest').load('views/admin/services_request.html');
        }
        if(tab == 'Test Results'){
            $(".tabConsultantLst").find('li').removeClass('active');
            $($(".tab-pane").removeClass('active')).addClass('fade');            
            $($($($($(this).addClass('active').find('a').attr('href')).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active');
            $('#tabLabTestResult').load('views/admin/lab_test_result.html');
        }
    });
}

//This function is used for convert timestamp to date 
function currentDate(visitDate){
	var fullDate = new Date(visitDate* 1000)
	
	//convert month to 2 digits
	var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '' + (fullDate.getMonth()+1);
	var todayDay = fullDate.getDate();
	
	if(twoDigitMonth < "10"){
		twoDigitMonth = "0"+twoDigitMonth;
	}
	 
	if(todayDay < "10"){
		todayDay = "0"+todayDay;
	}
	 
	var currentDate = fullDate.getFullYear() + "-" + twoDigitMonth+ "-" + todayDay ;		
	 
	$("#oldHistoryVisitDate").text(currentDate);
}
function bindStaffType() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/staffregistration.php",
        data: {
            "operation": "showstafftype"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
                }
                $('#myIPDRequestModal #selStaffType').html(option);
            }
        },
        error: function() {}
    });
}
function bindStaff(value) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/staffregistration.php",
        data: {
            "operation": "showStaff",
            "value" : value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#myIPDRequestModal #selOperatingConsultant').html(option);
            }
        },
        error: function() {}
    });
}
/*function calculate age*/
function getAge(dateString) {
    var now = new Date();
    var today = new Date(now.getYear(), now.getMonth(), now.getDate());

    var yearNow = now.getYear();
    var monthNow = now.getMonth();
    var dateNow = now.getDate();

    var dob = new Date(dateString.substring(0, 4), dateString.substring(5, 7) - 1, dateString.substring(8, 10));

    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();
    var age = {};
    var ageString = "";
    var yearString = "";
    var monthString = "";
    var dayString = "";


    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    age = {
        years: yearAge,
        months: monthAge,
        days: dateAge
    };

    if (age.years > 1) yearString = " years";
    else yearString = " year";
    if (age.months > 1) monthString = " months";
    else monthString = " month";
    if (age.days > 1) dayString = " days";
    else dayString = " day";


    if ((age.years > 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else ageString = "Oops! Could not calculate age!";

    return ageString;
}
function generalClick(){
	$(".tabConsultantLst").find('li').removeClass('active');
	$($(".tab-pane").removeClass('active')).addClass('fade');
	$($($($($(".tabConsultantLst").first('ul').children('li:first').addClass('active').find('a').attr('href')).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active');
	$('#tabTriage').load('views/admin/triage_general.html');
 }
function generalOldHistoryClick(){
	$("#oldHistoryModalDetails .tabViewConsultantLst").find('li').removeClass('active');
	$($(".tab-pane").removeClass('active')).addClass('fade');
	$($($($($("#oldHistoryModalDetails .tabViewConsultantLst").first('ul').children('li:first').addClass('active').find('a').attr('href')).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active')
	$('#tabViewTriage').load('views/admin/triage_general.html');
 }
function clear() {
    $("#txtRegisterService").val("");
    $("#txtvisitDate").val("");
    $("#txtVisitType").val("");
    $("#txtPatientID").val("");
    $("#txtpatientName").val("");
    $("#txtJoiningDate").val("");
    
    $("#txtgender").val("");
    $("#txtTotalVisit").val("");
}
//# sourceURL = consultant.js