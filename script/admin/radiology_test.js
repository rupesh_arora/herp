var radiologyTable;
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   radiology_test.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages radiology test data
 *************************************************************************************************** */
	loader();
	debugger;
	
	$("#advanced-wizard").hide();
	$("#radiologyTestList").addClass('list');
	$("#tabRadiologyTestList").addClass('tab-list-add');
	bindGLAccount("#selGLAccount",'');
	bindAccountReceivable("#selAccountPayable");
	bindLedger("#selLedger");//defined in common.js
	
	//Click function for change the tab
    $("#tabAddRadiologyTest").click(function() {    	
    	showAddTab();      
    	clear(); 
        
    });
	//Click function for show the list
	 $("#tabRadiologyTestList").click(function() {
		 $('#inactive-checkbox-tick').prop('checked', false).change();
        tabRadiologyTestList();//Calling the function for show radiology test list
    });
	
	// on change of select file type
	$("#file").on("change", function() {
		$('#txtFileError').hide();
	});
	
	// import excel on submit click
	$('#importDataFile').click(function(){
		var flag = false;
		if($('#file').prop('files')[0] == undefined || $('#file').val() == "") {
			$('#txtFileError').show();
			$('#txtFileError').text("Please choose file.");
			flag  = true;
		}
		if(flag == true){
			return false;
		}
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to upload this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			var file_data = $('#file').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: 'controllers/admin/radiology_test.php', // point to server-side PHP script 
				dataType: 'text', // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					if(data != "" && data != "invalid file" && data == "1"){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Your file has been successfully imported!!!");
						$('#messagemyModal').modal();
						$('#file').val("");
					}
					else if(data == "invalid file"){
						$('#txtFileError').show();
						$('#txtFileError').text("Please import only CSV/XLS file.");
						$('#file').val("");
					}
					else if (data =="You must have three column in your file i.e radiology test name, radiology test fee and description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="First column should be radiology test name" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Second column should be radiology test fee" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }                    
                    else if (data =="Third column should be description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Please insert data in first , second and third column only i.e A , B and C") {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if(data == "Data can't be null in radiology test name and radiology test fee column"){
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data == "Radiology test fee should be number") {
                    	$('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
					else if(data == ""){
						$('#txtFileError').show();
						$('#txtFileError').text("Data already exist.");
						$('#file').val("");
					}
				},
				error: function() {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Data Not Found!!!");
					$('#messagemyModal').modal();

				}
			});
		});
	});
	
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		loadDataTable();	
    }
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	radiologyTable.fnClearTable();
	    	radiologyTable.fnDestroy();
	    	//Datatable code
			radiologyTable = $('#tblRadiology').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"bAutoWidth" : false,
				"fnDrawCallback": function ( oSettings ) {
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  radiologyTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/radiology_test.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					{ "mData": "name" },
					{ "mData": "fee" },
					{ "mData": "description" },
					{
					  "mData": function (o) { 
						var data = o;
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},	
				],
				aoColumnDefs: [
		          	{ 'bSortable': false, 'aTargets': [ 2 ] },
					{ 'bSortable': false, 'aTargets': [ 3 ] }
		       	]
			} );
		}
		else{
			radiologyTable.fnClearTable();
	    	radiologyTable.fnDestroy();
	    	//Datatable code
			loadDataTable();
		}
    });
	
	//		
    //validation and ajax call on submit button and save the data
	//
    $("#btnSubmit").click(function() {
        var flag = "false";
		$("#txtTestfee").val($("#txtTestfee").val().trim());
		
		$("#txtRadiologyTestName").val($("#txtRadiologyTestName").val().trim());
		
		if ($("#selGLAccount").val() == "") {
			$('#selGLAccount').focus();
            $("#selGLAccountError").text("Please select gl account");
			$("#selGLAccount").addClass("errorStyle");
            flag = "true";
        }
		if ($("#selAccountPayable").val() == "") {
			$('#selAccountPayable').focus();
            $("#selAccountPayableError").text("Please select account payable");
			$("#selAccountPayable").addClass("errorStyle");
            flag = "true";
        }
        if ($("#selLedger").val() == "") {
			$('#selLedger').focus();
            $("#selLedgerError").text("Please select ledger");
			$("#selLedger").addClass("errorStyle");
            flag = "true";
        }
		if ($("#txtTestfee").val() == "") {
			$('#txtTestfee').focus();
            $("#txtTestfeeError").text("Please enter test fee");
			$("#txtTestfee").addClass("errorStyle");                
            flag = "true";
        }
		if(!(/^\d*(\.\d{1})?\d{0,9}$/).test($('#txtTestfee').val()))
		{			
			$('#txtTestfee').focus();
            $("#txtTestfeeError").text("Please enter number only");
			$("#txtTestfee").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtRadiologyTestName").val() == "") {
			$('#txtRadiologyTestName').focus();
            $("#txtRadiologyTestNameError").text("Please enter radiology test name");
			$("#txtRadiologyTestName").addClass("errorStyle");
            flag = "true";
        } 
        
        if (flag == "true") {
            return false;
        }
		var ledger = $("#selLedger").val();
		var glAccount = $("#selGLAccount").val();
		var accountPayable = $("#selAccountPayable").val();
		var testName = $("#txtRadiologyTestName").val();		
		var testFee = $("#txtTestfee").val();
		var description = $("#txtDescription").val();
		description = description.replace(/'/g, "&#39;");
		var postData = {
			"operation":"save",
			"testName":testName,
			"ledger":ledger,
			"glAccount":glAccount,
			"accountPayable":accountPayable,
			"testFee":testFee,
			"description":description
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/radiology_test.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Radiology test saved successfully!!!");
					$('#messagemyModal').modal();
					$('#inactive-checkbox-tick').prop('checked', false).change();
					tabRadiologyTestList();
				}
				else{
					$("#txtRadiologyTestNameError").text("Test name already exists");
					$("#txtRadiologyTestName").css({"border-color":"#a94442"});
					$("#txtRadiologyTestName").focus();
				}				
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	});	
	
	//
    //keyup functionality
	//
	
	$("#selLedger").change(function() {
        if ($("#selLedger").val() != "") {
            $("#selLedgerError").text("");
            $("#selLedger").removeClass("errorStyle");
        }
    });
	$("#selAccountPayable").change(function() {
        if ($("#selAccountPayable").val() != "") {
            $("#selAccountPayableError").text("");
            $("#selAccountPayable").removeClass("errorStyle");
        }
    });
    $("#selGLAccount").change(function() {
        if ($("#selGLAccount").val() != "") {
            $("#selGLAccountError").text("");
            $("#selGLAccount").removeClass("errorStyle");
        }
    });
	$("#selUnitMeasure").change(function() {
        if ($("#selUnitMeasure").val() != "-1") {
            $("#selUnitMeasureError").text("");
            $("#selUnitMeasure").removeClass("errorStyle");
        }
    });
	
    $("#txtRadiologyTestName").keyup(function() {
        if ($("#txtRadiologyTestName").val() != "") {
            $("#txtRadiologyTestNameError").text("");
            $("#txtRadiologyTestName").removeClass("errorStyle");
        }
    });
	
	
	$("#txtTestfee").keyup(function() {
        if ($("#txtTestfee").val() != "") {
            $("#txtTestfeeError").text("");
            $("#txtTestfee").removeClass("errorStyle");
        }
    });

     //on reset click focus on first textbox
    $("#btnReset").click(function(){
    	$("#txtRadiologyTestName").focus();
    	clear();
    });
});

//		
//function for edit the bed 
//
function editClick(id,name,fee,description,ledger_id,gl_account_id,account_payable_id){	
    
	showAddTab();
	$("#uploadFile").hide();
    $("#btnReset").hide();
    $("#btnSubmit").hide();
    $("#btnUpdateRadiology").show();
    $('#tabAddForensicTest').html("+Update Radiology Test");
    $("#txtRadiologyTestName").focus();
	$("#txtRadiologyTestName").val(name);
	$('#txtTestfee').val(fee);
	$('#selLedger').val(ledger_id);
	$('#selAccountPayable').val(account_payable_id);
	$('#selGLAccount').val(gl_account_id);
	$('#txtDescription').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id);	
	
	$("#btnUpdateRadiology").click(function(){
		var flag = "false";
		$("#txtTestfee").val($("#txtTestfee").val().trim());
		
		$("#txtRadiologyTestName").val($("#txtRadiologyTestName").val().trim());
		if ($("#selGLAccount").val() == "") {
			$('#selGLAccount').focus();
            $("#selGLAccountError").text("Please select gl account");
			$("#selGLAccount").addClass("errorStyle");
            flag = "true";
        }
		if ($("#selAccountPayable").val() == "") {
			$('#selAccountPayable').focus();
            $("#selAccountPayableError").text("Please select account payable");
			$("#selAccountPayable").addClass("errorStyle");
            flag = "true";
        }
        if ($("#selLedger").val() == "") {
			$('#selLedger').focus();
            $("#selLedgerError").text("Please select ledger");
			$("#selLedger").addClass("errorStyle");
            flag = "true";
        }
		if ($("#txtTestfee").val() == "") {
			$('#txtTestfee').focus();
            $("#txtTestfeeError").text("Please enter test fee");
			$(" #txtTestfee").addClass("errorStyle");
            flag = "true";
        }
		if(!(/^\d*(\.\d{1})?\d{0,9}$/).test($('#txtTestfee').val()))
		{			
			$('#txtTestfee').focus();
            $("#txtTestfeeError").text("Please enter number only");
			$("#txtTestfee").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtRadiologyTestName").val() == "") {
			$('#txtRadiologyTestName').focus();
            $("#txtRadiologyTestNameError").text("Please enter test name");
			$("#txtRadiologyTestName").addClass("errorStyle");
            flag = "true";
        }		
		
		if(flag == "true"){			
			return false;
		}
		else{
			var testName = $("#txtRadiologyTestName").val();
			var testFee = $("#txtTestfee").val();
			var ledger = $("#selLedger").val();
			var glAccount = $("#selGLAccount").val();
			var accountPayable = $("#selAccountPayable").val();
			var labType = $("#selLabType").val();
			var description = $("#txtDescription").val();
			description = description.replace(/'/g, "&#39;");
			var id = $('#selectedRow').val();			
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/radiology_test.php",
					data :{            
						"operation" : "update",
						"id" : id,
						"testName":testName,
						"ledger":ledger,
						"glAccount":glAccount,
						"accountPayable":accountPayable,
						"testFee":testFee,
						"description":description
					},
					success: function(data) {	
						if(data != "0" && data != ""){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Radiology test updated successfully!!!");
							radiologyTable.fnReloadAjax();
							tabRadiologyTestList();
							$('#messagemyModal').modal();
							clear();
						}
						
						else{
							$("#txtRadiologyTestNameError").text("Test name already exists");
							$("#txtRadiologyTestName").addClass("errorStyle");
							$("#txtRadiologyTestName").focus();
						
						}
					},
					error:function() {
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
				});
			});
		}
	});		
}
//		
//function for delete the radiology test
//
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete radiology test");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteRadiologyTest("'+type+'");');
}

//Click function for restore the radiology test
function restoreClick(id){
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Restore radiology test");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();

	var type="restore";
	$('#confirm').attr('onclick','deleteRadiologyTest("'+type+'");');
}

//Function for delete and restore the data 
function deleteRadiologyTest(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
			
			
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/radiology_test.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Radiology test deleted successfully!!!");
					$('#messagemyModal').modal();
					radiologyTable.fnReloadAjax();					 
				}
			},
			error: function()
			{
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal(); 
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/radiology_test.php",
			data :{            
				"operation" : "restore",
				"id" : id,
			},
			success: function(data) {	
				if(data == "Restored Successfully!!!" && data != ""){
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Radiology test restored successfully!!!");
					radiologyTable.fnReloadAjax();
					$('#messagemyModal').modal();
				}	
				if(data == "It can not be restore!!!"){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("It can not be restored!!!");
					radiologyTable.fnReloadAjax();
					$('#messagemyModal').modal();
					tabRadiologyTestList();
				}
			},
			error:function()
			{
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	}
}

//
//Function for clear the data
//
function clear(){
	$('#selLabTypeError').text("");
	$('#selSpecimenType').val("");
	$('#selSpecimenTypeError').text("");
	$('#selUnitMeasure').val("");
	$('#selUnitMeasureError').text("");
	$('#txtFileError').text('');
	$('#txtRadiologyTestName').val("");
	$('#txtRadiologyTestNameError').text("");
	$('#txtNormalRange').val("");
	$('#txtNormalRangeError').text("");
	$('#txtTestfee').val("");
	$('#txtTestfeeError').text("");
	$('#txtDescription').val("");
	$('#selLedger').val("");
	$('#selGLAccount').val("");
	$('#selAccountPayable').val("");
	$('#selLedgerError').text("");
	$('#selAccountPayableError').text("");
	$('#selGLAccountError').text("");
	$("#selAccountPayable").removeClass("errorStyle");
	$("#selLedger").removeClass("errorStyle");
	$("#selGLAccount").removeClass("errorStyle");
	$("#txtRadiologyTestName").removeClass("errorStyle");
	$("#selSpecimenType").removeClass("errorStyle");
	$("#selUnitMeasure").removeClass("errorStyle");
	$("#txtTestfee").removeClass("errorStyle");
	$("#selAccountPayable").removeClass("errorStyle");
	$("#selLedger").removeClass("errorStyle");
	$("#selGLAccount").removeClass("errorStyle");
}
//
//function for binding the room
//
/* function bindUnitMeasure(){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/radiology_test.php",
		data: {
			"operation":"showUnitMeasure"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>-- Select --</option>";
				for (var i=0;i<parseData.length;i++)
				{
				option+="<option value='"+parseData[i].id+"'>"+parseData[i].unit+"</option>";
				}				
				$('#selUnitMeasure').html(option);				
			}
		},
		error:function(){	
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();		
		}
	});	
}

//Function for bind the specimen type
function bindSpecimenType(){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/radiology_test.php",
		data: {
			"operation":"showSpecimenType"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>-- Select --</option>";
				for (var i=0;i<parseData.length;i++)
				{
				option+="<option value='"+parseData[i].id+"'>"+parseData[i].type+"</option>";
				}				
				$('#selSpecimenType').html(option);				
			}
		},
		error:function(){	
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();		
		}
	});	
} */

//Function for show the radiology test list
function tabRadiologyTestList(){
	$("#advanced-wizard").hide();
    $(".blackborder").show();

    $("#tabAddRadiologyTest").removeClass('tab-detail-add');
    $("#tabAddRadiologyTest").addClass('tab-detail-remove');
    $("#tabRadiologyTestList").removeClass('tab-list-remove');    
    $("#tabRadiologyTestList").addClass('tab-list-add');
    $("#radiologyTestList").addClass('list');


    $("#uploadFile").show();
    $("#btnReset").show();
    $("#btnSubmit").show();
    $("#btnUpdateRadiology").hide();
    $('#tabAddForensicTest').html("+Add Radiology Test");
	 
	clear();  
        
	 $("#txtTestfee").removeClass("errorStyle");
	 $("#txtNormalRange").removeClass("errorStyle");
	 $("#selUnitMeasure").removeClass("errorStyle");
	 $("#selLabType").removeClass("errorStyle");
	 $("#txtRadiologyTestName").removeClass("errorStyle");
	 $("#selSpecimenType").removeClass("errorStyle");
		
}

function showAddTab(){
	$("#advanced-wizard").show();
    $(".blackborder").hide();
	$('#txtRadiologyTestName').focus();
    
    $("#tabAddRadiologyTest").addClass('tab-detail-add');
    $("#tabAddRadiologyTest").removeClass('tab-detail-remove');
    $("#tabRadiologyTestList").removeClass('tab-list-add');
    $("#tabRadiologyTestList").addClass('tab-list-remove');
    $("#radiologyTestList").addClass('list');
}
// on escape remove pop up
$(document).keyup(function(event) {
    if(event.keyCode === 27) {
        $('.close').click();
    }
});

function loadDataTable(){
	radiologyTable = $('#tblRadiology').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"bAutoWidth" : false,
		"fnDrawCallback": function ( oSettings ) {
			$('.update').unbind();
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = radiologyTable.fnGetData(data);
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					var name = mData["name"];
					var fee = mData["fee"];
					var ledger_id = mData["ledger_id"];
					var gl_account_id = mData["gl_account_id"];
					var account_payable_id = mData["account_payable_id"];
					var description = mData["description"];
					editClick(id,name,fee,description,ledger_id,gl_account_id,account_payable_id);
					
   
				}
			});
			$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  radiologyTable.fnGetData(data);
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);
				}
			});
		},
		
		"sAjaxSource":"controllers/admin/radiology_test.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{ "mData": "name" },
			{ "mData": "fee" },
			{ "mData": "description" },
			{
			  "mData": function (o) { 
				var data = o;
				return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
				   " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
				   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
				   " style='font-size: 22px; color:#a94442; cursor:pointer;' "+
				   " data-original-title='Delete'></i>";
			  }
				
			},	
		],
		aoColumnDefs: [
			{ 'bSortable': false, 'aTargets': [ 2 ] },
			{ 'bSortable': false, 'aTargets': [ 3 ] }
		]
	} );
}
//# sourceURL = radiology.js