/**********************************************************************************************/
/**********************************************************************************************/
/**********************************************************************************************/
/* Hidden field for this screen is reviewStaffHiddenId used to do some operation***************/
/**********************************************************************************************/
/**********************************************************************************************/
/**********************************************************************************************/


var reviewStaffTable; //defing a global variable for datatable
$(document).ready(function() {
	loader();  
    debugger;
    reviewStaffTable = $('#reviewStaffTable').DataTable({
        "bPaginate": false,
        aoColumnDefs: [{'bSortable': false,'aTargets': [5] }]
    });

    getStaffReviewDetails();

    //on star click color the stars
    $(".fa-star").click(function() {
        var getStarParent = $(this).parent().attr('class').split(" ")[1];//get the parent div class
        colorStar(getStarParent);//call function  to remove current div star rating    
        $(this).prevAll().andSelf().addClass("activeStar");
    });

    $("#iconStaffSearch").click(function() {
        $("#myViewStaffModal").modal();
        $("#txtStaffId").val("");
        $("#viewStaffModalBody").load('views/admin/view_staff.html');
        $('#txtStaffIdError').text('');
        $('#txtStaffId').removeClass('errorStyle');
    });

    removeErrorMessage();// remove error messaages


    //save data on click to db
    $("#btnSaveEntitlement").click(function() {
    	var flag = 'false';
        var countStarDependablity = $(".countStarDependablity .activeStar").length;
        var countStarCooperativeness = $(".countStarCooperativeness .activeStar").length;
        var countStarAdaptability  = $(".countStarAdaptability .activeStar").length;
        var countStarSercice = $(".countStarSerive .activeStar").length;
        var countStarAttedance = $(".countStarAttedance .activeStar").length;
        var countStarCommunication = $(".countStarCommunication .activeStar").length;
    	if(validTextField('#txtStaffId','#txtStaffIdError','Please enter staff id') == true)
		{
			return false;
		}
        if (countStarDependablity == 0 || countStarCooperativeness == 0 ||countStarAdaptability == 0 || 
            countStarSercice == 0 || countStarAttedance == 0 || countStarCommunication == 0) {            
            callSuccessPopUp('Alert','Please fill all the perfomance measure.');
            return false;
        }
		if (flag == true) {
			return false;
		}
        var staffId = $("#txtStaffId").val().trim();            
        if (staffId !='') {
            if (staffId.length != 9) {
                $("#txtStaffId").addClass('errorStyle');
                $("#txtStaffId").focus();
                $("#txtStaffIdError").text("Please enter valid staff id");
                return false;
            }
            var getStaffPrefix = staffId.substring(0, 3);
            if (getStaffPrefix.toLowerCase() != staffPrefix.toLowerCase()) {
                $("#txtStaffId").addClass('errorStyle');
                $("#txtStaffId").focus();
                $("#txtStaffIdError").text("Please enter valid staff id");
                return false;
            }
            staffId = staffId.replace ( /[^\d.]/g, '' ); 
            staffId = parseInt(staffId);
        }

        var staffName = $("#txtStaffName").val().trim();
        var comments = $("#txtComments").val().trim();
        comments = comments.replace(/'/g, "&#39");
        var postData = {
            "operation": "save",
            "staffId": staffId,
            "staffName" : staffName,
            "countStarDependablity" : countStarDependablity,
            "countStarCooperativeness" : countStarCooperativeness,
            "countStarAdaptability" : countStarAdaptability,
            "countStarSercice" : countStarSercice,
            "countStarAttedance" : countStarAttedance,
            "countStarCommunication" : countStarCommunication,
            "comments" : comments
        }        
        dataCall("controllers/admin/review_staff.php", postData, function (result){
        	if (result.trim() == 1) {
        		callSuccessPopUp('Success','Saved sucessfully!!!');
                $(".fa-star").removeClass("activeStar");
        		clearFormDetails(".clearForm");
                getStaffReviewDetails();
        	}
            else if (result.trim() == 0) {
                $('#txtStaffId').focus();
                $('#txtStaffIdError').text("Staff id and staff name doesn't match with each other");
                $('#txtStaffId').addClass('errorStyle');
            }
        	else{
        		callSuccessPopUp('Error','Try to refresh page once');
        		clearFormDetails(".clearForm");
        	}
        });
    });

    //on blur make call to get staff name based on id
    $("#txtStaffId").blur(function(){
        getStaffName();
    });

     $('#txtStaffId').keypress(function (e) {
        getStaffName();
     });
    //reset data
    $("#btnReset").click(function(){
	    clearFormDetails(".clearForm");//function define in common.js
        $(".fa-star").removeClass("activeStar");
    });
});

// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});

//function to make a call to get data in datatable
function getStaffReviewDetails(){
    var postData = {
        "operation" : "viewStaffReviewDetails"
    }
    dataCall("controllers/admin/review_staff.php", postData, function (result){
        var parseData = JSON.parse(result);
        reviewStaffTable.fnClearTable();//clear data in datatable
        for(var i=0; i< parseData.length; i++){
            var staffId = parseData[i].staff_id;
            var comments = parseData[i].comments;
            var review_by = parseData[i].review_by;
            var staffName = parseData[i].staff_name;
            var reviewDate = parseData[i].review_date;
            var id = parseData[i].id;
            var dependablityRating = parseInt(parseData[i].dependability_rating);
            var cooperativenessRating = parseInt(parseData[i].cooperativeness_rating);
            var adaptabilityRating = parseInt(parseData[i].adaptability_rating);
            var serciceRating = parseInt(parseData[i].sercice_rating);
            var attedanceRating = parseInt(parseData[i].attedance_rating);
            var communicationRating = parseInt(parseData[i].communication_rating);
            var aggregateRating = (dependablityRating+cooperativenessRating+adaptabilityRating+serciceRating+attedanceRating+communicationRating)/6;
            aggregateRating = Math.round(aggregateRating * 100) / 100;//for rpound off upto 2 digits

            //add data to datatable
            reviewStaffTable.fnAddData([staffId,staffName,aggregateRating, review_by,reviewDate,
                '<i class="fa fa-eye" onclick="viewData($(this));" title="View"></i>',comments,
                dependablityRating,cooperativenessRating,adaptabilityRating,serciceRating,
                attedanceRating,communicationRating
            ]);//send one column null so user can't remove previous data
        }
    });
}
function viewData(mythis){
    var currRow = $(mythis).closest('tr')[0];
    var aData = reviewStaffTable.fnGetData(currRow);
    $('#myReviewStaffModal').modal();
    $("#lblStaffId").text(aData[0]);
    $("#lblStaffName").text(aData[1]);
    $("#lblReviewBy").text(aData[3]);
    $("#lblReviewDate").text(aData[4]);
    $("#lblComments").text(aData[6]);
    if($("#lblComments").text() == ''){
        $("#lblComments").text("N/A");
    }
    var dependedStarRating ='';
    var cooperativenessStarRating ='';
    var adaptabilityStarRating ='';
    var serciceStarRating ='';
    var attedanceStarRating ='';
    var communicationStarRating ='';

    $.each(aData,function(index,value){
        //start for loop after getting index 7
        if (index >= 7) {
            for(var i=0;i<value;i++){
                if (index == 7) {
                    dependedStarRating+='<i class="fa fa-star activeStar"></i>';
                } 
                if (index == 8) {
                    cooperativenessStarRating+='<i class="fa fa-star activeStar"></i>';
                }            
                if (index == 9) {
                    adaptabilityStarRating+='<i class="fa fa-star activeStar"></i>';
                } 
                if (index == 10) {
                    serciceStarRating+='<i class="fa fa-star activeStar"></i>';
                } 
                if (index == 11) {
                    attedanceStarRating+='<i class="fa fa-star activeStar"></i>';
                } 
                if (index == 12) {
                    communicationStarRating+='<i class="fa fa-star activeStar"></i>';
                } 
            }
        }
    });    
    //append stars
    $(".generateDependedRating").html(dependedStarRating);
    $(".generateCooperativenessRating").html(cooperativenessStarRating);
    $(".generateAdaptabilityRating").html(adaptabilityStarRating);
    $(".generateSerciceRating").html(serciceStarRating);
    $(".generateAttedanceRating").html(attedanceStarRating);
    $(".generateCommunicationRating").html(communicationStarRating);
}
function colorStar(getStarParent){
    $("."+getStarParent+" .fa-star").removeClass("activeStar");
    $("#starRatingError").text("");    
}
function getStaffName(){
    var staffId = $("#txtStaffId").val().trim();
    if (staffId !='') {
        if (staffId.length != 9) {
            return false;
        }
        var getStaffPrefix = staffId.substring(0, 3);
        if (getStaffPrefix.toLowerCase() != staffPrefix.toLowerCase()) {
            return false;
        }
        staffId = staffId.replace ( /[^\d.]/g, '' ); 
        staffId = parseInt(staffId);
    }  
    if (staffId !='') {
        var postData = {
            "operation": "searchStaff",
            "staffId": staffId
        } 
        dataCall("controllers/admin/review_staff.php", postData, function (result){
            var parse = JSON.parse(result);
            if (parse != '' && result != null && result !='0') {
                $("#txtStaffName").val(parse[0].first_name);
            }
            else{
                $("#btnReset").click();
                callSuccessPopUp('Alert','Staff id doesn\'t exist!!!');
                $("#txtStaffName").val('');
                $("#txtStaffId").focus();                                
                return false;
            }
        });     
    }
}