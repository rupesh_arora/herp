var inventoryResponseProcessTable;
var inventoryResponseProceedTable;
var itemId;
var requestQuantity;
var unitCost;
var printData = [];
var TotalCost;
var grandTotal= 0;
var finalQuantity;
var finalItemName;
var printDiv;
$(document).ready(function(){
	$(".amountPrefix").text(amountPrefix);
	printData = [];
	debugger;
	$("#selInventoryType").focus();
	// bind inventory type in drop down
	bindInvenoryType("#selInventoryType");	
	
	inventoryResponseProcessTable = $("#inventoryResponseProcessTbl").dataTable({
		"bFilter": true,
		"processing": true,
		"bAutoWidth":false,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {
			$("#inventoryResponseProcessTbl .process").on('change', function(){
				if($(this).is(":checked")){
					$(this).closest("tr").css('background-color','#ff6666');
				}
				else{
					$(this).closest("tr").css('background-color','#E7E3E4');
				}
			});

			$('#inventoryResponseProcessTbl td:nth-child(3)').on('dblclick', function() {
				
				  var previousData = $(this).text();
				  var stockValue = $($(this).siblings()[1]).text();
				  var parentObj = $(this);
				  $(this).addClass("cellEditing"); 
				  $(this).html("<input type='text' value='" + previousData + "' />");
				  $(this).children().first().keypress(function (e) { 
				   if (e.which == 13) {
						var newContent = parseInt($(this).val());
						 $(this).parent().removeClass("cellEditing");    
						if(stockValue > newContent) {
							$(this).parent().text(newContent);	
						}
						else{
						    $(this).parent().text(stockValue);	
						}												
					}
				});
				$(this).children().first().blur(function (e) {
					var newContent = parseInt($(this).val());
					 $(this).parent().removeClass("cellEditing");    
					if(stockValue > newContent) {
						$(this).parent().text(newContent);	
					}
					else{
						$(this).parent().text(stockValue);	
					}
				});
			});			
		},
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 5 ] }
		]
	});
	
	/*inventoryResponseProceedTable = $("#inventoryResponseProceedTbl").dataTable({
		"bFilter": true,
		"processing": true, 
		"bAutoWidth":false,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {
			$('#myInventoryResponseModal #inventoryResponseProceedTbl td:nth-child(3)').on('dblclick', function() {
				
				  var previousData = $(this).text();
				  var parentObj = $(this);
				  $(this).addClass("cellEditing"); 
				  $(this).html("<input type='text' value='" + previousData + "' />");
				  $(this).children().first().keypress(function (e) { 
				   if (e.which == 13) {
						var newContent = parseInt($(this).val());
						 $(this).parent().removeClass("cellEditing");    
						if(stockValue > newContent) {
							$(this).parent().text(newContent);	
						}
						else{
						    $(this).parent().text(stockValue);	
						}												
					}
				});
				$(this).children().first().blur(function (e) {
					var newContent = parseInt($(this).val());
					 $(this).parent().removeClass("cellEditing");    
					if(stockValue > newContent) {
						$(this).parent().text(newContent);	
					}
					else{
						$(this).parent().text(stockValue);	
					}
				});
			});			
		},
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 2 ] }
		]
	});*/
	
	// remove key up error message
	removeErrorMessage();
	
	//btn show click details
	$("#btnShow").on("click",function(){
		inventoryResponseProcessTable.fnClearTable();
		var flag = false;
		if(validTextField("#selInventoryType","#selInventoryTypeError","Please select inventory type") == true) {
			flag = true;
		}
		
		if(flag ==  true) {
			return false;
		}
		
		var inventoryType = $("#selInventoryType").val();
		// define value for post data in back end file
		var postData = {
			"operation":"showDetails",
			inventoryType:inventoryType
		};

		// call the function ajax call
		dataCall("./controllers/admin/inventory_response.php", postData, function (data) {
			
			var parseData = jQuery.parseJSON(data);
			if (parseData != null && parseData != "") {	
				$(parseData).each(function(){
					var itemName = this.name;
					var stock = this.stock;
					var quantity = this.quantity;
					if(quantity != "0" && quantity != null) {
						requestQuantity = this.received_quantity;
						var orderDate = this.ordered_date;
						var orderBy = this.user_name;
						itemId = this.item_id;
						unitCost = this.unit_cost;
						

							/*$('.process').unbind();
							$('.process').on('click',function(){
								inventoryResponseProceedTable.fnClearTable();
								var data=$(this).parents('tr')[0];
								var mData = inventoryResponseProcessTable.fnGetData(data);
								$("#myInventoryResponseModal").modal();
								var length = inventoryResponseProceedTable.fnGetData().length;							
								inventoryResponseProceedTable.fnAddData([(length+1),mData[0],mData[2]]);
						});*/
						if(parseInt(stock) >= parseInt(quantity)){ 							
							inventoryResponseProcessTable.fnAddData([itemName,stock,quantity,orderDate,orderBy,'<input type="checkbox" data-id="'+itemId+'" item-cost="'+unitCost+'" request-quantity="'+requestQuantity+'" class="process" />']);
										
						}
						else{ //Add row if stock is less than quantity 
							inventoryResponseProcessTable.fnAddData([itemName,stock,quantity,orderDate,orderBy,'<input type="checkbox" class="process" data-id="'+itemId+'" item-cost="'+unitCost+'" request-quantity="'+requestQuantity+'"  checked="checked" disabled="disabled"  />']);
							inventoryResponseProcessTable.find('tbody tr:first').css('background-color','#ff6666');
						}
					}
				});
			}	
			else{
				// call message pop up function.
				callSuccessPopUp('Alert','No request quantity available.');
				return false;
			}
		});
	});
	
	$("#btnSubmit").on("click",function(){

		var flag = false;
		if(validTextField("#selInventoryType","#selInventoryTypeError","Please select inventory type") == true) {
			flag = true;
		}

		var rowcollection =  inventoryResponseProcessTable.$(".process:unchecked", {"page": "all"});	//Select all unchecked row only
		var checkedRowcollection =  inventoryResponseProcessTable.$(".process:checked", {"page": "all"});	//Select all unchecked row only
		var inventoryType = $("#selInventoryType").val();

		if(rowcollection.length == 0){
			if(checkedRowcollection.length == 0){
				callSuccessPopUp('Alert','No request quantity available.');
			}
			else{
				callSuccessPopUp('Alert','Please uncheck any one.');
			}			
			flag = true;
		}

		
		if(flag ==  true) {
			return false;
		}
		var itemId;
		var dataArray = [];
		//Getting data from all unchecked rows 
		for (i=0,c=rowcollection.length; i<c; i++) {

			var obj ={};
			var printObj = {};
			obj.itemName = $(rowcollection[i]).parent().parent().find('td:eq(0)').text();
			obj.itemStock = $(rowcollection[i]).parent().parent().find('td:eq(1)').text();
			obj.itemQuantity = $(rowcollection[i]).parent().parent().find('td:eq(2)').text();
			var id = $(rowcollection[i]).attr('data-id');
			obj.itemId =id;
			obj.itemCost =$(rowcollection[i]).attr('item-cost');
			obj.requestQuantity =$(rowcollection[i]).attr('request-quantity');
			obj.stockQuantity = parseInt($(rowcollection[i]).parent().parent().find('td:eq(1)').text()) - parseInt($(rowcollection[i]).parent().parent().find('td:eq(2)').text());
			dataArray.push(obj);
			if(i==0){
				itemId = id;
			}
			else{
				itemId = itemId+','+id;
			}
			


			printObj.itemName = $(rowcollection[i]).parent().parent().find('td:eq(0)').text();
			printObj.itemStock = $(rowcollection[i]).parent().parent().find('td:eq(1)').text();
			printObj.itemQuantity = $(rowcollection[i]).parent().parent().find('td:eq(2)').text();
			printObj.itemId =$(rowcollection[i]).attr('data-id');
			printObj.itemCost =$(rowcollection[i]).attr('item-cost');
			printObj.requestQuantity =$(rowcollection[i]).attr('request-quantity');
			var total= parseInt($(rowcollection[i]).parent().parent().find('td:eq(2)').text()) * parseInt($(rowcollection[i]).attr('item-cost'));
			printObj.TotalCost =total;
			printData.push(printObj);
			grandTotal = grandTotal +total;

			//var quantity = $(data).find('td:nth-child(3)').text();
			//TotalCost = 
			//finalQuantity = quantity;
			//finalItemName = $(data).find('td:nth-child(2)').text();
		}
 		// define value for post data in back end file
		var postData = {
			"operation":"saveDetails",
			itemId:itemId,			
			inventoryType:inventoryType,			
			"dataArray":JSON.stringify(dataArray),
		};
		generateReceiptNo(postData);
		/* // call the function ajax call
		dataCall("./controllers/admin/inventory_response.php", postData, function (data) {
			if (data != null && data != "") {
				$("#myInventoryResponseModal").modal('hide');
				// call message pop up function.
				$("#messageMyResponseModal").modal();
				$("#mymessageResponseModalLabel").text("Success");
				$('#paymentMsg').text("Saved successfully !!!");
				$("#btnShow").click();
				
				
			}
		}); */
	});
	
	/*Print functionality*/
	$("#messageMyResponseModal #btn_print").click(function(){		
		/*Call print function*/
		//$.print("#PrintDiv");
		window.print();
		//$(".close").unbind();
		//$(".close").click();
		finalItemName = "";
		finalQuantity = "";
		TotalCost = "";
		unitCost = "";
	});
	
	// reset functionality*/
	$("#btnReset").on('click',function(){
		clearDetails();
	});	
});

function clearDetails() {
	inventoryResponseProcessTable.fnClearTable();
	printData = [];
	$("#selInventoryType").focus();
	$("#selInventoryType").val("");
	$("#selInventoryTypeError").text("");
	$("#selInventoryType").removeClass('errorStyle');
	
}
// call function for load inventory type
function bindInvenoryType(selInventoryType) {
	var postData = {
		"operation": "showInventoryType"
    }

    $.ajax({
        type: "POST",
        cache: false,
        url: "./controllers/admin/inventory_response.php",
        datatype: "json",
        data: postData,
        success: function(data) {
            if (data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";

                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
                }

                $(selInventoryType).html(option);
            }
        },
        error: function() {
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();
        }
    });
}

function generateReceiptNo(objCashData){
	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/cash_payment.php",
		datatype:"json",
		data: {
			"operation":"receipt"
		},					
		success: function(data) {
			if(data != null && data != ""){
				var receiptNo = parseInt(data.match(/-*[0-9]+/));
				receiptNo = receiptNo + 1;
				var receiptNoLength = receiptNo.toString().length;
				for(i=0;i<6-receiptNoLength;i++){
					receiptNo = "0"+receiptNo;
				}
				reciptNumber = receiptNo;//to fetch data during print time
				$("#reciptNoMsg").text(receiptNo);
				var htmlPrintData = printDetails();	

				objCashData.printDiv = htmlPrintData;
				objCashData.receiptNo = receiptNo;
				confirmSaveDetails(objCashData);			
			}			
			else{
				$("#reciptNoMsg").text("000001" );
			}			
		},
		error : function(){			
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
	});
}

function printDetails(){
	/*firstly empty page after that load data*/
	$("#codexplBody").find("tr:gt(0)").html("");
	$("#printTotalBill").text("");
	/*Load data to print page*/
	$("#lblPrintReceipt").text($("#reciptNoMsg").text());
	$("#lblPrintDate").text(todayDate());
	/*hospital information through global varibale in main.js*/
	$("#lblPrintEmail").text(hospitalEmail);
	$("#lblPrintPhone").text(hospitalPhoneNo);
	$("#lblPrintAddress").text(hospitalAddress);
	if (hospitalLogo != "null") {
		imagePath = "./images/" + hospitalLogo;
		$("#printLogo").attr("src",imagePath);
	}
	$("#printHospitalName").text(hospitalName);
	$("#printTotalBill").text(' '+amountPrefix+' '+grandTotal);
	//$("#").text();
	for(var i=0;i<printData.length;i++){
		$("#codexplBody").append('<tr><td>'+ i+1 +'</td><td>'+printData[i].itemName+'</td><td>'
		+printData[i].itemQuantity+'</td><td>'+printData[i].itemCost+'</td><td>'+printData[i].TotalCost+'</td>');
	}

	return $("#PrintDiv").html();
}

function confirmSaveDetails(objCashData){
 // call the function ajax call
	dataCall("./controllers/admin/inventory_response.php", objCashData, function (data) {
		if (data != null && data != "") {
			$("#myInventoryResponseModal").modal('hide');
			// call message pop up function.
			$("#messageMyResponseModal").modal();
			$("#mymessageResponseModalLabel").text("Success");
			$('#paymentMsg').text("Saved successfully !!!");
			clearDetails();
		}
	});
}