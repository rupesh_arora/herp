/*
 * File Name    :   view_item.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   17 feb, 2016
 * Description  :   This page use for load and send data to back hand.
 */
  var tblViewItem; // define tblViewItem variable for datatable

$(document).ready(function() {
loader();   
debugger;
    tblViewItem = $('#tblViewItem').dataTable({ // inilize datatable on load time.
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
		"bAutoWidth" : false,
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [3]
        }]
    });
	
		// load category 
	   $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/view_item.php",
        data: {
            "operation": "showcategory"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].category + "</option>";
                }
                $('#txtCategory').html(option);
            }
        },
        error: function() {}
    });

   /* search functioanlty on search button*/
    $("#viewItemForm #btnSearch").click(function() {
		if ($("#myItemModal #itemValue").val() != "1" && $("#myItemissuanceModal #itemValue").val() != "2" 
			&& $("#myOrderItemModal #hdnItemId").val() != "3" && $("#configureItemPriceModal #hdnConfigureItem").val() != "1") {
			var category = $("#txtCategory").val();
			var code = $("#txtCode").val().trim();
			var name = $("#txtName").val().trim();

			var postData = {
				"operation": "search",
				"category": category,
				"code": code,
				"name": name
			}

			loadItems('viewItemForm','','itemId','','','itemPrice','',postData);
		}
    });

    $("#myItemModal #btnReset").click(function() {
        $("#myItemModal #txtCategory").val("");
        $("#myItemModal #txtCode").val("");
        $("#myItemModal #txtName").val("");
        tblViewItem.fnClearTable();
    });

    //search details on click in search in stocking screen
    $("#myItemModal #btnSearch").click(function() {
		 tblViewItem.fnClearTable();
		var category = $("#myItemModal #txtCategory").val();
		var code = $("#myItemModal #txtCode").val().trim();
		var name = $("#myItemModal #txtName").val().trim();

		var postData = {
			"operation": "searchReceiveItem",
			"category": category,
			"code": code,
			"name": name
		}
		
		if ($("#myItemModal #itemValue").val() == "1") {	
			loadItems('myItemModal','txtItem','itemId','','txtExpireDate','','txtQuantity',postData);		
		}
	});
	
	$("#myOrderItemModal #btnReset").click(function() {
        $("#myOrderItemModal #txtCategory").val("");
        $("#myOrderItemModal #txtCode").val("");
        $("#myOrderItemModal #txtName").val("");
        tblViewItem.fnClearTable();
    });
	
	//search details on click in search in put order screen  
    $("#myOrderItemModal #btnSearch").click(function() {
		 tblViewItem.fnClearTable();
		var category = $("#myOrderItemModal #txtCategory").val();
		var code = $("#myOrderItemModal #txtCode").val().trim();
		var name = $("#myOrderItemModal #txtName").val().trim();

		var postData = {
			"operation": "search",
			"category": category,
			"code": code,
			"name": name
		}
		
		if($("#myOrderItemModal #hdnItemId").val() == "3"){
			loadItems('myOrderItemModal','txtItem','','','','','',postData);
		}
	});
	
	$("#configureItemPriceModal #btnSearch").click(function() {
		 tblViewItem.fnClearTable();
		var category = $("#configureItemPriceModal #txtCategory").val();
		var code = $("#configureItemPriceModal #txtCode").val().trim();
		var name = $("#configureItemPriceModal #txtName").val().trim();

		var postData = {
			"operation": "search",
			"category": category,
			"code": code,
			"name": name
		}		
		if ($("#hdnConfigureItem").val()==1) {
			loadItems('configureItemPriceModal','txtConfItem','','txtOldConfItemPrice','','','',postData);
		}
	});
	
	$("#myItemissuanceModal #btnReset").click(function() {
        $("#myItemissuanceModal #txtCategory").val("");
        $("#myItemissuanceModal #txtCode").val("");
        $("#myItemissuanceModal #txtName").val("");
        tblViewItem.fnClearTable();
    });

    //search details on click in search modal for patient visit for view opd booking screen 
    $("#myItemissuanceModal #btnSearch").click(function() {
		 tblViewItem.fnClearTable();
		if ($("#myItemissuanceModal #itemValue").val() == "2") {
			var category = $("#myItemissuanceModal #txtCategory").val();
			var code = $("#myItemissuanceModal #txtCode").val().trim();
			var name = $("#myItemissuanceModal #txtName").val().trim();

			var postData = {
				"operation": "searchStockItem",
				"category": category,
				"code": code,
				"name": name
			}
			
			loadItems('myItemissuanceModal','txtItem','itemID','itemCost','txtExpiryDate','itemPrice','',postData);		
		} 
	});
});

function loadItems(modalId,itemId,itemIdValue,itemCost,itemExpireDate,itemPrice,quantity,postData){
	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/view_item.php",
		datatype: "json",
		data: postData,

		success: function(data) {
			dataSet = JSON.parse(data);
			if(dataSet.length > 0) {
				//var dataSet = jQuery.parseJSON(data);
				tblViewItem.fnClearTable();
				tblViewItem.fnDestroy();
				tblViewItem = $('#'+ modalId + ' #tblViewItem').DataTable({
					"sPaginationType": "full_numbers",
					"bAutoWidth" : false,
					"fnDrawCallback": function ( oSettings ) {
						$('#'+ modalId + ' #tblViewItem  tbody tr').on( 'dblclick', function () {
							var mData = tblViewItem.fnGetData(this);
							if (null != mData)  // null if we clicked on title row
							{
								$("#"+itemId).val(mData["name"]);
								$("#"+itemId).attr("item-id",mData["id"]);
								$("#"+itemId).attr("item-receive-date",mData["receive_date"]);								
								$("#"+itemId).attr("item-Quantity",mData["quantity"]);
								$("#"+itemIdValue).val(mData["id"]);
								$("#"+itemCost).val(mData["unit_cost"]);
								$("#"+itemPrice).val(mData["price"]);
								$("#"+itemPrice).attr("item-quantity",mData["Quantity"]);
								if(mData["exp_date"] == "0000-00-00") {
									$("#checkNA").prop('checked',true);
								} else {
									$("#checkNA").prop('checked',false);
									$("#"+itemExpireDate).val(mData["exp_date"]);
								}
								$("#"+quantity).val(mData["quantity"]);
								
								$("#txtConfItemDesc").text(mData["description"]);
								
							}
							$('#'+ modalId +' #pop_up_close').click();
						});
					},
					"aaData": dataSet,
					"aoColumns": [{
					"mData":  "categoryName"
					},{
						"mData": "code"
					}, {
						"mData": "name"
					}, {
						"mData": "description"
					} ],
					aoColumnDefs: [{
						'bSortable': false,
						'aTargets': [3]
					}]
				});
			}
		},
		error: function() {
			
			$('.modal-body').text("");
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
	});
}