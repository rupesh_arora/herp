
$(document).ready(function() {
	debugger;
	getAttendanceDetails();
	/*call this function from commmon.js*/
	DateTime();
	$("#btnPunchOut").hide();
	$("#remark").hide();
	$("#lblDate").text(todayDate());

	$("#btnPunchIn").click(function() {
		$("#btnPunchIn").hide();
		$("#btnPunchOut").show();
		$("#remark").show();
		$("#lblPunchIn").text(DateTime());
		var punch = $("#lblDate").text(todayDate());
		var date = todayDate();
		var punchIn = $("#lblPunchIn").text();
		
		var postData = {
			date : date,
			"operation" : "punchIn"
		}
		 $.ajax({
			type : "POST",
			cache : false,
			url : "controllers/admin/attendance.php",
			datatype: "JSON",
			data : postData,

			success:function(data) {
				if (data !='' || data !=null || data !="0") {
					callSuccessPopUp("Success","Punched in Successfully At "+ $("#lblPunchIn").text());

				}
			},
			error : function() {
				
			}
		});
	});

	$("#btnPunchOut").click(function() {
		$("#btnPunchOut").hide();
		$("#btnPunchIn").show();
		$("#remark").hide();
		$("#lblPunchOut").text(DateTime());
		var pucnhOut = $("#txtPunchOut").text();

		var remarks = $("#txtRemarks").val();

		var postData = {
			remarks : remarks,
			"operation" : "punchOut"
		}

		$.ajax({
			type : "POST",
			cache : false,
			url : "controllers/admin/attendance.php",
			datatype: "JSON",
			data : postData,

			success:function(data) {
				if (data !='' || data !=null || data !="0") {
					callSuccessPopUp("Success","Punched Out Successfully At "+$("#lblPunchOut").text());
					clear();
					getAttendanceDetails();
				}
			},
			error : function() {}
		});
	});
	function clear() {
		$("#lblPunchIn").text('');
		$("#lblPunchOut").text('');
		$("#txtRemarks").val('');
	}
});
function getAttendanceDetails(){
	 $.ajax({
			type : "POST",
			cache : false,
			url : "controllers/admin/attendance.php",
			datatype: "JSON",
			data :{
				"operation" : "todayAttendanceDetails"
			},

			success:function(data) {
				if (data != '' && data != null && data != "0" && data.length > 2) {
					var parse = JSON.parse(data);					
					if (parse[0].punch_out == '0') {
						$("#lblPunchIn").text(parse[0].punch_in);
						$("#btnPunchOut").show();
						$("#btnPunchIn").hide();
						$("#remark").show();
					}

				}
				else{
					$("#btnPunchOut").hide();
					$("#btnPunchIn").show();
					$("#remark").hide();
				}
			},
			error : function() {}
		});
}