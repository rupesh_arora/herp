var patientOTTable; //defi
var OTYear = '';
var OTMonth = "";
var OTDate = "";
var splitTime = '';
var fromOTHour = '';
var fromOTMins = '';
var toOTHour = '';
var toOTMins = '';
var splitTime = '';
var completeToTime = '';
var completeFromTime = '';
var arraySurgeon = [];
$(document).ready(function() {
    loader(); 
    debugger;
    bindISurgery();
    bindISurgeon();
    bindIAnesthesian();
    bindOtRooms();
   
    $(".input-datepicker").datepicker({autoclose: true});
    $(".input-datepicker").click(function(){
        $(".datepicker").css({"z-index":"99999"});        
    });


    $("#txtFromTime").prop('disabled',true);
    $("#txtToTime").prop('disabled',true);

    $("#txtFromTime").timepicker().on('change', function(time) {
         $("#txtToTime").prop('disabled',false);
         $('#txtToTime').val('');
         var time = $("#txtFromTime").val();
          $('#txtToTime').timepicker('option', 'minTime', time);
    });
        
    /*$("#txtFromTime").change(function(){
        $("#txtToTime").prop('disabled',false);
        completeFromTime = $("#txtFromTime").val();
        //$("#txtToTime").val('');
        splitTime = completeFromTime.split(':');
        fromOTHour = splitTime[0];
        fromOTMins = parseInt(splitTime[1])+30;
        if (OTYear !='' && OTDate !='' && OTMonth !='') {
            $("#txtToTime").timepicker({
                timeFormat: 'H:i:s',
                startTime: new Date(OTYear, OTMonth, OTDate,fromOTHour,fromOTMins),
                minTime: new Date(OTYear, OTMonth, OTDate,fromOTHour,fromOTMins)
            });
        }            
    }) 
    $("#txtToTime").change(function(){
        completeToTime = $("#txtToTime").val();
        //$("#txtToTime").val('');
        splitTime = completeToTime.split(':');
        toOTHour = splitTime[0];
        toOTMins = parseInt(splitTime[1])+30;          
    });*/

    var startDate = new Date();

    $('#txtOperationDate').datepicker('setStartDate', startDate);

    removeErrorMessage();
    $("#form_dept").hide();
    $("#designation_list").addClass('list');
    $("#tab_dept").addClass('tab-list-add');
    $("#tab_add_dept").click(function() { // click on add designation tab
        showTableList();
    });
    $("#tab_dept").click(function() { // click on list designation tab
        showAddTab();
    });

    $("#searchPatientIcon ,#searchPatientIcon").click(function() {          // show popup for search patient
        
        $('#selfRequestModalLabel').text("Search Patient");
        $('#selfRequestModalBody').load('views/admin/view_patients.html');
        $('#mySelfRequestModal').modal();
    });
    debugger;
    $("#searchSurgeonIcon").on('click',function(){

        var postData = {
            operation : "showSurgeon"
        }
        showStaff(postData,'chkSurgeon','chkSurgeon','Search Surgeon','appendSurgeon','');
    });

    $("#searchAnesthesianIcon").on('click',function(){
        var postData = {
            operation : "showAnesthesian"
        }
        showStaff(postData,"chkAnesthesian","chkAnesthesian","Search Anesthesian","appendAnesthesian",'');
    });

    $("#searchNurseIcon").on('click',function(){
        var postData = {
            operation : "showNurse"
        }
        showStaff(postData,"chkNurse","chkNurse","Search Nurse","appendNurse",'');
    });

    $('#mySelfRequestModal #viewPatientTable tbody tr').on('dblclick', function() {

        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            otable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        var mData = otable.fnGetData(this); // get datarow
        if (null != mData) // null if we clicked on title row
        {
            $('#txtPatientid').val(mData["patient_id"]);
        }

        $("#btnShowOrderDetails").click();
        $("#btnShow").click();
        $(".close").click();
    });

    $('#searchPatientIcon').click(function(){
        $('#txtPatientidError').text('');
        $('#txtPatientid').removeClass('errorStyle');
    });

    //blur function for surgeon,anasthesian & nurse
    $("#txtSurgeon").blur(function(){
        $("#txtSurgeon").val('');
    });
    $("#txtAnesthesian").blur(function(){
        $("#txtAnesthesian").val('');
    });
    $("#txtNurse").blur(function(){
        $("#txtNurse").val('');
    });

    removeErrorMessage();//remove error messages



    $("#btnSavePatientOperation").click(function() {
        var flag = false;

        var patientId = $("#txtPatientid").val().trim();            
        if (patientId !='') {
            if (patientId.length != 9) {
                $("#txtPatientid").addClass('errorStyle');
                $("#txtPatientid").focus();
                $("#txtPatientidError").text("Please enter valid patient id");
                return false;
            }
            var getPatientPrefix = patientId.substring(0, 3);
            if (getPatientPrefix.toLowerCase() != patientPrefix.toLowerCase()) {
                $("#txtPatientid").addClass('errorStyle');
                $("#txtPatientid").focus();
                $("#txtPatientidError").text("Please enter valid patient id");
                return false;
            }
            patientId = patientId.replace ( /[^\d.]/g, '' ); 
            patientId = parseInt(patientId);
        } 

        if($("#txtOperationDate").val() == todayDate() && $('#txtFromTime').val() <= $('#currentTime').text()) {
            $('#txtFromTimeError').text("Please choose valid time");
            $('#txtFromTime').addClass('errorStyle');
            $('#txtFromTime').focus();
            return false;
        }

        if(validTextField('#txtToTime','#txtToTimeError','Please enter to time') == true)
        {
            flag = true;
        }
        if(validTextField('#txtFromTime','#txtFromTimeError','Please enter from time') == true)
        {
            flag = true;
        }
        if(validTextField('#selOTRoom','#selOTRoomError','Please select ot room') == true)
        {
            flag = true;
        }
        if($("#appendNurse .userContainer").find('label').length < 1) {
            $("#txtNurseError").text('Please enter nurse name');
            $("#txtNurse").focus();
            $("#txtNurse").addClass('errorStyle');
            flag = true;
        }
        if($("#appendAnesthesian .userContainer").find('label').length < 1) {
            $("#txtAnesthesianError").text('Please enter anesthesian name');
            $("#txtAnesthesian").focus();
            $("#txtAnesthesian").addClass('errorStyle');
            flag = true;
        }
        if($("#appendSurgeon .userContainer").find('label').length < 1) {
            $("#txtSurgeonError").text('Please enter surgeon name');
            $("#txtSurgeon").focus();
            $("#txtSurgeon").addClass('errorStyle');
            flag = true;
        }   
        if(validTextField('#selSurgeryType','#selSurgeryTypeError','Please select surgery') == true)
        {
            flag = true;
        }
        if(validTextField('#selSurgery','#selSurgeryError','Please select surgery') == true)
        {
            flag = true;
        }
    
        if($('#txtOperationDate').val() == '') {
            $('#txtOperationDateError').text('Please enter operation date');
            $('#txtOperationDate').addClass('errorStyle');
        }
       if(validTextField('#txtPatientid','#txtPatientidError','Please enter patient id') == true)
        {
            flag = true;
        } 

        
        var regExp = /(\d{1,2})\:(\d{1,2})\:(\d{1,2})/;
        var checkFrom = $("#txtFromTime").val();
        var checkTo = $("#txtToTime").val();

        if((parseInt(checkFrom.replace(regExp, "$1$2$3")) >= parseInt( checkTo.replace(regExp, "$1$2$3")))){
            $("#txtToTimeError").text("Please Choose Valid Time");
            $("#txtToTime").addClass('errorStyle');
            flag = true;
        }

         /*Check radio button checked or not*/
        if ($('#chkActive').is(":checked")==false) {
            $('#chkActive').val(0);
        }
        else{
            $('#chkActive').val(1);
        }
        if (flag == true) {
            return false;
        }
        var operationDate = $("#txtOperationDate").val();
        var surgeryType = $('#selSurgeryType').val();
        var surgery = $("#selSurgery").val();
        var surgeon = $("#selSurgeon").val();
        var anesthesian = $("#selAnesthesian").val();
        var otRoom = $('#selOTRoom').val();
        var fromTime = $("#txtFromTime").val().trim();
        var toTime = $("#txtToTime").val().trim();

        /*Get the time difference between to and from time*/
        //create date format          
        var timeStart = new Date(operationDate+' '+fromTime).getTime();
        var timeEnd = new Date(operationDate+' '+toTime).getTime();

        var hourDiff = timeEnd - timeStart;
        hourDiff = hourDiff/3600000;

        var arrSurgeon = [];
        var totalSurgeon = $('#appendSurgeon').find('label').length;
        for(var j=0;j < totalSurgeon;j++){            
            var userId = $($('#appendSurgeon').find('label')[j]).attr('user-id');
            arrSurgeon.push(userId);
        }

        var arrAnesthesian = [];
        var totalAnesthesian = $('#appendAnesthesian').find('label').length;
        for(var j=0;j < totalAnesthesian;j++){            
            var userId = $($('#appendAnesthesian').find('label')[j]).attr('user-id');
            arrAnesthesian.push(userId);
        }

        var arrNurse = [];
        var totalNurse = $('#appendNurse').find('label').length;
        for(var j=0;j < totalNurse;j++){            
            var userId = $($('#appendNurse').find('label')[j]).attr('user-id');
            arrNurse.push(userId);
        }
        
        if (arrSurgeon == '') {
            $('#txtSurgeon').focus();
            $('#txtSurgeon').addClass('errorStyle');
            $('#txtSurgeonError').text('Please enter surgeon name');
            return false;
        }
        if (arrAnesthesian == '') {
            $('#txtAnesthesian').focus();
            $('#txtAnesthesian').addClass('errorStyle');
            $('#txtAnesthesianError').text('Please enter anesthesian name');
            return false;
        }
        if (arrNurse == '') {
            $('#txtNurse').focus();
            $('#txtNurse').addClass('errorStyle');
            $('#txtNurseError').text('Please enter nurse name');
            return false;
        }

        var postData = {
            "operation": "savePatientOtReg",
            "patientId": patientId,
            "operationDate": operationDate,
            "surgeryType": surgeryType,
            "surgery" : surgery,
            "surgeon": surgeon,
            "anesthesian": anesthesian,
            "otRoom": otRoom,
            "fromTime" : fromTime,
            "toTime" : toTime,
            hourDiff : hourDiff,
            arrSurgeon : JSON.stringify(arrSurgeon),
            arrAnesthesian : JSON.stringify(arrAnesthesian),
            arrNurse : JSON.stringify(arrNurse)
        }        
        dataCall("controllers/admin/patient_ot_reg.php", postData, function (result){
            if (result == 1) {
            callSuccessPopUp('Success','Registered Successfully!!!');
               clearFormDetails(".clearForm");
               $('#txtPatientid').focus();
               $('.remove').click();
               //this hidden id coming from OT rooms availablity
                if ($("#bookCalendar").val() == "1") {
                    bindOTRooms();
                    clearAppendDiv();
                }
                $("#tab_dept").click();
            }
            else if(result =="Patient not exist."){
                callSuccessPopUp('Alert','Patient not exist.');
                $("#txtPatientid").addClass('errorStyle');
                $("#txtPatientid").focus();
                $("#txtPatientidError").text("Please enter valid patient id");
            }
            else if(result =="This room for given time slot is already booked."){
                callSuccessPopUp('Alert','This OT room for given time slot already booked!!!');
            }
            else if(result =="This surgeon for given time slot is already booked."){
                callSuccessPopUp('Alert','The selected surgeon for given time slot already booked!!!');
            }
            else if(result =="Surgeon is on leave."){
                callSuccessPopUp('Alert','The selected surgeon is on leave for the selected operation date!!!');
            }
            else if(result =="Patient alredy booked for this given time"){
                callSuccessPopUp('Alert','The selected patient for given time slot already booked!!!');
            }
            else if(result =="This ansthesian for given time slot is already booked."){
                callSuccessPopUp('Alert','The selected anesthesian for given time slot already booked!!!');
            }
            else if(result =="Ansthesian is on leave."){
                callSuccessPopUp('Alert','The selected ansthesian is on leave for the selected operation date!!!');
            }
            else if(result =="This nurse for given time slot is already booked."){
                callSuccessPopUp('Alert','The selected nurse for given time slot already booked!!!');
            }
            else if(result =="Nurse is on leave."){
                callSuccessPopUp('Alert','The selected nurse is on leave for the selected operation date!!!');
            }
            else if(result =="0"){
                callSuccessPopUp('Error','Something aweful happend .Please try to contact admin.');
            }
        });
    });

    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code
        patientOTTable = $('#patientOTTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "bAutoWidth":false,
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = patientOTTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var patientId = mData["patient_id"];
                        var operationDate = mData["operation_date"];
                        var surgery = mData["surgery_id"];
                        var surgeryType = mData["surgery_type"];
                        var surgeon = mData["surgeon_name"];
                        var anesthesian = mData["anethaesthest_name"];
                        var nurse = mData['nurse_name'];
                        var surgeonId = mData['surgeon_id'];
                        var anesthesianId = mData['anethaesthiest_id'];
                        var nurseId = mData['nurse_id'];
                        var otRoom = mData["ot_room_id"];
                        var duration = mData["duration"];
                        var fromTime = mData['from_time'];
                        var toTime = mData['to_time'];
                        editClick(id,patientId,operationDate,surgery,surgeryType,surgeon,anesthesian,nurse,otRoom,duration,fromTime,toTime,surgeonId,anesthesianId,nurseId);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = patientOTTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/patient_ot_reg.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": function(o) {
                        var patientId = o["patient_id"];
                        var patientIdLength = patientId.length;
                        for (var i=0;i<6-patientIdLength;i++) {
                            patientId = "0"+patientId;
                        }
                        patientId = patientPrefix+patientId;
                        return patientId;
                    }
                }, 
                {
                    "mData": "operation_date"
                },
                {
                    "mData": "surgery_name"
                },
                {
                    "mData": "surgery_type"
                },
                {
                    "mData": "surgeon_name"
                },
                {
                    "mData": "anethaesthest_name"
                },
                {
                    "mData": "nurse_name"
                },
                {
                    "mData": "room_no"
                },
                {
                    "mData": "from_time"
                }, 
                {
                    "mData": "to_time"
                },               
                {
                
                    "mData": function(o) {
                        var data = o;
                        return "<i class=' fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class=' fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [10]
            }]

        });
    }
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            patientOTTable.fnClearTable();
            patientOTTable.fnDestroy();
            patientOTTable = "";
            patientOTTable = $('#patientOTTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "bAutoWidth":false,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = patientOTTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var checkFromTime = mData['from_time'];
                            var checkToTime = mData['to_time'];
                            var checkOTDate = mData['operation_date'];
                            restoreClick(id,checkFromTime,checkToTime,checkOTDate);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/patient_ot_reg.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                {
                    "mData": function(o) {
                        var patientId = o["patient_id"];
                        var patientIdLength = patientId.length;
                        for (var i=0;i<6-patientIdLength;i++) {
                            patientId = "0"+patientId;
                        }
                        patientId = patientPrefix+patientId;
                        return patientId;
                    }
                },  
                {
                    "mData": "operation_date"
                },
                {
                    "mData": "surgery_name"
                },
                {
                    "mData": "surgery_type"
                },
                {
                    "mData": "surgeon_name"
                },
                {
                    "mData": "anethaesthest_name"
                },
                {
                    "mData": "nurse_name"
                },                
                {
                    "mData": "room_no"
                },
                {
                    "mData": "from_time"
                },
                {
                    "mData": "to_time"
                },
                {
                    "mData": function(o) {
                        var data = o;
                        return '<i class=" fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                    }
                },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [10]
                }]
            });
        } else { // show active data on unchecked   
            patientOTTable.fnClearTable();
            patientOTTable.fnDestroy();
            patientOTTable = "";
            patientOTTable = $('#patientOTTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "bAutoWidth":false,
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = patientOTTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                        var patientId = mData["patient_id"];
                        var operationDate = mData["operation_date"];
                        var surgery = mData["surgery_id"];
                        var surgeryType = mData["surgery_type"];
                        var surgeon = mData["surgeon_name"];
                        var anesthesian = mData["anethaesthest_name"];
                        var nurse = mData['nurse_name'];
                        var surgeonId = mData['surgeon_id'];
                        var anesthesianId = mData['anethaesthiest_id'];
                        var nurseId = mData['nurse_id'];
                        var otRoom = mData["ot_room_id"];
                        var duration = mData["duration"];
                        var fromTime = mData['from_time'];
                        var toTime = mData['to_time'];
                        editClick(id,patientId,operationDate,surgery,surgeryType,surgeon,anesthesian,nurse,otRoom,duration,fromTime,toTime,surgeonId,anesthesianId,nurseId);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = patientOTTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/patient_ot_reg.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                {
                    "mData": function(o) {
                        var patientId = o["patient_id"];
                        var patientIdLength = patientId.length;
                        for (var i=0;i<6-patientIdLength;i++) {
                            patientId = "0"+patientId;
                        }
                        patientId = patientPrefix+patientId;
                        return patientId;
                    }
                }, 
                {
                    "mData": "operation_date"
                },
                {
                    "mData": "surgery_name"
                },
                {
                    "mData": "surgery_type"
                },
                {
                    "mData": "surgeon_name"
                },
                {
                    "mData": "anethaesthest_name"
                },
                {
                    "mData": "nurse_name"
                },
                {
                    "mData": "room_no"
                },
                {
                    "mData": "from_time"
                }, 
                {
                    "mData": "to_time"
                },
                {
                        "mData": function(o) {
                            var data = o;
                            return "<i class=' fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px;cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class=' fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer; margin-top: -2%;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [10]
                }]
            });
        }
    });

    $("#btnReset").click(function() {
        $('#txtPatientid').focus();
       clearFormDetails(".clearForm");
       $('.remove').click();
       removeErrorMessage();
    });

});
 function editClick(id,patientId,operationDate,surgery,surgeryType,surgeon,anesthesian,nurse,otRoom,duration,fromTime,toTime,surgeonId,anesthesianId,nurseId){
    
    $('#tab_add_dept').click();
    $('#tab_add_dept').html('+ Update Patient OT Registration');
   
    $("#btnSavePatientOperation").hide();
    $("#btnReset").hide();
    
	$('#txtPatientid').focus();
    $("#btnUpdate").removeAttr("style");
    
    $("#txtPatientid").removeClass("errorStyle");
    $("#txtPatientidError").text("");
    $("#txtOperationDate").removeClass("errorStyle");
    $("#txtOperationDateError").text("");
    $("#selSurgery").removeClass("errorStyle");
    $("#selSurgeryError").text("");
    $("#selSurgeryType").removeClass("errorStyle");
    $("#selSurgeryTypeError").text("");

    $("#selSurgeon").removeClass("errorStyle");
    $("#selSurgeonError").text("");
    $("#selAnesthesian").removeClass("errorStyle");
    $("#selAnesthesianError").text("");
    $("#selOTRoom").removeClass("errorStyle");
    $("#selOTRoomError").text("");
    $("#txtToTime").removeClass("errorStyle");
    $("#txtToTimeError").text("");
    $("#txtFromTime").removeClass("errorStyle");
    $("#txtFromTimeError").text("");
    
    var patientIdLength = patientId.length;
    for (var i=0;i<6-patientIdLength;i++) {
        patientId = "0"+patientId;
    }
    patientId = patientPrefix+patientId;

    $(".input-datepicker").datepicker({autoclose: true});
    $(".input-datepicker").click(function(){
        $(".datepicker").css({"z-index":"99999"});
    });
    var startDate = new Date();
    $('#txtOperationDate').datepicker('setStartDate', startDate);

    $('#txtPatientid').val(patientId);
    $('#txtOperationDate').val(operationDate);
    $('#selSurgery').val(surgery);
    $('#selSurgeryType').val(surgeryType);
    $('#selOTRoom').val(otRoom);
    $('#txtToTime').val(toTime);
    $('#txtFromTime').val(fromTime);
    $('#selectedRow').val(id);
    $('#txtPatientid').attr('disabled','disabled');
    //validation
    //remove validation style
    // edit data function for update 
    removeErrorMessage();
    $("#txtOperationDate").datepicker({autoclose: true});

    $("#searchPatientIcon").click(function() {          // show popup for search patient
        
        $('#selfRequestModalLabel').text("Search Patient");
        $('#selfRequestModalBody').load('views/admin/view_patients.html');
        $('#mySelfRequestModal').modal();
    });
       
    $('#txtFromTime').prop('disabled',false);
    $('#txtToTime').prop('disabled',false);
    
    var splittedSurgeonId = surgeonId.split(',');
    var splittedSurgeonName = surgeon.split(',');
    for(var i=0; i<splittedSurgeonId.length ;i++){ 
        $('#appendSurgeon').append("<div class='userContainer'><label user-id='"+splittedSurgeonId[i]+"'>"+splittedSurgeonName[i]+"</label><div class='remove' onclick='removeUser($(this))'>X</div></div>");
    } 

    var splittedAnesthesianId = anesthesianId.split(',');
    var splittedAnesthesianName = anesthesian.split(',');
    for(var i=0; i<splittedAnesthesianId.length ;i++){ 
        $('#appendAnesthesian').append("<div class='userContainer'><label user-id='"+splittedAnesthesianId[i]+"'>"+splittedAnesthesianName[i]+"</label><div class='remove' onclick='removeUser($(this))'>X</div></div>");
    }      

    if (nurseId !=null) {
        var splittedNurseId = nurseId.split(',');
        var splittedNurseName = nurse.split(',');
        for(var i=0; i<splittedNurseId.length ;i++){ 
            $('#appendNurse').append("<div class='userContainer'><label user-id='"+splittedNurseId[i]+"'>"+splittedNurseName[i]+"</label><div class='remove' onclick='removeUser($(this))'>X</div></div>");
        }
    }
        

    $("#txtSurgeon,#txtAnesthesian,#txtNurse").on('keyup', function(e){    //Autocomplete functionality  for seller    
        autoComplete("#"+$(this).attr('id'));
    });
    
    //blur function for surgeon,anasthesian & nurse
    $("#txtSurgeon").blur(function(){
        $("#txtSurgeon").val('');
    });
    $("#txtAnesthesian").blur(function(){
        $("#txtAnesthesian").val('');
    });
    $("#txtNurse").blur(function(){
        $("#txtNurse").val('');
    });


    $("#searchSurgeonIcon").on('click',function(){
        var postData = {
            operation : "showSurgeon"
        }
        showStaff(postData,'chkSurgeon','chkSurgeon','Search Surgeon','appendSurgeon',"#appendSurgeon");
    });

    $("#searchAnesthesianIcon").on('click',function(){
        var postData = {
            operation : "showAnesthesian"
        }
        showStaff(postData,"chkAnesthesian","chkAnesthesian","Search Anesthesian","appendAnesthesian","#appendAnesthesian");
    });

    $("#searchNurseIcon").on('click',function(){
        var postData = {
            operation : "showNurse"
        }
        showStaff(postData,"chkNurse","chkNurse","Search Nurse","appendNurse","#appendNurse");
    });

    $("#btnUpdate").click(function() { // click update button
        var flag = validation();
        
        if(flag == true) {
            return false;
        }
        
        
        var patientId = $("#txtPatientid").val().trim();
        var operationDate = $("#txtOperationDate").val();
        var surgeryType = $('#selSurgeryType').val();
        var surgery = $("#selSurgery").val();
        var surgeon = $("#selSurgeon").val();
        var anesthesian = $("#selAnesthesian").val();
        var otRoom = $('#selOTRoom').val();
        var fromTime = $("#txtFromTime").val().trim();
        var toTime = $("#txtToTime").val().trim();
        
        
        var timeStart = new Date(operationDate+' '+fromTime).getTime();
        var timeEnd = new Date(operationDate+' '+toTime).getTime();

        var hourDiff = timeEnd - timeStart;
        hourDiff = hourDiff/3600000;

        patientId = patientId.replace ( /[^\d.]/g, '' ); 
        patientId = parseInt(patientId);

        var arrSurgeon = [];
        var totalSurgeon = $('#appendSurgeon').find('label').length;
        for(var j=0;j < totalSurgeon;j++){            
            var userId = $($('#appendSurgeon').find('label')[j]).attr('user-id');
            arrSurgeon.push(userId);
        }

        var arrAnesthesian = [];
        var totalAnesthesian = $('#appendAnesthesian').find('label').length;
        for(var j=0;j < totalAnesthesian;j++){            
            var userId = $($('#appendAnesthesian').find('label')[j]).attr('user-id');
            arrAnesthesian.push(userId);
        }

        var arrNurse = [];
        var totalNurse = $('#appendNurse').find('label').length;
        for(var j=0;j < totalNurse;j++){            
            var userId = $($('#appendNurse').find('label')[j]).attr('user-id');
            arrNurse.push(userId);
        }

        if (arrSurgeon == '') {
            $('#txtSurgeon').focus();
            $('#txtSurgeon').addClass('errorStyle');
            $('#txtSurgeonError').text('Please enter surgeon name');
            return false;
        }
        if (arrAnesthesian == '') {
            $('#txtAnesthesian').focus();
            $('#txtAnesthesian').addClass('errorStyle');
            $('#txtAnesthesianError').text('Please enter anesthesian name');
            return false;
        }
        if (arrNurse == '') {
            $('#txtNurse').focus();
            $('#txtNurse').addClass('errorStyle');
            $('#txtNurseError').text('Please enter nurse name');
            return false;
        }
        
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
        var postData = {
            "operation": "update",
            "patientId": patientId,
            "operationDate": operationDate,
            "surgery": surgery,
            "surgeryType": surgeryType,
            "surgeon": surgeon,
            "anesthesian": anesthesian,
            "otRoom": otRoom,
            "hourDiff":hourDiff,
            "toTime": toTime,
            "fromTime": fromTime,
            "id": id,
            arrSurgeon : JSON.stringify(arrSurgeon),
            arrAnesthesian : JSON.stringify(arrAnesthesian),
            arrNurse : JSON.stringify(arrNurse)
        }
        $.ajax( //ajax call for update data
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/patient_ot_reg.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data == "1") {
                        $('.close-confirm').click();
                        $('#messageMyModalLabel').text("Success");
                        $('#messageBody').text("Patient OT updated successfully!!!");
                        $('#messagemyModal').modal();
                        $('#inactive-checkbox-tick').prop('checked', false).change();
                        clearFormDetails(".clearForm");
                        $('#tab_dept').click();

                        //this hidden id coming from OT rooms availablity
                        if ($("#bookCalendar").val() == "1") {
                            bindOTRooms();
                        }
                    }
                    else if(data =="Patient not exist."){
                        callSuccessPopUp('Alert','Patient not exist.');
                        $("#txtPatientid").addClass('errorStyle');
                        $("#txtPatientid").focus();
                        $("#txtPatientidError").text("Please enter valid patient id");
                    }
                    else if(data =="This room for given time slot is already booked."){
                        callSuccessPopUp('Alert','This OT room for given time slot already booked!!!');
                    }
                    else if(data =="This surgeon for given time slot is already booked."){
                        callSuccessPopUp('Alert','The selected surgeon for given time slot already booked!!!');
                    }
                    else if(data =="Surgeon is on leave."){
                        callSuccessPopUp('Alert','The selected surgeon is on leave for the selected operation date!!!');
                    }
                    else if(data =="Patient alredy booked for this given time"){
                        callSuccessPopUp('Alert','The selected patient for given time slot already booked!!!');
                    }
                    else if(data =="This ansthesian for given time slot is already booked."){
                        callSuccessPopUp('Alert','The selected anesthesian for given time slot already booked!!!');
                    }
                    else if(data =="Ansthesian is on leave."){
                        callSuccessPopUp('Alert','The selected ansthesian is on leave for the selected operation date!!!');
                    }
                    else if(data =="This nurse for given time slot is already booked."){
                        callSuccessPopUp('Alert','The selected nurse for given time slot already booked!!!');
                    }
                    else if(data =="Nurse is on leave."){
                        callSuccessPopUp('Alert','The selected nurse is on leave for the selected operation date!!!');
                    }
                    else if(data =="0"){
                        callSuccessPopUp('Error','Something aweful happend .Please try to contact admin.');                        
                    }
                },
                error: function() {
                    $('.close-confirm').click();
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                    $('#messagemyModal').modal();
                }
            }); // end of ajax
        });
    });
} // end update button
function deleteClick(id) { // delete click function
    
    $('#confirmMyModalLabel').text("Deleted Patient OT");
    $('#myConfirmModalBody').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteSurgery("'+type+'","","","");');
} // end click fucntion

function restoreClick(id,checkFromTime,checkToTime,checkOTDate) { // restore click function
    
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restored Patient OT");
    $('#myConfirmModalBody').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";

    $('#confirm').attr('onclick', 'deleteSurgery("'+type+'","'+checkFromTime+'","'+checkToTime+'","'+checkOTDate+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteSurgery(type,checkFromTime,checkToTime,checkOTDate) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/patient_ot_reg.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('#messageMyModalLabel').text("Success");
                    $('#messageBody').text("Patient OT deleted successfully!!!");
                    $('#messagemyModal').modal();
                    patientOTTable.fnReloadAjax();
                    //this hidden id coming from OT rooms availablity
                    if ($("#bookCalendar").val() == "1") {
                        bindOTRooms();
                    }
                } else {
                    $('#messageMyModalLabel').text("Sorry");
                    $('#messageBody').text("This Patient OT is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end ajax 
    } 
    else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/patient_ot_reg.php",
            data: {
                "operation": "restore",
                "id": id,
                "checkFromTime":checkFromTime,
                "checkToTime":checkToTime,
                "checkOTDate":checkOTDate
            },
            success: function(data) {
                if (data != "0" && data != "" && data == "1") {
                    $('#messageMyModalLabel').text("Success");
                    $('#messageBody').text("Patient OT restored successfully!!!");
                    $('#messagemyModal').modal();
                    patientOTTable.fnReloadAjax();
                    //this hidden id coming from OT rooms availablity
                    if ($("#bookCalendar").val() == "1") {
                        bindOTRooms();
                    }
                }
                else if (data == "Already Booked") {
                    callSuccessPopUp('Alert','This slot is already booked you can\'t restore it.!!!');
                }
            },
            error: function() {  
                $('#messageMyModalLabel').text("Error");
                $('#messageBody').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}


function bindISurgery() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/patient_ot_reg.php",
        data: {
            "operation": "showSurgery"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selSurgery').html(option);
            }
        },
        error: function() {}
    });
}
function bindISurgeon() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/patient_ot_reg.php",
        data: {
            "operation": "showSurgeon"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selSurgeon').html(option);
            }
        },
        error: function() {}
    });
}
function bindIAnesthesian() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/patient_ot_reg.php",
        data: {
            "operation": "showAnesthesian"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selAnesthesian').html(option);
            }
        },
        error: function() {}
    });
}
function bindOtRooms() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/patient_ot_reg.php",
        data: {
            "operation": "showOtRooms"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].room_no + "</option>";
                }
                $('#selOTRoom').html(option);
            }
        },
        error: function() {}
    });
}

$("#txtSurgeon,#txtAnesthesian,#txtNurse").on('keyup', function(e){    //Autocomplete functionality  for seller    
    autoComplete("#"+$(this).attr('id'));
});

function autoComplete(textBoxId){
    var currentObj = $(textBoxId).val();
    jQuery(textBoxId).autocomplete({
        source: function(request, response) {
            if (textBoxId == "#txtSurgeon" || textBoxId == "#myModal #txtSurgeon") {
                var postData = {
                    "operation":"searchName",
                    "surgeonName": currentObj,
                    "searchSurgeon" : "searchSurgeon"
                }
            }
            if (textBoxId == "#txtAnesthesian" || textBoxId == "#myModal #txtAnesthesian") {
                var postData = {
                    "operation":"searchName",
                    "anesthesianName": currentObj,
                    "searchAnesthesian" : "searchAnesthesian"
                }
            }
            if (textBoxId == "#txtNurse" || textBoxId == "#myModal #txtNurse") {
                var postData = {
                    "operation":"searchName",
                    "nurseName": currentObj,
                    "searchNurse" : "searchNurse"
                }
            }
                
            
            $.ajax(
                {                   
                type: "POST",
                cache: false,
                url: "controllers/admin/patient_ot_reg.php",
                datatype:"json",
                data: postData,
                
                success: function(dataSet) {
                    response($.map(JSON.parse(dataSet), function (users) {
                        return {
                            id: users.id,
                            value: users.name
                        }
                    }));
                },
                error: function(){
                    
                }
            });
        },
        select: function (e, i) {
            var userId = i.item.id;
            var userName = i.item.value;

            if (textBoxId == "#txtSurgeon") {
                for(var j=0; j<$('#appendSurgeon').find('label').length ;j++){
                    if ($($('#appendSurgeon').find('label')[j]).attr('user-id') == userId) {
                        $('#txtSurgeonError').text('This surgeon already exist');
                        $(textBoxId).addClass('errorStyle');
                        return false;
                    }
                }            
                $('#appendSurgeon').append("<div class='userContainer'><label user-id='"+userId+"'>"+userName+"</label><div class='remove' onclick='removeUser($(this))'>X</div></div>");
                $(textBoxId).val("");
            }

            else if(textBoxId == "#txtAnesthesian"){
                for(var j=0; j<$('#appendAnesthesian').find('label').length ;j++){
                    if ($($('#appendAnesthesian').find('label')[j]).attr('user-id') == userId) {
                        $('#txtAnesthesianError').text('This anesthesian already exist');
                        $(textBoxId).addClass('errorStyle');
                        return false;
                    }
                }            
                $('#appendAnesthesian').append("<div class='userContainer'><label user-id='"+userId+"'>"+userName+"</label><div class='remove' onclick='removeUser($(this))'>X</div></div>");
                $(textBoxId).val("");
            }

            else if(textBoxId == "#txtNurse"){
                for(var j=0; j<$('#appendNurse').find('label').length ;j++){
                    if ($($('#appendNurse').find('label')[j]).attr('user-id') == userId) {
                        $('#txtNurseError').text('This nurse already exist');
                        $(textBoxId).addClass('errorStyle');
                        return false;
                    }
                }            
                $('#appendNurse').append("<div class='userContainer'><label user-id='"+userId+"'>"+userName+"</label><div class='remove' onclick='removeUser($(this))'>X</div></div>");
                $(textBoxId).val("");
            }

            else if(textBoxId == "#myModal #txtSurgeon"){
                for(var j=0; j<$('#myModal #appendSurgeon').find('label').length ;j++){
                    if ($($('#myModal #appendSurgeon').find('label')[j]).attr('user-id') == userId) {
                        $('#myModal #txtSurgeonError').text('This surgeon already exist');
                        $(textBoxId).addClass('errorStyle');
                        return false;
                    }
                }            
                $('#myModal #appendSurgeon').append("<div class='userContainer'><label user-id='"+userId+"'>"+userName+"</label><div class='remove' onclick='removeUser($(this))'>X</div></div>");
                $(textBoxId).val("");
            }

            else if(textBoxId == "#myModal #txtAnesthesian"){
                for(var j=0; j<$('#myModal #appendAnesthesian').find('label').length ;j++){
                    if ($($('#myModal #appendAnesthesian').find('label')[j]).attr('user-id') == userId) {
                        $('#myModal #txtAnesthesianError').text('This anesthesian already exist');
                        $(textBoxId).addClass('errorStyle');
                        return false;
                    }
                }            
                $('#myModal #appendAnesthesian').append("<div class='userContainer'><label user-id='"+userId+"'>"+userName+"</label><div class='remove' onclick='removeUser($(this))'>X</div></div>");
                $(textBoxId).val("");
            }

            else if(textBoxId == "#myModal #txtNurse"){
                for(var j=0; j<$('#myModal #appendNurse').find('label').length ;j++){
                    if ($($('#myModal #appendNurse').find('label')[j]).attr('user-id') == userId) {
                        $('#myModal #txtNurseError').text('This nurse already exist');
                        $(textBoxId).addClass('errorStyle');
                        return false;
                    }
                }            
                $('#myModal #appendNurse').append("<div class='userContainer'><label user-id='"+userId+"'>"+userName+"</label><div class='remove' onclick='removeUser($(this))'>X</div></div>");
                $(textBoxId).val("");
            }
        },
        minLength: 3
    }); 
}

function removeUser(myThis){
    $(myThis).parent().remove();
}
 function openCalendar(){
    $("#txtOperationDate").datepicker({autoclose: true});
    $(".datepicker").css({"z-index":"99999"});
}
function checkDate(){
    $("#txtFromTime").prop('disabled',false);
    $('#txtFromTime,#txtToTime').val('');
    if($("#txtOperationDate").val() == todayDate()) {       
        var OTCompleteDate = $("#txtOperationDate").val();
        var splitedDate = OTCompleteDate.split('-');
        OTYear = splitedDate[0];
        OTMonth = splitedDate[1]-1;
        OTDate = splitedDate[2];
        if (OTCompleteDate !='') {
            var newOtMinutes = '';
            var newOtHours = '';
            if (new Date().getMinutes() < 30) {
                newOtMinutes = 30;
                newOtHours = new Date().getHours();
            }
            else{
                newOtMinutes = 00;
                newOtHours = new Date().getHours()+1;
            }
            $('#txtFromTime,#txtToTime').timepicker({
                timeFormat: 'H:i:s',
                minTime: new Date(OTYear, OTMonth, OTDate,newOtHours,newOtMinutes,00)
            });
        }
    }
    else {
        $('#txtFromTime,#txtToTime').timepicker({
            timeFormat: 'H:i:s',
            minTime: new Date(OTYear, OTMonth, OTDate,00,00,00)
        });
    }
}
function showStaff(postData,inputClass,inputId,modalLabel,appendDiVId,modalAppendDivId){
    dataCall("controllers/admin/patient_ot_reg.php", postData, function (result){
        debugger;
        var parseData = JSON.parse(result);
        var html = '';
        if (parseData != '') {
            var arrayStaffId = [];
            $.each(parseData,function(i,v){
                html+='<div><span class="srNo">'+(i+1)+':</span><input type="checkbox" class="'+inputClass+'" id="'+inputId+v.id+'" value="'+v.id+'" staff-name="'+v.name+'"><label class="staffName" for="'+inputId+v.id+'">'+v.name+'</label></div>';
                if (modalAppendDivId == '') {  
                    if($('#'+appendDiVId).text() != '') {
                        var surgeonArray = [];
                        var j = $('#'+appendDiVId).find('label').length;
                        for(var i = 0; i < j; i++) {
                            var surgeon = $($('#'+appendDiVId).find('label')[i]).attr('user-id');
                            
                            surgeonArray.push(surgeon);
                        }
                        var arrIntersect = jQuery.inArray( v.id, surgeonArray );
                        if(arrIntersect != -1){
                            arrayStaffId.push(inputId+v.id);
                        }
                    } 
                }
                else{
                    if($(modalAppendDivId).text() != ''){
                        var surgeonArray = [];
                        var j = $(modalAppendDivId).find('label').length;
                        for(var i = 0; i < j; i++) {
                            var surgeon = $($(modalAppendDivId).find('label')[i]).attr('user-id');
                            
                            surgeonArray.push(surgeon);
                        }
                        var arrIntersect = jQuery.inArray( v.id, surgeonArray );
                        if(arrIntersect != -1){
                            arrayStaffId.push(inputId+v.id);
                        }
                    }
                }
            });

            //for insert

                
            html+='<div class="text-center"><input type="button" value="Select" onClick = "selectedStaff( \'' + inputClass + '\',\''+appendDiVId+'\',\''+modalAppendDivId+'\')" class="btn_save"></div>';
            $("#serachStaffBody").html(html);

            $('#serachStaffLabel').text(modalLabel);

            $('#serachStaff').modal();

            for(var i = 0; i <= arrayStaffId.length;i++){
                $('#'+arrayStaffId[i]).prop('checked',true);
            }
        }

        else{
            callSuccessPopUp("Alert","No data exist for "+modalLabel.split(' ')[1]+"");
        }
    });  
}


function selectedStaff(inputClass,appendDiVId,modalAppendDivId){

    if (modalAppendDivId !='' && modalAppendDivId != null) {
        $(modalAppendDivId).text('');
        $.each($('.'+inputClass),function(i,v){
            if ($(v).prop("checked") == true) {

                if($(modalAppendDivId).find('label').length > 0){
                    var counter = 0;
                    for(var j=0; j<$(modalAppendDivId).find('label').length ;j++){
                        if ($($(modalAppendDivId).find('label')[j]).attr('user-id') == v.value) {
                            counter++;
                        }
                    }
                    //if already exist then doesn't add
                    if (counter == 0) {
                        $(modalAppendDivId).append("<div class='userContainer'><label user-id='"+v.value+"'>"+$(v).attr('staff-name')+"</label><div class='remove' onclick='removeUser($(this))'>X</div></div>");
                    }
                }
                else{
                    $(modalAppendDivId).append("<div class='userContainer'><label user-id='"+v.value+"'>"+$(v).attr('staff-name')+"</label><div class='remove' onclick='removeUser($(this))'>X</div></div>");
                }                
            }
        });
    }

    else{
        $.each($('.'+inputClass),function(i,v){
            if ($(v).prop("checked") == true) {

                if($('#'+appendDiVId).find('label').length > 0){
                    var counter = 0;
                    for(var j=0; j<$('#'+appendDiVId).find('label').length ;j++){
                        if ($($('#'+appendDiVId).find('label')[j]).attr('user-id') == v.value) {
                            counter++;
                        }
                    }
                    //if already exist then doesn't add
                    if (counter == 0) {
                        $('#'+appendDiVId).append("<div class='userContainer'><label user-id='"+v.value+"'>"+$(v).attr('staff-name')+"</label><div class='remove' onclick='removeUser($(this))'>X</div></div>");
                    }
                }
                else{
                    $('#'+appendDiVId).append("<div class='userContainer'><label user-id='"+v.value+"'>"+$(v).attr('staff-name')+"</label><div class='remove' onclick='removeUser($(this))'>X</div></div>");
                }                
            }
        });
    }
    dismissSearchStaff();
}
function clearAppendDiv(){
    $("#appendAnesthesian ,#appendNurse ,#appendSurgeon").html('');
}
function showTableList(){
    $("#form_dept").show();
    $(".blackborder").hide();
    $("#tab_add_dept").addClass('tab-detail-add');
    $("#tab_add_dept").removeClass('tab-detail-remove');
    $("#tab_dept").removeClass('tab-list-add');
    $("#tab_dept").addClass('tab-list-remove');
    $("#designation_list").addClass('list');
    $('#txtPatientid').focus();
    clearFormDetails(".clearForm");
    $('#txtStartDate').focus();
    $("#txtFromTime").prop('disabled',true);
    $("#txtToTime").prop('disabled',true);
    $('#serachStaff input[type="checkbox"]').prop('checked',false);
}

function showAddTab(){
	$("#btnSavePatientOperation").show();
	$('#tab_add_dept').html('+ Add Patient OT Registration');
    $("#btnReset").show();
    $("#btnUpdate").hide();
    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#form_dept").hide();
    $(".blackborder").show();
    $("#tab_add_dept").removeClass('tab-detail-add');
    $("#tab_add_dept").addClass('tab-detail-remove');
    $("#tab_dept").addClass('tab-list-add');
    $("#tab_dept").removeClass('tab-list-remove');
    $("#designation_list").addClass('list');
    clearAppendDiv();//clear html of appended staff
}  

function validation(){
	var flag = false;
    if ($("#txtPatientid").val().trim()== '') {
        $("#txtPatientidError").text("Please enter value");
        $("#txtPatientid").focus();
        $("#txtPatientid").addClass("errorStyle");
        flag = true;
    }
    if ($("#txtOperationDate").val()== '') {
        $("#txtOperationDateError").text("Please enter value");
        $("#txtOperationDate").focus();
        $("#txtOperationDate").addClass("errorStyle");
        flag = true;
    }
    if ($("#selSurgery").val()== '') {
        $("#selSurgeryError").text("Please enter value");
        $("#selSurgery").focus();
        $("#selSurgery").addClass("errorStyle");
        flag = true;
    }
    if ($("#selSurgeryType").val()== '') {
        $("#selSurgeryTypeError").text("Please enter value");
        $("#selSurgeryType").focus();
        $("#selSurgeryType").addClass("errorStyle");
        flag = true;
    }
    if($("#txtOperationDate").val() == todayDate() && $('#txtFromTime').val() <= $('#currentTime').text()) {
        $('#txtToTimeError').text("Please choose valid time");
        $('#txtFromTime').addClass('errorStyle');
        flag = true;
    }
    if($('#txtFromTime').val() >= $('#myModal #txtToTime').val()) {
        $('#txtToTimeError').text("Please choose valid time");
        $('#txtToTime').addClass('errorStyle');
        flag = true;
    }
    if ($("#selOTRoom").val()== '') {
        $("#selOTRoomError").text("Please enter value");
        $("#selOTRoom").focus();
        $("#selOTRoom").addClass("errorStyle");
        flag = true;
    }

    if ($("#txtToTime").val().trim()== '') {
        $("#txtToTimeError").text("Please enter value");
        $("#txtToTime").focus();
        $("#txtToTime").addClass("errorStyle");
        flag = true;
    }
    if ($("#txtFromTime").val().trim()== '') {
        $("#txtFromTimeError").text("Please enter value");
        $("#txtFromTime").focus();
        $("#txtFromTime").addClass("errorStyle");
        flag = true;
    }
    return flag;
}

