 var imprestRequestTable; 
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   imprest_request.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   21st may 2016
 * Description  :   This page add and mange imprest request
 *************************************************************************************************** */
loader();
debugger;
	bindStaffType();
	removeErrorMessage();

	$('#selStaffType').change(function() {				
        var htmlRoom = "<option value='-1'>-- Select --</option>";
		$("#selStaffTypeError").text("");
		$("#selStaffType").removeClass("errorStyle");

        if ($('#selStaffType :selected').val() != -1) {
            var value = $('#selStaffType :selected').val();			
			//Calling function for binding the staff name
            bindStaffName(value,null);    
			
        } else {
            $('#selStaffName').html(htmlRoom);
        }
    });
	
	$("#advanced-wizard").hide();//by default hise first part
	$("#bedList").css({
        "background-color": "#fff"
    });
	$("#tabBedList").css({
        "background-color": "#0c71c8",
        "color": "#fff"
    });

    /*Click for go to add the room type screen part*/
    $("#tabAddBed").click(function() {
        $("#advanced-wizard").show();
        $(".blackborder").hide();
		clear();
        $("#tabAddBed").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tabBedList").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#bedList").css({
            "background": "#fff"
        });
		bindStaffType();		
		$('#selWard').focus();		
    });
    /*Click function for show the bed lists*/
    $("#tabBedList").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
		tabBedList();
    });

	var today = new Date(); 

	$("#txtAssignmentStarts").datepicker({ changeYear: true,changeMonth: true,autoclose: true,startDate: new Date(today.getFullYear(), today.getMonth(), today.getDate())}) .on('changeDate', function(selected){
		var startDate = new Date(selected.date.valueOf());
		startDate.setDate(startDate.getDate() + parseInt($("#txtEstimationDay").val()));
		var dd = startDate.getDate();
	    var mm = startDate.getMonth() + 1;
	    var y = startDate.getFullYear();
	     var someFormattedDate = y + '-' + mm + '-' + dd;
		$('#txtAssignmentEnds').datepicker('setStartDate', someFormattedDate);
	}); 
	$('#txtAssignmentEnds').datepicker({
		changeYear: true,
		changeMonth: true,
		autoclose: true
	});
	
	/*By default when radio button is not checked show all active data*/
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		loadDataTable();
    }

    /*On change of radio button check it is checked or not*/
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	imprestRequestTable.fnClearTable();
	    	imprestRequestTable.fnDestroy();
	    	//Datatable code
			imprestRequestTable = $('#tblImprestRequest').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  imprestRequestTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);//call restore click function with certain parameter to restore
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/imprest_request.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					{ "mData": "name" },
					{ "mData": "nature_of_duty" },
					{ "mData": "amount" },
					{ "mData": "est_day" },
					{ "mData": "assignment_start" },
					{ "mData": "assignment_end" },
					{  
						"mData": function (o) { 
						
							return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; 
						}
					},
				],
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 6 ] },
				   { 'bSortable': false, 'aTargets': [ 5 ] }
			    ]
			} );
		}
		else{
			imprestRequestTable.fnClearTable();
	    	imprestRequestTable.fnDestroy();
	    	//Datatable code
			loadDataTable();
		}
    });
	
	//		
    /*validation ,save and ajax call on submit button*/
	//
    $("#btnBedSubmit").click(function() {
        var flag = "false";
		
		$("#txtAmount").val($("#txtAmount").val().trim());
		$("#txtEstimationDay").val($("#txtEstimationDay").val().trim());
		
		if ($("#txtAssignmentEnds").val() == "") {
            $("#txtAssignmentEndsError").text("Please select assignment end date");
			$("#txtAssignmentEnds").addClass("errorStyle");       
            flag = "true";
        }

        if ($("#txtAssignmentStarts").val() == "") {
            $("#txtAssignmentStartsError").text("Please select assignment start date");
			$("#txtAssignmentStarts").addClass("errorStyle");       
            flag = "true";
        }

        if ($("#txtEstimationDay").val() == "") {
            $("#txtEstimationDayError").text("Please enter estimation day");
			$("#txtEstimationDay").addClass("errorStyle");       
            flag = "true";
        }
		
		if ($("#txtAmount").val() == "") {
			$('#txtAmount').focus();
            $("#txtAmountError").text("Please enter amount");
			$("#txtAmount").addClass("errorStyle");     
            flag = "true";
        }
		
        if ($("#txtNatureOfDuty").val() == "") {
			$('#txtNatureOfDuty').focus();
            $("#txtNatureOfDutyError").text("Please enter nature of duty");
			$("#txtNatureOfDuty").addClass("errorStyle");    
            flag = "true";
        }

        if ($("#selStaffName").val() == "-1") {
			$('#selStaffName').focus();
            $("#selStaffNameError").text("Please select staff name");
			$("#selStaffName").addClass("errorStyle");      
            flag = "true";
        }		

        if ($("#selStaffType").val() == "-1") {
			$('#selStaffType').focus();
            $("#selStaffTypeError").text("Please select staff type");
			$("#selStaffType").addClass("errorStyle");      
            flag = "true";
        }
		
        if (flag == "true") {
            return false;
        }
		
		var staffNameId = $("#selStaffName").val();
		var outstanding = $("#txtImprestOutstanding").val();
		var amount = $("#txtAmount").val();
		var estimationDay = $("#txtEstimationDay").val();
		var startDate = $("#txtAssignmentStarts").val();
		var endDate = $("#txtAssignmentEnds").val();
		var description = $("#txtNatureOfDuty").val();
		description = description.replace(/'/g, "&#39");
		var postData = {
			"operation":"save",
			"staffNameId":staffNameId,
			"outstanding":outstanding,
			"estimationDay":estimationDay,
			"amount":amount,
			"startDate":startDate,
			"endDate":endDate,
			"description":description
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/imprest_request.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					callSuccessPopUp('Success','Imprest request saved successfully!!!');					
					$('#inactive-checkbox-tick').prop('checked', false).change();
					tabBedList();
				}
				else{
					$("#txtBedNumberError").text("Bed number already exists");
					$('#txtBedNumber').focus();
					$("#txtBedNumber").addClass("errorStyle");
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	});		

 	$("#btnReset").click(function(){
 		clear();
 	});
	
});

//		
//function for edit the bed 
//
function editClick(id,isWarrant,staffId,staff_type_id,imprest_outstanding,amount,est_day,assignment_start,assignment_end,description){	
    
	$('#myModalLabel').text("Update Imprest Request");
	$('.modal-body').html($("#bedModel").html()).show();
	$('#myModal #selStaffType').val(staff_type_id);
	$('#myModal #selStaffType').attr("disabled","disabled");
    bindStaffName(staff_type_id,staffId);	
	$("#myModal #txtImprestOutstanding").val(imprest_outstanding);
	$("#myModal #txtImprestOutstanding").attr("disabled","disabled");
	$("#myModal #txtAmount").val(amount);
	$("#myModal #txtAmount").attr("disabled","disabled");
	$("#myModal #txtEstimationDay").val(est_day);
	$("#myModal #txtEstimationDay").attr("disabled","disabled");
	$("#myModal #txtAssignmentStarts").val(assignment_start);
	$("#myModal #txtAssignmentStarts").attr("disabled","disabled");
	$("#myModal #txtAssignmentEnds").val(assignment_end);
	$("#myModal #txtAssignmentEnds").attr("disabled","disabled");
	$('#myModal #txtNatureOfDuty').val(description.replace(/&#39/g, "'"));
	$('#myModal #txtNatureOfDuty').attr("disabled","disabled");
	if(isWarrant == "yes"){
		$("#myModal #btnCreateImprest").hide();
	}
	$('#selectedRow').val(id);
    $('#myModal').modal('show');
	 $('#myModal').on('shown.bs.modal', function() {
        $("#myModal #selStaffType").focus();
		$('#myModal #selStaffName').attr("disabled","disabled");
    });		
	
	/*on click of update button update data*/
	$("#myModal #btnCreateImprest").click(function(){
		var flag = "false";
		
		$("#myModal #txtAmount").val($("#myModal #txtAmount").val().trim());
		$("#myModal #txtEstimationDay").val($("#myModal #txtEstimationDay").val().trim());
		
		if ($("#myModal #txtAssignmentEnds").val() == "") {
            $("#myModal #txtAssignmentEndsError").text("Please select assignment end date");
			$("#myModal #txtAssignmentEnds").addClass("errorStyle");       
            flag = "true";
        }

        if ($("#myModal #txtAssignmentStarts").val() == "") {
            $("#myModal #txtAssignmentStartsError").text("Please select assignment start date");
			$("#myModal #txtAssignmentStarts").addClass("errorStyle");       
            flag = "true";
        }

        if ($("#myModal #txtEstimationDay").val() == "") {
            $("#myModal #txtEstimationDayError").text("Please enter estimation day");
			$("#myModal #txtEstimationDay").addClass("errorStyle");       
            flag = "true";
        }
		
		if ($("#myModal #txtAmount").val() == "") {
			$('#myModal #txtAmount').focus();
            $("#myModal #txtAmountError").text("Please enter amount");
			$("#myModal #txtAmount").addClass("errorStyle");     
            flag = "true";
        }
		
        if ($("#myModal #txtNatureOfDuty").val() == "") {
			$('#myModal #txtNatureOfDuty').focus();
            $("#myModal #txtNatureOfDutyError").text("Please enter nature of duty");
			$("#myModal #txtNatureOfDuty").addClass("errorStyle");    
            flag = "true";
        }

        if ($("#myModal #selStaffName").val() == "-1") {
			$('#myModal #selStaffName').focus();
            $("#myModal #selStaffNameError").text("Please select staff name");
			$("#myModal #selStaffName").addClass("errorStyle");      
            flag = "true";
        }		

        if ($("#myModal #selStaffType").val() == "-1") {
			$('#myModal #selStaffType').focus();
            $("#myModal #selStaffTypeError").text("Please select staff type");
			$("#myModal #selStaffType").addClass("errorStyle");      
            flag = "true";
        }
		
        if (flag == "true") {
            return false;
        }
		else{
			var staffNameId = $("#myModal #selStaffName").val();
			var outstanding = $("#myModal #txtImprestOutstanding").val();
			var amount = $("#myModal #txtAmount").val();
			var estimationDay = $("#myModal #txtEstimationDay").val();
			var startDate = $("#myModal #txtAssignmentStarts").val();
			var endDate = $("#myModal #txtAssignmentEnds").val();
			var description = $("#myModal #txtNatureOfDuty").val();
			description = description.replace(/'/g, "&#39");
			var id = $("#selectedRow").val();
			var postData = {
				"operation":"saveImprestWarrent",
				"staffNameId":staffNameId,
				"outstanding":outstanding,
				"estimationDay":estimationDay,
				"amount":amount,
				"startDate":startDate,
				"endDate":endDate,
				"id":id,
				"description":description
			}			
			
			$.ajax({
				type: "POST",
				cache: "false",
				url: "controllers/admin/imprest_request.php",
				data : postData,
				
				success: function(data) {	
					if(data != "0" && data != ""){
						$('#myModal').modal('hide');
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Imprest request updated successfully!!!");
						$('#inactive-checkbox-tick').prop('checked', false).change();
						imprestRequestTable.fnReloadAjax();
						$('#messagemyModal').modal();
						clear();
					}
				},
				error:function() {
					$('.close-confirm').click();
					$('.modal-body').text("");
					$('#messagemyModal').modal();
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				}
			});
		}
	});
	
	
	$("#myModal .customerBtn").click(function(){
        $("#myModal .customerBtn").removeClass('activeSelf');
        $(this).addClass('activeSelf');
        $("#myModal .tabScreen").addClass('hide');
        if ($(this).attr("attr") == "detailScreen") {
            $("#myModal #detailScreen").removeClass('hide');
        }
        else{
            $("#myModal #actionFeedBackScreen").removeClass('hide');            
        }
    });

    $($("#myModal .customerBtn")[0]).addClass("activeSelf");
}


//		
/*function for delete the bed*/
//
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete imprest request");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteBed("'+type+'");');	//pass attribute with sent opertion and function with it's type
}

function restoreClick(id){
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Restore imprest request");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();
		
	var type="restore";
	$('#confirm').attr('onclick','deleteBed("'+type+'");');
}

//
/*Function for clear the data*/
//
function deleteBed(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();			
			
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/imprest_request.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data == "1"){					
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Imprest request deleted successfully!!!");
					imprestRequestTable.fnReloadAjax();
					$('#messagemyModal').modal();
					 
				}
				if(data == ""){					
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("This Imprest Request is used , so you can not delete!!!");
					imprestRequestTable.fnReloadAjax();
					$('#messagemyModal').modal();
				}
			},
			error:function() {
				$('.modal-body').text("");
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/imprest_request.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data == "Restored Successfully!!!" && data != ""){
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Imprest request restored successfully!!!");
					imprestRequestTable.fnReloadAjax();
					$('#messagemyModal').modal();
				}		
				if(data == "It can not be restore!!!"){					
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text(data);
					imprestRequestTable.fnReloadAjax();
					$('#messagemyModal').modal();
				}				
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}
}

//
//Function for clear the data
//
function clear(){
	$('#selStaffType').val("-1");
	$('#selStaffName').val("-1");
	$('#selStaffTypeError').text("");
	$('#selStaffNameError').text("");
	$('#txtEstimationDay').val("");
	$('#txtNatureOfDuty').val("");
	$('#txtEstimationDayError').text("");
	$('#txtNatureOfDutyError').text("");
	$('#txtAssignmentStarts').val("");
	$('#txtAssignmentStartsError').text("");
	$('#txtAssignmentEnds').val("");
	$('#txtAssignmentEndsError').text("");
	$('#txtAmount').val("");
	$('#txtAmountError').text("");

	$("#selStaffType").removeClass("errorStyle");
	$("#selStaffName").removeClass("errorStyle");
	$("#txtEstimationDay").removeClass("errorStyle");
	$("#txtAssignmentStarts").removeClass("errorStyle");
	$("#txtAssignmentEnds").removeClass("errorStyle");
	$("#txtAmount").removeClass("errorStyle");
	$("#txtNatureOfDuty").removeClass("errorStyle");
	bindStaffType();
}


function tabBedList(){
	$("#advanced-wizard").hide();
    $(".blackborder").show();
	$('#btnAddDepartment').val("Add Imprest Request");
	$('#tabAddBed').html("+Add  Imprest Request");

    $("#tabAddBed").css({
        "background": "#fff",
        "color": "#000"
    });
    $("#tabBedList").css({
        "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
        "color": "#fff"
    });
    $("#bedList").css({"background": "#fff"});
	clear();
}


// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		$(".close").click();
    }
});

function loadDataTable(){
	imprestRequestTable = $('#tblImprestRequest').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {

			/*On click of update icon call this function*/
			$('.update').unbind();
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = imprestRequestTable.fnGetData(data);//get datatable data
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					var staffId = mData["staff_id"];
					var outstanding = mData["outstanding"];
					var imprest_outstanding ;
					if(outstanding != null){
						imprest_outstanding = outstanding;
					}
					else{
						imprest_outstanding = mData["amount"];
					}
					
					var amount = mData["amount"];
					var est_day = mData["est_day"];
					var assignment_start = mData["assignment_start"];
					var assignment_end = mData["assignment_end"];
					var description = mData["nature_of_duty"];
					var staff_type_id = mData["staff_type_id"];
					var isWarrant =  mData["is_warrant"];
					editClick(id,isWarrant,staffId,staff_type_id,imprest_outstanding,amount,est_day,assignment_start,assignment_end,description);//call edit click function with certain parameters to update		   	   
				}
			});
			/*On click of delete icon call this function*/
			$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  imprestRequestTable.fnGetData(data);
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);//call delete click function with certain parameter to delete
				}
			});
		},
		
		"sAjaxSource":"controllers/admin/imprest_request.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{ "mData": "name" },
			{ "mData": "nature_of_duty" },
			{ "mData": "amount" },
			{ "mData": "est_day" },
			{ "mData": "assignment_start" },
			{ "mData": "assignment_end" },
			{  
				"mData": function (o) { 
					/*Return update and delete data id*/
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
				   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
				   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
				   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
				   "  data-original-title='Delete'></i>"; 
				}
			},	
		],
		/*Disable sort for following columns*/
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 6 ] },
		   { 'bSortable': false, 'aTargets': [ 5 ] }
	   ]
	} );
}

//Function for binding the staff type
function bindStaffType(){
	var postData = {
		"operation": "showStaffType"
	}

	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/schedule_staff.php",
		datatype: "json",
		data: postData,
		success: function(data) {		
			if(data != ""){
				var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
				var option = "<option value='-1'>--Select--</option>";
				
				for (var i = 0; i < parseData.length; i++) {
					option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
				}
				
				$('#selStaffType').html(option);
			}
		},		
		error:function() {
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
	});
}

//Function for binding the staff name 
function bindStaffName(value,staffId){
	var postData = {
		"operation": "showStaffName",
		"staffTypeId": value
	}

	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/schedule_staff.php",
		datatype: "json",
		data: postData,
		success: function(data) {		
			if(data != ""){
				var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
				var option = "<option value='-1'>--Select--</option>";
				
				for (var i = 0; i < parseData.length; i++) {
					option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
				}
				
				$('#selStaffName').html(option);
				if(staffId != null){
					$('#myModal #selStaffName').html(option);
					$('#myModal #selStaffName').val(staffId);
				}
			}
		},		
		error:function() {
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
	});
}

//# sourceURL = bed.js