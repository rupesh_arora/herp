/*
 * File Name    :   designation.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre operation
 */
 
var designationTable; //defing a global variable for datatable
$(document).ready(function() {
	loader();
    $('#btnReset').click(function() {
        clear();
        $("#txtDesignation").removeClass("errorStyle");
        $('#txtDesignation').focus();
    });
    $("#form_dept").hide();
    $("#designation_list").addClass('list');    
    $("#tab_dept").addClass('tab-list-add');
   

    $("#tab_add_dept").click(function() { // click on add designation tab
        clear();
       showAddTab();
        
    });

    $("#tab_dept").click(function() { // click on list designation tab
		showTableList();
    });

		// import excel on submit click
	$('#importDataFile').click(function(){
		var flag = false;
		if($('#file').prop('files')[0] == undefined || $('#file').val() == "") {
			$('#txtFileError').show();
			$('#txtFileError').text("Please choose file.");
			flag  = true;
		}
		if(flag == true){
			return false;
		}
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to upload this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			var file_data = $('#file').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: 'controllers/admin/designation.php', // point to server-side PHP script 
				dataType: 'text', // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					if(data != "" && data != "invalid file" && data == "1"){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Your file has been successfully imported!!!");
						$('#messagemyModal').modal();
						$('#file').val("");
					}
                    else if (data =="You must have two column in your file i.e designation and description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="First column should be designation" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Second column should be description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Please insert data in first column and second column only i.e A & B") {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Data can't be null in designation column" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
					else if(data == "invalid file"){
						$('#txtFileError').show();
						$('#txtFileError').text("Please import only CSV/XLS file.");
						$('#file').val("");
					}
					else if(data == ""){
						$('#txtFileError').show();
						$('#txtFileError').text("Data already exist.");
						$('#file').val("");
					}
				},
				error: function() {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Data Not Found!!!");
					$('#messagemyModal').modal();

				}
			});
		});
	});
    /* click on button for save  inforamtion with validation */
    $("#btnAddDesignation").click(function() {
        
         var flag = validation();

        if (flag == 'true') {
            return false;
        }
        //ajax call for save operation
        var designationtName = $("#txtDesignation").val();
        var designationDescription = $("#txtDescription").val();
        designationDescription = designationDescription.replace(/'/g, "&#39");
        var postData = {
            "operation": "save",
            "designationtName": designationtName,
            "designationDescription": designationDescription
        }
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/designation.php",
            datatype: "json",
            data: postData,
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Designation saved successfully!!!");
                    $('#messagemyModal').modal();
					showTableList();
                } else {
                    $("#txt_val_desg_err").text("Designation name is already exists");
                    $("#txtDesignation").addClass("errorStyle");
                    $("#txtDesignation").focus();
                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    });
    //keyup functionality for remove validation style
    $("#txtDesignation").keyup(function() {
        if ($("#txtDesignation").val() != '') {
            $("#txt_val_desg_err").text("");
            $("#txtDesignation").removeClass("errorStyle");
        }
    });

    //load Datatable data on page load
    if ($('.inactive-checkbox').not(':checked')) {
        loadDataTable();
    }
    //checked or not checked functionality to show active or inactive data
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) {
            designationTable.fnClearTable();
            designationTable.fnDestroy();
            designationTable = "";
            designationTable = $('#designation_table').dataTable({
                "bFilter": true,
                "processing": true,
				
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
					$('.restore').unbind();
                    $('.restore').on('click', function() { // restor click event
                        var data = $(this).parents('tr')[0];
                        var mData = designationTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            restoreClick(id);
                        }

                    });
                },
                "sAjaxSource": "controllers/admin/designation.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [{
                    "mData": "designation"
                }, {
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                    }
                }, ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1,2]
                }]
            });
        } else {
            designationTable.fnClearTable();
            designationTable.fnDestroy();
            designationTable = "";
            loadDataTable();
        }
    });

});
// use fucntion for edit designation for update 
function editClick(id, name, description) {

    showAddTab();
    $('#tab_add_dept').html("+Update Designation");
	$("#uploadFile").hide();
    $("#btnReset").hide();
    $("#btnAddDesignation").hide();
    $('#btnUpdateDesignation').removeAttr("style");
    $("#txt_val_desg_err").text("");
    $("#txtDesignation").removeClass("errorStyle");
    $('#txtDesignation').val(name);
    $(' #txtDescription').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);

    
    $("#btnUpdateDesignation").click(function() {
        var flag = validation();

        if ($("#txt_val_desg_err").text() != "") {
            flag = "true";
        }


        if (flag == "true") {
            return false;
        }
		else{
			var designationtName = $("#txtDesignation").val();
			var designationDescription = $("#txtDescription").val();
			designationDescription = designationDescription.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/designation.php",
					data: {
						"operation": "update",
						"id": id,
						"designationtName": designationtName,
						"designationDescription": designationDescription
					},
					success: function(data) {
						if (data != null && data != "") {
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Designation updated successfully!!!");
							$('#messagemyModal').modal();
                            showTableList();
						}
						if (data == "") {
							$('.close-confirm').click();
							$("#txt_val_desg_err").text("Designation name is already exists");
							$("#txtDesignation").addClass("errorStyle");
							$("#txtDesignation").focus();

						}
					},
					error: function() {
						//$('#myModal').modal('hide');
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
				});
			});
		}
    });

}
// click function to delete designation
function deleteClick(id) {
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Delete Designation");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteDesignation("' + type + '");');
}

//Click function for restore the departement
function restoreClick(id) {
    $('#selectedRow').val(id);
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Restore Designation");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteDesignation("' + type + '");');
}

//function for delete and restore the designationtList
function deleteDesignation(type) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/designation.php",
            data: {
                "operation": "delete",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Designation deleted successfully!!!");
                    $('#messagemyModal').modal();
                    designationTable.fnReloadAjax();
                } else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This designation is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    } else {
        var id = $('#selectedRow').val();
        $('.modal-body').text("");
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/designation.php",
            data: {
                "operation": "restore",
                "id": id
            },
            success: function(data) {
                if (data == "1") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Designation restored successfully!!!");
                    $('#messagemyModal').modal();
                    designationTable.fnReloadAjax();
                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    }
}

// clear function
function clear() {
    $('#txtDesignation').val("");
    $('#txtDesignation').removeClass("errorStyle");
    $('#txt_val_desg_err').text("");
    $('#txtFileError').text('');
    $('#txtDescription').val("");
    $("#myModal #txtDesignation").removeClass("errorStyle");
};

// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});

function loadDataTable(){
	designationTable = $('#designation_table').dataTable({
		"bFilter": true,
		"processing": true,
		"sPaginationType": "full_numbers",
		"fnDrawCallback": function(oSettings) {
			$('.update').unbind();
			$('.update').on('click', function() { // update click event 
				var data = $(this).parents('tr')[0];
				var mData = designationTable.fnGetData(data);
				if (null != mData) // null if we clicked on title row
				{
					var id = mData["id"];
					var name = mData["designation"];
					var description = mData["description"];
					editClick(id, name, description);

				}
			});
			$('.delete').unbind();
			$('.delete').on('click', function() { // delete click event
				var data = $(this).parents('tr')[0];
				var mData = designationTable.fnGetData(data);

				if (null != mData) // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);
				}
			});
		},
		"sAjaxSource": "controllers/admin/designation.php",
		"fnServerParams": function(aoData) {
			aoData.push({
				"name": "operation",
				"value": "show"
			});
		},
		"aoColumns": [{
			"mData": "designation"
		}, {
			"mData": "description"
		}, {
			"mData": function(o) {
				var data = o;
				return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
					" style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
					" <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
					" style='font-size: 22px; color:#a94442; cursor:pointer;' " +
					"  data-original-title='Delete'></i>";
			}
		}, ],
		aoColumnDefs: [{
			'bSortable': false,
			'aTargets': [1,2]
		}]
	});
}

function validation(){
    var flag = 'false';
    $("#txtDesignation").val($("#txtDesignation").val().trim());
    if ($("#txtDesignation").val().length > 50) {
        $('#txtDesignation').focus();
        $("#txt_val_desg_err").text("Please enter name less than 50");

        flag = "true";
    }
    if ($("#txtDesignation").val() == '') {
        $('#txtDesignation').focus();
        $("#txt_val_desg_err").text("Please enter designation name");
        $("#txtDesignation").addClass("errorStyle");
        flag = "true";
    }
    return flag;
}

function showTableList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#form_dept").hide();
    $(".blackborder").show();

    $("#tab_add_dept").removeClass('tab-detail-add');
    $("#tab_add_dept").addClass('tab-detail-remove');
    $("#tab_dept").removeClass('tab-list-remove');    
    $("#tab_dept").addClass('tab-list-add');
    $("#department_list").addClass('list');


    $("#uploadFile").show();
    $("#btnReset").show();
    $("#btnAddDesignation").show();
    $('#btnUpdateDesignation').hide();
    $('#tab_add_dept').html("+Add Designation");
    clear();
}

function showAddTab(){
    $("#form_dept").show();
    $(".blackborder").hide();
   
    $("#tab_add_dept").addClass('tab-detail-add');
    $("#tab_add_dept").removeClass('tab-detail-remove');
    $("#tab_dept").removeClass('tab-list-add');
    $("#tab_dept").addClass('tab-list-remove');
    $("#designation_list").addClass('list');

    $("#txt_val_desg_err").text("");
    $("#txtDesignation").removeClass("errorStyle");
    $('#txtDesignation').focus();
}