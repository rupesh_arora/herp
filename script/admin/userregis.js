/*
 * File Name    :   userregis.js 
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load and save inforamtion
 */
$(document).ready(function() {
	loader();
    debugger;
    var idDataTable; //make idDataTable as global during update info use this id as refrence 
    var countryID = -1;
    var imagefile;
    var imagePath;
    //bind country
    country(countryID); // load country

    //bind sate on click of country
    $('#sel_val_Country').change(function() {
        var countryID = $("#sel_val_Country").val();
        bindState(countryID, '');
        bindCountryCode(countryID);
    });

    //bind city on click of state
    $('#sel_val_State').change(function() {
        var stateID = $("#sel_val_State").val();
        bindCity(stateID, '');
    });

    //function to bind designation, department and  staff type
    bindDesignation();
    bindDepartment();
    bindStaffType();
	removeErrorMessage();//remove error msg
	// focus on load
	$("#val_Salutation").focus();
    $('#val_Gender').val("M");

	// hide next details on load 
	$("#advanced-Second").hide(); 
	 
    $("#masked_phone_ext").keypress(function(e) {
  
		// for disable alphabates keys
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { 
            return false;
        }
    });
   
	$("#val_emg_contct").keypress(function(e) {

		// for disable alphabates keys
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { 
            return false;
        }
		
    });

    $('#val_Salutation').change(function(){
        if($('#val_Salutation').val() == "Mr.") {
            $('#val_Gender').val("M");
        }
        else if($('#val_Salutation').val() == "Mrs.") {
            $('#val_Gender').val("F");
        }
        else if($('#val_Salutation').val() == "Ms.") {
            $('#val_Gender').val("F");
        }
        else {
             $('#val_Gender').val("");
        }
    });

    // back button code
    $('#back').click(function() {
        $("#advanced-first").show();
        $("#advanced-Second").hide();
        $("#val_Salutation").focus();
    });
	
    // reset button code
    $('#reset').click(function() {
        clearPersonalDetails();
        $("#val_Salutation").focus();
    });
	
    $('#reset1').click(function() {
        clearStaffDesignation();
        $("#val_stafftype").focus();
    });

    // image upload js.
    $("#image-box").click(function() {
		
		// click use for upload image
        $("#upload").click();
		
		// change image on upload click
        $("#upload").on("change", function() {
			$("#myUpdateStaffModel #errorimage").text("");
			$("#errorimage").text("");
            var files = !!this.files ? this.files : [];
			
			// no file selected, or no FileReader support
            if (!files.length || !window.FileReader) return; 
			
			// only image file
            if (/^image/.test(files[0].type)) { 
			
				// instance of the FileReader
                var reader = new FileReader(); 
				
				// read the local file
                reader.readAsDataURL(files[0]); 
				
				// set image data as background of div
                reader.onloadend = function() { 
                    $("#user_reg_image").hide();
                    $("#imagetext").hide();
                    $("#image-box").css("background-image", "url(" + this.result + ")");
                }
            }
        });
    });

	$("#val_DOB").datepicker({ endDate: '+0d'}) .on('changeDate', function(selected){
		startDate = new Date(selected.date.valueOf());
		$('#val_join_date').datepicker('setStartDate', startDate);
	}); 
	
	$("#val_join_date").datepicker() .on('changeDate', function(selected){
		startDate = new Date(selected.date.valueOf());
		$('#val_DOB').datepicker('setEndDate', startDate);
	});   	
	
    /*Increase z-index of datepicker so it is visible on popup*/
    $("#val_DOB").click(function() {
        $(".datepicker").css({
            "z-index": "99999"
        });
    });
	
    /*$("#val_join_date").click(function() {
        $(".datepicker").css({
            "z-index": "99999"
        });
    });*/

    var startDate = new Date();

    $('#val_join_date').datepicker('setStartDate', startDate);
	
    //for next button check validation
    $("#next1").on('click', function() {
        var flag = false;
        if (validNumber($("#masked_phone").val(),"masked_phone","errormob","Please enter valid mobile  number")) {
            flag = true;
        }
        if ($("#masked_phone").val().trim() == "") {
            $("#errormob").show();
            $("#errormob").text("Please enter mobile number");
            $("#masked_phone").focus();
            $("#masked_phone").addClass("errorStyle");
            flag = true;
        }
        if ($('#masked_phone').val().trim().charAt(0) == "0") {
            $("#errormob").show();
            $("#errormob").text("Please enter valid mobile number");
            $("#masked_phone").focus();
            $("#masked_phone").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_Email").val($("#val_Email").val().trim()) == "" || !ValidateEmail($("#val_Email").val())) {
            $("#erroremail").show();
            $("#erroremail").text("Please enter valid email address");
            $("#val_Email").focus();
            $("#val_Email").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_zipcode").val().trim() == "") {
            $("#errorcode").show();
            $("#errorcode").text("Please enter zip code");
            $("#val_zipcode").focus();
            $("#val_zipcode").addClass("errorStyle");
            flag = true;
        }
        if ($("#sel_val_City").val() == "") {
            $("#errorcity").show();
            $("#errorcity").text("Please select city");
            $("#sel_val_City").focus();
            $("#sel_val_City").addClass("errorStyle");
            flag = true;
        }
        if ($("#sel_val_State").val() == "") {
            $("#errorstate").show();
            $("#errorstate").text("Please select state");
            $("#sel_val_State").focus();
            $("#sel_val_State").addClass("errorStyle");
            flag = true;
        }
        if ($("#sel_val_Country").val() == "-1") {
            $("#errorcountry").show();
            $("#errorcountry").text("Please select country");
            $("#sel_val_Country").focus();
            $("#sel_val_Country").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_Address").val().trim() == "") {
            $("#erroraddress").show();
            $("#erroraddress").text("Please enter your address");
            $("#val_Address").focus();
            $("#val_Address").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_join_date").val() == "") {
            $("#errorjoindate").show();
            $("#errorjoindate").text("Please select joining date");

            $("#val_join_date").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_Gender").val() == "") {
            $("#errorgender").show();
            $("#errorgender").text("Please select gender");
            $("#val_Gender").focus();
            $("#val_Gender").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_DOB").val() == "") {
            $("#errordob").show();
            $("#errordob").text("Please select date of birth");
            $("#val_DOB").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_FirstName").val().trim() == "") {
            $("#errorfname").show();
            $("#errorfname").text("Please enter first name");
            $("#val_FirstName").focus();
            $("#val_FirstName").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_LastName").val().trim() == "") {
            $("#errorlname").show();
            $("#errorlname").text("Please enter last name");
            $("#val_LastName").focus();
            $("#val_LastName").addClass("errorStyle");
            flag = true;
        }

        if ($("#val_Salutation").val() == "") {
            $("#errorsalutation").show();
            $("#errorsalutation").text("Please select salutation");
            $("#val_Salutation").focus();
            $("#val_Salutation").addClass("errorStyle");
            flag = true;
        }
        if (flag == true) {
            return false;
        } 
        else {
            var email = $("#val_Email").val().trim(); // ajax call for email validation

            //if get hidden field value equals to zero then perform validation
            if ($("#getValueToChangeJs").val() == 0) {
                $.ajax({
                    type: 'POST',
                    cache: false,
                    url: "controllers/admin/staffregistration.php",
                    data: {

                        "operation": "checkEmail",
                        Email: email
                    },
                    success: function(data) {
                        if (data == "1") {
                            $("#erroremail").show();
                            $("#erroremail").text("This email already exists");
                            $("#val_Email").addClass("errorStyle");
                            $("#val_Email").focus();
                        } else {
                            $("#advanced-Second").show();
                            $("#val_stafftype").focus();
                            $("#advanced-first").hide();
                            /*Scroll down code*/
                            $("html, body").animate({
                                scrollTop: 0
                            }, 500);
                        }
                    },
                    error: function() {}
                });
            }
        }
    });

    /*Disable space functionality*/
    $("#masked_phone").on("keydown", function(e) {
        return e.which !== 32;
    });

    //when going to open as pop up then check functionality
    $("#myUpdateStaffModel #next1").click(function() {
        var flag = false;
        if ($("#masked_phone").val().trim() == "" || !validnumber($("#masked_phone").val())) {
            $("#errormob").show();
            $("#errormob").text("Please enter valid mobile number");
            $("#masked_phone").focus();
            $("#masked_phone").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_Email").val($("#val_Email").val().trim()) == "" || !ValidateEmail($("#val_Email").val())) {
            $("#erroremail").show();
            $("#erroremail").text("Please enter valid email address");
            $("#val_Email").focus();
            $("#val_Email").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_zipcode").val().trim() == "") {
            $("#errorcode").show();
            $("#errorcode").text("Please enter zip code");
            $("#val_zipcode").focus();
            $("#val_zipcode").addClass("errorStyle");
            flag = true;
        }
        if ($("#sel_val_City").val() == "") {
            $("#errorcity").show();
            $("#errorcity").text("Please select city");
            $("#sel_val_City").focus();
            $("#sel_val_City").addClass("errorStyle");
            flag = true;
        }
        if ($("#sel_val_State").val() == "") {
            $("#errorstate").show();
            $("#errorstate").text("Please select state");
            $("#sel_val_State").focus();
            $("#sel_val_State").addClass("errorStyle");
            flag = true;
        }
        if ($("#sel_val_Country").val() == "") {
            $("#errorcountry").show();
            $("#errorcountry").text("Please select country");
            $("#sel_val_Country").focus();
            $("#sel_val_Country").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_Address").val().trim() == "") {
            $("#erroraddress").show();
            $("#erroraddress").text("Please enter your address");
            $("#val_Address").focus();
            $("#val_Address").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_join_date").val() == "") {
            $("#errorjoindate").show();
            $("#errorjoindate").text("Please select joining date");

            $("#val_join_date").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_Gender").val() == "") {
            $("#errorgender").show();
            $("#errorgender").text("Please select gender");
            $("#val_Gender").focus();
            $("#val_Gender").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_DOB").val() == "") {
            $("#errordob").show();
            $("#errordob").text("Please select date of birth");
            $("#val_DOB").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_FirstName").val().trim() == "") {
            $("#errorfname").show();
            $("#errorfname").text("Please enter first name");
            $("#val_FirstName").focus();
            $("#val_FirstName").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_LastName").val().trim() == "") {
            $("#errorlname").show();
            $("#errorlname").text("Please enter last name");
            $("#val_LastName").focus();
            $("#val_LastName").addClass("errorStyle");
            flag = true;
        }

        if ($("#val_Salutation").val() == "") {
            $("#errorsalutation").show();
            $("#errorsalutation").text("Please select salutation");
            $("#val_Salutation").focus();
            $("#val_Salutation").addClass("errorStyle");
            flag = true;
        }
        if (flag == true) {
            return false;
        } else {
            var email = $("#val_Email").val().trim(); // ajax call for email validation

            //if get hidden field value equals to zero then perform validation
            if ($("#getValueToChangeJs").val() == 0) {
                $.ajax({
                    type: 'POST',
                    cache: false,
                    url: "controllers/admin/staffregistration.php",
                    data: {

                        "operation": "checkEmail",
                        Email: email
                    },
                    success: function(data) {
                        if (data == "1") {
                            $("#erroremail").show();
                            $("#erroremail").text("This email already exists");
                            $("#val_Email").addClass("errorStyle");
                            $("#val_Email").focus();
                        } else {
                            $("#advanced-Second").show();
                            $("#val_stafftype").focus();
                            $("#advanced-first").hide();
                            /*Scroll down code*/
                            $("html, body").animate({
                                scrollTop: 0
                            }, 500);
                        }
                    },
                    error: function() {}
                });
            }
        }

        $("#advanced-Second").show();
        $("#val_stafftype").focus();
        $("#advanced-first").hide();
        /*Scroll down code*/
         $("html, body").animate({
            scrollTop: 0
        }, 500);
    });

    //hide reset button
    if ($("#getValueToChangeJs").val() == 1) {
        $("#reset").css({
            "display": "none"
        });
        $("#reset1").css({
            "display": "none"
        });
        $("#next2").css({
            "display": "none"
        });
        /*$("#back").css({"display":"none"});*/
        $("#updateStaffInfo").css({
            "display": "inline-block"
        });
    }


    // for submit button
    $("#next2").on('click', function() {
        var flag = false;
        if ($("#val_dept").val() == "") {
            $("#errordepartment").show();
            $("#errordepartment").text("Please enter department");
            $("#val_dept").focus();
            $("#val_dept").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_desig").val() == "") {
            $("#errordesignation").show();
            $("#errordesignation").text("Please select designation");
            $("#val_desig").focus();
            $("#val_desig").addClass("errorStyle");
            flag = true;
        }

        if ($("#val_stafftype").val() == "") {
            $("#errortype").show();
            $("#errortype").text("Please select staff type ");
            $("#val_stafftype").focus();
            $("#val_stafftype").addClass("errorStyle");
            flag = true;
        }


        if (flag == true) {

            return false;

        }
        // define variable to get value
        var salutation = $("#val_Salutation").val();
        var lname = $("#val_LastName").val().trim();
        lname =  lname.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });
        var fname = $("#val_FirstName").val().trim();
        fname =  fname.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });
        var dob = $("#val_DOB").val();
        var gender = $("#val_Gender").val();
        var martial = $("#sel_val_martial").val();
        var joiningdate = $("#val_join_date").val();
        var address = $("#val_Address").val().trim();
        var country = $("#sel_val_Country").val();
        var state = $("#sel_val_State").val();
        var city = $("#sel_val_City").val();
        var zipcode = $("#val_zipcode").val().trim();
        var email = $("#val_Email").val().trim();
        var mobile = $("#masked_phone").val().trim();
        var phone = $("#masked_phone_ext").val();
        var mobileCode = $("#mobileCode").val();
        var referred = $("#val_referred").val();
        referred =  referred.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });
        var stafftype = $("#val_stafftype").val();
        var designation = $("#val_desig").val();
        var department = $("#val_dept").val();
        var degree = $("#val_Degree").val();
        var emergencyperson = $("#val_username1").val();
        var emergencycontact = $("#val_emg_contct").val();
        var remark = $("#val_remarks").val();
        /* var file_data = $('#upload').prop('files')[0];   
        var form_data = new FormData();                  
        form_data.append('upload', file_data);   */
        var file_data = $('#upload').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        var imageName;
        $('#confirmStaffModalLabel').text("Save Staff Information");
        $('.modal-body').text("Are you sure that you want to register this user?");
        $('#confirmStaffModal').modal();
        $('#staffConfirm').unbind();
        $('#staffConfirm').on('click',function(){
            $.ajax({
                url: 'controllers/admin/staffregistration.php', // point to server-side PHP script 
                dataType: 'text', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(data) {
                    if (data == "invalid file") {
                        $("#errorimage").show();
                        $("#errorimage").text("Please upload image of less than 2mb size.");
                        $("#advanced-first").show();
                        $("#advanced-Second").hide();
                    } else {
                        imageName = data;
                        // for ajax call for insert
                        $.ajax({
                            type: 'POST',
                            cache: false,

                            url: "controllers/admin/staffregistration.php",
                            data: {
                                Salutation: salutation,
                                Lname: lname,
                                Fname: fname,
                                Dob: dob,
                                Gender: gender,
                                Martial: martial,
                                Joiningdate: joiningdate,
                                Address: address,
                                Country: country,
                                State: state,
                                City: city,
                                Zipcode: zipcode,
                                Email: email,
                                Mobile: mobile,
                                Phone: phone,
                                mobileCode:mobileCode,
                                Referred: referred,
                                Stafftype: stafftype,
                                Designation: designation,
                                Department: department,
                                Degree: degree,
                                Emergencyperson: emergencyperson,
                                Emergencycontact: emergencycontact,
                                Remark: remark,
                                ImageName: imageName,
                                operation: "save"
                            },
                            success: function(data) {
								
                                $('#regStaffModalLabel').text("Success");	
    							$("#success").text("Staff registered successfully !!!");
								
                                var parseData = jQuery.parseJSON(data);
    							var name = parseData[0].salutation + " " + parseData[0].first_name +" "+parseData[0].last_name;
    							
								var staffId = parseInt(parseData[0].id);
								var prefix = parseData[0].prefix;
								var staffIdLength = staffId.toString().length;
								for (var i=0;i<6-staffIdLength;i++) {
									staffId = "0"+staffId;
								}
								staffId = prefix+staffId;
										
								$("#lblUserId").text(staffId);
    							$("#lblUserName").text(name);
    							$("#lblEmail").text(parseData[0].email_id);
    							$("#lblPassword").text(parseData[0].pwd);								
    			
                                $('#myStaffRegModal').modal();
                                $("#advanced-first").show();
                                $("#advanced-Second").hide();
								
                                clearPersonalDetails();
                                clearStaffDesignation();
								
                                $("#val_Salutation").focus();
                                $('#val_Gender').val("M");
                                $('#val_Salutation').change(function(){
                                    if($('#val_Salutation').val() == "Mr.") {
                                        $('#val_Gender').val("M");
                                    }
                                    else if($('#val_Salutation').val() == "Mrs.") {
                                        $('#val_Gender').val("F");
                                    }
                                    else if($('#val_Salutation').val() == "Ms.") {
                                        $('#val_Gender').val("F");
                                    }
                                    else {
                                         $('#val_Gender').val("");
                                    }
                                });
                            },
                            error: function() {
                                $('.modal-body').text("");
                                $('#messageMyModalLabel').text("Error");
                                $('.modal-body').text("Data Not Found!!!");
                                $('#messagemyModal').modal();
                            }
                        });
                    }
                }
            });
        });
    });


    // for remove 	
    $("input[type=text]").keyup(function() {

        if ($(this).val() != "") {
            $(this).next("span").hide();
            $(this).removeClass("errorStyle");
        }
		
    });
    $("select").on('change', function() {

        if ($(this).val() != "") {
            $(this).next("span").hide();
            $(this).removeClass("errorStyle");    
        }
		
    });
    $("#val_join_date").change(function() {
        if ($("#val_join_date").val() != "") {
            $("#errorjoindate").hide();
            $("#val_join_date").removeClass("errorStyle");
            $("#val_join_date").datepicker("hide");
        }
    });
    $("#val_DOB").change(function() {
		
        if ($("#val_DOB").val() != "") {
            $("#errordob").hide();
            $("#val_DOB").removeClass("errorStyle");
            $("#val_DOB").datepicker("hide");
        }
		
    });
    $("#val_join_date").keyup(function() {
        $("#val_join_date").datepicker("show");
    });
	
    $("#val_DOB").keyup(function() {
        $("#val_DOB").datepicker("show");
    });


    //update staff details when it opens a pop up in view staff screen
    //this code used in view staff model page
    idDataTable = $("#dataTableHiddenId").val();
    if (idDataTable != "0" && idDataTable != "") {

       /*  // image upload js on update vew staff case only that is further use.
        $("#myUpdateStaffModel #image-box").click(function() {
            $("#myUpdateStaffModel #upload").click();
            $("#myUpdateStaffModel #upload").on("change", function() {
                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

                if (/^image/.test(files[0].type)) { // only image file
                    var reader = new FileReader(); // instance of the FileReader
                    reader.readAsDataURL(files[0]); // read the local file

                    reader.onloadend = function() { // set image data as background of div
                        $("#myUpdateStaffModel #user_reg_image").hide();
                        $("#myUpdateStaffModel #imagetext").hide();
                        $("#myUpdateStaffModel #image-box").css("background-image", "url(" + this.result + ")");

                    }
                }
            });
        }); */
        var postData = {
            "operation": "updateStaffDetail",
            "id": idDataTable
        }
		
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/staffregistration.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    var parseData = jQuery.parseJSON(data);

                    for (var i = 0; i < parseData.length; i++) {
                        $("#myUpdateStaffModel #user_reg_image").hide();
                        $("#myUpdateStaffModel #imagetext").hide();
                        if (parseData[i].images_path != null) {
                            imagePath = "./upload_images/staff_dp/" + parseData[i].images_path;
                            $("#myUpdateStaffModel #image-box").css({
                                "background-image": "url(" + imagePath + ")"
                            });
                        }
                        if (parseData[i].images_path == null || parseData[i].images_path == "") {

                            $("#myUpdateStaffModel #image-box").css({
                                "background-image": "url(./images/default.jpg)"
                            }); // for default image
                            $("#myUpdateStaffModel #imagetext").show();
                            $("#myUpdateStaffModel #imagetext").css({
                                "text-align": "center",
                                "position": "relative",
                                "top": "61%",
                                "display": "block"
                            });
                        }
                        $("#myUpdateStaffModel #val_salutation").val(parseData[i].salutation);
                        $("#myUpdateStaffModel #val_FirstName").val(parseData[i].first_name);
                        $("#myUpdateStaffModel #val_LastName").val(parseData[i].last_name);
                        $("#myUpdateStaffModel #val_DOB").val(parseData[i].dob);
                        $("#myUpdateStaffModel #val_Gender").val(parseData[i].gender);
                        $("#myUpdateStaffModel #sel_val_martial").val(parseData[i].martial_status);
                        $("#myUpdateStaffModel #val_join_date").val(parseData[i].joining_date);
                        $("#myUpdateStaffModel #val_Address").val(parseData[i].address);
                        country(parseData[i].country_id);
                        bindState(parseData[i].country_id, parseData[i].state_id);
                        bindCity(parseData[i].state_id, parseData[i].city_id);
                        $("#myUpdateStaffModel #val_zipcode").val(parseData[i].zip);
                        
                        $("#myUpdateStaffModel #val_Email").val(parseData[i].email_id);
                        $("#myUpdateStaffModel #mobileCode").val(parseData[i].country_code);
                        $("#myUpdateStaffModel #masked_phone").val(parseData[i].mobile);
                        
                        $("#myUpdateStaffModel #masked_phone_ext").val(parseData[i].phone);
                        $("#myUpdateStaffModel #val_referred").val(parseData[i].referred_by);
                        bindStaffType(parseData[i].staff_type_id);
                        bindDesignation(parseData[i].designation);
                        bindDepartment(parseData[i].department_id);
                        $("#myUpdateStaffModel #val_Degree").val(parseData[i].degree_held);
                        $("#myUpdateStaffModel #val_username1").val(parseData[i].emergency_name);
                        $("#myUpdateStaffModel #val_emg_contct").val(parseData[i].emergency_contact);
                        $("#myUpdateStaffModel #val_remarks").val(parseData[i].remarks);
                        /*$("#myUpdateStaffModel #").val(parseData[i].images_path);*/
                        imagefile = imagePath;
                    }
                }
            },
            error: function() {
                alert('error');
            }
        });
    }

    //update details of staff on click of button
    $("#updateStaffInfo").on('click', function() {
		var flag = false;
        if ($("#val_dept").val() == "") {
            $("#errordepartment").show();
            $("#errordepartment").text("Please enter department");
            $("#val_dept").focus();
            $("#val_dept").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_desig").val() == "") {
            $("#errordesignation").show();
            $("#errordesignation").text("Please select designation");
            $("#val_desig").focus();
            $("#val_desig").addClass("errorStyle");
            flag = true;
        }

        if ($("#val_stafftype").val() == "") {
            $("#errortype").show();
            $("#errortype").text("Please select staff type ");
            $("#val_stafftype").focus();
            $("#val_stafftype").addClass("errorStyle");
            flag = true;
        }


        if (flag == true) {

            return false;

        }
		
        var salutation = $("#myUpdateStaffModel #val_salutation").val();
        var first_name = $("#myUpdateStaffModel #val_FirstName").val();
        var last_name = $("#myUpdateStaffModel #val_LastName").val();
        var dob = $("#myUpdateStaffModel #val_DOB").val();
        var gender = $("#myUpdateStaffModel #val_Gender").val();
        var martial_status = $("#myUpdateStaffModel #sel_val_martial").val();
        var joining_date = $("#myUpdateStaffModel #val_join_date").val();
        var address = $("#myUpdateStaffModel #val_Address").val();
        var country = $("#myUpdateStaffModel #sel_val_Country").val();
        var state = $("#myUpdateStaffModel #sel_val_State").val();
        var city = $("#myUpdateStaffModel #sel_val_City").val();
        var zip = $("#myUpdateStaffModel #val_zipcode").val();
        var email = $("#myUpdateStaffModel #val_Email").val();
        var mobile = $("#myUpdateStaffModel #masked_phone").val();
        var mobileCode = $("#myUpdateStaffModel #mobileCode").val();
        var phone = $("#myUpdateStaffModel #masked_phone_ext").val();
        var referred_by = $("#myUpdateStaffModel #val_referred").val();
        var stafftype = $("#myUpdateStaffModel #val_stafftype").val();
        var designation = $("#myUpdateStaffModel #val_desig").val();
        var department = $("#myUpdateStaffModel #val_dept").val();
        var degree_held = $("#myUpdateStaffModel #val_Degree").val();
        var emergency_name = $("#myUpdateStaffModel #val_username1").val();
        var emergency_contact = $("#myUpdateStaffModel #val_emg_contct").val();
        var remarks = $("#myUpdateStaffModel #val_remarks").val();
        var file_data = $('#myUpdateStaffModel #upload').prop('files')[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        var imageName1;
		
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
            $.ajax({
                url: 'controllers/admin/staffregistration.php', // point to server-side PHP script 
                dataType: 'text', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(data) {
                    if (data == "invalid file") {
                        $("#myUpdateStaffModel #errorimage").show();
                        $("#myUpdateStaffModel #errorimage").text("Please upload image of less than 2mb size.");
                        $("#myUpdateStaffModel #advanced-first").show();
                        $("#myUpdateStaffModel #advanced-Second").hide();
                    } else {
                        imageName1 = data;
                        var postData = {
                            "operation": "editStaffDetail",
                            "id": idDataTable, //here use idDataTable to get which id is clicked
                            "salutation": salutation,
                            "first_name": first_name,
                            "last_name": last_name,
                            "dob": dob,
                            "gender": gender,
                            "martial_status": martial_status,
                            "joining_date": joining_date,
                            "address": address,
                            "country": country,
                            "state": state,
                            "city": city,
                            "zip": zip,
                            "email": email,
                            "mobile": mobile,
                            "mobileCode":mobileCode,
                            "phone": phone,
                            "referred_by": referred_by,
                            "stafftype": stafftype,
                            "designation": designation,
                            "department": department,
                            "degree_held": degree_held,
                            "emergency_name": emergency_name,
                            "emergency_contact": emergency_contact,
                            imageName: imageName1,
                            imagesPath: imagefile,
                            "remarks": remarks
                        }
                        $.ajax({
                            type: "POST",
                            cache: false,
                            url: "controllers/admin/staffregistration.php",
                            datatype: "json",
                            data: postData,
                            success: function(data) {
                                if (data != "0" && data != "") {
                                    $("#myUpdateStaffModel").modal('hide');
                                    $('#messageMyModalLabel').text("Success");
                                    $('.modal-body').text("Staff updated successfully!!!");
    								$('#btnSearch').click();
                                    $('#messagemyModal').modal();
                                }
                            },

                        });
                    }
                }
            });
        });
    });


    function ValidateEmail(email) {
        var expr = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;;
        return expr.test(email);
    };

    function validnumber(number) {
        intRegex = /^\d{9,15}$/;
        return intRegex.test(number);
    };
});

// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});

//clear function
function clearPersonalDetails() {
    
    $("#val_LastName").val("");
    $("#mobileCode").val("");
    $("#val_FirstName").val("");
    $("#val_DOB").val("");
    $("#val_Gender").val("");
    $("#sel_val_martial").val("");
    $("#val_join_date").val("");
    $("#val_Address").val("");
    $("#sel_val_Country").val("");
    $("#sel_val_State").val("");
    $("#sel_val_City").val("");
    $("#val_zipcode").val("");
    $("#val_Email").val("");
    $("#masked_phone").val("");
    $("#masked_phone_ext").val("");
    $("#val_referred").val("");
    $("#val_stafftype").val("");
    $("#val_desig").val("");
    $("#val_dept").val("");
    $("#val_Degree").val("");
    $("#val_username1").val("");
    $("#val_emg_contct").val("");
    $("#val_remarks").val("");
    $("#upload").val("");
    $("#errorsalutation").text("");
    $("#errorlname").text("");
    $("#errorfname").text("");
    $("#errordob").text("");
    $("#errorgender").text("");
    $("#errorjoindate").text("");
    $("#erroraddress").text("");
    $("#errorcountry").text("");
    $("#errorstate").text("");
    $("#errorcity").text("");
    $("#errorcode").text("");
    $("#erroremail").text("");
    $("#errormob").text("");
    $("#errortype").text("");
    $("#errordesignation").text("");
    $("#errordepartment").text("");


    $("#val_Salutation").removeClass("errorStyle");
    $("#val_LastName").removeClass("errorStyle");
    $("#val_FirstName").removeClass("errorStyle");
    $("#val_DOB").removeClass("errorStyle");
    $("#val_Gender").removeClass("errorStyle");
    $("#sel_val_martial").removeClass("errorStyle");
    $("#val_join_date").removeClass("errorStyle");
    $("#val_Address").removeClass("errorStyle");
    $("#sel_val_Country").removeClass("errorStyle");
    $("#sel_val_State").removeClass("errorStyle");
    $("#sel_val_City").removeClass("errorStyle");
    $("#val_zipcode").removeClass("errorStyle");
    $("#val_Email").removeClass("errorStyle");
    $("#masked_phone").removeClass("errorStyle");
    $("#masked_phone_ext").removeClass("errorStyle");
    $("#val_referred").removeClass("errorStyle");
    $("#val_stafftype").removeClass("errorStyle");
    $("#val_desig").removeClass("errorStyle");
    $("#val_dept").removeClass("errorStyle");
    $("#val_Degree").removeClass("errorStyle");
    $("#val_username1").removeClass("errorStyle");
    $("#val_emg_contct").removeClass("errorStyle");
    $("#val_remarks").removeClass("errorStyle");

    $("#user_reg_image").show();
    $("#imagetext").show();
    $("#image-box").css({
        "background-image": "#fff"
    });
    $("#errorimage").hide();
    $("#errorimage").text("");

}

function clearStaffDesignation() {
    $("#val_stafftype").val("");
    $("#val_desig").val("");
    $("#val_dept").val("");
    $("#val_Degree").val("");
    $("#val_username1").val("");
    $("#val_emg_contct").val("");
    $("#val_remarks").val("");

    $("#errortype").text("");
    $("#errordesignation").text("");
    $("#errordepartment").text("");
    $('#mobileCode').val('');

    $("#val_stafftype").removeClass("errorStyle");
    $("#val_desig").removeClass("errorStyle");
    $("#val_dept").removeClass("errorStyle");
    $("#val_Degree").removeClass("errorStyle");
    $("#val_username1").removeClass("errorStyle");
    $("#val_emg_contct").removeClass("errorStyle");
    $("#val_remarks").removeClass("errorStyle");

}
// function for designation
function bindDesignation(designation) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/staffregistration.php",
        data: {
            "operation": "showdesignation"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].designation + "</option>";
                }
                $('#val_desig').html(option);
            }
            if (designation != "") {
                $("#myUpdateStaffModel #val_desig").val(designation);
            }
        },
        error: function() {}
    });
}
// function for bind department
function bindDepartment(department_id) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/staffregistration.php",
        data: {
            "operation": "showdepartment"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].department + "</option>";
                }
                $('#val_dept').html(option);
            }
            if (department_id != "") {
                $("#myUpdateStaffModel #val_dept").val(department_id);
            }
        },
        error: function() {}
    });
}
// function for bind staff type
function bindStaffType(staff_type_id) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/staffregistration.php",
        data: {
            "operation": "showstafftype"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
                }
                $('#val_stafftype').html(option);
            }
            if (staff_type_id != "") {
                $("#myUpdateStaffModel #val_stafftype").val(staff_type_id);
            }
        },
        error: function() {}
    });
}
// function for bind country
function country(countryID) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/hospital_profile.php",
        data: {
            "operation": "showcountry"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $('#sel_val_Country').html(option);
            }
            if (countryID != "") {
                $("#myUpdateStaffModel #sel_val_Country").val(countryID);
                //bindCountryCode(countryID);
            }

        },
        error: function() {}
    });
}
// function for bind state
function bindState(countryID, stateID) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/hospital_profile.php",
        data: {
            "operation": "showstate",
            country_ID: countryID
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $('#sel_val_State').html(option);
                if (stateID != "") {
                    $("#myUpdateStaffModel #sel_val_State").val(stateID);
                }
            }
        },
        error: function() {}
    });
}


// function for bind city
function bindCity(stateID, cityID) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/hospital_profile.php",
        data: {
            "operation": "showcity",
            state_ID: stateID
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $('#sel_val_City').html(option);
                if (cityID != "") {
                    $("#myUpdateStaffModel #sel_val_City").val(cityID);
                }
            }

        },
        error: function() {}
    });

}
function bindCountryCode(countryID) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/hospital_profile.php",
        data: {
            "operation": "showCountryCode",
            countryID: countryID
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                
                var countryCode = '+' + parseData.country_code;
                $("#mobileCode").val(countryCode);
                
               
            }
        },
        error: function() {}
    });
}