$(document).ready(function() {
	debugger;	
	google.charts.setOnLoadCallback(drawChart);
});
function drawChart() {
	var chartWidth  = $("#page-content").width() -25;
	var patientRecord = [['Record-Group', 'Count']];
	var postData = {
		"operation" : "showChartDataForPatientMgt"
	}
	$.ajax({     
		type: "POST",
		cache: false,
		url: "controllers/admin/patient_mangament_dashboard.php",
		data: postData,
		success: function(data) { 
			if(data != null && data != ""){
				var parseData =  JSON.parse(data);		
				
				patientRecord = [['Record-Group', 'Count']];

				$.each(parseData,function(index,value){
					$.each(parseData[index],function(i,v){

						patientRecord.push([i,parseInt(v)]);
					});	
				});

	            /*google chart intialization for patientRecord group visit*/
	            var datapatientRecordGroup = google.visualization.arrayToDataTable(patientRecord);

			    var options = {
			      title: 'Registeration of patient per month','width':1080,'height':1000,is3D:true
			    };

        		var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        		chart.draw(datapatientRecordGroup, options);        		
			}
		}			
	});
}	