$(document).ready(function() {
loader();
	 var otable; 
	//
	//Calling the function for loading the ward 
	//
	bindOPDRooms();
	bindConsultants();
	$("#advanced-wizard").hide();
	$("#OPDConsultantsList").css({
            "background-color": "#fff"
        });
		$("#tabOPDConsultantsList").css({
            "background-color": "#0c71c8",
            "color": "#fff"
        });
    $("#tabAddOPDConsultants").click(function() {
        $("#advanced-wizard").show();
        $(".blackborder").hide();
		clear();
        $("#tabAddOPDConsultants").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tabOPDConsultantsList").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#OPDConsultantsList").css({
            "background": "#fff"
        });
		bindOPDRooms();
		bindConsultants();
		$('#selOPDRooms').focus();
    });
    $("#tabOPDConsultantsList").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
       tabOPDConsultantsList();
    });
	
	
	
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		otable = $('#OPDConsultantsTable').dataTable( {
			"bFilter": true,
			"processing": true,
			"sPaginationType":"full_numbers",
			"fnDrawCallback": function ( oSettings ) {
				$('.update').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData = $('#OPDConsultantsTable').dataTable().fnGetData(data);
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						var opd_room_id = mData["opd_room_id"];
						var consultant_id = mData["consultant_id"];
						editClick(id,opd_room_id,consultant_id);
	   
					}
				});
				$('.delete').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData =  $('#OPDConsultantsTable').dataTable().fnGetData(data);
					
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						deleteClick(id);
					}
				});
			},
			
			"sAjaxSource":"controllers/admin/opd_consultants.php",
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "operation", "value": "show" });
			},
			"aoColumns": [
				{ "mData": "room_number" },
				{ "mData": "name" },	
				{  
					"mData": function (o) { 
					
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
					   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
					   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
					   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
					   "  data-original-title='Delete'></i>"; 
					}
				},	
			],
			aoColumnDefs: [
			   { 'bSortable': false, 'aTargets': [ 2 ] }
		   ]
		} );
    }
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	otable.fnClearTable();
	    	otable.fnDestroy();
	    	//Datatable code
			otable = $('#OPDConsultantsTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  $('#OPDConsultantsTable').dataTable().fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var opd_room_id = mData["opd_room_id"];
							var consultant_id = mData["consultant_id"];
							restoreClick(id,opd_room_id,consultant_id);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/opd_consultants.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					{ "mData": "room_number" },
					{ "mData": "name" },
					{  
						"mData": function (o) { 
						
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},	
				],
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 2 ] }
			   ]
			} );
		}
		else{
			otable.fnClearTable();
	    	otable.fnDestroy();
	    	//Datatable code
			otable = $('#OPDConsultantsTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.update').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData = $('#OPDConsultantsTable').dataTable().fnGetData(data);
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var opd_room_id = mData["opd_room_id"];
							var consultant_id = mData["consultant_id"];
							editClick(id,opd_room_id,consultant_id);
		   
						}
					});
					$('.delete').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  $('#OPDConsultantsTable').dataTable().fnGetData(data);
						
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							deleteClick(id);
						}
					});
				},
				
				"sAjaxSource":"controllers/admin/opd_consultants.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "show" });
				},
				"aoColumns": [
					{ "mData": "room_number" },
					{ "mData": "name" },	
					{  
						"mData": function (o) { 
						
						return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
						   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
						   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
						   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
						   "  data-original-title='Delete'></i>"; 
						}
					},
				],
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 2 ] }
			   ]
			} );
		}
    });
	
	//		
    //validation and ajax call on submit button
	//
    $("#btnSubmit").click(function() {
        var flag = "false";		
        
		if ($("#selConsultant").val() == "-1") {
			$('#selConsultant').focus();
            $("#selConsultantError").text("Please select consultant");
			$("#selConsultant").addClass("errorStyle");       
            flag = "true";
        }		
        if ($("#selOPDRooms").val() == "-1") {
			$('#selOPDRooms').focus();
            $("#selOPDRoomsError").text("Please select opd room");
			$("#selOPDRooms").addClass("errorStyle");      
            flag = "true";
        }
		if($("#selConsultantError").text()!= ''){
			flag = "true";
		}
		
        if (flag == "true") {
            return false;
        }
		
		var opdRoom = $("#selOPDRooms").val();
		var consultant = $("#selConsultant").val();
		var postData = {
			"operation":"save",
			"opdRoom":opdRoom,
			"consultant":consultant
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/opd_consultants.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Save Successfully!!!");
					$('#messagemyModal').modal();
					$('#inactive-checkbox-tick').prop('checked', false).change();
					bindOPDRooms();
					bindConsultants();
					tabOPDConsultantsList();
				}
				else{
					$("#selConsultantError").text("Consultant already book");
					$('#selConsultant').focus();
					$("#selConsultant").addClass("errorStyle");  
				}
			},
			error:function() {
				alert('error');
			}
		});
	});	
	
	//
    //keyup functionality
	//
    $("#selOPDRooms").change(function() {
        if ($("#selOPDRooms").val() != "-1") {
            $("#selOPDRoomsError").text("");
            $("#selOPDRooms").removeClass("errorStyle");
        }
    });
	
	$("#selConsultant").change(function() {
        if ($("#selConsultant").val() != "-1") {
            $("#selConsultantError").text("");
            $("#selConsultant").removeClass("errorStyle");
        }
    });    
});

//		
//function for edit the bed 
//
function editClick(id,opd_room_id,consultant_id){	
    
	$('#myModalLabel').text("Update OPD Consultant");
	$('.modal-body').html($("#opdConsultantModel").html()).show();
	$('#myModal').modal();
	$("#myModal #btnUpdate").removeAttr("style");
	$('#myModal #selOPDRooms').val(opd_room_id);
	$("#myModal #selConsultant").val(consultant_id);
	$('#selectedRow').val(id);
	
	
	
	
	$("#myModal #btnUpdate").click(function(){
		var flag = "false";		
		
        
		if ($("#myModal #selConsultant").val() == "-1") {
			$('#myModal #selConsultant').focus();
            $("#myModal #selConsultantError").text("Please select cosultant");
			$("#myModal #selConsultant").addClass("errorStyle");
            flag = "true";
        }
		if ($("#myModal #selOPDRooms").val() == "-1") {
			$('#myModal #selOPDRooms').focus();
            $("#myModal #selOPDRoomsError").text("Please select opd room");
			$("#myModal #selOPDRooms").addClass("errorStyle");
            flag = "true";
        }
		
		if(flag == "true"){			
		return false;
		}
		
		var opdRoom = $("#myModal #selOPDRooms").val();
		var consultant = $("#myModal #selConsultant").val();
		var id = $('#selectedRow').val();
		
		
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/opd_consultants.php",
			data :{            
				"operation" : "update",
				"id" : id,
				"opdRoom":opdRoom,
				"consultant":consultant
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('#myModal').modal('toggle');
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Update Successfully!!!");
					$("#OPDConsultantsTable").dataTable().fnReloadAjax();
					$('#messagemyModal').modal();
					clear();
				}
				else{
					$("#myModal #selConsultantError").text("Consultant already book");
					$('#myModal #selConsultant').focus();
					$("#myModal #selConsultant").addClass("errorStyle"); 
				}
			},
			error:function() {
				alert('error');
			}
		});
	});
	
	$("#myModal #selOPDRooms").change(function() {
        if ($("#myModal #selOPDRooms").val() != "-1") {
            $("#myModal #selOPDRoomsError").text("");
            $("#myModal #selOPDRooms").removeClass("errorStyle");
        }
    });
	
	$("#myModal #selConsultant").change(function() {
        if ($("#myModal #selConsultant").val() != "-1") {
            $("#myModal #selConsultantError").text("");
            $("#myModal #selConsultant").removeClass("errorStyle");
        }
    });
	
}


//		
//function for delete the bed
//
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('#confirmMyModalLabel').text("Delete OPD Consultant");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteOPDConsultant("'+type+'");');	
}

function restoreClick(id,opd_room_id,consultant_id){
	$('#selectedRow').val(id);
	$('#selOPDRooms').val(opd_room_id);
	$('#selConsultant').val(consultant_id);
	$('#confirmMyModalLabel').text("Restore OPD Consultant");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();
	
	var type="restore";
	$('#confirm').attr('onclick','deleteOPDConsultant("'+type+'");');
}

function deleteOPDConsultant(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
			
			
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/opd_consultants.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data == "Delete Successfully!!!" && data != ""){					
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text(data);
					$('#messagemyModal').modal();
					$("#OPDConsultantsTable").dataTable().fnReloadAjax();
				}
			},
			error: function()
			{
				alert('error');
				  
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
		var opd_room_id = $('#selOPDRooms').val();
		var consultant_id = $('#selConsultant').val();
	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/opd_consultants.php",
			data :{            
				"operation" : "restore",
				"id" : id,
				"opdRoom" : opd_room_id,
				"consultant" : consultant_id
			},
			success: function(data) {	
				if(data == "Restored Successfully!!!" && data != ""){
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text(data);
					$('#messagemyModal').modal();
					$("#OPDConsultantsTable").dataTable().fnReloadAjax();
					
				}		
				if(data == "It can not be restore!!!"){					
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text(data);
					$('#messagemyModal').modal();
					$("#OPDConsultantsTable").dataTable().fnReloadAjax();
				}				
			},
			error:function()
			{
				alert('error');
				  
			}
		});
	}
}

//
//Function for clear the data
//
function clear(){
	$('#selOPDRooms').val("");
	$('#selOPDRoomsError').text("");
	$('#selConsultant').val("");
	$('#selConsultantError').text("");
}

//
//function binding ward
//
function bindOPDRooms(){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/opd_consultants.php",
		data: {
			"operation":"showOPDRooms"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>Select</option>";
				for (var i=0;i<parseData.length;i++)
				{
				option+="<option value='"+parseData[i].id+"'>"+parseData[i].number+"</option>";
				}
				$('#selOPDRooms').html(option);				
				
			}
		},
		error:function(){
			
		}
	});
}


//
//function for binding the department
//
function bindConsultants(){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/opd_consultants.php",
		data: {
			"operation":"showConsultants"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>Select</option>";
				for (var i=0;i<parseData.length;i++)
				{
				option+="<option value='"+parseData[i].id+"'>"+parseData[i].first_name+"</option>";
				}
				$('#selConsultant').html(option);				
				
			}
		},
		error:function(){
			
		}
	});
}
function tabOPDConsultantsList(){
	 $("#advanced-wizard").hide();
	$(".blackborder").show();
	$("#tabAddOPDConsultants").css({
		"background": "#fff",
		"color": "#000"
	});
	$("#tabOPDConsultantsList").css({
		"background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
		"color": "#fff"
	});
	$("#OPDConsultantsList").css({"background": "#fff"});
	$("#selOPDRooms").removeClass("errorStyle");
	$("#selConsultant").removeClass("errorStyle");
	$("#OPDConsultantsTable").dataTable().fnReloadAjax();
	clear();
}

// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) {  
		 $(".close").click();
    }
});

$("#btnReset").click(function(){
	clear();
	$("#selOPDRooms").removeClass("errorStyle");
	$("#selOPDRooms").focus();
	$("#selConsultant").removeClass("errorStyle");
	
	//
	//Calling the function for loading the ward 
	//
	bindOPDRooms();
	bindConsultants();
});

//# sourceURL = bed.js