var bedAvailabilityTbl; 
$(document).ready(function() {
	debugger;
	if($("#myBedAvailability #hdnBedAvailability").length != 0){
		var roomType = $("#hdnBedAvailability").val();
	}
	else{
		var roomType = $("#hdnRoom").val();
	}
	
	/*By default when radio button is not checked show all active data*/
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		bedAvailabilityTbl = $('#bedAvailabilityTable').dataTable( {
			"bFilter": true,
			"processing": true,
			"sPaginationType":"full_numbers",
			"fnDrawCallback": function ( oSettings ) {
				$('#bedAvailabilityTable tbody tr').on('dblclick', function() {
					if ($(this).hasClass('selected')) {
						$(this).removeClass('selected');
					} else {
						bedAvailabilityTbl.$('tr.selected').removeClass('selected');
						$(this).addClass('selected');
					}

					var mData = bedAvailabilityTbl.fnGetData(this); // get datarow
					
					if (null != mData) // null if we clicked on title row
					{
						$("#txtRoomNo").val(mData["ward_number"]);
						$("#txtBedNo").val(mData["bed_number"]);
						$("#txtPrice").val(mData["price"]);
						$("#txtBedNo").attr("data-id",mData["id"]);
					}
					$(".close").unbind();
					$(".close").click();

				});
			},
			
			"sAjaxSource":"controllers/admin/bed_availability.php",
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "operation", "value": "show" });
			  aoData.push( { "name": "roomType", "value": roomType });
			},
			"aoColumns": [
				{ "mData": "ward_number" },
				{ "mData": "bed_number" },
				{ "mData": "price" },
			],
		} );
    }

    /*On change of radio button check it is checked or not*/
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	bedAvailabilityTbl.fnClearTable();
	    	bedAvailabilityTbl.fnDestroy();
	    	//Datatable code
			bedAvailabilityTbl = $('#bedAvailabilityTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
				},
				
				"sAjaxSource":"controllers/admin/bed_availability.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				  aoData.push( { "name": "roomType", "value": roomType });
				},
				"aoColumns": [
					{ "mData": "ward_number" },
					{ "mData": "bed_number" },
					{ "mData": "price" },
				],
			} );
		}
		else{
			bedAvailabilityTbl.fnClearTable();
	    	bedAvailabilityTbl.fnDestroy();
	    	//Datatable code
			bedAvailabilityTbl = $('#bedAvailabilityTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('#bedAvailabilityTable tbody tr').on('dblclick', function() {
						if ($(this).hasClass('selected')) {
							$(this).removeClass('selected');
						} else {
							bedAvailabilityTbl.$('tr.selected').removeClass('selected');
							$(this).addClass('selected');
						}

						var mData = bedAvailabilityTbl.fnGetData(this); // get datarow
						
						if (null != mData) // null if we clicked on title row
						{
							$("#txtRoomNo").val(mData["ward_number"]);
							$("#txtBedNo").val(mData["bed_number"]);
							$("#txtBedNo").attr("data-id",mData["id"]);
							$("#txtPrice").val(mData["price"]);
						}
						$(".close").unbind();
						$(".close").click();
					});
					
				},
				
				"sAjaxSource":"controllers/admin/bed_availability.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "show" });
				  aoData.push( { "name": "roomType", "value": roomType });
				},
				"aoColumns": [
					{ "mData": "ward_number" },
					{ "mData": "bed_number" },
					{ "mData": "price" },
				],
				
			});
		}
    });
});