$(document).ready(function() {
/* ****************************************************************************************************
    * File Name    :   triage_general.js
    * Company Name :   Qexon Infotech
    * Created By   :   Rupesh Arora
    * Created Date :   4th Jan, 2016
    * Description  :   This page manages triage data in consultant module
*************************************************************************************************** */

    loader();
    debugger;
    $('#oldHistoryModalDetails input[type=text]').attr('readonly', true);
    $('#oldHistoryModalDetails input[type="button"]').addClass("btnHide");
	$("#oldHistoryModalDetails #txtNotes").attr("disabled","disabled");

    var otable; //define datatable variable
    refreshData();

    //making time a global varibale
    var time;
    var getBMI;

    /* save functionality*/
    $("#txtTemprature").blur(function() {
        if($("#txtTemprature").val().trim() != '' && $("#txtTemprature").val().trim() < 35.0){
            $("#txtTempratureError").text("hypothemia");
        }
        else if($("#txtTemprature").val().trim() >= 35 && $("#txtTemprature").val().trim() < 37.5){
            $("#txtTempratureError").text("");
        }
        else if($("#txtTemprature").val().trim() > 37.5){
            $("#txtTempratureError").text("fever");
        }
        else {
            $("#txtTempratureError").text("");
        }
    }); 

    $("#txtHeight").blur(function() {
        if($("#txtBMI").val().trim() != '' && $("#txtBMI").val().trim() < 18.5){
            $("#txtBMIError").text("underweight");
        }
        else if($("#txtBMI").val().trim() >= 18.5 && $("#txtBMI").val().trim() < 25){
            $("#txtBMIError").text("");
        }
        else if($("#txtBMI").val().trim() >= 25.0){
            $("#txtBMIError").text("overweight");
        }
        else {
            $("#txtBMIError").text("");
        }
    }); 

    $("#txtWeight").blur(function() {
        if($("#txtBMI").val().trim() != '' && $("#txtBMI").val().trim() < 18.5){
            $("#txtBMIError").text("underweight");
        }
        else if($("#txtBMI").val().trim() >= 18.5 && $("#txtBMI").val().trim() < 25){
            $("#txtBMIError").text("");
        }
        else if($("#txtBMI").val().trim() >= 25){
            $("#txtBMIError").text("overweight");
        }
        else {
            $("#txtBMIError").text("");
        }
    });
    
    
    $("#txtPulse").blur(function() {
        if($("#txtPulse").val().trim() != '' && $("#txtPulse").val().trim() < 60){
            $("#txtPulseError").text("bradycardia");
        }
        else if($("#txtPulse").val().trim() >= 60 && $("#txtPulse").val().trim() < 80){
            $("#txtPulseError").text("");
        }
        else if($("#txtPulse").val().trim() >= 80){
            $("#txtPulseError").text("tachycardia");
        }
        else {
            $("#txtPulseError").text("");
        }
    }); 
    $("#txtDiastolic").blur(function() {
        if($("#txtSystolic").val().trim() >= 140 || $("#txtDiastolic").val().trim() >= 90 ){
            $("#txtBloodPressureError").text("High BP");
        }
        else if($("#txtSystolic").val().trim() != ''){
            if($("#txtSystolic").val().trim() <= 90 || $("#txtDiastolic").val().trim() <= 60){
                $("#txtBloodPressureError").text("Low BP");
            }
            else {
                $("#txtBloodPressureError").text("");
            }
        }
        else if($("#txtSystolic").val().trim() < 140 && $("#txtDiastolic").val().trim() < 90 
            && $("#txtDiastolic").val().trim() > 90 && $("#txtDiastolic").val().trim() > 60){
            $("#txtBloodPressureError").text("");
        }
        else {
            $("#txtBloodPressureError").text("");
        }
    });
    $("#txtRespirationRate").blur(function() {
        if($("#txtRespirationRate").val().trim() != '' &&  $("#txtRespirationRate").val().trim() < 12){
            $("#txtRespirationRateError").text("bradypnea");
        }
        else if($("#txtRespirationRate").val().trim() >= 12 && $("#txtRespirationRate").val().trim() <= 18){
            $("#txtRespirationRateError").text("");
        }
        else if($("#txtRespirationRate").val().trim() > 18){
            $("#txtRespirationRateError").text("tachypnea");
        }
        else {
            $("#txtRespirationRateError").text("");
        }
    });
    $("#txtHeartRate").blur(function() {
        if($("#txtHeartRate").val().trim() != '' &&  $("#txtHeartRate").val().trim() < 60){
            $("#txtHeartRateError").text("bradycardia");
        }
        else if($("#txtHeartRate").val().trim() >= 60 && $("#txtHeartRate").val().trim() <= 100 ){
            $("#txtHeartRateError").text("");
        }
        else if($("#txtHeartRate").val().trim() > 100){
            $("#txtHeartRateError").text("tachycardia");
        }
        else {
            $("#txtHeartRateError").text("");
        }
    }); 
    
    /*$("#txtSystolic").keyup(function() {
        if(!validRangeNumber($(this).val())){
            $("#txtBloodPressureError").text("Please enter valid systolic value.");
        }
        else{
            $("#txtBloodPressureError").text("");
            $("#txtSystolic").removeClass("errorStyle");
        }
    }); 
    

    $("#txtDiastolic").keyup(function() {
        if(!validRangeNumber($(this).val())){
            $("#txtBloodPressureError").text("Please enter valid diastolic value.");
        }
        else{
            $("#txtBloodPressureError").text("");
            $("#txtDiastolic").removeClass("errorStyle");
        }
    }); */
    
    $("#txtHeight").keyup(function() {
        if ($("#txtHeight").val() != "") {
            $("#txtHeightError").text("");
            $("#txtHeight").removeClass("errorStyle");
        }
    });
    
    $("#txtWeight").keyup(function() {  
        if ($("#txtWeight").val() != "") {
            $("#txtWeightError").text("");
            $("#txtWeight").removeClass("errorStyle");
        }
    });
    
    $("#txtHeight").keyup(function() {
        $("#txtBMI").val(calculateBmi());
    });
    
    $("#txtWeight").keyup(function() {
        $("#txtBMI").val(calculateBmi());
    });
    $("#btnSubmit").click(function() {
        //validation
        var flag = false;
		var flagCheck = false;
		if ($("#textVisitId").val() == "") {
			$("#textVisitId").focus();
            $("#textVisitIdError").text("Please enter visit id");
            $("#textVisitId").addClass("errorStyle");
			flag=true;
        }
		
        if (flag == true) {
            return false;
        }   
		
        var visitId = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
        var patientId = parseInt($("#txtPatientID").val().replace ( /[^\d.]/g, '' ));
        var weight = $("#txtWeight").val();
        var temprature = $("#txtTemprature").val();
        var height = $("#txtHeight").val();
        var pulse = $("#txtPulse").val();
        var bloodPressure = ($("#txtSystolic").val() + "/" + $("#txtDiastolic").val());
        var headCircumference = $("#txtHeadCircumference").val();
        var bodySurfaceArea = $("#txtBodySurfaceArea").val();
        var respirationRate = $("#txtRespirationRate").val();
        var oxygenSaturation = $("#txtOxygenSaturation").val();
        var heartRate = $("#txtHeartRate").val();
        bmi = calculateBmi();
        var notes = $("#txtNotes").val();
        var postData = {
            "operation": "save",
            "visitId": visitId,
            "patientId": patientId,
            "weight": weight,
            "temprature": temprature,
            "height": height,
            "pulse": pulse,
            "bloodPressure": bloodPressure,
            "headCircumference": headCircumference,
            "bodySurfaceArea": bodySurfaceArea,
            "respirationRate": respirationRate,
            "oxygenSaturation": oxygenSaturation,
            "heartRate": heartRate,
            "bmi": bmi,
            "notes": notes
        }
        $('#confirmUpdateModalLabel').text();
		if(flagCheck == true) {
			$('#updateBody').text("Are you sure that you want to update with out of range value?");
		}
		else if(flagCheck == false) {
			$('#updateBody').text("Are you sure that you want to update this?");
		}
        
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
            $.ajax({
                type: "POST",
                cache: false,
                url: "controllers/admin/triage_general.php",
                datatype: "json",
                data: postData,
                success: function(data) {
                    if (data != "0" && data != "") {
                        $('#messagemyModal').modal(); 
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("");
                        $('.modal-body').text("Triage record updated successfully!!!");
						clearTriageError(); // remove for error message
                    }
                },
                error: function() {
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                    $('#messagemyModal').modal();
                }
            });
        });            
    });

   
    //on reset button click
    $("#btnResetTriage").click(function() {
        $("#textVisitId").focus();
        clearTriage();
    });
	$("#txtBloodPressure").keypress(function(e) {
        if ((e.which < 47 || e.which > 57)) { // for disable alphabates keys
            return false;
        }
    });
});
function validatePressure(number) {
	var expr = /\//ig;
	return expr.test(number);
}
/*function to clear data*/
function clearTriage() {
    $("#textVisitId").val("");
    $("#txtPatientId").val("");
    $("#txtPatientName").val("");
    $("#txtGender").val("");
    $("#txtVisitDate").val("");
    $("#txt_year").val("");
    $("#txt_month").val("");
    $("#txt_day").val("");

    $("#txtWeight").val("");
    $("#txtTemprature").val("");
    $("#txtHeight").val("");
    $("#txtPulse").val("");
    $("#txtBloodPressure").val("");
    $("#txtHeadCircumference").val("");
    $("#txtBodySurfaceArea").val("");
    $("#txtRespirationRate").val("");
    $("#txtOxygenSaturation").val("");
    $("#txtHeartRate").val("");
    $("#txtBMI").val("");
    $("#txtNotes").val("");


    $("#textVisitId").removeClass("errorStyle");
    $("#txtWeight").removeClass("errorStyle");
    $("#txtTemprature").removeClass("errorStyle");
    $("#txtHeight").removeClass("errorStyle");
    $("#txtPulse").removeClass("errorStyle");
    $("#txtBloodPressure").removeClass("errorStyle");
    $("#txtHeadCircumference").removeClass("errorStyle");
    $("#txtBodySurfaceArea").removeClass("errorStyle");
    $("#txtRespirationRate").removeClass("errorStyle");
    $("#txtOxygenSaturation").removeClass("errorStyle");
    $("#txtHeartRate").removeClass("errorStyle");

    $("#textVisitIdError").text("");
    $("#txtWeightError").text("");
    $("#txtTempratureError").text("");
    $("#txtHeightError").text("");
    $("#txtPulseError").text("");
    $("#txtBloodPressureError").text("");
    $("#txtHeadCircumferenceError").text("");
    $("#txtBodySurfaceAreaError").text("");
    $("#txtRespirationRateError").text("");
    $("#txtOxygenSaturationError").text("");
    $("#txtHeartRateError").text("");
    $("#textVisitIdError").text("");
}

/*Function to cal bmi of patient*/
function calculateBmi() {
    var getBMI;
    if (($("#txtWeight").val() != "") && ($("#txtHeight").val() != "")) {
        var getWeight = $("#txtWeight").val();
        var getpartialheight = $("#txtHeight").val();
        var getHeight = getpartialheight / 100;

        getBMI = getWeight / (getHeight * getHeight);
    } else {
        getBMI = "";
    }
    return getBMI;
}

/*Function to get value from db and set to text box*/
function refreshData() {
    if ($('#thing').val() ==3) {
        var visitId = $("#textvisitId").val();
		var visitPrefix = visitId.substring(0, 3);
		visitId = visitId.replace ( /[^\d.]/g, '' ); 
		visitId = parseInt(visitId);
    }
    if ($('#thing').val() ==4) {
        var visitId = $("#oldHistoryVisitId").text();
		var visitPrefix = visitId.substring(0, 3);
		visitId = visitId.replace ( /[^\d.]/g, '' ); 
		visitId = parseInt(visitId);
    }
    if ($('#thing').val() ==5) {
        var visitId = $("#oldHistoryVisitId").text();
		var visitPrefix = visitId.substring(0, 3);
		visitId = visitId.replace ( /[^\d.]/g, '' ); 
		visitId = parseInt(visitId);
    }
    var postData = {
        "operation": "showData",
        "visitId": visitId
    }

    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/triage_general.php",
        data: postData,
        success: function(data) {
            if (data != "0") {
                var parseData = jQuery.parseJSON(data);
                $("#txtWeight").val(parseData[0]["weight"]);
                $("#txtTemprature").val(parseData[0]["temperature"]);
                $("#txtHeight").val(parseData[0]["height"]);
                $("#txtPulse").val(parseData[0]["pulse"]);
				var arr=parseData[0]["blood_pressure"].split('/');
				
                $("#txtSystolic").val(arr[0]);
                $("#txtDiastolic").val(arr[1]);
				
                $("#txtHeadCircumference").val(parseData[0]["head_circum"]);
                $("#txtBodySurfaceArea").val(parseData[0]["body_surface_area"]);
                $("#txtRespirationRate").val(parseData[0]["respiration_rate"]);
                $("#txtOxygenSaturation").val(parseData[0]["oxygen_saturation"]);
                $("#txtHeartRate").val(parseData[0]["heart_rate"]);
                $("#txtBMI").val(parseData[0]["bmi"]);
                $("#txtNotes").val(parseData[0]["notes"]);

                /*Change save button value if data is present there*/
                if ($("#txtWeight").val() !="" ){
                    $("#btnSubmit").val("Update");
                }


                if($("#txtTemprature").val().trim() != '' && $("#txtTemprature").val().trim() < 35.0){
                    $("#txtTempratureError").text("Hypothemia");
                }
                else if($("#txtTemprature").val().trim() >= 35 && $("#txtTemprature").val().trim() < 37.5){
                    $("#txtTempratureError").text("");
                }
                else if($("#txtTemprature").val().trim() > 37.5){
                    $("#txtTempratureError").text("Fever");
                }
                else {
                    $("#txtTempratureError").text("");
                }
             
            
                if($("#txtBMI").val().trim() != '' && $("#txtBMI").val().trim() < 18.5){
                    $("#txtBMIError").text("Underweight");
                }
                else if($("#txtBMI").val().trim() >= 18.5 && $("#txtBMI").val().trim() < 25){
                    $("#txtBMIError").text("");
                }
                else if($("#txtBMI").val().trim() >= 25.0 && $("#txtBMI").val().trim() <= 29.9){
                    $("#txtBMIError").text("Overweight");
                }
                else if($("#txtBMI").val().trim() >= 30.0 && $("#txtBMI").val().trim() <= 34.9){
                    $("#txtBMIError").text("Obesity class I");
                }
                else if($("#txtBMI").val().trim() >= 35.0 && $("#txtBMI").val().trim() <= 39.9){
                    $("#txtBMIError").text("Obesity class II");
                }
                else if($("#txtBMI").val().trim() >= 40){
                    $("#txtBMIError").text("Obesity class III");
                }
                else {
                    $("#txtBMIError").text("");
                }
            

            
                if($("#txtBMI").val().trim() != '' && $("#txtBMI").val().trim() < 18.5){
                    $("#txtBMIError").text("Underweight");
                }
                else if($("#txtBMI").val().trim() >= 18.5 && $("#txtBMI").val().trim() < 25){
                    $("#txtBMIError").text("");
                }
                else if($("#txtBMI").val().trim() >= 25.0 && $("#txtBMI").val().trim() <= 29.9){
                    $("#txtBMIError").text("Overweight");
                }
                else if($("#txtBMI").val().trim() >= 30.0 && $("#txtBMI").val().trim() <= 34.9){
                    $("#txtBMIError").text("Obesity class I");
                }
                else if($("#txtBMI").val().trim() >= 35.0 && $("#txtBMI").val().trim() <= 39.9){
                    $("#txtBMIError").text("Obesity class II");
                }
                else if($("#txtBMI").val().trim() >= 40){
                    $("#txtBMIError").text("Obesity class III");
                }
                else {
                    $("#txtBMIError").text("");
                }
           
            
            
                if($("#txtPulse").val().trim() != '' && $("#txtPulse").val().trim() < 60){
                    $("#txtPulseError").text("Bradycardia");
                }
                else if($("#txtPulse").val().trim() >= 60 && $("#txtPulse").val().trim() <= 100){
                    $("#txtPulseError").text("");
                }
                else if($("#txtPulse").val().trim() > 100){
                    $("#txtPulseError").text("Tachycardia");
                }
                else {
                    $("#txtPulseError").text("");
                }
            
               if($("#txtSystolic").val().trim() >= 140 || $("#txtDiastolic").val().trim() >= 90 ){
                    $("#txtBloodPressureError").text("High BP");
                }
                else if($("#txtSystolic").val().trim() != ''){
                    if($("#txtSystolic").val().trim() <= 90 || $("#txtDiastolic").val().trim() <= 60){
                        $("#txtBloodPressureError").text("Low BP");
                    }
                    else {
                        $("#txtBloodPressureError").text("");
                    }
                }
                else if($("#txtSystolic").val().trim() < 140 && $("#txtDiastolic").val().trim() < 90 
                    && $("#txtDiastolic").val().trim() > 90 && $("#txtDiastolic").val().trim() > 60){
                    $("#txtBloodPressureError").text("");
                }
                else {
                    $("#txtBloodPressureError").text("");
                }

           
                if($("#txtRespirationRate").val().trim() != '' &&  $("#txtRespirationRate").val().trim() < 12){
                    $("#txtRespirationRateError").text("Bradypnea");
                }
                else if($("#txtRespirationRate").val().trim() >= 12 && $("#txtRespirationRate").val().trim() <= 18){
                    $("#txtRespirationRateError").text("");
                }
                else if($("#txtRespirationRate").val().trim() > 18){
                    $("#txtRespirationRateError").text("Tachypnea");
                }
                else {
                    $("#txtRespirationRateError").text("");
                }
           
                if($("#txtHeartRate").val().trim() != '' &&  $("#txtHeartRate").val().trim() < 60){
                    $("#txtHeartRateError").text("Bradycardia");
                }
                else if($("#txtHeartRate").val().trim() >= 60 && $("#txtHeartRate").val().trim() <= 100 ){
                    $("#txtHeartRateError").text("");
                }
                else if($("#txtHeartRate").val().trim() > 100){
                    $("#txtHeartRateError").text("Tachycardia");
                }
                else {
                    $("#txtHeartRateError").text("");
                }
            } 
            else {
                clearTriage();
            }

        },
        error: function() {
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();
        }
    });
}

/*Function to validate number*/
function validnumber(number) {
    floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
    return floatRegex.test(number);
}

function clearTriageError(){
	 $("#textVisitIdError").text("");
    $("#txtWeightError").text("");
    $("#txtTempratureError").text("");
    $("#txtHeightError").text("");
    $("#txtPulseError").text("");
    $("#txtBloodPressureError").text("");
    $("#txtHeadCircumferenceError").text("");
    $("#txtBodySurfaceAreaError").text("");
    $("#txtRespirationRateError").text("");
    $("#txtOxygenSaturationError").text("");
    $("#txtHeartRateError").text("");
    $("#textVisitIdError").text("");
    $('#txtBMIError').text("");
}