$("document").ready(function() {
	debugger;
	$("#txtDischargeDate").datepicker();
	$("#textIPDId").focus();

	$("#serachIcon").click(function(){
		$("#hdnDischarge").val("1");
        $('#BedTransferModalLabel').text("Search IPD");
        $('#BedTransferModalBody').load('views/admin/view_inpatients.html');
        $('#myBedTransferModal').modal();
	});

	$("#btnLoad").click(function(){
		var flag = "false";

		if($("#textIPDId").val() == ""){
			$("#textIPDId").focus();
			$("#textIPDIdError").text("Please enter ipd id");
			$("#textIPDId").addClass("errorStyle");       
            flag = "true";
		}

		if(flag == "true"){
			return false;
		}

		var ipdId = $("#textIPDId").val();
		var ipdPrefix = ipdId.substring(0, 3);
		ipdId = ipdId.replace ( /[^\d.]/g, '' ); 
		ipdId = parseInt(ipdId);
		if(isNaN(ipdId)){
			ipdId="";
		}
		postData = {
			"operation" : "showData",
			"ipdId" : ipdId
		}

		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/discharge_form.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					var parseData= jQuery.parseJSON(data);
					//$("#textIPDId").val(parseData[0].id);
					$("#textIPDId").attr("disabled","disabled");
					$("#txtPatientName").val(parseData[0].name);
					$("#txtReasonHospitalization").val(parseData[0].admission_reason);
					$("#txtHospitalizationDate").val(parseData[0].admission_date);
					var getDOB = parseData[0].dob; 
					var new_age = getAge(getDOB);

					var split = new_age.split(' ');
					var age_years = split[0];
					var age_month = split[1];
					var age_day = split[2];

					$("#txt_year").val(age_years);
					$("#txt_month").val(age_month);
					$("#txt_day").val(age_day);
								
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	});

	$("#saveDetails").click(function(){

		var flag = "false";

		if($("#txtDischargeDate").val() == ""){
			$("#txtDischargeDate").focus();
			$("#txtDischargeDateError").text("Please select discharge date");
			$("#txtDischargeDate").addClass("errorStyle");       
            flag = "true";
		}

		if($("#textIPDId").val() == ""){
			$("#textIPDId").focus();
			$("#textIPDIdError").text("Please enter ipd id");
			$("#textIPDId").addClass("errorStyle");       
            flag = "true";
		}

		if(flag == "true"){
			return false;
		}

		var ipdId = $("#textIPDId").val();
		var ipdPrefix = ipdId.substring(0, 3);
		ipdId = ipdId.replace ( /[^\d.]/g, '' ); 
		ipdId = parseInt(ipdId);
		if(isNaN(ipdId)){
			ipdId="";
		}
		var dischargeDate = $("#txtDischargeDate").val();
		var dischargeAdvice = $("#txtDischargeAdvice").val();

		postData = {
			"operation" : "saveDischargeDetails",
			"ipdId" : ipdId,
			"dischargeDate" : dischargeDate,
			"dischargeAdvice" : dischargeAdvice
		}

		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/discharge_form.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data == "1"){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Discharge successfully!!!");
					$('#messagemyModal').modal();
					clearDetails();
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
		
	});

    $("#txtDischargeDate").change(function() {
        if ($("#txtDischargeDate").val() != "") {
			$("#txtDischargeDate").datepicker("hide");
            $("#txtDischargeDateError").text("");
            $("#txtDischargeDate").removeClass("errorStyle");
        }
    });

    $("#textIPDId").keyup(function() {
        if ($("#textIPDId").val() != "") {
            $("#textIPDIdError").text("");
            $("#textIPDId").removeClass("errorStyle");
        }
    });

    $("#textIPDId").change(function() {
        if ($("#textIPDId").val() != "") {
            $("#textIPDIdError").text("");
            $("#textIPDId").removeClass("errorStyle");
        }
    });

    $("#btnResetDischargeDetails").click(function(){
    	$("#textIPDId").val("");
    	$("#textIPDId").focus();
    	clearDetails();
    });

});

function clearDetails() {
	$('input[type=text]').next("span").text("");
	$('input[type=text]').val("");
	$('input[type=text]').removeClass("errorStyle");
	$('#txtDischargeAdvice').val("");
	$('#txtReasonHospitalization').val("");
	$('#txtDischargeAdviceError').text("");
	$('#txtDischargeAdvice').removeClass("errorStyle");
	$("#textIPDId").removeAttr("disabled");
	$("#textIPDId").focus();
}

//This function is used for calculate the age 
function getAge(dateString) {
    var now = new Date();
    var today = new Date(now.getYear(), now.getMonth(), now.getDate());

    var yearNow = now.getYear();
    var monthNow = now.getMonth();
    var dateNow = now.getDate();

    var dob = new Date(dateString.substring(0, 4), dateString.substring(5, 7) - 1, dateString.substring(8, 10));

    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();
    var age = {};
    var ageString = "";
    var yearString = "";
    var monthString = "";
    var dayString = "";


    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    age = {
        years: yearAge,
        months: monthAge,
        days: dateAge
    };

    if (age.years > 1) yearString = " years";
    else yearString = " year";
    if (age.months > 1) monthString = " months";
    else monthString = " month";
    if (age.days > 1) dayString = " days";
    else dayString = " day";


    if ((age.years > 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else ageString = "Oops! Could not calculate age!";

    return ageString;
}