var schemeExclusionTable ;//global variable schemeExclusionTable
var saveServiceTypeName = '';//take value of service button that is active.
$(document).ready(function() {
	var countPlanChange = 0;
	loader();
	
	$("#btnSave").hide();
	debugger;
	$('#selCompanyName').focus();
	/*reset button functionality*/
	$("#btnReset").click(function(){
		$('#selCompanyName').focus();
		$("input[type=button]").removeClass('activeSelf');
		schemeExclusionTable.fnClearTable();
		$("#btnSave").show();
	});

	// bind nsurance company
	bindInsuranceCompany();
	
	// bind company schemeon change company
	$("#selCompanyName").change(function() {
		var option = "<option value=''>--Select--</option>";
		
		if ($("#selCompanyName :selected") != "") {
			var value = $("#selCompanyName :selected").val();
			
			//on change call this function for load state
			bindSchemeName(value); 
		} else {
			$("#selSchemeName").html(option);
		}	

		if ($("#selCompanyName").val() != "") {
			$("#selCompanyNameError").text("");
			$("#selCompanyName").removeClass("errorStyle");
		}
	});	
	
	// bind company schemeon change company
	$("#selSchemeName").change(function() {
		var option = "<option value=''>--Select--</option>";
		
		if ($("#selSchemeName :selected") != "") {
			var value = $("#selSchemeName :selected").val();
			
			//on change call this function for load state
			bindPlanName(value); 
		} else {
			$("#selPlanName").html(option);
		}	

		if ($("#selSchemeName").val() != "") {
			$("#selSchemeNameError").text("");
			$("#selSchemeName").removeClass("errorStyle");
		}
	});

	$("#selPlanName").change(function() {
		if ($("#selPlanName :selected") != "" && countPlanChange == 0) {
			$(".slfBtn")[0].click();
		}
		countPlanChange++;
	});
	
	/**DataTable Initialization**/
	schemeExclusionTable = $('#schemeExclusionTable').DataTable({
		"bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
		"bAutoWidth" : false,
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 1 ] }
		],aLengthMenu: [
	        [-1],
	        ["All"]
	    ],
	    iDisplayLength: -1,
	});
	
	// remove error message on key up and change
	removeErrorMessage();
	
	// click on tab button
	$(".slfBtn").on("click",function(){
		var flag=false;		
		if(validTextField('#selPlanName','#selPlanNameError','Please plan name') == true)
		{
			flag = true;
		}
		if(validTextField('#selSchemeName','#selSchemeNameError','Please select scheme name') == true)
		{
			flag = true;
		}
		if(validTextField('#selCompanyName','#selCompanyNameError','Please select company name') == true)
		{
			flag = true;
		}
		
		if (flag==true) {
			return false;
		}
		var insuranceCompnayId =  $("#selCompanyName").val();
		var serviceNameId =  $("#selSchemeName").val();
		var planNameId =  $("#selPlanName").val();
		schemeExclusionTable.fnClearTable();
		$(".slfBtn").removeClass("activeSelf");
		$(this).addClass("activeSelf");
		saveServiceTypeName = $(this).val();
		$('.myHideClass').hide();

		$("#testStatus").text('');
		$('#addedStatus').text('');

		var clickedId = $(this).attr("id");

		if (clickedId == "addServicePopUp") {
			var postData = {
				"operation": "showServiceDetails",
				"insuranceCompnayId":insuranceCompnayId,
				"serviceNameId":serviceNameId,
				"planNameId":planNameId,
				"type":"Service"
			}
			// call load data into data table
			loadData(postData);
		}
		else if(clickedId == "addLabPopUp"){
			var postData = {
				"operation": "showLabDetails",
				"insuranceCompnayId":insuranceCompnayId,
				"serviceNameId":serviceNameId,
				"planNameId":planNameId,
				"type":"Lab"
			}
			// call load data into data table
			loadData(postData);			
		}
		else if(clickedId == "addRadiologyPopUp"){
			var postData = {
				"operation": "showRadiologyDetails",
				"insuranceCompnayId":insuranceCompnayId,
				"serviceNameId":serviceNameId,
				"planNameId":planNameId,
				"type":"Radiology"
			}
			// call load data into data table
			loadData(postData);
		}
		else if(clickedId == "addForensicPopUp"){
			var postData = {
				"operation": "showForensicDetails",
				"insuranceCompnayId":insuranceCompnayId,
				"serviceNameId":serviceNameId,
				"planNameId":planNameId,
				"type":"Forensic"
			}
			// call load data into data table
			loadData(postData);
		}
		else if(clickedId == "addProcedurePopUp"){
			var postData = {
				"operation": "showProcedureDetails",
				"insuranceCompnayId":insuranceCompnayId,
				"serviceNameId":serviceNameId,
				"planNameId":planNameId,
				"type":"Procedure"
			}
			// call load data into data table
			loadData(postData);
		}
		else{
			var postData = {
				"operation": "showPharmacyDetails",
				"insuranceCompnayId":insuranceCompnayId,
				"serviceNameId":serviceNameId,
				"planNameId":planNameId,
				"type":"Pharmacy"
			}
			// call load data into data table
			loadData(postData);
		}		
	});

	/*Save data to database*/
	$("#btnSave").click(function(){

		/*perform validation*/
		var flag=false;		
		if(validTextField('#selPlanName','#selPlanNameError','Please plan name') == true)
		{
			flag = true;
		}
		if(validTextField('#selSchemeName','#selSchemeNameError','Please select scheme name') == true)
		{
			flag = true;
		}
		if(validTextField('#selCompanyName','#selCompanyNameError','Please select company name') == true)
		{
			flag = true;
		}
		if (flag==true) {
			return false;
		}
		if (schemeExclusionTable.fnGetData().length < 1) {
			$('#messagemyModal').modal("show");
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Choose any of the one services");
			$("#selServiceType").focus();
			flag=true;
		}		
		if (flag==true) {
			return false;
		}
		var arrayData = [];
		var tableLength = schemeExclusionTable.fnGetNodes();
		for( var i=0; i<tableLength.length; i++){
			if ($("#checked"+i).prop("checked") == true) {
				aData = schemeExclusionTable.fnGetData(tableLength[i]);
				arrayData.push(aData);
			}			
		}

		var insuranceCompnayId =  $("#selCompanyName").val();
		var serviceNameId =  $("#selSchemeName").val();
		var planNameId =  $("#selPlanName").val();
		
		var postData ={
			"operation":"saveDataTableData",				
			"data":JSON.stringify(arrayData),
			"insuranceCompnayId" : insuranceCompnayId,
			"serviceNameId":serviceNameId,
			"planNameId"   : planNameId,
			"saveServiceTypeName" : saveServiceTypeName
		}
		$('#confirmMyModalLabel').text("Save Request");
        $('.selfRequestConfirm-body').text("Are you sure that you want to save this?");
        $('#confirmmyModal').modal();
        $('#confirm').unbind();
        $('#confirm').on('click',function(){
			dataCall("controllers/admin/scheme_exclusion.php", postData, function (result){
	        	if (result.trim() == 1) {
					$('#selCompanyName').focus();				
					callSuccessPopUp('Success','Saved successfully!!!');
					$('#selCompanyName').focus();
	        	}
	        });
		});
	});
});//end document ready


//call function for load  table
function loadData(postData) {
	// call the function ajax call
	dataCall("./controllers/admin/scheme_exclusion.php", postData, function (data) {
		if (data != null && data != "") {
			var countCheckBox = 0;
			var parseData = jQuery.parseJSON(data);			
			$(parseData[1]).each(function(){
				var id = this.id;
				var name = this.name;
				schemeExclusionTable.fnAddData([name,'<input type="checkbox" id="checked'+countCheckBox+'" class="" name="" style="cursor:pointer;">',id]);	
				countCheckBox++;				
			});
			var dataNodes  = schemeExclusionTable.fnGetNodes();
			$(parseData[0]).each(function(){
				var itemId = this.item_id;
				var rowData = schemeExclusionTable.fnGetData();
				$(rowData).each(function(i){
					
					if(rowData[i][2] == itemId) {
						$(dataNodes[i]).find('input').attr('checked',true);						
					}
				});
			});
		}
		if($("#schemeExclusionTable input[type='checkbox']").is(":checked") == true){
			$("#btnSave").show();
		}
		else{
			$("#btnSave").hide();
		}
		$("#schemeExclusionTable input[type='checkbox']").change(function(){
			if($("#schemeExclusionTable input[type='checkbox']").is(":checked") == true){
				$("#btnSave").show();
			}
		});
	});
}

function FadeOut(){
    setTimeout(function() {
        $('#addedStatus').text('');
        $('#testStatus').text('');
    }, 3000);
}

// call function for bind data
function bindInsuranceCompany() {
	$.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/insurance_plan.php",
        data: {
            "operation": "showCompany"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selCompanyName').html(option);
            }
        },
        error: function() {}
    });
}

// call function for bind scheme name
function bindSchemeName(value) {
	loader();
	 $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/scheme_exclusion.php",
        data: {
            "operation": "showSchemeName",
            companyId: value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].plan_name + "</option>";
                }
                $('#selSchemeName').html(option);
            }
        },
        error: function() {}
    });
}

// call function for bind scheme name
function bindPlanName(value) {
	loader();
	 $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/scheme_exclusion.php",
        data: {
            "operation": "showPlanName",
            schemeId: value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].scheme_plan_name + "</option>";
                }
                $('#selPlanName').html(option);
            }
        },
        error: function() {}
    });
}

/*src url = self_request.js*/