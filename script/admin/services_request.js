var oServiceTable = null;
var paymentMode = '';
var insuranceCompanyId = "";
var insurancePlanId = "";
var insurancePlanNameid = "";
var revisedCost = '';
var paymentMethod = '';
var copayType = '';
var copayValue = '';
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   service_request.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Arora
 * Created Date :   31th dec, 2015
 * Description  :   This page  manages service tests in consultant module
 *************************************************************************************************** */

    loader();
    debugger;
	var check = "";
    
	$(".amountPrefix").text(amountPrefix);
	$('#oldHistoryModalDetails input[type=text]').addClass("as_label");
    $('#oldHistoryModalDetails input[type=text]').attr('readonly', true);
    $('#oldHistoryModalDetails input[type="button"]').addClass("btnHide");
    $('#oldHistoryModalDetails #divDropDownServiceRequest').hide();

    /*Focus on first element*/
    $("#selServiceLabType").focus();

    /**DataTable Initialization**/
    oServiceTable = $('#serviceRequestsTbl').DataTable({
        "bPaginate": false,
        aoColumnDefs: [{'bSortable': false,'aTargets': [4] }]
    });

    bindServiceType(); //Loading the Lab type dropdown at the start
	
	if ($('#thing').val() ==3) {
        var visitId = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
		getPaymentMode(visitId);
    }
    if ($('#thing').val() ==4) {
        var visitId = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
		getPaymentMode(visitId);
    }
    if ($('#thing').val() ==5) {
        var visitId = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
		getPaymentMode(visitId);
    }

    /*On selecting the lab type drop down*/
    $("#selServiceLabType").change(function() {
        var option = "<option value='-1'>Select</option>";
        if ($("#selServiceLabType :selected") != -1) {
            var value = $("#selServiceLabType :selected").val();
            $('#selServiceLabTypeError').text("");
            $('#selServiceLabType').removeClass("errorStyle");
        } else {
            $("#selServiceTest").html(option);
        }
        bindServices(value); //on change of lab type call this function
    });

   /* On selecting the lab test drop down*/
    $("#selServiceTest").change(function() {
        var option = "<option value='-1'>Select</option>";
        if ($("#selServiceTest :selected").val() != -1) {
            var serviceId = $("#selServiceTest :selected").val();
            $('#selServiceTestError').text("");
            $('#selServiceTest').removeClass("errorStyle");
            if (paymentMode.toLowerCase() == "insurance") {
                getRevisedCost(serviceId);
                checkExclusion(serviceId,insuranceCompanyId,insurancePlanId,insurancePlanNameid,"service");                
            }
            else{
                checkTestExcluded = '';
            }
        }
    });

    /*Binding Service Type*/
    function bindServiceType() {
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/self_request.php",
            data: {
                "operation": "showServiceType"
            },
            success: function(data) {
                if (data != null && data != "") {
                    var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                    var option = "<option value='-1'>--Select--</option>";
                    for (var i = 0; i < parseData.length; i++) {
                        option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
                    }
                    $('#selServiceLabType').html(option);
                }
            },
            error: function() {
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }

    /*Binding Services*/
    function bindServices(value) {
        var visitId = parseInt($('#textvisitId').val().replace ( /[^\d.]/g, '' ));
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/service_request.php",
            data: {
                "operation": "showServices",
                "value": value,
                "visitId" : visitId
            },
            success: function(data) {
                if (data != null && data != "") {

                    var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                    var option = "<option value='-1'>--Select--</option>";
                    for (var i = 0; i < parseData.length; i++) {
                        option += "<option value='" + parseData[i].id + "' data-cost='" + parseData[i].cost + "' data-name='" + parseData[i].name + "'>" + parseData[i].name + "</option>";
                    }
                    $('#selServiceTest').html(option);
                }
            },
            error: function() {
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }

    /*Add Lab Test click event*/
    $('#addServiceRequest').click(function() {
		
		check = "flase";
		
        $('#addServiceRequest').css('border', ''); //remove the red error border from add test button
        $('#noServiceDataError').html(""); //remove the error message after the add button

        if ($('#selServiceLabType').val() == "-1" || $('#selServiceLabType').val() == null) {
            $('#selServiceLabTypeError').text("Please select service type");
            $('#selServiceLabType').addClass("errorStyle");
			$('#selServiceLabType').focus();
            return false;
        }
        if ($('#selServiceTest').val() == "-1" || $('#selServiceTest').val() == null) {
            $('#selServiceTestError').text("Please select service");
            $('#selServiceTest').addClass("errorStyle");
			$('#selServiceTest').focus();
            return false;
        }
        if($("#selRequestType").val() == -1){
            $("#selRequestType").focus();
            $("#selRequestType").addClass("errorStyle");
            $("#selRequestTypeError").text("Please select request type");
            return false;
        }

        var id = $('#selServiceTest').val();//getting value of selected element
        var name = $('#selServiceTest  option:selected').attr('data-name');//getting attribute name of selected element
        var cost = $('#selServiceTest  option:selected').attr('data-cost');
        var requestType = $('#selRequestType').val();
        var newRevisedCost = '';
        if (revisedCost == "") {
            newRevisedCost = cost;
            paymentMethod = "cash";
        }
        else {
            paymentMethod = "insurance";
            newRevisedCost = revisedCost;
        }

        /*Check that test already exist*/
        for (var i = 0; i < oServiceTable.fnGetNodes().length; i++) {
            var iRow = oServiceTable.fnGetNodes()[i];   // assign current row to iRow variable
            var aData = oServiceTable.fnGetData(iRow); // Pull the row

            testId = aData[6];//getting service id
            var flag = false;
			var tstName = $(aData[0]).text();
			if (id == testId && aData[2]!="done") {
				$('#selServiceTest').focus();
                $('#selServiceTestError').text("Service already exist");
                $('#selServiceTest').addClass("errorStyle");
                return true;
			}
            if (flag == true) {
                return false;
            }
        }

        if (checkTestExcluded =="yes") {
            var newPaymentMethod = "cash";//cauz if service is excluded then always payment method always cash
            $('#confirmUpdateModalLabel').text();
            $('#updateBody').text("Are you sure you want to add this still it is not included in your insurance?");
            $('#confirmUpdateModal').modal();
            $("#btnConfirm").unbind();
            $("#btnConfirm").click(function(){

                newRevisedCost = cost;//if it is excluded then revised cost is equal to orignal cost

                if (requestType == "internal") {
                    /*Add data in table*/
                    var currData = oServiceTable.fnAddData(["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", "Unpaid", "Pending", cost,newRevisedCost, "<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>", id,requestType,newPaymentMethod]);
                }
                else {
                    var currData = oServiceTable.fnAddData(["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", "N/A", "N/A", 0,0, "<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>", id,requestType,newPaymentMethod]);
                }

                /*Get current row to add color in that*/
                var getCurrDataRow = oServiceTable.fnSettings().aoData[ currData ].nTr;
                $(getCurrDataRow).find('td').css({"background-color":"#fff","color":"#000"});

                if (requestType == "internal") {
                    $('#totalServiceBill').text(parseInt($('#totalServiceBill').text()) + parseInt(newRevisedCost));//add total bill
                }

            });

        }
        else{
            if (requestType == "internal") {
                /*Add data in table*/
                var currData = oServiceTable.fnAddData(["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", "Unpaid", "Pending", cost,newRevisedCost, "<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>", id,requestType,paymentMethod]);
            }
                
            else {
                var currData = oServiceTable.fnAddData(["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", "N/A", "N/A", 0,0, "<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>", id,requestType,paymentMethod]);
            }

            /*Get current row to add color in that*/
            var getCurrDataRow = oServiceTable.fnSettings().aoData[ currData ].nTr;
            $(getCurrDataRow).find('td').css({"background-color":"#fff","color":"#000"});

            if (requestType == "internal") {
                $('#totalServiceBill').text(parseInt($('#totalServiceBill').text()) + parseInt(newRevisedCost));//add total bill
            }
        }
    });


    /*Save button functionality*/
    $('#btnSaveServiceTestRequest').click(function() {

        var tableData = oServiceTable.fnGetData(); //fetching the datatable data

        /*Code to compare previpous and current data based on attribute so on click not to get previous data coming from database*/
        var arr= [];
        jQuery.grep(tableData, function( n, i ){
            if($(n[0]).attr("data-previous") == "false"){
                arr.push(n);
            }
        });
		if(check == "true"){
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Sorry");
			$('.modal-body').text("Your request already in pending..");
			return false;
		}
        //Checking whether datatable is empty or not
        if (tableData.length == 0) {

            $('#addServiceRequest').addClass("errorStyle");//Put the red border around the add lab button
            $('#noServiceDataError').html("&nbsp;&nbsp;Please add test(s)."); //Put the error message after the add lab test button
            return false;

        } else { //Saving the data

            var patientId = parseInt($('#txtPatientID').val().replace ( /[^\d.]/g, '' ));
            var visitId = parseInt($('#textvisitId').val().replace ( /[^\d.]/g, '' ));
			if(arr.length == 0){
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Sorry");
				$('.modal-body').text("Your request already in pending..");
			}
			else{
                $('#confirmUpdateModalLabel').text();
                $('#updateBody').text("Are you sure that you want to save this?");
                $('#confirmUpdateModal').modal();
                $("#btnConfirm").unbind();
                $("#btnConfirm").click(function(){

                    var totalCurrentReqBill = 0;
                    //get the total cost of all currently added request
                    $.each(arr ,function(i,v){  
                        if (arr[i][8] == 'insurance') {
                            totalCurrentReqBill= totalCurrentReqBill + parseInt(arr[i][4]);
                        }  
                    });

    				$.ajax({
    					type: "POST",
    					cache: false,
    					url: "controllers/admin/service_request.php",
    					datatype: "json",
    					data: {
    						'operation': 'save',
    						'data': JSON.stringify(arr),//send only current data
    						'patientId': patientId,
    						'visitId': visitId,
    						'tblName': 'services_request',
    						'fkColName': 'service_id',
                            'copayType' : copayType,
                            'copayValue' : copayValue,
                            'totalCurrentReqBill' : totalCurrentReqBill
    					},

    					success: function(data) {
    						if (data != "0" && data == "1") {
    							$('#messageMyModalLabel').text("Success");
    							$('#messagemyModal .modal-body').text("Service request saved successfully!!!");
    							$('#messagemyModal').modal();
    							check = "true";
    							Services(visitId,paymentMode);
                                clear();
    						} 
    						else if (data == "0"){ 
    							$('#messagemyModal').modal();
    							$('#messageMyModalLabel').text("Sorry");
    							$('.modal-body').text("Your test result is still not sent");
    						}
    						else{}//nothing
    					},
    					error: function() {
    						$('#messageMyModalLabel').text("Error");
    						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
    						$('#messagemyModal').modal();
    					}
    				});
                });
			}
        }
    }); 

    //Reset button functionality
    $('#btnResetServiceTestRequest').click(function() {
        var lengthDeleteRow = $(".btnEnabled").closest("tr").length;
        for(var i=0;i<lengthDeleteRow;i++){
            var myDeleteRow = $(".btnEnabled").closest("tr")[0];            
            $('#totalServiceBill').text(parseInt($('#totalServiceBill').text()) - parseInt($(myDeleteRow).find('td:eq(4)').text()));
            oServiceTable.fnDeleteRow(myDeleteRow);
        }
        clear();
        $("#selServiceLabType").focus();
    });

    $("#selRequestType").change(function() {
        if ($("#selRequestType :selected") != -1) {
            $('#selRequestTypeError').text("");
            $('#selRequestType').removeClass("errorStyle");
        }
    });
});

/**Row Delete functionality**/
function rowDelete(currInst) {
    var row = currInst.closest("tr").get(0);
    var oServiceTable = $('#serviceRequestsTbl').dataTable();
    $('#totalServiceBill').text(parseInt($('#totalServiceBill').text()) - parseInt($(row).find('td:eq(4)').text()));//subtract amount of that row from total amount on remove of a row
    oServiceTable.fnDeleteRow(row);
}

function clear() {
    $("#selServiceLabType").val("-1");
    $("#selServiceLabTypeError").text("");
    $("#selServiceTestError").text("");
    $("#addServiceRequest").removeClass("errorStyle");
    $("#noServiceDataError").text("");
    $("#selServiceLabType").focus();
    $("#selServiceTest").removeClass("errorStyle");
    $('#selServiceTest').html('<option value ="">--Select--</option>');
}
function Services(visitId,paymentMode){
    /*Fetch data from db and insert in to table during page load*/
    var postData = {
        "operation": "oldServiceSearch",
        "visit_id": visitId,
        "paymentMode" : paymentMode,
        "activeTab" : "service"
    }
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/old_test_requests.php",
        datatype: "json",
        data: postData,

        success: function(data) {
            dataSet = JSON.parse(data);
            oServiceTable.fnClearTable();
            $('#totalServiceBill').text("0");
            
            for(var i=0; i< dataSet.length; i++){
                var id = dataSet[i].id;
                var pay_status = dataSet[i].pay_status;
                var name = dataSet[i].name;
                var test_status = dataSet[i].test_status;
                var cost = dataSet[i].cost;
                var oldRevisedCost =''; 
                if (paymentMode != null && paymentMode.toLowerCase() == "insurance") {
                    oldRevisedCost = dataSet[i].revised_cost;
                }
                else {
                    oldRevisedCost = cost;
                }

                //check wheteher in name external is coming or not
                if (name.indexOf("(external)") > 1) {
                    pay_status = "N/A";
                    test_status = "N/A";
                    cost = 0;
                    oldRevisedCost = 0;
                }
                /*Apply here span to recognise previous or current data*/
                oServiceTable.fnAddData(["<span data-previous='true'>"+name+"</span>",pay_status, test_status,cost,oldRevisedCost,"<input type='button' class ='btnDisabled' value='Remove' disabled>",id]);//send one column null so user can't remove previous data
                $('#totalServiceBill').text(parseInt($('#totalServiceBill').text()) + parseInt(oldRevisedCost));//add total bill                
            }
            oServiceTable.find('tbody').find('tr').find('td').css({"background-color":"#ccc"});
        }
    });
}
//get payment mode insurance or other
function getPaymentMode(visitId){
    var postData = {
        "operation": "payment_mode",
        "visitId": visitId
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (result.trim() != '') {
            paymentMode = parseData[0].payment_mode;

            if (paymentMode.toLowerCase() == "insurance") {
                insuranceDetails(visitId);
            }            
        } 
        Services(visitId,paymentMode);
    });
}
function insuranceDetails(visitId){
    var postData = {
        "operation": "insuranceDetail",
        "visitId": visitId
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (parseData != '') {
            insuranceCompanyId = parseData[0].insurance_company_id;
            insurancePlanId = parseData[0].insurance_plan_id;
            insurancePlanNameid = parseData[0].insurance_plan_name_id;

            getCopayValue();//get the copay details
        }
    });
}
function getRevisedCost(serviceId){
    var postData = {
        "operation": "revisedCostDetail",
        "insuranceCompanyId": insuranceCompanyId,
        "insurancePlanId" : insurancePlanId,
        "insurancePlanNameid" : insurancePlanNameid,
        "componentId" : serviceId,
        "activeTab" : "Service"
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (result.length > 2) {
            revisedCost = parseData[0].revised_cost;
        }
        else{
            revisedCost ='';
        }
    });
}
function getCopayValue(){
    var postData = {
        "operation": "getCopayValue",
        "insuranceCompanyId": insuranceCompanyId,
        "insurancePlanId" : insurancePlanId,
        "insurancePlanNameid" : insurancePlanNameid
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (parseData != '' && parseData != null) {
            copayType = parseData[0].copay_type;
            copayValue = parseData[0].value;
        }
    });
}