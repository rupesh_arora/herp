var serviceTypeTable; //defile variable for serviceTypeTable 
$(document).ready(function() {

debugger;	
loader();
	//Click function for reset button
	$('#btnReset').click(function(){
        $("#txtServiceType").removeClass("errorStyle");
		$('#txtServiceType').focus();
		clear();
	});
// show the add service type 
    $("#form_service").hide();
    $("#service_list").addClass('list');    
    $("#tab_service").addClass('tab-list-add');
	
	
    $("#tab_add_service").click(function() {
        $("#form_service").show();
        $(".blackborder").hide();

        $("#tab_add_service").addClass('tab-detail-add');
        $("#tab_add_service").removeClass('tab-detail-remove');
        $("#tab_service").removeClass('tab-list-add');
        $("#tab_service").addClass('tab-list-remove');
        $("#service_list").addClass('list');
        
		$("#txtServiceNameError").text("");
		$('#txtServiceType').val("");
		$('#txtServiceType').focus();
        $("#txtServiceType").removeClass("errorStyle");
        clear();

    });
// show the add service type list
    $("#tab_service").click(function() {
    	showTableList();		
	});
	
	// import excel on submit click
	$('#importDataFile').click(function(){
		var flag = false;
		if($('#file').prop('files')[0] == undefined || $('#file').val() == "") {
			$('#txtFileError').show();
			$('#txtFileError').text("Please choose file.");
			flag  = true;
		}
		if(flag == true){
			return false;
		}
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to upload this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			var file_data = $('#file').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: 'controllers/admin/servicetype.php', // point to server-side PHP script 
				dataType: 'text', // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					if(data != "" && data != "invalid file" && data =="1"){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Your file has been successfully imported!!!");
						$('#messagemyModal').modal();
						$('#file').val("");
					}
					else if(data == "invalid file"){
						$('#txtFileError').show();
						$('#txtFileError').text("Please import only CSV/XLS file.");
						$('#file').val("");
					}
					else if (data =="You must have two column in your file i.e service type and description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="First column should be service type" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Second column should be description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Data can't be null in service type column" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Please insert data in first column and second column only i.e A & B") {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
					else if(data == ""){
						$('#txtFileError').show();
						$('#txtFileError').text("Data already exist.");
						$('#file').val("");
					}
				},
				error: function() {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Data Not Found!!!");
					$('#messagemyModal').modal();

				}
			});
		});
	});
	
	// check the validation
    $("#btnAddService").click(function() {

		var flag = "false";
        if ($("#txtServiceType").val().trim() == '') {
            $("#txtServiceNameError").text("Please enter service type");
			$('#txtServiceType').focus();
            $("#txtServiceType").addClass("errorStyle");
            flag = "true";
        }
		else
		{
			var serviceType = $("#txtServiceType").val().trim();
			var description = $("#txtServiceDesc").val(); // ajax call for service Type validation
			description = description.replace(/'/g, "&#39");
			$.ajax({
				type: "POST",
				cache: false,
				url: "controllers/admin/servicetype.php",
				datatype:"json",  
				data:{
				
					serviceType:serviceType,
					Id:"",
					operation:"checkserviceType"
				},
				success:function(data) {
					if(data == "1")
					{
					    $("#txtServiceNameError").show();
					    $("#txtServiceNameError").text("The service type is already exists");
					    $("#txtServiceType").addClass("errorStyle");
					    $("#txtServiceType").focus();
					}
					else{
						//ajax call for insert data into data base
						var postData = {
							"operation":"save",
							"serviceType":serviceType,
							"description":description
						}
						$.ajax(
							{					
							type: "POST",
							cache: false,
							url: "controllers/admin/servicetype.php",
							datatype:"json",
							data: postData,

							success: function(data) {
								if(data != "0" && data != ""){
									$('.modal-body').text("");
									$('#messageMyModalLabel').text("Success");
									$('.modal-body').text("Service type saved successfully!!!");
									$('#messagemyModal').modal();
									$('#ok').css({"visibility": "visible !important"  });
									showTableList();		        			
								}								
							},
							error:function() {
								$('#messageMyModalLabel').text("Error");
								$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
								$('#messagemyModal').modal();
							}
						});//  end ajax call
					}
				},
				error:function(){
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("data not found!!!");
					$('#messagemyModal').modal();
				}
			});
		}
   }); //end button click function

    //validation
    $("#txtServiceType").keyup(function() {
        if ($("#txtServiceType").val() != '') {
            $("#txtServiceNameError").text("");
            $("#txtServiceType").removeClass("errorStyle");
        } 
    }); 
    if($('.inactive-checkbox').not(':checked')){
		//Datatable code
		 loadDataTable();
	}
	$('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	serviceTypeTable.fnClearTable();
	    	serviceTypeTable.fnDestroy();
	    	serviceTypeTable="";
	    	serviceTypeTable = $('#tblServiceType').dataTable( {
				"bFilter": true,
				"processing": true,
				 "deferLoading": 57,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {	
					$('.restore').unbind();					
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  serviceTypeTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/servicetype.php",
				"fnServerParams": function ( aoData ) {
					aoData.push( { "name": "operation", "value": "checked" });
				},
				"aoColumns": [
					{  "mData": "type" },
					{ "mData": "description" },	
					{  
						"mData": function (o) { 
						var data = o;
						return '<i class="ui-tooltip fa fa-pencil-square-o restore"  style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},
				],
				aoColumnDefs: [
					{ 'bSortable': false, 'aTargets': [ 1 ] },
			        { 'bSortable': false, 'aTargets': [ 2 ] }
				]
			});
		}
		else{
			serviceTypeTable.fnClearTable();
	    	serviceTypeTable.fnDestroy();
	    	serviceTypeTable="";
			loadDataTable();
		}
    });	
});//end document.ready function 

// edit data dunction for update 
function editClick(id,type,description){	

	$("#tab_add_service").click();
    $("#uploadFile").hide();
    $("#btnReset").hide();
    $("#btnAddService").hide();
    $('#tab_add_service').html("+Update Service Type");
    $("#txtServiceType").focus();
	
	$("#btnUpdate").removeAttr("style");
	$("#txtServiceType").removeClass("errorStyle");
	$("#txtServiceNameError").text("");
	$('#txtServiceType').val(type);
	$('#txtServiceDesc').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id); 
	 
	    //validation
     $("#txtServiceType").keyup(function() {
        if ($("#txtServiceType").val() != '') {
            $("#txtServiceNameError").text("");
			
            $("#txtServiceType").removeClass("errorStyle");
        }
    }); 
	$(" #btnUpdate").click(function(){ // click update button
		var flag = "false";
		if ($("#txtServiceType").val().trim() == '') {
			$("#txtServiceNameError").text("Please enter service type");
			$('#txtServiceType').focus();
			$("#txtServiceType").addClass("errorStyle");
			flag = "true";
		}
		else
		{
			var serviceType = $("#txtServiceType").val().trim();
			var description = $("#txtServiceDesc").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();	
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				$.ajax({						// ajax call for service Type validation
					type: "POST",
					cache: false,
					url: "controllers/admin/servicetype.php",
					datatype:"json",  
					data:{
					
						serviceType:serviceType,
						Id:id,
						operation:"checkserviceType"
					},
					success:function(data) {
						if(data == "1")
						{
						    $("#txtServiceNameError").show();
							$("#txtServiceNameError").text("The service type is already exists");
							$("#txtServiceType").addClass("errorStyle");
							$("#txtServiceType").focus();
						}
						else{
								var postData = {
								"operation":"update",
								"serviceType":serviceType,
								"description":description,
								"id":id
								}
								$.ajax( //ajax call for update data
								{					
									type: "POST",
									cache: false,
									url: "controllers/admin/servicetype.php",
									datatype:"json",
									data: postData,

									success: function(data) {
										if(data != "0" && data != ""){
										$('#myModal').modal('hide');
										$('.close-confirm').click();
										$('.modal-body').text("");
										$('#messageMyModalLabel').text("Success");
										$('.modal-body').text("Service type updated successfully!!!");
										$('#messagemyModal').modal();
										$("#tab_service").click();
										}
									},
									error:function() {
										$('.close-confirm').click();
										$('#messageMyModalLabel').text("Error");
										$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
										$('#messagemyModal').modal();
									}
								});// end of ajax
						}
					},
					error:function(){
						$('.close-confirm').click();
						$('#messageMyModalLabel').text("Sorry");
						$('.modal-body').text("data not found!!!");
						$('#messagemyModal').modal();
					}
				});
			});
		}	
	});
}// end update button

function deleteClick(id){ // delete click function
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete Service Type");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	$('#selectedRow').val(id); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteServicesType("'+type+'");');
}// end click fucntion

function restoreClick(id){//function for restore the service type
	$('.modal-body').text("");
	$('#selectedRow').val(id);
	$('#confirmMyModalLabel').text("Restore Service Type");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal(); 
	var type="restore";
	$('#confirm').attr('onclick','deleteServicesType("'+type+'");');
}
//function for delete and restore the service type
function deleteServicesType(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
		var postData = {
			"operation":"delete",
			"id":id
		}
		$.ajax({						 // ajax call for delete
			type: "POST",
			cache: false,
			url: "controllers/admin/servicetype.php",
			datatype:"json",
			data: postData,

			success: function(data) {
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Service type deleted successfully!!!");
					$('#messagemyModal').modal();
					serviceTypeTable.fnReloadAjax();
					
				}
				else{
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("This service type is used , so you can not delete!!!");
					$('#messagemyModal').modal();
					serviceTypeTable.fnReloadAjax(); 
					
				}
			},
			error:function() {
				alert('error');
			}
		}); // end ajax 
	}
	else{
		var id = $('#selectedRow').val();
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/servicetype.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Service type restored successfully!!!");
					$('#messagemyModal').modal();
					serviceTypeTable.fnReloadAjax(); 
					 clear();
					 
				}			
			},
			error:function()
			{
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	}
}	

// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) {
		 $('.close').click();
    }
});

function clear(){
	$('#txtServiceType').val("");
	$('#txtServiceType').removeClass("errorStyle");
	$('#txtServiceNameError').text("");
	$('#txtFileError').text('');
	$('#txtServiceDesc').val("");
}

function loadDataTable(){
	serviceTypeTable = $('#tblServiceType').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {
			$('.update').unbind();				
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = serviceTypeTable.fnGetData(data);
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					var type = mData["type"];
					var description = mData["description"];
					editClick(id,type,description);
   
				}
			});
			$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  serviceTypeTable.fnGetData(data);
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);
				}
			});
		},
		"sAjaxSource":"controllers/admin/servicetype.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{  "mData": "type" },
			{  "mData": "description" },	
			{  
				"mData": function (o) { 
				var data = o;
				return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
				   " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
				   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
				   " style='font-size: 22px; color:#a94442; cursor:pointer;' "+
				   " data-original-title='Delete'></i>"; 
				}
			},
		],
		aoColumnDefs: [
			{ 'bSortable': false, 'aTargets': [ 1 ] },
			{ 'bSortable': false, 'aTargets': [ 2 ] }
		]

	} );
}

function showTableList(){
	
	$("#form_service").hide();
	$(".blackborder").show();

	$("#tab_add_service").removeClass('tab-detail-add');
    $("#tab_add_service").addClass('tab-detail-remove');
    $("#tab_service").removeClass('tab-list-remove');    
    $("#tab_service").addClass('tab-list-add');
    $("#service_list").addClass('list');


    $("#uploadFile").show();
    $("#btnReset").show();
    $("#btnAddService").show();
    $('#btnUpdate').hide();
    $('#tab_add_service').html("+Add Service Type");
    $('#inactive-checkbox-tick').prop('checked', false).change();
	clear();	
}
//# sourceURL=servicetype.js