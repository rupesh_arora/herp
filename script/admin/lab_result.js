/*
 * File Name    :   lab_result.js
 * Company Name :   Qexon Infotech
 * Created By   :   kamesh pathak
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load and send data to back hand.
 */
var otableLabComponent;
$(document).ready(function(){
	loader(); 
	debugger;
	$('#txtSpecimenId').focus();
	otableLabComponent = $('#tblLabComponent').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",  
		"fnDrawCallback" : function() {},
		aoColumnDefs: [
		   { aTargets: [ 2 ], bSortable: false },
		]
	});
	
	$("#btnSpecimenLoad").click(function(){
		$("#txtSpecimenIdError").text("");
		$("#txtSpecimenId").removeClass("errorStyle");
		loadSpecimenResults();
	});
	// for remove error style	
    $("#txtSpecimenId").keyup(function() {

        if ($("#txtSpecimenId").val().trim() != "") {
            $("#txtSpecimenIdError").hide();
			$("#txtSpecimenIdError").text("");
			$("#txtSpecimenId").removeClass("errorStyle");
        }
		else{
			$("#txtSpecimenIdError").show();
			$("#txtSpecimenIdError").text("Please enter specimen ID");
			$("#txtSpecimenId").addClass("errorStyle");
			$('#txtSpecimenId').focus();
			labclear();
		}
    });
	$("#txtSpecimenId").change(function() {

        if ($("#txtSpecimenId").val().trim() != "") {
            $("#txtSpecimenIdError").hide();
			$("#txtSpecimenIdError").text("");
			$("#txtSpecimenId").removeClass("errorStyle");
        }
	});
	$('#searchSpecimen').click(function(){
		$('#specimenModalLabel').text("Search Specimen");
		$('#specimenModalBody').load('views/admin/view_specimen.html',function(){
		});
		$('#mySpecimenModal').modal();
	});
	$("#txtSpecimenId").focusin(function(){
		labclear();
		
	});
	$('#btnSubmit').click(function(){
		saveResult();
	});
	$('#btnReset').click(function(){
		$('#txtSpecimenDescription').val("");
		$('#txtDrawnDate').val("");
		$('#txtLabTest').val("");
		$('#txtNormalRange').val("");
		$('#txtResult').val("");
		$('#txtReport').val("");
		$("#txtSpecimenId").removeClass("errorStyle");
		$("#txtSpecimenIdError").text("");
		labclear();
		otableLabComponent.fnClearTable();
		$('#txtSpecimenId').focus();
	});
	
	$('#txtSpecimenId').keypress(function (e) {
        if (e.which == 13) {
            $("#btnSpecimenLoad").click();
        }
    });
	
});
function saveResult(){
	var flag = false;
	if($("#txtSpecimenId").val().trim()== "")
	{
		$("#txtSpecimenIdError").show();
		$("#txtSpecimenIdError").text("Please enter specimen ID");
		$("#txtSpecimenId").addClass("errorStyle");
		$('#txtSpecimenId').focus();
		labclear();
		flag = true;
	}
	if($("#txtDrawnDate").val().trim()== "")
	{
		$("#txtSpecimenIdError").show();
		$("#txtSpecimenIdError").text("Please select load button");
		
		flag = true;
	}
	if(flag == true){
		return false;
	}
	else{
		var tableData = []; 
		
		var specimenId = $("#txtSpecimenId").val();
        var report = $("#txtReport").val();
		var aiRows = otableLabComponent.fnGetNodes();
		for(i=0;i<aiRows.length;i++){
			var ObjExtra = {};					
			ObjExtra.normalRange = $($($(aiRows).find('input')[i]).parent().siblings()[1]).text();
			ObjExtra.result = $($(aiRows).find('input')[i]).val();
			if (ObjExtra.result == '') {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("");
				$('.modal-body').html('');
				$('#messageMyModalLabel').text("Alert ");
				$('.modal-body').html('Result can\'t be null.');	
				return false;		
			}
			ObjExtra.componentId = $($(aiRows).find('input')[i]).attr('data-id');
			tableData.push(ObjExtra);
		}
        var postData = {
            "operation": "save",
            "specimenId": specimenId,
            "report": report,
			"tableData":JSON.stringify(tableData)
        }

        $('#confirmUpdateModalLabel').text();
	    $('#updateBody').text("Are you sure that you want to save this?");
	    $('#confirmUpdateModal').modal();
	    $("#btnConfirm").unbind();
	    $("#btnConfirm").click(function(){
	        $.ajax({
	            type: "POST",
	            cache: false,
	            url: "controllers/admin/lab_result.php",
	            datatype: "json",
	            data: postData,

	            success: function(data) {		
					if(data != ""){
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').html('Lab result saved successfully!!!');
						$('#messagemyModal').modal();
						otableLabComponent.fnClearTable();
						labclear();
						$('#txtSpecimenId').focus();
					}
				},
				
				error:function() {
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
			});
	    });
	}
}
//function to load Specimen Results
function loadSpecimenResults(){
	var flag = false;
		if($("#txtSpecimenId").val().trim()== "")
		{
			$("#txtSpecimenIdError").show();
			$("#txtSpecimenIdError").text("Please enter specimen ID");
			$("#txtSpecimenId").addClass("errorStyle");
			$("#txtSpecimenId").focus();
			labclear();
			flag = true;
		}
		if(flag == true){
			return false;
		}
		else{
			var specimenId =$("#txtSpecimenId").val();
			var requestId = $('#txtSpecimenId').attr("data-requestId");
			var postData = {
				"operation":"loadSpecimen",
				"specimenId":specimenId,
				"requestId":requestId
			}
			
			$.ajax({					
				type: "POST",
				cache: false,
				url: "controllers/admin/lab_result.php",
				datatype:"json",
				data: postData,
				
				success: function(data) {
					if(data != "0" && data != ""){
						otableLabComponent.fnClearTable();
						var parseData = jQuery.parseJSON(data);
						if(parseData == ""){
							$("#txtSpecimenIdError").show();
							$("#txtSpecimenIdError").text("Please enter a valid specimen ID");
							$("#txtSpecimenId").addClass("errorStyle");
							$('#txtSpecimenId').focus();
							return false;
						}
						for(var i= 0; i<parseData.length;i++){
							$('#txtSpecimenDescription').val(parseData[i]['description']);						
							$('#txtDrawnDate').val(convertTimestamp(parseData[i]['created_on']));
							$('#txtLabTest').val(parseData[i]['name']);					
							
							$('#txtResult').val(parseData[i]['result']);
							if(parseData[i]['result_flag'] != "" && parseData[i]['result_flag'] != null)
							{
								$('#ddlResultFlag option:contains(' + parseData[i]['result_flag'] + ')').attr('selected', 'selected');							
							}
							else{
								$('#ddlResultFlag option:contains("N/A")').attr('selected', 'selected');
							}
							if(parseData[i]['report'] != "" && parseData[i]['report'] != null)
							{
								$('#txtReport').val(parseData[i]['report']);
							}
							else{
								$('#txtReport').val("");
							}	
							var normalRange= parseData[i]['normal_range'];
							var componentName= parseData[i]['component_name'];
							var componentId= parseData[i]['component_id'];

							otableLabComponent.fnAddData([componentName,normalRange,"<input type='text' style='width:100%' data-id ="+componentId+" name='tblTestResult' class='tblTest' id='tblTestResult'>"]);
						}						
					}
					if(data == ""){
						$("#txtSpecimenIdError").show();
						$("#txtSpecimenIdError").text("Please enter a valid specimen ID");
						$("#txtSpecimenId").addClass("errorStyle");
						$('#txtSpecimenId').focus();
					}
				},
				error:function() {
					alert('error');
				}
			});
		}
}
// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		 $(".close").click();
    }
});
function convertTimestamp(timestamp) {
  var d = new Date(timestamp * 1000),	// Convert the passed timestamp to milliseconds
		yyyy = d.getFullYear(),
		mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
		dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
		hh = d.getHours(),
		h = hh,
		min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
		ampm = 'AM',
		time;
			
	if (hh > 12) {
		h = hh - 12;
		ampm = 'PM';
	} else if (hh === 12) {
		h = 12;
		ampm = 'PM';
	} else if (hh == 0) {
		h = 12;
	}
	
	// ie: 2013-02-18, 8:35 AM	
	time = yyyy + '-' + mm + '-' + dd + ' ' + h + ':' + min + ' ' + ampm;
		
	return time;
}
function labclear(){

	$('#labResultForm')[0].reset();
	$('#ddlResultFlag option:contains("N/A")').attr('selected', 'selected');
	$('#txtSpecimenDescription').val("");
	$('#txtDrawnDate').val("");
	$('#txtLabTest').val("");
	$('#txtNormalRange').val("");
	$('#txtResult').val("");
	$('#txtReport').val("");	
}