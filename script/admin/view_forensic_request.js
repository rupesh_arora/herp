var otable;
$(document).ready(function() {	
	loader();
    var flag = 0;
    debugger;
	otable = $('#forensicRequestTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"aaSorting": [],
		"fnDrawCallback": function ( oSettings ) {
				
			$('.process').on('click',function(){
				clear();
				var data=$(this).parents('tr')[0];
				var mData = $('#forensicRequestTable').dataTable().fnGetData(data);
				if (null != mData)  // null if we clicked on title row
				{
					$('#myforensicModal').modal();
					if(mData["patient_id"] != "")
					{
						$('#txtPatientID').val(mData["patient_id"]);
					}
					$('#hdnforensicTestRequestId').val(mData["id"]);						
					$('#txtPatientName').text(mData["salutation"] + " " + mData["first_name"]+ " "+ mData['middle_name']+" " +  mData["last_name"]);
					if(mData["images_path"] != "")
					{							
						$('#imgPatient').attr("src",'./upload_images/patient_dp/'+mData["images_path"]);
					}
					else{
						$('#imgPatient').attr("src",'./img/default.jpg');
					}
					/* $('#txtSonOf').text(mData['middle_name']); */
					if(mData["gender"] == "M"){
						$('#txtGender').text("Male");
					}
					else{
						$('#txtGender').text("Female");
					}
					$('#txtDob').text(mData["dob"]);
					
					var dob= mData["dob"];
					var new_age = getAge(dob);
					var split = new_age.split(' ');
					var age_years = split[0];
					var age = parseInt(age_years)+1;
					$("#txtAge").text(age);
					
					/* $('#txtAge').text(mData["age"]); */						
					$('#txtEmail').text(mData["email"]);					
					$('#txtMobile').text(mData["mobile"]);					
										
					$('#txtSpecimenType').text(mData["type"]);	
					$('#txtLabTest').text(mData["name"]);			
					clear();
				}
				else{
					clear();
				}
			});

			if(flag == 1){
				var aiRows = otable.fnGetNodes(); //Get all rows of data table

			 	if(aiRows.length == 0){
			 		return false;
			 	}
			 	for (var j=0,c=aiRows.length; j<c; j++) {
			 		var visitType = $(aiRows[j]).find('td:last').find('input').attr('visitType');
			 		if(visitType == "Emergency"){
			 			$(aiRows[j]).css('background-color','#ff6666');
			 		}
			 	}
			}
			
		},
		
		"sAjaxSource":"controllers/admin/view_forensic_request.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{  
				"mData": function (o) { 	
				var visitId = o["visit_id"];
				var patientPrefix = o["visit_prefix"];
					
				var visitIdLength = visitId.length;
				for (var i=0;i<6-visitIdLength;i++) {
					visitId = "0"+visitId;
				}
				visitId = patientPrefix+visitId;
				return visitId; 
				}
			},
			{  
				"mData": function (o) { 	
				var patientId = o["patient_id"];
				var patientPrefix = o["patient_prefix"];
					
				var patientIdLength = patientId.length;
				for (var i=0;i<6-patientIdLength;i++) {
					patientId = "0"+patientId;
				}
				patientId = patientPrefix+patientId;
				flag = 1;
				return patientId; 
				}
			},
			/* { "mData": "visit_id" },
			{ "mData": "patient_id" }, */
			{ "mData": "test_code" },
			{ "mData": "name" },
			{ "mData": "cost" },
			{ "mData": "type" },
			{ "mData": "pay_status" },
			{  
				"mData": function (o) { 
					var visitType = o["visitType"];
					if(visitType == "Emergency Case"){					
						return "<input type='button' class='btn btn-small btn-primary process' visitType="+visitType+"  data-val="+o["id"]+" value='Process' />"; 
					}
					else{
						return "<input type='button' class='btn btn-small btn-primary process' data-val="+o["id"]+" value='Process' />"; 
					}
				}
			},	
		],
		
	   aoColumnDefs: [{
            aTargets: [7],
            bSortable: false
        }]
	} );
   
	
	$("#btnSubmit").click(function(){
		var lab_test_id = $('#hdnforensicTestRequestId').val();
		var specimen_description =  $('#txtSpecimenDescription').val();
		var postData = {
				"operation":"update",
				"lab_test_id":lab_test_id,
				"specimen_description":specimen_description
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/view_forensic_request.php",
			datatype:"json",
			data: postData,
			success: function(data) {
				if(data != "0" && data != ""){
					$('#pop_up_close').click()
					$('#messagemyModal #messageMyModalLabel').text("Success");
					$('#messagemyModal .modal-body').html('<span style ="color:#000 !important;"><b>Specimen Recorded Successfully !!!</b></span><br/><br/><label for="specimenId">Specimen Id : </label><span style ="color:#000 !important;" id="specimenId"><b>'+ data +'</b></span>');
					$("#forensicRequestTable").dataTable().fnReloadAjax();						
					$('#messagemyModal').modal();
					$('#txtSpecimenDescription').val("");
				}
			},
			error: function(){
				
			}
		});
	});
	
});

function bindSpecimen(){
	$.ajax({     
		type: "POST",
		cache: false,
		url: "controllers/admin/view_forensic_request.php",
		data: {
		"operation":"loadSpecimen"
		},
		success: function(data) { 
		    var html = '<option value="-1">Please select</option>';
			if(data != null && data != ""){
				data = jQuery.parseJSON(data);
				$.each(data,function(index,value){
					html += '<option value="'+value['value']+'">'+value['text']+'</option>';
				});
				$('#ddlSpecimenType').append(html);
			}			
		},
		error:function(){
		}
	});	
}
function bindLabTests(){
	$.ajax({     
		type: "POST",
		cache: false,
		url: "controllers/admin/view_forensic_request.php",
		data: {
		"operation":"loadLabTests"
		},
		success: function(data) { 
		    var html = '<option value="-1">Please select</option>';
			if(data != null && data != ""){
				data = jQuery.parseJSON(data);
				$.each(data,function(index,value){
					html += '<option value="'+value['value']+'">'+value['text']+'</option>';
				});
				$('#ddlLabTests').append(html);
			}			
		},
		error:function(){
		}
	});	
}

//function to calculate age
function getAge(dateString) {
    var now = new Date();
    var today = new Date(now.getYear(), now.getMonth(), now.getDate());

    var yearNow = now.getYear();
    var monthNow = now.getMonth();
    var dateNow = now.getDate();

    var dob = new Date(dateString.substring(0, 4), dateString.substring(5, 7) - 1, dateString.substring(8, 10));

    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();
    var age = {};
    var ageString = "";
    var yearString = "";
    var monthString = "";
    var dayString = "";


    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    age = {
        years: yearAge,
        months: monthAge,
        days: dateAge
    };

    if (age.years > 1) yearString = " years";
    else yearString = " year";
    if (age.months > 1) monthString = " months";
    else monthString = " month";
    if (age.days > 1) dayString = " days";
    else dayString = " day";


    if ((age.years > 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else ageString = "Oops! Could not calculate age!";

    return ageString;
}

function clear(){
	$.each($('label'),function(index,value) {		
		if($(this).text() == "")
		{
			$(this).text("Not Available");
		}
	});
}

//# sourceURL=filename.js