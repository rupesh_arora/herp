/*
 * File Name    :   allergies.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre opeartion	
 */
var allergyTable; //defile variable for allergyTable 
$(document).ready(function() {
	loader();
    
debugger;
    $('#btnReset').click(function() { // reset functioanlity on reset button
        $("#txtCategory").removeClass("errorStyle");
        $("#txtCode").removeClass("errorStyle");
        $("#txtName").removeClass("errorStyle");
        clear();
        $("#txtCode").focus();
    });

    $("#form_diseases").hide();

    $("#allergiesList").addClass('list');    
    $("#tabAllergies").addClass('tab-list-add');

   
    $("#tabAddAllergies").click(function() { // show the add allergies tab        
        showAddTab();
        clear();
    });

    $("#tabAllergies").click(function() { // show the allergies list tab
		showTableList();
    });

    // Ajax call for show data on allergies Category
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/allergies.php",
        data: {
            "operation": "showcategory"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].category + "</option>";
                }
                $('#txtCategory').html(option);
            }
        },
        error: function() {}
    });

    // save  details with validation
    $("#btnAllergy").click(function() {
        // check the validation        
        var flag = validation();

        if (flag == "true") {
            return false;
        } 
        else {
            var code = $("#txtCode").val().trim();
            var name = $("#txtName").val().trim();
            var category_id = $("#txtCategory").val();
            $.ajax({ // ajax call for allergy duplicacy validation
                type: "POST",
                cache: false,
                url: "controllers/admin/allergies.php",
                datatype: "json",
                data: {
                    Code: code,
                    Name: name,
                    category_Id: category_id,
                    Id: "",
                    operation: "checkdiseases"
                },
                success: function(data) {
                    if (data == "1") {
                        $("#txtCodeError").text("Code is already exists");
                        $("#txtCode").addClass("errorStyle");
                        $("#txtCode").focus();
                    } else {
                        //ajax call for insert data into data base
                        var postData = {
                            "operation": "save",
                            Code: code,
                            Name: name,
                            category_Id: category_id,
                        }
                        $.ajax({
                            type: "POST",
                            cache: false,
                            url: "controllers/admin/allergies.php",
                            datatype: "json",
                            data: postData,

                            success: function(data) {
                                if (data != "0" && data != "") {
                                    $('.modal-body').text("");
                                    $('#messageMyModalLabel').text("Success");
                                    $('.modal-body').text("Allergy saved successfully!!!");
                                    $('#messagemyModal').modal();
									showTableList();
                                }

                            },
                            error: function() {
								$('.modal-body').text("");
								$('#messageMyModalLabel').text("Error");
								$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
								$('#messagemyModal').modal();
							}
                        }); //  end ajax call
                    }
                },
                error: function() {
                    alert("data not found");
                }
            });
        }
    }); //end button click function

    // key up,change validation to remove validation style
    $("#txtCode").keyup(function() {
        if ($("#txtCode").val() != '') {
            $("#txtCodeError").text("");
            $("#txtCode").removeClass("errorStyle");
        }
    });
    $("#txtName").keyup(function() {
        if ($("#txtName").val() != '') {
            $("#txtNameError").text("");
            $("#txtName").removeClass("errorStyle");
        }
    });
    $("select").on('change', function() {

        if ($("#txtCategory").val() != "") {
            $("#txtCategoryError").text("");
            $("#txtCategory").removeClass("errorStyle");

        }
    });

    if ($('.inactive-checkbox').not(':checked')) { // show details onload
        //Datatable code
        loadDataTable();
    }
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show inactive  details on checked 
            allergyTable.fnClearTable();
            allergyTable.fnDestroy();
            allergyTable = "";
            allergyTable = $('#allergiesDataTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
					$('.restore').unbind();
                    $('.restore').on('click', function() { // prerform restore event
                        var data = $(this).parents('tr')[0];
                        var mData = allergyTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var category = mData["allergy_cat_id"];
                            restoreClick(id, category);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/allergies.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [{
                    "mData": "code"
                }, {
                    "mData": "name"
                }, {
                    "mData": "category"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                    }
                }, ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [3]
                }]
            });
        } else { // show active data on unchecked	
            allergyTable.fnClearTable();
            allergyTable.fnDestroy();
            allergyTable = "";
            loadDataTable();
        }
    });

});
// edit data dunction for update 
function editClick(id, code, name, category) {
    showAddTab();
    $("#uploadFile").hide();
    $("#btnReset").hide();
    $("#btnAllergy").hide();
    $('#tabAddAllergies').html("+Update Allergy");

    $("#btnUpdate").removeAttr("style");
    $("#txtCode").removeClass("errorStyle");
    $("#txtCodeError").text("");
    $("#txtName").removeClass("errorStyle");
    $("#txtNameError").text("");
    $("#txtCategory").removeClass("errorStyle");
    $("#txtCategoryError").text("");

    $('#txtCode').val(code);
    $('#txtName').val(name);
    $('#txtCategory').val(category);
    $('#selectedRow').val(id);

    
    //validation
    $("#btnUpdate").click(function() { // click update button
        var flag = validation();
        
        if (flag == "true") {
            return false;
        } 
		else {
            var id = $('#selectedRow').val();
            var code = $("#txtCode").val().trim();
            var name = $("#txtName").val().trim();
            var category_id = $("#txtCategory").val();			
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				$.ajax({ // ajax call for diseases duplicacy validation
					type: "POST",
					cache: false,
					url: "controllers/admin/allergies.php",
					datatype: "json",
					data: {
						Code: code,
						Name: name,
						category_Id: category_id,
						Id: id,
						operation: "checkdiseases"
					},
					success: function(data) {
						if (data == "1") {
							$("#txtCodeError").text("Code is already exists");
							$("#txtCode").addClass("errorStyle");
							$("#txtCode").focus();

						} else {
							var postData = {
								"operation": "update",
								Code: code,
								Name: name,
								category_Id: category_id,
								Id: id
							}
							$.ajax( //ajax call for update data
								{
									type: "POST",
									cache: false,
									url: "controllers/admin/allergies.php",
									datatype: "json",
									data: postData,

									success: function(data) {
										if (data != "0" && data != "") {
											$('#myModal').modal('hide');
											$('.close-confirm').click();
											$('.modal-body').text("");
											$('#messageMyModalLabel').text("Success");
											$('.modal-body').text("Allergy updated successfully!!!");
											$('#messagemyModal').modal();
											$("#tabAllergies").click();
										}
									},
									error: function() {
										$('.close-confirm').click();
										$('.modal-body').text("");
										$('#messageMyModalLabel').text("Error");
										$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
										$('#messagemyModal').modal();
									}
								}); // end of ajax
						}
					},
					error: function() {
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
				});
			});
        }
    });
} // end update button
function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Allergy");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);

    var type = "delete";
    $('#confirm').attr('onclick', 'deleteAllergy("' + type + '");');
} // end click fucntion
function restoreClick(id, category) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Allergy");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteAllergy("' + type + '",' + category + ');');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});

// clear function
function clear() {
    $('#txtCode').val("");
    $('#txtCodeError').text("");
    $('#txtCode').removeClass("errorStyle");
    $('#txtName').val("");
    $('#txtNameError').text("");
    $('#txtName').removeClass("errorStyle");
    $('#txtCategory').val("");
    $('#txtCategoryError').text("");
    $('#txtCategory').removeClass("errorStyle");
}

// use that function for call delete and restore operation
function deleteAllergy(type, category) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax( // ajax call for delete
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/allergies.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Allergy deleted successfully!!!");
                        $('#messagemyModal').modal();
                        allergyTable.fnReloadAjax();
                    } else {
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Sorry");
                        $('.modal-body').text("This diseases is used , so you can not delete!!!");
                        $('#messagemyModal').modal();
                    }
                },
                error: function() {					
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
            }); // end ajax 
    } else {
		if(category == undefined){
			$('.modal-body').text("");
			$('#messageMyModalLabel').text("Sorry");
			$('.modal-body').text("It can not be restore!!!");
			allergyTable.fnReloadAjax();
			$('#messagemyModal').modal();
		}
		else{
			var id = $('#selectedRow').val();
			$.ajax({
				type: "POST",
				cache: "false",
				url: "controllers/admin/allergies.php",
				data: {
					"operation": "restore",
					"id": id,
					"category": category
				},
				success: function(data) {
					if (data != "0" && data != "") {
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Allergy restored successfully!!!");
						$('#messagemyModal').modal();
						allergyTable.fnReloadAjax();

					}
					if (data == "0") {
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Sorry");
						$('.modal-body').text("It can not be restore!!!");
						allergyTable.fnReloadAjax();
						$('#messagemyModal').modal();

					}
				},
				error: function() {					
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
			});
		}        
    }
}

function loadDataTable(){
	allergyTable = $('#allergiesDataTable').dataTable({
		"bFilter": true,
		"processing": true,
		"sPaginationType": "full_numbers",
		"fnDrawCallback": function(oSettings) {
			// perform update event
			$('.update').unbind();
			$('.update').on('click', function() {
				var data = $(this).parents('tr')[0];
				var mData = allergyTable.fnGetData(data);
				if (null != mData) // null if we clicked on title row
				{
					var id = mData["id"];
					var code = mData["code"];
					var name = mData["name"];
					var category = mData["allergy_cat_id"];
					editClick(id, code, name, category);

				}
			});
			// perform delete event
			$('.delete').unbind();
			$('.delete').on('click', function() {
				var data = $(this).parents('tr')[0];
				var mData = allergyTable.fnGetData(data);

				if (null != mData) // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);
				}
			});
		},
		"sAjaxSource": "controllers/admin/allergies.php",
		"fnServerParams": function(aoData) {
			aoData.push({
				"name": "operation",
				"value": "show"
			});
		},
		"aoColumns": [{
			"mData": "code"
		}, {
			"mData": "name"
		}, {
			"mData": "category"
		}, {
			"mData": function(o) {
				var data = o;
				return '<i class="ui-tooltip fa fa-pencil update" style="font-size: 22px;cursor:pointer;" data-original-title="Edit"></i> <i class="ui-tooltip fa fa-trash-o delete" style="font-size: 22px; color:#a94442; cursor:pointer;" data-original-title="Delete"></i>';
			}
		}, ],
		aoColumnDefs: [{
			'bSortable': false,
			'aTargets': [3]
		}]

	});
}

function showTableList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#form_diseases").hide();
    $(".blackborder").show();
    $("#tabAddAllergies").removeClass('tab-detail-add');
    $("#tabAddAllergies").addClass('tab-detail-remove');
    $("#tabAllergies").removeClass('tab-list-remove');    
    $("#tabAllergies").addClass('tab-list-add');
    $("#department_list").addClass('list');


    $("#uploadFile").show();
    $("#btnReset").show();
    $("#btnAllergy").show();
    $('#btnUpdate').hide();
    $('#tabAddAllergies').html("+Add Allergy");
    clear();   
    
}

function showAddTab(){
    $("#form_diseases").show();
    $(".blackborder").hide();

    $("#tabAddAllergies").addClass('tab-detail-add');
    $("#tabAddAllergies").removeClass('tab-detail-remove');
    $("#tabAllergies").removeClass('tab-list-add');
    $("#tabAllergies").addClass('tab-list-remove');
    $("#allergiesList").addClass('list');

   
    $("#txtCategoryError").text("");
    $("#txtCategory").removeClass("errorStyle");
    $("#txtCodeError").text("");
    $("#txtCode").removeClass("errorStyle");
    $("#txtNameError").text("");
    $("#txtName").removeClass("errorStyle");
    $("#txtCode").focus();
}

function validation(){
    var flag = "false";
    if ($("#txtCategory").val() == '') {
        $("#txtCategoryError").text("Please select category");
        $("#txtCategory").focus();
        $("#txtCategory").addClass("errorStyle");
        flag = "true";
    }
    if ($("#txtName").val().trim() == '') {
        $("#txtNameError").text("Please enter allergy name");
        $("#txtName").focus();
        $("#txtName").addClass("errorStyle");
        flag = "true";
    }
    if ($("#txtCode").val().trim() == '') {
        $("#txtCodeError").text("Please enter code");
        $("#txtCode").focus();
        $("#txtCode").addClass("errorStyle");
        flag = "true";
    }
    return flag;
}