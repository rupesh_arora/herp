/*
 * File Name    :   bed_transfer.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   27th feb, 2016
 * Description  :   This page use for load,save,update,delete,resotre operation
 */
$("document").ready(function(){
	debugger;
	//Calling function for binding the department
	bindDepartment();
	$('#textIPDId').focus();
	//Change event of department
	$("#selDepartment").change(function(){
		var htmlRoom ="<option value='-1'>-- Select --</option>";
		
		if ($('#selDepartment :selected').val() != -1) {
			var value = $('#selDepartment :selected').val();
			//Calling function for binding the ward
			bindWard(value, null);
		}
		else {
			$('#selWard').html(htmlRoom);
		}
	});

	//Change event of ward
	$("#selWard").change(function(){
		var htmlRoom ="<option value='-1'>-- Select --</option>";
		
		if ($('#selWard :selected').val() != -1) {
			var value = $('#selWard :selected').val();
			//Calling function for binding the room
			bindRoomType(value, null);
		}
		else {
			$('#selRoomType').html(htmlRoom);
		}
	});

	//Change event of room
	$("#selRoomType").change(function(){
		var htmlRoom ="<option value='-1'>-- Select --</option>";
		
		if ($('#selRoomType :selected').val() != -1) {
			var value = $('#selRoomType :selected').val();
			//Open modal and load bed avalability screen
			$("#hdnRoom").val(value); // Send value of room in hidden field
	        $('#BedModalLabel').text("Show Bed Details");
	        $('#bedModalBody').load('views/admin/bed_availability.html');
	        $('#myBedModal').modal();
		}
		else {
			$('#selRoomType').html(htmlRoom);
		}
	});

	//Click event for search icon
	$("#serachIcon").click(function(){
		//Open modal and load view inpatient screen
		$("#hdnBedTransfer").val("1");
        $('#BedTransferModalLabel').text("Search IPD");
        $('#BedTransferModalBody').load('views/admin/view_inpatients.html');
        $('#myBedTransferModal').modal();
	});


	//Click event for load button
	$("#btnLoad").click(function(){
		var flag = "false";

		if($("#textIPDId").val() == ""){
			$("#textIPDId").focus();
			$("#textIPDIdError").text("Please enter ipd id");
			$("#textIPDId").addClass("errorStyle");       
            flag = "true";
		}

		if(flag == "true"){
			return false;
		}

		var ipdId = $("#textIPDId").val();
		var ipdPrefix = ipdId.substring(0, 3);
		ipdId = ipdId.replace ( /[^\d.]/g, '' ); 
		ipdId = parseInt(ipdId);
		if(isNaN(ipdId)){
			ipdId="";
		}
		postData = {
			"operation" : "showData",
			"ipdId" : ipdId
		}

		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/bed_transfer.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					//Set value in their field 
					var parseData= jQuery.parseJSON(data);
					//$("#textIPDId").val(parseData[0].id);
					$("#textIPDId").attr("disabled","disabled");
					$("#selDepartment").val(parseData[0].department_id);
					var ward_id = parseData[0].ward_id;					
					var room_id = parseData[0].room_id;					
					bindWard(parseData[0].department_id, ward_id);
					bindRoomType(ward_id,room_id);
					$("#txtRoomNo").val(parseData[0].number);
					$("#txtBedNo").val(parseData[0].bedName);
					$("#hdnBesHistoryId").val(parseData[0].bed_history_id);
					$("#txtPrice").val(parseData[0].price);
					$("#txtBedNo").attr('data-id',parseData[0].bed_id);
					$("#hdnPatientId").val(parseData[0].patient_id);
					if(parseData[0].is_icu == "YES"){
						$('#checkICU').prop("checked",true);
					}
					else{
						$('#checkICU').prop("checked",false);
					}
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	});


	//Click event for bed transfer button
	$("#btnTransfer").click(function(){

		 if ($('#checkICU').is(":checked")==false) {
            $('#checkICU').val(0);
        }
        else{
            $('#checkICU').val(1);
        }

		var flag = "false";

		if($("#txtReasonTransfer").val() == ""){
			$("#txtReasonTransfer").focus();
			$("#txtReasonTransferError").text("Please enter transfer reason");
			$("#txtReasonTransfer").addClass("errorStyle");       
            flag = "true";
		}

		if($("#selRoomType").val() == "-1"){
			$("#selRoomType").focus();
			$("#selRoomTypeError").text("Please select room type");
			$("#selRoomType").addClass("errorStyle");       
            flag = "true";
		}

		if($("#selWard").val() == "-1"){
			$("#selWard").focus();
			$("#selWardError").text("Please select ward");
			$("#selWard").addClass("errorStyle");       
            flag = "true";
		}

		if($("#selDepartment").val() == "-1"){
			$("#selDepartment").focus();
			$("#selDepartmentError").text("Please select department");
			$("#selDepartment").addClass("errorStyle");       
            flag = "true";
		}

		if($("#textIPDId").val() == ""){
			$("#textIPDId").focus();
			$("#textIPDIdError").text("Please enter ipd id");
			$("#textIPDId").addClass("errorStyle");       
            flag = "true";
		}

		if(flag == "true"){
			return false;
		}

		//Getting the value 
		var ipdId = $("#textIPDId").val();
		var ipdPrefix = ipdId.substring(0, 3);
		ipdId = ipdId.replace ( /[^\d.]/g, '' ); 
		ipdId = parseInt(ipdId);
		if(isNaN(ipdId)){
			ipdId="";
		}
		var roomNo = $("#txtRoomNo").val();
		var bedNo = $("#txtBedNo").val();
		var price = $("#txtPrice").val();
		var transferReason = $("#txtReasonTransfer").val();
		var bedId = $("#txtBedNo").attr("data-id");
		var ICU = $("#checkICU").val();
		var id = $("#hdnBesHistoryId").val();
		var oldPrice = $("#hdnOldPrice").val();
		var patientId = $("#hdnPatientId").val();

		postData = {
			"operation" : "update",
			"transferReason" : transferReason,
			"ICU" : ICU,
			"bedId" : bedId,
			"ipdId" : ipdId,
			"price" : price,
			"oldPrice" : oldPrice,
			"patientId" : patientId,
			"id" : id
		}

		//Ajax call
		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/bed_transfer.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != "" && data != "2"){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Bed transferred successfully!!!");
					$('#messagemyModal').modal();
					bindDepartment();
					clearBedDetails();
				}
				if(data == "2"){
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("Insufficient balance!!!");
					$('#messagemyModal').modal();
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
		
	});

	 //keyup functionality   
    $("#selRoomType").change(function() {
        if ($("#selRoomType").val() != "-1") {
            $("#selRoomTypeError").text("");
            $("#selRoomType").removeClass("errorStyle");
        }
    });

    $("#selDepartment").change(function() {
        if ($("#selDepartment").val() != "-1") {
            $("#selDepartmentError").text("");
            $("#selDepartment").removeClass("errorStyle");
        }
    });

    $("#selWard").change(function() {
        if ($("#selWard").val() != "-1") {
            $("#selWardError").text("");
            $("#selWard").removeClass("errorStyle");
        }
    });

    $("#txtReasonTransfer").keyup(function() {
        if ($("#txtReasonTransfer").val() != "-1") {
            $("#txtReasonTransferError").text("");
            $("#txtReasonTransfer").removeClass("errorStyle");
        }
    });

    $("#textIPDId").keyup(function() {
        if ($("#textIPDId").val() != "") {
            $("#textIPDIdError").text("");
            $("#textIPDId").removeClass("errorStyle");
        }
    });

    $("#textIPDId").change(function() {
        if ($("#textIPDId").val() != "-1") {
            $("#textIPDIdError").text("");
            $("#textIPDId").removeClass("errorStyle");
        }
    });

    //click event for reset button
    $("#btnResetBedTranserDetails").click(function(){
    	$("#textIPDId").val("");
    	$("#textIPDId").focus();
    	clearBedDetails(); //Calling the clear function 

    });

}); // end document.ready function

//
//function binding ward
//
function bindWard(value, ward_id){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/bed.php",
		data: {
			"operation":"showWard",
			departmentId : value
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>-- Select --</option>";
				for (var i=0;i<parseData.length;i++)
				{
				option+="<option value='"+parseData[i].id+"'>"+parseData[i].name+"</option>";
				}
				$('#selWard').html(option);
				if(ward_id != null){
					$("#selWard").val(ward_id);
				}							
			}
		},
		error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
	});
}

// define function for bind department
function bindDepartment() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/opd_registration.php",
        data: {
            "operation": "showDepartment"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].department + "</option>";
                }
                $('#selDepartment').html(option);
            }
        },
        error: function() {}
    });
}

// define function for bind room typr
function bindRoomType(value, room_id) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/bed.php",
        data: {
			"operation":"showRoom",
			"ward" : value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].room_number + "</option>";
                }
                $('#selRoomType').html(option);
                if(room_id != null){
                	$("#selRoomType").val(room_id);
                }				
            }
        },
       error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
    });
}

// define function for clear the field
function clearBedDetails(){
	$('input[type=text]').next("span").text("");
	$('input[type=text]').val("");
	$('input[type=text]').removeClass("errorStyle");
	$('select').next("span").text("");
	$('#txtReasonTransfer').val("");
	$('#txtReasonTransferError').text("");
	$('select').val("-1");
	$('select').removeClass("errorStyle");
	$('#txtReasonTransfer').removeClass("errorStyle");
	$('#checkICU').attr('checked',false);
	$("#textIPDId").removeAttr("disabled");
}