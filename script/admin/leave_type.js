var leaveTypeTable; //defing a global variable for datatable
$(document).ready(function() {
	loader();    
    debugger;    

    $("#form_dept").hide();
    $("#designation_list").addClass('list');    
    $("#tab_dept").addClass('tab-list-add');

    $("#tab_add_dept").click(function() { // click on add designation tab
        showAddTab();
        clearFormDetails(".clearForm");        
    });
    $("#tab_dept").click(function() { // click on list designation tab
		showTableList();
    });

    $("#btnSaveLeaveType").click(function() {
    	var flag = 'false';
    	if(validTextField('#txtLeaveTypeName','#txtLeaveTypeNameError','Please enter leave type') == true)
		{
			flag = true;
		}
		if (flag == true) {
			return false;
		}
		
        var leaveTypeName = $("#txtLeaveTypeName").val().trim();
        var description = $("#txtDescription").val().trim();
        description = description.replace(/'/g, "&#39");
        var postData = {
            "operation": "save",
            "leaveTypeName": leaveTypeName,
            "description" : description
        }        
        dataCall("controllers/admin/leave_type.php", postData, function (result){
        	if (result.trim() == 1) {
        		callSuccessPopUp('Success','Saved sucessfully!!!');
        		clearFormDetails(".clearForm");
        		showTableList();
        	}
            else if (result.trim() == 0) {
                $('#txtLeaveTypeName').focus();
                $('#txtLeaveTypeNameError').text("Leave type already exist");
                $('#txtLeaveTypeName').addClass('errorStyle');
            }
        	else{
        		callSuccessPopUp('Error','Try to refresh page once');
        		clearFormDetails(".clearForm");
        	}
        });
    });

    //load Datatable data on page load
    if ($('.inactive-checkbox').not(':checked')) {
        leaveTypeTable = $('#leaveTypeTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "fnDrawCallback": function(oSettings) {
				$('.update').unbind();
                $('.update').on('click', function() { // update click event 
                    var data = $(this).parents('tr')[0];
                    var mData = leaveTypeTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var name = mData["name"];
                        var description = mData["description"];
                        editClick(id, name, description);

                    }
                });
				$('.delete').unbind();
                $('.delete').on('click', function() { // delete click event
                    var data = $(this).parents('tr')[0];
                    var mData = leaveTypeTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/leave_type.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [{
                "mData": "name"
            }, {
                "mData": "description"
            }, {
                "mData": function(o) {
                    var data = o;
                    return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                        " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                        " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                        " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                        "  data-original-title='Delete'></i>";
                }
            }, ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [1,2]
            }]
        });
    }
    //checked or not checked functionality to show active or inactive data
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) {
            leaveTypeTable.fnClearTable();
            leaveTypeTable.fnDestroy();
            leaveTypeTable = "";
            leaveTypeTable = $('#leaveTypeTable').dataTable({
                "bFilter": true,
                "processing": true,
				
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
					$('.restore').unbind();
                    $('.restore').on('click', function() { // restor click event
                        var data = $(this).parents('tr')[0];
                        var mData = leaveTypeTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            restoreClick(id);
                        }
                    });
                },
                "sAjaxSource": "controllers/admin/leave_type.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [{
                    "mData": "name"
                },{
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                    }
                }, ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1,2]
                }]
            });
        } else {
            leaveTypeTable.fnClearTable();
            leaveTypeTable.fnDestroy();
            leaveTypeTable = "";
            leaveTypeTable = $('#leaveTypeTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
					$('.update').unbind();
                    $('.update').on('click', function() { // for update click event
                        var data = $(this).parents('tr')[0];
                        var mData = leaveTypeTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var name = mData["name"];
                            var description = mData["description"];
                            editClick(id, name, description);
                        }
                    });
					$('.delete').unbind();
                    $('.delete').on('click', function() { // for delete click event
                        var data = $(this).parents('tr')[0];
                        var mData = leaveTypeTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },
                "sAjaxSource": "controllers/admin/leave_type.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [{
                    "mData": "name"
                },{
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;

                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            "  data-original-title='Delete'></i>";
                    }
                }, ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1,2]
                }]
            });
        }
    });


    $("#btnReset").click(function(){
	    clearFormDetails(".clearForm");
        $("#txtLeaveTypeName").focus();
    });

});

// use fucntion for edit designation for update 
function editClick(id, name, description) {
    showAddTab();
    $('#tab_add_dept').html("+Update Leave Type");
    $("#uploadFile").hide();
    $("#btnReset").hide();
    $("#btnAddDesignation").hide();
    $('#btnUpdateLeavePeriod').removeAttr("style");
    $("#txtLeaveTypeNameError").text("");
    $("#txtLeaveTypeName").removeClass("errorStyle");
    $('#txtLeaveTypeName').val(name);
    $('#txtDescription').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
    $(".btnContainer").hide();
    //keyup
    removeErrorMessage();
    $("#btnUpdateLeavePeriod").click(function() {
        var flag = false;
        if(validTextField('#txtLeaveTypeName','#txtLeaveTypeNameError','Please enter leave type') == true)
		{
			flag = true;
		}
		
        if (flag == true) {
            return false;
        }
		else{
	        var leaveTypeName = $("#txtLeaveTypeName").val().trim();
			var description = $("#txtDescription").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();
			var postData = {
	            "operation": "update",
	            "leaveTypeName": leaveTypeName,
	            "description" : description,
	            "id" : id
	        }  
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				dataCall("controllers/admin/leave_type.php", postData, function (result){
					if (result != null && result != "" && result !="0") {
						$(".close-modal").click();
						callSuccessPopUp('Success','Updated sucessfully!!!');
        				clearFormDetails(".clearForm");
                        showTableList();
					}
                    if (result =="0") {
                        $('#txtLeaveTypeNameError').text("Leave type already exist");
                        $('#txtLeaveTypeName').addClass('errorStyle');
                        $("#txtLeaveTypeName").focus();
                    }
				});
			});
		}
    });
}
// click function to delete designation
function deleteClick(id) {
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Delete leave type");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteLeaveType("' + type + '");');
}

//Click function for restore the departement
function restoreClick(id) {
    $('#selectedRow').val(id);
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Restore holiday");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteLeaveType("' + type + '");');
}
//function for delete and restore the designationtList
function deleteLeaveType(type) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id" : id
        }
        dataCall("controllers/admin/leave_type.php", postData, function (result){
			if (result != null && result != "" && result != "0") {
				callSuccessPopUp('Success','Leave type deleted sucessfully!!!');
				leaveTypeTable.fnReloadAjax();
			}
			else {
                $('.modal-body').text("");
                callSuccessPopUp('Sorry','This leave type is in used ,so you can not delete it.');
            }
		});         
    } 
    else {
        var id = $('#selectedRow').val();
        $('.modal-body').text("");
        var postData = {
            "operation": "restore",
            "id" : id
        }
        dataCall("controllers/admin/leave_type.php", postData, function (result){
			if (result != null && result != "") {
				callSuccessPopUp('Success','Leave type restored sucessfully!!!');
				leaveTypeTable.fnReloadAjax();
			}
		});        
    }
}

function showTableList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#form_dept").hide();
    $(".blackborder").show();

    $("#tab_add_dept").removeClass('tab-detail-add');
    $("#tab_add_dept").addClass('tab-detail-remove');
    $("#tab_dept").removeClass('tab-list-remove');    
    $("#tab_dept").addClass('tab-list-add');
    $("#department_list").addClass('list');    
    $("#btnReset").show();
    $("#btnAddDesignation").show();
    $('#btnUpdateDesignation').hide();
    $('#tab_add_dept').html("+Add Leave Type");
    clear();
}

function showAddTab(){
    $("#form_dept").show();
    $(".blackborder").hide();
   
    $("#tab_add_dept").addClass('tab-detail-add');
    $("#tab_add_dept").removeClass('tab-detail-remove');
    $("#tab_dept").removeClass('tab-list-add');
    $("#tab_dept").addClass('tab-list-remove');
    $("#designation_list").addClass('list');
    $('#txtLeaveTypeName').focus();
    removeErrorMessage();
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});