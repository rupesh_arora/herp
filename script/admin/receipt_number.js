var receiptNumberTable;
var tableOpenAmount = [];
var k = 0;
var getUserCredit = '';
var matchUserCredit = '';
$(document).ready(function(){
	debugger;
	k=0;
	var totalamount = 0;
	var openAmount = 0;
	removeErrorMessage();
	loadLedger();
	
	$(".input-datepicker").datepicker();

	receiptNumberTable = $('#receiptNumberTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth":false,
        "fnDrawCallback": function(oSettings) {
        	$(".chkProduct").unbind();
        	$(".chkProduct").on('change', function(){
        		var id = $(this).attr('data-id');
        		var amount = $($(this).parent().siblings()[2]).text();
        		var parentClass = $(this).attr('data-class'); 
        		var trClass = $(this).attr('tr-class');
        		if($(this).is(":checked")){
        			var m = false;
        			
    				var chkBox = $(this).parent().parent().parent().find("."+trClass);
    				for(n=0; n<chkBox.length; n++){
    					if(!$($(this).parent().parent().parent().find('.'+trClass).find('.chkProduct')[n]).prop('checked')){
    						m = true;
    					}
    				}
    				if(m == false){
    					$(this).parent().parent().parent().find("."+parentClass).prop('checked',true);
    				}
    			
					totalamount = parseFloat(totalamount) + parseFloat(amount);
					$("#txtAmount").val(totalamount);
					$("#txtCreditAmount").val(totalamount);
				}							
				else{
					$(this).parent().parent().parent().find("."+parentClass).prop('checked',false);
					totalamount = parseFloat(totalamount) - parseFloat(amount);
					$("#txtAmount").val(totalamount);
					$("#txtCreditAmount").val(totalamount);
				}
        	});
        	$(".chkInvoice").unbind();
        	$(".chkInvoice").on('change', function(){
        		var id = $(this).attr('data-id');
        		var subClass = $($(this).parent().siblings()[0]).text();
        		var amount = $($(this).parent().siblings()[3]).text();
        		if($(this).is(":checked")){
	        		var chkBox = $(this).parent().parent().parent().find('.'+subClass).find('input[type="checkbox"]');
	        		for(n=0; n<chkBox.length; n++){
    					if(!$($(this).parent().parent().parent().find('.'+subClass).find('input[type="checkbox"]')[n]).prop('checked')){
    						totalamount = parseFloat(totalamount) + parseFloat($($(chkBox[n]).parent().siblings()[3]).text());
    					}
    				}
	        		$(this).parent().parent().parent().find('.'+subClass).find('input[type="checkbox"]').prop('checked',true);	        		
	        		
					$("#txtAmount").val(totalamount);
					$("#txtCreditAmount").val(totalamount);
				}
				else{
					$(this).parent().parent().parent().find('.'+subClass).find('input[type="checkbox"]').prop('checked',false);	        		
	        		totalamount = parseFloat(totalamount) - parseFloat(amount);
					$("#txtAmount").val(totalamount);
					$("#txtCreditAmount").val(totalamount);
				}

        	});        	
        	
        },
    });

    $("#txtCustomerId").focus();

    $("#btnShowCustomerDetails").click(function(){
    	var flag = false;
    	k=0;

        if(validTextField('#txtCustomerId','#txtCustomerIdError','Please enter customer Id') == true)
        {
            flag = true;
        }
        if (flag == true) {
        	return false;
        }

        var customerId = $("#txtCustomerId").val();            
	    if (customerId !='') {
	        if (customerId.length != 9) {
	        	$("#txtCustomerId").addClass("errorStyle");
	        	$("#txtCustomerId").focus();
	        	$("#txtCustomerIdError").text("Customer Id is not valid");
	            return false;
	        }
	        var getCustomerPrefix = customerId.substring(0, 3);
	        if (getCustomerPrefix.toLowerCase() != customerPrefix.toLowerCase()) {
	        	$("#txtCustomerId").addClass("errorStyle");
	        	$("#txtCustomerId").focus();
	        	$("#txtCustomerIdError").text("Customer Id is not valid");
	            return false;
	        }
	        customerId = customerId.replace ( /[^\d.]/g, '' ); 
	        customerId = parseInt(customerId);
	    }

	    var postData = {
			"operation" : "showCustomerDetails",
			customerId : customerId
		}

		dataCall("controllers/admin/receipt_number.php", postData, function (result){
			var parseData = JSON.parse(result);
			if (parseData != '') {
				var grandTotal =0;
				$("#lblFirstName").text(parseData[0].first_name);
				$("#lblLastName").text(parseData[0].last_name);
				$("#lblEmail").text(parseData[0].email);
				$("#lblPhone").text(parseData[0].phone);
				if (parseData[0].phone == '' || parseData[0].phone == null) {
					$("#lblPhone").text('N/A');
				}	

				$("#txtCreditAmount").val("0");
				$("#txtAmount").val("0");			
				receiptNumberTable.fnClearTable();
				var id = '';
				var receiptNumber;
				totalamount = 0;
				openAmount = 0;
				tableOpenAmount = [];
				getUserCredit = parseData[0].balance;

				if (getUserCredit !='' && getUserCredit !=null) {
					$("#txtCredit").val(getUserCredit);
					matchUserCredit = getUserCredit;
				}
				else{
					getUserCredit = '';
				}
				$.each(parseData,function(i,v){
					var obj = {};
					receiptNumber = v.id;
					var prefix = v.invoice_prefix;
					var idLength = 6-receiptNumber.length;
					for(j=0; j<idLength; j++){
						receiptNumber="0"+receiptNumber;
					}
					receiptNumber=prefix+receiptNumber;
		            if (v.id !=id) {
		            	
		                receiptNumberTable.fnAddData(["<input type='checkbox'  class='chkInvoice chkInvoice"+v.id+"' data-id="+v.id+" style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;'"+					 
									   " style='font-size: 22px; cursor:pointer;'>"+"<span class='details-control' onclick='showRows($(this));'></span>"+"<span class='hide-details-control hide' onclick='hideRows($(this));'></span>",receiptNumber,v.description,v.total_bill,v.total_bill,v.billing_date]);

		                var currData = receiptNumberTable.fnAddData(["<input type='checkbox' tr-class="+receiptNumber+" data-class='chkInvoice"+v.id+"'  class='chkProduct' data-id="+v.id+"  productId="+v.customer_product_id+"  style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative; margin-left: 42%;'"+					 
									   " style='font-size: 22px; cursor:pointer;'>",v.name,"",v.total,v.total,v.billing_date]);

		                var getCurrDataRow = receiptNumberTable.fnSettings().aoData[ currData ].nTr;
		                $(getCurrDataRow).addClass('hide '+(receiptNumber).replace(/ /g,''));
		                id = v.id;
		                grandTotal += parseFloat(v.total);
		                if(i != 0){
		                	obj.openAmount = openAmount;
		                	tableOpenAmount.push(obj);
		                	openAmount =0;
		                }
		                openAmount += parseFloat(v.total);
		            }

		            else {		            	

		                var currData = receiptNumberTable.fnAddData(["<input type='checkbox' tr-class="+receiptNumber+" data-class='chkInvoice"+v.id+"' class='chkProduct' data-id="+v.id+" productId="+v.customer_product_id+"  style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;  margin-left: 42%;'"+					 
									   " style='font-size: 22px; cursor:pointer;'>",v.name,"",v.total,v.total,v.billing_date]);

		                var getCurrDataRow = receiptNumberTable.fnSettings().aoData[ currData ].nTr;
		                $(getCurrDataRow).addClass('hide '+(receiptNumber).replace(/ /g,''));
		                grandTotal += parseFloat(v.total);
		                 openAmount += parseFloat(v.total);
		                id = v.id;
		            }
		             if(i == parseData.length-1){
		             	obj = {};
	                	obj.openAmount = openAmount;
	                	tableOpenAmount.push(obj);
	                	openAmount =0;
						k=1;
	                }
		        });	
		        if(k == 1){
	        		var airows = receiptNumberTable.$(".chkInvoice:unchecked", {"page": "all"});	//Select all unchecked row only
		        	for(i=0; i<airows.length; i++){
		        		$($(airows[i]).parent().siblings()[3]).text(tableOpenAmount[i].openAmount);
		        	}
	        	}				
				$("#txtBalance").val(grandTotal);
			}
			else{
				callSuccessPopUp("Alert","No data exist for choosen id.");
				$("#lblFirstName").text("");
				$("#lblLastName").text("");
				$("#lblEmail").text("");
				$("#lblPhone").text('');
			}
		});
    });

	$("#btnSaveReceipt").click(function(){
		var flag = false;
		if($("#txtReference").val() ==""){
			$("#txtReference").focus();
			$("#txtReferenceError").text("Please enter reference");
			$("#txtReference").addClass("errorStyle");
			flag = true;
		}
		if($("#selLedger").val() ==""){
			$("#selLedger").focus();
			$("#selLedgerError").text("Please select ledger");
			$("#selLedger").addClass("errorStyle");
			flag = true;
		}

		if($("#txtCustomerId").val() ==""){
			$("#txtCustomerId").focus();
			$("#txtCustomerIdError").text("Please enter customer id");
			$("#txtCustomerId").addClass("errorStyle");
			flag = true;
		}
		
		if($("#selPaymentMode").val() ==""){
			$("#selPaymentMode").focus();
			$("#selPaymentModeError").text("Please select payment mode");
			$("#selPaymentMode").addClass("errorStyle");
			flag = true;
		}

		if(flag == true){
			return false;
		}
		var customerId = $("#txtCustomerId").val();
		customerId = customerId.replace ( /[^\d.]/g, '' ); 
        customerId = parseInt(customerId);

		var productDetail = [];
		var receiptDetail = [];
		
		var aiRows = receiptNumberTable.fnGetNodes(); //Get all rows of data table 		
		var rowcollection =  receiptNumberTable.$(".chkProduct:checked", {"page": "all"});	//Select all unchecked row only
		var lastId ='';
		var totalAmount =0;
		//Getting data from all unchecked rows 
		for (i=0,c=rowcollection.length; i<c; i++) {	
			var objProduct ={};
			var objReceipt ={};
			var invoiceId = $($(rowcollection)[i]).attr('data-id');
			if(i== 0){
				
				objProduct.productId = $($(rowcollection)[i]).attr('productid');
				totalAmount += parseFloat($($($(rowcollection)[i]).parent().siblings()[2]).text());
				productDetail.push(objProduct);
				lastId = invoiceId;
			}
			else if(invoiceId == lastId && i != 0){				
				objProduct.productId = $($(rowcollection)[i]).attr('productid');
				totalAmount += parseFloat($($($(rowcollection)[i]).parent().siblings()[2]).text());				
				productDetail.push(objProduct);
				lastId = invoiceId;
			}
			else{
				objReceipt.invoiceId = $($(rowcollection)[i-1]).attr('data-id');
				objReceipt.total = totalAmount;
				receiptDetail.push(objReceipt);
				totalAmount = parseFloat($($($(rowcollection)[i]).parent().siblings()[2]).text());
				objProduct.productId = $($(rowcollection)[i]).attr('productid');				
				productDetail.push(objProduct);
				lastId = invoiceId;
			}	
			if(i == c-1){
				objReceipt ={};
				objReceipt.invoiceId = $($(rowcollection)[i]).attr('data-id');
				objReceipt.total = totalAmount;
				receiptDetail.push(objReceipt);
			}

		}
		var ledgerId = $("#selLedger").val();
		var paymentMode = $("#selPaymentMode").val();
		var reference = $("#txtReference").val();
		var userCredit = 0;
		var txtCredit = $("#txtCredit").val();
		var txtAmount = parseFloat($("#txtCreditAmount").val());
		var txtInvoice = parseFloat($("#txtAmount").val());
		var calAmount = 0;

		if (txtCredit == '') {
			if (txtAmount < txtInvoice) {
				callSuccessPopUp('Alert',"Invoice should be equal or greater than amount.")
				return false;
			}
			else if (txtAmount >= txtInvoice) {
				userCredit = txtAmount - txtInvoice;
			}
		}
		else{
			txtCredit = parseFloat(txtCredit);
			if (txtInvoice > txtCredit) {
				calAmount = (txtCredit + txtAmount) - txtInvoice;
				if (calAmount == 0) {
					userCredit = 0;
				}
				else if (calAmount < 0) {
					callSuccessPopUp("Alert","Invoice should be equal or greater than amount.");
					return false;
				}
				else{
					userCredit = calAmount;
				}
			}

			else{
				calAmount = (txtCredit + txtAmount) - txtInvoice;
				userCredit = calAmount;
			}
		}
			
		//check some one changed credit then
		if(matchUserCredit != getUserCredit){
			callSuccessPopUp("Alert","Some one has changed your credits");
			return false;
		}

		var postData ={
			"operation":"save",				
			"receiptDetail":JSON.stringify(receiptDetail),
			"productDetail":JSON.stringify(productDetail),
			"reference":reference,
			"ledgerId":ledgerId,
			"customerId":customerId,
			"paymentMode" : paymentMode,
			"userCredit" : userCredit
		}

		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/receipt_number.php",
			datatype:"json",
			data: postData,
			success : function(data){
				if(data !="" && data == '1'){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Saved successfully !!!");
					$('#messagemyModal').modal();	
					receiptNumberTable.fnClearTable();
					clearMainDetail();
					clearFormDetails(".clearForm");
					matchUserCredit = '';
					getUserCredit = '';
				}
				else{
					callSuccessPopUp('Error','Please select atleast one invoice id');
				}
			},
			error : function(){
				$('.close-confirm').click();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	});

    $("#searchCustomerId").click(function(){
    	 clearFormDetails(".clearForm");
    	 clearMainDetail();
    	 receiptNumberTable.fnClearTable();
        $('#searchModalBody').load('views/admin/customer.html',function(){

        });
		$('#mySearchModal').modal();
    });

    $("#selLedger").change(function(){
		if ($(this).val() != '') {
			paymentMode($(this).val());
			$("#selLedger").removeClass('errorStyle');
			$("#selLedgerError").text('');			
		}
		if ($(this).val() == '') {
			$("#selPaymentMode").html("<option value =''>--Select--</option>");
		}
	});

	$("#selPaymentMode").change(function(){
		if ($(this).val() != '') {
			$("#selPaymentMode").removeClass('errorStyle');
			$("#selPaymentModeError").text('');
		}
	});

	$("#btnReceiptReset").click(function(){
    	 clearFormDetails(".clearForm");
    	 clearMainDetail();
    	 receiptNumberTable.fnClearTable();
	});
});

function showRows(myThis){
    var ledgerText = $(myThis).parent().siblings()[0];
    var ledgerClass = $(ledgerText).text().replace(/ /g,'');
    var parentTr = $(myThis).parent().parent()[0];
    var trClass = $(parentTr).parent().find("."+ledgerClass);
    $(parentTr).parent().find("."+ledgerClass).removeClass('hide');
    $(myThis).addClass('hide');
    $(myThis).siblings().removeClass('hide');
}
function hideRows(myThis){
    var ledgerText = $(myThis).parent().siblings()[0];
    var ledgerClass = $(ledgerText).text().replace(/ /g,'');
    var parentTr = $(myThis).parent().parent()[0];
    var trClass = $(parentTr).parent().find("."+ledgerClass);
    $(parentTr).parent().find("."+ledgerClass).addClass('hide');
    $(myThis).addClass('hide');
    $(myThis).siblings().removeClass('hide');
}

function paymentMode(ledgerId){
	var postData = {
		operation : "showPaymentMode",
		ledgerId : ledgerId
	}
	dataCall("controllers/admin/payment_voucher.php", postData, function (result){
		if (result !='') {
			var parseData = JSON.parse(result);
			var option = "<option value=''>--Select--</option>";
			$.each(parseData,function(i,v){
				option += "<option value='" + v.id + "'>" +v.name+"</option>";
			});
			$("#selPaymentMode").html(option);
		}
	});
}

function loadLedger(){
	var postData = {
		operation : "showLedger"
	}

	dataCall("controllers/admin/receipt_number.php", postData, function (result){
		var parseData = JSON.parse(result);
		var option = "<option value=''>--Select--</option>";
		if (parseData != '') {		
			for (var i=0;i<parseData.length;i++){				

				option += "<option value='" + parseData[i].id + "'>"  + parseData[i].name + "</option>";
			}
			$("#selLedger").html(option);
		}
		else{
			$("#selLedger").html(option);
		}
	});
}

function clearMainDetail(){
	$("#txtCreditAmount").val("0");
	$("#txtCustomerId").val("");
	$("#txtAmount").val("0");
	$("#selLedger").val("");
	$("#txtBalance").val("0");
	$("#txtCredit").val("");
	$("#txtReference").val("");
	$("#txtCustomerIdError").text("");
	$("#selLedgerError").text("");
	$("#lblFirstName").text("");
	$("#lblLastName").text("");
	$("#lblEmail").text("");
	$("#txtReferenceError").text("");
	$("#lblPhone").text("");
	$("#selLedger").removeClass("errorStyle");
	$("#txtCustomerId").removeClass("errorStyle");
	$("#txtReference").removeClass("errorStyle");
}