$(document).ready(function() {
	var otable;		

	loader();

	//
	//Calling the function for loading the lab type and test 
	//	
	bindTest();
	bindUnitMeasure();	
	
	$("#advanced-wizard").hide();
	 $("#radiologySubTestList").css({
            "background": "#fff"
        });
		$("#tabRadiologySubTestList").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
    $("#tabAddRadiologySubTest").click(function() {
        $("#advanced-wizard").show();
        $(".blackborder").hide();
        $("#txtSubRadiologyTestName").focus();
		clear();
        $("#tabAddRadiologySubTest").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tabRadiologySubTestList").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#radiologySubTestList").css({
            "background": "#fff"
        });		
		bindTest();
		bindUnitMeasure();
    });
    $("#tabRadiologySubTestList").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
        $("#advanced-wizard").hide();
        $(".blackborder").show();
		$("#radiologySubTable").dataTable().fnReloadAjax();
        $("#tabAddRadiologySubTest").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#tabRadiologySubTestList").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#radiologySubTestList").css({
            "background": "#fff"
        });
    });	


    ///
    //     check box is check then show data

    if($('.inactive-checkbox').not(':checked')){
    	otable = $('#radiologySubTable').dataTable( {
			"bFilter": true,
			"processing": true,
			"sPaginationType":"full_numbers",
			"fnDrawCallback": function ( oSettings ) {
				$('.update').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData = $('#radiologySubTable').dataTable().fnGetData(data);
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						var name = mData["name"];//this test name is off radiologysubtestname
						var test_name = mData["radiology_type_id"];//this test name is off radiologytestname
						var range = mData["range"];
						var unit_id = mData["unit_id"];
						var description = mData["description"];
						editClick(id,name,test_name,range,unit_id,description);
						
       
					}
				});
				$('.delete').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData =  $('#radiologySubTable').dataTable().fnGetData(data);
					
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						deleteClick(id);
					}
				});
			},
			
			"sAjaxSource":"controllers/admin/radiology_sub_test.php",
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "operation", "value": "show" });
			},
			"aoColumns": [
				{ "mData": "name"},
				{ "mData": "test_name" },
				{ "mData": "range" },
				{ "mData": "unit" },
				{ "mData": "description" },
				{
				  "mData": function (o) { 
					var data = o;
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
					   " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
					   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
					   " style='font-size: 22px; color:red; cursor:pointer;' "+
					   " data-original-title='Delete'></i>";
				  }
				},
			],
			aoColumnDefs: [
		        { 'bSortable': false, 'aTargets': [ 4 ] },
				{ 'bSortable': false, 'aTargets': [ 5 ] }
		    ]
		});
    }

    //show data active or inactive on check functionality in data table
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	otable.fnClearTable();
	    	otable.fnDestroy();
	    	otable="";
	    	otable = $('#radiologySubTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  $('#radiologySubTable').dataTable().fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var test_name = mData["name"];
							restoreClick(id,test_name);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/radiology_sub_test.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "checked" });
				},
				"aoColumns": [
					{ "mData": "name" },
					{ "mData": "test_name" },
					{ "mData": "range" },
					{ "mData": "unit" },
					{ "mData": "description" },
					{
					  "mData": function (o) { 
						var data = o;
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},
				],
				aoColumnDefs: [
		          	{ 'bSortable': false, 'aTargets': [ 4 ] },
					{ 'bSortable': false, 'aTargets': [ 5 ] }
		       	]
			});
		}
		else{
			otable.fnClearTable();
	    	otable.fnDestroy();
	    	otable="";
			otable = $('#radiologySubTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.update').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData = $('#radiologySubTable').dataTable().fnGetData(data);
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						var name = mData["name"];//this test name is off radiologysubtestname
						var test_name = mData["radiology_type_id"];//this test name is off radiologytestname
						var range = mData["range"];
						var unit_id = mData["unit_id"];
						var description = mData["description"];
						editClick(id,name,test_name,range,unit_id,description);
						
       
					}
				});
				$('.delete').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData =  $('#radiologySubTable').dataTable().fnGetData(data);
					
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						deleteClick(id);
					}
				});
				},
				
				"sAjaxSource":"controllers/admin/radiology_sub_test.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "show" });
				},
				"aoColumns": [
					{ "mData": "name"},
					{ "mData": "test_name" },
					{ "mData": "range" },
					{ "mData": "unit" },
					{ "mData": "description" },
					{
					  "mData": function (o) { 
						var data = o;
						return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
						   " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
						   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
						   " style='font-size: 22px; color:red; cursor:pointer;' "+
						   " data-original-title='Delete'></i>";
					  }
						
					},
				],
				aoColumnDefs: [
		          	{ 'bSortable': false, 'aTargets': [ 4 ] },
					{ 'bSortable': false, 'aTargets': [ 5 ] }
		       	]
			});
		}
    });
	
    //validation and ajax call on submit button
	//
    $("#btnSubmit").click(function() {

        var flag = "false";
		$("#txtNormalRange").val($("#txtNormalRange").val().trim());
		$("#txtSubRadiologyTestName").val($("#txtSubRadiologyTestName").val().trim());		
		
		if ($("#txtTestfee").val() == "") {
			$('#txtTestfee').focus();
            $("#txtTestfeeError").text("Please enter test fee");
			$("#txtTestfee").css({"border-color": "red"});                
            flag = "true";
        }
		if ($("#selUnitMeasure").val() == "-1") {
			$('#selUnitMeasure').focus();
            $("#selUnitMeasureError").text("Please select unit measure");
			$("#selUnitMeasure").css({"border-color": "red"});
            flag = "true";
        }
		if ($("#selTest").val() == "-1") {
            $("#selTestError").text("Please select radiology test");
            $("#selTest").css({"border-color": "red"});
            flag = "true";
        }
        if ($("#txtSubRadiologyTestName").val() == "") {
            $("#txtSubRadiologyTestNameError").text("Please enter radiology test name");
			$("#txtSubRadiologyTestName").css({"border-color": "red"});
            flag = "true";
        }
        if (flag == "true") {
            return false;
        }
		var unitMeasure = $("#selUnitMeasure").val();
		var test = $("#txtSubRadiologyTestName").val();
		var testName = $("#selTest").val();
		var normalRange = $("#txtNormalRange").val();
		var description = $("#txtDescription").val();
		description = description.replace(/'/g, "&#39");
		var postData = {
			"operation":"save",
			"unitMeasure":unitMeasure,
			"test" : test,
			"testName":testName,
			"normalRange":normalRange,
			"description":description
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/radiology_sub_test.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#myModalLabel').text("Success");
					$('.modal-body').text("Radiology sub test saved successfully!!!");
					$('#modal').click();
					$('#inactive-checkbox-tick').prop('checked', false).change();
					$('#ok').click(function(){
						$("#radiologySubTable").dataTable().fnReloadAjax();					
						clear();
						bindUnitMeasure();
						bindTest();

						//after sucessful data sent to database redirect to datatable list
						$("#advanced-wizard").hide();
						$(".blackborder").show();
	       				$("#tabAddRadiologySubTest").css({
				            "background": "#fff",
				            "color": "#000"
				        });
				        $("#tabRadiologySubTestList").css({
				            "background":"linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
				            "color": "#fff"
				        });
	        			$("#radiologySubTable").dataTable().fnReloadAjax();

					});
				}
				else{
					$("#txtSubRadiologyTestNameError").text("Test name already exists");
				}

				bindUnitMeasure();
				bindTest();
			},
			error:function() {
				alert('error');
			}
		});
	});	
	
	//
    //keyup functionality
	//
	$("#selTest").change(function() {
        if ($("#selTest").val() != "-1") {
            $("#selTestError").text("");
            $("#selTest").removeAttr("style");
        }
        else{
			$("#selTestError").text("Please select test");
			$("#selTest").css({"border-color": "red"});
        }
    });

    $("#selUnitMeasure").change(function() {
        if ($("#selUnitMeasure").val() != "-1") {
            $("#selUnitMeasureError").text("");
            $("#selUnitMeasure").removeAttr("style");
        }
        else{
			$("#selUnitMeasureError").text("Please select unit measure");
			$("#selUnitMeasure").css({"border-color": "red"});
        }
    });
	
    $("#txtSubRadiologyTestName").keyup(function() {
        if ($("#txtSubRadiologyTestName").val() != "") {
            $("#txtSubRadiologyTestNameError").text("");
            $("#txtSubRadiologyTestName").removeAttr("style");
        }
        else{
        	$("#txtSubRadiologyTestNameError").text("Please enter radiology test name");
			$("#txtSubRadiologyTestName").css({"border-color": "red"});
        }
    });
    $("#btnReset").click(function(){
    	$("#txtSubRadiologyTestName").focus();
    	clear();
    });
});

//		
//function for edit the bed 
//
function editClick(id,name,test_name,range,unit_id,description){	
    
	$('#myModalLabel').text("Update radiology sub test");
	$('.modal-body').html($("#subLabTestModel").html()).show();
	$('#modal').click();
	$("#myModal #btnUpdateRadiology").removeAttr("style");
	$('#myModal #txtSubRadiologyTestName').val(name);
	$("#myModal #selTest").val(test_name);
	$("#myModal #txtNormalRange").val(range);
	$("#myModal #selUnitMeasure").val(unit_id);
	$('#myModal #txtDescription').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id);	
	
	
	$("#myModal #btnUpdateRadiology").click(function(){
		var flag = "false";
		
		if ($("#myModal #selTest").val() == "-1") {
            $("#myModal #selTestError").text("Please select radiology test");
            flag = "true";
        }

        if ($("#myModal #txtSubRadiologyTestName").val() == "") {
			$('#myModal #txtSubRadiologyTestName').focus();
            $("#myModal #txtSubRadiologyTestNameError").text("Please enter radiology sub test");
			$("#myModal #txtSubRadiologyTestName").css({"border-color": "red"});
            flag = "true";
        }
        if ($("#myModal #selTest").val() == "-1") {
			$('#myModal #selTest').focus();
            $("#myModal #selTestError").text("Please select test");
			$("#myModal #selTest").css({"border-color": "red"});
            flag = "true";
        }
        if ($("#myModal #selUnitMeasure").val() == "-1") {
			$('#myModal #selUnitMeasure').focus();
            $("#myModal #selUnitMeasureError").text("Please select unit measure");
			$("#myModal #selUnitMeasure").css({"border-color": "red"});
            flag = "true";
        }

		
		if(flag == "true"){			
		return false;
		}
		
		var test = $("#myModal #txtSubRadiologyTestName").val();
		var subTestName = $("#myModal #selTest").val();
		var range = $("#myModal #txtNormalRange").val();
		var unit_id = $("#myModal #selUnitMeasure").val();
		var description = $("#myModal #txtDescription").val();
		description = description.replace(/'/g, "&#39");
		var id = $('#selectedRow').val();
		
		
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/radiology_sub_test.php",
			data :{            
				"operation" : "update",
				"id" : id,
				"test":test,
				"subTestName":subTestName,
				"range":range,
				"unit_id":unit_id,
				"description":description
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('#myModal').modal('toggle');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Radiology sub test update successfully!!!");
					$('#mymessageModal').click();	
					$("#radiologySubTable").dataTable().fnReloadAjax();
					clear();
				}
			},
			error:function() {
				alert('error');
			}
		});
	});
	
	$("#myModal #selUnitMeasure").change(function() {
        if ($("#myModal #selUnitMeasure").val() != "-1") {
            $("#myModal #selUnitMeasureError").text("");
            $("#myModal #selLabType").removeAttr("style");
        }
    });
	
	$("#myModal #selTest").change(function() {
        if ($("#myModal #selTest").val() != "-1") {
            $("#myModal #selTestError").text("");
            $("#myModal #selTest").removeAttr("style");
        }
    });
	
    $("#myModal #txtSubRadiologyTestName").keyup(function() {
        if ($("#myModal #txtSubRadiologyTestName").val() != "") {
            $("#myModal #txtSubRadiologyTestNameError").text("");
            $("#myModal #txtSubRadiologyTestName").removeAttr("style");
        }
    });	
}

//		
//function for delete the bed
//
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('#confirmMyModalLabel').text("Deleting radiology sub lab test record");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#myConfirmModal').click(); 
	
	$("#confirm").click(function(){
		var id = $('#selectedRow').val();		
			
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/radiology_sub_test.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Radiology sub test delete successfully!!!");
					$('#mymessageModal').click();
					$("#radiologySubTable").dataTable().fnReloadAjax();
					 
				}
				else{
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("This test is used , so you can not delete!!!");
					$('#mymessageModal').click();
				}
			},
			error: function()
			{
				alert('error');
				  
			}
		});
	});	
}

function restoreClick(id,test_name){
	$('#selectedRow').val(id);	
	$('#confirmMyModalLabel').text("Restore radiology sub test record");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#myConfirmModal').click();
	
	$("#confirm").click(function(){
		var id = $('#selectedRow').val();
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/radiology_sub_test.php",
			data :{            
				"operation" : "restore",
				"test_name" : test_name,
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Radiology sub test restored successfully!!!");
					$('#mymessageModal').click();
					$("#radiologySubTable").dataTable().fnReloadAjax();
					clear();
				}	
				if(data == "0"){
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("It can not be restore!!!");
					$('#mymessageModal').click();
					$("#radiologySubTable").dataTable().fnReloadAjax();
				}				
			},
			error:function()
			{
				alert('error');
				  
			}
		});
	});
}

//
//Function for clear the data
//
function clear(){
	$('#selTest').val("");
	$('#selTestError').text("");
	$('#txtSubRadiologyTestName').val("");
	$('#txtSubRadiologyTestNameError').text("");
	$('#txtNormalRange').val("");
	$('#selUnitMeasure').val("");
	$('#selUnitMeasureError').text("");
	$('#txtDescription').val("");
	$("#txtSubRadiologyTestName").removeAttr("style");
	$("#selUnitMeasure").removeAttr("style");
	$("#selTest").removeAttr("style");
}
function bindTest(){
	$("#selTest").prop("selectedIndex", -1);
	$.ajax({     
        type: "POST",
        cache: false,
        url: "controllers/admin/radiology_sub_test.php",
        data: {
        "operation":"showTest"
        },
        success : function(data) { 
        if(data != null && data != ""){
            var parseData = jQuery.parseJSON(data);

            var option ="<option value='-1'>Please select</option>";
            for (var i=0;i<parseData.length;i++)
            {
            option+="<option value='"+parseData[i].id+"'>"+parseData[i].name+"</option>";
            }
            $('#selTest').html(option);          
            }
        },
        error:function(){
        }
    }); 
}
function bindUnitMeasure(){

	$.ajax({     
        type: "POST",
        cache: false,
        url: "controllers/admin/radiology_sub_test.php",
        data: {
        "operation":"showUnitMeasure"
        },
        success : function(data) { 
        if(data != null && data != ""){
            var parseData = jQuery.parseJSON(data);

            var option ="<option value='-1'>Please select</option>";
            for (var i=0;i<parseData.length;i++)
            {
            option+="<option value='"+parseData[i].id+"'>"+parseData[i].unit+"</option>";
            }
            $('#selUnitMeasure').html(option);          
            }
        },
        error:function(){
        }
    }); 
}
// function for check validation for numeric and float value
 function validnumber(number) {
  floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
        return floatRegex.test(number);
    };
//# sourceURL = sub_lab_test.js