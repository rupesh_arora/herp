/*
 * File Name    :   stocking.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   16th feb, 2016
 * Description  :   This page use for load and send data to back hand.
 */
var tableStocking; // define otable variable for datatable

$(document).ready(function() {
loader();   
	debugger;
	// inilize data table
	var date = todayDate();
	tableStocking = $('#tblStocking').dataTable({
		"sPaginationType": "full_numbers",
		"bAutoWidth" : false,
		"fnDrawCallback": function(oSettings) {},
		aoColumnDefs: [{
            aTargets: [5],
            bSortable: false
        }]
	});

	$('#txtStockingDate').val(date);
	
	// click to load  item search pop updateCommands
	$("#iconStaffSearch").on('click',function(){
		$('#itemModalLabel').text("Search Items");
        $('#itemModalBody').load('views/admin/view_item.html', function() {
            $("#itemValue").val("1");
        });
        $('#myItemModal').modal();
		$("#txtItemError").text("");
		$("#txtItem").removeClass("errorStyle");
	});
				
	// call date picker
	$("#txtStockingDate").datepicker();
	
	// keyup functionalty for remove error messae
	    // for remove 	
    $("input[type=text]").keyup(function() {

        if ($(this).val() != "") {
            $(this).next("span").text("");
			$(this).next().next("span").text("");
            $(this).removeClass("errorStyle");
        }		
    });
	$("input[type=number]").keyup(function() {

        if ($(this).val() != "") {
            $(this).next("span").text("");
			 $(this).next().next("span").text("");
            $(this).removeClass("errorStyle");
        }		
    });
	$("input[type=number]").change(function() {

        if ($(this).val() != "") {
            $(this).next("span").text("");
			 $(this).next().next("span").text("");
            $(this).removeClass("errorStyle");
        }		
    });
    $("select").on('change', function() {

        if ($(this).val() != "") {
            $(this).next("span").text("");
            $(this).removeClass("errorStyle");    
        }		
    });
    $("#txtExpireDate").change(function() {
        if ($("#txtExpireDate").val() != "") {
            $("#txtExpireDateError").hide();
            $("#txtExpireDate").removeClass("errorStyle");
            $("#txtExpireDate").datepicker("hide");
        }
    });
	 $("#txtStockingDate").change(function() {
        if ($("#txtStockingDate").val() != "") {
            $("#txtStockingDateError").hide();
            $("#txtStockingDate").removeClass("errorStyle");
            $("#txtStockingDate").datepicker("hide");
        }
    });
	
	// check click event
	 $('#checkNA').change(function() {
		if($(this).is(":checked")== false) {
			$("#txtExpireDate").prop( "disabled", false );
		}
		else{
		
			$("#txtExpireDate").prop( "disabled", true );
			$("#txtExpireDateError").text("");
			$("#txtExpireDate").val("");
			$("#txtExpireDate").removeClass("errorStyle");
		}
	 });
	
	// save data with validation
	$("#btnSave").on('click',function(){
		var flag = false;
		var expireDate;
		var receiveDate = $("#txtItem").attr('item-receive-date');
		
		//check validation
		if (receiveDate > $("#txtStockingDate").val()) {
            $("#txtStockingDateError").show();
            $("#txtStockingDateError").text("Please select correct stocking date.");
            $("#txtStockingDate").addClass("errorStyle");
            flag = true;
        }
		
		if ($("#txtStockingDate").val().trim() == "") {
            $("#txtStockingDateError").show();
            $("#txtStockingDateError").text("Please select stocking date.");
            $("#txtStockingDate").addClass("errorStyle");
            flag = true;
        }
		if ($("#txtShelfNo").val().trim() == "") {
            $("#txtShelfNoError").show();
            $("#txtShelfNoError").text("Please enter shelf no.");
            $("#txtShelfNo").focus();
            $("#txtShelfNo").addClass("errorStyle");
            flag = true;
        }
		/*Check radio button checked or not*/
        if ($('#checkNA').is(":checked")==false) {
			
			if ($("#txtExpireDate").val().trim() == "") {
				$("#txtExpireDateError").show();
				$("#txtExpireDateError").text("Please select expire date.");
				$("#txtExpireDate").addClass("errorStyle");
				flag = true;
			}
			else {
				expireDate = $("#txtExpireDate").val();
			}
		}
		else {
			expireDate = "";
		}
		
		if ($("#txtQuantity").val() < 0 || $("#txtQuantity").val() == "") {
            $("#txtQuantityError").show();
            $("#txtQuantityError").text("Please enter quantity.");
            $("#txtQuantity").focus();
            $("#txtQuantity").addClass("errorStyle");
            flag = true;
        }
		if ($("#txtQuantity").val() > $("#txtItem").attr('item-quantity')) {
            $("#txtQuantityError").show();
            $("#txtQuantityError").text("Please enter valid quantity.");
            $("#txtQuantity").focus();
            $("#txtQuantity").addClass("errorStyle");
            flag = true;
        }
		if ($("#txtItem").val().trim() == "") {
            $("#txtItemError").show();
            $("#txtItemError").text("Please select item.");
            $("#txtItem").focus();
            $("#txtItem").addClass("errorStyle");
            flag = true;
        }
        if ($("#txtItem").attr('item-id') == undefined) {
			$('#txtItem').focus();
            $("#txtItemError").text("Please select valid item that exist.");
            $('#txtItem').addClass("errorStyle");
            flag=true;
		}
		
		if(flag == true) {
			return false;
		}
		
		// define variable to get value
        var item = $("#txtItem").attr('item-id');
        var quantity = $("#txtQuantity").val();
        var shelNo = $("#txtShelfNo").val().trim();
        var notes = $("#txtNotes").val().trim();
        var stockingDate = $("#txtStockingDate").val();

		$('.modal-body').text("");
		$('#confirmCashModalLabel').text("Stocking");
		$('.modal-body').text("Are you sure that you want to save this?");
		$('#confirmCashModal').modal();
		$("#btnconfirm").unbind();
		$("#btnconfirm").on('click',function(){	
			$.ajax({ // ajax call for save content
				type: "POST",
				cache: "false",
				url: "controllers/admin/stocking.php",
				datatype: "json",
				data: {
					item: item,
					quantity:quantity,
					expireDate:expireDate,
					shelNo:shelNo,
					notes:notes,
					stockingDate:stockingDate,
					"operation": "save"
				},
				success: function(data) {
					if (data == "1") {
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Stocking is saved successfully!!!");
						$('#messagemyModal').modal();
						clearDetails();
					
						$("#btnSearch").click();
					}
				},
				error: function() {
					$('.close-confirm').click();					
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
					clearDetails();
				}
			});
		});
	});
	
	// for search click
	// reset functionality	
	$("#btnSearch").on('click',function(){ 
	    tableStocking.fnClearTable();
		var item = $("#itemId").val();
        var quantity = $("#txtQuantity").val();
        var shelNo = $("#txtShelfNo").val();
        var notes = $("#txtNotes").val();
        var stockingDate = $("#txtStockingDate").val();
		if ($('#checkNA').is(":checked")==false) {
			var expireDate = $("#txtExpireDate").val();
		}
		else{
			var chkData = "NULL";
		}
		
		var postData = {
			"operation": "searchDetails",
			"item": item,
			"quantity": quantity,
			"shelNo": shelNo,
			"notes": notes,
			"stockingDate": stockingDate,
            "expireDate" : expireDate,
			"chkData" : chkData
		}
			
		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/stocking.php",
			datatype: "json",
			data: postData,

			success: function(dataSet) {
				dataSet = JSON.parse(dataSet);
					if (dataSet.length > 0) {				
						for(var i=0; i< dataSet.length; i++){
							//Add data to data table
							tableStocking.fnAddData([dataSet[i].itemName,dataSet[i].Quantity,dataSet[i].expire_date,dataSet[i].shelf_no,dataSet[i].stocking_date,dataSet[i].notes]);
						}						
					}
				},
				error: function() {
			
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
		});		 
	});
	
	// reset functionality	
	$("#btnReset").on('click',function(){ 
		clearDetails();
		tableStocking.fnClearTable();
	});

	$("#txtQuantity,#txtExpireDate").keydown(function(e){
        if (e.keyCode < 500) {
            return false;
        }
    });

	$("#txtItem").on('keyup', function(e){	//Autocomplete functionality for item		
		if(e.keyCode !=13){
			$("#txtItem").removeAttr('item-id');
			$("#txtQuantity").val("");
			$("#txtExpireDate").val("");
		}
		var currentObj = $(this);
		jQuery("#txtItem").autocomplete({
			source: function(request, response) {
				var postData = {
					"operation":"searchItem",
					"itemName": currentObj.val(),
					"receivedItem":"receivedItem"
				}
				
				$.ajax(
					{					
					type: "POST",
					cache: false,
					url: "controllers/admin/put_orders.php",
					datatype:"json",
					data: postData,
					
					success: function(dataSet) {
						if (dataSet.length == 2) {
							$("#txtItemError").text("No such item received for this order id");
							return false;
						}
						response($.map(JSON.parse(dataSet), function (item) {								
							return {
								id: item.id,
								value: item.name,
								quantity:item.quantity,
								exp_date : item.exp_date
							}
						}));
					},
					error: function(){
						
					}
				});
			},
			select: function (e, i) {
				var itemId = i.item.id;
				currentObj.attr("item-id", itemId);
				$("#txtItem").val(i.item.value);
				$("#txtQuantity").val(i.item.quantity);
				$("#txtQuantity,#txtExpireDate").removeClass('errorStyle');
				$("#txtQuantityError,#txtExpireDateError").text("");
				if (i.item.exp_date == "0000-00-00") {
					$("#txtExpireDate").val("");
					$('#checkNA').prop('checked',true);
				}
				else{
					$("#txtExpireDate").val(i.item.exp_date);
				}								
			},
			minLength: 3
		});		
	});
});

// clear function
function clearDetails() {
	 $("#txtNotes").val("");
	 $("input[type=text]").val("");
	 $("input[type=number]").val("");
	 $("input[type=text]").next("span").text("");
	 $("input[type=text]").next().next("span").text("");
	 $("input[type=text]").removeClass("errorStyle");
	 $("input[type=number]").next("span").text("");
	 $("input[type=number]").removeClass("errorStyle");
	 $("#txtItem").focus();
	 $('#checkNA').prop('checked', false);
	 $("#txtExpireDate").prop( "disabled", false );
	 $("#itemId").val("");
}
function bindQuantity() {
	$('#txtItem').change(function() {
		var itemName = $('#txtItem').val();

		var postData = {
			"operation": "showQuantity",
			"itemName": itemName
		}
	});
}