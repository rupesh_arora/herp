var disabledDates  = [];
$(document).ready(function(){
	debugger;
	//call datepicker on text box
	var forbidden=['2016-9-7','2016-12-24'];
	
	// remove keyup error message
	removeErrorMessage();
	showHoliday();
	// button reset
	$("#btnReset").on('click',function(){
		clearFormDetails('.leaveApplyForm');
		$('#lblLeaveBalance').text("N/A");
		$('#lblLeaveBalanceError').text("");
		$("#chkIncludeWeekend").prop( "checked", false );
		$("#selLeaveType").focus();
		$('#txtFromDate').datepicker('setEndDate', '');
		$('#txtToDate').datepicker('setStartDate', '');
	});
	
	// load leave type drop down
	bindLeaveType();
	
	
	//on change load leave  balance
	 $('#selLeaveType').change(function(){
		 $("#lblLeaveBalanceError").text("");
		 var value = $('#selLeaveType :selected').val();
		 if($('#selLeaveType :selected').val() == "") {
			 $("#lblLeaveBalance").text('N/A');
		 }
		 else {
			 var postData = {
				"operation":"showLeaveBalance",
				value:value
			};

			// call the function ajax call
			dataCall("./controllers/admin/leave_apply.php", postData, function (result) {
				if (result.trim() != "") {
					var parseData = jQuery.parseJSON(result);
					if (parseData[0].entitlements == null) {
						parseData[0].entitlements = 0;
					}			
					var totalLeaves = parseInt(parseData[0].entitlements) - (parseInt(parseData[0].count_taken) + parseInt(parseData[0].count_schedule) + parseInt(parseData[0].count_approval));
					if(totalLeaves < '0'){
						$("#lblLeaveBalance").text("N/A");
						$("#lblLeaveBalanceError").text("you don't have sufficient leaves");
					}
					else{
						$("#lblLeaveBalance").text(totalLeaves);
					}
							
				}
			});			 
		 }		
	 });
	 
	// save details
	$("#btnApply").on('click',function(){
		
		var flag = false;
		var checked = 0;
		// form validation
		if(validTextField("#txtToDate","#txtToDateError","Please select to date") == true) {
			flag = true;
			$('#txtToDate').focus().datepicker("hide");
		}
		
		if(validTextField("#txtFromDate","#txtFromDateError","Please select from date") == true) {
			flag = true;
			$('#txtFromDate').focus().datepicker("hide");
		}
		
		if(validTextField("#selLeaveType","#selLeaveTypeError","Please select leave type") == true) {
			flag = true;
		}
		if($("#lblLeaveBalance").text() <= "0" || $("#lblLeaveBalance").text() == 'N/A') {
			$("#lblLeaveBalanceError").text("Leave is not available");
			flag = true;
		}
		if(flag == true) {
			return false;
		}
		if ($("#chkIncludeWeekend").prop('checked') == true) {
			checked = 1;
		}

		var d1 = $('#txtFromDate').datepicker('getDate');
	    var d2 = $('#txtToDate').datepicker('getDate');
	    var oneDay = 24*60*60*1000;
	    var diff = 0;
	    if (d1 && d2) {
	  
	      diff = Math.round(Math.abs((d2.getTime() - d1.getTime())/(oneDay)));
	    }
		
		var leaveType = $('#selLeaveType').val();
		var leaveBalance = $('#lblLeaveBalance').text();
		var fromDate = $('#txtFromDate').val();
		var toDate = $('#txtToDate').val();
		var report = $('#txtReport').val();
	 	var value = $('#selLeaveType :selected').val();
		
		// define value for post data in back end file
		var postData = {
			"operation":"checkAppointmentDate",
			fromDate:fromDate,
			toDate:toDate
		};

		// call the function ajax call
		dataCall("./controllers/admin/leave_apply.php", postData, function (result) {
			var parseData = jQuery.parseJSON(result);
			if(parseData.length > 0){				
				$('#updateBody').text("Appointment booked between the leave time period,are you sure do you want to take leave?");				
			}
			else{
				$('#updateBody').text("Are you sure that you want to take leave?");
			}
			$('#confirmUpdateModalLabel').text('Confirm');
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				// define value for post data in back end file
				var postData = {
					"operation":"save",
					leaveType:leaveType,
					leaveBalance:leaveBalance,
					fromDate:fromDate,
					toDate:toDate,
					report:report,
					checked : checked,
					value : value,
					diff:diff
				};

				// call the function ajax call
				dataCall("./controllers/admin/leave_apply.php", postData, function (result) {					
					if (result.trim() == '1') {
						// call message pop up function.
						callSuccessPopUp('Success','Saved Successfully.');
						$("#selLeaveType").focus();
						// call for clear details
						clearFormDetails('.leaveApplyForm');
						$("#chkIncludeWeekend").prop( "checked", false );
						$('#lblLeaveBalance').text("N/A");	
						$('#txtFromDate').datepicker('setEndDate', '');
						$('#txtToDate').datepicker('setStartDate', '');
					    $("#lblLeaveBalanceError").text("");
					}
					else if(result == 'Leave already taken') {
						// call message pop up function.
						callSuccessPopUp('Success','Leave already taken.');
					}
					else if(result == 'you have not sufficient leave') {
						// call message pop up function.
						callSuccessPopUp('Sorry','You have not sufficient leave.');
					}
					else if (result.indexOf("scheduled a operation") > 0) {
						callSuccessPopUp('Sorry',result);
					}
				});
			});
		});
	});
	
});

// call function for bind leave
function bindLeaveType() {
	$.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/leave_apply.php",
        data: {
            "operation": "showBindType"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = JSON.parse(data);

                var option = "<option value=''>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selLeaveType').html(option);
            }
        },
        error: function() {}
    });
}
function showHoliday(){
	var postData = {
        "operation": "showHoliday"
    }
    dataCall("controllers/admin/leave_apply.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (result.length > 2) {
            for (var i = 0; i < parseData.length; i++) {
            	var splitDate = parseData[i].date.split("-");
            	var month = '';
            	var date ='';
            	if (splitDate[1][0] =="0") {
            		month = splitDate[1][1];
            	}
            	else{
            		month = splitDate[1];
            	}
            	if (splitDate[2][0] =="0") {
            		date = splitDate[2][1];
            	}
            	else{
            		date = splitDate[2];
            	}
            	var combinedDate = splitDate[0]+'-'+month+'-'+date;
                disabledDates.push(combinedDate);
            }         
        }
        //beforeShowDay:function(Date) function to disable dates of hoildays
        $("#txtFromDate").datepicker({autoclose:true,startDate: '+0d',beforeShowDay:function(Date){
		        var curr_date = Date.getDate();
		        var curr_month = Date.getMonth()+1;
		        var curr_year = Date.getFullYear();        
		        var curr_date=curr_year+'-'+curr_month+'-'+curr_date;  
		        
		        if (disabledDates.indexOf(curr_date)>-1) return false;        
		    }
			}).on('changeDate', function(selected){
			startDate = new Date(selected.date.valueOf());
			$('#txtToDate').datepicker('setStartDate', startDate);			
		});
		$("#txtToDate").datepicker({autoclose:true,beforeShowDay:function(Date){
		        var curr_date = Date.getDate();
		        var curr_month = Date.getMonth()+1;
		        var curr_year = Date.getFullYear();        
		        var curr_date=curr_year+'-'+curr_month+'-'+curr_date;  
		        
		        if (disabledDates.indexOf(curr_date)>-1) return false;        
		    }
			}).on('changeDate', function(selected){
			startDate = new Date(selected.date.valueOf());
			$('#txtFromDate').datepicker('setEndDate', startDate);
		});            
    });
}