var searchEntitlementTable;
var validFrom;
var validTo;
$(document).ready(function(){
	// checklist checked
	debugger;
	// load leave type drop down
	bindLeaveType();
	
	// load leave type drop down
	bindLeavePeriod();
	
	// inilize datatable
	searchEntitlementTable = $('#tblSearchEntitlement').dataTable({ // inilize datatable on load time.
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
		"bAutoWidth" : false
    });
	
	var data = {"operation": "SearchEntitlements",leaveType:"",leavePeriod:""};
	// load data on load
	loadData(data);
	
	// button reset
	$("#btnReset").on('click',function(){
		searchEntitlementTable.fnClearTable();
		clearFormDetails('.searchEntitlement');
		var data = {"operation": "SearchEntitlements",leaveType:"",leavePeriod:""};
		// load data on load
		loadData(data);
	});
	
	//search functionality
	$("#btnSearch").on('click',function(){
		searchEntitlementTable.fnClearTable();
		var leaveType = $("#selLeaveType").val();
		var leavePeriod = $("#txtLeavePeriod").val();
		var postData = 	{
			"operation": "SearchEntitlements",
			leaveType:leaveType,
			leavePeriod:leavePeriod
		};	
		loadData(postData);			
	});
}); 

// load data
function loadData(postData) {
	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/view_entitlement.php",
		data: postData,
		success: function(data) {
			if (data != null && data != "") {
				var parseData = jQuery.parseJSON(data);
				for (var i = 0; i < parseData.length; i++) {
					var leaveType = parseData[i].name;
					var days = parseData[i].entitlements;
					var validFrom = parseData[i].from_date;
					var validTo = parseData[i].to_date;
					
					searchEntitlementTable.fnAddData([leaveType,validFrom,validTo,days]);
				}
			}
		},
		error: function() {}
	});
}

// call function for bind leave
function bindLeaveType() {
	$.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/leave_apply.php",
        data: {
            "operation": "showBindType"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value=''>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selLeaveType').html(option);
            }
        },
        error: function() {}
    });
}

// call function for bind leave period
function bindLeavePeriod() {
	$.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/view_entitlement.php",
        data: {
            "operation": "showBindLeavePeriod"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value=''>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].period + "</option>";
                }
                $('#txtLeavePeriod').html(option);
            }
        },
        error: function() {}
    });
}