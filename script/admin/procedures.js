var procedureTable; //defile variable for procedureTable
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   procedure.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages procedure data
 *************************************************************************************************** */
	loader();
	
	// show the add procedures
    $("#form_services").hide();
    $("#procedures_list").addClass('list');    
    $("#tab_procedures").addClass('tab-list-add');
	bindGLAccount("#selGLAccount",'');
	bindAccountReceivable("#selAccountPayable");
	bindLedger("#selLedger");//defined in common.js

	
    $("#tab_add_procedures").click(function() {
		
        $("#form_services").show();
        $(".blackborder").hide();

        $("#tab_add_procedures").addClass('tab-detail-add');
        $("#tab_add_procedures").removeClass('tab-detail-remove');
        $("#tab_procedures").removeClass('tab-list-add');
        $("#tab_procedures").addClass('tab-list-remove');
        $("#procedures_list").addClass('list');

       
		clear();
		$("#txtCode").focus();
    });
	
    // show the add procedures list
    $("#tab_procedures").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
		tabProcedure();
	});
        
	// on change of select file type
	$("#file").on("change", function() {
		$('#txtFileError').hide();
	});
	
	// import excel on submit click
	$('#importDataFile').click(function(){
		var flag = false;
		if($('#file').prop('files')[0] == undefined || $('#file').val() == "") {
			$('#txtFileError').show();
			$('#txtFileError').text("Please choose file.");
			flag  = true;
		}
		if(flag == true){
			return false;
		}
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to upload this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			var file_data = $('#file').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: 'controllers/admin/procedures.php', // point to server-side PHP script 
				dataType: 'text', // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					if(data != "" && data != "invalid file" && data =="1"){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Your file has been successfully imported!!!");
						$('#messagemyModal').modal();
						$('#file').val("");
					}
					else if (data =="You must have five column in your file i.e procedure code, procedure name, short name, price and description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="First column should be procedure code" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Second column should be procedure name" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Third column should be short name" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Fourth column should be price" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Fifth column should be description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Please insert data in first , second , third , fourth and fifth column only i.e A , B , C , D and E") {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if(data == "Data can't be null in code ,name and price column"){
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data == "Price should be number") {
                    	$('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
					else if(data == "invalid file"){
						$('#txtFileError').show();
						$('#txtFileError').text("Please import only CSV/XLS file.");
						$('#file').val("");
					}
					else if(data == ""){
						$('#txtFileError').show();
						$('#txtFileError').text("Data already exist.");
						$('#file').val("");
					}
				},
				error: function() {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Data Not Found!!!");
					$('#messagemyModal').modal();

				}
			});
		});
	});
// check the validation and save the data
    $("#btnSubmit").click(function() {
		
		var flag = "false";
        if ($("#selGLAccount").val() == "") {
			$('#selGLAccount').focus();
            $("#selGLAccountError").text("Please select gl account");
			$("#selGLAccount").addClass("errorStyle");
            flag = "true";
        }
		if ($("#selAccountPayable").val() == "") {
			$('#selAccountPayable').focus();
            $("#selAccountPayableError").text("Please select account payable");
			$("#selAccountPayable").addClass("errorStyle");
            flag = "true";
        }
        if ($("#selLedger").val() == "") {
			$('#selLedger').focus();
            $("#selLedgerError").text("Please select ledger");
			$("#selLedger").addClass("errorStyle");
            flag = "true";
        }
		if ($("#txtUnitPrice").val().trim() == '') {
			$("#txtUnitPrice").focus();
            $("#txtUnitPriceError").text("Please enter unit price");
            $("#txtUnitPrice").addClass("errorStyle");
            flag = "true";
        }
		else if(!validnumber($("#txtUnitPrice").val())){
			$("#txtUnitPrice").focus();
			$("#txtUnitPriceError").text("Please enter number only");
            $("#txtUnitPrice").addClass("errorStyle");
            flag = "true";
		}
		if ($("#txtName").val().trim() == '') {
			$("#txtName").focus();
            $("#txtNameError").text("Please enter  name");
            $("#txtName").addClass("errorStyle");
            flag = "true";
        }
		if ($("#txtCode").val().trim() == '') {
			$("#txtCode").focus();
            $("#txtCodeError").text("Please enter code");
            $("#txtCode").addClass("errorStyle");
            flag = "true";
        }
		if (flag == "true")
		{
			return false;
		}
		var ledger = $("#selLedger").val();
		var glAccount = $("#selGLAccount").val();
		var accountPayable = $("#selAccountPayable").val();
		var code = $("#txtCode").val().trim();
		var name = $("#txtName").val().trim();
		var unitPrice = $("#txtUnitPrice").val().trim();
		var shortName = $("#txtShortName").val().trim();
		var description = $("#txtDesc").val();
		description = description.replace(/'/g, "&#39");
		
		//ajax call for insert data into data base
		var postData = {
			"operation":"save",
			"code":code,
			"name":name,
			"ledger":ledger,
			"glAccount":glAccount,
			"accountPayable":accountPayable,
			"unitPrice":unitPrice,
			"shortName":shortName,
			"description":description,
		}
		$.ajax({					
			type: "POST",
			cache: false,
			url: "controllers/admin/procedures.php",
			datatype:"json",
			data: postData,

			success: function(data) {
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Procedure saved sccessfully!!!");
					$('#messagemyModal').modal();
					$('#inactive-checkbox-tick').prop('checked', false).change();
					tabProcedure();
				}
				else{
					$("#txtCodeError").text("Code is already exists");
					$("#txtCode").addClass("errorStyle");
					$("#txtCode").focus();
				}
				
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});//  end ajax call	
   }); //end button click function
   
   
	// key up validation
    $("#txtCode").keyup(function() {
        if ($("#txtCode").val() != '') {
            $("#txtCodeError").text("");
            $("#txtCode").removeClass("errorStyle");
        } 
    }); 
	 $("#txtName").keyup(function() {
        if ($("#txtName").val() != '') {
            $("#txtNameError").text("");
            $("#txtName").removeClass("errorStyle");
        } 
    }); 
	$("#txtUnitPrice").keyup(function() {
        if ($("#txtUnitPrice").val() != '') {
            $("#txtUnitPriceError").text("");
            $("#txtUnitPrice").removeClass("errorStyle");
        } 
    }); 
	 $("#txtShortName").keyup(function() {
        if ($("#txtShortName").val() != '') {
            $("#txtShortNameError").text("");
            $("#txtShortName").removeClass("errorStyle");
        } 
    });  	
    $("#selLedger").change(function() {
        if ($("#selLedger").val() != "") {
            $("#selLedgerError").text("");
            $("#selLedger").removeClass("errorStyle");
        }
    });
	$("#selAccountPayable").change(function() {
        if ($("#selAccountPayable").val() != "") {
            $("#selAccountPayableError").text("");
            $("#selAccountPayable").removeClass("errorStyle");
        }
    });
    $("#selGLAccount").change(function() {
        if ($("#selLedger").val() != "") {
            $("#selGLAccountError").text("");
            $("#selGLAccount").removeClass("errorStyle");
        }
    });
	
	if($('.inactive-checkbox').not(':checked')){
		//Datatable code
		loadDataTable();
	}
	$('.inactive-checkbox').change(function() {
		if($('.inactive-checkbox').is(":checked")){
			procedureTable.fnClearTable();
			procedureTable.fnDestroy();
			procedureTable="";
			procedureTable = $('#proceduresDataTable').dataTable( {
				"bFilter": true,
				"processing": true,
				 "deferLoading": 57,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  procedureTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/procedures.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "checked" });
				},
				"aoColumns": [
					{  "mData": "code" },
					{  "mData": "name" },
					{  "mData": "short_name" },
					{  "mData": "price" },
					{  "mData": "description" },	
					{  
						"mData": function (o) { // This function is used for restore button						
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},
				],
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 4 ] },
				   { 'bSortable': false, 'aTargets': [ 5 ] }
			   ]
			});
		}
		else{
			procedureTable.fnClearTable();
			procedureTable.fnDestroy();
			procedureTable="";
			loadDataTable();
		}
	});
	
	//Click function for reset button
	$("#btnReset").click(function(){
		$('#txtCode').focus();
		clear();
	});
});//  end document.ready function

// edit data dunction for update 
function editClick(id,code,name,price,shortName,description,ledger_id,gl_account_id,account_payable_id){	
	$("#tab_add_procedures").click();
    $("#uploadFile").hide();
    $("#btnReset").hide();
    $("#btnSubmit").hide();
    $('#tab_add_procedures').html("+Update Procedure");

	$("#btnUpdate").removeAttr("style");
	$("#txtCode").removeClass("errorStyle");
	$("#txtCodeError").text("");
	$("#txtName").removeClass("errorStyle");
	$("#txtNameError").text("");
	$("#txtUnitPrice").removeClass("errorStyle");
	$("#txtUnitPriceError").text("");
	$("#txtShortNameError").text("");
    $("#txtShortName").removeClass("errorStyle");
    $("#txtCode").focus();
	
	$('#txtCode').val(code);
	$('#txtName').val(name);
	$('#txtUnitPrice').val(price);
	$('#txtShortName').val(shortName);
	$('#txtDesc').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id); 
	$('#selLedger').val(ledger_id);
	$('#selAccountPayable').val(account_payable_id);
	$('#selGLAccount').val(gl_account_id);
   
	
 
	//validation and update the data
	$("#btnUpdate").click(function(){ // click update button
		var flag = "false";
		if ($("#selGLAccount").val() == "") {
			$('#selGLAccount').focus();
            $("#selGLAccountError").text("Please select gl account");
			$("#selGLAccount").addClass("errorStyle");
            flag = "true";
        }
		if ($("#selAccountPayable").val() == "") {
			$('#selAccountPayable').focus();
            $("#selAccountPayableError").text("Please select account payable");
			$("#selAccountPayable").addClass("errorStyle");
            flag = "true";
        }
        if ($("#selLedger").val() == "") {
			$('#selLedger').focus();
            $("#selLedgerError").text("Please select ledger");
			$("#selLedger").addClass("errorStyle");
            flag = "true";
        }
		if ($("#txtUnitPrice").val().trim() == '') {
			$("#txtUnitPrice").focus();
            $("#txtUnitPriceError").text("Please enter unit price");
            $("#txtUnitPrice").addClass("errorStyle");
            flag = "true";
        }
		else if(!validnumber($("#txtUnitPrice").val())){
			$("#txtUnitPrice").focus();
			$("#txtUnitPriceError").text("Please enter number only");
            $("#txtUnitPrice").addClass("errorStyle");
            flag = "true";
		}
		if ($("#txtName").val().trim() == '') {
			$("#txtName").focus();
            $("#txtNameError").text("Please enter name");
            $("#txtName").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtCode").val().trim() == '') {
			$("#txtCode").focus();
            $("#txtCodeError").text("Please enter code");
            $("#txtCode").addClass("errorStyle");
            flag = "true";
        }
		if (flag == "true")
		{			
			return false;			
		}
		else{
			var id = $('#selectedRow').val();	
			var code = $("#txtCode").val().trim();	
			var ledger = $("#selLedger").val();
			var glAccount = $("#selGLAccount").val();
			var accountPayable = $("#selAccountPayable").val();
			var name = $("#txtName").val().trim();
			var unitPrice = $("#txtUnitPrice").val().trim();
			var shortName = $("#txtShortName").val().trim();
			var description = $("#txtDesc").val();
			description = description.replace(/'/g, "&#39");			
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				var postData = {
					"operation":"update",
					"ledger":ledger,
					"glAccount":glAccount,
					"accountPayable":accountPayable,
					"code":code,
					"name":name,
					"unitPrice":unitPrice,
					"shortName":shortName,
					"description":description,
					"id":id
				}
				$.ajax( //ajax call for update data
				{					
					type: "POST",
					cache: false,
					url: "controllers/admin/procedures.php",
					datatype:"json",
					data: postData,

					success: function(data) {
						if(data != "0" && data != ""){
							$('#myModal').modal('hide');
						    $('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Procedure updated successfully!!!");
							$('#messagemyModal').modal();
							$("#tab_procedures").click()
						}
						else{
							$("#txtCodeError").text("Code is already exists");
							$("#txtCode").addClass("errorStyle");
							$("#txtCode").focus();
						}
					},
					error:function() {
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
				});// end of ajax		
			});	
		}
	});
}

function deleteClick(id){ // delete click function
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete Procedure");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	$('#selectedRow').val(id); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteProcedure("'+type+'");'); // calling function for delete
}// end click fucntion

//function for restore
function restoreClick(id){
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Restore Procedure");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal(); 
	$('#selectedRow').val(id);
	var type="restore";
	$('#confirm').attr('onclick','deleteProcedure("'+type+'");');//calling function for restore
}

//function for delete and restore teh data
function deleteProcedure(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
		var postData = {
			"operation":"delete",
			"id":id
		}
		$.ajax( // ajax call for delete
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/procedures.php",
			datatype:"json",
			data: postData,

			success: function(data) {
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Procedure deleted successfully!!!");
					$('#messagemyModal').modal();
					$("#proceduresDataTable").dataTable().fnReloadAjax();
				}
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		}); // end ajax 
	}
	else{
		var id = $('#selectedRow').val();
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/procedures.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Procedure restored successfully!!!");
					$('#messagemyModal').modal();
					procedureTable.fnReloadAjax(); 
					 clear();
				}			
			},
			error:function()
			{
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	}
}

//this function is used for show the procedure list
function tabProcedure(){
	$("#form_services").hide();
	$(".blackborder").show();

	$("#tab_add_procedures").removeClass('tab-detail-add');
    $("#tab_add_procedures").addClass('tab-detail-remove');
    $("#tab_procedures").removeClass('tab-list-remove');    
    $("#tab_procedures").addClass('tab-list-add');
    $("#procedures_list").addClass('list');


    $("#uploadFile").show();
    $("#btnReset").show();
    $("#btnSubmit").show();
    $('#btnUpdate').hide();
    $('#tab_add_procedures').html("+Add Procedure");

	clear();	
}

//function for clear the data
function clear(){
	$('#txtCode').val("");
	$('#txtCodeError').text("");
	$("#txtCode").removeClass("errorStyle");
	$('#txtName').val("");
	$('#txtShortName').val("");
	$('#txtNameError').text("");
	$('#txtFileError').text('');
	$("#txtName").removeClass("errorStyle");
	$('#txtUnitPrice').val("");
	$('#txtUnitPriceError').text("");
	$("#txtUnitPrice").removeClass("errorStyle");
	$('#txtDesc').val("");
	$('#file').val("");
	$('#txtFileError').hide();
	$('#selLedger').val("");
	$('#selGLAccount').val("");
	$('#selAccountPayable').val("");
	$('#selLedgerError').text("");
	$('#selAccountPayableError').text("");
	$('#selGLAccountError').text("");
	$("#selAccountPayable").removeClass("errorStyle");
	$("#selLedger").removeClass("errorStyle");
	$("#selGLAccount").removeClass("errorStyle");
}
// function for check validation for numeric and float value
 function validnumber(number) {
		floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
        return floatRegex.test(number);
    }; 
	
// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) {  
		 $(".close").click();
    }
});

function loadDataTable(){
	 procedureTable = $('#proceduresDataTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {
			$('.update').unbind();
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = procedureTable.fnGetData(data);
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					var code = mData["code"];
					var name = mData["name"];
					var price = mData["price"];
					var shortName = mData["short_name"];
					var ledger_id = mData["ledger_id"];
					var gl_account_id = mData["gl_account_id"];
					var account_payable_id = mData["account_payable_id"];
					var description = mData["description"];
					editClick(id,code,name,price,shortName,description,ledger_id,gl_account_id,account_payable_id);
   
				}
			});
			$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  procedureTable.fnGetData(data);
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);
				}
			});
		},
		"sAjaxSource":"controllers/admin/procedures.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{  "mData": "code" },
			{  "mData": "name" },
			{  "mData": "short_name" },
			{  "mData": "price" },
			{  "mData": "description" },	
			{  
				"mData": function (o) { //this function is used for edit and delete button
					var data = o;
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
					"  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
					" <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
					"  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
					"  data-original-title='Delete'></i>"; 
				}
			},
		],
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 4 ] },
		   { 'bSortable': false, 'aTargets': [ 5] }
	   ]

	} );
}
//# sourceURL=diseasescategory.js