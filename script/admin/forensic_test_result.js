$(document).ready(function() {
	loader();
	debugger;
	if ($('#thing').val() == 3) {
		var getVisitValue = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
		var forensicPatientId = parseInt($("#txtPatientID").val().replace ( /[^\d.]/g, '' ));
	}
	if ($('#thing').val() == 4) {
		var getVisitValue = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
		var forensicPatientId = parseInt($("#txtPatientID").val().replace ( /[^\d.]/g, '' ));
	}
	if ($('#thing').val() == 5) {
		var getVisitValue = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
		var forensicPatientId = parseInt($("#txtPatientIdHistory").val().replace ( /[^\d.]/g, '' ));
	}
	$("#PrintForensicTest").addClass("hide");
	var otable = $('#forensicResultsTbl').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"sAjaxSource":"controllers/admin/forensic_test_result.php",
		"fnDrawCallback": function ( oSettings ) {
			$('.Print').click(function(){
			
				var data=$(this).parents('tr')[0];
				var mData = $('#forensicResultsTbl').dataTable().fnGetData(data);
				if (null != mData)  // null if we clicked on title row
				{
					$('#lblPrintVisitId').text($("#textvisitId").val());								
					$('#lblPrintPatientId').text($("#txtPatientID").val());
					$('#lblPrintPatient').text(mData["patient_name"]);
					$('#lblPrintTestName').text(mData["name"]);
					$('#lblPrintNormalRange').text(mData["normal_range"]);
					$('#lblPrintResult').text(mData["result"]);
					$('#lblPrintStatus').text(mData["result_flag"]);
					$("#lblPrintRemark").text(mData["report"]);	 
					var visitDate = convertDate(mData["visit_date"]);
					$('#lblPrintVisitDate').text(visitDate);

					/*hospital information through global varibale in main.js*/
					$("#lblPrintEmail").text(hospitalEmail);
					$("#lblPrintPhone").text(hospitalPhoneNo);
					$("#lblPrintAddress").text(hospitalAddress);
					if (hospitalLogo != "null") {
						imagePath = "./images/" + hospitalLogo;
						$("#printLogo").attr("src",imagePath);
					}
					$("#printHospitalName").text(hospitalName);
					//$.print("#PrintForensicTest");
					window.print();

				}
			});
		}, 
		"fnServerParams": function ( aoData ) {
		  aoData.push( {"name": "operation", "value": "bindTableData" },
		  {"name": "visitId", "value": getVisitValue },
		  {"name": "paitentId", "value": forensicPatientId });
		},
		"aoColumns": [
				{  "mData": "name" },
				{  "mData": "normal_range" },
				{  "mData": "result" },	
				{  "mData": "report" },
				{  "mData": function(o) {
                    var data = o;
					var id=data.id;
                    return "<i class='fa fa-print Print' title='Print'" +
                        " data-id="+id+" data-original-title='Print'/>";
                   }	
				}
				],
				aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [3]
            },{
                'bSortable': false,
                'aTargets': [4]
            }]
	});
});
//function to calculate date
function convertDate(o){
        var dbDateTimestamp = o;
        var dateObj = new Date(dbDateTimestamp * 1000);
        var yyyy = dateObj.getFullYear().toString();
        var mm = (dateObj.getMonth()+1).toString(); // getMonth() is zero-based
        var dd  = dateObj.getDate().toString();
        return yyyy +"-"+ (mm[1]?mm:"0"+mm[0]) +"-"+ (dd[1]?dd:"0"+dd[0]);
}
