var roomTypeTable;//define global variable for datatable
$(document).ready(function(){
	loader();	
	debugger;
/* ****************************************************************************************************
 * File Name    :   room_type.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Arora
 * Created Date :   30th dec, 2015
 * Description  :   This page add and mange room types
 *************************************************************************************************** */	
	
	/*By default hide this add screen part*/
	$("#advanced-wizard").hide();
	$("#roomTypeList").addClass('list');
	$("#tabRoomTypeList").addClass('tab-list-add');
		
	//Click for go to add the room type screen part
    $("#tabAddRoomType").click(function() {
        $("#advanced-wizard").show();
        $(".blackborder").hide();
        $("#uploadFile").hide();
		$("#btnSubmit").show();
		$("#btnReset").show();
		$("#btnUpdateRoomType").hide();
		clear();
        $("#tabAddRoomType").addClass('tab-detail-add');
        $("#tabAddRoomType").removeClass('tab-detail-remove');
        $("#tabRoomTypeList").removeClass('tab-list-add');
        $("#tabRoomTypeList").addClass('tab-list-remove');
        $("#roomTypeList").addClass('list');
		
		$('#txtRoomType').focus();
    });
	
	// import excel on submit click
	$('#importDataFile').click(function(){
		var flag = false;
		if($('#file').prop('files')[0] == undefined || $('#file').val() == "") {
			$('#txtFileError').show();
			$('#txtFileError').text("Please choose file.");
			flag  = true;
		}
		if(flag == true){
			return false;
		}
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to upload this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			var file_data = $('#file').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: 'controllers/admin/room_type.php', // point to server-side PHP script 
				dataType: 'text', // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					if(data != "" && data != "invalid file" && data == "1"){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Your file has been successfully imported!!!");
						$('#messagemyModal').modal();
						$('#file').val("");
					}
					else if(data == "invalid file"){
						$('#txtFileError').show();
						$('#txtFileError').text("Please import only CSV/XLS file.");
						$('#file').val("");
					}
					else if (data =="You must have two column in your file i.e room type and description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="First column should be room type" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Second column should be description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Please insert data in first column and second column only i.e A & B") {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Data can't be null in room type column" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
					else if(data == ""){
						$('#txtFileError').show();
						$('#txtFileError').text("Data already exist.");
						$('#file').val("");
					}
				},
				error: function() {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Data Not Found!!!");
					$('#messagemyModal').modal();

				}
			});
		});
	});
	
	/*Click function for show the room type lists*/
    $("#tabRoomTypeList").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();		
        tabRoomTypeList();// Call the function for show the room type lists
    });
	
	/*By default when radio button is not checked show all active data*/
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		loadDataTable();
    }
	// Ajax call for loading the data table for inactive room type
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	roomTypeTable.fnClearTable();
	    	roomTypeTable.fnDestroy();
	    	//Datatable code
			roomTypeTable = $('#roomTypeTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					/*On click of restore icon call this function*/
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  roomTypeTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/room_type.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					/* {  "mData": "id" }, */
					{ "mData": "type" },
					{ "mData": "description" },
					{
						"mData": function (o) { 
						var data = o;
						var id = data["id"];
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},
				],
				aoColumnDefs: [
					{ 'bSortable': false, 'aTargets': [ 1 ] },
					{ 'bSortable': false, 'aTargets': [ 2 ] }
				]
			} );
		}
		else{
			roomTypeTable.fnClearTable();
	    	roomTypeTable.fnDestroy();
	    	//Datatable code
			loadDataTable();
		}
    });
	
	//Click function for save the room type 
	$("#btnSubmit").click(function(){
		/*perform validation*/
		var flag="false";
		$("#txtRoomType").val($("#txtRoomType").val().trim());
		
		if($("#txtRoomType").val()==""){
			$('#txtRoomType').focus();
			$("#txtRoomTypeError").text("Please enter room type");
			$("#txtRoomType").addClass("errorStyle");    
			flag="true";
		}
		
		if(flag=="true"){
		return false;
		}
		
		var roomType = $("#txtRoomType").val();
		var description = $("#txtDescription").val();
		description = description.replace(/'/g, "&#39");
		var postData = {
			"operation":"save",
			"roomType":roomType,
			"description":description
		}
		
		// Ajax call for save room type
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/room_type.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Room type saved successfully!!!");
					$('#messagemyModal').modal();
					$('#inactive-checkbox-tick').prop('checked', false).change();
					tabRoomTypeList();
				}
				else{
					$("#txtRoomTypeError").text("Room Type already exists");
					$("#txtRoomType").addClass("errorStyle");
					$('#txtRoomType').focus();
				}				
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	});
	
	//keyup functionality
	$("#txtRoomType").keyup(function(){
		if($("#txtRoomType").val()!=""){
			$("#txtRoomTypeError").text("");
			$("#txtRoomType").removeClass("errorStyle");	    	 
		}
	});		
});

/*define edit Click function for update*/
function editClick(id,room_type,description){	
	$('#tabAddRoomType').html("+Update room Type");
	$('#tabAddRoomType').click();
	$("#uploadFile").hide();
	$("#btnSubmit").hide();
	$("#btnReset").hide();
	$("#btnUpdateRoomType").removeAttr("style");
	$('#txtRoomType').val(room_type);
	$('#txtDescription').val(description.replace(/&#39/g, "'"));
	
	$('#selectedRow').val(id);
	
	//Click function for update room type 
	$("#btnUpdateRoomType").click(function(){
		
		var flag = validation();
		
		if(flag == "true"){			
			return "false";
		}
		else{
			var roomType = $("#txtRoomType").val();
			var description = $("#txtDescription").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();			
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				// Ajax call for update room type
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/room_type.php",
					data :{            
						"operation" : "update",
						"id" : id,
						"roomType":roomType,
						"description":description
					},
					success: function(data) {	
						if(data != "0" && data != ""){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Room type updated successfully!!!");
							roomTypeTable.fnReloadAjax();
							$('#messagemyModal').modal();
							$('#tabRoomTypeList').click();
							clear();
						}
						if(data == "0"){
							$("#myModal #txtRoomTypeError").text("Room type already exists");
							$("#myModal #txtRoomType").addClass("errorStyle");
							$('#myModal #txtRoomType').focus();
							
						}
					},
					error:function() {
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messagemyModal').modal();
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					}
				});
			});
		}
	});		
	
	$("#myModal #txtRoomType").keyup(function(){
		if($("#myModal #txtRoomType").val()!=""){
			$("#myModal #txtRoomTypeError").text("");
			$("#myModal #txtRoomType").removeClass("errorStyle");	
		}
	});	
}
/*Define function for delete the room type*/ 
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete room type");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	var type="delete";
	$('#confirm').attr('onclick','deleteRoomType("'+type+'");');//pass attribute with sent opertion and function with it's type
	
}

/*Define function for restore the room type*/ 
function restoreClick(id){
	$('#selectedRow').val(id);	
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Restore room type");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();
	var type="restore";
	$('#confirm').attr('onclick','deleteRoomType("'+type+'");');
	
}

//function for show the list of room type
function tabRoomTypeList(){
	$("#advanced-wizard").hide();
	$(".blackborder").show();
	$("#roomTypeTable").dataTable().fnReloadAjax();
	$("#tabAddRoomType").removeClass('tab-detail-add');
	$("#tabAddRoomType").html('+Add Room Type');
	$("#tabAddRoomType").addClass('tab-detail-remove');
	$("#tabRoomTypeList").addClass('tab-list-add');
	$("#tabRoomTypeList").removeClass('tab-list-remove');
	$("#roomTypeList").addClass('list');
	$("#txtRoomType").removeClass("errorStyle");
	clear();
}
function deleteRoomType(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
			
		//Ajax call for delete the room type 	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/room_type.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Room type deleted successfully!!!");
					roomTypeTable.fnReloadAjax();
					$('#messagemyModal').modal();
					 
				}
				else{
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("This room type is used , so you can not delete!!!");
					$('#messagemyModal').modal();
				}
			},
			error: function()
			{
				alert('error');
				  
			}
		});
	}
	else{

	var id = $('#selectedRow').val();
	
		//Ajax call for restore the room type 
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/room_type.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Room type restored successfully!!!");
					$('#messagemyModal').modal();
					roomTypeTable.fnReloadAjax();
				}			
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}
}
/*key press event on ESC button*/
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		 window.location.href = "http://localhost/herp/";
    }
});
/*reset button functionality*/
$("#btnReset").click(function(){
	$("#txtRoomType").removeClass("errorStyle");
	clear();
	$('#txtRoomType').focus();
});

//function for clear the data
function clear(){
	$('#txtRoomType').val("");
	$('#txtFileError').text('');
	$('#txtRoomTypeError').text("");
	$('#txtDescription').val("");
}

function loadDataTable(){
roomTypeTable = $('#roomTypeTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {
			/*On click of update icon call this function*/
			$('.update').unbind();
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = roomTypeTable.fnGetData(data);//get datatable data
				if (null != mData)  // null if we clicked on title row
				{
					/*get the value of that row from database column*/
					var id = mData["id"];
					var type = mData["type"];
					var description = mData["description"];
					editClick(id,type,description);//call edit click function to update data       
				}
			});
			/*On click of delete icon call this function*/
			$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  roomTypeTable.fnGetData(data);
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);
				}
			});
		},
		
		"sAjaxSource":"controllers/admin/room_type.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{ "mData": "type" },
			{ "mData": "description" },
			{
				"mData": function (o) { 
					var data = o;
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
				   " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
				   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
				   " style='font-size: 22px; color:#a94442; cursor:pointer;' "+
				   " data-original-title='Delete'></i>"; 
				}
			},	
			],
			/*Disable sort for following columns*/
			aoColumnDefs: [
				{ 'bSortable': false, 'aTargets': [ 1 ] },
				{ 'bSortable': false, 'aTargets': [ 2 ] }
			]
	} );
}
function validation(){
	var flag = "false";
		$("#txtRoomType").val($("#txtRoomType").val().trim());
		
		if ($("#txtRoomType").val() == '') {
			$("#txtRoomType").focus();
			$("#txtRoomTypeError").text("Please enter room type");
			$("#txtRoomType").addClass("errorStyle");
			flag = "true";
		}
		
		if ($("#txtRoomTypeError").text() != '') {
			flag = "true";
		}
		return flag;
}
//# sourceURL = room_type.js