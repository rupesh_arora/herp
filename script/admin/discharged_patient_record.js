$(document).ready(function() {
	debugger;
	$(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});

	$("#toDate").attr("disabled",true);
	$("#fromDate").attr("disabled",true);	

	$('.changeRadio').change(function() {
		if ($('#radToFromDate').prop( "checked") == true) {
	        $("#toDate").attr("disabled",false);
			$("#fromDate").attr("disabled",false);
	    }
	    else{
	    	$("#toDate").attr("disabled",true);
			$("#fromDate").attr("disabled",true);
			$("#toDate").val("");
			$("#fromDate").val("");
	    }
	});
	$("#fromDate").change(function() {
		if ($("#fromDate").val() !='') {
			$("#fromDate").removeClass("errorStyle");
			$("#txtFromDateError").text("");
		}
	});
	$("#toDate").change(function() {
		if ($("#toDate").val() !='') {
			$("#toDate").removeClass("errorStyle");
			$("#txtToDateError").text("");
		}
	});	
	$("#clear").click(function(){
		clear();
	});
	google.charts.setOnLoadCallback(drawChart);
});
function drawChart() {
	  
	$("#viewReport").on("click",function(){
		var dataLoad ='';
		var fromDate ;
		var toDate ;
		if ($('#radToday').prop( "checked") == true) {
			dataLoad = "today";
		}
		else if($('#radLastSevenDays').prop( "checked") == true) {
			dataLoad = "last7days";
		}
		else if($('#radToFromDate').prop( "checked") == true) {
			dataLoad = "betweenDate";
			fromDate = $("#fromDate").val();
			toDate = $("#toDate").val();
			if (fromDate =='') {
				$("#txtFromDateError").text("Please choose from date");
				$("#fromDate").addClass("errorStyle");
				$("#fromDate").focus();
				return false;
			}
			if (toDate =='') {
				$("#txtToDateError").text("Please choose from date");
				$("#toDate").addClass("errorStyle");
				$("#toDate").focus();
				return false;
			}
		}
		else if($('#radLastThirtyDays').prop( "checked") == true) {
			dataLoad = "last30days";
		}
		else if($('#radAllTime').prop( "checked") == true) {
			dataLoad = "all";
		}

		if (fromDate == undefined) {
			fromDate ="";
		}
		if (toDate == undefined) {
			toDate = "";
		}
		if (dataLoad=='') {
			return false;
		}

		var postData = {
			"operation" : "showChartData",
			"dataLoad" : dataLoad,
			"fromDate" : fromDate,
			"toDate" : toDate
		}
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/discharged_patient_record.php",
			data: postData,
			success: function(data) {
				if(data != null && data != ""){
					var bigArray = '';
					var parseData =  $.parseJSON(data);

					//how to get property name Object.getOwnPropertyNames(parseData[0])
					var labBill = parseInt(parseData[0]['Lab Bill']);
					var forensicBill  = parseInt(parseData[0]['Forensic Bill']);
					var procedureBill = parseInt(parseData[0]['Procedure Bill']);
					var radiologyBill = parseInt(parseData[0]['Radiology Bill']);
					var serviceBill = parseInt(parseData[0]['Service Bill']);

					if (labBill ==0 && forensicBill ==0 && procedureBill ==0 && radiologyBill ==0 && serviceBill ==0) {
						$("#chart_div").hide();
						$('#messagemyModal').modal();
						$('#messageMyModalLabel').text("Alert");
						$('.modal-body').text("No data exist!!!");
						$("#btnSaveExcel,#btnSavePdf").hide();
						return false;
					}
					else{
						
						bigArray = [
							['Lab name','Bill'],
							['Lab Bill', labBill],
							['Forensic Bill', forensicBill],
							['Procedure Bill', procedureBill],
							['Radiology Bill', radiologyBill],
							['Service Bill', serviceBill]
			            ];					

						var dataBill = google.visualization.arrayToDataTable(bigArray);
						var optionsBill = {
					      title: 'Bill Analysis','width':1200,'height':1000
					    };
					    var chartBill = new google.visualization.ColumnChart(document.getElementById('chart_div'));
						chartBill.draw(dataBill, optionsBill);
						$("#chart_div,#btnSaveExcel,#btnSavePdf").show();
						$('svg').css("max-width",$("#page-content").width()-50);//max-width for graph
					}

					window.saveFile = function saveFile () {

						var dataExcel = parseData;
						var opts = [{sheetid:'One',header:true}];
    					var res = alasql('SELECT INTO XLSX("DischargePatient.xlsx",?) FROM ?',
                     	[opts,[dataExcel]]);
					}
				}
			}
		});
	});

	$("#btnSavePdf").click(function(){
		$.print("#chart_div");
	});
}
function clear(){
	$("#chart_div,#btnSaveExcel,#btnSavePdf").hide();
	$("#toDate,#fromDate").val("");
	$("#txtFromDateError,#txtToDateError").text("");
	$("#fromDate,#toDate").removeClass("errorStyle");
}