var receiverTable;
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   receiver.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   16 feb, 2016
 * Description  :   This page  manages receiver name data
 *************************************************************************************************** */
    loader();
    $(".amountPrefix").text(amountPrefix);
	$("#form_dept").hide();
    $("#drug_list").addClass('list');    
    $("#tab_drug").addClass('tab-list-add');
	
	//This click function is used for change the tab
    $("#tab_add_drug").click(function() {
        showAddTab();
		clear();
    });
	
	//This click function is used for show the seller name list
    $("#tab_drug").click(function() {
        tabReceiverList();//Calling this function for show the list 
    });
	
	// import excel on submit click
	$('#importDataFile').click(function(){
		var flag = false;
		if($('#file').prop('files')[0] == undefined || $('#file').val() == "") {
			$('#txtFileError').show();
			$('#txtFileError').text("Please choose file.");
			flag  = true;
		}
		if(flag == true){
			return false;
		}
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to upload this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			var file_data = $('#file').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: 'controllers/admin/allergies_categories.php', // point to server-side PHP script 
				dataType: 'text', // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					if(data != "" && data != "invalid file"){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Your file has been successfully imported!!!");
						$('#messagemyModal').modal();
						$('#file').val("");
					}
					if(data == "invalid file"){
						$('#txtFileError').show();
						$('#txtFileError').text("Please import only CSV/XLS file.");
						$('#file').val("");
					}
					if(data == ""){
						$('#txtFileError').show();
						$('#txtFileError').text("Data already exist.");
						$('#file').val("");
					}
				},
				error: function() {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Data Not Found!!!");
					$('#messagemyModal').modal();

				}
			});
		});
	});
	
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
			receiverTable = $('#tblReceiver').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {	
					$('.update').unbind();				
					$('.update').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData = receiverTable.fnGetData(data);
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var name = mData["name"];
							var description = mData["description"];
							editClick(id,name,description); //Calling this function for update the data 
						}
					});
					$('.delete').unbind();
					$('.delete').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  receiverTable.fnGetData(data);
						
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							deleteClick(id);//Calling this function for delete the data 
						}
					});	
				},
				
				"sAjaxSource":"controllers/admin/receivers.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "show" });
				},
				"aoColumns": [
					{ "mData": "name" },
					{ "mData": "description" }, 
					{
						"mData": function (o) { 
						return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
						   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
						   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
						   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
						   "  data-original-title='Delete'></i>"; 
						}	
					},	
				],
				aoColumnDefs: [
			        { 'bSortable': false, 'aTargets': [ 2 ] },
			        { 'bSortable': false, 'aTargets': [ 1 ] }
			    ]
			} );
    }
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	receiverTable.fnClearTable();
	    	receiverTable.fnDestroy();
	    	//Datatable code
			receiverTable = $('#tblReceiver').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  receiverTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);//Calling this function for restore the data
						}						
					});
				},
				
				"sAjaxSource":"controllers/admin/receivers.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					{ "mData": "name" },
					{ "mData": "description" }, 
					{
					  "mData": function (o) { 
					   		return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; 
					   }		
					},	
				],
				aoColumnDefs: [
			        { 'bSortable': false, 'aTargets': [ 2 ] },
			        { 'bSortable': false, 'aTargets': [ 1 ] }
			    ]
			} );
		}
		else{
			receiverTable.fnClearTable();
	    	receiverTable.fnDestroy();
	    	//Datatable code
			receiverTable = $('#tblReceiver').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.update').unbind();
					$('.update').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData = receiverTable.fnGetData(data);
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var name = mData["name"];
							var description = mData["description"];
							editClick(id,name,description); //Calling this function for update the data 
						}
					});
					$('.delete').unbind();
					$('.delete').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  receiverTable.fnGetData(data);
						
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							deleteClick(id);//Calling this function for delete the data 
						}
					});
				},
				
				"sAjaxSource":"controllers/admin/receivers.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "show" });
				},
				"aoColumns": [
					{ "mData": "name" },
					{ "mData": "description" }, 
					{
						"mData": function (o) { 
						return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
						   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
						   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
						   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
						   "  data-original-title='Delete'></i>"; 
						}
					},	
				],
				aoColumnDefs: [
			        { 'bSortable': false, 'aTargets': [ 2 ] },
			        { 'bSortable': false, 'aTargets': [ 1 ] }
			    ]
			} );
		}
    });
	
	//This click function is used for validate and save the data on save button
    $("#btnSave").click(function() {
		
		var flag = "false";
		
		$("#txtReceiverName").val().trim();
		
        if ($("#txtReceiverName").val() == '') {
            $("#txtReceiverNameError").text("Please enter receiver name");
            $("#txtReceiverName").addClass("errorStyle");
            $("#txtReceiverName").focus();
            flag = "true";
        }
		if(flag == "true"){			
			return false;
		}
		
		//ajax call
		
		var receiverName = $("#txtReceiverName").val();
		var description = $("#txtDescription").val();
		description = description.replace(/'/g, "&#39");
		var postData = {
			"operation":"save",
			"receiverName":receiverName,
			"description":description
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/receivers.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Receiver saved successfully!!!");
					$('#messagemyModal').modal();
						tabReceiverList();
				}				
				else{
					$("#txtReceiverNameError").text("Receiver is already exists");
					$("#txtReceiverName").addClass("errorStyle");
					$("#txtReceiverName").focus();
				}				
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});		
    });//Close save button

    //Keyup functionality
    $("#txtReceiverName").keyup(function() {
        if ($("#txtReceiverName").val() != '') {
            $("#txtReceiverNameError").text("");
            $("#txtReceiverName").removeClass("errorStyle");
        } 
    });
	
	//Click function is used for reset button
    $("#btnReset").click(function(){
    	$("#txtReceiverName").focus();
    	clear();
    });
});//Close document.ready function

//This function will call on edit button
function editClick(id,name,description){	
	showAddTab();
    $("#btnReset").hide();
    $("#btnSave").hide();
    $('#btnUpdate').show();
    $('#tab_add_drug').html("+Update Receiver");
    $("#txtReceiverName").focus();
    
	$("#uploadFile").hide();
	$("#txtReceiverNameError").text("");
    $("#txtReceiverName").removeClass("errorStyle");
	$('#txtReceiverName').val(name);
	$('#txtDescription').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id);
	
	//Click function for update button
	$("#btnUpdate").click(function(){
		var flag = "false";
		$("#txtReceiverName").val($("#txtReceiverName").val().trim());
		if ($("#txtReceiverName").val() == '') {
			$("#txtReceiverNameError").text("Please enter receiver name");
			$("#txtReceiverName").addClass("errorStyle");
			flag = "true";
		}
		if(flag == "true"){			
			return false;
		}
		else{
			var receiverName = $("#txtReceiverName").val();
			var description = $(" #txtDescription").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();	
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/receivers.php",
					data :{            
						"operation" : "update",
						"id" : id,
						"receiverName":receiverName,
						"description":description
					},
					success: function(data) {	
						if(data != "0" && data != ""){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Receiver updated successfully!!!");
							receiverTable.fnReloadAjax();
							$('#messagemyModal').modal();
							tabReceiverList();
							clear();
						}
						else{
							$("#txtReceiverNameError").text("Receiver is already exists");
							$("#txtReceiverName").addClass("errorStyle");
							$("#txtReceiverName").focus();
						}
					},
					error:function()
					{
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();			  
					}
				});
			});
		}
	});
}

//Click function for delete the drugcategory
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete receiver");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteSeller("'+type+'");');
}

//Click function for restore the drugcategory
function restoreClick(id){
	$('#selectedRow').val(id);
	$('.modal-body').text("");	
	$('#confirmMyModalLabel').text("Restore receiver");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();
	
	var type="restore";
	$('#confirm').attr('onclick','deleteSeller("'+type+'");');
}
//function for delete the drugcategoryList
function deleteSeller(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
			
		//Ajax call for delete the drugcategory 	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/receivers.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Receiver deleted successfully!!!");
					receiverTable.fnReloadAjax();
					$('#messagemyModal').modal();
					 
				}
			},
			error:function()
			{
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
	
		//Ajax call for restore the drugcategory 
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/receivers.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Receiver restored successfully!!!");
					receiverTable.fnReloadAjax();
					$('#messagemyModal').modal();
					 clear();
				}			
			},
			error:function()
			{
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	}
}

//This function is used for show the list of seller name
function tabReceiverList(){
	$('#inactive-checkbox-tick').prop('checked', false).change();
	$("#form_dept").hide();
	$(".blackborder").show();
    $("#tab_add_drug").removeClass('tab-detail-add');
    $("#tab_add_drug").addClass('tab-detail-remove');
    $("#tab_drug").removeClass('tab-list-remove');    
    $("#tab_drug").addClass('tab-list-add');
    $("#drug_list").addClass('list');
    $("#uploadFile").show();
    $("#btnReset").show();
    $("#btnSave").show();
    $('#btnUpdate').hide();
    $('#tab_add_drug').html("+Add Receiver");
	clear();
}

function showAddTab(){
    $("#form_dept").show();
    $(".blackborder").hide();

    $("#tab_add_drug").addClass('tab-detail-add');
    $("#tab_add_drug").removeClass('tab-detail-remove');
    $("#tab_drug").removeClass('tab-list-add');
    $("#tab_drug").addClass('tab-list-remove');
    $("#drug_list").addClass('list');       
	$("#txtReceiverNameError").text("");
    $("#txtReceiverName").removeClass("errorStyle");
    $('#txtReceiverName').focus();
}

// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		 $(".close").click();
    }
});

//This function is used for clear the data
function clear(){
	$('#txtReceiverName').val("");
	$('#txtReceiverNameError').text("");
	$('#txtDescription').val("");
	$('#txtReceiverName').removeClass("errorStyle");
	$("#txtReceiverName").removeClass("errorStyle");
	$("#myModal #txtReceiverName").removeClass("errorStyle");
}
//# sourceURL=filename.js