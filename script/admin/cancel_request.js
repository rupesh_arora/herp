var labTable;
var radiolgyTable;
var forensicTable;
var serviceTable;
var procedureTable;
$(document).ready(function(){
	debugger;
	labTable = $('#tblLabTest').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "fnDrawCallback": function(oSettings) {
           $(".cancelLab").click(function(){
	           	debugger;
	           	var id = $(this).attr('data-val');
	           	var cost = $($(this).parent().parent().find('td')[4]).text();
	           	var visitId = parseInt($($(this).parent().parent().find('td')[0]).text().replace(/\D/g,''));
           		var mData = labTable.fnGetData();
				if (null != mData)  // null if we clicked on title row
				{
					var postData = {
						"operation":"cancelLab",
						"visitId":visitId,
						"cost":cost,
						"id":id
					}
					CancelRequest(postData,"labTable");
				}
           });
        },
			
		"sAjaxSource":"controllers/admin/view_lab_request.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{  
				"mData": function (o) { 	
				var visitId = o["visit_id"];
				var patientPrefix = o["visit_prefix"];
					
				var visitIdLength = visitId.length;
				for (var i=0;i<6-visitIdLength;i++) {
					visitId = "0"+visitId;
				}
				visitId = patientPrefix+visitId;
				return visitId; 
				}
			},
			{  
				"mData": function (o) { 	
				var patientId = o["patient_id"];
				var patientPrefix = o["patient_prefix"];
					
				var patientIdLength = patientId.length;
				for (var i=0;i<6-patientIdLength;i++) {
					patientId = "0"+patientId;
				}
				patientId = patientPrefix+patientId;
				return patientId; 
				}
			},
			/* { "mData": "visit_id" },
			{ "mData": "patient_id" }, */
			{ "mData": "test_code" },
			{ "mData": "name" },
			{ "mData": "cost" },
			{ "mData": "type" },
			{ "mData": "pay_status" },
			{  
				"mData": function (o) { 					
					return "<input type='button' class='btn btn-small btn-primary process cancelLab' data-val="+o["id"]+" value='Cancel' />"; 
				}
			},	
		],
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 7 ] }
	   ]
    });

	radiolgyTable = $('#tblRadiologyTest').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "fnDrawCallback": function(oSettings) {
           	$(".cancelRadiology").click(function(){
           		debugger;
           		var id = $(this).attr('data-val');
	           	var cost = $($(this).parent().parent().find('td')[4]).text();
	           	var visitId = parseInt($($(this).parent().parent().find('td')[0]).text().replace(/\D/g,''));
           		var mData = radiolgyTable.fnGetData();
				if (null != mData)  // null if we clicked on title row
				{
					var postData = {
						"operation":"cancelRadiology",
						"visitId":visitId,
						"cost":cost,
						"id":id
					}
					CancelRequest(postData,"radiolgyTable");
				}
           });
        },
			
		"sAjaxSource":"controllers/admin/view_radiology_request.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{  
				"mData": function (o) { 	
				var visitId = o["visit_id"];
				var patientPrefix = o["visit_prefix"];
					
				var visitIdLength = visitId.length;
				for (var i=0;i<6-visitIdLength;i++) {
					visitId = "0"+visitId;
				}
				visitId = patientPrefix+visitId;
				flag = 1;
				return visitId; 
				}
			},
			{  
				"mData": function (o) { 	
				var patientId = o["patient_id"];
				var patientPrefix = o["patient_prefix"];
					
				var patientIdLength = patientId.length;
				for (var i=0;i<6-patientIdLength;i++) {
					patientId = "0"+patientId;
				}
				patientId = patientPrefix+patientId;
				return patientId; 
				}
			},
			/* { "mData": "visit_id" },
			{ "mData": "patient_id" }, */
			{ "mData": "test_code" },
			{ "mData": "name" },
			{ "mData": "cost" },
			{ "mData": "pay_status" },
			{  
				"mData": function (o) { 
					var visitType = o["visitType"];
					if(visitType == "Emergency Case"){					
						return "<input type='button' class='btn btn-small btn-primary process cancelRadiology' visitType="+visitType+"  data-val="+o["id"]+" value='Cancel' />"; 
					}
					else{
						return "<input type='button' class='btn btn-small btn-primary process cancelRadiology' data-val="+o["id"]+" value='Cancel' />"; 
					}
				}
			},	
		],
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 6 ] }
	   ]
    });

	forensicTable = $('#tblForensicTest').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "fnDrawCallback": function(oSettings) {
           	$(".cancel").click(function(){
           		debugger;
           		var id = $(this).attr('data-val');
	           	var cost = $($(this).parent().parent().find('td')[4]).text();
	           	var visitId = parseInt($($(this).parent().parent().find('td')[0]).text().replace(/\D/g,''));
           		var mData = forensicTable.fnGetData();
				if (null != mData)  // null if we clicked on title row
				{
					var postData = {
						"operation":"cancelForensic",
						"visitId":visitId,
						"cost":cost,
						"id":id
					}
					CancelRequest(postData,"forensicTable");
				}
           });
        },
		
		"sAjaxSource":"controllers/admin/view_forensic_request.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{  
				"mData": function (o) { 	
				var visitId = o["visit_id"];
				var patientPrefix = o["visit_prefix"];
					
				var visitIdLength = visitId.length;
				for (var i=0;i<6-visitIdLength;i++) {
					visitId = "0"+visitId;
				}
				visitId = patientPrefix+visitId;
				return visitId; 
				}
			},
			{  
				"mData": function (o) { 	
				var patientId = o["patient_id"];
				var patientPrefix = o["patient_prefix"];
					
				var patientIdLength = patientId.length;
				for (var i=0;i<6-patientIdLength;i++) {
					patientId = "0"+patientId;
				}
				patientId = patientPrefix+patientId;
				flag = 1;
				return patientId; 
				}
			},
			/* { "mData": "visit_id" },
			{ "mData": "patient_id" }, */
			{ "mData": "test_code" },
			{ "mData": "name" },
			{ "mData": "cost" },
			{ "mData": "type" },
			{ "mData": "pay_status" },
			{  
				"mData": function (o) { 
					var visitType = o["visitType"];
					if(visitType == "Emergency Case"){					
						return "<input type='button' class='btn btn-small btn-primary process cancel' visitType="+visitType+"  data-val="+o["id"]+" value='Cancel' />"; 
					}
					else{
						return "<input type='button' class='btn btn-small btn-primary process cancel' data-val="+o["id"]+" value='Cancel' />"; 
					}
				}
			},	
		],
		
	   aoColumnDefs: [{
            aTargets: [7],
            bSortable: false
        }]
    });

	serviceTable = $('#tblService').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "fnDrawCallback": function(oSettings) {
           	$(".cancelService").click(function(){
           		var id = $(this).attr('data-val');
	           	var cost = $($(this).parent().parent().find('td')[4]).text();
	           	var visitId = parseInt($($(this).parent().parent().find('td')[0]).text().replace(/\D/g,''));
           		var mData = serviceTable.fnGetData();
				if (null != mData)  // null if we clicked on title row
				{
					var postData = {
						"operation":"cancelService",
						"visitId":visitId,
						"cost":cost,
						"id":id
					}
					CancelRequest(postData,"serviceTable");
				}
           });
        },
        "sAjaxSource":"controllers/admin/patient_service.php",
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "operation", "value": "show" });
			},
			"aoColumns": [
				{  
					"mData": function (o) { 	
					var visitId = o["visit_id"];
					var patientPrefix = o["visit_prefix"];
						
					var visitIdLength = visitId.length;
					for (var i=0;i<6-visitIdLength;i++) {
						visitId = "0"+visitId;
					}
					visitId = patientPrefix+visitId;
					return visitId; 
					}
				},
				{  
					"mData": function (o) { 	
					var patientId = o["patient_id"];
					var patientPrefix = o["patient_prefix"];
						
					var patientIdLength = patientId.length;
					for (var i=0;i<6-patientIdLength;i++) {
						patientId = "0"+patientId;
					}
					patientId = patientPrefix+patientId;
					return patientId; 
					}
				},
				/* { "mData": "visit_id" },
				{ "mData": "patient_id" }, */
				{ "mData": "code" },
				{ "mData": "name" },
				{ "mData": "cost" },
				{ "mData": "pay_status" },
				{  
					"mData": function (o) { 					
						flag = 1;
						var visitType = o["visitType"];
						if(visitType == "Emergency Case"){					
							return "<input type='button' class='btn btn-small btn-primary process cancelService' visitType="+visitType+"  data-val="+o["id"]+" value='Cancel' />"; 
						}
						else{
							return "<input type='button' class='btn btn-small btn-primary process cancelService' data-val="+o["id"]+" value='Cancel' />"; 
						}
					}
				},	
			],
			aoColumnDefs: [
			   { 'bSortable': false, 'aTargets': [ 6 ] }
		   ]
    });

	procedureTable = $('#tblProcedure').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "fnDrawCallback": function(oSettings) {
           	$(".cancelProcedure").click(function(){
           		var id = $(this).attr('data-val');
	           	var cost = $($(this).parent().parent().find('td')[4]).text();
	           	var visitId = parseInt($($(this).parent().parent().find('td')[0]).text().replace(/\D/g,''));
           		var mData = procedureTable.fnGetData();
				if (null != mData)  // null if we clicked on title row
				{
					var postData = {
						"operation":"cancelProcedure",
						"visitId":visitId,
						"cost":cost,
						"id":id
					}
					CancelRequest(postData,"procedureTable");
				}
           });
        },			
		"sAjaxSource":"controllers/admin/patient_procedure.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{  
				"mData": function (o) { 	
				var visitId = o["visit_id"];
				var patientPrefix = o["visit_prefix"];
					
				var visitIdLength = visitId.length;
				for (var i=0;i<6-visitIdLength;i++) {
					visitId = "0"+visitId;
				}
				visitId = patientPrefix+visitId;
				return visitId; 
				}
			},
			{  
				"mData": function (o) { 	
				var patientId = o["patient_id"];
				var patientPrefix = o["patient_prefix"];
					
				var patientIdLength = patientId.length;
				for (var i=0;i<6-patientIdLength;i++) {
					patientId = "0"+patientId;
				}
				patientId = patientPrefix+patientId;
				return patientId; 
				}
			},
			/* { "mData": "visit_id" },
			{ "mData": "patient_id" }, */
			{ "mData": "code" },
			{ "mData": "name" },
			{ "mData": "cost" },
			{ "mData": "pay_status" },
			{  
				"mData": function (o) { 					
				return "<input type='button' class='btn btn-small btn-primary process cancelProcedure' data-val="+o["id"]+" value='Cancel' />"; 
				}
			},	
		],
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 6 ] }
	   ]
	} );

	$(".customerBtn").removeClass('activeSelf');
    $($('.customerBtn')[0]).addClass('activeSelf');
    $("#tabProcedure").hide();
    $("#tabLabTest").hide();
    $("#tabRadiologyTest").hide();
    $("#tabForensicTest").hide();
    $("#tabService").show();

	 $(".customerBtn").click(function(){
       tabButton(this);
    });
});

function tabButton(current){
    $(".customerBtn").removeClass('activeSelf');
    $(current).addClass('activeSelf');
    if($(current).val() == 'Procedure'){
        $("#tabProcedure").show();
        $("#tabLabTest").hide();
        $("#tabRadiologyTest").hide();
        $("#tabForensicTest").hide();
        $("#tabService").hide();
    }
    else if($(current).val() == 'Lab Test'){
        $("#tabProcedure").hide();
        $("#tabLabTest").show();
        $("#tabRadiologyTest").hide();
        $("#tabForensicTest").hide();
        $("#tabService").hide();
    }
    else if($(current).val() == 'Radiology Test'){
        $("#tabProcedure").hide();
        $("#tabLabTest").hide();
        $("#tabRadiologyTest").show();
        $("#tabForensicTest").hide();
        $("#tabService").hide();
    }
    else if($(current).val() == 'Forensic Test'){
        $("#tabProcedure").hide();
        $("#tabLabTest").hide();
        $("#tabRadiologyTest").hide();
        $("#tabForensicTest").show();
        $("#tabService").hide();
    }
    else{
        $("#tabProcedure").hide();
        $("#tabLabTest").hide();
        $("#tabRadiologyTest").hide();
        $("#tabForensicTest").hide();
        $("#tabService").show();
    }
}

function CancelRequest(postData,tblName){
	$('#confirmUpdateModalLabel').text();
	$('#updateBody').text("Are you sure that you want to cancel this?");
	$('#confirmUpdateModal').modal();
	$("#btnConfirm").unbind();
	$("#btnConfirm").click(function(){
		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/cancel_request.php",
			datatype:"json",
			data: postData,
			success: function(data) {
				if(data != "0" && data != ""){
					if(tblName == "labTable"){
						labTable.fnReloadAjax();
						$('#myModal').modal('hide');
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Canceled Successfully!!!");
						$('#messagemyModal').modal();
					}
					else if(tblName == "radiolgyTable"){
						radiolgyTable.fnReloadAjax();
						$('#myModal').modal('hide');
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Canceled Successfully!!!");
						$('#messagemyModal').modal();
					}
					else if(tblName == "forensicTable"){
						forensicTable.fnReloadAjax();
						$('#myModal').modal('hide');
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Canceled Successfully!!!");
						$('#messagemyModal').modal();
					}
					else if(tblName == "procedureTable"){
						procedureTable.fnReloadAjax();
						$('#myModal').modal('hide');
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Canceled Successfully!!!");
						$('#messagemyModal').modal();
					}
					else {
						serviceTable.fnReloadAjax();
						$('#myModal').modal('hide');
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Canceled Successfully!!!");
						$('#messagemyModal').modal();
					}
					
				}
			},
			error: function(){
				
			}
		});
	});
}