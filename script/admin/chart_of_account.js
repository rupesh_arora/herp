$(document).ready(function(){
    loader();
    debugger;
    var dataTableIntialization = 0;
    $('#selIsChild').prop('disabled',true);
    bindAccountType();   //biind account type
    bindClass();         //bind class
    bindSubType();     //bind sybtypes
    bindChild(); 
	/*Hide add ward by default functionality*/

	$("#advanced-wizard").hide();
    $("#chartOfAccountList").addClass('list');    
    $("#tabChartOfAccountList").addClass('tab-list-add');
	
	/*Click for add the sub Type*/
    $("#tabAddChartOfAccount").click(function() {
        clear();
        showAddTab();
    });
	//Click function for show the chart of account lists
    $("#tabChartOfAccountList").click(function() {
        clear();
        tabChartOfAccountList();

    });

    removeErrorMessage();

    $('#chkBoxChild').change(function() {
        if($('#chkBoxChild').prop('checked') == true) {
            $('#selIsChild').prop('disabled',false);
        }
        else{
            $('#selIsChild').prop('disabled',true);
        }
        $("#selIsChild").val('');
    });

    /*$('#lblChild').click(function() {
        if($('#chkBoxChild').prop('checked') == true) {
            $('#chkBoxChild').prop('checked',false);
        }
        else {
            $('#chkBoxChild').prop('checked',true);
        }
    });
*/
    $('#btnSubmit').click(function() {
        var flag = false;
        if($('#selClass').val() == '') {
            $('#selClassError').text('Please select class');
            $('#selClass').focus();
            $('#selClass').addClass("errorStyle"); 
            flag = true;
        }
        if($('#selSubType').val() == '') {
            $('#selSubTypeError').text('Please select subtype');
            $('#selSubType').focus();
            $('#selSubType').addClass("errorStyle"); 
            flag = true;
        }
        if($('#selAccountType').val() == '') {
            $('#selAccountTypeError').text('Please select account type');
            $('#selAccountType').focus();
            $('#selAccountType').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtAccountName').val() == '') {
            $('#txtAccountNameError').text('Please enter name');
            $('#txtAccountName').focus();
            $('#txtAccountName').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtAccountNumber').val() == '') {
            $('#txtAccountNumberError').text('Please enter account number');
            $('#txtAccountNumber').focus();
            $('#txtAccountNumber').addClass("errorStyle"); 
            flag = true;
        }
        if(flag == true) {
            return false;
        }

        var accountNo = $('#txtAccountNumber').val();
        var accountName = $('#txtAccountName').val();
        var accountType = $('#selAccountType').val();
        var isChild = $('#selIsChild').val();
        var subType = $('#selSubType').val()
        var accountClass = $('#selClass').val();
        var notes = $('#txtNotes').val();

        var postData = {
            accountNo : accountNo,
            accountName : accountName,
            accountType : accountType,
            isChild : isChild,
            subType : subType,
            accountClass : accountClass,
            notes : notes,
            operation : "saveCashOfAccount"
        }

        $.ajax(
            {                   
            type: "POST",
            cache: false,
            url: "controllers/admin/chart_of_account.php",
            datatype:"json",
            data: postData,
            
            success: function(data) {
                if(data != "0" && data != ""){
                    callSuccessPopUp('Success','Saved successfully!!!');
                    tabChartOfAccountList();
                    clear();
                    bindChild();
                }
                else {
                    $('#txtAccountNumberError').text('Account number is already exist');
                    $('#txtAccountNumber').focus();
                    $('#txtAccountNumber').addClass('errorStyle');
                }
            },
            error: function(){

            }
        });
    });

    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code 
        dataTableIntialization = 0;
        chartOfAccountTable = $('#chartOfAccountTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "bAutoWidth": false,
            "order": [[ 2, 'asc' ]],
            "aaSorting": [],
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = chartOfAccountTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var accountNo = mData["account_no"];   
                        var accountName = mData["account_name"];                   
                        var accountTypeName = mData["account_type_name"];
                        var accountType = mData["account_type"];             
                        var parentAccountNo = mData["parent_account_no"];
                        var parentAccountName = mData["parent_account_name"];
                        var accountClass = mData["class"];  
                        var accountClassName = mData["class_name"];                    
                        var subType = mData["subtype"];
                        var subTypeName = mData["sub_type_name"];
                        var notes = mData["notes"];
                        var isParent = mData['is_parent'];
                        editClick(id,accountNo,accountName,accountType,accountTypeName,parentAccountNo,parentAccountName,
                            accountClass,accountClassName,subType,subTypeName,notes,isParent);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = chartOfAccountTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });

                 
                if (dataTableIntialization == 1) {

                    var aiRows = chartOfAccountTable.fnGetNodes();//Get all rows of data table
                    var aiData =  chartOfAccountTable.fnGetData();

                    if(aiRows.length == 0){
                        return false;
                    }
                    $.each(aiData,function(index,value){
                        var isParent = value.is_parent;
                        if(isParent == "yes"){
                            $(aiRows[index]).css({'background-color':'#2F8DB1'});
                           /* $(aiRows[index]).find('td').css("border","2px solid #000");
                            $(aiRows[index]).find('td:even').addClass('no-border-right');
                            $(aiRows[index]).find('td:last-child').css("border-right","2px solid #000");*/
                        }
                    });
                }
            },
            "sAjaxSource": "controllers/admin/chart_of_account.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "account_no"
                }, {
                    "mData": "account_name"
                }, {
                    "mData": "account_type_name"
                }, {
                    "mData": "sub_type_name"
                }, {
                    "mData": "class_name"
                }, {
                    "mData": "parent_account_name"
                }, {
                    "mData": "notes"
                }, {
                    "mData": function(o) {
                        var data = o;
                        dataTableIntialization = 1;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [6,7]
            }]

        });
    }

    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            chartOfAccountTable.fnClearTable();
            chartOfAccountTable.fnDestroy();
            dataTableIntialization = 0;
            chartOfAccountTable = "";
            chartOfAccountTable = $('#chartOfAccountTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "aaSorting": [],
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = chartOfAccountTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var accountNo = mData["account_no"];   
                            var accountName = mData["account_name"];                   
                            var accountTypeName = mData["account_type_name"];
                            var accountType = mData["account_type"];             
                            var parentAccountNo = mData["parent_account_no"];
                            var parentAccountName = mData["parent_account_name"];
                            var accountClass = mData["class"];  
                            var accountClassName = mData["class_name"];                    
                            var subType = mData["subtype"];
                            var subTypeName = mData["sub_type_name"];
                            var notes = mData["notes"];
                            restoreClick(id,accountNo,accountName,accountType,accountTypeName,parentAccountNo,parentAccountName,
                                accountClass,accountClassName,subType,subTypeName,notes);

                        }

                    });

                    if (dataTableIntialization == 1) {

                        var aiRows = chartOfAccountTable.fnGetNodes();//Get all rows of data table
                        var aiData =  chartOfAccountTable.fnGetData();

                        if(aiRows.length == 0){
                            return false;
                        }
                        $.each(aiData,function(index,value){
                            var isParent = value.is_parent;
                            if(isParent == "yes"){
                                $(aiRows[index]).css('background-color','#2F8DB1');
                            }
                        });
                    }
                },

                "sAjaxSource": "controllers/admin/chart_of_account.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                {
                    "mData": "account_no"
                }, {
                    "mData": "account_name"
                }, {
                    "mData": "account_type_name"
                }, {
                    "mData": "sub_type_name"
                }, {
                    "mData": "class_name"
                }, {
                    "mData": "parent_account_name"
                }, {
                    "mData": "notes"
                }, {
                    "mData": function(o) {
                        var data = o;
                        dataTableIntialization = 1;
                        return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                    }
                },
            ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [6,7]
                }]
            });
        } else { // show active data on unchecked   
            chartOfAccountTable.fnClearTable();
            chartOfAccountTable.fnDestroy();
            dataTableIntialization = 0;
            chartOfAccountTable = "";
            chartOfAccountTable = $('#chartOfAccountTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "aaSorting": [],
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = chartOfAccountTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var accountNo = mData["account_no"];   
                            var accountName = mData["account_name"];                   
                            var accountTypeName = mData["account_type_name"];
                            var accountType = mData["account_type"];             
                            var parentAccountNo = mData["parent_account_no"];
                            var parentAccountName = mData["parent_account_name"];
                            var accountClass = mData["class"];  
                            var accountClassName = mData["class_name"];                    
                            var subType = mData["subtype"];
                            var subTypeName = mData["sub_type_name"];
                            var notes = mData["notes"];
                            var isParent = mData['is_parent'];
                            editClick(id,accountNo,accountName,accountType,accountTypeName,parentAccountNo,parentAccountName,
                                accountClass,accountClassName,subType,subTypeName,notes,isParent);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = chartOfAccountTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });

                    if (dataTableIntialization == 1) {

                        var aiRows = chartOfAccountTable.fnGetNodes();//Get all rows of data table
                        var aiData =  chartOfAccountTable.fnGetData();

                        if(aiRows.length == 0){
                            return false;
                        }
                        $.each(aiData,function(index,value){
                            var isParent = value.is_parent;
                            if(isParent == "yes"){
                                $(aiRows[index]).css('background-color','#2F8DB1');
                            }
                        });
                    }
                },

                "sAjaxSource": "controllers/admin/chart_of_account.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                {
                    "mData": "account_no"
                }, {
                    "mData": "account_name"
                }, {
                    "mData": "account_type_name"
                }, {
                    "mData": "sub_type_name"
                }, {
                    "mData": "class_name"
                }, {
                    "mData": "parent_account_name"
                }, {
                    "mData": "notes"
                }, {
                    "mData": function(o) {
                        var data = o;
                        dataTableIntialization = 1;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [6,7]
                }]
            });
        }
    });


    $('#btnReset').click(function() {
        clear();
    });
});

function editClick(id,accountNo,accountName,accountType,accountTypeName,parentAccountNo,parentAccountName,
                                accountClass,accountClassName,subType,subTypeName,notes,isParent) {
  // bindChild();
    showAddTab()  
    $("#btnReset").hide();
    $("#btnSubmit").hide();
    $('#btnUpdate').show();
    $('#tabAddChartOfAccount').html("+Update Chart Of Account");    
    $("#txtAccountNumber").focus();
   
    
    $("#txtAccountNumber").removeClass("errorStyle");
    $("#txtAccountNumberError").text("");
    $("#txtAccountName").removeClass("errorStyle");
    $("#txtAccountNameError").text("");
    $("#selAccountType").removeClass("errorStyle");
    $("#selAccountTypeError").text("");
    $("#selSubType").removeClass("errorStyle");
    $("#selSubTypeError").text("");
    $("#selClass").removeClass("errorStyle");
    $("#selClassError").text("");


    $('#txtAccountNumber').val(accountNo);
    $('#txtAccountName').val(accountName);
    $('#selAccountType').val(accountType);
    if (isParent == 'yes') {
        $('#selIsChild').val('');
    }
    else{
        $('#selIsChild').val(parentAccountNo);
        $('#selIsChild').attr("disabled",false);
        $("#chkBoxChild").prop("checked",true);
    }       
        
    $('#selSubType').val(subType);
    $('#selClass').val(accountClass);
    $('#txtNotes').val(notes.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
    //validation
    //remove validation style
    // edit data function for update 


    removeErrorMessage(); //remove error messages

    $('#chkBoxChild').change(function() {     // disable and enable dropdown by clicking on checkbox
        if($('#chkBoxChild').prop('checked') == true) {
            $('#selIsChild').prop('disabled',false);
        }
        else{
            $('#selIsChild').prop('disabled',true);
        }
        $("#selIsChild").val('');
    });
    
    
    $("#btnUpdate").click(function() { // click update button
        var flag = false;
        if($('#selClass').val() == '') {
            $('#selClassError').text('Please select class');
            $('#selClass').focus();
            $('#selClass').addClass("errorStyle"); 
            flag = true;
        }
        if($('#selSubType').val() == '') {
            $('#selSubTypeError').text('Please select subtype');
            $('#selSubType').focus();
            $('#selSubType').addClass("errorStyle"); 
            flag = true;
        }
        if($('#selAccountType').val() == '') {
            $('#selAccountTypeError').text('Please select account type');
            $('#selAccountType').focus();
            $('#selAccountType').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtAccountName').val() == '') {
            $('#txtAccountNameError').text('Please enter name');
            $('#txtAccountName').focus();
            $('#txtAccountName').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtAccountNumber').val() == '') {
            $('#txtAccountNumberError').text('Please enter account number');
            $('#txtAccountNumber').focus();
            $('#txtAccountNumber').addClass("errorStyle"); 
            flag = true;
        }
        if(flag == true) {
            return false;
        }
        var id = $('#selectedRow').val();
        var accountNo = $('#myModal #txtAccountNumber').val();
        var accountName = $('#myModal #txtAccountName').val();
        var accountType = $('#myModal #selAccountType').val();
        var isChild = $('#myModal #selIsChild').val();
        var subType = $('#myModal #selSubType').val()
        var accountClass = $('#myModal #selClass').val();
        var notes = $('#myModal #txtNotes').val();

        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){

        var postData = {
            id : id,
            accountNo : accountNo,
            accountName : accountName,
            accountType : accountType,
            isChild : isChild,
            subType : subType,
            accountClass : accountClass,
            notes : notes,
            operation : "update"
        }
        $.ajax( //ajax call for update data
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/chart_of_account.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('#myModal').modal('hide');
                        $('.close-confirm').click();
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Chart of account updated successfully!!!");
                        $('#messagemyModal').modal();
                        chartOfAccountTable.fnReloadAjax();
                        tabChartOfAccountList();
                        clear();
                    }
                    else {                
                        $("#txtAccountNumberError").text("Account number is already exist");
                        $("#txtAccountNumber").focus();               
                        $("#txtAccountNumber").addClass('errorStyle');
                    }
                },
                error: function() {
                    $('.close-confirm').click();
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                    $('#messagemyModal').modal();
                }
            }); // end of ajax
        });
    });
} // end update button

function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete chart of account");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteChartOfAccount("' + type + '","'+id+'","","","","","","");');
} // end click fucntion

function restoreClick(id,accountNo,accountName,accountType,accountTypeName,parentAccountNo,parentAccountName,
                                accountClass,accountClassName,subType,subTypeName,notes) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore chart of account");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteChartOfAccount("' + type + '","'+id+'","'+accountNo+'","'+accountName+'","'+accountType+'","'+parentAccountNo+'","'+accountClass+'","'+subType+'","'+notes+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteChartOfAccount(type,id,accountNo,accountType,parentAccountNo,accountClass,subType,notes) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/chart_of_account.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Chart of account deleted successfully!!!");
                    $('#messagemyModal').modal();
                    chartOfAccountTable.fnReloadAjax();
                } 
                else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This is parent account number is used, so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/chart_of_account.php",
            data: {
                "operation": "restore",
                "accountNo":accountNo,
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Chart of account restored successfully!!!");
                    $('#messagemyModal').modal();
                    chartOfAccountTable.fnReloadAjax();
                }
                else {
                    callSuccessPopUp('Sorry','Account number already exist you can not restore !!!');
                }
            },
            error: function() {             
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary unavailable to respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}

    
function tabChartOfAccountList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
   
    $("#tabAddChartOfAccount").removeClass('tab-detail-add');
    $("#tabAddChartOfAccount").addClass('tab-detail-remove');
    $("#tabChartOfAccountList").removeClass('tab-list-remove');    
    $("#tabChartOfAccountList").addClass('tab-list-add');
    $("#chartOfAccountList").addClass('list');
   
    $("#btnReset").show();
    $("#btnSubmit").show();
    $('#btnUpdate').hide();
    $('#tabAddChartOfAccount').html("+Add Asset Location");
    $('#inactive-checkbox-tick').prop('checked', false).change();
    clear();
}
function showAddTab(){
    $("#advanced-wizard").show();
    $(".blackborder").hide();

    $("#tabAddChartOfAccount").addClass('tab-detail-add');
    $("#tabAddChartOfAccount").removeClass('tab-detail-remove');
    $("#tabChartOfAccountList").removeClass('tab-list-add');
    $("#tabChartOfAccountList").addClass('tab-list-remove');
    $("#chartOfAccountList").addClass('list');
    $('#txtAccountNumber').focus();
}

function bindAccountType() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/chart_of_account.php",
        data: {
            "operation": "showAccountType"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option data-value='" + parseData[i].value + "' value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
                }
                $('#selAccountType').html(option);
            }
        },
        error: function() {}
    });
}
function bindClass() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/chart_of_account.php",
        data: {
            "operation": "showClass"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option data-value='" + parseData[i].value + "' value='" + parseData[i].id + "'>" + parseData[i].class + "</option>";
                }
                $('#selClass').html(option);
            }
        },
        error: function() {}
    });
}
function bindSubType() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/chart_of_account.php",
        data: {
            "operation": "showSubType"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option data-value='" + parseData[i].value + "' value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selSubType').html(option);
            }
        },
        error: function() {}
    });
}
function bindChild() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/chart_of_account.php",
        data: {
            "operation": "showChild"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option data-value='" + parseData[i].value + "' value='" + parseData[i].id + "'>" + parseData[i].account_name + "</option>";
                }
                $('#selIsChild').html(option);
            }
        },
        error: function() {}
    });
}
function clear() {
    $('#txtAccountNumber').val('');
    $('#txtAccountNumberError').text('');
    $('#txtAccountNumber').removeClass("errorStyle");

    $('#txtAccountName').val('');
    $('#txtAccountNameError').text('');
    $('#txtAccountName').removeClass("errorStyle");

    $('#selAccountType').val('');
    $('#selAccountTypeError').text('');
    $('#selAccountType').removeClass("errorStyle");

    $('#selIsChild').val('');
    $('#selIsChildError').text('');
    $('#selIsChild').removeClass("errorStyle");

    $('#selSubType').val('');
    $('#selSubTypeError').text('');
    $('#selSubType').removeClass("errorStyle");

    $('#selClass').val('');
    $('#selClassError').text('');
    $('#selClass').removeClass("errorStyle");

    $('#txtNotes').val('');
    $('#txtNotesError').text('');
    $('#selIsChild').prop('disabled',true);
    $('#chkBoxChild').prop('checked',false);
    $('#txtAccountNumber').focus();
}