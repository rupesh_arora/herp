$(document).ready(function(){
	loader(); 
	$("#txtSpecimenId").focus();
	var otable;
	
	$("#btnSpecimenLoad").click(function(){
		$("#txtSpecimenIdError").text("");
		$("#txtSpecimenId").removeClass("errorStyle");
		loadSpecimenResults();
	});
	// for remove error style	
    $("#txtSpecimenId").keyup(function() {

        if ($("#txtSpecimenId").val().trim() != "") {
            $("#txtSpecimenIdError").hide();
			$("#txtSpecimenIdError").text("");
			$("#txtSpecimenId").removeClass("errorStyle");
        }
		else{
			$("#txtSpecimenIdError").show();
			$("#txtSpecimenIdError").text("Please enter specimen ID");
			$("#txtSpecimenId").addClass("errorStyle");
			$('#txtSpecimenId').focus();
			forensicclear();
		}
    });
	$("#txtSpecimenId").change(function() {

        if ($("#txtSpecimenId").val().trim() != "") {
            $("#txtSpecimenIdError").hide();
			$("#txtSpecimenIdError").text("");
			$("#txtSpecimenId").removeClass("errorStyle");
        }
	});
	$('#searchSpecimen').click(function(){
		$('#specimenModalLabel').text("Search Specimen");
		$('#specimenModalBody').load('views/admin/view_specimen.html',function(){
		});
		$('#mySpecimenModal').modal();
	});
	$("#txtSpecimenId").focusin(function(){
		forensicclear();
		
	});
	$('#btnSubmit').click(function(){
		saveResult();
	});
	$('#btnReset').click(function(){
		$('#txtSpecimenDescription').val("");
		$('#txtDrawnDate').val("");
		$('#txtLabTest').val("");
		$('#txtNormalRange').val("");
		$('#txtResult').val("");
		$('#txtReport').val("");
		$("#txtSpecimenId").removeClass("errorStyle");
		$("#txtSpecimenIdError").text("");
		$('#txtSpecimenId').focus();
		forensicclear();
	});
	
	$('#txtSpecimenId').keypress(function (e) {
        if (e.which == 13) {
            $("#btnSpecimenLoad").click();
        }
    });
	
});
function saveResult(){
	var flag = false;
	if($("#txtSpecimenId").val().trim()== "")
	{
		$("#txtSpecimenIdError").show();
		$("#txtSpecimenIdError").text("Please enter specimen ID");
		$("#txtSpecimenId").addClass("errorStyle");
		$('#txtSpecimenId').focus();
		forensicclear();
		flag = true;
	}
	if($("#txtDrawnDate").val().trim()== "")
	{
		$("#txtSpecimenIdError").show();
		$("#txtSpecimenIdError").text("Please select load button");
		
		flag = true;
	}
	if(flag == true){
		return false;
	}
	else{
		$('#operation').val("saveResult");
		var formData = $('#forensicResultForm').serializeArray();
		$.post("controllers/admin/forensic_result.php", formData).done(function (data) {
			if(data){
				$('#messageMyModalLabel').text("Success");
				$('.modal-body').html('Forensic result saved successfully!!!');
				$('#messagemyModal').modal();
				$('#txtSpecimenId').focus();
				forensicclear();
			}
		});
	}
}
//function to load Specimen Results
function loadSpecimenResults(){
	var flag = false;
	if($("#txtSpecimenId").val().trim()== "")
	{
		$("#txtSpecimenIdError").show();
		$("#txtSpecimenIdError").text("Please enter specimen ID");
		$("#txtSpecimenId").addClass("errorStyle");
		$('#txtSpecimenId').focus();
		forensicclear();
		flag = true;
	}
	if(flag == true){
		return false;
	}
	else{
		var specimenId =$("#txtSpecimenId").val();
		var postData = {
			"operation":"loadSpecimen",
			"specimenId":specimenId
		}
		
		$.ajax({					
			type: "POST",
			cache: false,
			url: "controllers/admin/forensic_result.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					var parseData = jQuery.parseJSON(data);
					if(parseData == ""){
						$("#txtSpecimenIdError").show();
						$("#txtSpecimenIdError").text("Please enter a valid specimen ID");
						$("#txtSpecimenId").addClass("errorStyle");
						$('#txtSpecimenId').focus();
						return false;
					}
					
					$('#txtSpecimenDescription').val(parseData[0]['description']);						
					$('#txtDrawnDate').val(convertTimestamp(parseData[0]['created_on']));
					$('#txtLabTest').val(parseData[0]['name']);
					if(parseData[0]['rangeNormalModified'] != "" && parseData[0]['rangeNormalModified'] != null){
						$('#txtNormalRange').val(parseData[0]['rangeNormalModified']);
					}
					else{
						$('#txtNormalRange').val(parseData[0]['rangeNormal']);
					}
					
					$('#txtResult').val(parseData[0]['result']);
					if(parseData[0]['result_flag'] != "" && parseData[0]['result_flag'] != null)
					{
						$('#ddlResultFlag option:contains(' + parseData[0]['result_flag'] + ')').attr('selected', 'selected');							
					}
					else{
						$('#ddlResultFlag option:contains("N/A")').attr('selected', 'selected');
					}
					if(parseData[0]['report'] != "" && parseData[0]['report'] != null)
					{
						$('#txtReport').val(parseData[0]['report']);
					}
					else{
						$('#txtReport').val("");
					}	
				}
				if(data == ""){
						$("#txtSpecimenIdError").show();
						$("#txtSpecimenIdError").text("Please enter a valid specimen ID");
						$("#txtSpecimenId").addClass("errorStyle");
						$('#txtSpecimenId').focus();
				}
			},
			error:function() {
				alert('error');
			}
		});
	}
}
// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		 $(".close").click();
    }
});
function convertTimestamp(timestamp) {
  var d = new Date(timestamp * 1000),	// Convert the passed timestamp to milliseconds
		yyyy = d.getFullYear(),
		mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
		dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
		hh = d.getHours(),
		h = hh,
		min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
		ampm = 'AM',
		time;
			
	if (hh > 12) {
		h = hh - 12;
		ampm = 'PM';
	} else if (hh === 12) {
		h = 12;
		ampm = 'PM';
	} else if (hh == 0) {
		h = 12;
	}
	
	// ie: 2013-02-18, 8:35 AM	
	time = yyyy + '-' + mm + '-' + dd + ' ' + h + ':' + min + ' ' + ampm;
		
	return time;
}
function forensicclear(){
	$('#forensicResultForm')[0].reset();
	$('#ddlResultFlag option:contains("N/A")').attr('selected', 'selected');
	$('#txtSpecimenDescription').val("");
	$('#txtDrawnDate').val("");
	$('#txtLabTest').val("");
	$('#txtNormalRange').val("");
	$('#txtResult').val("");
	$('#txtReport').val("");

}
//# sourceURL = view_visit.js