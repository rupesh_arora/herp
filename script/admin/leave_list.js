var leaveListTable;
var selectValue;
$(document).ready(function(){
	// checklist checked
	debugger;
	/* $("#txtFromDate").val(new Date().getFullYear()+'-01-01');
	$("#txtToDate").val(new Date().getFullYear()+'-12-31'); */
	
	$('#leaveList_chkSearchFilter_checkboxgroup_allcheck').click(function() {
	   var checked = $(this).is(":checked");
	   $('input[name="leaveList[chkSearchFilter][]"]').prop('checked', checked);
	});
						
	$('input[type="checkbox"]').click(function() {
		var notCheckedCount = $('input[name="leaveList[chkSearchFilter][]"]:not(:checked)').length;
		var check = (notCheckedCount == 0);
		$('#leaveList_chkSearchFilter_checkboxgroup_allcheck').prop('checked', check);
	});
	
	// inilize datatable
	leaveListTable = $('#tblSearchLeaveList').dataTable({ // inilize datatable on load time.
        "bFilter": true,
        "processing": true,
		"aaSorting": [],
        "sPaginationType": "full_numbers",
		"bAutoWidth" : false,
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [5]
        }]
    });
	
	
	//call datepicker on text box
	$("#txtFromDate").datepicker().on('changeDate', function(selected){
		startDate = new Date(selected.date.valueOf());
		$('#txtToDate').datepicker('setStartDate', startDate);
	});  
	
	$("#txtToDate").datepicker().on('changeDate', function(selected){
		startDate = new Date(selected.date.valueOf());
		$('#txtFromDate').datepicker('setEndDate', startDate);
	});
	
	$("#txtFromDate").click(function() {
        $("#txtFromDate").datepicker("show");
    });
	
    $("#txtToDate").click(function() {
        $("#txtToDate").datepicker("show");
    });
	
	$("#txtFromDate").change(function() {
        if ($("#txtFromDate").val() != "") {
            $("#txtFromDateError").text("");
            $("#txtFromDate").removeClass("errorStyle");
            $("#txtFromDate").datepicker("hide");
        }
    });
    $("#txtToDate").change(function() {
		
        if ($("#txtToDate").val() != "") {
            $("#txtToDateError").text("");
            $("#txtToDate").removeClass("errorStyle");
            $("#txtToDate").datepicker("hide");
        }		
    });
		
	// button reset
	$("#btnReset").on('click',function(){
		clearFormDetails('.searchLeaveList');
		$('#txtFromDate').datepicker('setEndDate', '');
		$('#txtToDate').datepicker('setStartDate', '');
		leaveListTable.fnClearTable();
	});
	
	//search functionality
	$("#btnSearch").on('click',function(){
		leaveListTable.fnClearTable();
		var flag = false;
		// form validation
		if(validTextField("#txtToDate","#txtToDateError","Please select to date") == true) {
			flag = true;
			$('#txtToDate').focus().datepicker("hide");
		}
		
		if(validTextField("#txtFromDate","#txtFromDateError","Please select from date") == true) {
			flag = true;
			$('#txtFromDate').focus().datepicker("hide");
		}
		
		if(flag == true) {
			return false;
		}
		/* if($("#leaveList_chkSearchFilter_checkboxgroup_allcheck").is(":checked") == false) {
			// call message pop up function.
			callSuccessPopUp('Alert','Please select any one status.');
			return true;
		} */
		
		var fromDate = $('#txtFromDate').val();
		var toDate = $('#txtToDate').val();
		var checkRejected = "";
		var checkCancelled = "";
		var checkApproval = "";
		var checkScheduled = "";
		var checkTaken = "";
		if ($('#leaveList0').is(":checked")== true) {
            checkRejected = "cancel";
        }
		if ($('#leaveList1').is(":checked")== true) {
            checkCancelled = "cancel";
        }
		if ($('#leaveList2').is(":checked")== true) {
           checkApproval = "schedule";
        }
		if ($('#leaveList3').is(":checked")== true) {
            checkScheduled = "approval";
        }
		if ($('#leaveList4').is(":checked")== true) {
            checkTaken = "taken";
        }
		// define value for post data in back end file
		var postData = {
			"operation":"showLeaveList",
			fromDate:fromDate,
			toDate:toDate,
			checkRejected:checkRejected,
			checkCancelled:checkCancelled,
			checkApproval:checkApproval,
			checkScheduled:checkScheduled,
			checkTaken:checkTaken
		};

		// call the function ajax call
		dataCall("./controllers/admin/leave_list.php", postData, function (data) {
			var totalLeaves = 0;
			var totLeaves = 0;
			if (data != null && data != "") {
				var parseData = jQuery.parseJSON(data);
				for (var i = 0; i < parseData.length; i++) {
					var entitlements = parseData[i].entitlements;
					var leave_status = parseData[i].leave_status;
					if(leave_status != "cancel") {
						totLeaves ++;
					}
					totalLeaves = parseInt(entitlements) - totLeaves;				
				}
				for (var j = 0; j < parseData.length; j++) {
					var date = parseData[j].date;
					var entitlements = parseData[j].entitlements;
					var name = parseData[j].name;
					var leave_status = parseData[j].leave_status;
					if(leave_status == "schedule") {
						leave_status = "pending approval"
					}
					var leave_reason = parseData[j].leave_reason;
					var leaveId = parseData[j].id;
					if(leave_status == "pending approval") {
						leaveListTable.fnAddData([date,name,1,leave_status,leave_reason,'<select class="selAction" name="selLeaveType" data-id = '+leaveId+' class="form-control" size="1"><option value="">Select Action</option><option value="cancel">cancel</option></select>']);
					}
					else if(leave_status == "approval") {
						leaveListTable.fnAddData([date,name,1,leave_status,leave_reason,'<select class="selAction" name="selLeaveType" data-id = '+leaveId+' class="form-control" size="1"><option value="">Select Action</option><option value="cancel">cancel</option></select>']);
					}

					else {
						leaveListTable.fnAddData([date,name,1,leave_status,leave_reason,'']);
					}					
				}
			}
		});
	});
	
	//save functionality
	$("#btnSave").on('click',function(){
		
		var dataTable = leaveListTable.fnGetData();
		
		var selArray = [];
		if(dataTable == undefined || dataTable.length == 0) {
			// call message pop up function.
			callSuccessPopUp('Alert','No data available in table');
			return true;
		}
		
		var data = $('#tblSearchLeaveList tbody tr').find('.selAction');
		if(data.length == 0) {
			// call message pop up function.
			callSuccessPopUp('Sorry','No leave are taken for action');
			return true;
		}
		for(i = 0;i<data.length;i++) {
			var selObject = {};
			var value = $($('#tblSearchLeaveList tbody tr').find('.selAction :selected')[i]).val();
			if(value != "") {
				selObject.val = value;
				selObject.id = $($('#tblSearchLeaveList tbody tr').find('.selAction')[i]).attr('data-id');
				selArray.push(selObject);
			}						
		}
		// define value for post data in back end file
		var postData = {
			"operation":"saveActionDetails",
			selArray:JSON.stringify(selArray)
		};

		// call the function ajax call
		dataCall("./controllers/admin/leave_list.php", postData, function (data) {
			if (data != null && data != "") {
				// call message pop up function.
				callSuccessPopUp('Success','Saved successfully !!!');	
				$("#btnSearch").click();
			}
		});
	});
}); 
