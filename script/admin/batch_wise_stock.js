$(document).ready(function() {
	debugger;
	$(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});

	$("#toDate").attr("disabled",true);
	$("#fromDate").attr("disabled",true);	

	$('.changeRadio').change(function() {
		if ($('#radToFromDate').prop( "checked") == true) {
	        $("#toDate").attr("disabled",false);
			$("#fromDate").attr("disabled",false);
	    }
	    else{
	    	$("#toDate").attr("disabled",true);
			$("#fromDate").attr("disabled",true);
			$("#toDate").val("");
			$("#fromDate").val("");
	    }
	});	
	$("#fromDate").change(function() {
		if ($("#fromDate").val() !='') {
			$("#fromDate").removeClass("errorStyle");
			$("#txtFromDateError").text("");
		}
	});
	$("#toDate").change(function() {
		if ($("#toDate").val() !='') {
			$("#toDate").removeClass("errorStyle");
			$("#txtToDateError").text("");
		}
	});	

	$("#clear").click(function(){
		clear();
	});
	google.charts.setOnLoadCallback(drawChart);
});

function drawChart() {
	var dataLoad;
	var chartWidth  = $("#page-content").width() -25;
	var bigArrayBatchWiseItem = [['Batch','Item Stock']];

	$("#viewReport").on("click",function(){
		var fromDate ;
		var toDate ;
		var arr = new Array();
		var opts = new Array();
		if ($('#radToday').prop( "checked") == true) {
			dataLoad = "today";
		}
		else if($('#radLastSevenDays').prop( "checked") == true) {
			dataLoad = "last7days";
		}
		else if($('#radToFromDate').prop( "checked") == true) {
			dataLoad = "betweenDate";
			fromDate = $("#fromDate").val();
			toDate = $("#toDate").val();
			if (fromDate =='') {
				$("#txtFromDateError").text("Please choose from date");
				$("#fromDate").addClass("errorStyle");
				$("#fromDate").focus();
				return false;
			}
			if (toDate =='') {
				$("#txtToDateError").text("Please choose from date");
				$("#toDate").addClass("errorStyle");
				$("#toDate").focus();
				return false;
			}
		}
		else if($('#radLastThirtyDays').prop( "checked") == true) {
			dataLoad = "last30days";
		}
		else if($('#radAllTime').prop( "checked") == true) {
			dataLoad = "all";
		}

		if (fromDate == undefined) {
			fromDate ="";
		}
		if (toDate == undefined) {
			toDate = "";
		}
		if (dataLoad==undefined) {
			return false;
		}
		var postData = {
			"operation" : "showChartData",
			"dataLoad" : dataLoad,
			"fromDate" : fromDate,
			"toDate" : toDate
		}
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/batch_wise_stock.php",
			data: postData,
			success: function(data) {				
				var parseData =  JSON.parse(data);

				if(parseData != null && parseData != ""){

					$(".chartDiv,#btnSaveExcel,#btnSavePdf").show();
					bigArrayBatchWiseItem = [['Batch','Item Stock']];

					$.each(parseData,function(index,v){

						smallArrayBatchWiseItem = [];
						smallArrayBatchWiseItem.push(parseData[index]['Batch']);
						smallArrayBatchWiseItem.push(parseInt(parseData[index]['Item Stock']));
						bigArrayBatchWiseItem.push(smallArrayBatchWiseItem);
					});


					var dataBatchWiseItem = google.visualization.arrayToDataTable([['','']]);
					var dataBatchWiseItem = google.visualization.arrayToDataTable(bigArrayBatchWiseItem);
					var optionsBatchWiseItem = {
				      title: 'Batch NO. Wise Item Stock','width':1200,'height':1000
				    };
				    var chartBatchWiseItem = new google.visualization.BarChart(document.getElementById('chart_div'));
				    chartBatchWiseItem.draw(dataBatchWiseItem, optionsBatchWiseItem);				    
				}

				else {
					clear();
					$('#messagemyModal').modal();
					$('#messageMyModalLabel').text("Alert");
					$('.modal-body').text("No data exist!!!");
					$("#btnSaveExcel,#btnSavePdf").hide();
					return false;
				}

				$('svg').css("max-width",$("#page-content").width()-50);//max-width for graph
				$('svg').css("min-height","400");
				/*Scroll down code*/
				$('html, body').animate({
			        scrollTop: $("#chart_div").offset().top
			    }, 1000);

				$("#btnSaveExcel").show();

				/*function to save in to excel file*/
				window.saveFile = function saveFile () {
					var opts = [{sheetid:'One',header:true}];
					var res = alasql('SELECT INTO XLSX("Batch-Wise Item Stock.xlsx",?) FROM ?',[opts,[parseData]]);
				}

			}
		});
	});

	$("#btnSavePdf").click(function(){
		$.print("#printableArea");
	});
}
function clear(){
	$(".chartDiv,#btnSaveExcel,#btnSavePdf").hide();
	$("#toDate,#fromDate").val("");
	$("#txtFromDateError,#txtToDateError").text("");
	$("#fromDate,#toDate").removeClass("errorStyle");
}