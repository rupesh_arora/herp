/*
 * File Name    :   designation.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Gupta
 * Created Date :   15 march, 2016
 * Description  :   This page use for load,save,update,delete,resotre operation
 */
 
var holidayTable; //defing a global variable for datatable
$(document).ready(function() {
	loader();    
    debugger;
    $('#btnReset').click(function() {
        clearFormDetails(".clearForm");
        $("#txtHolidayName").removeClass("errorStyle");
        $("#checkBox").prop( "checked", false );
        $('#txtHolidayName').focus();       
    });

    $("#txtDate").datepicker({ // date picker function
        autoclose: true
    });
    
    $("#txtDate").change(function(){
        if ($("#txtDate").val() !='') {
            $("#txtDate").removeClass("errorStyle");
            $("#txtDateError").text('');
        }
    });
    $("#form_dept").hide();
    $("#designation_list").addClass('list');    
    $("#tab_dept").addClass('tab-list-add');

    $("#tab_add_dept").click(function() { // click on add designation tab
        showAddTab();
        clearFormDetails(".clearForm");
    });
    $("#tab_dept").click(function() { // click on list designation tab
		showTableList();
    });

    /* click on button for save  inforamtion with validation */
    $("#btnSaveHoliday").click(function() {
        var flag = 'false';
       
        if ($("#txtDate").val() == '') {
            $("#txtDateError").text("Please enter date");
            $("#txtDate").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtHolidayName").val().trim() =='') {
            $('#txtHolidayName').focus();
            $("#txtHolidayNameError").text("Please enter holiday name");
            $("#txtHolidayName").addClass("errorStyle");
            flag = "true";
        }
        if (flag == 'true') {
            return false;
        }

        if ($('#checkBox').is(":checked")==false) {
            $('#checkBox').val(0);
        }
        else{
            $('#checkBox').val(1);
        }

        //ajax call for save operation
        var holidayName = $("#txtHolidayName").val().trim();
        var holidayDate = $("#txtDate").val();
        var availability = $('#checkBox').val();
        var hoildayDescription = $("#txtDescription").val().trim();
        hoildayDescription = hoildayDescription.replace(/'/g, "&#39");
        var postData = {
            "operation": "save",
            "holidayName": holidayName,
            "holidayDate" :holidayDate,
            "availability" :availability,
            "hoildayDescription": hoildayDescription
        }
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/holidays.php",
            datatype: "json",
            data: postData,
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Holiday saved successfully!!!");
                    $('#messagemyModal').modal();
                    clearFormDetails(".clearForm");
                    showTableList();
                }
                else if (data == 0) {
                    callSuccessPopUp('Alert','This holiday with same date already exist!!!');
                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    });
    //keyup functionality for remove validation style
    $("#txtHolidayName").keyup(function() {
        if ($("#txtHolidayName").val() != '') {
            $("#txtHolidayNameError").text("");
            $("#txtHolidayName").removeClass("errorStyle");
        }
    });

    //load Datatable data on page load
    if ($('.inactive-checkbox').not(':checked')) {
        holidayTable = $('#holidayTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "fnDrawCallback": function(oSettings) {
				$('.update').unbind();
                $('.update').on('click', function() { // update click event 
                    var data = $(this).parents('tr')[0];
                    var mData = holidayTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var name = mData["name"];
                        var description = mData["description"];
                        var date = mData["date"]
                        var repeat_annually = mData["repeat_annually"];
                        editClick(id, name, description,date,repeat_annually);
                    }
                });
				$('.delete').unbind();
                $('.delete').on('click', function() { // delete click event
                    var data = $(this).parents('tr')[0];
                    var mData = holidayTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/holidays.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [{
                "mData": "name"
            }, {
                "mData": "date"
            },{
                "mData": "description"
            }, {
                "mData": function(o) {
                    var data = o;
                    return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                        " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                        " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                        " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                        "  data-original-title='Delete'></i>";
                }
            }, ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [3,2]
            }]
        });
    }
    //checked or not checked functionality to show active or inactive data
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) {
            holidayTable.fnClearTable();
            holidayTable.fnDestroy();
            holidayTable = "";
            holidayTable = $('#holidayTable').dataTable({
                "bFilter": true,
                "processing": true,
				
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
					$('.restore').unbind();
                    $('.restore').on('click', function() { // restor click event
                        var data = $(this).parents('tr')[0];
                        var mData = holidayTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var checkholidayName = mData['name'];
                            var checkholidayDate = mData['date'];
                            restoreClick(id,checkholidayName,checkholidayDate);
                        }

                    });
                },
                "sAjaxSource": "controllers/admin/holidays.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [{
                    "mData": "name"
                }, {
                    "mData": "date"
                },{
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                    }
                }, ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [3,2]
                }]
            });
        } else {
            holidayTable.fnClearTable();
            holidayTable.fnDestroy();
            holidayTable = "";
            holidayTable = $('#holidayTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
					$('.update').unbind();
                    $('.update').on('click', function() { // for update click event
                        var data = $(this).parents('tr')[0];
                        var mData = holidayTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var name = mData["name"];
                            var description = mData["description"];
                            var date = mData["date"];
                            var repeat_annually = mData["repeat_annually"];
                            editClick(id, name, description,date,repeat_annually);

                        }
                    });
					$('.delete').unbind();
                    $('.delete').on('click', function() { // for delete click event
                        var data = $(this).parents('tr')[0];
                        var mData = holidayTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },
                "sAjaxSource": "controllers/admin/holidays.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [{
                    "mData": "name"
                }, {
                    "mData": "date"
                },{
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;

                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            "  data-original-title='Delete'></i>";
                    }
                }, ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [2,3]
                }]
            });
        }
    });

});
// use fucntion for edit designation for update 
function editClick(id, name, description,date,repeat_annually) {
    showAddTab();
    $('#tab_add_dept').html("+Update Holiday");
    $("#uploadFile").hide();
    $("#btnReset").hide();
    $("#btnAddDesignation").hide();
    $('#btnUpdateDesignation').removeAttr("style");
    $('#btnUpdateHoliday').removeAttr("style");
    $("#txtHolidayNameError").text("");
    $("#txtHolidayName").removeClass("errorStyle");
    $('#txtHolidayName').val(name);
    $("#txtDate").removeClass("errorStyle");
    $("#txtDateError").text("");
    $('#txtDate').val(date);
    if (repeat_annually == 0) {
        $('#checkBox').prop( "checked", false );
    }
    else{
        $('#checkBox').prop( "checked", true );
    }
    $('#txtDescription').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
    $(".btnContainer").hide();
    //keyup
    $("#txtDate").datepicker({ // date picker function
        autoclose: true
    });
    $("#txtDate").click(function() {
        $(".datepicker").css({
            "z-index": "99999"
        });
    });
    $("#checkPointer").click(function(){
        if ($('#checkBox').is(":checked") == true) {
            $("#checkBox").prop('checked', false);
        }
        else{
            $("#checkBox").prop('checked', true);
        }        
    });
    $("#btnUpdateHoliday").click(function() {
        var flag = "false";
        if ($("#txtHolidayNameError").text() != "") {
            flag = "true";
        }
        if ($("#txtDateError").text() != "") {
            flag = "true";
        }

        if ($("#txtHolidayName").val().trim() =='') {
            $('#txtHolidayName').focus();
            $("#txtHolidayNameError").text("Please enter holiday name");
            $("#txtHolidayName").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtDate").val() == '') {
            $('#txtDate').focus();
            $("#txtDateError").text("Please enter date");
            $("#txtDate").addClass("errorStyle");
            flag = "true";
        }


        if (flag == "true") {
            return false;
        }
		else{
            if ($('#checkBox').is(":checked")==false) {
            $('#checkBox').val(0);
            }
            else{
                $('#checkBox').val(1);
            }

			var holidayName = $("#txtHolidayName").val();
			var hoildayDescription = $("#txtDescription").val();
            var holidayDate = $("#txtDate").val();
            var availability = $('#checkBox').val();
			hoildayDescription = hoildayDescription.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/holidays.php",
					data: {
						"operation": "update",
						"id": id,
                        "holidayName": holidayName,
						"availability": availability,
						"hoildayDescription": hoildayDescription,
                        "holidayDate" : holidayDate
					},
					success: function(data) {
						if (data != null && data != "" && data != "0") {
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("holiday updated successfully!!!");
							$('#messagemyModal').modal();
							holidayTable.fnReloadAjax();
                            showTableList();
						}
						else if (data == "0") {
							$('.close-confirm').click();
                            callSuccessPopUp('Alert','This holiday with same date already exist!!!');               
							$("#txtHolidayName").focus();
						}
					},
					error: function() {
						//$('#myModal').modal('hide');
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
				});
			});
		}
    });

}
// click function to delete designation
function deleteClick(id) {
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Delete holiday");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteHoliday("'+type+'","","");');
}

//Click function for restore the departement
function restoreClick(id,checkholidayName,checkholidayDate) {
    $('#selectedRow').val(id);
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Restore holiday");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteHoliday("'+type+'","'+checkholidayName+'","'+checkholidayDate+'");');
}

//function for delete and restore the designationtList
function deleteHoliday(type,checkholidayName,checkholidayDate) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/holidays.php",
            data: {
                "operation": "delete",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Holiday deleted successfully!!!");
                    $('#messagemyModal').modal();
                    holidayTable.fnReloadAjax();
                } else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This Holiday is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    } else {
        var id = $('#selectedRow').val();
        $('.modal-body').text("");
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/holidays.php",
            data: {
                "operation": "restore",
                "id": id,
                "checkholidayName" : checkholidayName,
                "checkholidayDate" : checkholidayDate
            },
            success: function(data) {
                if (data == "1") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Holiday restored successfully!!!");
                    $('#messagemyModal').modal();
                    holidayTable.fnReloadAjax();
                }
                else if (data =="0") {
                    callSuccessPopUp("Alert","This holiday with same date already exist. So you can't restore it.!!!");
                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    }
}

function showTableList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#form_dept").hide();
    $(".blackborder").show();

    $("#tab_add_dept").removeClass('tab-detail-add');
    $("#tab_add_dept").addClass('tab-detail-remove');
    $("#tab_dept").removeClass('tab-list-remove');    
    $("#tab_dept").addClass('tab-list-add');
    $("#department_list").addClass('list');


    $("#uploadFile").show();
    $("#btnReset").show();
    $("#btnAddDesignation").show();
    $('#btnUpdateDesignation').hide();
    $('#tab_add_dept').html("+Add Holiday");
    clear();
}

function showAddTab(){
    $("#form_dept").show();
    $(".blackborder").hide();
   
    $("#tab_add_dept").addClass('tab-detail-add');
    $("#tab_add_dept").removeClass('tab-detail-remove');
    $("#tab_dept").removeClass('tab-list-add');
    $("#tab_dept").addClass('tab-list-remove');
    $("#designation_list").addClass('list');
    $('#txtHolidayName').focus();
}


// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});