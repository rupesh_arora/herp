var subTypeTable;
var dynamicTable;
var tdLength =0;
var table =[];
var updateTable =[];
var glLedgerarray = [];
var aTable =[];
var textBox ='';
var html ;
$(document).ready(function(){
    loader();
    debugger;
    removeErrorMessage();
     bindDepartment();
     bindBudget();
	
	/*Hide add ward by default functionality*/
	$("#advanced-wizard").hide();
	$("#subTypeList").css({
        "background-color": "#fff"
    });
	$("#tabsubTypeList").css({
        "background-color": "#0c71c8",
        "color": "#fff"
    });

	/*Click for add the sub Type*/
    $("#tabAddsubType").click(function() {
        $("#advanced-wizard").show();
        $(".blackborder").hide();
        $("#tebleContent").hide();
		clear();
        $("#tabAddsubType").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tabsubTypeList").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#subTypeList").css({
            "background": "#fff"
        });
    });

    $('#txtName').keyup(function() {
         if($('#txtName').val() != '') {
            $('#txtNameError').text('');
            $('#txtName').removeClass("errorStyle"); 
        }
    });   
	
	//Click function for show the sub_type lists
    $("#tabsubTypeList").click(function() {
        clear();
        tabsubTypeList();
    });

    $('#btnSubmit').click(function() {
        var flag = false;

        if($('#selDepartment').val() == null) {
            $('#selDepartmentError').text('Please select department');
            $('.multiselect').focus();
            $('.multiselect').addClass("errorStyle"); 
            flag = true;
        }

        if($('#selBudget').val() == '-1') {
            $('#selBudgetError').text('Please select budget period');
            $('#selBudget').focus();
            $('#selBudget').addClass("errorStyle"); 
            flag = true;
        }
        if(flag == true) {
            return false;
        }

        var departmentId = $('#selDepartment').val();
        var budgetId = $('#selBudget').val();

        var postData = {
            departmentId : JSON.stringify(departmentId),
            budgetId : budgetId,
            operation : "save"
        }


        $.ajax(
            {                   
            type: "POST",
            cache: false,
            url: "controllers/admin/department_budget.php",
            datatype:"json",
            data: postData,
            
            success: function(data) {
                if(data != "0" && data != ""){
                    callSuccessPopUp('Success','Saved successfully!!!');
                    tabsubTypeList();
                    subTypeTable.fnReloadAjax();
                    clear();
                }
                else {
                    $('#txtNameError').text('Sub type is already exist');
                    $('#txtName').focus();
                    $('#txtName').addClass('errorStyle');
                }
            },
            error: function(){

            }
        });
    });

    

    //if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code 
        subTypeTable = $('#tblDepartmentBudget').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "autoWidth": false,
            "fnDrawCallback": function(oSettings) {    


                $("#tblDepartmentBudget tbody tr").on('dblclick', function () { 

                   subTypeTable.$('tr.selected').removeClass('selected');
                   $(this).addClass('selected');

                   var mData = subTypeTable.fnGetData(this); // get datarow
                   $("#selectedRow").val(mData['id']);
                   var selectedColumn = $.trim(mData.name.split('(')[0]);
                    var postData = {
                        operation : "showColumnName"
                    }
                    $.ajax({                   
                        type: "POST",
                        cache: false,
                        url: "controllers/admin/department_budget.php",
                        datatype:"json",
                        data: postData,
                        
                        success: function(data) {
                            if(data != "0" && data != ""){
                                aTable =[];
                                html ='';
                                html = "<div style=margin-top:1%;' class='blackborder'>"+
                                        "<div class='table-responsive'>" +           
                                            "<table id='tblBudgetMatrix' class='table table-vcenter table-responsive table-condensed table-bordered'>"+
                                                "<thead>"+
                                                    "<tr>"+
                                                        "<th class='text-center'>Account No.</th>"+
                                                        "<th class='text-center'>GL Account</th>"+
                                                        "<th class='text-center'>Amount</th>";

                                var parseData = jQuery.parseJSON(data);
                                var j=9;
                                for(i=1; i<parseData.length-8; i++){
                                    if(selectedColumn == parseData[j].COLUMN_NAME){
                                        html +="<th class='text-center edit'>"+parseData[j].COLUMN_NAME+"</th>";
                                        j++;
                                    }
                                    else{
                                        html +="<th class='text-center'>"+parseData[j].COLUMN_NAME+"</th>";
                                        j++;
                                    }
                                }

                                $.grep(parseData,function(v,i){
                                    if(i != 0 && i != 3 && i != 4){
                                        aTable.push(v.COLUMN_NAME);              
                                    }                                    
                                });
                                $.grep(parseData,function(v,i){
                                    if(i != 0 && i != 1 && i != 2 && i != 3 && i != 4 && i != 5 && i != 6  && i != 7){
                                        updateTable.push(v.COLUMN_NAME);          
                                    }                                    
                                });
                                html +="</tr>"+
                                            "</thead>"+
                                        "</table>"+
                                        "<input id='selectedTableRow' type='hidden' value='0' />"+
                                    "</div>"+
                                "</div>";

                            
                                $('#mainTableContent').html(html);
                                $("#blackborderTable").hide();
                                $("#tebleContent").show();
                                var columnIndex = $('#tblBudgetMatrix .edit').index();

                                dynamicTable = $('#tblBudgetMatrix').dataTable({
                                    "bFilter": true,
                                    "processing": true,
                                    "sPaginationType": "full_numbers",
                                    "bAutoWidth": false,
                                    "fnDrawCallback": function(oSettings) {
                                        $('#tblBudgetMatrix').unbind();
                                        $('#tblBudgetMatrix').on('dblclick', '.inputClass', function() {
                                            var previousData = $(this).text();
                                            var parentObj = $(this);
                                            $(this).addClass("cellEditing");                  
                                            $(this).html("<input type='number' class='inputBox'  value='" + previousData + "'/>");
                                            $(this).children().first().keypress(function (e) { 
                                                if (e.which == 13) {
                                                    var newContent = $(this).val();
                                                    if (newContent !='') {
                                                        $(this).parent().removeClass("cellEditing");    
                                                        $(this).parent().text(newContent);
                                                    }
                                                    if ($(this).closest('td').hasClass('acceptNumber')) {
                                                        var floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
                                                        var value = floatRegex.test(newContent);
                                                        if(value == false){
                                                            callSuccessPopUp('Alert',"Enter number only");
                                                            return false;
                                                        }   
                                                    }                                    
                                                }
                                                // for disable alphabates keys
                                                if ($(this).closest('td').hasClass('acceptNumber')) {
                                                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { 
                                                        return false;
                                                    }
                                                }
                                            });
                                            $(this).children().first().blur(function (e) {
                                                var newContent = $(this).val();
                                                if ($(this).closest('td').hasClass('acceptNumber')) {
                                                    var floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
                                                    var value = floatRegex.test(newContent);
                                                    if(value == false){
                                                        callSuccessPopUp('Alert',"Enter number only");
                                                        return false;
                                                    }
                                                }
                                                if (newContent !='') {
                                                    $(this).parent().removeClass("cellEditing");    
                                                    $(this).parent().text(newContent);
                                                }
                                            });
                                            /*Code to focus on last charcter*/
                                            var SearchInput = $(this).children();
                                            SearchInput.val(SearchInput.val());
                                            var strLength= SearchInput.val().length;
                                            SearchInput.focus();
                                        });
                                    },
                                });                           
                                
                                glLedgerarray = [];
                                tdLength = dynamicTable.find('thead th').length-2;                                         
                                textBox = "<input type ='number' class='inputBox'>";                               
                                empty = "";                               
                                var postData = {
                                    operation : "showGLAccount",
                                    id : $("#selectedRow").val()
                                }

                                 $.ajax({                   
                                    type: "POST",
                                    cache: false,
                                    url: "controllers/admin/department_budget.php",
                                    datatype:"json",
                                    data: postData,
                                    
                                    success: function(data) {
                                        if(data != "0" && data != ""){
                                            var parseData = jQuery.parseJSON(data);
                                            var dynamicVariableGenerator  = [];
                                            for(k=0; k<tdLength; k++){
                                                if(k == columnIndex-2){
                                                    dynamicVariableGenerator.push(textBox);
                                                }
                                                else{
                                                    dynamicVariableGenerator.push(empty);
                                                }
                                                
                                            }
                                            for(i=0; i<parseData.length; i++){
                                                glLedgerarray.push(parseData[i].id);
                                                var newarray = [parseData[i].account_no,parseData[i].account_name];
                                                var concatarray = newarray.concat(dynamicVariableGenerator);
                                                dynamicTable.fnAddData(concatarray);
                                            }

                                            var postData = {
                                                "operation" : "showBudgetMatrix",
                                                "aTable" : JSON.stringify(aTable),
                                                "id" : $("#selectedRow").val()
                                            }

                                             $.ajax({                   
                                                type: "POST",
                                                cache: false,
                                                url: "controllers/admin/department_budget.php",
                                                datatype:"json",
                                                data: postData,
                                                
                                                success: function(data) {
                                                    if(data != "0" && data != ""){
                                                        var parseData = jQuery.parseJSON(data);
                                                        var row = dynamicTable.fnGetNodes();
                                                        for(i=0; i<parseData.length; i++){
                                                            var dynamicTableTdLength = $(row[i]).find('td').length;
                                                            var counter =6;
                                                            var amount = 0;
                                                            $($(row[i]).find('td')[columnIndex]).addClass('inputClass');
                                                            for(j=3; j<dynamicTableTdLength; j++){ 
                                                                if(parseData[i][aTable[counter]] != null){
                                                                    $($(row[i]).find('td')[j]).text(parseData[i][aTable[counter]]);
                                                                    $($(row[i]).find('td')[j]).attr('data-id',parseData[i].id);
                                                                    amount += parseInt(parseData[i][aTable[counter]]);
                                                                }                                                               
                                                                
                                                                counter++;                                                                
                                                            }
                                                            $($(row[i]).find('td')[2]).text(amount);
                                                        }
                                                    } 
                                                },
                                                error: function(){

                                                }
                                            });
                                        } 
                                    },
                                    error: function(){

                                    }
                                });

                                $("#btnSaveTableData").unbind();
                                $("#btnSaveTableData").on('click', function(){

                                    var tableData = [];
                                    var whereData = [];
                                    var dynamic = [];
                                    var flag = false;
                                    var rowCollection = dynamicTable.fnGetNodes();
                                    var checkUpdate = $(rowCollection).find('.inputBox').length;
                                    if(checkUpdate != 0){
                                         debugger;
                                        for(i=0; i<rowCollection.length; i++){
                                            var obj = {};
                                            var subTdLength = $(rowCollection[i]).find('td').length;
                                            var fixedLength = 2;
                                            for(j=0; j<subTdLength-2; j++){                                            
                                                dynamic[j] = "Q"+(j+1);
                                                if($($(rowCollection[i]).find('td')[fixedLength]).find('input').val() == undefined){
                                                    obj[dynamic[j]] = "0";                                                    
                                                }
                                                else{
                                                    obj[dynamic[j]] = $($(rowCollection[i]).find('td')[fixedLength]).find('input').val();
                                                }
                                                fixedLength++;
                                                //bigAyyay.push($($(rowCollection[i]).find('.inputBox')[j]).val());
                                            }
                                            tableData.push(obj);
                                            //tableData.push(bigAyyay);
                                        }

                                        var departmentBudgetId = $("#selectedRow").val();
                                       

                                        var postData = { 
                                            "operation" : "saveBdgetMatrix",
                                            "tableData" : JSON.stringify(tableData),
                                            "glLedgerarray" : JSON.stringify(glLedgerarray),
                                            "aTable" : JSON.stringify(aTable),
                                            "id" : departmentBudgetId
                                        }
                                    }
                                    else{
                                         debugger;
                                        for(i=0; i<rowCollection.length; i++){
                                            var obj = {};
                                            var objId = {};
                                            var id ;
                                            var subTdLength = $(rowCollection[i]).find('td').length;
                                            var fixedLength = 2;
                                            var getColumn = $('#tblBudgetMatrix .edit').index();
                                            for(j=0; j<subTdLength-2; j++){                                            
                                                dynamic[j] = "Q"+(j+1);
                                                obj[dynamic[j]] = $($(rowCollection[i]).find('td')[fixedLength]).text();
                                                id = $($(rowCollection[i]).find('td')[getColumn]).attr('data-id');
                                                //bigAyyay.push($($(rowCollection[i]).find('.inputBox')[j]).val());
                                                fixedLength++;
                                            }
                                            tableData.push(obj);
                                            whereData.push(id);
                                        }

                                        var departmentBudgetId = $("#selectedRow").val();
                                       

                                        var postData = { 
                                            "operation" : "updateBdgetMatrix",
                                            "tableData" : JSON.stringify(tableData),
                                            "whereData" : JSON.stringify(whereData),
                                            "updateTable" : JSON.stringify(updateTable),
                                            "id" : departmentBudgetId
                                        }
                                    }
                                   

                                     $.ajax({                   
                                        type: "POST",
                                        cache: false,
                                        url: "controllers/admin/department_budget.php",
                                        datatype:"json",
                                        data: postData,
                                        
                                        success: function(data) {
                                            if(data != "0" && data != ""){
                                                callSuccessPopUp('Success','Saved successfully!!!');
                                            }
                                        },
                                        error: function(){

                                        }
                                    });

                                });

                                 $("#btnBack").on('click', function(){
                                    $("#blackborderTable").show();
                                    $("#tebleContent").hide();
                                 });
                            }
                        },
                        error: function(){

                        }
                    });
                });        
                
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = subTypeTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/department_budget.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "name"
                }, {
                    "mData": "department"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [1,2]
            }]

        });

        $('.multiselect').on('click', function(){
            $('#selDepartmentError').text('');
            $('.multiselect').removeClass("errorStyle");
        });
   // }

   /* $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            subTypeTable.fnClearTable();
            subTypeTable.fnDestroy();
            subTypeTable = "";
            subTypeTable = $('#tblDepartmentBudget').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "autoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = subTypeTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var name = mData["name"];                      
                            var description = mData["description"];
                            restoreClick(id,name,description);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/department_budget.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "name"
                    }, {
                        "mData": "department"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1,2]
                }]
            });
        } else { // show active data on unchecked   
            subTypeTable.fnClearTable();
            subTypeTable.fnDestroy();
            subTypeTable = "";
            subTypeTable = $('#tblDepartmentBudget').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "autoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = subTypeTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var name = mData["name"];                      
                            var description = mData["description"];
                            editClick(id,name,description);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = subTypeTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/department_budget.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "name"
                    }, {
                        "mData": "department"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1,2]
                }]
            });
        }
    });*/


    $('#btnReset').click(function(){
        clear();
    });
});

/*function editClick(id,name,description) {
    
    $('.modal-body').text("");
    $('#myModalLabel').text("Update sub types");
    $('.modal-body').html($(".step").html()).show();
    $('#myModal').modal('show');

    $('#myModal').on('shown.bs.modal', function() {
       $('#myModal #txtName').focus();
    });
    
    $("#myModal #btnUpdate").removeAttr("style");
    
    $("#myModal #txtName").removeClass("errorStyle");
    $("#myModal #txtNameError").text("");

    
    $('#myModal #txtName').val(name);
    $('#myModal #txtDescription').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
    //validation
    //remove validation style
    // edit data function for update 
    $("#myModal #txtName").keyup(function() {
        if ($("#myModal #txtName").val() != '') {
            $("#myModal #txtNameError").text("");
            $("#myModal #txtName").removeClass("errorStyle");
        }
    });
    
    
    $("#myModal #btnUpdate").click(function() { // click update button
        var flag = false;
        if ($("#myModal #txtName").val()== '') {
            $("#myModal #txtNameError").text("Please enter name");
            $("#myModal #txtName").focus();
            $("#myModal #txtName").addClass("errorStyle");
            flag = true;
        }
        
        if(flag == true) {
            return false;
        }

        var name = $("#myModal #txtName").val().trim();
        var description = $("#myModal #txtDescription").val().trim();
        description = description.replace(/'/g, "&#39");
        var id = $('#selectedRow').val();
        
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
        var postData = {
            "operation": "update",
            "name": name,
            "description": description,
            "id": id
        }
        $.ajax( //ajax call for update data
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/sub_type.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('#myModal').modal('hide');
                        $('.close-confirm').click();
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Sub type updated successfully!!!");
                        $('#messagemyModal').modal();
                        subTypeTable.fnReloadAjax();
                        clear();
                    }
                    else {                
                        $("#myModal #txtNameError").text("Sub type is already exist");
                        $("#myModal #txtName").focus();               
                        $("#myModal #txtName").addClass('errorStyle');
                    }
                },
                error: function() {
                    $('.close-confirm').click();
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                    $('#messagemyModal').modal();
                }
            }); // end of ajax
        });
    });*/
//} // end update button
function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete department budget");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteSubType("' + type + '");');
} // end click fucntion

/*function restoreClick(id,name,description) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore sub type");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteSubType("' + type + '","'+id+'","'+name+'","'+description+'");');
}*/
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteSubType(type) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/department_budget.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Department budget deleted successfully!!!");
                    $('#messagemyModal').modal();
                    subTypeTable.fnReloadAjax();
                } 
                else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This department budget is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/sub_type.php",
            data: {
                "operation": "restore",
                "name":name,
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Sub type restored successfully!!!");
                    $('#messagemyModal').modal();
                    subTypeTable.fnReloadAjax();
                }
                else {
                    callSuccessPopUp('Sorry','Sub type already exist you can not restore !!!');
                }
            },
            error: function() {             
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary unavailable to respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}

function tabsubTypeList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
    $("#tabAddsubType").css({
        "background": "#fff",
        "color": "#000"
    });
    $("#tabsubTypeList").css({
        "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
        "color": "#fff"
    });
    $("#subTypeList").css({
        "background-color": "#fff"
    });
    clear();
}
function clear() {
    $("#selDepartment").multiselect("destroy");
    bindDepartment();
    $('#selBudget').val('-1');
    $('#selBudgetError').text('');
    $('#selDepartmentError').text('');
    $('#selBudget').removeClass("errorStyle");
    $('#selDepartment').removeClass("errorStyle");
    $('#selBudget').focus();
}

// define function for bind department
function bindDepartment() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/opd_registration.php",
        data: {
            "operation": "showDepartment"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option ;
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].department + "</option>";
                }
                $('#selDepartment').html(option);
                $('#selDepartment').multiselect({includeSelectAllOption: true,enableFiltering:true,numberDisplayed: 1 });
            }
        },
        error: function() {}
    });
}

// define function for bind department
function bindBudget() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/department_budget.php",
        data: {
            "operation": "showBudget"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option ="<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selBudget').html(option);
            }
        },
        error: function() {}
    });
}