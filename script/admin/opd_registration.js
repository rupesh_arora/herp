/*
 * File Name    :   opd_registration.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load and send data to back hand.
 */
var hrnPatientid ='';
var insurancePayment;
$(document).ready(function() {
    debugger;
    var flag = "false";
	loader();
     $(".insurance").hide();
	/* define global variable */
	$(".amountPrefix").text(amountPrefix);
	var serType;
	var serName;
	var serFee;
	var departmentName;
    insurancePayment = '';

    // bind nsurance company
    $("#selInsuranceCompany").change(function() {
        var option = "<option value='-1'>--Select--</option>";
        if ($("#selInsuranceCompany :selected") != "") {
            var value = $("#selInsuranceCompany :selected").val();
            
            //on change call this function for load state
            //bindInsuranceCompany(value);
            bindInsuranceSchemeName(value,hrnPatientid);
            //bindPlanName(value)
        } 
        else {
            $("#selInsuranceCompany").html(option);
        }   
    });

    // bind company schemeon change company
    /*$("#selInsuranceCompany").change(function() {
        var option = "<option value='-1'>--Select--</option>";
        
        if ($("#selInsuranceCompany :selected") != "") {
            var value = $("#selInsuranceCompany :selected").val();
            
            //on change call this function for load state
            bindInsuranceSchemeName(value, null); 
        } else {
            $("#selInsurancePlan").html(option);
        }   

        if ($("#selInsuranceCompany").val() != "") {
            $("#selInsuranceCompanyError").text("");
            $("#selInsuranceCompany").removeClass("errorStyle");
        }
    });*/ 
    
    // bind company schemeon change company
    $("#selInsurancePlan").change(function() {
        var option = "<option value='-1'>--Select--</option>";
        
        if ($("#selInsurancePlan :selected") != "") {
            var value = $("#selInsurancePlan :selected").val();
            
            bindPlanName(value,hrnPatientid);
        } else {
            $("#selInsuranceScheme").html(option);
        }   

        if ($("#selInsurancePlan").val() != "") {
            $("#selInsurancePlanError").text("");
            $("#selInsurancePlan").removeClass("errorStyle");
        }
    });

    bindServiceType();			// calling for load service type   
    bindVisitType();			// calling for load visit type
	bindDepartment();			// calling for load department
    $("#txtHrn").focus();
    $('#selServiceType').change(function() {				// calling to load service on change on service type
        var htmlRoom = "<option value='-1'>-- Select --</option>";

        if ($('#selServiceType :selected').val() != -1) {
            var value = $('#selServiceType :selected').val();
			serType = $("#selServiceType :selected").text();	// define service type for print value
            bindServices(value);
        } else {
            $('#selServices').html(htmlRoom);
        }
    });

    $('#selServices').change(function() {	
        $('#txtServiceFee').val('')
    	// calling to show service fee on change on service 
        var htmlServiceFee = "";
        /*$('#txtServiceFee').val('');*/
        if($('#selPaymentMode :selected').val() == 'insurance'){
            if ($('#selServices :selected').val() != -1) {
                var value = $('#selServices :selected').val();  // define service name for print value
                var companyId =  $('#selInsuranceCompany :selected').val();
                var planId =  $('#selInsurancePlan :selected').val();
                var schemeId =  $('#selInsuranceScheme :selected').val();
                serName = $("#selServices :selected").text();
                insurancePayment = 'insurance';
                bindInsuranceServiceFee(value, companyId, planId, schemeId);
                
            } else {
                $('#txtServiceFee').val(htmlServiceFee);
            }
        }
        else{
            if ($('#selServices :selected').val() != -1) {
                var value = $('#selServices :selected').val();  // define service name for print value
                serName = $("#selServices :selected").text();
                insurancePayment = 'cash';
                bindServiceFee(value);
            } else {
                $('#txtServiceFee').val(htmlServiceFee);
            }
        }        
    });

    $("#btnSelect").click(function() {						 //getting patient details click on select button
		filledClear();						// clear fill  data on click load
		
		var getHRN = $("#txtHrn").val();
        if ($("#txtHrn").val() != '-1') {
            $("#txtHrnError").text("");
            $("#txtHrn").removeClass("errorStyle");
        }

        var flag = "false";
        
		
		if(getHRN.length != 9){
			 $("#txtHrnError").text("Please enter valid patient Id");
			$("#txtHrn").addClass("errorStyle");
            $("#txtHrn").focus();
			flag = "true";
		}
        if ($("#txtHrn").val() == "") {
            $("#txtHrnError").text("Please enter patient Id");
            $("#txtHrn").addClass("errorStyle");
            $("#txtHrn").focus();
            $("#selPaymentMode").removeClass("errorStyle");
            $("#selServiceType").removeClass("errorStyle");
            $("#selServices").removeClass("errorStyle");
            $("#selVisitType").removeClass("errorStyle");
            flag = "true";
        }
        if (flag == "true") {
            return false;
        }
        var getHRN = $("#txtHrn").val();
		var patientPrefix = getHRN.substring(0, 3);
		getHRN = getHRN.replace ( /[^\d.]/g, '' ); 
		getHRN = parseInt(getHRN);

        
        $.ajax({							//call ajax for show details
            type: "POST",
            cache: "false",
            url: "controllers/admin/opd_registration.php",
            data: {
                "operation": "selectHRNInfo",
                "GetHRN": getHRN,
                "patientPrefix": patientPrefix
            },
            success: function(data) {
                if (data != "0" && data != "2") {

                    var parseData = jQuery.parseJSON(data);

                    for (var i = 0; i < parseData.length; i++) {
                        $("#txtFirstName").val(parseData[i].first_name);
                        $("#txtLastName").val(parseData[i].last_name);
                        $("#txtMobile").val(parseData[i].mobile);
                        var getDOB = parseData[i].dob;
                    }
                    var new_age = getAge(getDOB);

                    var split = new_age.split(' ');
                    var age_years = split[0];
                    var age_month = split[1];
                    var age_day = split[2];

                    $("#txt_year").val(age_years);
                    $("#txt_month").val(age_month);
                    $("#txt_day").val(age_day);
                    $("#txtHrn").attr("disabled", "disabled");
                    bindInsuranceCompany(getHRN);

                } else {
                    $("#txtHrnError").text("Please enter patient Id");
                    $("#txtHrn").addClass("errorStyle");                   
                }

            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });

    });
   
    //save information with check validation on save button
    $("#btnBookIpd").click(function() {					
        flag = "false";
        
        if ($("#selVisitType").val() == '-1') {
            $("#selVisitType").focus();
            $("#selVisitTypeError").text("Please select visit type");
            $("#selVisitType").addClass("errorStyle");
            flag = "true";
        }
        if ($("#selServices").val() == '-1') {
            $("#selServices").focus();
            $("#selServicesError").text("Please select services");
            $("#selServices").addClass("errorStyle");
            flag = "true";
        }
        if ($("#selServiceType").val() == '-1') {
            $("#selServiceType").focus();
            $("#selServiceTypeError").text("Please select service type");
            $("#selServiceType").addClass("errorStyle");
            flag = "true";
        }
        if($("#selPaymentMode").val() == "-1"){
            $("#selPaymentMode").focus();
            $("#selPaymentModeError").text("Please select payment mode");
            $("#selPaymentMode").addClass("errorStyle");
            flag = "true";
        }
        if($("#selPaymentMode").val() == "insurance"){

            if($("#selInsuranceCompany").val() == "-1"){
                $("#selInsuranceCompany").focus();
                $("#selInsuranceCompanyError").text("Please select insurance company");
                $("#selInsuranceCompany").addClass("errorStyle");
                flag = "true";
            }
            if($("#selInsurancePlan").val() == "-1"){
                $("#selInsurancePlan").focus();
                $("#selInsurancePlanError").text("Please select insurance plan");
                $("#selInsurancePlan").addClass("errorStyle");
                flag = "true";
            }
            if($("#selInsuranceScheme").val() == "-1"){
                $("#selInsuranceScheme").focus();
                $("#selInsuranceSchemeError").text("Please select insurance scheme");
                $("#selInsuranceScheme").addClass("errorStyle");
                flag = "true";
            }
            if($("#selInsuranceNo").val() == ""){
                $("#selInsuranceNo").focus();
                $("#selInsuranceNoError").text("Please enter insurance no.");
                $("#selInsuranceNo").addClass("errorStyle");
                flag = "true";
            }
        }
        if ($("#txtHrn").val() == "") {
            $("#txtHrn").focus();
            $("#txtHrnError").text("Please enter patient Id");
            $("#txtHrn").addClass("errorStyle");
            flag = "true";
        }
        if (flag == "true") {
            return false;
        }
		var visitId = $("#txtVisitId").val();
        var patientId = $("#txtHrn").val();
        var hrn = parseInt($("#txtHrn").val().replace ( /[^\d.]/g, '' ));
		
		
		
		serFee =  $("#txtServiceFee").val();		// define service fee for print value
        
        var service = $("#selServices").val();
        var insuranceNo = $("#selInsuranceNo").val();
        var insuranceCompany = $("#selInsuranceCompany").val();
        var insuranceScheme = $("#selInsurancePlan").val();
        var schemePlan = $("#selInsuranceScheme").val();
		var paymentMode = $("#selPaymentMode").val();
        var visitType = $("#selVisitType").val();
        var txtVisitType = $("#selVisitType :selected").text();
        var serviceFee = $("#txtServiceFee").val();
		var selDepartment = $("#selDepartment").val();
		departmentName = $("#selDepartment option:selected").text();
        var remarks = $("#txtRemarks").val();
        var postData = {
            "operation": "save",
            "hrn": hrn,
            "service": service,
            "visitType": visitType,
            "insuranceNo": insuranceNo,
            "paymentMode": paymentMode,
            "serviceFee": serviceFee,
            "txtVisitType": txtVisitType,
			"selDepartment": selDepartment,
            "remarks": remarks,
            "insurancePayment":insurancePayment,
            "insuranceCompany" :insuranceCompany,
            "insuranceScheme" : insuranceScheme,
            "schemePlan" : schemePlan
        }

        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/opd_registration.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != ""  && data != "Follow up" && data != "visit" && data != "Insurance") {
                    $('#regModalLabel').text("Success");
                    //$('#printModalBody').text();
                    if (data != "Follow up" && data != "visit" && data != "Insurance" && data !='') {
                        data1 = data.split(']')[0]+']';
                        data2 = data.split(']')[1];
                        var parseData = jQuery.parseJSON(data1);
                    }
                    else{
                        var parseData = jQuery.parseJSON(data);
                    }                        

                    for (var i = 0; i < parseData.length; i++) {
                        $("#lblPatientName").text(parseData[i].patient_name);
                        if (departmentName != "-- Select --") {
                            $("#lblDepartment").text(departmentName);
                        }
                        else{
                             $("#lblDepartment").text("N/A");
                        }
                        $("#lblPrintPatient").text(parseData[i].patient_name);
                        if(departmentName == "-- Select --"){
                            $("#lblPrintDepartment").text("N/A");
                        }
						else{
                             $("#lblPrintDepartment").text(departmentName);
                        }
						$("#lblPrintVisitType").text(parseData[i].visit_type);
						var prefix = parseData[i].prefix;
						var visitId = parseData[i].reg_id;
						var gender = parseData[i].gender;
                        var getDOB = parseData[i].dob;

                    }
					var visitIdLength = visitId.length;
					for (var i=0;i<6-visitIdLength;i++) {
						visitId = "0"+visitId;
					}
					visitId = prefix+visitId;
					$("#lblPrintVisitId").text(visitId);
					$("#lblOPDRegistrationId").text(visitId);
					$("#lblPatientId").text(patientId);
					$("#lblPrintPatientId").text(patientId);
					if(gender == "F"){
						$("#lblGender").text("Female");
						$("#lblPrintGender").text("Female");
					}
					else{
						$("#lblGender").text("Male");
						$("#lblPrintGender").text("Male");
					}
                    if (data2 != undefined && data2 != '') {
                        $('.showRemainigAmount').show();
                        $("#lblRemainingAmount").text(amountPrefix+' '+data2);
                    }
                    else {
                        $('.showRemainigAmount').hide();
                    }
                    

                    var new_age = getAge(getDOB);

                    var split = new_age.split(' ');
                    var age_years = split[0];
                    var age_month = split[1];
                    var age_day = split[2];

                    $("#lblAge").html(age_years + "yr" + " " + age_month + "mon" + " " + age_day + "day");
                    $("#lblPrintAge").html(age_years + "yr" + " " + age_month + "mon" + " " + age_day + "day");

                    $('#myRegModal').modal();
                    $("#txtHrn").prop('disabled', false);
                    clearOPD();
                    filledClear();
                }
				else if(data == "Follow up"){
					$('#messageMyModalLabel').text("Alert");
					$('.modal-body').text("Follow up can't be booked. No previous visit. Please try New OPD booking");
					$('#messagemyModal').modal();
					$("#selVisitType").focus();
				}
				else if(data == "visit"){
					$('#messageMyModalLabel').text("Alert");
					$('.modal-body').text("Your visit is already exist for today, Please Try next day.");
					$('#messagemyModal').modal();
					$("#selVisitType").focus();
				}
                else if(data == "Insurance"){
                    $("#selInsuranceNo").focus();
                    $("#selInsuranceNoError").text("Insurance no is not valid");
                    $("#selInsuranceNo").addClass("errorStyle");                    
                }
				else{
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
            },
            error: function() {				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    });
    
    $("#cancel").click(function(){
        $("#txtServiceFee").val("");
    });

	// remove validation style on change and key up event
    $("#selVisitType").change(function() {
        if ($("#selVisitType").val() != '-1') {
            $("#selVisitTypeError").text("");
            $("#selVisitType").removeClass("errorStyle");
        }
    });
    $("#selServiceType").change(function() {
        if ($("#selServiceType").val() != '-1') {
            $("#selServiceTypeError").text("");
            $("#selServiceType").removeClass("errorStyle");
        }
    });
    $("#selPaymentMode").change(function() {
        $('#selServiceType').val('-1');
        $('#selServices').val('-1');
        $("#btnOk").unbind();
        if ($("#selPaymentMode").val() != '-1') {
            $("#selPaymentModeError").text("");
            $("#selPaymentMode").removeClass("errorStyle");

            $("#txtServiceFee").val("");           

            if($("#selPaymentMode").val() == 'insurance'){
                if ($("#txtHrn").val() == "") {
                    $("#txtHrn").focus();
                    $("#txtHrnError").text("Please enter patient Id");
                    $("#txtHrn").addClass("errorStyle");
                    return;
                }
                if($("#selPaymentMode").val() == "insurance"){
                    $(".insurance").show();
                    $("#selInsuranceNo").html("<option value=''>-- Select --</option>"); 
                    
                }
                else{
                    $(".insurance").hide();
                    $("#selInsuranceCompany").val("-1");
                    $("#selInsurancePlan").val("-1");
                    $("#selInsuranceScheme").val("-1");
                    $("#selInsuranceNo").html("<option value=''>-- Select --</option>");                   
                }

                var patientId = $("#txtHrn").val();
                var hrn = parseInt($("#txtHrn").val().replace ( /[^\d.]/g, '' ));
                hrnPatientid = hrn;
                var postData = {
                    "operation": "insuranceDetails",
                    "hrn": hrn
                }

                $.ajax({
                    type: "POST",
                    cache: false,
                    url: "controllers/admin/opd_registration.php",
                    datatype: "json",
                    data: postData,

                    success: function(data) {
                        var parseData = jQuery.parseJSON(data);
                         if (parseData != "0" && parseData != "") {                        
                            /*$('#selInsuranceCompany').val(parseData[0].insurance_company_id);
                            $('#selInsuranceNo').val(parseData[0].insurance_number);*/
                            /*bindInsuranceSchemeName(parseData[0].insurance_company_id, parseData[0].insurance_plan_id);*/
                            /*bindPlanName(parseData[0].insurance_plan_id, parseData[0].insurance_plan_name_id);*/             
                        }
                        else{                        
                            $('#confirmInsuranceModalLabel').text("Alert");
                            $('#confirmInsuranceBody').text("Insurance detail is not present.Do you want to continue?");
                            $('#confirmInsuranceModal').modal();

                            $("#btnOk").click(function(){
                                $('#insuranceModalLabel').text("Insurance");
                                $("#hdnInsurance").val("1");
                                $("#hdnPatientId").val($("#txtHrn").val());
                                $('#insuranceBody').load('views/admin/modal_insurance.html');
                                $('#insuranceModal').modal();
                            });                            

                            $("#cancel").click(function(){
                                $(".insurance").hide();
                                $("#selPaymentMode").val("-1");
                            });
                        }
                    },
                    error: function(){

                    }
                });
            } 
            else{
                $(".insurance").hide();
                $("#selInsuranceCompany").val("-1");
                $("#selInsurancePlan").val("-1");
                $("#selInsuranceScheme").val("-1");
                $("#selInsuranceNo").html("<option value='-1'>-- Select --</option>");
                $("#selServices").html("<option value='-1'>-- Select --</option>");
                $("#txtServiceFee").val("");
                $("#selServiceType").val("-1")
            }           
        }
        else{
            $(".insurance").hide();
            $("#selInsuranceCompany").val("-1");
            $("#selInsurancePlan").val("-1");
            $("#selInsuranceScheme").val("-1");
            $("#selInsuranceNo").html("<option value='-1'>-- Select --</option>");
            $("#selServices").html("<option value='-1'>-- Select --</option>");
            $("#txtServiceFee").val("");
            $("#selServiceType").val("-1")
        }
    });
    $("#selInsuranceCompany").change(function() {
        if ($("#selInsuranceCompany").val() != '-1') {
            $("#selInsuranceCompanyError").text("");
            $("#selInsuranceCompany").removeClass("errorStyle");
        }
    });
    $("#selInsuranceScheme").change(function() {
        if ($("#selInsuranceScheme").val() != '-1') {
            $("#selInsuranceSchemeError").text("");
            $("#selInsuranceScheme").removeClass("errorStyle");
            $("#selInsuranceNoError").text("");
            $("#selInsuranceNo").removeClass("errorStyle");
            bindInsuranceNumber($("#selInsuranceCompany :selected").val(),$("#selInsurancePlan :selected").val(),$("#selInsuranceScheme :selected").val(),hrnPatientid);
        }
    });
    $("#selInsurancePlan").change(function() {
        if ($("#selInsurancePlan").val() != '-1') {
            $("#selInsurancePlanError").text("");
            $("#selInsurancePlan").removeClass("errorStyle");
        }
    });
    $("#selServices").change(function() {
        if ($("#selServices").val() != '-1') {
            $("#selServicesError").text("");
            $("#selServices").removeClass("errorStyle");
            $("#txtServiceFeeError").text("");
            $("#txtServiceFee").removeClass("errorStyle");
        }
    });
    $("#txtHrn").keyup(function() {
        if ($("#txtHrn").val() != '') {
            $("#txtHrnError").text("");
            $("#txtHrn").removeClass("errorStyle");
        }
    });
    $("#selInsuranceNo").change(function() {
        if ($("#selInsuranceNo").val() != '-1') {
            $("#selInsuranceNoError").text("");
            $("#selInsuranceNo").removeClass("errorStyle");
        }
    });
    $("#txtServiceFee").keyup(function() {
        if ($("#txtServiceFee").val() != '') {
            $("#txtServiceFeeError").text("");
            $("#txtServiceFee").removeClass("errorStyle");
        }
    });

    $("#txtHrn").change(function() {
        if ($("#txtHrn").val() != '-1') {
            $("#txtHrnError").text("");
            $("#txtHrn").removeClass("errorStyle");
        }
    });

    $("#iconSearch").click(function() {			// show popup for search patient
        $('#searchModalLabel').text("Search Patient");
		$("#hdnOpdReg").val("1");
        $('#searchModalBody').load('views/admin/view_patients.html');
        $('#mySearchModal').modal();
    });

	
	// reset functionality
    $("#btnRegReset").click(function() {
        $("#txtServiceFee").removeClass("errorStyle");
        $("#selPaymentMode").removeClass("errorStyle");
        $("#txtHrn").removeClass("errorStyle");
        $("#selServiceType").removeClass("errorStyle");
        $("#selServices").removeClass("errorStyle");
        $("#selVisitType").removeClass("errorStyle");
        $("#txtHrn").prop('disabled', false);
        bindServiceType();
        bindVisitType();
        $("#txtHrn").focus();
        clearOPD();
        filledClear();
    });
	
	
	$('#btnPrint').click(function(){
		/*firstly empty page after that load data*/
		$("#codexplBody").find("tr:gt(0)").html("");
		$("#printTotalBill").text("");
        //hospital info
        $("#lblPrintEmail").text(hospitalEmail);
        $("#lblPrintPhone").text(hospitalPhoneNo);
        $("#lblPrintAddress").text(hospitalAddress);
        $("#printHospitalName").text(hospitalName);	
		$("#lblPrintDate").text(todayDate());	
		if (hospitalLogo != "null") {
			imagePath = "./images/" + hospitalLogo;
			$("#printLogo").attr("src",imagePath);
		}
		var i = 0;
		$("#codexplBody").append('<tr><td>'+ (i+1) +'</td><td>'+serType+'</td><td>'
						+serName+'</td><td>'+serFee+'</td>');
		$("#printTotalBill").text(serFee);
      // $.print("#PrintDiv");
      window.print();
	});
    
    $('#txtHrn').keypress(function (e) {
        if (e.which == 13) {
            $("#btnSelect").click();
        }
    });

    $("#txtServiceFee").keydown(function(e){
        if (e.keyCode < 500) {
            return false;
        }
    });
});


//function to calculate age
function getAge(dateString) {
    var now = new Date();
    var today = new Date(now.getYear(), now.getMonth(), now.getDate());

    var yearNow = now.getYear();
    var monthNow = now.getMonth();
    var dateNow = now.getDate();

    var dob = new Date(dateString.substring(0, 4), dateString.substring(5, 7) - 1, dateString.substring(8, 10));

    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();
    var age = {};
    var ageString = "";
    var yearString = "";
    var monthString = "";
    var dayString = "";


    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    age = {
        years: yearAge,
        months: monthAge,
        days: dateAge
    };

    if (age.years > 1) yearString = " years";
    else yearString = " year";
    if (age.months > 1) monthString = " months";
    else monthString = " month";
    if (age.days > 1) dayString = " days";
    else dayString = " day";


    if ((age.years > 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else ageString = "Oops! Could not calculate age!";

    return ageString;
}

// define function for bind service type
function bindServiceType() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/opd_registration.php",
        data: {
            "operation": "showServiceType"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = JSON.parse(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
                }
                $('#selServiceType').html(option);
            }
        },
        error: function() {}
    });
}
// define function for bind service 
function bindServices(value) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/opd_registration.php",
        data: {
            "operation": "showServices",
            "serviceType": value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selServices').html(option);
            }
        },
        error: function() {}
    });
}
// define function for bind service fee
function bindInsuranceServiceFee(value, companyId, planId, schemeId) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/opd_registration.php",
        data: {
            "operation": "showInsuranceServiceFee",
            "serviceId": value,
            "companyId": companyId,
            "planId": planId,
            "schemeId": schemeId
        },
        success: function(data) {
            var parseData = jQuery.parseJSON(data);
            if (data != null && data != "" && data != "0" && data.length > 2) {                
                for (var i = 0; i < parseData.length; i++) {
                    $("#txtServiceFee").val(parseData[i].fee);
                }
            }
            //if revised cost does'nt exist then come back woith it's old value
            else if(data.length == 2){
                callSuccessPopUp("Alert","This service is not include in your insurance. You need to pay for this.");
                bindServiceFee(value);
                $('#selPaymentMode').val('cash');
                $('.insurance').hide();
            }
            else if(data == '0'){ 
                $('#confirmUpdateModalLabel').text();
                $('#updateBody').text("This service is not include in your insurance. Do you want to continue?");
                $('#confirmUpdateModal').modal();
                $("#btnConfirm").unbind();
                $("#btnConfirm").click(function(){ 
                    var value = $('#selServices :selected').val();  // define service name for print value
                    serName = $("#selServices :selected").text();
                    insurancePayment = 'cash';
                    bindServiceFee(value);
                    /*$("#selPaymentMode").val("cash");
                    $(".insurance").hide();
                    $("#selInsuranceNo").val("");*/
                });
                $("#confirmUpdateModal #cancel").unbind();
               $("#confirmUpdateModal #cancel").click(function(){
                    var value = $('#selServiceType :selected').val();
                    serType = $("#selServiceType :selected").text();    // define service type for print value
                    insurancePayment = 'insurance';
                    bindServices(value);
                });
            }
        },
        error: function() {}
    });
}
// define function for bind service fee
function bindServiceFee(value) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/opd_registration.php",
        data: {
            "operation": "showServiceFee",
            "serviceId": value
        },
        success: function(data) {
            var parseData = jQuery.parseJSON(data);
            if (data != null && data != "") {                
                for (var i = 0; i < parseData.length; i++) {
                    $("#txtServiceFee").val(parseData[i].fee);
                }
            }
        },
        error: function() {}
    });
}
// define function for bind service type
function bindVisitType() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/opd_registration.php",
        data: {
            "operation": "showVisitType"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = JSON.parse(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
                }
                $('#selVisitType').html(option);
            }
        },
        error: function() {}
    });
}
// define function for bind department
function bindDepartment() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/opd_registration.php",
        data: {
            "operation": "showDepartment"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = JSON.parse(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].department + "</option>";
                }
                $('#selDepartment').html(option);
            }
        },
        error: function() {}
    });
}

// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        $('.close').click();
    }
});

function bindInsuranceNumber(insuranceCompany,insuranceScheme,insuranceSchemePlan,patientId){
    var postData = {
        "operation": "showInsuranceNumber",
        "insuranceCompany" : insuranceCompany,
        "insuranceScheme" : insuranceScheme,
        "insuranceSchemePlan" : insuranceSchemePlan,
        "patientId" : patientId
    }
    dataCall("controllers/admin/opd_registration.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (result.length > 2) { 

            var parseData = JSON.parse(result);
            var option = ''; // parse the value in Array string jquery
            for (var i = 0; i < parseData.length; i++) {
                option += "<option value='" + parseData[i].insurance_number + "'>" + parseData[i].insurance_number + "</option>";
            }
            $('#selInsuranceNo').html(option);
        }
    });
}
// call function for bind data
function bindInsuranceCompany(patientId) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/insurance_plan.php",
        data: {
            "operation": "showCompany",
            "patientId" : patientId
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);
                var option = '<option value="-1">--Select--</option>'; // parse the value in Array string jquery
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selInsuranceCompany').html(option);
            }
        },
        error: function() {}
    });
}

// call function for bind scheme name
function bindInsuranceSchemeName(insuranceCompany,patientId) {
     $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/scheme_exclusion.php",
        data: {
            "operation": "showSchemeName",
            insuranceCompany: insuranceCompany,
            patientId : patientId
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = '<option value="-1">--Select--</option>';
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].plan_name + "</option>";
                }
                $('#selInsurancePlan').html(option);
                /*if(scheme != null){
                    $('#selInsurancePlan').val(scheme);
                }*/

            }
        },
        error: function() {}
    });
}

// call function for bind scheme name
function bindPlanName(insuranceScheme,patientId) {
     $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/scheme_exclusion.php",
        data: {
            "operation": "showPlanName",
            insuranceScheme: insuranceScheme,
            "patientId" : patientId
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);
                var option = '<option value="-1">--Select--</option>'; // parse the value in Array string jquery
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].scheme_plan_name + "</option>";
                }
                $('#selInsuranceScheme').html(option);
                /*if(plan != null){
                    $('#selInsuranceScheme').val(plan);
                }*/
            }
        },
        error: function() {}
    });
}


// clear function
function clearOPD() {
    $("#txtHrn").val("");
    $("#txtHrnError").text("");
    $("#txtFirstName").val("");
    $("#txtLastName").val("");
    $("#txtMobile").val("");
    $("#txt_year").val("");
    $("#txt_month").val("");
    $("#txt_day").val("");
    $("#selDepartment").val("-1");
    $("#selDepartmentError").text("");
    $("#selServiceType").val("");
    $("#selServiceTypeError").text("");
    $("#selServices").val("-1");
    $("#selServicesError").text("");
    $("#selRoomNo").val("");
    $("#selRoomNoError").text("");
    $("#selVisitType").val("-1");
    $("#selVisitTypeError").text("");
    $("#lblConsultantName").text("");
    $("#txtServiceFee").val("");
    $("#txtServiceFeeError").text("");
    $("#txtRemarks").val("");
	bindDepartment();
    bindVisitType();
    bindServiceType();
	bindServices("-1");
}

function filledClear(){
    $("#selDepartment").val("-1");
    $("#selPaymentMode").val("-1");
    $("#selInsuranceCompany").val("-1");
    $("#selInsuranceScheme").val("-1");
    $("#selInsuranceScheme").val("-1");
	$("#selInsuranceNo").val("");
    $("#selServiceType").val("-1");
    $("#selServiceTypeError").text("");
    $("#selPaymentModeError").text("");
    $("#selInsurancePlanError").text("");
    $("#selInsuranceCompanyError").text("");
    $("#selInsuranceSchemeError").text("");
    $("#selInsuranceNoError").text("");
    $("#selServices").val("-1");
    $("#selServicesError").text("");
    $("#selRoomNo").val("");
    $("#selRoomNoError").text("");
    $("#selVisitType").val("-1");
    $("#selVisitTypeError").text("");
    $("#lblConsultantName").text("");
    $("#txtServiceFee").val("");
    $("#txtServiceFeeError").text("");
    $("#txtRemarks").val("");
    $(".insurance").hide();
}
//# sourceURL=filename.js