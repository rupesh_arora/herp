$(document).ready(function() {
    loader();

    //global variables
    var getStateId;
    var getCountryId;
    var getCityId;
    selectCountry = 0;
    selectState = 0;
    selectCity = 0;

    bindCountry();

    /*On change call function bind state*/
    $("#selNewCountry").change(function() {
        var option = "<option value='-1'>Select</option>";
        if ($("#selNewCountry :selected") != -1) {
            var value = $("#selNewCountry :selected").val();
            getCountryId = value;//get country id that selected
        } else {
            $("#selState").html(option);
        }
        bindState('selState', value); //on change call this function

        if ($("#selNewCountry").val() != "-1") {
            $("#selNewCountryError").text("");
            $("#selNewCountry").removeClass("errorStyle");
        }
    });

    /*On change call bind state function*/
    $("#selNewNewCountry").change(function() {
        var option = "<option value='-1'>Select</option>";
        if ($("#selNewNewCountry :selected") != -1) {
            var value = $("#selNewNewCountry :selected").val();
            getCountryId = value;
        } else {
            $("#selNewState").html(option);
        }
        bindState('selNewState', value); //on change call this function

        /*Remove style*/
        if ($("#selNewNewCountry").val() != "-1") {
            $("#selNewNewCountryError").text("");
            $("#selNewNewCountry").removeClass("errorStyle");
        }
    });

    /*On change call bind city*/
    $("#selNewState").change(function() {
        var option = "<option value='-1'>Select</option>";
        if ($("#selNewState :selected") != -1) {
            var value = $("#selNewState :selected").val();
            getStateId = value;// getting state id
        } else {
            $("#selNewCity").html(option);
        }
        bindCity(value); //on change call this function

        if ($("#selNewState").val() != "-1") {
            $("#selNewStateError").text("");
            $("#selNewState").removeClass("errorStyle");
        }
    });

    //during load of screen make these tab of white color
    $("#edit_country_click").css({
        "background": "#fff",
        "color": "#000"
    });
    $("#edit_state_click").css({
        "background": "#fff",
        "color": "#000"
    });
    $("#edit_city_click").css({
        "background": "#fff",
        "color": "#000"
    });

    //on select value from drpdown set value in text field
    $("#selCountry").change(function() {
        var getCountry = $('#selCountry option:selected').text();
        if ($('#selCountry option:selected').val() == -1) {
            $("#txtNewCountry").val("");
        } else {
            $("#txtNewCountry").val(getCountry);
        }
        getCountryId = $('#selCountry').val();

        if ($("#selCountry").val() != "-1") {
            $("#selCountryNewError").text("");
            $("#selCountry").removeClass("errorStyle");
        }
        if ($("#txtNewCountry").val() != "") {
            $("#txtNewCountryError").text("");
            $("#txtNewCountry").removeClass("errorStyle");
        }
    });

    $("#selState").change(function() {
        var getState = $('#selState option:selected').text();
        $("#txtState").val(getState);
        getStateId = $('#selState').val();

        if ($("#selState").val() != "-1") {
            $("#selStateError").text("");
            $("#selState").removeClass("errorStyle");
        }
    });

    $("#selNewCity").change(function() {
        var getCity = $('#selNewCity option:selected').text();
        $("#txtCity").val(getCity);
        getCityId = $('#selNewCity').val();

        if ($("#selNewCity").val() != "-1") {
            $("#selNewCityError").text("");
            $("#selNewCity").removeClass("errorStyle");
        }

    });

    /*onclick functionality*/
    $("#save_country_click").click(function() {
        //to do value null of country on click
        if (selectCountry == 1) {
            selectCountry = 0;
            $("#selCountry").val("-1");
        }
        //do value null of country on click save 
        $("#txtCountry").val("");
        $(".save-country").show();
        $(".edit-country").hide();
        $("#save_country_click").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#edit_country_click").css({
            "background": "#fff",
            "color": "#000"
        });
        clear();
    });

    $("#edit_country_click").click(function() {
        if (selectCountry == 0) {
            selectCountry = 1;
            $("#selCountry").val("-1");
        }
        $("#txtNewCountry").val("");
        $(".save-country").hide();
        $(".edit-country").show();
        $("#edit_country_click").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#save_country_click").css({
            "background": "#fff",
            "color": "#000"
        });
        bindCountry();
        clear();
    });

    $("#save_state_click").click(function() {
        //to do value null of country on click
        if (selectState == 1) {
            selectState = 0;
            $("#selNewCountry").val("-1");
            $("#selState").val("-1");
        }
        $("#txtState").val("");
        $(".save-state").show();
        $(".edit-state").hide();
        $("#save_state_click").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#edit_state_click").css({
            "background": "#fff",
            "color": "#000"
        });
        clear();
        bindCountry();
    });

    $("#edit_state_click").click(function() {
        if (selectState == 0) {
            selectState = 1;
            $("#selState").val("-1");
            $("#selNewCountry").val("-1");
        }
        $("#txtNewState").val("");
        $(".save-state").hide();
        $(".edit-state").show();
        $("#edit_state_click").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#save_state_click").css({
            "background": "#fff",
            "color": "#000"
        });

        //getting value from drop down
        $("#selState").change(function() {
            var getState = $('#selState option:selected').text();
            if ($('#selState option:selected').val() == -1) {
                $("#txtNewState").val("");
            } 
            else {
                $("#txtNewState").val(getState);
            }
            getStateId = $('#selState').val();

            if ($("#txtNewState").val() != "") {
                $("#txtNewStateError").text("");
                $("#txtNewState").removeClass("errorStyle");
            }
        });
        clear();
        bindCountry();
    });

    $("#save_city_click").click(function() {
        if (selectCity == 1) {
            selectCity = 0;
            $("#selNewNewCountry").val("-1");
            $("#selNewState").val("-1");
            $("#selNewCity").val("-1");
        }
        $("#txtCity").val("");
        $(".save-city").show();
        $(".edit-city").hide();
        $("#save_city_click").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#edit_city_click").css({
            "background": "#fff",
            "color": "#000"
        });
        clear();
        bindCountry();
    });

    $("#edit_city_click").click(function() {
        if (selectCity == 0) {
            selectCity = 1;
            $("#selNewNewCountry").val("-1");
            $("#selNewState").val("-1");
            $("#selNewCity").val("-1");
        }
        $("#txtNewCity").val("");
        $(".save-city").hide();
        $(".edit-city").show();
        $("#edit_city_click").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#save_city_click").css({
            "background": "#fff",
            "color": "#000"
        });

        $("#selNewCity").change(function() {
            var getCity = $('#selNewCity option:selected').text();
            if ($('#selNewCity option:selected').val() == -1) {
                $("#txtNewCity").val("");
            } else {
                $("#txtNewCity").val(getCity);
            }

            getCityId = $('#selNewCity').val();
            if ($("#txtNewCity").val() != "") {
                $("#txtNewCityError").text("");
                $("#txtNewCity").removeClass("errorStyle");
            }
        });
        clear();
        bindCountry();
    });

    /*Save country data*/
    $("#btnSaveCountry").click(function() {
        var flag = false;
        if ($("#txtCountry").val() == "") {
            $("#txtCountryError").text("Please enter country");
            $("#txtCountry").addClass("errorStyle");
            flag = true;
        }
        if (flag == true) {
            return false;
        }
        var country = $("#txtCountry").val();
        var postData = {
            "operation": "saveCountry",
            "country": country
        }
        /*AJAX call to save country*/
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/location_screen.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Saved Successfully!!!");
                    $('#messagemyModal').modal();
                    bindCountry();
                    clear();
                } else {
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("Already Exist");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
                $('#messagemyModal').modal();
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            }
        });
    });

	/*Update country data*/
    $("#btnEditCountry").click(function() {
        var flag = false;
        if ($("#selCountry").val() == "-1") {
            $("#selCountryNewError").text("Please select country");
            $("#selCountry").addClass("errorStyle");
            flag = true;
        }
        if ($("#txtNewCountry").val() == "") {
            $("#txtNewCountryError").text("Please enter country");
            $("#txtNewCountry").addClass("errorStyle");
            flag = true;
        }/*
        if ($('#selCountry option:selected').text() == $("#txtNewCountry").val()) {
            $("#txtNewCountryError").text("Please enter differnt country");
            $("#txtNewCountry").focus();
            flag = true;
        }*/
        if (flag == true) {
            return false;
        }
        var newCountry = $("#txtNewCountry").val();
        var postData = {
            "operation": "updateCountry",
            "newCountry": newCountry,
            "getCountryId": getCountryId
        }
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
        /*AJAX call to update country*/
            $.ajax({
                type: "POST",
                cache: false,
                url: "controllers/admin/location_screen.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Country Updated Successfully!!!");
                        $('#messagemyModal').modal();
                        clear();
                        bindCountry();
                    } else {
                        $('#messageMyModalLabel').text("Sorry");
                        $('.modal-body').text("Already Exist");
                        $('#messagemyModal').modal();
                    }
                },
                error: function() {
                    $('#messagemyModal').modal();
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                }
            });
        });
    });
	
	/*Save new state*/
    $("#btnSaveState").click(function() {
        var flag = false;
        if ($("#selNewCountry").val() == "-1") {
            $("#selNewCountryError").text("Please select country");
            $("#selNewCountry").addClass("errorStyle");
            flag = true;
        }
        if ($("#txtState").val() == "") {
            $("#txtStateError").text("Please enter country");
            $("#txtState").addClass("errorStyle");
            flag = true;
        }
        if (flag == true) {
            return false;
        }
        var state = $("#txtState").val();
        var postData = {
            "operation": "saveState",
            "getCountryId": getCountryId,
            "state": state
        }
        /*AJAX call to save state*/
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/location_screen.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Saved Successfully!!!");
                    $('#messagemyModal').modal();
                    clear();
                } else {
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("Already Exist");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
                $('#messagemyModal').modal();
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            }
        });
    });
	
	/*Update state*/
    $("#btnEditState").click(function() {
        var flag = false;
        if ($("#selNewCountry").val() == "-1") {
            $("#selNewCountryError").text("Please select country");
            $("#selNewCountry").addClass("errorStyle");
            flag = true;
        }
        if ($("#selState").val() == "-1") {
            $("#selStateError").text("Please select state");
            $("#selState").addClass("errorStyle");
            flag = true;
        }
        if ($("#txtNewState").val() == "") {
            $("#txtNewStateError").text("Please enter new state");
            $("#txtNewState").addClass("errorStyle");
            flag = true;
        }
        if (flag == true) {
            return false;
        }
        var newState = $("#txtNewState").val();
        var postData = {
            "operation": "updateState",
            "newState": newState,
            "getStateId": getStateId,
            "getCountryId": getCountryId
        }
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
            /*AJAX call to update state*/
            $.ajax({
                type: "POST",
                cache: false,
                url: "controllers/admin/location_screen.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("State Updated Successfully!!!");
                        $('#messagemyModal').modal();
                        clear();
                    } else {
                        $('#messageMyModalLabel').text("Sorry");
                        $('.modal-body').text("Already Exist");
                        $('#messagemyModal').modal();
                    }
                },
                error: function() {
                    $('#messagemyModal').modal();
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                }
            });
        });
    });
	
	/*Update city*/
    $("#btnEditCity").click(function() {
        var flag = false;
        if ($("#selNewNewCountry").val() == "-1") {
            $("#selNewNewCountryError").text("Please select country");
            $("#selNewNewCountry").addClass("errorStyle");
            flag = true;
        }
        if ($("#selNewState").val() == "-1") {
            $("#selNewStateError").text("Please select state");
            $("#selNewState").addClass("errorStyle");
            flag = true;
        }
        if ($("#selNewCity").val() == "-1") {
            $("#selNewCityError").text("Please select state");
            $("#selNewCity").addClass("errorStyle");
            flag = true;
        }
        if ($("#txtNewCity").val() == "") {
            $("#txtNewCityError").text("Please enter country");
            $("#txtNewCity").addClass("errorStyle");
            flag = true;
        }        
        if (flag == true) {
            return false;
        }
        var newCity = $("#txtNewCity").val();
        var postData = {
            "operation": "updateCity",
            "newCity": newCity,
            "getCountryId": getCountryId,
            "getStateId": getStateId,
            "getCityId": getCityId
        }
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
            /*AJAX call to update city*/
            $.ajax({
                type: "POST",
                cache: false,
                url: "controllers/admin/location_screen.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("City Updated Successfully!!!");
                        $('#messagemyModal').modal();
                        clear();
                    } else {
                        $('#messageMyModalLabel').text("Sorry");
                        $('.modal-body').text("Already Exist");
                        $('#messagemyModal').modal();
                    }
                },
                error: function() {
                    $('#messagemyModal').modal();
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                }
            });
        });
    });

    /*save city*/
    $("#btnSaveCity").click(function() {
        var flag = false;
        if ($("#selNewState").val() == "-1") {
            $("#selNewStateError").text("Please select state");
            $("#selNewState").addClass("errorStyle");
            flag = true;
        }
        if ($("#selNewNewCountry").val() == "-1") {
            $("#selNewNewCountryError").text("Please select country");
            $("#selNewNewCountry").addClass("errorStyle");
            flag = true;
        }

        if ($("#txtCity").val() == "") {
            $("#txtCityError").text("Please enter city");
            $("#txtCity").addClass("errorStyle");
            flag = true;
        }
        if (flag == true) {
            return false;
        }

        var city = $("#txtCity").val();
        var postData = {
            "operation": "saveCity",
            "city": city,
            "getStateId": getStateId,
            "getCountryId": getCountryId,
            "getCityId": getCityId
        }
        /*AJAX call to save city*/
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/location_screen.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Saved Successfully!!!");
                    $('#messagemyModal').modal();
                    clear();
                }
                if (data == "0") {
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("Already Exist");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
                $('#messagemyModal').modal();
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            }
        });
    });
    
    /*Delete country*/
    $("#btnDeleteCountry").click(function(){
        if ($('#selCountry').val() == -1) {
            return false;
        }
        var deleteMsg = "Country";    
        var delCountryId = $("#selCountry").val();
        var postData = {
            "operation": "deleteLocation",
            "delCountryId": delCountryId
        }
        deleteLocation(postData,deleteMsg);
    });

    /*Delete state*/
    $("#btnDeleteState").click(function(){
        if ($('#selState').val() == -1 || $('#selState').val() == null) {
            return false;
        }
        var deleteMsg = "State";
        var delStateId = $("#selState").val();
        var postData = {
            "operation": "deleteLocation",
            "delStateId": delStateId
        }
        deleteLocation(postData,deleteMsg);
    });

    /*Delete city*/
    $("#btnDeleteCity").click(function(){
        if ($('#selNewCity').val() == -1 || $('#selNewCity').val() == null) {
            return false;
        }
        var deleteMsg = "City"; 
        var delCityId = $("#selNewCity").val();
        var postData = {
            "operation": "deleteLocation",
            "delCityId": delCityId
        }
        deleteLocation(postData,deleteMsg);
    });

    //keyup functionality
    $("#txtCountry").keyup(function() {
        if ($("#txtCountry").val() != "") {
            $("#txtCountryError").text("");
            $("#txtCountry").removeClass("errorStyle");
        }
    });
    $("#txtNewCountry").keyup(function() {
        if ($("#txtNewCountry").val() != "") {
            $("#txtNewCountryError").text("");
            $("#txtNewCountry").removeClass("errorStyle");
        }
    });
    $("#txtState").keyup(function() {
        if ($("#txtState").val() != "") {
            $("#txtStateError").text("");
            $("#txtState").removeClass("errorStyle");
        }
    });
    $("#txtNewState").keyup(function() {
        if ($("#txtNewState").val() != "") {
            $("#txtNewStateError").text("");
            $("#txtNewState").removeClass("errorStyle");
        }
    });
    $("#txtNewCity").keyup(function() {
        if ($("#txtNewCity").val() != "") {
            $("#txtNewCityError").text("");
            $("#txtNewCity").removeClass("errorStyle");
        }
    });
    $("#txtCity").keyup(function() {
        if ($("#txtCity").val() != "") {
            $("#txtCityError").text("");
            $("#txtCity").removeClass("errorStyle");
        }
    });

}); //end document ready

// Ajax call for show data on country 
function bindCountry() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/location_screen.php",
        data: {
            "operation": "showcountry"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $('#selNewNewCountry').html(option);
                $('#selNewCountry').html(option);
                $('#selCountry').html(option);
            }
        },
        error: function() {
            $('#messagemyModal').modal();
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
        }
    });
}

// Ajax call for show data on state select country 
function bindState(id, value) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/location_screen.php",
        data: {
            "operation": "showstate",
            "value": value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $('#' + id).html(option);
            }
        },
        error: function() {
            $('#messagemyModal').modal();
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
        }
    });
}


// Ajax call for show data on country select state 
function bindCity(value) {

    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/location_screen.php",
        data: {
            "operation": "showcity",
            "value": value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $('#selNewCity').html(option);
            }
        },
        error: function() {
            $('#messagemyModal').modal();
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
        }
    });
}

/*function to delete city , state and country*/
function deleteLocation(myPostData,deleteMsg){
    $('#confirmUpdateModalLabel').text();
    $('#updateBody').text("Are you sure that you want to delete this?");
    $('#confirmUpdateModal').modal();
    $("#btnConfirm").unbind();
    $("#btnConfirm").click(function(){
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/location_screen.php",
            datatype: "json",
            data: myPostData,

            success: function(data) {
                if (data != "0" && data != "") {
                     $('#messagemyModal').modal();
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text(deleteMsg+" Deleted Successfully!!!");
                    bindCountry();
                    clear();
                }
                if (data == "0") {
                    $('#messagemyModal').modal();                
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("Can't delete");
                }
            },
            error: function() {
                $('#messagemyModal').modal();
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            }
        });
    });
}
function clear() {

    $("#selCountry").val("-1");
    $("#selCountryNewError").text("");

    $("#selState").val("-1");
    $("#selStateError").text("");

    $("#txtCountry").val("");
    $("#txtCountryError").text("");

    $("#selNewCountry").val("-1");
    $("#selNewCountryError").text("");

    $("#txtCountry").removeClass("errorStyle");
    $("#selCountry").removeClass("errorStyle");
    $("#txtNewCountry").removeClass("errorStyle");
    $("#selNewCountry").removeClass("errorStyle");
    $("#selState").removeClass("errorStyle");
    $("#txtNewState").removeClass("errorStyle");
    $("#txtState").removeClass("errorStyle");
    $("#selNewNewCountry").removeClass("errorStyle");
    $("#selNewState").removeClass("errorStyle");
    $("#txtCity").removeClass("errorStyle");
    $("#txtNewCity").removeClass("errorStyle");
    $("#selNewCity").removeClass("errorStyle");

    $("#txtState").val("");
    $("#txtStateError").text("");

    $("#txtNewCountry").val("");
    $("#txtNewCountryError").text("");

    $("#txtNewState").val("");
    $("#txtNewStateError").text("");

    $("#selNewNewCountry").val("-1");
    $("#selNewNewCountryError").text("");

    $("#selNewState").val("-1");
    $("#selNewStateError").text("");

    $("#txtCity").val("");
    $("#txtCityError").text("");

    $("#selNewCity").val("-1");
    $("#selNewCityError").text("");

    $("#txtNewCity").val("");
    $("#txtNewCityError").text("");
}