var insuranceRevisedCostTable ;//global variable insuranceRevisedCostTable
var saveServiceTypeName = '';//take value of service button that is active.
$(document).ready(function() {
	loader();
	debugger;
	$('#selCompanyName').focus();
	/*reset button functionality*/
	$("#btnReset").click(function(){
		clearFormDetails('.viewInsuranceRevisedCostForm');
		$('#selCompanyName').focus();
		insuranceRevisedCostTable.fnClearTable();
	});

	// bind nsurance company
	bindInsuranceCompany();
	// bind company schemeon change company
	$("#selCompanyName").change(function() {
		var option = "<option value=''>--Select--</option>";
		
		if ($("#selCompanyName :selected") != "") {
			var value = $("#selCompanyName :selected").val();
			
			//on change call this function for load state
			bindSchemeName(value); 
		} else {
			$("#selSchemeName").html(option);
		}	

		if ($("#selCompanyName").val() != "") {
			$("#selCompanyNameError").text("");
			$("#selCompanyName").removeClass("errorStyle");
		}
	});	
	
	// bind company schemeon change company
	$("#selSchemeName").change(function() {
		var option = "<option value=''>--Select--</option>";
		
		if ($("#selSchemeName :selected") != "") {
			var value = $("#selSchemeName :selected").val();
			
			//on change call this function for load state
			bindPlanName(value); 
		} else {
			$("#selPlanName").html(option);
		}	

		if ($("#selSchemeName").val() != "") {
			$("#selSchemeNameError").text("");
			$("#selSchemeName").removeClass("errorStyle");
		}
	});
	$("#selPlanName").change(function() {
		$("#addServicePopUp").click();
	});
	
	
	/**DataTable Initialization**/
	insuranceRevisedCostTable = $('#insuranceRevisedCostTable').DataTable({
		"bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
		"bAutoWidth" : false,aLengthMenu: ["All"],//declare so that show all rows
	    iDisplayLength: -1,
		"fnDrawCallback": function ( oSettings ) {
			$('#insuranceRevisedCostTable td:nth-child(3)').unbind();
			$('#insuranceRevisedCostTable td:nth-child(3)').on('dblclick', function() {
				var previousData = $(this).text();
				var parentObj = $(this);
				$(this).addClass("cellEditing");				  
				$(this).html("<input type='text'  value='" + previousData + "'/>");
				$(this).children().first().keypress(function (e) { 
					if (e.which == 13) {
						var newContent = $(this).val();
						if (newContent !='') {
							$(this).parent().removeClass("cellEditing");    
							$(this).parent().text(newContent);
						}
						var floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
						var value = floatRegex.test(newContent);
						if(value == false){
							callSuccessPopUp('Alert',"Enter number only");
							return false;
						}										
					}
					// for disable alphabates keys
					if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { 
						return false;
					}
				});
				$(this).children().first().blur(function (e) {
					var newContent = $(this).val();
					var floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
					var value = floatRegex.test(newContent);
					if(value == false){
						callSuccessPopUp('Alert',"Enter number only");
						return false;
					}
					if (newContent !='') {
						$(this).parent().removeClass("cellEditing");    
						$(this).parent().text(newContent);
					}
				});
				/*Code to focus on last charcter*/
				var SearchInput = $(this).children();
				SearchInput.val(SearchInput.val());
				var strLength= SearchInput.val().length;
				SearchInput.focus();
			});			
		},		
	});
	
	// remove error message on key up and change
	removeErrorMessage();
	
	// click on tab button
	$(".slfBtn").on("click",function(){

		var flag = false;

		if($("#selPlanName").val() == ""){
			$("#selPlanName").focus();
			$("#selPlanNameError").text("Please select plan name");
            $("#selPlanName").addClass("errorStyle");
            flag = true;
		}

		if($("#selSchemeName").val() == ""){
			$("#selSchemeName").focus();
			$("#selSchemeNameError").text("Please select scheme name");
            $("#selSchemeName").addClass("errorStyle");
            flag = true;
		}

		if($("#selCompanyName").val() == ""){
			$("#selCompanyName").focus();
			$("#selCompanyNameError").text("Please select company");
            $("#selCompanyName").addClass("errorStyle");
            flag = true;
		}

		if(flag == true){
			return false;
		}

		insuranceRevisedCostTable.fnClearTable();
		$(".slfBtn").removeClass("activeSelf");
		$(this)	.addClass("activeSelf");
		saveServiceTypeName = $(this).val();
		$('.myHideClass').hide();

		$("#testStatus").text('');
		$('#addedStatus').text('');

		var clickedId = $(this).attr("id");
		var companyId = $("#selCompanyName").val();
		var planId = $("#selPlanName").val();
		var schemeId = $("#selSchemeName").val();

		if (clickedId == "addServicePopUp") {
			var postData = {
				"operation": "showServiceDetails",
	            "companyId": companyId,
	            "planId": planId,
	            "schemeId": schemeId
			}
			// call load data into data table
			loadData(postData);
		}
		else if(clickedId == "addLabPopUp"){
			var postData = {
				"operation": "showLabDetails",
	            "companyId": companyId,
	            "planId": planId,
	            "schemeId": schemeId
			}
			// call load data into data table
			loadData(postData);			
		}
		else if(clickedId == "addRadiologyPopUp"){
			var postData = {
				"operation": "showRadiologyDetails",
	            "companyId": companyId,
	            "planId": planId,
	            "schemeId": schemeId
			}
			// call load data into data table
			loadData(postData);
		}
		else if(clickedId == "addForensicPopUp"){
			var postData = {
				"operation": "showForensicDetails",
	            "companyId": companyId,
	            "planId": planId,
	            "schemeId": schemeId
			}
			// call load data into data table
			loadData(postData);
		}
		else if(clickedId == "addProcedurePopUp"){
			var postData = {
				"operation": "showProcedureDetails",
	            "companyId": companyId,
	            "planId": planId,
	            "schemeId": schemeId
			}
			// call load data into data table
			loadData(postData);
		}
		else{
			var postData = {
				"operation": "showPharmacyDetails",
	            "companyId": companyId,
	            "planId": planId,
	            "schemeId": schemeId
			}
			// call load data into data table
			loadData(postData);
		}		
	});
	
	/*Save data to database*/
	$("#btnSave").click(function(){

		/*perform validation*/
		var flag=false;
		
		if(validTextField('#selPlanName','#selPlanNameError','Please plan name') == true)
		{
			flag = true;
		}
		if(validTextField('#selSchemeName','#selSchemeNameError','Please select scheme name') == true)
		{
			flag = true;
		}
		if(validTextField('#selCompanyName','#selCompanyNameError','Please select company name') == true)
		{
			flag = true;
		}	
		if (flag==true) {
			return false;
		}		 
		if (insuranceRevisedCostTable.fnGetData().length < 1) {
			$('#messagemyModal').modal("show");
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Can't save null data");
			$("#selServiceType").focus();
			flag=true;
		}
		if (flag==true) {
			return false;
		}
		var arrayData = [];
		var tableLength = insuranceRevisedCostTable.fnGetNodes();
		for( var i=0; i<tableLength.length; i++){

			var smallArray =[];
			aData = insuranceRevisedCostTable.fnGetData(tableLength[i]);
			//not able to get actual cost correct from fnGet data
			var newCost = $(insuranceRevisedCostTable.fnGetNodes()[i]).find('td:eq(2)').text();
			if (newCost == '') {
				callSuccessPopUp('Alert','Cost can\'t be null.' );
				return false;
			}
			smallArray.push(aData[0],aData[1],newCost,aData[3]);//item name,actual cost, revised cost,item id
			arrayData.push(smallArray);
		} 
		var insuranceCompnayId =  $("#selCompanyName").val();
		var serviceNameId =  $("#selSchemeName").val();
		var planNameId =  $("#selPlanName").val();
		
		var postData ={
			"operation":"saveDataTableData",				
			"data":JSON.stringify(arrayData),
			"insuranceCompnayId" : insuranceCompnayId,
			"serviceNameId":serviceNameId,
			"planNameId"   : planNameId,
			"saveServiceTypeName" : saveServiceTypeName
		}

        $('#confirmMyModalLabel').text("Save Request");
        $('.selfRequestConfirm-body').text("Are you sure that you want to save this?");
        $('#confirmmyModal').modal();
        $('#confirm').unbind();
        $('#confirm').on('click',function(){
			dataCall("controllers/admin/insurance_revised_cost.php", postData, function (result){
	        	if (result.trim() == 1) {
					$('#selCompanyName').focus();					
					callSuccessPopUp('Success','Saved successfully!!!');
					$('#selCompanyName').focus();
	        	}
	        });
		});
	});
});//end document ready


//call function for load  table
function loadData(postData) {
	// call the function ajax call
	dataCall("./controllers/admin/insurance_revised_cost.php", postData, function (data) {
		if (data != null && data != "") {
			var countCheckBox = 0;
			var parseData = jQuery.parseJSON(data);
			$(parseData).each(function(){
				var id = this.id;
				var name = this.name;
				var actualCost = parseInt(this.cost);
				var revisedCost = parseInt(this.revised_cost);
				
				insuranceRevisedCostTable.fnAddData([name,actualCost,revisedCost,id]);
				countCheckBox++;
			});
		}
	});
}

function FadeOut(){
    setTimeout(function() {
        $('#addedStatus').text('');
        $('#testStatus').text('');
    }, 3000);
}

// call function for bind data
function bindInsuranceCompany() {
	$.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/insurance_plan.php",
        data: {
            "operation": "showCompany"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selCompanyName').html(option);
            }
        },
        error: function() {}
    });
}

// call function for bind scheme name
function bindSchemeName(value) {
	loader();
	 $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/insurance_revised_cost.php",
        data: {
            "operation": "showSchemeName",
            companyId: value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].plan_name + "</option>";
                }
                $('#selSchemeName').html(option);
            }
        },
        error: function() {}
    });
}

// call function for bind scheme name
function bindPlanName(value) {
	loader();
	 $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/insurance_revised_cost.php",
        data: {
            "operation": "showPlanName",
            schemeId: value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].scheme_plan_name + "</option>";
                }
                $('#selPlanName').html(option);
            }
        },
        error: function() {}
    });
}

/*src url = self_request.js*/