var assetCheckInTable;
var addAssetCheckInTable;
var addPopUpAssetCheckInTable;
$(document).ready(function(){
    var table = true;
    loader();
    debugger;

     bindRoom();	// bind all rooms
    bindLocation();	// bind all location

    removeErrorMessage();
	
	/*Hide add ward by default functionality*/
	$("#advanced-wizard").hide();
	$("#ledgerList").addClass('list');
	$("#tabledgerList").addClass('tab-list-add');

	assetCheckInTable = $('#assetCheckInTable').dataTable();
    LoadDataTable();
	addAssetCheckInTable = $('#addAssetCheckInTable').dataTable({
		 "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "autoWidth": false,
        "fnDrawCallback": function(oSettings) {
        	$('.btnEnabled').click(function(){
		    	var data = $(this).parents('tr')[0];
		        var mData = addAssetCheckInTable.fnGetData(data);
		    });
        },
	});
	

	/*Click for add the ledger*/
    $("#tabAddledger").click(function() {
        $("#advanced-wizard").show();
        $(".blackborder").hide();
		clear();
        $("#tabAddledger").addClass('tab-detail-add');
        $("#tabAddledger").removeClass('tab-detail-remove');
        $("#tabledgerList").removeClass('tab-list-add');
        $("#tabledgerList").addClass('tab-list-remove');
        $("#ledgerList").addClass('list');
        $('#txtName').focus();
        $('#txtPeriod').focus();
        $('#btnSubmit').show();
		$('#btnReset').show();
		$('#btnUpdate').hide();
    });
	
	//Click function for show the ledger lists
    $("#tabledgerList").click(function() {
        clear();
        tabledgerList();
    });
    $("#iconStaffSearch").click(function() {
        $("#myViewStaffModal").modal();
        $("#txtStaffId").val("");
        $("#viewStaffModalBody").load('views/admin/view_staff.html');
        $('#txtStaffIdError').text('');
        $('#txtStaffId').removeClass('errorStyle');
        $('#txtStaffNameError').text('');
        $('#txtStaffName').removeClass('errorStyle');
    });

    $("#selAssetClass").change(function(){
        var htmlRoom ="<option value='-1'>Select</option>";
        
        if ($('#selAssetClass :selected').val() != -1) {
            var value = $('#selAssetClass :selected').val();
            bindAssetSubClass(value);
        }
        else {
            $('#selAssetSubClass').html(htmlRoom);
        }
    });

    $("#btnAddAsset").click(function() {
    	bindLocation();	// bind all locations
    	bindAssetClass();	// bind all assets class    	

        var flag =false;
        if ($("#txtStaffName").val() == "") {
	        $('#txtStaffName').focus();
	        $("#txtStaffNameError").text("Please enter staff name");
	        $("#txtStaffName").addClass("errorStyle");
	        flag = true;
	    }
	    if ($("#txtStaffId").val() == "") {
	        $('#txtStaffId').focus();
	        $("#txtStaffIdError").text("Please select staff Id");
	        $("#txtStaffId").addClass("errorStyle");
	        flag = true;
	    }
	    if(flag == true){
	    	return false;
	    }

	    $("#myAssetCheckInModal").modal();

        var staffId = parseInt($("#txtStaffId").val().replace ( /[^\d.]/g, '' ));
        var manufacturerId = $("#txtManufacturer").attr('data-id');            
        var brandNameId = $("#txtBrandName").attr('data-id');            
        var modalId = $("#txtModal").attr('data-id');  

        var assetNo = $("#txtAssetNo").val();
        var assetDescription = $("#txtAssetDescription").val();
        var assetAdvanceDescription = $("#txtAssetAdvanceDescription").val();
        var barcode = $("#txtAssetBarcode").val();
        var description = $("#txtAssetDescription").val();
        var advanceDescription = $("#txtAssetAdvanceDescription").val();
        var assetClass = $("#selAssetClass").val();
        var assetSubClass = $("#selAssetSubClass").val();
        var assetLocation = $("#selAssetLocation").val();

        var postData = {
            "operation" : "search",
            "staffId" :  staffId,
             "assetAdvanceDescription" : assetAdvanceDescription,
             "assetDescription" : assetDescription,
             "manufacturerId" : manufacturerId,
            "brandNameId" : brandNameId,
            "modalId" : modalId,
            "assetNo" : assetNo,
            "barcode" : barcode,
            "description" : description,
            "advanceDescription" : advanceDescription,
            "assetClass" : assetClass,
            "assetSubClass" : assetSubClass,
            "assetLocation" : assetLocation
        }

        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/asset_check_in.php",
            datatype:"json",
            data: postData,
            
            success: function(data) {
                if(table != true){
                    addPopUpAssetCheckInTable.fnClearTable();
                    addPopUpAssetCheckInTable.fnDestroy();
                }
                
                addPopUpAssetCheckInTable = $('#addPopUpAssetCheckInTable').dataTable({
                    "bFilter": true,
                    "processing": true,
                    "sPaginationType": "full_numbers",
                    "autoWidth": false,
                    "fnDrawCallback": function(oSettings) {
                       
                                    
                        
                    },
                });
                if(data != "0" && data != "" ){
                    var parseData = jQuery.parseJSON(data);
                    for(i=0; i<parseData.length;i++){
                        var assetNo = parseData[i].asset_no;
                        var barcode = parseData[i].asset_barcode;

                        addPopUpAssetCheckInTable.fnAddData(["<input type='checkbox' data-id="+parseData[i].id+" class='chkBox'>",assetNo,barcode]);
                    }
                   table = false;
                     
                }
            },
            error: function(){

            }
        });
        
        
    });

    $('#btnSubmit').click(function(){
    	var flag = validation();

    	if(flag == true){
    		return false;
    	}

    	var saveArray = [];
    	var totalAmount = 0;
        var row = addAssetCheckInTable.fnGetNodes();
    	$.grep(addAssetCheckInTable.fnGetNodes(),function(v,i){
    		var obj = {};
    		obj.assetId = $(row[i]).find('td:last').find('input').attr('id');
    		saveArray.push(obj);
    	});

    	var staffId = parseInt($("#txtStaffId").val().replace ( /[^\d.]/g, '' ));
    	var staffName = $('#txtStaffName').val();
    	var setRoomTo = $('#txtSetRoom').val();
    	var setLocation = $('#txtSetLocation').val();

    	var postData = {
    		operation : "saveData",
    		staffId : staffId,
    		staffName : staffName ,
    		setRoomTo : setRoomTo,
    		setLocation : setLocation,
    		saveArray : JSON.stringify(saveArray)
    	}
    	dataCall("controllers/admin/asset_check_in.php", postData, function (result){
    		if (result =='1') {
    			callSuccessPopUp("Success","Asset check in saved successfully.");
    			addAssetCheckInTable.fnClearTable();
                LoadDataTable();
    			clear();
                $('#tab_dept').click();
    		}
    	});
    });

    $('#btnReset').click(function(){
    	clear();
    }); 

    $('#btnAddAssetCheckIn').click(function(){
    	var arr = [];
    	matchArray = [];
        $.grep(addPopUpAssetCheckInTable.fnGetNodes(), function( n, i ){
            if($(n).find('td:eq(0) input').prop('checked') == true){
                var array = [];
                var id = $(n).find('td:eq(0) input').attr('data-id');
                /*array.push($(n).find('td:eq(1) span').attr('value'));*/
                addAssetCheckInTable.fnAddData([$(n).find('td:eq(1)').text(),$(n).find('td:eq(2)').text(),'<input type="button" id='+id+' class="btnEnabled" onclick="rowDelete($(this))" value="Remove">']);
                //array.push($(n).find('td:eq(1)').text());
               // array.push($(n).find('td:eq(2)').text());              
               // arr.push(array);
               // matchArray.push(array);
            }
        });
        /*if (arr.length == 0) {
            callSuccessPopUp("Alert","Please insert new data in table");
            flag = true;
            return false;
        }*/
        /*addAssetCheckInTable.fnClearTable();
        var removeBtn = [];
        removeBtn.push('<input type="button" class="btnEnabled" onclick="rowDelete($(this))" value="Remove">');
        $.each(arr,function(i,v){
        	var mergeArray = v.concat(removeBtn);
        	addAssetCheckInTable.fnAddData(mergeArray);
        });*/
        $("#myAssetCheckInModal").modal('hide');
    });

    $("#btnApply").click(function(){

        var manufacturerId = $("#txtManufacturer").attr('data-id');            
        var brandNameId = $("#txtBrandName").attr('data-id');            
        var modalId = $("#txtModal").attr('data-id');  

        var assetNo = $("#txtAssetNo").val();
        var barcode = $("#txtAssetBarcode").val();
        var description = $("#txtAssetDescription").val();
        var advanceDescription = $("#txtAssetAdvanceDescription").val();
        var assetClass = $("#selAssetClass").val();
        var assetSubClass = $("#selAssetSubClass").val();
        var assetLocation = $("#selAssetLocation").val();
        var staffId = parseInt($("#txtStaffId").val().replace ( /[^\d.]/g, '' ));
        
        var postData = {
            "operation" : "search",
            "staffId" : staffId,
            "manufacturerId" : manufacturerId,
            "brandNameId" : brandNameId,
            "modalId" : modalId,
            "assetNo" : assetNo,
            "barcode" : barcode,
            "description" : description,
            "advanceDescription" : advanceDescription,
            "assetClass" : assetClass,
            "assetSubClass" : assetSubClass,
            "assetLocation" : assetLocation
        }

        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/asset_check_in.php",
            datatype:"json",
            data: postData,
            
            success: function(data) {
                addPopUpAssetCheckInTable.fnClearTable();
                addPopUpAssetCheckInTable.fnDestroy();
                 addPopUpAssetCheckInTable = $('#addPopUpAssetCheckInTable').dataTable({
                    "bFilter": true,
                    "processing": true,
                    "sPaginationType": "full_numbers",
                    "autoWidth": false,
                    "fnDrawCallback": function(oSettings) {
                       
                                    
                        
                    },
                });
                if(data != "0" && data != "" ){
                    var parseData = jQuery.parseJSON(data);
                    for(i=0; i<parseData.length;i++){
                        var assetNo = parseData[i].asset_no;
                        var barcode = parseData[i].asset_barcode;

                        addPopUpAssetCheckInTable.fnAddData(["<input type='checkbox' data-id="+parseData[i].id+" class='chkBox'>",assetNo,barcode]);
                    }
                   
                     
                }
            },
            error: function(){

            }
        });
    });


    $("#txtManufacturer").on('keyup', function(){   
        
        var postData = {
            "operation":"searchManufacturer",
            "name": $("#txtManufacturer").val()
        } 
        $("#txtBrandName").val('');
        $("#txtModal").val('');
        autoComplete("#txtManufacturer",postData);
    });

    $("#txtBrandName").on('keyup', function(){   
        if($("#txtManufacturer").attr('data-id') != ""){
            var postData = {
                "operation":"searchBrandName",
                "name": $("#txtBrandName").val(),
                "parentId" : $("#txtManufacturer").attr('data-id')
            } 
             $("#txtModal").val('');
            autoComplete("#txtBrandName",postData);
        }
        
    });

    $("#txtModal").on('keyup', function(){ 
        if($("#txtBrandName").attr('data-id') != ""){
            var postData = {
                "operation":"searchModal",
                "name":  $("#txtModal").val(),
                "parentId" : $("#txtBrandName").attr('data-id')
            }  
            
            autoComplete("#txtModal",postData);
        }
        
    });
    
});
function rowDelete(currInst){
	var row = currInst.closest("tr").get(0);
	addAssetCheckInTable.fnDeleteRow(row);
}
function tabledgerList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
    $("#tabAddledger").html('+Add Asset Check In');
    $("#tabAddledger").removeClass('tab-detail-add');
    $("#tabAddledger").addClass('tab-detail-remove');
    $("#tabledgerList").addClass('tab-list-add');
    $("#tabledgerList").removeClass('tab-list-remove');
    $("#ledgerList").addClass('list');
    clear();
}
function bindRoom() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/asset_check_in.php",
        data: {
            "operation": "showRoom"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].number + "</option>";
                }
                $('#txtSetRoom').html(option);
            }
        },
       error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
    });
}
function bindLocation() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/asset_check_in.php",
        data: {
            "operation": "showLocation"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].location + "</option>";
                }
                $('#txtSetLocation').html(option);
                $('#myAssetCheckInModal #selAssetLocation').html(option);
            }
        },
       error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
    });
}
function validation(){
	var flag = false;
   if ($("#txtSetLocation").val() == "-1") {
        $('#txtSetLocation').focus();
        $("#txtSetLocationError").text("Please select location");
        $("#txtSetLocation").addClass("errorStyle");
        flag = true;
    }
    if ($("#txtSetRoom").val() == "-1") {
        $('#txtSetRoom').focus();
        $("#txtSetRoomError").text("Please select room");
        $("#txtSetRoom").addClass("errorStyle");
        flag = true;
    }
    if ($("#txtStaffName").val() == "") {
        $('#txtStaffName').focus();
        $("#txtStaffNameError").text("Please enter staff name");
        $("#txtStaffName").addClass("errorStyle");
        flag = true;
    }
    if ($("#txtStaffId").val() == "") {
        $('#txtStaffId').focus();
        $("#txtStaffIdError").text("Please select staff Id");
        $("#txtStaffId").addClass("errorStyle");
        flag = true;
    }
    return flag;
}
function clear(){
	$('input[type=text]').val("");
    $('input[type=text]').next().text(""); 
    $('input[type=text]').removeClass("errorStyle");
    $('select').val("-1");
    $('select').next().text("");
    $('select').removeClass("errorStyle");
    $('textarea').val("");
    $('textarea').next().text(""); 
    $('textarea').removeClass("errorStyle");
    $('#txtStaffId').focus();
    $("#txtManufacturer").attr('data-id','');
    $("#txtBrandName").attr('data-id','');
    $("#txtModal").attr('data-id','');
    addAssetCheckInTable.fnClearTable();
}
function bindAssetClass() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/asset_check_in.php",
        data: {
            "operation": "showAssetClass"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].asset_class_name + "</option>";
                }
               $('#myAssetCheckInModal #selAssetClass').html(option);
            }
        },
       error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
    });
}
function bindAssetSubClass(value) {
     $.ajax({                    
        type: "POST",
        cache: false,
        url: "controllers/admin/asset_register.php",
        data: {
            "operation":"showAssetsubClass",
            "subClass" : value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].sub_class_name + "</option>";
                }
               $('#myAssetCheckInModal #selAssetSubClass').html(option);
            }
        },
       error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
    });
}

function autoComplete(id,postData){
    //Autocomplete functionality 
   /* $(id).val("");*/
    var currentObj = $(id);
    jQuery(id).autocomplete({
        source: function(request, response) {      
            
            $.ajax(
                {                   
                type: "POST",
                cache: false,
                url: "controllers/admin/asset_register.php",
                datatype:"json",
                data: postData,
                
                success: function(dataSet) {
                    loader();
                    response($.map(JSON.parse(dataSet), function (item) {
                        return {
                            id: item.id,
                            value: item.name
                        }
                    }));
                },
                error: function(){
                    
                }
            });
        },
        select: function (e, i) {
            var id = i.item.id;
            currentObj.attr("data-id", id);                
            
        },
        minLength: 2
    });
     $(id).autocomplete( "option", "appendTo", ".eventInsForm" );
     $(".ui-menu-item").hover(function(){
        $(this).addClass('ui-state-focus');
     });
}

function LoadDataTable(){
    var postData = {
        "operation" : "showTableData"
    }

    dataCall("controllers/admin/asset_check_in.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (parseData != '') {           
            assetCheckInTable.fnClearTable();
            $.each(parseData,function(i,v){
                staffId = v.staff_id;
                var prefix = v.staff_prefix;
                var idLength = 6-staffId.length;
                for(j=0; j<idLength; j++){
                    staffId="0"+staffId;
                }
                staffId=prefix+staffId;
                    
                assetCheckInTable.fnAddData([staffId,v.staff_name,v.number,v.location]);

                    
            }); 
        }
    });
}

