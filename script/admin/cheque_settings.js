$(document).ready(function() {
    loader(); 
    debugger;

    removeErrorMessage();

   $('#btnSubmit').click(function() {
   	var flag = false;
   		if($("#txtFontSize").val()==""){
				$('#txtFontSize').focus();
				$("#txtFontSizeError").text("Please enter font size");
				$("#txtFontSize").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtAmountFiguresSuffix").val()==""){
				$('#txtAmountFiguresSuffix').focus();
				$("#txtAmountFiguresSuffixError").text("Please enter suffix");
				$("#txtAmountFiguresSuffix").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtAmountFiguresPrefix").val()==""){
				$('#txtAmountFiguresPrefix').focus();
				$("#txtAmountFiguresPrefixError").text("Please enter prefix");
				$("#txtAmountFiguresPrefix").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtAmountFiguresWidth").val()==""){
				$('#txtAmountFiguresWidth').focus();
				$("#txtAmountFiguresWidthError").text("Please enter width");
				$("#txtAmountFiguresWidth").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtAmountFiguresLeft").val()==""){
				$('#txtAmountFiguresLeft').focus();
				$("#txtAmountFiguresLeftError").text("Please enter left");
				$("#txtAmountFiguresLeft").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtAmountFiguresLeft").val()==""){
				$('#txtAmountFiguresLeft').focus();
				$("#txtAmountFiguresLeftError").text("Please enter left");
				$("#txtAmountFiguresLeft").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtAmountFiguresTop").val()==""){
				$('#txtAmountFiguresTop').focus();
				$("#txtAmountFiguresTopError").text("Please enter top");
				$("#txtAmountFiguresTop").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtAmountWordPrefix").val()==""){
				$('#txtAmountWordPrefix').focus();
				$("#txtAmountWordPrefixError").text("Please enter prefix");
				$("#txtAmountWordPrefix").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtAmountWordDigits").val()==""){
				$('#txtAmountWordDigits').focus();
				$("#txtAmountWordDigitsError").text("Please enter digits");
				$("#txtAmountWordDigits").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtAmountWordLeft").val()==""){
				$('#txtAmountWordLeft').focus();
				$("#txtAmountWordLeftError").text("Please enter left");
				$("#txtAmountWordLeft").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtAmountWordTop").val()==""){
				$('#txtAmountWordTop').focus();
				$("#txtAmountWordTopError").text("Please enter top");
				$("#txtAmountWordTop").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtAmountWordHeight").val()==""){
				$('#txtAmountWordHeight').focus();
				$("#txtAmountWordHeightError").text("Please enter height");
				$("#txtAmountWordHeight").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtAmountWordWidth").val()==""){
				$('#txtAmountWordWidth').focus();
				$("#txtAmountWordWidthError").text("Please enter width");
				$("#txtAmountWordWidth").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtPayeeSuffix").val()==""){
				$('#txtPayeeSuffix').focus();
				$("#txtPayeeSuffixError").text("Please enter suffix");
				$("#txtPayeeSuffix").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtPayeePrefix").val()==""){
				$('#txtPayeePrefix').focus();
				$("#txtPayeePrefixError").text("Please enter prefix");
				$("#txtPayeePrefix").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtPayeeWidth").val()==""){
				$('#txtPayeeWidth').focus();
				$("#txtPayeeWidthError").text("Please enter width");
				$("#txtPayeeWidth").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtPayeeLeft").val()==""){
				$('#txtPayeeLeft').focus();
				$("#txtPayeeLeftError").text("Please enter left");
				$("#txtPayeeLeft").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtPayeeTop").val()==""){
				$('#txtPayeeTop').focus();
				$("#txtPayeeTopError").text("Please enter top");
				$("#txtPayeeTop").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtDateLeft").val()==""){
				$('#txtDateLeft').focus();
				$("#txtDateLeftError").text("Please enter left");
				$("#txtDateLeft").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtDateTop").val()==""){
				$('#txtDateTop').focus();
				$("#txtDateTopError").text("Please enter top");
				$("#txtDateTop").addClass("errorStyle");    
				flag=true;
		}
		if($("#txtPaperHeight").val()==""){
				$('#txtPaperHeight').focus();
				$("#txtPaperHeightError").text("Please enter height");
				$("#txtPaperHeight").addClass("errorStyle");    
				flag=true;
		}
	   	if($("#txtPaperWidth").val()==""){
				$('#txtPaperWidth').focus();
				$("#txtPaperWidthError").text("Please enter width");
				$("#txtPaperWidth").addClass("errorStyle");    
				flag=true;
		}
		if(flag == true){
			return false;
		}

		var paperWidth = $('#txtPaperWidth').val().trim();
		var paperHeight = $('#txtPaperHeight').val().trim();
		var dateTop = $('#txtDateTop').val().trim();
		var dateLeft = $('#txtDateLeft').val().trim();
		var payeeTop = $('#txtPayeeTop').val().trim();
		var payeeLeft = $('#txtPayeeLeft').val().trim();
		var payeeWidth = $('#txtPayeeWidth').val().trim();
		var payeePrefix = $('#txtPayeePrefix').val().trim();
		var payeeSuffix = $('#txtPayeeSuffix').val().trim();
		var amountWordWidth = $('#txtAmountWordWidth').val().trim();
		var amountWordHeight = $('#txtAmountWordHeight').val().trim();
		var amountWordTop = $('#txtAmountWordTop').val().trim();
		var amountWordLeft = $('#txtAmountWordLeft').val().trim();
		var maxDigits = $('#txtAmountWordDigits').val().trim();
		var NullPrefix = $('#txtAmountWordPrefix').val().trim();
		var amountFigureTop = $('#txtAmountFiguresTop').val().trim();
		var amountFigureLeft = $('#txtAmountFiguresLeft').val().trim();
		var amountFigureWidth = $('#txtAmountFiguresWidth').val().trim();
		var amountFigurePrefix = $('#txtAmountFiguresPrefix').val().trim();
		var amountFigureSuffix = $('#txtAmountFiguresSuffix').val().trim();
		var fontSize = $('#txtFontSize').val().trim();

		var postData = {
			"paperWidth" : paperWidth,
			"paperHeight" : paperHeight,
			"dateTop" : dateTop,
			"dateLeft" : dateLeft,
			"payeeTop" : payeeTop,
			"payeeLeft" : payeeLeft,
			"payeeWidth" : payeeWidth,
			"payeePrefix" : payeePrefix,
			"payeeSuffix" : payeeSuffix,
			"amountWordWidth" : amountWordWidth,
			"amountWordHeight" : amountWordHeight,
			"amountWordTop" : amountWordTop,
			"amountWordLeft" : amountWordLeft,
			"maxDigits" : maxDigits,
			"NullPrefix" : NullPrefix,
			"amountFigureTop" : amountFigureTop,
			"amountFigureLeft" : amountFigureLeft,
			"amountFigureWidth" : amountFigureWidth,
			"amountFigurePrefix" : amountFigurePrefix,
			"amountFigureSuffix" : amountFigureSuffix,
			"fontSize" : fontSize,
			"operation" : "saveChequeSetting"
		}

		$.ajax(
            {                   
            type: "POST",
            cache: false,
            url: "controllers/admin/cheque_setting.php",
            datatype:"json",
            data: postData,
            
            success: function(data) {
                if(data != "0" && data != ""){
                    callSuccessPopUp('Success','Saved successfully!!!');
                    clearFormDetails(".clearForm");
					$('#txtPaperWidth').focus();
					$('#chkBox').prop('checked',false);
                }
            },
            error: function(){

            }
        });
 	});
	$('#btnReset').click(function() {
		clearFormDetails(".clearForm");
		removeErrorMessage();
		$('#txtPaperWidth').focus();
		$('#chkBox').prop('checked',false);
	});

	$('#btnResetDefault').click(function() {
		restoreDefault();
	});
});

function restoreDefault() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/cheque_setting.php",
        data: {
            "operation": "showDefaultValues"
        },
        success: function(data) {
            if (data != null && data != "") {
            	var parseData = jQuery.parseJSON(data);
            	
            	$('#txtPaperWidth').val(parseData.paper_width);
				$('#txtPaperHeight').val(parseData.paper_height);
				$('#txtDateTop').val(parseData.date_top);
				$('#txtDateLeft').val(parseData.date_left);
				$('#txtPayeeTop').val(parseData.payee_top);
				$('#txtPayeeLeft').val(parseData.payee_left);
				$('#txtPayeeWidth').val(parseData.payee_width);
				$('#txtPayeePrefix').val(parseData.payee_prefix);
				$('#txtPayeeSuffix').val(parseData.payee_suffix);
				$('#txtAmountWordWidth').val(parseData.amount_word_width);
				$('#txtAmountWordHeight').val(parseData.amount_word_height);
				$('#txtAmountWordTop').val(parseData.amount_word_top);
				$('#txtAmountWordLeft').val(parseData.amount_word_left);
				$('#txtAmountWordDigits').val(parseData.max_no_of_digit);
				$('#txtAmountWordPrefix').val(parseData.prefix_for_null_value);
				$('#txtAmountFiguresTop').val(parseData.amount_figure_top);
				$('#txtAmountFiguresLeft').val(parseData.amount_figure_left);
				$('#txtAmountFiguresWidth').val(parseData.amount_figure_width);
				$('#txtAmountFiguresPrefix').val(parseData.amount_figure_prefix);
				$('#txtAmountFiguresSuffix').val(parseData.amount_figure_suffix);
				$('#txtFontSize').val(parseData.font_size);
		    }
        },
        error: function() {}
    });
}

