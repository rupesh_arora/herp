/*
 * File Name    :   insurance_plan.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre operation
 */
var insuranceCompaniesTbl; //defile variable for insurancecompaniesTbl 
var countryID = -1;
$(document).ready(function() {
	debugger;
	loader();
    $("#formInsurancePlan").hide();
    $("#divInsurancePlan").addClass('list');    
    $("#tabInsurancePlanList").addClass('tab-list-add');
    
    $("#tabInsurancePlan").click(function() { // show the add allergies categories tab
        showAddTab();
        clear();
    });

    $("#tabInsurancePlanList").click(function() { // show allergies list tab
        showTableList();
    });

	//bind country
    country(countryID); // load country

    //bind sate on click of country
    $('#sel_val_Country').change(function() {
        var countryID = $("#sel_val_Country").val();
        bindState(countryID, '');
        bindCountryCode(countryID);
    });

    //bind city on click of state
    $('#sel_val_State').change(function() {
        var stateID = $("#sel_val_State").val();
        bindCity(stateID, '');
    });
	
	$("#hideField").hide();
	// click event on check box
	$("#chkPreauthorization").on('click',function(){
		if($("#chkPreauthorization").prop("checked") == true) {
			$("#hideField").show();
		}else{
			$("#hideField").hide();
			$("#txtPreAuthorization").val("");
		}
	});
    // save details with validation
    $("#btnAddInsuranceCompanies").click(function() {
		var email;
        var flag = false;
		if ($("#txtAddress").val().trim() == "") {
            $("#txtAddressError").show();
            $("#txtAddressError").text("Please enter your address");
            $("#txtAddress").focus();
            $("#txtAddress").addClass("errorStyle");
            flag = true;
        }
		if ($("#txtPostalAddress").val().trim() == "") {
            $("#txtPostalAddressError").show();
            $("#txtPostalAddressError").text("Please enter postal address");
            $("#txtPostalAddress").focus();
            $("#txtPostalAddress").addClass("errorStyle");
            flag = true;
        }
       
        if ($("#val_zipcode").val().trim() == "") {
            $("#errorcode").show();
            $("#errorcode").text("Please enter zip code");
            $("#val_zipcode").focus();
            $("#val_zipcode").addClass("errorStyle");
            flag = true;
        }
        if ($("#sel_val_City").val() == "-1") {
            $("#errorcity").show();
            $("#errorcity").text("Please select city");
            $("#sel_val_City").focus();
            $("#sel_val_City").addClass("errorStyle");
            flag = true;
        }
        if ($("#sel_val_State").val() == "-1") {
            $("#errorstate").show();
            $("#errorstate").text("Please select state");
            $("#sel_val_State").focus();
            $("#sel_val_State").addClass("errorStyle");
            flag = true;
        }
        if ($("#sel_val_Country").val() == "-1") {
            $("#errorcountry").show();
            $("#errorcountry").text("Please select country");
            $("#sel_val_Country").focus();
            $("#sel_val_Country").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_Email").val().trim() == "") {
            $("#erroremail").show();
            $("#erroremail").text("Please enter email");
            $("#val_Email").focus();
            $("#val_Email").addClass("errorStyle");
            flag = true;
        }
		if (!ValidateEmail($("#val_Email").val())) {
			email = "";
        }
		else{
			 email = $("#val_Email").val().trim();
		}
		if (!validnumber($("#masked_phone_ext").val())) {
            $("#errorPhone").show();
            $("#errorPhone").text("Please enter valid phone number");
            $("#masked_phone_ext").focus();
            $("#masked_phone_ext").addClass("errorStyle");
            flag = true;
        }
        if ($("#masked_phone_ext").val().trim() == "") {
            $("#errorPhone").show();
            $("#errorPhone").text("Please enter phone number");
            $("#masked_phone_ext").focus();
            $("#masked_phone_ext").addClass("errorStyle");
            flag = true;
        }
		if ($("#txtName").val().trim() == "") {
            $("#txtNameError").show();
            $("#txtNameError").text("Please enter company name");
            $("#txtName").focus();
            $("#txtName").addClass("errorStyle");
            flag = true;
        }
		if(flag == true) {
			return false;
		}		
		
		if($("#chkPreauthorization").prop("checked") == true){
			var preauthorizationCkeck = '1';
			 if ($("#txtPreAuthorization").val().trim() == "" || $("#txtPreAuthorization").val().trim() < 0) {
				$("#txtPreAuthorizationError").show();
				$("#txtPreAuthorizationError").text("Please enter zip authorization period days");
				$("#txtPreAuthorization").focus();
				$("#txtPreAuthorization").addClass("errorStyle");
				return true;
			}
		}else{
			var preauthorizationCkeck = '0';
		}
		
		var companyName = $("#txtName").val().trim();
		var address = $("#txtAddress").val().trim();
		var phone = $("#masked_phone_ext").val().trim();
		var fax = $("#txtFax").val().trim();
		var website = $("#txtWebsite").val().trim();
		var contactPerson = $("#txtContactPerson").val().trim();
		var telephone = $("#txtTelephone").val().trim();
		
		var preAuthorization = $("#txtPreAuthorization").val().trim();
		var postalAddress = $("#txtPostalAddress").val().trim();
		
		var country = $("#sel_val_Country").val();
		var state = $("#sel_val_State").val();
        var city = $("#sel_val_City").val();
		var mobileCode = $("#mobileCode").val();
		var zipcode = $("#val_zipcode").val().trim();
		var description = $("#txtDesc").val().trim();
		description = description.replace(/'/g, "&#39");
		
		//ajax call for insert data into data base
		var postData = {
			"operation": "save",
			"companyName": companyName,
			"address": address,
			"phone": phone,
			"email": email,
			"country": country,
			"state": state,
			"city": city,
			"zipcode": zipcode,
            "mobileCode":mobileCode,
			"description": description,
			"fax": fax,
			"website": website,
			"contactPerson": contactPerson,
			"telephone": telephone,
			"preAuthorization": preAuthorization,
			"postalAddress": postalAddress,
			"preauthorizationCkeck": preauthorizationCkeck
		}
		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/insurance_companies.php",
			datatype: "json",
			data: postData,

			success: function(data) {
				if (data == "1") {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Insurance company saved successfully !!!");
					$('#messagemyModal').modal();
					showTableList();
				}
				if(data == "0") {
					$("#txtNameError").show();
					$("#txtNameError").text("Company name already exists");
					$("#txtName").focus();
					$("#txtName").addClass("errorStyle");
				}

			},
			error: function() {
				alert('error');
			}
		}); //  end ajax call
	}); //end button click function

    // for remove 	
    $("input[type=text]").keyup(function() {

        if ($(this).val() != "") {
            $(this).next("span").hide();
            $(this).removeClass("errorStyle");
        }
		
    });
	 $("input[type=number]").keyup(function() {

        if ($(this).val() != "") {
            $(this).next("span").hide();
            $(this).removeClass("errorStyle");
        }
		
    });
    $("select").on('change', function() {

        if ($(this).val() != "") {
            $(this).next("span").hide();
            $(this).removeClass("errorStyle");    
        }
		
    });
	$("textarea").keyup(function() {

        if ($(this).val() != "") {
            $(this).next("span").hide();
            $(this).removeClass("errorStyle");
        }
		
    });
	// reset
	$("#btnReset").on('click',function(){
		clear();
	});
	
    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code
        insuranceCompaniesTbl = $('#insuranceCompaniesTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "fnDrawCallback": function(oSettings) {
                // perform update event
				$('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = insuranceCompaniesTbl.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var companyName = mData["name"];
                        var address = mData["address"];
                        var phone = mData["phone"];
                        var email = mData["email_id"];
                        var country = mData["country_id"];
                        var state = mData["state_id"];
                        var city = mData["city_id"];
                        var zipcode = mData["zip"];
                        var description = mData["description"];
                        var fax = mData["fax"];
                        var mobileCode = mData["country_code"];
						var website = mData["website"];
						var contactPerson = mData["contact_person"];
						var telephone = mData["telephone"];
						var preAuthorization = mData["preauthorization_period"];
						var postalAddress = mData["postal_address"];
						var preauthorizationCkeck = mData["preauthorization_ip"];
						editClick(id,companyName,address,phone,email,country,state,city,zipcode,description,fax,mobileCode,website,contactPerson,telephone,preAuthorization,postalAddress,preauthorizationCkeck);

                    }
                });
                //perform delete event
				$('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = insuranceCompaniesTbl.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/insurance_companies.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                /* {  "sWidth": "3%","mData": "id" }, */
                {
                    "mData": "name"
                }, {
                    "mData": "address"
                },  {
                    "mData": "phone"
                },{
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [3]
            }]

        });
    }
	$('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            insuranceCompaniesTbl.fnClearTable();
            insuranceCompaniesTbl.fnDestroy();
            insuranceCompaniesTbl = "";
            insuranceCompaniesTbl = $('#insuranceCompaniesTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
					$('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = insuranceCompaniesTbl.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            restoreClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/insurance_companies.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [

                {
                    "mData": "name"
                }, {
                    "mData": "address"
                },  {
                    "mData": "phone"
                }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [3]
                }]
            });
        } else { // show active data on unchecked	
            insuranceCompaniesTbl.fnClearTable();
            insuranceCompaniesTbl.fnDestroy();
            insuranceCompaniesTbl = "";
            insuranceCompaniesTbl = $('#insuranceCompaniesTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform update event
					$('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = insuranceCompaniesTbl.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
							var id = mData["id"];
							var companyName = mData["name"];
							var address = mData["address"];
							var phone = mData["phone"];
							var email = mData["email_id"];
							var country = mData["country_id"];
							var state = mData["state_id"];
							var city = mData["city_id"];
							var zipcode = mData["zip"];
							var description = mData["description"];
                            var fax = mData["fax"];
							var mobileCode = mData["country_code"];
							var website = mData["website"];
							var contactPerson = mData["contact_person"];
							var telephone = mData["telephone"];
							var preAuthorization = mData["preauthorization_period"];
							var postalAddress = mData["postal_address"];
							var preauthorizationCkeck = mData["preauthorization_ip"];
							editClick(id,companyName,address,phone,email,country,state,city,zipcode,description,fax,mobileCode,website,contactPerson,telephone,preAuthorization,postalAddress,preauthorizationCkeck);
                        }
                    });
                    // perform delete event
					$('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = insuranceCompaniesTbl.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/insurance_companies.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                /* {  "sWidth": "3%","mData": "id" }, */
                {
                    "mData": "name"
                }, {
                    "mData": "address"
                },  {
                    "mData": "phone"
                }, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [3]
                }]
            });
        }
    });
});

function ValidateEmail(email) {
	var expr = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;;
	return expr.test(email);
};

function validnumber(number) {
	intRegex = /^\d{10,15}$/;
	return intRegex.test(number);
};
	
// edit data dunction for update 
function editClick(id,companyName,address,phone,email,country,state,city,zipcode,description,fax,mobileCode,website,contactPerson,telephone,preAuthorization,postalAddress,preauthorizationCkeck) {
    showAddTab();
    $('#tabInsurancePlan').html("+Update Insurance Company");
    $("#btnReset").hide();
    $("#btnAddInsuranceCompanies").hide();
	$("#hideField").hide();
	// click event on check box
	$("#chkPreauthorization").on('click',function(){
		if($("#chkPreauthorization").prop("checked") == true) {
			$("#hideField").show();
		}else{
			$("#hideField").hide();
			$("#txtPreAuthorization").val("");
		}
	});
	
    $("#btnUpdate").removeAttr("style");
	
   	$('input[type=text]').next("span").hide();
    $('input[type=text]').removeClass("errorStyle");
	$('input[type=number]').next("span").hide();
    $('input[type=number]').removeClass("errorStyle");
	$('select').next("span").hide();
    $('select').removeClass("errorStyle");
	
    $('#txtName').val(companyName);
    $('#txtAddress').val(address);
    $('#masked_phone_ext').val(phone);
    $('#val_Email').val(email);
    $('#sel_val_Country').val(country);
	bindState(country, state);
	bindCity(state, city);
	
    $('#val_zipcode').val(zipcode);
	$("#mobileCode").val(mobileCode);
    $('#txtFax').val(fax);
    $('#txtWebsite').val(website);
    $('#txtContactPerson').val(contactPerson);
    $('#txtTelephone').val(telephone);
    $('#txtPreAuthorization').val(preAuthorization);
    $('#txtPostalAddress').val(postalAddress);
	
	if(preauthorizationCkeck == "1") {
		$("#chkPreauthorization").prop("checked",true);
		$("#hideField").show();
	}
	else{
		$("#chkPreauthorization").prop("checked",false);
	}	
	
    $('#txtDesc').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);  
	
	
	 //bind sate on click of country
    $('#sel_val_Country').change(function() {
        var countryID = $("#sel_val_Country").val();
        bindState(countryID, '');
    });

    //bind city on click of state
    $('#sel_val_State').change(function() {
        var stateID = $("#sel_val_State").val();
        bindCity(stateID, '');
    });
	
    $("#btnUpdate").click(function() { // click update button
		 var email;
         var flag = false;
		if ($("#txtAddress").val().trim() == "") {
            $("#txtAddressError").show();
            $("#txtAddressError").text("Please enter your address");
            $("#txtAddress").focus();
            $("#txtAddress").addClass("errorStyle");
            flag = true;
        }
		if ($("#txtPostalAddress").val().trim() == "") {
            $("#txtPostalAddressError").show();
            $("#txtPostalAddressError").text("Please enter postal address");
            $("#txtPostalAddress").focus();
            $("#txtPostalAddress").addClass("errorStyle");
            flag = true;
        }
       
        if (!validnumber($("#masked_phone_ext").val())) {
            $("#errorPhone").show();
            $("#errorPhone").text("Please enter valid phone number");
            $("#masked_phone_ext").focus();
            $("#masked_phone_ext").addClass("errorStyle");
            flag = true;
        }
        if ($("#masked_phone_ext").val().trim() == "") {
            $("#errorPhone").show();
            $("#errorPhone").text("Please enter phone number");
            $("#masked_phone_ext").focus();
            $("#masked_phone_ext").addClass("errorStyle");
            flag = true;
        }
        if (!ValidateEmail($("#val_Email").val().trim())) {
			email = "";
        }
		else{
			 email = $("#val_Email").val().trim();
		}
        if ($("#val_zipcode").val().trim() == "") {
            $("#errorcode").show();
            $("#errorcode").text("Please enter zip code");
            $("#val_zipcode").focus();
            $("#val_zipcode").addClass("errorStyle");
            flag = true;
        }
        if ($("#sel_val_City").val() == "-1") {
            $("#errorcity").show();
            $("#errorcity").text("Please select city");
            $("#sel_val_City").focus();
            $("#sel_val_City").addClass("errorStyle");
            flag = true;
        }
        if ($("#sel_val_State").val() == "-1") {
            $("#errorstate").show();
            $("#errorstate").text("Please select state");
            $("#sel_val_State").focus();
            $("#sel_val_State").addClass("errorStyle");
            flag = true;
        }
        if ($("#sel_val_Country").val() == "-1") {
            $("#errorcountry").show();
            $("#errorcountry").text("Please select country");
            $("#sel_val_Country").focus();
            $("#sel_val_Country").addClass("errorStyle");
            flag = true;
        }
        if ($("#val_Email").val().trim() == "") {
            $("#erroremail").show();
            $("#erroremail").text("Please enter email");
            $("#val_Email").focus();
            $("#val_Email").addClass("errorStyle");
            flag = true;
        }
		if ($("#txtName").val().trim() == "") {
            $("#txtNameError").show();
            $("#txtNameError").text("Please enter company name");
            $("#txtName").focus();
            $("#txtName").addClass("errorStyle");
            flag = true;
        }
		if(flag == true) {
			return false;
		}
		if($("#chkPreauthorization").prop("checked") == true){
			var preauthorizationCkeck = '1';
			 if ($("#txtPreAuthorization").val().trim() == "") {
				$("#txtPreAuthorizationError").show();
				$("#txtPreAuthorizationError").text("Please enter zip authorization period days");
				$("#txtPreAuthorization").focus();
				$("#txtPreAuthorization").addClass("errorStyle");
				return true;
			}
		}else{
			var preauthorizationCkeck = '0';
		}
		
		var companyName = $("#txtName").val().trim();
		var address = $("#txtAddress").val().trim();
		var phone = $("#masked_phone_ext").val().trim();
		
		var country = $("#sel_val_Country").val();
		var state = $("#sel_val_State").val();
		var city = $("#sel_val_City").val();
        var zipcode = $("#val_zipcode").val().trim();
		var mobileCode = $("#mobileCode").val().trim();
		var description = $("#txtDesc").val();
		description = description.replace(/'/g, "&#39");
		
		var fax = $("#txtFax").val().trim();
		var website = $("#txtWebsite").val().trim();
		var contactPerson = $("#txtContactPerson").val().trim();
		var telephone = $("#txtTelephone").val().trim();
		
		var preAuthorization = $("#txtPreAuthorization").val().trim();
		var postalAddress = $("#txtPostalAddress").val().trim();
		
		var id = $('#selectedRow').val();
		
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to update this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			var postData = {
				"operation": "update",
				"companyName": companyName,
				"address": address,
				"phone": phone,
				"email": email,
				"country": country,
				"state": state,
				"city": city,
				"zipcode": zipcode,
                "mobileCode":mobileCode,
				"description": description,
				"fax": fax,
				"website": website,
				"contactPerson": contactPerson,
				"telephone": telephone,
				"preAuthorization": preAuthorization,
				"postalAddress": postalAddress,
				"preauthorizationCkeck": preauthorizationCkeck,
				"id": id
			}
			$.ajax( //ajax call for update data
			{
				type: "POST",
				cache: false,
				url: "controllers/admin/insurance_companies.php",
				datatype: "json",
				data: postData,

				success: function(data) {
					if (data == "1") {
						$('#myModal').modal('hide');
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Insurance company updated successfully!!!");
						$('#messagemyModal').modal();
						showTableList();
					}
					if(data == "0") {
						$("#txtNameError").show();
						$("#txtNameError").text("Company name already exists");
						$("#txtName").focus();
						$("#txtName").addClass("errorStyle");
					}
				},
				error: function() {
					$('.close-confirm').click();
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
			}); // end of ajax
		});
    });
} // end update button

function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Insurance Company");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteInsuranceCompany("' + type + '");');
} // end click fucntion

function restoreClick(id) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Insurance Company");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteInsuranceCompany("' + type + '");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
// clear function
function clear() {
	$('input[type=text]').next("span").hide();
	$('input[type=text]').val("");
    $('input[type=text]').removeClass("errorStyle");
	$('input[type=number]').next("span").hide();
	$('textarea').val("");
	$('input[type=number]').val("");
    $('input[type=number]').removeClass("errorStyle");
    $('textarea').removeClass("errorStyle");
    $('textarea').next("span").hide();
	$('select').next("span").hide();
    $('select').removeClass("errorStyle");
	country(countryID); // load country
    $('select').val("-1");
	$('#sel_val_State').val("-1");
	$('#sel_val_City').val("-1");
	$('#txtDesc').val("");
	$('#txtAddress').val("");
	$('#txtAddress').removeClass("errorStyle");
	$('#txtAddressError').text("");
	$("#txtName").focus();
	$('select[name=insuranceCompaniesTable_length]').val("10");
}
// define that function perform for delete and restore event
function deleteInsuranceCompany(type) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete		
            type: "POST",
            cache: false,
            url: "controllers/admin/insurance_companies.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Insurance company deleted successfully!!!");
                    $('#messagemyModal').modal();
                    insuranceCompaniesTbl.fnReloadAjax();
                } else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This Insurance company is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/insurance_companies.php",
            data: {
                "operation": "restore",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Insurance company restored successfully!!!");
                    $('#messagemyModal').modal();
                    insuranceCompaniesTbl.fnReloadAjax();

                }
            },
            error: function() {				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    }
}

// function for bind country
function country(countryID) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/hospital_profile.php",
        data: {
            "operation": "showcountry"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $('#sel_val_Country').html(option);
            }
            if (countryID != "") {
                $("#sel_val_Country").val(countryID);
                //bindCountryCode(countryID);
            }

        },
        error: function() {}
    });
}
// function for bind state
function bindState(countryID, stateID) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/hospital_profile.php",
        data: {
            "operation": "showstate",
            country_ID: countryID
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $('#sel_val_State').html(option);	
                if (stateID != "") {								
                    $("#sel_val_State").val(stateID);
                }
            }
        },
        error: function() {}
    });
}


// function for bind city
function bindCity(stateID, cityID) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/hospital_profile.php",
        data: {
            "operation": "showcity",
            state_ID: stateID
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $('#sel_val_City').html(option);
                if (cityID != "") {				
                    $("#sel_val_City").val(cityID);
                }
            }

        },
        error: function() {}
    });
}

function showTableList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#formInsurancePlan").hide();
    $(".blackborder").show();

    $("#tabInsurancePlan").removeClass('tab-detail-add');
    $("#tabInsurancePlan").addClass('tab-detail-remove');
    $("#tabInsurancePlanList").removeClass('tab-list-remove');    
    $("#tabInsurancePlanList").addClass('tab-list-add');
    $("#divInsurancePlan").addClass('list');    
    $("#btnReset").show();
    $("#btnAddInsuranceCompanies").show();
    $('#btnUpdate').hide();
    $('#tabInsurancePlan').html("+Add Insurance Company");
    clear();
}

function showAddTab(){
    $("#formInsurancePlan").show();
    $(".blackborder").hide();
   
    $("#tabInsurancePlan").addClass('tab-detail-add');
    $("#tabInsurancePlan").removeClass('tab-detail-remove');
    $("#tabInsurancePlanList").removeClass('tab-list-add');
    $("#tabInsurancePlanList").addClass('tab-list-remove');
    $("#divInsurancePlan").addClass('list');
    $("#selCompanyNameError").text("");
    $("#selCompanyName").removeClass("errorStyle");
     $("#txtPlanNameError").text("");
    $("#txtPlanName").removeClass("errorStyle");
    $("#txtName").focus();    
    removeErrorMessage();
}
function bindCountryCode(countryID) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/hospital_profile.php",
        data: {
            "operation": "showCountryCode",
            countryID: countryID
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                
                var countryCode = '+' + parseData.country_code;
                $("#mobileCode").val(countryCode);
            }
        },
        error: function() {}
    });
}