var pettyCashTable = '';
$(document).ready(function() {
	debugger;
	pettyCashTable = $('#pettyCashTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "aaSorting": []
	});
	$(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});
	var arr=[];
	removeErrorMessage();//call from common.js
	

    $("#viewReport").on('click',function(){
    	var flag = false;
    	if(validTextField("#selPettyCashType","#selPettyCashTypeError","Please select imprest type") == true) {
			flag = true;
		}
		if(flag == true) {
			return false;
		}
		var toDate = $("#toDate").val().trim();
		var fromDate = $("#fromDate").val().trim();
		var pettyType = $("#selPettyCashType").val();

		pettyCashTable.fnClearTable();
		pettyCashTable.fnDestroy();
		pettyCashTable = $('#pettyCashTable').dataTable({
	        "bFilter": true,
	        "processing": true,
	        "sPaginationType": "full_numbers",
	        "bAutoWidth": false,
	        "aaSorting": [],

	        "sAjaxSource": "controllers/admin/petty_cash.php",
            "fnServerParams": function(aoData) {

                aoData.push({
                    "name": "fromDate",
                    "value": fromDate
                });
                aoData.push({
                    "name": "toDate",
                    "value": toDate
                });

                if (pettyType == 1) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                }
                else if ( pettyType == 2 ) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                    aoData.push({
                        "name": "showPending",
                        "value": "showPending"
                    });
                }
                
            },
            "aoColumns": [{
                "mData": "petty_date"
            }, {
                "mData": function(o){
                    var pettyId = o["id"];                        
                    var pettyIdLength = pettyId.length;
                    for (var i=0;i<6-pettyIdLength;i++) {
                        pettyId = "0"+pettyId;
                    }
                    pettyId = pettyPrefix+pettyId;
                    return pettyId;  
                } 
            }, {
                "mData": "staff_customer_name"
            }, {
                "mData": "description"
            }, {
                "mData": "amount"
            } ]
	    });
	});
	$('#clear').click(function(){
		$('#selPettyCashType,#fromDate,#toDate').val('');
		pettyCashTable.fnClearTable();
		$('#selPettyCashTypeError').text('');
		$('#selPettyCashTypeError').removeClass('errorStyle');
	});
});