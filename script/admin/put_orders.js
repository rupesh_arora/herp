/* ****************************************************************************************************
 * File Name    :   put_orders.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   16th feb, 2016
 * Description  :   This page add the orders of inventory module.
 *************************************************************************************************** */
 
var putOrdersTable ;
var sellerHiddenFieldValue;
var orderId;
var orderDate;
var sellerName;
var printData;
var totalCost;
$(document).ready(function() {
	loader();	
	debugger;
	$(".amountPrefix").text(amountPrefix);
	
	/**DataTable Initialization**/
	putOrdersTable = $('#tblPutOrders').DataTable({
		aLengthMenu: [
	        [25, 50, 100, 200, -1],
	        [25, 50, 100, 200, "All"]//show all results at once
	    ],
	    iDisplayLength: -1,
		"bAutoWidth" : false,
		aoColumnDefs: [{ aTargets: [ 0,1,2,3], bSortable: false }]
	});

	
		// hide data table heading like search,length etc
	$("#tblPutOrders_wrapper #tblPutOrders_length").hide();
	$("#tblPutOrders_wrapper .dataTables_filter").hide();
	$("#tblPutOrders_wrapper .dataTables_info").hide();
	$("#tblPutOrders_wrapper #tblPutOrders_paginate").hide(); 
	
	$("#txtDate").val(todayDate());
	var startDate = new Date();
	$("#txtDate").datepicker({'setEndDate':startDate,"autoClose":true});
	
	/*$("#txtDate").change(function(){
		$("#txtDate").datepicker("hide");
	});*/

	/*Search seller on icon click*/
	$("#iconSellerSearch").click(function(){
		sellerHiddenFieldValue = 0;
		$('#SellerModalLabel').text("Search Seller");
		$('#SellerModalBody').load('views/admin/view_seller.html', function(){
			if ($("#txtSeller").val() !='') {
				/*$("#mySellerModal #txtSellerName").val($('#txtSeller').val());
				$("#mySellerModal #btnShowSeller").click();*/
			}	
		});
		$('#mySellerModal').modal();
		$("#txtSellerError").text("");// adding here if get an validation error on select of wrong input
		$("#txtSeller").removeClass("errorStyle");
	});
	
	/*Search item on icon click*/
	$("#iconItemSearch").click(function(){
		$("#hdnItemId").val("3");
		$('#ItemModalLabel').text("Search Item");
		$('#ItemModalBody').load('views/admin/view_item.html');//load the html to serach drug
		$('#myOrderItemModal').modal();
		$("#txtItemError").text("");// adding here if get an validation error on select of wrong input
		$("#txtItem").removeClass("errorStyle");
	});

	/*Save data to database*/
	$("#btnDrugAdd").click(function(){
		var flag = false;
		if (putOrdersTable.fnGetData().length < 1) {
			$('#messagemyModal').modal("show");
			$(".modal-footer").css({"display":"block"});
			$('#messageMyModalLabel').text("Error");
			$('#messagemyModal .modal-body').text("Please fill data in datatable");
			flag=true;
		}
		if (flag == true) {
			return false;
		}


		/*Defining array outside loop so to hold multiple values*/
		var smallArray = new Array(); //small array contain forenscic test price and id
		var bigArray = new Array(); // big 2d array conatin small array
				
		var aiRows = putOrdersTable.fnGetNodes();
		printData =[];
		for (i=0,c=aiRows.length; i<c; i++) {
			var obj = {};
			obj.itemId = $(aiRows[i]).find('.itemName').attr('item-id');
			obj.quantity = $($(aiRows[i]).find('td')[2]).text();
			//obj.cost = $($(aiRows[i]).find('td')[3]).text();
			obj.itemName = $($(aiRows[i]).find('td')[1]).text();
			//obj.totCost = $($(aiRows[i]).find('td')[4]).text();
			bigArray.push(obj);
			printData.push(obj); // puch data for print
		}		
		
		var sellerId = $("#txtSeller").attr("seller-id");
		var date = $("#txtDate").val();
		var notes = $("#txtNotes").val();
		
		orderDate = date;
		sellerName = $("#txtSeller").val();
		var sellerEmail = $('#txtSellerEmailId').val();
		//totalCost = $("#totalAmount").text(); // total cost

		var postData ={
			"operation" : "saveData",
			"sellerId" : sellerId,
			"date" : date,
			"notes" :notes,
			"bigArray" : JSON.stringify(bigArray),
			"sellerEmail" : sellerEmail,
			"hospitalName" : hospitalName,
			"sellerName" : sellerName,
			"hospitalEmail":hospitalEmail
		}
		$('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to place this order?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
			$.ajax({
				type: "POST",
				cache: false,
				url: "controllers/admin/put_orders.php",
				datatype:"json",
				data: postData,
				success : function(data){
					if(data !="" && data != "0"){
						$('#putOrderModalLabel').text("Success");	
						$("#success").text("Order saved successfully!!!");
						$("#lblOrderId").text(data);
						orderId = $("#lblOrderId").text();
						$('#myPutOrderModal').modal();
						putOrdersTable.fnClearTable();	
						clear();
						$("#txtDate").val(todayDate());
					}
					if(data ==""){
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Something awful happened!! Can't fetch old requests. Please try to contact admin.");
						$('#messagemyModal').modal();
					}
				},			
				error : function(){
					alert('error');
				}
			});
		});
	});

	// print button functioanlity
	$("#myPutOrderModal #print").on('click',function(){
		/*firstly empty page after that load data*/
		$("#codexplBody").find("tr:gt(0)").html("");
		//$("#printTotalCost").text("");
		
		/*hospital information through global varibale in main.js*/
		$("#lblPrintEmail").text(hospitalEmail);
		$("#lblPrintPhone").text(hospitalPhoneNo);
		$("#lblPrintAddress").text(hospitalAddress);
		if (hospitalLogo != "null") {
			imagePath = "./images/" + hospitalLogo;
			$("#printLogo").attr("src",imagePath);
		}
		$("#printHospitalName").text(hospitalName);
		$("#lblPrintOrderId").text(orderId);
		$("#lblSellerName").text(sellerName);
		$("#lblPrintDate").text(orderDate);
		//$("#printTotalCost").text(totalCost);
		
		for(var i=0;i<printData.length;i++){
			$("#codexplBody").append('<tr><td>'+ (i+1) +'</td><td>'+printData[i].itemName+'</td><td>'
			+printData[i].quantity+'</td>');
		}
		
		/*Call print function*/
		//$.print("#PrintDiv");
		window.print();
	});

	$("#txtSeller").keyup(function(){
		$("#txtSeller").removeAttr("seller-id");
	});
	
	/* Add data to datatable*/
	$("#btnAddData").click(function(){
		var flag = false;
		if ($("#txtQuantity").val()=="" || $("#txtQuantity").val() < 0) {
			$('#txtQuantity').focus();
            $("#txtQuantityError").text("Please enter quantity");
           $('#txtQuantity').addClass("errorStyle");
            flag=true;
		}
		if ($("#txtItem").val()=="") {
			$('#txtItem').focus();
            $("#txtItemError").text("Please enter item name");
           $('#txtItem').addClass("errorStyle");
            flag=true;
		}
		if ($("#txtDate").val()=="") {
            $("#txtDateError").text("Please select date");
           $('#txtDate').addClass("errorStyle");
            flag=true;
		}
		if ($("#txtSeller").val()=="") {
			$('#txtSeller').focus();
            $("#txtSellerError").text("Please enter seller name");
           $('#txtSeller').addClass("errorStyle");
            flag=true;
		}
		if ($("#txtItem").attr('item-id') == undefined) {
			$('#txtItem').focus();
            $("#txtItemError").text("Please select valid item that exist.");
            $('#txtItem').addClass("errorStyle");
            flag=true;
		}
		if ($("#txtSeller").attr('seller-id') == undefined) {
			$('#txtSeller').focus();
            $("#txtSellerError").text("Please select seller that exist.");
            $('#txtSeller').addClass("errorStyle");
            flag=true;
		}
		if (flag == true) {
			return false;
		}		
		
		var itemName = $("#txtItem").val();	
		var quantity = $("#txtQuantity").val();
		var itemId = $("#txtItem").attr("item-id");
		var rowcount = putOrdersTable.fnGetData().length;
		var sellerEmail = $('#txtSellerEmailId').val();
		/*Add data in table*/
		if (putOrdersTable.fnGetData().length > 0) {
			for(var i=0;i<putOrdersTable.length;i++){
				var matchSellerEmail = putOrdersTable.fnGetData()[i][4];
				if (sellerEmail != matchSellerEmail) {
					callSuccessPopUp("Alert ","Can't more than one seller at a time.");
					return false;
				}
			}
		}
		var currData = putOrdersTable.fnAddData( [rowcount+1,"<span class='itemName' item-id="+itemId+">"+itemName+"</span>",quantity,"<input type = 'button' class = 'removeDelete' value='Remove' >",sellerEmail]);
		$("#tblPutOrders").find('tbody').find('td:eq(0),td:eq(2)').addClass('text-center');//align center items
		$("#txtItem").removeAttr('item-id');
		//$('#totalAmount').text(parseInt($('#totalAmount').text())+parseInt(total));//add total bill
			
		$("#txtItem").val("");	
		//$("#txtCost").val("");	
		$("#txtQuantity").val("");	
		
		$(".removeDelete").unbind();
		$(".removeDelete").on('click',function(){
			var row = $(this).closest("tr").get(0);
			var total = 0;
			putOrdersTable.fnDeleteRow(row);
			
			var rowData = putOrdersTable.fnGetData();
			 for(j = 0;j < rowData.length;j++) {
				//total = total + rowData[j][4];
				$($('.sorting_1')[j]).text(j+1);
			} 
			
			//$('#totalAmount').text(parseInt(total));//add total bill
		});
							
	});

	/*Reset functionality*/
	$("#btnDrugreset").click(function(){
		clear();
		$('#txtSeller').focus();
		putOrdersTable.fnClearTable();	
		$("#txtSeller").removeAttr("disabled");
		$('#totalAmount').text(0)
	});

	/*key up functionality*/
	$("#txtQuantity").keyup(function(){
		if ($("#txtQuantity").val() !="") {
			$("#txtQuantity").removeClass("errorStyle");
			$("#txtQuantityError").text("");
		}
	});
	$("#txtQuantity").change(function(){
		if ($("#txtQuantity").val() !="") {
			$("#txtQuantity").removeClass("errorStyle");
			$("#txtQuantityError").text("");
		}
	});
	$("#txtSeller").keyup(function(){
		if ($("#txtSeller").val() !="") {
			$("#txtSeller").removeClass("errorStyle");
			$("#txtSellerError").text("");
		}
	});
	$("#txtItem").keyup(function(){
		if ($("#txtItem").val() !="") {
			$("#txtItem").removeClass("errorStyle");
			$("#txtItemError").text("");
		}
	});
	$("#txtCost").keyup(function(){
		if ($("#txtCost").val() !="") {
			$("#txtCost").removeClass("errorStyle");
			$("#txtCostError").text("");
		}
	});
	$("#txtDate").keyup(function(){
		if ($("#txtDate").val() !="") {
			$("#txtDate").removeClass("errorStyle");
			$("#txtDateError").text("");
		}
	});
	$("#txtDate").change(function(){
		if ($("#txtDate").val() !="") {
			$("#txtDate").removeClass("errorStyle");
			$("#txtDateError").text("");
		}
	});

	/*On keyup calculate amount*/
	$("#txtQuantity").keyup(function(){
		if($("#txtUnitPrice").val() !=""){
			calAmount();
		}
	});	

	$("#txtDate").keydown(function(e){
        if (e.keyCode < 500) {
            return false;
        }
    });
	
	$("#txtSeller").on('keyup', function(e){	//Autocomplete functionality  for seller
		if(e.keyCode !=13){
			$("#txtSeller").removeAttr('seller-id');
		}
		var currentObj = $(this);
		jQuery("#txtSeller").autocomplete({
			source: function(request, response) {
				var postData = {
					"operation":"search",
					"sellerName": currentObj.val()
				}
				
				$.ajax(
					{					
					type: "POST",
					cache: false,
					url: "controllers/admin/put_orders.php",
					datatype:"json",
					data: postData,
					
					success: function(dataSet) {
						response($.map(JSON.parse(dataSet), function (item) {
							return {
								id: item.id,
								value: item.name,
								email : item.email
							}
						}));
					},
					error: function(){
						
					}
				});
			},
			select: function (e, i) {
				var sellerId = i.item.id;
				currentObj.attr("seller-id", sellerId);
				$("#txtSeller").val(i.item.value);
				$('#txtSellerEmailId').val(i.item.email)			
				$("#txtSeller").attr("disabled","disabled");				
			},
			minLength: 3
		});		
	});
	
	$("#txtItem").on('keyup', function(e){	//Autocomplete functionality for item
		if(e.keyCode !=13){
			$("#txtItem").removeAttr('item-id');
		}
		var currentObj = $(this);
		jQuery("#txtItem").autocomplete({
			source: function(request, response) {
				var postData = {
					"operation":"searchItem",
					"itemName": currentObj.val()
				}
				
				$.ajax(
					{					
					type: "POST",
					cache: false,
					url: "controllers/admin/put_orders.php",
					datatype:"json",
					data: postData,
					
					success: function(dataSet) {
						response($.map(JSON.parse(dataSet), function (item) {
							return {
								id: item.id,
								value: item.name
							}
						}));
					},
					error: function(){
						
					}
				});
			},
			select: function (e, i) {
				var itemId = i.item.id;
				currentObj.attr("item-id", itemId);
				$("#txtItem").val(i.item.value);				
			},
			minLength: 3
		});		
	});
});
function clear(){
	$("#txtSeller").val("");
	$("#txtDate").val("");
	$("#txtItem").val("");
	$("#txtQuantity").val("");
	$("#txtNotes").val("");
	$("#txtCost").val("");
	$("#totalAmount").text("0");

	$("#txtSellerError").text("");
	$("#txtDateError").text("");
	$("#txtItemError").text("");
	$("#txtQuantityError").text("");
	$("#txtCostError").text("");
	$("#txtSeller").removeAttr("disabled");

	$("#txtSeller").removeClass("errorStyle");
	$("#txtDate").removeClass("errorStyle");
	$("#txtItem").removeClass("errorStyle");
	$("#txtQuantity").removeClass("errorStyle");
	$("#txtCost").removeClass("errorStyle");
	$("#codexplBody").find("tr:gt(0)").html("");
	$("#printTotalCost").text("");
}

/**Row Delete functionality**/
function rowDelete(currInst) {
    var row = currInst.closest("tr").get(0);
	var total = 0;
    putOrdersTable.fnDeleteRow(row);
	var rowData = putOrdersTable.fnGetData();
	/* for(j = 0;j < rowData.length;j++) {
		total = total + rowData[j][4];
	}
	$('#totalAmount').text(parseInt(total));//add total bill */
}

/*Function to calculate amount*/
function calAmount(){
	var unitPrice = $("#txtUnitPrice").val();
	var quantity = $("#txtQuantity").val();
	var intialAmount = unitPrice * quantity;
	$("#txtAmount").val(intialAmount);
}

/*function to delete row and subtract from total amount*/
function rowDelete(currInst){
	var row = currInst.closest("tr").get(0);//getting closest tr
	var oTable = $('#tblPutOrders').dataTable();
	oTable.fnDeleteRow(oTable.fnGetPosition(row));
	$('#totalAmount').text(parseInt($('#totalAmount').text())-parseInt($(row).find('td:eq(9)').text()));//subtract amount of that row from total amount on remove of a row
}

/*Function to validate number*/
function validnumber(number) {
    floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
    return floatRegex.test(number);
}

/*key press event on ESC button*/
$(document).keyup(function(e) {
    if (e.keyCode == 27) { 
		/* window.location.href = "http://localhost/herp/"; */
		$('.close').click();
    }
});
//src : seller.js