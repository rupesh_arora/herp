var mainReceiptTable;
var bankingScheduleTbl;
var addReceiptTable;
$(document).ready(function(){
	loader();
	var matchArray = [];

    loadTable();    // load datatable of banking schedule

	$("#form_dept").hide();
    $("#designation_list").css({
        "background": "#fff"
    });
    $("#tab_dept").css({
        "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
        "color": "#fff"
    });
    $("#tab_add_dept").click(function() { // click on add designation tab
        $("#form_dept").show();
        $(".blackborder").hide();
        $("#tab_add_dept").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tab_dept").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#designation_list").css({
            "background": "#fff"
        });
        clearFormDetails(".clearForm");
        $('#selLedger').focus();
        removeErrorMessage();
    });
    $("#tab_dept").click(function() { // click on list designation tab
		$('#inactive-checkbox-tick').prop('checked', false).change();
        $("#form_dept").hide();
        $(".blackborder").show();
        $("#tab_add_dept").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#tab_dept").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#designation_list").css({
            "background": "#fff"
        });
        removeErrorMessage();
    });

	bindLedger('#selLedger');
    $("#selLedger").change(function(){
        var ledgerId = $('#selLedger').val();
        if(ledgerId != ''){
            bindPaymentMode("#selSource","source",ledgerId);
            bindPaymentMode("#selBank","bank",ledgerId);
        }
        else{
            $("#selSource").html("<option value=''>--Select--</option>");
            $("#selBank").html("<option value=''>--Select--</option>");
        }
    });
	removeErrorMessage();
	debugger;
	mainReceiptTable = $('#mainReceiptTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "order": [[ 2, 'asc' ]],
        aoColumnDefs: [{'bSortable': false,'aTargets': [5] }],
        "fnDrawCallback": function(oSettings) {}
    });
    addReceiptTable = $('#addReceiptTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "order": [[ 2, 'asc' ]],
        "fnDrawCallback": function(oSettings) {}
    });

    if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		loadDataTable();
    }

    $("#btnAddDetail").on('click',function(){
    	var flag = false;
    	var source = $("#selSource").val();
    	if ($("#selBank").val() == '') {
    		$("#selBank").focus();
    		$("#selBank").addClass('errorStyle');
    		$("#selBankError").text("Please choose bank.");
    		flag = true;
    	}    
    	if (source == '') {
    		$("#selSource").focus();
    		$("#selSource").addClass('errorStyle');
    		$("#selSourceError").text("Please choose source.");
    		flag = true;
    	}   
    	if ($("#selLedger").val() == '') {
    		$("#selLedger").focus();
    		$("#selLedger").addClass('errorStyle');
    		$("#selLedgerError").text("Please choose ledger.");
    		flag = true;
    	}  
    	if(flag == true){
    		return false;
    	}   	
    	if (addReceiptTable != undefined) {
	        addReceiptTable.fnClearTable();
	        addReceiptTable.fnDestroy();
	        addReceiptTable = "";
	    }
	    addReceiptTable = $('#addReceiptTable').dataTable({
	        "bFilter": true,
	        "processing": true,
	        "sPaginationType": "full_numbers",
	        "bAutoWidth": false,
	        "order": [[ 2, 'asc' ]],
	        aoColumnDefs: [{'bSortable': false,'aTargets': [0] }],
	        "fnDrawCallback": function(oSettings) {}
	    });    	
    	var postData = {
    		operation : "showReceiptNumber",
    		source : source
    	}
    	dataCall("controllers/admin/banking_schedule.php", postData, function (result){
    		if (result.length > 2) {
    			$.each(JSON.parse(result),function(i,v){
    				var receiptId = v.id;                                    
                    var receiptIdLength = receiptId.length;
                    for (var j=0;j<6-receiptIdLength;j++) {
                        receiptId = "0"+receiptId;
                    }
                    receiptId = receiptprefix+receiptId;
                    var receiptDetail = '<span value='+v.id+'>'+receiptId+'</span>';
                    var checkBox = '<input type="checkbox" class="addReceiptCheckBox">';
                    var currData = addReceiptTable.fnAddData([checkBox,receiptDetail,v.payee_name,v.amount,v.refernce,v.date])
                    var getCurrDataRow = addReceiptTable.fnSettings().aoData[ currData ].nTr;
                    //match receipt number already in datatable then check that
                    if (matchArray.length !=0) {
                    	$.each(matchArray,function(ind,val){
                    		var arrIntersect = jQuery.inArray( receiptId, val );
                    		if (arrIntersect != -1) {
                    			$(getCurrDataRow).find('td:eq(0) input').prop("checked",true);
                    		}
                    	});
                    }
    			});
    			$("#myModalTable").modal();
    		}
    		else{
    			callSuccessPopUp('Alert','No data exist for choosen source.');
    		}
    	});
    });

    $('#btnReset').click(function() {
    	clear();
    });

    $("#btnAddReceipt").on('click',function(){
    	var arr = [];
    	matchArray = [];
        $.grep(addReceiptTable.fnGetNodes(), function( n, i ){
            if($(n).find('td:eq(0) input').prop('checked') == true){
                var array = [];
                /*array.push($(n).find('td:eq(1) span').attr('value'));*/
                array.push($(n).find('td:eq(1) span').text());
                array.push($(n).find('td:eq(2)').text());
                array.push($(n).find('td:eq(3)').text());
                array.push($(n).find('td:eq(4)').text());
                array.push($(n).find('td:eq(5)').text());                
                arr.push(array);
                matchArray.push(array);
            }
        });
        if (arr.length == 0) {
            callSuccessPopUp("Alert","Please insert new data in table");
            flag = true;
            return false;
        }
        mainReceiptTable.fnClearTable();
        var removeBtn = [];
        removeBtn.push('<input type="button" class="btnEnabled" onclick="rowDelete($(this))" value="Remove">');
        $.each(arr,function(i,v){
        	var mergeArray = v.concat(removeBtn);
        	mainReceiptTable.fnAddData(mergeArray);
        });
        $("#myModalTable").modal('hide');
    });

    $("#btnSave").on('click',function(){
    	var flag = 'false';
    	if(validTextField('#selLedger','#selLedgerError','Please select ledger') == true)
		{
			flag = true;
		}
		if(validTextField('#selSource','#selSourceError','Please select source') == true)
		{
			flag = true;
		}
		if(validTextField('#selBank','#selBankError','Please select bank') == true)
		{
			flag = true;
		}
		if (flag == true) {
			return false;
		}
    	var saveArray = [];
    	var totalAmount = 0;
    	$.grep(mainReceiptTable.fnGetNodes(),function(v,i){
    		var obj = {};
    		obj.receiptId = parseInt($(v).find('td:eq(0)').text().replace ( /[^\d.]/g, '' ));
    		obj.amount = $(v).find('td:eq(2)').text();
    		totalAmount+=totalAmount+parseFloat(obj.amount);
    		saveArray.push(obj);
    	});
    	var ledgerId = $("#selLedger").val();
    	var sourceId = $("#selSource").val();
    	var bankId = $("#selBank").val();
    	totalAmount = totalAmount.toFixed(2);
    	var postData = {
    		operation : "saveData",
    		ledgerId : ledgerId,
    		bankId : bankId ,
    		sourceId : sourceId,
    		totalAmount : totalAmount,
    		saveArray : JSON.stringify(saveArray)
    	}
    	dataCall("controllers/admin/banking_schedule.php", postData, function (result){
    		if (result =='1') {
    			callSuccessPopUp("Success","Banking schedule saved successfully.");
    			mainReceiptTable.fnClearTable();
    			addReceiptTable.fnClearTable();
    			clearFormDetails('.clearForm');
                $('#tab_dept').click();
                bankingScheduleTbl.fnClearTable();
                bankingScheduleTbl.fnDestroy();
                loadTable();

    		}
    	});

    });

    $("#selSource").on('change',function(){
    	mainReceiptTable.fnClearTable();
    	matchArray = [];
    });
});

function bindPaymentMode(selField,type,ledgerId){
	var postData = {
		operation : "showPaymentmode",
        ledgerId : ledgerId,
		type : type
	}
	dataCall("controllers/admin/banking_schedule.php", postData, function (result){
		if (result.length > 2) {
			var option = "<option value=''>--Select--</option>";
			$.each(JSON.parse(result),function(i,v){
				option += "<option value='" +v.id+ "'>" + v.name + "</option>";
			});
			$(selField).html(option);
		}
	});
}
function rowDelete(currInst){
	var row = currInst.closest("tr").get(0);
	mainReceiptTable.fnDeleteRow(row);
}
function loadDataTable(){
	
}

function viewClick(id,ledger_id,payment_mode_id,source_id,receipt_id,amount,ledgerName,payeeName,date) {
    $('#myBankingScheduleModel').modal("show");
    $('#bankingScheduleModelLabel').text("Banking Schedule Details");

    var receiptIdLength = receipt_id.length;
    for (var j=0;j<6-receiptIdLength;j++) {
        receipt_id = "0"+receipt_id;
    }
    receipt_id = receiptprefix+receipt_id;

    $('#lblReceiptNo').text(receipt_id);
    $('#lblName').text(id);
    $('#lblLedgerName').text(ledgerName);
    $('#lblBankName').text(payeeName);

    var amount = amountPrefix + ' ' + amount;

    $('#lblAmount').text(amount);
    $('#lblReference').text(payment_mode_id);
    $('#lblDate').text(date);

}




function clear() {
	$('#selLedger').val('');
	$('#selLedgerError').text('');
	$('#selLedger').removeClass('errorStyle');

	$('#selSource').val('');
	$('#selSourceError').text('');
	$('#selSource').removeClass('errorStyle');

	$('#selBank').val('');
	$('#selBankError').text('');
	$('#selBank').removeClass('errorStyle');

	$('#selLedger').focus();
}
function loadTable() {
    bankingScheduleTbl = $('#bankingScheduleTbl').dataTable( {
        "bFilter": true,
        "processing": true,
        "sPaginationType":"full_numbers",
        "fnDrawCallback": function ( oSettings ) {

            /*On click of update icon call this function*/
            $('#btnView').unbind();
            $('#btnView').on('click',function(){
                var data=$(this).parents('tr')[0];
                var mData = bankingScheduleTbl.fnGetData(data);//get datatable data
                if (null != mData)  // null if we clicked on title row
                {
                    var id = mData["id"];
                    var ledger_id = mData["ledger_id"];
                    var payment_mode_id = mData["payment_mode_id"];
                    var source_id = mData["source_id"];
                    var receipt_id = mData["receipt_id"];
                    var amount = mData["amount"];
                    var ledgerName = mData["ledger_name"];
                    var payeeName = mData["payee_name"];
                    var date = mData["bank_date"];
                    viewClick(id,ledger_id,payment_mode_id,source_id,receipt_id,amount,ledgerName,payeeName,date);//call edit click function with certain parameters to update               
                }
            });
            
        },
        
        "sAjaxSource":"controllers/admin/banking_schedule.php",
        "fnServerParams": function ( aoData ) {
          aoData.push( { "name": "operation", "value": "show" });
        },
        "aoColumns": [
            { "mData": "ledger_name" },
            { "mData": "payee_name" },
            {  
                "mData": function (o) { 
                    /*Return update and delete data id*/
                    return "<i class='bankingSchedule fa fa-eye' id='btnView' title='View' ></i>"; 
                }
            },  
        ],
        /*Disable sort for following columns*/
        aoColumnDefs: [
           { 'bSortable': false, 'aTargets': [ 2 ] }
       ]
    });
}
