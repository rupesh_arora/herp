$(document).ready(function(){
/* ****************************************************************************************************
 * File Name    :   view_patient_history.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages cash payment
 *************************************************************************************************** */	
	debugger;
	loader();
	$("#txtPatientIdHistory").focus();
	
	//Initialize the data table on document.ready 
	var otable = $('#tblViewPatientHistory').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {
			$(".patientDetails").click(function() {
				$('#oldHistoryModalDetails').modal();
				$.each($(".tab-pane"),function(index,value){
					if($(this).attr("data-url") != undefined){
						$(this).html("");
					}
				});
				generalOldHistoryClick();
				$('#thing').val("5");//set hidden field value =5

				$('#oldHistoryModalDetails input[type=text]').attr('readonly', true);
				
				var visitId = $(this).attr('data-id');
				var visitDate = $(this).attr('data-visitDate');
				$("#oldHistoryVisitDate").text(visitDate); 
				
				$("#oldHistoryVisitId").text(visitId);
				$($($(".tabViewConsultantLst")[1]).find('li')[1]).click();
				$("#oldHistoryModalDetails .tabViewConsultantLst").unbind();
	
				$(".tabViewConsultantLst").click(function(e) {
					var id = $(e.target); //getting the click event tag
					var contentId = id.attr("href"); //get id 

					/*on click if we get data url load page */
					if (($(contentId).attr("data-url") != undefined)) {
						loadUrl = $(contentId).attr("data-url");
						/*Empty html page*/
						var prevData = $(".tab-content .tab-pane");
						$.each(prevData,function(index,value){
							if($(this).attr("data-url") != undefined){
								$(this).html("");
							}
						});
						$(contentId).load('views/admin/' + loadUrl + '.html');
					} else {
						$(id).removeClass('active');
                        var tab =  $(e.target).text();
                        if(tab == 'General'){
                            $("#oldHistoryModalDetails .tabViewConsultantLst").find('li').removeClass('active');
                            $($(".tab-pane").removeClass('active')).addClass('fade');
                            $($($($($("#oldHistoryModalDetails .tabViewConsultantLst").first('ul').children('li:first').addClass('active').find('a').attr('href')).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active')
                            $('#tabViewTriage').load('views/admin/triage_general.html');
                        }
                        if(tab == 'Test Request'){
                           $("#oldHistoryModalDetails .tabViewConsultantLst").find('li').removeClass('active');
                            $($(".tab-pane").removeClass('active')).addClass('fade');
                            $($($($(contentId).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active');
                            $('#tabViewLabTestRequest').load('views/admin/lab_test_request.html');
                            //$('#tabLabTestRequest').load('views/admin/lab_test_request.html');
                        }
                        if(tab == 'Services Request'){
                            $("#oldHistoryModalDetails .tabViewConsultantLst").find('li').removeClass('active');
                            $($(".tab-pane").removeClass('active')).addClass('fade');
                            $($($($(contentId).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active');
                            $('#divViewServicesRequest').load('views/admin/services_request.html');
                            //$('#oldHistoryModalDetails #divServicesRequest').load('views/admin/services_request.html');
                        }
                        if(tab == 'Test Results'){
                            $("#oldHistoryModalDetails .tabViewConsultantLst").find('li').removeClass('active');
                            $($(".tab-pane").removeClass('active')).addClass('fade');
                            $($($($(contentId).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active');
                            $('#tabViewLabTestResult').load('views/admin/lab_test_result.html');
                            //$('#oldHistoryModalDetails #tabLabTestResult').load('views/admin/lab_test_result.html');
                        }
					}
				});
			})
		},
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 4 ] }
	   ],
	});
	removeErrorMessage();// remove error messaages

	$("#btnShow").click(function() {						 //getting patient details click on select button

        if ($("#txtPatientIdHistory").val() != '-1') {
            $("#txtPatientIdHistoryError").text("");
            $("#txtPatientIdHistory").removeClass("errorStyle");
        }
		var patientId = $("#txtPatientIdHistory").val();
        var flag = "false";
        if ($("#txtPatientIdHistory").val() == "") {
            $("#txtPatientIdHistoryError").text("Please enter patient id");
            $("#txtPatientIdHistory").addClass("errorStyle");
            flag = "true";
        }
        if(patientId.length != 9){
			$("#txtPatientIdHistory").focus();
           $("#txtPatientIdHistoryError").text("Patient doesn't exist");
			$("#txtPatientIdHistory").addClass("errorStyle");	
            flag = "true";
		}
        if (flag == "true") {
            return false;
        }
        var patientId = $("#txtPatientIdHistory").val();
		var patientPrefix = patientId.substring(0, 3);
		patientId = patientId.replace ( /[^\d.]/g, '' ); 
		patientId = parseInt(patientId);
		

        
        $.ajax({							//call ajax for show details
            type: "POST",
            cache: "false",
            url: "controllers/admin/view_patient_history.php",
            data: {
                "operation": "show",
                "patientId": patientId,
                "patientPrefix": patientPrefix
            },
            success: function(data) {
                if (data != "0" && data != "") {

                    var parseData = jQuery.parseJSON(data);
					if (parseData != "0" && parseData != "") {
						otable.fnClearTable();
						for (var i = 0; i < parseData.length; i++) {
							$("#txtFirstName").val(parseData[i].first_name);
							$("#txtLastName").val(parseData[i].last_name);
							$("#txtMobile").val(parseData[i].mobile);
							var getDOB = parseData[i].dob;							
							var VisitDate = parseData[i].created_on;
							var date = new Date(VisitDate * 1000);
							
							var get_date = date.getDate();
							var year = date.getFullYear();
							var month = date.getMonth();
							month = month +1;
							if(month < "10"){
								month = "0"+month;
							}
							 
							if(get_date < "10"){
								get_date = "0"+get_date;
							}
							 var VisitDate = year + "-" + month + "-" + get_date;						
							
							var visitType = parseData[i].type;
							var Consultant = parseData[i].consultant_name;
							var visitId = parseData[i].visit_id;
							var visitPrefix = parseData[i].visit_prefix;
							var visitIdLength = visitId.length;
							for (var j=0;j<6-visitIdLength;j++) {
								visitId = "0"+visitId;
							}
							visitId = visitPrefix+visitId;
										
							otable.fnAddData([visitId, visitType,VisitDate,Consultant,
								"<i class='fa fa-eye patientDetails' id='btnView' data-id=" + visitId + " data-visitDate = "+ VisitDate +" style='font-size: 17px; margin-left: 20px; cursor:pointer;' title='view' value='view'></i>"]);
						}
						var new_age = getAge(getDOB);

						var split = new_age.split(' ');
						var age_years = split[0];
						var age_month = split[1];
						var age_day = split[2];

						$("#txtYear").val(age_years);
						$("#txtMonth").val(age_month);
						$("#txtDay").val(age_day);
						$("#txtPatientIdHistory").attr("disabled", "disabled");	
					}	
					else {
						$("#txtPatientIdHistoryError").text("Patient doesn't exist");
						$("#txtPatientIdHistory").addClass("errorStyle");
					}	
				 } 
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    });
	
	$("#iconPatientSearch").click(function() {			// show popup for search patient
        $('#viewPatientModalLabel').text("Search Patient");
		$("#hdnViewPatientHistory").val("1");
        $('#viewPatientModalBody').load('views/admin/view_patients.html');
		$('#myViewPatientModal').modal();
    });
	
	$("#btnPatientHistoryReset").click(function() {
        $("#txtPatientIdHistory").removeAttr("style");
        $("#txtPatientIdHistory").prop('disabled', false);
		$("#txtPatientIdHistory").val("");
		$("#txtPatientIdHistoryError").text("");
		$("#txtFirstName").val("");
		$("#txtLastName").val("");
		$("#txtMobile").val("");
		$("#txtYear").val("");
		$("#txtMonth").val("");
		$("#txtDay").val("");
        $("#txtPatientIdHistory").focus();
        clear();
		otable.fnClearTable();
    });
	
	
	$("#oldHistoryModalDetails #pop_up_close").click(function(){
		 $.each($(".tab-pane"),function(index,value){
			if($(this).attr("data-url") != undefined){
				$(this).html("");
			}
		});
	});

	$('#txtPatientIdHistory').keypress(function (e) {
	    if (e.which == 13) {
		    $("#btnShow").click();
    	}
 	});

});

//function to calculate age
function getAge(dateString) {
    var now = new Date();
    var today = new Date(now.getYear(), now.getMonth(), now.getDate());

    var yearNow = now.getYear();
    var monthNow = now.getMonth();
    var dateNow = now.getDate();

    var dob = new Date(dateString.substring(0, 4), dateString.substring(5, 7) - 1, dateString.substring(8, 10));

    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();
    var age = {};
    var ageString = "";
    var yearString = "";
    var monthString = "";
    var dayString = "";


    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    age = {
        years: yearAge,
        months: monthAge,
        days: dateAge
    };

    if (age.years > 1) yearString = " years";
    else yearString = " year";
    if (age.months > 1) monthString = " months";
    else monthString = " month";
    if (age.days > 1) dayString = " days";
    else dayString = " day";


    if ((age.years > 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else ageString = "Oops! Could not calculate age!";

    return ageString;
}
function generalOldHistoryClick(){
	$("#oldHistoryModalDetails .tabViewConsultantLst").find('li').removeClass('active');
	$($(".tab-pane").removeClass('active')).addClass('fade');
	$($($($($("#oldHistoryModalDetails .tabViewConsultantLst").first('ul').children('li:first').addClass('active').find('a').attr('href')).removeClass('fade')).addClass('active').find('ul').find('li:first').addClass('active').find('a').attr('href')).click().removeClass('fade')).addClass('active')
	$('#tabViewTriage').load('views/admin/triage_general.html');
 }
function clear() {
    $("#txtPatientIdHistory").val("");
    $("#txtPatientIdHistoryError").text("");
    $("#txtFirstName").val("");
    $("#txtLastName").val("");
    $("#txtMobile").val("");
    $("#txtYear").val("");
    $("#txtMonth").val("");
    $("#txtDay").val("");
}
