$(document).ready(function() {

var otable; //defile variable for otable 
	
loader();
$('#reset').click(function(){
		clear();
		$('#txtStaffType').focus();
	});
// show the add staff type 
    $("#form_staff").hide();
	 $("#staff_list").css({
            "background": "#fff"
        });
		$("#tab_staff").css({
            "background": "background: linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200);",
            "color": "#fff"
        });
    $("#tab_add_staff").click(function() {
        $("#form_staff").show();
        $(".blackborder").hide();
        $("#tab_add_staff").css({
            "background": "background: linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200);",
            "color": "#fff"
        });
        $("#tab_staff").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#staff_list").css({
            "background-color": "#fff"
        });
		$("#txtStaffNameError").text("");
		$('#txtStaffType').val("");
		$('#txtStaffType').focus();
        $("#txtStaffType").removeClass("errorStyle");
    });
// show the add staff type list
    $("#tab_staff").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
        $("#form_staff").hide();
        $(".blackborder").show();
        $("#tab_add_staff").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#tab_staff").css({
            "background": "background: linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200);",
            "color": "#fff"
        });
        $("#staff_list").css({
            "background": "#fff"
        });
		$("#example-datatable").dataTable().fnReloadAjax(); 
	});
// check the validation
    $("#btnAddStaff").click(function() {
		var flag = "false";
        if ($("#txtStaffType").val().trim() == '') {
            $("#txtStaffNameError").text("Please enter staff type");
			$('#txtStaffType').focus();
            $("#txtStaffType").addClass("errorStyle");
            flag = "true";
        }
		else
		{
			var staffTypetype = $("#txtStaffType").val().trim();
			var description = $("#txtStaffDesc").val(); // ajax call for Staff Type validation
			description = description.replace(/'/g, "&#39");
			
			$.ajax({
				type: "POST",
				cache: false,
				url: "controllers/admin/stafftype.php",
				datatype:"json",  
				data:{
				
					stafftypetype:staffTypetype,
					Id:"",
					operation:"checkStaffType"
				},
				success:function(data) {
					if(data == "1")
					{
						  $("#txtStaffNameError").show();
						  $("#txtStaffNameError").text("The staff type is already exists");
						  $("#txtStaffType").addClass("errorStyle");
						  $("#txtStaffType").focus(); 
					}
					else{
						//ajax call for insert data into data base
						var postData = {
							"operation":"save",
							"staffTypetype":staffTypetype,
							"description":description
						}
						$.ajax(
							{					
							type: "POST",
							cache: false,
							url: "controllers/admin/stafftype.php",
							datatype:"json",
							data: postData,

							success: function(data) {
								if(data != "0" && data != ""){
									$('.modal-body').text("");
									$('#messageMyModalLabel').text("Success");
									$('.modal-body').text("Staff type saved successfully!!!");
									$('#messagemyModal').modal();
									$('#inactive-checkbox-tick').prop('checked', false).change();
									$("#example-datatable").dataTable().fnReloadAjax();
									clear();

									//after sucessful data sent to database redirect to datatable list
									$("#form_staff").hide();
				       				$(".blackborder").show();
				       				$("#tab_add_staff").css({
							            "background": "#fff",
							            "color": "#000"
							        });
							        $("#tab_staff").css({
							            "background": "background: linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200);",
							            "color": "#fff"
							        });
				        			$("#example-datatable").dataTable().fnReloadAjax();
								}
								
							},
							error:function() {
								alert('error');
							}
						});//  end ajax call
					}
				},
				error:function(){
					alert ("data not found");
				}
			});
		}
   }); //end button click function

    //validation
    $("#txtStaffType").keyup(function() {
        if ($("#txtStaffType").val() != '') {
            $("#txtStaffNameError").text("");
            $("#txtStaffType").removeClass("errorStyle");
        } 
    }); 
    if($('.inactive-checkbox').not(':checked')){
		//Datatable code
		 otable = $('#example-datatable').dataTable( {
			"bFilter": true,
			"processing": true,
			"sPaginationType":"full_numbers",
			"fnDrawCallback": function ( oSettings ) {
				// Need to redo the counters if filtered or sorted /
				/* if ( oSettings.bSorted || oSettings.bFiltered )
				{
					for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
					{
						$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
					}
				} */
				$('.update').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData = $('#example-datatable').dataTable().fnGetData(data);
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						var type = mData["type"];
						var description = mData["description"];
						editClick(id,type,description);
       
					}
				});
				$('.delete').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData =  $('#example-datatable').dataTable().fnGetData(data);
					
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						deleteClick(id);
					}
				});
			},
			"sAjaxSource":"controllers/admin/stafftype.php",
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "operation", "value": "show" });
			},
			"aoColumns": [
				/* {  "mData": "id" }, */
				{  "mData": "type" },
				{  "mData": "description" },	
				{  
					"mData": function (o) { 
					
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
					   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
					   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
					   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
					   "  data-original-title='Delete'></i>"; 
					}
				},
				],
			aoColumnDefs: [
				{ 'bSortable': false, 'aTargets': [ 1 ] },
			    { 'bSortable': false, 'aTargets': [ 2 ] }
			]

		} );
	}
	$('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	otable.fnClearTable();
	    	otable.fnDestroy();
	    	otable="";
	    	otable = $('#example-datatable').dataTable( {
				"bFilter": true,
				"processing": true,
				 "deferLoading": 57,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					// Need to redo the counters if filtered or sorted /
					/* if ( oSettings.bSorted || oSettings.bFiltered )
					{
						for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
						{
							$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
						}
					} */
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  $('#example-datatable').dataTable().fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/stafftype.php",
				"fnServerParams": function ( aoData ) {
					aoData.push( { "name": "operation", "value": "checked" });
				},
				"aoColumns": [
					/* { "mData": "id" }, */
					{  "mData": "type" },
					{ "mData": "description" },	
					{  
						"mData": function (o) { 
						
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},
				],
				aoColumnDefs: [
					{ 'bSortable': false, 'aTargets': [ 1 ] },
			        { 'bSortable': false, 'aTargets': [ 2 ] }
				]
			});
		}
		else{
			otable.fnClearTable();
	    	otable.fnDestroy();
	    	otable="";
			otable = $('#example-datatable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					// Need to redo the counters if filtered or sorted /
					/* if ( oSettings.bSorted || oSettings.bFiltered )
					{
						for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
						{
							$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
						}
					} */
					$('.update').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData = $('#example-datatable').dataTable().fnGetData(data);
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var type = mData["type"];
							var description = mData["description"];
							editClick(id,type,description);
		   
						}
					});
					$('.delete').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  $('#example-datatable').dataTable().fnGetData(data);
						
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							deleteClick(id);
						}
					});
				},
				"sAjaxSource":"controllers/admin/stafftype.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "show" });
				},
				"aoColumns": [
					/* {  "mData": "id" }, */
					{  "mData": "type" },
					{  "mData": "description" },	
					{  
						"mData": function (o) { 
						var data = o;
						return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
						   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
						   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
						   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
						   "  data-original-title='Delete'></i>"; 
						}
					},
					],
				aoColumnDefs: [
					{ 'bSortable': false, 'aTargets': [ 1 ] },
			        { 'bSortable': false, 'aTargets': [ 2 ] }
				]
			});
		}
		
    });
	
	
});

// edit data dunction for update 
function editClick(id,type,description){	
	$('.modal-body').text("");
	$('#myModalLabel').text("Update Staff Type");
	$('.modal-body').html($("#popUPForm").html()).show();
	$('#myModal').modal();
	$("#myModal #btnUpdate").removeAttr("style");
	$("#myModal #txtStaffType").removeClass("errorStyle");
	$("#myModal #txtStaffNameError").text("");
	$('#myModal #txtStaffType').val(type);
	$('#myModal #txtStaffDesc').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id); 
	 
	    //validation
     $("#myModal #txtStaffType").keyup(function() {
        if ($("#myModal #txtStaffType").val() != '') {
            $("#myModal #txtStaffNameError").text("");
			
            $("#myModal #txtStaffType").removeAttr("style");
        }
    }); 
	$("#myModal #btnUpdate").click(function(){ // click update button
		var flag = "false";
		if ($("#myModal #txtStaffType").val().trim() == '') {
			$("#myModal #txtStaffNameError").text("Please enter staff type");
			$('#myModal #txtStaffType').focus();
			$("#myModal #txtStaffType").css({
				"border-color": "#a94442"
			});
			flag = "true";
		}
		else
		{
			var staffTypetype = $("#myModal #txtStaffType").val().trim();
			var description = $("#myModal #txtStaffDesc").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();	
			$.ajax({						// ajax call for Staff Type validation
				type: "POST",
				cache: false,
				url: "controllers/admin/stafftype.php",
				datatype:"json",  
				data:{
				
					stafftypetype:staffTypetype,
					Id:id,
					operation:"checkStaffType"
				},
				success:function(data) {
					if(data == "1")
					{
						  $("#myModal #txtStaffNameError").show();
						  $("#myModal #txtStaffNameError").text("The staff type is already exists");
						  $("#myModal #txtStaffType").addClass("errorStyle");
						  $("#myModal #txtStaffType").focus(); 
					}
					else{
							var postData = {
							"operation":"update",
							"staffTypetype":staffTypetype,
							"description":description,
							"id":id
							}
							$.ajax( //ajax call for update data
							{					
								type: "POST",
								cache: false,
								url: "controllers/admin/stafftype.php",
								datatype:"json",
								data: postData,

								success: function(data) {
									if(data != "0" && data != ""){
									$('#myModal').modal('toggle');
									$('.modal-body').text("");
									$('#messageMyModalLabel').text("Success");
									$('.modal-body').text("Staff type updated successfully!!!");
									$('#messagemyModal').modal();
									$("#example-datatable").dataTable().fnReloadAjax();
												 
									clear();
						
									}
								},
								error:function() {
									alert('error');
								}
							});// end of ajax
					}
				},
				error:function(){
					alert ("data not found");
				}
			});
		}	
	});
	
		//now aData[0] - 1st column(count_id), aData[1] -2nd, etc.
		/* $("#form_staff").show();
		$(".blackborder").hide();
		$('#btnAddStaff').val("Update");
		$('#tab_add_staff').html("Update Staff");
		$("#tab_add_staff").css({
		"background-color": "#0c71c8",
		"color": "#fff"
		});
		$("#tab_staff").css({
		"background-color": "#fff",
		"color": "#000"
		});
		$("#staff_list").css({
		"background-color": "#fff"
		});
		$('#txtStaffType').val(type);
		$('#txtStaffDesc').val(description);
		$('#selectedRow').val(id); */
}// end update button
function deleteClick(id){ // delete click function
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete Staff Type");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	$('#selectedRow').val(id); 
	var type="delete";
	$('#confirm').attr('onclick','deleteStaffType("'+type+'");');
	
}// end click fucntion

function restoreClick(id){
	$('.modal-body').text("");
	$('#selectedRow').val(id);
	$('#confirmMyModalLabel').text("Restore Staff Type");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal(); 
	var type="restore";
	$('#confirm').attr('onclick','deleteStaffType("'+type+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		 /* window.location.href = "http://localhost/herp/"; */
		 $('.close').click();
    }
});

function clear(){
	$('#txtStaffType').val("");
	$("#txtStaffType").removeClass("errorStyle");
	$('#txtStaffNameError').text("");
	$('#txtStaffDesc').val("");
}
function deleteStaffType(type){
		if(type=="delete"){
			var id = $('#selectedRow').val();
			var postData = {
				"operation":"delete",
				"id":id
			}
			$.ajax( // ajax call for delete
				{					
				type: "POST",
				cache: false,
				url: "controllers/admin/stafftype.php",
				datatype:"json",
				data: postData,

				success: function(data) {
					if(data != "0" && data != ""){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Staff type deleted successfully!!!");
						$('#messagemyModal').modal();
						$("#example-datatable").dataTable().fnReloadAjax(); 
						
						
					}
					else{
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Sorry");
						$('.modal-body').text("This staff type is used , so you can not delete!!!");
						$('#messagemyModal').modal();
						
					}
				},
				error:function() {
						alert('error');
					}
				}); // end ajax 
		}
		else{
		var id = $('#selectedRow').val();
		$.ajax({
				type: "POST",
				cache: "false",
				url: "controllers/admin/stafftype.php",
				data :{            
					"operation" : "restore",
					"id" : id
				},
				success: function(data) {	
					if(data != "0" && data != ""){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Staff type restored successfully!!!");
						$('#messagemyModal').modal();
						$("#example-datatable").dataTable().fnReloadAjax(); 
						
						 clear();
						
					}			
				},
				error:function()
				{
					alert('error');
					  
				}
			});
		}
	}

//# sourceURL=stafftype.js