/*
 * File Name    :   patient_reg.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for save opeartion	
 */
 
var valueAppointment;
$(document).ready(function() {
    debugger;
    //removeErrorMessage();
	valueAppointment = $("#hdnRegister").val();
	if(valueAppointment == 1) {
		$(".block-title").hide();
	}

	loader();
	var flag = false;
    $('#reset').click(function() { // reset functionality
        clear();
        $("#val_Salutation").focus();
        $("#txtDOB").datepicker('setStartDate');
    });

    $('#txtGender').val("1");

    $('#txtDOB').datepicker({		// show datepicker
        endDate: '+0d',
        autoclose: true
    });
    $("#txtDOB").keyup(function() {
        $("#txtDOB").datepicker("show");
    });
	
	$("#val_Salutation").focus();
	
	// increase datapicker index for appointment screen
	$("#txtDOB").click(function() {
		$(".datepicker").css({
			"z-index": "99999999999"
		});
	});

    $('#val_Salutation').change(function(){
        if($('#val_Salutation').val() == "Mr.") {
            $('#txtGender').val("1");
        }
        else if($('#val_Salutation').val() == "Mrs.") {
            $('#txtGender').val("2");
        }
        else if($('#val_Salutation').val() == "Ms.") {
            $('#txtGender').val("2");
        }
        else {
             $('#txtGender').val("");
        }
    });
		
    // image upload js.
    $("#image-box").click(function() {
        $("#upload").click();
        $("#upload").on("change", function() {
		   $("#errorimage").hide();
		    $("#errorimage").text("");
            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

            if (/^image/.test(files[0].type)) {				// only image file
                var reader = new FileReader(); 				// instance of the FileReader
                reader.readAsDataURL(files[0]); 			// read the local file

                reader.onloadend = function() { 			// set image data as background of div
                    $("#user_reg_image").hide();
                    $("#imagetext").hide();
                    $("#image-box").css("background-image", "url(" + this.result + ")");

                }
            }
        });
    });

    // ESC button functionality	to close popup	
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            /* window.location.href = "http://localhost/herp/"; */
            $('.close').click();
        }
    });

    // Ajax call for show data on country 
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/patient.php",
        data: {
            "operation": "showcountry"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $('#txtCountry').html(option);
            }

        },
        error: function() {
			
			$('.modal-body').text("");
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
    });
    // Ajax call for show data on state select country 
    $('#txtCountry').change(function() {
        var countryID = $("#txtCountry").val();
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/patient.php",
            data: {
                "operation": "showstate",
                country_ID: countryID
            },
            success: function(data) {
                if (data != null && data != "") {
                    var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                    var option = "<option value=''>--Select--</option>";
                    for (var i = 0; i < parseData.length; i++) {
                        option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                    }
                    $('#txtState').html(option);
                    bindCountryCode(countryID);
                }

            },
            error: function() {}
        });
    });

    // Ajax call for show data on country select state 
    $('#txtState').change(function() {
        var stateID = $("#txtState").val();
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/patient.php",
            data: {
                "operation": "showcity",
                state_ID: stateID
            },
            success: function(data) {
                if (data != null && data != "") {
                    var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                    var option = "<option value=''>--Select--</option>";
                    for (var i = 0; i < parseData.length; i++) {
                        option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                    }
                    $('#txtCity').html(option);
                }

            },
            error: function() {}
        });
    });
	
	$("#txtFamilyAccount").keyup(function () {
		if ($("#txtFamilyAccount").val() != '') {
			$("#txtFamilyAccountError").text('');
			if ($("#txtFamilyAccount").val().length == 9) {
				var accountNo = $("#txtFamilyAccount").val();
				var accountPrefix = accountNo.substring(0, 3);
				accountNo = accountNo.replace ( /[^\d.]/g, '' ); 
				accountNo = parseInt(accountNo);
				
				var postData = {
					"operation":"checkAccount",
					"accountNo":accountNo,
					"accountPrefix":accountPrefix
				}
				
				$.ajax({
					type: "POST",
					cache: false,
					url: "controllers/admin/patient.php",
					datatype:"json",
					data: postData,
					
					success: function(data) {
						if(data != "0" && data != "" && data != "2"){
							var parseData = jQuery.parseJSON(data);
							$("#txtFamilyAccountError").show();
							$("#txtFamilyAccountError").text(parseData[0].name);
							flag = false;
						}
						else if(data == "2"){
							$("#txtFamilyAccountError").show();
							$("#txtFamilyAccountError").text("Please enter Correct Account No");
							$("#txtFamilyAccountError").addClass("errorStyle");
							$("#txtFamilyAccountError").focus();
							flag = true;
						}
						else{
							$("#txtFamilyAccountError").show();
							$("#txtFamilyAccountError").text("Please enter Correct Account No");
							$("#txtFamilyAccountError").addClass("errorStyle");
							$("#txtFamilyAccountError").focus();
							flag = true;
						}
					},
					error : function(){				
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
				});
			}
		}
	});
	
	// close popup
	$("#myAccountNoModal #pop_up_close").on("click",function(){
		 $('#myAccountNoModal').modal('hide');
	});
	$("#iconSearch").click(function() {	// show popup for search patient
		
		$("#txtFamilyAccountError").text("");
		
        $('#accountNoModalLabel').text("Search Patient");
		$("#hdnAccountNo").val("1");           
        $('#accountNoModalBody').load('views/admin/view_patients.html');
        $('#myAccountNoModal').modal();

    });
	
    /*removeErrorMessage();   //remove all error messages*/

    $('#txtmob').keyup(function() {
        if($('#txtmob').val() != ''){
            $('#txtmob').removeClass('errorStyle');
            $('#errormob').text('');
        }
    });

    //on submit button save information with validation	
    $("#submit").on('click', function() {      
		var flag = false;
		if ($("#txtFamilyAccount").val().trim() != "") {
			var accountLength = $("#txtFamilyAccount").val();
			if(accountLength.length < 9){
				$("#txtFamilyAccountError").show();
				$("#txtFamilyAccountError").text("Please enter Correct Account No");
				$("#txtFamilyAccountError").addClass("errorStyle");
				$("#txtFamilyAccountError").focus();
				flag = true;
			}           
        }
		
        if ($("#txtzipcode").val().trim() == "") {
            $("#errorcode").show();
            $("#errorcode").text("Please enter zip code");
            $("#txtzipcode").addClass("errorStyle");
            $("#txtzipcode").focus();
            flag = true;
        }
        if ($("#txtCity").val() == "") {
            $("#errorcity").show();
            $("#errorcity").text("Please select city");
            $("#txtCity").addClass("errorStyle");
            $("#txtCity").focus();
            flag = true;
        }
        if ($("#txtState").val() == "") {
            $("#errorstate").show();
            $("#errorstate").text("Please select state");
            $("#txtState").addClass("errorStyle");
            $("#txtState").focus();
            flag = true;
        }
        if ($("#txtCountry").val() == "") {
            $("#errorcountry").show();
            $("#errorcountry").text("Please select country");
            $("#txtCountry").addClass("errorStyle");
            $("#txtCountry").focus();
            flag = true;
        }
        /* if ($("#txtAddress").val().trim() == "") {
            $("#erroraddress").show();
            $("#erroraddress").text("Please enter your address");
            $("#txtAddress").css({"border-Color": "#a94442"});
			$("#txtAddress").focus();
			flag=true;
        } */
        if ($("#txtmob").val().trim() == "" || !validnumber($("#txtmob").val())) {
            $("#errormob").show();
            $("#errormob").text("Please enter valid mobile number");
            $("#txtmob").addClass("errorStyle");
            $("#txtmob").focus();
            flag = true;
        }
        if ($('#txtmob').val().trim().charAt(0) == '0') {
            $("#errormob").show();
            $("#errormob").text("Please enter valid mobile number");
            $("#txtmob").addClass("errorStyle");
            $("#txtmob").focus();
            flag = true;
        }
        if (!ValidateEmail($("#txtEmail").val())) {
         	$("#txtEmail").val("");
        }
        if ($("#txtDOB").val() == "") {
            $("#errordob").show();
            $("#errordob").text("Please select date");
            $("#txtDOB").addClass("errorStyle");

            flag = true;
        }
        if ($("#txtGender").val() == "") {
            $("#errorgender").show();
            $("#errorgender").text("Please select gender");
            $("#txtGender").addClass("errorStyle");
            $("#txtGender").focus();
            flag = true;
        }
		if ($("#txtLastName").val().trim() == "") {
            $("#errorlname").show();
            $("#errorlname").text("Please enter last name");
            $("#txtLastName").addClass("errorStyle");
            $("#txtLastName").focus();
            flag = true;
        }
        if ($("#txtFirstName").val().trim() == "") {
            $("#errorfname").show();
            $("#errorfname").text("Please enter first name");
            $("#txtFirstName").addClass("errorStyle");
            $("#txtFirstName").focus();
            flag = true;
        }
        
        if ($("#val_Salutation").val() == "-1") {
            $("#errorsalutation").show();
            $("#errorsalutation").text("Please select salutation");
            $("#val_Salutation").focus();
            $("#val_Salutation").addClass("errorStyle");
            flag = true;
        }/*
        if ($('#txtmob').val().charAt(0) == '0') {
            $("#errormob").text("Please enter valid mobile number");
            $("#txtmob").addClass("errorStyle");
            $("#txtmob").focus();
            flag = true;
        }*/

        if (flag == true) {
            return false;

        } else {
            // define variable to get value
            var salutation = $("#val_Salutation").val();
            var lname = $("#txtLastName").val().trim();

            lname =  lname.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });

            var fname = $("#txtFirstName").val().trim();

            fname =  fname.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });


            var middleName = $("#txtMiddleName").val().trim();

            middleName =  middleName.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
            
            var dob = $("#txtDOB").val();
            var gender = $("#txtGender").val();
            var address = $("#txtAddress").val().trim();
            var country = $("#txtCountry").val();
            var state = $("#txtState").val();
            var city = $("#txtCity").val();
            var zipcode = $("#txtzipcode").val().trim();
            var email = $("#txtEmail").val().trim();
            var mobile =  $("#txtmob").val().trim();
            var mobileCode = $("#mobileCode").val().trim()
            var phone = $("#txtphone").val().trim();
            var careOff = $("#val_CO").val().trim();
            var careOffPhone = $("#val_COPhoneNo").val().trim();
			if($("#txtFamilyAccount").val()!=''){
				var accountNo = $("#txtFamilyAccount").val();
				var accountPrefix = accountNo.substring(0, 3);
				accountNo = accountNo.replace ( /[^\d.]/g, '' ); 
				accountNo = parseInt(accountNo);
			}

            var file_data = $('#upload').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            var imageName;
            $.ajax({
                url: 'controllers/admin/patient.php', // point to server-side PHP script 
                dataType: 'text', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(data) {
                    if (data == "invalid file") {
                        $("#errorimage").show();
                        $("#errorimage").text("Please upload image file only.");
                    } else {
                        imageName = data;
                        // for ajax call for insert
                        $.ajax({
                            type: 'POST',
                            cache: false,
                            url: "controllers/admin/patient.php",
                            data: {
                                salutation: salutation,
                                Lname: lname,
                                Fname: fname,
                                middleName: middleName,
                                Dob: dob,
                                Gender: gender,
                                Address: address,
                                Country: country,
                                State: state,
                                City: city,
                                Zipcode: zipcode,
                                Email: email,
                                Mobile: mobile,
                                mobileCode:mobileCode,
                                Phone: phone,
                                CareOff: careOff,
                                CareOffPhone: careOffPhone,
                                imageName: imageName,
                                accountNo: accountNo,
                                operation: "save"
                            },
                            success: function(data) {
                                 if (data != "0" && data != "") {
									var parseData = jQuery.parseJSON(data);							
									if(valueAppointment != 1) {
										$('#regPatientModalLabel').text("Success");	
										$("#success").text("Patient registered successfully !!!");
										for (var i = 0; i < parseData.length; i++) {
											var name = parseData[i].salutation + " " + parseData[i].first_name +" "+parseData[i].middle_name+ " " + parseData[i].last_name;
											
											$("#lblPatientName").text(name);
                                            var mobile = parseData[i].country_code + parseData[i].mobile;
											$("#lblMobile").text(mobile);
											var gender = parseData[i].gender;
											var getDOB = parseData[i].dob;
											var patientId = parseInt(parseData[i].id);
											var prefix = parseData[i].prefix;
											var patientIdLength = patientId.toString().length;
											for (var i=0;i<6-patientIdLength;i++) {
												patientId = "0"+patientId;
											}
											patientid = prefix+patientId;
											$("#lblPatientId").text(patientid);
											
										}
										if(gender == "F"){
											$("#lblGender").text("Female");
										}
										else{
											$("#lblGender").text("Male");
										}
										var new_age = getAge(getDOB);
										var split = new_age.split(' ');
										var age_years = split[0];
										var age_month = split[1];
										var age_day = split[2];
										$("#lblAge").html(age_years + "yr" + " " + age_month + "mon" + " " + age_day + "day");
										$('#myPatientRegModal').modal();
									}
									else {
										var patientId = parseInt(parseData[0].id);
										var prefix = parseData[0].prefix;
										var patientIdLength = patientId.toString().length;
										for (var i=0;i<6-patientIdLength;i++) {
											patientId = "0"+patientId;
										}
										patientid = prefix+patientId;
										$("#txtPatientId").val(patientid);
										$(".close_patient").unbind();
										$(".close_patient").click();
									}
									clear();
                                    $("#txtDOB").datepicker('setStartDate');
									$("#val_Salutation").focus();
                                    $("#val_Salutation").val('Mr.');
                                    $('#txtGender').val('1');
                                    $('#val_Salutation').change(function(){
                                        if($('#val_Salutation').val() == "Mr.") {
                                            $('#txtGender').val("1");
                                        }
                                        else if($('#val_Salutation').val() == "Mrs.") {
                                            $('#txtGender').val("2");
                                        }
                                        else if($('#val_Salutation').val() == "Ms.") {
                                            $('#txtGender').val("2");
                                        }
                                        else {
                                             $('#txtGender').val("");
                                        }
                                    });
								}
                            },
                            error: function() {
								
								$('.modal-body').text("");
								$('#messageMyModalLabel').text("Error");
								$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
								$('#messagemyModal').modal();
							}
                        });
                    }
                }
            });
        }
		});

    // for remove error style	
    $("input[type=text]").keyup(function() {

        if ($(this).val() != "") {
            $(this).next("span").hide();
            $(this).removeClass("errorStyle");
            //	$("label[for='"+$(this).attr("id")+"']").removeClass("errorStyle");
        }
    });
    $("#txtmob").on("keydown", function(e) {
        return e.which !== 32;
    });   

    $("select").on('change', function() {

        if ($(this).val() != "") {
            $(this).next("span").hide();
            /*  $(this).removeClass("errorStyle"); */

            $(this).removeClass("errorStyle");
            //	$("label[for='"+$(this).attr("id")+"']").removeClass("errorStyle");
        }
    });

    $("#val_Salutation").change(function() {
        if ($("#val_Salutation").val() != "-1") {
            $("#val_Salutation").removeClass("errorStyle");
            $("#errorsalutation").text("");
        }
    });
    $("#txtCountry").change(function() {
        if ($("#txtCountry").val() != "") {
            $("#txtCountry").removeClass("errorStyle");
            $("#errorcountry").text("");
        }
    });
    $("#txtState").change(function() {
        if ($("#txtState").val() != "") {
            $("#txtState").removeClass("errorStyle");
            $("#errorstate").text("");
        }
    });
    $("#txtCity").change(function() {
        if ($("#txtCity").val() != "") {
            $("#txtCity").removeClass("errorStyle");
            $("#errorcity").text("");
        }
    });
    $("#txtGender").change(function() {
        if ($("#txtGender").val() != "") {
            $("#txtGender").removeClass("errorStyle");
            $("#errorgender").text("");
        }
    });
	$("#txtDOB").change(function() {
		if ($('#txtDOB').val() != "") {
			$('#txtDOB').removeClass("errorStyle");
			$("#errordob").text("");
		}
	});
	
	$("#txtDOB").change(function() {
        var user_date = $("#txtDOB").val();
        /* 	var user_date= $('#txtDOB').datepicker({ dateFormat: 'dd/mm/yyyy' }).val(); */
        var new_age = getAge(user_date);

        var split = new_age.split(' ');
        var age_years = split[0];
        var age_month = split[1];
        var age_day = split[2];

        $("#txt_year").val(age_years);
        $("#txt_month").val(age_month);
        $("#txt_day").val(age_day);
    });

});
//clear function
function clear() {
    $("#val_Salutation").val("-1");
    $("#txtLastName").val("");
    $("#txtFirstName").val("");
    $("#txtMiddleName").val("");
    $("#txtDOB").val("");
    $("#txtGender").val("");
    $("#txtAddress").val("");
    $("#txtCountry").val("");
    $("#txtState").val("");
    $("#txtCity").val("");
    $("#txtmob").val("");
    $("#txtphone").val("");
    $("#txtEmail").val("");
    $("#val_CO").val("");
    $("#val_COPhoneNo").val("");
    $("#txtzipcode").val("");
    $("#txt_year").val("");
    $("#txt_month").val("");
    $("#txt_day").val("");
    $("#upload").val("");
    $("#mobileCode").val("");
    $("#txtFamilyAccount").val("");
    $("#txtFamilyAccountError").text("");
    $("#errorsalutation").text("");
    $("#errorlname").text("");
    $("#errorfname").text("");
    $("#errorfathername").text("");
    $("#errorgender").text("");
    $("#errordob").text("");
    $("#erroremail").text("");
    $("#errormob").text("");
    $("#erroraddress").text("");
    $("#errorcountry").text("");
    $("#errorstate").text("");
    $("#errorcity").text("");
    $("#errorcode").text("");
    
    $("#user_reg_image").show();
    $("#imagetext").show();
    $("#image-box").css({
        "background-image": "#fff"
    });
    $("#errorimage").hide();
    $("#errorimage").text("");
    $("#val_Salutation").removeClass("errorStyle");
    $("#txtLastName").removeClass("errorStyle");
    $("#txtMiddleName").removeClass("errorStyle");
    $("#txtFirstName").removeClass("errorStyle");
    $("#txtGender").removeClass("errorStyle");
    $("#txtDOB").removeClass("errorStyle");
    $("#txtmob").removeClass("errorStyle");
    $("#txtCountry").removeClass("errorStyle");
    $("#txtzipcode").removeClass("errorStyle");
    $("#txtCity").removeClass("errorStyle");
    $("#txtState").removeClass("errorStyle");
}

// check email validation
function ValidateEmail(email) {
	var expr = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;;
	return expr.test(email);
};
// check mobile number validation
function validnumber(number) {
	intRegex = /^\d{9,15}$/;
	return intRegex.test(number);
};
// calculate age from date
function getAge(dateString) {
	var now = new Date();
	var today = new Date(now.getYear(), now.getMonth(), now.getDate());

	var yearNow = now.getYear();
	var monthNow = now.getMonth();
	var dateNow = now.getDate();

	var dob = new Date(dateString.substring(0, 4), dateString.substring(5, 7) - 1, dateString.substring(8, 10));

	var yearDob = dob.getYear();
	var monthDob = dob.getMonth();
	var dateDob = dob.getDate();
	var age = {};
	var ageString = "";
	var yearString = "";
	var monthString = "";
	var dayString = "";


	yearAge = yearNow - yearDob;

	if (monthNow >= monthDob)
		var monthAge = monthNow - monthDob;
	else {
		yearAge--;
		var monthAge = 12 + monthNow - monthDob;
	}

	if (dateNow >= dateDob)
		var dateAge = dateNow - dateDob;
	else {
		monthAge--;
		var dateAge = 31 + dateNow - dateDob;

		if (monthAge < 0) {
			monthAge = 11;
			yearAge--;
		}
	}

	age = {
		years: yearAge,
		months: monthAge,
		days: dateAge
	};

	if (age.years > 1) yearString = " years";
	else yearString = " year";
	if (age.months > 1) monthString = " months";
	else monthString = " month";
	if (age.days > 1) dayString = " days";
	else dayString = " day";


	if ((age.years > 0) && (age.months > 0) && (age.days > 0))
		ageString = age.years + " " + age.months + " " + age.days + "";

	else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
		ageString = age.years + " " + age.months + " " + age.days + "";

	else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
		ageString = age.years + " " + age.months + " " + age.days + "";

	else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
		ageString = age.years + " " + age.months + " " + age.days + "";

	else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
		ageString = age.years + " " + age.months + " " + age.days + "";

	else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
		ageString = age.years + " " + age.months + " " + age.days + "";

	else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
		ageString = age.years + " " + age.months + " " + age.days + "";

	else ageString = "00"+" "+"00"+" "+"00"+" ";

	return ageString;
}
function bindCountryCode(countryID) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/hospital_profile.php",
        data: {
            "operation": "showCountryCode",
            countryID: countryID
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                
                var countryCode = '+' + parseData.country_code;
                $("#mobileCode").val(countryCode);
                
               
            }
        },
        error: function() {}
    });
}