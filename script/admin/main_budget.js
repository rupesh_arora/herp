var mainBudgetTable;
$(document).ready(function(){
    loader();
    debugger;

    mainBudgetTable = $('#mainBudgetTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "fnDrawCallback": function(oSettings) {
        },

        "sAjaxSource": "controllers/admin/main_budget.php",
        "fnServerParams": function(aoData) {
            aoData.push({
                "name": "operation",
                "value": "show"
            });
        },
        "aoColumns": [
            {
                "mData": "account_no"
            }, {
                "mData": "account_name"
            }, {
                "mData": "department_name"
            }, {
                "mData": "amount"
            }, 
        ],
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [1,2]
        }]
    });

});