var oTableRoom; //define global variable for datatable	
$(document).ready(function() {
    loader();
    
    /*By default hide this add screen part*/
    $("#advanced-wizard").hide();
    $("#roomList").css({
        "background-color": "#fff"
    });
    $("#tabRoomList").css({
        "background-color": "#0c71c8",
        "color": "#fff"
    });
    /*Click for go to add the room type screen part*/
    $("#tabAddRoom").click(function() {
        $(".blackborder").hide();
        $("#advanced-wizard").show();
        $("#tabAddRoom").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tabRoomList").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#roomList").css({
            "background": "#fff"
        });
        $("#selRoomType").focus();
        clear();
        bindRoomType();
    });
    /*Click function for show the room lists*/
    $("#tabRoomList").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
        tabRoomList(); // Call the function for show the room lists
    });

    /*Call function to bind drop down data*/
    bindRoomType();

    /*By default when radio button is not checked show all active data*/
    if ($('.inactive-checkbox').not(':checked')) {
        //Datatable code
        loadDataTable();
    }
    /*On change of radio button check it is checked or not*/
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) {
            oTableRoom.fnClearTable();
            oTableRoom.fnDestroy();
            //Datatable code
            oTableRoom = $('#roomTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = oTableRoom.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var room_id = mData["room_type_id"];
                            restoreClick(id, room_id); //call restore click function with certain parameter to restore
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/room.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "showInActive"
                    });
                },
                "aoColumns": [{
                    "mData": "type"
                }, {
                    "mData": "number"
                }, {
                    "mData": function(o){

                        if (o.availability =="0") {
                            return "<span>No</span>";
                        }
                        else{
                            return "<span>Yes</span>";
                        }                    
                    }
                },{
                    "mData" :"description"
                },{
                    "mData": function(o) {
                        var data = o;
                        return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                    }
                }, ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [4]
                }, {
                    'bSortable': false,
                    'aTargets': [3]
                }, {
                    'bSortable': false,
                    'aTargets': [2]
                }]
            });
        } else {
            oTableRoom.fnClearTable();
            oTableRoom.fnDestroy();
            //Datatable code
            loadDataTable();
        }
    });

    /*Save data on click*/
    $("#btnSubmit").click(function() {

        /*Check radio button checked or not*/
        if ($('#checkOPDRoom').is(":checked")==false) {
            $('#checkOPDRoom').val(0);
        }
        else{
            $('#checkOPDRoom').val(1);
        }
        /*perform validation*/
        var flag = "false";
        $("#txtRoomNumber").val($("#txtRoomNumber").val().trim());
        if ($("#selRoomType").val() == "-1") {
            $('#selRoomType').focus();
            $("#selRoomTypeError").text("Please select room type");
            $("#selRoomType").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtRoomNumber").val() == "") {
            $('#txtRoomNumber').focus();
            $("#txtRoomNumberError").text("Please enter room number");
            $("#txtRoomNumber").addClass("errorStyle");
            flag = "true";
        }       
        if (flag == "true") {
            return false;
        }
        
        var roomType = $("#selRoomType").val();
        var roomNumber = $("#txtRoomNumber").val();
        var description = $("#txtDescription").val();
        var availability = $('#checkOPDRoom').val();
        description = description.replace(/'/g, "&#39");
        var postData = {
                "operation": "save",
                "roomType": roomType,
                "roomNumber": roomNumber,
                "availability":availability,
                "description": description
            }
            /*AJAX call to save data*/
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/room.php",
            datatype: "json",
            data: postData,
            success: function(data) {
                if (data != "0" && data != "") {
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Room saved successfully!!!");
                    $('#messagemyModal').modal(); //show modal
					$('#inactive-checkbox-tick').prop('checked', false).change();
                    tabRoomList();
                  
                } else {
                    $("#txtRoomNumberError").text("Room name already exists");
                    $("#txtRoomNumber").focus();
                }
            },
            error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
        });
    });

    //keyup functionality   
    $("#selRoomType").change(function() {
        if ($("#selRoomType").val() != "-1") {
            $("#selRoomTypeError").text("");
            $("#selRoomType").removeClass("errorStyle");
        }
    });
    $("#txtRoomNumber").keyup(function() {
        if ($("#txtRoomNumber").val() != "") {
            $("#txtRoomNumberError").text("");
            $("#txtRoomNumber").removeClass("errorStyle");
        }
    });
    // key press event on ESC button
	$(document).keyup(function(e) {
	    if (e.keyCode == 27) {
	        $(".close").click();
	    }
	});

	/*reset button functionality*/
	$("#btnReset").click(function() {
	    clear();
	    $("#selRoomType").removeClass("errorStyle");
	    $("#selWard").removeClass("errorStyle");
	    $("#txtRoomNumber").removeClass("errorStyle");
	    bindRoomType();
	    $("#selRoomType").focus();
	});
});

/*define edit Click function for update*/
function editClick(id, room_type_id, number, description,availabilityOPD) {
    
    $('#myModalLabel').text("Update Room");
    $('.modal-body').html($("#roomModel").html()).show();
    $("#myModal #btnUpdateRoom").removeAttr("style");
    $("#myModal #selRoomType").removeClass("errorStyle");
    $("#myModal #txtRoomNumber").removeClass("errorStyle");

    $('#myModal #selRoomType').val(room_type_id);
    $('#myModal #txtRoomNumber').val(number);
    $('#myModal #txtDescription').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
    $('#myModal').modal('show');
	$('#myModal').on('shown.bs.modal', function() {
        $("#myModal #selRoomType").focus();
    });

    $("#myModal #checkOpdRoomPointer").click(function(){
        if ($('#myModal #checkOPDRoom').is(":checked") == true) {
            $("#myModal #checkOPDRoom").prop('checked', false);
        }
        else{
            $("#myModal #checkOPDRoom").prop('checked', true);
        }
        
    });

    if (availabilityOPD == 0) {
        $('#myModal #checkOPDRoom').prop('checked', false);
    }
    else{
        $('#myModal #checkOPDRoom').prop('checked', true);
    }

    /*Click function for update room*/
    $("#myModal #btnUpdateRoom").click(function() {

        /*Check radio button checked or not*/
        if ($('#myModal #checkOPDRoom').is(":checked")==false) {
            $('#myModal #checkOPDRoom').val(0);
        }
        else{
            $('#myModal #checkOPDRoom').val(1);
        }

        /*perform validation*/
        var flag = "false";
        $("#myModal #txtRoomNumber").val($("#myModal #txtRoomNumber").val().trim());
        if ($("#myModal #txtRoomNumberError").text() != "") {
            flag = "true";
        }
        if ($("#myModal #selRoomType").val() == "-1") {
            $('#myModal #selRoomType').focus();
            $("#myModal #selRoomTypeError").text("Please select room type");
            $("#myModal #selRoomType").addClass("errorStyle");
            flag = "true";
            $("#selRoomType").focus();
        }
        if ($("#myModal #txtRoomNumber").val() == "") {
            $('#myModal #txtRoomNumber').focus();
            $("#myModal #txtRoomNumberError").text("Please enter room number");
            $("#myModal #txtRoomNumber").addClass("errorStyle");
            flag = "true";
            $("#txtRoomNumber").focus();
        }
        if (flag == "true") {
            return false;
        }
		else{
			var roomType = $("#myModal #selRoomType").val();
			var roomNumber = $("#myModal #txtRoomNumber").val();
			var description = $(" #myModal #txtDescription").val();
			description = description.replace(/'/g, "&#39");
			var availability = $('#myModal #checkOPDRoom').val();
			var id = $('#selectedRow').val();			
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				/*AJAX call to update*/
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/room.php",
					data: {
						"operation": "update",
						"id": id,
						"availability":availability,
						"roomType": roomType,
						"roomNumber": roomNumber,
						"description": description
					},
					success: function(data) {
						if (data != "0" && data != "") {
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Room updated successfully!!!");
							oTableRoom.fnReloadAjax();
							$('#messagemyModal').modal();
							clear();
							$("#selRoomType").focus();
						}
						if (data == "0") {
							$("#myModal #txtRoomNumberError").text("Room name already exists");
							$("#myModal #txtRoomNumber").focus();
							$("#myModal #txtRoomNumber").addClass("errorStyle");
						}
					},
					error:function() {
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messagemyModal').modal();
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					}
				});
			});
		}
    });

    /*Remove style*/
    $("#myModal #selRoomType").change(function() {
        if ($("#myModal #selRoomType").val() != "-1") {
            $("#myModal #selRoomTypeError").text("");
            $("#myModal #selRoomType").removeClass("errorStyle");
        }
    });
    $("#myModal #txtRoomNumber").keyup(function() {
        if ($("#myModal #txtRoomNumber").val() != "") {
            $("#myModal #txtRoomNumberError").text("");
            $("#myModal #txtRoomNumber").removeClass("errorStyle");
        }
    });
}

/*define delete Click function for delete*/
function deleteClick(id) {
    $('#selectedRow').val(id);
	$('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Room");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();

    var type = "delete";
    $('#confirm').attr('onclick', 'deleteRoom("' + type + '");'); //pass attribute with sent opertion and function with it's type		
}

function restoreClick(id, room_id) {
    $('#selectedRow').val(id);
	$('.modal-body').text("");
    $('#confirmMyModalLabel').text("Restore Room");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();

    var type = "restore";
    $('#confirm').attr('onclick', 'deleteRoom("' + type + '",' + room_id + ');');
}

function deleteRoom(type, room_id) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        /*AJAX call to delete*/
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/room.php",
            data: {
                "operation": "delete",
                "id": id
            },
            success: function(data) {
                if (data == "1") {
					$('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Room deleted successfully!!!");
					oTableRoom.fnReloadAjax();
                    $('#messagemyModal').modal();
                } 
				if (data == "0") {
					$('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This room is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error:function() {
				$('.modal-body').text("");
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
        });
    } else {
        var id = $('#selectedRow').val();
        /*AJAX call to restore*/
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/room.php",
            data: {
                "operation": "restore",
                "id": id,
                "room_id": room_id
            },
            success: function(data) {
                if (data == "1") {
                    $('.modal-body').text('');
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Room restored successfully!!!");
                    $("#roomTable").dataTable().fnReloadAjax();
                    $('#messagemyModal').modal();
                }
                if (data == "0") {
					$('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("It can not be restore!!!");
					oTableRoom.fnReloadAjax();
                    $('#messagemyModal').modal();
                }
            },
            error:function() {
				$('.modal-body').text("");
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
        });
    }
}

function bindRoomType() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/room.php",
        data: {
            "operation": "showRoomType"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
                }
                $('#selRoomType').html(option);
            }
        },
       error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
    });
}

function tabRoomList() {
    $("#advanced-wizard").hide();
    $(".blackborder").show();
    
    $("#tabAddRoom").css({
        "background": "#fff",
        "color": "#000"
    });
    $("#tabRoomList").css({
        "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
        "color": "#fff"
    });
    $("#roomList").css({
        "background": "#fff"
    });
    clear();
    $("#selWard").removeClass("errorStyle");
    $("#txtRoomNumber").removeClass("errorStyle");
}

function clear() {
    $('#selRoomType').val("");
    $('#selRoomTypeError').text("");
    $('#txtRoomNumber').val("");
    $('#txtRoomNumberError').text("");
    $('#txtDescription').val("");
    $('#checkOPDRoom').prop('checked', false);
}


function loadDataTable(){
oTableRoom = $('#roomTable').dataTable({
		"bFilter": true,
		"processing": true,
		"sPaginationType": "full_numbers",
		"fnDrawCallback": function(oSettings) {
		   
			/*On click of update icon call this function*/
			$('.update').unbind();
			$('.update').on('click', function() {
				var data = $(this).parents('tr')[0];
				var mData = oTableRoom.fnGetData(data); //get datatable data
				if (null != mData) // null if we clicked on title row
				{
					var id = mData["id"];
					var room_type_id = mData["room_type_id"];
					var number = mData["number"];
					var description = mData["description"];
					var availabilityOPD = mData["availability"];
					editClick(id, room_type_id, number, description,availabilityOPD); //call edit click function with certain parameters to update		   
				}
			});
			/*On click of delete icon call this function*/
			$('.delete').unbind();
			$('.delete').on('click', function() {
				var data = $(this).parents('tr')[0];
				var mData = oTableRoom.fnGetData(data);

				if (null != mData) // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id); //call delete click function with certain parameter to delete
				}
			});
		},

		"sAjaxSource": "controllers/admin/room.php",
		"fnServerParams": function(aoData) {
			aoData.push({
				"name": "operation",
				"value": "show"
			});
		},
		/*defining datatables column*/
		"aoColumns": [{
			"mData": "type"
		}, {
			"mData": "number"
		}, 
		{
			"mData": function(o){
				if (o.availability =="0") {
					return "<span>No</span>";
				}
				else{
					return "<span>Yes</span>";
				}                    
			}
		}, 
		{
			"mData": "description"
		},
		{
			"mData": function(o) {
				var data = o;
				return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
					" style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
					" <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
					" style='font-size: 22px; color:#a94442; cursor:pointer;' " +
					" data-original-title='Delete'></i>";
			}
		}, ],
		/*Disable sort for following columns*/
		aoColumnDefs: [{
			'bSortable': false,
			'aTargets': [4]
		}, {
			'bSortable': false,
			'aTargets': [3]
		}, {
			'bSortable': false,
			'aTargets': [2]
		}]
	});
}
//# sourceURL = room.js