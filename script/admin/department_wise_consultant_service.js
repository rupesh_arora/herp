var deptOPDServiceTbl = '';
$(document).ready(function() {
	debugger;
	$(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});
	deptOPDServiceTbl = $('#deptOPDServiceTbl').DataTable();
	$("#toDate").attr("disabled",true);
	$("#fromDate").attr("disabled",true);	

	$('.changeRadio').change(function() {
		if ($('#radToFromDate').prop( "checked") == true) {
	        $("#toDate").attr("disabled",false);
			$("#fromDate").attr("disabled",false);
	    }
	    else{
	    	$("#toDate").attr("disabled",true);
			$("#fromDate").attr("disabled",true);
			$("#toDate").val("");
			$("#fromDate").val("");
	    }
	});	
	$("#fromDate").change(function() {
		if ($("#fromDate").val() !='') {
			$("#fromDate").removeClass("errorStyle");
			$("#txtFromDateError").text("");
		}
	});
	$("#toDate").change(function() {
		if ($("#toDate").val() !='') {
			$("#toDate").removeClass("errorStyle");
			$("#txtToDateError").text("");
		}
	});	

	$("#viewReport").on("click",function(){
		var fromDate ;
		var toDate ;
		var dataLoad = '';
		var arr = new Array();
		var opts = new Array();
		if ($('#radToday').prop( "checked") == true) {
			dataLoad = "today";
		}
		else if($('#radLastSevenDays').prop( "checked") == true) {
			dataLoad = "last7days";
		}
		else if($('#radToFromDate').prop( "checked") == true) {
			dataLoad = "betweenDate";
			fromDate = $("#fromDate").val();
			toDate = $("#toDate").val();
			if (fromDate =='') {
				$("#txtFromDateError").text("Please choose from date");
				$("#fromDate").addClass("errorStyle");
				$("#fromDate").focus();
				return false;
			}
			if (toDate =='') {
				$("#txtToDateError").text("Please choose from date");
				$("#toDate").addClass("errorStyle");
				$("#toDate").focus();
				return false;
			}
		}
		else if($('#radLastThirtyDays').prop( "checked") == true) {
			dataLoad = "last30days";
		}
		else if($('#radAllTime').prop( "checked") == true) {
			dataLoad = "all";
		}

		if (fromDate == undefined) {
			fromDate ="";
		}
		if (toDate == undefined) {
			toDate = "";
		}
		if (dataLoad=='') {
			return false;
		}
		var postData = {
			"operation" : "showChartData",
			"dataLoad" : dataLoad,
			"fromDate" : fromDate,
			"toDate" : toDate
		}
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/department_wise_consultant_service.php",
			data: postData,
			success: function(data) {				
				var parseData =  JSON.parse(data);

				if(parseData != null && parseData != ""){
					deptOPDServiceTbl.fnClearTable();
					$("#btnSavePdf").show();

					$.each(parseData,function(index,v){

						var departmentId = v['Department Id'];
                        var departmentName = v['Department Name'];                  
                        var countServices = v['Total Services'];                  
                        var serviceName = v['Service Name'];
                        deptOPDServiceTbl.fnAddData([departmentId,departmentName,serviceName,countServices]);
					});


					
				}

				else {
					clear();
					$('#messagemyModal').modal();
					$('#messageMyModalLabel').text("Alert");
					$('.modal-body').text("No data exist!!!");
					$("#btnSaveExcel,#btnSavePdf").hide();
					return false;
				}

				$("#btnSaveExcel").show();

				/*function to save in to excel file*/
				window.saveFile = function saveFile () {
					var opts = [{sheetid:'One',header:true}];
					var res = alasql('SELECT INTO XLSX("Department-Wise-OPD Services.xlsx",?) FROM ?',[opts,[parseData]]);
				}

			}
		});
	});

	$("#btnSavePdf").click(function(){
		$.print("#printableArea");
	});

	$("#clear").click(function(){
		clear();
	});
});


function clear(){
	$(".chartDiv,#btnSaveExcel,#btnSavePdf").hide();
	$("#toDate,#fromDate").val("");
	$("#txtFromDateError,#txtToDateError").text("");
	$("#fromDate,#toDate").removeClass("errorStyle");
	deptOPDServiceTbl.fnClearTable();
}