$(document).ready(function() {	
debugger;
	loader();
    var otable;
	var labRequestId;
	var labTestId;
	otable = $('#labRequestTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {				
				$('.process').on('click',function(){
					clear();

					var tableData = otable.fnGetData($(this).parent().parent()[0]);
					var visitId = tableData['visit_id'];
					var patientId = tableData['patient_id'];
					labTestId = tableData['lab_test_id']
					
					var requestId = $(this).attr('data-val');
					labRequestId = requestId;
					var data=$(this).parents('tr')[0];
					var mData = otable.fnGetData(data);
					if (null != mData)  // null if we clicked on title row
					{
						$('#myLabModal').modal();
						if(mData["patient_id"] != "")
						{
							$('#txtPatientID').val(mData["patient_id"]);
						}
						$('#hdnLabTestRequestId').val(mData["id"]);						
						$('#txtPatientName').text(mData["salutation"] + " " + mData["first_name"]+ " "+mData['middle_name']+" " + mData["last_name"]);
						if(mData["images_path"] != "")
						{							
							$('#imgPatient').attr("src",'./upload_images/patient_dp/'+mData["images_path"]);
						}
						else{
							$('#imgPatient').attr("src",'./img/default.jpg');
						}
						/* $('#txtSonOf').text(); */
						if(mData["gender"] == "M"){
							$('#txtGender').text("Male");
						}
						else{
							$('#txtGender').text("Female");
						}
						$('#txtDob').text(mData["dob"]);
						
						var dob= mData["dob"];
						var new_age = getAge(dob);
						var split = new_age.split(' ');
						var age_years = split[0];
						var age = parseInt(age_years)+1;						
						$("#txtAge").text(age);
						
						/* $('#txtAge').text(mData["age"]); */							
						$('#txtEmail').text(mData["email"]);					
						$('#txtMobile').text(mData["mobile"]);					
											
						$('#txtSpecimenType').text(mData["type"]);	
						$('#txtLabTest').text(mData["name"]);			
						clear();
						postData={
							visitId : visitId,
							patientId : patientId,
							labTestId : labTestId,
							"operation":"getComponentName"
						}
						var tempName = "";
						$.ajax({
							type: "POST",
							cache: false,
							url: "controllers/admin/view_lab_request.php",
							datatype: "json",
							data: postData,

							success: function(data) {	
								var dataSet = jQuery.parseJSON(data);
								
								if(dataSet.length != 0) {
									$(dataSet).each(function( index ){
									if (dataSet[index].component_name == null) {
										tempName += (index+1)+'.  &nbsp; '+"N/A" + ".<br>";
									}
									else {
										tempName += (index+1)+'.  &nbsp; '+dataSet[index].component_name + ".<br>";
									}								
																		

									});
									$('#txtLabTestComponent').html(tempName);
								}
								else{
									$('#txtLabTestComponent').text("N/A");
								}
								
							},
							error:function() {
								$('#messageMyModalLabel').text("Error");
								$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
								$('#messagemyModal').modal();
							}
						});
					}
					else{
						clear();
					}
				});
			},
			
			"sAjaxSource":"controllers/admin/view_lab_request.php",
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "operation", "value": "show" });
			},
			"aoColumns": [
				{  
					"mData": function (o) { 	
					var visitId = o["visit_id"];
					var patientPrefix = o["visit_prefix"];
						
					var visitIdLength = visitId.length;
					for (var i=0;i<6-visitIdLength;i++) {
						visitId = "0"+visitId;
					}
					visitId = patientPrefix+visitId;
					return visitId; 
					}
				},
				{  
					"mData": function (o) { 	
					var patientId = o["patient_id"];
					var patientPrefix = o["patient_prefix"];
						
					var patientIdLength = patientId.length;
					for (var i=0;i<6-patientIdLength;i++) {
						patientId = "0"+patientId;
					}
					patientId = patientPrefix+patientId;
					return patientId; 
					}
				},
				/* { "mData": "visit_id" },
				{ "mData": "patient_id" }, */
				{ "mData": "test_code" },
				{ "mData": "name" },
				{ "mData": "cost" },
				{ "mData": "type" },
				{ "mData": "pay_status" },
				{  
					"mData": function (o) { 					
						return "<input type='button' class='btn btn-small btn-primary process' data-val="+o["id"]+" value='Process' />"; 
					}
				},	
			],
			aoColumnDefs: [
			   { 'bSortable': false, 'aTargets': [ 7 ] }
		   ]
	} );
   
	
	$("#btnSubmit").click(function(){
		var lab_test_id = $('#hdnLabTestRequestId').val();
		var specimen_description =  $('#txtSpecimenDescription').val();
		var postData = {
			"operation":"update",
			"lab_test_id":lab_test_id,
			"labRequestId":labRequestId,
			"specimen_description":specimen_description
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/view_lab_request.php",
			datatype:"json",
			data: postData,
			success: function(data) {
				if(data != "0" && data != ""){
					$('#pop_up_close').click()
					$('#messagemyModal #messageMyModalLabel').text("Success");
					$('#messagemyModal .modal-body').html('<span style ="color:#000 !important;"><b>Specimen Recorded Successfully!!!</b></span><br/><br/><label for="specimenId">Specimen Id : </label><span style ="color:#000 !important;" id="specimenId"><b>'+ data +'</b></span>');
					$("#labRequestTable").dataTable().fnReloadAjax();						
					$('#messagemyModal').modal();
					$('#txtSpecimenDescription').val("");
				}
			},
			error: function(){
				
			}
		});
	});
	
});

function bindSpecimen(){
	$.ajax({     
		type: "POST",
		cache: false,
		url: "controllers/admin/view_lab_request.php",
		data: {
		"operation":"loadSpecimen"
		},
		success: function(data) { 
		    var html = '<option value="-1">Please select</option>';
			if(data != null && data != ""){
				data = jQuery.parseJSON(data);
				$.each(data,function(index,value){
					html += '<option value="'+value['value']+'">'+value['text']+'</option>';
				});
				$('#ddlSpecimenType').append(html);
			}			
		},
		error:function(){
		}
	});	
}
function bindLabTests(){
	$.ajax({     
		type: "POST",
		cache: false,
		url: "controllers/admin/view_lab_request.php",
		data: {
		"operation":"loadLabTests"
		},
		success: function(data) { 
		    var html = '<option value="-1">Please select</option>';
			if(data != null && data != ""){
				data = jQuery.parseJSON(data);
				$.each(data,function(index,value){
					html += '<option value="'+value['value']+'">'+value['text']+'</option>';
				});
				$('#ddlLabTests').append(html);
			}			
		},
		error:function(){
		}
	});	
}

//function to calculate age
function getAge(dateString) {
    var now = new Date();
    var today = new Date(now.getYear(), now.getMonth(), now.getDate());

    var yearNow = now.getYear();
    var monthNow = now.getMonth();
    var dateNow = now.getDate();

    var dob = new Date(dateString.substring(0, 4), dateString.substring(5, 7) - 1, dateString.substring(8, 10));

    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();
    var age = {};
    var ageString = "";
    var yearString = "";
    var monthString = "";
    var dayString = "";


    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    age = {
        years: yearAge,
        months: monthAge,
        days: dateAge
    };

    if (age.years > 1) yearString = " years";
    else yearString = " year";
    if (age.months > 1) monthString = " months";
    else monthString = " month";
    if (age.days > 1) dayString = " days";
    else dayString = " day";


    if ((age.years > 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else ageString = "Oops! Could not calculate age!";

    return ageString;
}

function clear(){
	$.each($('label'),function(index,value) {		
		if($(this).text() == "")
		{
			$(this).text("Not Available");
		}
	});
}

//# sourceURL=filename.js