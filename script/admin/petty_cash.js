var pettyCashTable;
var pettyCashLineItemTable;
var pettyCashCustomerLineItemTable;
var validateAmount = 0;
var initTable;
$(document).ready(function() {
    loader();
    debugger;
    initTable = true;
    bindGLAccount("#selGLAccount",'');
    bindGLAccount("#selGLAccountCustomer",'');
    bindStaffType();
    bindLedger();
    bindCustomerType();

    $("#selLedger").change(function(){
        if ($(this).val() != '') {
            paymentMode($(this).val(),null);
            $("#selLedger").removeClass('errorStyle');
            $("#selLedgerError").text('');          
        }
        if ($(this).val() == '') {
            $("#selPaymentMode").html("<option value =''>--Select--</option>");
        }
    });

    $("#selLedgerCustomer").change(function(){
        if ($(this).val() != '') {
            paymentMode($(this).val(),null);
            $("#selLedgerCustomer").removeClass('errorStyle');
            $("#selLedgerCustomerError").text('');          
        }
        if ($(this).val() == '') {
            $("#selPaymentModeCustomer").html("<option value =''>--Select--</option>");
        }
    });

    /*Hide add ward by default functionality*/
    $("#advanced-wizard").hide();
    $("#advanced-wizard-2").hide();
    $("#pettyCashList").addClass('list');
    $("#tabpettyCashList").addClass('tab-list-add');

    $("#tabAddpettyCash").click(function() {
        showAddTab();  
        clear();
    $('#employeePopUp').addClass("activeSelf");
    $('#btnMain').hide();
    $('#btnSubmit').show();
    $('#btnReset').show();
    $('#btnUpdateCustomer').hide();
        
    });

    $("#tabpettyCashList").click(function() {
        clear();
        tabpettyCashList();
    });
    $("#selStaffType").change(function() {
        var staffCustomerId = $('#selStaffType').val();
        if (staffCustomerId != '') {
            bindStaffId(staffCustomerId, null);
        } 
    });


    removeErrorMessage();

    $('#employeePopUp').click(function() {
        $('#employeePopUp').addClass("activeSelf");
        $('#customerPopUp').removeClass("activeSelf");
        $('.customerPopUp').hide();
        $('.employeePopUp').show();
        $('#btnSubmitCustomer').show();
        $('#btnResetCustomer').show();
        $('#selStaffType').focus();       

    });

    $('#btnSubmit').click(function() {
        var flag = false;

        if ($('#txtAmount').val() == '') {
            $('#txtAmountError').text('Please enter amount');
            $('#txtAmount').focus();
            $('#txtAmount').addClass("errorStyle");
            flag = true;
        }

        if($("#txtReference").val() ==""){
            $("#txtReference").focus();
            $("#txtReferenceError").text("Please enter reference");
            $("#txtReference").addClass("errorStyle");
            flag = true;
        }
        
        if($("#selPaymentMode").val() ==""){
            $("#selPaymentMode").focus();
            $("#selPaymentModeError").text("Please select payment mode");
            $("#selPaymentMode").addClass("errorStyle");
            flag = true;
        }

        if ($('#selGLAccount').val() == '') {
            $('#selGLAccountError').text('Please select gl account');
            $('#selGLAccount').focus();
            $('#selGLAccount').addClass("errorStyle");
            flag = true;
        }
        if ($('#selLedger').val() == '') {
            $('#selLedgerError').text('Please select ledger');
            $('#selLedger').focus();
            $('#selLedger').addClass("errorStyle");
            flag = true;
        }
        if ($('#selStaffName').val() == '') {
            $('#selStaffNameError').text('Please select staff name');
            $('#selStaffName').focus();
            $('#selStaffName').addClass("errorStyle");
            flag = true;
        }
        if ($('#selStaffType').val() == '') {
            $('#selStaffTypeError').text('Please select staff type');
            $('#selStaffType').focus();
            $('#selStaffType').addClass("errorStyle");
            flag = true;
        }
        if (flag == true) {
            return false;
        }

        var staffCustomerId = $('#selStaffType').val().trim();
        var paymentMode = $("#selPaymentMode").val();
        var reference = $("#txtReference").val().trim();
        var staffCustomerNameId = $('#selStaffName').val().trim();
        var staffCustomerName = $('#selStaffName option:selected').text();
        var ledger = $('#selLedger').val().trim();
        var glAccount = $('#selGLAccount').val().trim();
        var amount = $('#txtAmount').val().trim();
        var description = $('#txtDescription').val().trim();
        var type = "S";

        var postData = {
            "staffCustomerId": staffCustomerId,
            "staffCustomerNameId": staffCustomerNameId,
            "staffCustomerName": staffCustomerName,
            "ledger": ledger,
            "reference":reference,
            "paymentMode" : paymentMode,
            "glAccount": glAccount,
            "amount": amount,
            'description': description,
            "type": type,
            "operation": "savePettyCash"
        }


        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/petty_cash.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    callSuccessPopUp('Success', 'Saved successfully!!!');
                    clearFormDetails('.clearForm');
                    $('#selStaffType').focus();
                    clear();
                    tabpettyCashList();
                }
            },
            error: function() {

            }
        });
    });
    $('#btnReset').click(function() {
        clear();
        $('#selCustomerId').focus();
    });



    $('#customerPopUp').click(function() {
        $('#employeePopUp').removeClass("activeSelf");
        $('#customerPopUp').addClass("activeSelf");
        $('.customerPopUp').removeAttr('style');
        $('.employeePopUp').hide();
        $('#selCustomerId').focus();
        
    });

    $("#selCustomerId").change(function() {
            var customerId = $('#selCustomerId').val();
            if (customerId != '') {
                bindCustomerId(customerId, null);
            } else {
                $("#selCustomerName").html("<option value=''>--Select--</option>");
            }
        });

    $('#btnSubmitCustomer').click(function() {
        var flag = false;

        if ($('#txtAmountCustomer').val() == '') {
            $('#txtAmountCustomerError').text('Please enter amount');
            $('#txtAmountCustomer').focus();
            $('#txtAmountCustomer').addClass("errorStyle");
            flag = true;
        }

        if($("#txtReferenceCustomer").val() ==""){
            $("#txtReferenceCustomer").focus();
            $("#txtReferenceCustomerError").text("Please enter reference");
            $("#txtReferenceCustomer").addClass("errorStyle");
            flag = true;
        }
        
        if($("#selPaymentModeCustomer").val() ==""){
            $("#selPaymentModeCustomer").focus();
            $("#selPaymentModeCustomerError").text("Please select payment mode");
            $("#selPaymentModeCustomer").addClass("errorStyle");
            flag = true;
        }
        if ($('#selGLAccountCustomer').val() == '') {
            $('#selGLAccountCustomerError').text('Please select gl account');
            $('#selGLAccountCustomer').focus();
            $('#selGLAccountCustomer').addClass("errorStyle");
            flag = true;
        }
        if ($('#selLedgerCustomer').val() == '') {
            $('#selLedgerCustomerError').text('Please select ledger');
            $('#selLedgerCustomer').focus();
            $('#selLedgerCustomer').addClass("errorStyle");
            flag = true;
        }
        if ($('#selCustomerName').val() == '') {
            $('#selCustomerNameError').text('Please select customer name');
            $('#selCustomerName').focus();
            $('#selCustomerName').addClass("errorStyle");
            flag = true;
        }
        if ($('#selCustomerId').val() == '') {
            $('#selCustomerIdError').text('Please select customer type');
            $('#selCustomerId').focus();
            $('#selCustomerId').addClass("errorStyle");
            flag = true;
        }
        if (flag == true) {
            return false;
        }

        var staffCustomerId = $('#selCustomerId').val().trim();
        var staffCustomerNameId = $('#selCustomerName').val().trim();
        var paymentMode = $("#selPaymentModeCustomer").val();
        var reference = $("#txtReferenceCustomer").val().trim();
        var staffCustomerName = $("#selCustomerName option:selected").text();
        var ledger = $('#selLedgerCustomer').val().trim();
        var glAccount = $('#selGLAccountCustomer').val().trim();
        var amount = $('#txtAmountCustomer').val().trim();
        var description = $('#txtDescriptionCustomer').val().trim();
        var type = "C";
        var postData = {
            "staffCustomerId": staffCustomerId,
            "staffCustomerNameId": staffCustomerNameId,
            "staffCustomerName": staffCustomerName,
            "ledger": ledger,
            "reference":reference,
            "paymentMode" : paymentMode,
            "glAccount": glAccount,
            "amount": amount,
            "description": description,
            "type": type,
            "operation": "savePettyCash"
        }


        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/petty_cash.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    callSuccessPopUp('Success', 'Saved successfully!!!');
                    clearFormDetails('.customerPopUp');
                    $('#selCustomerId').focus();
                    clear();
                    tabpettyCashList();
                }
            },
            error: function() {

            }
        });
    });
    $('#btnResetCustomer').click(function() {
        clear();
        $('#selCustomerId').focus();
    });

    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code 
        pettyCashTable = $('#pettyCashTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "autoWidth": false,
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = pettyCashTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var staffCustomerId = mData["staff_customer_id"];
                        var staffCustomerNameId = mData["staff_customer_name_id"];
                        var staffCustomerName = mData["staff_customer_name"];
                        var name = mData["staff_customer_id"];
                        var ledger = mData["ledger_id"];
                        var ledgerName = mData["ledger_name"];
                        var glAccountId = mData["gl_account_id"];
                        var glAccountName = mData["account_name"];
                        var amount = mData["amount"];
                        var payment_mode_id = mData["payment_mode_id"];
                        var reference = mData["reference"];
                        var description = mData["description"];
                        var type = mData["type"];
                        editClick(id,payment_mode_id, reference, staffCustomerId, staffCustomerNameId, staffCustomerName, ledger, glAccountId , glAccountName ,amount, description, type);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = pettyCashTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/petty_cash.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [{
                "mData": "staff_customer_name"
            }, {
                "mData": "ledger_name"
            }, {
                "mData": "account_name"
            }, {
                "mData": "amount"
            }, {
                "mData": "description"
            }, {
                "mData": function(o) {
                    var data = o;
                    return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                        " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                        " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                        " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                        " data-original-title='Delete'></i>";
                }
            }, ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [3, 4, 5]
            }]

        });
    }

    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            pettyCashTable.fnClearTable();
            pettyCashTable.fnDestroy();
            pettyCashTable = "";
            pettyCashTable = $('#pettyCashTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "autoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = pettyCashTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var staffCustomerId = $('#selCustomerId').val().trim();
                            var staffCustomerNameId = $('#selCustomerName').val().trim();
                            restoreClick(id, staffCustomerId, staffCustomerNameId);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/petty_cash.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [{
                    "mData": "staff_customer_name"
                }, {
                    "mData": "ledger_name"
                }, {
                    "mData": "account_name"
                }, {
                    "mData": "amount"
                }, {
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                    }
                }, ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [3, 4, 5]
                }]
            });
        } else { // show active data on unchecked   
            pettyCashTable.fnClearTable();
            pettyCashTable.fnDestroy();
            pettyCashTable = "";
            pettyCashTable = $('#pettyCashTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "autoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = pettyCashTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var staffCustomerId = mData["staff_customer_id"];
                            var staffCustomerNameId = mData["staff_customer_name_id"];
                            var staffCustomerName = mData["staff_customer_name"];
                            var name = mData["staff_customer_id"];
                            var ledger = mData["ledger_id"];
                            var ledgerName = mData["ledger_name"];
                            var glAccountId = mData["gl_account_id"];
                            var glAccountName = mData["account_name"];
                            var payment_mode_id = mData["payment_mode_id"];
                            var reference = mData["reference"];
                            var amount = mData["amount"];
                            var description = mData["description"];
                            var type = mData["type"];
                            editClick(id,payment_mode_id, reference,staffCustomerId, staffCustomerNameId, staffCustomerName, ledger, glAccountId , glAccountName ,amount, description, type);

                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = pettyCashTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/petty_cash.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [{
                    "mData": "staff_customer_name"
                }, {
                    "mData": "ledger_name"
                }, {
                    "mData": "account_name"
                }, {
                    "mData": "amount"
                }, {
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                }, ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [3, 4, 5]
                }]
            });
        }
    });

});

function editClick(id,payment_mode_id, reference, staffCustomerId, staffCustomerNameId, staffCustomerName, ledger, glAccountId , glAccountName ,amount, description, type) {
    debugger;
    mainPkId = id;
    showAddTab();
    $("#btnReset").hide();
    $("#btnSubmit").hide();
    $("#btnUpdateCustomer").hide();
    $('#tabAddpettyCash').html("+Update Petty Cash");

    removeErrorMessage();

    // pop for employee's
    if (type == "S") {
        $("#lineItemsPopUp").removeClass('activeSelf');
        $(".employeePopUp").show();
        $('#employeePopUp').show();
        $('#customerPopUp').hide();
        $('.pettyCashLineItem').hide();
        $("#btnUpdate").show();
        $('#employeePopUp').addClass('activeSelf');
        $("#mainUpdateEmployee").removeClass('hide');
        $('#btnMain').removeAttr('style');
        $('#btnSubmitCustomer').hide();
        $('#btnResetCustomer').hide();
        $('#mainPopUp').addClass('activeSelf');
        $("#btnUpdateEmployee").show();


        $("#selStaffType").change(function() {
            var staffCustomerId = $('#selStaffType').val();
            if (staffCustomerId != '') {
                bindStaffId(staffCustomerId, staffCustomerNameId);
            } else {
                $("#selStaffName").html("<option value=''>--Select--</option>");
            }
        });

        $('#selStaffType').val(staffCustomerId);

        bindStaffId(staffCustomerId,staffCustomerNameId);

        

        $('#selStaffName').val(staffCustomerNameId);
        $('#selLedger').val(ledger);
        $('#selGLAccount').val(glAccountId);
        $('#txtAmount').val(amount);
        paymentMode(ledger,payment_mode_id);
        $('#txtReference').val(reference);
        $('#txtDescription').val(description);
        $('#selectedRow').val(id);

        $("#lineItemsPopUp").unbind();
        $("#lineItemsPopUp").on('click', function() {
            $("#mainUpdateEmployee").addClass('hide');
            $('#updateLineItemEmployee').removeClass('hide');
            $('#updateLineItemEmployee').addClass('show');
            $('.pettyCashLineItem').hide();
            $('.pettyCashLineItem').addClass('hide');
            $(this).addClass('activeSelf');
            $(this).parent().siblings().children().removeClass('activeSelf');
            if(initTable != true){
                pettyCashLineItemTable.fnClearTable();
                pettyCashLineItemTable.fnDestroy();  
            }
            else{
                initTable = false;
            }

            pettyCashLineItemTable = $("#pettyCashLineItemTable").dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "bAutoWidth":false,
                "fnDrawCallback": function(oSettings) {
                },
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [2, 3]
                }]
            });
            bindLineItems("employee");
        });
        $("#mainPopUp").unbind();
        $("#mainPopUp").on('click', function() {
            $('#updateLineItemEmployee').hide();
            $('#updateLineItemEmployee').addClass('hide');
            $('#updateLineItemEmployee').removeClass('show');
            $('#btnUpdate').show();
            $(this).addClass('activeSelf');
            $(this).parent().siblings().children().removeClass('activeSelf');
            $("#mainUpdateEmployee").removeClass('hide');
        });
        $("#btnAddEmployeeDetail").unbind();
        $("#btnAddEmployeeDetail").on('click', function() {
            var currentData = pettyCashLineItemTable.fnAddData(['<select id ="selChartOfAccount"><option value="">--Select--</option></select>',
                '<input type="number"  id="txtLineAmount" placeholder = "Enter amount">',
                '<input type="text"  id="txtLineremarks" placeholder = "Enter remarks">',
                '<input type="button" id ="newButtonColor" class="btnEnabled" onclick="rowDelete($(this));" value="Remove">'
            ]);
            var getCurrDataRow = pettyCashLineItemTable.fnSettings().aoData[currentData].nTr;
            bindGLAccount($(getCurrDataRow).find('td:eq(0)').children()[0], '');
        });
        $("#btnUpdate").unbind();
        $("#btnUpdate").click(function() { // click update button
            var flag = false;
            if ($('#txtAmount').val() == '') {
                $('#txtAmountError').text('Please enter amount');
                $('#txtAmount').focus();
                $('#txtAmount').addClass("errorStyle");
                flag = true;
            }

            if($("#txtReference").val() ==""){
                $("#txtReference").focus();
                $("#txtReferenceError").text("Please enter reference");
                $("#txtReference").addClass("errorStyle");
                flag = true;
            }
            
            if($("#selPaymentMode").val() ==""){
                $("#selPaymentMode").focus();
                $("#selPaymentModeError").text("Please select payment mode");
                $("#selPaymentMode").addClass("errorStyle");
                flag = true;
            }
            if ($('#selLedger').val() == '') {
                $('#selLedgerError').text('Please select ledger');
                $('#selLedger').focus();
                $('#selLedger').addClass("errorStyle");
                flag = true;
            }
            if ($('#selStaffName').val() == '') {
                $('#selStaffNameError').text('Please select staff name');
                $('#selStaffName').focus();
                $('#selStaffName').addClass("errorStyle");
                flag = true;
            }
            if ($('#selStaffType').val() == '') {
                $('#selStaffTypeError').text('Please select staff type');
                $('#selStaffType').focus();
                $('#selStaffType').addClass("errorStyle");
                flag = true;
            }

            if (flag == true) {
                return false;
            }

            if ($("#mainPopUp").hasClass("activeSelf") == true) {
                var staffCustomerId = $('#selStaffType').val().trim();
                var staffCustomerNameId = $('#selStaffName').val().trim();
                var staffCustomerName = $('#selStaffName option:selected').text();
                var paymentMode = $("#selPaymentMode").val();
                var reference = $("#txtReference").val().trim();
                var ledger = $('#selLedger').val().trim();
                var amount = $('#txtAmount').val().trim();
                var gLAccount = $("#selGLAccount").val().trim();
                var type = "S";
                var description = $('#txtDescription').val().trim();
                description = description.replace(/'/g, "&#39");
                var id = $('#selectedRow').val();

                $('#confirmUpdateModalLabel').text();
                $('#updateBody').text("Are you sure that you want to update this?");
                $('#confirmUpdateModal').modal();
                $("#btnConfirm").unbind();
                $("#btnConfirm").click(function() {
                    var postData = {
                        "operation": "update",
                        "staffCustomerId": staffCustomerId,
                        "staffCustomerNameId": staffCustomerNameId,
                        "staffCustomerName": staffCustomerName,
                        "ledger": ledger,
                        "reference":reference,
                        "paymentMode" : paymentMode,
                        "amount": amount,
                        "type": type,
                        "description": description,
                        "id": id,
                        "gLAccount" : gLAccount
                    }
                    $.ajax({ //ajax call for update data

                        type: "POST",
                        cache: false,
                        url: "controllers/admin/petty_cash.php",
                        datatype: "json",
                        data: postData,

                        success: function(data) {
                            if (data != "0" && data != "") {
                                $('#myModal').modal('hide');
                                $('.close-confirm').click();
                                $('.modal-body').text("");
                                $('#messageMyModalLabel').text("Success");
                                $('.modal-body').text("Petty cash updated successfully!!!");
                                $('#messagemyModal').modal();
                                pettyCashTable.fnReloadAjax();
                                tabpettyCashList();
                                clear();
                            }
                        },
                        error: function() {
                            $('.close-confirm').click();
                            $('.modal-body').text("");
                            $('#messageMyModalLabel').text("Error");
                            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                            $('#messagemyModal').modal();
                        }
                    }); // end of ajax
                });
            }

            else{
                debugger;
                var updateLineItems = [];
                var flag = false;
                var rows = $("#pettyCashLineItemTable tbody tr");
                var arr= [];
                jQuery.grep(pettyCashLineItemTable.fnGetData(), function( n, i ){
                    if(n[3] != ""){
                        arr.push(n);
                    }
                });
                if (arr.length == 0) {
                    callSuccessPopUp("Alert","Please insert new data in table");
                    flag = true;
                    return false;
                }
                var calTotalAmount = 0;
                $.each(rows,function(i,v){
                    //fetch only fresh rows
                    if ($(v).find('input[type="button"]').length > 0) {
                        var obj = { };
                        obj.glLedger = $(v).find('td:eq(0)').children().val();
                        obj.amount = $(v).find('td:eq(1)').children().val();
                        obj.remarks = $(v).find('td:eq(2)').children().val();

                        if(obj.glLedger == ''){
                            callSuccessPopUp("Alert","Please enter line item GL account");
                            flag = true;
                            return false;
                        }
                        
                        if(obj.amount == ''){
                            callSuccessPopUp("Alert","Please enter line item amount");
                            flag = true;
                            return false;
                        }                
                        
                        
                        calTotalAmount+=parseFloat(obj.amount);
                        updateLineItems.push(obj);
                    }
                });
                
                
                if(parseFloat($("#txtAmount").val()) < calTotalAmount+validateAmount){
                    callSuccessPopUp('Alert',"You have crossed the limit of sum amount");
                    flag = true;
                    return false;
                }
                if(flag == true){
                    return false;
                }
                
                var id = $("#selectedRow").val();
                var addData = {
                    "operation" : "saveEmployeeLineItem",
                    "id" : id,
                    "updateLineItems" : JSON.stringify(updateLineItems)
                }
                dataCall("controllers/admin/petty_cash.php", addData, function (result){
                    if (result == 1) {
                        callSuccessPopUp("Sucess","Data saved successfully.");
                        tabpettyCashList();
                    }
                });
            }                
        });
    } else { //pop for customer
        debugger;
        $(".customerPopUp").show();
        $('#employeePopUp').hide();
        $('#customerPopUp').addClass('activeSelf');
        $('.employeePopUp').hide();
        $('#main').addClass('activeSelf');
        $("#lineItems").removeClass('activeSelf');
        $('#btnMain').show();
        $('.pettyCashButtons').show();
        $('#btnUpdateEmployee').show();
        $('#btnLineItem').hide();

        $('#btnSubmit').hide();
        $('#btnReset').hide();
        $('#btnUpdateCustomer').removeAttr('style');
        $('#btnSubmitCustomer').hide();
        $('#btnResetCustomer').hide();
        $('#mainPopUp').addClass('activeSelf');
        $('#btnCustomer').removeAttr('style');
        $("#btnUpdate").removeAttr('style');

        $("#selCustomerId").change(function() {
            var staffCustomerId = $('#selCustomerId').val();
            if (staffCustomerId != '') {
                bindCustomerId(staffCustomerId, null);
            } else {
                $("#selCustomerId").html("<option value=''>--Select--</option>");
            }
        });

        $('#selCustomerId').val(staffCustomerId);
        bindCustomerId(staffCustomerId, staffCustomerNameId);
        $("#lineItems").unbind();
        $("#lineItems").click(function() {
            $("#lineItems").addClass('activeSelf');
            $("#mainPopUp").removeClass('activeSelf');
            $(this).addClass('activeSelf');
            $(this).parent().siblings().children().removeClass('activeSelf');
            $(".updateLineItemCustomer").removeClass('hide');
            $(".updateCustomerDetail").addClass('hide');

            
            if(initTable != true){
                pettyCashCustomerLineItemTable.fnClearTable();
                pettyCashCustomerLineItemTable.fnDestroy();  
            }
            else{
                initTable = false;
            }
            
            pettyCashCustomerLineItemTable = $("#pettyCashCustomerLineItemTable").dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "bAutoWidth":false,
                "fnDrawCallback": function(oSettings) {
                },
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [2, 3]
                }]
            });

            bindLineItems("customer");
        });

        $("#main").click(function() {
            $(this).addClass('activeSelf');
            $(this).parent().siblings().children().removeClass('activeSelf');
            $(".updateLineItemCustomer").addClass('hide');
            $(".updateCustomerDetail").removeClass('hide');
        });
        $("#btnAddCustomerDetail").unbind();
        $("#btnAddCustomerDetail").on('click', function() {
            var currentData = pettyCashCustomerLineItemTable.fnAddData(['<select id ="selChartOfAccount"><option value="">--Select--</option></select>',
                '<input type="number"  id="txtLineAmount" placeholder = "Enter amount">',
                '<input type="text"  id="txtLineremarks" placeholder = "Enter remarks">',
                '<input type="button" id ="newButtonColor" class="btnEnabled" onclick="rowDelete($(this));" value="Remove">'
            ]);
            var getCurrDataRow = pettyCashCustomerLineItemTable.fnSettings().aoData[currentData].nTr;
            bindGLAccount($(getCurrDataRow).find('td:eq(0)').children()[0], '');
        });

        $('#selCustomerName').val(staffCustomerNameId);
        $('#selLedgerCustomer').val(ledger);
        $('#selGLAccountCustomer').val(glAccountId);
        paymentMode(ledger,payment_mode_id);
        $('#txtReferenceCustomer').val(reference);
        $('#txtAmountCustomer').val(amount);
        $('#txtDescriptionCustomer').val(description);
        $('#selectedRow').val(id);

        $("#selCustomerId").change(function() {
            var customerId = $('#selCustomerId').val();
            if (customerId != '') {
                bindCustomerId(customerId, null);
            } else {
                $("#selCustomerId").html("<option value=''>--Select--</option>");
            }
        });

        $("#btnUpdateCustomerAdd").unbind();
        $("#btnUpdateCustomerAdd").click(function() { // click update button

            if ($("#main").hasClass("activeSelf") == true) {
                var flag = false;

                if ($('#txtAmountCustomer').val() == '') {
                    $('#txtAmountCustomerError').text('Please enter amount');
                    $('#txtAmountCustomer').focus();
                    $('#txtAmountCustomer').addClass("errorStyle");
                    flag = true;
                }

                if($("#txtReferenceCustomer").val() ==""){
                    $("#txtReferenceCustomer").focus();
                    $("#txtReferenceCustomerError").text("Please enter reference");
                    $("#txtReferenceCustomer").addClass("errorStyle");
                    flag = true;
                }
                
                if($("#selPaymentModeCustomer").val() ==""){
                    $("#selPaymentModeCustomer").focus();
                    $("#selPaymentModeCustomerError").text("Please select payment mode");
                    $("#selPaymentModeCustomer").addClass("errorStyle");
                    flag = true;
                }
                if ($('#selLedgerCustomer').val() == '') {
                    $('#selLedgerCustomerError').text('Please select ledger');
                    $('#selLedgerCustomer').focus();
                    $('#selLedgerCustomer').addClass("errorStyle");
                    flag = true;
                }
                if ($('#selCustomerName').val() == '') {
                    $('#selCustomerNameError').text('Please select customer name');
                    $('#selCustomerName').focus();
                    $('#selCustomerName').addClass("errorStyle");
                    flag = true;
                }
                if ($('#selCustomerId').val() == '') {
                    $('#selCustomerIdError').text('Please select customer type');
                    $('#selCustomerId').focus();
                    $('#selCustomerId').addClass("errorStyle");
                    flag = true;
                }

                if (flag == true) {
                    return false;
                }

                var staffCustomerId = $('#selCustomerId').val();
                var staffCustomerNameId = $('#selCustomerName').val();
                var staffCustomerName = $('#selCustomerName option:selected').text();
                var ledger = $('#selLedgerCustomer').val();
                var paymentMode = $("#selPaymentModeCustomer").val();
                var reference = $("#txtReferenceCustomer").val().trim();
                var amount = $('#txtAmountCustomer').val().trim();
                var type = "C";
                var gLAccount = $("#selGLAccount").val().trim();
                var description = $('#txtDescriptionCustomer').val().trim();
                description = description.replace(/'/g, "&#39");
                var id = $('#selectedRow').val();

                $('#confirmUpdateModalLabel').text();
                $('#updateBody').text("Are you sure that you want to update this?");
                $('#confirmUpdateModal').modal();
                $("#btnConfirm").unbind();
                $("#btnConfirm").click(function() {
                    var postData = {
                        "operation": "update",
                        "staffCustomerId": staffCustomerId,
                        "staffCustomerNameId": staffCustomerNameId,
                        "staffCustomerName": staffCustomerName,
                        "ledger": ledger,
                        "reference":reference,
                        "paymentMode" : paymentMode,
                        "amount": amount,
                        "type": type,
                        "description": description,
                        "id": id,
                        'gLAccount' : gLAccount
                    }
                    $.ajax({ //ajax call for update data
                        
                        type: "POST",
                        cache: false,
                        url: "controllers/admin/petty_cash.php",
                        datatype: "json",
                        data: postData,

                        success: function(data) {
                            if (data != "0" && data != "") {
                                $('#myModal').modal('hide');
                                $('.close-confirm').click();
                                $('.modal-body').text("");
                                $('#messageMyModalLabel').text("Success");
                                $('.modal-body').text("Petty cash updated successfully!!!");
                                $('#messagemyModal').modal();
                                pettyCashTable.fnReloadAjax();
                                tabpettyCashList();
                                clear();
                            }
                        },
                        error: function() {
                            $('.close-confirm').click();
                            $('.modal-body').text("");
                            $('#messageMyModalLabel').text("Error");
                            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                            $('#messagemyModal').modal();
                        }
                    }); // end of ajax
                });
            }

            else{

                var updateLineItems = [];
                var flag = false;
                var rows = $("#pettyCashCustomerLineItemTable tbody tr");
                
                var arr= [];
                jQuery.grep(pettyCashCustomerLineItemTable.fnGetData(), function( n, i ){
                    if(n[3] != ""){
                        arr.push(n);
                    }
                });
                if (arr.length == 0) {
                    callSuccessPopUp("Alert","Please insert new data in table");
                    flag = true;
                    return false;
                }
                var customertGlLedger;
                var customerRemarks;
                var customerAmount;
                var calTotalAmount = 0;
                $.each(rows,function(i,v){
                    //fetch only fresh rows
                    if ($(v).find('input[type="button"]').length > 0) {
                        var smallArray = [];
                        customertGlLedger = $(v).find('td:eq(0)').children().val();
                        customerAmount = $(v).find('td:eq(1)').children().val();
                        customerRemarks = $(v).find('td:eq(2)').children().val();        

                        if(customertGlLedger == ''){
                            callSuccessPopUp("Alert","Please enter line item GL account");
                            flag = true;
                            return false;
                        }            
                        
                        if(customerAmount == ''){
                            callSuccessPopUp("Alert","Please enter line item amount");
                            flag = true;
                            return false;
                        }
                        calTotalAmount+=parseFloat(customerAmount);
                        smallArray.push(customertGlLedger,customerAmount,customerRemarks);
                        updateLineItems.push(smallArray);
                    }                        
                });

                if(parseFloat($("#txtAmountCustomer").val()) < calTotalAmount+validateAmount){
                    callSuccessPopUp('Alert',"You have crossed the limit of sum amount");
                    flag = true;
                    return false;
                }
                if(flag == true){
                    return false;
                }

                var id = $("#selectedRow").val();
                var addData = {
                    "operation" : "saveCustomerLineItem",
                    "id" : id,
                    "updateLineItems" : JSON.stringify(updateLineItems)
                }
                dataCall("controllers/admin/petty_cash.php", addData, function (result){
                    if (result == 1) {
                        callSuccessPopUp("Sucess","Data saved successfully.");                        
                        pettyCashTable.fnReloadAjax();
                        tabpettyCashList();
                    }
                });
            }
        });

    }

   /* $('#lineItemsPopUp').click(function() {
        $('.modal-body').html($(".pettyCashLineItem").html()).show();
        $('.pettyCashMain').hide();
        $('.pettyCashLineItem').removeAttr('style');
        $('#lineItemsPopUp').addClass("activeSelf");
    });*/



} // end update button

function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Petty Cash");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deletePettyCash("' + type + '","' + id + '","","");');
} // end click fucntion

function restoreClick(id, staffCustomerId, staffCustomerNameId) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Petty Cash");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deletePettyCash("' + type + '","' + id + '","' + staffCustomerId + '","' + staffCustomerNameId + '");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});

function deletePettyCash(type, id, staffCustomerId, staffCustomerNameId) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/petty_cash.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Petty cash deleted successfully!!!");
                    $('#messagemyModal').modal();
                    pettyCashTable.fnReloadAjax();
                }
            },
            error: function() {

                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/petty_cash.php",
            data: {
                "operation": "restore",
                "staffCustomerId": staffCustomerId,
                "staffCustomerNameId": staffCustomerNameId,
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Petty cash restored successfully!!!");
                    $('#messagemyModal').modal();
                    pettyCashTable.fnReloadAjax();
                }
            },
            error: function() {
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary unavailable to respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}


function tabpettyCashList() {
    $("#advanced-wizard").hide();
    $("#advanced-wizard-2").hide();
    $('#advanced-wizard-3').hide();
    $(".blackborder").show();
    $("#tabAddpettyCash").removeClass('tab-detail-add');
    $("#tabAddpettyCash").addClass('tab-detail-remove');
    $("#tabpettyCashList").removeClass('tab-list-remove');    
    $("#tabpettyCashList").addClass('tab-list-add');
    $("#pettyCashList").addClass('list');
    $('#inactive-checkbox-tick').prop('checked', false).change();

    $("#btnReset").show();
    $("#btnSubmit").show();
    $("#employeePopUp").show();
    $("#employeePopUp").addClass('activeSelf');
    $("#customerPopUp").show();
    $("#customerPopUp").removeClass('activeSelf');
    $("#btnUpdate").hide();
    $('#btnCustomer').hide();
    $("#btnUpdateCustomer").hide();
    $('#tabAddpettyCash').html("+Add Petty Cash");
    $('#updateLineItemEmployee').removeClass('show');
    $('#updateLineItemEmployee').addClass('hide');
    clear();
}


function showAddTab(){
    $("#advanced-wizard").show();
    $("#advanced-wizard-2").show();
    $(".blackborder").hide();
    
    $("#tabAddpettyCash").addClass('tab-detail-add');
    $("#tabAddpettyCash").removeClass('tab-detail-remove');
    $("#tabpettyCashList").removeClass('tab-list-add');
    $("#tabpettyCashList").addClass('tab-list-remove');
    $("#pettyCashList").addClass('list');
}

function bindStaffType() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/staffregistration.php",
        data: {
            "operation": "showstafftype"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
                }
                $('#selStaffType').html(option);
            }
        },
        error: function() {}
    });
}

function bindStaffId(staffCustomerId, staffCustomerNameId) {
    var staffType = staffCustomerId;
    var postData = {
        staffType: staffType,
        "operation": "showstaffid"
    };

    dataCall("controllers/admin/petty_cash.php", postData, function(result) {
        if (result.length > 2) {
            var parseData = jQuery.parseJSON(result);
            var option = "<option value=''>--Select--</option>";
            $.each(parseData, function(i, v) {
                option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
            });
            $('#selStaffName').html(option);

            if (staffCustomerNameId != "") {
                $('#selStaffName').val(staffCustomerNameId);
            }
        }
    });
}

function bindLedger() { //  function for loading ledger
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/petty_cash.php",
        data: {
            "operation": "showLedger"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option data-value='" + parseData[i].value + "' value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selLedger').html(option);
                $('#selLedgerCustomer').html(option);
            }
        },
        error: function() {}
    });
}

function paymentMode(ledgerId,payment_mode_id){
    var postData = {
        operation : "showPaymentMode",
        ledgerId : ledgerId
    }
    dataCall("controllers/admin/payment_voucher.php", postData, function (result){
        if (result !='') {
            var parseData = JSON.parse(result);
            var option = "<option value=''>--Select--</option>";
            $.each(parseData,function(i,v){
                option += "<option value='" + v.id + "'>" +v.name+"</option>";
            });
            $("#selPaymentMode").html(option);
            $("#selPaymentModeCustomer").html(option);
            if(payment_mode_id != null){
                $("#selPaymentMode").val(payment_mode_id);
                $("#selPaymentModeCustomer").val(payment_mode_id);
            }
        }
    });
}

function bindCustomerType() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/petty_cash.php",
        data: {
            "operation": "showCustomerType"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
                }
                $('#selCustomerId').html(option);
            }
        },
        error: function() {}
    });
}

function bindCustomerId(staffCustomerId, staffCustomerNameId) {
    var customerType = staffCustomerId;
    var postData = {
        customerType: customerType,
        "operation": "showCustomerId"
    };

    dataCall("controllers/admin/petty_cash.php", postData, function(result) {
        if (result.length > 2) {
            var parseData = jQuery.parseJSON(result);
            var option = "<option value=''>--Select--</option>";
            $.each(parseData, function(i, v) {
                option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
            });
            $('#selCustomerName').html(option);

            if (staffCustomerNameId != "") {
                $('#selCustomerName').val(staffCustomerNameId);
            }
        } else {
            $('#selCustomerName').text("<option value=''>--Select--</option>");
        }

    });
}

function clear() {
    removeErrorMessage();
    clearFormDetails('.clearForm');
    $('#selStaffType').focus();
}

/*function getCustomerSellerAmt(){
    var id = $("#selectedRow").val();
    var postData = {
        id :id,
        operation : "getCustomerSellerAmount"
    }
    dataCall("controllers/admin/petty_cash.php", postData, function(result) {
        if (result !='') {
            var parseData = JSON.parse(result);
            validateAmount = parseFloat(parseData[0]['SUM(amount)']);
        }
    });
}*/

function bindLineItems(type){
    validateAmount = 0;
    if (type == "customer") {
        var dataTable = pettyCashCustomerLineItemTable;
    }
    else{
        var dataTable = pettyCashLineItemTable;
    }
    var id = $("#selectedRow").val();
    var postData = {
        id :id,
        operation : "getLineItems"
    }
    dataCall("controllers/admin/petty_cash.php", postData, function(result) {
        if (result !='') {
            var parseData = JSON.parse(result);
            $.each(parseData,function(i,v){
                dataTable.fnAddData([v.account_name,v.amount,v.remarks,'']);
                validateAmount+=parseFloat(v.amount);
            });
        }
    });
}
function rowDelete(currInst){
    var row = currInst.closest("tr").get(0);
    if(currInst.closest("table").attr('id') == "pettyCashLineItemTable"){
        pettyCashLineItemTable.fnDeleteRow(row);
    }
    else{
        pettyCashCustomerLineItemTable.fnDeleteRow(row);
    }
}