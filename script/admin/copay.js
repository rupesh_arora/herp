/*
 * File Name    :   copay.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre operation
 */
var insurancePlanTbl; //defile variable for insurancePlanTbl 
$(document).ready(function() {
    debugger;
	loader();
    $('#btnReset').click(function() {
		clear();
    });

    $("#formInsurancePlan").hide();
    $("#divInsurancePlan").addClass('list');    
    $("#tabInsurancePlanList").addClass('tab-list-add');
   
    $("#tabInsurancePlan").click(function() { // show the add allergies categories tab
        showAddTab();
        clear();
    });

    $("#tabInsurancePlanList").click(function() { // show allergies list tab
		showTableList();
    });

    // save details with validation
    $("#btnAddInsurancePlan").click(function() {

        var flag = "false";

        if ($("#selValue").val().trim() == '') {
            $("#selValueError").text("Please select value");
            $("#selValue").focus();
            $("#selValue").addClass("errorStyle");
            flag = "true";
        }

        if ($("#txtType").val().trim() == '') {
            $("#txtTypeError").text("Please enter copay type");
            $("#txtType").focus();
            $("#txtType").addClass("errorStyle");
            flag = "true";
        }

		if(flag == "true") {
			return false;
		}		
		
           
        var copayType = $("#txtType").val().trim();
        var value = $("#selValue").val().trim();
        var description = $("#txtDesc").val();
        description = description.replace(/'/g, "&#39");
		//ajax call for insert data into data base
		var postData = {
			"operation": "save",
			"copayType": copayType,
			"value": value,
			"description": description
		}
		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/copay.php",
			datatype: "json",
			data: postData,

			success: function(data) {
				if (data == "1") {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Copay saved successfully!!!");
					$('#messagemyModal').modal();
					showTableList();
				}
				if (data == "0") {
					$("#txtType").addClass('errorStyle');
					$("#txtType").focus();
					$('#txtTypeError').text('Copay type already exists.');
				}

			},
			error: function() {
				alert('error');
			}
		}); //  end ajax call
	}); //end button click function

    //remove validation style
    $("#selValue").change(function() {
        if ($("#selValue").val() != '') {
            $("#selValueError").text("");
            $("#selValue").removeClass("errorStyle");
        }
    });

     $("#txtType").keyup(function() {
        if ($("#txtType").val() != '') {
            $("#txtTypeError").text("");
            $("#txtType").removeClass("errorStyle");
        }
    });
	
    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code
        insurancePlanTbl = $('#tblCopay').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "fnDrawCallback": function(oSettings) {
                // perform update event
				$('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = insurancePlanTbl.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var copay_type = mData["copay_type"];
                        var value = mData["value"];
                        var description = mData["description"];
                        editClick(id,copay_type,value,description);

                    }
                });
                //perform delete event
				$('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = insurancePlanTbl.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/copay.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "copay_type"
                }, {
                    "mData": "value"
                }, {
                    "mData": "description"
                },{
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [2,3] 
            }]

        });
    }
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            insurancePlanTbl.fnClearTable();
            insurancePlanTbl.fnDestroy();
            insurancePlanTbl = "";
            insurancePlanTbl = $('#tblCopay').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
					$('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = insurancePlanTbl.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var copay_type = mData["copay_type"];
                            var value = mData["value"];
                            restoreClick(id,copay_type,value);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/copay.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                {
                    "mData": "copay_type"
                }, {
                    "mData": "value"
                }, {
                    "mData": "description"
                }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [2,3]
                }]
            });
        } else { // show active data on unchecked	
            insurancePlanTbl.fnClearTable();
            insurancePlanTbl.fnDestroy();
            insurancePlanTbl = "";
            insurancePlanTbl = $('#tblCopay').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform update event
					$('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = insurancePlanTbl.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
							var copay_type = mData["copay_type"];
                            var value = mData["value"];
							var description = mData["description"];
							editClick(id,copay_type,value,description);

                        }
                    });
                    // perform delete event
					$('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = insurancePlanTbl.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/copay.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
						"mData": "copay_type"
					}, {
                        "mData": "value"
                    }, {
						"mData": "description"
					}, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [2,3]
                }]
            });
        }
    });

});
// edit data dunction for update 
function editClick(id,copay_type,value,description) {
    showAddTab();
    $('#tabInsurancePlan').html("+Update Copay");
    $("#btnReset").hide();
    $("#btnAddInsurancePlan").hide();    
    $("#btnUpdate").removeAttr("style");   
	$("#txtType").removeClass("errorStyle");
    $("#txtTypeError").text("");
    $("#selValue").removeClass("errorStyle");
    $("#selValueError").text("");
	
   
    $('#txtType').val(copay_type);
    $('#selValue').val(value);
    $('#txtDesc').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
    //validation   
	
    $("#btnUpdate").click(function() { // click update button
		var flag = "false";

        if ($("#selValue").val().trim() == '') {
            $("#selValueError").text("Please select value");
            $("#selValue").focus();
            $("#selValue").addClass("errorStyle");
            flag = "true";
        }

        if ($("#txtType").val().trim() == '') {
            $("#txtTypeError").text("Please enter copay type");
            $("#txtType").focus();
            $("#txtType").addClass("errorStyle");
            flag = "true";
        }

		if(flag == "true") {
			return false;
		}

		var copayType = $("#txtType").val().trim();
		var value = $("#selValue").val().trim();
		var description = $("#txtDesc").val();
		description = description.replace(/'/g, "&#39");
		var id = $('#selectedRow').val();
		
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to update this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
		var postData = {
			"operation": "update",
			"copayType": copayType,
			"value": value,
			"description": description,
			"id": id
		}
		$.ajax( //ajax call for update data
			{
				type: "POST",
				cache: false,
				url: "controllers/admin/copay.php",
				datatype: "json",
				data: postData,

				success: function(data) {
					if (data == "1") {
						$('#myModal').modal('hide');
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Copay type updated successfully!!!");
						$('#messagemyModal').modal();
						showTableList();
					}
					if (data == "0") {
						$("#txtType").addClass('errorStyle');
						$("#txtType").focus();
						$('#txtTypeError').text('Copay type already exists.');
					}
				},
				error: function() {
					$('.close-confirm').click();
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
			}); // end of ajax
		});
    });
} // end update button

function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Deleted Copay");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteInsurancePlan("' + type + '","","");');
} // end click fucntion

function restoreClick(id,copay_type,value) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restored Copay");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteInsurancePlan("' + type + '","' + copay_type + '","' + value + '");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
// clear function
function clear() {
	$("#txtTypeError").text("");
	$("#txtType").removeClass("errorStyle");
	$("#txtType").val("");	
	$("#selValueError").text("");
	$("#selValue").removeClass("errorStyle");
	$("#selValue").val("");
	$("#selCompanyNameError").text("");
	$("#selCompanyName").removeClass("errorStyle");
	$("#selCompanyName").val("-1");
	$("#txtDesc").val("");
	$("#selCompanyName").focus();
}
// define that function perform for delete and restore event
function deleteInsurancePlan(type,copay_type,value) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete		
            type: "POST",
            cache: false,
            url: "controllers/admin/copay.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Copay type deleted successfully!!!");
                    $('#messagemyModal').modal();
                    insurancePlanTbl.fnReloadAjax();
                } else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This Copay type is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/copay.php",
            data: {
                "operation": "restore",
                "id": id,
                "copay": copay_type,
                "value": value,
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Copay type restored successfully!!!");
                    $('#messagemyModal').modal();
                    insurancePlanTbl.fnReloadAjax();

                }
                else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This Copay type is already exist , so you can not restore!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    }
}

function showTableList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#formInsurancePlan").hide();
    $(".blackborder").show();

    $("#tabInsurancePlan").removeClass('tab-detail-add');
    $("#tabInsurancePlan").addClass('tab-detail-remove');
    $("#tabInsurancePlanList").removeClass('tab-list-remove');    
    $("#tabInsurancePlanList").addClass('tab-list-add');
    $("#divInsurancePlan").addClass('list');    
    $("#btnReset").show();
    $("#btnAddInsurancePlan").show();
    $('#btnUpdate').hide();
    $('#tabInsurancePlan').html("+Add Copay");
    clear();
}

function showAddTab(){
    $("#formInsurancePlan").show();
    $(".blackborder").hide();
   
    $("#tabInsurancePlan").addClass('tab-detail-add');
    $("#tabInsurancePlan").removeClass('tab-detail-remove');
    $("#tabInsurancePlanList").removeClass('tab-list-add');
    $("#tabInsurancePlanList").addClass('tab-list-remove');
    $("#divInsurancePlan").addClass('list');
    $("#txtTypeError").text("");
    $("#txtType").removeClass("errorStyle");
    $("#txtType").focus();    
    removeErrorMessage();
}