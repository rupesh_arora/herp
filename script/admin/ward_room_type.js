var wardRoomTypeTable;//define global variable for datatable
$(document).ready(function(){
loader();	
/* ****************************************************************************************************
 * File Name    :   ward_room_type.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Arora
 * Created Date :   30th dec, 2015
 * Description  :   This page add and mange ward room type
 *************************************************************************************************** */
	
	/*By default hide this add screen part*/
	$("#advanced-wizard").hide();
	$("#wardList").css({
        "background-color": "#fff"
    });
	$("#tabRoomTypeList").css({
        "background-color": "#0c71c8",
        "color": "#fff"
    });
		
	//Click for go to add the room type screen part
    $("#tabAddRoomType").click(function() {
        $("#advanced-wizard").show();
        $(".blackborder").hide();
		clear();
        $("#tabAddRoomType").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tabRoomTypeList").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#wardList").css({
            "background": "#fff"
        });
		
		$('#txtRoomType').focus();
    });
	
	/*Click function for show the room type lists*/
    $("#tabRoomTypeList").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
        tabRoomTypeList();// Call the function for show the room type lists
    });
	
		// import excel on submit click
	$('#importDataFile').click(function(){
		var flag = false;
		if($('#file').prop('files')[0] == undefined || $('#file').val() == "") {
			$('#txtFileError').show();
			$('#txtFileError').text("Please choose file.");
			flag  = true;
		}
		if(flag == true){
			return false;
		}
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to upload this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			var file_data = $('#file').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: 'controllers/admin/ward_room_type.php', // point to server-side PHP script 
				dataType: 'text', // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					if(data != "" && data != "invalid file" && data =="1") {
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Your file has been successfully imported!!!");
						$('#messagemyModal').modal();
						$('#file').val("");
					}
					else if(data == "invalid file"){
						$('#txtFileError').show();
						$('#txtFileError').text("Please import only CSV/XLS file.");
						$('#file').val("");
					}
					else if (data =="You must have two column in your file i.e ward room type and description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="First column should be ward room type" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Data can't be null in ward room type column" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Second column should be description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Please insert data in first column and second column only i.e A & B") {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
					else if(data == ""){
						$('#txtFileError').show();
						$('#txtFileError').text("Data already exist.");
						$('#file').val("");
					}
				},
				error: function() {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Data Not Found!!!");
					$('#messagemyModal').modal();

				}
			});
		});
	});
	
	/*By default when radio button is not checked show all active data*/
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		loadDataTable();
    }
	/*On change of radio button check it is checked or not*/
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	wardRoomTypeTable.fnClearTable();//clear datatable
	    	wardRoomTypeTable.fnDestroy();//destroy datatable
	    	//Datatable code
			wardRoomTypeTable = $('#roomTypeTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					
					/*On click of restore icon call this function*/
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  wardRoomTypeTable.fnGetData(data);//fetch datatable data
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);//call function with id
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/ward_room_type.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				/*defining datatables column*/
				"aoColumns": [
					{ "mData": "type" },
					{ "mData": "description" },
					{
						"mData": function (o) { 
						var data = o;
						var id = data["id"];
						/*Return values that we get on click of restore*/
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},
				],
				/*Disable sort for following columns*/
				aoColumnDefs: [
					{ 'bSortable': false, 'aTargets': [ 1 ] },
					{ 'bSortable': false, 'aTargets': [ 2 ] }
				]
			} );
		}
		else{
			wardRoomTypeTable.fnClearTable();//clear datatable
	    	wardRoomTypeTable.fnDestroy();//destroy datatable
	    	//Datatable code
			loadDataTable();
		}
    });
	
	//Click function for add the room type to database
	$("#btnSubmit").click(function(){
		/*perform validation*/
		var flag="false";
		$("#txtRoomType").val($("#txtRoomType").val().trim());
		
		if($("#txtRoomType").val()==""){
			$('#txtRoomType').focus();
			$("#txtRoomTypeError").text("Please enter room type");
			$("#txtRoomType").addClass("errorStyle");   
			flag="true";
		}
		
		if(flag=="true"){
		return false;
		}
		
		var roomType = $("#txtRoomType").val();
		var description = $("#txtDescription").val();
		description = description.replace(/'/g, "&#39");//handling commas exception
		var postData = {
			"operation":"save",
			"roomType":roomType,
			"description":description
		}
		
		// Ajax call for save room type
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/ward_room_type.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){					
					$('#messagemyModal').modal();//showing modal
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Ward room type saved successfully!!!");
					$('#inactive-checkbox-tick').prop('checked', false).change();
					tabRoomTypeList();//call roomtype function to upadte it's list
				}
				else{
					$("#txtRoomTypeError").text("Room Type already exists");
					$("#txtRoomType").css({"border-color":"#a94442"});
					$('#txtRoomType').focus();
				}
				
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	});
	
	//keyup functionality
	$("#txtRoomType").keyup(function(){
		if($("#txtRoomType").val()!=""){
			$("#txtRoomTypeError").text("");
			$("#txtRoomType").removeClass("errorStyle");    	 
		}
	});		
});

/*define edit Click function for update*/
function editClick(id,room_type,description){	
	$('.modal-body').text("");
	$('#myModalLabel').text("Update room Type");
	$('.modal-body').html($("#advanced-first").html()).show();
    $('#myModal').modal('show');
	$('#myModal').on('shown.bs.modal', function() {
        $("#myModal #txtRoomType").focus();
    });
	$("#myModal #uploadFile").hide();
	$("#myModal #btnUpdateRoomType").removeAttr("style");
	$('#myModal #txtRoomType').val(room_type);
	$('#myModal #txtDescription').val(description.replace(/&#39/g, "'"));//handling commas and enter exceptions
	
	$('#selectedRow').val(id);	
	
	//Click function for update room type 
	$("#myModal #btnUpdateRoomType").click(function(){
		/*perform validation*/
		var flag = "false";
		$("#myModal #txtRoomType").val($("#myModal #txtRoomType").val().trim());
		
		if ($("#myModal #txtRoomType").val() == '') {
			$("#myModal #txtRoomType").focus();
			$("#myModal #txtRoomTypeError").text("Please enter room type");
			$("#myModal #txtRoomType").addClass("errorStyle");
			flag = "true";
		}
		
		if ($("#myModal #txtRoomTypeError").text() != '') {
			flag = "true";
		}
		
		if(flag == "true"){			
			return false;
		}
		else{
			var roomType = $("#myModal #txtRoomType").val();
			var description = $("#myModal #txtDescription").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();			
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				// Ajax call for update room type
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/ward_room_type.php",
					data :{            
						"operation" : "update",
						"id" : id,
						"roomType":roomType,
						"description":description
					},
					success: function(data) {	
						if(data != "0" && data != ""){
							$('#myModal').modal("hide");
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messagemyModal').modal();
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Ward room type updated successfully!!!");
							wardRoomTypeTable.fnReloadAjax();//reload datatable			
							clear();
						}
						if(data == "0"){
							$('.close-confirm').click();
							$("#myModal #txtRoomTypeError").text("Room type already exists");
							$("#myModal #txtRoomType").addClass("errorStyle");
							$('#myModal #txtRoomType').focus();					
						}
					},
					error:function() {
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messagemyModal').modal();
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					}
				});
			});
		}
	});		
	
	$("#myModal #txtRoomType").keyup(function(){
		if($("#myModal #txtRoomType").val()!=""){
			$("#myModal #txtRoomTypeError").text("");
			$("#myModal #txtRoomType").removeClass("errorStyle");
		}
	});	
}


/*Define click function for delete the room type*/ 
function deleteClick(id){	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete ward room type");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	var type="delete";
	$('#confirm').attr('onclick','deleteRoomType("'+type+'");');//pass attribute with sent opertion and function with it's type	
}

/*Define click function for restore the room type*/
function restoreClick(id){
	$('#selectedRow').val(id);	
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Restore ward room type");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();
	var type="restore";
	$('#confirm').attr('onclick','deleteRoomType("'+type+'");');//pass attribute with sent opertion and function with it's type	
	
}

/*function for show the list of room type*/
function tabRoomTypeList(){
	$("#advanced-wizard").hide();
	$(".blackborder").show();
	$("#tabAddRoomType").css({
		"background": "#fff",
		"color": "#000"
	});
	$("#tabRoomTypeList").css({
		"background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
		"color": "#fff"
	});
	$("#wardList").css({
		"background-color": "#fff"
	});
	$("#txtRoomType").removeClass("errorStyle");
	clear();
}
/*function to delete or update room type*/
function deleteRoomType(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
			
		//Ajax call for delete the room type 	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/ward_room_type.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Ward room type deleted successfully!!!");
					wardRoomTypeTable.fnReloadAjax();
					$('#messagemyModal').modal();
					 
				}
				else{
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("This room type is used , so you can not delete!!!");
					$('#messagemyModal').modal();
				}
			},
			error:function() {
				$('.modal-body').text("");
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}
	else{

	var id = $('#selectedRow').val();
	
		//Ajax call for restore the room type 
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/ward_room_type.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Ward room type restored successfully!!!");
					$('#messagemyModal').modal();
					wardRoomTypeTable.fnReloadAjax();
				}			
			},
			error:function() {
				$('.modal-body').text("");
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}
}
// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		 window.location.href = "http://localhost/herp/";
    }
});

/*reset button functionality*/
$("#btnReset").click(function(){
	$("#txtRoomType").removeClass("errorStyle");
	clear();
	$('#txtRoomType').focus();
});

//function for clear the data
function clear(){
	$('#txtRoomType').val("");
	$('#txtRoomTypeError').text("");
	$('#txtFileError').text('');
	$('#txtDescription').val("");
}
//# sourceURL = ward_room_type.js

function loadDataTable(){
	wardRoomTypeTable = $('#roomTypeTable').dataTable( {
			"bFilter": true,
			"processing": true,
			"sPaginationType":"full_numbers",
			"fnDrawCallback": function ( oSettings ) {
				
				/*On click of update icon call this function*/
				$('.update').unbind();
				$('.update').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData = wardRoomTypeTable.fnGetData(data);//get datatable data
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						var type = mData["type"];
						var description = mData["description"];
						editClick(id,type,description);//call edit click function with certain parameters to update
       
					}
				});
				/*On click of delete icon call this function*/
				$('.delete').unbind();
				$('.delete').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData =  wardRoomTypeTable.fnGetData(data);//get datatable data
					
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						deleteClick(id);//call delete click function with certain parameters to delete
					}
				});
			},
			
			"sAjaxSource":"controllers/admin/ward_room_type.php",
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "operation", "value": "show" });
			},
			/*defining datatables column*/
			"aoColumns": [
				{ "mData": "type" },
				{ "mData": "description" },
				{
					"mData": function (o) { 
					var data = o;
					/*Return values that we get on click of update and delete*/
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
					   " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
					   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
					   " style='font-size: 22px; color:#a94442; cursor:pointer;' "+
					   " data-original-title='Delete'></i>"; 
					}
				},	
			],
			/*Disable sort for following columns*/
			aoColumnDefs: [
				{ 'bSortable': false, 'aTargets': [ 1 ] },
				{ 'bSortable': false, 'aTargets': [ 2 ] }
			]
		} );
}