var viewReportTable;
$(document).ready(function(){
	// checklist checked
	debugger;

	// load leave type drop down
	bindLeavePeriod();
	
	removeErrorMessage();
	/* var data = {"operation": "SearchReports",leavePeriod:""};
	// load data on load
	loadData(data); */
	
	// inilize datatable
	viewReportTable = $('#tblSearchReports').dataTable({ // inilize datatable on load time.
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
		"bAutoWidth" : false
    });
	
	// button reset
	$("#btnReset").on('click',function(){
		viewReportTable.fnClearTable();
		/* var data = {"operation": "SearchReports",leavePeriod:""};
		// load data on load
		loadData(data); */
		clearFormDetails('.searchReportsForm');
	});
	
	//search functionality
	$("#btnSearch").on('click',function(){
		viewReportTable.fnClearTable();
		if($("#txtLeavePeriod").val() == "") {
			$("#txtLeavePeriod").addClass("errorStyle");
			$("#txtLeavePeriod").focus();
			$("#txtLeavePeriodError").text("Please select date");
			return true;			
		}
		var leavePeriod = $("#txtLeavePeriod").val();
		var postData = 	{
			"operation": "SearchReports",
			leavePeriod:leavePeriod
		};	
		loadData(postData);		
	});
}); 

// load data
function loadData(postData) {
	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/view_reports.php",
		data: postData,
		success: function(data) {
			if (data != null && data != "") {
				var parseData = jQuery.parseJSON(data);
				for (var i = 0; i < parseData.length; i++) {
					var taken = parseData[i].count_taken;
					var schedule = parseData[i].count_schedule;
					var approval = parseData[i].count_approval;
					var entitlements = parseData[i].entitlements;
					var name = parseData[i].name;
					var totalDays = parseInt(entitlements) - (parseInt(schedule) + parseInt(approval) + parseInt(taken))
					if( entitlements != null && name != null) {						
						viewReportTable.fnAddData([name,entitlements,approval,schedule,taken,totalDays]);
					}
				}
			}
		},
		error: function() {}
	});
}

// call function for bind leave period
function bindLeavePeriod() {
	$.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/view_reports.php",
        data: {
            "operation": "showBindLeavePeriod"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value=''>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].period + "</option>";
                }
                $('#txtLeavePeriod').html(option);
            }
        },
        error: function() {}
    });
}