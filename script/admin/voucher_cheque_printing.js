var vocherTable;
$(document).ready(function(){
    loader();
    var dataTableIntialization = 0;
    debugger;
    $(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});
   	vocherTable = $('#vocherTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "order": [[ 2, 'asc' ]],
        "aaSorting": [],
        "fnDrawCallback": function(oSettings) {
            // perform update event
            $('.updatePayment').unbind();
            $('.updatePayment').on('click', function() {
                var data = $(this).parents('tr')[0];
                var mData = vocherTable.fnGetData(data);
                if (null != mData) // null if we clicked on title row
                {
                    var id = mData["id"];
                    var ledgerId = mData["ledger_id"];
                    var ledgerName = mData['ledger'];
                    var paymentModeId = mData["payment_mode_id"];
                    var paymentMode = mData['payment_mode']                 ;
                    var registerPayeeId = mData["registered_payee_id"];
                    var registerPayeeType = mData["registered_payee_type"];             
                    var payeeName = mData["payee_name"];
                    var amount = mData['amount'];
                    var chequeNo = mData['cheque_no'];
                    editClick(id,ledgerId,paymentModeId,registerPayeeId,registerPayeeType,
                    	payeeName,ledgerName,paymentMode,chequeNo,amount
                    );
                }
            });
            //perform delete event
            $('.delete').unbind();
            $('.delete').on('click', function() {
                var data = $(this).parents('tr')[0];
                var mData = vocherTable.fnGetData(data);

                if (null != mData) // null if we clicked on title row
                {
                    var id = mData["id"];
                    deleteClick(id);
                }
            });

            if (dataTableIntialization == 1) {
            	var aiRows = vocherTable.fnGetNodes();//Get all rows of data table
                var aiData =  vocherTable.fnGetData();

                if(aiRows.length == 0){
                    return false;
                }
                $.each(aiData,function(index,value){
                    var isDisbursed = value.voucher_disbursement;
                    if(isDisbursed == "yes"){
                        $(aiRows[index]).css({'opacity':'.7','background':'#E7E3E4'});
                        $($(aiRows[index]).find(".fa")).unbind();
                        $($(aiRows[index]).find(".fa")).css({'cursor':'not-allowed','opacity':'.4'});
                    }
                });
            }               
        },
        "sAjaxSource": "controllers/admin/payment_voucher.php",
        "fnServerParams": function(aoData) {
            aoData.push({
                "name": "operation",
                "value": "show"
            });
        },
        "aoColumns": [
            {
                "mData": function (o) {  
                    var voucherId = o["id"];
                        
                    var voucherIdLength = voucherId.length;
                    for (var i=0;i<6-voucherIdLength;i++) {
                        voucherId = "0"+voucherId;
                    }
                    voucherId = voucherPrefix+voucherId;
                    return voucherId; 
                }
            }, {
                "mData": "payee_name"
            }, {
                "mData": "ledger"
            }, {
                "mData": "amount"
            },{
                "mData": "voucher_date"
            }, {
                "mData": "payment_mode"
            }, {
                "mData": "cheque_no"
            },  {
                "mData": function(o) {
                    var data = o;
                    dataTableIntialization = 1;
                    return "<i class='ui-tooltip fa fa-pencil updatePayment' title='Edit'" +
                        " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>";
                }
            },
        ],
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [7]
        }]
    });
   
});

function editClick(id,ledgerId,paymentModeId,registerPayeeId,registerPayeeType,payeeName,ledgerName,paymentMode,chequeNo,amount) {
	$("#myVoucherModal").modal();
    var voucherId = id;
    var voucherIdLength = voucherId.length;
    for (var i=0;i<6-voucherIdLength;i++) {
        voucherId = "0"+voucherId;
    }
    voucherId = voucherPrefix+voucherId;
	$("#txtVoucherNo").val(voucherId);
	$("#txtPayeeName").val(payeeName);
	$("#selLedger").html('<option value='+ledgerId+'>'+ledgerName+'</option>');
	$("#txtAmount").val(amount);
	$("#selPaymentMode").html('<option value='+paymentModeId+'>'+paymentMode+'</option>');
	$("#txtChequeNo").val(chequeNo);
	$('#selectedRow').val(id);
	$(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});
    $("#txtPostingDateError").text('');
    $("#txtPostingDate").removeClass('errorStyle');
	var startDate = new Date();
	$("#txtPostingDate").datepicker('setStartDate',startDate);
	$("#txtPostingDate").click(function() {
        $(".datepicker").css({
            "z-index": "99999"
        });
    });

	$("#txtPostingDate").val('');

	$("#btnSave").on('click',function(){
		var flag = false;
        if($('#txtPostingDate').val() == ''){
            $('#txtPostingDateError').text('Please select date');
            $('#txtPostingDate').addClass('errorStyle');
            flag = true;
        }
		if (flag == true) {
            $(".datepicker").css({
                "z-index": "99999"
            });
			return false;
		}
		var chequeDate = $("#txtPostingDate").val().trim();
		var postData = {
	        "operation": "voucherDisbursement",
	        id : id,
	        chequeDate : chequeDate
	    }
   		dataCall("controllers/admin/payment_voucher.php", postData, function (result){
   			if (result == '1') {
   				$('#myVoucherModal').modal('hide');
   				$('#messageBody').text("");
   				$('#disbursementModalLabel').text("Success");
				$('#disbursementModalBody').text("Voucher disbursed successfully!!!");
				$('#printDisbursementModal').modal();
				vocherTable.fnReloadAjax();
                $("#printDisbursementModal .modal-footer").html('<input type="button" class="btn_save" value="Print" onclick= "chequeDetails(\''+chequeDate+'\',\''+payeeName+'\',\''+amount+'\');" id="print">');
				dataTableIntialization = 0;
   			}
   		});
	});

	removeErrorMessage();

	$("#btnPrintCheque").on('click',function(){
		var flag = false;
		if(validTextField('#txtPostingDate','#txtPostingDateError','Please select date') == true)
		{
			flag = true;
		}
		if (flag == true) {
			return false
		}
		var chequeDate = $("#txtPostingDate").val().trim();
		
		chequeDetails(chequeDate,payeeName,amount);//defined in main js
	});
}
