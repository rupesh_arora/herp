$(document).ready(function(){
	debugger;
	restoreDefault();	// load default values
	//clickCheckBox();
	$('.plusIcon').click(function(){
		$(this).parent().siblings().removeClass('hide');
		$(this).parent().siblings().addClass('show');
		$(this).next().addClass('show');
		$(this).addClass('hide');
		clickCheckBox();// click checkbox if they have value
	});
	$('.minusIcon').click(function(){
		$(this).parent().siblings().removeClass('show');
		$(this).parent().siblings().addClass('hide');
		$(this).prev().removeClass('hide');
		$(this).removeClass('show');
	});

	$('input[type="checkBox"]').change(function(){
		if($(this).prop('checked') == true){
			$(this).siblings().siblings().last().find('textarea').prop('disabled',false);
		}
		else{
			$(this).siblings().siblings().last().find('textarea').prop('disabled',true);			
		}
	});

	$('#btnSubmit').click(function(){

		var PatientOtRegSms = $('#txtPatientOtRegSms').val().trim();
		var appointmentSms = $('#txtAppointmentSms').val().trim();
		var appointmentEmail = $('#txtAppointmentEmail').val().trim();
		var opdBookingSms = $('#txtOpdBookingSms').val().trim();
		var pateintRegSms = $('#txtPatientRegistrationSms').val().trim();
		var applyLeaveEmail = $('#txtApplyLeaveEmail').val().trim();
		var leaveCancelSms = $('#txtLeaveCancelSms').val().trim();
		var leaveCancelEmail = $('#txtLeaveCancelEmail').val().trim();
		var leaveApprovedSms = $('#txtLeaveApprovedSms').val().trim();
		var leaveApprovedEmail = $('#txtLeaveApprovedEmail').val().trim();
		var rosterManagementEmail = $('#txtRosterManagementEmail').val().trim();
		var billingEmail = $('#txtBillingEmail').val().trim();
		var OTBookingsSms = $('#txtOTBookingsSms').val().trim();
		var OTBookingsEmail = $('#txtOTBookingsEmail').val().trim();       
		var appointmentCancelSms = $('#txtAppointmentCancelSms').val().trim();       
		var appointmentCancelEmail = $('#txtAppointmentCancelEmail').val().trim();       
		var appointmentScheduledSms = $('#txtAppointmentScheduledSms').val().trim();       
		var appointmentScheduledEmail = $('#txtAppointmentScheduledEmail').val().trim();       

        var postData = {
            PatientOtRegSms : PatientOtRegSms,
            appointmentSms : appointmentSms,
            appointmentEmail : appointmentEmail,
            opdBookingSms : opdBookingSms,
            pateintRegSms : pateintRegSms,
            applyLeaveEmail : applyLeaveEmail,
            leaveCancelSms : leaveCancelSms,
            leaveCancelEmail : leaveCancelEmail,
            leaveApprovedSms : leaveApprovedSms,
            leaveApprovedEmail : leaveApprovedEmail,
            rosterManagementEmail : rosterManagementEmail,
            billingEmail : billingEmail,
            OTBookingsSms : OTBookingsSms,
            OTBookingsEmail : OTBookingsEmail,
            appointmentCancelSms:appointmentCancelSms,
            appointmentCancelEmail:appointmentCancelEmail,
            appointmentScheduledSms:appointmentScheduledSms,
            appointmentScheduledEmail:appointmentScheduledEmail,
            operation : "saveSmsEmail"
        }


        $.ajax(
            {                   
            type: "POST",
            cache: false,
            url: "controllers/admin/sms_email.php",
            datatype:"json",
            data: postData,
            
            success: function(data) {
                if(data != "0" && data != ""){
                    callSuccessPopUp('Success','Saved successfully!!!');
                    $('.minusIcon').click();
                    clear();
                }
            },
            error: function(){

            }
        });
	});
	
});
function restoreDefault() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/sms_email.php",
        data: {
            "operation": "showDefaultValues"
        },
        success: function(data) {
            if (data != null && data != "") {
            	var parseData = jQuery.parseJSON(data);
            	
            	$('#txtPatientOtRegSms').val(parseData.patient_ot_reg_sms);
				$('#txtAppointmentSms').val(parseData.appointment_sms);
				$('#txtAppointmentEmail').val(parseData.appointment_email);
				$('#txtOpdBookingSms').val(parseData.opd_booking_sms);
				$('#txtPatientRegistrationSms').val(parseData.patient_reg_sms);
				$('#txtApplyLeaveEmail').val(parseData.apply_leave_email);
				$('#txtLeaveCancelSms').val(parseData.leave_cancel_sms);
				$('#txtLeaveCancelEmail').val(parseData.leave_cancel_email);
				$('#txtLeaveApprovedSms').val(parseData.leave_approved_sms);
				$('#txtLeaveApprovedEmail').val(parseData.leave_approved_email);
				$('#txtRosterManagementEmail').val(parseData.roster_management_email);
				$('#txtBillingEmail').val(parseData.billing_email);
				$('#txtOTBookingsSms').val(parseData.ot_booking_sms);
				$('#txtOTBookingsEmail').val(parseData.ot_booking_email);
				
		    }
        },
        error: function() {}
    });
}

function clickCheckBox(){

	var flag = false;

	if($('#txtPatientOtRegSms').val() != ''){
		$('#PatientOTRegistrationSmscheckBox').prop('checked',true);
		$('#txtPatientOtRegSms').prop('disabled',false);
	}
	if($('#txtAppointmentSms').val() != ''){
		$('#AppointmentSmscheckBox').prop('checked',true);
		$('#txtAppointmentSms').prop('disabled',false);
	}
	if($('#txtAppointmentEmail').val() != ''){
		$('#AppointmentcheckBoxEmail').prop('checked',true);
		$('#txtAppointmentEmail').prop('disabled',false);
	}
	if($('#txtOpdBookingSms').val() != ''){
		$('#OpdBookingSmscheckBox').prop('checked',true);
		$('#txtOpdBookingSms').prop('disabled',false);
	}
	if($('#txtPatientRegistrationSms').val() != ''){
		$('#PatientRegistrationSmscheckBox').prop('checked',true);
		$('#txtPatientRegistrationSms').prop('disabled',false);
	}
	if($('#txtApplyLeaveEmail').val() != ''){
		$('#ApplyLeaveEmailcheckBox').prop('checked',true);
		$('#txtApplyLeaveEmail').prop('disabled',false);
	}
	if($('#txtLeaveCancelSms').val() != ''){
		$('#LeaveCancelSmscheckBox').prop('checked',true);
		$('#txtLeaveCancelSms').prop('disabled',false);
	}
	if($('#txtLeaveCancelEmail').val() != ''){
		$('#LeaveCancelEmailcheckBox').prop('checked',true);
		$('#txtLeaveCancelEmail').prop('disabled',false);
	}
	if($('#txtLeaveApprovedSms').val() != ''){
		$('#LeaveApprovedSmscheckBox').prop('checked',true);
		$('#txtLeaveApprovedSms').prop('disabled',false);
	}
	if($('#txtLeaveApprovedEmail').val() != ''){
		$('#LeaveApprovedEmailcheckBox').prop('checked',true);
		$('#txtLeaveApprovedEmail').prop('disabled',false);
	}
	if($('#txtRosterManagementEmail').val() != ''){
		$('#RosterManagementEmailcheckBox').prop('checked',true);
		$('#txtRosterManagementEmail').prop('disabled',false);
	}
	if($('#txtBillingEmail').val() != ''){
		$('#BillingEmailCheckBox').prop('checked',true);
		$('#txtBillingEmail').prop('disabled',false);
	}
	if($('#txtOTBookingsSms').val() != ''){
		$('#OTBookingsSmsCheckBox').prop('checked',true);
		$('#txtOTBookingsSms').prop('disabled',false);
	}
	if($('#txtOTBookingsEmail').val() != ''){
		$('#OTBookingsEmailCheckBox').prop('checked',true);
		$('#txtOTBookingsEmail').prop('disabled',false);
	}
	if($('#txtPatientOtRegSms').val() != ''){
		$('#PatientOTRegistrationSmscheckBox').prop('checked',true);
		$('#txtPatientOtRegSms').prop('disabled',false);
	}
}