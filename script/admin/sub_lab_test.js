$(document).ready(function() {
	var otable;		

	loader();

	//
	//Calling the function for loading the lab type and test 
	//	
	bindLabType(); 
	bindTest();
	bindUnitMeasure();	
	
	$("#advanced-wizard").hide();
	 $("#subLabTestList").css({
            "background": "#fff"
        });
		$("#tabSubLabTestList").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
    $("#tabAddSubLabTest").click(function() {
        $("#advanced-wizard").show();
        $(".blackborder").hide();
        $("#txtSubTestName").focus();
        $("#tabAddSubLabTest").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tabSubLabTestList").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#subLabTestList").css({
            "background": "#fff"
        });		
		bindLabType(); 
		bindTest();
		bindUnitMeasure();
		clear();
    });
    $("#tabSubLabTestList").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
        $("#advanced-wizard").hide();
        $(".blackborder").show();
        $('#btnAddDepartment').val("Add Lab Test");
		$('#tabAddLabTest').html("+Add Lab Test");
		$("#sublabTable").dataTable().fnReloadAjax();
        $("#tabAddSubLabTest").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#tabSubLabTestList").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#subLabTestList").css({
            "background": "#fff"
        });
    });	


    ///
    //     check box is check then show data

    if($('.inactive-checkbox').not(':checked')){
    	otable = $('#sublabTable').dataTable( {
			"bFilter": true,
			"processing": true,
			"sPaginationType":"full_numbers",
			"fnDrawCallback": function ( oSettings ) {
				$('.update').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData = $('#sublabTable').dataTable().fnGetData(data);
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						var name = mData["test_name"];
						var lab_type_id = mData["lab_type_id"];// this type comes from labtypes table
						var lab_test_id = mData["lab_test_id"];//This name form lab_test table
						var range = mData["range"];
						var unit_id = mData["unit_id"];//this unit comes from units table
						var description = mData["description"];
						editClick(id,name,lab_type_id,lab_test_id,range,unit_id,description);
       
					}
				});
				$('.delete').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData =  $('#sublabTable').dataTable().fnGetData(data);
					
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						deleteClick(id);
					}
				});
			},
			
			"sAjaxSource":"controllers/admin/sub_lab_test.php",
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "operation", "value": "show" });
			},
			"aoColumns": [
				{ "mData": "test_name" },
				{ "mData": "type" },
				{ "mData": "name" },
				{ "mData": "range" },
				{ "mData": "unit" },
				{ "mData": "description" },	
				{  
					"mData": function (o) { 
					
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
					   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
					   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
					   "  style='font-size: 22px; color:red; cursor:pointer;' "+
					   "  data-original-title='Delete'></i>"; 
					}
				},
			],
			aoColumnDefs: [
	           { aTargets: [ 6 ], bSortable: false },
        	]
		});
    }

    //show data active or inactive on check functionality in data table
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	otable.fnClearTable();
	    	otable.fnDestroy();
	    	otable="";
	    	otable = $('#sublabTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  $('#sublabTable').dataTable().fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var lab_type_id = mData["lab_type_id"];// this type comes from labtypes table
							var lab_test_id = mData["lab_test_id"];//This name form lab_test table
							restoreClick(id,lab_type_id,lab_test_id);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/sub_lab_test.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "checked" });
				},
				"aoColumns": [
					{ "mData": "test_name" },
					{ "mData": "type" },
					{ "mData": "name" },
					{ "mData": "range" },
					{ "mData": "unit" },
					{ "mData": "description" },	
					{  
						"mData": function (o) { 
						
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},
				],
				aoColumnDefs: [
		           { aTargets: [ 6 ], bSortable: false },
	        	]
			});
		}
		else{
			otable.fnClearTable();
	    	otable.fnDestroy();
	    	otable="";
			otable = $('#sublabTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.update').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData = $('#sublabTable').dataTable().fnGetData(data);
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var name = mData["test_name"];
							var lab_type_id = mData["lab_type_id"];// this type comes from labtypes table
							var lab_test_id = mData["lab_test_id"];//This name form lab_test table
							var range = mData["range"];
							var unit_id = mData["unit_id"];//this unit comes from units table
							var description = mData["description"];
							editClick(id,name,lab_type_id,lab_test_id,range,unit_id,description);
		   
						}
					});
					$('.delete').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  $('#sublabTable').dataTable().fnGetData(data);
						
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							deleteClick(id);
						}
					});
				},
				
				"sAjaxSource":"controllers/admin/sub_lab_test.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "show" });
				},
				"aoColumns": [
					{ "mData": "test_name" },
					{ "mData": "type" },
					{ "mData": "name" },
					{ "mData": "range" },
					{ "mData": "unit" },
					{ "mData": "description" },	
					{  
						"mData": function (o) { 
						
						return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
						   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
						   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
						   "  style='font-size: 22px; color:red; cursor:pointer;' "+
						   "  data-original-title='Delete'></i>"; 
						}
					},
				],
				aoColumnDefs: [
		           { aTargets: [ 6 ], bSortable: false },
	        	]
			});
		}
    });
	
    //validation and ajax call on submit button
	//
    $("#btnSubmit").click(function() {
        var flag = false;

        if ($("#selLabType").val() == "-1") {
            $("#selLabTypeError").text("Please select lab type");
            $("#selLabType").css({"border-color": "red"});
            flag = true;
        }
		if ($("#selTest").val() == "-1") {
            $("#selTestError").text("Please select test");
            $("#selTest").css({"border-color": "red"});
            flag = true;
        }
        if ($("#txtSubTestName").val().trim() == "") {
            $("#txtSubTestNameError").text("Please enter sub test name");
            $("#txtSubTestName").css({"border-color": "red"});
            flag = true;
        }     
        if ($("#selUnitMeasure").val().trim() == "-1") {
            $("#selUnitMeasureError").text("Please select unit measure");
            $("#selUnitMeasure").css({"border-color": "red"});
            flag = true;
        }      
        if (flag == true) {
            return false;
        }
		
		var labType = $("#selLabType").val();
		var test = $("#selTest").val();
		var subTestName = $("#txtSubTestName").val().trim();
		var range = $("#txtNormalRange").val();
		var unit_id = $("#selUnitMeasure").val();
		var description = $("#txtDescription").val();
		description = description.replace(/'/g, "&#39;");
		var postData = {
			"operation":"save",
			"labType":labType,
			"test":test,
			"subTestName":subTestName,
			"range":range,
			"unit_id":unit_id,
			"description":description
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/sub_lab_test.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Sub lab test saved successfully!!!");
					$('#mymessageModal').click();
					clear();
					$('#inactive-checkbox-tick').prop('checked', false).change();
					//after sucessful data sent to database redirect to datatable list
					$("#advanced-wizard").hide();
       				$(".blackborder").show();
       				$("#tabSubLabTestList").css({
            			"background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            			"color": "#fff"
        			});
        			$("#tabAddSubLabTest").css({
            			"background": "#fff",
            			"color": "#000"
        			});
        			$("#sublabTable").dataTable().fnReloadAjax();

				}
				else{
					$("#txtSubTestNameError").text("Sub test name already exists");
				}				
			},
			error:function() {
				alert('error');
			}
		});
	});	
	
	//
    //keyup functionality
	//
    $("#selLabType").change(function() {
        if ($("#selLabType").val() != "-1") {
            $("#selLabTypeError").text("");
            $("#selLabType").removeClass("errorStyle");
        }
        else{
        	$("#selLabTypeError").text("Please select lab type");
        	$("#selLabType").addClass("errorStyle");
        }
    });
	
	$("#selTest").change(function() {
        if ($("#selTest").val() != "-1") {
            $("#selTestError").text("");
            $("#selTest").removeClass("errorStyle");
        }
        else{
			$("#selTestError").text("Please select test");
			$("#selTest").addClass("errorStyle");
        }
    });

    $("#selUnitMeasure").change(function() {
        if ($("#selUnitMeasure").val() != "-1") {
            $("#selUnitMeasureError").text("");
            $("#selUnitMeasure").removeClass("errorStyle");
        }
        else{
			$("#selUnitMeasureError").text("Please select unit measure");
			$("#selUnitMeasure").addClass("errorStyle");
        }
    });
	
    $("#txtSubTestName").keyup(function() {
        if ($("#txtSubTestName").val() != "") {
            $("#txtSubTestNameError").text("");
            $("#txtSubTestName").removeClass("errorStyle");
        }
        else{
        	$("#txtSubTestNameError").text("Please enter test name");
			$("#txtSubTestName").addClass("errorStyle");
        }
    });	

    $("#btnReset").click(function(){
    	$("#txtSubTestName").focus();
		clear();
    });
});

//		
//function for edit the bed 
//
function editClick(id,name,lab_type_id,lab_test_id,range,unit_id,description){	
    
	$('#myModalLabel').text("Update Sub Lab Test");
	$('.modal-body').html($("#subLabTestModel").html()).show();
	$('#modal').click();
	$("#myModal #btnUpdateSubLab").removeAttr("style");
	$('#myModal #txtSubTestName').val(name);
	$("#myModal #selTest").val(lab_test_id);
	$("#myModal #selLabType").val(lab_type_id);
	$("#myModal #txtNormalRange").val(range);
	$("#myModal #selUnitMeasure").val(unit_id);
	$('#myModal #txtDescription').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id);	
	
	
	$("#myModal #btnUpdateSubLab").click(function(){
		var flag = "false";
		
		//validation
		if ($("#myModal #selLabType").val() == "-1") {
            $("#myModal #selLabTypeError").text("Please select ward");
             $("#myModal #selLabType").addClass("errorStyle");
            flag = "true";
        }
        if ($("#myModal #selUnitMeasure").val() == "-1") {
            $("#myModal #selUnitMeasureError").text("Please select unit measure");
             $("#myModal #selUnitMeasure").addClass("errorStyle");
            flag = "true";
        }
		if ($("#myModal #selTest").val() == "-1") {
            $("#myModal #selTestError").text("Please select room");
             $("#myModal #selTest").addClass("errorStyle");
            flag = "true";
        }
        if ($("#myModal #txtSubTestName").val().trim() == "") {
            $("#myModal #txtSubTestNameError").text("Please enter bed lab_test_id");
             $("#myModal #txtSubTestName").addClass("errorStyle");
            flag = "true";
        }
		
		if(flag == "true"){			
		return false;
		}
		
		var labType = $("#myModal #selLabType").val();
		var test = $("#myModal #selTest").val();
		var subTestName = $("#myModal #txtSubTestName").val().trim();
		var range = $("#myModal #txtNormalRange").val();
		var unit_id = $("#myModal #selUnitMeasure").val();
		var description = $("#myModal #txtDescription").val();
		description = description.replace(/'/g, "&#39;");
		var id = $('#selectedRow').val();
		
		
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/sub_lab_test.php",
			data :{            
				"operation" : "update",
				"id" : id,
				"labType":labType,
				"test":test,
				"subTestName":subTestName,
				"range":range,
				"unit_id":unit_id,
				"description":description
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('#myModal').modal('toggle');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Sub lab test updated successfully!!!");
					$('#mymessageModal').click();
					$("#sublabTable").dataTable().fnReloadAjax();
					clear();
				}
			},
			error:function() {
				alert('error');
			}
		});
	});
	
	$("#myModal #selLabType").change(function() {
        if ($("#myModal #selLabType").val() != "-1") {
            $("#myModal #selLabTypeError").text("");
            $("#myModal #selLabType").removeClass("errorStyle");
        }
    });
    $("#myModal #selUnitMeasure").change(function() {
        if ($("#myModal #selUnitMeasure").val() != "-1") {
            $("#myModal #selUnitMeasureError").text("");
            $("#myModal #selUnitMeasure").removeClass("errorStyle");
        }
    });
	
	$("#myModal #selTest").change(function() {
        if ($("#myModal #selTest").val() != "-1") {
            $("#myModal #selTestError").text("");
            $("#myModal #selTest").removeClass("errorStyle");
        }
    });
	
    $("#myModal #txtSubTestName").keyup(function() {
        if ($("#myModal #txtSubTestName").val() != "") {
            $("#myModal #txtSubTestNameError").text("");
            $("#myModal #txtSubTestName").removeClass("errorStyle");
        }
    });
	
	$("#myModal #txtrange").keyup(function() {
        if ($("#myModal #txtrange").val() != "") {
            $("#myModal #txtrangeError").text("");
            $("#myModal #txtrange").removeClass("errorStyle");
        }
    });
	
	$("#myModal #txtunit_id").keyup(function() {
        if ($("#myModal #txtunit_id").val() != "") {
            $("#myModal #txtunit_idError").text("");
            $("#myModal #txtunit_id").removeClass("errorStyle");
        } 
    });	
}

//		
//function for delete the sublab
//

function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('#confirmMyModalLabel').text("Delete sub lab test");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#myConfirmModal').click(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteSubLabTest("'+type+'");');
}

//Click function for restore the forensic test
function restoreClick(id,lab_type_id,lab_test_id){
	$('#selectedRow').val(id);
	$('#selLabType').val(lab_type_id);
	$('#selTest').val(lab_test_id);
	$('#confirmMyModalLabel').text("Restore sub lab test");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#myConfirmModal').click();

	var type="restore";
	$('#confirm').attr('onclick','deleteSubLabTest("'+type+'");');
}

function deleteSubLabTest(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
			
			
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/sub_lab_test.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Sub lab test deleted successfully!!!");
					$('#mymessageModal').click();
					$("#sublabTable").dataTable().fnReloadAjax();					 
				}
				else{
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("This test is used , so you can not delete!!!");
					$('#mymessageModal').click();
				}
			},
			error: function()
			{
				alert('error');
				  
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
		var lab_type_id = $('#selLabType').val();
		var lab_test_id = $("#selTest").val();
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/sub_lab_test.php",
			data :{            
				"operation" : "restore",
				"lab_type_id" : lab_type_id,
				"lab_test_id" : lab_test_id,
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Sub lab test restored successfully!!!");
					$('#mymessageModal').click();
					$("#sublabTable").dataTable().fnReloadAjax();
					
					clear();
				}	
				if(data == "0"){
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("It can not be restore!!!");
					$('#mymessageModal').click();
					$("#sublabTable").dataTable().fnReloadAjax();
					
				}				
			},
			error:function()
			{
				alert('error');
				  
			}
		});
	}
}

//
//Function for clear the data
//
function clear(){
	$('#selLabType').val("");
	$('#selLabTypeError').text("");
	$('#selTest').val("");
	$('#selTestError').text("");
	$('#txtSubTestName').val("");
	$('#txtSubTestNameError').text("");
	$('#txtNormalRange').val("");
	$('#selUnitMeasure').val("");
	$('#selUnitMeasureError').text("");
	$('#txtDescription').val("");
	$("#txtSubTestName").removeClass("errorStyle");
	$("#selLabType").removeClass("errorStyle");
	$("#selTest").removeClass("errorStyle");
	$("#selUnitMeasure").removeClass("errorStyle");
}
 function bindLabType(){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/sub_lab_test.php",
		data: {
			"operation":"showLabType"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>Select</option>";
				for (var i=0;i<parseData.length;i++)
				{
				option+="<option value='"+parseData[i].id+"'>"+parseData[i].type+"</option>";
				}				
				$('#selLabType').html(option);				
			}
		},
		error: function(){			
		}
	});	
} 
function bindTest(){
	$("#selTest").prop("selectedIndex", -1);
	$.ajax({     
        type: "POST",
        cache: false,
        url: "controllers/admin/sub_lab_test.php",
        data: {
        "operation":"showTest"
        },
        success : function(data) { 
        if(data != null && data != ""){
            var parseData = jQuery.parseJSON(data);

            var option ="<option value='-1'>Please select</option>";
            for (var i=0;i<parseData.length;i++)
            {
            option+="<option value='"+parseData[i].id+"'>"+parseData[i].name+"</option>";
            }
            $('#selTest').html(option);          
            }
        },
        error:function(){
        }
    }); 
}
function bindUnitMeasure(){

	$.ajax({     
        type: "POST",
        cache: false,
        url: "controllers/admin/sub_lab_test.php",
        data: {
        "operation":"showUnitMeasure"
        },
        success : function(data) { 
        if(data != null && data != ""){
            var parseData = jQuery.parseJSON(data);

            var option ="<option value='-1'>Please select</option>";
            for (var i=0;i<parseData.length;i++)
            {
            option+="<option value='"+parseData[i].id+"'>"+parseData[i].unit+"</option>";
            }
            $('#selUnitMeasure').html(option);          
            }
        },
        error:function(){
        }
    }); 
}
//# sourceURL = sub_lab_test.js