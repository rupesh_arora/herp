var summarizedIncome = '';
$(document).ready(function() {
	debugger;
	$(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});

	allGlAccount();//call from common.js
	removeErrorMessage();//call from common.js

	var startDate = new Date();
	$('#toDate,#fromDate').datepicker('setEndDate', startDate);	

	summarizedIncome = $('#summarizedIncome').DataTable({
        "bPaginate": true,
        aaSorting:[],
        aLengthMenu: [
	        [25, 50, 100, 200, -1],
	        [25, 50, 100, 200, "All"]
	    ],
    	iDisplayLength: -1    
    });

    $("#viewReport").on('click',function(){
		var toDate = $("#toDate").val().trim();
		var fromDate = $("#fromDate").val().trim();
    	var postData = {
			"operation" : "showData",
			'toDate' : toDate,
			'fromDate' : fromDate
		}
		summarizedIncome.fnClearTable();
		dataCall("./controllers/admin/income_expense.php", postData, function (result) {
			if (result.trim() != "") {
				var parseData = JSON.parse(result);
				var forensicCash = parseData[0][0]['Forensic Cash'];
				var labCash = parseData[0][0]['Lab Cash'];
				var procedureCash = parseData[0][0]['Procedure Cash'];
				var radiologyCash = parseData[0][0]['Radiology Cash'];
				var serviceCash = parseData[0][0]['Service Cash'];

				var imprestExpense = parseData[1][0]['Imprest Disbursment'];
				var voucherExpense = parseData[1][0]['Payment Voucher'];
				var pettyCashExpense = parseData[1][0]['Petty Cash'];
				var inventoryPaymentExpense = parseData[1][0]['Receive Order'];

				var forensicCashPrev = parseData[0][0]['Forensic Cash_1'];
				var labCashPrev = parseData[0][0]['Lab Cash_1'];
				var procedureCashPrev = parseData[0][0]['Procedure Cash_1'];
				var radiologyCashPrev = parseData[0][0]['Radiology Cash_1'];
				var serviceCashPrev = parseData[0][0]['Service Cash_1'];

				var imprestExpensePrev = parseData[1][0]['Imprest Disbursment_1'];
				var voucherExpensePrev = parseData[1][0]['Payment Voucher_1'];
				var pettyCashExpensePrev = parseData[1][0]['Petty Cash_1'];
				var inventoryPaymentExpensePrev = parseData[1][0]['Receive Order_1'];

				var totalIncome = 0;
				var totalExpense = 0;

				var totalIncomePrev = 0;
				var totalExpensePrev = 0;

				if (forensicCash !='' && forensicCash != null) {
					totalIncome+=parseInt(forensicCash);
				}
				if (labCash !='' && labCash != null) {
					totalIncome+=parseInt(labCash);
				}
				if (procedureCash !='' && procedureCash != null) {
					totalIncome+=parseInt(procedureCash);
				}
				if (radiologyCash !='' && radiologyCash != null) {
					totalIncome+=parseInt(radiologyCash);
				}
				if (serviceCash !='' && serviceCash != null) {
					totalIncome+=parseInt(serviceCash);
				}

				if (imprestExpense !='' && imprestExpense != null) {
					totalExpense+=parseInt(imprestExpense);
				}
				if (voucherExpense !='' && voucherExpense != null) {
					totalExpense+=parseInt(voucherExpense);
				}
				if (pettyCashExpense !='' && pettyCashExpense != null) {
					totalExpense+=parseInt(pettyCashExpense);
				}
				if (inventoryPaymentExpense !='' && inventoryPaymentExpense != null) {
					totalExpense+=parseInt(inventoryPaymentExpense);
				}

				//previous year
				if (forensicCashPrev !='' && forensicCashPrev != null) {
					totalIncomePrev+=parseInt(forensicCashPrev);
				}
				if (labCashPrev !='' && labCashPrev != null) {
					totalIncomePrev+=parseInt(labCashPrev);
				}
				if (procedureCashPrev !='' && procedureCashPrev != null) {
					totalIncomePrev+=parseInt(procedureCashPrev);
				}
				if (radiologyCashPrev !='' && radiologyCashPrev != null) {
					totalIncomePrev+=parseInt(radiologyCashPrev);
				}
				if (serviceCashPrev !='' && serviceCashPrev != null) {
					totalIncomePrev+=parseInt(serviceCashPrev);
				}

				if (imprestExpensePrev !='' && imprestExpensePrev != null) {
					totalExpensePrev+=parseInt(imprestExpensePrev);
				}
				if (voucherExpensePrev !='' && voucherExpensePrev != null) {
					totalExpensePrev+=parseInt(voucherExpensePrev);
				}
				if (pettyCashExpensePrev !='' && pettyCashExpensePrev != null) {
					totalExpensePrev+=parseInt(pettyCashExpensePrev);
				}
				if (inventoryPaymentExpensePrev !='' && inventoryPaymentExpensePrev != null) {
					totalExpensePrev+=parseInt(inventoryPaymentExpensePrev);
				}

				summarizedIncome.fnAddData(['<span class="highLighted"><u>Income</u></span>','','']);
				summarizedIncome.fnAddData(['Lab Income',labCash,labCashPrev]);
				summarizedIncome.fnAddData(['Radiology Income',radiologyCash,radiologyCashPrev]);
				summarizedIncome.fnAddData(['Forensic Income',forensicCash,forensicCashPrev]);
				summarizedIncome.fnAddData(['Service Income',serviceCash,serviceCashPrev]);
				summarizedIncome.fnAddData(['Procedure Income',procedureCash,procedureCashPrev]);
				summarizedIncome.fnAddData(['<span class="highLighted">Total Income</span>',totalIncome,totalIncomePrev]);
				summarizedIncome.fnAddData(['','','']);
				summarizedIncome.fnAddData(['<span class="highLighted"><u>Expense</u></span>','','']);
				summarizedIncome.fnAddData(['Imprest Expense',imprestExpense,imprestExpensePrev]);
				summarizedIncome.fnAddData(['Payment Voucher Expense',voucherExpense,voucherExpensePrev]);
				summarizedIncome.fnAddData(['Petty Cash Expense',pettyCashExpense,pettyCashExpensePrev]);
				summarizedIncome.fnAddData(['Inventory Expense',inventoryPaymentExpense,inventoryPaymentExpensePrev]);
				summarizedIncome.fnAddData(['<span class="highLighted">Total Expense</span>',totalExpense,totalExpensePrev]);
				summarizedIncome.fnAddData(['','','']);
				summarizedIncome.fnAddData(['<span class="highLighted">SURPLUS/DEFICT FOR THE YEAR</span>',totalIncome-totalExpense,totalIncomePrev-totalExpensePrev]);				
			}
		});
    });
    getAmountPrefix();

});
function getAmountPrefix(){
    var postData = {
        "operation": "show"
    }
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/configuration.php",
        datatype: "json",
        data: postData,

        success: function(data) {
            if(data != "0" && data != ""){
				var parseData = jQuery.parseJSON(data);				
				amountPrefix = parseData[5].value;
			}
			$('#summarizedIncome thead th:eq(1)').html(todayDate().split('-')[0]+'<br>'+amountPrefix);
			$('#summarizedIncome thead th:eq(2)').html((todayDate().split('-')[0]-1)+'<br>'+amountPrefix);
        },
        error: function() {             
            $('.modal-body').text("");
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();
        }            
    });
}