var paymentModeTable;
$(document).ready(function(){
    loader();
    debugger;   
    bindLedger('#selLedger');  //bind for loading ledger
    bindAssetAccount()  //bind for loading account number of asset type
    /*Hide add ward by default functionality*/
    $("#advanced-wizard").hide();
    $("#paymentModeList").addClass('list');    
    $("#tabPaymentModeList").addClass('tab-list-add');
   
    /*Click for add the sub Type*/
    $("#tabAddPaymentMode").click(function() {
        if ($("#tabPaymentModeList").text() == "Payment Mode List" ) {
            $("#chkDefaultPayMode").prop("checked",false);
            $("#chkDefaultPayMode").prop("disabled",true);
            clear();
        } 
        
       showAddTab();       
    });
    
    $("#tabPaymentModeList").click(function() {
        clear();
        tabPaymentModeList();
        $(this).html('<i class="fa fa-bars fa-fw"></i>Payment Mode List');
    });

    /*$("#tabsubTypeList").click(function() {
        $('#inactive-checkbox-tick').prop('checked', false).change();
        clear();
        tabsubTypeList();
    });*/

    removeErrorMessage();  // remove error messages

    $('#btnSubmit').click(function() {
        var flag = false;

        if($('#txtSoapPassword').val() == '') {
            $('#txtSoapPasswordError').text('Please enter soap password');
            $('#txtSoapPassword').focus();
            $('#txtSoapPassword').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtSoapUserName').val() == '') {
            $('#txtSoapUserNameError').text('Please enter soap user name');
            $('#txtSoapUserName').focus();
            $('#txtSoapUserName').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtPayeeName').val() == '') {
            $('#txtPayeeNameError').text('Please enter payee name');
            $('#txtPayeeName').focus();
            $('#txtPayeeName').addClass("errorStyle"); 
            flag = true;
        }
        if($('#selAssetAccount').val() == '') {
            $('#selAssetAccountError').text('Please select asset account');
            $('#selAssetAccount').focus();
            $('#selAssetAccount').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtStartingBalance').val() == '') {
            $('#txtStartingBalanceError').text('Please enter starting balance');
            $('#txtStartingBalance').focus();
            $('#txtStartingBalance').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtAccountNumber').val() == '') {
            $('#txtAccountNumberError').text('Please enter account number');
            $('#txtAccountNumber').focus();
            $('#txtAccountNumber').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtAccountNumber').val().length > 10) {
            $('#txtAccountNumberError').text('Please enter valid account number');
            $('#txtAccountNumber').focus();
            $('#txtAccountNumber').addClass("errorStyle"); 
            flag = true;
        }
        if($('#selLedger').val() == '') {
            $('#selLedgerError').text('Please select ledger');
            $('#selLedger').focus();
            $('#selLedger').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtName').val() == '') {
            $('#txtNameError').text('Please enter name');
            $('#txtName').focus();
            $('#txtName').addClass("errorStyle"); 
            flag = true;
        }
        if(flag == true) {
            return false;
        }
        if ($("#radioAsSource").is(':checked') == false && $("#radioAsBank").is(':checked') == false) {
            callSuccessPopUp('Alert',"Please check any of the one radiobutton");
            return false;
        }
        var source_bank = '';
        if ($("#radioAsSource").is(':checked') == true){
            source_bank = "source";
        }
        if ($("#radioAsBank").is(':checked') == true){
            source_bank = "bank";
        }

        if ($("#chkReceivedPayment").prop("checked") == true) {
            var receivedPayment = 1;
        }
        else{
            var receivedPayment = 0;
        }

        if ($("#chkMakePayment").prop("checked") == true) {
            var makePayment = 1;
        }
        else{
            var makePayment = 0;
        }

        if ($("#chkAlnumMode").prop("checked") == true) {
            var alnumMode = 1;
        }
        else{
            var alnumMode = 0;
        }

        if ($("#chkAccountActive").prop("checked") == true) {
            var accountActive = 1;
        }
        else{
            var accountActive = 0;
        }

        if ($("#chkAvailableForBanking").prop("checked") == true) {
            var availableForbanking = 1;
        }
        else{
            var availableForbanking = 0;
        }

        var defaultMode = '';
        if ($("#chkDefaultPayMode").prop("checked") == true) {
            var defaultMode = 1;
        }
        else{
            var defaultMode = 0;
        }
        var name = $('#txtName').val().trim();
        var ledger = $('#selLedger').val();
        var accountNumber = $('#txtAccountNumber').val().trim();
        var startingBalance = $('#txtStartingBalance').val().trim();
        var assetAccount = $('#selAssetAccount').val();
        var payeeName = $('#txtPayeeName').val().trim();
        var soapUserName = $('#txtSoapUserName').val().trim();
        var soapPassword = $('#txtSoapPassword').val().trim();

        var postData = {
            name : name,
            ledger : ledger,
            accountNumber : accountNumber,
            startingBalance : startingBalance,
            assetAccount : assetAccount,
            payeeName : payeeName,
            soapUserName : soapUserName,
            soapPassword : soapPassword,
            operation : "savePaymentMode",
            receivedPayment : receivedPayment,
            makePayment : makePayment,
            alnumMode : alnumMode,
            accountActive : accountActive,
            availableForbanking : availableForbanking,
            source_bank : source_bank,
            defaultMode : defaultMode
        }


        $.ajax(
            {                   
            type: "POST",
            cache: false,
            url: "controllers/admin/payment_mode.php",
            datatype:"json",
            data: postData,
            
            success: function(data) {
                if(data != "0" && data == "1"){
                    callSuccessPopUp('Success','Saved successfully!!!');
                    tabPaymentModeList();
                    clear();
                }
                else {
                    $('#txtAccountNumberError').text('Name is already exist');
                    $('#txtAccountNumber').focus();
                    $('#txtAccountNumber').addClass('errorStyle');
                }
            },
            error: function(){

            }
        });
    });

    $('#btnReset').click(function() {
        clear();
    });

    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load        
        showActiveRecords();
    }

    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked            
            showInActiveRecords()
        } 
        else { // show active data on unchecked 
            showActiveRecords();
        }
    });
    $("#paymentModeTable td:first-child").removeClass('sorting_1');

    $('#selLedger').on('change',function(){
        if ($(this).val() != '') {
            $("#chkDefaultPayMode").attr('disabled',false);
        }
        else{
            $("#chkDefaultPayMode").attr('disabled',true);            
        }
        $("#chkDefaultPayMode").prop("checked",false);
    });

    
    $("#chkDefaultPayMode").on('change',function(){
        if ($("#tabPaymentModeList").text() == "Payment Mode List" ) {
            if ( $(this).is(":checked") ) {
                var ledger = $('#selLedger').val();
                checkDefaultPayMode(ledger,'');
            }        
        }
    });
        
});

function editClick(id,name,ledgerName,accountNumber,startingBalance,assetAccount,payeeName,soapUserName,soapPassword,
    receivePayment,makePayment,alnumModeNo,accountActive,availableForbanking,is_available_bank_source,is_default_payment_mode) {
    
    showAddTab();
    $("#btnReset").hide();
    $("#btnSubmit").hide();
    $('#btnUpdate').show();
    $('#tabPaymentModeList').html("+Update Payment Mode");
    $("#txtName").removeClass("errorStyle");
    $("#txtNameError").text("");

   
    $('#txtName').val(name);
    $('#selLedger').val(ledgerName);
    $('#txtAccountNumber').val(accountNumber);
    $('#txtStartingBalance').val(startingBalance);
    $('#selAssetAccount').val(assetAccount);
    $('#txtPayeeName').val(payeeName);
    $('#txtSoapUserName').val(soapUserName);
    $('#txtSoapPassword').val(soapPassword);

    if (receivePayment == "1"){
        $('#chkReceivedPayment').prop("checked",true);
    }
    if (makePayment == "1") {
        $('#chkMakePayment').prop("checked",true);
    }
    if (alnumModeNo =="1") {
        $('#chkAlnumMode').prop("checked",true);
    }
    if (accountActive =="1") {
        $('#chkAccountActive').prop("checked",true);
    }
    if (availableForbanking =="1") {
        $('#chkAvailableForBanking').prop("checked",true);
    }
    $('#chkDefaultPayMode').attr("disabled",false);
    if (is_default_payment_mode =="1") {
        $('#chkDefaultPayMode').prop("checked",true);
    }
    else{
        $('#chkDefaultPayMode').prop("checked",false);
    }
    if (is_available_bank_source == "bank") {
        $("#radioAsBank").prop('checked',true);
        $("#radioAsSource").prop('checked',false);
    }
    else{
        $("#radioAsSource").prop('checked',true);
        $("#radioAsBank").prop('checked',false);
    }
    $('#selectedRow').val(id);
    //validation
    //remove validation style
        
    removeErrorMessage();

    $("#btnUpdate").click(function() { // click update button
        var flag = false;
        if ($("#txtSoapPassword").val()== '') {
            $("#txtSoapPasswordError").text("Please enter soap password");
            $("#txtSoapPassword").focus();
            $("#txtSoapPassword").addClass("errorStyle");
            flag = true;
        }
        if ($("#txtSoapUserName").val()== '') {
            $("#ttxtSoapUserNameError").text("Please enter soap user name");
            $("#txtSoapUserName").focus();
            $("#txtSoapUserName").addClass("errorStyle");
            flag = true;
        }
        if ($("#txtPayeeName").val()== '') {
            $("#txtPayeeNameError").text("Please enter payee name");
            $("#txtPayeeName").focus();
            $("#txtPayeeName").addClass("errorStyle");
            flag = true;
        }
        if ($("#selAssetAccount").val()== '') {
            $("#selAssetAccountError").text("Please select asset account");
            $("#selAssetAccount").focus();
            $("#selAssetAccount").addClass("errorStyle");
            flag = true;
        }
        if ($("#txtStartingBalance").val()== '') {
            $("#txtStartingBalanceError").text("Please enter starting balance");
            $("#txtStartingBalance").focus();
            $("#txtStartingBalance").addClass("errorStyle");
            flag = true;
        }
        if ($("#txtAccountNumber").val()== '') {
            $("#txtAccountNumberError").text("Please enter account number");
            $("#txtAccountNumber").focus();
            $("#txtAccountNumber").addClass("errorStyle");
            flag = true;
        }
        if ($("#selLedger").val()== '') {
            $("#selLedgerError").text("Please select ledger");
            $("#selLedger").focus();
            $("#selLedger").addClass("errorStyle");
            flag = true;
        }
        if ($("#txtName").val()== '') {
            $("#txtNameError").text("Please enter name");
            $("#txtName").focus();
            $("#txtName").addClass("errorStyle");
            flag = true;
        }

        
        if(flag == true) {
            return false;
        }

        if ($("#chkReceivedPayment").prop("checked") == true) {
            var receivedPayment = 1;
        }
        else{
            var receivedPayment = 0;
        }

        if ($("#chkMakePayment").prop("checked") == true) {
            var makePayment = 1;
        }
        else{
            var makePayment = 0;
        }

        if ($("#chkAlnumMode").prop("checked") == true) {
            var alnumMode = 1;
        }
        else{
            var alnumMode = 0;
        }

        if ($("#chkAccountActive").prop("checked") == true) {
            var accountActive = 1;
        }
        else{
            var accountActive = 0;
        }

        if ($("#chkAvailableForBanking").prop("checked") == true) {
            var availableForbanking = 1;
        }
        else{
            var availableForbanking = 0;
        }
        var source_bank = '';
        if ($("#radioAsSource").is(':checked') == true){
            source_bank = "source";
        }
        if ($("#radioAsBank").is(':checked') == true){
            source_bank = "bank";
        }
        var defaultMode = '';
        if ($("#chkDefaultPayMode").prop("checked") == true) {
            var defaultMode = 1;
        }
        else{
            var defaultMode = 0;
        }
        var name = $('#txtName').val().trim();
        var ledger = $('#selLedger').val();
        var accountNumber = $('#txtAccountNumber').val().trim();
        var startingBalance = $('#txtStartingBalance').val().trim();
        var assetAccount = $('#selAssetAccount').val();
        var payeeName = $('#txtPayeeName').val().trim();
        var soapUserName = $('#txtSoapUserName').val().trim();
        var soapPassword = $('#txtSoapPassword').val().trim();
        var id = $('#selectedRow').val();
        
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
        var postData = {
            "operation": "update",
            name : name,
            ledger : ledger,
            accountNumber : accountNumber,
            startingBalance : startingBalance,
            assetAccount : assetAccount,
            payeeName : payeeName,
            soapUserName : soapUserName,
            soapPassword : soapPassword,
            "id": id,
            receivedPayment : receivedPayment,
            makePayment : makePayment,
            alnumMode : alnumMode,
            accountActive : accountActive,
            availableForbanking : availableForbanking,
            source_bank : source_bank,
            defaultMode : defaultMode
        }
        $.ajax( //ajax call for update data
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/payment_mode.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('#myModal').modal('hide');
                        $('.close-confirm').click();
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Payment mode updated successfully!!!");
                        $('#messagemyModal').modal();
                        tabPaymentModeList();
                        clear();
                    }
                    else {                
                        $("#txtAccountNumberError").text("Name is already exist");
                        $("#txtAccountNumber").focus();               
                        $("#txtAccountNumber").addClass('errorStyle');
                    }
                },
                error: function() {
                    $('.close-confirm').click();
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                    $('#messagemyModal').modal();
                }
            }); // end of ajax
        });
    });
    
    $("#chkDefaultPayMode").on('change',function(){
        if ( $(this).is(":checked") ) {
            var ledger = $('#selLedger').val();
            checkDefaultPayMode(ledger,id);
        }
    });
    
} // end update button

function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete payment mode");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteSubType("' + type + '","'+id+'","","","","","","","","");');
} // end click fucntion

function  restoreClick(id,name,ledgerName,accountNumber,startingBalance,assetAccount,payeeName,soapUserName,soapPassword){ // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore payment mode");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteSubType("' + type + '","'+id+'","'+name+'","'+ledgerName+'","'+accountNumber+'","'+startingBalance+'","'+assetAccount+'","'+payeeName+'","'+soapUserName+'","'+soapPassword+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteSubType(type,id,name,ledgerName,accountNumber,startingBalance,assetAccount,payeeName,soapUserName,soapPassword) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/payment_mode.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Payment mode deleted successfully!!!");
                    $('#messagemyModal').modal();
                    showActiveRecords();
                } 
                else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This Payment mode is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/payment_mode.php",
            data: {
                "operation": "restore",
                "accountNumber":accountNumber,
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Payment mode restored successfully!!!");
                    $('#messagemyModal').modal();
                    showInActiveRecords();
                }
                else {
                    callSuccessPopUp('Sorry','Payment Mode already exist you can not restore !!!');
                }
            },
            error: function() {             
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary unavailable to respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}

function bindAssetAccount() {     // function for loading account number of asset type
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/payment_mode.php",
        data: {
            "operation": "showAssetAccount"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                $.each(parseData,function(i,v){
                    option += "<option value='" +v.id+ "'>" + v.account_name + "</option>";
                });
                
                $('#selAssetAccount').html(option);
            }
        },
        error: function() {}
    });
}

function showAddTab(){
    $("#advanced-wizard").show();
    $(".blackborder").hide();

    $("#tabAddPaymentMode").addClass('tab-detail-add');
    $("#tabAddPaymentMode").removeClass('tab-detail-remove');
    $("#tabPaymentModeList").removeClass('tab-list-add');
    $("#tabPaymentModeList").addClass('tab-list-remove');
    $("#paymentModeList").addClass('list');
    $("#txtName").focus();
}


function tabPaymentModeList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
    
    $("#tabAddPaymentMode").removeClass('tab-detail-add');
    $("#tabAddPaymentMode").addClass('tab-detail-remove');
    $("#tabPaymentModeList").removeClass('tab-list-remove');    
    $("#tabPaymentModeList").addClass('tab-list-add');
    $("#paymentModeList").addClass('list');
   
    $("#btnReset").show();
    $("#btnSubmit").show();
    $('#btnUpdate').hide();
    $('#tabPaymentModeList').html("+Add Payment Mode");
    $('#inactive-checkbox-tick').prop('checked', false).change();
    clear();
}

function clear() {
    $('#txtName').val('');
    $('#txtNameError').text('');
    $('#txtName').removeClass('errorStyle');

    $('#selLedger').val('');
    $('#selLedgerError').text('');
    $('#selLedger').removeClass('errorStyle');

    $('#txtAccountNumber').val('');
    $('#txtAccountNumberError').text('');
    $('#txtAccountNumber').removeClass('errorStyle');

    $('#txtStartingBalance').val('');
    $('#txtStartingBalanceError').text('');
    $('#txtStartingBalance').removeClass('errorStyle');

    $('#selAssetAccount').val('');
    $('#selAssetAccountError').text('');
    $('#selAssetAccount').removeClass('errorStyle');

    $('#txtPayeeName').val('');
    $('#txtPayeeNameError').text('');
    $('#txtPayeeName').removeClass('errorStyle');

    $('#txtSoapUserName').val('');
    $('#txtSoapUserNameError').text('');
    $('#txtSoapUserName').removeClass('errorStyle');

    $('#txtSoapPassword').val('');
    $('#txtSoapPasswordError').text('');
    $('#txtSoapPassword').removeClass('errorStyle');

    $('.chk-box').prop('checked',false);
    $('#txtName').focus();

    $("#radioAsSource,#radioAsSource").is(':checked',false);
    $("#radioAsBank,#radioAsBank").is(':checked',false);
}

function showActiveRecords(){
    if (paymentModeTable != undefined) {
        paymentModeTable.fnClearTable();
        paymentModeTable.fnDestroy();
        paymentModeTable = "";
    }    
    paymentModeTable = $('#paymentModeTable').dataTable({
        "bFilter": true,
        "processing": true,
        "deferLoading": 57,
        "sPaginationType": "full_numbers",
        "autoWidth": false,
        "aaSorting": [],
        aoColumnDefs: [{'bSortable': false,'aTargets': [6] }],
        aLengthMenu: [
            [25, 50, 100, 200, -1],
            [25, 50, 100, 200, "All"]
        ],
        iDisplayLength: -1            
    });
    var postData = {
        "operation": "show"
    }
    dataCall("controllers/admin/payment_mode.php", postData, function (result){
        var parseData = JSON.parse(result);
        var lastLedgerName = '';
        $.each(parseData,function(i,v){
            if (v.ledger_name !=lastLedgerName) {

                var currLedger = paymentModeTable.fnAddData(["<span class='details-control' onclick='showRows($(this));'></span>"+"<span class='hide-details-control hide' onclick='hideRows($(this));'></span>",v.ledger_name,'','','','','']);
                var getCurrLedgerRow = paymentModeTable.fnSettings().aoData[ currLedger ].nTr;
                /*$(getCurrLedgerRow).find('td').slice(2, 7).remove();*/
                /*var mergeRow = $(getCurrLedgerRow).find('td')[1];
                $(mergeRow).attr("colspan",6);*/


                var currData = paymentModeTable.fnAddData([v.name,v.ledger_name,v.account_no,
                    v.starting_balance,v.account_name,v.payee_name,"<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                    " style='font-size: 22px; cursor:pointer;' data-original-title='Edit' onclick ='updateRecord($(this));'></i>"+"<i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                    " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                    " data-original-title='Delete' onclick ='deleteRecord($(this));'></i>",v.soap_user_name,v.soap_password,v.is_receive_payment,v.is_make_payment,v.is_alnum_mode,
                    v.is_account_active,v.is_available_for_banking,v.ledger_type,v.asset_account_type,v.id,v.is_available_bank_source,v.is_default_payment_mode
                ]);
                var getCurrDataRow = paymentModeTable.fnSettings().aoData[ currData ].nTr;
                $(getCurrDataRow).addClass('hiddenn '+(v.ledger_name).replace(/ /g,''));
                lastLedgerName = v.ledger_name;
            }

            else {

                var currData = paymentModeTable.fnAddData([v.name,v.ledger_name,v.account_no,
                    v.starting_balance,v.account_name,v.payee_name,"<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                    " style='font-size: 22px; cursor:pointer;' data-original-title='Edit' onclick ='updateRecord($(this));'></i>"+"<i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                    " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                    " data-original-title='Delete' onclick ='deleteRecord($(this));'></i>",v.soap_user_name,v.soap_password,v.is_receive_payment,v.is_make_payment,v.is_alnum_mode,
                    v.is_account_active,v.is_available_for_banking,v.ledger_type,v.asset_account_type,v.id,v.is_available_bank_source,v.is_default_payment_mode
                ]);
                var getCurrDataRow = paymentModeTable.fnSettings().aoData[ currData ].nTr;
                $(getCurrDataRow).addClass('hiddenn '+(v.ledger_name).replace(/ /g,''));
                lastLedgerName = v.ledger_name;
            }
        });
    });
} 
function showInActiveRecords(){
   if (paymentModeTable != undefined) {
        paymentModeTable.fnClearTable();
        paymentModeTable.fnDestroy();
        paymentModeTable = "";
    }  
    paymentModeTable = $('#paymentModeTable').dataTable({
        "bFilter": true,
        "processing": true,
        "deferLoading": 57,
        "sPaginationType": "full_numbers",
        "autoWidth": false,
        "aaSorting": [],
        aoColumnDefs: [{'bSortable': false,'aTargets': [6] }],
        aLengthMenu: [
            [25, 50, 100, 200, -1],
            [25, 50, 100, 200, "All"]
        ],
        iDisplayLength: -1            
    });
    var postData = {
        "operation": "checked"
    }
    dataCall("controllers/admin/payment_mode.php", postData, function (result){
        var parseData = JSON.parse(result);
        var lastLedgerName = '';
        $.each(parseData,function(i,v){
            if (v.ledger_name !=lastLedgerName) {
                paymentModeTable.fnAddData(["<span class='details-control' onclick='showRows($(this));'></span>"+"<span class='hide-details-control hide' onclick='hideRows($(this));'></span>",v.ledger_name,'','','','','']);

                var currData = paymentModeTable.fnAddData([v.name,v.ledger_name,v.account_no,
                    v.starting_balance,v.account_name,v.payee_name,'<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"onclick ="restoreRecord($(this));""></i>',
                    v.soap_user_name,v.soap_password,v.is_receive_payment,v.is_make_payment,v.is_alnum_mode,
                    v.is_account_active,v.is_available_for_banking,v.ledger_type,v.asset_account_type,v.id
                ]);
                var getCurrDataRow = paymentModeTable.fnSettings().aoData[ currData ].nTr;
                $(getCurrDataRow).addClass('hide '+(v.ledger_name).replace(/ /g,''));
                lastLedgerName = v.ledger_name;
            }

            else {

                var currData = paymentModeTable.fnAddData([v.name,v.ledger_name,v.account_no,
                    v.starting_balance,v.account_name,v.payee_name,'<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore" onclick ="restoreRecord($(this));""></i>',
                    v.soap_user_name,v.soap_password,v.is_receive_payment,v.is_make_payment,v.is_alnum_mode,
                    v.is_account_active,v.is_available_for_banking,v.ledger_type,v.asset_account_type,v.id
                ]);
                var getCurrDataRow = paymentModeTable.fnSettings().aoData[ currData ].nTr;
                $(getCurrDataRow).addClass('hide '+(v.ledger_name).replace(/ /g,''));
                lastLedgerName = v.ledger_name;
            }
        });
    });
} 


function showRows(myThis){
    var ledgerText = $(myThis).parent().siblings()[0];
    var ledgerClass = $(ledgerText).text().replace(/ /g,'');
    var parentTr = $(myThis).parent().parent()[0];
    var trClass = $(parentTr).parent().find("."+ledgerClass);
    $(parentTr).parent().find("."+ledgerClass).fadeIn(700);
    $(myThis).addClass('hide');
    $(trClass).removeClass('hide');
    $(myThis).siblings().removeClass('hide');
}
function hideRows(myThis){
    var ledgerText = $(myThis).parent().siblings()[0];
    var ledgerClass = $(ledgerText).text().replace(/ /g,'');
    var parentTr = $(myThis).parent().parent()[0];
    var trClass = $(parentTr).parent().find("."+ledgerClass);
    $(parentTr).parent().find("."+ledgerClass).fadeOut();
    $(myThis).addClass('hide');
    $(myThis).siblings().removeClass('hide');
}
function restoreRecord(myThis){
    var data = $(myThis).parents('tr')[0];
    var mData = paymentModeTable.fnGetData(data);

    if (null != mData) // null if we clicked on title row
    {
        var id = mData[16];
        var name = mData[0];                      
        var ledgerName = mData[14];
        var accountNumber = mData[2];
        var startingBalance = mData[3];                      
        var assetAccount = mData[15];
        var payeeName = mData[5];
        var soapUserName = mData[7];                      
        var soapPassword = mData[8];
        var receivePayment = mData[9];
        var makePayment = mData[10];
        var alnumModeNo = mData[11];
        var accountActive = mData[12];
        var availableForbanking = mData[13];
        restoreClick(id,name,ledgerName,accountNumber,startingBalance,assetAccount,payeeName,soapUserName,soapPassword);

    }
}
function updateRecord(myThis){
    var data = $(myThis).parents('tr')[0];
    var mData = paymentModeTable.fnGetData(data);
    if (null != mData) // null if we clicked on title row
    {
        var id = mData[16];
        var name = mData[0];                      
        var ledgerName = mData[14];
        var accountNumber = mData[2];
        var startingBalance = mData[3];                      
        var assetAccount = mData[15];
        var payeeName = mData[5];
        var soapUserName = mData[7];                      
        var soapPassword = mData[8];
        var receivePayment = mData[9];
        var makePayment = mData[10];
        var alnumModeNo = mData[11];
        var accountActive = mData[12];
        var availableForbanking = mData[13];
        var is_available_bank_source = mData[17];
        var is_default_payment_mode = mData[18];
        editClick(id,name,ledgerName,accountNumber,startingBalance,assetAccount,payeeName,soapUserName,
            soapPassword,receivePayment,makePayment,alnumModeNo,accountActive,availableForbanking,is_available_bank_source,is_default_payment_mode
        );

    }
}
function deleteRecord(myThis){
    var data = $(myThis).parents('tr')[0];
    var mData = paymentModeTable.fnGetData(data);

    if (null != mData) // null if we clicked on title row
    {
        var id = mData[16];
        deleteClick(id);
    }
}
function checkDefaultPayMode(ledger,pkId){    
    var postData = {
        operation : "checkDeafaultPaymentMode",
        ledger : ledger,
        id : pkId
    }
    dataCall("./controllers/admin/payment_mode.php", postData, function (result) {
        if (result != '') {
            callSuccessPopUp('Alert','Already there is one payment mode for selected ledger i.e \''+result+'\' Do you want to change it?');
        }
    });
}