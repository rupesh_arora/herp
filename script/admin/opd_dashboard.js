$(document).ready(function() {
	debugger;	
	google.charts.setOnLoadCallback(drawChart);
});
function drawChart() {
	var chartWidth  = $("#page-content").width() -25;
	var opdBookingRecord = [['Record-Group', 'Count']];
	var visitsToDrRecord = [['Record-Group', 'Count']];

	var postData = {
		"operation" : "showChartDataForOPD"
	}
	$.ajax({     
		type: "POST",
		cache: false,
		url: "controllers/admin/patient_mangament_dashboard.php",
		data: postData,
		success: function(data) { 
			if(data != null && data != ""){
				var parseData =  JSON.parse(data);		
				
				opdBookingRecord = [['Record-Group', 'Count']];
				visitsToDrRecord = [['Record-Group', 'Count']];

				$.each(parseData,function(index,value){

					/*Check for null data*/
					if (parseData[index].length > 0) {

						for(var i=0;i<parseData[index].length;i++){
							if (index == 0) {
								smallArrayopdBooking = [];
								smallArrayopdBooking.push(parseData[index][i].Department);
								smallArrayopdBooking.push(parseInt(parseData[index][i].Visits));
								opdBookingRecord.push(smallArrayopdBooking);	
							}
							else {
								
								$.each(parseData[index][i],function(i,v){
									visitsToDrRecord.push([i,parseInt(v)]);
								});
							}
						}
					}												
				});

	            /*google chart intialization for patientRecord group visit*/
	            var opdBookingRecordGroup = google.visualization.arrayToDataTable(opdBookingRecord);

	            var visitsToDrRecordGroup = google.visualization.arrayToDataTable(visitsToDrRecord);

			    var options = {
			      title: 'Patient OPD Booking','width':1080,'height':600,is3D:true
			    };

			    var options2 = {
			      title: 'Patient Visited To Consultant','width':1080,'height':600,is3D:true
			    };

			    if (parseData[0].length <= 0) {
					$("#chart_div").hide();
				}
				else{
					var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
	        		chart.draw(opdBookingRecordGroup, options); 
				}
	        		 
				if (parseData[1].length <= 0) {
					$("#chart_div2").hide();
				}
				else{
					var chart2 = new google.visualization.PieChart(document.getElementById('chart_div_2'));
	        		chart2.draw(visitsToDrRecordGroup, options2);
				}		
			}
		}			
	});
}	