$(document).ready(function() {	
/* ****************************************************************************************************
 * File Name    :   patient_service.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page manages service request data
 *************************************************************************************************** */
	loader();
    var otable;
    var flag = 0;
	if($('.inactive-checkbox').not(':checked')){
		otable = $('#tblPatientService').dataTable( {
			"bFilter": true,
			"processing": true,
			"sPaginationType":"full_numbers",
			"aaSorting": [],
			"fnDrawCallback": function ( oSettings ) {		
				//This click function is used for process button for show data for proceed 
				$('.process').on('click',function(){
					clear();
					var data=$(this).parents('tr')[0];
					var mData = $('#tblPatientService').dataTable().fnGetData(data);
					if (null != mData)  // null if we clicked on title row
					{
						$('#myServiceModal').modal();
						if(mData["patient_id"] != "")
						{
							$('#txtPatientID').val(mData["patient_id"]);
						}
						$('#hdnServiceRequestId').val(mData["id"]);						
						$('#lblPatientName').text(mData["salutation"] + " " + mData["first_name"]+ " " + mData["middle_name"]+ " " + mData["last_name"]);
						if(mData["images_path"] != "")
						{							
							$('#imgPatient').attr("src",'./upload_images/patient_dp/'+mData["images_path"]);
						}
						else{
							$('#imgPatient').attr("src",'./img/default.jpg');
						}
						$('#lblSonOf').text(mData['middle_name']);
						if(mData["gender"] == "F"){
							$('#lblGender').text('Female');
						}
						else{							
							$('#lblGender').text('Male');
						}
						$('#lblDob').text(mData["dob"]);
						
						var dob= mData["dob"];
						var new_age = getAge(dob);
						var split = new_age.split(' ');
						var age_years = split[0];
						var age = parseInt(age_years)+1;
						$("#lblAge").text(age);						
												
						$('#lblEmail').text(mData["email"]);					
						$('#lblMobile').text(mData["mobile"]);									
						$('#lblServiceType').text(mData["type"]);	
						$('#lblService').text(mData["name"]);			
						clear();
					}
					else{
						clear();
					}
				});
				if(flag == 1){
					var aiRows = otable.fnGetNodes(); //Get all rows of data table

				 	if(aiRows.length == 0){
				 		return false;
				 	}
				 	for (var j=0,c=aiRows.length; j<c; j++) {
				 		var visitType = $(aiRows[j]).find('td:last').find('input').attr('visitType');
				 		if(visitType == "Emergency"){
				 			$(aiRows[j]).css('background-color','#ff6666');
				 		}
				 	}
				}
			},
			
			"sAjaxSource":"controllers/admin/patient_service.php",
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "operation", "value": "show" });
			},
			"aoColumns": [
				{  
					"mData": function (o) { 	
					var visitId = o["visit_id"];
					var patientPrefix = o["visit_prefix"];
						
					var visitIdLength = visitId.length;
					for (var i=0;i<6-visitIdLength;i++) {
						visitId = "0"+visitId;
					}
					visitId = patientPrefix+visitId;
					return visitId; 
					}
				},
				{  
					"mData": function (o) { 	
					var patientId = o["patient_id"];
					var patientPrefix = o["patient_prefix"];
						
					var patientIdLength = patientId.length;
					for (var i=0;i<6-patientIdLength;i++) {
						patientId = "0"+patientId;
					}
					patientId = patientPrefix+patientId;
					return patientId; 
					}
				},
				/* { "mData": "visit_id" },
				{ "mData": "patient_id" }, */
				{ "mData": "code" },
				{ "mData": "name" },
				{ "mData": "cost" },
				{ "mData": "pay_status" },
				{  
					"mData": function (o) { 					
						flag = 1;
						var visitType = o["visitType"];
						if(visitType == "Emergency Case"){					
							return "<input type='button' class='btn btn-small btn-primary process' visitType="+visitType+"  data-val="+o["id"]+" value='Process' />"; 
						}
						else{
							return "<input type='button' class='btn btn-small btn-primary process' data-val="+o["id"]+" value='Process' />"; 
						}
					}
				},	
			],
			aoColumnDefs: [
			   { 'bSortable': false, 'aTargets': [ 6 ] }
		   ]
		} );
	}
	$('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	otable.fnClearTable();
	    	otable.fnDestroy();
			otable = $('#tblPatientService').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {

					if(flag == 1){
						var aiRows = otable.fnGetNodes(); //Get all rows of data table

					 	if(aiRows.length == 0){
					 		return false;
					 	}
					 	for (var j=0,c=aiRows.length; j<c; j++) {
					 		var visitType = $(aiRows[j]).find('td:last').find('input').attr('visitType');
					 		if(visitType == "Emergency"){
					 			$(aiRows[j]).css('background-color','#ff6666');
					 		}
					 	}
					}
				},
				
				"sAjaxSource":"controllers/admin/patient_service.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showinactive" });
				},
				"aoColumns": [
					{  
						"mData": function (o) { 	
						var visitId = o["visit_id"];
						var patientPrefix = o["visit_prefix"];
							
						var visitIdLength = visitId.length;
						for (var i=0;i<6-visitIdLength;i++) {
							visitId = "0"+visitId;
						}
						visitId = patientPrefix+visitId;
						return visitId; 
						}
					},
					{  
						"mData": function (o) { 	
						var patientId = o["patient_id"];
						var patientPrefix = o["patient_prefix"];
							
						var patientIdLength = patientId.length;
						for (var i=0;i<6-patientIdLength;i++) {
							patientId = "0"+patientId;
						}
						patientId = patientPrefix+patientId;
						return patientId; 
						}
					},
					/* { "mData": "visit_id" },
					{ "mData": "patient_id" }, */
					{ "mData": "code" },
					{ "mData": "name" },
					{ "mData": "cost" },
					{ "mData": "pay_status" },
					{  
						"mData": function (o) { 					
							return "processed"; 
						}
					},	
				],
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 6 ] }
			   ]
			} );
		}
		else{
			otable.fnClearTable();
	    	otable.fnDestroy();
			otable = $('#tblPatientService').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {		
					//This click function is used for process button for show data for proceed 
					$('.process').on('click',function(){
						clear();
						var data=$(this).parents('tr')[0];
						var mData = $('#tblPatientService').dataTable().fnGetData(data);
						if (null != mData)  // null if we clicked on title row
						{
							$('#myServiceModal').modal();
							if(mData["patient_id"] != "")
							{
								$('#txtPatientID').val(mData["patient_id"]);
							}
							$('#hdnServiceRequestId').val(mData["id"]);						
							$('#lblPatientName').text(mData["salutation"] + " " + mData["first_name"]+ " " + mData["last_name"]);
							if(mData["images_path"] != "")
							{							
								$('#imgPatient').attr("src",'./upload_images/patient_dp/'+mData["images_path"]);
							}
							else{
								$('#imgPatient').attr("src",'./img/default.jpg');
							}
							$('#lblSonOf').text(mData['middle_name']);
							if(mData["gender"] == "F"){
								$('#lblGender').text('Female');
							}
							else{							
								$('#lblGender').text('Male');
							}
							$('#lblDob').text(mData["dob"]);
							
							var dob= mData["dob"];
							var new_age = getAge(dob);
							var split = new_age.split(' ');
							var age_years = split[0];
							var age = parseInt(age_years)+1;
							$("#lblAge").text(age);						
													
							$('#lblEmail').text(mData["email"]);					
							$('#lblMobile').text(mData["mobile"]);									
							$('#lblServiceType').text(mData["type"]);	
							$('#lblService').text(mData["name"]);			
							clear();
						}
						else{
							clear();
						}
					});

					if(flag == 1){
						var aiRows = otable.fnGetNodes(); //Get all rows of data table

					 	if(aiRows.length == 0){
					 		return false;
					 	}
					 	for (var j=0,c=aiRows.length; j<c; j++) {
					 		var visitType = $(aiRows[j]).find('td:last').find('input').attr('visitType');
					 		if(visitType == "Emergency"){
					 			$(aiRows[j]).css('background-color','#ff6666');
					 		}
					 	}
					}
				},
				
				"sAjaxSource":"controllers/admin/patient_service.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "show" });
				},
				"aoColumns": [
					{  
						"mData": function (o) { 	
						var visitId = o["visit_id"];
						var patientPrefix = o["visit_prefix"];
							
						var visitIdLength = visitId.length;
						for (var i=0;i<6-visitIdLength;i++) {
							visitId = "0"+visitId;
						}
						visitId = patientPrefix+visitId;
						return visitId; 
						}
					},
					{  
						"mData": function (o) { 	
						var patientId = o["patient_id"];
						var patientPrefix = o["patient_prefix"];
							
						var patientIdLength = patientId.length;
						for (var i=0;i<6-patientIdLength;i++) {
							patientId = "0"+patientId;
						}
						patientId = patientPrefix+patientId;
						return patientId; 
						}
					},
					/* { "mData": "visit_id" },
					{ "mData": "patient_id" }, */
					{ "mData": "code" },
					{ "mData": "name" },
					{ "mData": "cost" },
					{ "mData": "pay_status" },
					{  
						"mData": function (o) { 
							flag = 1;
							var visitType = o["visitType"];
							if(visitType == "Emergency Case"){					
								return "<input type='button' class='btn btn-small btn-primary process' visitType="+visitType+"  data-val="+o["id"]+" value='Process' />"; 
							}
							else{
								return "<input type='button' class='btn btn-small btn-primary process' data-val="+o["id"]+" value='Process' />"; 
							}
						}
					},	
				],
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 6 ] }
			   ]
			} );
		}
	});
   
	//Click function for proceed button 
	$("#btnProceed").click(function(){
		var ServiceId = $('#hdnServiceRequestId').val();
		var postData = {
				"operation":"update",
				"ServiceId":ServiceId
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/patient_service.php",
			datatype:"json",
			data: postData,
			success: function(data) {
				if(data != "0" && data != ""){
					$('#pop_up_close').click()
					$('#messagemyModal #messageMyModalLabel').text("Success");
					$('#messagemyModal .modal-body').html('Patient service processed successfully!!!');
					$("#tblPatientService").dataTable().fnReloadAjax();						
					$('#messagemyModal').modal();
				}
			},
			error: function(){
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();				
			}
		});
	});
	
});
//function to calculate age
function getAge(dateString) {
    var now = new Date();
    var today = new Date(now.getYear(), now.getMonth(), now.getDate());

    var yearNow = now.getYear();
    var monthNow = now.getMonth();
    var dateNow = now.getDate();

    var dob = new Date(dateString.substring(0, 4), dateString.substring(5, 7) - 1, dateString.substring(8, 10));

    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();
    var age = {};
    var ageString = "";
    var yearString = "";
    var monthString = "";
    var dayString = "";


    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    age = {
        years: yearAge,
        months: monthAge,
        days: dateAge
    };

    if (age.years > 1) yearString = " years";
    else yearString = " year";
    if (age.months > 1) monthString = " months";
    else monthString = " month";
    if (age.days > 1) dayString = " days";
    else dayString = " day";


    if ((age.years > 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else ageString = "Oops! Could not calculate age!";

    return ageString;
}

function clear(){
	$.each($('label'),function(index,value) {		
		if($(this).text() == "")
		{
			$(this).text("Not Available");
		}
	});
}

//# sourceURL=filename.js