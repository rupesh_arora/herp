var serviceTable; //defile variable for serviceTable 
$(document).ready(function() {
debugger;
loader();

	$('#reset').click(function(){
		clear();
		$("#txtService").focus();
	});
// show the add services
    $("#form_services").hide();
    $("#services_list").addClass('list');    
    $("#tab_services").addClass('tab-list-add');
	bindGLAccount("#selGLAccount",'');
	bindAccountReceivable("#selAccountPayable");
	bindLedger("#selLedger");//defined in common.js
	
    $("#tab_add_services").click(function() {
    	showAddTab();        
       	clear();
    });
// show the add services list
    $("#tab_services").click(function() {
    	showTableList();
		
	});
	
// Ajax call for show data on Service Type
	$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/services.php",
			data: {
			"operation":"showtype"
			},
			success: function(data) { 
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data); // parse the value in Array string  jquery

				var option ="<option value=''>--Select--</option>";
				for (var i=0;i<parseData.length;i++)
				{
					option+="<option value='"+parseData[i].id+"'>"+parseData[i].type+"</option>";
				}
				$('#txtService').html(option);          
				}
			},
			error:function(){
			}
		});
	// check the validation
    $("#btnAddServices").click(function() {
		
		var flag = "false";
		if ($("#selGLAccount").val() == "") {
			$('#selGLAccount').focus();
            $("#selGLAccountError").text("Please select gl account");
			$("#selGLAccount").addClass("errorStyle");
            flag = "true";
        }
		if ($("#selAccountPayable").val() == "") {
			$('#selAccountPayable').focus();
            $("#selAccountPayableError").text("Please select account payable");
			$("#selAccountPayable").addClass("errorStyle");
            flag = "true";
        }
        if ($("#selLedger").val() == "") {
			$('#selLedger').focus();
            $("#selLedgerError").text("Please select ledger");
			$("#selLedger").addClass("errorStyle");
            flag = "true";
        }
		if ($("#txtUnitCost").val().trim() == '') {
            $("#txtUnitCostError").text("Please enter unit cost");
			$("#txtUnitCost").focus();
            $("#txtUnitCost").addClass("errorStyle");
            flag = "true";
        }
		else if(!validnumber($("#txtUnitCost").val())){
			$("#txtUnitCostError").text("Please enter number only");
			$("#txtUnitCost").focus();
            $("#txtUnitCost").addClass("errorStyle");
            flag = "true";
		}
		if ($("#txtName").val().trim() == '') {
            $("#txtNameError").text("Please enter disease name");
			$("#txtName").focus();
            $("#txtName").addClass("errorStyle");
            flag = "true";
        }
		if ($("#txtCode").val().trim() == '') {
            $("#txtCodeError").text("Please enter code");
			$("#txtCode").focus();
            $("#txtCode").addClass("errorStyle");
            flag = "true";
        }
		 if ($("#txtService").val() == '') {
            $("#txtServiceError").text("Please select service type");
			$("#txtService").focus();
            $("#txtService").addClass("errorStyle");
            flag = "true";
        }
		if (flag == "true")
		{
			return false;
		}
		else
		{
			var ledger = $("#selLedger").val();
			var glAccount = $("#selGLAccount").val();
			var accountPayable = $("#selAccountPayable").val();
			var servicetype=$("#txtService").val();
			var code = $("#txtCode").val().trim();
			var name = $("#txtName").val().trim();
			var unitCost = $("#txtUnitCost").val().trim();
			var desc = $("#txtDesc").val();
			desc = desc.replace(/'/g, "&#39");
			$.ajax({									// ajax call for disease duplicacy validation
				type: "POST",
				cache: false,
				url: "controllers/admin/services.php",
				datatype:"json",  
				data:{
					Servicetype:servicetype,
					Code:code,
					name:name,
					UnitCost:unitCost,
					Desc:desc,
					Id:"",
					operation:"checkservices"
				},
				success:function(data) {
					if(data == "1")
					{
						  $("#txtCodeError").text("Code is already exists");
						  $("#txtCode").addClass("errorStyle");
						  $("#txtCode").focus();
					}
					else{
						//ajax call for insert data into data base
						var postData = {
							"operation":"save",
							"Servicetype":servicetype, 
							"Code":code,
							"name":name,
							"ledger":ledger,
							"glAccount":glAccount,
							"accountPayable":accountPayable,
							"Name":name,
							"UnitCost":unitCost,
							"Desc":desc,
						}
						$.ajax(
							{					
							type: "POST",
							cache: false,
							url: "controllers/admin/services.php",
							datatype:"json",
							data: postData,

							success: function(data) {
								if(data != "0" && data != ""){
									$('#messageMyModalLabel').text("Success");
									$('.modal-body').text("Service saved successfully!!!");
									$('#messagemyModal').modal();
									$('#inactive-checkbox-tick').prop('checked', false).change();
									clear();
									showTableList();
								}
							},
							error:function() {
								$('#messageMyModalLabel').text("Error");
								$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
								$('#messagemyModal').modal();
							}
						});//  end ajax call
					}
				},
				error:function(){
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("data not found!!!");
					$('#messagemyModal').modal();
				}
			});
		}
   }); //end button click function
   
// key up validation
	$("select").on('change', function() {
        if ($("#txtService").val() != "") {
            $("#txtServiceError").text("");
            $("#txtService").removeClass("errorStyle");
        }
    });
    $("#txtCode").keyup(function() {
        if ($("#txtCode").val() != '') {
            $("#txtCodeError").text("");
            $("#txtCode").removeClass("errorStyle");
        } 
    }); 
	 $("#txtName").keyup(function() {
        if ($("#txtName").val() != '') {
            $("#txtNameError").text("");
            $("#txtName").removeClass("errorStyle");
        } 
    }); 
	$("#txtUnitCost").keyup(function() {
        if ($("#txtUnitCost").val() != '') {
            $("#txtUnitCostError").text("");
            $("#txtUnitCost").removeClass("errorStyle");
        } 
    }); 	
    $("#selLedger").change(function() {
        if ($("#selLedger").val() != "") {
            $("#selLedgerError").text("");
            $("#selLedger").removeClass("errorStyle");
        }
    });
	$("#selAccountPayable").change(function() {
        if ($("#selAccountPayable").val() != "") {
            $("#selAccountPayableError").text("");
            $("#selAccountPayable").removeClass("errorStyle");
        }
    });
    $("#selGLAccount").change(function() {
        if ($("#selLedger").val() != "") {
            $("#selGLAccountError").text("");
            $("#selGLAccount").removeClass("errorStyle");
        }
    });
	
	if($('.inactive-checkbox').not(':checked')){
		//Datatable code
		 loadDataTable();
	}
	$('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	serviceTable.fnClearTable();
	    	serviceTable.fnDestroy();
	    	serviceTable="";
	    	serviceTable = $('#tblServices').dataTable( {
				"bFilter": true,
				"processing": true,
				 "deferLoading": 57,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {	
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  serviceTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var servicetype=mData["service_type_id"];
							restoreClick(id,servicetype);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/services.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "checked" });
				},
				"aoColumns": [
					{  "mData": "code" },
					{  "mData": "type" },
					{  "mData": "name" },
					{  "mData": "cost" },
					{  "mData": "description" },	
					{  
						"mData": function (o) { 
							var data = o;
							return "<i class='ui-tooltip fa fa-pencil-square-o restore' title='Edit'"+
								   " style='font-size: 22px; cursor:pointer;' data-original-title='restore'></i>"; 
						}
					},
				],
				aoColumnDefs: [
					{ 'bSortable': false, 'aTargets': [ 5 ] },
					{ 'bSortable': false, 'aTargets': [ 4 ] }
				]
			});
		}
		else{
			serviceTable.fnClearTable();
	    	serviceTable.fnDestroy();
	    	serviceTable="";
			loadDataTable();
		}
    });
});

// edit data dunction for update 
function editClick(id,serviceType,code,name,cost,desc,ledger_id,gl_account_id,account_payable_id){	
	showAddTab();
	$("#uploadFile").hide();
    $("#reset").hide();
    $("#btnAddServices").hide();
    $('#btnUpdate').show();
    $('#tab_add_services').html("+Update Service");
    $("#txtService").focus();
	
	$("#txtCode").removeClass("errorStyle");
	$("#txtCodeError").text("");
	$("#txtName").removeClass("errorStyle");
	$("#txtNameError").text("");
	$("#txtUnitCost").removeClass("errorStyle");
	$("#txtUnitCostError").text("");
	$('#txtService').val(serviceType);
	$('#txtCode').val(code);
	$('#selLedger').val(ledger_id);
	$('#selAccountPayable').val(account_payable_id);
	$('#selGLAccount').val(gl_account_id);
	$('#txtName').val(name);
	$('#txtUnitCost').val(cost);
	$(' #txtDesc').val(desc.replace(/&#39/g, "'"));
	$('#selectedRow').val(id); 
	    
  
	//validation
	$(" #btnUpdate").click(function(){ // click update button
		var flag = "false";
		if ($("#selGLAccount").val() == "") {
			$('#selGLAccount').focus();
            $("#selGLAccountError").text("Please select gl account");
			$("#selGLAccount").addClass("errorStyle");
            flag = "true";
        }
		if ($("#selAccountPayable").val() == "") {
			$('#selAccountPayable').focus();
            $("#selAccountPayableError").text("Please select account payable");
			$("#selAccountPayable").addClass("errorStyle");
            flag = "true";
        }
        if ($("#selLedger").val() == "") {
			$('#selLedger').focus();
            $("#selLedgerError").text("Please select ledger");
			$("#selLedger").addClass("errorStyle");
            flag = "true";
        }
		if ($("#txtUnitCost").val().trim() == '') {
            $(" #txtUnitCostError").text("Please enter unit cost");
			$("#txtUnitCost").focus();
            $("#myModal #txtUnitCost").addClass("errorStyle");
            flag = "true";
        }
		else if(!validnumber($("#txtUnitCost").val())){
			$("#txtUnitCostError").text("Please enter number only");
			$("#txtUnitCost").focus();
            $("#txtUnitCost").addClass("errorStyle");
            flag = "true";
		}
		if ($("#txtName").val().trim() == '') {
            $("#txtNameError").text("Please enter disease name");
			$("#txtName").focus();
            $("#txtName").addClass("errorStyle");
            flag = "true";
        }
		if ($("#txtCode").val().trim() == '') {
            $("#txtCodeError").text("Please enter code");
			$("#txtCode").focus();
            $("#txtCode").addClass("errorStyle");
            flag = "true";
        }
		if ($("#txtService").val().trim() == '') {
            $("#txtServiceError").text("Please select service type");
			$("#txtService").focus();
            $("#txtService").addClass("errorStyle");
            flag = "true";
        }
	
		if (flag == "true")
		{		
			return false;			
		}
		else
		{
			var id = $('#selectedRow').val();	
			var ledger = $("#selLedger").val();
			var glAccount = $("#selGLAccount").val();
			var accountPayable = $("#selAccountPayable").val();
			var serviceType = $('#txtService').val().trim();
			var code = $("#txtCode").val().trim();
			var name = $("#txtName").val().trim();
			var unitCost = $("#txtUnitCost").val().trim();
			var Desc=$('#txtDesc').val().trim();
			Desc = Desc.replace(/'/g, "&#39");			
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){				
				$.ajax({								
					type: "POST",
					cache: false,
					url: "controllers/admin/services.php",
					datatype:"json",  
					data:{
						Code:code,
						Id:id,	
						operation:"checkservices"
					},
					success:function(data) {
						if(data == "1")
						{
							  $("#txtCodeError").text("Code is already exists");
							  $("#txtCode").addClass("errorStyle");
							  $("#txtCode").focus();
						}
						else{
							var postData = {
								"operation":"update",
								"ServiceType":serviceType,
								"Code":code,
								"ledger":ledger,
								"glAccount":glAccount,
								"accountPayable":accountPayable,
								"name":name,
								"UnitCost":unitCost,
								"desc":Desc,
								"Id":id
							}
							$.ajax( //ajax call for update data
							{					
								type: "POST",
								cache: false,
								url: "controllers/admin/services.php",
								data: postData,

								success: function(data) {
									if(data != "0" && data != ""){
										$('#myModal').modal('hide');
										$('.close-confirm').click();
										$('.modal-body').text("");
										$('#messageMyModalLabel').text("Success");
										$('.modal-body').text("Service updated successfully!!!");
										$('#messagemyModal').modal();
										serviceTable.fnReloadAjax();
										showTableList();
										clear();
									}
								},
								error:function() {
									$('.close-confirm').click();
									$('.modal-body').text("");
									$('#messageMyModalLabel').text("Error");
									$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
									$('#messagemyModal').modal();
								}
							});// end of ajax
						}
					},
					error:function(){
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Sorry");
						$('.modal-body').text("data not found!!!");
						$('#messagemyModal').modal();
					}
				});
			});
		}	
	});
}// end update button
function deleteClick(id){ // delete click function
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete Service");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	$('#selectedRow').val(id); 
	var type="delete";
	$('#confirm').attr('onclick','deleteServices("'+type+'");');
}// end click fucntion
function restoreClick(id,serviceType){
	$('.modal-body').text("");
	$('#selectedRow').val(id);
	$('#confirmMyModalLabel').text("Restore Service");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal(); 
	var type="restore";
	$('#confirm').attr('onclick','deleteServices("'+type+'",'+serviceType+');');
}
// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		 /* window.location.href = "http://localhost/herp/"; */
		 $('.close').click();
    }
});

function clear(){
	$('#txtService').val("");
	$("#txtServiceError").text("");
    $("#txtService").removeClass("errorStyle");
	$('#txtCode').val("");
	$('#txtCodeError').text("");
	$("#txtCode").removeClass("errorStyle");
	$('#txtName').val("");
	$('#txtNameError').text("");
	$("#txtName").removeClass("errorStyle");
	$('#txtUnitCost').val("");
	$('#txtUnitCostError').text("");
	$("#txtUnitCost").removeClass("errorStyle");
	$('#txtStandardFee').val("");
	$('#selLedger').val("");
	$('#selGLAccount').val("");
	$('#selAccountPayable').val("");
	$('#selLedgerError').text("");
	$('#selAccountPayableError').text("");
	$('#selGLAccountError').text("");
	$("#selAccountPayable").removeClass("errorStyle");
	$("#selLedger").removeClass("errorStyle");
	$("#selGLAccount").removeClass("errorStyle");
	/* $('#txtStandardFeeError').text("");
	$("#txtStandardFee").removeClass("errorStyle"); */
	$('#txtDesc').val("");
}
// function for check validation for numeric and float value
 function validnumber(number) {
		floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
        return floatRegex.test(number);
    }; 
function deleteServices(type,serviceType){
		if(type=="delete"){
			var id = $('#selectedRow').val();
			var postData = {
				"operation":"delete",
				"id":id
			}
			$.ajax( // ajax call for delete
				{					
				type: "POST",
				cache: false,
				url: "controllers/admin/services.php",
				datatype:"json",
				data: postData,

				success: function(data) {
					if(data != "0" && data != ""){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Service deleted successfully!!!");
						$('#messagemyModal').modal();
						serviceTable.fnReloadAjax();
					}
				},
				error:function() {
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
			}); // end ajax 
		}
		else{
			var id = $('#selectedRow').val();
			$.ajax({
				type: "POST",
				cache: "false",
				url: "controllers/admin/services.php",
				data :{            
					"operation" : "restore",
					"id" : id,
					ServiceType:serviceType
				},
				success: function(data) {	
					if(data != "0" && data != ""){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Service restored successfully!!!");
						$('#messagemyModal').modal();
						serviceTable.fnReloadAjax();
											
					}	
					if(data=="0"){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Sorry");
						$('.modal-body').text("It can not be restore!!!");
						serviceTable.fnReloadAjax();
						$('#messagemyModal').modal();
						
					}				
				},
				error:function()
				{
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();					  
				}
			});
		}
	}	

function loadDataTable(){
	serviceTable = $('#tblServices').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {	
			$('.update').unbind();
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = serviceTable.fnGetData(data);
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					var servicetype=mData["service_type_id"];
					var code = mData["code"];
					var name = mData["name"];
					var cost = mData["cost"];
					var ledger_id = mData["ledger_id"];
					var gl_account_id = mData["gl_account_id"];
					var account_payable_id = mData["account_payable_id"];
					var desc = mData["description"];
					editClick(id,servicetype,code,name,cost,desc,ledger_id,gl_account_id,account_payable_id);   
				}
			});
			$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  serviceTable.fnGetData(data);
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);
				}
			});
		},
		"sAjaxSource":"controllers/admin/services.php",
		"fnServerParams": function ( aoData ) {
			aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{  "mData": "code" },
			{  "mData": "type" },
			{  "mData": "name" },
			{  "mData": "cost" },
			{  "mData": "description" },	
			{  
				"mData": function (o) { 
					var data = o;
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
						   " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
						   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
						   " style='font-size: 22px; color:#a94442; cursor:pointer;' "+
						   " data-original-title='Delete'></i>"; 
				}
			},
		],
		aoColumnDefs: [
			{ 'bSortable': false, 'aTargets': [ 5 ] },
			{ 'bSortable': false, 'aTargets': [ 4 ] }
		]

	} );
}

function showAddTab(){
	$("#form_services").show();
    $(".blackborder").hide();

    $("#tab_add_services").addClass('tab-detail-add');
    $("#tab_add_services").removeClass('tab-detail-remove');
    $("#tab_services").removeClass('tab-list-add');
    $("#tab_services").addClass('tab-list-remove');
    $("#services_list").addClass('list');
	$("#txtService").focus();

}

function showTableList(){
	
	$('#inactive-checkbox-tick').prop('checked', false).change();
    $("#form_services").hide();
    $(".blackborder").show();

    $("#tab_add_services").removeClass('tab-detail-add');
    $("#tab_add_services").addClass('tab-detail-remove');
    $("#tab_services").removeClass('tab-list-remove');    
    $("#tab_services").addClass('tab-list-add');
    $("#services_list").addClass('list');


    $("#uploadFile").show();
    $("#reset").show();
    $("#btnAddServices").show();
    $('#btnUpdate').hide();
    $('#tab_add_services').html("+Add Service");
    clear();
}
//# sourceURL=services.js