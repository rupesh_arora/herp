/* ****************************************************************************************************
 * File Name    :   insurance_payment.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Arora
 * Created Date :   2nd ,Apr 2015
 * Description  :   This page  manages lab test in consultant module
 *************************************************************************************************** */
var insuranceServiceCost;
$(document).ready(function() {
	debugger;
    $("#amountPrefixCopayValue").text(amountPrefix);
    $("#totalCost").text(amountPrefix+" 0");
    removeErrorMessage();//calling from common.js
    $('#txtVisit').focus();
	//$(".amountPrefix").text(amountPrefix);
	insuranceServiceCost = $('#insuranceServiceCost').DataTable({
        "bPaginate": false,
        "bAutoWidth": false
    });

    $("#searchVisitIcon").click(function(){	
    	$('#myCashModal').modal();
    	$('#cashModalBody').load('views/admin/search_visit_cash_payment.html',function(){
			$("#cashModalBody #txtFirstName").focus();
            $("#cashModalBody .hideAtInsurance").hide();
		});
    });

    $('#ok').click(function() {
        $('#txtVisit').focus();
    });
    $('#txtVisit').keyup(function() {
        if($('#txtVisit').val() != '') {
            $('#txtVisitError').text('');
        }
    });
    $('#searchVisitIcon').click(function() {
        $('#txtVisitError').text('');
        $('#txtVisit').removeClass('errorStyle');
    });
    $("#btnSelect").click(function(){
        $('#amountPrefixCopayValue').text(amountPrefix);
        var visitId = $("#txtVisit").val().trim();
        if (visitId !='') {
            if (visitId.length != 9) {
                $("#txtVisitError").text("Please enter valid patient Id");
                $("#txtVisit").addClass('errorStyle');
                $("#txtVisit").focus();
                return false;
            }
            var getVisitPrefix = visitId.substring(0, 3);
            if (getVisitPrefix.toLowerCase() != visitPrefix.toLowerCase()) {
                $("#txtVisitError").text("Please enter valid patient Id");
                $("#txtVisit").addClass('errorStyle');
                $("#txtVisit").focus();
                return false;
            }
            visitId = visitId.replace ( /[^\d.]/g, '' ); 
            visitId = parseInt(visitId);
        }
        var postData = {
            operation : 'checkVisitId',
            visitId : visitId
        }
        dataCall("controllers/admin/insurance_payment.php", postData, function (result){
            if (result.trim() == "1") {
                insuranceNumber(visitId);
                copayDetails(visitId);
            }
            else{
                $("#txtVisit").focus();
                $("#txtVisit").addClass("errorStyle");
                $("#txtVisitError").text("Please enter patient Id");
            }
        });
    	
    });

    $("#btnSaveInsurancePayment").click(function(){

        var flag = false;
        if(validTextField('#txtVisit','#txtVisitError','Please enter visit id') == true)
        {
            flag = true;
        }
        if(flag == true) {
            return false;
        }
    	var visitId = parseInt($("#txtVisit").val().replace ( /[^\d.]/g, '' ));
    	var patientId = parseInt($("#txtPatientId").val().replace ( /[^\d.]/g, '' ));
    	var insuranceNo = $("#txtInsuranceNo").val();
    	var insuranceCompanyId = $("#txtInsuranceCompany").attr("valueId");
    	var insurancePlanId = $("#txtInsuranceScheme").attr("valueId");
    	var insuranceSchemeId  = $("#txtInsurancePlan").attr("valueId");
        
    	if ($('.showOnClickLoad').attr('style') !="display: block;") {
    		callSuccessPopUp("Alert","No insurance details exist for this visit id.!!!");
    		return false;
    	}
        if ($("#txtCopayType").val().toLowerCase() == "none") {
            var bill = parseFloat($("#totalCost").text().split(' ')[1]);
        }
        else if ($("#txtCopayType").val().toLowerCase() == "fixed") {
            var bill = $("#txtCopayValue").val();
        }
        else if ($("#txtCopayType").val().toLowerCase() == "percentage") {
            var bill = parseFloat($("#totalCost").text().split(' ')[1]) - (parseFloat(($("#totalCost").text().split(' ')[1]))*$("#txtCopayValue").val())/100;
        }


        if (bill < 0) {
            callSuccessPopUp("Alert","Bill after reduction of copay value can't be negative!!!");
            return false;
        }

        if ($("#checkCopayValue").text() != $("#txtCopayValue").val()) {
            callSuccessPopUp("Alert","Bill has been changed!!!");
            return false;
        }
        if ($("#checkTotalBill").text() != parseFloat($("#totalCost").text().split(' ')[1])) {
            callSuccessPopUp("Alert","Bill has been changed!!!");
            return false;
        }
        var serviceArray = [];
        var forensicArray = [];
        var procedureArray = [];
        var labArray = [];
        var medicineArray = [];
        var pharmacyArray = [];
        var radiologyArray = [];
        $.each(insuranceServiceCost.fnGetData(),function(i,v){

            if(insuranceServiceCost.fnGetData()[i][4] == "forensic") {
                forensicArray.push(insuranceServiceCost.fnGetData()[i][3]);
            }
            else if(insuranceServiceCost.fnGetData()[i][4] == "radiology") {
                radiologyArray.push(insuranceServiceCost.fnGetData()[i][3]);
            }
            else if(insuranceServiceCost.fnGetData()[i][4] == "service") {
                serviceArray.push(insuranceServiceCost.fnGetData()[i][3]);
            }
            else if(insuranceServiceCost.fnGetData()[i][4] == "procedure") {
                procedureArray.push(insuranceServiceCost.fnGetData()[i][3]);
            }
            else if(insuranceServiceCost.fnGetData()[i][4] == "Lab") {
                labArray.push(insuranceServiceCost.fnGetData()[i][3]);
            }
            else if(insuranceServiceCost.fnGetData()[i][4] == "Medicine") {
                medicineArray.push(insuranceServiceCost.fnGetData()[i][3]);
            }
        });

        
    	var postData = {
    		"operation" :  "saveInsurancePaymentdetails",
    		"visitId" :visitId,
    		"patientId" : patientId,
    		insuranceNo : insuranceNo,
    		insuranceCompanyId : insuranceCompanyId,
    		insuranceSchemeId : insuranceSchemeId,
    		insurancePlanId : insurancePlanId,
    		date : todayDate(),
    		bill : bill,
            radiologyArray : JSON.stringify(radiologyArray),
            forensicArray : JSON.stringify(forensicArray),
            serviceArray : JSON.stringify(serviceArray),
            procedureArray : JSON.stringify(procedureArray),
            labArray : JSON.stringify(labArray),
            medicineArray : JSON.stringify(medicineArray)
    	};
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to save this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
        	dataCall("controllers/admin/insurance_payment.php", postData, function (result){
        		if (result.trim() !='' && result.trim() !=null) {
        			callSuccessPopUp("Saved","Details save successfully!!!");
                    clear();
        		}
        	});
        });
    });
    $('#btnResetInsurancePayment').click(function() {
        clear();
    });
});
function clear() {
    $('#txtVisit').val('');
    $('#txtVisit').focus();
    $('#txtVisit').removeClass('errorStyle');
    $('#txtVisitError').text('');
    clearFormDetails(".clearForm");
    $('.showOnClickLoad').hide();
    insuranceServiceCost.fnClearTable();
    $('#totalCost').text("0");
}

function insuranceNumber(visitId){
    var postData = {
        "operation": "payment_mode",
        "visitId": visitId
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        var insuranceNumber ='';
        if (result.length > 2) {
        	insuranceNumber = parseData[0].insurance_no;
        	getInsuranceDetails(insuranceNumber);
        	$("#txtInsuranceNo").val(insuranceNumber);
        }           
    });
}
function getInsuranceDetails(insuranceNumber){
	var postData = {
        "operation": "insuranceDetails",
        "insuranceNumber": insuranceNumber
    }
    dataCall("controllers/admin/insurance_payment.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (result.length > 2) {
        	$("#txtInsuranceCompany").val(parseData[0].insurance_company_name);
        	$("#txtInsuranceCompany").attr("valueId",parseData[0].insurance_company_id);

        	$("#txtInsuranceScheme").val(parseData[0].insurance_paln_name);
        	$("#txtInsuranceScheme").attr("valueId",parseData[0].insurance_id);

        	$("#txtInsurancePlan").val(parseData[0].scheme_plan_name);
        	$("#txtInsurancePlan").attr("valueId",parseData[0].insurance_scheme_id);

        	$("#txtCopayType").val(parseData[0].copay_type);

            $("#txtCopayValue").val(parseData[0].value);
            if (parseData[0].copay_type.toLowerCase() == "percentage") {
                $("#txtCopayValue").val(parseData[0].value);
                $("#amountPrefixCopayValue").text("%");
            }
            else {
                $("#amountPrefixCopayValue").text(amountPrefix);
            }
            $("#checkCopayValue").text(parseData[0].value);

        	$("#txtName").val(parseData[0].patient_name);

        	var patientId = parseInt(parseData[0].patient_id);
            var prefix = parseData[0].patient_prefix;
            
            var patientIdLength = patientId.toString().length;
            for (var i=0;i<6-patientIdLength;i++) {
                patientId = "0"+patientId;
            }
            patientid = prefix+patientId;
            $('#txtPatientId').val(patientid);

            $('.showOnClickLoad').show();
        }
        else{
        	callSuccessPopUp("Alert","No insurance details exist!!!");
        	$(".modal-footer").show();
        	$("#txtInsuranceCompany").val("");
        	$("#txtInsuranceCompany").attr("valueId","");

        	$("#txtInsurancePlan").val("");
        	$("#txtInsurancePlan").attr("valueId","");

        	$("#txtInsuranceScheme").val("");
        	$("#txtInsuranceScheme").attr("valueId","");

        	$('.showOnClickLoad').hide();
        }       
    });
}

function copayDetails(visitId){
	var postData = {
		"operation" : "ShowTableData",
		"visitId" : visitId
	}
	dataCall("controllers/admin/insurance_payment.php", postData, function (result){
		var parseData = JSON.parse(result);
		insuranceServiceCost.fnClearTable();
        if (result.length > 2) {
            $("#totalCost").text("0");
        	$.each(parseData,function(index,value){
        		for(var i =0; i<parseData[index].length;i++){

        			var revisedCost = parseData[index][i].revised_cost;
        			var actualCost = parseData[index][i].actual_cost;
        			var name = parseData[index][i].name;
                    var id = parseData[index][i].id;
                    var tableName = parseData[index][i].tbl_name;
        			insuranceServiceCost.fnAddData([name,revisedCost,actualCost,id,tableName]);
        			$("#totalCost").text(parseInt($("#totalCost").text())+parseInt(revisedCost));
        		}
        	});
            $("#checkTotalBill").text($("#totalCost").text());
            $("#totalCost").text(amountPrefix+' '+$("#totalCost").text());
        }
	});
}