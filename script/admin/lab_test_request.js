var otable = null;
var paymentMode = '';
var insuranceCompanyId = "";
var insurancePlanId = "";
var insurancePlanNameid = "";
var revisedCost = '';
var paymentMethod ='';
var copayType = '';
var copayValue = '';
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   lab_test_request.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Arora
 * Created Date :   30th dec, 2015
 * Description  :   This page  manages lab test in consultant module
 *************************************************************************************************** */
	$(".amountPrefix").text(amountPrefix);
    loader();
    var pay_status = '';
    var test_status = '';
    var cost = '';
 
	debugger;
	$('#oldHistoryModalDetails input[type=text]').addClass("as_label");
    $('#oldHistoryModalDetails input[type=text]').attr('readonly', true);
    $('#oldHistoryModalDetails input[type="button"]').addClass("btnHide");
    $('#oldHistoryModalDetails .hideInPopUp').addClass('hide');
    $('#oldHistoryModalDetails #divDropDownLabRequest').hide();

    /*Focus on first element*/
    $("#selLabType").focus();

    /**DataTable Initialization**/
    otable = $('#labRequestsTbl').DataTable({
        "bPaginate": false,
        aoColumnDefs: [{'bSortable': false,'aTargets': [6] }]
    });

    bindLabType(); //Loading the Lab type dropdown at the start
	

	if ($('#thing').val() ==3) {
        var visitId = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
        getPaymentMode(visitId);		
    }
    if ($('#thing').val() ==4) {
        var visitId = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
        getPaymentMode(visitId);
    }
    if ($('#thing').val() ==5) {
        var visitId = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
        getPaymentMode(visitId);
    }

    /*On selecting the lab type drop down*/
    $("#selLabType").change(function() {
        var option = "<option value='-1'>-- Select --</option>";
        if ($("#selLabType :selected") != -1) {
            var value = $("#selLabType :selected").val();
            $('#selLabTypeError').text("");
            $('#selLabType').removeClass("errorStyle");
        } else {
            $("#selLabTest").html(option);
        }
        bindLabTest(value); //on change of lab type call this function
    });

    $("#selLabTest").change(function() {
        var option = "<option value='-1'>-- Select --</option>";
        if ($("#selLabTest :selected") != -1) {
            var value = $("#selLabTest :selected").val();
            $('#selLabTestError').text("");
            $('#selLabTest').removeClass("errorStyle");
        } else {
            $("#selLabComponent").html(option);
        }
        bindLabComponent(value); //on change of lab type call this function
    });

    
    $("#selLabComponent").change(function() {
        var labComponentid = $("#selLabComponent :selected").val();
        if (labComponentid != -1) {
            $('#selLabComponentError').text("");
            $('#selLabComponent').removeClass("errorStyle");
            if (paymentMode.toLowerCase() == "insurance") {
                getRevisedCost();
                checkExclusion(labComponentid,insuranceCompanyId,insurancePlanId,insurancePlanNameid,"lab");
            }
            else{
                checkTestExcluded = '';
            }
        }
    });
    $("#selRequestType").change(function() {
        if ($("#selRequestType :selected") != -1) {
            $('#selRequestTypeError').text("");
            $('#selRequestType').removeClass("errorStyle");
        }
    });

    /*Binding the Lab Type dropdown*/
    function bindLabType() {
        /*AJAX call to show lab type*/
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/self_request.php",
            data: {
                "operation": "showLabType"
            },
            success: function(data) {
                if (data != null && data != "") {
                    var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                    var option = "<option value='-1'>-- Select --</option>";
                    for (var i = 0; i < parseData.length; i++) {
                        option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
                    }
                    $('#selLabType').html(option);
                }
            },
            error: function() {
                $('#messagemyModal').modal();
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            }
        });
    }

    /*Binding Lab Test drop down*/
    function bindLabTest(value) {
        var visitId = parseInt($('#textvisitId').val().replace( /[^\d.]/g, '' ));

        /*AJAX call to show lab type*/
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/lab_test_request.php",
            data: {
                "operation": "showLabTest",
                "value": value,
                "visitId" : visitId
            },
            success: function(data) {
                if (data != null && data != "") {

                    var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                    var option = "<option value='-1'>-- Select --</option>";
                    for (var i = 0; i < parseData.length; i++) {
                        option += "<option value='" + parseData[i].id + "' data-name='" + parseData[i].name + "'>" + parseData[i].name + "</option>";
                    }
                    $('#selLabTest').html(option);
                }
            },
            error: function() {
                $('#messagemyModal').modal();
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            }
        });
    }

    /*Add Lab Test click event to add test in table*/
    $('#addLabRequest').click(function() {        

        $('#addLabRequest').removeClass("errorStyle"); //remove the red error border from add test button
        $('#noDataError').html(""); //remove the error message after the add button

        if ($('#selLabType').val() == "-1" || $('#selLabType').val() == null) {
            $('#selLabTypeError').text("Please select lab type");
            $('#selLabType').addClass("errorStyle");
            $('#selLabType').focus();
            return false;
        }
        if ($('#selLabTest').val() == "-1" || $('#selLabTest').val() == null) {
            $('#selLabTestError').text("Please select lab test");
            $('#selLabTest').addClass("errorStyle");
            $('#selLabTest').focus();
            return false;
        }        
        if ($('#selLabComponent').val() == "-1" || $('#selLabComponent').val() == null) {
            $('#selLabComponentError').text("Please select lab type");
            $('#selLabComponent').addClass("errorStyle");
            $('#selLabComponent').focus();
            return false;
        }
        if($("#selRequestType").val() == -1){
            $("#selRequestType").focus();
            $("#selRequestType").addClass("errorStyle");
            $("#selRequestTypeError").text("Please select request type");
            return false;
        }

        var id = $('#selLabTest').val();//getting id of selected element
        var newName = $('#selLabTest  option:selected').attr('data-name');//getting attribute name of selected element
        var componentName = $("#selLabComponent option:selected").attr('data-name');
        var newCost = $('#selLabComponent  option:selected').attr('data-cost');
        var componentId = $("#selLabComponent").val();
        var requestType = $('#selRequestType').val();
        var paidStatus = "Unpaid";//by default
        var testStatus = "Pending";//by default
        var newRevisedCost = '';        
        if (revisedCost == "") {
            newRevisedCost = newCost;
            paymentMethod = "cash";
        }
        else {
            newRevisedCost = revisedCost;
            paymentMethod = "insurance";
        }    

        if (checkTestExcluded =="yes") {
            $('#confirmUpdateModalLabel').text();
            $('#updateBody').text("Are you sure you want to add this still it is not included in your insurance?");
            $('#confirmUpdateModal').modal();
            $("#btnConfirm").unbind();
            $("#btnConfirm").click(function(){
                  
                var newPaymentMethod = "cash";                   
                newRevisedCost = newCost;//if it is excluded then revised cost is equal to orignal cost

                var addData = ["<span id ='newDataOfTable' data-previous='false'>"+newName+"</span>",componentName, paidStatus, testStatus, newCost, newRevisedCost,"<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>", id,requestType,"notRestrictBill",componentId,newPaymentMethod];
              
                /*Check that test already exist*/
                for (var i = 0; i < otable.fnGetNodes().length; i++) {
                    var iRow = otable.fnGetNodes()[i];   // assign current row to iRow variable
                    var aData = otable.fnGetData(iRow); // Pull the row

                    matchComponenId = aData[10];//getting component id
                    var flag = false;
                    if (componentId == matchComponenId && aData[3]!="resultsent") { 
                        $('#selLabComponent').focus();
                        $('#selLabComponentError').text("Component already exist");
                        $('#selLabComponent').addClass("errorStyle");
                        return true;
                    }
                    if (flag == true) {
                        return false;
                    }
                    if (componentId == matchComponenId && aData[3]=="resultsent") {
                        addData = ["<span id ='newDataOfTable' data-previous='false'>"+newName+"</span>", componentName,paidStatus, testStatus, newCost, newRevisedCost,"<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>", id,requestType,"restrictBill",componentId,newPaymentMethod];
                    } 
                }

                //check external internal in lab test req
                if (requestType == "external") {
                    var newExtCost = 0;
                    var paidExtStatus = "N/A";
                    var testExtStatus = "N/A";
                    var extPaymentMethod = "cash";

                    var addData = ["<span id ='newDataOfTable' data-previous='false'>"+newName+"</span>",componentName, paidExtStatus, testExtStatus, newExtCost, newExtCost,"<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>", id,requestType,"restrictBill",componentId,extPaymentMethod];
                }

                var aiRows = $(otable).find('.btnEnabled');
                for(var i=0;i<aiRows.length;i++){
                    iRow = $(aiRows).closest('tr')[i];
                    aData = otable.fnGetData(iRow);

                    if (id == aData[7]) {
                        if (requestType != aData[8]) {

                            $('#messagemyModal').modal();
                            $('#messageMyModalLabel').text("Sorry");
                            $('.modal-body').text("You can't request same test component with differnt request status!!!");     
                            return false;
                        }                
                    }
                }
                /*Add data in table*/
                var currData = otable.fnAddData(addData);

                /*Get current row to add color in that*/
                var getCurrDataRow = otable.fnSettings().aoData[ currData ].nTr;
                $(getCurrDataRow).find('td').css({"background-color":"#fff","color":"#000"});

                if (requestType == "internal") {
                    $('#totalBill').text(parseInt($('#totalBill').text()) + parseInt(newRevisedCost));//add total bill
                }
            });
        }

        /*if */
        else{
            
            if (requestType == "internal") {
                addData = ["<span id ='newDataOfTable' data-previous='false'>"+newName+"</span>",componentName, paidStatus, testStatus, newCost, newRevisedCost,"<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>", id,requestType,"notRestrictBill",componentId,paymentMethod];
            }

            /*Check that test already exist*/
            for (var i = 0; i < otable.fnGetNodes().length; i++) {
                var iRow = otable.fnGetNodes()[i];   // assign current row to iRow variable
                var aData = otable.fnGetData(iRow); // Pull the row

                matchComponenId = aData[10];//getting component id
                var flag = false;
                if (componentId == matchComponenId && aData[3]!="resultsent") { 
                    $('#selLabComponent').focus();
                    $('#selLabComponentError').text("Component already exist");
                    $('#selLabComponent').addClass("errorStyle");
                    return true;
                }
                if (flag == true) {
                    return false;
                }
                if (componentId == matchComponenId && aData[3]=="resultsent") {
                    addData = ["<span id ='newDataOfTable' data-previous='false'>"+newName+"</span>", componentName,paidStatus, testStatus, newCost, newRevisedCost,"<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>", id,requestType,"restrictBill",componentId,paymentMethod];
                }
            }

            var aiRows = $(otable).find('.btnEnabled');
            for(var i=0;i<aiRows.length;i++){
                iRow = $(aiRows).closest('tr')[i];
                aData = otable.fnGetData(iRow);

                if (id == aData[7]) {
                    if (requestType != aData[8]) {

                        $('#messagemyModal').modal();
                        $('#messageMyModalLabel').text("Sorry");
                        $('.modal-body').text("You can't request same test component with differnt request status!!!");     
                        return false;
                    }                
                }
            }

            if (requestType == "external") {
                var newExtCost = 0;
                var paidExtStatus = "N/A";
                var testExtStatus = "N/A";
                var extPaymentMethod = "cash";
                
                var addData = ["<span id ='newDataOfTable' data-previous='false'>"+newName+"</span>",componentName, paidExtStatus, testExtStatus, newExtCost, newExtCost,"<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>", id,requestType,"restrictBill",componentId,extPaymentMethod];
            }

            /*Add data in table*/
            var currData = otable.fnAddData(addData);

            /*Get current row to add color in that*/
            var getCurrDataRow = otable.fnSettings().aoData[ currData ].nTr;
            $(getCurrDataRow).find('td').css({"background-color":"#fff","color":"#000"});

            if (requestType == "internal") {
                $('#totalBill').text(parseInt($('#totalBill').text()) + parseInt(newRevisedCost));//add total bill
            }
        }
    });


    /*Save button functionality*/
    $('#btnSaveLabTestRequest').click(function() {

        var tempId = "0";
        var componentId = "0";
        var tempTotal = 0;

        var tableData = otable.fnGetData(); //fetching the datatable data

        /*Code to get only current data on click means not to get previous data coming from database*/
        var arr= [];
        var smallArray = new Array();
        var bigPrice_l = new Array();
        var Price_l = new Array();
        var tempArray =  new Array();
        var Price  = new Array();
        var tempId = "0";
        var componentId = "0";
        var requestType = "";
        var restrictBill = "";
        var tempTotal = 0;
        var savePaymentMethod = '';
        var isFirst = "1";
        var j = 1;
        var componentCost = '';
        var totalCurrentReqBill = 0;
        var aiRows = $(otable).find('.btnEnabled');
        for(var i=0;i<aiRows.length;i++){
            smallArray = new Array();
            iRow = $(aiRows).closest('tr')[i];
            aData = otable.fnGetData(iRow);


            var labId = aData[7];

            if(tempId == "0"){
                var Obj = {};
                Obj.labId = labId;
                Obj.componentId = aData[10];
                Obj.savePaymentMethod = aData[11];
                Obj.componentCost = aData[5];//to save individual component cost           
                tempTotal = parseInt(aData[5]);// to save all total component cost in bill table 
                restrictBill = aData[9];
                requestType = aData[8];                
                tempArray.push(Obj);
            }
            if(tempId != "0"){
                if(tempId == labId){
                    var Obj = {};
                    Obj.labId = tempId;
                    Obj.componentId = aData[10];
                    Obj.componentCost = aData[5];                
                    tempTotal = tempTotal + parseInt(aData[5]);
                    Obj.savePaymentMethod = aData[11];
                    tempArray.push(Obj);
                }
                else{
                    bigPrice_l.push(tempArray);

                    var newObj = {};
                    newObj.labId = tempId;
                    newObj.total = tempTotal;
                    newObj.savePaymentMethod = savePaymentMethod;

                    newObj.restrictBill = restrictBill;
                    newObj.requestType = requestType;

                    Price_l.push(newObj);
                    Price.push(Price_l);
                    tempArray =  new Array();
                    Price_l = new Array();
                    restrictBill = aData[9];
                    requestType = aData[8];
                    tempId = labId;
                    var Obj = {};
                    Obj.labId = tempId;
                    Obj.componentId = aData[10];
                    Obj.componentCost = aData[5];
                    tempTotal = parseInt(aData[5]);
                    Obj.savePaymentMethod = aData[11];
                    tempArray.push(Obj);
                }
            }
            else{
                tempId = labId;
            }
            if(aiRows.length == j){
                var newObj = {};
                if(tempId == "0"){
                    newObj.labId = labId;
                }   
                else{
                    newObj.labId = tempId;
                }
                newObj.total = tempTotal;
                newObj.savePaymentMethod = savePaymentMethod;
                newObj.restrictBill = restrictBill;
                newObj.requestType = requestType;
                Price_l.push(newObj);
                Price.push(Price_l);

                bigPrice_l.push(tempArray);
                Price_l = new Array();
            }

            //getting the current added test total bill
            if (Obj.savePaymentMethod == "insurance") {
                totalCurrentReqBill= totalCurrentReqBill + parseInt(aData[5]);
            }                
            j++;    
        }

        //Checking whether datatable is empty or not
        if (tableData.length == 0) {

            $('#addLabRequest').addClass("errorStyle"); //Put the red border around the add lab button
            $('#noDataError').html("&nbsp;&nbsp;Please add test(s)."); //Put the error message after the add lab test button
            return false;

        } else { //Saving the data

            var patientId =  parseInt($('#txtPatientID').val().replace ( /[^\d.]/g, '' ));
            var visitId =  parseInt($('#textvisitId').val().replace ( /[^\d.]/g, '' ));
            var requestType = $("#selRequestType").val();
			if(bigPrice_l.length == 0 && Price.length == 0){
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Sorry");
				$('.modal-body').text("Your request already in pending!!!");
			}
			else{
				/*AJAX call to save data*/
                $('#confirmUpdateModalLabel').text();
                $('#updateBody').text("Are you sure that you want to save this?");
                $('#confirmUpdateModal').modal();
                $("#btnConfirm").unbind();
                $("#btnConfirm").click(function(){

    				$.ajax({
    					type: "POST",
    					cache: false,
    					url: "controllers/admin/lab_test_request.php",
    					datatype: "json",
    					data: {
    						'operation': 'save',
                            'Price': JSON.stringify(Price),//send current data only
    						'bigPrice_l': JSON.stringify(bigPrice_l),//send current data only
    						'patientId': patientId,
    						'visitId': visitId,
    						'tblName': 'lab_test_requests',
    						'fkColName': 'lab_test_id',
                            'requestType': requestType,
                            'totalCurrentReqBill' : totalCurrentReqBill,
                            'copayType' : copayType,
                            'copayValue' : copayValue
    					},

    					success: function(data) {
    						if (data != "0" && data != "") {
    							$('#messageMyModalLabel').text("Success");
    							$('.modal-body').text("Lab test requested successfully!!!");
    							$('#messagemyModal').modal();
    							$("#selLabTest").val("-1");
    							$("#selLabTest").removeClass("errorStyle");
    							$("#selLabTestError").text("");
    							bindLabType();
    							var visitId = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
    							LabTest(visitId,paymentMode);
    							$('#labRequestsTbl input[type="button"]').addClass("btnHide");
    							$('#labRequestsTbl input[type="button"]').closest('tr').find('td').removeAttr("style");
                                clear();
    						} 
    						else if (data == "0") {
    							$('#messagemyModal').modal();
    							$('#messageMyModalLabel').text("Sorry");
    							$('.modal-body').text("Your test result is still not sent!!!");
    						}
    						else {
    							$('#messagemyModal').modal();
    							$('#messageMyModalLabel').text("Error");
    							$('.modal-body').text("Please refresh your page once");                        
    						}
    					},
    					error: function() {
    						$('#messageMyModalLabel').text("Error");
    						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
    						$('#messagemyModal').modal();
    					}
    				});
                });
			}            
        }
    });  

    //Reset button functionality
    $('#btnResetLabTestRequest').click(function() {
        $('#selLabType').val("-1");
        var lengthDeleteRow = $(".btnEnabled").closest("tr").length;
        for(var i=0;i<lengthDeleteRow;i++){
            var myDeleteRow = $(".btnEnabled").closest("tr")[0];            
            $('#totalBill').text(parseInt($('#totalBill').text()) - parseInt($(myDeleteRow).find('td:eq(5)').text()));
            otable.fnDeleteRow(myDeleteRow);
        }
           
        clear();
    });  
});

function bindLabComponent(value){
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/lab_test_request.php",
        data: {
            "operation": "showLabTestComponent",
            "labTestId" : value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "' data-cost='" + parseData[i].fee + "' data-name='"+parseData[i].name+"'>" + parseData[i].name + "</option>";
                }
                $('#selLabComponent').html(option);                            
            }
        },
        error: function() {
            $('#messagemyModal').modal();
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
        }
    });
}


/**Row Delete functionality**/
function rowDelete(currInst) {
    var row = currInst.closest("tr").get(0);
    $('#totalBill').text(parseInt($('#totalBill').text()) - parseInt($(row).find('td:eq(5)').text()));
    otable.fnDeleteRow(row);
}

function clear() {
    $("#selLabType").val("-1");
    $("#selLabType").removeClass("errorStyle");
    $("#selLabTypeError").text("");
    $("#selLabTest").html("<option value='-1'>-- Select --</option>");
    $("#selLabTest").removeClass("errorStyle");
    $("#selLabTestError").text("");
    $("#noDataError").text("");
    $(".addValues").removeClass("errorStyle");
    $("#selLabType").focus();
    $("#selRequestType").val("-1");
    $("#selRequestTypeError").text("");
    $("#selRequestType").removeClass("errorStyle");
    $("#selLabComponent").html("<option value='-1'>-- Select --</option>");
    $("#selLabComponentError").text("");
    $("#selLabComponent").removeClass("errorStyle");
}
function getPaymentMode(visitId){
    var postData = {
        "operation": "payment_mode",
        "visitId": visitId
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (result.trim() != '') {
            paymentMode = parseData[0].payment_mode;

            if (paymentMode.toLowerCase() == "insurance") {
                insuranceDetails(visitId);
                /*$("#selRequestType").val("internal");
                $("#selRequestType").prop("disabled", true);*/
            }            
        } 
        LabTest(visitId,paymentMode);             
    });
}
function insuranceDetails(visitId){
    var postData = {
        "operation": "insuranceDetail",
        "visitId": visitId
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (parseData != '') {
            insuranceCompanyId = parseData[0].insurance_company_id;
            insurancePlanId = parseData[0].insurance_plan_id;
            insurancePlanNameid = parseData[0].insurance_plan_name_id;

            getCopayValue();//get copay details
        }
    });
}

function getRevisedCost(){
    var componentId = $("#selLabComponent").val();
    var postData = {
        "operation": "revisedCostDetail",
        "insuranceCompanyId": insuranceCompanyId,
        "insurancePlanId" : insurancePlanId,
        "insurancePlanNameid" : insurancePlanNameid,
        "componentId" : componentId,
        "activeTab" : "lab"
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (result.length > 2) {
            revisedCost = parseData[0].revised_cost;
        }
        else{
            revisedCost ='';
        }
    });
}
function LabTest(visitId,paymentMode){
    /*Fetch data from db and insert in to table during page load*/
    var postData = {
        "operation": "lab_search",
        "visit_id": visitId,
        "paymentMode" : paymentMode,
        "activeTab" : "lab"
    }
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/old_test_requests.php",
        datatype: "json",
        data: postData,

        success: function(data) {
            dataSet = JSON.parse(data);
            otable.fnClearTable();
            $('#totalBill').text("0");
            
            for(var i=0; i< dataSet.length; i++){
                var id = dataSet[i].id;
                pay_status = dataSet[i].pay_status;
                test_status = dataSet[i].test_status;
                cost =  dataSet[i].cost;
                var fee = dataSet[i].fee
                if (cost == null) {
                    cost = 0;
                }
                var name = dataSet[i].name;
                var componentName = dataSet[i].component_name;
                var componentId = dataSet[i].component_id;
                var oldRevisedCost = '';
                //check wheteher in name external is coming or not
                if (name.indexOf("(external)") > 1) {
                    var test_status = dataSet[i].test_status;
                    var cost = dataSet[i].cost;
                    pay_status = "N/A";
                    test_status = "N/A";
                    cost = 0;
                    fee = 0;
                }  

                oldRevisedCost = cost;//put cost in to old revised cost var
                /*Apply here span to recognise previous or current data*/
                if (paymentMode.toLowerCase() == "insurance") {
                    otable.fnAddData(["<span data-previous='true'>"+name+"</span>",componentName,pay_status, test_status,fee,oldRevisedCost,"<input type='button' class ='btnDisabled' value='Remove' disabled>",id,'increase','length',componentId]);//send one column null so user can't remove previous data
                }
                else{
                    otable.fnAddData(["<span data-previous='true'>"+name+"</span>",componentName,pay_status, test_status,cost,oldRevisedCost,"<input type='button' class ='btnDisabled' value='Remove' disabled>",id,'increase','length',componentId]);//send one column null so user can't remove previous datatable
                }
                $('#totalBill').text(parseInt($('#totalBill').text()) + parseInt(oldRevisedCost));//add total bill
            }
            otable.find('tbody').find('tr').find('td').css({"background-color":"#ccc"});
        }
    });
}
function getCopayValue(){
    var postData = {
        "operation": "getCopayValue",
        "insuranceCompanyId": insuranceCompanyId,
        "insurancePlanId" : insurancePlanId,
        "insurancePlanNameid" : insurancePlanNameid
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (parseData != '' && parseData != null) {
            copayType = parseData[0].copay_type;
            copayValue = parseData[0].value;
        }
    });
}