var imprestWarrantTable;
var lineItemsTable;
var totalamount =0;
$(document).ready(function(){
    loader();
    totalamount =0;
    debugger;
    bindLedger();
	
    imprestWarrantTable = $('#tblImprestsurrender').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "bAutoWidth":false,
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = imprestWarrantTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var warrent_id = mData["warrent_id"];
                        var imprestNo = mData["imprest_request_id"];
                        var imprestPrefix = mData["imprest_prefix"];
                            
                        var visitIdLength = imprestNo.length;
                        for (var i=0;i<6-visitIdLength;i++) {
                            imprestNo = "0"+imprestNo;
                        }
                        imprestNo = imprestPrefix+imprestNo;
                        var staffId = mData["staff_id"];
                        var ledger = mData["ledger_id"];
                        var name = mData["name"];
                        var amount = mData["amount"];
                        var natureOfDuty = mData["nature_of_duty"];
                        var EstDays = mData['est_day'];
                        var startDate = mData['assignment_start'];
                        var surrendorDate = mData['assignment_end'];
                        editClick(id,warrent_id,imprestNo,staffId,ledger,name,amount,natureOfDuty,EstDays,startDate,surrendorDate);
                    }
                });
                //perform view event
                $('.imprestWarrantview').unbind();
                $('.imprestWarrantview').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = imprestWarrantTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var imprestNo = mData["imprest_request_id"];
                        var imprestPrefix = mData["imprest_prefix"];
                            
                        var visitIdLength = imprestNo.length;
                        for (var i=0;i<6-visitIdLength;i++) {
                            imprestNo = "0"+imprestNo;
                        }
                        imprestNo = imprestPrefix+imprestNo;
                        var staffId = mData["staff_id"];
                        var ledger = mData["ledger_id"];
                        var name = mData["name"];
                        var amount = mData["amount"];
                        var natureOfDuty = mData["nature_of_duty"];
                        var EstDays = mData['est_day'];
                        var startDate = mData['assignment_start'];
                        var surrendorDate = mData['assignment_end'];
                        var ledgerName = mData['ledger_name']; 
                        viewClick(id,imprestNo,staffId,ledger,name,amount,natureOfDuty,EstDays,startDate,surrendorDate,ledgerName);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/imprest_surrender.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": function(o){
                        var imprestId = o["imprest_request_id"];
                        var imprestPrefix = o["imprest_prefix"];
                            
                        var visitIdLength = imprestId.length;
                        for (var i=0;i<6-visitIdLength;i++) {
                            imprestId = "0"+imprestId;
                        }
                        imprestId = imprestPrefix+imprestId;
                        return imprestId;  

                    } 
                },
                {
                    "mData": "name"
                },
                {
                    "mData": "nature_of_duty"
                },
                {
                    "mData": "assignment_start"
                },
                {
                    "mData": "assignment_end"
                },        
                {
                
                    "mData": function(o) {
                        var data = o;
                        return "<i class=' fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            "<i class='fa fa-eye imprestWarrantview' style='font-size: 17px; cursor:pointer;'' title='view' value='view'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [5]
            }]

        });
    lineItemsTable = $('#tblSurrenderLineItem').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth":false,
        "fnDrawCallback": function(oSettings) {
        },
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [2]
        }]
    });

});
function editClick(id,warrent_id,imprestNo,staffId,ledger,name,amount,natureOfDuty,EstDays,startDate,surrendorDate) {
    $('.updateMain').removeAttr('style');
    $('#imprestWarrantButtons').removeAttr("style");
    $(".blackborder").hide();
    $('.tblLineItems').hide();
    $('#btnBackToList').removeAttr("style");
     $('#mainPopUp').addClass("activeSelf");
        $('#lineItemsPopUp').removeClass("activeSelf");
        $("#mainPopUp").css("background-color", "rgb(115, 115, 115)");
        $(".updateMain").show();
        $('.tblLineItems').hide();
        removeErrorMessage();
        
        $('#txtImprestNo').val(imprestNo);
        $('#txtStaffId').val(staffId);
        $('#selLedger').val(ledger);
        $('#txtName').val(name);
        $('#txtAmount').val(amount);
        $('#txtNatureOfDuty').val(natureOfDuty);
        $('#txtEstDays').val(EstDays);
        $('#txtStartDate').val(startDate);
        $('#txtSurrenderDate').val(surrendorDate);
        $('#selectedRow').val(id);
    bindSpentAmount(parseInt($("#txtImprestNo").val().replace ( /[^\d.]/g, '' )));

    $('#BackToList').click(function() {
        $(".blackborder").show();
        $(".updateMain").hide();
        $('.tblLineItems').hide();
        $('#imprestWarrantButtons').hide();
        $('#btnBackToList').hide();
    });

    
    $('#mainPopUp').click(function() {
        $('#mainPopUp').addClass("activeSelf");
        $('#lineItemsPopUp').removeClass("activeSelf");
        $("#mainPopUp").css("background-color", "rgb(115, 115, 115)");
        $(".updateMain").show();
        $('.tblLineItems').hide();
        removeErrorMessage();
        
        $('#txtImprestNo').val(imprestNo);
        $('#txtStaffId').val(staffId);
        $('#selLedger').val(ledger);
        $('#txtName').val(name);
        $('#txtAmount').val(amount);
        $('#txtNatureOfDuty').val(natureOfDuty);
        $('#txtEstDays').val(EstDays);
        $('#txtStartDate').val(startDate);
        $('#txtSurrenderDate').val(surrendorDate);
        $('#selectedRow').val(id);
        //validation
        //remove validation style
        // edit data function for update 
        
        
        
        
    });

    $('#lineItemsPopUp').click(function() {
        $('#lineItemsPopUp').addClass("activeSelf");
        $('#mainPopUp').removeClass("activeSelf");
        $('.tblLineItems').removeAttr('style');
        $('.tblLineItems').show();
        $(".updateMain").hide();

        loadLineItemTable();
        
      /*  $('#btnAdd').unbind();
        $('#btnAdd').click(function() {
            var currentData = lineItemsTable.fnAddData(['<input type="text"  id="txtdescription" placeholder="Enter description">',
                '<input type="number"  id="txtLineAmount" placeholder = "Enter amount">',
                '<select id ="selChartOfAccount"><option value="">--Select--</option></select>',
                '<input type="button" id ="newButtonColor" class="btnEnabled" onclick="rowDelete($(this));" value="Remove">'
            ]);
            var getCurrDataRow = lineItemsTable.fnSettings().aoData[ currentData ].nTr;
            bindGLAccount($(getCurrDataRow).find('td:eq(2)').children()[0],'');
        });*/

        $('#btnSave').unbind();
        $('#btnSave').click(function() {
        	var checkTotal = totalamount;
            var updateLineItems = [];
            var flag = false;
            var mainAmount = $('#txtAmount').val();
            var items = $("#lineItemsTable tbody tr");
            var rows = $(lineItemsTable).find('tbody').find('input[type="number"]');

            if($('#lineItemsTable tr').find('td').text() == "No data available in table"){
                callSuccessPopUp("Sorry","Please insert row on datatable");
                return false;
            }
            
            $.each(rows,function(i,v){
                var obj = { };
                var amount = $($(rows[i]).parent().parent().find('input[type="number"]')).val();
                obj.id = $($(rows[i]).parent().parent().find('input[type="number"]')).attr('data-id');
                obj.amount = amount;
                if(obj.amount == ''){
                    callSuccessPopUp("Alert","Please enter spent amount");
                    flag = true;
                    return false;
                }  
                updateLineItems.push(obj);
            });            

           
            if(flag == true){
                return false;
            }

            var addData = {
                "updateLineItems" : JSON.stringify(updateLineItems),
                "warrentId" : warrent_id,
                "operation" : "updateLineItems"
            }

            $.ajax(
            {                   
                type: "POST",
                cache: false,
                url: "controllers/admin/imprest_surrender.php",
                datatype:"json",
                data: addData,
                
                success: function(data) {
                    if(data != "0" && data != ""){
                    	//$("#messagemyModal .modal-footer").html('');
                        callSuccessPopUp('Success','Updated successfully!!!');
                        loadLineItemTable();
                        //$("#messagemyModal .modal-footer").html('<button v></button>');
                    }
                },
                error: function(){

                }
            });
        });

    });
    $('#mainPopUp').addClass("activeSelf");
} // end update button

function loadLineItemTable(){
	var addData = {
        "imprestRequestId" : parseInt($("#txtImprestNo").val().replace ( /[^\d.]/g, '' )),
        "operation" : "showWarrentDetail"
    }

    $.ajax(
    {                   
        type: "POST",
        cache: false,
        url: "controllers/admin/imprest_surrender.php",
        datatype:"json",
        data: addData,
        
        success: function(data) {
            if(data != "0" && data != ""){
            	 var parseData = jQuery.parseJSON(data);
            	 lineItemsTable.fnClearTable();
            	 totalamount =0;
            	  $.each(parseData,function(i,v){
                    if(v.spent_amount_status == 'unpaid'){
                        var currentData = lineItemsTable.fnAddData([v.account_name,v.description,v.initial_amount,'<input type="number" data-id="'+v.id+'" class="spent_amount"  id="txtSpentAmount" placeholder = "Enter amount">']);
                        totalamount =totalamount +parseFloat(v.initial_amount);
                    }
                    else{
                        var currentData = lineItemsTable.fnAddData([v.account_name,v.description,v.initial_amount,v.spent_amount]);
                        totalamount =totalamount +parseFloat(v.initial_amount);
                    }
                 	
                 });
                  bindSpentAmount(parseInt($("#txtImprestNo").val().replace ( /[^\d.]/g, '' )));
            }
        },
        error: function(){

        }
    });
}
function viewClick(id,imprestNo,staffId,ledger,name,amount,natureOfDuty,EstDays,startDate,surrendorDate,ledgerName) {
    $('#myImprestWarrantModel').modal("show");
    $('#imprestWarrantModelLabel').text("Imprest Details");

    var staffIdLength = staffId.length;
    for (var j=0;j<6-staffIdLength;j++) {
        staffId = "0"+staffId;
    }
    staffId = staffPrefix+staffId;

    $('#lblStaffId').text(staffId);
    $('#lblName').text(name);
    $('#lblLedger').text(ledgerName);
    $('#lblImprestRequestId').text(imprestNo);
    $('#lblNatureOfDuty').text(natureOfDuty);
    $('#lblEstDay').text(EstDays);
    $('#lblAmount').text(amount);
    $('#lblStartDate').text(startDate);
    $('#lblEndDate').text(surrendorDate);

} 
function bindLedger() {     //  function for loading ledger
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/payment_mode.php",
        data: {
            "operation": "showLedger"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option data-value='" + parseData[i].value + "' value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selLedger').html(option);
            }
        },
        error: function() {}
    });
}

function bindSpentAmount(imprestRequestId) {     //  function for loading ledger
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/imprest_surrender.php",
        data: {
            "operation": "spentAmount",
            "imprestRequestId": imprestRequestId
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery
                $('#txtAmountSpent').val(parseData[0].spent_amount);
            }
        },
        error: function() {}
    });
}

function rowDelete(currInst) {
    var row = currInst.closest("tr").get(0);

    lineItemsTable.fnDeleteRow(row);
    /*row.remove();*/
}
function clear() {
    $('#selChartOfAccount').val('');
    $('#txtdescription').val('');
    $('#txtLineAmount').val('');
}