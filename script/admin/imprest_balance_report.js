var imprestBalTable = '';
$(document).ready(function() {
	debugger;
	$(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});
	var arr=[];
	removeErrorMessage();//call from common.js

	imprestBalTable = $('#imprestBalTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "aaSorting": []
    });

    $("#viewReport").on('click',function(){
    	var toDate = $("#toDate").val().trim();
		var fromDate = $("#fromDate").val().trim();
    	var postData = {
			"operation" : "showData",
			'toDate' : toDate,
			'fromDate' : fromDate
		}
		dataCall("./controllers/admin/imprest_balance_report.php", postData, function (result) {
			imprestBalTable.fnClearTable();
			if (result.trim().length > 2) {
				var parseData = JSON.parse(result);				

		    	$.each(parseData,function(i,v){
		    		var imprestId = v.imprest_request_id;
                            
                    var imprestIdLength = imprestId.length;
                    for (var i=0;i<6-imprestIdLength;i++) {
                        imprestId = "0"+imprestId;
                    }
                    imprestId = imprestPrefix+imprestId;
		    		imprestBalTable.fnAddData([imprestId,v.payee_name,v.balance]);
		    	});
			}
		});			
    });

    $("#clear").on('click',function(){
    	clear();
    })
});

function clear() {
	$("#toDate,#fromDate").val('');
	imprestBalTable.fnClearTable();
}