var bankReconciliationTable;
var initTable;
var tblBanking;
var tblDeposit;
var tblPayment;
var tblJournalVoucher;
$(document).ready(function(){
    loader();
    debugger;
	initTable = true;
    bindLedger('#selLedger');  //bind for loading ledger

	/*Hide add ward by default functionality*/
	$("#advanced-wizard").hide();
	$("#ledgerList").css({
        "background-color": "#fff"
    });
	$("#tabledgerList").css({
        "background-color": "#0c71c8",
        "color": "#fff"
    });
	
	/*Click for add the ledger*/
    $("#tabAddledger").click(function() {
        $("#advanced-wizard").show();
        $(".blackborder").hide();
		clear();
        $("#tabAddledger").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tabledgerList").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#ledgerList").css({
            "background": "#fff"
        });
    });
	
	//Click function for show the ledger lists
    $("#tabledgerList").click(function() {
        clear();
        tabledgerList();
    });

    var startDate = new Date();
    $("#txtReconciliationDate").datepicker('setEndDate',startDate).on('changeDate', function(ev){                 
        $('#txtReconciliationDate').datepicker('hide');
    });

    $('#txtReconciliationClosedDate').datepicker('setStartDate', startDate).on('changeDate', function(ev){                 
        $('#txtReconciliationClosedDate').datepicker('hide');
    });

    $("#selLedger").change(function(){
        var ledgerId = $('#selLedger').val();
        if(ledgerId != ''){
            bindPaymentMode(ledgerId);
        }
        else{
            $("#selPaymentMode").html("<option value=''>--Select--</option>");
        }
    });

    $('#txtReconciliationDate').change(function() {
        var lastReconcilation = $('#txtReconciliationDate').val();
        $('#txtLastReconciliationDate').val(lastReconcilation);
    });

    removeErrorMessage();


    $('#btnSubmit').click(function() {
        var flag = false;

        if($('#txtReconciliationClosedDate').val() == '') {
            $('#txtReconciliationClosedDateError').text('Please enter reconciliation closed date');
            $('#txtReconciliationClosedDate').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtReconciliationDate').val() == '') {
            $('#txtReconciliationDateError').text('Please enter reconciliation date');
            $('#txtReconciliationDate').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtStatementBalance').val() == '') {
            $('#txtStatementBalanceError').text('Please enter statement balance');
            $('#txtStatementBalance').focus();
            $('#txtStatementBalance').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtName').val() == '') {
            $('#txtNameError').text('Please enter name');
            $('#txtName').focus();
            $('#txtName').addClass("errorStyle"); 
            flag = true;
        }
        if($('#selPaymentMode').val() == '') {
            $('#selPaymentModeError').text('Please select payment mode');
            $('#selPaymentMode').focus();
            $('#selPaymentMode').addClass("errorStyle"); 
            flag = true;
        }
        if($('#selLedger').val() == '') {
            $('#selLedgerError').text('Please select ledger');
            $('#selLedger').focus();
            $('#selLedger').addClass("errorStyle"); 
            flag = true;
        }
        if(flag == true) {
            return false;
        }

        
        var ledger = $('#selLedger').val();
        var paymentMode = $('#selPaymentMode').val();
        var name = $('#txtName').val().trim();
        var statementBalance = $('#txtStatementBalance').val();
        var reconciliationDate = $('#txtReconciliationDate').val();
        var reconcilationClosedDate = $('#txtReconciliationClosedDate').val();
        var lastReconcilationDate = $('#txtLastReconciliationDate').val();

        var postData = {
            ledger : ledger,
            paymentMode : paymentMode,
            name : name,
            statementBalance : statementBalance,
            reconciliationDate : reconciliationDate,
            reconcilationClosedDate : reconcilationClosedDate,
            lastReconcilationDate : lastReconcilationDate,
            operation : "saveBankReconciliation"
        }


        $.ajax(
            {                   
            type: "POST",
            cache: false,
            url: "controllers/admin/bank_reconciliation.php",
            datatype:"json",
            data: postData,
            
            success: function(data) {
                if(data != "0" && data != ""){
                    callSuccessPopUp('Success','Saved successfully!!!');
                     tabledgerList();
                    clear();
                }
            },
            error: function(){

            }
        });
    });

    $('#btnReset').click(function() {
        clear();
    });

    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
    //Datatable code 
    bankReconciliationTable = $('#bankReconciliationTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "autoWidth": false,
        "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = bankReconciliationTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var ledgerId = mData["ledger_id"];                      
                        var paymentModeId = mData["payment_mode_id"];
                        var name = mData["name"];                      
                        var statementBalance = mData["statement_balance"];
                        var reconciliationDate = mData["reconciliation_date"];                      
                        var lastReconcilation = mData["last_reconciliation_date"];
                        var reconcilationClosedDate = mData["reconciliation_close_date"];
                        editClick(id,ledgerId,paymentModeId,name,statementBalance,reconciliationDate,lastReconcilation,reconcilationClosedDate);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = bankReconciliationTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/bank_reconciliation.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "ledger_name"
                }, {
                    "mData": "payement_mode_name"
                }, {
                    "mData": "name"
                }, {
                    "mData": "statement_balance"
                }, {
                    "mData": "reconciliation_date"
                }, {
                    "mData": "last_reconciliation_date"
                }, {
                    "mData": "reconciliation_close_date"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [7]
            }]

        });
    }

    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            bankReconciliationTable.fnClearTable();
            bankReconciliationTable.fnDestroy();
            bankReconciliationTable = "";
            bankReconciliationTable = $('#bankReconciliationTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "autoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = bankReconciliationTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            restoreClick(id);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/bank_reconciliation.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "ledger_name"
                    }, {
                        "mData": "payement_mode_name"
                    }, {
                        "mData": "name"
                    }, {
                        "mData": "statement_balance"
                    }, {
                        "mData": "reconciliation_date"
                    }, {
                        "mData": "last_reconciliation_date"
                    }, {
                        "mData": "reconciliation_close_date"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [7]
                }]
            });
        } else { // show active data on unchecked   
            bankReconciliationTable.fnClearTable();
            bankReconciliationTable.fnDestroy();
            bankReconciliationTable = "";
            bankReconciliationTable = $('#bankReconciliationTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "autoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = bankReconciliationTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var ledgerId = mData["ledger_id"];                      
                            var paymentModeId = mData["payment_mode_id"];
                            var name = mData["name"];                      
                            var statementBalance = mData["statement_balance"];
                            var reconciliationDate = mData["reconciliation_date"];                      
                            var lastReconcilation = mData["last_reconciliation_date"];
                            var reconcilationClosedDate = mData["reconciliation_close_date"];
                            editClick(id,ledgerId,paymentModeId,name,statementBalance,reconciliationDate,lastReconcilation,reconcilationClosedDate);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = bankReconciliationTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/bank_reconciliation.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "ledger_name"
                    }, {
                        "mData": "payement_mode_name"
                    }, {
                        "mData": "name"
                    }, {
                        "mData": "statement_balance"
                    }, {
                        "mData": "reconciliation_date"
                    }, {
                        "mData": "last_reconciliation_date"
                    }, {
                        "mData": "reconciliation_close_date"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [7]
                }]
            });
        }
    });
    


    $('#btnBackToList').click(function() {
        $('#ledgerList').show(); 
        $('.reconciliationTable').show();
        $('#bankReconciliationButtons').hide();
        $('.depositTable').hide();
        $('.bankingTable').hide();
        $('.paymentTable').hide();
        $('.journalVoucherTable').hide();
    });
    $('#depositPopUp').click(function() {
        $('#depositPopUp').addClass('activeSelf');
        $('#bankingPopUp').removeClass('activeSelf');
        $('#paymentsPopUp').removeClass('activeSelf');
        $('#journalVoucherPopUp').removeClass('activeSelf');
        $('.depositTable').removeAttr('style');
        $('.bankingTable').hide();
        $('.paymentTable').hide();
        $('.journalVoucherTable').hide();
    });

    $('#bankingPopUp').click(function() {
        $('#bankingPopUp').addClass('activeSelf');
        $('#depositPopUp').removeClass('activeSelf');
        $('#paymentsPopUp').removeClass('activeSelf');
        $('#journalVoucherPopUp').removeClass('activeSelf');
        $('.bankingTable').removeAttr('style');
        $('.depositTable').hide();
        $('.paymentTable').hide();
        $('.journalVoucherTable').hide();
    });
    $('#paymentsPopUp').click(function() {
        $('#paymentsPopUp').addClass('activeSelf');
        $('#journalVoucherPopUp').removeClass('activeSelf');
        $('#bankingPopUp').removeClass('activeSelf');
        $('#depositPopUp').removeClass('activeSelf');
        $('.paymentTable').removeAttr('style');
        $('.journalVoucherTable').hide();
        $('.depositTable').hide();
        $('.bankingTable').hide();
    });
    $('#journalVoucherPopUp').click(function() {
        $('#journalVoucherPopUp').addClass('activeSelf');
        $('#paymentsPopUp').removeClass('activeSelf');
        $('#bankingPopUp').removeClass('activeSelf');
        $('#depositPopUp').removeClass('activeSelf');
        $('.journalVoucherTable').removeAttr('style');
        $('.depositTable').hide();
        $('.bankingTable').hide();
        $('.paymentTable').hide();
    });
});

function editClick(id,ledgerId,paymentModeId,name,statementBalance,reconciliationDate,lastReconcilation,reconcilationClosedDate) {
    
    $('#depositPopUp').click();
    $('#ledgerList').hide(); 
    $('.reconciliationTable').hide();
    $('#bankReconciliationButtons').removeAttr('style');

    if(initTable != true){
        tblBanking.fnClearTable();
        tblBanking.fnDestroy();
        tblDeposit.fnClearTable();
        tblDeposit.fnDestroy();
        tblPayment.fnClearTable();
        tblPayment.fnDestroy();
        tblJournalVoucher.fnClearTable();
        tblJournalVoucher.fnDestroy();
    }
    else{
        initTable = false;
    }

    //...............Datatable for Banking Items..............................................................................
    tblBanking = $('#bankingTable').dataTable({ 
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "fnDrawCallback": function(oSettings) {
           
        },

        "sAjaxSource": "controllers/admin/bank_reconciliation.php",
        "fnServerParams": function(aoData) {
            aoData.push({
                "name": "operation",
                "value": "showBankingItems"
            });
            aoData.push({
                "name": "ledger",
                "value": ledgerId
            });
        },
        "aoColumns": [
            {   
                "mData": function (o) {     
                    var id = o["receipt_id"];
                    var receiptPrefix = o["receipt_prefix"];
                        
                    var visitIdLength = id.length;
                    for (var i=0;i<6-visitIdLength;i++) {
                        id = "0"+id;
                    }
                    id = receiptPrefix+id;
                    return id; 
                }
                
            }, {
                "mData": "name"
            }, {
                "mData": "amount"
            }, {
                "mData": "reference"
            }, {
                "mData": "date"
            }, 
        ],
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [4]
        }]
    });
//.................Data Table for Banking Items.............................................................................


//...............Datatable for Payment..............................................................................
    tblPayment = $('#paymentTable').dataTable({ 
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "fnDrawCallback": function(oSettings) {
           
        },

        "sAjaxSource": "controllers/admin/bank_reconciliation.php",
        "fnServerParams": function(aoData) {
            aoData.push({
                "name": "operation",
                "value": "showPayment"
            });
            aoData.push({
                "name": "ledger",
                "value": ledgerId
            });
        },
        "aoColumns": [
            {
                "mData": function (o) {     
                    var id = o["id"];
                    var receiptPrefix = o["voucher_prefix"];
                        
                    var visitIdLength = id.length;
                    for (var i=0;i<6-visitIdLength;i++) {
                        id = "0"+id;
                    }
                    id = receiptPrefix+id;
                    return id; 
                }
            }, {
                "mData": "payee_name"
            }, {
                "mData": "ledger_name"
            }, {
                "mData": "amount"
            }, {
                "mData": "name"
            }, {
                "mData": "cheque_no"
            }, 
        ],
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [4]
        }]
    });
//.................Data Table for Payment.............................................................................


//...............Datatable for Direct Deposit..............................................................................
    tblDeposit = $('#depositTable').dataTable({ 
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "fnDrawCallback": function(oSettings) {
           
        },

        "sAjaxSource": "controllers/admin/bank_reconciliation.php",
        "fnServerParams": function(aoData) {
            aoData.push({
                "name": "operation",
                "value": "showDepositIntem"
            });
            aoData.push({
                "name": "ledger",
                "value": ledgerId
            });
        },
        "aoColumns": [
            {
                "mData": function (o) {     
                    var id = o["id"];
                    var receiptPrefix = o["receipt_prefix"];
                        
                    var visitIdLength = id.length;
                    for (var i=0;i<6-visitIdLength;i++) {
                        id = "0"+id;
                    }
                    id = receiptPrefix+id;
                    return id; 
                }
            }, {
                "mData": "reference"
            }, {
                "mData": "transaction_date"
            }, {
                "mData": "document_date"
            }, {
                "mData": "name"
            }, {
                "mData": "amount"
            }, {
                "mData": function (o) {     //function for checkbox  
                    return "<input type='checkbox'  class='chkbox' style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;'"+                  
                       " style='font-size: 22px; cursor:pointer;'>"; 
                }
            },
        ],
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [4]
        }]
    });
//.................Data Table for Direct Deposit............................................................................

    //...............Datatable for Journal Voucher..............................................................................
    tblJournalVoucher = $('#journalVoucherTable').dataTable({ 
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "fnDrawCallback": function(oSettings) {
           
        },

        "sAjaxSource": "controllers/admin/bank_reconciliation.php",
        "fnServerParams": function(aoData) {
            aoData.push({
                "name": "operation",
                "value": "showJournalVoucher"
            });
            aoData.push({
                "name": "ledger",
                "value": ledgerId
            });
        },
        "aoColumns": [
            {
                "mData": function (o) {     
                    var id = o["id"];
                    var receiptPrefix = o["voucher_prefix"];
                        
                    var visitIdLength = id.length;
                    for (var i=0;i<6-visitIdLength;i++) {
                        id = "0"+id;
                    }
                    id = receiptPrefix+id;
                    return id; 
                }
            }, {
                "mData": "cheque_no"
            }, {
                "mData": "date"
            }, {
                "mData": "payee_name"
            }, {
                "mData": "amount"
            },  {
                "mData": function (o) {     //function for checkbox  
                    return "<input type='checkbox'  class='chkbox' style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;'"+                  
                       " style='font-size: 22px; cursor:pointer;'>"; 
                }
            }, 
        ],
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [4]
        }]
    });
//.................Data Table for Journal Voucher.............................................................................

    
} // end update button

function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Bank Reconciliation");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteBankReconciliation("' + type + '","'+id+'");');
} // end click fucntion

function restoreClick(id) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Bank Reconciliation");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteBankReconciliation("' + type + '","'+id+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteBankReconciliation(type) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/bank_reconciliation.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Bank Reconciliation deleted successfully!!!");
                    $('#messagemyModal').modal();
                    bankReconciliationTable.fnReloadAjax();
                } 
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/bank_reconciliation.php",
            data: {
                "operation": "restore",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Bank Reconciliation restored successfully!!!");
                    $('#messagemyModal').modal();
                    bankReconciliationTable.fnReloadAjax();
                }
            },
            error: function() {             
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary unavailable to respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}
function tabledgerList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
    $("#tabAddledger").css({
        "background": "#fff",
        "color": "#000"
    });
    $("#tabledgerList").css({
        "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
        "color": "#fff"
    });
    $("#ledgerList").css({
        "background-color": "#fff"
    });
    $('#inactive-checkbox-tick').prop('checked', false).change();
    clear();
}
function bindPaymentMode(ledgerId){
    var postData = {
        ledgerId:ledgerId,
        "operation":"ShowPaymentMode"
     }
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/bank_reconciliation.php",
            datatype:"json",
            data: postData,
            success: function(data) {
                if (data != null && data != "") {
                    var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                    var option = "<option value=''>--Select--</option>";
                    for (var i = 0; i < parseData.length; i++) {
                        option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                    }
                    $('#selPaymentMode').html(option);
                }
            },
            error: function() {}
        });  
}

function clear() {
    removeErrorMessage();
    clearFormDetails(".clearForm"); 
    $('#selLedger').focus();
}