var assetCountPeriodTable;
$(document).ready(function(){
    loader();
    debugger;
	
	/*Hide add ward by default functionality*/
	$("#advanced-wizard").hide();
	$("#ledgerList").addClass('list');
	$("#tabledgerList").addClass('tab-list-add');

		
	/*Click for add the ledger*/
    $("#tabAddledger").click(function() {
        $("#advanced-wizard").show();
        $(".blackborder").hide();
		clear();
        $("#tabAddledger").addClass('tab-detail-add');
        $("#tabAddledger").removeClass('tab-detail-remove');
        $("#tabledgerList").removeClass('tab-list-add');
        $("#tabledgerList").addClass('tab-list-remove');
        $("#ledgerList").addClass('list');
        $('#txtName').focus();
        $('#txtPeriod').focus();
        $('#btnSubmit').show();
		$('#btnReset').show();
		$('#btnUpdate').hide();
    });
	
	//Click function for show the ledger lists
    $("#tabledgerList").click(function() {
        clear();
        tabledgerList();
    });

    removeErrorMessage();

    var startDate = new Date();
    $("#txtStartDate").datepicker({ changeYear: true,changeMonth: true,autoclose: true}) .on('changeDate', function(selected){
        startDate = new Date(selected.date.valueOf());
        $('#txtEndDate').datepicker('setStartDate', startDate);
    }); 
    $('#txtEndDate').datepicker({
        changeYear: true,
        changeMonth: true,
        autoclose: true
    });
    
    $(".input-datepicker").datepicker({ changeYear: true,changeMonth: true,autoclose: true});

    $('#btnSubmit').click(function() {
        var flag = validation();
        if(flag == true) {
            return false;
        }

        var period = $('#txtPeriod').val().trim();
        var startDate = $('#txtStartDate').val().trim();
        var endDate = $('#txtEndDate').val().trim();
        var description = $('#txtDescription').val().trim();

        var postData = {
            period : period,
            startDate : startDate,
            endDate : endDate,
            description : description,
            operation : "saveAssetCountPeriod"
        }


        $.ajax(
            {                   
            type: "POST",
            cache: false,
            url: "controllers/admin/asset_count_period.php",
            datatype:"json",
            data: postData,
            
            success: function(data) {
                if(data != "0" && data != ""){
                    callSuccessPopUp('Success','Saved successfully!!!');
                    tabledgerList();
                    assetCountPeriodTable.fnReloadAjax();
                    clear();
                }
                else {
                    $('#txtNameError').text('Ledger name is already exist');
                    $('#txtName').focus();
                    $('#txtName').addClass('errorStyle');
                }
            },
            error: function(){

            }
        });
    });
    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
    //Datatable code 
    assetCountPeriodTable = $('#assetCountPeriodTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = assetCountPeriodTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                 		var period = mData["period"];                      
                        var startDate = mData["start_date"];                      
                        var endDate = mData["end_date"];                      
                        var description = mData["description"];
                        editClick(id,period,startDate,endDate,description);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = assetCountPeriodTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/asset_count_period.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "period"
                },  {
                    "mData": "start_date"
                }, {
                    "mData": "end_date"
                }, {
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [4]
            }]

        });
    }

    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            assetCountPeriodTable.fnClearTable();
            assetCountPeriodTable.fnDestroy();
            assetCountPeriodTable = "";
            assetCountPeriodTable = $('#assetCountPeriodTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = assetCountPeriodTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            restoreClick(id);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/asset_count_period.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    {
	                    "mData": "period"
	                },  {
	                    "mData": "start_date"
	                }, {
	                    "mData": "end_date"
	                }, {
	                    "mData": "description"
	                }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [4]
                }]
            });
        } else { // show active data on unchecked   
            assetCountPeriodTable.fnClearTable();
            assetCountPeriodTable.fnDestroy();
            assetCountPeriodTable = "";
            assetCountPeriodTable = $('#assetCountPeriodTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = assetCountPeriodTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
	                 		var period = mData["period"];                      
	                        var startDate = mData["start_date"];                      
	                        var endDate = mData["end_date"];                      
	                        var description = mData["description"];
	                        editClick(id,period,startDate,endDate,description);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = assetCountPeriodTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/asset_count_period.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
	                    "mData": "period"
	                },  {
	                    "mData": "start_date"
	                }, {
	                    "mData": "end_date"
	                }, {
	                    "mData": "description"
	                }, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [4]
                }]
            });
        }
    });


    $('#btnReset').click(function(){
        clear();
        $('#txtPeriod').focus();
    });
});

function editClick(id,period,startDate,endDate,description){	
	$('#tabAddledger').html("+Update Asset Count Period");
	$('#tabAddledger').click();
	$('#btnSubmit').hide();
	$('#btnReset').hide();
	
	$("#btnUpdate").removeAttr("style");
	$("#txtLabTypeError").text("");
    $("#txtLabType").removeClass("errorStyle");
	$('#txtPeriod').val(period);
	$('#txtStartDate').val(startDate);
	$('#txtEndDate').val(endDate);
	$('#txtDescription').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id);
	$("#uploadFile").hide();
	//Click function for update the data
	$("#btnUpdate").click(function(){
		var flag = validation();
		if(flag == true){			
			return false;
		}
		else{
			var period = $('#txtPeriod').val().trim();
	        var startDate = $('#txtStartDate').val().trim();
	        var endDate = $('#txtEndDate').val().trim();
			var description = $("#txtDescription").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();			
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){	
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/asset_count_period.php",
					data :{            
						"operation" : "update",
						"id" : id,
						"period":period,
						"startDate":startDate,
						"endDate":endDate,
						"description":description
					},
					success: function(data) {	
						if(data != "0" && data != ""){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Asset count period updated successfully!!!");
							assetCountPeriodTable.fnReloadAjax();
							$('#messagemyModal').modal();
							clear();
							$('#tabledgerList').click();
						}
					},
					error:function()
					{
						$('.close-confirm').click();
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();			  
					}
				});
			});
		}
	});
}
function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Asset Count Period");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteAssetCountPeriiod("' + type + '","'+id+'");');
} // end click fucntion

function restoreClick(id) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Asset Count Period");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteAssetCountPeriiod("' + type + '","'+id+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteAssetCountPeriiod(type,id) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/asset_count_period.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Asset count period deleted successfully!!!");
                    $('#messagemyModal').modal();
                    assetCountPeriodTable.fnReloadAjax();
                } 
            },
            error: function() {
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/asset_count_period.php",
            data: {
                "operation": "restore",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Asset count period restored successfully!!!");
                    $('#messagemyModal').modal();
                    assetCountPeriodTable.fnReloadAjax();
                }
            },
            error: function() {             
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary unavailable to respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}


function tabledgerList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
    $("#tabAddledger").html('+Add Asset Count Period');
    $("#tabAddledger").removeClass('tab-detail-add');
    $("#tabAddledger").addClass('tab-detail-remove');
    $("#tabledgerList").addClass('tab-list-add');
    $("#tabledgerList").removeClass('tab-list-remove');
    $("#ledgerList").addClass('list');
    clear();
}
function validation(){
	var flag = false;

        if($('#txtEndDate').val() == '') {
            $('#txtEndDateError	').text('Please enter end date');
            $('#txtEndDate').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtStartDate').val() == '') {
            $('#txtStartDateError').text('Please enter start date');
            $('#txtStartDate').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtPeriod').val().trim() == '') {
            $('#txtPeriodError').text('Please enter period');
            $('#txtPeriod').focus();
            $('#txtPeriod').addClass("errorStyle"); 
            flag = true;
        }
        return flag;
}
function clear(){
	removeErrorMessage();
	clearFormDetails('.clearForm');
}