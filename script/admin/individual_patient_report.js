$(document).ready(function() {
	debugger;
	$(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});

	$("#toDate").attr("disabled",true);
	$("#fromDate").attr("disabled",true);	

	$('.changeRadio').change(function() {
		if ($('#radToFromDate').prop( "checked") == true) {
	        $("#toDate").attr("disabled",false);
			$("#fromDate").attr("disabled",false);
	    }
	    else{
	    	$("#toDate").attr("disabled",true);
			$("#fromDate").attr("disabled",true);
			$("#toDate").val("");
			$("#fromDate").val("");
	    }
	});	
	google.charts.setOnLoadCallback(drawChart);
});

function drawChart() {
	var dataLoad;
	var chartWidth  = $("#page-content").width()/2 -25;
	var bigArrayLabBill = [['Paid','Unpaid']];
	var bigArrayRadiologyBill = [['Paid','Unpaid']];
	var bigArrayServiceBill = [['Paid','Unpaid']];
	var bigArrayProcedureBill = [['Paid','Unpaid']];
	var bigArrayForensicBill = [['Paid','Unpaid']];

	var bigArrayGpLabBill = [['Test Name','Paid','Unpaid']];
	var bigArrayGpRadiologyBill = [['Test Name','Paid','Unpaid']];
	var bigArrayGpServiceBill = [['Test Name','Paid','Unpaid']];
	var bigArrayGpProcedureBill = [['Test Name','Paid','Unpaid']];
	var bigArrayGpForensicBill = [['Test Name','Paid','Unpaid']];

	$("#viewReport").on("click",function(){
		var fromDate ;
		var toDate ;
		if ($('#radToday').prop( "checked") == true) {
			dataLoad = "today";
		}
		else if($('#radLastSevenDays').prop( "checked") == true) {
			dataLoad = "last7days";
		}
		else if($('#radToFromDate').prop( "checked") == true) {
			dataLoad = "betweenDate";
			fromDate = $("#fromDate").val();
			toDate = $("#toDate").val();
		}
		else if($('#radLastThirtyDays').prop( "checked") == true) {
			dataLoad = "last30days";
		}
		else if($('#radAllTime').prop( "checked") == true) {
			dataLoad = "all";
		}

		if (fromDate == undefined) {
			fromDate ="";
		}
		if (toDate == undefined) {
			toDate = "";
		}
		if (dataLoad==undefined) {
			return false;
		}
		var postData = {
			"operation" : "showChartData",
			"dataLoad" : dataLoad,
			"fromDate" : fromDate,
			"toDate" : toDate
		}
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/individual_patient_report.php",
			data: postData,
			success: function(data) {
				if(data != null && data != ""){
					var parseData =  JSON.parse(data);
					bigArrayLabBill = [['Paid','Unpaid']];
					bigArrayRadiologyBill = [['Paid','Unpaid']];
					bigArrayServiceBill = [['Paid','Unpaid']];
					bigArrayProcedureBill = [['Paid','Unpaid']];
					bigArrayForensicBill = [['Paid','Unpaid']];

					bigArrayGpLabBill = [['Test Name','Paid','Unpaid']];
					bigArrayGpRadiologyBill = [['Test Name','Paid','Unpaid']];
					bigArrayGpServiceBill = [['Test Name','Paid','Unpaid']];
					bigArrayGpProcedureBill = [['Test Name','Paid','Unpaid']];
					bigArrayGpForensicBill = [['Test Name','Paid','Unpaid']];

					$.each(parseData,function(index,v){
						for(var i=0;i<parseData[index].length;i++){
							if(index==0){

								smallArrayGpLabBill = [];
								smallArrayGpLabBill.push(parseData[index][i].name);
								smallArrayGpLabBill.push(parseInt(parseData[index][i].paid));
								smallArrayGpLabBill.push(parseInt(parseData[index][i].unpaid));
								bigArrayGpLabBill.push(smallArrayGpLabBill);
							}
							else if (index == 1) {

								$.each(parseData[index][i],function(im,nn){
									bigArrayLabBill.push([im,parseInt(nn)]);
								});
							}

							else if (index == 2) {

								smallArrayGpRadiologyBill = [];
								smallArrayGpRadiologyBill.push(parseData[index][i].name);
								smallArrayGpRadiologyBill.push(parseInt(parseData[index][i].paid));
								smallArrayGpRadiologyBill.push(parseInt(parseData[index][i].unpaid));
								bigArrayGpRadiologyBill.push(smallArrayGpRadiologyBill);
							}

							else if (index == 3) {

								$.each(parseData[index][i],function(im,nn){
									bigArrayRadiologyBill.push([im,parseInt(nn)]);
								});
							}

							else if (index == 4) {

								smallArrayGpServiceBill = [];
								smallArrayGpServiceBill.push(parseData[index][i].name);
								smallArrayGpServiceBill.push(parseInt(parseData[index][i].paid));
								smallArrayGpServiceBill.push(parseInt(parseData[index][i].unpaid));
								bigArrayGpServiceBill.push(smallArrayGpServiceBill);
							}

							else if (index == 5) {

								$.each(parseData[index][i],function(im,nn){
									bigArrayServiceBill.push([im,parseInt(nn)]);
								});
							}	

							else if (index == 6) {

								smallArrayGpProcedureBill = [];
								smallArrayGpProcedureBill.push(parseData[index][i].name);
								smallArrayGpProcedureBill.push(parseInt(parseData[index][i].paid));
								smallArrayGpProcedureBill.push(parseInt(parseData[index][i].unpaid));
								bigArrayGpProcedureBill.push(smallArrayGpProcedureBill);
							}

							else if (index == 7) {
								
								$.each(parseData[index][i],function(im,nn){
									bigArrayProcedureBill.push([im,parseInt(nn)]);
								});
							}

							else if (index == 8) {

								smallArrayGpForensicBill = [];
								smallArrayGpForensicBill.push(parseData[index][i].name);
								smallArrayGpForensicBill.push(parseInt(parseData[index][i].paid));
								smallArrayGpForensicBill.push(parseInt(parseData[index][i].unpaid));
								bigArrayGpForensicBill.push(smallArrayGpForensicBill);
							}

							else if (index == 9) {
								$.each(parseData[index][i],function(im,nn){
									bigArrayForensicBill.push([im,parseInt(nn)]);
								});								
							}
						}
					});

					var dataLabBill = google.visualization.arrayToDataTable(bigArrayLabBill);
					var dataGpLabBill = google.visualization.arrayToDataTable(bigArrayGpLabBill);
					var dataRadiologyBill = google.visualization.arrayToDataTable(bigArrayRadiologyBill);
					var dataGpRadiologyBill = google.visualization.arrayToDataTable(bigArrayGpRadiologyBill);
					var dataServiceBill = google.visualization.arrayToDataTable(bigArrayServiceBill);
					var dataGpServiceBill = google.visualization.arrayToDataTable(bigArrayGpServiceBill);
					var dataProcedureBill = google.visualization.arrayToDataTable(bigArrayProcedureBill);
					var dataGpProcedureBill = google.visualization.arrayToDataTable(bigArrayGpProcedureBill);
					var dataForensicBill = google.visualization.arrayToDataTable(bigArrayForensicBill);
					var dataGpForensicBill = google.visualization.arrayToDataTable(bigArrayGpForensicBill);


					var optionsLabBill = {
				      title: 'Lab Bill Analysis','width':chartWidth,'height':500
				    };
				    var optionsGpLabBill = {
				      title: 'Lab Bill Analysis','width':chartWidth,'height':500
				    };
				    var optionsRadiologyBill = {
				      title: 'RadiologyBill Analysis','width':chartWidth,'height':500
				    };
				    var optionsGpRadiologyBill = {
				      title: 'RadiologyBill Analysis','width':chartWidth,'height':500
				    };
				    var optionsServiceBill = {
				      title: 'ServiceBill Analysis','width':chartWidth,'height':500
				    };
				    var optionsGpServiceBill = {
				      title: 'ServiceBill Analysis','width':chartWidth,'height':500
				    };
				    var optionsProcedureBill = {
				      title: 'ProcedureBill Analysis','width':chartWidth,'height':500
				    };
				    var optionsGpProcedureBill = {
				      title: 'ProcedureBill Analysis','width':chartWidth,'height':500
				    };
				    var optionsForensicBill = {
				      title: 'ForensicBill Analysis','width':chartWidth,'height':500
				    };
				    var optionsGpForensicBill = {
				      title: 'ForensicBill Analysis','width':chartWidth,'height':500
				    };


				    var chartLabBill = new google.visualization.PieChart(document.getElementById('chart_div'));
				    chartLabBill.draw(dataLabBill, optionsLabBill);

				    var chartRadiologyBill = new google.visualization.PieChart(document.getElementById('chart_div2'));
				    chartRadiologyBill.draw(dataRadiologyBill, optionsRadiologyBill);

				    var chartServiceBill = new google.visualization.PieChart(document.getElementById('chart_div3'));
				    chartServiceBill.draw(dataServiceBill, optionsServiceBill);

				    var chartProcedureBill = new google.visualization.PieChart(document.getElementById('chart_div4'));
				    chartProcedureBill.draw(dataProcedureBill, optionsProcedureBill);

				    var chartForensicBill = new google.visualization.PieChart(document.getElementById('chart_div5'));
				    chartForensicBill.draw(dataForensicBill, optionsForensicBill);


				    /*GroupChart BarChart*/


				    var chartGpLabBill = new google.visualization.BarChart(document.getElementById('chart_div6'));
				    chartGpLabBill.draw(dataGpLabBill, optionsGpLabBill);

				    var chartGpRadiologyBill = new google.visualization.BarChart(document.getElementById('chart_div7'));
				    chartGpRadiologyBill.draw(dataGpRadiologyBill, optionsGpRadiologyBill);

				    var chartGpServiceBill = new google.visualization.BarChart(document.getElementById('chart_div8'));
				    chartGpServiceBill.draw(dataGpServiceBill, optionsGpServiceBill);

				    var chartGpProcedureBill = new google.visualization.BarChart(document.getElementById('chart_div9'));
				    chartGpProcedureBill.draw(dataGpProcedureBill, optionsGpProcedureBill);

				    var chartGpForensicBill = new google.visualization.BarChart(document.getElementById('chart_div10'));
				    chartGpForensicBill.draw(dataGpForensicBill, optionsGpForensicBill);


				}
			}
		});
	});
}