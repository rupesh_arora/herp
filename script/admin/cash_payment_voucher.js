var mainCashPaymentVoucherTable;
var CashPaymentVoucherTbl;
var addCashPaymentVoucherTable;
$(document).ready(function(){
    loader();
    var matchArray = [];

    loadTable();    // load datatable of banking schedule
    bindLedger();

    $("#form_dept").hide();
    $("#designation_list").css({
        "background": "#fff"
    });
    $("#tab_dept").css({
        "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
        "color": "#fff"
    });
    $("#tab_add_dept").click(function() { // click on add designation tab
        $("#form_dept").show();
        $(".blackborder").hide();
        $("#tab_add_dept").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tab_dept").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#designation_list").css({
            "background": "#fff"
        });
        clearFormDetails(".clearForm");
        removeErrorMessage();
    });
    $("#tab_dept").click(function() { // click on list designation tab
        $('#inactive-checkbox-tick').prop('checked', false).change();
        $("#form_dept").hide();
        $(".blackborder").show();
        $("#tab_add_dept").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#tab_dept").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#designation_list").css({
            "background": "#fff"
        });
        removeErrorMessage();
    });

    
    removeErrorMessage();
    debugger;
    mainCashPaymentVoucherTable = $('#mainCashPaymentVoucherTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "order": [[ 2, 'asc' ]],
        aoColumnDefs: [{'bSortable': false,'aTargets': [3] }],
        "fnDrawCallback": function(oSettings) {}
    });
    addCashPaymentVoucherTable = $('#addCashPaymentVoucherTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "order": [[ 2, 'asc' ]],
        "fnDrawCallback": function(oSettings) {}
    });

    $("#selLedger").change(function(){
        if ($(this).val() != '') {
            paymentMode($(this).val(),'',"#selPaymentMode");
            $("#selLedger").removeClass('errorStyle');
            $("#selLedgerError").text('');          
        }
    });

    if($('.inactive-checkbox').not(':checked')){
        //Datatable code
        loadDataTable();
    }

    $("#btnAddDetail").on('click',function(){
        var flag = false;
        var source = $("#selSource").val();
        if ($("#txtCashierDetails").val() == '') {
            $("#txtCashierDetails").focus();
            $("#txtCashierDetails").addClass('errorStyle');
            $("#txtCashierDetailsError").text("Please enter cashier detail.");
            flag = true;
        }   
        if(flag == true){
            return false;
        }       
        if (addCashPaymentVoucherTable != undefined) {
            addCashPaymentVoucherTable.fnClearTable();
            addCashPaymentVoucherTable.fnDestroy();
            addCashPaymentVoucherTable = "";
        }
        addCashPaymentVoucherTable = $('#addCashPaymentVoucherTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "bAutoWidth": false,
            "order": [[ 2, 'asc' ]],
            aoColumnDefs: [{'bSortable': false,'aTargets': [0] }],
            "fnDrawCallback": function(oSettings) {}
        });     
        var postData = {
            operation : "showPettyCash",
            source : source
        }
        dataCall("controllers/admin/cash_payment_voucher.php", postData, function (result){
            if (result.length > 2) {
                $.each(JSON.parse(result),function(i,v){
                    var pettyId = v.id;                                    
                    var pettyIdLength = pettyId.length;
                    for (var j=0;j<6-pettyIdLength;j++) {
                        pettyId = "0"+pettyId;
                    }
                    pettyId = pettyPrefix+pettyId;
                    var pettyDetail = '<span value='+v.id+'>'+pettyId+'</span>';
                    var checkBox = '<input type="checkbox" class="addPettyCheckBox">';
                    var currData = addCashPaymentVoucherTable.fnAddData([checkBox,pettyDetail,v.staff_customer_name,v.amount])
                    var getCurrDataRow = addCashPaymentVoucherTable.fnSettings().aoData[ currData ].nTr;
                    //match petty cash already in datatable then check that
                    if (matchArray.length !=0) {
                        $.each(matchArray,function(ind,val){
                            var arrIntersect = jQuery.inArray( pettyId, val );
                            if (arrIntersect != -1) {
                                $(getCurrDataRow).find('td:eq(0) input').prop("checked",true);
                            }
                        });
                    }
                });
                $("#myModalTable").modal();
            }
            else{
                callSuccessPopUp('Alert','No data exist for choosen source.');
            }
        });
    });

    $('#btnReset').click(function() {
        clear();
    });
    

    $("#btnAddReceipt").on('click',function(){
        var arr = [];
        matchArray = [];
        var saveArray = [];
        var totalAmount = 0;
        $.grep(addCashPaymentVoucherTable.fnGetNodes(), function( n, i ){
            if($(n).find('td:eq(0) input').prop('checked') == true){
                var array = [];
                /*array.push($(n).find('td:eq(1) span').attr('value'));*/
                array.push($(n).find('td:eq(1) span').text());
                array.push($(n).find('td:eq(2)').text());
                array.push($(n).find('td:eq(3)').text());              
                arr.push(array);
                matchArray.push(array);
            }
        });
        if (arr.length == 0) {
            callSuccessPopUp("Alert","Please insert new data in table");
            flag = true;
            return false;
        }
        mainCashPaymentVoucherTable.fnClearTable();
        var removeBtn = [];
        removeBtn.push('<input type="button" class="btnEnabled" onclick="rowDelete($(this))" value="Remove">');
        $.each(arr,function(i,v){
            var mergeArray = v.concat(removeBtn);
            mainCashPaymentVoucherTable.fnAddData(mergeArray);
        });
        $("#myModalTable").modal('hide');
        $.grep(mainCashPaymentVoucherTable.fnGetNodes(),function(v,i){
            var obj = {};
            obj.pettyId = parseInt($(v).find('td:eq(0)').text().replace ( /[^\d.]/g, '' ));
            obj.amount = $(v).find('td:eq(2)').text();
            totalAmount=totalAmount+parseFloat(obj.amount);
            saveArray.push(obj);
        });
        $("#txtTotalAmount").val(totalAmount);
    });

        
        

    $("#btnSave").on('click',function(){
        var flag = 'false';
        if(validTextField('#txtRefNo','#txtRefNoError','Please enter reference no') == true)
        {
            flag = true;
        }
        if(validTextField('#selPaymentMode','#selPaymentModeError','Please select payment mode') == true)
        {
            flag = true;
        }
        if(validTextField('#selLedger','#selLedgerError','Please select ledger') == true)
        {
            flag = true;
        }
        if(validTextField('#txtTotalAmount','#txtTotalAmountError','Please enter total amount') == true)
        {
            flag = true;
        }
        if (flag == true) {
            return false;
        }
        
        var saveArray = [];
        var totalAmount = 0;
        $.grep(mainCashPaymentVoucherTable.fnGetNodes(),function(v,i){
            var obj = {};
            obj.pettyCashId = parseInt($(v).find('td:eq(0)').text().replace ( /[^\d.]/g, '' ));
            obj.amount = $(v).find('td:eq(2)').text();
            totalAmount+=totalAmount+parseFloat(obj.amount);
            saveArray.push(obj);
        });
        var cashierDetail = $("#txtCashierDetails").val();
        var totalAmount1 = $("#txtTotalAmount").val();
        var ledger_id = $("#selLedger").val();
        var paymentMode = $("#selPaymentMode").val();
        var refNo = $("#txtRefNo").val();
        var notes = $("#txtNotes").val();
        totalAmount = totalAmount.toFixed(2);
        var postData = {
            operation : "saveData",
            cashierDetail : cashierDetail,
            totalAmount : totalAmount1 ,
            ledger_id : ledger_id ,
            paymentMode : paymentMode,
            refNo : refNo,
            notes : notes,
            saveArray : JSON.stringify(saveArray)
        }
        dataCall("controllers/admin/cash_payment_voucher.php", postData, function (result){
            if (result =='1') {
                callSuccessPopUp("Success","Cash payment voucher saved successfully.");
                mainCashPaymentVoucherTable.fnClearTable();
                addCashPaymentVoucherTable.fnClearTable();
                clearFormDetails('.clearForm');
                $('#tab_dept').click();
                CashPaymentVoucherTbl.fnClearTable();
                CashPaymentVoucherTbl.fnDestroy();
                loadTable();

            }
        });

    });

    $("#selSource").on('change',function(){
        mainCashPaymentVoucherTable.fnClearTable();
        matchArray = [];
    });
});

function rowDelete(currInst){
    var row = currInst.closest("tr").get(0);
    mainCashPaymentVoucherTable.fnDeleteRow(row);
}
function loadDataTable(){
    
}

function viewClick(receipt_id,amount,ledgerName,paymentModeName,date,cashierName) {
    $('#myCashPaymentVoucherModel').modal("show");
    $('#CashPaymentVoucherModelLabel').text("Cash Payment Voucher Details");

    var receiptIdLength = receipt_id.length;
    for (var j=0;j<6-receiptIdLength;j++) {
        receipt_id = "0"+receipt_id;
    }
    receipt_id = receiptprefix+receipt_id;

    $('#lblReceiptNo').text(receipt_id);
    $('#lblCashierName').text(cashierName);
    $('#lblLedgerName').text(ledgerName);
    $('#lblPaymentMode').text(paymentModeName);
    //$('#lblAmount').text(amount);
    $('#lblDate').text(date);

    var amount = amountPrefix + ' ' + amount;

    $('#lblAmount').text(amount);
    //$('#lblReference').text(payment_mode_id);
    $('#lblDate').text(date);

}




function clear() {
    $('#selLedger').val('');
    $('#selLedgerError').text('');
    $('#selLedger').removeClass('errorStyle');

    $('#selSource').val('');
    $('#selSourceError').text('');
    $('#selSource').removeClass('errorStyle');

    $('#selBank').val('');
    $('#selBankError').text('');
    $('#selBank').removeClass('errorStyle');

    $('#selLedger').focus();
}
function loadTable() {
    CashPaymentVoucherTbl = $('#CashPaymentVoucherTbl').dataTable( {
        "bFilter": true,
        "processing": true,
        "sPaginationType":"full_numbers",
        "fnDrawCallback": function ( oSettings ) {

            /*On click of update icon call this function*/
            //$('.iconCashPaymentVoucher ').unbind();
            $('.iconCashPaymentVoucher ').on('click',function(){
                var data=$(this).parents('tr')[0];
                var mData = CashPaymentVoucherTbl.fnGetData(data);//get datatable data
                if (null != mData)  // null if we clicked on title row
                {
                    //var id = mData["id"];
                    var receipt_id = mData["id"];
                    var amount = mData["total_amount"];
                    var ledgerName = mData["ledger_name"];
                    var paymentModeName = mData["payment_mode_name"];
                    var date = mData["cpv_date"];
                    var cashierName = mData["cashier_name"];
                    viewClick(receipt_id,amount,ledgerName,paymentModeName,date,cashierName);//call edit click function with certain parameters to update               
                }
            });
            
        },
        
        "sAjaxSource":"controllers/admin/cash_payment_voucher.php",
        "fnServerParams": function ( aoData ) {
          aoData.push( { "name": "operation", "value": "show" });
        },
        "aoColumns": [
            { "mData": "ledger_name" },
            { "mData": "cashier_name" },
            {  
                "mData": function (o) { 
                    /*Return update and delete data id*/
                    return "<i class='iconCashPaymentVoucher fa fa-eye' id='btnView' title='View' ></i>"; 
                }
            },  
        ],
        /*Disable sort for following columns*/
        aoColumnDefs: [
           { 'bSortable': false, 'aTargets': [ 2 ] }
       ]
    });
}
function bindLedger() {     //  function for loading ledger
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/payment_mode.php",
        data: {
            "operation": "showLedger"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option data-value='" + parseData[i].value + "' value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selLedger').html(option);
            }
        },
        error: function() {}
    });
}
function paymentMode(ledgerId,payment_mode,selField){
    var postData = {
        operation : "showPaymentMode",
        ledgerId : ledgerId
    }
    dataCall("controllers/admin/payment_voucher.php", postData, function (result){
        if (result !='') {
            var parseData = JSON.parse(result);
            var option = "<option value=''>--Select--</option>";
            $.each(parseData,function(i,v){
                option += "<option value='" + v.id + "'>" +v.name+"</option>";
            });
            $(selField).html(option); 
            if (payment_mode !='' && payment_mode !=null) {
                $(selField).val(payment_mode);
            }
        }
    });
}