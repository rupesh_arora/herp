var addTreatmentsTable;  // define variable for initialize the table 
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   add_treatments.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   1st mar, 2016
 * Description  :   This page  manages  treatments data
 *************************************************************************************************** */
  loader(); // Calling loader function 
  debugger;

  //Initialize the table
 	addTreatmentsTable = $('#tblAddTreatments').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {},
        aoColumnDefs: [{'bSortable': false,'aTargets': [2] }]
	});

 	//Datepicker function
	 $("#txtDate").datepicker({        
        changeYear: true,
        changeMonth: true,
        autoclose: true,
        startDate: '+0d',
    });

	 //Click event for search icon
	$("#searchPatientIcon").click(function(){
		$("#txtIPDId").val("")
		$("#txtIPDError").text("");
		$("#txtIPDId").removeClass("errorStyle");
		//Open modal and load view in patient screen 
		$("#hdnIPD").val("1");
        $('#BedTransferModalLabel').text("Search IPD");
        $('#BedTransferModalBody').load('views/admin/view_inpatients.html');
        $('#myBedTransferModal').modal();
	});

	//Click event for add treatment 
	$("#btnAddTreatment").click(function(){
		var flag = "false";
		if($("#selShift").val() == "-1"){
			$("#selShift").focus();
			$("#selShiftError").text("Please select shift");
			$("#selShift").addClass("errorStyle");
			flag = "true";
		}
		if($("#txtDate").val() == ""){
			$("#txtDate").focus();
			$("#txtDateError").text("Please select date");
			$("#txtDate").addClass("errorStyle");
			flag = "true";
		}
		if($("#txtIPDId").val() == ""){
			$("#txtIPDId").focus();
			$("#txtIPDError").text("Please enter ipd id");
			$("#txtIPDId").addClass("errorStyle");
			flag = "true";
		}
		if(flag == "true"){
			return false;
		}
		else{

			//Open modal and load treatment screen
			$("#hdnAddTreatment").val("1");
	        $('#AddTreatmentModalLabel').text("Search Treatment");
	        $('#AddTreatmentModalBody').load('views/admin/treatments.html');
	        $('#myAddTreatmentModal').modal();
		}
	});

	//click event for save the data 
	$("#btnSave").click(function(){
		var flag = "false";

		if($("#txtIPDId").val() == ""){
			$("#txtIPDId").focus();
			$("#txtIPDError").text("Please enter ipd id");
			$("#txtIPDId").addClass("errorStyle");
			flag = "true";
		}

		if($("#txtDate").val() == ""){
			$("#txtDate").focus();
			$("#txtDateError").text("Please select date");
			$("#txtDate").addClass("errorStyle");
			flag = "true";
		}

		if($("#selShift").val() == "-1"){
			$("#selShift").focus();
			$("#selShiftError").text("Please select shift");
			$("#selShift").addClass("errorStyle");
			flag = "true";
		}

		if(flag == "true"){
			return false;
		}

		var treatments = [];  // Array for store the treatment from table 
		var aiRows = addTreatmentsTable.fnGetNodes();  //Get all rows of table 

		for (i=0; i<aiRows.length; i++) {
			var ObjData = {};
			ObjData.id = $($(aiRows[i]).find('td')[2]).find('.remove').attr('data-id');
			treatments.push(ObjData);
		}

		if(treatments.length == 0){
			$('#messageMyModalLabel').text("Sorry");
			$('.modal-body').text("Please add treatment!!!");
			$('#messagemyModal').modal();
			return false;
		}

		var Ipdid = $("#txtIPDId").val().replace ( /[^\d.]/g, '' ); ;
		var date = $("#txtDate").val();
		var shift = $("#selShift").val();
		var patientId = $("#txtIPDId").attr('patientid');
		var postData = {
			"operation":"save",				
			"treatments":JSON.stringify(treatments),				
			"shift":shift,
			"date":date,
			"Ipdid":Ipdid,
			"patientId":patientId
		}
		//Ajax call
		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/add_treatment.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data == "1"){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Saved successfully!!!");
					$('#messagemyModal').modal();
    				clearTreatment();								
				}
				if(data == "0") {
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("Insufficient balance!!!");
					$('#messagemyModal').modal();
				}
			},
			error : function(){
				$('.close-confirm').click();
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});

	});

	//keyup functionality for remove validation style
    $("#txtIPDId").keyup(function() {
        if ($("#txtIPDId").val() != '') {
            $("#txtIPDError").text("");
            $("#txtIPDId").removeClass("errorStyle");
        }
    });

    //keyup functionality for remove validation style
    $("#txtIPDId").change(function() {
        if ($("#txtIPDId").val() != '') {
            $("#txtIPDError").text("");
            $("#txtIPDId").removeClass("errorStyle");
        }
    });

    //keyup functionality for remove validation style
    $("#selShift").change(function() {
        if ($("#selShift").val() != '') {
            $("#selShiftError").text("");
            $("#selShift").removeClass("errorStyle");
        }
    });

    //keyup functionality for remove validation style
    $("#txtDate").change(function() {
        if ($("#txtDate").val() != '') {
            $("#txtDateError").text("");
            $("#txtDate").removeClass("errorStyle");
        }
    });

    //Click event for reset button
    $("#btnResetTreatment").click(function(){
    	clearTreatment();
    });

});

/**Row Delete functionality**/
function rowRadDelete(currInst) {
    var row = currInst.closest("tr").get(0);
    addTreatmentsTable.fnDeleteRow(row);

}

//Clear function 
function clearTreatment(){
	$("#txtIPDId").val("")
    $("#txtIPDError").text("");
    $("#txtIPDId").removeClass("errorStyle");
    $("#selShift").val('-1')
    $("#selShiftError").text("");
    $("#selShift").removeClass("errorStyle");
    $("#txtDate").val('')
    $("#txtDateError").text("");
    $("#txtDate").removeClass("errorStyle");
    addTreatmentsTable.fnClearTable();
}