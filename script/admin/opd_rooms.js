var opdRoomTable; 
$(document).ready(function() {
loader();
debugger;
/* ****************************************************************************************************
 * File Name    :   opd_rooms.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Arora
 * Created Date :   30th dec, 2015
 * Description  :   This page add and opd rooms
 *************************************************************************************************** */
	//
	/*Calling the function for loading the ward and department and consultant*/
	//
	bindOPDRooms();
	bindDepartment();
	bindConsultant();

	/*Hide second part of screen*/
	$("#advanced-wizard").hide();
	$("#OPDRoomsList").css({
        "background-color": "#fff"
    });
	$("#tabOPDRoomsList").css({
        "background-color": "#0c71c8",
        "color": "#fff"
    });
		
	//
	/*Click function for opd room ward*/
	//
    $("#tabAddOPDRooms").click(function() {
        $("#advanced-wizard").show();
        $(".blackborder").hide();
		clear();
        $("#tabAddOPDRooms").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tabOPDRoomsList").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#OPDRoomsList").css({
            "background": "#fff"
        });
		bindOPDRooms();
		bindDepartment();
		$('#selOPDRooms').focus();
    });
	
	/*
	Click function for show the list of opd rooms
	*/
    $("#tabOPDRoomsList").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
       tabOPDRoomsList();
    });
	
	//Ajax call for show the active opd rooms 
	//
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		loadDataTable();
    }
	
	//
	/*Change function for show the inactive opd rooms*/
	//
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	opdRoomTable.fnClearTable();
	    	opdRoomTable.fnDestroy();
	    	//Datatable code
			opdRoomTable = $('#OPDRoomsTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"bAutoWidth":false,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  opdRoomTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var roomNumber = mData["room_id"];
							var firstName = mData["users_id"];
							var department_id = mData["department_id"];
							restoreClick(id,roomNumber,firstName,department_id);//call restore click function with certain parameters to restore data
						}						
					});
				},
				
				"sAjaxSource":"controllers/admin/opd_rooms.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					{ "mData": "first_name" },
					{ "mData": "number" },
					{ "mData": "department" },	
					{  
						"mData": function (o) { 
						/*Return values that we get on click restore icon*/
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},	
				],
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 3 ] }
			    ]
			} );
		}
		else if($('.inactive-checkbox').not(':checked')){
			opdRoomTable.fnClearTable();
	    	opdRoomTable.fnDestroy();
	    	//Datatable code
			loadDataTable();
		}
    });
	
	//		
    /*save, validation and ajax call on submit button*/
	//
    $("#btnSubmit").click(function() {

        var flag = "false";		
        if ($("#selDepartment").val() == "-1") {
			$('#selDepartment').focus();
            $("#selDepartmentError").text("Please select department");
			$("#selDepartment").addClass("errorStyle");      
            flag = "true";
        }
		if ($("#selConsultant").val() == "") {
			$('#selConsultant').focus();
            $("#selConsultantError").text("Please select room");
			$("#selConsultant").addClass("errorStyle");       
            flag = "true";
        }		
        if ($("#selOPDRooms").val() == "-1") {
			$('#selOPDRooms').focus();
            $("#selOPDRoomsError").text("Please select ward");
			$("#selOPDRooms").addClass("errorStyle");      
            flag = "true";
        }
		
        if (flag == "true") {
            return false;
        }
		
		var OPDRooms = $("#selOPDRooms").val();
		var consultant = $("#selConsultant").val();
		var department = $("#selDepartment").val();
		var postData = {
			"operation":"save",
			"OPDRooms":OPDRooms,
			"consultant":consultant,
			"department":department
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/opd_rooms.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("OPD room saved successfully!!!");
					$('#inactive-checkbox-tick').prop('checked', false).change();
					$('#messagemyModal').modal();
					bindOPDRooms();
					tabOPDRoomsList();
				}
				if(data == "0"){
					$("#selConsultantError").text("Consultant already exists");
					$("#selConsultant").addClass("errorStyle");
					$("#selConsultant").focus();			
				}
				if(data == ""){
					$("#selConsultantError").text("Consultant already booked");
					$("#selConsultant").addClass("errorStyle");
					$("#selConsultant").focus();			
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	});	
	
	//
    //keyup functionality
	//
    $("#selOPDRooms").change(function() {
        if ($("#selOPDRooms").val() != "-1") {
            $("#selOPDRoomsError").text("");
            $("#selConsultantError").text("");
            $("#selOPDRooms").removeClass("errorStyle");
        }
    });
	
	$("#selConsultant").change(function() {
        if ($("#selConsultant").val() != "") {
            $("#selConsultantError").text("");
            $("#selConsultant").removeClass("errorStyle");
        }
    });
	
    $("#selDepartment").change(function() {
        if ($("#selDepartment").val() != "-1") {
			$("#selConsultantError").text("");
            $("#selConsultant").removeClass("errorStyle");
            $("#selDepartmentError").text("");
            $("#selDepartment").removeClass("errorStyle");
        }
    });	
});

//		
//function for edit the opd rooms 
//
function editClick(id,roomNumber,firstName,department_id){	
    
	$('#myModalLabel').text("Update OPD Room");
	$('.modal-body').html($("#opdRoomModel").html()).show();
    $('#myModal').modal('show');
	$('#myModal').on('shown.bs.modal', function() {
        $("#myModal #selOPDRooms").focus();
    });
	$("#myModal #btnUpdate").removeAttr("style");
	$('#myModal #selOPDRooms').val(roomNumber);
	$('#myModal #selConsultant').val(firstName);
	$("#myModal #selDepartment").val(department_id);
	$('#selectedRow').val(id);
	
		
	$("#myModal #btnUpdate").click(function(){

		var flag = "false";		
        if ($("#myModal #selDepartment").val() == "-1") {
			$('#myModal #selDepartment').focus();
            $("#myModal #selDepartmentError").text("Please select department");
			$("#myModal #selDepartment").addClass("errorStyle");
            flag = "true";
        }
		if ($("#myModal #selConsultant").val() == "") {
			$('#myModal #selConsultant').focus();
            $("#myModal #selConsultantError").text("Please select room");
			$("#myModal #selConsultant").addClass("errorStyle");
            flag = "true";
        }
		if ($("#myModal #selOPDRooms").val() == "-1") {
			$('#myModal #selOPDRooms').focus();
            $("#myModal #selOPDRoomsError").text("Please select ward");
			$("#myModal #selOPDRooms").addClass("errorStyle");
            flag = "true";
        }
		
		if(flag == "true"){			
			return false;
		}
		else{
			var OPDRooms = $("#myModal #selOPDRooms").val();
			var consultant = $("#myModal #selConsultant").val();
			var department = $("#myModal #selDepartment").val();
			var id = $('#selectedRow').val();			
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/opd_rooms.php",
					data :{            
						"operation" : "update",
						"id" : id,
						"OPDRooms":OPDRooms,
						"consultant":consultant,
						"department":department
					},
					success: function(data) {	
						if(data == "1"){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("OPD room updated successfully!!!");
							opdRoomTable.fnReloadAjax();
							$('#messagemyModal').modal();
							clear();
						}
						if(data == "0"){
							$('.close-confirm').click();
							$("#myModal #selConsultantError").text("Consultant already exists");
							$('#myModal #selConsultant').focus();
							$("#myModal #selConsultant").addClass("errorStyle");
						}
						if(data == ""){
							$("#myModal #selConsultantError").text("Consultant already booked");
							$('#myModal #selConsultant').focus();
							$("#myModal #selConsultant").addClass("errorStyle");
						}
					},
					error:function() {
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messagemyModal').modal();
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					}
				});
			});
		}
	});
	
	$("#myModal #selOPDRooms").change(function() {
        if ($("#myModal #selOPDRooms").val() != "-1") {
            $("#myModal #selOPDRoomsError").text("");
            $("#myModal #selOPDRooms").removeClass("errorStyle");
        }
    });
	
	$("#myModal #selConsultant").change(function() {
        if ($("#myModal #selConsultant").val() != "") {
            $("#myModal #selConsultantError").text("");
            $("#myModal #selConsultant").removeClass("errorStyle");
        }
    });
	
    $("#myModal #selDepartment").change(function() {
        if ($("#myModal #selDepartment").val() != "-1") {
            $("#myModal #selDepartmentError").text("");
            $("#myModal #selDepartment").removeClass("errorStyle");
        }
    });	
	$('#myModal #selOPDRooms').change(function () {
		var htmlRoom ="<option value='-1'>Select</option>";
		
		if ($('#myModal #selOPDRooms :selected').val() != -1) {
			var value = $('#myModal #selOPDRooms :selected').val();
		}
		else {
			$('#myModal #selConsultant').html(htmlRoom);
		}
	});		
}


//		
//function for delete the opd rooms
//
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete opd room");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteOPDRoom("'+type+'");');
}

// function for restore the opd rooms 
function restoreClick(id){
	$('#selectedRow').val(id);		
	$('.modal-body').text("");		
	$('#confirmMyModalLabel').text("Restore opd room");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();
	
	var type="restore";
	$('#confirm').attr('onclick','deleteOPDRoom("'+type+'");');
}

function deleteOPDRoom(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
			
			
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/opd_rooms.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data == "Delete Successfully!!!" && data != ""){					
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("OPD room deleted successfully!!!");
					opdRoomTable.fnReloadAjax();
					$('#messagemyModal').modal();
				}
			},
			error: function()
			{
				alert('error');
				  
			}
		});
	}
	else{
		
		var id = $('#selectedRow').val();
	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/opd_rooms.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data == "Restored Successfully!!!" && data != ""){
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("OPD room restored successfully!!!");
					opdRoomTable.fnReloadAjax();
					$('#messagemyModal').modal();
				}		
				if(data == "It can not be restore!!!"){					
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text(data);
					opdRoomTable.fnReloadAjax();
					$('#messagemyModal').modal();
				}				
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}
}

//
//Function for clear the data
//
function clear(){
	$('#selOPDRooms').val("");
	$('#selOPDRoomsError').text("");
	$('#selConsultant').val("");
	$('#selConsultantError').text("");
	$('#selDepartment').val("");
	$('#selDepartmentError').text("");
}

//
//function binding ward
//
function bindOPDRooms(){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/opd_rooms.php",
		data: {
			"operation":"showOPDRooms"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>Select</option>";
				for (var i=0;i<parseData.length;i++)
				{
				option+="<option value='"+parseData[i].id+"'>"+parseData[i].opd_room+"</option>";
				}
				$('#selOPDRooms').html(option);				
				
			}
		},
		error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
	});
}

//
//function for binding the room
//
function bindConsultant(){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/opd_rooms.php",
		data: {
			"operation":"showConsultants"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value=''>Select</option>";
				for (var i=0;i<parseData.length;i++)
				{
				option+="<option value='"+parseData[i].id+"'>"+parseData[i].first_name+"</option>";
				}
				$('#selConsultant').html(option);	
				
			}
		},
		error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
	});
}

//
//function for binding the department
//
function bindDepartment(){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/opd_rooms.php",
		data: {
			"operation":"showDepartment"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>Select</option>";
				for (var i=0;i<parseData.length;i++)
				{
				option+="<option value='"+parseData[i].id+"'>"+parseData[i].department+"</option>";
				}
				$('#selDepartment').html(option);				
				
			}
		},
		error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
	});
}
function tabOPDRoomsList(){
	 $("#advanced-wizard").hide();
	$(".blackborder").show();
	$("#tabAddOPDRooms").css({
		"background": "#fff",
		"color": "#000"
	});
	$("#tabOPDRoomsList").css({
		"background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
		"color": "#fff"
	});
	$("#OPDRoomsList").css({"background": "#fff"});
	$("#selOPDRooms").removeClass("errorStyle");
	$("#selConsultant").removeClass("errorStyle");
	$("#selDepartment").removeClass("errorStyle");
	clear();        
}

// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) {  
		 $(".close").click();
    }
});

$("#btnReset").click(function(){
	$("#selOPDRooms").removeClass("errorStyle");
	$("#selOPDRooms").focus();
	$("#selConsultant").removeClass("errorStyle");
	$("#selDepartment").removeClass("errorStyle");
	clear(); 
	
	//
	//Calling the function for loading the ward and department
	//
	bindOPDRooms();
	bindDepartment();
});

function loadDataTable(){
	
	opdRoomTable = $('#OPDRoomsTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"bAutoWidth":false,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {

			/*On click of update icon call this function*/
			$('.update').unbind();
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = opdRoomTable.fnGetData(data);
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					var roomNumber = mData["room_id"];
					var firstName = mData["users_id"];
					var department_id = mData["department_id"];
					editClick(id,roomNumber,firstName,department_id);//call update click function with certain parameters to update data
				}
			});
			/*On click of delete icon call this function*/
			$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  opdRoomTable.fnGetData(data);
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);//call delete click function with certain parameters to delete data
				}
			});
		},
		
		"sAjaxSource":"controllers/admin/opd_rooms.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{ "mData": "first_name" },
			{ "mData": "number" },
			{ "mData": "department" },	
			{  
				"mData": function (o) { 
					/*Return values that we get on click of update and delete*/
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
				   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
				   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
				   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
				   "  data-original-title='Delete'></i>"; 
				}
			},
		],
		/*Disable sort for following columns*/
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 3 ] }
	   ]
		} );
}

//# sourceURL = bed.js