$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   triage.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta 
 * Created Date :   27th feb, 2016
 * Description  :   This page  manages patient hospitalization details
 *************************************************************************************************** */	
	$("#txtAdmissionDate").val(todayDate());
	loader();
	$("#textPatientId").focus();
	debugger;
	// on search icon function
	$("#serachIcon").click(function() {
		$("#textPatientId").val("");
		$("#textPatientIdError").text("");
		$("#hdnPatientHospitalization").val("1");
		$('#patientHospitalizationLabel').text("Search Patient");
		$('#patientHospitalizationBody').load('views/admin/view_patients.html');
		$('#myPatientHospitalizationModal').modal();
		$("#textPatientId").removeClass("errorStyle");
	});
	
	$("#txtAdmissionDate").datepicker();
	$("#txtAdmissionDate").change(function() {
        if ($("#txtAdmissionDate").val() != "") {
            $("#txtAdmissionDateError").hide();
            $("#txtAdmissionDate").removeClass("errorStyle");
            $("#txtAdmissionDate").datepicker("hide");
        }
    });
	
	// load department
	bindDepartment();
	
	 //bind ward on click of department
    $('#selDepartment').change(function() {
        var departmentID = $("#selDepartment").val();
        bindWard(departmentID);
    });
	
	$("#selWard").change(function(){
		var htmlRoom ="<option value='-1'>Select</option>";

		if ($('#selWard :selected').val() != -1) {
		   var value = $('#selWard :selected').val();
		   bindRoomType(value);
		}
		else {
			$('#selRoomType').html(htmlRoom);
		}
	});
	
	// load department
	bindAttendingConsultant();
	
	// load department
	bindOperatingConsultant();
	
	//bind ward on click of department
    $('#selRoomType').change(function() {
		var htmlRoom ="<option value='-1'>--Select--</option>";
		
		if ($('#selRoomType :selected').val() != -1) {
			var value = $('#selRoomType :selected').val();
			$("#hdnBedAvailability").val(value);
	        $('#bedAvailabilityLabel').text("Show Bed Details");
	        $('#bedAvailabilityBody').load('views/admin/bed_availability.html');
	        $('#myBedAvailability').modal();
		}
		else {
			$('#selRoomType').html(htmlRoom);
		}
    });
	
	// for remove 	
    $("input[type=text]").keyup(function() {

        if ($(this).val() != "") {
            $(this).next("span").hide();
            $(this).removeClass("errorStyle");
        }
		
    });
    $("select").on('change', function() {

        if ($(this).val() != "") {
            $(this).next("span").hide();
            $(this).removeClass("errorStyle");    
        }
		
    });
	
	$("#btnReset").click(function(){ 
		clear();
	});
	// save data
	$("#btnSave").on('click',function(){
		/*Check radio button checked or not*/
        if ($('#checkICU').is(":checked")==false) {
            $('#checkICU').val(0);
        }
        else{
            $('#checkICU').val(1);
        }
		flag = false;
		if($("#selAdmissionType").val() == "-1") {
			$("#selAdmissionType").focus();
			$("#selAdmissionType").addClass('errorStyle');
			$("#selAdmissionTypeError").text("Please select admission type");
			flag = true;
		}
		if($("#selRoomType").val() == "-1") {
			$("#selRoomType").focus();
			$("#selRoomType").addClass('errorStyle');
			$("#selRoomTypeError").text("Please select room type");
			flag = true;
		}
		if($("#selWard").val() == "-1") {
			$("#selWard").focus();
			$("#selWard").addClass('errorStyle');
			$("#selWardError").text("Please select ward");
			flag = true;
		}
		if($("#selDepartment").val() == "-1") {
			$("#selDepartment").focus();
			$("#selDepartment").addClass('errorStyle');
			$("#selDepartmentError").text("Please select department");
			flag = true;
		}
		if($("#txtAdmissionDate").val() == "") {
			
			$("#txtAdmissionDate").addClass('errorStyle');
			$("#txtAdmissionDateError").text("Please select admission date");
			flag = true;
		}
		if($("#textPatientId").val() == "") {
			$("#textPatientId").focus();
			$("#textPatientId").addClass('errorStyle');
			$("#textPatientIdError").text("Please select patient id");
			flag = true;
		}
		if(flag == true) {
			return false;
		}
		
		var patientId = $("#textPatientId").val();

		var patientPrefix = patientId.substring(0, 3);
		patientId = patientId.replace ( /[^\d.]/g, '' ); 
		patientId = parseInt(patientId);	
		
		var admissionDate = $("#txtAdmissionDate").val().trim();
		var department = $("#selDepartment").val();
		var ward = $("#selWard").val();
		var roomType = $("#selRoomType").val();
		var roomNo = $("#txtRoomNo").val().trim();
		var bedNo = $("#txtBedNo").attr('data-id');
		var price = $("#txtPrice").val().trim();
		var attendingConsultant = $("#selAttendingConsultantValue").val();
		var operatingConsultant = $("#selOperatingConsultantValue").val();
		var admissionType = $("#selAdmissionType").val();
		var reasonAdmission = $("#txtReasonAdmission").val().trim();
		var notes = $("#txtNotes").val().trim();
		var ICU = $("#checkICU").val();
		
		$.ajax({					
			type: "POST",
			cache: false,
			url: "controllers/admin/patient_hospitalization.php",
			data: {
				"operation":"save",
				"patientId":patientId,
				"admissionDate":admissionDate,
				"department":department,
				"ward":ward,
				"roomType":roomType,
				"roomNo":roomNo,
				"bedNo":bedNo,
				"price":price,
				"attendingConsultant":attendingConsultant,
				"operatingConsultant":operatingConsultant,
				"admissionType":admissionType,
				"reasonAdmission":reasonAdmission,
				"notes":notes,
				"ICU":ICU
			},
			success: function(data) {	
				if(data != "") {	
					var parseData = jQuery.parseJSON(data);
					var ipdid = parseInt(parseData[1]);
					var prefix = parseData[0];
					var ipdIdLength = ipdid.toString().length;
					for (var i=0;i<6-ipdIdLength;i++) {
						ipdid = "0"+ipdid;
					}
					ipdid = prefix+ipdid;
					
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').html("<span>Patient hospitalization saved successfully !!!</span> </br> Your IPD ID is :" +ipdid);
					//$('.modal-body').text("Patient hospitalization saved successfully !!!" +data);
					$('#messagemyModal').modal();
					clear();
				}
				/* if(data == "0") {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Alert");
					$('.modal-body').text("Insufficient balance !!!");
					$('#messagemyModal').modal();
				} */
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
		
	});
	
});

// bind department
function bindDepartment() {
		$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/patient_hospitalization.php",
		data: {
			"operation":"showDepartment"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>--Select--</option>";
				for (var i=0;i<parseData.length;i++)
				{
					option+="<option value='"+parseData[i].id+"'>"+parseData[i].department+"</option>";
				}
				$('#selDepartment').html(option);				
				
			}
		},
		error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
	});
}

// bind ward
function bindWard(departmentID) {
	 $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/patient_hospitalization.php",
        data: {
            "operation": "showWard",
            departmentID: departmentID
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selWard').html(option);
            }
        },
        error: function() {}
    });
}
function bindRoomType(value) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/bed.php",
        data: {
		   "operation":"showRoom",
		   "ward" : value
		},
		success: function(data) {
			if (data != null && data != "") {
				var parseData = jQuery.parseJSON(data);

				var option = "<option value='-1'>-- Select --</option>";
				for (var i = 0; i < parseData.length; i++) {
					option += "<option value='" + parseData[i].id + "'>" + parseData[i].room_number + "</option>";
				}
				$('#selRoomType').html(option);
			}
		},
	   error:function() {
		   $('#messagemyModal').modal();
		   $('#messageMyModalLabel').text("Error");
		   $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
    });
}

// bind department
function bindAttendingConsultant() {
		$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/patient_hospitalization.php",
		data: {
			"operation":"showAttendingConsultant"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>--Select--</option>";
				for (var i=0;i<parseData.length;i++)
				{
					option+="<option value='"+parseData[i].id+"'>"+parseData[i].name+"</option>";
				}
				$('#selAttendingConsultantValue').html(option);				
				
			}
		},
		error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
	});
}

// bind department
function bindOperatingConsultant() {
		$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/patient_hospitalization.php",
		data: {
			"operation":"showOperatingConsultant"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>--Select--</option>";
				for (var i=0;i<parseData.length;i++)
				{
					option+="<option value='"+parseData[i].id+"'>"+parseData[i].name+"</option>";
				}
				$('#selOperatingConsultantValue').html(option);				
				
			}
		},
		error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
	});
}

function clear(){
	$('input[type=text]').next("span").hide();
	$('input[type=text]').val("");
	$('input[type=text]').removeClass("errorStyle");
	$('select').next("span").hide();
	$('#txtReasonAdmission').val("");
	$('#txtNotes').val("");
	$('select').val("-1");
	$('select').removeClass("errorStyle");
	$('#checkICU').attr('checked',false);
}
