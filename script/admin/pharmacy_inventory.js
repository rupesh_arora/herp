$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   pharmacy_inventory.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages pharmacy inventory data
 *************************************************************************************************** */
	loader();
	debugger;
	var searchedDrug = '';
	$("#txtDrug").on('keyup', function(){	
		$("#hidDrugId").val("");
		//Autocomplete functionality 
		var currentObj = $(this);
		jQuery("#txtDrug").autocomplete({
			source: function(request, response) {
				var postData = {
					"operation":"search",
					"drugId": '',
					"drugName": currentObj.val(),
					"drugCode": ''
				}
				
				$.ajax(
					{					
					type: "POST",
					cache: false,
					url: "controllers/admin/search_drug.php",
					datatype:"json",
					data: postData,
					
					success: function(dataSet) {
						loader();
						//searchedDrug = JSON.parse(dataSet);
						response($.map(JSON.parse(dataSet), function (item) {
							return {
								id: item.id,
								value: item.name
							}
						}));
					},
					error: function(){
						
					}
				});
			},
            select: function (e, i) {
				var drugId = i.item.id;
                currentObj.attr("drug-id", drugId);
				
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/search_drug.php",
					data :{            
						"operation" : "searchId",
						"id" : drugId
					},
					success: function(data) {	
						$("#hidDrugId").val(drugId);
						if(data != "0" && data != ""){
								
							var parseData = jQuery.parseJSON(data);

							for (var i=0;i<parseData.length;i++) {
								$("#hidDrugId").val(parseData[i].drug_name_id);
								$("#txtDrug").val(parseData[i].name);    					
								$("#txtQuantity").val(parseData[i].quantity);								
								$("#txtUnitVol").val(parseData[i].unit_v);								
								$("#selUnit").val(parseData[i].unit_id);								
								$("#txtManufactureName").val(parseData[i].manufacture_name);								
								$("#txtManufactureDate").val(parseData[i].manufacture_date);								
								$("#txtExpiryDate").val(parseData[i].expiry_date);
								$("#txtUnitMeasure").val(parseData[i].unit_measure);
								if(parseData[i].composition == "") {
									$("#lblComposition").text("N/A");
								}
								else {
									$("#lblComposition").text(parseData[i].composition);
								}
								if(parseData[i].drug_code == "") {
									$("#lblDrugCode").text("N/A");
								}
								else {
									$("#lblDrugCode").text(parseData[i].drug_code);
								}
															
								$("#selectedRow").val(parseData[i].id);
								$("#btnSave").val("Update");
							}
						}		
						else{	
							$("#txtDrugError").text("");
							$("#txtDrug").removeClass("errorStyle"); 					
							$("#txtQuantity").val('');								
							$("#txtUnitVol").val('');								
							$("#selUnit").val(-1);								
							$("#txtManufactureName").val('');								
							$("#txtManufactureDate").val('');		
							$("#txtExpiryDate").val('');
							$("#txtUnitMeasure").val('');

						}
						currentDate();				
					},
					error:function()
					{					
						$('#messageMyModalLabel').text("Sorry");
						$('.modal-body').text("Error!!!");
						$('#messagemyModal').modal();
					}
				});
            },
			minLength: 3
		});		
	});

	bindUnit();
	currentDate();
	$("#txtDrug").focus();
    var otable;
	otable = $('#pharmacyInventoryTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {
				
				$('.update').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData = $('#pharmacyInventoryTable').dataTable().fnGetData(data);
					if (null != mData)  // null if we clicked on title row
					{
						
						$('#hidDrugId').val(mData["drug_name_id"]);								
						$('#txtDrug').val(mData["name"]);
						$('#txtQuantity').val(mData["quantity"]);
						$('#txtUnitVol').val(mData["unit_v"]);
						$('#selUnit').val(mData["unit_id"]);
						$('#txtManufactureName').val(mData["manufacture_name"]);
						$('#txtManufactureDate').val(mData["manufacture_date"]);
						$('#txtExpiryDate').val(mData["expiry_date"]);
						$("#selectedRow").val(mData["id"]);
						if(mData["composition"] == "") {
							$("#lblComposition").text("N/A");
						}
						else {
							$("#lblComposition").text(mData["composition"]);
						}
						if(mData["drug_code"] == "") {
							$("#lblDrugCode").text("N/A");
						}
						else {
							$("#lblDrugCode").text(mData["drug_code"]);
						}
						$("#btnSave").val("Update");
						$("#txtDrug").removeClass("errorStyle");
						$("#txtQuantity").removeClass("errorStyle");
						$("#txtUnitVol").removeClass("errorStyle");
						$("#selUnit").removeClass("errorStyle");
						$("#txtDrugError").text("");
						$("#txtQuantityError").text("");
						$("#txtUnitVolError").text("");
						$("#txtQuantity").focus();	   
					}
				});
				$('.delete').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData =  $('#pharmacyInventoryTable').dataTable().fnGetData(data);
					
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						deleteClick(id);
					}
				});
			},
			
			"sAjaxSource":"controllers/admin/pharmacy_inventory.php",
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "operation", "value": "show" });
			},
			"aoColumns": [
				{ "mData": "name" },
				{ "mData": "quantity" },
				{ "mData": "manufacture_name" },
				{ "mData": "unit_vol" },
				
				{ "mData": "manufacture_date" },
				{ "mData": "expiry_date" },
				{  
					"mData": function (o) { 
					return "<i class='fa fa-pencil update' drug_id=" + o.id +" title='Edit'"+
					   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
					   " <i class='fa fa-trash-o delete' title='Delete' "+
					   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
					   "  data-original-title='Delete'></i>"; 
					}
				},	
			],
			aoColumnDefs: [
			   { 'bSortable': false, 'aTargets': [ 5 ] },
			   { 'bSortable': false, 'aTargets': [ 6 ] }
		   ]
	});   
	
/*     $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	otable.fnClearTable();
	    	otable.fnDestroy();
	    	//Datatable code
			otable = $('#pharmacyInventoryTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  $('#pharmacyInventoryTable').dataTable().fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];							
							restoreClick(id);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/pharmacy_inventory.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					{ "mData": "name" },
					{ "mData": "quantity" },
					{ "mData": "manufacture_name" },
					{ "mData": "unit_vol" },
					{ "mData": "manufacture_date" },
					{ "mData": "expiry_date" },
					{  
						"mData": function (o) { 
						
						return '<i class="fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center; cursor:pointer;" title="Restore"></i>'; }
					},
				],
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 5 ] },
				   { 'bSortable': false, 'aTargets': [ 6 ] }
			   ]
			} );
		}
		else{
			otable.fnClearTable();
	    	otable.fnDestroy();
	    	//Datatable code
			otable = $('#pharmacyInventoryTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					
					$('.update').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData = $('#pharmacyInventoryTable').dataTable().fnGetData(data);
						if (null != mData)  // null if we clicked on title row
						{
							$('#hidDrugId').val(mData["drug_name_id"]);								
							$('#txtDrug').val(mData["name"]);
							$('#txtQuantity').val(mData["quantity"]);
							$('#txtUnitVol').val(mData["unit_v"]);
							$('#selUnit').val(mData["unit"]);
							$('#txtManufactureName').val(mData["manufacture_name"]);
							$('#txtManufactureDate').val(mData["manufacture_date"]);
							$('#txtExpiryDate').val(mData["expiry_date"]);
							$("#selectedRow").val(mData["id"]);
							$("#btnSave").val("Update");
							$("#txtDrug").removeClass("errorStyle");
							$("#txtQuantity").removeClass("errorStyle");
							$("#txtUnitVol").removeClass("errorStyle");
							$("#selUnit").removeClass("errorStyle");
							$("#txtDrugError").text("");
							$("#txtQuantityError").text("");
							$("#txtUnitVolError").text("");
							$("#lblComposition").text("");
							$("#lblDrugCode").text("");
							$("#txtQuantity").focus();
		   
						}
					});
					$('.delete').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  $('#pharmacyInventoryTable').dataTable().fnGetData(data);
						
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							deleteClick(id);
						}
					});
				},
				
				"sAjaxSource":"controllers/admin/pharmacy_inventory.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "show" });
				},
				"aoColumns": [
					{ "mData": "name" },
					{ "mData": "quantity" },
					{ "mData": "manufacture_name" },
					{ "mData": "unit_vol" },
					{ "mData": "manufacture_date" },
					{ "mData": "expiry_date" },
					{  
					"mData": function (o) { 
					
					return "<i class='fa fa-pencil update' title='Edit'"+
					   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
					   " <i class='fa fa-trash-o delete' title='Delete' "+
					   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
					   "  data-original-title='Delete'></i>"; 
					}
				},
				],
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 5 ] },
				   { 'bSortable': false, 'aTargets': [ 6 ] }
			   ]
			} );
		}
    });	 */
	
	/* $('#txtManufactureDate').datepicker({
		changeYear: true,
		changeMonth: true,
		autoclose: true
	});
	
	$('#txtExpiryDate').datepicker({
		changeYear: true,
		changeMonth: true,
		autoclose: true
	}); */
	/*$("#txtManufactureDate").datepicker({ changeYear: true,changeMonth: true,autoclose: true}) .on('changeDate', function(selected){
		startDate = new Date(selected.date.valueOf());
		$('#txtExpiryDate').datepicker('setStartDate', startDate);
	}); 
	$('#txtExpiryDate').datepicker({
		changeYear: true,
		changeMonth: true,
		autoclose: true
	});*/

	var startDate = new Date();
    $("#txtManufactureDate").datepicker('setEndDate',startDate);
    $('#txtExpiryDate').datepicker('setStartDate', startDate);
	
	//Click function for validation ,save and update the data 
	$("#btnSave").click(function(){
		
		var flag = "false";

		if($("#txtUnitVol").val()== "") {
			$("#txtUnitVol").focus();
			$("#txtUnitVolError").text("Please enter unit vol");
            $("#txtUnitVol").addClass("errorStyle");
			flag="true";
		}
		if($("#txtUnitVol").val()!= ""){
			if($("#selUnit").val()=="-1") {
				$("#selUnit").focus();
				$("#txtUnitVolError").text("Please select unit");
				$("#selUnit").addClass("errorStyle");
				flag="true";
			}
		}
		if($("#txtQuantity").val()== "" || $("#txtQuantity").val() < 0){
			$("#txtQuantity").focus();
			$("#txtQuantityError").text("Please enter quantity");
            $("#txtQuantity").addClass("errorStyle");
			flag="true";
		}
		if($("#txtDrug").val()== ""){
			$("#txtDrugError").text("Please select drug name");
            $("#txtDrug").addClass("errorStyle");
            $("#txtDrug").focus();
			flag="true";
		}
		if($("#hidDrugId").val() == "") {
			$("#txtDrugError").text("Please enter valid drug name");
            $("#txtDrug").focus();
            $("#txtDrug").addClass("errorStyle");
			flag="true";
		}
		if(flag == "true"){
			return false;
		}
		
		var drugId = $("#hidDrugId").val();
		var quantity = $("#txtQuantity").val();
		var manufactureName = $("#txtManufactureName").val();
		var unitVol = $("#txtUnitVol").val();
		var unit = $("#selUnit").val();
		var manufactureDate = $("#txtManufactureDate").val();
		var expiryDate = $("#txtExpiryDate").val();
		var id = $('#selectedRow').val();
		
		if($("#btnSave").val() == "Save"){  //Ajax call for save the data 		
				
			var postData = {
				"operation":"save",
				"drugId":drugId,
				"quantity":quantity,
				"manufactureName":manufactureName,
				"unitVol":unitVol,
				"unit":unit,
				"manufactureDate":manufactureDate,
				"expiryDate":expiryDate
			}
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to save this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){				
				$.ajax(
				{					
					type: "POST",
					cache: false,
					url: "controllers/admin/pharmacy_inventory.php",
					datatype:"json",
					data: postData,
					
					success: function(data) {
						if(data != "0" && data != ""){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Pharmacy inventory saved successfully!!!");
							$("#pharmacyInventoryTable").dataTable().fnReloadAjax();
							clear();
							bindUnit();
							$('#messagemyModal').modal();
						}
					},
					error: function(){	
						$('.close-confirm').click();				
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
				});
			});
		}
		else{			
			var postData = {				//Ajax call for update the data 
				"operation":"Update",
				"drugId":drugId,
				"id":id,
				"quantity":quantity,
				"manufactureName":manufactureName,
				"unitVol":unitVol,
				"unit":unit,
				"manufactureDate":manufactureDate,
				"expiryDate":expiryDate
			}
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){	
				$.ajax(
					{					
					type: "POST",
					cache: false,
					url: "controllers/admin/pharmacy_inventory.php",
					datatype:"json",
					data: postData,
					
					success: function(data) {
						if(data != "0" && data != ""){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Pharmacy inventory updated successfully!!!");
							$("#pharmacyInventoryTable").dataTable().fnReloadAjax();
							$("#btnSave").val("Save");
							clear();
							bindUnit();
							$('#messagemyModal').modal();
						}
					},
					error: function(){	
						$('.close-confirm').click();				
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
				});
			});
		}		
	});
	
	//Keyup functionality 
	$("#txtQuantity").keyup(function(){
		if($("#txtQuantity").val() != ""){
			$("#txtQuantityError").text("");
			$("#txtQuantity").removeClass("errorStyle");
		}
	});
	$("#txtUnitVol").keyup(function(){
		if($("#txtUnitVol").val() != ""){
			$("#txtUnitVolError").text("");
			$("#txtUnitVol").removeClass("errorStyle");
		}
	});
	$("#selUnit").change(function(){
		if($("#selUnit").val() != ""){
			$("#txtUnitVolError").text("");
			$("#selUnit").removeClass("errorStyle");
		}
	});
	
	//Click function for reset button 
	$("#btnReset").click(function(){
		$("#txtDrug").removeClass("errorStyle");
		$("#txtQuantity").removeClass("errorStyle");
		$("#txtUnitVol").removeClass("errorStyle");
		$("#selUnit").removeClass("errorStyle");
		$("#btnSave").val("Save");
		$("#txtDrug").focus();
		bindUnit();		//Calling this function for binding the units
		clear();      //Calling this function for clear the data
	});
	
	//Click function for search icon 
	$("#iconSearch").click(function(){
		clear(); 		//Calling this function for clear the previous data 
		bindUnit();		//Calling this function for binding the units
		$("#txtQuantity").removeClass("errorStyle");
		$("#txtUnitVol").removeClass("errorStyle");
		$("#selUnit").removeClass("errorStyle");
		$('#drugModalLabel').text("Search Drug");
		$('#drugModalBody').load('views/admin/search_drug.html', function(){
			
		});
		$('#drugModal').click();	
	});	
});//Close the document.ready function

//This function is used for binding the units 
function bindUnit(){
	$.ajax({     
		type: "POST",
		cache: false,
		url: "controllers/admin/pharmacy_inventory.php",
		data: {
		"operation":"showUnit"
		},
		success: function(data) { 
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data); // parse the value in Array string jquery
				var option ="<option value='-1'>Select</option>";
				for (var i=0;i<parseData.length;i++)
				{
						option+="<option value='"+parseData[i].id+"'>"+parseData[i].unit_abbr+"</option>";
				}
				$('#selUnit').html(option);
			}
			
		},
		error:function(){
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
	});	
}

 //Click function for delete the inventory
function deleteClick(id){
	$('#confirmMyModalLabel').text("Delete Inventory");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	$('#confirm').unbind();
	$('#confirm').on('click',function(){
		//Ajax call for delete the drug 	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/pharmacy_inventory.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Pharmacy inventory deleted successfully!!!");
					$("#pharmacyInventoryTable").dataTable().fnReloadAjax();
					$('#messagemyModal').modal();
				}
			},
			error:function()
			{					
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	});
}

/*//Click function for restore the inventory
function restoreClick(id){
	$('#selectedRow').val(id);
	
	$('#confirmMyModalLabel').text("Restore Inventory");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();
	
	var type="restore";
	$('#confirm').attr('onclick','deleteInventory("'+type+'");');
}

//function for delete the drugList
function deleteInventory(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
			
		//Ajax call for delete the drug 	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/pharmacy_inventory.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Pharmacy inventory deleted successfully!!!");
					$("#pharmacyInventoryTable").dataTable().fnReloadAjax();
					$('#messagemyModal').modal();
				}
			},
			error:function()
			{					
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
	
		//Ajax call for restore the drug 
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/pharmacy_inventory.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Pharmacy inventory restored successfully!!!");
					$("#pharmacyInventoryTable").dataTable().fnReloadAjax();
					$('#messagemyModal').modal();
					 clear();
				}			
			},
			error:function()
			{					
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();	  
			}
		});
	}
} */

//This function is used for wshow the current date 
function currentDate(){
	var fullDate = new Date();
	
	//convert month to 2 digits
	var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '' + (fullDate.getMonth()+1);
	var todayDay = fullDate.getDate();
	
	if(twoDigitMonth < "10"){
		twoDigitMonth = "0"+twoDigitMonth;
	}
	 
	if(todayDay < "10"){
		todayDay = "0"+todayDay;
	}
	 
	var currentDate = fullDate.getFullYear() + "-" + twoDigitMonth+ "-" + todayDay ;		
	
	$('#txtExpiryDate').val(currentDate);
	$('#txtManufactureDate').val(currentDate);
}

//This function is used for clear the data 
function clear(){
	$("#txtDrug").val("");
	$("#txtDrug").removeClass("errorStyle");
	$("#txtDrugError").text("");
	$("#txtQuantity").val("");
	$("#txtQuantityError").text("");
	$("#txtExpiryDate").val("");
	$("#txtManufactureDate").val("");
	$("#txtManufactureName").val("");
	$("#txtUnitVol").val("");
	$("#txtUnitVolError").text("");
	$("#selUnit").val("-1");
	$("#lblComposition").text("N/A");
	$("#lblDrugCode").text("N/A");
	currentDate();
}

//# sourceURL=filename.js