$(document).ready(function() {
	/* ****************************************************************************************************
 * File Name    :   view_ipd_lab_result.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   4th mar, 2016
 * Description  :   This page  manages ipd lab results
 *************************************************************************************************** */	
	loader();
	debugger;
	var labResultTable;

	labResultTable = $('#labResultsTbl').dataTable({
		aoColumnDefs: [
			{'bSortable': false,'aTargets': [0]},
			{'bSortable': false,'aTargets': [1]},
			{'bSortable': false,'aTargets': [2]},
			{'bSortable': false,'aTargets': [3]},
			{'bSortable': false,'aTargets': [4]},
			{'bSortable': false,'aTargets': [5]}
		]
	});

	$("#PrintLabTest").addClass("hide");	

	$("#txtPatientId").focus();

	$("#btnSearch").on("click",function(){

		var flag = "false";		
		var patientId = $("#txtPatientId").val().trim();
		var ipdId =  $("#txtIPDId").val().trim();
		var patientName = $("#txtPatientName").val().trim();

		if(ipdId != ""){
			if(ipdId.length != 9){					
				flag = "true";
			}
			else{
				var ipdPrefix = ipdId.substring(0, 3);
				ipdId = ipdId.replace ( /[^\d.]/g, '' ); 
				ipdId = parseInt(ipdId);
			}
		}
		else{
			ipdPrefix = "";
		}		

		if($("#txtPatientId").val() != ""){
			if(patientId.length != 9){					
				flag = "true";
			}
			else{
				var patientId = $("#txtPatientId").val();
				var patientPrefix = patientId.substring(0, 3);
				patientId = patientId.replace ( /[^\d.]/g, '' ); 
				patientId = parseInt(patientId);
			}
		}
		else{
			patientPrefix = "";
		}
		if (flag == "true") {
			return false;
		}
		
		labResultTable.fnClearTable();
		labResultTable.fnDestroy();

		labResultTable = $('#labResultsTbl').dataTable( {
			"bFilter": true,
			"processing": true,
			"sPaginationType":"full_numbers",
			"sAjaxSource":"controllers/admin/view_ipd_lab_result.php",
			"fnDrawCallback": function ( oSettings ) {
				
			}, 
			"fnServerParams": function ( aoData ) {
			  aoData.push( {"name": "operation", "value": "bindTableData" },
			  {"name": "ipdId", "value": ipdId },
			  {"name": "patientId", "value": patientId },
			  {"name": "patientName", "value": patientName},
			  {"name": "patientPrefix", "value": patientPrefix},
			  {"name": "ipdPrefix", "value": ipdPrefix});
			},
			"aoColumns": [
				{  "mData": function (o) { 	
						var ipdId = o["ipd_id"];
						var ipdPrefix = o["ipd_prefix"];
							
						var ipdIdLength = ipdId.length;
						for (var i=0;i<6-ipdIdLength;i++) {
							ipdId = "0"+ipdId;
						}
						ipdId = ipdPrefix+ipdId;
						return ipdId; 
					}
				},
				{  "mData": function (o) { 	
						var patientId = o["patient_id"];
						var patient_prefix = o["patient_prefix"];
							
						var visitIdLength = patientId.length;
						for (var i=0;i<6-visitIdLength;i++) {
							patientId = "0"+patientId;
						}
						patientId = patient_prefix+patientId;
						return patientId; 
					}
				},
				{  "mData": "patient_name" },
				{  "mData": "lab_test_name" },
				{  "mData": "report" },
				{  "mData": function(o){
					var data = o;
					return "<i class='fa fa-eye viewLabDetails' style='font-size: 17px; margin-left: 20px; cursor:pointer;' onclick = 'view($(this))' title='view' value='view'></i>";
				} }			
			],
			aoColumnDefs: [
			{'bSortable': false,'aTargets': [0]},
			{'bSortable': false,'aTargets': [1]},
			{'bSortable': false,'aTargets': [2]},
			{'bSortable': false,'aTargets': [3]},
			{'bSortable': false,'aTargets': [4]},
			{'bSortable': false,'aTargets': [5]}
			]
		});
	});
		
	$("#printLabTestDetails").click(function(){
		
		if ($('#labResultsViewTable').dataTable().fnGetData().length > 0) {
			$("#printContainerTable").html('');
			var printData = $('#labResultsViewTable').dataTable().fnGetData();
			     	
				$('#lblPrintVisitId').text($("#getVisitId").val());								
				$('#lblPrintPatientId').text($("#getPatientId").val());
				$('#lblPrintPatient').text($("#getPatientName").val());	
				$('#lblPrintVisitDate').text($("#getDate").val());
				//hospital information through global varibale in main.js
				$("#lblPrintEmail").text(hospitalEmail);
				$("#lblPrintPhone").text(hospitalPhoneNo);
				$("#lblPrintAddress").text(hospitalAddress);
				if (hospitalLogo != "null") {
					imagePath = "./images/" + hospitalLogo;
					$("#printLogo").attr("src",imagePath);
				}
				$("#printHospitalName").text(hospitalName);	

				var html = '';
				html ='<table id="codexpl" width="100%; height: 450px;">';
				html +=	'<label>Test Name: '+printData[0][0]+'</label>';
				html +=	'<thead><tr><td width=25%>Component Name</td><td width=25%>Result</td><td width=50%>Normal Range</td></tr></thead>';
				html +=	'<tbody id="codexplBody">';
				for(var i=0;i<printData.length;i++){
					html += '<tr><td>'+printData[i][1]+'</td><td>'+printData[i][2]+'</td><td>'+printData[i][3]+'</td></tr>';
				}
				html +=	'</tbody>';				
				html +=	'</table>';
				html +='<label>Report :</label><label style="font-weight:100; text-transform: initial;">'+$("#individualReport").text()+'</label>';
				$("#printContainerTable").append(html);
				$.print("#PrintLabTest");
		}			
	});

	$("#popUpClose").click(function(){
		$('#labResultsViewTable').DataTable().fnDestroy();
	});

	$('#txtPatientId, #txtVisitid, #txtPatientName').keypress(function (e) {
       
        if (e.which == 13) {
        	e.preventDefault();
            $("#btnSearch").click();
        }
    });
});
//function to calculate date
function convertDate(o){
    var dbDateTimestamp = o;
    var dateObj = new Date(dbDateTimestamp * 1000);
    var yyyy = dateObj.getFullYear().toString();
    var mm = (dateObj.getMonth()+1).toString(); // getMonth() is zero-based
    var dd  = dateObj.getDate().toString();
    return yyyy +"-"+ (mm[1]?mm:"0"+mm[0]) +"-"+ (dd[1]?dd:"0"+dd[0]);
}
function view(myThis){
	$('#myLabTestModal').modal();
	$('#myLabTestModalBodyLabel').text("Your Result");

	var row = myThis.closest("tr")[0];
	var currData = $('#labResultsTbl').dataTable().fnGetData(row);
	var labTestId = $(currData)[0]['lab_test_id'];
	var paitentId = $(currData)[0]['patient_id'];
	var ipdId = $(currData)[0]['ipd_id'];

	/*for print get id in label*/
	var visitIdLabel = $(row).find('td:eq(0)').text();
	var patientName = $(currData)[0]['patient_name'];
	var visitDate = $(currData)[0]['visit_date'];
	var patientLabel = $(row).find('td:eq(1)').text();
	$("#getVisitId").val(visitIdLabel);
	$("#getPatientId").val(patientLabel);
	$("#getDate").val(visitDate);
	$("#getPatientName").val(patientName);

	var labViewtable = $('#labResultsViewTable').DataTable({
        "bPaginate": false,
        "bSort": false,
        "bAutoWidth": false      
    });
    $("#labResultsViewTable_info").hide();
	var postData ={
		"operation" : "bindTableDataByOrder",
		"paitentId" : paitentId,
		"ipdId" : ipdId,
		"labTestId" : labTestId
	}
	$.ajax({     
		type: "POST",
		cache: false,
		url: "controllers/admin/view_ipd_lab_result.php",
		data: postData,
		success: function(data) {
			if (data !='' && data !=null) {
				labViewtable.fnClearTable();
				parseData = JSON.parse(data);

				for(var i=0;i<parseData.length;i++){
					var testname = parseData[i].lab_test_name;
					var componentName = parseData[i].name;
					var result = parseData[i].result;
					var normalRange = parseData[i].normal_range;
					labViewtable.fnAddData([testname,componentName, result,normalRange]);//send one column null so user can't remove previous data
				}
			}
		}
	});
	if ($(currData)[0]['report'] !='') {
		$("#individualReport").text($(currData)[0]['report']);
	}
	else {
		$("#individualReport").text("N/A");
	}
	
}