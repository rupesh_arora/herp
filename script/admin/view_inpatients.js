var viewInPatientTable;
var oldBedId;
var bedHistoryId;
var oldbedPrice;
$("document").ready(function(){	
	debugger;
	
	
	// disable dropdown and hide button
	$('#myBedTransferModal #selDepartment').attr('disabled','true');
	$('#myBedTransferModal #selWard').attr('disabled','true');
	$('#myBedTransferModal #selRoomType').attr('disabled','true');
	$('#myBedTransferModal #checkICU').attr('disabled','true');
	$('#myBedTransferModal #btnUpdate').hide();
	$('#myBedTransferModal #btnResetDetails').hide();
	
	// load department
	bindDepartment(null);
	
	// load datatable details
	loadDetails();
	
	$(".closeButton").click(function(){
		$('#mySearchInPatient').modal('hide');
	});
	 //bind ward on click of department
    $('#selDepartment').change(function() {
        var departmentID = $("#selDepartment").val();
        bindWard(departmentID,'');
    });
	
	$("#selWard").change(function(){
		var htmlRoom ="<option value='-1'>Select</option>";

		if ($('#selWard :selected').val() != -1) {
		   var value = $('#selWard :selected').val();
		   bindRoomType(value,'');
		}
		else {
			$('#selRoomType').html(htmlRoom);
		}
	});
	
	//bind ward on click of department
    $('#selRoomType').change(function() {
		var htmlRoom ="<option value='-1'>--Select--</option>";
		
		if ($('#selRoomType :selected').val() != -1) {
			var value = $('#selRoomType :selected').val();
			$("#hdnBedAvailability").val(value);
	        $('#bedAvailabilityLabel').text("Show Bed Details");
	        $('#bedAvailabilityBody').load('views/admin/bed_availability.html');
	        $('#myBedAvailability').modal();
		}
		else {
			$('#selRoomType').html(htmlRoom);
		}
    });
	
	$("#btnResetDetails").on('click',function() {
		
		viewInPatientTable.fnClearTable(); // clear datatable
		viewInPatientTable.fnDestroy(); // remove datatable
		clear();
	});
	
	$("#btnReset").on('click',function(){
			
		viewInPatientTable.fnClearTable(); // clear datatable
		viewInPatientTable.fnDestroy(); // remove datatable
		clear();
	});
	
	$("#btnUpdate").on('click',function(){
		
		if ($('#checkICU').is(":checked")==false) {
            $('#checkICU').val(0);
        }
        else{
            $('#checkICU').val(1);
        }

		var flag = "false";
		
		if($("#selRoomType").val() == "-1"){
			$("#selRoomType").focus();
			$("#selRoomTypeError").text("Please select room type");
			$("#selRoomType").addClass("errorStyle");       
            flag = "true";
		}

		if($("#selWard").val() == "-1"){
			$("#selWard").focus();
			$("#selWardError").text("Please select ward");
			$("#selWard").addClass("errorStyle");       
            flag = "true";
		}

		if($("#selDepartment").val() == "-1"){
			$("#selDepartment").focus();
			$("#selDepartmentError").text("Please select department");
			$("#selDepartment").addClass("errorStyle");       
            flag = "true";
		}
		
		if(flag == "true"){
			return false;
		}
		
		var ipdId = $("#lblIPDId").text();
		var ipdPrefix = ipdId.substring(0, 3);
		ipdId = ipdId.replace ( /[^\d.]/g, '' ); 
		ipdId = parseInt(ipdId);
		
		var patientId = $("#lblPatientId").text();
		var patientPrefix = patientId.substring(0, 3);
		patientId = patientId.replace ( /[^\d.]/g, '' ); 
		patientId = parseInt(patientId);
		
		var departmentId = $("#selDepartment").val();
		var wardId = $("#selWard").val();
		var roomtypeId = $("#selRoomType").val();
		var roomNo = $("#txtRoomNo").val();
		var bedNo = $("#txtBedNo").val();
		var price = $("#txtPrice").val();
		var bedId = $("#txtBedNo").attr("data-id");
		var ICU = $("#checkICU").val();

		postData = {
			"operation" : "update",
			"ICU" : ICU,
			"bedId" : bedId,
			"ipdId" : ipdId,
			"patientId" : patientId,
			"price" : price,
			"oldBedId":oldBedId,
			"bedHistoryId":bedHistoryId,
			"oldbedPrice":oldbedPrice
		}

		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/view_inpatients.php",
			datatype:"json",
			data: postData,			
			success: function(data) {
				if(data == "1") {			
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Updated successfully !!!");
					$('#messagemyModal').modal();
					viewInPatientTable.fnClearTable(); // clear datatable
					viewInPatientTable.fnDestroy(); // remove datatable
					loadDetails();
					$(".closeButton").click();
				}
				if(data == "0") {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Alert");
					$('.modal-body').text("Insufficient balance !!!");
					$('#messagemyModal').modal();
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
		
	});
	
	// call event for search details
	$("#btnSearch").unbind();
	$("#btnSearch").on('click',function() {
		var IPDId = $("#txtIPDId").val();
		var ipdPrefix = IPDId.substring(0, 3);
		IPDId = IPDId.replace ( /[^\d.]/g, '' ); 
		IPDId = parseInt(IPDId);
		if(isNaN(IPDId)){
			IPDId="";
		}
		var firstName = $("#txtFirstName").val();
		var lastName = $("#txtLastName").val();
		var mobile = $("#txtMobile").val();
		
		var postData = {
			"operation": "search",
			"IPDId": IPDId,
			"ipdPrefix": ipdPrefix,
			"firstName": firstName,
			"lastName": lastName,
			"mobile": mobile
		}
		
		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/view_inpatients.php",
			datatype: "json",
			data: postData,

			success: function(result) {
				
				//$('input[type=text]').val("");
				$('select[name=tblViewInPatient_length]').val("10");
	
				dataSet = JSON.parse(result); // convert data into json
				//var dataSet = jQuery.parseJSON(data);
				viewInPatientTable.fnClearTable(); // clear datatable
				viewInPatientTable.fnDestroy(); // remove datatable
			
				//Datatable code
				viewInPatientTable = $("#tblViewInPatient").dataTable({
					"bFilter": true,
					"processing": true,
					"bAutoWidth":false,
					"sPaginationType":"full_numbers",
					"fnDrawCallback": function ( oSettings ) {
						$('#myBedTransferModal #tblViewInPatient tbody tr').on('dblclick', function() {
							if ($(this).hasClass('selected')) {
								$(this).removeClass('selected');
							} else {
								viewInPatientTable.$('tr.selected').removeClass('selected');
								$(this).addClass('selected');
							}

							var mData = viewInPatientTable.fnGetData(this); // get datarow
							
							if (null != mData) // null if we clicked on title row
							{	
								if($("#hdnIPD").val() != "1")
								{	
									var ipdId = mData['id'];	
									var ipdPrefix = mData['ipdPrefix'];						
									var ipdIdLength = ipdId.length;
									for (var j=0;j<6-ipdIdLength;j++) {
										ipdId = "0"+ipdId;
									}
									ipdid = ipdPrefix+ipdId;
									$("#textIPDId").val(ipdid);
									$("#textIPDId").attr("disabled","disabled");
									
									$("#btnLoad").click();
									$("#selDepartment").val(mData['department_id']);
									var ward_id = mData['ward_id'];					
									var room_id = mData['room_id'];					
									bindWard(mData['department_id'], ward_id);
									bindRoomType(ward_id,room_id);
									$("#txtRoomNo").val(mData['number']);
									$("#txtBedNo").val(mData['bedName']);
									$("#hdnBesHistoryId").val(mData['bed_history_id']);
									$("#txtPrice").val(mData['price']);
									$("#txtBedNo").attr('data-id',mData['bed_id']);
									$("#hdnPatientId").val(mData["patient_id"]);
									 if(mData["is_icu"] == "YES"){
									  $('#checkICU').prop("checked",true);
									 }
									 else{
									  $('#checkICU').prop("checked",false);
									 }
								}
								else{
									var ipdId = mData['id'];	
									var ipdPrefix = mData['ipdPrefix'];	
									var patientId = mData['patient_id'];	
						
									$("#txtIPDId").attr('patientId',patientId);
									var ipdIdLength = ipdId.length;
									for (var j=0;j<6-ipdIdLength;j++) {
										ipdId = "0"+ipdId;
									}
									ipdid = ipdPrefix+ipdId;
									$("#txtIPDId").val(ipdid);									
								}
				 
							}
							$(".close").unbind();
							$(".close").click();

						});
						
						// view icon show detail
						$(".inPatientDetails").click(function(){
							var data=$(this).parents('tr')[0];
							var mData = viewInPatientTable.fnGetData(data);
							$("#mySearchInPatient").modal();
							$("#searchInPatientLabel").text("Patient Details");
							var ipdId = mData['id'];	
							var ipdPrefix = mData['ipdPrefix'];						
							var ipdIdLength = ipdId.length;
							for (var j=0;j<6-ipdIdLength;j++) {
								ipdId = "0"+ipdId;
							}
							ipdid = ipdPrefix+ipdId;
							
							$("#mySearchInPatient #lblIPDId").text(ipdid);
							
							var patientId = mData['patient_id'];	
							var patientPrefix = mData['prefix'];						
							var patientIdLength = patientId.length;
							for (var j=0;j<6-patientIdLength;j++) {
								patientId = "0"+patientId;
							}
							patientid = patientPrefix+patientId;
								
							$("#mySearchInPatient #lblPatientId").text(patientid);
							$("#mySearchInPatient #lblPatientName").text(mData['patient_name']);
							$("#mySearchInPatient #lblDOB").text(mData['dob']);

							bindDepartment(mData['department_id']);						
							
							
							var ward_id = mData['ward_id'];					
							var room_id = mData['room_id'];					
							bindWard(mData['department_id'], ward_id);
							bindRoomType(ward_id,room_id);
							
							$("#mySearchInPatient #txtRoomNo").val(mData['number']);
							$("#mySearchInPatient #txtBedNo").val(mData['bedName']);
							
							oldBedId = mData['bed_id'];
							bedHistoryId = mData['bed_history_id'];
							oldbedPrice = mData['price'];
							
							$("#mySearchInPatient #txtBedNo").attr('data-id',mData['bed_id']);
							$("#mySearchInPatient #txtPrice").val(mData['price']);
							
							if(mData['is_icu'] == "YES") {
								$("#mySearchInPatient #checkICU").prop("checked",true);
							}else {
								$("#mySearchInPatient #checkICU").prop("checked",false);
							}				
						});
						
					},
					"aaData": dataSet,
					"aoColumns": [
						{ "mData": function (o) { 	
							var ipdId = o["id"];
							var ipdPrefix = o["ipdPrefix"];
								
							var ipdIdLength = ipdId.length;
							for (var i=0;i<6-ipdIdLength;i++) {
								ipdId = "0"+ipdId;
							}
							ipdId = ipdPrefix+ipdId;
							return ipdId; 
							}
						},
						{ "mData": "patient_name" },
						{ "mData": "department" },
						{ "mData": "name" },
						{ "mData": "number" },
						{ "mData": "bedName" },
						{ "mData": "is_icu" },
						{  
							"mData": function (o) { 
								var data = o;
								var id = data["id"];
								return "<i class='fa fa-eye inPatientDetails' id='btnView' data-id=" + id + " style='font-size: 17px; margin-left: 20px; cursor:pointer;' title='view' value='view'" +
									"></i>";
							}
						},
					],
					/*Disable sort for following columns*/
					aoColumnDefs: [
					   { 'bSortable': false, 'aTargets': [ 6 ] },
					   { 'bSortable': false, 'aTargets': [ 7 ] }
				   ]
				} );
			},
			error: function() {
									
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
		
	});
});

// bind department
function bindDepartment(value) {
		$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/patient_hospitalization.php",
		data: {
			"operation":"showDepartment"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>--Select--</option>";
				for (var i=0;i<parseData.length;i++)
				{
					option+="<option value='"+parseData[i].id+"'>"+parseData[i].department+"</option>";
				}
				$('#selDepartment').html(option);	
				if(value != ""){
					$('#mySearchInPatient #selDepartment').html(option);
					$("#mySearchInPatient #selDepartment").val(value);
					$("#selDepartment").val(value);
				}			
				
			}
		},
		error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
	});
}

//
//function binding ward
//
function bindWard(value,ward_id){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/bed.php",
		data: {
			"operation":"showWard",
			departmentId : value
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>--Select--</option>";
				for (var i=0;i<parseData.length;i++)
				{
				option+="<option value='"+parseData[i].id+"'>"+parseData[i].name+"</option>";
				}
				$('#selWard').html(option);
				if(ward_id != "") {
					$("#mySearchInPatient #selWard").html(option);
					$("#mySearchInPatient #selWard").val(ward_id);
					$("#selWard").val(ward_id);
				}												
			}
		},
		error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
	});
}

function bindRoomType(value, room_id) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/bed.php",
        data: {
			"operation":"showRoom",
			"ward" : value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].room_number + "</option>";
                }
                $('#selRoomType').html(option);
				if(room_id != "") { 
					$("#mySearchInPatient #selRoomType").html(option);
					$("#mySearchInPatient #selRoomType").val(room_id);
					$("#selRoomType").val(room_id);
				}
            }
        },
       error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
    });
}

// function load details
function loadDetails() {
		
	//Datatable code
	viewInPatientTable = $("#tblViewInPatient").dataTable({
		"bFilter": true,
		"processing": true,
		"bAutoWidth":false,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {
			$('#myBedTransferModal #tblViewInPatient tbody tr').on('dblclick', function() {
				if ($(this).hasClass('selected')) {
					$(this).removeClass('selected');
				} else {
					viewInPatientTable.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}

				var mData = viewInPatientTable.fnGetData(this); // get datarow
				
				if (null != mData) // null if we clicked on title row
				{	
					if($("#hdnIPD").val() != "1")
					{
						var ipdId = mData['id'];	
						var ipdPrefix = mData['ipdPrefix'];						
						var ipdIdLength = ipdId.length;
						for (var j=0;j<6-ipdIdLength;j++) {
							ipdId = "0"+ipdId;
						}
						ipdid = ipdPrefix+ipdId;
						$("#textIPDId").val(ipdid);
						$("#textIPDId").attr("disabled","disabled");
						
						$("#btnLoad").click();
						$("#selDepartment").val(mData['department_id']);
						var ward_id = mData['ward_id'];					
						var room_id = mData['room_id'];					
						bindWard(mData['department_id'], ward_id);
						bindRoomType(ward_id,room_id);
						$("#txtRoomNo").val(mData['number']);
						$("#txtBedNo").val(mData['bedName']);
						$("#hdnBesHistoryId").val(mData['bed_history_id']);
						$("#txtPrice").val(mData['price']);
						$("#txtBedNo").attr('data-id',mData['bed_id']);
						$("#hdnPatientId").val(mData["patient_id"]);
						 if(mData["is_icu"] == "YES"){
						  $('#checkICU').prop("checked",true);
						 }
						 else{
						  $('#checkICU').prop("checked",false);
						 }
					}
					else{
						var ipdId = mData['id'];	
						var patientId = mData['patient_id'];	
						var ipdPrefix = mData['ipdPrefix'];						
						var ipdIdLength = ipdId.length;
						for (var j=0;j<6-ipdIdLength;j++) {
							ipdId = "0"+ipdId;
						}
						ipdid = ipdPrefix+ipdId;
						$("#txtIPDId").val(ipdid);
						$("#txtIPDId").attr('patientId',patientId);
					}
	 
				}
				$(".close").unbind();
				$(".close").click();
			});
			
			// view icon show detail
			$(".inPatientDetails").click(function(){
				var data=$(this).parents('tr')[0];
				var mData = viewInPatientTable.fnGetData(data);
				$("#mySearchInPatient").modal();
				$("#searchInPatientLabel").text("Patient Details");
				var ipdId = mData['id'];	
				var ipdPrefix = mData['ipdPrefix'];						
				var ipdIdLength = ipdId.length;
				for (var j=0;j<6-ipdIdLength;j++) {
					ipdId = "0"+ipdId;
				}
				ipdid = ipdPrefix+ipdId;
				
				$("#mySearchInPatient #lblIPDId").text(ipdid);
				
				var patientId = mData['patient_id'];	
				var patientPrefix = mData['prefix'];						
				var patientIdLength = patientId.length;
				for (var j=0;j<6-patientIdLength;j++) {
					patientId = "0"+patientId;
				}
				patientid = patientPrefix+patientId;
					
				$("#mySearchInPatient #lblPatientId").text(patientid);
				$("#mySearchInPatient #lblPatientName").text(mData['patient_name']);
				$("#mySearchInPatient #lblDOB").text(mData['dob']);
				
				bindDepartment(mData['department_id']);
				
				var ward_id = mData['ward_id'];					
				var room_id = mData['room_id'];					
				bindWard(mData['department_id'], ward_id);
				bindRoomType(ward_id,room_id);
				
				$("#mySearchInPatient #txtRoomNo").val(mData['number']);
				$("#mySearchInPatient #txtBedNo").val(mData['bedName']);
				
				oldBedId = mData['bed_id'];
				bedHistoryId = mData['bed_history_id'];
				oldbedPrice = mData['price'];
				
				$("#mySearchInPatient #txtBedNo").attr('data-id',mData['bed_id']);
				$("#mySearchInPatient #txtPrice").val(mData['price']);
				
				if(mData['is_icu'] == "YES") {
					$("#mySearchInPatient #checkICU").prop("checked",true);
				}else {
					$("#mySearchInPatient #checkICU").prop("checked",false);
				}				
			});
			
		},
		
		"sAjaxSource":"controllers/admin/view_inpatients.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{ "mData": function (o) { 	
				var ipdId = o["id"];
				var ipdPrefix = o["ipdPrefix"];
					
				var ipdIdLength = ipdId.length;
				for (var i=0;i<6-ipdIdLength;i++) {
					ipdId = "0"+ipdId;
				}
				ipdId = ipdPrefix+ipdId;
				return ipdId; 
				}
			},
			{ "mData": "patient_name" },
			{ "mData": "department" },
			{ "mData": "name" },
			{ "mData": "number" },
			{ "mData": "bedName" },
			{ "mData": "is_icu" },
			{  
				"mData": function (o) { 
					var data = o;
					var id = data["id"];
					return "<i class='fa fa-eye inPatientDetails' id='btnView' data-id=" + id + " style='font-size: 17px; margin-left: 20px; cursor:pointer;' title='view' value='view'" +
						"></i>";
				}
			},
		],
		/*Disable sort for following columns*/
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 6 ] },
		   { 'bSortable': false, 'aTargets': [ 7 ] }
	   ]
	} );
}
// clear details
function clear(){
	$('input[type=text]').next("span").hide();
	$('input[type=text]').val("");
	$('input[type=text]').removeClass("errorStyle");
	$('select').next("span").hide();
	$('#txtReasonAdmission').val("");
	$('#txtNotes').val("");
	$('select').val("-1");
	$('select').removeClass("errorStyle");
	$('#checkICU').attr('checked',false);
	$("#textIPDId").removeAttr("disabled");
	$("#txtMobile").val("");
	$('select[name=tblViewInPatient_length]').val("10");
	loadDetails();
}