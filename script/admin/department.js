/*
 * File Name    :   department
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre operation
 */
var departementTable; //defining a global varibale for data table
$(document).ready(function() {
	loader();
    debugger;
    $("#form_dept").hide();
    $("#department_list").addClass('list');    
    $("#tab_dept").addClass('tab-list-add');

    $("#tab_add_dept").click(function() { // click on add department tab
        $("#form_dept").show();
        $(".blackborder").hide();
        clear();

        $("#tab_add_dept").addClass('tab-detail-add');
        $("#tab_add_dept").removeClass('tab-detail-remove');
        $("#tab_dept").removeClass('tab-list-add');
        $("#tab_dept").addClass('tab-list-remove');
        $("#department_list").addClass('list');
        $('#txtDepartmentName').focus();

        $("#btnAddDepartment").show();
        $("#btnReset").show();
        $("#uploadFile").show();
        $("#btnUpdateDepartment").hide();
        clear();
    });
    $("#tab_dept").click(function() { // click on list department tab
		showTableList();
    });

		// import excel on submit click
	$('#importDataFile').click(function(){
		var flag = false;
		if($('#file').prop('files')[0] == undefined || $('#file').val() == "") {
			$('#txtFileError').show();
			$('#txtFileError').text("Please choose file.");
			flag  = true;
		}
		if(flag == true){
			return false;
		}
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to upload this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			var file_data = $('#file').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: 'controllers/admin/department.php', // point to server-side PHP script 
				dataType: 'text', // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					if(data != "" && data != "invalid file" && data == "1"){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Your file has been successfully imported!!!");
						$('#messagemyModal').modal();
						$('#file').val("");
                        $("#txtFileError").text('');
					}
					else if(data == "invalid file"){
						$('#txtFileError').show();
						$('#txtFileError').text("Please import only CSV/XLS file.");
						$('#file').val("");
					}
                    else if (data =="You must have two column in your file i.e department and description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="First column should be department" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Second column should be description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Please insert data in first column and second column only i.e A & B") {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Data can't be null in department column" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
					else if(data == ""){
						$('#txtFileError').show();
						$('#txtFileError').text("Data already exist.");
						$('#file').val("");
					}
				},
				error: function() {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Data Not Found!!!");
					$('#messagemyModal').modal();

				}
			});
		});
	});
	
    /* click on button for save  inforamtion */
    $("#btnAddDepartment").click(function() {
        // validation
        var flag = validation();


        if (flag == "true") {
            return false;
        }

        //ajax call for save operation
        var DepartmentName = $("#txtDepartmentName").val();
        var description = $("#txtDescription").val();
        description = description.replace(/'/g, "&#39");
        var postData = {
            "operation": "save",
            "DepartmentName": DepartmentName,
            "description": description
        }

        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/department.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Department saved successfully!!!");
                    $('#messagemyModal').modal();
                    clear();
                    $("#txtFileError").text('');
                    //after sucessful data sent to database sent to datatable list
                   showTableList();

                } else {
                    $("#txtDepartmentNameError").text("Department name is already exists");
                    $("#txtDepartmentName").addClass("errorStyle");
                    $("#txtDepartmentName").focus();
                }

            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });

    });

    //keyup functionality for remove validation style
    $("#txtDepartmentName").keyup(function() {
        if ($("#txtDepartmentName").val() != '') {
            $("#txtDepartmentNameError").text("");
            $("#txtDepartmentName").removeClass("errorStyle");
        }
    });

    //load Datatable data on page load
    if ($('.inactive-checkbox').not(':checked')) {
      loadDatatable();  
    }

    //checked or not checked functionality to show active or inactive data
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) {
            departementTable.fnClearTable();
            departementTable.fnDestroy();
            departementTable = "";
            departementTable = $('#example-datatable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // restor click event
					$('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = departementTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            restoreClick(id);
                        }   
                    });
                },

                "sAjaxSource": "controllers/admin/department.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [{
                    "mData": "department"
                }, {
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                    }
                }, ],
                aoColumnDefs: [{
                    aTargets: [2],
                    bSortable: false
                }, {
                    aTargets: [1],
                    bSortable: false
                }]
            });
        } else {
            departementTable.fnClearTable();
            departementTable.fnDestroy();
            departementTable = "";
            loadDatatable();
        }
    });

    // for reset functionality
    $("#btnReset").click(function() {
        $("#txtDepartmentName").focus();
        $("#txtDepartmentName").removeClass("errorStyle");
        clear();
    });

}); //close document.ready

// use fucntion for edit department for update 
function editClick(id, department, description) {

    $("#tab_add_dept").click();
    $('#tab_add_dept').html("+Update Department");

    $("#btnAddDepartment").hide();
    $("#btnReset").hide();
	$("#uploadFile").hide();
    $("#btnUpdateDepartment").removeAttr("style");
    $("#txtDepartmentNameError").text("");
    $("#txtDepartmentName").removeClass("errorStyle");
    $('#txtDepartmentName').val(department);
    $('#txtDescription').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);

    $("#btnUpdateDepartment").click(function() {
        var flag = validation();

        if (flag == "true") {
            return false;
        }
		else{
			var DepartmentName = $("#txtDepartmentName").val();
			var description = $("#txtDescription").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/department.php",
					data: {
						"operation": "update",
						"id": id,
						"DepartmentName": DepartmentName,
						"description": description
					},
					success: function(data) {
						if (data != "0" && data != "") {
							$('#myModal').modal("hide");
							$('.close-confirm').click();
							$('.modal-body').text('');
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Department updated successfully!!!");
							$('#messagemyModal').modal();
                            $("#tab_dept").click();
							//departementTable.fnReloadAjax();
							clear();
						} else {
							$('.close-confirm').click();
							$("#txtDepartmentNameError").text("Department name is already exists");
							$("#txtDepartmentName").addClass("errorStyle");
							$('#txtDepartmentName').focus();

						}
					},
					error: function() {
						//$('#myModal').modal('hide');
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
				});
			});
		}
    });
}
// click function to delete departement
function deleteClick(id) {

    $('#selectedRow').val(id);
	$('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete department");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();

    var type = "delete";
    $('#confirm').attr('onclick', 'deleteDepartement("' + type + '");');
}

//Click function for restore the departement
function restoreClick(id) {
    $('#selectedRow').val(id);
	$('.modal-body').text("");
    $('#confirmMyModalLabel').text("Restore department");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();

    var type = "restore";
    $('#confirm').attr('onclick', 'deleteDepartement("' + type + '");');
}

//function for delete and restore the departementList
function deleteDepartement(type) {
    if (type == "delete") {
        var id = $('#selectedRow').val();

        //Ajax call for delete the departement 	
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/department.php",
            data: {
                "operation": "delete",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
					$('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Department deleted successfully!!!");
                    $('#messagemyModal').modal();
                    departementTable.fnReloadAjax();
                } else {
					$('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This department is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    } else {
        var id = $('#selectedRow').val();

        //Ajax call for restore the departement 
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/department.php",
            data: {
                "operation": "restore",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
					$('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Department restored successfully!!!");
                    departementTable.fnReloadAjax();
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    }
}
// clear function
function clear() {
    $('#txtDepartmentName').val("");
    $('#txtDepartmentNameError').text("");
    $('#txtFileError').text('');
    $('#txtDescription').val("");
    $('#txtDepartmentName').removeClass("errorStyle");
    $("#txtDepartmentName").removeClass("errorStyle");
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        $(".close").click();
    }
});
function loadDatatable(){
	departementTable = $('#example-datatable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "fnDrawCallback": function(oSettings) {
            // update click event   
			$('.update').unbind();
            $('.update').on('click', function() {
                var data = $(this).parents('tr')[0];
                var mData = departementTable.fnGetData(data);
                if (null != mData) // null if we clicked on title row
                {
                    var id = mData["id"];
                    var department = mData["department"];
                    var description = mData["description"];
                    editClick(id, department, description);
                }
            });
            // delete click event
			$('.delete').unbind();
            $('.delete').on('click', function() {
                var data = $(this).parents('tr')[0];
                var mData = departementTable.fnGetData(data);

                if (null != mData) // null if we clicked on title row
                {
                    var id = mData["id"];
                    deleteClick(id);
                }
            });
        },

        "sAjaxSource": "controllers/admin/department.php",
        "fnServerParams": function(aoData) {
            aoData.push({
                "name": "operation",
                "value": "show"
            });
        },
        "aoColumns": [{
            "mData": "department"
        }, {
            "mData": "description"
        }, {
            "mData": function(o) {
                return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                    "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                    " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                    "  style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                    "  data-original-title='Delete'></i>";
            }
        }, ],
        aoColumnDefs: [{
            aTargets: [2],
            bSortable: false
        }, {
            aTargets: [1],
            bSortable: false
        }]
    });
}

function validation(){
    var flag = "false";

    $("#txtDepartmentName").val($("#txtDepartmentName").val().trim());

    if ($("#txtDepartmentName").val() == '') {
        $('#txtDepartmentName').focus();
        $("#txtDepartmentNameError").text("Please enter department name");
        $("#txtDepartmentName").addClass("errorStyle");
        flag = "true";
    }
    if ($("#txtDepartmentName").val().length > 50) {
        $('#txtDepartmentName').focus();
        $("#txtDepartmentNameError").text("Please enter name less than 50");
        $("#txtDepartmentName").addClass("errorStyle");
        flag = "true";
    }
    return flag;
}

function showTableList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#form_dept").hide();
    $(".blackborder").show();

    $("#tab_add_dept").removeClass('tab-detail-add');
    $("#tab_add_dept").addClass('tab-detail-remove');
    $("#tab_dept").removeClass('tab-list-remove');    
    $("#tab_dept").addClass('tab-list-add');
    $("#department_list").addClass('list');

    $('#tab_add_dept').html("+Add Department");
    
    $("#txtDepartmentNameError").text("");
    $("#txtDepartmentName").removeClass("errorStyle");
}