var treatmentsTable;
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   treatments.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th feb, 2016
 * Description  :   This page  manages  treatments data
 *************************************************************************************************** */
  loader();

    $("#form_dept").hide();
	$("#lab_type_list").addClass('list');
	$("#tab_lab_type").addClass('tab-list-add');
	
	//Click function for change the tab
    $("#tab_add_lab_type").click(function() {
        $("#form_dept").show();
        $(".treatment").hide();
        $('#btnSave').show();
		$('#btnReset').show();
		$("#btnUpdate").hide();
        $("#myAddTreatmentModal  #btnAdd").hide();
		clear();
        $("#tab_add_lab_type").addClass('tab-detail-add');
        $("#tab_add_lab_type").removeClass('tab-detail-remove');
        $("#tab_lab_type").removeClass('tab-list-add');
        $("#tab_lab_type").addClass('tab-list-remove');
        $("#lab_type_list").addClass('list');
		$("#txtPriceError").text("");
        $("#txtPrice").removeClass("errorStyle");
		$('#txtPrice').focus();
    });
	
	//Click function for show the lab list
    $("#tab_lab_type").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
		tabLabType();    //Calling the function for show the lab type list    
    });

    if($("#myAddTreatmentModal #hdnAddTreatment").val() != "1"){
    	if($('.inactive-checkbox').not(':checked')){
	    	//Datatable code
		loadDataTable();	
	    }
	    $('.inactive-checkbox').change(function() {
	    	if($('.inactive-checkbox').is(":checked")){
		    	treatmentsTable.fnClearTable();
		    	treatmentsTable.fnDestroy();
		    	//Datatable code
				treatmentsTable = $('#tblTreatments').dataTable( {
					"bFilter": true,
					"processing": true,
					"bAutoWidth":false,
					"sPaginationType":"full_numbers",
					"fnDrawCallback": function ( oSettings ) {
						$('.restore').unbind();
						$('.restore').on('click',function(){
							var data=$(this).parents('tr')[0];
							var mData =  treatmentsTable.fnGetData(data);
						
							if (null != mData)  // null if we clicked on title row
							{
								var id = mData["id"];
								restoreClick(id);
							}
							
						});
					},
					
					"sAjaxSource":"controllers/admin/treatments.php",
					"fnServerParams": function ( aoData ) {
					  aoData.push( { "name": "operation", "value": "showInActive" });
					},
					"aoColumns": [
						{ "mData": "price" },
						{ "mData": "treatment" },	
						{  
							"mData": function (o) { 
							
							return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
						},	
					],
					aoColumnDefs: [
					   { 'bSortable': false, 'aTargets': [ 1 ] },
					   { 'bSortable': false, 'aTargets': [ 2 ] }
				   ]
				} );
			}
			else{
				treatmentsTable.fnClearTable();
		    	treatmentsTable.fnDestroy();
		    	//Datatable code
				loadDataTable();
			}
	    });
    }
    else{
    	$("#btnAdd").removeAttr("style");
    	treatmentsTable = $('#tblTreatments').dataTable( {
			"bFilter": true,
			"processing": true,
			"bAutoWidth":false,
			"sPaginationType":"full_numbers",
			"fnDrawCallback": function ( oSettings ) {
				$("#myAddTreatmentModal #btnAdd").unbind();
				$("#myAddTreatmentModal #btnAdd").click(function(){
					var aiRows = treatmentsTable.fnGetNodes();
					var rowcollection =  treatmentsTable.$(".checkDetails:checked", {"page": "all"});	//Select all unchecked row only
					for (i=0,c=rowcollection.length; i<c; i++) {
						var id = $(rowcollection[i]).attr('data-id');
						var treatment = $($(rowcollection[i]).parent().siblings()[1]).text();

						addTreatmentsTable.fnAddData([i+1,treatment,
						"<input type='button'  class='remove' style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;'"+					 
						   " style='font-size:22px; cursor:pointer;' data-id="+id+"  onclick='rowRadDelete($(this));' value='Remove'>"]);
					}
					$(".close").unbind();
					$(".close").click();
				});
				
			},
			
			"sAjaxSource":"controllers/admin/treatments.php",
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "operation", "value": "show" });
			},
			"aoColumns": [
				{ "mData": "price" },
				{ "mData": "treatment" },	
				{  
					"mData": function (o) { 
						var data = o;
						var id = data["id"];
						return "<input type='checkbox'  class='checkDetails' data-id="+id+" "+
						" style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;'"+					 
					    " style='font-size: 22px; cursor:pointer;'>"; 
					}
				},	
			],
			aoColumnDefs: [
			   { 'bSortable': false, 'aTargets': [ 1 ] },
			   { 'bSortable': false, 'aTargets': [ 2 ] }
		   ]
		} );
    }
	
	
	
	//Click function for validate and save the data
    $("#btnSave").click(function() {
		
		var flag = "false";
		
		$("#txtPrice").val($("#txtPrice").val().trim());

		if ($("#txtTreatments").val() == '') {
			$('#txtTreatments').focus();
            $("#txtTreatmentsError").text("Please enter treatments");
            $("#txtTreatments").addClass("errorStyle");
            flag = "true";
        }
		
        if ($("#txtPrice").val() == '' || $("#txtPrice").val() < 0) {
			$('#txtPrice').focus();
            $("#txtPriceError").text("Please enter price");
            $("#txtPrice").addClass("errorStyle");
            flag = "true";
        }
		
		if(flag == "true"){			
			return false;
		}
		
		//ajax call
		
		var price = $("#txtPrice").val();
		var treatment = $("#txtTreatments").val();
		var postData = {
			"operation":"save",
			"price":price,
			"treatment":treatment
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/treatments.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Treatments saved successfully!!!");
					$('#messagemyModal').modal();
					$('#inactive-checkbox-tick').prop('checked', false).change();
					tabLabType();
					clear();
				}			
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
		
    });

    //Click function for validate and save the data
    $("#myAddTreatmentModal #btnSave").click(function() {
		
		var flag = "false";
		
		$("#myAddTreatmentModal #txtPrice").val($("#myAddTreatmentModal #txtPrice").val().trim());

		if ($("#myAddTreatmentModal #txtTreatments").val() == '') {
			$('#myAddTreatmentModal #txtTreatments').focus();
            $("#myAddTreatmentModal #txtTreatmentsError").text("Please enter treatments");
            $("#myAddTreatmentModal #txtTreatments").addClass("errorStyle");
            flag = "true";
        }
		
        if ($("#myAddTreatmentModal #txtPrice").val() == '') {
			$('#myAddTreatmentModal #txtPrice').focus();
            $("#myAddTreatmentModal #txtPriceError").text("Please enter price");
            $("#myAddTreatmentModal #txtPrice").addClass("errorStyle");
            flag = "true";
        }
		
		if(flag == "true"){			
			return false;
		}
		
		//ajax call
		
		var price = $("#myAddTreatmentModal #txtPrice").val();
		var treatment = $("#myAddTreatmentModal #txtTreatments").val();
		var postData = {
			"operation":"save",
			"price":price,
			"treatment":treatment
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/treatments.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					treatmentsTable.fnReloadAjax();
					tabLabType();
					clear();
				}			
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
		
    });

    //keyup functionality
    $("#myAddTreatmentModal #txtPrice").keyup(function() {
        if ($("#txtPrice").val() != '') {
            $("#txtPriceError").text("");
            $("#txtPrice").removeClass("errorStyle");
        } 
    });   
	
	//keyup functionality
    $("#myAddTreatmentModal #txtTreatments").keyup(function() {
        if ($("#txtTreatments").val() != '') {
            $("#txtTreatmentsError").text("");
            $("#txtTreatments").removeClass("errorStyle");
        } 
    }); 

    //keyup functionality
    $("#txtPrice").keyup(function() {
        if ($("#txtPrice").val() != '') {
            $("#txtPriceError").text("");
            $("#txtPrice").removeClass("errorStyle");
        } 
    });   
	
	//keyup functionality
    $("#txtTreatments").keyup(function() {
        if ($("#txtTreatments").val() != '') {
            $("#txtTreatmentsError").text("");
            $("#txtTreatments").removeClass("errorStyle");
        } 
    }); 
});//Close document.ready function 

//Function for edit button
function editClick(id,price,treatment){	
	
	$('#tab_add_lab_type').click();
	$('#tab_add_lab_type').html("+Update Treatment");
	$('#btnSave').hide();
	$('#btnReset').hide();
	$("#btnUpdate").removeAttr("style");
	$("#txtPriceError").text("");
    $("#txtPrice").removeClass("errorStyle");
	$('#txtPrice').val(price);
	$('#txtTreatments').val(treatment);
	$('#selectedRow').val(id);
	
	//Click function for update the data
	$("#btnUpdate").click(function(){
		var flag = validation();
		
		if(flag == "true"){			
			return false;
		}
		else{
			var price = $("#txtPrice").val();
			var treatments = $("#txtTreatments").val();
			var id = $('#selectedRow').val();			
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){	
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/treatments.php",
					data :{            
						"operation" : "update",
						"id" : id,
						"price":price,
						"treatments":treatments
					},
					success: function(data) {	
						if(data != "0" && data != ""){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Treatments updated successfully!!!");
							treatmentsTable.fnReloadAjax();
							$('#messagemyModal').modal();
							$('#tab_lab_type').click();
							clear();
						}
					},
					error:function()
					{
						$('.close-confirm').click();
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();			  
					}
				});
			});
		}
	});
	
	$('#myModal #txtPrice').keyup(function() {
        if ($("#myModal #txtPrice").val() != '') {
            $("#myModal #txtPriceError").text("");
            $("#myModal #txtPrice").removeClass("errorStyle");
        }
    });

    $('#myModal #txtTreatments').keyup(function() {
        if ($("#myModal #txtTreatments").val() != '') {
            $("#myModal #txtTreatmentsError").text("");
            $("#myModal #txtTreatments").removeClass("errorStyle");
        }
    });
}

//Click function for delete button
function deleteClick(id){	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete Treatment");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteLabType("'+type+'");');	//Calling the deleteLabType function for delete the data 
}

function restoreClick(id){
	$('#selectedRow').val(id);	
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Restore Treatment");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal(); 
	
	var type="restore";
	$('#confirm').attr('onclick','deleteLabType("'+type+'");'); //Calling the deleteLabType function for restore the data 
}

//Function for delete and restore the data
function deleteLabType(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();		
			
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/treatments.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Treatments  deleted successfully!!!");
					treatmentsTable.fnReloadAjax();
					$('#messagemyModal').modal();
				}
				else{
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("This treatments is used , so you can not delete!!!");
					$('#messagemyModal').modal();
				}
			},
			error:function()
			{
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
		
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/treatments.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Treatments restored successfully!!!");
					treatmentsTable.fnReloadAjax();
					$('#messagemyModal').modal();
				}			
			},
			error:function()
			{
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();			  
			}
		});
	}
}

//Function for show the list 
function tabLabType(){
	$("#form_dept").hide();
	$(".treatment").show();
    $("#myAddTreatmentModal  #btnAdd").show();
	$('#tab_add_lab_type').html("+Add Treatments");
	$("#tab_add_lab_type").removeClass('tab-detail-add');
	$("#tab_add_lab_type").addClass('tab-detail-remove');
	$("#tab_lab_type").addClass('tab-list-add');
	$("#tab_lab_type").removeClass('tab-list-remove');
}

// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		 $(".close").click();
    }
});

//Click function for reset the button 
$("#btnReset").click(function(){
	clear();
});

//Click function for reset the button 
$("#myAddTreatmentModal #btnReset").click(function(){
	clear();
	$("#txtPrice").focus();
});


function clear(){
	$("#txtPrice").focus();
	$('#txtPrice').val("");
	$('#txtPriceError').text("");
	$('#txtTreatmentsError').text("");
	$('#txtTreatments').val("");
	$("#txtPrice").removeClass("errorStyle");
	$("#txtTreatments").removeClass("errorStyle");
}

function loadDataTable(){
	treatmentsTable = $('#tblTreatments').dataTable( {
			"bFilter": true,
			"processing": true,
			"bAutoWidth":false,
			"sPaginationType":"full_numbers",
			"fnDrawCallback": function ( oSettings ) {
				$('.update').unbind();
				$('.update').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData = treatmentsTable.fnGetData(data);
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						var price = mData["price"];
						var treatment = mData["treatment"];
						editClick(id,price,treatment);
	   
					}
				});
				$('.delete').unbind();
				$('.delete').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData =  treatmentsTable.fnGetData(data);
					
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						deleteClick(id);
					}
				});
			},
			
			"sAjaxSource":"controllers/admin/treatments.php",
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "operation", "value": "show" });
			},
			"aoColumns": [
				{ "mData": "price" },
				{ "mData": "treatment" },	
				{  
					"mData": function (o) { 
					var data = o;
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
					   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
					   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
					   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
					   "  data-original-title='Delete'></i>"; 
					}
				},	
			],
			aoColumnDefs: [
			   { 'bSortable': false, 'aTargets': [ 1 ] },
			   { 'bSortable': false, 'aTargets': [ 2 ] }
		   ]
		} );
}
//# sourceURL=filename.js
function validation(){		// validation
	var flag="false";
	$("#txtPrice").val($("#txtPrice").val().trim());
		
		if ($("#txtTreatments").val().trim() == '') {
			$('#txtTreatments').focus();
			$('#txtTreatmentsError').text("Please enter treatments");
			$("#txtTreatments").addClass("errorStyle");
			flag = "true";
		}

		if ($("#txtPrice").val() == '') {
			$('#txtPrice').focus();
			$("#txtPriceError").text("Please enter price");
			$("#txtPrice").addClass("errorStyle");
			flag = "true";
		}
		return flag;
}