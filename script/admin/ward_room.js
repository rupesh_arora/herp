var wardRoomTable;//define global variable for datatable
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   ward_room.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Arora
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages ward room
 *************************************************************************************************** */

	loader();
	/*By default hide this add screen part*/
	$("#advanced-wizard").hide();
    $("#roomList").addClass('list');    
    $("#tabRoomList").addClass('tab-list-add');
	// load department
	bindDepartment();
	
	 //bind ward on click of department
    $('#selDepartment').change(function() {
        var departmentID = $("#selDepartment :selected").val();
        bindWard(departmentID,'');
    });
    debugger;
    $('#checkOtherRoom').change(function(){
    	 if ($('#checkOtherRoom').is(":checked")==false) {
	        $('#checkOtherRoom').val("N");
			$('#selWard').prop('disabled',false);
	    }
	    else{            
	        $("#selWard").val("-1")
			$('#selWard').prop('disabled',true);
	    }
    })
   
	
    //Click for go to add the room screen part
    $("#tabAddRoom").click(function() {
    	showAddTab();
		clear();
    });

    /*Click function for show the room lists*/
    $("#tabRoomList").click(function() {
        tabRoomList();// Call the function for show the room type lists
    });

	/*call functions to update it's list */
	bindDepartment();
	
	/*By default when radio button is not checked show all active data*/
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		loadDataTable();
    }
    /*On change of radio button check it is checked or not*/
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){//show inactive data
	    	wardRoomTable.fnClearTable();//clear datatable
	    	wardRoomTable.fnDestroy();//destroy datatable
	    	//Datatable code
			wardRoomTable = $('#roomTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"bAutoWidth":false,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					/*On click of restore call this function*/
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  wardRoomTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var wardId = mData["ward_id"];
							//var roomType = mData["room_type_id"];
							restoreClick(id,wardId);//call restore click function with certain parameters to restore data
						}						
					});
				},
				
				"sAjaxSource":"controllers/admin/ward_room.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					{ "mData": "departmentName" },
					{ "mData": "name" },
					//{ "mData": "type" },
					{ "mData": "number" },
					{ "mData": "description" },
					{  
						"mData": function (o) { 
							/*Return values that we get on click restore icon*/
							return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
						}
					},	
				],
				/*Disable sort for following columns*/
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 3 ] },
				   { 'bSortable': false, 'aTargets': [ 4 ] }
				]
			} );
		}
		else{
			wardRoomTable.fnClearTable();
	    	wardRoomTable.fnDestroy();
	    	//Datatable code
			loadDataTable();
		}
    });
    
    /*On click on this button save data*/
    $("#btnSubmit").click(function() {
    	/*perform validation*/
        var flag = "false";

		$("#txtRoomNumber").val($("#txtRoomNumber").val().trim());
		if ($("#txtRoomNumber").val() == "") {
			$('#txtRoomNumber').focus();
			$("#txtRoomNumberError").text("Please enter room number");
			$("#txtRoomNumber").addClass("errorStyle");  
			flag = "true";
		}
		if ($('#checkOtherRoom').is(":checked")==true) {
            $('#checkOtherRoom').val("Y");
        }
        else{
            $('#checkOtherRoom').val("N");
            if ($("#selWard").val() == "-1") {
				$('#selWard').focus();
				$("#selWardError").text("Please select ward");
				$("#selWard").addClass("errorStyle");    
				flag = "true";
			}
        }
		
		if ($("#selDepartment").val() == "-1") {
			$('#selDepartment').focus();
			$("#selDepartmentError").text("Please select department");
			$("#selDepartment").addClass("errorStyle");    
			flag = "true";
		}
		if (flag == "true") {
			return false;
		}
		
		var wardId = $("#selWard").val();
		var departmentId = $("#selDepartment").val();
		var roomType = $("#checkOtherRoom").val();
		var roomNumber = $("#txtRoomNumber").val();
		var description = $("#txtDescription").val();
		description = description.replace(/'/g, "&#39");
		var postData = {
			"operation":"save",
			"wardId":wardId,
			"departmentId":departmentId,
			"roomType":roomType,
			"roomNumber":roomNumber,
			"description":description
		}
		/*AJAX call to save data*/
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/ward_room.php",
			datatype:"json",
			data: postData,			
			success: function(data) {
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Room saved successfully!!!");
					$('#messagemyModal').modal();
					$('#inactive-checkbox-tick').prop('checked', false).change();
					bindDepartment();
					tabRoomList();
				}
				else{
					$('.modal-body').text("");
					$("#txtRoomNumberError").text("Room number already exists");
					$('#txtRoomNumber').focus();
					$("#txtRoomNumber").addClass("errorStyle");
				}
				
			},
			error:function() {
				$('.modal-body').text("");
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});	
	
	});	
	
    //keyup functionality
    $("#selWard").change(function() {
        if ($("#selWard").val() != "-1") {
            $("#selWardError").text("");
			$("#txtRoomNumberError").text("");
            $("#selWard").removeClass("errorStyle");
            $("#txtRoomNumber").removeClass("errorStyle");
        } 
    });
	$("#selDepartment").change(function() {
        if ($("#selDepartment").val() != "-1") {
            $("#selDepartmentError").text("");
            $("#selDepartment").removeClass("errorStyle");
        } 
    });
	$("#selRoomType").change(function() {
        if ($("#selRoomType").val() != "-1") {
            $("#selRoomTypeError").text("");
            $("#selRoomType").removeClass("errorStyle");
            $("#txtRoomNumberError").text("");
            $("#txtRoomNumber").removeClass("errorStyle");
        } 
    });
    $("#txtRoomNumber").keyup(function() {
        if ($("#txtRoomNumber").val() != "") {
            $("#txtRoomNumberError").text("");
            $("#txtRoomNumber").removeClass("errorStyle");
        }
    });
});

/*define edit Click function for update*/
function editClick(id,ward_id,number,description,department_id,other){	
	showAddTab();
    $("#btnReset").hide();
    $("#btnSubmit").hide();
    $('#btnUpdateRoom').show();
    $('#tabAddRoom').html("+Update Room");
	$('#selDepartment').val(department_id);
	
	//bind ward on click of department
    $('#selDepartment').change(function() {
        var departmentID = $("#selDepartment :selected").val();
		$('#selWard').val("-1");
        bindWard(departmentID,'');
    });	
	$('#txtRoomNumber').val(number);
	$('#txtDescription').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id);
	if(other == "Y"){
		$('#checkOtherRoom').prop("checked",true);         
        $("#selWard").val("-1")
		$('#selWard').prop('disabled',true);
	}
	else{
		$('#checkOtherRoom').prop("checked",false); 
		$('#selWard').prop('disabled',false);
		bindWard(department_id,ward_id);
	}
	
	$("#btnUpdateRoom").click(function(){
		var flag = "false";
		$("#txtRoomNumber").val($("#txtRoomNumber").val().trim());
		
		if ($("#txtRoomNumber").val() == "") {
			$('#txtRoomNumber').focus();
			$("#txtRoomNumberError").text("Please enter room number");
			$("#txtRoomNumber").addClass("errorStyle");   
			flag = "true";
		}
		if ($('#checkOtherRoom').is(":checked")==true) {
            $('#checkOtherRoom').val("Y");
        }
        else{
            $('#checkOtherRoom').val("N");
            if ($("#selWard").val() == "-1") {
				$('#selWard').focus();
				$("#selWardError").text("Please select ward");
				$("#selWard").addClass("errorStyle");    
				flag = "true";
			}
        }
		if ($("#selDepartment").val() == "-1") {
			$('#selDepartment').focus();
			$("#selDepartmentError").text("Please select department");
			$("#selDepartment").addClass("errorStyle");    
			flag = "true";
		}
		if ($("#txtRoomNumberError").text() != "") {
			flag = "true";
		}
		
		if(flag == "true"){			
			return false;
		}
		else{
			var wardId = $("#selWard").val();
			var roomType = $("#checkOtherRoom").val();
			var roomNumber = $("#txtRoomNumber").val();
			var description = $(" #txtDescription").val();
			var departmentId = $(" #selDepartment").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();	
			
			$('#updateBody').text('');
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				/*AJAX call to update data*/
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/ward_room.php",
					data :{            
						"operation" : "update",
						"id" : id,
						"wardId":wardId,
						"departmentId":departmentId,
						"roomType":roomType,
						"roomNumber":roomNumber,
						"description":description
					},
					success: function(data) {	
						if(data != "0" && data != ""){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Room updated successfully!!!");
							$('#messagemyModal').modal();
							tabRoomList();
						}
						if(data == "0"){
							$("#txtRoomNumberError").text("Room number already exists");
							$('#txtRoomNumber').focus();
							$("#txtRoomNumber").addClass("errorStyle");
							
						}
					},
					error:function() {
						$('.close-confirm').click();
						$('#messagemyModal').modal();
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					}
				});
			});
		}
	});
}

/*Define function for delete the room */ 
function deleteClick(id){	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete Room");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteRoom("'+type+'");');//pass attribute with sent opertion and function with it's type	
}

/*Define function for delete the room */ 
function restoreClick(id,wardId){
	$('#selectedRow').val(id);
	$('#selWard').val(wardId);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Restore Room");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();	
	
	var type="restore";
	$('#confirm').attr('onclick','deleteRoom("'+type+'",'+roomType+');');	
}

function deleteRoom(type,roomType){
	if(type=="delete"){
		var id = $('#selectedRow').val();			
		/*AJAX call to delete data*/	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/ward_room.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Room deleted successfully!!!");
					wardRoomTable.fnReloadAjax();
					$('#messagemyModal').modal();					 
				}
				else{
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("This room is used , so you can not delete!!!");
					$('#messagemyModal').modal();
				}
			},
			error:function() {
				$('.modal-body').text("");
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
		var ward_id = $('#selWard').val();
		var room_type = roomType;
		if(ward_id == null){
			$('.modal-body').text("");
			$('#messageMyModalLabel').text("Sorry");
			$('.modal-body').text("Room can not be restore!!!");
			wardRoomTable.fnReloadAjax();
			$('#messagemyModal').modal();
		}
		else{
			/*Ajax call to restore data*/
			$.ajax({
				type: "POST",
				cache: "false",
				url: "controllers/admin/ward_room.php",
				data :{            
					"operation" : "restore",
					"id" : id,
					"ward_id" : ward_id,
					"room_type":room_type
				},
				success: function(data) {	
					if(data == "1"){
						$('.modal-body').text('');
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Room restored successfully!!!");
						wardRoomTable.fnReloadAjax();
						$('#messagemyModal').modal();
					}		
					if(data == "0"){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Sorry");
						$('.modal-body').text("Room can not be restore!!!");
						wardRoomTable.fnReloadAjax();
						$('#messagemyModal').modal();
					}					
				},
				error:function() {
					$('#messagemyModal').modal();
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				}
			});
		}		
	}
}

/*Function to hide second part of screen*/
function tabRoomList(){
	$("#advanced-wizard").hide();
	$(".blackborder").show();
	$('#inactive-checkbox-tick').prop('checked', false).change();
	$("#tabAddRoom").removeClass('tab-detail-add');
    $("#tabAddRoom").addClass('tab-detail-remove');
    $("#tabRoomList").removeClass('tab-list-remove');    
    $("#tabRoomList").addClass('tab-list-add');
    $("#divTaxType").addClass('list');

    $("#btnReset").show();
    $("#btnSubmit").show();
    $('#btnUpdateRoom').hide();
    $('#tabAddRoom').html("+Add Room");

	clear();
	bindDepartment();
	$("#selWard").removeClass("errorStyle");
	$("#txtRoomNumber").removeClass("errorStyle")
}
function showAddTab(){
    $("#advanced-wizard").show();
    $(".blackborder").hide();

    $("#tabAddRoom").addClass('tab-detail-add');
    $("#tabAddRoom").removeClass('tab-detail-remove');
    $("#tabRoomList").removeClass('tab-list-add');
    $("#tabRoomList").addClass('tab-list-remove');
    $("#roomList").addClass('list');
    $("#selDepartment").focus();
}

// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) { 
		$(".close").click();
    }
});

/*reset button functionality*/
$("#btnReset").click(function(){
	clear();
	$("#selRoomType").removeClass("errorStyle");
	$("#selWard").removeClass("errorStyle");
	$("#selWard").focus();
	$("#txtRoomNumber").removeClass("errorStyle");
	$("#selDepartment").removeClass("errorStyle");
	bindDepartment();
});

function clear(){
	$('#selWard').val("-1");
	$('#selDepartment').val("-1");
	$('#selWardError').text("");
	$('#txtRoomNumber').val("");
	$('#txtRoomNumberError').text("");
	$('#selDepartmentError').text("");
	$('#txtDescription').val("");
}

// bind department
function bindDepartment() {
	$('#selWard').val("-1");
	$('#myModal #selWard').val("-1");
		$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/patient_hospitalization.php",
		data: {
			"operation":"showDepartment"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>--Select--</option>";
				for (var i=0;i<parseData.length;i++)
				{
					option+="<option value='"+parseData[i].id+"'>"+parseData[i].department+"</option>";
				}
				$('#selDepartment').html(option);				
				$('#selWard').html("<option value='-1'>--Select--</option>");				
				
			}
		},
		error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
	});
}

// bind ward
function bindWard(departmentID,ward_id) {	
	
	 $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/patient_hospitalization.php",
        data: {
            "operation": "showWard",
            departmentID: departmentID
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selWard').html(option);
				if(ward_id != "") {
					$('#selWard').val(ward_id);
				}				
            }
        },
        error: function() {}
    });
}
//# sourceURL = ward_room.js

function loadDataTable(){
wardRoomTable = $('#roomTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"bAutoWidth":false,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {
			/*On click of update icon call this function*/
			$('.update').unbind();
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = wardRoomTable.fnGetData(data);//get datatable data
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					var ward_id = mData["ward_id"];
					var other = mData["other"];
					var number = mData["number"];
					var description = mData["description"];
					var department_id = mData["department_id"];
					editClick(id,ward_id,number,description,department_id,other);//call edit click function to update data		   
				}
			});
			/*On click of delete icon call this function*/
			$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  wardRoomTable.fnGetData(data);//get datatable data
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);//call dlete click function with certain parameters to delete data
				}
			});
		},
		
		"sAjaxSource":"controllers/admin/ward_room.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		/*defining datatables column*/
		"aoColumns": [
			{ "mData": "departmentName" },
			{ "mData": "name" },
			//{ "mData": "type" },
			{ "mData": "number" },
			{ "mData": "description" },
			{  
				"mData": function (o) { 
				/*Return values that we get on click of update and delete*/
				return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
				   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
				   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
				   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
				   "  data-original-title='Delete'></i>"; 
				}
			},	
		],
		/*Disable sort for following columns*/
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 4 ] },
		   { 'bSortable': false, 'aTargets': [ 3 ] }
		]
	} );
}