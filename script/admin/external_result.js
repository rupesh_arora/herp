var uploadTable;
var files = '';
$(document).ready(function() {
	debugger;

	if ($('#thing').val() == 3) {
		var getVisitValue = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
		var labPatientId = parseInt($("#txtPatientID").val().replace ( /[^\d.]/g, '' ));
		showResult(getVisitValue,labPatientId);
	}
	else if ($('#thing').val() == 4) {
		var getVisitValue = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
		var labPatientId = parseInt($("#txtPatientID").val().replace ( /[^\d.]/g, '' ));
		showResult(getVisitValue,labPatientId);
	}
	else {
		var getVisitValue = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
		var labPatientId = parseInt($("#txtPatientIdHistory").val().replace ( /[^\d.]/g, '' ));
		showResult(getVisitValue,labPatientId);
	}

	uploadTable = $("#uploadExternalResultTbl").dataTable();
	
});

function getMultipleFiles(label,containerDiv,inputTypeId){
	$(label).text("");
	$(containerDiv).html('');
	files = !!inputTypeId.files ? inputTypeId.files : [];

	if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	$(label).text(files.length +" files");
	for(i = 0 ;i<files.length;i++){
		if (/^image/.test(files[i].type)) { // only image file
			var reader = new FileReader();
			$(containerDiv).append('<label class="control-label imageLabel" id ="imageList'+i+'">'+files[i].name+'<i class="fa fa-times deleteImage" title="Delete" ></i></label>');// instance of the FileReader
		}
		else {
			$(label).text("Choose file");
			(inputTypeId).val('');//reset the file type
			callSuccessPopUp("Alert","Choose only image");
			return false;
		}
		$('.deleteImage').on('click', function(event) {   				
			   
			var fileName = $(this).parent().text();	                     
			$(this).parent().remove(); 
			files = jQuery.grep(files, function(value) {
				return value.name != fileName;
			});	
			$(label).text(files.length +" files");
			if(files.length == 0){
				$(label).text("No file choosen.");//change label
				$(containerDiv).text("");//remove text from div

				$(inputTypeId).val('');//reset the file type
			}
		});
	}
}

function showResult(visitId,patientId){
	var postData = {
		operation : "showResult",
		visitId : visitId,
		patientId : patientId
	}
	dataCall("controllers/admin/external_result.php", postData, function (result){
		var parseData = JSON.parse(result)
		if (result.length > 2) {
			uploadTable.fnClearTable();
			$.each(parseData,function(i,v){
				var tableName = v.tbl_name;
				var name = v.name;
				if (tableName !='' && name!='' && v.id !='') {
					uploadTable.fnAddData([tableName,name,'<input type="file" name="pic[]" multiple="true" onchange="getLabelDiv($(this))" class ="uploadImage" accept="image/*"/><label id="fileLabelLab">Choose file</label><div class="uploadFilenames"></div>','<i class="fa fa-upload" onclick="uploadData($(this))" aria-hidden="true"></i><i class="staffDetails fa fa-eye" title="View"></i>',v.id]);
				}
			});
		}
	});
}
function getLabelDiv(mythis){
	var label = $($(mythis).siblings()[0]);
	var containerDiv = $(mythis).siblings()[1];
	
	getMultipleFiles(label,containerDiv,mythis[0]);
}
function uploadData(thisUpload) {
	if(files.length > 0){

		var form_data = new FormData();
		for (var i = 0; i < files.length; i++) {
			fileup = files[i];
			form_data.append('filename[]',fileup,fileup.name);
		}
		var imageName = "";
		$.ajax({
			url: 'controllers/admin/external_result.php', // point to server-side PHP script 
			dataType: 'text', // what to expect back from the PHP script, if anything
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'post',
			success: function(data) {
				if (data == "invalid file") {

				}
				else {
					var parseData = jQuery.parseJSON(data);
					for (var i = 0; i < parseData.length; i++) {
						img = parseData[i];
						imageName = imageName+img+";";
					}
					$('#operation').val("saveResult");
					var imageArray = {};
					imageArray.name = $($(thisUpload).parent().siblings()[0]).text();
					imageArray.value = imageName;
					var formData = [];
					formData.push(imageArray);

					dataCall("controllers/admin/external_result.php", postData, function (result){});
				}
			}
		});
	}
}