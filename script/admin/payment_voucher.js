var sellerHiddenFieldValue;
var receiptNumberTable;
var nonRegisteredPayeeTable;
var countTax = 0;
var countBlur = 0;
var countGrossAmount = 0;
var countNRNetAmount = 0;
var countNRTaxAmount = 0;
var payeeName = '';
var vocherTable;
var customerId ='';
var updateNonRegisteredPayeeTable;
var updateReceiptNumberTable;
var sumGrossAmount = 0;
var sumTaxAmount = 0;
var sumNetAmount = 0;
var sumDiscount = 0;
var totalamount = 0;
var netTotalAmount = 0;
var glAccountId ;
var printItemDetails = '';
var printSellerInvoice = '';
var dataTableIntialization = 0;
var filldiscount =0;
$(document).ready(function(){
    loader();
    $('#hdnAccountPayable').hide();
    $("#advanced-wizard").hide();
	$("#chartOfAccountList").css({
        "background-color": "#fff"
    });
	$("#tabChartOfAccountList").css({
        "background-color": "#0c71c8",
        "color": "#fff"
    });

	/*Click for add the sub Type*/
    $("#tabAddChartOfAccount").click(function() {
        $("#advanced-wizard").show();
        $(".blackborder").hide();
		clear();
        $("#tabAddChartOfAccount").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tabChartOfAccountList").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#chartOfAccountList").css({
            "background": "#fff"
        });
        
        $("#mainScreen, #registerPayee, #supplier").addClass('activeSelf');
        $("#mainScreen").click();
        filldiscount = 0;
    });
	//Click function for show the chart of account lists
    $("#tabChartOfAccountList").click(function() {
        $("#txtSupplier").removeAttr('supplier-id');
        clear();
        tabChartOfAccountList();
        $('#hdnAccountPayable').hide();
    });

    debugger;
    bindLedger();
    removeErrorMessage()
    appendTaxes();
	bindGLAccount("#nonRegisteredPayeeModal #selLedger",'');
	$('.saveUpdateButton').hide();
		

    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code 
        vocherTable = $('#vocherTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "bAutoWidth": false,
            "order": [[ 2, 'asc' ]],
            "aaSorting": [],
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.updatePayment').unbind();
                $('.updatePayment').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = vocherTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var ledgerId = mData["ledger_id"];   
                        var paymentModeId = mData["payment_mode_id"];                   
                        var registerPayeeId = mData["registered_payee_id"];
                        var registerPayeeType = mData["registered_payee_type"];             
                        var payeeName = mData["payee_name"];
                        editPaymentClick(id,ledgerId,paymentModeId,registerPayeeId,registerPayeeType,
                        	payeeName
                        );

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = vocherTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
                if (dataTableIntialization == 1) {
                	disableRows();
                }                
            },
            "sAjaxSource": "controllers/admin/payment_voucher.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": function (o) {  
	                    var voucherId = o["id"];
	                        
	                    var voucherIdLength = voucherId.length;
	                    for (var i=0;i<6-voucherIdLength;i++) {
	                        voucherId = "0"+voucherId;
	                    }
	                    voucherId = voucherPrefix+voucherId;
	                    return voucherId; 
	                }
                }, {
                    "mData": "payee_name"
                }, {
                    "mData": "ledger"
                }, {
                    "mData": "amount"
                }, {
                    "mData": "payment_mode"
                }, {
                    "mData": "cheque_no"
                },  {
                    "mData": function(o) {
                        var data = o;
                        dataTableIntialization = 1;
                        return "<i class='ui-tooltip fa fa-pencil updatePayment' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [6]
            }]
        });
    }

    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            vocherTable.fnClearTable();
            vocherTable.fnDestroy();
            dataTableIntialization = 0;
            vocherTable = $('#vocherTable').dataTable({
	            "bFilter": true,
	            "processing": true,
	            "sPaginationType": "full_numbers",
	            "bAutoWidth": false,
	            "order": [[ 2, 'asc' ]],
	            "aaSorting": [],
	            "fnDrawCallback": function(oSettings) {
	                // perform update event
	                $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = vocherTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            restoreClick(id);

                        }
                    });
	                //perform delete event
	                $('.delete').unbind();
	                $('.delete').on('click', function() {
	                    var data = $(this).parents('tr')[0];
	                    var mData = vocherTable.fnGetData(data);

	                    if (null != mData) // null if we clicked on title row
	                    {
	                        var id = mData["id"];
	                        deleteClick(id);
	                    }
	                });

	                if (dataTableIntialization == 1) {
	                	disableRows();
	                }               
	            },
	            "sAjaxSource": "controllers/admin/payment_voucher.php",
	            "fnServerParams": function(aoData) {
	                aoData.push({
	                    "name": "operation",
	                    "value": "showInactive"
	                });
	            },
	            "aoColumns": [
	                {
	                    "mData": function (o) {  
		                    var voucherId = o["id"];
		                        
		                    var voucherIdLength = voucherId.length;
		                    for (var i=0;i<6-voucherIdLength;i++) {
		                        voucherId = "0"+voucherId;
		                    }
		                    voucherId = voucherPrefix+voucherId;
		                    return voucherId; 
		                }
	                }, {
	                    "mData": "payee_name"
	                }, {
	                    "mData": "ledger"
	                }, {
	                    "mData": "amount"
	                }, {
	                    "mData": "payment_mode"
	                }, {
	                    "mData": "cheque_no"
	                },  {
	                    "mData": function(o) {
	                        var data = o;
	                        dataTableIntialization = 1;
	                        return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
	                    }
	                },
	            ],
	            aoColumnDefs: [{
	                'bSortable': false,
	                'aTargets': [6]
	            }]
	        });
        } 
        else { // show active data on unchecked   
            vocherTable.fnClearTable();
            vocherTable.fnDestroy();
            vocherTable = "";
            dataTableIntialization = 0;
            vocherTable = $('#vocherTable').dataTable({
	            "bFilter": true,
	            "processing": true,
	            "sPaginationType": "full_numbers",
	            "bAutoWidth": false,
	            "order": [[ 2, 'asc' ]],
	            "aaSorting": [],
	            "fnDrawCallback": function(oSettings) {
	                // perform update event
	                $('.updatePayment').unbind();
	                $('.updatePayment').on('click', function() {
	                    var data = $(this).parents('tr')[0];
	                    var mData = vocherTable.fnGetData(data);
	                    if (null != mData) // null if we clicked on title row
	                    {
	                        var id = mData["id"];
	                        var ledgerId = mData["ledger_id"];   
	                        var paymentModeId = mData["payment_mode_id"];                   
	                        var registerPayeeId = mData["registered_payee_id"];
	                        var registerPayeeType = mData["registered_payee_type"];             
	                        var payeeName = mData["payee_name"];
	                        editPaymentClick(id,ledgerId,paymentModeId,registerPayeeId,registerPayeeType,
	                        	payeeName
	                        );

	                    }
	                });
	                //perform delete event
	                $('.delete').unbind();
	                $('.delete').on('click', function() {
	                    var data = $(this).parents('tr')[0];
	                    var mData = vocherTable.fnGetData(data);

	                    if (null != mData) // null if we clicked on title row
	                    {
	                        var id = mData["id"];
	                        deleteClick(id);
	                    }
	                });

	                if (dataTableIntialization == 1) {
	                	disableRows();
	                }               
	            },
	            "sAjaxSource": "controllers/admin/payment_voucher.php",
	            "fnServerParams": function(aoData) {
	                aoData.push({
	                    "name": "operation",
	                    "value": "show"
	                });
	            },
	            "aoColumns": [
	                {
	                    "mData": function (o) {  
		                    var voucherId = o["id"];
		                        
		                    var voucherIdLength = voucherId.length;
		                    for (var i=0;i<6-voucherIdLength;i++) {
		                        voucherId = "0"+voucherId;
		                    }
		                    voucherId = voucherPrefix+voucherId;
		                    return voucherId; 
		                }
	                }, {
	                    "mData": "payee_name"
	                }, {
	                    "mData": "ledger"
	                }, {
	                    "mData": "amount"
	                }, {
	                    "mData": "payment_mode"
	                }, {
	                    "mData": "cheque_no"
	                },  {
	                    "mData": function(o) {
	                        var data = o;
	                        dataTableIntialization = 1;
	                        return "<i class='ui-tooltip fa fa-pencil updatePayment' title='Edit'" +
	                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
	                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
	                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
	                            " data-original-title='Delete'></i>";
	                    }
	                },
	            ],
	            aoColumnDefs: [{
	                'bSortable': false,
	                'aTargets': [6]
	            }]
	        });
        }
    });


    $("#mainScreen, #registerPayee, #supplier").addClass('activeSelf');

    $('#customer').click(function(){
    	$(this).addClass('activeSelf');
    	$(this).parent().siblings().children().removeClass('activeSelf');
    	$('.supplier').addClass('hide');
    	$('.customer').removeClass('hide');
    	$("#txtSupplier,#txtFirmName,#txtSupplierEmail").val('');
    	$("#txtSupplier").removeAttr('supplier-id');
    })

    $('#supplier').click(function(){
    	$(this).addClass('activeSelf');
    	$(this).parent().siblings().children().removeClass('activeSelf');
    	$('.customer').addClass('hide');
    	$('.supplier').removeClass('hide');
    	$("#txtCustomer,#txtCustomerName,#txtCustomerEmail").val('');
    	$("#txtCustomer").removeAttr('customer-id');
    });

    $("#nonRegisterPayee").click(function(){
    	$(this).addClass('activeSelf');
    	$(this).parent().siblings().children().removeClass('activeSelf');
    	$('#customer,#supplier').removeClass('activeSelf');
    	$('.customer,.supplier,.registerPayee').addClass('hide');
    	$('.nonRegisterPayee').removeClass('hide');
    	$("#txtSupplier,#txtFirmName,#txtSupplierEmail").val('');
    	$("#txtSupplier").removeAttr('supplier-id');
    	$("#txtCustomer,#txtCustomerName,#txtCustomerEmail").val('');
    	$("#txtCustomer").removeAttr('customer-id');
    });

    $("#registerPayee").click(function(){
    	$(this).addClass('activeSelf');
    	$("#supplier").addClass('activeSelf');
    	$(this).parent().siblings().children().removeClass('activeSelf');
    	$('.nonRegisterPayee').addClass('hide');
    	$('.registerPayee ,.supplier').removeClass('hide');
    	$("#txtPayeeName").val('');
    });

    

    $('#mainScreen').click(function(){
    	$("#mainScreen, #registerPayee, #supplier").addClass('activeSelf');
    	$(this).parent().siblings().children().removeClass('activeSelf');
    	$("#nonRegisterPayee,#customer").removeClass('activeSelf');
    	$('.lineItem,.nonRegisterPayee,.customer').addClass('hide');
    	$('.main,.supplier,.registerPayee').removeClass('hide');
    	$('.saveUpdateButton').hide();
        $('.firstLegendcontainer').show();
        clearFormDetails('.firstLegendcontainer');
        $("#txtSupplier").removeAttr('supplier-id');
    });

    $("#searchSupplier").click(function(){
		sellerHiddenFieldValue = 2;
		$('#SellerModalBody').load('views/admin/view_seller.html', function(){});
		$('#mySellerModal').modal();
		$("#txtSupplierError").text("");// adding here if get an validation error on select of wrong input
		$("#txtSupplier").removeClass("errorStyle");
	});

	$("#searchCustomer").click(function(){
		clearFormDetails(".clearForm");		
        $('#searchModalBody').load('views/admin/customer.html',function(){

        });
		$('#mySearchModal').modal();
    });

    $("#btnAddPayee").on('click',function(){
    	$('#nonRegisteredPayeeModal').modal();
    	clearFormDetails("#nonRegisteredPayeeModalBody");
    	$('#nonRegisteredPayeeModalBody .chkTaxes').prop("checked",false);
    });       
    
	$("#lineItemScreen").click(function(){
        debugger;
        bindGLAccount('#selAccountPayable','accPayable');
        $('#hdnAccountPayable').hide();
       $('.firstLegendcontainer').hide();
        $('.lineItem').show();
       $('.main').removeClass('hide');

        clear();
		filldiscount = 0;
        sumGrossAmount = 0;
        sumTaxAmount = 0;
        sumNetAmount = 0;
        sumDiscount = 0;
		$('.saveUpdateButton').show();
		if (nonRegisteredPayeeTable !=undefined) {
			nonRegisteredPayeeTable.fnDestroy();
		}
		if (receiptNumberTable !=undefined) {
			receiptNumberTable.fnDestroy();
		}

		insertDataTables();
		$("#updateReceiptNumberTable_wrapper,#updateNonRegisteredPayeeTable,#updateReceiptNumberTable").hide();
		$("#receiptNumberTable,#nonRegisteredPayeeTable").show();
		/*if($("#selLedger").val() == ''){
			$("#selLedger").focus();
			$('#selLedger').addClass('errorStyle');
			$("#selLedgerError").text("Please select ledger.");
            $('.saveUpdateButton').hide();
			return false;
		}
		if($("#selPaymentMode").val() == ''){
			$("#selPaymentMode").focus();
			$('#selPaymentMode').addClass('errorStyle');
			$("#selPaymentModeError").text("Please select payment mode.");
            $('.saveUpdateButton').hide();
			return false;
		}*/
		if ($("#txtCustomer").val() == '' && $("#txtSupplier").attr('supplier-id') == undefined && $("#txtPayeeName").val() == '') {
            
            $('#messageMyModalLabel').text("Alert");
            $('#messageBody').text("Please choose any regsitered or non-registered person.");
            $('#messagemyModal').modal();
            $('.saveUpdateButton').hide();
            $('.firstLegendcontainer').removeAttr('style');
			return false;
		}
		$(this).addClass('activeSelf');
    	$(this).parent().siblings().children().removeClass('activeSelf');
    	$('.lineItem').removeClass('hide');
    	/*$('.main').addClass('hide');*/
		receiptNumberTable.fnClearTable();
		nonRegisteredPayeeTable.fnClearTable();

		
		var supplierId = '';
		$("#btnAddPayee").addClass('hide');	


		if ($("#txtCustomer").val() != '') {
            $('#hdnAccountPayable').removeAttr('style');
            
			customerId = $("#txtCustomer").val();
			if(customerId.length != 9){
				return false;
			}			
			var getCustomerPrefix = customerId.substring(0, 3);
			if (customerPrefix.toLocaleLowerCase() != getCustomerPrefix.toLocaleLowerCase()) {
				return false;
			}
			customerId = customerId.replace ( /[^\d.]/g, '' );
			customerId = parseInt(customerId);
			$("#nonRegisteredPayeeTable_wrapper,#btnAddPayee").removeClass('hide');
			$("#receiptNumberTable_wrapper").addClass('hide');
		}
		else if ($("#txtPayeeName").val() != '') {
            $('#hdnAccountPayable').removeAttr('style');
			$("#btnAddPayee ,#nonRegisteredPayeeTable_wrapper").removeClass('hide');
			$("#receiptNumberTable_wrapper").addClass('hide');
		}
		else if ($("#txtSupplier").attr('supplier-id') != '' && $("#txtSupplier").attr('supplier-id') != undefined) {
			supplierId = $("#txtSupplier").attr('supplier-id');
			$("#nonRegisteredPayeeTable_wrapper").addClass('hide');
			$("#receiptNumberTable_wrapper").removeClass('hide');
			var postData = {
				operation : "showDataTableDetails",
				supplierId : supplierId,
				"saveSupplierInv" : "saveSupplierInv"
			}
			dataCall("controllers/admin/payment_voucher.php", postData, function (result){
				var id = '';
				var grandTotal = '';
				if (result == "") {
					return  false;
				}
                var invoiceTotal = 0;
                var invoiceRow = '';
                var getInvoiceRow = '';
				var parseData = JSON.parse(result);
				if (parseData !='') {
					glAccountId = parseData[0].gl_account;
					$.each(parseData,function(i,v){
						
						receiptNumber = v.id;
						
						var totalAmount = v.item_unitprice * v.item_quantity;

			            if (v.id != id) {
			            	invoiceTotal = 0;
			                invoiceRow = receiptNumberTable.fnAddData(["<input type='checkbox'  class='chkInvoice "+v.id+"' data-id="+v.id+" style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;'"+					 
								" style='font-size: 22px; cursor:pointer;'>"+"<span class='details-control' onclick='showRows($(this));'></span>"+"<span class='hide-details-control hide' onclick='hideRows($(this));'></span>",receiptNumber,'','','','','0.00',v.invoice_total,'','',''
							]);
                            getInvoiceRow = receiptNumberTable.fnSettings().aoData[ invoiceRow ].nTr;

			                var currData = receiptNumberTable.fnAddData(['',"<input type='checkbox' discount="+v.discount+" class='chkProduct' data-id="+v.id+" main-id="+v.pk_table+" productId="+v.customer_product_id+"  style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative; margin-left: 42%;'"+					 
							   " style='font-size: 22px; cursor:pointer;'>",v.item_name,v.item_unitprice,v.item_quantity,v.item_discount,v.item_tax,totalAmount,v.item_total,v.gl_account,v.account_payable
							]);

			                var getCurrDataRow = receiptNumberTable.fnSettings().aoData[ currData ].nTr;
                            $(getCurrDataRow).find('td').last().hide();
                            $(getCurrDataRow).find('td').last().prev().hide();
			                $(getCurrDataRow).addClass('hide '+(receiptNumber).replace(/ /g,''));
			                id = v.id;
			                grandTotal += parseFloat(v.total);
                            invoiceTotal=parseFloat(v.item_total);
                            $(getInvoiceRow).find('td:last-child').prev().prev().html(invoiceTotal);
                            $(getInvoiceRow).find('td:last-child').hide();
                            $(getInvoiceRow).find('td:last-child').prev().hide();
			            }

			            else {		            	

			                var currData = receiptNumberTable.fnAddData(['',"<input type='checkbox' discount="+v.discount+"  class='chkProduct' main-id="+v.pk_table+" data-id="+v.id+"  productId="+v.customer_product_id+"  style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative; margin-left: 42%;'"+					 
							   " style='font-size: 22px; cursor:pointer;'>",v.item_name,v.item_unitprice,v.item_quantity,v.item_discount,v.item_tax,totalAmount,v.item_total,v.gl_account,v.account_payable
							]);

			                var getCurrDataRow = receiptNumberTable.fnSettings().aoData[ currData ].nTr;
                            $(getCurrDataRow).find('td').last().hide();
                            $(getCurrDataRow).find('td').last().prev().hide();
			                $(getCurrDataRow).addClass('hide '+(receiptNumber).replace(/ /g,''));
			                grandTotal += parseFloat(v.total);
			                id = v.id;
                            invoiceTotal+=parseFloat(v.item_total);
                            $(getInvoiceRow).find('td:last-child').prev().prev().html(invoiceTotal);
                            $(getInvoiceRow).find('td:last-child').hide();
                            $(getInvoiceRow).find('td:last-child').prev().hide();
			            }
			        });
			        $("#txtBalance").val(grandTotal);
				}
						
			});
		}
		else {
			return false;
		}			
	});

	sumGrossAmount = 0;
	sumTaxAmount = 0;
	sumNetAmount = 0;
    sumDiscount = 0;

	$("#addItems").on('click',function(){
		removeErrorMessage();
		if ($("#updateScreen").attr('style') == "display: block;") {
			AddItems(updateNonRegisteredPayeeTable,"#updateScreen #txtGrossAmount","#updateScreen #txtTaxAmount","#updateScreen #txtNetAmount","#updateScreen #txtDiscount");
		}
		else{
			AddItems(nonRegisteredPayeeTable,"#txtGrossAmount","#txtTaxAmount","#txtNetAmount","#txtDiscount");
		}						
    }); 


	$("#btnSubmit").on('click',function(){
		var flag = false;
        
		if($('#txtDescription').val() == '') {
            $('#txtDescriptionError').text('Please enter description.');
            $('#txtDescription').focus();
            $('#txtDescription').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtCheque').val() == '') {
            $('#txtChequeError').text('Please enter cheque number.');
            $('#txtCheque').focus();
            $('#txtCheque').addClass("errorStyle"); 
            flag = true;
        }
        
        if($("#selPaymentMode").val() =="-1"){
            $("#selPaymentMode").focus();
            $("#selPaymentModeError").text("Please select payment mode");
            $("#selPaymentMode").addClass("errorStyle");
            flag = true;
        }
        if($("#selLedger").val() ==""){
            $("#selLedger").focus();
            $("#selLedgerError").text("Please select ledger");
            $("#selLedger").addClass("errorStyle");
            flag = true;
        }

        if($("#selAccountPayable").val() =="" && $("#txtCustomer").val() !=""){
            $("#selAccountPayable").focus();
            $("#selAccountPayableError").text("Please select account payable");
            $("#selAccountPayable").addClass("errorStyle");
            return false;
        }
        if($("#selAccountPayable").val() =="-1" && $("#txtPayeeName").val() !=""){
            $("#selAccountPayable").focus();
            $("#selAccountPayableError").text("Please select account payable");
            $("#selAccountPayable").addClass("errorStyle");
            return false;
        }
        if($("#selAccountPayable").val() =="" && $("#txtPayeeName").val() !=""){
            $("#selAccountPayable").focus();
            $("#selAccountPayableError").text("Please select account payable");
            $("#selAccountPayable").addClass("errorStyle");
            return false;
        }
		if($("#txtExpected").val() == "" || $("#txtExpected").val() == "0"){
			callSuccessPopUp("Alert","Your total bill is zero");
			flag = true;
		}

		if(flag == true){
			return false;
		}
		var ledgerId = $("#selLedger").val();
		var ledgerName = $("#selLedger :selected").text();
		var chequeNo = $("#txtCheque").val();
		var registerdPayeeId = '';
		var registeredPayeeType = "";
		var description = $("#txtDescription").val().trim();
		var paymentMode = $("#selPaymentMode").val();
		var saveNetAmount = $("#txtNetAmount").val().trim();
        var account_payable_id;
		if ($("#txtSupplier").val() !='') {
			registerdPayeeId = $("#txtSupplier").attr("supplier-id");
			registeredPayeeType = "seller";
		}
		else if($("#txtCustomer").val() != '') {
            accountPayableId = $("#selAccountPayable").val();
			registerdPayeeId = customerId;
			registeredPayeeType = "customer";
		}
		else {
			registerdPayeeId = "";
			registeredPayeeType = "";
		}


		if (($("#txtPayeeName").val() != '') || ($("#txtCustomer").val() != '')) {
            $('#hdnAccountPayable').removeAttr('style');
            if (nonRegisteredPayeeTable.fnGetData() == '') {
                $('#messagemyModal').modal();
                $("#messageMyModalLabel").text('Alert');
                $("#messageBody").text("Can't save null data.");
                return false;
            }
            var accountPayableId = $("#selAccountPayable").val();
			var itemDetails = [];
			
			$.each($("#nonRegisteredPayeeTable tbody tr"),function(i,v){
				var obj = {};
				obj.GLAccountId = $(v).find('td:eq(0) span').attr('value');
				obj.GLAccountName =   $(v).find('td:eq(0) span').text();
                var netAmount = $(v).find('td:eq(5)').text();
				obj.netAmount = netAmount;
				obj.remarks = $(v).find('td:eq(6)').text();
                var discount = $(v).find('td:eq(3)').text();
                var netDis = (parseFloat(netAmount)+parseFloat(discount))/parseFloat(discount);
				obj.discount = netDis;
				var taxIDArray = [];
				$.each($(v).find('td:eq(1)').find('span'),function(ind,val){
					taxIDArray.push($(val).attr('value'));
				})
				obj.taxArray = taxIDArray;
				itemDetails.push(obj);
			});
			printItemDetails = itemDetails;
			
			var postData = {
				"operation" : "saveCustomer_NonRegisterData",
				registerdPayeeId : registerdPayeeId,
				registeredPayeeType : registeredPayeeType,
				ledgerId : ledgerId,
				paymentMode : paymentMode,
				cheque_no : chequeNo,
				invoiceNo : '',
				saveNetAmount : saveNetAmount,
				payeeName : payeeName,
				description : description,
                accountPayableId :accountPayableId,
				itemDetails : JSON.stringify(itemDetails)
			}
			dataCall("controllers/admin/payment_voucher.php", postData, function (result){
				if (result != '') {
                    $('#printMyModal').modal();
                    $('#printMyModalLabel').text("Success");
                    $('#printMessageBody').text("Details saved successfully");
                        
					clearFormDetails('#advanced-wizard');
					clearFormDetails('#aupdateScreen');
					receiptNumberTable.fnClearTable();
					nonRegisteredPayeeTable.fnClearTable();
					$("#tabChartOfAccountList").click();						
					vocherTable.fnReloadAjax();
					dataTableIntialization = 0;
					printSellerInvoice = ''		;
                    $("#txtSupplier").removeAttr('supplier-id');
					$("#printModalFooter").html('<input type="button" class="btn_save" value="Print" onclick= "printDetails(\''+result+'\',\''+ledgerName+'\',\''+description+'\',\''+saveNetAmount+'\',\''+payeeName+'\');" id="print">');
				}
			});
		}

		else {

            if (receiptNumberTable.fnGetData() == '') {
                $('#messagemyModal').modal();
                $("#messageMyModalLabel").text('Alert');
                $("#messageBody").text("Can't save null data.");
                return false;
            }
            if($("#selAccountPayable").val() =="-1"){
                $("#selAccountPayable").focus();
                $("#selAccountPayableError").text("Please select account payable");
                $("#selAccountPayable").addClass("errorStyle");
                return false;
            }

			var productDetail = [];
			var receiptDetail = [];
			
			var aiRows = receiptNumberTable.fnGetNodes(); //Get all rows of data table 		
			var rowcollection =  receiptNumberTable.$(".chkProduct:checked", {"page": "all"});	//Select all unchecked row only
			var lastId ='';
			var totalAmount =0;
			var discountItemAmt = 0;
			//Getting data from all unchecked rows 
			for (i=0,c=rowcollection.length; i<c; i++) {	
				var objProduct ={};
				var objReceipt ={};
				var invoiceId = $($(rowcollection)[i]).attr('data-id');
				if(i== 0){
					
                    objProduct.mainId = $($(rowcollection)[i]).attr('main-id');
                    objProduct.invoiceId = $($(rowcollection)[i]).attr('data-id');
                    objProduct.glAccountId = parseFloat($($($(rowcollection)[i]).parent().siblings()[8]).text());
                    objProduct.aPId = parseFloat($($($(rowcollection)[i]).parent().siblings()[9]).text());
                    objProduct.netAmount = parseFloat($($($(rowcollection)[i]).parent().siblings()[7]).text());
                    objProduct.discount = parseFloat($($($(rowcollection)[i]).parent().siblings()[4]).text());
					totalAmount += parseFloat($($($(rowcollection)[i]).parent().siblings()[7]).text());
					discountItemAmt+= parseFloat($($($(rowcollection)[i]).parent().siblings()[4]).text());
					productDetail.push(objProduct);
					lastId = invoiceId;
				}
				else if(invoiceId == lastId && i != 0){				
					objProduct.invoiceId = $($(rowcollection)[i]).attr('data-id');
                    objProduct.glAccountId = parseFloat($($($(rowcollection)[i]).parent().siblings()[8]).text());
                    objProduct.aPId = parseFloat($($($(rowcollection)[i]).parent().siblings()[9]).text());
                    objProduct.netAmount = parseFloat($($($(rowcollection)[i]).parent().siblings()[7]).text());
                    objProduct.discount = parseFloat($($($(rowcollection)[i]).parent().siblings()[4]).text());
					totalAmount += parseFloat($($($(rowcollection)[i]).parent().siblings()[7]).text());	
					discountItemAmt+= parseFloat($($($(rowcollection)[i]).parent().siblings()[4]).text());			
					productDetail.push(objProduct);
					lastId = invoiceId;
				}
				else{
					objReceipt.invoiceId = $($(rowcollection)[i-1]).attr('data-id');
					objReceipt.total = totalAmount;
					objReceipt.discount = discountItemAmt;
					receiptDetail.push(objReceipt);
					totalAmount = parseFloat($($($(rowcollection)[i]).parent().siblings()[7]).text());
					discountItemAmt = parseFloat($($($(rowcollection)[i]).parent().siblings()[4]).text());
					objProduct.invoiceId = $($(rowcollection)[i]).attr('data-id');
                    objProduct.glAccountId = parseFloat($($($(rowcollection)[i]).parent().siblings()[8]).text());
                    objProduct.aPId = parseFloat($($($(rowcollection)[i]).parent().siblings()[9]).text());
                    objProduct.netAmount = parseFloat($($($(rowcollection)[i]).parent().siblings()[7]).text());
                    objProduct.discount = parseFloat($($($(rowcollection)[i]).parent().siblings()[4]).text());		
					productDetail.push(objProduct);
					lastId = invoiceId;
				}	
				if(i == c-1){
					objReceipt ={};
					objReceipt.invoiceId = $($(rowcollection)[i]).attr('data-id');
					objReceipt.total = totalAmount;
					objReceipt.discount = discountItemAmt;
					receiptDetail.push(objReceipt);
				}
			}
			var saveNetAmount = $("#txtNetAmount").val().trim();
			printSellerInvoice = receiptDetail;
			var postData = {
				operation : "saveRegisteredData",
				ledgerId : ledgerId,
				chequeNo : chequeNo,
				productDetail : JSON.stringify(productDetail),
				receiptDetail : JSON.stringify(receiptDetail),
				payeeName : payeeName,
				registerdPayeeId : registerdPayeeId,
				registeredPayeeType : registeredPayeeType,
				description : description,
				paymentMode : paymentMode,
				glAccountId : glAccountId,
                account_payable_id : account_payable_id,
				saveNetAmount :saveNetAmount
			}
			dataCall("controllers/admin/payment_voucher.php", postData, function (result){
				if (result !='') {
					$('#printMyModalLabel').text("Success");
                    $('#printMessageBody').text("Details saved successfully");
                    $('#printMyModal').modal();
					clearFormDetails('#advanced-wizard');
					clearFormDetails('#aupdateScreen');
					receiptNumberTable.fnClearTable();
					nonRegisteredPayeeTable.fnClearTable();
					$("#tabChartOfAccountList").click();
                    $("#txtSupplier").removeAttr('supplier-id');
					vocherTable.fnReloadAjax();
					dataTableIntialization = 0;
					printItemDetails = '';
					filldiscount = 0;
					$("#printModalFooter").html('<input type="button" class="btn_save" value="Print" onclick= "printDetails(\''+result+'\',\''+ledgerName+'\',\''+description+'\',\''+saveNetAmount+'\',\''+payeeName+'\');" id="print">');
				}
			});
		}
	});

    $('#btnReset').click(function() {
        clear();
        $('#mainScreen').click();
    });
	
    $("#txtSupplier").on('keyup', function(e){	//Autocomplete functionality  for seller
		if(e.keyCode !=13){
			$("#txtSupplier").removeAttr('supplier-id');
		}
		var currentObj = $(this);
		autoComplete(currentObj,"#txtSupplier",'#txtSupplierEmail',"#txtFirmName");				
	});

	$('#nonRegisteredPayeeModal #selLedger').on('change', function() {
	    if (this.value == '-1') {
	        $('#nonRegisteredPayeeModal #selLedger option').prop('disabled', true);
	    } else {
	        $('#nonRegisteredPayeeModal #selLedger option').prop('disabled', false);
	    }
	});

	$("#selLedger").change(function(){
		if ($(this).val() != '') {
			paymentMode($(this).val(),'',"#selPaymentMode");
			$("#selLedger").removeClass('errorStyle');
			$("#selLedgerError").text('');			
		}
	});

	$("#selPaymentMode").change(function(){
		if ($(this).val() != '') {
			getPayeeName($("#selLedger").val(),$(this).val());
			$("#selPaymentMode").removeClass('errorStyle');
			$("#selPaymentModeError").text('');
		}
	});
});


function editPaymentClick(id,ledgerId,paymentModeId,registerPayeeId,registerPayeeType,payeeName) {
	var netdiscount =0;
	var totalamount =0;
	var netTotalAmount =0;

	
	$('.saveUpdateButton').hide();
	debugger;
	$("#updateReceiptNumberTable,#updateNonRegisteredPayeeTable").removeClass('hide');
	$("#updateReceiptNumberTable,#updateNonRegisteredPayeeTable").show();
	$("#receiptNumberTable_wrapper,#nonRegisteredPayeeTable_wrapper,#receiptNumberTable,#nonRegisteredPayeeTable").hide();

	$("#updateScreen").show();
	$('#updateScreenBody').html($("#advanced-wizard").html()).show();
    $(".blackborder").hide();
    $("#chartOfAccountList").hide();
    $('#updateScreen .backtext').click(function(){
		$(".blackborder").show();
    	$("#chartOfAccountList").show();
    	$("#updateScreen").hide();
	});
    $("#updateScreen #txtGrossAmount").val(0);
    $("#updateScreen #txtDiscount").val(0);
    $("#updateScreen #txtTaxAmount").val(0);
    $("#updateScreen #txtNetAmount").val(0);
	updateNonRegisteredPayeeTable = $("#updateScreen #updateNonRegisteredPayeeTable").dataTable({
		"bFilter": true,
        "processing": true,
        "bPaginate": false,
        "bAutoWidth": false,
        "aaSorting": [],
        aoColumnDefs: [{'bSortable': false,'aTargets': [7]}],
        aLengthMenu: [
            [25, 50, 100, 200, -1],
            [25, 50, 100, 200, "All"]
        ],
        iDisplayLength: -1
	});


    updateReceiptNumberTable = $("#updateScreen #updateReceiptNumberTable").dataTable({
    	"bFilter": true,
        "processing": true,
        "deferLoading": 57,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "aaSorting": [],
        aoColumnDefs: [{'bSortable': false,'aTargets': [6,0]}],
        aLengthMenu: [
            [25, 50, 100, 200, -1],
            [25, 50, 100, 200, "All"]
        ],
        iDisplayLength: -1,
        "fnDrawCallback": function(oSettings) {        	
            $("#updateScreen .chkProduct").unbind();
            $("#updateScreen .chkProduct").on('change', function(){
                var id = $(this).attr('data-id');
                var amount = $($(this).parent().siblings()[6]).text();
                var fillNetAmount = $($(this).parent().siblings()[7]).text();
                var filldiscount = $($(this).parent().siblings()[4]).text();
                var parentClass = $(this).attr('data-id');
                if($(this).is(":checked")){
                    var m = false;
                    
                    var chkBox = $(this).parent().parent().parent().find("."+id+':not(:first)');
                    for(n=0; n<chkBox.length; n++){
                        if(!$($(this).parent().parent().parent().find('.'+id).find('.chkProduct')[n]).prop('checked')){
                            m = true;
                        }
                    }
                    if(m == false){
                        $(this).parent().parent().parent().find("."+parentClass+".chkInvoice").prop('checked',true);
                    }    
                    totalamount = totalamount + parseFloat(amount);
                    netTotalAmount = netTotalAmount + parseFloat(fillNetAmount);
                    netdiscount = netdiscount + parseFloat(filldiscount); 
                    $("#updateScreen #txtGrossAmount").val(totalamount);
                    $("#updateScreen #txtNetAmount").val(netTotalAmount);
                    $("#updateScreen #txtDiscount").val(netdiscount);
                    $("#updateScreen #txtTaxAmount").val(0.00);
                }                           
                else{    
                    var m = false;
                    
                    var chkBox = $(this).parent().parent().parent().find("."+id+':not(:first)');                  
                    $(this).parent().parent().parent().find("."+parentClass+".chkInvoice").prop('checked',false);

                    totalamount = totalamount - parseFloat(amount);
                    netTotalAmount = netTotalAmount - parseFloat(fillNetAmount);
                    netdiscount = netdiscount - parseFloat(filldiscount);

                    $("#updateScreen #txtGrossAmount").val(totalamount);
                    $("#updateScreen #txtNetAmount").val(netTotalAmount);
                    $("#updateScreen #txtDiscount").val(netdiscount);
                    $("#updateScreen #txtTaxAmount").val(0.00);
                    if (totalamount < 0) {
                        $("#updateScreen #txtGrossAmount").val(0);
                    }
                    if (netTotalAmount < 0) {
                        $("#updateScreen #txtNetAmount").val(0);
                    }
                    if (filldiscount < 0) {
                        $("#updateScreen #txtDiscount").val(0);
                    }
                }
            });
            $("#updateScreen .chkInvoice").unbind();          
            $("#updateScreen .chkInvoice").on('change', function(){
                var id = $(this).attr('data-id');
                var subClass = $($(this).parent().siblings()[0]).text();
                var amount = $($(this).parent().siblings()[6]).text();
                var fillNetAmount = $($(this).parent().siblings()[7]).text();               
                if($(this).is(":checked")){
                    var chkBox = $(this).parent().parent().parent().find('.'+subClass).find('input[type="checkbox"]');
                    for(n=0; n<chkBox.length; n++){
                        if(!$($(this).parent().parent().parent().find('.'+subClass).find('input[type="checkbox"]')[n]).prop('checked')){
                            totalamount = totalamount + parseFloat($($(chkBox[n]).parent().siblings()[6]).text());
                            netTotalAmount = netTotalAmount + parseFloat($($(chkBox[n]).parent().siblings()[7]).text());
                            netdiscount = netdiscount + parseFloat($($(chkBox[n]).parent().siblings()[4]).text());
                        }
                    }   
                    $(this).parent().parent().parent().find('.'+subClass).find('input[type="checkbox"]').prop('checked',true);              
                    
                    $.grep($(this).parent().parent().parent().find('.'+subClass+':not(:first)'),function(val,ind){
                        filldiscount += parseFloat($(val).find('td:eq(5)').text());
                    });
                    $("#updateScreen #txtGrossAmount").val(totalamount);
                    $("#updateScreen #txtNetAmount").val(netTotalAmount);
                    $("#updateScreen #txtDiscount").val(netdiscount);
                    $("#updateScreen #txtTaxAmount").val(0.00);
                }
                else{

                    var chkBox = $(this).parent().parent().parent().find('.'+subClass).find('input[type="checkbox"]');
                    for(n=0; n<chkBox.length; n++){
                        if($($(this).parent().parent().parent().find('.'+subClass).find('input[type="checkbox"]')[n]).prop('checked')){
                            netdiscount = netdiscount - parseFloat($($(chkBox[n]).parent().siblings()[4]).text());
                        }
                    }
                    $(this).parent().parent().parent().find('.'+subClass).find('input[type="checkbox"]').prop('checked',false);                 
                    totalamount = totalamount - parseFloat(amount);
                    netTotalAmount = netTotalAmount - parseFloat(fillNetAmount);
                    $.grep($(this).parent().parent().parent().find('.'+subClass+':not(:first)'),function(val,ind){
                        filldiscount -= parseFloat($(val).find('td:eq(5)').text());
                    });

                    $("#updateScreen #txtGrossAmount").val(totalamount);
                    $("#updateScreen #txtNetAmount").val(netTotalAmount);
                    $("#updateScreen #txtDiscount").val(netdiscount);
                    $("#updateScreen #txtTaxAmount").val(0.00);
                    if (totalamount < 0) {
                        $("#updateScreen #txtGrossAmount").val(0);
                    }
                    if (netTotalAmount < 0) {
                        $("#updateScreen #txtNetAmount").val(0);
                    }
                    if (filldiscount < 0) {
                        $("#updateScreen #txtDiscount").val(0);
                    }
                }
            });
        }
    });
	paymentMode(ledgerId,paymentModeId,"#updateScreen #selPaymentMode");

    $("#updateScreen #btnSubmit").val('Update');
	$("#updateScreen #txtSupplier").on('keyup', function(e){	//Autocomplete functionality  for seller
		if(e.keyCode !=13){
			$("#updateScreen #txtSupplier").removeAttr('supplier-id');
		}
		var currentObj = $(this);
		autoComplete(currentObj,"#updateScreen #txtSupplier",'#updateScreen #txtSupplierEmail',"#updateScreen #txtFirmName");
	});
    
    $("#updateScreen #selLedger").removeClass("errorStyle");
    $("#updateScreen #selLedgerError").text("");
    $("#updateScreen #selPaymentMode").removeClass("errorStyle");
    $("#updateScreen #selPaymentModeError").text("");
    $("#updateScreen #selAccountType").removeClass("errorStyle");
    $("#updateScreen #selAccountTypeError").text("");
    $("#updateScreen #selSubType").removeClass("errorStyle");
    $("#updateScreen #selSubTypeError").text("");
    $("#updateScreen #selClass").removeClass("errorStyle");
    $("#updateScreen #selClassError").text("");


    $('#updateScreen #selLedger').val(ledgerId);
    $('#updateScreen #selPaymentMode').val(paymentModeId);         
      
    $('#selectedRow').val(id);

    removeErrorMessage(); //remove error messages

    if (registerPayeeType == "seller") {
		getSupplierDetails(registerPayeeId,"#updateScreen #txtSupplier","#updateScreen #txtFirmName","#updateScreen #txtSupplierEmail");
		$("#updateScreen #customer,#updateScreen #nonRegisterPayee,#updateScreen #txtSupplier").attr('disabled',"disabled");
		$("#updateScreen #searchSupplier").hide();
	}
	else if(registerPayeeType == "customer"){
		$("#updateScreen .customer").removeClass('hide');
		$("#updateScreen .supplier,#updateScreen #searchCustomer").addClass('hide');
		$("#updateScreen #supplier,#updateScreen #nonRegisterPayee,#updateScreen #txtCustomer").attr('disabled',"disabled");
		$("#updateScreen #customer").addClass('activeSelf');
		getCustomerDetails(registerPayeeId,"#updateScreen #txtCustomer","#updateScreen #txtCustomerName","#updateScreen #txtCustomerEmail");
	}
	else{
		$("#updateScreen .nonRegisterPayee").removeClass('hide');
		$("#updateScreen .registerPayee").addClass('hide');
		$("#updateScreen #registerPayee,#updateScreen #txtPayeeName").attr('disabled',"disabled");
		$("#updateScreen #txtPayeeName").val(payeeName);
		$("#updateScreen #nonRegisterPayee").addClass('activeSelf');
	}

	$('#updateScreen #mainScreen').click(function(){
        $('.firstLegendcontainer').show();
		if (registerPayeeType == "seller") {
			$("#updateScreen #mainScreen,#updateScreen #registerPayee,#updateScreen #supplier").addClass('activeSelf');
			$(this).parent().siblings().children().removeClass('activeSelf');
			$("#updateScreen #nonRegisterPayee,#updateScreen #customer").removeClass('activeSelf');
	    	$('#updateScreen .lineItem,#updateScreen .nonRegisterPayee,#updateScreen .customer').addClass('hide');
	    	$('#updateScreen .main,#updateScreen .supplier,#updateScreen .registerPayee').removeClass('hide');
		}
		else if (registerPayeeType == "customer") {
			$(this).parent().siblings().children().removeClass('activeSelf');
			$("#updateScreen #nonRegisterPayee,#updateScreen #supplier").removeClass('activeSelf');
			$('#updateScreen .lineItem,#updateScreen .nonRegisterPayee,#updateScreen .supplier').addClass('hide');
			$('#updateScreen .main,#updateScreen .customer,#updateScreen .registerPayee').removeClass('hide');
			$("#updateScreen #mainScreen,#updateScreen #registerPayee,#updateScreen #customer").addClass('activeSelf');

		}
		else{
			$("#updateScreen #mainScreen, #nonRegisterPayee").addClass('activeSelf');
			$(this).parent().siblings().children().removeClass('activeSelf');
			$("#updateScreen #registerPayee").removeClass('activeSelf');
			$('#updateScreen .lineItem,#updateScreen .registerPayee').addClass('hide');
			$('#updateScreen .main,#updateScreen .customer,#updateScreen .nonRegisterPayee').removeClass('hide');
		}
		$('.saveUpdateButton').hide();
    });

	$("#updateScreen #btnAddPayee").on('click',function(){
    	$('#nonRegisteredPayeeModal').modal();
    	clearFormDetails("#nonRegisteredPayeeModalBody");
    	$('#nonRegisteredPayeeModalBody .chkTaxes').prop("checked",false);
    }); 


	$("#updateScreen #lineItemScreen").click(function(){
        debugger;
        $('#updateScreen .main').removeClass('hide');
        $('#updateScreen .firstLegendcontainer').hide();
        $('#updateScreen .lineItem').show();

		filldiscount = 0;
		$('.saveUpdateButton').show();

		if($("#updateScreen #selLedger").val() == ''){
			$("#updateScreen #selLedger").focus();
			$('#updateScreen #selLedger').addClass('errorStyle');
			$("#updateScreen #selLedgerError").text("Please select ledger.");
			return false;
		}
		if($("#updateScreen #selPaymentMode").val() == ''){
			$("#updateScreen #selPaymentMode").focus();
			$('#updateScreen #selPaymentMode').addClass('errorStyle');
			$("#updateScreen #selPaymentModeError").text("Please select payment mode.");
			return false;
		}
		
		$(this).addClass('activeSelf');
    	$(this).parent().siblings().children().removeClass('activeSelf');
    	$('#updateScreen .lineItem').removeClass('hide');
    	/*$('#updateScreen .main').addClass('hide');*/
		updateReceiptNumberTable.fnClearTable();
		updateNonRegisteredPayeeTable.fnClearTable();

		
		var supplierId = '';
		$("#updateScreen #btnAddPayee").addClass('hide');	


		if ($("#updateScreen #txtCustomer").val() != '') {
            
			customerId = $("#updateScreen #txtCustomer").val();
			if(customerId.length != 9){
				return false;
			}			
			var getCustomerPrefix = customerId.substring(0, 3);
			if (customerPrefix.toLocaleLowerCase() != getCustomerPrefix.toLocaleLowerCase()) {
				return false;
			}
			customerId = customerId.replace ( /[^\d.]/g, '' );
			customerId = parseInt(customerId);
			$("#updateScreen #updateNonRegisteredPayeeTable_wrapper,#updateScreen #btnAddPayee").removeClass('hide');
			$("#updateScreen #updateReceiptNumberTable_wrapper").addClass('hide');
			addPaymentVoucherItem(id);
		}
		else if ($("#updateScreen #txtPayeeName").val() != '') {
			$("#updateScreen #btnAddPayee ,#updateScreen #updateNonRegisteredPayeeTable_wrapper").removeClass('hide');
			$("#updateScreen #updateReceiptNumberTable_wrapper").addClass('hide');
			addPaymentVoucherItem(id)
		}
		else if ($("#updateScreen #txtSupplier").attr('supplier-id') != '' && $("#updateScreen #txtSupplier").attr('supplier-id') != undefined) {
			supplierId = $("#updateScreen #txtSupplier").attr('supplier-id');
			$("#updateScreen #updateNonRegisteredPayeeTable_wrapper").addClass('hide');
			$("#updateScreen #updateReceiptNumberTable_wrapper").removeClass('hide');
			var postData = {
				operation : "showDataTableDetails",
				supplierId : supplierId
			}
			dataCall("controllers/admin/payment_voucher.php", postData, function (result){
				var id = '';
				var grandTotal = 0;
				if (result == "") {
					return  false;
				}
                var invoiceRow = '';
                var getInvoiceRow = '';
				var parseData = JSON.parse(result);
				glAccountId = parseData[0].gl_account;
				$.each(parseData,function(i,v){
					
					receiptNumber = v.id;
					
					var totalAmount = v.item_unitprice * v.item_quantity;

		            if (v.id != id) {
		            	invoiceTotal = 0;
		                invoiceRow = updateReceiptNumberTable.fnAddData(["<input type='checkbox'  class='chkInvoice "+v.id+"' data-id="+v.id+" style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;'"+					 
							" style='font-size: 22px; cursor:pointer;'>"+"<span class='details-control' onclick='showRows($(this));'></span>"+"<span class='hide-details-control hide' onclick='hideRows($(this));'></span>",receiptNumber,'','','','','0.00',v.invoice_total,'','',''
						]);
                        getInvoiceRow = updateReceiptNumberTable.fnSettings().aoData[ invoiceRow ].nTr;

		                var currData = updateReceiptNumberTable.fnAddData(['',"<input type='checkbox'  class='chkProduct' data-id="+v.id+" main-id="+v.pk_table+" productId="+v.customer_product_id+"  style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative; margin-left: 42%;'"+					 
						   " style='font-size: 22px; cursor:pointer;'>",v.item_name,v.item_unitprice,v.item_quantity,v.item_discount,v.item_tax,totalAmount,v.item_total,v.gl_account,v.account_payable
						]);

		                var getCurrDataRow = updateReceiptNumberTable.fnSettings().aoData[ currData ].nTr;
                        $(getCurrDataRow).find('td').last().hide();
                        $(getCurrDataRow).find('td').last().prev().hide();
		                $(getCurrDataRow).addClass('hide '+(receiptNumber).replace(/ /g,''));
		                id = v.id;
		                grandTotal += parseFloat(v.total);
                        invoiceTotal=parseFloat(v.item_total);
                        $(getInvoiceRow).find('td:last-child').prev().prev().html(invoiceTotal);
                        $(getInvoiceRow).find('td:last-child').hide();
                        $(getInvoiceRow).find('td:last-child').prev().hide();
		            }

		            else {		            	

		                var currData = updateReceiptNumberTable.fnAddData(['',"<input type='checkbox'  class='chkProduct' main-id="+v.pk_table+" data-id="+v.id+"  productId="+v.customer_product_id+"  style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative; margin-left: 42%;'"+					 
						   " style='font-size: 22px; cursor:pointer;'>",v.item_name,v.item_unitprice,v.item_quantity,v.item_discount,v.item_tax,totalAmount,v.item_total,v.gl_account,v.account_payable
						]);

		                var getCurrDataRow = updateReceiptNumberTable.fnSettings().aoData[ currData ].nTr;
                        $(getCurrDataRow).find('td').last().hide();
                        $(getCurrDataRow).find('td').last().prev().hide();
		                $(getCurrDataRow).addClass('hide '+(receiptNumber).replace(/ /g,''));
		                grandTotal += parseFloat(v.total);
		                id = v.id;
                        invoiceTotal+=parseFloat(v.item_total);
                        $(getInvoiceRow).find('td:last-child').prev().prev().html(invoiceTotal);
                        $(getInvoiceRow).find('td:last-child').hide();
                        $(getInvoiceRow).find('td:last-child').prev().hide();
		            }
		        });
		        $("#updateScreen #txtBalance").val(grandTotal);		        
			});
			getChequeDeatils(id,"#updateScreen #txtCheque","#updateScreen #txtDescription");
		}
		else {
			return false;
		}	
	}); 

	$("#updateScreen #btnSubmit").on('click',function(){
		var flag = false;

		if($('#updateScreen #txtDescription').val() == '') {
            $('#updateScreen #txtDescriptionError').text('Please enter description.');
            $('#updateScreen #txtDescription').focus();
            $('#updateScreen #txtDescription').addClass("errorStyle"); 
            flag = true;
        }
        if($('#updateScreen #txtCheque').val() == '') {
            $('#updateScreen #txtChequeError').text('Please enter cheque number.');
            $('#updateScreen #txtCheque').focus();
            $('#updateScreen #txtCheque').addClass("errorStyle"); 
            flag = true;
        }
        if($('#updateScreen #selPaymentMode').val() == '') {
            $('#updateScreen #selPaymentModeError').text('Please enter payment mode.');
            $('#updateScreen #selPaymentMode').focus();
            $('#updateScreen #selPaymentMode').addClass("errorStyle"); 
            flag = true;
        }
        if($('#updateScreen #selLedger').val() == '') {
            $('#updateScreen #selLedgerError').text('Please select ledger.');
            $('#updateScreen #selLedger').focus();
            $('#updateScreen #selLedger').addClass("errorStyle"); 
            flag = true;
        }
        if (flag == true) {
            return false;
        }	

		var ledgerId = $("#updateScreen #selLedger").val();
		var chequeNo = $("#updateScreen #txtCheque").val();
		var registerdPayeeId = '';
		var registeredPayeeType = "";
		var description = $("#updateScreen #txtDescription").val().trim();
		var paymentMode = $("#updateScreen #selPaymentMode").val();
        var ledgerName = $("#updateScreen #selLedger :selected").text();

		if ($("#updateScreen #txtSupplier").val() !='') {
			registerdPayeeId = $("#updateScreen #txtSupplier").attr("supplier-id");
			registeredPayeeType = "seller";
		}
		else if($("#updateScreen #txtCustomer").val() != '') {
			registerdPayeeId = customerId;
			registeredPayeeType = "customer";
		}
		else {
			registerdPayeeId = "";
			registeredPayeeType = "";
		}


		if (($("#updateScreen #txtPayeeName").val() != '') || ($("#updateScreen #txtCustomer").val() != '')) {

			if (updateNonRegisteredPayeeTable.fnGetData() == '') {                
                $('#messagemyModal').modal();
                $("#messageBody").text("Can't save null data.");
                $("#messageMyModalLabel").text('Alert');
                return false;
            }

			var itemDetails = [];

			$.each($("#updateNonRegisteredPayeeTable tbody tr"),function(i,v){
				var obj = {};
				obj.GLAccountId = $(v).find('td:eq(0) span').attr('value');
                var netAmount = $(v).find('td:eq(5)').text();
				obj.netAmount = netAmount;
				obj.remarks = $(v).find('td:eq(6)').text();
                var netDis = $(v).find('td:eq(3)').text();
				obj.discount = (parseFloat(netAmount)+parseFloat(netDis))/parseFloat(netDis);
				var taxIDArray = [];
				$.each($(v).find('td:eq(1)').find('span'),function(ind,val){
					taxIDArray.push($(val).attr('value'));
				})
				obj.taxArray = taxIDArray;
				itemDetails.push(obj);
			});
            printItemDetails = itemDetails;
			var saveNetAmount = $("#updateScreen #txtNetAmount").val().trim();
			var postData = {
				"operation" : "updateCustomer_NonRegisterData",
				registerdPayeeId : registerdPayeeId,
				registeredPayeeType : registeredPayeeType,
				ledgerId : ledgerId,
				paymentMode : paymentMode,
				cheque_no : chequeNo,
				invoiceNo : '',
				saveNetAmount : saveNetAmount,
				payeeName : payeeName,
				description : description,
				id:id,
				itemDetails : JSON.stringify(itemDetails)
			}
			dataCall("controllers/admin/payment_voucher.php", postData, function (result){
				if (result == 1) {
                    $('#printMyModalLabel').text("Success");
                    $('#printMessageBody').text("Details saved successfully");
                    $('#printMyModal').modal();
					clearFormDetails('#advanced-wizard');
					clearFormDetails('#aupdateScreen');
                    vocherTable.fnReloadAjax();
                    $(".backtext").click();
					updateNonRegisteredPayeeTable.fnClearTable();
					updateReceiptNumberTable.fnClearTable();
                    printSellerInvoice = '';
                    $("#printModalFooter").html('<input type="button" class="btn_save" value="Print" onclick= "printDetails(\''+result+'\',\''+ledgerName+'\',\''+description+'\',\''+saveNetAmount+'\',\''+payeeName+'\');" id="print">');
				}
			});
		}

		else {

            if (updateReceiptNumberTable.fnGetData() == '') {                
                $('#messagemyModal').modal();
                $("#messageBody").text("Can't save null data.");
                $("#messageMyModalLabel").text('Alert');
                return false;
            }
			var productDetail = [];
			var receiptDetail = [];
			
			var aiRows = updateReceiptNumberTable.fnGetNodes(); //Get all rows of data table 		
			var rowcollection =  updateReceiptNumberTable.$(".chkProduct:checked", {"page": "all"});	//Select all unchecked row only
			var lastId ='';
			var discountItemAmt = 0;
			var totalAmount =0;
            var netdiscount = 0;
			//Getting data from all unchecked rows 
			for (i=0,c=rowcollection.length; i<c; i++) {	
				var objProduct ={};
				var objReceipt ={};
				var invoiceId = $($(rowcollection)[i]).attr('data-id');
				if(i== 0){
					
					objProduct.mainId = $($(rowcollection)[i]).attr('main-id');
                    objProduct.invoiceId = $($(rowcollection)[i]).attr('data-id');
                    objProduct.glAccountId = parseFloat($($($(rowcollection)[i]).parent().siblings()[8]).text());
                    objProduct.aPId = parseFloat($($($(rowcollection)[i]).parent().siblings()[9]).text());
                    objProduct.netAmount = parseFloat($($($(rowcollection)[i]).parent().siblings()[7]).text());
                    objProduct.discount = parseFloat($($($(rowcollection)[i]).parent().siblings()[4]).text());
					totalAmount += parseFloat($($($(rowcollection)[i]).parent().siblings()[7]).text());
					discountItemAmt+= parseFloat($($($(rowcollection)[i]).parent().siblings()[4]).text());
					productDetail.push(objProduct);
					lastId = invoiceId;
				}
				else if(invoiceId == lastId && i != 0){				
					objProduct.mainId = $($(rowcollection)[i]).attr('main-id');
                    objProduct.invoiceId = $($(rowcollection)[i]).attr('data-id');
                    objProduct.glAccountId = parseFloat($($($(rowcollection)[i]).parent().siblings()[8]).text());
                    objProduct.aPId = parseFloat($($($(rowcollection)[i]).parent().siblings()[9]).text());
                    objProduct.netAmount = parseFloat($($($(rowcollection)[i]).parent().siblings()[7]).text());
                    objProduct.discount = parseFloat($($($(rowcollection)[i]).parent().siblings()[4]).text());
					totalAmount += parseFloat($($($(rowcollection)[i]).parent().siblings()[7]).text());	
					discountItemAmt+= parseFloat($($($(rowcollection)[i]).parent().siblings()[4]).text());			
					productDetail.push(objProduct);
					lastId = invoiceId;
				}
				else{
					objReceipt.invoiceId = $($(rowcollection)[i-1]).attr('data-id');
					objReceipt.total = totalAmount;
					objReceipt.discount = discountItemAmt;
					receiptDetail.push(objReceipt);
					totalAmount = parseFloat($($($(rowcollection)[i]).parent().siblings()[7]).text());
					discountItemAmt = parseFloat($($($(rowcollection)[i]).parent().siblings()[4]).text());
					objProduct.mainId = $($(rowcollection)[i]).attr('main-id');
                    objProduct.invoiceId = $($(rowcollection)[i]).attr('data-id');
                    objProduct.glAccountId = parseFloat($($($(rowcollection)[i]).parent().siblings()[8]).text());
                    objProduct.aPId = parseFloat($($($(rowcollection)[i]).parent().siblings()[9]).text());
                    objProduct.netAmount = parseFloat($($($(rowcollection)[i]).parent().siblings()[7]).text());
                    objProduct.discount = parseFloat($($($(rowcollection)[i]).parent().siblings()[4]).text());
					productDetail.push(objProduct);
					lastId = invoiceId;
				}	
				if(i == c-1){
					objReceipt ={};
					objReceipt.invoiceId = $($(rowcollection)[i]).attr('data-id');
					objReceipt.total = totalAmount;
					objReceipt.discount = discountItemAmt;
					receiptDetail.push(objReceipt);
				}
			}
			var saveNetAmount = $("#updateScreen #txtNetAmount").val().trim();
            printSellerInvoice = receiptDetail;

			var postData = {
				operation : "updateRegisteredData",
				ledgerId : ledgerId,
				chequeNo : chequeNo,
				productDetail : JSON.stringify(productDetail),
				receiptDetail : JSON.stringify(receiptDetail),
				payeeName : payeeName,
				registerdPayeeId : registerdPayeeId,
				registeredPayeeType : registeredPayeeType,
				description : description,
				paymentMode : paymentMode,
				id:id,
				saveNetAmount : saveNetAmount,
				sellerId : registerPayeeId,
				glAccountId : glAccountId,
				saveNetAmount :saveNetAmount
			}
			dataCall("controllers/admin/payment_voucher.php", postData, function (result){
				if (result == 1) {
					clearFormDetails('#advanced-wizard');
					clearFormDetails('#aupdateScreen');
                    $(".backtext").click();
					updateNonRegisteredPayeeTable.fnClearTable();
					updateReceiptNumberTable.fnClearTable();
					filldiscount = 0;
                    printItemDetails = '';
                    $('#printMyModalLabel').text("Success");
                    $('#printMessageBody').text("Details saved successfully");
                    $('#printMyModal').modal();
                    $("#printMyModal #printModalFooter").html('<input type="button" class="btn_save" value="Print" onclick= "printDetails(\''+result+'\',\''+ledgerName+'\',\''+description+'\',\''+saveNetAmount+'\',\''+payeeName+'\');" id="print">');
				}
			});
		}
	});

    $('#updateScreen #btnReset').click(function() {
        clearModal();
        $('#updateScreen #mainScreen').click();
    });
    
   $("#updateScreen #mainScreen").click(); 
} // end update button

function showRows(myThis){
    var ledgerText = $(myThis).parent().siblings()[0];
    var ledgerClass = $(ledgerText).text().replace(/ /g,'');
    var parentTr = $(myThis).parent().parent()[0];
    var trClass = $(parentTr).parent().find("."+ledgerClass);
    $(parentTr).parent().find("."+ledgerClass).removeClass('hide');
    $(myThis).addClass('hide');
    $(myThis).siblings().removeClass('hide');
}
function hideRows(myThis){
    var ledgerText = $(myThis).parent().siblings()[0];
    var ledgerClass = $(ledgerText).text().replace(/ /g,'');
    var parentTr = $(myThis).parent().parent()[0];
    var trClass = $(parentTr).parent().find("."+ledgerClass);
    $(parentTr).parent().find("."+ledgerClass).addClass('hide');
    $(myThis).addClass('hide');
    $(myThis).siblings().removeClass('hide');
}

function bindLedger() {     //  function for loading ledger
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/payment_mode.php",
        data: {
            "operation": "showLedger"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option data-value='" + parseData[i].value + "' value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selLedger').html(option);
            }
        },
        error: function() {}
    });
}

function appendTaxes(){
    var postData = {
        operation : "getTaxes"
    }
    dataCall("controllers/admin/products.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (postData !='') {
            var html = '';
            $.each(parseData,function(i,v){
                html+='<div class="taxContainer"><input type="checkbox" class="chkTaxes" id="chkTax'+v.id+'" value="'+v.id+'" tax-rate = '+v.tax_rate+'><label class="productName" for="chkTax'+v.id+'">'+v.tax_name+'</label></div>';
            });
            $("#appendTaxes").html(html);
        }
        removeErrorMessage();
        countBlur = 0;
        $(".chkTaxes").change(function(){
        	if ($("#txtNRGrossAmount").val() == '') {
        		$("#txtNRGrossAmountError").text("Please enter amount");
        		$(this).prop("checked",false);
        		return false;
        	}
        	if ($("#txtNRDiscount").val() !='') {
        		$('.chkTaxes').prop("checked",false);
        		clearFormDetails("#nonRegisteredPayeeModalBody");
        		countBlur = 0;
        		return false;
        	}
        	if ($(this).prop("checked") == true) {

		    	var chkTaxAmount = $(this).attr('tax-rate');
		    	countTax += parseFloat(($("#txtNRGrossAmount").val() * chkTaxAmount)/100);

		    	$("#txtNRTaxAmount").val(countTax);
		    	$("#txtNRNetAmount").val(parseFloat($("#txtNRGrossAmount").val()) + countTax);
		    }
		    else{
		    	var chkTaxAmount = $(this).attr('tax-rate');
		    	if ($("#txtNRTaxAmount").val() !='') {
		    		countTax -= parseFloat(($("#txtNRGrossAmount").val() * chkTaxAmount)/100);

			    	$("#txtNRTaxAmount").val(countTax);
			    	if($("#txtNRTaxAmount").val() < 0){
			    		$("#txtNRTaxAmount").val(0);
			    	}
			    	$("#txtNRNetAmount").val(parseFloat($("#txtNRGrossAmount").val()) + countTax);
		    	}			    	
		    }
	    });

	    $("#txtNRGrossAmount").blur(function(){
			$("#txtNRDiscount").val('');
			$("#txtNRNetAmount").val($("#txtNRGrossAmount").val());
			$('.chkTaxes').prop("checked",false);
			countTax = 0;
			countBlur = 0;
		});
	   
		$("#txtNRDiscount").blur(function(){
			var nrDiscount = $("#txtNRDiscount").val();
			var nrNet = $("#txtNRNetAmount").val();
			if ((nrNet == '') || ($('.chkTaxes:checked').length == 0)) {
				$(this).val('');
				countTax = 0;
				countBlur = 0;
				return false;
			}
			if (countBlur > 0) {
				clearFormDetails("#nonRegisteredPayeeModalBody");
				$('.chkTaxes').prop("checked",false);
				countBlur = 0;
			}
			
					
			$("#txtNRNetAmount").val(parseFloat(nrNet-((nrNet * nrDiscount)/100)));
			countBlur++;
		});
    });
}


function totalNonRegisterPayment(myThis){

	if ($(myThis).prop("checked") == true) {
		countNRTaxAmount+=parseFloat($($(myThis).parent().siblings()[3]).text());
		$("#txtTaxAmount").val(countNRTaxAmount);
		countGrossAmount+=parseFloat($($(myThis).parent().siblings()[4]).text());
		$("#txtGrossAmount").val(countGrossAmount);
		countNRNetAmount+=parseFloat($($(myThis).parent().siblings()[5]).text());
		$("#txtNetAmount").val(countNRNetAmount);		
	}
	else{
		countNRTaxAmount-=parseFloat($($(myThis).parent().siblings()[3]).text());
		$("#txtTaxAmount").val(countNRTaxAmount);
		countGrossAmount-=parseFloat($($(myThis).parent().siblings()[4]).text());
		$("#txtGrossAmount").val(countGrossAmount);
		countNRNetAmount-=parseFloat($($(myThis).parent().siblings()[5]).text());
		$("#txtNetAmount").val(countNRNetAmount);

		if ($("#txtTaxAmount").val() < 0) {
			$("#txtTaxAmount").val(0);
		}
		if ($("#txtGrossAmount").val() < 0) {
			$("#txtGrossAmount").val(0);
		}
		if ($("#txtNetAmount").val() < 0) {
			$("#txtNetAmount").val(0);
		}
	}
}
function getPayeeName(ledgerId,paymentMode){
	var postData = {
		operation : "showPayeeName",
		ledgerId : ledgerId,
		paymentMode : paymentMode
	}
	dataCall("controllers/admin/payment_voucher.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (parseData !='') {
        	payeeName = parseData[0]['payee_name'];
        }
        else{
        	if ($("#txtSupplier").val() != '') {
        		payeeName = $("#txtSupplier").val();
        	}
        	else if ($("#txtCustomer").val() != '') {
        		payeeName = $("#txtCustomer").val();
        	}
        	else{
        		payeeName = $("#txtPayeeName").val();
        	}
        }
        if ($("#txtPayeeName").val() != '') {
        	payeeName = $("#txtPayeeName").val();
        }
    });
}
function paymentMode(ledgerId,payment_mode,selField){
	var postData = {
		operation : "showPaymentMode",
		ledgerId : ledgerId
	}
	dataCall("controllers/admin/payment_voucher.php", postData, function (result){
		if (result !='') {
			var parseData = JSON.parse(result);
			var option = "<option value=''>--Select--</option>";
			$.each(parseData,function(i,v){
				option += "<option value='" + v.id + "'>" +v.name+"</option>";
			});
			$(selField).html(option); 
			if (payment_mode !='' && payment_mode !=null) {
				$(selField).val(payment_mode);
			}
		}
	});
}

function rowDelete(currInst,txtGrossAmount,txtTaxAmount,txtNetAmount,txtDisAmt){
	var dataTable;
	var row = currInst.closest("tr").get(0);
	var gAmount = $(txtGrossAmount).val(parseFloat($(txtGrossAmount).val()) - parseFloat($(row).find('td:eq(4)').text()));
	var tAmount = $(txtTaxAmount).val(parseFloat($(txtTaxAmount).val()) - parseFloat($(row).find('td:eq(2)').text()));
	var nAmount = $(txtNetAmount).val(parseFloat($(txtNetAmount).val()) - parseFloat($(row).find('td:eq(5)').text()));
    var netDisAmt = $(txtDisAmt).val(parseFloat($(txtDisAmt).val()) - parseFloat($(row).find('td:eq(3)').text()));
	if (txtGrossAmount == "#txtGrossAmount") {
		nonRegisteredPayeeTable.fnDeleteRow(row);
	}
	else{
		updateNonRegisteredPayeeTable.fnDeleteRow(row);
	}
	
	if (parseFloat(gAmount.val()) < 0) {
		$(txtGrossAmount).val(0);
	}
	if (parseFloat(tAmount.val()) < 0) {
		$(txtTaxAmount).val(0);
	}
	if (parseFloat(nAmount.val()) < 0) {
		$(txtNetAmount).val(0);
	}
    if (parseFloat(netDisAmt.val() )< 0) {
        $(txtDisAmt).val(0);
    }
}
function tabChartOfAccountList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
    $("#tabAddChartOfAccount").css({
        "background": "#fff",
        "color": "#000"
    });
    $("#tabChartOfAccountList").css({
        "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
        "color": "#fff"
    });
    $("#chartOfAccountList").css({
        "background-color": "#fff"
    });
    $('#inactive-checkbox-tick').prop('checked', false).change();
}
function getSupplierDetails(supplierId,txtSupplierName,txtFirmName,txtEmail){
	var postData = {
		"operation" : "showSupplierDetail",
		supplierId : supplierId
	}
	dataCall("controllers/admin/payment_voucher.php", postData, function (result){
		if (result !='') {
			var parseData = JSON.parse(result);
			$(txtSupplierName).val(parseData[0].name);
			$(txtSupplierName).attr('supplier-id',supplierId);
			$(txtFirmName).val(parseData[0].firm_name)
			$(txtEmail).val(parseData[0].email);
		}
	});
}

function getCustomerDetails(customerId,txtCustomerId,txtCustomerName,txtEmail){
	var postData = {
		"operation" : "showCustomerDetail",
		customerId : customerId
	}
	dataCall("controllers/admin/payment_voucher.php", postData, function (result){
		if (result !='') {
			var parseData = JSON.parse(result);
			var customerIdLength = customerId.length;
			for (var i=0;i<6-customerIdLength;i++) {
				customerId = "0"+customerId;
			}
			customerId = customerPrefix+customerId;
			$(txtCustomerId).val(customerId);
			$(txtCustomerName).val(parseData[0].name)
			$(txtEmail).val(parseData[0].email);
		}
	});
}
function autoComplete(currentObj,txtNameField,txtEmailField,txtFirmField){
	jQuery(txtNameField).autocomplete({
		source: function(request, response) {
			var postData = {
				"operation":"search",
				"sellerName": currentObj.val()
			}
			
			$.ajax(
				{					
				type: "POST",
				cache: false,
				url: "controllers/admin/put_orders.php",
				datatype:"json",
				data: postData,
				
				success: function(dataSet) {
					response($.map(JSON.parse(dataSet), function (item) {
						return {
							id: item.id,
							value: item.name,
							email : item.email,
							firmName : item.firm_name
						}
					}));
				},
				error: function(){
					
				}
			});
		},
		select: function (e, i) {
			var sellerId = i.item.id;
			currentObj.attr("supplier-id", sellerId);
			$(txtNameField).val(i.item.value);
			$(txtEmailField).val(i.item.email);
			$(txtFirmField).val(i.item.firmName)		;
		},
		minLength: 3
	});
}
function insertDataTables(){
	nonRegisteredPayeeTable = $("#nonRegisteredPayeeTable").dataTable({
		"bFilter": true,
        "processing": true,
        "bPaginate": false,
        "bAutoWidth": false,
        "aaSorting": [],
        aoColumnDefs: [{'bSortable': false,'aTargets': [7]}],
        aLengthMenu: [
            [25, 50, 100, 200, -1],
            [25, 50, 100, 200, "All"]
        ],
        iDisplayLength: -1,
        "fnDrawCallback": function(oSettings) {

        }
	});

	totalamount = 0;
	netTotalAmount = 0;
    netdiscount = 0;
    receiptNumberTable = $("#receiptNumberTable").dataTable({
    	"bFilter": true,
        "processing": true,
        "deferLoading": 57,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "aaSorting": [],
        aoColumnDefs: [{'bSortable': false,'aTargets': [6,0]}],
        aLengthMenu: [
            [25, 50, 100, 200, -1],
            [25, 50, 100, 200, "All"]
        ],
        iDisplayLength: -1,
        "fnDrawCallback": function(oSettings) {
        	$(".chkProduct").unbind();
        	$(".chkProduct").on('change', function(){
        		var id = $(this).attr('data-id');
        		var amount = $($(this).parent().siblings()[6]).text();
        		var fillNetAmount = $($(this).parent().siblings()[7]).text();
                var filldiscount = $($(this).parent().siblings()[4]).text();
                var parentClass = $(this).attr('data-id');
        		if($(this).is(":checked")){
                    var m = false;
                    
                    var chkBox = $(this).parent().parent().parent().find("."+id+':not(:first)');
                    for(n=0; n<chkBox.length; n++){
                        if(!$($(this).parent().parent().parent().find('.'+id).find('.chkProduct')[n]).prop('checked')){
                            m = true;
                        }
                    }
                    if(m == false){
                        $(this).parent().parent().parent().find("."+parentClass+".chkInvoice").prop('checked',true);
                    }    
                    totalamount = totalamount + parseFloat(amount);
                    netTotalAmount = netTotalAmount + parseFloat(fillNetAmount);
                    netdiscount = netdiscount + parseFloat(filldiscount); 
                    $("#txtGrossAmount").val(totalamount);
                    $("#txtNetAmount").val(netTotalAmount);
                    $("#txtDiscount").val(netdiscount);
                    $("#txtTaxAmount").val(0.00);
                }							
				else{    
                    var m = false;
                    
                    var chkBox = $(this).parent().parent().parent().find("."+id+':not(:first)');                  
                    $(this).parent().parent().parent().find("."+parentClass+".chkInvoice").prop('checked',false);

                    totalamount = totalamount - parseFloat(amount);
                    netTotalAmount = netTotalAmount - parseFloat(fillNetAmount);
                    netdiscount = netdiscount - parseFloat(filldiscount);

                    $("#txtGrossAmount").val(totalamount);
                    $("#txtNetAmount").val(netTotalAmount);
                    $("#txtDiscount").val(netdiscount);
                    $("#txtTaxAmount").val(0.00);
                    if (totalamount < 0) {
                        $("#txtGrossAmount").val(0);
                    }
                    if (netTotalAmount < 0) {
                        $("#txtNetAmount").val(0);
                    }
                    if (filldiscount < 0) {
                        $("#txtDiscount").val(0);
                    }
                }
        	});
        	$(".chkInvoice").unbind();        	
        	$(".chkInvoice").on('change', function(){
        		var id = $(this).attr('data-id');
        		var subClass = $($(this).parent().siblings()[0]).text();
        		var amount = $($(this).parent().siblings()[6]).text();
        		var fillNetAmount = $($(this).parent().siblings()[7]).text();        		
        		if($(this).is(":checked")){
                    var chkBox = $(this).parent().parent().parent().find('.'+subClass).find('input[type="checkbox"]');
                    for(n=0; n<chkBox.length; n++){
                        if(!$($(this).parent().parent().parent().find('.'+subClass).find('input[type="checkbox"]')[n]).prop('checked')){
                            totalamount = totalamount + parseFloat($($(chkBox[n]).parent().siblings()[6]).text());
                            netTotalAmount = netTotalAmount + parseFloat($($(chkBox[n]).parent().siblings()[7]).text());
                            netdiscount = netdiscount + parseFloat($($(chkBox[n]).parent().siblings()[4]).text());
                        }
                    }	
                    $(this).parent().parent().parent().find('.'+subClass).find('input[type="checkbox"]').prop('checked',true);        		
	        		
                    $.grep($(this).parent().parent().parent().find('.'+subClass+':not(:first)'),function(val,ind){
                        filldiscount += parseFloat($(val).find('td:eq(5)').text());
                    });
					$("#txtGrossAmount").val(totalamount);
					$("#txtNetAmount").val(netTotalAmount);
					$("#txtDiscount").val(netdiscount);
					$("#txtTaxAmount").val(0.00);
				}
				else{

                    var chkBox = $(this).parent().parent().parent().find('.'+subClass).find('input[type="checkbox"]');
                    for(n=0; n<chkBox.length; n++){
                        if($($(this).parent().parent().parent().find('.'+subClass).find('input[type="checkbox"]')[n]).prop('checked')){
                            netdiscount = netdiscount - parseFloat($($(chkBox[n]).parent().siblings()[4]).text());
                        }
                    }
					$(this).parent().parent().parent().find('.'+subClass).find('input[type="checkbox"]').prop('checked',false);	        		
	        		totalamount = totalamount - parseFloat(amount);
	        		netTotalAmount = netTotalAmount - parseFloat(fillNetAmount);
	        		$.grep($(this).parent().parent().parent().find('.'+subClass+':not(:first)'),function(val,ind){
                        filldiscount -= parseFloat($(val).find('td:eq(5)').text());
                    });

					$("#txtGrossAmount").val(totalamount);
					$("#txtNetAmount").val(netTotalAmount);
					$("#txtDiscount").val(netdiscount);
					$("#txtTaxAmount").val(0.00);
					if (totalamount < 0) {
	        			$("#txtGrossAmount").val(0);
	        		}
	        		if (netTotalAmount < 0) {
	        			$("#txtNetAmount").val(0);
	        		}
	        		if (filldiscount < 0) {
	        			$("#txtDiscount").val(0);
	        		}
				}
        	});
        }
    });
}
function AddItems(dataTable,txtGrossAmount,txtTaxAmount,txtNetAmount,txtDiscountField){
	var flag =  false;
	var glAccountId = $("#nonRegisteredPayeeModal #selLedger").val();
	var glAccountName = $("#nonRegisteredPayeeModal #selLedger option:selected").text();
	var discount  = parseFloat($("#nonRegisteredPayeeModal #txtNRDiscount").val());
	var grossAmount = parseFloat($("#nonRegisteredPayeeModal #txtNRGrossAmount").val());

	
    if(validTextField('#nonRegisteredPayeeModal #txtNRDiscount','#nonRegisteredPayeeModal #txtNRDiscountError','Please enter discount') == true)
    {
        flag = true;
    }
    if(validTextField('#nonRegisteredPayeeModal #txtNRGrossAmount','#nonRegisteredPayeeModal #txtNRGrossAmountError','Please enter gross amount') == true)
    {
        flag = true;
    }
    if(validTextField('#nonRegisteredPayeeModal #selLedger','#nonRegisteredPayeeModal #selLedgerError','Please select ledger') == true)
    {
        flag = true;
    }
	if (flag == true) {
		return false
	}


	var remarks = $("#nonRegisteredPayeeModal #txtNRRemarks").val().trim();
	var taxHtml = '';
	$.each($('.chkTaxes'),function(i,v){
		if ($(v).prop("checked") == true  ) {
			taxHtml+= '<span value="'+$(v).val()+'">'+$(v).siblings().text()+'</span>';    			
		}
	});
	var taxAmount = parseFloat($("#nonRegisteredPayeeModal #txtNRTaxAmount").val());

	var netAmount = parseFloat($("#nonRegisteredPayeeModal #txtNRNetAmount").val());
    var calculatedDis = ((grossAmount+taxAmount) * discount)/100;
	dataTable.fnAddData(['<span value="'+glAccountId+'">'+glAccountName+'</span>',
		taxHtml,taxAmount,calculatedDis,grossAmount,netAmount,remarks,
		'<input type="button" class="btnEnabled" onclick="rowDelete($(this),\''+txtGrossAmount+'\',\''+txtTaxAmount+'\',\''+txtNetAmount+'\',\''+txtDiscountField+'\');" value="Remove">'
	]);

	clearFormDetails("#nonRegisteredPayeeModalBody");
	$('#nonRegisteredPayeeModalBody .chkTaxes').prop("checked",false);
	$("#nonRegisteredPayeeModal").modal('hide');
	
	sumGrossAmount = sumGrossAmount + grossAmount;
	sumTaxAmount = sumTaxAmount + taxAmount;
	sumNetAmount = sumNetAmount + netAmount;
    sumDiscount = sumDiscount + calculatedDis;

	$(txtGrossAmount).val(sumGrossAmount);
	$(txtTaxAmount).val(sumTaxAmount);
	$(txtNetAmount).val(sumNetAmount);
    $(txtDiscountField).val(sumDiscount);
}

function addPaymentVoucherItem(id){
	var postData = {
		"operation" : "showPaymentVoucherItem",
		"paymentVoucherId" : id
	}

	dataCall("controllers/admin/payment_voucher.php", postData, function (result){
		if (result != '') {
			var txtGrossAmount = "#updateScreen #txtGrossAmount";
			var txtTaxAmount = "#updateScreen #txtTaxAmount";
			var txtNetAmount = "#updateScreen #txtNetAmount";
            var txtDiscountField = "#updateScreen #txtDiscount";
			var parseData = JSON.parse(result);
			var grossAmt = 0;
			var totalDis = 0;
			var totalTaxAmt = 0;
			var totalNetAmt = 0;
            sumGrossAmount = 0;
            sumTaxAmount = 0;
            sumNetAmount = 0;
            sumDiscount = 0;
			$.each(parseData,function(i,v){
				var glAccDetail = '<span value="'+v.gl_account_id+'">'+v.account_name+'</span>';
				var taxDetails = '';
				var taxPercentage = 0;
				var netAmt = parseFloat(parseFloat(v.net_amount).toFixed(2));
				var discPercentage = parseFloat(parseFloat(v.discount).toFixed(2));
				$.each((v.tax_id).split(','),function(ind,val){
					taxDetails+='<span value="'+val+'">'+(v.tax_name).split(',')[ind]+'</span>';
					taxPercentage+=parseFloat((v.tax_rate).split(',')[ind]);
				});

				var taxAmt = parseFloat(((netAmt*100)/(100-discPercentage)).toFixed(2));
				var actAmt = parseFloat(((taxAmt*100)/(100+taxPercentage)).toFixed(2));
				var totalDisc = parseFloat(((taxAmt*discPercentage)/100).toFixed(2));
				var totalTax =  parseFloat(((actAmt*taxPercentage)/100).toFixed(2));
                grossAmt+=actAmt;
                totalDis+=totalDisc;
                totalTaxAmt+=totalTax;
                totalNetAmt+=netAmt
				updateNonRegisteredPayeeTable.fnAddData([glAccDetail,taxDetails,totalTax,totalDisc,actAmt,
					netAmt,v.remarks,
					'<input type="button" class="btnEnabled" onclick="rowDelete($(this),\''+txtGrossAmount+'\',\''+txtTaxAmount+'\',\''+txtNetAmount+'\',\''+txtDiscountField+'\');" value="Remove">'
				]);				
			});
            $("#updateScreen #txtGrossAmount").val(parseFloat(grossAmt.toFixed(2)));
            $("#updateScreen #txtDiscount").val(parseFloat(totalDis.toFixed(2)));
            $("#updateScreen #txtTaxAmount").val(parseFloat(totalTaxAmt.toFixed(2)));
            $("#updateScreen #txtNetAmount").val(parseFloat(totalNetAmt.toFixed(2)));
            sumGrossAmount+=grossAmt;
            sumTaxAmount+=totalTaxAmt;
            sumNetAmount+=totalNetAmt;
            sumDiscount+=totalDis;
			getChequeDeatils(id,"#updateScreen #txtCheque","#updateScreen #txtDescription");
		}
	});
}

function deleteClick(id) { // delete click function
    $('#confirmMyModalLabel').text("Delete payment voucher");
    $('#myConfirmModalBody').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deletePaymentVoucher("'+type+'");');
} // end click fucntion

function restoreClick(id) { // restore click function
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore payment voucher");
    $('#myConfirmModalBody').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deletePaymentVoucher("'+type+'");');
}
function deletePaymentVoucher(type) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        dataCall("controllers/admin/payment_voucher.php", postData, function (result){
            if (result != "0" && result == "1") {
                $('#messageMyModalLabel').text("Success");
                $('#messageBody').text("Payment voucher deleted successfully!!!");
                $('#messagemyModal').modal();
                vocherTable.fnReloadAjax();
                dataTableIntialization = 0;
            } 
            else {
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Sorry");
                $('.modal-body').text("Payment voucher is used, so you can not delete!!!");
                $('#messagemyModal').modal();
            }            
        }); 
    } 
    else {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "restore",
            "id": id
        }
        dataCall("controllers/admin/payment_voucher.php", postData, function (result){
            if (result != "0" && result != "") {
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Success");
                $('.modal-body').text("Payment voucher restored successfully!!!");
                $('#messagemyModal').modal();
                vocherTable.fnReloadAjax();
                dataTableIntialization = 0;
            }
        });
    }
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function printDetails(result,ledgerName,description,saveNetAmount,payeeName){
	/*hospital information through global varibale in main.js*/
	$("#lblPrintEmail").text(hospitalEmail);
	$("#lblPrintPhone").text(hospitalPhoneNo);
	$("#lblPrintAddress").text(hospitalAddress);
	if (hospitalLogo != "null") {
		imagePath = "./images/" + hospitalLogo;
		$("#printLogo").attr("src",imagePath);
	}
	$("#printHospitalName").text(hospitalName);

    var voucherLength = result.length;
    for(var i=0;i<6-voucherLength;i++){
        result = "0" + result;
    }
    result = voucherPrefix + result;

	$("#lblVoucherId").text(result);
    $('#lblPayeeName').text(payeeName);
	$("#lblLedgerName").text(ledgerName);
	$("#lblDate").text(todayDate());
	$("#amountPrefix").text(amountPrefix);
	$("#lblDescription").text(description);
	$(".lblTotalAmount").text(saveNetAmount);
	var myHtml = '';
	var myHtml2 = '';
	if (printSellerInvoice !='') {
		myHtml2+= '<div style="margin-left=4px"><label>'+description+'</label></div>'
		$.each(printSellerInvoice,function(i,v){
			myHtml2+= '<div><label>Invoices: </label><span> '+v.invoiceId+'</span></div>';
		});
		$(".voucherDescription tbody td:first-child").html(myHtml2);
        $('.bottom-dooted-border').addClass('hide');
	}
	if (printItemDetails !='') {
		$.each(printItemDetails,function(i,v){
			myHtml+='<tr><td>'+v.GLAccountName+'</td><td>'+v.netAmount+'</td></tr>';
		});						
		$("#GLAccountDesc").html(myHtml);
        $('.bottom-dooted-border').removeClass('hide');
	}		
	//$.print("#PrintDiv");		
    window.print();			
}

function disableRows(){
	var aiRows = vocherTable.fnGetNodes();//Get all rows of data table
    var aiData =  vocherTable.fnGetData();

    if(aiRows.length == 0){
        return false;
    }
    $.each(aiData,function(index,value){
        var isDisbursed = value.voucher_disbursement;
        if(isDisbursed == "yes"){
            $(aiRows[index]).css({'opacity':'.7','background':'#E7E3E4'});
            $($(aiRows[index]).find(".fa")).unbind();
            $($(aiRows[index]).find(".fa")).css({'cursor':'not-allowed','opacity':'.4'});
        }
    });
}

function getChequeDeatils(id,txtChequeField,txtDescriptionField){
	var postData = {
		operation : "getChequeDescription",
		id : id
	}
	dataCall("controllers/admin/payment_voucher.php", postData, function (result){
		if (result !='') {
			var parseData = JSON.parse(result);
			$(txtChequeField).val(parseData[0].cheque_no);
			$(txtDescriptionField).val(parseData[0].description);
		}
	});
}
function clear() {
    $('#txtGrossAmount').val('');
    $('#txtDiscount').val('');
    $('#txtTaxAmount').val('');
    $('#txtNetAmount').val('');
    $('#txtCheque').val('');
    $('#txtDescription').val('');
    removeErrorMessage();
}
function clearModal(){
    $('#updateScreen #txtGrossAmount').val('');
    $('#updateScreen #txtDiscount').val('');
    $('#updateScreen #txtTaxAmount').val('');
    $('#updateScreen #txtNetAmount').val('');
    $('#updateScreen #txtCheque').val('');
    $('#updateScreen #txtDescription').val('');
    removeErrorMessage();
}