var forensicTestTable;
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   forensic_test.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages forensic test data
 *************************************************************************************************** */
	loader();
	debugger;
	//
	//Calling the function for binding the data 
	//
	bindUnitMeasure();
	bindSpecimenType();
	bindGLAccount("#selGLAccount",'');
	bindAccountReceivable("#selAccountPayable");
	bindLedger("#selLedger");//defined in common.js
	
	$("#advanced-wizard").hide();
	$("#radiologyTestList").addClass('list');
	$("#tabForensicTestList").addClass('tab-list-add');
	
	//Click function is used for change the tab
    $("#tabAddForensicTest").click(function() {
    	showAddTab();      
    	clear();  
    });
	
	//Click function for show the list of forensic test
	 $("#tabForensicTestList").click(function() {
		 $('#inactive-checkbox-tick').prop('checked', false).change();
        tabForensicTestList();//Calling the function for show the list 
    });
	
	
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		loadDataTable();	
    }
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	forensicTestTable.fnClearTable();
	    	forensicTestTable.fnDestroy();
	    	//Datatable code
			forensicTestTable = $('#forensicTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"bAutoWidth" : false,
				"fnDrawCallback": function ( oSettings ) {
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  forensicTestTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var specimen_type = mData["specimen_id"];
							restoreClick(id,specimen_type);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/forensic_test.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					{ "mData": "name" },
					{ "mData" : "type"},//getting specimen id from specimen_type table
					{ "mData": "range" },
					{ "mData": "unit" },
					{ "mData": "fee" },
					{ "mData": "description" },
					
					{
					  "mData": function (o) { 
						var data = o;
						var id = data["id"];
						var specimen_type = data["specimen_id"];
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},	
				],
				aoColumnDefs: [
		          	{ 'bSortable': false, 'aTargets': [ 5 ] },
					{ 'bSortable': false, 'aTargets': [ 6 ] }
		       	]
			} );
		}
		else{
			forensicTestTable.fnClearTable();
	    	forensicTestTable.fnDestroy();
	    	//Datatable code
			loadDataTable();
		}
    });
	
	//		
    //validation , save data and ajax call on submit button 
	//
    $("#btnSubmit").click(function() {
        var flag = "false";
		$("#txtTestfee").val($("#txtTestfee").val().trim());
		$("#txtNormalRange").val($("#txtNormalRange").val().trim());
		$("#txtForensicTestName").val($("#txtForensicTestName").val().trim());
		
		if ($("#selGLAccount").val() == "") {
			$('#selGLAccount').focus();
            $("#selGLAccountError").text("Please select gl account");
			$("#selGLAccount").addClass("errorStyle");
            flag = "true";
        }
		if ($("#selAccountPayable").val() == "") {
			$('#selAccountPayable').focus();
            $("#selAccountPayableError").text("Please select account payable");
			$("#selAccountPayable").addClass("errorStyle");
            flag = "true";
        }
        if ($("#selLedger").val() == "") {
			$('#selLedger').focus();
            $("#selLedgerError").text("Please select ledger");
			$("#selLedger").addClass("errorStyle");
            flag = "true";
        }
		if ($("#txtTestfee").val() == "") {
			$('#txtTestfee').focus();
            $("#txtTestfeeError").text("Please enter test fee");
			$("#txtTestfee").addClass("errorStyle");              
            flag = "true";
        }
		if(!(/^\d*(\.\d{1})?\d{0,9}$/).test($('#txtTestfee').val()))
		{			
			$('#txtTestfee').focus();
            $("#txtTestfeeError").text("Please enter number only");
			$("#txtTestfee").addClass("errorStyle");
            flag = "true";
        }
		/* if ($("#selUnitMeasure").val() == "-1") {
			$('#selUnitMeasure').focus();
            $("#selUnitMeasureError").text("Please select unit measure");
			$("#selUnitMeasure").addClass("errorStyle");
            flag = "true";
        } */
		if ($("#selSpecimenType").val() == "-1") {
			$('#selSpecimenType').focus();
            $("#selSpecimenTypeError").text("Please select specimen type");
			$("#selSpecimenType").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtForensicTestName").val() == "") {
			$('#txtForensicTestName').focus();
            $("#txtForensicTestNameError").text("Please enter forensic test name");
			$("#txtForensicTestName").addClass("errorStyle");
            flag = "true";
        }
        
        if (flag == "true") {
            return false;
        }
		var specimenType = $("#selSpecimenType").val();
		var ledger = $("#selLedger").val();
		var glAccount = $("#selGLAccount").val();
		var accountPayable = $("#selAccountPayable").val();
		var unitMeasure = $("#selUnitMeasure").val();
		var testName = $("#txtForensicTestName").val();
		var normalRange = $("#txtNormalRange").val();
		var testFee = $("#txtTestfee").val();
		var description = $("#txtDescription").val();
		description = description.replace(/'/g, "&#39");
		var postData = {
			"operation":"save",
			"accountPayable":accountPayable,
			"ledger":ledger,
			"glAccount":glAccount,
			"specimenType":specimenType,
			"unitMeasure":unitMeasure,
			"testName":testName,
			"normalRange":normalRange,
			"testFee":testFee,
			"description":description
		}		
			
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/forensic_test.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#myModal').modal('hide');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Forensic test saved successfully!!!");
					$('#messagemyModal').modal();
					$('#inactive-checkbox-tick').prop('checked', false).change();
					bindUnitMeasure();
					bindSpecimenType();
					tabForensicTestList();
				}
				else{
					$("#txtForensicTestNameError").text("Test name already exists");
					$('#txtForensicTestName').focus();
					$("#txtForensicTestName").addClass("errorStyle");
				}				
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	});	
	
	//
    //keyup functionality
	//
	$("#selLedger").change(function() {
        if ($("#selLedger").val() != "") {
            $("#selLedgerError").text("");
            $("#selLedger").removeClass("errorStyle");
        }
    });
	$("#selAccountPayable").change(function() {
        if ($("#selAccountPayable").val() != "") {
            $("#selAccountPayableError").text("");
            $("#selAccountPayable").removeClass("errorStyle");
        }
    });
    $("#selGLAccount").change(function() {
        if ($("#selGLAccount").val() != "") {
            $("#selGLAccountError").text("");
            $("#selGLAccount").removeClass("errorStyle");
        }
    });
	$("#selSpecimenType").change(function() {
        if ($("#selSpecimenType").val() != "-1") {           
        	$("#selSpecimenTypeError").text("");
            $("#selSpecimenType").removeClass("errorStyle");
        }
    });
	
	$("#selUnitMeasure").change(function() {
        if ($("#selUnitMeasure").val() != "-1") {           
        	$("#selUnitMeasureError").text("");
            $("#selUnitMeasure").removeClass("errorStyle");
        }
    });
	
    $("#txtForensicTestName").keyup(function() {
        if ($("#txtForensicTestName").val() != "") {
            $("#txtForensicTestNameError").text("");
            $("#txtForensicTestName").removeClass("errorStyle");
        }
    });
	
	$("#txtTestfee").keyup(function() {
        if ($("#txtTestfee").val() != "") {
            $("#txtTestfeeError").text("");
            $("#txtTestfee").removeClass("errorStyle");
        }
    });

    //on reset click focus on first textbox
    $("#btnReset").click(function(){
    	$("#txtForensicTestName").focus();
    	$("#txtTestfee").removeClass("errorStyle");
		$("#selUnitMeasure").removeClass("errorStyle");
		$("#selLabType").removeClass("errorStyle");
		$("#txtForensicTestName").removeClass("errorStyle");
		$("#selSpecimenType").removeClass("errorStyle");
		clear();
    });
});

//		
//function for edit the forensic test
//
function editClick(id,name,specimen,normalRange,unit,fee,description,ledger,account_payable,glAccount){	
    
	showAddTab();
	$("#uploadFile").hide();
    $("#btnReset").hide();
    $("#btnSubmit").hide();
    $('#tabAddForensicTest').html("+Update Forensic Test");

	$("#btnUpdateForensic").show();
	$("#selSpecimenType").val(specimen);
	$("#selLedger").val(ledger);
	$("#selGLAccount").val(glAccount);
	$("#selAccountPayable").val(account_payable);
	$("#txtForensicTestName").val(name);
	$("#txtNormalRange").val(normalRange);
	$("#selUnitMeasure").val(unit);
	$("#txtTestfee").val(fee);
	$('#txtDescription').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id);
	$("#txtForensicTestName").focus();
	
	//bindUnitMeasure();
	//bindSpecimenType();
	
	
	//Click function for update button
	$("#btnUpdateForensic").click(function(){
		var flag = "false";
		$("#txtTestfee").val($("#txtTestfee").val().trim());
		$("#txtNormalRange").val($("#txtNormalRange").val().trim());
		$("#txtForensicTestName").val($("#txtForensicTestName").val().trim());
		
		if ($("#selGLAccount").val() == "-1") {
			$('#selGLAccount').focus();
            $("#selGLAccountError").text("Please select gl account");
			$("#selGLAccount").addClass("errorStyle");
            flag = true;
        }
		if ($("#selAccountPayable").val() == "-1") {
			$('#selAccountPayable').focus();
            $("#selAccountPayableError").text("Please select account payable");
			$("#selAccountPayable").addClass("errorStyle");
            flag = true;
        }
        if ($("#selLedger").val() == "-1") {
			$('#selLedger').focus();
            $("#selLedgerError").text("Please select ledger");
			$("#selLedger").addClass("errorStyle");
            flag = true;
        }
		if ($("#txtTestfee").val() == "") {
			$('#txtTestfee').focus();
            $("#txtTestfeeError").text("Please enter test fee");
			$("#txtTestfee").addClass("errorStyle");
            flag = "true";
        }
		if(!(/^\d*(\.\d{1})?\d{0,9}$/).test($('#txtTestfee').val()))
		{			
			$('#txtTestfee').focus();
            $("#txtTestfeeError").text("Please enter number only");
			$("#txtTestfee").addClass("errorStyle");
        }
		/* if ($("#selUnitMeasure").val() == "-1") {
			$('#selUnitMeasure').focus();
            $("#selUnitMeasureError").text("Please select unit measure");
			$("#selUnitMeasure").addClass("errorStyle");
            flag = "true";
        } */
		if ($("#selSpecimenType").val() == "-1") {
			$('#selSpecimenType').focus();
            $("#selSpecimenTypeError").text("Please select specimen type");
			$("#selSpecimenType").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtForensicTestName").val() == "") {
			$('#txtForensicTestName').focus();
            $("#txtForensicTestNameError").text("Please enter test name");
			$("#txtForensicTestName").addClass("errorStyle");
            flag = "true";
        }
		
		
		if(flag == "true"){			
			return false;
		}
		else{
			var ledger = $("#selLedger").val();
			var glAccount = $("#selGLAccount").val();
			var accountPayable = $("#selAccountPayable").val();
			var specimenType = $("#selSpecimenType").val();
			var unitMeasure = $("#selUnitMeasure").val();
			var testName = $("#txtForensicTestName").val();
			var normalRange = $("#txtNormalRange").val();
			var testFee = $("#txtTestfee").val();
			var description = $("#txtDescription").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){				
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/forensic_test.php",
					data :{            
						"operation" : "update",
						"id" : id,
						"ledger":ledger,
						"glAccount":glAccount,
						"accountPayable":accountPayable,
						"specimenType":specimenType,
						"unitMeasure":unitMeasure,
						"testName":testName,
						"normalRange":normalRange,
						"testFee":testFee,
						"description":description
					},
					success: function(data) {	
						if(data != "0" && data != ""){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Forensic test updated successfully!!!");
							$('#messagemyModal').modal();
							forensicTestTable.fnReloadAjax();
       						tabForensicTestList();//Calling the function for show the list 
							clear();
						}
						
						else{
							$("#txtForensicTestNameError").text("Test name already exists");
							$('#txtForensicTestName').focus();
							$("#txtForensicTestName").addClass("errorStyle");				
						}
					},
					error:function() {
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
				});
			});
		}
	});		
		
}

//Click function for delete the forensic test
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete forensic test");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteForensicTest("'+type+'");');
}

//Click function for restore the forensic test
function restoreClick(id,specimen_type){
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#selSpecimenType').val(specimen_type);
	$('#confirmMyModalLabel').text("Restore forensic test");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();

	var type="restore";
	$('#confirm').attr('onclick','deleteForensicTest("'+type+'");');
}

//function for delete the forensic test
function deleteForensicTest(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
			
		//Ajax call for delete the forensic test 	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/forensic_test.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Forensic test deleted successfully!!!");
					$('#messagemyModal').modal();
					forensicTestTable.fnReloadAjax();					 
				}
				else{
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("This test is used , so you can not delete!!!");
					$('#messagemyModal').modal();
				}
			},
			error: function()
			{
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();  
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
		var specimen_type = $("#selSpecimenType").val();
	
		//Ajax call for restore the forensic test 
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/forensic_test.php",
			data :{            
				"operation" : "restore",
				"id" : id,
				"specimen_type" : specimen_type
			},
			success: function(data) {	
				if(data == "Restored Successfully!!!" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Forensic test restored successfully!!!");
					$('#messagemyModal').modal();
					forensicTestTable.fnReloadAjax();
				}	
				if(data == "It can not be restore!!!"){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("It can not be restore!!!");
					$('#messagemyModal').modal();
					forensicTestTable.fnReloadAjax();
				}
			},
			error:function()
			{
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	}
}

//
//Function for clear the data
//
function clear(){
	$('#selSpecimenType').val("");
	$('#selSpecimenTypeError').text("");
	$('#selUnitMeasure').val("");
	$('#selUnitMeasureError').text("");
	$('#txtForensicTestName').val("");
	$('#txtForensicTestNameError').text("");
	$('#txtNormalRange').val("");
	$('#txtTestfee').val("");
	$('#txtTestfeeError').text("");
	$('#txtDescription').val("");
	$('#selLedger').val("");
	$('#selGLAccount').val("");
	$('#selAccountPayable').val("");
	$('#selLedgerError').text("");
	$('#selAccountPayableError').text("");
	$('#selGLAccountError').text("");
	$("#selAccountPayable").removeClass("errorStyle");
	$("#selLedger").removeClass("errorStyle");
	$("#selGLAccount").removeClass("errorStyle");
	bindUnitMeasure();//Calling this function for binding the unit measure 
	bindSpecimenType();//Calling this function for binding the specimen type
}

//
//function for binding the unit measure
//
function bindUnitMeasure(){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/forensic_test.php",
		data: {
			"operation":"showUnitMeasure"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value=''>-- Select --</option>";
				for (var i=0;i<parseData.length;i++)
				{
				option+="<option value='"+parseData[i].id+"'>"+parseData[i].unit+"</option>";
				}				
				$('#selUnitMeasure').html(option);				
			}
		},
		error:function(){
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();			
		}
	});	
}

//This function is used for binding the specimen type
function bindSpecimenType(){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/forensic_test.php",
		data: {
			"operation":"showSpecimenType"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>-- Select --</option>";
				for (var i=0;i<parseData.length;i++)
				{
				option+="<option value='"+parseData[i].id+"'>"+parseData[i].type+"</option>";
				}				
				$('#selSpecimenType').html(option);				
			}
		},
		error:function(){	
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();		
		}
	});	
}

//This function is used for forensic test list
function tabForensicTestList(){
	$("#advanced-wizard").hide();
    $(".blackborder").show();

    $("#tabAddForensicTest").removeClass('tab-detail-add');
    $("#tabAddForensicTest").addClass('tab-detail-remove');
    $("#tabForensicTestList").removeClass('tab-list-remove');    
    $("#tabForensicTestList").addClass('tab-list-add');
    $("#radiologyTestList").addClass('list');


    $("#uploadFile").show();
    $("#btnReset").show();
    $("#btnSubmit").show();
    $("#btnUpdateForensic").hide();
    $('#tabAddForensicTest').html("+Add Forensic Test");

  
	 $("#txtTestfee").removeClass("errorStyle");
	 $("#txtNormalRange").removeClass("errorStyle");
	 $("#selUnitMeasure").removeClass("errorStyle");
	 $("#selLabType").removeClass("errorStyle");
	 $("#txtForensicTestName").removeClass("errorStyle");
	 $("#selSpecimenType").removeClass("errorStyle");
	clear();
}

function showAddTab(){
	$("#advanced-wizard").show();
    $(".blackborder").hide();
    
    $("#tabAddForensicTest").addClass('tab-detail-add');
    $("#tabAddForensicTest").removeClass('tab-detail-remove');
    $("#tabForensicTestList").removeClass('tab-list-add');
    $("#tabForensicTestList").addClass('tab-list-remove');
    $("#radiologyTestList").addClass('list');
	$('#txtForensicTestName').focus();
}
//click escape button to close popup
$(document).keyup(function(event) {
    if(event.keyCode === 27) {
        $('.close').click();
    }
});

function loadDataTable(){
	forensicTestTable = $('#forensicTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"bAutoWidth" : false,
		"fnDrawCallback": function ( oSettings ) {
			$('.update').unbind();
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = forensicTestTable.fnGetData(data);
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					var name = mData["name"];
					var specimen = mData["specimen_id"];
					var normalRange = mData["range"];
					var unit = mData["unit_id"];
					var fee = mData["fee"];
					var description = mData["description"];
					var ledger = mData["ledger"];
					var glAccount = mData["gl_account_id"];
					var account_payable = mData["account_payable_id"];
					editClick(id,name,specimen,normalRange,unit,fee,description,ledger,account_payable,glAccount);
					
   
				}
			});
			$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  forensicTestTable.fnGetData(data);
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);
				}
			});
		},
		
		"sAjaxSource":"controllers/admin/forensic_test.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{ "mData": "name" },
			{ "mData": "type" },
			{ "mData": "range" },
			{ "mData": "unit" },
			{ "mData": "fee" },
			{ "mData": "description" },
			{
			  "mData": function (o) { 
				var data = o;
				return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
					   " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
					   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
					   " style='font-size: 22px; color:#a9425a; cursor:pointer;' "+
					   " data-original-title='Delete'></i>";
			  }
				
			},	
		],
		aoColumnDefs: [
			{ 'bSortable': false, 'aTargets': [ 5 ] },
			{ 'bSortable': false, 'aTargets': [ 6 ] }
		]
	} );
}
//# sourceURL = forensic.js