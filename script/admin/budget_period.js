var budgetPeriodTable;
var startDate;
var endDate;
$(document).ready(function(){
    loader();
    debugger;

	bindFiscalYear();  //bind fiscal year 
    $('#txtBeginDate').prop('disabled', true);
    $('#txtEndDate').prop('disabled', true);

    $(".input-datepicker").datepicker({ // date picker function
        autoclose: true
    });

	/*Hide add ward by default functionality*/
	$("#advanced-wizard").hide();
	$("#budgetPeriodList").addClass('list');
	$("#tabBudgetPeriodList").addClass('tab-list-add');

    $("#tabAddBudgetPeriod").click(function() {
        tabBudgetPeriodShow();
    });
    

    $('#selFiscalYear').blur(function() {
        $('#txtBeginDate').prop('disabled', false);
        $('#txtEndDate').prop('disabled', false);
        startDate = new Date($('#selFiscalYear :selected').text().substring(0,10));
        endDate = new Date($('#selFiscalYear :selected').text().substring(13));
        $("#txtBeginDate").datepicker('setStartDate',startDate);
        $("#txtBeginDate").datepicker('setEndDate',endDate);
        $("#txtEndDate").datepicker('setEndDate',endDate);
        $("#txtEndDate").datepicker('setStartDate',startDate);
    });

    $("#tabBudgetPeriodList").click(function() {
        tabBudgetPeriodList();
        clear();
    });

    removeErrorMessage();  //remove error message

    $('#btnSubmit').click(function() {
        var flag = false;
        if($('#txtEndDate').val() == '') {
            $('#txtEndDateError').text('Please enter end date');
            $('#txtEndDate').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtBeginDate').val() >= $('#txtEndDate').val()) {
            $('#txtEndDateError').text('End date should not be less than or equal to begin date');
            $('#txtEndDate').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtBeginDate').val() == '') {
            $('#txtBeginDateError').text('Please enter begin date');
            $('#txtBeginDate').addClass("errorStyle"); 
            flag = true;
        }
        if($('#selFiscalYear').val() == '') {
            $('#selFiscalYearError').text('Please select fiscal year');
            $('#selFiscalYear').focus();
            $('#selFiscalYear').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtName').val() == '') {
            $('#txtNameError').text('Please enter name');
            $('#txtName').focus();
            $('#txtName').addClass("errorStyle"); 
            flag = true;
        }
        if(flag == true) {
            return false;
        }

        var name = $('#txtName').val().trim();
        var fiscalYear = $('#selFiscalYear').val().trim();
        var beginDate = $('#txtBeginDate').val().trim();
        var endDate = $('#txtEndDate').val().trim();
        var description = $('#txtDescription').val().trim();

        var postData = {
            name : name,
            fiscalYear : fiscalYear,
            beginDate : beginDate,
            endDate : endDate,
            description : description,
            operation : "saveBudgetPeriod"
        }


        $.ajax(
            {                   
            type: "POST",
            cache: false,
            url: "controllers/admin/budget_period.php",
            datatype:"json",
            data: postData,
            
            success: function(data) {
                if(data != "0" && data != ""){
                    callSuccessPopUp('Success','Saved successfully!!!');
                    clear();
                    $("#tabBudgetPeriodList").click();
                    $("#txtStartDate").datepicker('setEndDate',startDate);
                    $('#txtEndDate').datepicker('setStartDate', startDate);
                }
                else {                
                    $("#txtNameError").text("Budget period name is already exist");
                    $("#txtName").focus();               
                    $("#txtName").addClass('errorStyle');
                }
            },
            error: function(){

            }
        });
    });

    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
    //Datatable code 
        budgetPeriodTable = $('#budgetPeriodTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "bAutoWidth": false,
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = budgetPeriodTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var name = mData["name"];  
                        var fiscalYear = mData["fiscal_year"];
                        var fiscalYearPeriod = mData["fiscal_year_period"];
                        var beginDate = mData["begin_date"];                    
                        var endDate = mData["end_date"];
                        var description = mData["description"];
                        editClick(id,name,fiscalYear,fiscalYearPeriod,beginDate,endDate,description);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = budgetPeriodTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/budget_period.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "name"
                }, {
                    "mData": "fiscal_year_period"
                }, {
                    "mData": "begin_date"
                }, {
                    "mData": "end_date"
                }, {
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [4,5]
            }]

        });
    }

    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            budgetPeriodTable.fnClearTable();
            budgetPeriodTable.fnDestroy();
            budgetPeriodTable = "";
            budgetPeriodTable = $('#budgetPeriodTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = budgetPeriodTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var name = mData["name"];
                            restoreClick(id,name);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/budget_period.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "name"
                    }, {
                        "mData": "fiscal_year_period"
                    }, {
                        "mData": "begin_date"
                    }, {
                        "mData": "end_date"
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [4,5]
                }]
            });
        } else { // show active data on unchecked   
            budgetPeriodTable.fnClearTable();
            budgetPeriodTable.fnDestroy();
            budgetPeriodTable = "";
            budgetPeriodTable = $('#budgetPeriodTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = budgetPeriodTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var name = mData["name"];  
                            var fiscalYear = mData["fiscal_year"];
                            var fiscalYearPeriod = mData["fiscal_year_period"];
                            var beginDate = mData["begin_date"];                    
                            var endDate = mData["end_date"];
                            var description = mData["description"];
                            editClick(id,name,fiscalYear,fiscalYearPeriod,beginDate,endDate,description);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = budgetPeriodTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/budget_period.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "name"
                    }, {
                        "mData": "fiscal_year_period"
                    }, {
                        "mData": "begin_date"
                    }, {
                        "mData": "end_date"
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [4,5]
                }]
            });
        }
    });

    $('#btnReset').click(function(){
        clear();
    });
});

function editClick(id,name,fiscalYear,fiscalYearPeriod,beginDate,endDate,description) {
    
    $('#tabAddBudgetPeriod').click();
    $('#tabAddBudgetPeriod').html('+Update Budget Period');
    $('#btnSubmit').hide();
    $('#btnReset').hide();
    $('#txtBeginDate').prop('disabled', false);
    $('#txtEndDate').prop('disabled', false);
    
    $("#btnUpdate").removeAttr("style");
    
    $("#txtName").removeClass("errorStyle");
    $("#txtNameError").text("");

    $('#txtName').val(name);
    $('#selFiscalYear').val(fiscalYear);
    $('#txtBeginDate').val(beginDate);
    $('#txtEndDate').val(endDate);
    $('#txtDescription').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);

    $("#txtBeginDate,#txtEndDate").click(function(){
        $(".datepicker").css({"z-index":"99999","autoclose": true});
    });

        $('#txtBeginDate,#txtEndDate').datepicker({
            autoclose: true
        });

        startDate = new Date($('#selFiscalYear :selected').text().substring(0,10));
        endDate = new Date($('#selFiscalYear :selected').text().substring(13));
        $("#txtBeginDate").datepicker('setStartDate',startDate);
        $("#txtBeginDate").datepicker('setEndDate',endDate);
        $("#txtEndDate").datepicker('setEndDate',endDate);
        $("#txtEndDate").datepicker('setStartDate',startDate);

    $('#selFiscalYear').blur(function() {
        startDate = new Date($('#selFiscalYear :selected').text().substring(0,10));
        endDate = new Date($('#selFiscalYear :selected').text().substring(13));
        $("#txtBeginDate").datepicker('setStartDate',startDate);
        $("#txtBeginDate").datepicker('setEndDate',endDate);
        $("#txtEndDate").datepicker('setEndDate',endDate);
        $("#txtEndDate").datepicker('setStartDate',startDate);
    });

   



    removeErrorMessage();
    //validation
    if($('#txtEndDate').val() == '') {
        $('#txtEndDateError').text('Please enter end date');
        $('#txtEndDate').addClass("errorStyle"); 
        flag = true;
    }
    if($('#txtBeginDate').val() >= $('#txtEndDate').val()) {
        $('#txtEndDateError').text('End date should not be less than or equal to begin date');
        $('#txtEndDate').addClass("errorStyle"); 
        flag = true;
    }
    if($('#txtBeginDate').val() == '') {
        $('#txtBeginDateError').text('Please enter begin date');
        $('#txtBeginDate').addClass("errorStyle"); 
        flag = true;
    }
    if($('#selFiscalYear').val() == '') {
        $('#selFiscalYearError').text('Please select fiscal year');
        $('#selFiscalYear').focus();
        $('#selFiscalYear').addClass("errorStyle"); 
        flag = true;
    }
    $("#txtName").keyup(function() {
        if ($("#txtName").val() != '') {
            $("#txtNameError").text("");
            $("#txtName").removeClass("errorStyle");
        }
    });
    
    
    $("#btnUpdate").click(function() { // click update button
        var flag = false;
        if ($("#txtName").val()== '') {
            $("#txtNameError").text("Please enter name");
            $("#txtName").focus();
            $("#txtName").addClass("errorStyle");
            flag = true;
        }
        
        if(flag == true) {
            return false;
        }

        var name = $("#txtName").val().trim();
        var fiscalYear = $('#selFiscalYear').val().trim();
        var beginDate = $('#txtBeginDate').val().trim();
        var endDate = $('#txtEndDate').val().trim();
        var description = $("#txtDescription").val().trim();
        description = description.replace(/'/g, "&#39");
        var id = $('#selectedRow').val();
        
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
        var postData = {
            "operation": "update",
            "name": name,
            "fiscalYear" : fiscalYear,
            "beginDate" : beginDate,
            "endDate" :endDate,
            "description": description,
            "id": id
        }
        $.ajax( //ajax call for update data
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/budget_period.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                    	$('#tabBudgetPeriodList').click();
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Budget Period updated successfully!!!");
                        $('#messagemyModal').modal();
                        budgetPeriodTable.fnReloadAjax();
                        clear();
                    }
                   else {                
                        $("#txtNameError").text("Budget Period is already exist");
                        $("#txtName").focus();               
                        $("#txtName").addClass('errorStyle');
                    }
                },
                error: function() {
                    $('.close-confirm').click();
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                    $('#messagemyModal').modal();
                }
            }); // end of ajax
        });
    });
} // end update button

function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Budget Period");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteBudgetPeriod("' + type + '","'+id+'","");');
} // end click fucntion

function restoreClick(id,name) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Budget Period");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteBudgetPeriod("' + type + '","'+id+'","'+name+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteBudgetPeriod(type,id,name) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/budget_period.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Budget Period deleted successfully!!!");
                    $('#messagemyModal').modal();
                    budgetPeriodTable.fnReloadAjax();
                } 
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/budget_period.php",
            data: {
                "operation": "restore",
                "name":name,
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Budget Period restored successfully!!!");
                    $('#messagemyModal').modal();
                    budgetPeriodTable.fnReloadAjax();
                }
                else {
                    callSuccessPopUp('Sorry','Budget Period name already exist you can not restore !!!');
                }
            },
            error: function() {             
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary unavailable to respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}
function tabBudgetPeriodShow(){
    $("#advanced-wizard").show();
    $(".blackborder").hide();
	clear();
    $("#tabAddBudgetPeriod").addClass('tab-detail-add');
    $("#tabAddBudgetPeriod").removeClass('tab-detail-remove');
    $("#tabBudgetPeriodList").removeClass('tab-list-add');
    $("#tabBudgetPeriodList").addClass('tab-list-remove');
    $("#budgetPeriodList").addClass('list');
    $('#txtName').focus();
}
function tabBudgetPeriodList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
    $("#tabAddBudgetPeriod").removeClass('tab-detail-add');
    $("#tabAddBudgetPeriod").addClass('tab-detail-remove');
    $("#tabBudgetPeriodList").removeClass('tab-list-remove');
    $("#tabBudgetPeriodList").addClass('tab-list-add');
    $("#budgetPeriodList").addClass('list');
    $("#btnReset").show();
    $("#btnSubmit").show();
    $('#btnUpdate').hide();
    $('#tabAddBudgetPeriod').html("+Add Budget Period");
    $('#inactive-checkbox-tick').prop('checked', false).change();
    clear();
}


function bindFiscalYear() {
    var postData ={
        operation : "showFiscalYear"
    }
    dataCall("controllers/admin/budget_period.php", postData, function (result){
        if (result != null && result != "") {
            var parseData = jQuery.parseJSON(result);

            var option = "<option value=''>-- Select --</option>";
            for (var i = 0; i < parseData.length; i++) {
                option += "<option value='" + parseData[i].id + "'>" + parseData[i].period + "</option>";
            }
            $('#selFiscalYear').html(option);
        } 
    });
}
function clear() {
    removeErrorMessage();
    clearFormDetails('.clearForm');
    $('#txtName').focus();
}