var otable ;
var paymentMode = '';
var insuranceCompanyId = "";
var insurancePlanId = "";
var insurancePlanNameid = "";
var revisedCost = '';
var paymentMethod ='';
var copayType = '';
var copayValue = '';
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   drug_prescription.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Arora
 * Created Date :   31th dec, 2015
 * Description  :   This page add procedure tests in consultant module
 *************************************************************************************************** */
	loader();
	debugger;
	$(".amountPrefix").text(amountPrefix);
	$('#oldHistoryModalDetails input[type=text]').addClass("as_label");
    $('#oldHistoryModalDetails input[type=text]').attr('readonly', true);
    $('#oldHistoryModalDetails input[type="button"]').addClass("btnHide");
    $('#oldHistoryModalDetails #opdform').hide();

	$(".tab-content .block-title").hide(); // hide title in consultant screen

	/**DataTable Initialization**/
	otable = $('#drugPresecriptionTbl').DataTable({
		aLengthMenu: [
	        [25, 50, 100, 200, -1],
	        [25, 50, 100, 200, "All"]//show all results at once
	    ],
	    iDisplayLength: -1,
		"bAutoWidth" : false,
		aoColumnDefs: [
		   { aTargets: [ 10 ], bSortable: false },//remove sort from 4th column
		]
	});
	
	if ($('#thing').val() ==3) {
        var visitId = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
        getPaymentMode(visitId);
    }
    if ($('#thing').val() ==4) {
        var visitId = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
        getPaymentMode(visitId);
    }
    if ($('#thing').val() ==5) {
        var visitId = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
        getPaymentMode(visitId);
    }
	

	/*Search drug on icon click*/
	$("#iconSearch").click(function(){
		$('#drugModalLabel').text("Search Drug");
		$('#drugModalBody').load('views/admin/search_drug.html', function(){});//load the html to serach drug
		$('#myDrugModal').modal();
		$("#txtDrugNameError").text("");// adding here if get an validation error on select of wrong input
		$("#txtDrugName").removeClass("errorStyle");
	});

	/*On button click display drug details*/
	$("#btnSelect").click(function(){
		var flag = false;
		if ($("#txtDrugName").val()=="") {
			$('#txtDrugName').focus();
            $("#txtDrugNameError").text("Please enter drug name");
           $('#txtDrugName').addClass("errorStyle");
            flag=true;
		}
		if (flag == true) {
			return false;
		}

		var drug = $("#txtDrugName").val();
		var postData = {
			"operation":"searchDrug",
			"drug":drug
		}
		$.ajax({					
			type: "POST",
			cache: false,
			url: "controllers/admin/drug_prescription.php",
			datatype:"json",
			data: postData,
			success: function(data) {
				if(data != "0" && data != ""){
					var parseData = jQuery.parseJSON(data);

					for (var i=0;i<parseData.length;i++) {    					
						$("#txtAvailableStocks").val(parseData[i].quantity);								
						$("#txtAlternateName").val(parseData[i].alternate_name);								
						$("#txtUnitMeasure").val(parseData[i].unit_measure);								
						$("#txtUnitPrice").val(parseData[i].price);
						$("#txtDrugId").val(parseData[i].id);
						var drugId = $("#txtDrugId").val();
						if (paymentMode.toLowerCase() == "insurance") {
				            getRevisedCost(drugId);
				            checkExclusion(drugId,insuranceCompanyId,insurancePlanId,insurancePlanNameid,"pharmacy");
				        }
				        else{
				            checkTestExcluded = '';
				        }
					}
				}
				if (data == 0) {
					$('#messagemyModal').modal();
					$('#messageMyModalLabel').text("Error");
                    $('#messagemyModal .modal-body').text("Drug Doesn't Exisit!!!");                    
				}
			},
			error:function(){
				alert('error');				  
			}
		});
	});
	
	/*Save data to database*/
	$("#btnDrugAdd").click(function(){
		var flag = false;
		if (otable.fnGetData().length < 1) {
			$('#messagemyModal').modal("show");
			$(".modal-footer").css({"display":"block"});
			$('#messageMyModalLabel').text("Error");
			$('#messagemyModal .modal-body').text("Please fill data in datatable");
			flag=true;
		}
		if (flag == true) {
			return false;
		}

		/*Code to get only current data on click means not to get previous data coming from database*/
        var arr= [];
        jQuery.grep(otable.fnGetData(), function( n, i ){
            if($(n[0]).attr("data-previous") == "false"){
                arr.push(n);
            }
        });

		/*Defining array outside loop so to hold multiple values*/
		var smallArray = new Array(); //small array contain forenscic test price and id
		var bigArray = new Array(); // big 2d array conatin small array

		var aiRows = arr;//pass here arr variable as we have to send only current data not past data

		for (i=0,c=aiRows.length; i<c; i++) {
			iRow = aiRows[i];   // assign current row to iRow variable
			aData = iRow; // Pull the row
			smallArray = new Array();//so that hold only one value after traverse
			smallArray.push(aData[12],aData[1],aData[2],aData[3],aData[4],aData[13],aData[10]);//getting id, dosage,duration,quantity ,notes ,payment mode,amount
			bigArray.push(smallArray);
		}

		var totalCurrentReqBill = 0;
        //get the total cost of all currently added request
        $.each(bigArray ,function(i,v){
            if (v[5] == 'insurance') {
                totalCurrentReqBill= totalCurrentReqBill + parseInt(v[6]);
            }                            
        });

		var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();
		/*get date in(yyyy-mm-dd)format*/
		var outputDate = d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day;

		if(arr.length == 0){
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Sorry");
			$('.modal-body').text("No new drug added!!!");
			return false;
		}
		var visitId =  parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
		var patientId = parseInt($("#txtPatientID").val().replace ( /[^\d.]/g, '' ));
		var postData ={
			"operation" : "saveData",
			"visitId" : visitId,
			"outputDate" : outputDate,
			"patientId" :patientId,
			arrayData : JSON.stringify(bigArray),
			'copayType' : copayType,
            'copayValue' : copayValue,
            'totalCurrentReqBill' : totalCurrentReqBill
		}
		$('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to save this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
			$.ajax({
				type: "POST",
				cache: false,
				url: "controllers/admin/drug_prescription.php",
				datatype:"json",
				data: postData,
				success : function(data){
					if(data !="" && data != "0"){
						$('#messagemyModal').modal("show");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Drug prescription saved successfully!!!");

						if (paymentMode == 'insurance') {
							loadData(visitId,paymentMode,insuranceCompanyId,insurancePlanId,insurancePlanNameid);
						}
						else {
							loadData(visitId,paymentMode,'','','');
						}							
						clear();
					}
					if(data ==""){
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Something awful happened!! Can't fetch old requests. Please try to contact admin.");
						$('#messagemyModal').modal();
					}
				},			
				error : function(){
					alert('error');
				}
			});
		});
	});

	/* Add data to datatable*/
	$("#btnAddData").click(function(){
		var flag = false;
		
		if (!validnumber($("#txtQuantity").val())) {
			$('#txtQuantity').focus();
            $("#txtQuantityError").text("Please enter only number");
            $('#txtQuantity').addClass("errorStyle");
          flag=true;  
		}
		if ($("#txtQuantity").val()=="") {
			$('#txtQuantity').focus();
            $("#txtQuantityError").text("Please enter quantity");
           $('#txtQuantity').addClass("errorStyle");
            flag=true;
		}
		if ($("#txtDuration").val()=="") {
			$('#txtDuration').focus();
            $("#txtDurationError").text("Please enter duration");
           $('#txtDuration').addClass("errorStyle");
            flag=true;
		}
		if ($("#txtDosage").val()=="") {
			$('#txtDosage').focus();
            $("#txtDosageError").text("Please enter dosage");
           $('#txtDosage').addClass("errorStyle");
            flag=true;
		}
		if ($("#txtDrugName").val()=="") {
			$('#txtDrugName').focus();
            $("#txtDrugNameError").text("Please enter drug name");
           $('#txtDrugName').addClass("errorStyle");
            flag=true;
		}
		if (flag == true) {
			return false;
		}		

		var drug = $("#txtDrugName").val();
		var dosage =  $("#txtDosage").val();
		var duration = $("#txtDuration").val();
		var quantity = $("#txtQuantity").val();
		var notes = $("#txtNotes").val();
		var alternateName = $("#txtAlternateName").val();
		var availableStocks = $("#txtAvailableStocks").val();
		var unitMeasure = $("#txtUnitMeasure").val();
		var unitPrice = $("#txtUnitPrice").val();		
		var drugId = $("#txtDrugId").val();
		var newRevisedCost = '';
        if (revisedCost == "") {
            newRevisedCost = unitPrice;
            paymentMethod = "cash";
        }
        else {
            newRevisedCost = revisedCost;
            paymentMethod = "insurance";
        }

		var aiRows = otable.fnGetNodes();//get nodes
		var previousCount = 0;

		for (i=0,c=aiRows.length; i<c; i++) {
			iRow = aiRows[i];   // assign current row to iRow variable
			aData = otable.fnGetData(iRow); // Pull the row
		}

		if (checkTestExcluded =="yes") {
            $('#confirmUpdateModalLabel').text();
            $('#updateBody').text("Are you sure you want to add this still it is not included in your insurance?");
            $('#confirmUpdateModal').modal();
            $("#btnConfirm").unbind();
            $("#btnConfirm").click(function(){
                var newPaymentMethod = "cash";
                newRevisedCost = unitPrice;//if it is excluded then revised cost is equal to orignal cost
                var amount = newRevisedCost*quantity;

	            /*Add data in ttable*/
				var currData = otable.fnAddData( ["<span id ='newDataOfTable' data-previous='false'>"+drug+"</span>", 
					dosage, duration, quantity, notes, alternateName, availableStocks,
					unitMeasure, unitPrice,newRevisedCost, amount,"<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>",drugId,newPaymentMethod
				] );

				/*Get current row to add color in that*/
				var getCurrDataRow = otable.fnSettings().aoData[ currData ].nTr;
				$(getCurrDataRow).find('td').css({"background-color":"#fff","color":"#000"});
				$('#totalAmount').text(parseInt($('#totalAmount').text())+parseInt(amount));//add total bill
            });
            
        }
        else {

        	var amount = newRevisedCost*quantity;
        	/*Add data in ttable*/
			var currData = otable.fnAddData( ["<span id ='newDataOfTable' data-previous='false'>"+drug+"</span>", 
				dosage, duration, quantity, notes, alternateName, availableStocks,
				unitMeasure, unitPrice,newRevisedCost, amount,"<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>",drugId,paymentMethod
			] );

			/*Get current row to add color in that*/
			var getCurrDataRow = otable.fnSettings().aoData[ currData ].nTr;
			$(getCurrDataRow).find('td').css({"background-color":"#fff","color":"#000"});
			$('#totalAmount').text(parseInt($('#totalAmount').text())+parseInt(amount));//add total bill
        }
			

		
	   /*  $(getCurrDataRow).find('span').css("color","#fff !important;"); */

		
		
		/*Blank data of text box after sucessful addition to datatable*/
		$("#txtDrugName").val("");
		$("#txtDosage").val("");
		$("#txtDuration").val("");
		$("#txtQuantity").val("");
		$("#txtNotes").val("");
		$("#txtAlternateName").val("");
		$("#txtAvailableStocks").val("");
		$("#txtUnitMeasure").val("");
		$("#txtUnitPrice").val("");
		$("#txtAmount").val("");
	});

	/*Reset functionality*/
	$("#btnDrugreset").click(function(){
		clear();
		$('#txtDrugName').focus();
		var lengthDeleteRow = $(".btnEnabled").closest("tr").length;
        for(var i=0;i<lengthDeleteRow;i++){
            var myDeleteRow = $(".btnEnabled").closest("tr")[0];            
            $('#totalAmount').text(parseInt($('#totalAmount').text()) - parseInt($(myDeleteRow).find('td:eq(10)').text()));
            otable.fnDeleteRow(myDeleteRow);
        }
	});

	$("#btnPrintDrug").click(function(){
		if (otable.dataTable().fnGetData().length > 0) {
			$('#lblPrintVisitId').text($("#textvisitId").val());								
			$('#lblPrintPatientId').text($("#txtPatientID").val());
			$('#lblPrintPatient').text($("#txtpatientName").val());
			$('#lblPrintVisitDate').text($("#txtvisitDate").val());
			//hospital information through global varibale in main.js
			$("#lblPrintEmail").text(hospitalEmail);
			$("#lblPrintPhone").text(hospitalPhoneNo);
			$("#lblPrintAddress").text(hospitalAddress);
			if (hospitalLogo != "null") {
				imagePath = "./images/" + hospitalLogo;
				$("#printLogo").attr("src",imagePath);
			}
			$("#printHospitalName").text(hospitalName);

			var printTableData = otable.fnGetData();
			$("#printedTable").html('');
			var html = '<table id="printedTable"><tr><th class="text-center" width="20%">Drug</th><th class="text-center" width="20%">Dosage</th><th class="text-center" width="40%">Duration</th><th class="text-center" width="20%">Quantity</th></tr>';
			$.each(printTableData,function(i,value){
				html +="<tr>";
				html +="<td class='text-center'>"+$(printTableData[i][0]).text()+"</td>";
				html +="<td class='text-center'>"+printTableData[i][1]+"</td>";
				html +="<td class='text-center'>"+printTableData[i][2]+"</td>";
				html +="<td class='text-center'>"+printTableData[i][3]+"</td>";
				html +="</tr>";
			});
			html += "</table>";
			$("#printContainerTable").html(html);	
			window.print();
			//$.print("#PrintPrescription");
		}		
	});

	/*key up functionality*/
	$("#txtQuantity").keyup(function(){
		if ($("#txtQuantity").val() !="") {
			$("#txtQuantity").removeClass("errorStyle");
			$("#txtQuantityError").text("");
		}
	});
	$("#txtDrugName").keyup(function(){
		if ($("#txtDrugName").val() !="") {
			$("#txtDrugName").removeClass("errorStyle");
			$("#txtDrugNameError").text("");
		}
	});
	$("#txtDosage").keyup(function(){
		if ($("#txtDosage").val() !="") {
			$("#txtDosage").removeClass("errorStyle");
			$("#txtDosageError").text("");
		}
	});
	$("#txtNotes").keyup(function(){
		if ($("#txtNotes").val() !="") {
			$("#txtNotes").removeClass("errorStyle");
			$("#txtNotesError").text("");
		}
	});
	$("#txtDuration").keyup(function(){
		if ($("#txtDuration").val() !="") {
			$("#txtDuration").removeClass("errorStyle");
			$("#txtDurationError").text("");
		}
	});

	/*On keyup calculate amount*/
	$("#txtQuantity").keyup(function(){
		if($("#txtUnitPrice").val() !=""){
			calAmount();
		}
	});	
	
	$("#txtDrugName").on('keyup', function(){		//Autocomplete functionality 
		var currentObj = $(this);
		jQuery("#txtDrugName").autocomplete({
			source: function(request, response) {
				var postData = {
					"operation":"search",
					"drugId": '',
					"drugName": currentObj.val(),
					"drugCode": ''
				}
				
				$.ajax(
					{					
					type: "POST",
					cache: false,
					url: "controllers/admin/search_drug.php",
					datatype:"json",
					data: postData,
					
					success: function(dataSet) {
						loader();
						//searchedDrug = JSON.parse(dataSet);
						response($.map(JSON.parse(dataSet), function (item) {
							return {
								id: item.id,
								value: item.name
							}
						}));
					},
					error: function(){
						
					}
				});
			},
			select: function (e, i) {
				var drugId = i.item.id;
				currentObj.attr("drug-id", drugId);
				$("#txtDrugName").val(i.item.value);
				if (paymentMode.toLowerCase() == "insurance") {
	                getRevisedCost(drugId);
	                checkExclusion(drugId,insuranceCompanyId,insurancePlanId,insurancePlanNameid,"pharmacy");
	            }
	            else{
	                checkTestExcluded = '';
	            }
				$("#btnSelect").click();				
			},
			minLength: 3
		});		
	});
});
function loadData(visitId,paymentMode,insuranceCompanyId,insurancePlanId,insurancePlanNameid){
	/* var visitId = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' )); */
	var postData = {
        "operation": "getOldData",
        "visit_id": visitId,
        "paymentMode":paymentMode,
        "activeTab" : "pharmacy",
        "insuranceCompanyId" : insuranceCompanyId,
        "insurancePlanId" : insurancePlanId,
        "insurancePlanNameid" : insurancePlanNameid
    }
	$.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/drug_prescription.php",
        datatype: "json",
        data: postData,

        success: function(data) {
            dataSet = JSON.parse(data);  
			otable.fnClearTable();
			$('#totalAmount').text("0");
            
            for(var i=0; i< dataSet.length; i++){
            	var id = dataSet[i].id;
                var name = dataSet[i].name;
                var quantity = dataSet[i].quantity;
                var dosage = dataSet[i].dosage;
                var duration = dataSet[i].duration;
                var notes = dataSet[i].notes;
                var alternate_name = dataSet[i].alternate_name;
                var unit = dataSet[i].unit;
                var price = dataSet[i].price;
                var stock = dataSet[i].stock;                
                var oldRevisedCost =''; 
                if (paymentMode != null && paymentMode.toLowerCase() == "insurance") {
                    oldRevisedCost = dataSet[i].revised_cost;
                }
                else {
                    oldRevisedCost = price;
                }
                var amount = quantity * oldRevisedCost;

                /*Apply here span to recognise previous or current data*/
                otable.fnAddData(["<span data-previous='true'>"+name+"</span>",dosage, duration, quantity, 
                	notes, alternate_name, stock, unit, price,oldRevisedCost, amount,"<input type='button' class ='btnDisabled' value='Remove' disabled>",id]);

                $('#totalAmount').text(parseInt($('#totalAmount').text()) + parseInt(amount));//add total bill   

				otable.find('tbody').find('tr').find('td').css({"background-color": "rgb(204, 204, 204)"});
            }
        }
    });
}
function clear(){
	$("#txtDrugName").val("");
	$("#txtDosage").val("");
	$("#txtDuration").val("");
	$("#txtQuantity").val("");
	$("#txtNotes").val("");
	$("#txtAlternateName").val("");
	$("#txtAvailableStocks").val("");
	$("#txtUnitMeasure").val("");
	$("#txtUnitPrice").val("");
	$("#txtAmount").val("");

	$("#txtDrugNameError").text("");
	$("#txtDosageError").text("");
	$("#txtDurationError").text("");
	$("#txtQuantityError").text("");
	$("#txtNotesError").text("");

	$("#txtDrugName").removeClass("errorStyle");
	$("#txtDosage").removeClass("errorStyle");
	$("#txtDuration").removeClass("errorStyle");
	$("#txtQuantity").removeClass("errorStyle");
	$("#txtNotes").removeClass("errorStyle");
}

/*Function to calculate amount*/
function calAmount(){
	var unitPrice = $("#txtUnitPrice").val();
	var quantity = $("#txtQuantity").val();
	var intialAmount = unitPrice * quantity;
	$("#txtAmount").val(intialAmount);
}

/*function to delete row and subtract from total amount*/
function rowDelete(currInst){
	var row = currInst.closest("tr").get(0);//getting closest tr
	var oTable = $('#drugPresecriptionTbl').dataTable();
	oTable.fnDeleteRow(oTable.fnGetPosition(row));
	$('#totalAmount').text(parseInt($('#totalAmount').text())-parseInt($(row).find('td:eq(10)').text()));//subtract amount of that row from total amount on remove of a row
}

/*Function to validate number*/
function validnumber(number) {
    floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
    return floatRegex.test(number);
}

/*key press event on ESC button*/
$(document).keyup(function(e) {
    if (e.keyCode == 27) { 
		/* window.location.href = "http://localhost/herp/"; */
		$('.close').click();
    }
});


//get payment mode insurance or other
function getPaymentMode(visitId){
    var postData = {
        "operation": "payment_mode",
        "visitId": visitId
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (result.trim() != '') {
            paymentMode = parseData[0].payment_mode;

            if (paymentMode.toLowerCase() == "insurance") {
                insuranceDetails(visitId);
                paymentMethod = "insurance";
            }
            else{
                paymentMethod = "cash";
                /*Fetch data from db and insert in to table during page load*/
    			loadData(visitId,paymentMode,'','','');
            }         
        }         
    });
}
function insuranceDetails(visitId){
    var postData = {
        "operation": "insuranceDetail",
        "visitId": visitId
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (parseData != '') {
            insuranceCompanyId = parseData[0].insurance_company_id;
            insurancePlanId = parseData[0].insurance_plan_id;
            insurancePlanNameid = parseData[0].insurance_plan_name_id;

            loadData(visitId,paymentMode,insuranceCompanyId,insurancePlanId,insurancePlanNameid);

            getCopayValue();//get the copay details
        }
    });
}
function getRevisedCost(drugId){
    var postData = {
        "operation": "revisedCostDetail",
        "insuranceCompanyId": insuranceCompanyId,
        "insurancePlanId" : insurancePlanId,
        "insurancePlanNameid" : insurancePlanNameid,
        "componentId" : drugId,
        "activeTab" : "pharmacy"
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (result.length > 2) {
            revisedCost = parseData[0].revised_cost;
        }
        else{
            revisedCost ='';
        }
    });
}
function getCopayValue(){
    var postData = {
        "operation": "getCopayValue",
        "insuranceCompanyId": insuranceCompanyId,
        "insurancePlanId" : insurancePlanId,
        "insurancePlanNameid" : insurancePlanNameid
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (parseData != '' && parseData != null) {
            copayType = parseData[0].copay_type;
            copayValue = parseData[0].value;
        }
    });
}
//src : drug_prescription.js