var lineItemTable;
$(document).ready(function(){
	debugger;
	//bindRecurrencePeriod();
	removeErrorMessage();
	bindARAccount();
	$("#txtCurrency").val(amountPrefix);
	$("#lineItemScreen").hide();
	$("#recurranceDetailScreen").hide();
	
    $($(".customerBtn")[0]).addClass("activeSelf");

	$(".customerBtn").click(function(){
		$(".customerBtn").removeClass('activeSelf');
        $(this).addClass('activeSelf');
		if($(this).val() == 'Line Items'){
			$("#lineItemScreen").show();
			$("#recurranceDetailScreen").hide();
			$("#mainCustomerScreen").hide();
		}
		else if($(this).val() == 'Recurrance Detail'){
			$("#lineItemScreen").hide();
			$("#recurranceDetailScreen").show();
			$("#mainCustomerScreen").hide();
		}
		else{
			$("#lineItemScreen").hide();
			$("#recurranceDetailScreen").hide();
			$("#mainCustomerScreen").show();
		}
	});
	$("#txtBillingDate").datepicker({ changeYear: true,changeMonth: true,autoclose: true}) .on('changeDate', function(selected){
		startDate = new Date(selected.date.valueOf());
		$('#txtDueDate').datepicker('setStartDate', startDate);
	}); 
	$('#txtDueDate').datepicker({
		changeYear: true,
		changeMonth: true,
		autoclose: true
	});
	
	$(".input-datepicker").datepicker({ changeYear: true,changeMonth: true,autoclose: true});

	lineItemTable = $('#lineItemTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "fnDrawCallback": function(oSettings) {
        	$('#lineItemTable .txtQuantity').blur(function(){
        		var quantity = $(this).val();
        		if(quantity == ""){
        			quantity = 0;
        		}
        		var cost = $($(this).parent().siblings()[1]).find('option:selected').attr('cost');
        		var total = parseFloat(cost)*parseFloat(quantity);
        		var tax = $($(this).parent().siblings()[1]).find('option:selected').attr('tax');
        		var taxAmount = ((parseFloat(total)*parseFloat(tax))/100);
        		var newTotal = parseFloat(total)+parseFloat(taxAmount);
        		var discount = $($(this).parent().siblings()[4]).find('input').val();
        		if(discount != ""){ 
        		if(discount>100)  {
        			discount = 100;
        			$($(this).parent().siblings()[4]).find('input').val(100);
        		}     			
        			//var amount =  $($(this).parent().siblings()[6]).text();
        		var percentage = ((parseFloat(newTotal)*parseFloat(discount))/100);
        		var newTotal = parseFloat(newTotal)-parseFloat(percentage);
        		//$($(this).parent().siblings()[4]).parent().html("<label class='lblDiscount lbl'>" + discount + "</label>");
        		}
        		
        		$($(this).parent().siblings()[5]).text(newTotal);
        		$($(this).parent().siblings()[3]).text(taxAmount);
        		//$(this).parent().html("<label class='lblQuantity lbl' >" + quantity + "</label>");
        		calculateTotal();
        	});
        	$('#lineItemTable .txtDiscount').blur(function(){
        		var discount = $(this).val();
        		if(discount == ""){
        			discount = 0;
        		}
        		if(discount>100)  {
        			discount = 100;
        			$(this).val(100);
        		}  
        		var qty = $($(this).parent().siblings()[2]).find('input').val();
        		var cost = $($(this).parent().siblings()[1]).find('option:selected').attr('cost');
        		var tax = $($(this).parent().siblings()[1]).find('option:selected').attr('tax');
        		var total =  parseFloat(cost)*parseFloat(qty);
        		var taxAmount = ((parseFloat(total)*parseFloat(tax))/100);
        		var amount =  (parseFloat(cost)*parseFloat(qty))+parseFloat(taxAmount);
        		var percentage = ((parseFloat(amount)*parseFloat(discount))/100);
        		var newAmount = parseFloat(amount)-parseFloat(percentage);        		
        		$($(this).parent().siblings()[5]).text(newAmount);
        		//$(this).parent().html("<label class='lblDiscount lbl' >" + discount + "</label>");
        		calculateTotal();

        	});
        }
    });

    $("#txtCustomerId").focus();

    $("#btnShowCustomerDetails").click(function(){
    	var flag = false;

        if(validTextField('#txtCustomerId','#txtCustomerIdError','Please enter customer Id') == true)
        {
            flag = true;
        }
        if (flag == true) {
        	return false;
        }

        var customerId = $("#txtCustomerId").val();            
	    if (customerId !='') {
	        if (customerId.length != 9) {
	        	$("#txtCustomerId").addClass("errorStyle");
	        	$("#txtCustomerId").focus();
	        	$("#txtCustomerIdError").text("Customer Id is not valid");
	            return false;
	        }
	        var getCustomerPrefix = customerId.substring(0, 3);
	        if (getCustomerPrefix.toLowerCase() != customerPrefix.toLowerCase()) {
	        	$("#txtCustomerId").addClass("errorStyle");
	        	$("#txtCustomerId").focus();
	        	$("#txtCustomerIdError").text("Customer Id is not valid");
	            return false;
	        }
	        customerId = customerId.replace ( /[^\d.]/g, '' ); 
	        customerId = parseInt(customerId);
	    }

	    var postData = {
			"operation" : "showCustomerDetails",
			customerId : customerId
		}

		dataCall("controllers/admin/customer_invoice.php", postData, function (result){
			var parseData = JSON.parse(result);
			if (parseData != '') {
				$("#lblFirstName").text(parseData[0].first_name);
				$("#lblLastName").text(parseData[0].last_name);
				$("#lblEmail").text(parseData[0].email);
			}
			else{
				callSuccessPopUp("Alert","No data exist for choosen id.");
				$("#lblFirstName").text("");
				$("#lblLastName").text("");
				$("#lblEmail").text("");
			}
		});
    });

    $("#btnCustomerSave").click(function(){
    	var flag= "false";

    	if($("#selARAccount").val()=="-1")
        {
        	$("#selARAccountError").text("Please select account.");
        	$("#selARAccount").addClass("errorStyle");
        	$("#selARAccount").focus();
            flag = true;
        }
        if(validTextField('#txtDueDate','#txtDueDateError','Please select due date') == true)
        {
            flag = true;
        }
        if(validTextField('#txtBillingDate','#txtBillingDateError','Please select billing date') == true)
        {
            flag = true;
        }
        if(validTextField('#txtCustomerId','#txtCustomerIdError','Please enter customer Id') == true)
        {
            flag = true;
        }
        if (flag == true) {
        	$(".customerBtn").removeClass('activeSelf');
        	$("#lineItemScreen").hide();
			$("#recurranceDetailScreen").hide();
			$("#mainCustomerScreen").show();
			$($(".customerBtn")[0]).addClass("activeSelf");
        	return false;
        }
        else{
        	$(".customerBtn").removeClass('activeSelf');
			$("#lineItemScreen").show();
			$("#recurranceDetailScreen").hide();
			$("#mainCustomerScreen").hide();
        	$($(".customerBtn")[1]).addClass("activeSelf");

        	if(lineItemTable.find('tbody tr td').text() == "No data available in table"){
        		//$("#addProductDetails").unbind();
        		//$("#addProductDetails").click();
        		flag = 'true';
        	}
        	else{
        		flag = 'false';
        		var tableLength = lineItemTable.find('tbody tr').length;
        		for(i=0; i<tableLength; i++){
        			if($($(lineItemTable.find('tbody tr')[i]).find('td')[0]).text() == ""){
        				flag = "true";
        			}
        			if($($(lineItemTable.find('tbody tr')[i]).find('td')[2]).find('input').val() == ""){
        				flag = "true";
        			}
        			if($($(lineItemTable.find('tbody tr')[i]).find('td')[3]).text() == ""){
        				flag = "true";
        			}
        			if($($(lineItemTable.find('tbody tr')[i]).find('td')[4]).text() == ""){
        				flag = "true";
        			}
        			if($($(lineItemTable.find('tbody tr')[i]).find('td')[5]).find('input').val() == ""){
        				flag = "true";
        			}
        			if($($(lineItemTable.find('tbody tr')[i]).find('td')[6]).text() == ""){
        				flag = "true";
        			}
        		}
        		if(flag == "true"){
        			$(".customerBtn").removeClass('activeSelf');
					$("#lineItemScreen").show();
					$("#recurranceDetailScreen").hide();
					$("#mainCustomerScreen").hide();
		        	$($(".customerBtn")[1]).addClass("activeSelf");
        			callSuccessPopUp("Alert","Please fill datatable field.");
        			return false;
        		}
        		else{
        			$(".customerBtn").removeClass('activeSelf');
					$("#lineItemScreen").hide();
					$("#recurranceDetailScreen").show();
					$("#mainCustomerScreen").hide();
		        	$($(".customerBtn")[2]).addClass("activeSelf");
		        	
			        

		        	if($("#selReccurencePeriod").val()!="Does Not Recur"){			        	

			            if($("#txtEndDate").val()=="")
				        {
				        	$("#txtEndDateError").text("Please select end date.");
				        	$("#txtEndDate").addClass("errorStyle");
				            flag = "true";
				        }

				        if($("#txtNextDate").val()=="")
				        {
				        	$("#txtNextDateError").text("Please select next date.");
				        	$("#txtNextDate").addClass("errorStyle");
				            flag = "true";
				        }
			        }

			        if(flag == "true"){
			        	return false;
			        }

			        else{
			        	var productData =  [];
			        	var aiRows = lineItemTable.fnGetNodes(); //Get all rows of data table 	

			        	for(i=0; i<aiRows.length; i++){
			        		var obj ={};
			        		var code = $($(lineItemTable.find('tbody tr')[i]).find('td')[0]).text();
			        		code = code.replace ( /[^\d.]/g, '' ); 
        					obj.productCode = parseInt(code);
                            obj.productId = $($(lineItemTable.find('tbody tr')[i]).find('td')[1]).find('option:selected').val();
        					obj.glAccountId = $($(lineItemTable.find('tbody tr')[i]).find('td')[7]).find('option:selected').val();
        					obj.productQuantity = $($(lineItemTable.find('tbody tr')[i]).find('td')[2]).find('input').val();
        					obj.productCost = $($(lineItemTable.find('tbody tr')[i]).find('td')[3]).text();
        					obj.productTax = $($(lineItemTable.find('tbody tr')[i]).find('td')[4]).text();
        					obj.productDiscount = $($(lineItemTable.find('tbody tr')[i]).find('td')[5]).find('input').val();
        					obj.productTotal = $($(lineItemTable.find('tbody tr')[i]).find('td')[6]).text();
        					productData.push(obj);

			        	}
			        	var customerId = $("#txtCustomerId").val();
			        	customerId = customerId.replace ( /[^\d.]/g, '' ); 
        				customerId = parseInt(customerId);
			        	var billingDate = $("#txtBillingDate").val(); 
			        	var dueDate = $("#txtDueDate").val(); 
			        	var arAccount = $("#selARAccount").val(); 
			        	var currency = $("#txtCurrency").val(); 
			        	var salesRepresentative = $("#salesRepresentative").val(); 
			        	var recurrencePeriod = $("#selReccurencePeriod").val();
			        	var nextDate = $("#txtNextDate").val();
			        	var endDate = $("#txtEndDate").val();
			        	var grandTotal = $("#totalBill").text();
			        	
			        	var postData = {
				   			"operation":"save",
				   			"customerId" : customerId,
				   			"billingDate" : billingDate,
				   			"salesRepresentative" : salesRepresentative,
				   			"recurrencePeriod" : recurrencePeriod,
				   			"arAccount" : arAccount,
				   			"dueDate" : dueDate,
				   			"grandTotal" : grandTotal,
				   			"currency" : currency,
				   			"nextDate" : nextDate,
				   			"endDate" : endDate,				
							"productData":JSON.stringify(productData)
				   		}
					    $.ajax({     
							type: "POST",
							cache: false,
							url: "controllers/admin/customer_invoice.php",
							data: postData,
							success: function(data) { 
								if(data != "0" && data != ""){
									$('.modal-body').text("");
									$('#messageMyModalLabel').text("Success");
									$('.modal-body').text("Product detailed saved successfully !!!");
									$('#messagemyModal').modal();
									clearMainDetail();
									clearRecurrenceDetail();
									lineItemTable.fnClearTable();
								}
							},
							error:function(){

							}
						});
			        }
        		}
        	}
        }
    });

    $("#searchCustomerId").click(function(){
		$("#txtCustomerIdError").text("");
		$("#txtCustomerId").removeClass("errorStyle");
        $('#searchModalBody').load('views/admin/customer.html',function(){

        });
		$('#mySearchModal').modal();
    });

    $("#addProductDetails").on('click',function(){
    	var currentData = lineItemTable.fnAddData(['','<select class ="selProduct" onchange="productCode($(this));"></select>','<input type="number" style="width:100%" class="txtQuantity">','','','<input type="number" style="width:100%" class="txtDiscount">','','<select class ="selGLAccount" ></select>']);

    	var getCurrDataRow = lineItemTable.fnSettings().aoData[ currentData ].nTr;
    	//bindUnitMeasure($(getCurrDataRow).find('td:eq(2)').children()[0]);
    	getProductDetails($(getCurrDataRow).find('td:eq(1)').children()[0]);
        bindGLAccount($(getCurrDataRow).find('td:last').children()[0],'')
    });

    $("#btnCustomerReset").click(function(){
    	var tab = $(".customerBtn").parent().find('.activeSelf').val();
    	if(tab == 'Main'){
			clearMainDetail();			
    	}
    	else if(tab == 'Line Items'){
    		lineItemTable.fnClearTable();
    	}
    	else{
    		clearRecurrenceDetail();
    	}
    });
});
function bindRecurrencePeriod(){
	var postData = {
		"operation" : "showRecurrencePeriod"
	}
	dataCall("controllers/admin/customer_invoice.php", postData, function (result){
		var parseData = JSON.parse(result);
		if (parseData != '') {
			var option = "<option value=''>--Select--</option>";
			$.each(parseData,function(i,v){
				option += "<option value='" + v.id + "'>" + v.recurrence_period + "</option>";
			});
			$("#selReccurencePeriod").html(option);
		}
	});
}
/*function bindUnitMeasure(textField){
    $.ajax({                    
        type: "POST",
        cache: false,
        url: "controllers/admin/drugs.php",
        data: {
            "operation":"showUnitMeasure"
        },
        success: function(data) {   
            if(data != null && data != ""){
                var parseData= jQuery.parseJSON(data);
            
                var option ="<option value='-1'>-- Select --</option>";
                for (var i=0;i<parseData.length;i++)
                {
                    option+="<option value='"+parseData[i].id+"'>"+parseData[i].unit_abbr+"</option>";
                }
                $(textField).html(option); 
            }
        },
        error:function(){
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();           
        }
    });
}*/
function bindARAccount(){
    $.ajax({                    
        type: "POST",
        cache: false,
        url: "controllers/admin/customer_invoice.php",
        data: {
            "operation":"showARAccount"
        },
        success: function(data) {   
            if(data != null && data != ""){
                var parseData= jQuery.parseJSON(data);
            
                var option ="<option value='-1'>-- Select --</option>";
                for (var i=0;i<parseData.length;i++)
                {
                    option+="<option value='"+parseData[i].id+"'>"+parseData[i].account_name+"</option>";
                }
                $("#selARAccount").html(option); 
            }
        },
        error:function(){
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();           
        }
    });
}
function getProductDetails(textField){
	var postData = {
		operation : "productDetails"
	}

	dataCall("controllers/admin/customer_invoice.php", postData, function (result){
		var parseData = JSON.parse(result);
		var option = "<option value=''>--Select--</option>";
		if (parseData != '') {		
			for (var i=0;i<parseData.length;i++){

				var codeLength = 6-parseData[i].id.length;
            	var code = parseData[i].id;
            	for(j=0; j<codeLength; j++){
            		code = "0"+code;
            	}
            	code =parseData[i].product_prefix+code;

				option += "<option value='" + parseData[i].id + "' cost='" + parseData[i].cost_price + "' tax='" + parseData[i].tax + "' product-code='"+code+"'>"  + parseData[i].name + "</option>";
			}
			$(textField).html(option);
		}
		else{
			$(textField).html(option);
		}
	});
}
function productCode(myThis){
	var prdtCode = $(myThis).find('option:selected').attr('product-code');
	var productCost = $(myThis).find('option:selected').attr('cost');
	var productColumn = $(myThis).parent().siblings()[0];
	$($(myThis).parent().siblings()[2]).text(productCost);
	$($(myThis).parent().siblings()[5]).text(productCost);
    $($(myThis).parent().siblings()[1]).find('input').val('');
    $($(myThis).parent().siblings()[4]).find('input').val('');
	if (prdtCode != undefined) {
		$(productColumn).text(prdtCode);
	}
	else{
		$(productColumn).text('');
	}	
	calculateTotal();
}
function clearRecurrenceDetail(){
	$("#selReccurencePeriod").val("Does Not Recur");
	$("#txtNextDate").val("");
	$("#txtEndDate").val("");
	$("#txtEndDateError").text("");
	$("#txtNextDateError").text("");
	$("#txtNextDate").removeClass("errorStyle");
	$("#txtEndDate").removeClass("errorStyle");
}

function clearMainDetail(){
	$("#txtBillingDate").val("");
	$("#txtCustomerId").val("");
	$("#txtDueDate").val("");
	$("#selARAccount").val("-1");
	$("#salesRepresentative").val("");
	$("#txtCustomerIdError").text("");
	$("#txtBillingDateError").text("");
	$("#txtDueDateError").text("");
	$("#selARAccountError").text("");
	$("#txtCurrencyError").text("");
	$("#lblFirstName").text("");
	$("#lblLastName").text("");
    $("#lblEmail").text("");
	$("#totalBill").text("");
	$("#txtBillingDate").removeClass("errorStyle");
	$("#txtCustomerId").removeClass("errorStyle");
	$("#txtDueDate").removeClass("errorStyle");
	$("#selARAccount").removeClass("errorStyle");
	$("#selARAccount").removeClass("errorStyle");
	$("#txtCurrency").removeClass("errorStyle");
}

function calculateTotal(){
	var total =0;
	var aiRows = lineItemTable.fnGetNodes();
	for(i=0; i<aiRows.length; i++){
		total += parseFloat($(aiRows[i]).find('td:eq(6)').text());
	}	
	$("#totalBill").text(total);
}