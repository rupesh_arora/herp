var ledgerTable;
$(document).ready(function(){
    loader();
    debugger;
	
	/*Hide add ward by default functionality*/
	$("#advanced-wizard").hide();
    $("#ledgerList").addClass('list');    
    $("#tabledgerList").addClass('tab-list-add');
	

    /*$("#ledgerTable").dataTable();*/		
	/*Click for add the ledger*/
    $("#tabAddledger").click(function() {
       clear();
       showAddTab();
    });
	
	//Click function for show the ledger lists
    $("#tabledgerList").click(function() {
        clear();
        tabledgerList();
    });

    $('#txtName').keyup(function() {
         if($('#txtName').val() != '') {
            $('#txtNameError').text('');
            $('#txtName').removeClass("errorStyle"); 
        }
    });


    $('#btnSubmit').click(function() {
        var flag = false;

        if($('#txtName').val() == '') {
            $('#txtNameError').text('Please enter name');
            $('#txtName').focus();
            $('#txtName').addClass("errorStyle"); 
            flag = true;
        }
        if(flag == true) {
            return false;
        }

        var name = $('#txtName').val().trim();
        var description = $('#txtDescription').val().trim();

        var postData = {
            name : name,
            description : description,
            operation : "saveLedger"
        }


        $.ajax(
            {                   
            type: "POST",
            cache: false,
            url: "controllers/admin/ledger_finance.php",
            datatype:"json",
            data: postData,
            
            success: function(data) {
                if(data != "0" && data != ""){
                    callSuccessPopUp('Success','Saved successfully!!!');
                     tabledgerList();
                    clear();
                }
                else {
                    $('#txtNameError').text('Ledger name is already exist');
                    $('#txtName').focus();
                    $('#txtName').addClass('errorStyle');
                }
            },
            error: function(){

            }
        });
    });



    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
    //Datatable code 
    ledgerTable = $('#ledgerTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "autoWidth": false,
        "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = ledgerTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var name = mData["name"];                      
                        var description = mData["desciption"];
                        editClick(id,name,description);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = ledgerTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/ledger_finance.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "name"
                }, {
                    "mData": "desciption"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [1,2]
            }]

        });
    }

    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            ledgerTable.fnClearTable();
            ledgerTable.fnDestroy();
            ledgerTable = "";
            ledgerTable = $('#ledgerTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "autoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = ledgerTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var name = mData["name"];                      
                            var description = mData["desciption"];
                            restoreClick(id,name,description);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/ledger_finance.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "name"
                    }, {
                        "mData": "desciption"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1,2]
                }]
            });
        } else { // show active data on unchecked   
            ledgerTable.fnClearTable();
            ledgerTable.fnDestroy();
            ledgerTable = "";
            ledgerTable = $('#ledgerTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "autoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = ledgerTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var name = mData["name"];                      
                            var description = mData["desciption"];
                            editClick(id,name,description);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = ledgerTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/ledger_finance.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "name"
                    }, {
                        "mData": "desciption"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1,2]
                }]
            });
        }
    });


    $('#btnReset').click(function(){
        clear();
    });
});

function editClick(id,name,description) {
    
    showAddTab()  
    $("#btnReset").hide();
    $("#btnSubmit").hide();
    $('#btnUpdate').show();
    $('#tabAddledger').html("+Update Ledger");
    $('#txtName').focus();
    $("#txtName").removeClass("errorStyle");
    $("#txtNameError").text("");

   
    $('#txtName').val(name);
    $('#txtDescription').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
    //validation
    
    $("#btnUpdate").click(function() { // click update button
        var flag = false;
        if ($("#txtName").val()== '') {
            $("#txtNameError").text("Please enter name");
            $("#txtName").focus();
            $("#txtName").addClass("errorStyle");
            flag = true;
        }
        
        if(flag == true) {
            return false;
        }

        var name = $("#txtName").val().trim();
        var description = $("#txtDescription").val().trim();
        description = description.replace(/'/g, "&#39");
        var id = $('#selectedRow').val();
        
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
        var postData = {
            "operation": "update",
            "name": name,
            "description": description,
            "id": id
        }
        $.ajax( //ajax call for update data
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/ledger_finance.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('#myModal').modal('hide');
                        $('.close-confirm').click();
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Ledger updated successfully!!!");
                        $('#messagemyModal').modal();
                        ledgerTable.fnReloadAjax();
                        tabledgerList();
                        clear();
                    }
                   else {                
                        $("#txtNameError").text("Ledger name is already exist");
                        $("#txtName").focus();               
                        $("#txtName").addClass('errorStyle');
                    }
                },
                error: function() {
                    $('.close-confirm').click();
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                    $('#messagemyModal').modal();
                }
            }); // end of ajax
        });
    });
} // end update button
function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Ledger");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteLedger("' + type + '","'+id+'","","");');
} // end click fucntion

function restoreClick(id,name,description) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Ledger");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteLedger("' + type + '","'+id+'","'+name+'","'+description+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteLedger(type,id,name,description) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/ledger_finance.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Ledger deleted successfully!!!");
                    $('#messagemyModal').modal();
                    ledgerTable.fnReloadAjax();
                } 
                else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This Ledger is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/ledger_finance.php",
            data: {
                "operation": "restore",
                "name":name,
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Ledger restored successfully!!!");
                    $('#messagemyModal').modal();
                    ledgerTable.fnReloadAjax();
                }
                else {
                    callSuccessPopUp('Sorry','Ledger name already exist you can not restore !!!');
                }
            },
            error: function() {             
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary unavailable to respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}


function tabledgerList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
   
    $("#tabAddledger").removeClass('tab-detail-add');
    $("#tabAddledger").addClass('tab-detail-remove');
    $("#tabledgerList").removeClass('tab-list-remove');    
    $("#tabledgerList").addClass('tab-list-add');
    $("#ledgerList").addClass('list');
   
    $("#btnReset").show();
    $("#btnSubmit").show();
    $('#btnUpdate').hide();
    $('#tabAddledger').html("+Add Ledger");
    $('#inactive-checkbox-tick').prop('checked', false).change();
    clear();
}
function showAddTab(){
    $("#advanced-wizard").show();
    $(".blackborder").hide();

    $("#tabAddledger").addClass('tab-detail-add');
    $("#tabAddledger").removeClass('tab-detail-remove');
    $("#tabledgerList").removeClass('tab-list-add');
    $("#tabledgerList").addClass('tab-list-remove');
    $("#ledgerList").addClass('list');
    $('#txtName').focus();
}


function clear() {
    $('#txtName').val('');
    $('#txtNameError').text('');
    $('#txtDescription').val('')
    $('#txtName').removeClass("errorStyle");
    $('#txtName').focus();
}