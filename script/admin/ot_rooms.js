var otRoomsTable;
$(document).ready(function(){
	debugger;
	$("#formOtRooms").hide();
    $("#divOtRooms").addClass('list');    
    $("#tabOtRoomsList").addClass('tab-list-add');
    
    $("#tabOtRooms").click(function() { // show the add allergies categories tab
        showAddTab();
        clearFormDetails(".clearForm");
    });

    $("#tabOtRoomsList").click(function() { // show allergies list tab
        showTableList();
    });

    removeErrorMessage();// remove error messaages

    $("#btnSave").click(function(){
    	var flag = false;

        if(validTextField('#txtOtRooms','#txtOtRoomsError','Please enter OT room') == true)
        {
            flag = true;
        }
        if(flag == true) {
			return false;
		}

		var roomNo = $("#txtOtRooms").val().trim();
		var desc = $("#txtDesc").val().trim();
        desc = desc.replace(/'/g, "&#39");

		var postData = {
			roomNo : roomNo,
			desc : desc,
			"operation" : "save"
		}

		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/ot_rooms.php",
			datatype: "json",
			data: postData,
			 success: function(data){
                if(data !='' && data!=null && data !='0'){
                    callSuccessPopUp("Success","Saved  successfully!!!");                    
                    showTableList();
                    $("#txtOtRooms").focus();
                }
                else if (data == "0") {
                    $("#txtOtRooms").focus();
                    $("#txtOtRooms").addClass("errorStyle");
                    $("#txtOtRoomsError").text("This room alreday exist.");                    
                }
            },
            error:function(){

            }
        });

    });

    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code
        otRoomsTable = $('#otRoomsTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = otRoomsTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var roomNo = mData["room_no"];
                        var desc = mData["description"];
                        editClick(id,roomNo,desc);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = otRoomsTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/ot_rooms.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "room_no"
                }, {
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [1,2]
            }, {
                'bSortable': false,
                'aTargets': [1,2]
            }]

        });
    }
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            otRoomsTable.fnClearTable();
            otRoomsTable.fnDestroy();
            otRoomsTable = "";
            otRoomsTable = $('#otRoomsTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = otRoomsTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var room_no = mData['room_no'];
                            restoreClick(id,room_no);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/ot_rooms.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    {
                    "mData": "room_no"
                }, {
                    "mData": "description"
                }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1,2]
                }, {
                    'bSortable': false,
                    'aTargets': [1,2]
                }]
            });
        } else { // show active data on unchecked   
            otRoomsTable.fnClearTable();
            otRoomsTable.fnDestroy();
            otRoomsTable = "";
            otRoomsTable = $('#otRoomsTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = otRoomsTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                        var id = mData["id"];
                        var roomNo = mData["room_no"];
                        var desc = mData["description"];
                        editClick(id, roomNo,desc);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = otRoomsTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/ot_rooms.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "room_no"
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1,2]
                }]
            });
        }
    });

    $("#btnReset").click(function() {
       clearFormDetails(".clearForm");
       removeErrorMessage();
       $("#txtOtRooms").focus();
    });

});
 function editClick(id,roomNo,desc) {
     showAddTab();
    $('#tabOtRooms').html("+Update Ot Rooms");
    $("#btnReset").hide();
    $("#btnSave").hide(); 

    
    $("#btnUpdate").removeAttr("style");
    
    $("#txtOtRooms").removeClass("errorStyle");
    $("#txtOtRoomsError").text("");
    
    
    $('#txtOtRooms').val(roomNo);
    $('#txtDesc').val(desc.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
    //validation
    
    $("#btnUpdate").click(function() { // click update button
        var flag = "false";
        if ($("#txtOtRooms").val().trim()== '') {
            $("#txtOtRoomsError").text("Please enter value");
            $("#txtOtRooms").focus();
            $("#txtOtRooms").addClass("errorStyle");
            flag = "true";
        }
        
        if(flag == "true") {
            return false;
        }
        
        
        var roomNo = $("#txtOtRooms").val().trim();
        var desc = $("#txtDesc").val().trim();
        desc = desc.replace(/'/g, "&#39");
        
        
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
        var postData = {
            "operation": "update",
            "roomNo": roomNo,
            "desc": desc,
            "id": id
        }
        $.ajax( //ajax call for update data
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/ot_rooms.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data == "1") {
                        callSuccessPopUp("Success","OT Rooms updated Successfully!!!");
                        $('#myModal .close').click();
                        showTableList();
                    }
                    else{
                        callSuccessPopUp("Success","This room alreday exist.");
                    }
                },
                error: function() {
                    callSuccessPopUp("Error","Temporary Unavailable to Respond.Try again later!!!");
                }
            }); // end of ajax
        });
    });
} // end update button
function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete OT Rooms");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    var roomNumber = '';
    $('#confirm').attr('onclick', 'deleteOtRooms("'+type+'","'+roomNumber+'");');
} // end click fucntion

function restoreClick(id,room_no) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore OT Rooms");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    var roomNumber = room_no;
    $('#confirm').attr('onclick', 'deleteOtRooms("'+type+'","'+roomNumber+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteOtRooms(type,roomNumber) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/ot_rooms.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    callSuccessPopUp("Success","OT Rooms deleted Successfully!!!");
                    otRoomsTable.fnReloadAjax();
                } else {
                    callSuccessPopUp("Sorry","This OT Rooms is used , so you can not delete!!!");
                }
            },
            error: function() {
                callSuccessPopUp("Error","Temporary Unavailable to Respond.Try again later!!!");
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/ot_rooms.php",
            data: {
                "operation": "restore",
                "id": id,
                "roomNumber" : roomNumber
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    callSuccessPopUp("Success","OT Rooms restored Successfully!!!");
                    otRoomsTable.fnReloadAjax();
                }
                else{
                    callSuccessPopUp('Alert','This room already exist. So you can\'t restore it.!!!');
                }
            },
            error: function() {
                callSuccessPopUp("Error","Temporary Unavailable to Respond.Try again later!!!"); 
            }
        });
    }
}
function showTableList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#formOtRooms").hide();
    $(".blackborder").show();

    $("#tabOtRoomsList").addClass('tab-detail-add');
    $("#tabOtRoomsList").removeClass('tab-detail-remove');
    $("#tabOtRooms").removeClass('tab-list-add');
    $("#tabOtRooms").addClass('tab-list-remove');
    $("#divOtRooms").addClass('list');    

    $("#btnReset").show();
    $("#btnSave").show();
    $('#btnUpdate').hide();
    $('#tabOtRooms').html("+Add OT Room");
    clearFormDetails(".clearForm");
}

function showAddTab(){
    $("#formOtRooms").show();
    $(".blackborder").hide();
   
    $("#tabOtRoomsList").removeClass('tab-detail-add');
    $("#tabOtRoomsList").addClass('tab-detail-remove');
    $("#tabOtRooms").removeClass('tab-list-remove');    
    $("#tabOtRooms").addClass('tab-list-add');
    $("#divOtRooms").addClass('list');
    $("#txtOtRooms").focus();   
    removeErrorMessage();
} 
