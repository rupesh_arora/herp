var selfRequestOtable ;//global variable selfRequestOtable
var revisedCost;
var paymentModeValue;
var insuranceNumberValue;
var paymentMode = 'cash';
var hrnPatientid ='';
$(document).ready(function() {
	loader();
	debugger;
	var patientMiddleName;

	$(".amountPrefix").text(amountPrefix);//for amount prefix

	/*reset button functionality*/
	$("#btnResetDetails").click(function(){
		clear();
		$('#txtPatientId').focus();
	});

	/**DataTable Initialization**/
	selfRequestOtable = $('#viewPrice').DataTable({
		aLengthMenu: [
	        [25, 50, 100, 200, -1],
	        [25, 50, 100, 200, "All"]
	    ],//declare so that show all rows
	    iDisplayLength: -1,
		aoColumnDefs: [
		   { aTargets: [ 3,5,6 ], bSortable: false },
		]
	});
	
	/**Binding data in drop downs**/
	bindServiceType();
	bindLabType();
	bindRadiologyTest();
	bindForensicTest();
	bindProcedures();
	bindDrug();

	$('#txtPatientId').focus();//focus on patient id
	
	// bind nsurance company
    
    // bind company schemeon change company
    $("#selInsuranceCompany").change(function() {
        var option = "<option value='-1'>--Select--</option>";
        if ($("#selInsuranceCompany :selected") != "") {
            var value = $("#selInsuranceCompany :selected").val();            
           
            bindRequestSchemeName(value,hrnPatientid);            
        } 
        else {
            $("#selInsuranceCompany").html(option);
        }   
    });
    
    // bind company schemeon change company
    $("#selInsurancePlan").change(function() {
        var option = "<option value='-1'>--Select--</option>";
        
        if ($("#selInsurancePlan :selected") != "") {
            var value = $("#selInsurancePlan :selected").val();
            
            //on change call this function for load state
            bindRequestPlanName(value,hrnPatientid); 
        } else {
            $("#selInsuranceScheme").html(option);
        }   

        if ($("#selInsurancePlan").val() != "") {
            $("#selInsurancePlanError").text("");
            $("#selInsurancePlan").removeClass("errorStyle");
        }
    });
	
	$("#selInsuranceScheme").change(function() {
        if ($("#selInsuranceScheme").val() != "-1") {
            $("#selInsuranceSchemeError").text("");
            $("#selInsuranceScheme").removeClass("errorStyle");
            bindInsuranceNumber($("#selInsuranceCompany :selected").val(),$("#selInsurancePlan :selected").val(),$("#selInsuranceScheme :selected").val(),hrnPatientid);
        }
    });

	
	/*Binding Service Type*/
	function bindServiceType(){
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/self_request.php",
			data: {
			"operation":"showServiceType"
			},
			success: function(data) { 
				if(data != null && data != ""){
					var parseData= jQuery.parseJSON(data); // parse the value in Array string  jquery

					var option ="<option value='-1'>--Select--</option>";
					for (var i=0;i<parseData.length;i++)
					{
						option+="<option value='"+parseData[i].id+"'>"+parseData[i].type+"</option>";
					}
					$('#selServiceType').html(option);          
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}

	/*Binding Services*/
	function bindServices(value){
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/self_request.php",
			data: {
			"operation":"showServices",
			"value" : value
			},
			success: function(data) { 
				if(data != null && data != ""){

					var parseData= jQuery.parseJSON(data); // parse the value in Array string  jquery

					var option ="<option value='-1'>--Select--</option>";
					for (var i=0;i<parseData.length;i++){
						option+="<option value='"+parseData[i].id+"' data-cost='"+parseData[i].cost+"' data-name='"+parseData[i].name+"'>"+parseData[i].name+"</option>";
					}
					$('#selServices').html(option);          
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}

	/*Binding Lab Type*/
	function bindLabType(){
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/self_request.php",
			data: {
			"operation":"showLabType"
			},
			success: function(data) { 
				if(data != null && data != ""){
					var parseData= jQuery.parseJSON(data); // parse the value in Array string  jquery

					var option ="<option value='-1'>--Select--</option>";
					for (var i=0;i<parseData.length;i++)
					{
						option+="<option value='"+parseData[i].id+"'>"+parseData[i].type+"</option>";
					}
					$('#selLabType').html(option);          
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}

	/*Binidng Lab Test*/
	function bindLabTest(value){
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/self_request.php",
			data: {
			"operation":"showLabTest",
			"value" : value
			},
			success: function(data) { 
				if(data != null && data != ""){

					var parseData= jQuery.parseJSON(data); // parse the value in Array string  jquery

					var option ="<option value='-1'>--Select--</option>";
					for (var i=0;i<parseData.length;i++)
					{
						option+="<option value='"+parseData[i].id+"' data-name='"+parseData[i].name+"'>"+parseData[i].name+"</option>";
					}
					$('#selLabTest').html(option);          
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}

	/*Binidng Lab Test*/
	function bindLabTestComponent(value){
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/self_request.php",
			data: {
			"operation":"showLabTestComponent",
			"labTestId" : value
			},
			success: function(data) { 
				if(data != null && data != ""){

					var parseData= jQuery.parseJSON(data); // parse the value in Array string  jquery

					var option ="<option value='-1'>--Select--</option>";
					for (var i=0;i<parseData.length;i++)
					{
						option+="<option value='"+parseData[i].id+"' data-cost='"+parseData[i].fee+"' lab_test_id='"+parseData[i].lab_test_id+"' data-name='"+parseData[i].name+"'>"+parseData[i].name+"</option>";
					}
					$('#selLabTestComponent').html(option);          
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}
	
	/*Binding Radiology Test*/
	function bindRadiologyTest(){
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/self_request.php",
			data: {
			"operation":"showRadiologyTest"
			},
			success: function(data) { 
				if(data != null && data != ""){
					var parseData= jQuery.parseJSON(data); // parse the value in Array string  jquery

					var option ="<option value=''>--Select--</option>";
					for (var i=0;i<parseData.length;i++)
					{
						option+="<option value='"+parseData[i].id+"' data-cost='"+parseData[i].fee+"' data-name='"+parseData[i].name+"'>"+parseData[i].name+"</option>";
					}
					$('#selRadiologyTest').html(option);      
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}
	
	/*Binding Forensic Test*/
	function bindForensicTest(value){
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/self_request.php",
			data: {
			"operation":"showForensicTest"
			},
			success: function(data) { 
				if(data != null && data != ""){
					var parseData= jQuery.parseJSON(data); // parse the value in Array string  jquery

					var option ="<option value=''>--Select--</option>";
					for (var i=0;i<parseData.length;i++)
					{
						option+="<option value='"+parseData[i].id+"' data-cost='"+parseData[i].fee+"' data-name='"+parseData[i].name+"'>"+parseData[i].name+"</option>";
					}
					$('#selForensicTest').html(option);     
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}
	
	/*Bind Procedures method*/
	function bindProcedures(){
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/self_request.php",
			data: {
			"operation":"showProcedure"
			},
			success: function(data) { 
				if(data != null && data != ""){
					var parseData= jQuery.parseJSON(data); // parse the value in Array string  jquery

					var option ="<option value=''>--Select--</option>";
					for (var i=0;i<parseData.length;i++)
					{
						option+="<option value='"+parseData[i].id+"' data-cost='"+parseData[i].price+"' data-name='"+parseData[i].name+"'>"+parseData[i].name+"</option>";
					}
					$('#selProcedure').html(option);          
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}	

	/*Binding Drug */
	function bindDrug(){
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/self_request.php",
			data: {
			"operation":"showDrug"
			},
			success: function(data) { 
				if(data != null && data != ""){
					var parseData= jQuery.parseJSON(data); // parse the value in Array string  jquery

					var option ="<option value=''>--Select--</option>";
					for (var i=0;i<parseData.length;i++)
					{
						option+="<option value='"+parseData[i].id+"' data-cost='"+parseData[i].price+"' data-name='"+parseData[i].name+"'>"+parseData[i].name+"</option>";
					}
					$('#selDrugs').html(option);      
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}
	
	/*On changing the service type calling the bindServices function*/ 
	$("#selServiceType").change(function(){
		var option = "<option value='-1'>Select</option>";
		if($("#selServiceType :selected") != -1){
			var value = $("#selServiceType :selected").val();
		}
		else{
			$("#selServices").html(option);
		}
		bindServices(value);//on change call this function
	});
	
	/*call revised cost on change.*/
	$("#selServices").change(function() {
		var paymentMode = $('#selPaymentMode :selected').val();	
		if(paymentMode == "Insurance") {
			var companyId =  $('#selInsuranceCompany :selected').val();
			var schemeId =  $('#selInsurancePlan :selected').val();
			var planId =  $('#selInsuranceScheme :selected').val();
			var type = "Service";
			if($("#selServices :selected") != -1){
				var value = $("#selServices :selected").val();
			}
			getRevisedCost(value,companyId,schemeId,planId,type);//on change call this function
		}
		else{
		}
	});
	
	
	/*call revised cost on change.*/
	$("#selLabTestComponent").change(function() {
		var paymentMode = $('#selPaymentMode :selected').val();	
		if(paymentMode == "Insurance") {
			var companyId =  $('#selInsuranceCompany :selected').val();
			var schemeId =  $('#selInsurancePlan :selected').val();
			var planId =  $('#selInsuranceScheme :selected').val();
			var type = "Lab";
			if($("#selLabTestComponent :selected") != -1){
				var value = $("#selLabTestComponent :selected").val();
			}
			getRevisedCost(value,companyId,schemeId,planId,type);//on change call this function
		}
		else{
		}
	});
	
	/*call revised cost on change.*/
	$("#selRadiologyTest").change(function() {
		var paymentMode = $('#selPaymentMode :selected').val();	
		if(paymentMode == "Insurance") {
			var companyId =  $('#selInsuranceCompany :selected').val();
			var schemeId =  $('#selInsurancePlan :selected').val();
			var planId =  $('#selInsuranceScheme :selected').val();
			var type = "Radiology";
			if($("#selRadiologyTest :selected") != -1){
				var value = $("#selRadiologyTest :selected").val();
			}
			getRevisedCost(value,companyId,schemeId,planId,type);//on change call this function
		}
		else{
		}
	});
	
	/*call revised cost on change.*/
	$("#selForensicTest").change(function() {
		var paymentMode = $('#selPaymentMode :selected').val();	
		if(paymentMode == "Insurance") {
			var companyId =  $('#selInsuranceCompany :selected').val();
			var schemeId =  $('#selInsurancePlan :selected').val();
			var planId =  $('#selInsuranceScheme :selected').val();
			var type = "Forensic";
			if($("#selForensicTest :selected") != -1){
				var value = $("#selForensicTest :selected").val();
			}
			getRevisedCost(value,companyId,schemeId,planId,type);//on change call this function
		}
		else{
		}
	});
	
	/*call revised cost on change.*/
	$("#selProcedure").change(function() {
		var paymentMode = $('#selPaymentMode :selected').val();	
		if(paymentMode == "Insurance") {
			var companyId =  $('#selInsuranceCompany :selected').val();
			var schemeId =  $('#selInsurancePlan :selected').val();
			var planId =  $('#selInsuranceScheme :selected').val();
			var type = "Procedure";
			if($("#selProcedure :selected") != -1){
				var value = $("#selProcedure :selected").val();
			}
			getRevisedCost(value,companyId,schemeId,planId,type);//on change call this function
		}
		else{
		}
	});
	
	/*call revised cost on change.*/
	$("#selDrugs").change(function() {
		var paymentMode = $('#selPaymentMode :selected').val();	
		if(paymentMode == "Insurance") {
			var companyId =  $('#selInsuranceCompany :selected').val();
			var schemeId =  $('#selInsurancePlan :selected').val();
			var planId =  $('#selInsuranceScheme :selected').val();
			var type = "Pharmacy";
			if($("#selDrugs :selected") != -1){
				var value = $("#selDrugs :selected").val();
			}
			getRevisedCost(value,companyId,schemeId,planId,type);//on change call this function
		}
		else{
		}
	});

	/*On changing the lab type calling the bindLabTest function*/
	$("#selLabType").change(function(){
		var option = "<option value='-1'>Select</option>";
		if($("#selLabType :selected") != -1){
			var value = $("#selLabType :selected").val();
		}
		else{
			$("#selLabTest").html(option);
		}
		bindLabTest(value);//on change of lab type call this function
	});

	// hide insurance div
	$(".insurance").hide();
	
	/*On changing the lab type calling the bindLabTest function*/
	$("#selLabTest").change(function(){
		var option = "<option value='-1'>Select</option>";
		if($("#selLabTest :selected") != -1){
			var value = $("#selLabTest :selected").val();
		}
		else{
			$("#selLabTestComponent").html(option);
		}
		bindLabTestComponent(value);//on change of lab type call this function
	});

	
	/*Function to add to bill*/
	function addToBill(elemId,type,quantity,alertMsg,revisedCost,paymentType){
		
		var cost = $("#"+elemId+" option:selected").attr('data-cost');//getting cost of selected element
		var id = $("#"+elemId).val();
		var name = $("#"+elemId+" option:selected").attr('data-name');
		if(alertMsg == "lab"){
			var labTestId = $("#"+elemId+" option:selected").attr('lab_test_id');
		}
		
		var totalPrice = cost * quantity;	
		var totalRevisedCost = revisedCost * quantity;
		if(revisedCost == "") {
			totalRevisedCost = totalPrice;
		}
		var aiRows = selfRequestOtable.fnGetNodes();
		var previousCount = 0;	
		var arrTests = [];//creating array

		/*Check data already exist in datatable i.e remove duplicasy*/
		if($(aiRows).length > 0) {
			arrTests = $(aiRows).find('td:eq(1)').map(function(index, td) {
		        return $(this).parent().attr('data-id') + "_" + $(this).text();
		    });
		}

		var testType = id + "_" + name;

		/*Compare data exist already*/
	    if($.inArray(testType, arrTests) != -1)
	    {
			/*$('#mySelfRequestErrorModal').modal();
			$('#selfRequestErrorModalLabel').text("Error");*/
			$("#testStatus").text('');
			$('#addedStatus').text("This data already exist!!!");
			return true;
	    }
		/*Add new row in datatable*/
		if(alertMsg == "lab"){
			var newRow = selfRequestOtable.dataTable().fnAddData([type,name,cost,quantity,totalPrice,totalRevisedCost,"<input type='button' class='lab' onclick='rowDelete($(this));' lab-id='"+labTestId+"' value='Remove'>",id,paymentType]);
		}
		else{
			var newRow = selfRequestOtable.dataTable().fnAddData([type,name,cost,quantity,totalPrice,totalRevisedCost,"<input type='button' onclick='rowDelete($(this));' value='Remove'>",id,paymentType]);
		}
		FadeOut();
		if (alertMsg=="service") {
			$("#testStatus").text("Service added successfully!!!");
		}
		else if(alertMsg=="lab") {
			$("#testStatus").text("Lab test added successfully!!!");
		}
		else if(alertMsg=="radiology") {
			$("#testStatus").text("Radiology test added successfully!!!");
		}
		else if(alertMsg=="forensic") {
			$("#testStatus").text("Forensic test added successfully!!!");
		}
		else if(alertMsg=="procedure") {
			$("#testStatus").text("Procedure added successfully!!!");
		}
		else {
			$("#testStatus").text("Drug added successfully!!!");
		}

		//change cell attributes (add data attribute id with datatable so to comapre data alreday exist)
		var oSettings = selfRequestOtable.fnSettings();
		var nTr = oSettings.aoData[ newRow[0] ].nTr;
		$('td', nTr).parent().attr( 'data-id', id );

		$('#totalBill').text(parseInt($('#totalBill').text())+parseInt(totalPrice));//getting total bill
		$('#totalCost').text(parseInt($('#totalCost').text())+parseInt(totalRevisedCost));//getting total bill
	}
	
	/**Add click events**/
	$("#addServices").click(function(){
		/*Apply validation on this click event*/
		var flag = false;
		if($("#selServices").val() == "" || $("#selServices").val() == "-1"){
			$("#selServices").focus();
			$("#selServicesError").text("Please choose service");
			flag = true;
		}
		if($('#selPaymentMode').val() == "Insurance" && $("#selServiceType").val() == "-1"){
			$("#selServiceType").focus();
			$("#selServiceTypeError").text("Please choose service type");
			flag = true;
		}
		
		if($('#selPaymentMode').val() == "Insurance" && $("#selInsuranceCompany").val() == "-1"){
			$("#selInsuranceCompany").focus();
			$("#selInsuranceCompany").addClass('errorStyle');
			$("#selInsuranceCompanyError").text("Please select insurance company name");
			flag = true;
		}
		if($('#selPaymentMode').val() == "Insurance" && $("#selInsurancePlan").val() == "-1"){
			$("#selInsurancePlan").focus();
			$("#selInsurancePlan").addClass('errorStyle');
			$("#selInsurancePlanError").text("Please select insurance plan name");
			flag = true;
		}
		if($('#selPaymentMode').val() == "Insurance" && $("#selInsuranceScheme").val() == "-1"){
			$("#selInsuranceScheme").focus();
			$("#selInsuranceScheme").addClass('errorStyle');
			$("#selInsuranceSchemeError").text("Please select insurance scheme name");
			flag = true;
		}
		if($('#selPaymentMode').val() == "Insurance" && $("#selInsuranceNo").val() == "-1"){
			$("#selInsuranceNo").focus();
			$("#selInsuranceNo").addClass('errorStyle');
			$("#selInsuranceNoError").text("Please select insurance number");
			flag = true;
		}
		if (flag == true) {
			return false;
		}//end validation
		if($("#selPaymentMode :selected").val() == "Insurance") {
			var paymentType = paymentMode;
		}
		else{
			var paymentType = "cash";
			revisedCost=$("#selServices :selected").attr('data-cost');
		}

		if($("#selServices").val() != ""){
			addToBill("selServices","Services",1,"service",revisedCost,paymentType);//call function with two parameters so that we add data to table
			FadeOut();
		}
		else{
			$("#selServices").css();
		}
	});//end click function


	$("#addLabService").click(function(){
		/*Apply validation on this click event*/
		var flag = false;
		if($("#selLabType").val() == "-1"){
			$("#selLabType").focus();
			$("#selLabTypeError").text("Please choose lab type");
			flag = true;
		}
		if($("#selLabTest").val() == "" || $("#selLabTest").val() == "-1"){
			$("#selLabTest").focus();
			$("#selLabTestError").text("Please choose lab test");
			flag = true;
		}
		if($("#selLabTestComponent").val() == "" || $("#selLabTestComponent").val() == "-1"){
			$("#selLabTestComponent").focus();
			$("#selLabTestComponentError").text("Please choose test component");
			flag = true;
		}
		if (flag == true) {
			return false;
		}//end validation
		if($("#selPaymentMode :selected").val() == "Insurance") {
			var paymentType = paymentMode;
		}
		else{
			var paymentType = "cash";
			revisedCost=$("#selLabTestComponent :selected").attr('data-cost');
		}
		addToBill("selLabTestComponent","Lab Test",1,"lab",revisedCost,paymentType);
		FadeOut();
	});//end click function

	$("#addRadiologyService").click(function(){

		/*Apply validation on this click event*/
		var flag = false;
		if($("#selRadiologyTest").val() == ""){
			$("#selRadiologyTest").focus();
			$("#selRadiologyTestError").text("Please choose radiology test");
			flag = true;
		}
		if (flag == true) {
			return false;
		}//end validation
		if($("#selPaymentMode :selected").val() == "Insurance") {
			var paymentType = paymentMode;
		}
		else{
			var paymentType = "cash";
			revisedCost=$("#selRadiologyTest :selected").attr('data-cost');
		}
		addToBill("selRadiologyTest","Radiology Test",1,"radiology",revisedCost,paymentType);
		FadeOut();
	});//end click function

	$("#addForensicService").click(function(){

		/*Apply validation on this click event*/
		var flag = false;
		if($("#selForensicTest").val() == ""){
			$("#selForensicTest").focus();
			$("#selForensicTestError").text("Please choose forensic test");
			flag = true;
		}
		if (flag == true) {
			return false;
		}//end validation
		if($("#selPaymentMode :selected").val() == "Insurance") {
			var paymentType = paymentMode;
		}
		else{
			var paymentType = "cash";
			revisedCost=$("#selForensicTest :selected").attr('data-cost');
		}
		addToBill("selForensicTest","Forensic Test",1,"forensic",revisedCost,paymentType);
		FadeOut();
	});//end click function

	$("#addProcedure").click(function(){

		/*Apply validation on this click event*/
		var flag = false;
		if($("#selProcedure").val() == ""){
			$("#selProcedure").focus();
			$("#selProcedureError").text("Please choose procedure test");
			flag = true;
		}
		if (flag == true) {
			return false;
		}//end validation
		if($("#selPaymentMode :selected").val() == "Insurance") {
			var paymentType = paymentMode;
		}
		else{
			var paymentType = "cash";
			revisedCost=$("#selProcedure :selected").attr('data-cost');
		}
		addToBill("selProcedure","Procedure",1,"procedure",revisedCost,paymentType);
		FadeOut();
	});//end click function

	$("#addDrugs").click(function(){

		/*Apply validation on this click event*/
		var flag = false;
		if (!numberValidation($("#txtDrugQuantity").val())) {
			$('#txtDrugQuantity').focus();
            $("#txtDrugQuantityError").text("Please enter only number");
            $('#txtDrugQuantity').addClass("errorStyle");
          flag=true;  
		}
		if($("#txtDrugQuantity").val() == ""){
			$("#txtDrugQuantity").focus();
			$("#txtDrugQuantityError").text("Please enter drug quantity");
			flag = true;
		}
		if($("#selDrugs").val() == ""){
			$("#selDrugs").focus();
			$("#selDrugsError").text("Please choose drug");
			flag = true;
		}		
		if (flag == true) {
			return false;
		}//end validation
		if($("#selPaymentMode :selected").val() == "Insurance") {
			var paymentType = paymentMode;
		}
		else{
			var paymentType = "cash";
			revisedCost=$("#selDrugs :selected").attr('data-cost');
		}
		var drugQuantity = $('#txtDrugQuantity').val(); 
		addToBill("selDrugs","Drug",drugQuantity,"drug",revisedCost,paymentType);
		FadeOut();
	});//end click function

	$("#selInsuranceCompany").change(function() {
		if($("#selInsuranceCompany").val() != '-1') {
			$("#selInsuranceCompanyError").text('');
			$("#selInsuranceCompany").removeClass('errorStyle');
		}
	});

	$(".slfBtn").on("click",function(){
		var flag = false;
		var paymentMode = $("#selPaymentMode :selected").val();
		if(paymentMode == "Insurance") {
			if($("#selInsuranceNo").val() == '') {
				$("#selInsuranceNoError").text("Please enter insurance number");
				$("#selInsuranceNo").focus();
				$("#selInsuranceNo").addClass("errorStyle");       
				flag = true;
			}
			if($("#selInsuranceScheme :selected").val() == '-1') {
				$("#selInsuranceSchemeError").text("Please select plan name");
				$("#selInsuranceScheme").focus();
				$("#selInsuranceScheme").addClass("errorStyle");       
				flag = true;
			}
			if($("#selInsurancePlan :selected").val() == '-1') {
				$("#selInsurancePlanError").text("Please select scheme name");
				$("#selInsurancePlan").focus();
				$("#selInsurancePlan").addClass("errorStyle");       
				flag = true;
			}
			if($("#selInsuranceCompany :selected").val() == '-1') {
				$("#selInsuranceCompanyError").text("Please select company name");
				$("#selInsuranceCompany").focus();
				$("#selInsuranceCompany").addClass("errorStyle");       
				flag = true;
			}
			if(flag == true){
				return false;
			}
		}
		
		$(".slfBtn").removeClass("activeSelf");
		$(this)	.addClass("activeSelf");

		$('.myHideClass').hide();

		$("#testStatus").text('');
		$('#addedStatus').text('');

		var clickedId = $(this).attr("id");

		if (clickedId == "addServicePopUp") {
			$('.myServiceClass').show();
		}
		else if(clickedId == "addLabPopUp"){
			$('.myLabClass').show();			
		}
		else if(clickedId == "addRadiologyPopUp"){
			$('.myRadiologyClass').show();
		}
		else if(clickedId == "addForensicPopUp"){
			$('.myForensicClass').show();
		}
		else if(clickedId == "addProcedurePopUp"){
			$('.myProcedureClass').show();
		}
		else{
			$('.myDrugsClass').show();
		}		
	});

	/*Load patient details*/
	$("#btnLoadPatientId").click(function(){

		var flag= false;
		$("#selPaymentMode").val("-1");
		if($("#txtPatientId").val() ==""){
			$("#txtPatientIdError").text("Please enter patient id");
			$("#txtPatientId").focus();
			$("#txtPatientId").addClass("errorStyle");       
            flag = true;
		}
		if($("#txtPatientId").val().length != 9 ){
			$("#txtPatientIdError").text("Please enter patient id");
			$("#txtPatientId").focus();
			$("#txtPatientId").addClass("errorStyle");       
            flag = true;
		}
		

		if (flag ==true) {
			return false;
		}
		if ($("#txtPatientId").val !="") {
			$("#txtPatientId").removeClass("errorStyle");
			$("#txtPatientIdError").text("");
		}
		var patientId =  $("#txtPatientId").val();
		var patientPrefix = patientId.substring(0, 3);
		patientId = patientId.replace ( /[^\d.]/g, '' ); 
		patientid = parseInt(patientId);
		hrnPatientid = patientid;

		var postData = {
			"operation":"patientDetail",
			"patientId":patientid,
			"patientPrefix":patientPrefix
		}	
		/*AJAX call to get patient details*/
		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/self_request.php",
			datatype:"json",
			data: postData,
			success: function(data) {
				if(data != "0" && data != ""){
					var parseData = jQuery.parseJSON(data);//parse JSON data 
					$("#txtPatientFirstName").val(parseData.first_name);//setting in to the text box
					$("#txtPatientLastName").val(parseData.last_name);
					$("#txtPatientMobile").val(parseData.mobile);
					$("#txtPatientEmail").val(parseData.email);
					$("#thing").val(parseData.salutation);
					bindInsuranceCompany(patientid);//function to get insurance number
					patientMiddleName = parseData.middle_name;
					$("#txtPatientId").attr("disabled", "disabled"); 
					$(".showOnClickLoad").css({"display":"block"});
				}
				if (data == "0") {
					$('#messagemyModal').modal("show");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Patient id not exist");
					clear();
				}
				if (data == "") {
					$("#txtPatientIdError").text("Please enter patient id");
					$("#txtPatientId").focus();
					$("#txtPatientId").addClass("errorStyle");  
				}
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	});

	$("#selPaymentMode").change(function() {
		$("#addServicePopUp").click();
		$("#btnOk").unbind();
		selfRequestOtable.fnClearTable();
        if ($("#selPaymentMode").val() != '-1') {
            $("#selPaymentModeError").text("");
            $("#selPaymentMode").removeClass("errorStyle");			
							
            if($("#selPaymentMode").val() == 'Insurance'){
				if ($("#txtPatientId").val().trim() == "") {
					$("#txtPatientId").focus();
					$("#txtPatientIdError").text("Please enter patient Id");
					$("#txtPatientId").addClass("errorStyle");
					$("#selInsuranceNo").html("<option value=''>-- Select --</option>");
					return false;
				}
		
				paymentModeValue = $("#selPaymentMode").val();
							
				if($("#selPaymentMode").val() == "Insurance"){
					$(".insurance").show();
					$("#selInsuranceNo").html("<option value=''>-- Select --</option>");
					$("#selInsuranceCompany").text('');
					$("#selInsuranceCompany").removeClass('errorStyle');

					$("#selInsurancePlan").text('');
					$("#selInsurancePlan").removeClass('errorStyle');

					$("#selInsuranceScheme").text('');
					$("#selInsuranceScheme").removeClass('errorStyle');
					$("#selInsuranceNo").text('');
					$("#selInsuranceNo").removeClass('errorStyle');
					$("#addServicePopUp").click();
				}
				else{
					$("#addServicePopUp").click();
					$(".insurance").hide();
					$("#selInsuranceCompany").val("-1");
                    $("#selInsurancePlan").val("-1");
                    $("#selInsuranceScheme").val("-1");
                    $("#selInsuranceNo").html("<option value=''>-- Select --</option>"); 
				}
                var patientId = $("#txtPatientId").val();
                var hrn = parseInt($("#txtPatientId").val().replace ( /[^\d.]/g, '' ));

                var postData = {
                    "operation": "insuranceDetails",
                    "hrn": hrn
                }

                $.ajax({
                    type: "POST",
                    cache: false,
                    url: "controllers/admin/opd_registration.php",
                    datatype: "json",
                    data: postData,

                    success: function(data) {
                        var parseData = jQuery.parseJSON(data);
                         if (parseData != "0" && parseData != "") {     
							insuranceNumberValue = parseData[0].insurance_number;
							var hrn = parseInt($("#txtPatientId").val().replace ( /[^\d.]/g, '' ));
							bindInsuranceCompany(hrn);        
                        }
                        else{                        
                            $('#confirmInsuranceModalLabel').text("Alert");
                            $('#confirmInsuranceBody').text("Insurance detail is not present.Do you want to continue?");
                            $('#confirmInsuranceModal').modal();

                            $("#btnOk").click(function(){
                                $('#insuranceModalLabel').text("Insurance");
                                $("#hdnInsurance").val("1");
                                $("#hdnPatientId").val($("#txtPatientId").val());
                                $('#insuranceBody').load('views/admin/modal_insurance.html');
                                $('#insuranceModal').modal();
                            });

                            $("#cancel").click(function(){
                                $(".insurance").hide();
                                $("#selPaymentMode").val("-1");
                            });
                        }
                    },
                    error: function(){

                    }
                });
            }
            else{
	            $(".insurance").hide();
	            $("#selInsuranceCompany").val("-1");
	            $("#selInsurancePlan").val("-1");
	            $("#selInsuranceScheme").val("-1");
	            $("#selInsuranceNo").html("<option value=''>-- Select --</option>");
	        }           
        }
        else{
            $(".insurance").hide();
            $("#selInsuranceCompany").val("-1");
            $("#selInsurancePlan").val("-1");
            $("#selInsuranceScheme").val("-1");
            $("#selInsuranceNo").html("<option value=''>-- Select --</option>");
        }
    });
	
	/*Save data to database*/
	$("#btnSave").click(function(){

		/*perform validation*/
		var flag=false;
		if (selfRequestOtable.fnGetData().length < 1) {
			$('#messagemyModal').modal("show");
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Choose any of the one services");
			$("#selServiceType").focus();
			flag=true;
		}
		if($("#selPaymentMode :selected").val() =="-1"){
			$("#selPaymentModeError").text("Please select payment mode");
			$("#selPaymentMode").focus();
			$("#selPaymentMode").addClass("errorStyle");       
            flag = true;
		}
		if($("#txtPatientId").val() ==""){
			$("#txtPatientIdError").text("Please enter patient id");
			$("#txtPatientId").focus();
			$("#txtPatientId").addClass("errorStyle");       
            flag = true;
		}
		if($("#txtPatientFirstName").val() ==""){
			$("#txtPatientIdError").text("Please load patient id");
			$("#txtPatientId").focus();
			$("#txtPatientId").addClass("errorStyle");       
            flag = true;
		}	
		
		if (flag==true) {
			return false;
		}

		var insuranceTotal = 0;

		/*Defining array outside loop so to hold multiple values*/
		var Price_f = new Array(); //small array contain forenscic test price and id
		var bigPrice_f =new Array(); // big 2d array conatin small array

		var Price_p = new Array();
		var bigPrice_p = new Array();

		var Price_s = new Array();
		var bigPrice_s = new Array();

		var Price_l = new Array();
		var Price = new Array();
		var bigPrice_l = new Array();

		var Price_r = new Array();
		var bigPrice_r = new Array();

		var Price_d = new Array();
		var bigPrice_d = new Array();

		var tempArray =  new Array();

		var tempId = "0";
		var componentId = "0";
		var tempTotal = 0;
		var isFirst = "1";
		var j = 1;

		var aiRows = selfRequestOtable.fnGetNodes();
		var labLength = $(aiRows).find('td:last').find('.lab').length;
		
		for (i=0,c=aiRows.length; i<c; i++) {
			iRow = aiRows[i];   // assign current row to iRow variable
			aData = selfRequestOtable.fnGetData(iRow); // Pull the row
			 
			// Getting the first column then compare result and then store i9n diffrent database table
			sType = aData[0];

			/*If column type is equals to service then fetch all service type data*/
			if((sType == "Services")){
				/*Putting array value into a variable*/
				if(aData[8] == "insurance"){
					insuranceTotal = insuranceTotal+aData[5];
				}
				
				Price_s = new Array();//so that hold only one value after traverse
				/*At six get id , at four get total price and at one get name of test or drug*/
				Price_s.push(aData[7],aData[5],aData[1],aData[8]);
				bigPrice_s.push(Price_s);
			}
			if((sType == "Lab Test")){

				if(aData[8] == "insurance"){
					insuranceTotal = insuranceTotal+aData[5];
				}
				
				/*Putting array value into a variable*/
				var labId = $(aData[6]).attr('lab-id');
				if(tempId == "0"){
					var Obj = {};
					Obj.labId = labId;
					Obj.componentId = aData[7];
					Obj.componentName = aData[1];
					Obj.paymentType = aData[8];
					Obj.labBill = aData[5];
					tempTotal = parseInt(aData[5]);
					tempArray.push(Obj);
				}
				if(tempId != "0"){
					if(tempId == labId){
						var Obj = {};
						Obj.labId = tempId;
						Obj.componentId = aData[7];
						Obj.componentName = aData[1];
						Obj.paymentType = aData[8];
						Obj.labBill = aData[5];
						tempTotal = tempTotal + parseInt(aData[5]);							
						tempArray.push(Obj);
					}
					else{
						//Price_l = new Array();//so that hold only one value after traverse
						//Price_l.push(tempArray);

						bigPrice_l.push(tempArray);
						var newObj = {};
						newObj.labId = tempId;
						newObj.total = tempTotal;
						Price_l.push(newObj);
						Price.push(Price_l);
						tempArray =  new Array();
						Price_l = new Array()
						tempId = labId;
						var Obj = {};
						Obj.labId = tempId;
						Obj.componentId = aData[7];
						Obj.componentName = aData[1];
						Obj.paymentType = aData[8];
						Obj.labBill = aData[5];
						tempTotal = parseInt(aData[5]);
						tempArray.push(Obj);
					}
				}
				else{
					tempId = labId;
				}
				if(labLength == j){
					
					//Price_l.push(tempTotal,componentId,tempId);
					var newObj = {};
					if(tempId == "0"){
						newObj.labId = labId;
					}	
					else{
						newObj.labId = tempId;
					}
					newObj.total = tempTotal;
					Price_l.push(newObj);
					Price.push(Price_l);

					bigPrice_l.push(tempArray);
					Price_l = new Array();//so that hold only one value after traverse
					
				}
				j++;				
			}
			if((sType == "Radiology Test")){

				if(aData[8] == "insurance"){
					insuranceTotal = insuranceTotal+aData[5];
				}
				/*Putting array value into a variable*/
				Price_r = new Array();//so that hold only one value after traverse
				Price_r.push(aData[7],aData[5],aData[1],aData[8]);
				bigPrice_r.push(Price_r);
			}
			if((sType == "Forensic Test")){

				if(aData[8] == "insurance"){
					insuranceTotal = insuranceTotal+aData[5];
				}

				/*Putting array value into a variable*/
				Price_f = new Array(); //so that hold only one value after traverse
			    Price_f.push(aData[7],aData[5],aData[1],aData[8]);
			    bigPrice_f.push(Price_f);
			}
			if((sType == "Procedure")){

				if(aData[8] == "insurance"){
					insuranceTotal = insuranceTotal+aData[5];
				}

				/*Putting array value into a variable*/
				Price_p = new Array();//so that hold only one value after traverse
				Price_p.push(aData[7],aData[5],aData[1],aData[8]);
				bigPrice_p.push(Price_p);
			}
			if((sType == "Drug")){

				if(aData[8] == "insurance"){
					insuranceTotal = insuranceTotal+aData[5];
				}

				/*Putting array value into a variable*/
				Price_d = new Array();//so that hold only one value after traverse
				Price_d.push(aData[7],aData[5],aData[1],aData[3],aData[8]);//at 3 we take quantity
				bigPrice_d.push(Price_d);
			}
		}
	
		var patientId =  $("#txtPatientId").val();
		var patientPrefix = patientId.substring(0, 3);
		patientId = patientId.replace ( /[^\d.]/g, '' ); 
		patientid = parseInt(patientId);
		var remarks   = $("#txtRemarks").val();
		//var paymentMode = $("#selPaymentMode :selected").val();
		if($("#selPaymentMode :selected").val() == "Cash"){
			var insuranceNumber = '';
			paymentModeValue = $("#selPaymentMode :selected").val();
		}
		else{
			var insuranceNumber = $("#selInsuranceNo  :selected").text();
		}
		
		var postData ={
			"operation":"saveDataTableData",				
			"bigPrice_p":JSON.stringify(bigPrice_p),
			"bigPrice_r":JSON.stringify(bigPrice_r),
			"bigPrice_l":JSON.stringify(bigPrice_l),
			"bigPrice_f" : JSON.stringify(bigPrice_f),
			"bigPrice_s":JSON.stringify(bigPrice_s),
			"bigPrice_d":JSON.stringify(bigPrice_d),
			"Price":JSON.stringify(Price),
			"patientId" : patientid,
			"prefix":patientPrefix,
			"paymentMode":paymentModeValue,
			"insuranceNumber":insuranceNumber,
			"insuranceTotal":insuranceTotal,
			"remarks"   : remarks
		}

        $('#confirmMyModalLabel').text("Save Request");
        $('.selfRequestConfirm-body').text("Are you sure that you want to save this?");
        $('#confirmmyModal').modal();
        $('#confirm').unbind();
        $('#confirm').on('click',function(){
		/*AJAX call to save data*/	
			$.ajax({
				type: "POST",
				cache: false,
				url: "controllers/admin/self_request.php",
				datatype:"json",
				data: postData,
				success : function(data){
					if(data !="" && data != "0" && data != "check insuranc number"){
						var patientID =  $("#txtPatientId").val();
						var salu = $("#thing").val();
						var f_name = $("#txtPatientFirstName").val();
						var l_name = $("#txtPatientLastName").val();
						$('#mySelfModal').modal("show");
						$('#selfModalLabel').text("Success");
						$("#success").text("Self request saved successfully!!!");
						$("#mySelfModal #lblPatientId").text(patientID);
						var forName = "";
						var serName = "";
						var radName = "";
						var labName = "";
						var phaName = "";
						var testName = "";
						var drugName = "";
						if(bigPrice_f.length != 0 )
						{	
							for(var j = 0 ;j < bigPrice_f.length ; j++){
							   forName = forName + bigPrice_f[j][2] + ','+ "\n";						
							}
							testName = testName + forName + "\n";					
						}
						if(bigPrice_p.length != 0 )
						{	
							for(j = 0 ;j < bigPrice_p.length ; j++){
							   phaName = phaName + bigPrice_p[j][2] + ','+ "\n";						  
							}
							testName = testName + phaName + "\n";					
						}
						if(bigPrice_s.length != 0 )
						{	
							for(j = 0 ;j < bigPrice_s.length ; j++){
							   serName = serName + bigPrice_s[j][2] + ','+ "\n";						   
							}
							testName = testName + serName + "\n";						
						}
						if(bigPrice_l.length != 0 )
						{	
							for(j = 0 ;j < bigPrice_l.length ; j++){
								$(bigPrice_l[j]).each(function(){
									labName = labName + this.componentName + ','+ "\n";
								});							   						  
							}
							testName = testName + labName + "\n";						
						}
						if(bigPrice_r.length != 0 )
						{	
							for(j = 0 ;j < bigPrice_r.length ; j++){
							   radName = radName + bigPrice_r[j][2] + ','+ "\n";						 
							}
							testName = testName + radName + "\n";						
						} 
						if(bigPrice_d.length != 0 )
						{	
							for(j = 0 ;j < bigPrice_d.length ; j++){
							   drugName = drugName + bigPrice_d[j][2] + ','+ "\n";						 
							}						
						} 

						/*Remove last comma*/
						testName = testName.replace(/,\s*$/, ".");						
						$("#lblTestName").text(testName);

						if (drugName!='') {
							drugName = drugName.replace(/,\s*$/, "."); 
							$("#lblDrugName").text(drugName);
						}
						else{
							$("#lblDrugName").text("N/A");
						}
							
						var parseData = jQuery.parseJSON(data);
						var visitId = parseData[0];
						var patientPrefix = parseData[1];
							
						var visitIdLength = visitId.toString().length;
						for (var j=0;j<6-visitIdLength;j++) {
							visitId = "0"+visitId;
						}
						visitid = patientPrefix+visitId;
						$("#lblVisitId").text(visitid);
						$("#lblName").text(salu +" "+f_name+" "+patientMiddleName+" "+l_name);
					
						clear();
					}
					if(data ==""){
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Something awful happened!! Can't fetch old requests. Please try to contact admin.");
						$('#messagemyModal').modal();
					}
					if(data =="0"){
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Please enter valid patient id");
						$('#messagemyModal').modal();
						clear();
					}
					if(data == "check insuranc number"){
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Please check insurance number");
						$('#messagemyModal').modal();
					}
				},
				error:function() {
					$('#messagemyModal').modal();
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				}
			});
		});
	});	

	/*Key up functionality*/	
	$("#txtPatientId").keyup(function(){
		if ($("#txtPatientId").val() !="") {
			$("#txtPatientIdError").text("");
			$("#txtPatientId").removeClass("errorStyle");			
		}
	});
	$("#selInsuranceNo").change(function(){
		if ($("#selInsuranceNo").val() !="-1") {
			$("#selInsuranceNoError").text("");
			$("#selInsuranceNo").removeClass("errorStyle");			
		}
	});
	$("#txtDrugQuantity").keyup(function(){
		if ($("#txtDrugQuantity").val() !="") {
			$("#txtDrugQuantityError").text("");
			$("#txtDrugQuantity").removeClass("errorStyle");			
		}
		else{
			$("#txtDrugQuantityError").text("Please enter drug quantity");
			$("#txtDrugQuantity").addClass("errorStyle");
		}
	});	
	$("#selProcedure").change(function(){
		if ($("#selProcedure").val() != "") {
			$("#selProcedureError").text("");
			$("#selProcedure").removeClass("errorStyle");
		}	
	});
	$("#selPaymentMode").change(function(){
		if ($("#selPaymentMode").val() != "") {
			$("#selPaymentModeError").text("");
			$("#selPaymentMode").removeClass("errorStyle");
		}	
	});
	$("#selForensicTest").change(function(){
		if ($("#selForensicTest").val() != "") {
			$("#selForensicTestError").text("");
			$("#selForensicTest").removeClass("errorStyle");
		}	
	});
	$("#selRadiologyTest").change(function(){
		if ($("#selRadiologyTest").val() != "") {
			$("#selRadiologyTestError").text("");
			$("#selRadiologyTest").removeClass("errorStyle");
		}	
	});
	$("#selRadiologyTest").change(function(){
		if ($("#selRadiologyTest").val() != "") {
			$("#selRadiologyTestError").text("");
			$("#selRadiologyTest").removeClass("errorStyle");
		}	
	});
	$("#selLabTest").change(function(){
		if ($("#selLabTest").val() != "") {
			$("#selLabTestError").text("");
			$("#selLabTest").removeClass("errorStyle");
		}	
	});
	$("#selLabType").change(function(){
		if ($("#selLabType").val() != "") {
			$("#selLabTypeError").text("");
			$("#selLabType").removeClass("errorStyle");
		}	
	});
	$("#selServiceType").change(function(){
		if ($("#selServiceType").val() != "") {
			$("#selServiceTypeError").text("");
			$("#selServiceType").removeClass("errorStyle");
		}	
	});
	$("#selServices").change(function(){
		if ($("#selServices").val() != "") {
			$("#selServicesError").text("");
			$("#selServices").removeClass("errorStyle");
		}	
	});
	$("#selLabTestComponent").change(function(){
		if ($("#selLabTestComponent").val() != "") {
			$("#selLabTestComponentError").text("");
			$("#selLabTestComponent").removeClass("errorStyle");
		}	
	});
	$("#selDrugs").change(function(){
		if ($("#selDrugs").val() != "") {
			$("#selDrugsError").text("");
			$("#selDrugs").removeClass("errorStyle");
		}
		$("#txtDrugQuantity").val("");	
	});
	//End key up functionality	
	
	$("#searchPatientIcon").click(function() {			// show popup for search patient
		
        $('#selfRequestModalLabel').text("Search Patient");
		$("#hdnSelfRequest").val("1");
        $('#selfRequestModalBody').load('views/admin/view_patients.html');
        $('#mySelfRequestModal').modal();
    });
	
	$('select ').change(function(){
		$('#testStatus').text('');
		$('#addedStatus').text('');
	});

	$('#txtPatientId').keypress(function (e) {
	    if (e.which == 13) {
		    $("#btnLoadPatientId").click();
    	}
 	});
});//end document ready

/**Row Delete functionality**/
function rowDelete(currInst) {
	var row = currInst.closest("tr").get(0);
	var oTable = $('#viewPrice').dataTable();
	oTable.fnDeleteRow(oTable.fnGetPosition(row));
	$('#totalBill').text(parseInt($('#totalBill').text())-parseInt($(row).find('td:eq(4)').text()));//subtract bill from total amount
	$('#totalCost').text(parseInt($('#totalCost').text())-parseInt($(row).find('td:eq(5)').text()));//subtract bill from total amount
}
function numberValidation(number) {
    floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
    return floatRegex.test(number);
}
function clear(){
	$("#selPaymentMode").val("-1");
	$("#selInsuranceCompany").val("-1");
	$("#selInsurancePlan").val("-1");
	$("#selInsuranceScheme").val("-1");
	$("#selInsuranceNo").val("");
	 $(".insurance").hide();
	$("#selPaymentMode").removeClass("errorStyle");
	$("#selInsuranceCompany").removeClass("errorStyle");
	$("#selInsurancePlan").removeClass("errorStyle");
	$("#selInsuranceScheme").removeClass("errorStyle");
	$("#selInsuranceNo").removeClass("errorStyle");
	
	$("#txtPatientFirstName").val("");
	$("#txtPatientMobile").val("");
	$("#txtPatientEmail").val("");
	$("#txtPatientLastName").val("");
	$("#selServiceType").val("-1");
	$("#selLabType").val("-1");
	$("#selRadiologyTest").val("");
	$("#selForensicTest").val("");
	$("#selProcedure").val("");
	$("#selServices").val("");
	$("#selLabTestComponent").val("-1");
	$("#selLabTest").val("");
	$("#txtPatientId").val("");
	$("#selDrugs").val("");
	$("#txtDrugQuantity").val("");

	$("#selPaymentModeError").text("");
	$("#selInsuranceCompanyError").text("");
	$("#selInsurancePlanError").text("");
	$("#selInsuranceSchemeError").text("");
	$("#selInsuranceNoError").text("");
	
	$("#txtPatientIdError").text("");
	$("#selProcedureError").text("");
	$("#selForensicTestError").text("");
	$("#selRadiologyTestError").text("");
	$("#selLabTestComponentError").text("");
	$("#selLabTypeError").text("");
	$("#selLabTestError").text("");
	$("#selServiceTypeError").text("");
	$("#selServicesError").text("");
	$("#selDrugsError").text("");
	$("#txtDrugQuantityError").text("");

	selfRequestOtable.fnClearTable();
	$("#txtPatientId").removeAttr("disabled", "disabled");
	$("#txtPatientId").removeClass("errorStyle");
	$("#totalBill").text("0");
	$("#totalCost").text("0");
}
function FadeOut(){
    setTimeout(function() {
        $('#addedStatus').text('');
        $('#testStatus').text('');
    }, 3000);
}

//call function for revised cost
function getRevisedCost(value,companyId,schemeId,planId,type){
    var postData = {
        "operation": "revisedCostDetail",
        "insuranceCompanyId": companyId,
        "insurancePlanId" : schemeId,
        "insurancePlanNameid" : planId,
        "componentId" : value,
        "activeTab" : type
    }
    dataCall("controllers/admin/self_request.php", postData, function (result){
		if (result != null && result != "" && result != "0") {                
			var parseData = JSON.parse(result);
			if (result.length > 2) {
				revisedCost = parseData[0].revised_cost;
				paymentMode = 'insurance';
			}
			else{
				revisedCost ='';
				paymentMode = 'insurance';
			}
		}			
		if(result == '0'){ 
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("This service is not include in your insurance. Do you want to continue?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				paymentMode = 'cash';
				//$("#selPaymentMode").val("Cash");
				revisedCost ='';
			});
			$("#cancel").unbind();
			$("#cancel").click(function(){ 
				revisedCost ='';
			}); 			
		}
    });
}
function bindInsuranceNumber(insuranceCompany,insuranceScheme,insuranceSchemePlan,patientId){
    var postData = {
        "operation": "showInsuranceNumber",
        "insuranceCompany" : insuranceCompany,
        "insuranceScheme" : insuranceScheme,
        "insuranceSchemePlan" : insuranceSchemePlan,
        "patientId" : patientId
    }
    dataCall("controllers/admin/opd_registration.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (result.length > 2) { 

            var parseData = jQuery.parseJSON(result);
            var option = ''; // parse the value in Array string jquery
            for (var i = 0; i < parseData.length; i++) {
                option += "<option value='" + parseData[i].insurance_number + "'>" + parseData[i].insurance_number + "</option>";
            }
            $('#selInsuranceNo').html(option);
        }
    });
}
// call function for bind data
function bindInsuranceCompany(patientId) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/insurance_plan.php",
        data: {
            "operation": "showCompany",
            "patientId" : patientId
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);
                var option = '<option value="-1">--Select--</option>'; // parse the value in Array string jquery
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selInsuranceCompany').html(option);
            }
        },
        error: function() {}
    });
}

// call function for bind scheme name
function bindRequestSchemeName(insuranceCompany,patientId) {
     $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/scheme_exclusion.php",
        data: {
            "operation": "showSchemeName",
            insuranceCompany: insuranceCompany,
            patientId : patientId
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = '<option value="-1">--Select--</option>';
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].plan_name + "</option>";
                }
                $('#selInsurancePlan').html(option);
                /*if(scheme != null){
                    $('#selInsurancePlan').val(scheme);
                }*/

            }
        },
        error: function() {}
    });
}

// call function for bind scheme name
function bindRequestPlanName(insuranceScheme,patientId) {
     $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/scheme_exclusion.php",
        data: {
            "operation": "showPlanName",
            insuranceScheme: insuranceScheme,
            "patientId" : patientId
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);
                var option = '<option value="-1">--Select--</option>'; // parse the value in Array string jquery
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].scheme_plan_name + "</option>";
                }
                $('#selInsuranceScheme').html(option);
                /*if(plan != null){
                    $('#selInsuranceScheme').val(plan);
                }*/
            }
        },
        error: function() {}
    });
}
/*src url = self_request.js*/