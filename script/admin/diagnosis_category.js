$(document).ready(function() {
var otable; //defile variable for otable 
	
loader();
	$('#btnReset').click(function(){
		clear();
        $("#txtCategories").removeClass("errorStyle");
		$("#txtCategories").focus();
	});
// show the add diagnosis categories
    $("#form_diseases").hide();
	$("#diagnosisList").css({
            "background": "#fff"
        });
		$("#tabDiagnosis").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
    $("#tabAddDiagnosis").click(function() {
        $("#form_diseases").show();
        $(".blackborder").hide();
        $("#tabAddDiagnosis").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tabDiagnosis").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#diagnosisList").css({
            "background-color": "#fff"
        });
		$("#txtCategoryError").text("");
        $("#txtCategories").removeClass("errorStyle");
		$("#txtCategories").focus();
    });
// show the add category list
    $("#tabDiagnosis").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
        $("#form_diseases").hide();
        $(".blackborder").show();
        $("#tabAddDiagnosis").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#tabDiagnosis").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#diagnosisList").css({
            "background": "#fff"
        });
		$("#diagnosisCategoryDataTable").dataTable().fnReloadAjax(); 
	});
// check the validation
    $("#btnAddCategory").click(function() {
		
		var flag = "false";
        if ($("#txtCategories").val().trim() == '') {
            $("#txtCategoryError").text("Please enter diagnosis category");
			$("#txtCategories").focus();
            $("#txtCategories").addClass("errorStyle");
            flag = "true";
        }
		else
		{
			var diagnosisCategory = $("#txtCategories").val().trim();
			var description = $("#txtDesc").val();
			description = description.replace(/'/g, "&#39");
			$.ajax({									// ajax call for categories validation
				type: "POST",
				cache: false,
				url: "controllers/admin/diagnosis_category.php",
				datatype:"json",  
				data:{
				
					diagnosisCategory:diagnosisCategory,
					Id:"",
					operation:"checkdiseasescategory"
				},
				success:function(data) {
					if(data == "1")
					{
						$("#txtCategoryError").show();
						$("#txtCategoryError").text("This category is already exist");
						$("#txtCategories").addClass("errorStyle");
						$("#txtCategories").focus();
					}
					else{
						//ajax call for insert data into data base
						var postData = {
							"operation":"save",
							"diagnosisCategory":diagnosisCategory,
							"description":description
						}
						$.ajax(
							{					
							type: "POST",
							cache: false,
							url: "controllers/admin/diagnosis_category.php",
							datatype:"json",
							data: postData,

							success: function(data) {
								if(data != "0" && data != ""){
									$('.modal-body').text("");
									$('#messageMyModalLabel').text("Success");
									$('.modal-body').text("Diagnosis category saved successfully!!!");
									$('#messagemyModal').modal();
									$('#inactive-checkbox-tick').prop('checked', false).change();
									$("#diagnosisCategoryDataTable").dataTable().fnReloadAjax();
									clear();

									//after sucessful data sent to database redirect to datatable list
									$("#form_diseases").hide();
        							$(".blackborder").show();
				       				$("#tabAddDiagnosis").css({
							            "background": "#fff",
							            "color": "#000"
							        });
							        $("#tabDiagnosis").css({
							            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
							            "color": "#fff"
							        });
				        			$("#diagnosisCategoryDataTable").dataTable().fnReloadAjax();

								}
								
							},
							error:function() {
								alert('error');
							}
						});//  end ajax call
					}
				},
				error:function(){
					alert ("data not found");
				}
			});
		}
   }); //end button click function

//validation
    $("#txtCategories").keyup(function() {
        if ($("#txtCategories").val() != '') {
            $("#txtCategoryError").text("");
            $("#txtCategories").removeClass("errorStyle");
        } 
    }); 
	  if($('.inactive-checkbox').not(':checked')){
		//Datatable code
		 otable = $('#diagnosisCategoryDataTable').dataTable( {
			"bFilter": true,
			"processing": true,
			"sPaginationType":"full_numbers",
			"fnDrawCallback": function ( oSettings ) {
				// Need to redo the counters if filtered or sorted /
				/* if ( oSettings.bSorted || oSettings.bFiltered )
				{
					for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
					{
						$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
					}
				} */
				$('.update').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData = $('#diagnosisCategoryDataTable').dataTable().fnGetData(data);
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						var category = mData["category"];
						var description = mData["description"];
						editClick(id,category,description);
       
					}
				});
				$('.delete').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData =  $('#diagnosisCategoryDataTable').dataTable().fnGetData(data);
					
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						deleteClick(id);
					}
				});
			},
			"sAjaxSource":"controllers/admin/diagnosis_category.php",
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "operation", "value": "show" });
			},
			"aoColumns": [
				/* {  "sWidth": "3%","mData": "id" }, */
				{  "sWidth": "7%","mData": "category" },
				{ "sWidth": "15%", "mData": "description" },	
				{  "sWidth": "3%",
					"mData": function (o) { 
					var data = o;
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
					   " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
					   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
					   " style='font-size: 22px; color:#a94442; cursor:pointer;' "+
					   " data-original-title='Delete'></i>"; 
					}
				},
				],
			aoColumnDefs: [
				{ 'bSortable': false, 'aTargets': [ 1 ] },
				{ 'bSortable': false, 'aTargets': [ 2 ] }
			]

		} );
	 }
	$('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	otable.fnClearTable();
	    	otable.fnDestroy();
	    	otable="";
	    	otable = $('#diagnosisCategoryDataTable').dataTable( {
				"bFilter": true,
				"processing": true,
				 "deferLoading": 57,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					// Need to redo the counters if filtered or sorted /
					/* if ( oSettings.bSorted || oSettings.bFiltered )
					{
						for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
						{
							$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
						}
					} */
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  $('#diagnosisCategoryDataTable').dataTable().fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/diagnosis_category.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "checked" });
				},
				"aoColumns": [
					/* {  "sWidth": "3%","mData": "id" }, */
					{  "sWidth": "7%","mData": "category" },
					{ "sWidth": "15%", "mData": "description" },	
					{  "sWidth": "3%",
						"mData": function (o) { 
						var data = o;
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},
				],
			aoColumnDefs: [
				{ 'bSortable': false, 'aTargets': [ 1 ] },
				{ 'bSortable': false, 'aTargets': [ 2 ] }
			]
			});
		}
		else{
			otable.fnClearTable();
	    	otable.fnDestroy();
	    	otable="";
			otable = $('#diagnosisCategoryDataTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					// Need to redo the counters if filtered or sorted /
					/* if ( oSettings.bSorted || oSettings.bFiltered )
					{
						for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
						{
							$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
						}
					} */
					$('.update').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData = $('#diagnosisCategoryDataTable').dataTable().fnGetData(data);
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var category = mData["category"];
							var description = mData["description"];
							editClick(id,category,description);
		   
						}
					});
					$('.delete').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  $('#diagnosisCategoryDataTable').dataTable().fnGetData(data);
						
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							deleteClick(id);
						}
					});
				},
				
				"sAjaxSource":"controllers/admin/diagnosis_category.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "show" });
				},
				"aoColumns": [
					/* {  "sWidth": "3%","mData": "id" }, */
					{  "sWidth": "7%","mData": "category" },
					{ "sWidth": "15%", "mData": "description" },	
					{  "sWidth": "3%",
						"mData": function (o) { 
						var data = o;
						return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
						   " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
						   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
						   " style='font-size: 22px; color:#a94442; cursor:pointer;' "+
						   " data-original-title='Delete'></i>"; 
						}
					},
					],
				aoColumnDefs: [
					{ 'bSortable': false, 'aTargets': [ 1 ] },
					{ 'bSortable': false, 'aTargets': [ 2 ] }
				]
			});
		}
    });
	
});
// edit data dunction for update 
function editClick(id,category,description){	
	$('.modal-body').text("");
	$('#myModalLabel').text("Update Dignosis Category");
	$('.modal-body').html($("#diagnosispopUPForm").html()).show();
    $('#myModal').modal('show');
	$('#myModal').on('shown.bs.modal', function() {
        $("#myModal #txtCategories").focus();
    });
	$("#myModal #btnUpdate").removeAttr("style");
	$("#myModal #txtCategories").removeClass("errorStyle");
	$("#myModal #txtCategoryError").text("");
	$('#myModal #txtCategories').val(category);
	$('#myModal #txtDesc').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id); 
	    //validation
     $("#myModal #txtCategories").keyup(function() {
        if ($("#myModal #txtCategories").val() != '') {
            $("#myModal #txtCategoryError").text("");
            $("#myModal #txtCategories").removeClass("errorStyle");
        } 
    }); 
	$("#myModal #btnUpdate").click(function(){ // click update button
		var flag = "false";
		if ($("#myModal #txtCategories").val().trim() == '') {
			$("#myModal #txtCategoryError").text("Please enter disease category");
			$("#myModal #txtCategories").focus();
			$("#myModal #txtCategories").addClass("errorStyle");
			flag = "true";
		}
		else
		{
			var diagnosisCategory = $("#myModal #txtCategories").val().trim();
			var description = $("#myModal #txtDesc").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();	
			$.ajax({						// ajax call for Staff Type validation
				type: "POST",
				cache: false,
				url: "controllers/admin/diagnosis_category.php",
				datatype:"json",  
				data:{
				
					diagnosisCategory:diagnosisCategory,
					Id:id,	
					operation:"checkdiseasescategory"
				},
				success:function(data) {
					if(data == "1")
					{
						  $("#myModal #txtCategoryError").show();
						  $("#myModal #txtCategoryError").text("This category is already exist");
						  $("#myModal #txtCategories").addClass("errorStyle");
						  $("#myModal #txtCategories").focus();
					}
					else{
							var postData = {
							"operation":"update",
							"diagnosisCategory":diagnosisCategory,
							"description":description,
							"id":id
							}
							$.ajax( //ajax call for update data
							{					
								type: "POST",
								cache: false,
								url: "controllers/admin/diagnosis_category.php",
								datatype:"json",
								data: postData,

								success: function(data) {
									if(data != "0" && data != ""){
									$('#myModal').modal('toggle');
									$('.modal-body').text("");
									$('#messageMyModalLabel').text("Success");
									$('.modal-body').text("Diagnosis category update successfully!!!");
									$('#messagemyModal').modal();
									$("#diagnosisCategoryDataTable").dataTable().fnReloadAjax();
									clear();
									
									}
								},
								error:function() {
									alert('error');
								}
							});// end of ajax
					}
				},
				error:function(){
					alert ("data not found");
				}
			});
		}	
	});
}// end update button

function deleteClick(id){ // delete click function
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete Diagnosis Category");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	$('#selectedRow').val(id); 
	var type="delete";
	$('#confirm').attr('onclick','deleteDiagnosis("'+type+'");');
}// end click fucntion

function restoreClick(id){
	$('.modal-body').text("");
	$('#selectedRow').val(id);
	$('#confirmMyModalLabel').text("Restore Diagnosis Category");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal(); 
	var type="restore";
	$('#confirm').attr('onclick','deleteDiagnosis("'+type+'");');
// key press event on ESC button
}
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		 /* window.location.href = "http://localhost/herp/"; */
		 $('.close').click();
    }
});

function clear(){
	$('#txtCategories').val("");
	$('#txtCategoryError').text("");
	$('#txtDesc').val("");
	$("#txtCategories").removeClass("errorStyle");
}
function deleteDiagnosis(type){
		if(type=="delete"){
			var id = $('#selectedRow').val();
			var postData = {
				"operation":"delete",
				"id":id
			}
			$.ajax({			// ajax call for delete		
				type: "POST",
				cache: false,
				url: "controllers/admin/diagnosis_category.php",
				datatype:"json",
				data: postData,

				success: function(data) {
					if(data != "0" && data != ""){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Diagnosis category deleted successfully!!!");
						$('#messagemyModal').modal();
						$("#diagnosisCategoryDataTable").dataTable().fnReloadAjax(); 
					}
					else{
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Sorry");
						$('.modal-body').text("This diagnosis category is used , so you can not delete!!!");
						$('#messagemyModal').modal();
					}
				},
				error:function() {
					alert('error');
				}
			}); // end ajax 
		}
		else{
		var id = $('#selectedRow').val();
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/diagnosis_category.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Diagnosis category restored successfully!!!");
					$('#messagemyModal').modal();
					$("#diagnosisCategoryDataTable").dataTable().fnReloadAjax(); 
				 
				}			
			},
			error:function()
			{
				alert('error');
				  
			}
		});
		}
	}
//# sourceURL=diseasescategory.js