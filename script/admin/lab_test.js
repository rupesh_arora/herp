var otableLabComponent ;
var otableUpdateLabComponent ;
var labTable ;
var table ;
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   lab_test.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages lab test data
 *************************************************************************************************** */
	loader();	
	table = false;
	//Calling the function for binding the data 	
	bindLabType();
	bindUnitMeasure('','');
	bindSpecimenType();
	debugger;
	bindGLAccount("#selGLAccount",'');
	bindAccountReceivable("#selAccountPayable");
	bindLedger("#selLedger");//defined in common.js
	/* labComponentTable(false, '#tblLabTestComponent'); */
	otableLabComponent = $('#tblLabTestComponent').dataTable( {
		"bFilter": true,
		"processing": true,
		"bSort" :false,
		"sPaginationType":"full_numbers",  
		"fnDrawCallback" : function() {
			
		},
	});
	
	$('#btnAddComponent').click(function() {	 			
		if($("#divLabComponent").css('display') != "none"){
			 var currentData = otableLabComponent.fnAddData(["<input type ='text' class='editTextBox' placeholder='Please click for add name'>",
	         "<input type ='text' class='editTextBox' placeholder='Please click for add range'>",
	          "<select class='editable-drop-down' style='width:100%; padding:0px;'><option value='-1'>--Select--</option><select>",
	          "<input type ='number' class='editTextBox' placeholder='Please enter fee'>",
	          "<input type='button' class='btn-remove' onclick='rowDelete($(this));' value='Remove'>"]);
			 
			var getCurrDataRow = otableLabComponent.fnSettings().aoData[ currentData ].nTr;	

			bindUnitMeasure('',$(getCurrDataRow).find('td:eq(2)').children());
		}   
		else{
			var currentData = otableUpdateLabComponent.fnAddData(["<input type ='text' class='editTextBox' placeholder='Please click for add name'>",
	         "<input type ='text' class='editTextBox' placeholder='Please click for add range'>",
	          "<select class='editable-drop-down' style='width:100%; padding:0px;'><option value='-1'>--Select--</option><select>",
	          "<input type ='number' class='editTextBox' placeholder='Please enter fee'>",
	          "<input type='button' class='btn-remove' onclick='rowDelete($(this));' value='Remove'>"]);		 
			var getCurrDataRow = otableUpdateLabComponent.fnSettings().aoData[ currentData ].nTr;
			bindUnitMeasure('',$(getCurrDataRow).find('td:eq(2)').children());
		}	

    });
	
	$("#advanced-wizard").hide();
	$("#labTestList").addClass('list');
	$("#tabLabTestList").addClass('tab-list-add');
	
    $("#tabAddLabTest").click(function() {
    	showAddTab();      
    	clear();
		//Calling the function for binding the data 
		bindLabType();
		bindUnitMeasure('',"");
		bindSpecimenType();
    });
	
	//Click function for show the list of lab test
	 $("#tabLabTestList").click(function() {
        tabLabTestList(); //Calling the function for show the list 
    });
	
	
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		loadDataTable();
    }
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	labTable.fnClearTable();
	    	labTable.fnDestroy();
	    	//Datatable code
			labTable = $('#labTestDataTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  labTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var labType = mData["lab_type_id"];
							restoreClick(id,labType);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/lab_test.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					{ "mData": "name" },
					{ "mData": "test_code" },
					{ "mData": "lab_type" },
					{ "mData": "specimen_type" },
					{ "mData": "description" },
					{  
						"mData": function (o) { 
						
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},	
				],
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 5 ] },
				   { 'bSortable': false, 'aTargets': [ 4 ] }
				]
			} );
		}
		else{
			labTable.fnClearTable();
	    	labTable.fnDestroy();
	    	//Datatable code
			loadDataTable();
		}
    });	
		
    //validation and ajax call on submit button and save the data
    $("#btnSubmit").click(function() {
		var labTestComponent = [];
		var rowcollection = $("#tblLabTestComponent tbody tr td:nth-child(3)");
        var flag = false;
		$("#txtTestName").val($("#txtTestName").val().trim());

		if ($("#selGLAccount").val() == "-1") {
			$('#selGLAccount').focus();
            $("#selGLAccountError").text("Please select gl account");
			$("#selGLAccount").addClass("errorStyle");
            flag = true;
        }
		if ($("#selAccountPayable").val() == "-1") {
			$('#selAccountPayable').focus();
            $("#selAccountPayableError").text("Please select account payable");
			$("#selAccountPayable").addClass("errorStyle");
            flag = true;
        }
        if ($("#selLedger").val() == "-1") {
			$('#selLedger').focus();
            $("#selLedgerError").text("Please select ledger");
			$("#selLedger").addClass("errorStyle");
            flag = true;
        }
        if ($("#selSpecimenType").val() == "-1") {
			$('#selSpecimenType').focus();
            $("#selSpecimenTypeError").text("Please select specimen type");
			$("#selSpecimenType").addClass("errorStyle");
            flag = true;
        }
        if ($("#selLabType").val() == "-1") {
			$('#selLabType').focus();
            $("#selLabTypeError").text("Please select lab type");
			$("#selLabType").addClass("errorStyle");
            flag = true;
        }
        if(validTextField("#txtTestName","#txtTestNameError","Please enter test name") == true) {
			flag = true;
		} 
        if ($(".editTextBox").val()=='') {
        	callSuccessPopUp("Error", "Fill the name and normal range in table");
			flag = true;
        }
        if(validTextField("#txtTestCode","#txtTestCodeError","Please enter test code") == true) {
			flag = true;
		} 
        if (flag == true) {
            return false;
        }
        if(rowcollection.length == 0){
        	callSuccessPopUp("Sorry", "Please add component");
			return false;
		}
		
		for (i=0;i<rowcollection.length;i++) {
			var tdData = $(rowcollection.parent()[i]).find('td');						
			var ObjComponent = {};
			var unitId = $(rowcollection.children()[i]).val();
			if(unitId != undefined){
				ObjComponent.unitId = unitId;
				ObjComponent.unitName =  $(rowcollection[i]).find('.editable-drop-down option:selected').text(); 
				ObjComponent.componentName = $(tdData[0]).children().val();
				ObjComponent.componentRange = $(tdData[1]).children().val();
				ObjComponent.testFee = $(tdData[3]).children().val();
				labTestComponent.push(ObjComponent);
			}
			else{
				$('#messageMyModalLabel').text("Sorry");
				$('.modal-body').text("Please fill component");
				$('#messagemyModal').modal();
				return false;
			}			
		}
		
		var labType = $("#selLabType").val();
		var ledger = $("#selLedger").val();
		var glAccount = $("#selGLAccount").val();
		var accountPayable = $("#selAccountPayable").val();
		var specimenType = $("#selSpecimenType").val();
		var testName = $("#txtTestName").val();
		var description = $("#txtDescription").val().trim();
		var testCode = $("#txtTestCode").val().trim();
		description = description.replace(/'/g, "&#39").trim();
		var postData = {
			"operation":"save",
			"accountPayable":accountPayable,
			"ledger":ledger,
			"glAccount":glAccount,
			"labType":labType,
			"specimenType":specimenType,
			"testName":testName,
			"description":description,
			"testCode" : testCode,
			"labTestComponent":JSON.stringify(labTestComponent),
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/lab_test.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != "" && data !='Code already exist'){
        			callSuccessPopUp("Success", "Lab test saved successfully!!!");
					tabLabTestList();
					bindLabType();
					bindUnitMeasure('',"");
					bindSpecimenType();
				}
				else if (data =="Code already exist") {

					$("#txtTestCodeError").text("Test code already exists");
					$('#txtTestCode').focus();
					$("#txtTestCode").addClass("errorStyle");
				}
				else{
					$("#txtTestNameError").text("Test name already exists");
					$('#txtTestName').focus();
					$("#txtTestName").addClass("errorStyle");
				}
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	});	
	
	//
    //keyup functionality
	//
    $("#selLabType").change(function() {
        if ($("#selLabType").val() != "-1") {
            $("#selLabTypeError").text("");
			$("#txtTestNameError").text("");
            $("#selLabType").removeClass("errorStyle");
        }
    });
	
	$("#selSpecimenType").change(function() {
        if ($("#selSpecimenType").val() != "-1") {
            $("#selSpecimenTypeError").text("");
            $("#selSpecimenType").removeClass("errorStyle");
        }
    });
	
    $("#txtTestName").keyup(function() {
        if ($("#txtTestName").val() != "") {
            $("#txtTestNameError").text("");
            $("#txtTestName").removeClass("errorStyle");
        }
    });
    $("#txtTestCode").keyup(function() {
        if ($("#txtTestCode").val() != "") {
            $("#txtTestCodeError").text("");
            $("#txtTestCode").removeClass("errorStyle");
        }
    });	
    $("#selLedger").change(function() {
        if ($("#selLedger").val() != "") {
            $("#selLedgerError").text("");
            $("#selLedger").removeClass("errorStyle");
        }
    });
	$("#selAccountPayable").change(function() {
        if ($("#selAccountPayable").val() != "") {
            $("#selAccountPayableError").text("");
            $("#selAccountPayable").removeClass("errorStyle");
        }
    });
    $("#selGLAccount").change(function() {
        if ($("#selGLAccount").val() != "") {
            $("#selGLAccountError").text("");
            $("#selGLAccount").removeClass("errorStyle");
        }
    });	
	
});

//		
//function for edit the bed 
//
function editClick(id,name,labType,specimen,description,testCode,ledger,account_payable,glAccount){	
	showAddTab();
    $("#btnReset").hide();
    $("#btnSubmit").hide();
    $("#divLabComponent").hide();
    $("#divUpdateLabComponent").show();
     $('#tabAddLabTest').html("+Update Lab Test");

	//$('.modal-body').text("");    
	//$('#myModalLabel').text("Update Lab Test");
	//$('.modal-body').html($("#bedModel").html()).show();
	$("#btnUpdate").removeAttr("style");
	$("#divUpdateLabComponent").removeAttr("style");
	$('#selLabType').val(labType);
	$("#selSpecimenType").val(specimen);
	$("#selLedger").val(ledger);
	$("#selGLAccount").val(glAccount);
	$("#selAccountPayable").val(account_payable);
	$("#txtTestName").val(name);
	$("#txtTestCode").val(testCode);
	$('#txtDescription').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id);
   
	
	//Calling the function for binding the data
	/*bindLabType();
	bindUnitMeasure('',"");
	bindSpecimenType();*/
	
	if(table == false){		
		otableUpdateLabComponent = $('#tblUpdateLabTestComponent').dataTable( {
			"bFilter": true,
			"processing": true,
			"sPaginationType":"full_numbers",  
			"destroy": true,
			"bSort" :false,
			"bAutoWidth": false,
			"fnDrawCallback" : function() {		
				
			},
			aoColumnDefs: [
			   { 'bSortable': false, 'aTargets': [ 3 ] },
			   { 'bSortable': false, 'aTargets': [ 4 ] }
			]
		});
	}


 	var labTestId = $('#selectedRow').val();
 	var postData = {
	   "operation":"tblSowData",
	   labTestId :labTestId
	}
  
  	$.ajax({     
		type: "POST",
		cache: false,
		url: "controllers/admin/lab_test.php",
		datatype:"json",
		data: postData,

			success: function(data) {
			var unitIdArray = new Array();
			table = true;
			if(data != "0" && data != ""){
				var parseData = jQuery.parseJSON(data);
				var dataLength = parseData.length;
				otableUpdateLabComponent.fnClearTable();
				for (var i=0;i<dataLength;i++) {

					var labTestId = parseData[i].lab_test_id;
					var unitId = parseData[i].unit_id;
					var unitName = parseData[i].unit;
					var name = parseData[i].name;
					var fee = parseData[i].fee;
					var normalRange = parseData[i].normal_range;
					unitIdArray.push(unitId);
					var currentData = otableUpdateLabComponent.fnAddData(["<input type ='text' class='editTextBox' value='"+name+"' placeholder='Please click for add name'>",
					 "<input type ='text' class='editTextBox'  value='"+normalRange+"' placeholder='Please click for add range'>",
					  "<select class='editable-drop-down' style='width:100%; padding:0px;'><option value='"+unitId+"'>"+unitName+"</option><select>",
         			 "<input type ='number' class='editTextBox' placeholder='Please enter fee'  value='"+fee+"' >",
					  "<input type='button' class='btn-remove' onclick='rowDelete($(this));' value='Remove'>"]);
					bindUnitMeasure(unitId,$(otableUpdateLabComponent.fnGetNodes()[i]).find('td:eq(2)').children());
					
				}
				/*for(j=0;j<unitIdArray.length;j++){
					$(otableUpdateLabComponent.fnGetNodes()[j]).find('td:eq(2)').children().val(unitIdArray[j]);
				}*/
				
			}
		},
		error:function() {
		$('#messageMyModalLabel').text("Error");
		$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		$('#messagemyModal').modal();
		}
  	});
	
	//Click function for update button 
	$("#btnUpdate").click(function(){
		
		var updateLabComponent = [];
		var rowcollection = $("#tblUpdateLabTestComponent tbody tr td:nth-child(3)");
		
		var flag = false;
		$("#txtTestName").val($("#txtTestName").val().trim());
		
		for (i=0;i<rowcollection.length;i++) {
			var tdData = $(rowcollection.parent()[i]).find('td');						
			var ObjComponent = {};
			var unitId = $(rowcollection.children()[i]).val();
			if(unitId != undefined){
				ObjComponent.unitId = unitId;
				ObjComponent.unitName = $(rowcollection[i]).find('.editable-drop-down option:selected').text();
				ObjComponent.componentName = $(tdData[0]).children().val();
				ObjComponent.componentRange = $(tdData[1]).children().val();
				ObjComponent.testFee = $(tdData[3]).children().val();
				updateLabComponent.push(ObjComponent);
			}
			else{
				flag = true;
			}			
		}
		if ($("#selGLAccount").val() == "-1") {
			$('#selGLAccount').focus();
            $("#selGLAccountError").text("Please select gl account");
			$("#selGLAccount").addClass("errorStyle");
            flag = true;
        }
		if ($("#selAccountPayable").val() == "-1") {
			$('#selAccountPayable').focus();
            $("#selAccountPayableError").text("Please select account payable");
			$("#selAccountPayable").addClass("errorStyle");
            flag = true;
        }
        if ($("#selLedger").val() == "-1") {
			$('#selLedger').focus();
            $("#selLedgerError").text("Please select ledger");
			$("#selLedger").addClass("errorStyle");
            flag = true;
        }

		if ($("#selSpecimenType").val() == "-1") {
			$('#selSpecimenType').focus();
            $("#selSpecimenTypeError").text("Please select specimen type");
			$("#selSpecimenType").addClass("errorStyle");
            flag = true;
        }
		if ($("#selLabType").val() == "-1") {
			$('#selLabType').focus();
            $("#selLabTypeError").text("Please select lab type");
			$("#selLabType").addClass("errorStyle");
            flag = true;
        }
        if(validTextField("#txtTestName","#txtTestNameError","Please enter test name") == true) {
			flag = true;
		} 

		if ($("#myModal .editTextBox").val()=='') {  
			$('#confirmUpdateModalLabel').text("");  
			$('#updateBody').text("");  	
			$('#confirmUpdateModalLabel').text("Error");
			$('#updateBody').text("Fill the name and normal range");
			$('#confirmUpdateModal').modal();
			flag = true;
        }
        if(validTextField("#txtTestCode","#txtTestCodeError","Please enter test code") == true) {
			flag = true;
		}  
		if(flag == true){			
			return false;
		}
		else{
			var ledger = $("#selLedger").val();
			var glAccount = $("#selGLAccount").val();
			var accountPayable = $("#selAccountPayable").val();
			var labType = $("#selLabType").val();
			var specimenType = $("#selSpecimenType").val();
			var testName = $("#txtTestName").val().trim();
			var description = $("#txtDescription").val().trim();
			description = description.replace(/'/g, "&#39");
			var testCode = $("#txtTestCode").val().trim();
			var id = $('#selectedRow').val();			
			
			$('#confirmUpdateModalLabel').text("");  
			$('#updateBody').text(""); 
			$('#confirmUpdateModalLabel').text("Confirmation");
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){	
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/lab_test.php",
					data :{            
						"operation" : "update",
						"id" : id,
						"labType":labType,
						"ledger":ledger,
						"glAccount":glAccount,
						"accountPayable":accountPayable,
						"specimenType":specimenType,
						"testName":testName,
						"updateLabComponent":JSON.stringify(updateLabComponent),
						"description":description,
						"testCode" : testCode
					},
					success: function(data) {	
						if(data != "0" && data != "" && data !='Code already exist'  && data != 'Component is used'){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Lab test updated successfully!!!");							
							$('#messagemyModal').modal();
							 tabLabTestList();
						}
						else if (data =="Code already exist") {
							$("#txtTestCodeError").text("Test code already exists");
							$('#txtTestCode').focus();
							$("#txtTestCode").addClass("errorStyle");
						}
						else if(data == 'Component is used'){
							$('#messageMyModalLabel').text("Sorry");
							$('#messageBody').text("Component is used so you can not delete!!!");
							$('#messagemyModal').modal();
						}
						else{
							$("#txtTestNameError").text("Test name already exists");
							$('#txtTestName').focus();
							$("#txtTestName").addClass("errorStyle");							
						}
					},
					error:function() {
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
				});
			});
		}
	});	
}
//		
//function for delete button
//
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('.modal-body').text('');
	$('#confirmMyModalLabel').text("Delete Lab Test");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteLabTest("'+type+'");');
}

//Function for restore button
function restoreClick(id,labType){
	$('#selectedRow').val(id);
	 $('#selLabType').val(labType);
	$('.modal-body').text('');
	$('#confirmMyModalLabel').text("Restore Lab Test");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal(); 
	
	var type="restore";
	$('#confirm').attr('onclick','deleteLabTest("'+type+'");');
}

//Function for delete and restore the data 
function deleteLabTest(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
			
			
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/lab_test.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Lab test deleted successfully!!!");
					labTable.fnReloadAjax();
					$('#messagemyModal').modal();
					 
				}
			},
			error: function()
			{
				$('.modal-body').text('');
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal(); 
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
		var labType = $('#selLabType').val();
		
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/lab_test.php",
			data :{            
				"operation" : "restore",
				"id" : id,
				"labType" : labType
			},
			success: function(data) {	
				if(data == "Restored Successfully!!!" && data != ""){
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Lab test restored successfully!!!");
					labTable.fnReloadAjax();
					$('#messagemyModal').modal();
				}	
				if(data == "It can not be restore!!!"){
					$('.modal-body').text('');
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("It can not be restore!!!");
					labTable.fnReloadAjax();
					$('#messagemyModal').modal();
				}
			},
			error:function()
			{
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal(); 
			}
		});
	}
}
//
//Function for clear the data
//
function clear(){
	$('#selLabType').val("");
	$('#selLabTypeError').text("");
	$('#selSpecimenType').val("");
	$('#txtTestCode').val("");
	$('#txtTestCodeError').text("");
	$('#selSpecimenTypeError').text("");
	$('#txtTestName').val("");
	$('#txtTestNameError').text("");
	$('#txtDescription').val("");
	otableLabComponent.fnClearTable();
	$('#selLedger').val("");
	$('#selGLAccount').val("");
	$('#selAccountPayable').val("");
	$('#selLedgerError').text("");
	$('#selAccountPayableError').text("");
	$('#selGLAccountError').text("");
	$("#selAccountPayable").removeClass("errorStyle");
	$("#selLedger").removeClass("errorStyle");
	$("#selGLAccount").removeClass("errorStyle");
}
//
//function for reset button 
//
$("#btnReset").click(function(){
	$("#selLabType").removeClass("errorStyle");
	$("#txtTestName").removeClass("errorStyle");
	$("#selSpecimenType").removeClass("errorStyle");
	$("#txtTestCode").removeClass("errorStyle");
	otableLabComponent.fnClearTable();
	bindLabType();
	bindUnitMeasure("","");
	bindSpecimenType();
	$('#txtTestName').focus();
	clear();
});
//
//function binding ward
//
function bindLabType(){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/lab_test.php",
		data: {
			"operation":"showLabType"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>-- Select --</option>";
				for (var i=0;i<parseData.length;i++)
				{
				option+="<option value='"+parseData[i].id+"'>"+parseData[i].type+"</option>";
				}
				$('#selLabType').html(option);				
				
			}
		},
		error:function(){
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();			
		}
	});
}

//
//function for binding the room
//
function bindUnitMeasure(unitId,myeditableDropDown){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/lab_test.php",
		data: {
			"operation":"showUnitMeasure"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>-- Select --</option>";
				for (var i=0;i<parseData.length;i++)
				{
					option+="<option value='"+parseData[i].id+"'>"+parseData[i].unit_abbr+"</option>";
				}				
				$('#selUnitMeasure').html(option);
				if(myeditableDropDown != ""){
					$(myeditableDropDown).html(option);
					if(unitId != ""){
						$(myeditableDropDown).val(unitId);
					}					
				}
				
			}
		},
		error:function(){			
		}
	});	
}

//Function for binding the specimen type
function bindSpecimenType(){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/lab_test.php",
		data: {
			"operation":"showSpecimenType"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>-- Select --</option>";
				for (var i=0;i<parseData.length;i++)
				{
				option+="<option value='"+parseData[i].id+"'>"+parseData[i].type+"</option>";
				}				
				$('#selSpecimenType').html(option);				
			}
		},
		error:function(){	
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();		
		}
	});	
}

/**Row Delete functionality**/
function rowDelete(currInst) {
    var row = currInst.closest("tr").get(0);
    otableLabComponent.fnDeleteRow(row);
    otableUpdateLabComponent.fnDeleteRow(row);
}

//Function for show the lab test list
function tabLabTestList(){
    $("#advanced-wizard").hide();
	$(".blackborder").show();
	$('#inactive-checkbox-tick').prop('checked', false).change();
	$("#tabAddLabTest").removeClass('tab-detail-add');
    $("#tabAddLabTest").addClass('tab-detail-remove');
    $("#tabLabTestList").removeClass('tab-list-remove');    
    $("#tabLabTestList").addClass('tab-list-add');

    
    $("#btnReset").show();
    $("#btnSubmit").show();
    $("#btnUpdate").hide();
    $("#divLabComponent").show();
    $("#divUpdateLabComponent").hide();
    $('#tabAddLabTest').html("+Add Lab Test");

    $("#labTestList").addClass('list');
	$("#selLabType").removeClass("errorStyle");
	$("#txtTestName").removeClass("errorStyle");
	$("#selSpecimenType").removeClass("errorStyle");
	clear();
}

function showAddTab(){
	$("#advanced-wizard").show();
    $(".blackborder").hide();
    
    $("#tabAddLabTest").addClass('tab-detail-add');
    $("#tabAddLabTest").removeClass('tab-detail-remove');
    $("#tabLabTestList").removeClass('tab-list-add');
    $("#tabLabTestList").addClass('tab-list-remove');
    $("#labTestList").addClass('list');
	$('#txtTestName').focus();
}

//Dtat table initialization 
function labComponentTable(value, tblName){
	
	if(value == true){
		otableLabComponent.fnClearTable();
		otableLabComponent.html("");
		otableLabComponent.fnDestroy();
	}
}

function loadDataTable(){
	labTable = $('#labTestDataTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {
			$('.update').unbind();
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = labTable.fnGetData(data);
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					var name = mData["name"];
					var labType = mData["lab_type_id"];
					var specimen = mData["specimen_id"];
					var description = mData["description"];
					var testCode = mData["test_code"];
					var ledger = mData["ledger"];
					var glAccount = mData["gl_account"];
					var account_payable = mData["account_payable"];
					editClick(id,name,labType,specimen,description,testCode,ledger,account_payable,glAccount);       
				}
			});
			$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  labTable.fnGetData(data);
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);
				}
			});
		},
		
		"sAjaxSource":"controllers/admin/lab_test.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{ "mData": "name" },
			{ "mData": "test_code" },
			{ "mData": "lab_type" },
			{ "mData": "specimen_type" },
			{ "mData": "description" },
			{  
				"mData": function (o) { 
				
				return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
				   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
				   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
				   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
				   "  data-original-title='Delete'></i>"; 
				}
			},
		],
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 5 ] },
		   { 'bSortable': false, 'aTargets': [ 4 ] }
		]
	} );
}

// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) {  
		 $(".close").click();
    }
});

//# sourceURL = bed.js