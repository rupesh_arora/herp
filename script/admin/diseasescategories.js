/*
 * File Name    :   diseasescategories.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre operation
 */
var diseaseCategoryTable; //defile variable for diseaseCategoryTable 
$(document).ready(function() {
    loader();
    debugger;
    $('#btnReset').click(function() { // for reset functionality
        clear();
        $("#txtCategories").removeClass("errorStyle");
        $("#txtCategories").focus();
    });

    $("#form_diseases").hide();
    $("#diseases_list").addClass('list');    
    $("#tab_diseases").addClass('tab-list-add');

    $("#tab_add_diseases").click(function() { // click on add diseasesCategory tab
        $("#form_diseases").show();
        $(".blackborder").hide();

        $("#tab_add_diseases").addClass('tab-detail-add');
        $("#tab_add_diseases").removeClass('tab-detail-remove');
        $("#tab_diseases").removeClass('tab-list-add');
        $("#tab_diseases").addClass('tab-list-remove');
        $("#diseases_list").addClass('list');
        
        $("#txtCategoryError").text("");
        $("#txtCategories").removeClass("errorStyle");
        $("#txtCategories").focus();
        clear();
    });

    $("#tab_diseases").click(function() { // show the diseasesCategory list tab
        showTableList();
    });

	// import excel on submit click
	$('#importDataFile').click(function(){
		var flag = false;
		if($('#file').prop('files')[0] == undefined || $('#file').val() == "") {
			$('#txtFileError').show();
			$('#txtFileError').text("Please choose file.");
			flag  = true;
		}
		if(flag == true){
			return false;
		}
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to upload this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			var file_data = $('#file').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: 'controllers/admin/diseasescategory.php', // point to server-side PHP script 
				dataType: 'text', // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					if(data != "" && data != "invalid file" && data == "1"){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Your file has been successfully imported!!!");
						$('#messagemyModal').modal();
						$('#file').val("");
					}
					else if(data == "invalid file"){
						$('#txtFileError').show();
						$('#txtFileError').text("Please import only CSV/XLS file.");
						$('#file').val("");
					}
                    else if (data =="You must have two column in your file i.e category and description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="First column should be category" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Second column should be description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Data can't be null in category column" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Please insert data in first column and second column only i.e A & B") {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
					else if(data == ""){
						$('#txtFileError').show();
						$('#txtFileError').text("Data already exist.");
						$('#file').val("");
					}
				},
				error: function() {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Data Not Found!!!");
					$('#messagemyModal').modal();
				}
			});
		});
	});
	
    $("#btnAddCategory").click(function() { // save details on click with validation

        var flag = "false";
        if ($("#txtCategories").val().trim() == '') {
            $("#txtCategoryError").text("Please enter disease category");
            $("#txtCategories").focus();
            $("#txtCategories").addClass("errorStyle");
            flag = "true";
        } else {
            var diseasesCategory = $("#txtCategories").val().trim();
            var description = $("#txtDesc").val();
            description = description.replace(/'/g, "&#39");
            $.ajax({ // ajax call for categories validation
                type: "POST",
                cache: false,
                url: "controllers/admin/diseasescategory.php",
                datatype: "json",
                data: {

                    diseasesCategory: diseasesCategory,
                    Id: "",
                    operation: "checkdiseasescategory"
                },
                success: function(data) {
                    if (data == "1") {
                        $("#txtCategoryError").show();
                        $("#txtCategoryError").text("This category is already exist");
                        $("#txtCategories").addClass("errorStyle");
                        $("#txtCategories").focus();
                    } else {
                        //ajax call for insert data into data base
                        var postData = {
                            "operation": "save",
                            "diseasesCategory": diseasesCategory,
                            "description": description
                        }
                        $.ajax({
                            type: "POST",
                            cache: false,
                            url: "controllers/admin/diseasescategory.php",
                            datatype: "json",
                            data: postData,

                            success: function(data) {
                                if (data != "0" && data != "" && data == "1") {
                                    $('.modal-body').text("");
                                    $('#messageMyModalLabel').text("Success");
                                    $('.modal-body').text("Disease category saved successfully!!!");
                                    $('#messagemyModal').modal();
									showTableList();
                                }
                            },
                            error: function() {
                                alert('error');
                            }
                        }); //  end ajax call
                    }
                },
                error: function() {
			
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
            });
        }
    }); //end button click function

    //validation style remove on keyup
    $("#txtCategories").keyup(function() {
        if ($("#txtCategories").val() != '') {
            $("#txtCategoryError").text("");
            $("#txtCategories").removeClass("errorStyle");
        }
    });
    if ($('.inactive-checkbox').not(':checked')) { // show datatable on load 
        diseaseCategoryTable = $('#diseasescategoriesdatatable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "fnDrawCallback": function(oSettings) {
				$('.update').unbind();
                $('.update').on('click', function() { // perform update click event
                    var data = $(this).parents('tr')[0];
                    var mData = diseaseCategoryTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var category = mData["category"];
                        var description = mData["description"];
                        editClick(id, category, description);

                    }
                });
				$('.delete').unbind();
                $('.delete').on('click', function() { // perform delete click event
                    var data = $(this).parents('tr')[0];
                    var mData = diseaseCategoryTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/diseasescategory.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [{
                "sWidth": "7%",
                "mData": "category"
            }, {
                "sWidth": "15%",
                "mData": "description"
            }, {
                "sWidth": "3%",
                "mData": function(o) {
                    var data = o;
                    return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                        " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                        " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                        " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                        " data-original-title='Delete'></i>";
                }
            }, ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [1]
            }, {
                'bSortable': false,
                'aTargets': [2]
            }]

        });
    }
    $('.inactive-checkbox').change(function() { // perform to checked and unchecked event
        if ($('.inactive-checkbox').is(":checked")) { // load inactive data on checked 
            diseaseCategoryTable.fnClearTable();
            diseaseCategoryTable.fnDestroy();
            diseaseCategoryTable = "";
            diseaseCategoryTable = $('#diseasescategoriesdatatable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
					$('.restore').unbind();
                    $('.restore').on('click', function() { // perform restore event	
                        var data = $(this).parents('tr')[0];
                        var mData = diseaseCategoryTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            restoreClick(id);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/diseasescategory.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [{
                    "sWidth": "7%",
                    "mData": "category"
                }, {
                    "sWidth": "15%",
                    "mData": "description"
                }, {
                    "sWidth": "3%",
                    "mData": function(o) {
                        var data = o;
                        return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                    }
                }, ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1]
                }, {
                    'bSortable': false,
                    'aTargets': [2]
                }]
            });
        } else {
            diseaseCategoryTable.fnClearTable();
            diseaseCategoryTable.fnDestroy();
            diseaseCategoryTable = "";
            diseaseCategoryTable = $('#diseasescategoriesdatatable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
					$('.update').unbind();	
                    $('.update').on('click', function() { // perform update click event
                        var data = $(this).parents('tr')[0];
                        var mData = diseaseCategoryTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var category = mData["category"];
                            var description = mData["description"];
                            editClick(id, category, description);

                        }
                    });
					$('.delete').unbind();	
                    $('.delete').on('click', function() { // perform delete click event
                        var data = $(this).parents('tr')[0];
                        var mData = diseaseCategoryTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/diseasescategory.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    /* {  "sWidth": "3%","mData": "id" }, */
                    {
                        "sWidth": "7%",
                        "mData": "category"
                    }, {
                        "sWidth": "15%",
                        "mData": "description"
                    }, {
                        "sWidth": "3%",
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1]
                }, {
                    'bSortable': false,
                    'aTargets': [2]
                }]
            });
        }
    });

});
// edit data dunction for update 
function editClick(id, category, description) {
    
    $("#tab_add_diseases").click();
    $("#uploadFile").hide();
    $("#btnReset").hide();
    $("#btnAddCategory").hide();
    $('#tab_add_diseases').html("+Update Disease Category");

    $("#btnUpdate").removeAttr("style");
    $("#txtCategories").removeClass("errorStyle");
    $("#txtCategoryError").text("");
    $('#txtCategories').val(category);
    $('#txtDesc').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
    //validation
   
    $("#btnUpdate").click(function() { // click update button
        var flag = "false";

        if ($("#txtCategories").val().trim() == '') {
            $("#txtCategoryError").text("Please enter disease category");
            $("#txtCategories").focus();
            $("#txtCategories").addClass("errorStyle");
            flag = "true";
        } 
        else {
            var diseasesCategory = $("#txtCategories").val().trim();
            var description = $("#txtDesc").val();
            description = description.replace(/'/g, "&#39");
            var id = $('#selectedRow').val();			
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				$.ajax({ // ajax call for Staff Type validation
					type: "POST",
					cache: false,
					url: "controllers/admin/diseasescategory.php",
					datatype: "json",
					data: {

						diseasesCategory: diseasesCategory,
						Id: id,
						operation: "checkdiseasescategory"
					},
					success: function(data) {
						if (data == "1") {
							$("#txtCategoryError").show();
							$("#txtCategoryError").text("This category is already exist");
							$("#txtCategories").addClass("errorStyle");
							$("#txtCategories").focus();
						} else {
							var postData = {
								"operation": "update",
								"diseasesCategory": diseasesCategory,
								"description": description,
								"id": id
							}
							$.ajax( //ajax call for update data
								{
									type: "POST",
									cache: false,
									url: "controllers/admin/diseasescategory.php",
									datatype: "json",
									data: postData,

									success: function(data) {
										if (data != "0" && data != "") {
											$('#myModal').modal('hide');
											$('.close-confirm').click();
											$('.modal-body').text("");
											$('#messageMyModalLabel').text("Success");
											$('.modal-body').text("Disease category updated successfully!!!");
											$('#messagemyModal').modal();
                                            $("#tab_diseases").click();

										}
									},
									error: function() {
										$('.modal-body').text("");
										$('#messageMyModalLabel').text("Error");
										$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
										$('#messagemyModal').modal();
									}
								}); // end of ajax
						}
					},
					error: function() {
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
				});
			});
        }
    });
} // end update button

function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Disease Category");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteDisease("' + type + '");');
} // end click fucntion

function restoreClick(id) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Disease Category");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteDisease("' + type + '");');
}
// function perform delete and restore value from calling that function
function deleteDisease(type) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete		
            type: "POST",
            cache: false,
            url: "controllers/admin/diseasescategory.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Disease category deleted successfully!!!");
                    $('#messagemyModal').modal();
                    diseaseCategoryTable.fnReloadAjax();
                } else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This diseases category is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/diseasescategory.php",
            data: {
                "operation": "restore",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Disease category restored successfully!!!");
                    $('#messagemyModal').modal();
                    diseaseCategoryTable.fnReloadAjax();

                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    }
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
// clear function
function clear() {
    $('#txtCategories').val("");
    $('#txtCategoryError').text("");
    $('#txtFileError').text('');
    $('#txtDesc').val("");
    $("#txtCategories").removeClass("errorStyle");
}

function showTableList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();

    $("#form_diseases").hide();
    $(".blackborder").show();

    $("#tab_add_diseases").removeClass('tab-detail-add');
    $("#tab_add_diseases").addClass('tab-detail-remove');
    $("#tab_diseases").removeClass('tab-list-remove');    
    $("#tab_diseases").addClass('tab-list-add');
    $("#diseases_list").addClass('list');


    $("#uploadFile").show();
    $("#btnReset").show();
    $("#btnAddCategory").show();
    $('#btnUpdate').hide();
    $('#tab_add_diseases').html("+Add Disease Category");
    clear();
}