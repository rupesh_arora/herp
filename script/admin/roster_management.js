$(document).ready(function(){
	debugger;
	// call date picker
	var startDate = new Date();
	//$("#txtDate").datepicker('setStartDate', startDate);
	$("#txtDate").datepicker({ changeYear: true,changeMonth: true,autoclose: true,startDate:startDate}); 
	
	// show div assigne staff
	$('#checkAssigneDutyTo').click(function() {
		if($("#checkAssigneDutyTo").prop('checked') == true) {
			$(".assigneStaff").show();
			$("#selStaffTypeAssigne").val("");
			 $("#selStaffNameAssigne").val("");
		}
		else{
			$(".assigneStaff").hide();
			 $("#selStaffTypeAssigne").val("");
			 $("#selStaffNameAssigne").val("");
		}
	});
	
	//load staff type
	bindStaffType("#selStaffType");	
	
	//Change event of staff type to load staff name
	$('#selStaffType').change(function() {
		var htmlRoom = "<option value=''>-- Select --</option>";
		$("#selStaffTypeError").text("");
		$("#selStaffType").removeClass("errorStyle");

		if ($('#selStaffType :selected').val() != '') {
			var value = $('#selStaffType :selected').val();
			//Calling function for binding the staff name
			bindStaffName(value,"#selStaffName");
		} else {
			$('#selStaffName').html(htmlRoom);				
		}
	});
		
	// load assigne staff type
	bindStaffType("#selStaffTypeAssigne");	
	
	//Change event of assigne staff type to load staff name
	$('#selStaffTypeAssigne').change(function() {
		var htmlRoom = "<option value=''>-- Select --</option>";
		$("#selStaffTypeAssigneError").text("");
		$("#selStaffTypeAssigne").removeClass("errorStyle");

		if ($('#selStaffTypeAssigne :selected').val() != "") {
			var value = $('#selStaffTypeAssigne :selected').val();
			//Calling function for binding the staff name
			bindStaffName(value,"#selStaffNameAssigne");
		} else {
			$('#selStaffNameAssigne').html(htmlRoom);				
		}
	});
	
	// remove error message on key up and change event
	removeErrorMessage();
	
	// save event
	$("#btnSave").on("click",function(){
		var flag = false;
		if(validTextField("#txtDate","#txtDateError","Please select date") == true) {
			flag = true;
			$('#txtDate').focus().datepicker("hide");
		}
		if(validTextField("#selStaffName","#selStaffNameError","Please select staff name") == true) {
			flag = true;
		}
		if(validTextField("#selStaffType","#selStaffTypeError","Please select staff type") == true) {
			flag = true;
		}
		
		if(flag ==  true) {
			return false;
		}
		var staffName = $("#selStaffName").val();
		var date = $("#txtDate").val();
		
		if($("#checkFieldWork").prop("checked") == true){
			var fieldValue = 'P';
		}else{
			var fieldValue = 'A';
		}
		if($("#checkAssigneDutyTo").prop('checked') == true){
			var flag = false;
			if(validTextField("#selStaffNameAssigne","#selStaffNameAssigneError","Please select staff name") == true) {
				flag = true;
			}
			if(validTextField("#selStaffTypeAssigne","#selStaffTypeAssigneError","Please select staff type") == true) {
				flag = true;
			}
			
			if(flag ==  true) {
				return false;
			}
			var assignStaffName = $("#selStaffNameAssigne").val();
		}
		else{
			var assignStaffName = "";
		}
		var currentDateTime = DateTime();
		// define value for post data in back end file
		var postData = {
			"operation":"saveDetails",
			staffName:staffName,
			date:date,
			fieldValue:fieldValue,
			assignStaffName:assignStaffName,
			currentDateTime:currentDateTime
		};

		// call the function ajax call
		dataCall("./controllers/admin/roster_management.php", postData, function (data) {
			if (data != null && data != "" && data != "He is on leave") {
				// call message pop up function.
				callSuccessPopUp('Success','Saved successfully !!!');	
				clearFormDetails(".rosterManagementForm");
				$("#checkAssigneDutyTo").prop('checked',false);
				$("#checkFieldWork").prop('checked',false);
				$(".assigneStaff").hide();
			}
			if(data == "He is on leave") {
				callSuccessPopUp('Alert','He is on leave.');
			}
		});
	});
	
	// button reset
	$("#btnReset").on('click',function(){
		clearFormDetails(".rosterManagementForm");
		$("#checkAssigneDutyTo").prop('checked',false);
		$("#checkFieldWork").prop('checked',false);
		$(".assigneStaff").hide();
	});
		
});
//Function for binding the staff type
function bindStaffType(staffTypeId) {
    var postData = {
        "operation": "showStaffType"
    }
    $.ajax({
        type: "POST",
        cache: false,
        url: "./controllers/admin/schedule_staff.php",
        datatype: "json",
        data: postData,
        success: function(data) {
            if (data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";

                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
                }

                $(staffTypeId).html(option);
            }
        },
        error: function() {
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();
        }
    });
}

//Function for binding the staff name 
function bindStaffName(value,staffNameId) {
    var postData = {
        "operation": "showStaffName",
        "staffTypeId": value
    }

    $.ajax({
        type: "POST",
        cache: false,
        url: "./controllers/admin/schedule_staff.php",
        datatype: "json",
        data: postData,
        success: function(data) {
            if (data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";

                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }

                $(staffNameId).html(option);
            }
        },
        error: function() {
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();
        }
    });
}