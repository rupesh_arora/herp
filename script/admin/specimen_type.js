var specimenTypeTable;//defining a global varibale for data table
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   specimen_type.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages specimen type data
 *************************************************************************************************** */
	loader();
	$("#form_dept").hide();
    $("#specimen_type_list").addClass('list');    
    $("#tabSpecimenTypelist").addClass('tab-list-add');
	
	//Click function for change the tab
    $("#tabAddSpecimenType").click(function() {
        showAddTab();
		clear();
    });
	//Click function for change the tab
    $("#tabSpecimenTypelist").click(function() {
        tabSpecimenTypelist();//calling the function for show the list
    });

	// import excel on submit click
	$('#importDataFile').click(function(){
		var flag = false;
		if($('#file').prop('files')[0] == undefined || $('#file').val() == "") {
			$('#txtFileError').show();
			$('#txtFileError').text("Please choose file.");
			flag  = true;
		}
		if(flag == true){
			return false;
		}
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to upload this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			var file_data = $('#file').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: 'controllers/admin/specimen_type.php', // point to server-side PHP script 
				dataType: 'text', // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					if(data != "" && data != "invalid file" && data =="1"){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Your file has been successfully imported!!!");
						$('#messagemyModal').modal();
						$('#file').val("");
					}
					else if (data =="You must have two column in your file i.e specimen type and description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="First column should be specimen type" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Second column should be description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Please insert data in first column and second column only i.e A & B") {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Data can't be null in specimen type column" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
					else if(data == "invalid file"){
						$('#txtFileError').show();
						$('#txtFileError').text("Please import only CSV/XLS file.");
						$('#file').val("");
					}
					else if(data == ""){
						$('#txtFileError').show();
						$('#txtFileError').text("Data already exist.");
						$('#file').val("");
					}
				},
				error: function() {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Data Not Found!!!");
					$('#messagemyModal').modal();

				}
			});
		});
	});
	
	//Click function for validation and save the data
    $("#btnSubmit").click(function() {
		
		var flag = "false";
		$("#txtSpecimenType").val($("#txtSpecimenType").val().trim());
		
        if ($("#txtSpecimenType").val() == '') {
			$('#txtSpecimenType').focus();
            $("#txtSpecimenTypeError").text("Please enter specimen type");
            $("#txtSpecimenType").addClass("errorStyle");
            flag = "true";
        }
		if(flag == "true"){			
			return false;
		}		
		
		var specimenType = $("#txtSpecimenType").val();
		var description = $("#txtDescription").val();
		description = description.replace(/'/g, "&#39");
		var postData = {
			"operation":"save",
			"specimenType":specimenType,
			"description":description
		}
		
		$.ajax( //ajax call		
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/specimen_type.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Specimen type saved successfully!!!");
					$('#messagemyModal').modal();
					tabSpecimenTypelist();//Calling function for show the list
					clear();
				}
				else{
					$("#txtSpecimenTypeError").text("Specimen type is already exists");
					$('#txtSpecimenType').focus();
					$("#txtSpecimenType").addClass("errorStyle");
				}				
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});		
    });

    //keyup functionality
    $("#txtSpecimenType").keyup(function() {
        if ($("#txtSpecimenType").val() != '') {
            $("#txtSpecimenTypeError").text("");
            $("#txtSpecimenType").removeClass("errorStyle");
        } 
    });

    //load Datatable data on page load
    if($('.inactive-checkbox').not(':checked')){
    	loadDataTable();
    }

    //checked or not functionality to show active or inactive data
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
    		specimenTypeTable.fnClearTable();
	    	specimenTypeTable.fnDestroy();
	    	specimenTypeTable="";
	    	specimenTypeTable = $('#specimenTypeDataTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  specimenTypeTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/specimen_type.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "checked" });
				},
				"aoColumns": [
					{ "mData": "type" },
					{ "mData": "description" },	
					{  
						"mData": function (o) { 
						
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},	
				],
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 1 ] },
				   { 'bSortable': false, 'aTargets': [ 2 ] }
			   ]
			});
    	}
    	else{
    		specimenTypeTable.fnClearTable();
	    	specimenTypeTable.fnDestroy();
	    	specimenTypeTable="";
    		loadDataTable();
    	}
    });	
	//Click function for reset button
	$("#btnReset").click(function(){
		$("#txtSpecimenType").removeClass("errorStyle");
		$("#txtSpecimenType").focus();
		clear();
	})
});//close document.ready

function editClick(id,type,description){	

	 showAddTab()
    $("#btnReset").hide();
    $("#btnSubmit").hide();
    $('#tabAddSpecimenType').html("+Update Specimen Type");
	
	$("#btnUpdate").removeAttr("style");
	$("#txtSpecimenTypeError").text("");
    $("#txtSpecimenType").removeClass("errorStyle");
	$('#txtSpecimenType').val(type);
	$('#txtDescription').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id);

	$("#uploadFile").hide();
	//Click function for update button 
	$("#btnUpdate").click(function(){
		var flag = "false";
		
		$("#txtSpecimenType").val($("#txtSpecimenType").val().trim());
		
		if ($("#txtSpecimenType").val() == '') {
			$('#txtSpecimenType').focus();
			$("#txtSpecimenTypeError").text("Please enter specimen type");
			$("#txtSpecimenType").addClass("errorStyle");
			flag = "true";
		}
		if(flag == "true"){			
			return false;
		}
		else{
			var specimenType = $("#txtSpecimenType").val();
			var description = $(" #txtDescription").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/specimen_type.php",
					data :{            
						"operation" : "update",
						"id" : id,
						"specimenType":specimenType,
						"description":description
					},
					success: function(data) {	
						if(data != "0" && data != ""){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Specimen type updated Successfully!!!");
							$('#messagemyModal').modal();
							tabSpecimenTypelist();
						}
						else{
							$("#txtSpecimenTypeError").text("Specimen type already exists");
							$('#txtSpecimenType').focus();
							$("#txtSpecimenType").addClass("errorStyle");
							
						}
					},
					error:function()
					{
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
				});
			});
		}
	});
	
	$('#myModal #txtSpecimenType').keyup(function() {
        if ($("#myModal #txtSpecimenType").val() != '') {
            $("#myModal #txtSpecimenTypeError").text("");
            $("#myModal #txtSpecimenType").removeClass("errorStyle");
        }
    });
}

//Function for delete the data 
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete Specimen Type");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal();
	
	var type="delete";
	$('#confirm').attr('onclick','deleteSpecimenType("'+type+'");');
}

//Function for restore the data 
function restoreClick(id){
	$('#selectedRow').val(id);
	$('.modal-body').text("");	
	$('#confirmMyModalLabel').text("Restore Specimen Type");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();
	
	var type="restore";
	$('#confirm').attr('onclick','deleteSpecimenType("'+type+'");');	//Calling tabSpecimenTypelist function for restore 
}

//Function for delete and restore the data 
function tabSpecimenTypelist(){
	$("#form_dept").hide();
	$(".blackborder").show();
	$('#inactive-checkbox-tick').prop('checked', false).change();
	clear();	
	$("#tabAddSpecimenType").removeClass('tab-detail-add');
    $("#tabAddSpecimenType").addClass('tab-detail-remove');
    $("#tabSpecimenTypelist").removeClass('tab-list-remove');    
    $("#tabSpecimenTypelist").addClass('tab-list-add');
    $("#specimen_type_list").addClass('list'); 

    $("#btnReset").show();
    $("#btnSubmit").show();
    $('#btnUpdate').hide();
    $("#uploadFile").show();
    $('#tabAddSpecimenType').html("+Add Specimen Type");

	$("#txtSpecimenTypeError").text("");
	$("#txtSpecimenType").removeClass("errorStyle");
}

function showAddTab(){
    $("#form_dept").show();
    $(".blackborder").hide();
   
    $("#tabAddSpecimenType").addClass('tab-detail-add');
    $("#tabAddSpecimenType").removeClass('tab-detail-remove');
    $("#tabSpecimenTypelist").removeClass('tab-list-add');
    $("#tabSpecimenTypelist").addClass('tab-list-remove');
    $("#specimen_type_list").addClass('list');
	$('#txtSpecimenType').focus();
    removeErrorMessage();
}

function deleteSpecimenType(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();	
			
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/specimen_type.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Specimen type deleted successfully!!!");
					specimenTypeTable.fnReloadAjax();
					$('#messagemyModal').modal();				 
				}
				else{
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("This specimen type is used , so you can not delete!!!");
					$('#messagemyModal').modal();
				}
			},
			error:function()
			{
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/specimen_type.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Specimen type restored successfully!!!");
					specimenTypeTable.fnReloadAjax();
					$('#messagemyModal').modal();
				}			
			},
			error:function()
			{
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal(); 
			}
		});
	}
}

// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) {  
		 $(".close").click();
    }
});

//Function for clear the data
function clear(){
	$('#txtSpecimenType').val("");
	$('#txtSpecimenTypeError').text("");
	$('#txtFileError').text('');
	$('#txtDescription').val("");
}

function loadDataTable(){
	specimenTypeTable = $('#specimenTypeDataTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {
			$('.update').unbind();
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = specimenTypeTable.fnGetData(data);
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					var type = mData["type"];
					var description = mData["description"];
					editClick(id,type,description);
   
				}
			});
			$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  specimenTypeTable.fnGetData(data);
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);
				}
			});
		},
		
		"sAjaxSource":"controllers/admin/specimen_type.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{ "mData": "type" },
			{ "mData": "description" },
			{  
				"mData": function (o) { 
				var data = o;
				return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
				   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
				   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
				   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
				   "  data-original-title='Delete'></i>"; 
				}
			},	
		],
		aoColumnDefs: [
		   { 'bSortable': false, 'aTargets': [ 1 ] },
		   { 'bSortable': false, 'aTargets': [ 2 ] }
	   ]
	});
}
//# sourceURL=filename.js