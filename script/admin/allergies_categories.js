/*
 * File Name    :   allergies_categories.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre operation
 */
var allergyCategoryTable; //defile variable for allergyCategoryTable 
$(document).ready(function() {
	loader();
    $('#btnReset').click(function() {
        $("#txtCategories").removeClass("errorStyle");
        clear();
        $("#txtCategories").focus();
    });

    $("#formAllergyCategory").hide();

    $("#divAllergyListist").addClass('list');    
    $("#tabAllergyLst").addClass('tab-list-add');

   
    $("#tabAllergyCategory").click(function() { // show the add allergies categories tab
        $("#formAllergyCategory").show();
        $(".blackborder").hide();

        $("#tabAllergyCategory").addClass('tab-detail-add');
        $("#tabAllergyCategory").removeClass('tab-detail-remove');
        $("#tabAllergyLst").removeClass('tab-list-add');
        $("#tabAllergyLst").addClass('tab-list-remove');
        $("#divAllergyListist").addClass('list');

        
        $("#txtCategoryError").text("");
        $("#txtCategories").removeClass("errorStyle");
        $("#txtCategories").focus();
        clear();
    });

    $("#tabAllergyLst").click(function() { // show allergies list tab
        showTableList();		
    });
	
	// on change of select file type
	$("#file").on("change", function() {
		$('#txtFileError').hide();
	});
	
	// import excel on submit click
	$('#importDataFile').click(function(){
		var flag = false;
		if($('#file').prop('files')[0] == undefined || $('#file').val() == "") {
			$('#txtFileError').show();
			$('#txtFileError').text("Please choose file.");
			flag  = true;
		}
		if(flag == true){
			return false;
		}
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to upload this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			var file_data = $('#file').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: 'controllers/admin/allergies_categories.php', // point to server-side PHP script 
				dataType: 'text', // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					if(data != "" && data != "invalid file" && data =="1"){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Your file has been successfully imported!!!");
						$('#messagemyModal').modal();
						$('#file').val("");
					}
					else if(data == "invalid file"){
						$('#txtFileError').show();
						$('#txtFileError').text("Please import only CSV/XLS file.");
						$('#file').val("");
					}
                    else if (data =="You must have two column in your file i.e category and description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="First column should be category" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Second column should be description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Data can't be null in category column" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Please insert data in first column and second column only i.e A & B") {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
					else if(data == ""){
						$('#txtFileError').show();
						$('#txtFileError').text("Data already exist.");
						$('#file').val("");
					}
				},
				error: function() {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Data Not Found!!!");
					$('#messagemyModal').modal();

				}
			});
		});
	});
	
    // save details with validation
    $("#btnAddAllergyCategory").click(function() {

        var flag = "false";
        if ($("#txtCategories").val().trim() == '') {
            $("#txtCategoryError").text("Please enter allergy category");
            $("#txtCategories").focus();
            $("#txtCategories").addClass("errorStyle");
            flag = "true";
        } else {
            var allergiesCategories = $("#txtCategories").val().trim();
            var description = $("#txtDesc").val();
            description = description.replace(/'/g, "&#39");
            $.ajax({ // ajax call for categories validation
                type: "POST",
                cache: false,
                url: "controllers/admin/allergies_categories.php",
                datatype: "json",
                data: {
                    allergiesCategories: allergiesCategories,
                    Id: "",
                    operation: "checkallergiescategory"
                },
                success: function(data) {
                    if (data == "1") {
                        $("#txtCategoryError").show();
                        $("#txtCategoryError").text("This category is already exist");
                        $("#txtCategories").addClass("errorStyle");
                        $("#txtCategories").focus();
                    } else {
                        //ajax call for insert data into data base
                        var postData = {
                            "operation": "save",
                            "allergiesCategories": allergiesCategories,
                            "description": description
                        }
                        $.ajax({
                            type: "POST",
                            cache: false,
                            url: "controllers/admin/allergies_categories.php",
                            datatype: "json",
                            data: postData,

                            success: function(data) {
                                if (data != "0" && data != "") {
                                    $('.modal-body').text("");
                                    $('#messageMyModalLabel').text("Success");
                                    $('.modal-body').text("Allergy category saved successfully!!!");
                                    $('#messagemyModal').modal();
									$('#inactive-checkbox-tick').prop('checked', false).change();
                                    clear();

                                    //after sucessful data sent to database redirect to datatable list
                                    $("#formAllergyCategory").hide();
                                    $(".blackborder").show();
                                    $("#tabAllergyCategory").css({
                                        "background": "#fff",
                                        "color": "#000"
                                    });
                                    $("#tabAllergyLst").css({
                                        "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
                                        "color": "#fff"
                                    });
                                }

                            },
                            error: function() {
                                alert('error');
                            }
                        }); //  end ajax call
                    }
                },
                error: function() {
					
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
            });
        }
    }); //end button click function

    //remove validation style
    $("#txtCategories").keyup(function() {
        if ($("#txtCategories").val() != '') {
            $("#txtCategoryError").text("");
            $("#txtCategories").removeClass("errorStyle");
        }
    });
    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code
       loadDataTable(); 
    }
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            allergyCategoryTable.fnClearTable();
            allergyCategoryTable.fnDestroy();
            allergyCategoryTable = "";
            allergyCategoryTable = $('#allergyCategoryDataTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
					$('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = allergyCategoryTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            restoreClick(id);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/allergies_categories.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    /* {  "sWidth": "3%","mData": "id" }, */
                    {
                        "sWidth": "7%",
                        "mData": "category"
                    }, {
                        "sWidth": "15%",
                        "mData": "description"
                    }, {
                        "sWidth": "3%",
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1]
                }, {
                    'bSortable': false,
                    'aTargets': [2]
                }]
            });
        } else { // show active data on unchecked	
            allergyCategoryTable.fnClearTable();
            allergyCategoryTable.fnDestroy();
            allergyCategoryTable = "";
            loadDataTable();
        }
    });

});
// edit data dunction for update 
function editClick(id, category, description) {

    $("#tabAllergyCategory").click();	   
    $("#txtCategories").focus();

    $("#uploadFile").hide();
    $("#btnReset").hide();
    $("#btnAddAllergyCategory").hide();
    $('#tabAllergyCategory').html("+Update Allergy Category");

    $("#btnUpdate").removeAttr("style");
    $("#txtCategories").removeClass("errorStyle");
    $("#txtCategoryError").text("");
    $('#txtCategories').val(category);
    $('#txtDesc').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
    //validation
   
    $("#btnUpdate").click(function() { // click update button
        var flag = "false";
        if ($("#txtCategories").val().trim() == '') {
            $("#txtCategoryError").text("Please enter allergy category");
            $("#txtCategories").focus();
            $("#txtCategories").addClass("errorStyle");
            flag = "true";
        }
		else {
            var allergiesCategories = $("#txtCategories").val().trim();
            var description = $("#txtDesc").val();
            description = description.replace(/'/g, "&#39");
            var id = $('#selectedRow').val();
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				$.ajax({ // ajax call for Staff Type validation
					type: "POST",
					cache: false,
					url: "controllers/admin/allergies_categories.php",
					datatype: "json",
					data: {

						allergiesCategories: allergiesCategories,
						Id: id,
						operation: "checkallergiescategory"
					},
					success: function(data) {
						if (data == "1") {
							$("#txtCategoryError").show();
							$("#txtCategoryError").text("This category is already exist");
							$("#txtCategories").addClass("errorStyle");
							$("#txtCategories").focus();
						} else {
							var postData = {
								"operation": "update",
								"allergiesCategories": allergiesCategories,
								"description": description,
								"id": id
							}
							$.ajax( //ajax call for update data
								{
									type: "POST",
									cache: false,
									url: "controllers/admin/allergies_categories.php",
									datatype: "json",
									data: postData,

									success: function(data) {
										if (data != "0" && data != "") {
											$('#myModal').modal('hide');
											$('.close-confirm').click();
											$('.modal-body').text("");
											$('#messageMyModalLabel').text("Success");
											$('.modal-body').text("Allergy category updated successfully!!!");
											$('#messagemyModal').modal();
                                             $("#tabAllergyLst").click();
										}
									},
									error: function() {
										$('.close-confirm').click();
										$('.modal-body').text("");
										$('#messageMyModalLabel').text("Error");
										$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
										$('#messagemyModal').modal();
									}
								}); // end of ajax
						}
					},
					error: function() {
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
				});
			});
        }
    });
} // end update button

function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Allergy Category");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteDisease("' + type + '");');
} // end click fucntion

function restoreClick(id) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Allergy Category");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteDisease("' + type + '");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
// clear function
function clear() {
    $('#txtCategories').val("");
    $('#txtCategoryError').text("");
    $('#txtFileError').text("");
    $('#txtDesc').val("");
    $("#txtCategories").removeClass("errorStyle");
}
// define that function perform for delete and restore event
function deleteDisease(type) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete		
            type: "POST",
            cache: false,
            url: "controllers/admin/allergies_categories.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Allergy category deleted successfully!!!");
                    $('#messagemyModal').modal();
                    allergyCategoryTable.fnReloadAjax();
                } else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This diseases category is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/allergies_categories.php",
            data: {
                "operation": "restore",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Allergy category restored successfully!!!");
                    $('#messagemyModal').modal();
                    allergyCategoryTable.fnReloadAjax();

                }
            },
            error: function() {				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    }
}

function loadDataTable(){
	allergyCategoryTable = $('#allergyCategoryDataTable').dataTable({
		"bFilter": true,
		"processing": true,
		"sPaginationType": "full_numbers",
		"fnDrawCallback": function(oSettings) {
			// perform update event
			$('.update').unbind();
			$('.update').on('click', function() {
				var data = $(this).parents('tr')[0];
				var mData = allergyCategoryTable.fnGetData(data);
				if (null != mData) // null if we clicked on title row
				{
					var id = mData["id"];
					var category = mData["category"];
					var description = mData["description"];
					editClick(id, category, description);

				}
			});
			//perform delete event
			$('.delete').unbind();
			$('.delete').on('click', function() {
				var data = $(this).parents('tr')[0];
				var mData = allergyCategoryTable.fnGetData(data);

				if (null != mData) // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);
				}
			});
		},
		"sAjaxSource": "controllers/admin/allergies_categories.php",
		"fnServerParams": function(aoData) {
			aoData.push({
				"name": "operation",
				"value": "show"
			});
		},
		"aoColumns": [
			/* {  "sWidth": "3%","mData": "id" }, */
			{
				"sWidth": "7%",
				"mData": "category"
			}, {
				"sWidth": "15%",
				"mData": "description"
			}, {
				"sWidth": "3%",
				"mData": function(o) {
					var data = o;
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
						" style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
						" <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
						" style='font-size: 22px; color:#a94442; cursor:pointer;' " +
						" data-original-title='Delete'></i>";
				}
			},
		],
		aoColumnDefs: [{
			'bSortable': false,
			'aTargets': [1]
		}, {
			'bSortable': false,
			'aTargets': [2]
		}]

	});
}

function showTableList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();

    $("#formAllergyCategory").hide();
    $(".blackborder").show();


    $("#tabAllergyCategory").removeClass('tab-detail-add');
    $("#tabAllergyCategory").addClass('tab-detail-remove');
    $("#tabAllergyLst").removeClass('tab-list-remove');    
    $("#tabAllergyLst").addClass('tab-list-add');
    $("#divAllergyListist").addClass('list');


    $("#uploadFile").show();
    $("#btnReset").show();
    $("#btnAddAllergyCategory").show();
    $('#btnUpdate').hide();
    $('#tabAllergyCategory').html("+Add Allergy Category");
    clear();
}