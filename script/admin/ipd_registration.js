$(document).ready(function(){
    loader();

    //call function to load ward

    bindWard();
    //on changing the ward call bind Room() function

    $("#selWard").change(function(){
        var htmlRoom ="<option value='-1'>Select</option>";
        var htmlBed ="<option value='-1'>Select</option>";

        if ($("#selWard:selected").val() !=-1) {
            var value = $('#selWard :selected').val();
            bindRoom(value);
             bindBed(value);
        }
        else{
            $('#selRoomNo').html(htmlRoom);
            $('#selBed').html(htmlBed);
        }
    });

    $("#selRoomNo").change(function(){
        var htmlBed="<option value='-1'>Select</option>";
        if ($("#selRoomNo :selected").val() !=-1) {
            var value = $('#selRoomNo :selected').val();
            bindBed(value);
        }
        else{
            $('#selBed').html(htmlBed);
        }
    });
	// show the department name in select box
    $.ajax({     
        type: "POST",
        cache: false,
        url: "controllers/admin/ipd_registration.php",
        data: {
        "operation":"showDepartmentName"
        },
        success: function(data) { 
        if(data != null && data != ""){
            var parseData= jQuery.parseJSON(data);

            var option ="<option value='-1'>Select</option>";
            for (var i=0;i<parseData.length;i++)
            {
            option+="<option value='"+parseData[i].id+"'>"+parseData[i].department+"</option>";
            }
            $('#selDepartment').html(option);          
            }
        },
        error:function(){
        }
    });	
	
	$("#btnBookIpd").click(function(){     
		if ($("#selRoomNo").val() == '-1') {
			$("#selRoomNoError").text("Please select room No.");
			$("#selRoomNo").css({"border-color":"red"});           
		}
		if ($("#selBed").val() == '-1') {
			$("#selBedError").text("Please choose bed");
			$("#selBed").css({"border-color":"red"});
			
		}
		if ($("#selDepartment").val() == '-1') {
			$("#selDepartmentError").text("Please choose department");
			$("#selDepartment").css({"border-color":"red"});
			
		}
		if ($("#selWard").val() == '-1') {
			$("#selWardError").text("Please choose ward");
			$("#selWard").css({"border-color":"red"});
			
		}
		return true;
	});


	$("#selRoomNoNo").change(function(){
		if ($("#selRoomNoNo").val() != '-1') {
			$("#selRoomNoNoError").text("");
			$("#selRoomNoNo").removeAttr("style");          
		}
		else{
			$("#selRoomNoNoError").text("Please choose room No.");
			$("#selRoomNoNo").css({"border-color":"red"});
		}
	}); 
	$("#selBed").change(function(){
		if ($("#selBed").val() != '-1') {
			$("#selBedError").text("");
			$("#selBed").removeAttr("style");          
		}
		else{
			$("#selBedError").text("Please choose bed");
			$("#selBed").css({"border-color":"red"});
		}
	});  
	$("#selDepartment").change(function(){
		if ($("#selDepartment").val() != '-1') {
			$("#selDepartmentError").text("");
			$("#selDepartment").removeAttr("style");          
		}
		else{
			$("#selDepartmentError").text("Please choose department.");
			$("#selDepartment").css({"border-color":"red"});
		}
	});   
	$("#selWard").change(function(){
		if ($("#selWard").val() != '-1') {
			$("#selWardError").text("");
			$("#selWard").removeAttr("style");          
		}
		else{
			$("#selWardError").text("Please choose ward");
			$("#selWard").css({"border-color":"red"});
		}
	}); 

	// ajax for show value in text box from data base	
	
	$("#btnSelect").click(function(){
		var flag = "false";
        if ($("#textHrn").val() == '') {
            $("#txthrnError").text("Please enter hrn number");
            $("#textHrn").css({
                "border-color": "red"
            });
            flag = "true";
        }
		if(flag == "true"){			
        return false;
		}
		var getHRN = $("#textHrn").val();
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/ipd_registration.php",
			data :{            
				"operation" : "selectHRNInfo",
				"GetHRN" : getHRN
			},
			success: function(data) { 
				if(data != "0"){		 
					var parseData = jQuery.parseJSON(data);
			
					for (var i=0;i<parseData.length;i++) {
    					$("#txtFirstName").val(parseData[i].first_name);
    					$("#txtLastName").val(parseData[i].last_name);
    					$("#txtMobile").val(parseData[i].mobile);
					var getDOB = parseData[i].dob;
					}    
					var new_age = getAge(getDOB);

					var split = new_age.split(' ');
					var age_years = split[0];
					var age_month = split[1];
					var age_day = split[2];

					$("#txt_year").val(age_years);
					$("#txt_month").val(age_month);
					$("#txt_day").val(age_day); 
				} 
			    else{
					 $('#myModalLabel').text("Success");
					 $('.modal-body').text("Data doen exists..");
					 $('#modal').click();
					 clear();
				} 
	   
			},
			error: function(){
				$('#myModalLabel').text("Success");
				$('.modal-body').text("Error!!!");
				$('#modal').click();
			}
		}); 
	});
	$("#textHrn").keyup(function(){
		if ($("#textHrn").val() != '') {
			$("#txthrnError").text("");
			$("#textHrn").removeAttr("style");
		}
		else {
			$("#txthrnError").text("Please enter hrn number");
			$("#textHrn").css({
				"border-color": "red"
			});
		}
	});
});

function getAge(dateString) {
    var now = new Date();
    var today = new Date(now.getYear(), now.getMonth(), now.getDate());

    var yearNow = now.getYear();
    var monthNow = now.getMonth();
    var dateNow = now.getDate();

    var dob = new Date(dateString.substring(0, 4), dateString.substring(5, 7) - 1, dateString.substring(8, 10));

    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();
    var age = {};
    var ageString = "";
    var yearString = "";
    var monthString = "";
    var dayString = "";


    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    age = {
        years: yearAge,
        months: monthAge,
        days: dateAge
    };

    if (age.years > 1) yearString = " years";
    else yearString = " year";
    if (age.months > 1) monthString = " months";
    else monthString = " month";
    if (age.days > 1) dayString = " days";
    else dayString = " day";


    if ((age.years > 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else ageString = "Oops! Could not calculate age!";

    return ageString;
}

//function for wind ward
function bindWard(){
    $.ajax({                    
        type: "POST",
        cache: false,
        url: "controllers/admin/ipd_registration.php",
        data: {
            "operation":"showWard"
        },
        success: function(data) {   
            if(data != null && data != ""){
                var parseData= jQuery.parseJSON(data);
            
                var option ="<option value='-1'>Select</option>";
                for (var i=0;i<parseData.length;i++)
                {
                option+="<option value='"+parseData[i].id+"'>"+parseData[i].name+"</option>";
                }
                $('#selWard').html(option);             
                
            }
        },
        error:function(){
            
        }
    });
}

//function for bind room
function bindRoom(value){
    $.ajax({                    
        type: "POST",
        cache: false,
        url: "controllers/admin/ipd_registration.php",
        data: {
            "operation":"showRoom",
             ward : value
        },
        success: function(data) {   
            if(data != null && data != ""){
                var parseData= jQuery.parseJSON(data);
            
                var htmlRoom ="<option value='-1'>Select</option>";
                for (var i=0;i<parseData.length;i++)
                {
                htmlRoom+="<option value='"+parseData[i].id+"'>"+parseData[i].number+"</option>";
                }
                $('#selRoomNo').html(htmlRoom);
            }
        },
        error:function(){           
        }
    }); 
}

function bindBed(value){
    $.ajax({                    
        type: "POST",
        cache: false,
        url: "controllers/admin/ipd_registration.php",
        data: {
            "operation":"showBed",
             room : value
        },
        success: function(data) {   
            if(data != null && data != ""){
                var parseData= jQuery.parseJSON(data);
            
                var htmlBed ="<option value='-1'>Select</option>";
                for (var i=0;i<parseData.length;i++)
                {
                htmlBed+="<option value='"+parseData[i].id+"'>"+parseData[i].number+"</option>";
                }
                $('#selBed').html(htmlBed);
            }
        },
        error:function(){           
        }
    }); 
}


//clear function
function clear()
{
    $("#textHrn").val("");
    $("#txtFirstName").val("");
    $("#txtLastName").val("");
    $("#txtMobile").val("");
    $("#txt_year").val("");
    $("#txt_month").val("");
    $("#txt_day").val("");
}
