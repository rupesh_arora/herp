/*
 * File Name    :   diseases.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre opeartion	
 */
var diseaseTable; //define variable for diseaseTable 
$(document).ready(function() {
	loader();
    debugger;
    $("#form_diseases").hide();

    $("#diseases_list").addClass('list');    
    $("#tab_diseases").addClass('tab-list-add');

   
    $("#tab_add_diseases").click(function() { // show the add diseases tab
        $("#form_diseases").show();
        $(".blackborder").hide();
        $("#tab_add_diseases").addClass('tab-detail-add');
        $("#tab_add_diseases").removeClass('tab-detail-remove');
        $("#tab_diseases").removeClass('tab-list-add');
        $("#tab_diseases").addClass('tab-list-remove');
        $("#diseases_list").addClass('list');

        $("#txtCategoryError").text("");
        $("#txtCategory").removeClass("errorStyle");
        $("#txtCodeError").text("");
        $("#txtCode").removeClass("errorStyle");
        $("#txtNameError").text("");
        $("#txtName").removeClass("errorStyle");
        $("#txtCode").focus();
    });
    $("#tab_diseases").click(function() { // show the diseases list tab
    	showTableList();		
    });

    /* // Ajax call for show data on Diseases Category
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/diseases.php",
        data: {
            "operation": "showcategory"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].category + "</option>";
                }
                $('#txtCategory').html(option);
            }
        },
        error: function() {}
    }); */
	
	// on change of select file type
	$("#file").on("change", function() {
		$('#txtFileError').hide();
	});
	
	// import excel on submit click
	$('#importData').click(function(){
		var flag = false;
		if($('#file').prop('files')[0] == undefined || $('#file').val() == "") {
			$('#txtFileError').show();
			$('#txtFileError').text("Please choose file.");
			flag  = true;
		}
		if(flag == true){
			return false;
		}
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to upload this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			var file_data = $('#file').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: 'controllers/admin/diseases.php', // point to server-side PHP script 
				dataType: 'text', // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					if(data != "" && data != "invalid file" && data == "1"){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Your file has been successfully imported!!!");
						$('#messagemyModal').modal();
						$('#file').val("");
					}
                    else if (data =="You must have three column in your file i.e disease code, disease name and description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="First column should be disease code" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Second column should be disease name" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Third column should be description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if(data == "Data can't be null in code and name column"){
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Please insert data in first , second and third column only i.e A , B & C") {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
					else if(data == "invalid file"){
						$('#txtFileError').show();
						$('#txtFileError').text("Please import only CSV/XLS file.");
						$('#file').val("");
					}
					else if(data == ""){
						$('#txtFileError').show();
						$('#txtFileError').text("Data already exist.");
						$('#file').val("");
					}
				},
				error: function() {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Data Not Found!!!");
					$('#messagemyModal').modal();

				}
			});
		});
	});
    $("#btnAddDiseases").click(function() { // save details with validation
        var flag = "false";
        /* if ($("#txtCategory").val() == '') {
            $("#txtCategoryError").text("Please select category");
            $("#txtCategory").focus();
            $("#txtCategory").addClass("errorStyle");
            flag = "true";
        } */
        if ($("#txtName").val().trim() == '') {
            $("#txtNameError").text("Please enter disease name");
            $("#txtName").focus();
            $("#txtName").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtCode").val().trim() == '') {
            $("#txtCodeError").text("Please enter code");
            $("#txtCode").focus();
            $("#txtCode").addClass("errorStyle");
            flag = "true";
        }


        if (flag == "true") {
            return false;
        } else {
            var code = $("#txtCode").val().trim();
            var name = $("#txtName").val().trim();
            var description = $("#txtDescription").val();
			description = description.replace(/'/g, "&#39");
            $.ajax({ // ajax call for disease duplicacy validation
                type: "POST",
                cache: false,
                url: "controllers/admin/diseases.php",
                datatype: "json",
                data: {
                    Code: code,
                    Name: name,
					"description": description,
                    Id: "",
                    operation: "checkdiseases"
                },
                success: function(data) {
                    if (data == "1") {
                        $("#txtCodeError").text("Code is already exists");
                        $("#txtCode").addClass("errorStyle");
                        $("#txtCode").focus();
                    } else {
                        //ajax call for insert data into data base
                        var postData = {
                            "operation": "save",
                            Code: code,
                            Name: name,
							"description": description
                        }
                        $.ajax({
                            type: "POST",
                            cache: false,
                            url: "controllers/admin/diseases.php",
                            datatype: "json",
                            data: postData,

                            success: function(data) {
                                if (data != "0" && data != "") {
                                    $('.modal-body').text("");
                                    $('#messageMyModalLabel').text("Success");
                                    $('.modal-body').text("Disease saved successfully!!!");
                                    $('#messagemyModal').modal();

                                    showTableList();
                                }
                                if (data == "") {
                                    $("#txtName").addClass("errorStyle");
                                    $("#txtNameError").text("Disease name already exists");
                                    $("#txtName").focus();
                                }

                            },
                            error: function() {
                                alert('error');
                            }
                        }); //  end ajax call
                    }
                },
                error: function() {
					
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
            });
        }
    }); //end button click function

    // key up and change functionality for remove style 
    $("#txtCode").keyup(function() {
        if ($("#txtCode").val() != '') {
            $("#txtCodeError").text("");
            $("#txtCode").removeClass("errorStyle");
        }
    });
    $("#txtName").keyup(function() {
        if ($("#txtName").val() != '') {
            $("#txtNameError").text("");
            $("#txtName").removeClass("errorStyle");
        }
    });
    /* $("select").on('change', function() {

        if ($("#txtCategory").val() != "") {
            $("#txtCategoryError").text("");
            $("#txtCategory").removeClass("errorStyle");

        }
    }); */

    if ($('.inactive-checkbox').not(':checked')) { // show active details on load 
        //Datatable code
        loadDataTable();
    }
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show inactive details on checked event
            diseaseTable.fnClearTable();
            diseaseTable.fnDestroy();
            diseaseTable = "";
            diseaseTable = $('#diseasesdatatable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "bAutoWidth":false,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
					$('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = diseaseTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            restoreClick(id);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/diseases.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    /* {  "mData": "id" }, */
                    {
                        "mData": "code"
                    }, {
                        "mData": "name"
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [2,3]
                }]
            });
        } else { // show active details unchecked event
            diseaseTable.fnClearTable();
            diseaseTable.fnDestroy();
            diseaseTable = "";
            loadDataTable();
        }
    });
    // perform reset functionality on reset click event	
    $("#btnReset").click(function() {
        $("#txtCategory").removeClass("errorStyle");
        $("#txtCodeError").text("");
        $("#txtCode").removeClass("errorStyle");
        $("#txtNameError").text("");
        $("#txtName").removeClass("errorStyle");
        $("#txtCode").focus();
        clear();
    });

});
// edit data dunction for update 
function editClick(id, code, name, description) {
	

    $("#tab_add_diseases").click();
    $("#uploadFile").hide();
    $("#btnReset").hide();
    $("#btnAddDiseases").hide();
    $('#tab_add_diseases').html("+Update Disease");

    $("#txtCode").focus();
    $("#btnUpdate").removeAttr("style");
    $("#txtCode").removeClass("errorStyle");
    $("#txtCodeError").text("");
    $("#txtName").removeClass("errorStyle");
    $("#txtNameError").text("");

    $('#txtCode').val(code);
    $('#txtName').val(name);
    if(description != null){
    	$('#txtDescription').val(description.replace(/&#39/g, "'"));
    }
	
    $('#selectedRow').val(id);

    // key up validation
    $("#txtCode").keyup(function() {
        if ($("#txtCode").val() != '') {
            $("#txtCodeError").text("");
            $("#txtCode").removeClass("errorStyle");
        }
    });
    $("#txtName").keyup(function() {
        if ($("#txtName").val() != '') {
            $("#txtNameError").text("");
            $("#txtName").removeClass("errorStyle");
        }
    });
    //validation
    $("#btnUpdate").click(function() { // click update button
        var flag = "false";
        if ($("#txtName").val().trim() == '') {
            $("#txtNameError").text("Please enter disease name");
            $("#txtName").focus();
            $("#txtName").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtCode").val().trim() == '') {
            $("#txtCodeError").text("Please enter code");
            $("#txtCode").focus();
            $("#txtCode").addClass("errorStyle");
            flag = "true";
        }


        if (flag == "true") {
            return false;
        }
		else {
            var id = $('#selectedRow').val();
            var code = $("#txtCode").val().trim();
            var name = $("#txtName").val().trim();
			var description = $("#txtDescription").val();
			description = description.replace(/'/g, "&#39");			
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				$.ajax({ // ajax call for diseases duplicacy validation
					type: "POST",
					cache: false,
					url: "controllers/admin/diseases.php",
					datatype: "json",
					data: {
						Code: code,
						Name: name,
						Id: id,
						"description": description,
						operation: "checkdiseases"
					},
					success: function(data) {
						if (data == "1") {
							$("#txtCodeError").text("Code is already exists");
							$("#txtCode").addClass("errorStyle");
							$("#txtCode").focus();

						} else {
							var postData = {
								"operation": "update",
								Code: code,
								Name: name,
								"description": description,
								Id: id
							}
							$.ajax( //ajax call for update data
							{
								type: "POST",
								cache: false,
								url: "controllers/admin/diseases.php",
								datatype: "json",
								data: postData,

								success: function(data) {
									if (data != "0" && data != "") {
										$('#myModal').modal('hide');
										$('.close-confirm').click();
										$('.modal-body').text("");
										$('#messageMyModalLabel').text("Success");
										$('.modal-body').text("Disease updated successfully!!!");
										$('#messagemyModal').modal();
										 $("#tab_diseases").click();

									}
									if (data == "") {
										$('.close-confirm').click();
										$("#txtName").addClass("errorStyle");
										$("#txtNameError").text("Disease name already exists");
										$("#txtName").focus();
									}
								},
								error: function() {
									$('.close-confirm').click();
									//$('#myModal').modal('hide');
									$('.modal-body').text("");
									$('#messageMyModalLabel').text("Error");
									$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
									$('#messagemyModal').modal();
								}
							}); // end of ajax
						}
					},
					error: function() {
						alert("data not found");
					}
				});
			});
        }
    });
} // end update button

function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Disease");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);

    var type = "delete";
    $('#confirm').attr('onclick', 'deleteDiseases("' + type + '");');
} // end click fucntion

function restoreClick(id) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Disease");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteDiseases("' + type + '");');
}
// define function for restore and delete when this is call 
function deleteDiseases(type) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax( // ajax call for delete
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/diseases.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Disease deleted successfully!!!");
                        $('#messagemyModal').modal();
                        diseaseTable.fnReloadAjax();
                    } else {
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Sorry");
                        $('.modal-body').text("This diseases is used , so you can not delete!!!");
                        $('#messagemyModal').modal();
                    }
                },
                error: function() {
					
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
            }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/diseases.php",
            data: {
                "operation": "restore",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Disease restored successfully!!!");
                    $('#messagemyModal').modal();
                    diseaseTable.fnReloadAjax();
                }
                if (data == "0") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("It can not be restore!!!");
                    diseaseTable.fnReloadAjax();
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    }
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});

// define clear function
function clear() {
    $('#txtCode').val("");
    $('#txtCodeError').text("");
    $('#txtCode').removeClass("errorStyle");
    $('#txtName').val("");
    $('#txtNameError').text("");
    $('#txtName').removeClass("errorStyle");
    $('#txtCategory').val("");
    $('#txtDescription').val("");
	$('#file').val("");
	$('#txtFileError').hide();
}

function loadDataTable(){
	diseaseTable = $('#diseasesdatatable').dataTable({
		"bFilter": true,
		"processing": true,
		"bAutoWidth":false,
		"sPaginationType": "full_numbers",
		"fnDrawCallback": function(oSettings) {
			//perform update event
			$('.update').unbind();
			$('.update').on('click', function() {
				var data = $(this).parents('tr')[0];
				var mData = diseaseTable.fnGetData(data);
				if (null != mData) // null if we clicked on title row
				{
					var id = mData["id"];
					var code = mData["code"];
					var name = mData["name"];
					var description = mData["description"];
					editClick(id, code, name, description);

				}
			});
			// perform delete event
			$('.delete').unbind();
			$('.delete').on('click', function() {
				var data = $(this).parents('tr')[0];
				var mData = diseaseTable.fnGetData(data);

				if (null != mData) // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);
				}
			});
		},
		"sAjaxSource": "controllers/admin/diseases.php",
		"fnServerParams": function(aoData) {
			aoData.push({
				"name": "operation",
				"value": "show"
			});
		},
		"aoColumns": [{
			"mData": "code"
		}, {
			"mData": "name"
		}, {
			"mData": "description"
		}, {
			"mData": function(o) {
				var data = o;
				return '<i class="ui-tooltip fa fa-pencil update" style="font-size: 22px;cursor:pointer;" data-original-title="Edit"></i> <i class="ui-tooltip fa fa-trash-o delete" style="font-size: 22px; color:#a94442; cursor:pointer;" data-original-title="Delete"></i>';
			}
		}, ],
		aoColumnDefs: [{
			'bSortable': false,
			'aTargets': [2,3]
		}]

	});
}

function showTableList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();

    $("#form_diseases").hide();
    $(".blackborder").show();

    $("#tab_add_diseases").removeClass('tab-detail-add');
    $("#tab_add_diseases").addClass('tab-detail-remove');
    $("#tab_diseases").removeClass('tab-list-remove');    
    $("#tab_diseases").addClass('tab-list-add');
    $("#diseases_list").addClass('list');


    $("#uploadFile").show();
    $("#btnReset").show();
    $("#btnAddDiseases").show();
    $('#btnUpdate').hide();
    $('#tab_add_diseases').html("+Add Disease");
    clear();
}