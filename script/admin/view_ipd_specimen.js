/*view specimen.js*/
var otable;
$(document).ready(function(){
	loader(); 
	debugger;
	
	//varibale for time stammps
	var getTimeStamp;
	var getFromDateTimeStamp;
	var getToDateTimeStamp;
	var toDate;
	var fromDate;
	$('#txtFromDate').datepicker(function(){				
		autoclose: true
	});
	$('#txtToDate').datepicker(function(){				
		autoclose: true
	});
	$("#txtFromDate").click(function(){
		$(".datepicker").css({"z-index":"99999"});
	});
	$("#txtToDate").click(function(){
		$(".datepicker").css({"z-index":"99999"});
	});
	//datepicker function
	$("#btnReset").click(function(){
		clear();
	});

	//getting value on change function
	$("#txtFromDate").change(function(){
		fromDate=$("#txtFromDate").val();
		/* getFromDateTimeStamp = calculateTimeStamp(fromDate); */
	/* 	getFromDateTimeStamp = Date.parse(fromDate)/1000; */
		
	});
	$("#txtToDate").change(function(){
		toDate=$("#txtToDate").val();
		/* getToDateTimeStamp = calculateTimeStamp(toDate); */
		/* getToDateTimeStamp = Date.parse(toDate)/1000; */
	});
	
	otable = $('#viewSpecimenTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"bAutoWidth" : false,
		"sPaginationType":"full_numbers",	
	});
	
	$("#btnSearch").click(function(){
		var firstName = $ ("#txtFirstName").val().trim();
		var lastName = $ ("#txtLastName").val().trim();
		var mobile = $ ("#txtMobile").val().trim();
		var patientId = $("#txtPatientId").val().trim();

		var patientPrefix = patientId.substring(0, 3);
		patientId = patientId.replace ( /[^\d.]/g, '' ); 
		patientId = parseInt(patientId);
		if(isNaN(patientId)) {
			patientId = "";
		}
				
		var specimenID = $("#specimenID").val(); // hidden value for different popup screen
		if (specimenID == "1" ){
			labSpecimen(firstName,lastName,mobile,patientId,fromDate,toDate,otable);
		}
	});		
	
	
	$("#btnBackToList").click(function(){
		$("#searchVisit").show();
		$("#viewDetails").hide();
	});
	
	//Code for click on search icon  in pharmacy dispense
	$("#btnReset").click(function(){
		$("#txtFromDate").val("");
		$("#txtToDate").val("");	
		$("#txtSpecimenId").val("");	
		clear();
	});
	
	$("#btnBackToList").click(function(){
		$("#mySpecimenModal #searchSpecimen").show();
		$("#mySpecimenModal #viewDetails").hide();
	});
});
// lab specimen 
function labSpecimen(firstName,lastName,mobile,patientId,fromDate,toDate,otable){
	var postData = {
		"operation":"searchIPDSpecimen",
		"getFromDateTimeStamp" : fromDate,
		"getToDateTimeStamp" : toDate,
		"firstName" : firstName,
		"lastName" : lastName,
		"mobile" : mobile,
		"patientId" : patientId
	}

	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/view_specimen.php",
		datatype:"json",
		data: postData,
		
		success: function(data) {
			var dataSet = JSON.parse(data);
			//var dataSet = jQuery.parseJSON(data);
			otable.fnClearTable();
			otable.fnDestroy();
			otable = $('#viewSpecimenTable').DataTable({
				"sPaginationType":"full_numbers",
				"bAutoWidth" : false,
				"fnDrawCallback": function ( oSettings ) {
					$('#viewSpecimenTable tbody tr').on( 'dblclick', function () {

						if ( $(this).hasClass('selected') ) {
						  $(this).removeClass('selected');
						}
						else {
						  otable.$('tr.selected').removeClass('selected');
						   $(this).addClass('selected');
						}
						
						var mData = otable.fnGetData(this); // get datarow
						if (null != mData)  // null if we clicked on title row
						{
							//now aData[0] - 1st column(count_id), aData[1] -2nd, etc. 								
							
							$('#txtSpecimenId').val(mData["specimen_id"]);								
							$('#txtSpecimenId').attr("data-requestId",mData["lab_test_requested_id"]);								
						}							
						
						$("#btnSpecimenLoad").click();
						$(".close").click();									
					});

					$("#mySpecimenModal .visitDetails").on('click',function(){
						var id = $(this).attr('data-id');
						var postData = {
							"operation":"visitIPDDetailSpecimen",
							"id":id
						}
						
						$.ajax({					
							type: "POST",
							cache: false,
							url: "controllers/admin/view_specimen.php",
							datatype:"json",
							data: postData,
							
							success: function(data) {
								if(data != "0" && data != ""){
									var parseData = jQuery.parseJSON(data);

										$("#lblSpecimenId").text(parseData[0].specimen_id);
										$("#lblSpecimenType").text(parseData[0].type);
										var patientId = parseData[0].patient_id;
										var patientPrefix = parseData[0].patient_prefix;
											
										var patientIdLength = patientId.length;
										for (var i=0;i<6-patientIdLength;i++) {
											patientId = "0"+patientId;
										}
										patientid = patientPrefix+patientId;
										$("#lblPatientId").text(patientid);
										$("#lblPatientName").text(parseData[0].patient_name);
										$("#lblTestName").text(parseData[0].lab_test_name);
										$("#lblConsultantName").text(parseData[0].consultant_name);
										$("#lblMobile").text(parseData[0].mobile);										

										var visitId = parseInt(parseData[0].visit_id);
										var patientPrefix = parseData[0].prefix;
											
										var visitIdLength = visitId.toString().length;
										for (var j=0;j<6-visitIdLength;j++) {
											visitId = "0"+visitId;
										}
										visitid = patientPrefix+visitId;
										$("#lblVisitId").text(visitid);
			 
									$("#mySpecimenModal #searchSpecimen").hide();
									$("#mySpecimenModal #viewDetails").show();
								}
							},
							error:function() {
								$('#messageMyModalLabel').text("Error");
								$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
								$('#messagemyModal').modal();
							}
						});
					});
					
				},
				"aaData": dataSet,
				"aoColumns": [
					{ "mData": "specimen_id" },
					{"mData":  function (o) 
						{ 	
							var patientId = o["patient_id"];
							var patientPrefix = o["patient_prefix"];
								
							var patientIdLength = patientId.length;
							for (var i=0;i<6-patientIdLength;i++) {
								patientId = "0"+patientId;
							}
							patientid = patientPrefix+patientId;
							return patientid;	
						}
					},
					{ "mData": "type" },
					{ "mData": "name" },						
					
					{ "mData": function(o){return convertTimestamp(o["created_on"])}},
					{
					  "mData": function (o) { 				
							var data = o;
							var id = data["id"];	
							return "<i class='visitDetails fa fa-eye' id='btnView"+o['specimen_id']+"' title='View' data-id="+id+" "+ "></i>"; 
						}
					}
				]
			});			
		},
		error:function() {
			alert('error');
		}
	});
}
function convertTimestamp(timestamp) {
  var d = new Date(timestamp * 1000),	// Convert the passed timestamp to milliseconds
		yyyy = d.getFullYear(),
		mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
		dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
		hh = d.getHours(),
		h = hh,
		min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
		ampm = 'AM',
		time;
			
	if (hh > 12) {
		h = hh - 12;
		ampm = 'PM';
	} else if (hh === 12) {
		h = 12;
		ampm = 'PM';
	} else if (hh == 0) {
		h = 12;
	}
	
	// ie: 2013-02-18, 8:35 AM	
	time = yyyy + '-' + mm + '-' + dd + ' ' + h + ':' + min + ' ' + ampm;
		
	return time;
}
//function to calculate datestamp
function calculateTimeStamp(date){
	var myDate = date;
	myDate=myDate.split("-");
	var newDate=myDate[1]+"/"+myDate[2]+"/"+myDate[0]; //mm-dd-yyyy
	getTimeStamp=(new Date(newDate).getTime());	
	return getTimeStamp;
}

// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) { 
		 $(".close").click();
    }
});

function clear(){
	$("#txtFirstName").val("");
	$("#txtLastName").val("");
	$("#txtPatientId").val("");
	$("#txtMobile").val("");
	$("#txtFromDate").val("");
	$("#txtToDate").val("");
}