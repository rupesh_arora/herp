var customerBalanceTable = '';
var pendingInvoiceTable = '';
$(document).ready(function() {
	debugger;
	$(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});
	var arr=[];
	removeErrorMessage();//call from common.js

	customerBalanceTable = $('#customerBalanceTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "aaSorting": []
    });
    pendingInvoiceTable = $('#pendingInvoiceTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "aaSorting": []
    });

    $("#viewReport").on('click',function(){
    	var toDate = $("#toDate").val().trim();
		var fromDate = $("#fromDate").val().trim();
    	var postData = {
			"operation" : "showTotalBill",
			'toDate' : toDate,
			'fromDate' : fromDate
		}
		dataCall("./controllers/admin/customer_balance_statement.php", postData, function (result) {
			customerBalanceTable.fnClearTable();
			if (result.trim().length > 2) {
				var parseData = JSON.parse(result);				

		    	$.each(parseData,function(i,v){
		    		var customerId = v.customer_id;
                            
                   var customerIdlength = customerId.length;
                    for (var i=0;i<6-customerIdlength;i++) {
                        customerId = "0"+customerId;
                    }
                    customerId = customerPrefix+customerId;
		    		customerBalanceTable.fnAddData(['<span class="pointer" onclick="invoiceDetails($(this))";><b>'+customerId+'<b></span>',v.customer_name,v.balance]);
		    	});
			}
		});	
    });

    $("#clear").on('click',function(){
    	clear();
    });

    $("#backCustomer").on('click',function(){
    	$('#pendingInvoiceScreen').hide();
    	$('#customerBalanceTableWrapper').fadeIn('slow');
    });
});

function clear() {
	$("#toDate,#fromDate").val('');
	imprestBalTable.fnClearTable();
}
function invoiceDetails(myThis){	
	var postData = {
        "operation" : "showCustomerDetails",
        customerId : parseInt(($(myThis).text()).replace(/\D/g,''))
    }
	dataCall("controllers/admin/receipt_number.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (parseData != '') { 
            var grandTotal =0;         
            pendingInvoiceTable.fnClearTable();
            var id = '';
            var receiptNumber;
            totalamount = 0;
            openAmount = 0;
            tableOpenAmount = [];
            $.each(parseData,function(i,v){
                var obj = {};
                receiptNumber = v.id;
                var prefix = v.invoice_prefix;
                var idLength = 6-receiptNumber.length;
                for(j=0; j<idLength; j++){
                    receiptNumber="0"+receiptNumber;
                }
                receiptNumber=prefix+receiptNumber;
                if (v.id !=id) {
                    
                    pendingInvoiceTable.fnAddData(["<span class='details-control' onclick='showRows($(this));'></span>"+"<span class='hide-details-control hide' onclick='hideRows($(this));'></span>",receiptNumber,v.description,v.total_bill,v.total_bill,v.billing_date]);

                    var currData = pendingInvoiceTable.fnAddData(["<input type='checkbox' tr-class="+receiptNumber+" data-class='chkInvoice"+v.id+"'  class='chkProduct hideDetails' data-id="+v.id+"  productId="+v.customer_product_id+"  style='display:none; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative; margin-left: 42%;'"+                  
                                   " style='font-size: 22px; cursor:pointer;'>",v.name,"",v.total,v.total,v.billing_date]);

                    var getCurrDataRow = pendingInvoiceTable.fnSettings().aoData[ currData ].nTr;
                    $(getCurrDataRow).addClass('hideDetails '+(receiptNumber).replace(/ /g,''));
                    id = v.id;
                    grandTotal += parseFloat(v.total);
                    if(i != 0){
                        obj.openAmount = openAmount;
                        tableOpenAmount.push(obj);
                        openAmount =0;
                    }
                    openAmount += parseFloat(v.total);
                }

                else {                      

                    var currData = pendingInvoiceTable.fnAddData(["<input type='checkbox' tr-class="+receiptNumber+" data-class='chkInvoice"+v.id+"' class='chkProduct hideDetails' data-id="+v.id+" productId="+v.customer_product_id+"  style='display:none; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;  margin-left: 42%;'"+                   
                                   " style='font-size: 22px; cursor:pointer;'>",v.name,"",v.total,v.total,v.billing_date]);

                    var getCurrDataRow = pendingInvoiceTable.fnSettings().aoData[ currData ].nTr;
                    $(getCurrDataRow).addClass('hideDetails '+(receiptNumber).replace(/ /g,''));
                    grandTotal += parseFloat(v.total);
                     openAmount += parseFloat(v.total);
                    id = v.id;
                }
                if(i == parseData.length-1){
                    obj = {};
                    obj.openAmount = openAmount;
                    tableOpenAmount.push(obj);
                    openAmount =0;
                    k=1;
                }
            }); 
            if(k == 1){
                var airows = pendingInvoiceTable.$(".chkInvoice", {"page": "all"});    //Select all unchecked row only
                for(i=0; i<airows.length; i++){
                    $($(airows[i]).parent().siblings()[3]).text(tableOpenAmount[i].openAmount);
                }
            }
            $('#customerBalanceTableWrapper').hide();
            $('#pendingInvoiceScreen').fadeIn('slow');            
        }
    });
}
function showRows(myThis){
    var ledgerText = $(myThis).parent().siblings()[0];
    var ledgerClass = $(ledgerText).text().replace(/ /g,'');
    var parentTr = $(myThis).parent().parent()[0];
    var trClass = $(parentTr).parent().find("."+ledgerClass);
    $(parentTr).parent().find("."+ledgerClass).fadeIn(700);
    $(myThis).addClass('hide');
    $(myThis).siblings().removeClass('hide');
}
function hideRows(myThis){
    var ledgerText = $(myThis).parent().siblings()[0];
    var ledgerClass = $(ledgerText).text().replace(/ /g,'');
    var parentTr = $(myThis).parent().parent()[0];
    var trClass = $(parentTr).parent().find("."+ledgerClass);
    $(parentTr).parent().find("."+ledgerClass).fadeOut();
    $(myThis).addClass('hide');
    $(myThis).siblings().removeClass('hide');
}