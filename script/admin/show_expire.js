$(document).ready(function() {
	debugger;
    var expireItemTable;//data table variable
	bindInventoryType("#SelInventoryType");

	removeErrorMessage(); //remove error is defined in common.js

	expireItemTable = $('#tblShowExpire').dataTable({ // inilize datatable on load time.
        "bFilter": true,
        "processing": true,
        "aaSorting": [],
        "sPaginationType": "full_numbers",
        "bAutoWidth" : false,
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [3]
        }]
    });

   $("#btnShow").click(function(){
		var flag = false;
		if(validTextField('#SelInventoryType','#SelInventoryTypeError','Please select inventory type') == true)
        {
            flag = true;
        }
        if(flag == true) {
            return false;
        }
        var inventoryType = $("#SelInventoryType").val();
    	var postData = {
            inventoryType:inventoryType,
            "operation":"showExpireItem"
        }

        dataCall("controllers/admin/show_expire.php", postData, function (result){
        	expireItemTable.fnClearTable();
            if (result !='' && result.length > 2) {             
                
                var parseData = jQuery.parseJSON(result);

                for(var i=0;i<parseData.length;i++) {
                    var ItemName = parseData[i].item_name;
                    var SellerName = parseData[i].seller_name;
                    var MfgDate = parseData[i].mfg_date;
                    var ExpDate = parseData[i].exp_date;
                    if(MfgDate == '' || MfgDate == null){
                    	MfgDate = 'N/A';
                    }
                    if(ExpDate == '' || ExpDate == null){
                    	ExpDate = 'N/A';
                    }
                    expireItemTable.fnAddData([ItemName,SellerName,MfgDate,ExpDate]);                
                }
            }
            else {
            	callSuccessPopUp("Alert","No data exist");
            }            
        });
    });

    $("#btnReset").click(function() {
   		clearFormDetails("#advanced-first");//call clear method from common.js
        expireItemTable.fnClearTable();
    });
});