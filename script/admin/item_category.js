var itemCategoryTable;
$(document).ready(function() {
	debugger;
/* ****************************************************************************************************
 * File Name    :   item_category.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   24th feb, 2016
 * Description  :   This page  manages item categories data
 *************************************************************************************************** */
    loader();

    removeErrorMessage();//call error remove method from common.js
	$("#form_dept").hide();
    $("#drug_list").addClass('list');    
    $("#tab_drug").addClass('tab-list-add');
	
	//This click function is used for change the tab
    $("#tab_add_drug").click(function() {
        clearFormDetails("#form_submit_dept");
  		showAddTab();
    });
	
	//This click function is used for show the item category list
    $("#tab_drug").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
        tabDrugCategoryList();//Calling this function for show the list 
    });
	
    bindInventoryType("#selInventoryType");//call function define in common.js

	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
			itemCategoryTable = $('#tblItemCategory').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {	
					$('.update').unbind();				
					$('.update').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData = itemCategoryTable.fnGetData(data);
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var category = mData["category"];
							var description = mData["description"];
							var inventoryTypeId = mData["inventory_type_id"];
							editClick(id,category,description,inventoryTypeId); //Calling this function for update the data 
						}
					});
					$('.delete').unbind();
					$('.delete').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  itemCategoryTable.fnGetData(data);
						
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							deleteClick(id);//Calling this function for delete the data 
						}
					});	
				},
				
				"sAjaxSource":"controllers/admin/item_category.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "show" });
				},
				"aoColumns": [
					{ "mData": "type" },
					{ "mData": "category" },
					{ "mData": "description" }, 
					{
						"mData": function (o) { 
						return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
						   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
						   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
						   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
						   "  data-original-title='Delete'></i>"; 
						}	
					},	
				],
				aoColumnDefs: [
			        { 'bSortable': false, 'aTargets': [ 2 ] },
			        { 'bSortable': false, 'aTargets': [ 3 ] }
			    ]
			} );
    }
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	itemCategoryTable.fnClearTable();
	    	itemCategoryTable.fnDestroy();
	    	//Datatable code
			itemCategoryTable = $('#tblItemCategory').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  itemCategoryTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);//Calling this function for restore the data
						}						
					});
				},
				
				"sAjaxSource":"controllers/admin/item_category.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					{ "mData": "type" },
					{ "mData": "category" },
					{ "mData": "description" }, 
					{
					  "mData": function (o) { 
					   		return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; 
					   }		
					},	
				],
				aoColumnDefs: [
			        { 'bSortable': false, 'aTargets': [ 2 ] },
			        { 'bSortable': false, 'aTargets': [ 3 ] }
			    ]
			} );
		}
		else{
			itemCategoryTable.fnClearTable();
	    	itemCategoryTable.fnDestroy();
	    	//Datatable code
			itemCategoryTable = $('#tblItemCategory').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.update').unbind();
					$('.update').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData = itemCategoryTable.fnGetData(data);
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var category = mData["category"];
							var description = mData["description"];
							var inventoryTypeId = mData["inventory_type_id"];
							editClick(id,category,description,inventoryTypeId);
						}
					});
					$('.delete').unbind();
					$('.delete').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  itemCategoryTable.fnGetData(data);
						
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							deleteClick(id);//Calling this function for delete the data 
						}
					});
				},
				
				"sAjaxSource":"controllers/admin/item_category.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "show" });
				},
				"aoColumns": [
					{ "mData": "type" },
					{ "mData": "category" },
					{ "mData": "description" }, 
					{
						"mData": function (o) { 
						return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
						   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
						   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
						   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
						   "  data-original-title='Delete'></i>"; 
						}
					},	
				],
				aoColumnDefs: [
			        { 'bSortable': false, 'aTargets': [ 2 ] },
			        { 'bSortable': false, 'aTargets': [ 3 ] }
			    ]
			} );
		}
    });
	
	//This click function is used for validate and save the data on save button
    $("#btnSave").click(function() {
		
		var flag = "false";
		
		$("#txtCategories").val($("#txtCategories").val().trim());
		
        if ($("#txtCategories").val() == '') {
			$("#txtCategories").focus();
            $("#txtCategoriesError").text("Please enter category name");
            $("#txtCategories").addClass("errorStyle");
            flag = "true";
        }
        if ($("#selInventoryType").val() == '') {
			$("#selInventoryType").focus();
            $("#selInventoryTypeError").text("Please select inventory type");
            $("#selInventoryType").addClass("errorStyle");
            flag = "true";
        }
		if(flag == "true"){			
			return false;
		}
		
		//ajax call
		
		var categoriesName = $("#txtCategories").val();
		var inventoryTypeId = $("#selInventoryType").val();
		var description = $("#txtDescription").val();
		description = description.replace(/'/g, "&#39");
		var postData = {
			"operation":"save",
			"categoriesName":categoriesName,
			"inventoryTypeId" : inventoryTypeId,
			"description":description
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/item_category.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Item category saved successfully!!!");
					$('#messagemyModal').modal();
					$('#inactive-checkbox-tick').prop('checked', false).change();
						tabDrugCategoryList();
				}				
				else{
					$("#txtCategoriesError").text("Item category is already exists");
					$("#txtCategories").addClass("errorStyle");
					$("#selInventoryType").focus();
				}				
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});		
    });//Close save button

    //Keyup functionality
    $("#txtCategories").keyup(function() {
        if ($("#txtCategories").val() != '') {
            $("#txtCategoriesError").text("");
            $("#txtCategories").removeClass("errorStyle");
        } 
    });
	
	//Click function is used for reset button
    $("#btnReset").click(function(){
    	$("#selInventoryType").focus();
    	clearFormDetails("#form_submit_dept");
    });
});//Close document.ready function

//This function will call on edit button
function editClick(id,category,description,inventoryTypeId){
	showAddTab();
    $("#btnReset").hide();
    $("#btnSave").hide();
    $('#btnUpdate').show();
    $('#tab_add_drug').html("+Update Item Category");
    $("#txtCategories").focus();
   
	$("#btnUpdate").removeAttr("style");
	$("#txtCategoriesError").text("");
    $("#txtCategories").removeClass("errorStyle");
    $("#selInventoryTypeError").text("");
    $("#selInventoryType").removeClass("errorStyle");
	$('#txtCategories').val(category);
	$('#txtDescription').val(description.replace(/&#39/g, "'"));
	$('#selInventoryType').val(inventoryTypeId);
	$('#selectedRow').val(id);

	removeErrorMessage();//call error remove method from common.js
	//Click function for update button
	$("#btnUpdate").click(function(){
		var flag = "false";
		$("#txtCategories").val($("#txtCategories").val().trim());
		if ($("#txtCategories").val() == '') {
			$("#txtCategories").focus();
			$("#txtCategoriesError").text("Please enter category name");
			$("#txtCategories").addClass("errorStyle");
			flag = "true";
		}
		if ($("#selInventoryType").val() == '' || $("#selInventoryType").val() == null) {
			$("#selInventoryType").focus();
			$("#selInventoryTypeError").text("Please select inventory type");
			$("#selInventoryType").addClass("errorStyle");
			flag = "true";
		}
		if(flag == "true"){			
			return false;
		}
		else{
			var categoriesName = $("#txtCategories").val();
			var inventoryTypeId = $("#selInventoryType").val();
			var description = $(" #txtDescription").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();	
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/item_category.php",
					data :{            
						"operation" : "update",
						"id" : id,
						"inventoryTypeId" : inventoryTypeId,
						"categoriesName":categoriesName,
						"description":description
					},
					success: function(data) {	
						if(data != "0" && data != ""){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Item category updated successfully!!!");
							itemCategoryTable.fnReloadAjax();
							$('#messagemyModal').modal();
							tabDrugCategoryList();
							clearFormDetails("#form_submit_dept");
						}
						else{
							$("#txtCategoriesError").text("Item category is already exists");
							$("#txtCategories").addClass("errorStyle");
							$("#txtCategories").focus();
						}
					},
					error:function()
					{
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();			  
					}
				});
			});
		}
	});
}

//Click function for delete the item categories
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete item category");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteItemCategory("'+type+'");');
}

//Click function for restore the item categories
function restoreClick(id){
	$('#selectedRow').val(id);
	$('.modal-body').text("");	
	$('#confirmMyModalLabel').text("Restore item category");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();
	
	var type="restore";
	$('#confirm').attr('onclick','deleteItemCategory("'+type+'");');
}
//function for delete the item categories 
function deleteItemCategory(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
			
		//Ajax call for delete the item categories 	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/item_category.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Item category deleted successfully!!!");
					itemCategoryTable.fnReloadAjax();
					$('#messagemyModal').modal();
					 
				}
			},
			error:function()
			{
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
	
		//Ajax call for restore the item categories 
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/item_category.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Item category restored successfully!!!");
					itemCategoryTable.fnReloadAjax();
					$('#messagemyModal').modal();
					 clearFormDetails("#form_submit_dept");
				}			
			},
			error:function()
			{
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	}
}

//This function is used for show the list of item category
function tabDrugCategoryList(){
	$("#form_dept").hide();
	$(".blackborder").show();
    $("#tab_add_drug").removeClass('tab-detail-add');
    $("#tab_add_drug").addClass('tab-detail-remove');
    $("#tab_drug").removeClass('tab-list-remove');    
    $("#tab_drug").addClass('tab-list-add');
    $("#drug_list").addClass('list');
    $("#btnReset").show();
    $("#btnSave").show();
    $('#btnUpdate').hide();
    $('#tab_add_drug').html("+Add Item Category");
	clearFormDetails("#form_submit_dept");
}

function showAddTab(){
    $("#form_dept").show();
    $(".blackborder").hide();

    $("#tab_add_drug").addClass('tab-detail-add');
    $("#tab_add_drug").removeClass('tab-detail-remove');
    $("#tab_drug").removeClass('tab-list-add');
    $("#tab_drug").addClass('tab-list-remove');
    $("#drug_list").addClass('list');		
	$("#txtCategoriesError").text("");
    $("#txtCategories").removeClass("errorStyle");
    $('#txtCategories').focus();
}

// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		$(".close").click();
    }
});