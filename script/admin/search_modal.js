 $(document).ready(function(){
	 
	 loader();
 //Code for modal-body 
	$("#btnReset").click(function(){
		$("#txtFirstName").val("");
		$("#txtLastName").val("");
		$("#txtDOB").val("");
		$("#txtMobile").val("");
		$("#txtEmail").val("");
	});
	
	otable = $('#viewPatientTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		
	} );
	
	$("#btnSearch").click(function(){
		var firstName = $("#txtFirstName").val();
		var lastName = $("#txtLastName").val();
		var dob = $("#txtDOB").val();
		var mobile = $("#txtMobile").val();
		var email = $("#txtEmail").val();
		
		var postData = {
			"operation":"search",
			"firstName":firstName,
			"lastName":lastName,
			"dob":dob,
			"mobile":mobile,
			"email":email
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/view_patients.php",
			datatype:"json",
			data: postData,
			
			success: function(dataSet) {
				dataSet = JSON.parse(dataSet);
				//var dataSet = jQuery.parseJSON(data);
				otable.fnClearTable();
				otable.fnDestroy();
				otable = $('#viewPatientTable').DataTable( {
					
					"fnDrawCallback": function ( oSettings ) {
					},
					"aaData": dataSet,
					"aoColumns": [
						{ "mData": "id"},
						{ "mData": "name" },
						{ "mData": "dob" },
						{ "mData": "mobile" },
						{ "mData": "email" },
						{
						  "mData": function (o) { 				
							var data = o;
							var id = data["id"];	
							return "<input type='button' class='patientDetails' id='btnView' data-id="+id+" style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;' value='view'"+					 
							   " style='font-size: 22px; cursor:pointer;'>"; 
							}
						},
					],
				} );				
				
			},
			error:function() {
				alert('error');
			}
		});
	});
});

//# sourceURL = bed.js