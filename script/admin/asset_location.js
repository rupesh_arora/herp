var assetLocationTable;
$(document).ready(function(){
    loader();
    debugger;
	
	/*Hide add ward by default functionality*/
	$("#advanced-wizard").hide();
    $("#ledgerList").addClass('list');    
    $("#tabledgerList").addClass('tab-list-add');

    $("#tabAddledger").click(function() {
        clear();
       showAddTab();
    });
	
	//Click function for show the ledger lists
    $("#tabledgerList").click(function() {
        clear();
        tabledgerList();
    });

    removeErrorMessage();


    $('#btnSubmit').click(function() {
        var flag = false;

        if($('#txtLocation').val() == '') {
            $('#txtLocationError').text('Please enter location');
            $('#txtLocation').focus();
            $('#txtLocation').addClass("errorStyle"); 
            flag = true;
        }
        if(flag == true) {
            return false;
        }

        var location = $('#txtLocation').val().trim();
        var description = $('#txtDescription').val().trim();

        var postData = {
            location : location,
            description : description,
            operation : "saveAssetLocation"
        }


        $.ajax(
            {                   
            type: "POST",
            cache: false,
            url: "controllers/admin/asset_location.php",
            datatype:"json",
            data: postData,
            
            success: function(data) {
                if(data != "0" && data != ""){
                    callSuccessPopUp('Success','Saved successfully!!!');
                     tabledgerList();
                    clear();
                }
                else{
                    $('#txtLocationError').text("Location is already exist");
                    $('#txtLocation').addClass('errorStyle');
                }
            },
            error: function(){

            }
        });
    });

    $('#btnReset').click(function(){
        clear();
    });


    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
    //Datatable code 
    assetLocationTable = $('#assetLocationTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "autoWidth": false,
        "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = assetLocationTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var location = mData["location"];                      
                        var description = mData["description"];
                        editClick(id,location,description);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = assetLocationTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/asset_location.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "location"
                }, {
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [1,2]
            }]

        });
    }

    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            assetLocationTable.fnClearTable();
            assetLocationTable.fnDestroy();
            assetLocationTable = "";
            assetLocationTable = $('#assetLocationTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "autoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = assetLocationTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var location = mData["location"];
                            restoreClick(id,location);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/asset_location.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "location"
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1,2]
                }]
            });
        } else { // show active data on unchecked   
            assetLocationTable.fnClearTable();
            assetLocationTable.fnDestroy();
            assetLocationTable = "";
            assetLocationTable = $('#assetLocationTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "autoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = assetLocationTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var location = mData["location"];                      
                            var description = mData["description"];
                            editClick(id,location,description);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = assetLocationTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/asset_location.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "location"
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1,2]
                }]
            });
        }
    });

});

function editClick(id,location,description) {
    
    showAddTab()  
    $("#btnReset").hide();
    $("#btnSubmit").hide();
    $('#btnUpdate').show();
    $('#tabAddledger').html("+Update Asset Location");
    $('#txtLocation').focus();    
    $("#txtLocation").removeClass("errorStyle");
    $("#txtLocationError").text("");
   
    $('#txtLocation').val(location);
    $('#txtDescription').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
    //validation
    //remove validation style

    removeErrorMessage();
    
    
    $("#btnUpdate").click(function() { // click update button
        var flag = false;
        if ($("#txtLocation").val()== '') {
            $("#txtLocationError").text("Please enter location");
            $("#txtLocation").focus();
            $("#txtLocation").addClass("errorStyle");
            flag = true;
        }
        
        if(flag == true) {
            return false;
        }

        var location = $("#txtLocation").val().trim();
        var description = $("#txtDescription").val().trim();
        description = description.replace(/'/g, "&#39");
        var id = $('#selectedRow').val();
        
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
        var postData = {
            "operation": "update",
            "location": location,
            "description": description,
            "id": id
        }
        $.ajax( //ajax call for update data
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/asset_location.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('#myModal').modal('hide');
                        $('.close-confirm').click();
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Asset location updated successfully!!!");
                        $('#messagemyModal').modal();
                        assetLocationTable.fnReloadAjax();
                        tabledgerList()
                        clear();
                    }
                    else{
                        $("#myModal #txtLocationError").text("Location already exist");
                        $('#myModal #txtLocation').addClass('errorStyle')
                        $("#myModal #txtLocation").focus();
                    }
                },
                error: function() {
                    $('.close-confirm').click();
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                    $('#messagemyModal').modal();
                }
            }); // end of ajax
        });
    });
} // end update button

function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Asset Location");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteAssetLocation("' + type + '","'+id+'","");');
} // end click fucntion

function restoreClick(id,location) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Asset Location");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteAssetLocation("' + type + '","'+id+'","'+location+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteAssetLocation(type,id,location) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/asset_location.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Asset location deleted successfully!!!");
                    $('#messagemyModal').modal();
                    assetLocationTable.fnReloadAjax();
                } 
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/asset_location.php",
            data: {
                "operation": "restore",
                "location":location,
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Asset location restored successfully!!!");
                    $('#messagemyModal').modal();
                    assetLocationTable.fnReloadAjax();
                }
                else{
                    callSuccessPopUp("Sorry","This location is already exist so you can not restore it!!!");
                }
            },
            error: function() {             
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary unavailable to respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}
function tabledgerList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
   
    $("#tabAddledger").removeClass('tab-detail-add');
    $("#tabAddledger").addClass('tab-detail-remove');
    $("#tabledgerList").removeClass('tab-list-remove');    
    $("#tabledgerList").addClass('tab-list-add');
    $("#ledgerList").addClass('list');
   
    $("#btnReset").show();
    $("#btnSubmit").show();
    $('#btnUpdate').hide();
    $('#tabAddledger').html("+Add Asset Location");
    $('#inactive-checkbox-tick').prop('checked', false).change();
    clear();
}
function showAddTab(){
    $("#advanced-wizard").show();
    $(".blackborder").hide();

    $("#tabAddledger").addClass('tab-detail-add');
    $("#tabAddledger").removeClass('tab-detail-remove');
    $("#tabledgerList").removeClass('tab-list-add');
    $("#tabledgerList").addClass('tab-list-remove');
    $("#ledgerList").addClass('list');
    $('#txtLocation').focus();
}
function clear() {
    removeErrorMessage();
    clearFormDetails(".clearForm"); 
    $('#txtLocation').focus();
}