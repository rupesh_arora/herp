/*
 * File Name    :   view_seller.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Gupta
 * Created Date :   16th Feb, 2016
 * Description  :   This page is use for load and view data of orders
*/
var sellerTable;
$(document).ready(function() {
loader();
	/*Hidden field value one for view order screen*/
	if (sellerHiddenFieldValue == 1) {
		ajaxCall('mySellerRequestModal','txtSellerId','','','');
	}

	/*Hidden field value zero for put order screen*/
	if (sellerHiddenFieldValue == 0) {
		ajaxCall('mySellerModal','','txtSeller','txtSellerEmailId','');
	}

	if (sellerHiddenFieldValue == 2) {
		ajaxCall('mySellerModal','','txtSupplier','txtSupplierEmail','txtFirmName');
	}
	
	$("#btnResetSeller").click(function(){
		$("#txtSellerName").val("");
		sellerTable.fnClearTable();
	});

});
function clearFilter() {
	// remove filter on search
	sellerTable.fnFilter("",0);
}
function ajaxCall(modalId,textId,textName,textsellerEmailId,txtFirmName){
	var postData = {
		"operation" : "showSeller"
	}
	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/view_orders.php",
		datatype: "json",
		data: postData,
		success: function(data) {
		    dataSet = JSON.parse(data);		    
		    sellerTable = $('#viewSellerDetailsTable').DataTable({
		    	"sPaginationType": "full_numbers",
				"bAutoWidth" : false,
	            
	            "fnDrawCallback": function(oSettings) {

	            	$("#btnShowSeller").click(function(){
						
						// clear filter
						clearFilter();
						
						var searchSellerName = $("#txtSellerName").val();
						
						if(searchSellerName != "") {
							sellerTable.fnFilter(searchSellerName, 0);
						}
					});
	            	//Double click functionality
	            	
			        $('#'+modalId+' #viewSellerDetailsTable tbody tr').on('dblclick', function() {
			        	if ($(this).hasClass('selected')) {
		                    $(this).removeClass('selected');
		                } 
		                else {
		                    sellerTable.$('tr.selected').removeClass('selected');
		                    $(this).addClass('selected');
		                }

		                var mData = sellerTable.fnGetData(this); // get datarow
		                if (null != mData) // null if we clicked on title row
		                {
		                    $('#'+textName).val(mData["name"]);
		                    if (sellerHiddenFieldValue == 2) {
		                    	$('#'+textName).attr("supplier-id",mData["id"]);
		                    }
		                    else{
		                    	$('#'+textName).attr("seller-id",mData["id"]);
		                    }
		                    
		                    $('#'+textId).val(mData["id"]);
		                    $('#'+textsellerEmailId).val(mData["email"]);
		                    $('#'+txtFirmName).val(mData["firm_name"]);
		                }

		                $(".close").click();
			        });
                },
                "aaData": dataSet,
                "aoColumns": [ 
	                {"mData": "name"},
	                {"mData": "email"},
	                {"mData": "firm_name"},
	                {"mData": "description"},
	                {"mData": "id","bVisible": false}
	            ],
	            aoColumnDefs: [{'bSortable': false,'aTargets': [3]}]
            });
		},
        error: function() {
		
			$('.modal-body').text("");
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
	});
}