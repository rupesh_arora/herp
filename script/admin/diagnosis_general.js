var otable = null;

$(document).ready(function() {
    loader();
	var flag = "false";
	
	$('#oldHistoryModalDetails input[type=text]').addClass("as_label");
    $('#oldHistoryModalDetails input[type=text]').attr('readonly', true);
    $('#oldHistoryModalDetails input[type="button"]').addClass("btnHide");
    $('#oldHistoryModalDetails #diagnosisRequestsTbl input[type="button"]').addClass("btnHide");
    $('#oldHistoryModalDetails #divDropDownAllergyRequest').hide();

    /**DataTable Initialization**/
    otable = $('#diagnosisRequestsTbl').DataTable({
        "bPaginate": false,
        aoColumnDefs: [{'bSortable': false,'aTargets': [2] }],
		"fnDrawCallback" : function() {
			//function for make editable column of quantity in data table			
		},			
    });
	
	if ($('#thing').val() ==3) {
        var visitId = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
		var patientId = parseInt($('#txtPatientID').val().replace ( /[^\d.]/g, '' ));
    }
    if ($('#thing').val() ==4) {
        var visitId = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
		var patientId = parseInt($('#txtPatientID').val().replace ( /[^\d.]/g, '' ));
    }
    if ($('#thing').val() ==5) {
        var visitId = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
		var patientId = $('#txtPatientID').val();
    }
	
	/*Fetch data from db and insert in to table during page load*/
    loadDiagonsisData(visitId);
      

    //Add Lab Test click event
    $('#addDiagnosisRequest').click(function() {

        $('#addDiagnosisRequest').css('border', ''); //remove the red error border from add test button
        $('#noDataError').html(""); //remove the error message after the add button

		flag = "false";
        var id = $('#selDiagnosis').val();
        var name = $('#selDiagnosis  option:selected').attr('data-name');

        var currentData = otable.fnAddData(["<input type='text' id='txtAutoDisease' class='editable-text txtDrugName' autocomplete='off' style='width:100%; padding:0px;' placeholder='Please Click for add Diagnosis Name'/>","<input type='text' class='editable-text' readonly  style='width:100%; padding:0px;'/>","<input type='button' class='btn-remove' onclick='rowDelete($(this));' value='Remove'>"]);
		 
		var getCurrDataRow = otable.fnSettings().aoData[ currentData ].nTr;
		/*$($(getCurrDataRow).find('td')[0]).html("Please click for add diagnosis name");*/ // show text in data column 1
		
        /*Function for autocomplete disease*/
        keyUpDisease();

        /*Blur function to ensure correct disease should be enter by user*/
        $(".txtDrugName ").blur(function(){

            var detectDiseaseId = $(otable.$(".btn-remove")).parent().prev().prev().children().attr('disease-id');
            if (detectDiseaseId ==undefined && $(".txtDrugName ").val() !='') {
                $(".txtDrugName ").val('');

                $('#messageMyModalLabel').text("Alert");
                $('#messagemyModal .modal-body').text("Please choose correct diagnosis from suggestion.");
                $('#messagemyModal').modal();
                return false;
            }
        });
    });

    //Save button functionality
    $('#btnSaveLabTestRequest').click(function() {
		
        var tableData = otable.fnGetData(); //fetching the datatable data
		var diagnosis = new Array(); //small array contain prescription  id
		var bigdiagnosis =new Array(); // big 2d array conatin small array
		var rowcollection =  otable.$(".btn-remove");
        if (rowcollection.length <= 0) {
            $('#messageMyModalLabel').text("Alert");
            $('#messagemyModal .modal-body').text("Please enter any new diagnosis name");
            $('#messagemyModal').modal();
            return false;
        }
		for (i=0;i<rowcollection.length;i++) {
			diagnosis = new Array();
            var myData = $(otable.fnGetNodes()).find('.btn-remove').closest('tr')[i];

			var diagnosisName = $(myData).find('td:eq(1)').find('input').val();
            var detectDiseaseId = $(myData).find('td:first-child').find('input').attr('disease-id');
            if (detectDiseaseId == undefined) {
                return false;
            }
			var diseaseId = detectDiseaseId;
			if(diagnosisName != "" && diagnosisName != "Please click for add diagnosis name"){
				diagnosis.push(diagnosisName,diseaseId);
				bigdiagnosis.push(diagnosis);
			}	
			else{
				$('#messageMyModalLabel').text("Alert");
				$('#messagemyModal .modal-body').text("Please enter diagnosis name.");
				$('#messagemyModal').modal();
                return false;
			}
		}
		if (flag == "true") {
			return false;
		}
        //Checking whether datatable is empty or not
        if (tableData.length == 0) {

            $('#addDiagnosisRequest').css('border', '1px solid #a94442 '); //Put the red border around the add lab button
            $('#noDataError').html("&nbsp;&nbsp;Please add test(s)."); //Put the error message after the add lab test button
            return false;

        } 
        else { //Saving the data            
            $('#confirmUpdateModalLabel').text();
            $('#updateBody').text("Are you sure that you want to save this?");
            $('#confirmUpdateModal').modal();
            $("#btnConfirm").unbind();
            $("#btnConfirm").click(function(){
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: "controllers/admin/diagnosis_general.php",
                    datatype: "json",
                    data: {
                        'operation': 'save',
                        'bigdiagnosis': JSON.stringify(bigdiagnosis),
                        'patientId': patientId,
                        'visitId': visitId,
                        'tblName': 'patients_diagnosis_request',
                        'fkColName': 'created_by'
                    },

                    success: function(data) {
                        if (data != "0" && data != "") {
                            flag = "true";
                            $('#messageMyModalLabel').text("Success");
                            $('#messagemyModal .modal-body').text("Diagnosis general saved successfully!!!");
                            $('#messagemyModal').modal();
                            loadDiagonsisData(visitId);
                        } else {
                            $('#messageMyModalLabel').text("Error");
                            $('#messagemyModal .modal-body').text("Something awful happened!! Please try to contact admin.");
                            $('#messagemyModal').modal();
                        }
                    },
                    error: function() {
                        $('#messageMyModalLabel').text("Error");
                        $('#messagemyModal .modal-body').text("Something awful happened!! Please try to contact admin.");
                        $('#messagemyModal').modal();
                    }
                });
            });                 
        }
    });

    //Reset button functionality
    $('#btnResetLabTestRequest').click(function() {
        $('#selDiagnosis').html("");
        $('#selDiagnosisCategory').val("-1");
        $('#totalBill').text("0");
        var lengthDeleteRow = $(".btn-remove").closest("tr").length;
        for(var i=0;i<lengthDeleteRow;i++){
            var myDeleteRow = $(".btn-remove").closest("tr")[0];
            otable.fnDeleteRow(myDeleteRow);
        }
    });

    //View Requested Tests button functionality
    $('#viewOldTests').click(function() {
        $("#oldDiagnosisTestModal").modal();
        $("#oldDiagnosisTestModalLabel").text("Old Diagnosis Requests");
        var oDiagnosisOldTable = $("#oldDiagnosisRequestsTbl").dataTable();
        var postData = {
            "operation": "oldDiagnosisSearch",
            "visit_id": visitId
        }
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/old_test_requests.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                oDiagnosisOldTable.fnClearTable();
                oDiagnosisOldTable.fnDestroy();
                dataSet = JSON.parse(data);
                oldReqTbl = $('#oldDiagnosisRequestsTbl').DataTable({
                    "bPaginate": false,
                    "bFilter": false,
                    "aaData": dataSet,
                    "aoColumns": [{
                        "mData": "name"
                    }, {
                        "mData": function(o) {
                            var dbDateTimestamp = o;
                            var dateObj = new Date(dbDateTimestamp["request_date"] * 1000);
                            var yyyy = dateObj.getFullYear().toString();
                            var mm = (dateObj.getMonth() + 1).toString(); // getMonth() is zero-based
                            var dd = dateObj.getDate().toString();
                            return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]); // padding
                        }
                    }, {
                        "mData": "code"
                    }]
                });
            }
        });
    });

    
});

/**Row Delete functionality**/
function rowDelete(currInst) {
    var row = currInst.closest("tr").get(0);
    /* var oTable = $('#diagnosisRequestsTbl').dataTable(); */
    $('#totalBill').text(parseInt($('#totalBill').text()) - parseInt($(row).find('td:eq(3)').text()));
    otable.fnDeleteRow(row);
}

//
//function for binding the room
//
function bindDisease(parentObj){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/diagnosis_general.php",
		data: {
			"operation":"showDisease"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>-- Select --</option>";
				for (var i=0;i<parseData.length;i++)
				{
					option+="<option value='"+parseData[i].id+"'>"+parseData[i].name+"</option>";
				}
				
				if(parentObj != ""){
					parentObj.find('.editable-drop-down').html(option);									
				}
			}
		},
		error:function(){			
		}
	});	
}

function loadDiagonsisData(visitId){
    /* var visitId = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' )); */
    var postData = {
        "operation": "oldDiagnosisSearch",
        "visit_id": visitId
    }
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/old_test_requests.php",
        datatype: "json",
        data: postData,

        success: function(data) {
            dataSet = JSON.parse(data);
            otable.fnClearTable();
            
            for(var i=0; i< dataSet.length; i++){
                var id = dataSet[i].id;
                /* var code = dataSet[i].diagnosis_code; */
                var diagnosisName = dataSet[i].diagnosis_name;
                var diseaseName = dataSet[i].disease_name;

                /*Apply here span to recognise previous or current data*/
                otable.fnAddData([diseaseName,"<span data-previous='true'>"+diagnosisName+"</span>","<input type='button' class ='btnDisabled' value='Remove' disabled>"]);
                otable.find('tbody').find('tr').find('td').css({"background-color":"#ccc"});
            }
        }
    });
}
function keyUpDisease(){
    $(".txtDrugName").keyup(function(){       //Autocomplete functionality 
        var currentObj = $(this);
        debugger;
        jQuery(".txtDrugName").autocomplete({
            source: function(request, response) {
                var postData = {
                    "operation":"searchDisease",
                    "diseaseName": currentObj.val()
                }
                
                $.ajax(
                    {                   
                    type: "POST",
                    cache: false,
                    url: "controllers/admin/diagnosis_general.php",
                    datatype:"json",
                    data: postData,
                    
                    success: function(dataSet) {
                        loader();
                        //searchedDrug = JSON.parse(dataSet);
                        response($.map(JSON.parse(dataSet), function (item) {
                            
                            return {
                                id: item.id,
                                code:item.code,
                                value: item.name
                            }
                        }));
                    },
                    error: function(){
                        
                    }
                });
            },
            select: function (e, i) {
                var diseaseId = i.item.id;
                currentObj.attr("disease-id", diseaseId);
                $(this).val(i.item.value);
                $($(this).parent().siblings()[0]).find('input').val(i.item.code);
            },
            minLength: 3
        });     
    });
}