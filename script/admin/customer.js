var customerTable;
var pendingInvoiceTable;
var paymentHistoryTable;
var tableOpenAmount = [];
var k = 0;
var table ;
$(document).ready(function(){
	debugger;
    var totalamount = 0;
    var openAmount = 0;
    table = true;
    bindCustomerType();
    removeErrorMessage();// remove error messaages    
   
	$("#customerPopUPForm").hide();
    $("#divProducts").addClass('list');    
    $("#tabCustomerList").addClass('tab-list-add');
    

    $("#tabCustomer").click(function() { // show the add allergies categories tab
        showAddTab();
        clearFormDetails(".clearForm");
        $("#txtFirstName").focus();
        bindCustomerType();
        countryID = ''; // defile countryID variable
        bindcountry(countryID,"#selCountry"); // function call for show data on country
        $('#paymentHistoryScreen').hide();
    });

    $("#tabCustomerList").click(function() { // show allergies list tab
        tabReceiverList();//Calling this function for show the list		
    });    

    $("#btnSave").click(function(){
    	var flag = false;

        if(validTextField('#txtEmail','#txtEmailError','Please enter email') == true)
        {
            flag = true;
        }
        if (!ValidateEmail($("#txtEmail").val())) {
            $("#txtEmailError").text("Please enter valid email address");
            $("#txtEmail").focus();
            $("#txtEmail").addClass("errorStyle");
            flag = true;
        }
        if(validTextField('#selCustomerType','#selCustomerTypeError','Please select customer type') == true)
        {
            flag = true;
        }
        if(validTextField('#txtPincode','#txtPincodeError','Please enter pin code') == true)
        {
            flag = true;
        }
        if(validTextField('#selCity','#selCityError','Please select city') == true)
        {
            flag = true;
        }
        if(validTextField('#selState','#selStateError','Please select state') == true)
        {
            flag = true;
        }
        if(validTextField('#selCountry','#selCountryError','Please select country') == true)
        {
            flag = true;
        }
        if(validTextField('#txtAddress','#txtAddressError','Please enter address') == true)
        {
            flag = true;
        }
        if(validTextField('#txtLastName','#txtLastNameError','Please enter last name') == true)
        {
            flag = true;
        }
         if(validTextField('#txtFirstName','#txtFirstNameError','Please enter first name') == true)
        {
            flag = true;
        }

        if(flag == true) {
			return false;
		}

		var firstName = $("#txtFirstName").val().trim();
        var lastName = $("#txtLastName").val().trim();
        var address = $("#txtAddress").val().trim();
        var country = $("#selCountry").val().trim();
        var state = $("#selState").val().trim();
        var city = $("#selCity").val().trim();
        var customerType = $("#selCustomerType").val().trim();
        var pincode = $("#txtPincode").val().trim();
        var email = $("#txtEmail").val().trim();
        var website = $("#txtWebsite").val().trim();
        var phone = $("#txtPhone").val().trim();
        

		var postData = {
			firstName : firstName,
			lastName : lastName,
            customerType : customerType,
            address : address,
            country : country,
            state : state,
            city : city,
            pincode : pincode,
            email : email,
            website : website,
            phone : phone,
			"operation" : "save"
		}

		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/customer.php",
			datatype: "json",
			data: postData,
			 success: function(data){
                if(data !='' && data!=null && data =='1'){
                    callSuccessPopUp("Success","Saved  successfully!!!");
                     tabReceiverList();//Calling this function for show the list     
                   
                }
                else if (data == "email exist") {
                    $("#txtEmail").focus();
                    $("#txtEmail").addClass("errorStyle");
                    $("#txtEmailError").text("This email alreday exist.");                    
                }
            },
            error:function(){

            }
        });
    });


    // Ajax call for show data on state select country 
    $('#selCountry').change(function() {
        var countryID = $("#selCountry").val();
        bindState(countryID, '',"#selState");
    });

    // Ajax call for show data on country select state 
    $('#selState').change(function() {
        var stateID = $("#selState").val();
        bindCity(stateID, '',"#selCity");
    });

    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code
        customerTable = $('#customerTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = customerTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var fisrtName = mData["first_name"];
                        var lastName = mData["last_name"];
                        var address = mData["address"];
                        var country = mData["country_id"];
                        var city = mData["city_id"];
                        var state = mData["state_id"];
                        var pincode = mData["pincode"];
                        var email = mData["email"];
                        var website = mData["website"];
                        var customer_type_id = mData["customer_type_id"];
                        var phone = mData["phone"];
                        editClick(id,fisrtName,lastName,address,country,state,city,pincode,email,website,phone,customer_type_id);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = customerTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });

                $('#mySearchModal #customerTable tbody tr').on('dblclick', function() {
                    if ($(this).hasClass('selected')) {
                        $(this).removeClass('selected');
                    } else {
                        customerTable.$('tr.selected').removeClass('selected');
                        $(this).addClass('selected');
                    }

                    var mData = customerTable.fnGetData(this); // get datarow
                    if (null != mData) {
                        var compelteCustomerId = $(this).find('td:eq(0)').text();
                        $('#txtCustomerId').val(compelteCustomerId);
                        $("#txtCustomer").val(compelteCustomerId);
                        $("#txtCustomerName").val(mData.name);
                        $("#txtCustomerEmail").val(mData.email);
                    }// null if we clicked on title row
                    $("#btnShowCustomerDetails").click();
                    $(".close").click();
                });
            },
            "sAjaxSource": "controllers/admin/customer.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": function (o) {  
                        var customerId = o["id"];
                            
                        var customerIdlength = customerId.length;
                        for (var i=0;i<6-customerIdlength;i++) {
                            customerId = "0"+customerId;
                        }
                        customerId = customerPrefix+customerId;
                        return customerId; 
                    }
                }, {
                    "mData": "name"
                },{
                    "mData": "email"
                },{
                    "mData": "phone"
                },{
                    "mData": "address"
                },{
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [5]
            }]
        });
    }
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            customerTable.fnClearTable();
            customerTable.fnDestroy();
            customerTable = "";
            customerTable = $('#customerTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = customerTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var tax_name = mData['tax_name'];
                            restoreClick(id,tax_name);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/customer.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                {
                    "mData": function (o) {  
                        var customerId = o["id"];
                            
                        var customerIdlength = customerId.length;
                        for (var i=0;i<6-customerIdlength;i++) {
                            customerId = "0"+customerId;
                        }
                        customerId = customerPrefix+customerId;
                        return customerId; 
                    }
                }, {
                    "mData": "name"
                },{
                    "mData": "email"
                },{
                    "mData": "phone"
                },{
                    "mData": "address"
                },{
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [5]
                }]
            });
        } else { // show active data on unchecked   
            customerTable.fnClearTable();
            customerTable.fnDestroy();
            customerTable = "";
            customerTable = $('#customerTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = customerTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var fisrtName = mData["first_name"];
                            var lastName = mData["last_name"];
                            var address = mData["address"];
                            var country = mData["country_id"];
                            var city = mData["city_id"];
                            var state = mData["state_id"];
                            var pincode = mData["pincode"];
                            var email = mData["email"];
                            var website = mData["website"];
                            var phone = mData["phone"];
                            var customer_type_id = mData["customer_type_id"];
                            editClick(id,fisrtName,lastName,address,country,state,city,pincode,email,website,phone,customer_type_id);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = customerTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/customer.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                {
                    "mData": function (o) {  
                        var customerId = o["id"];
                            
                        var customerIdlength = customerId.length;
                        for (var i=0;i<6-customerIdlength;i++) {
                            customerId = "0"+customerId;
                        }
                        customerId = customerPrefix+customerId;
                        return customerId; 
                    }
                }, {
                    "mData": "name"
                },{
                    "mData": "email"
                },{
                    "mData": "phone"
                },{
                    "mData": "address"
                },{
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [5]
                }]
            });
        }
    });

    $("#btnReset").click(function() {
       clearFormDetails(".clearForm");
       removeErrorMessage();
        $("#txtFirstName").focus();
        bindCustomerType();
        countryID = ''; // defile countryID variable
        bindcountry(countryID,"#selCountry"); // function call for show data on country
    });

});
function editClick(id,fisrtName,lastName,address,country,state,city,pincode,email,website,phone,customer_type_id) {
    showAddTab();
    $(".customerBtn").removeClass('activeSelf');
    $("#btnReset").hide();
    $("#btnSave").hide();
    $('#btnUpdate').show();
    $('#tab_add_drug').html("+Update Customer");
    $("#mainScreen").removeClass("hide");
    $(".showCustomerTabs").show();
    
    $("#txtFirstName").removeClass("errorStyle");
    $("#txtFirstNameError").text("");
    
    
    $('#txtFirstName').val(fisrtName);
    $('#txtLastName').val(lastName);
    $("#txtAddress").val(address);
    $("#txtPincode").val(pincode);
    $("#txtEmail").val(email);
    $("#txtWebsite").val(website);
    $("#txtPhone").val(phone);
    $("#selCustomerType").val(customer_type_id);
    bindcountry(country,"#selCountry");
    bindState(country, state,"#selState");
    bindCity(state, city,"#selCity");

    $('#selectedRow').val(id);
    //validation
   
    $('#selCountry').change(function() {
        var countryID = $("#selCountry").val();
        bindState(countryID, '',"#selState");
    });

    // Ajax call for show data on country select state 
    $('#selState').change(function() {
        var stateID = $("#selState").val();
        bindCity(stateID, '',"#selCity");
    });
    
    $("#btnUpdate").click(function() { // click update button
        var flag = "false";
        if ($("#txtEmail").val().trim()== '') {
            $("#txtEmailError").text("Please enter email");
            $("#txtEmail").focus();
            $("#txtEmail").addClass("errorStyle");
            flag = "true";
        }

        if ($("#selCustomerType").val().trim()== '') {
            $("#selCustomerTypeError").text("Please select customer type");
            $("#selCustomerType").focus();
            $("#selCustomerType").addClass("errorStyle");
            flag = "true";
        }

        if ($("#txtPincode").val().trim()== '') {
            $("#txtPincodeError").text("Please enter pin code");
            $("#txtPincode").focus();
            $("#txtPincode").addClass("errorStyle");
            flag = "true";
        }

        if ($("#selCity").val().trim()== '') {
            $("#selCityError").text("Please select city");
            $("#selCity").focus();
            $("#selCity").addClass("errorStyle");
            flag = "true";
        }

        if ($("#selState").val().trim()== '') {
            $("#selStateError").text("Please select state");
            $("#selState").focus();
            $("#selState").addClass("errorStyle");
            flag = "true";
        }

        if ($("#selCountry").val().trim()== '') {
            $("#selCountryError").text("Please select country");
            $("#selCountry").focus();
            $("#selCountry").addClass("errorStyle");
            flag = "true";
        }

        if ($("#txtAddress").val().trim()== '') {
            $("#txtAddressError").text("Please enter address");
            $("#txtAddress").focus();
            $("#txtAddress").addClass("errorStyle");
            flag = "true";
        }

        if ($("#txtLastName").val().trim()== '') {
            $("#txtLastNameError").text("Please enter last name");
            $("#txtLastName").focus();
            $("#txtLastName").addClass("errorStyle");
            flag = "true";
        }

        if ($("#txtFirstName").val().trim()== '') {
            $("#txtFirstNameError").text("Please enter first name");
            $("#txtFirstName").focus();
            $("#txtFirstName").addClass("errorStyle");
            flag = "true";
        }
        
        if(flag == "true") {
            return false;
        }
        
        var firstName = $("#txtFirstName").val().trim();
        var lastName = $("#txtLastName").val().trim();
        var address = $("#txtAddress").val().trim();
        var country = $("#selCountry").val().trim();
        var state = $("#selState").val().trim();
        var city = $("#selCity").val().trim();
        var customerType = $("#selCustomerType").val().trim();
        var pincode = $("#txtPincode").val().trim();
        var email = $("#txtEmail").val().trim();
        var website = $("#txtWebsite").val().trim();
        var phone = $("#txtPhone").val().trim();
        var id = $('#selectedRow').val();   
        
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();

        $("#btnConfirm").click(function(){
            var postData = {
                "operation": "update",
                firstName : firstName,
                lastName : lastName,
                customerType : customerType,
                address : address,
                country : country,
                state : state,
                city : city,
                pincode : pincode,
                email : email,
                website : website,
                phone : phone,
                id: id
            }
            $.ajax({ //ajax call for update data
                
                type: "POST",
                cache: false,
                url: "controllers/admin/customer.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data == "1") {
                        callSuccessPopUp("Success","Customer updated Successfully!!!");
                        $('#myModal .close').click();
                         tabReceiverList();//Calling this function for show the list    
                    }
                    else{
                        $("#txtFirstName").focus();
                        $("#txtFirstName").addClass('errorStyle');
                        $("#txtFirstNameError").text("This name alreday exist.");
                    }
                },
                error: function() {
                    callSuccessPopUp("Error","Temporary Unavailable to Respond.Try again later!!!");
                }
            }); // end of ajax
        });
    });
    if(table == true){
         pendingInvoiceTable =  $('#pendingInvoiceTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "bAutoWidth":false
        });
     }
     if(table == true){
        paymentHistoryTable =  $('#paymentHistoryTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "bAutoWidth":false
        });
    }
   

    $(".customerBtn").on('click',function(){
        $(".customerBtn").removeClass('activeSelf');
        $(this).addClass('activeSelf');
        $(".tabScreen").addClass('hide');
        if ($(this).attr("attr") == "mainScreen") {
            $("#mainScreen").removeClass('hide');
        }
        else if($(this).attr("attr") == "pendingInvoiceScreen"){
            $("#pendingInvoiceScreen").removeClass('hide');
            table = false;
            var postData = {
                "operation" : "showCustomerDetails",
                customerId : $('#selectedRow').val()
            }

            dataCall("controllers/admin/receipt_number.php", postData, function (result){
                var parseData = JSON.parse(result);
                if (parseData != '') { 
                    var grandTotal =0;         
                    pendingInvoiceTable.fnClearTable();
                    var id = '';
                    var receiptNumber;
                    totalamount = 0;
                    openAmount = 0;
                    tableOpenAmount = [];
                    $.each(parseData,function(i,v){
                        var obj = {};
                        receiptNumber = v.id;
                        var prefix = v.invoice_prefix;
                        var idLength = 6-receiptNumber.length;
                        for(j=0; j<idLength; j++){
                            receiptNumber="0"+receiptNumber;
                        }
                        receiptNumber=prefix+receiptNumber;
                        if (v.id !=id) {
                            
                            pendingInvoiceTable.fnAddData(["<input type='checkbox'  class='chkInvoice hide chkInvoice"+v.id+"' data-id="+v.id+" style='display:none; margin:0 auto; position:relative;'"+                    
                                       " style='font-size: 22px; cursor:pointer;'>"+"<span class='details-control' onclick='showRows($(this));'></span>"+"<span class='hide-details-control hide' onclick='hideRows($(this));'></span>",receiptNumber,v.description,v.total_bill,v.total_bill,v.billing_date,'']);

                            var currData = pendingInvoiceTable.fnAddData(["<input type='checkbox' tr-class="+receiptNumber+" data-class='chkInvoice"+v.id+"'  class='chkProduct hide' data-id="+v.id+"  productId="+v.customer_product_id+"  style='display:none; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative; margin-left: 42%;'"+                  
                                           " style='font-size: 22px; cursor:pointer;'>",v.name,"",v.total,v.total,v.billing_date,'']);

                            var getCurrDataRow = pendingInvoiceTable.fnSettings().aoData[ currData ].nTr;
                            $(getCurrDataRow).addClass('hide '+(receiptNumber).replace(/ /g,''));
                            id = v.id;
                            grandTotal += parseFloat(v.total);
                            if(i != 0){
                                obj.openAmount = openAmount;
                                tableOpenAmount.push(obj);
                                openAmount =0;
                            }
                            openAmount += parseFloat(v.total);
                        }

                        else {                      

                            var currData = pendingInvoiceTable.fnAddData(["<input type='checkbox' tr-class="+receiptNumber+" data-class='chkInvoice"+v.id+"' class='chkProduct hide' data-id="+v.id+" productId="+v.customer_product_id+"  style='display:none; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;  margin-left: 42%;'"+                   
                                           " style='font-size: 22px; cursor:pointer;'>",v.name,"",v.total,v.total,v.billing_date,'']);

                            var getCurrDataRow = pendingInvoiceTable.fnSettings().aoData[ currData ].nTr;
                            $(getCurrDataRow).addClass('hide '+(receiptNumber).replace(/ /g,''));
                            grandTotal += parseFloat(v.total);
                             openAmount += parseFloat(v.total);
                            id = v.id;
                        }
                        if(i == parseData.length-1){
                            obj = {};
                            obj.openAmount = openAmount;
                            tableOpenAmount.push(obj);
                            openAmount =0;
                            k=1;
                        }
                    }); 
                    if(k == 1){
                        var airows = pendingInvoiceTable.$(".chkInvoice", {"page": "all"});    //Select all unchecked row only
                        for(i=0; i<airows.length; i++){
                            $($(airows[i]).parent().siblings()[3]).text(tableOpenAmount[i].openAmount);
                        }
                    } 
                }
            });
        }
        else{
            $("#paymentHistoryScreen").removeClass('hide');
            $("#paymentHistoryScreen").show();
            table = false;
            var postData = {
                "operation" : "showPaymentHistory",
                customerId : $('#selectedRow').val()
            }

            dataCall("controllers/admin/customer.php", postData, function (result){
                var parseData = JSON.parse(result);
                if (parseData != '' && parseData[0].id != null) {        
                    paymentHistoryTable.fnClearTable();
                    $.each(parseData,function(i,v){
                        var obj = {};
                        var receiptNumber = v.id;
                        var invoiceNumber = v.invoice_id;
                        var prefix = v.invoice_prefix;
                        var receipt_prefix = v.receipt_prefix;
                        var idLength = 6-receiptNumber.length;
                        for(j=0; j<idLength; j++){
                            receiptNumber="0"+receiptNumber;
                        }
                        receiptNumber=receipt_prefix+receiptNumber;

                        var invoiceLength = 6-invoiceNumber.length;
                        for(m=0; m<invoiceLength; m++){
                            invoiceNumber="0"+invoiceNumber;
                        }
                        invoiceNumber=prefix+invoiceNumber;                                             

                        paymentHistoryTable.fnAddData([receiptNumber,invoiceNumber,v.description,v.amount,v.pay_date]);
                           
                    });
                }
            });
        }
    });

    $($(".customerBtn")[0]).addClass("activeSelf");

} // end update button
function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete customer");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deletefisrtName("'+type+'");');
} // end click fucntion

function restoreClick(id) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore customer");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deletefisrtName("'+type+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deletefisrtName(type) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/customer.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data == "1") {
                    callSuccessPopUp("Success","Customer deleted Successfully!!!");
                    customerTable.fnReloadAjax();
                } 
            },
            error: function() {
                callSuccessPopUp("Error","Temporary Unavailable to Respond.Try again later!!!");
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/customer.php",
            data: {
                "operation": "restore",
                "id": id,
                "fisrtName" : fisrtName
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    callSuccessPopUp("Success","Customer restored Successfully!!!");
                    customerTable.fnReloadAjax();
                }
            },
            error: function() {
                callSuccessPopUp("Error","Temporary Unavailable to Respond.Try again later!!!"); 
            }
        });
    }
}

function bindaddressMeasure(){
    $.ajax({                    
        type: "POST",
        cache: false,
        url: "controllers/admin/drugs.php",
        data: {
            "operation":"showaddressMeasure"
        },
        success: function(data) {   
            if(data != null && data != ""){
                var parseData= jQuery.parseJSON(data);
            
                var option ="<option value='-1'>-- Select --</option>";
                for (var i=0;i<parseData.length;i++)
                {
                    option+="<option value='"+parseData[i].id+"'>"+parseData[i].address_abbr+"</option>";
                }
                $('#seladdress').html(option);              
                
            }
        },
        error:function(){
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();           
        }
    });
}
function appendTaxes(){
    var postData = {
        operation : "getTaxes"
    }
    dataCall("controllers/admin/customer.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (postData !='') {
            var html = '';
            $.each(parseData,function(i,v){
                html+='<div class="taxContainer"><input type="checkbox" class="chkTaxes" id="chkTax'+v.id+'" value="'+v.id+'"><label class="fisrtName" for="chkTax'+i+'">'+v.tax_name+'</label></div>';
            });
            $("#appendTaxes").html(html);
        }
    });
}
function bindcountry(countryID,txtFieldId) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/hospital_profile.php",
        data: {
            "operation": "showcountry"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=' '>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $(txtFieldId).html(option);
                $("#selState").html("<option value=' '>--Select--</option>");
                $("#selCity").html("<option value=' '>--Select--</option>");
            }
            if (countryID != "") {
                $(txtFieldId).val(countryID);
            }

        },
        error: function() {}
    });
}

// function for state 
function bindState(countryID, stateID,txtFieldId) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/hospital_profile.php",
        data: {
            "operation": "showstate",
            country_ID: countryID
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=' '>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $(txtFieldId).html(option);
                if (stateID != "") {
                    $(txtFieldId).val(stateID);
                }
            }
        },
        error: function() {}
    });
}

// function for city
function bindCity(stateID, cityID,txtFieldId) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/hospital_profile.php",
        data: {
            "operation": "showcity",
            state_ID: stateID
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=' '>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $(txtFieldId).html(option);
                if (cityID != "") {
                    $(txtFieldId).val(cityID);
                }
            }
        },
        error: function() {}
    });
}
function bindCustomerType(){
    $.ajax({                    
        type: "POST",
        cache: false,
        url: "controllers/admin/customer.php",
        data: {
            "operation":"showCustomerType"
        },
        success: function(data) {   
            if(data != null && data != ""){
                var parseData= jQuery.parseJSON(data);
            
                var option ="<option value=' '>-- Select --</option>";
                for (var i=0;i<parseData.length;i++)
                {
                    option+="<option value='"+parseData[i].id+"'>"+parseData[i].type+"</option>";
                }
                $('#selCustomerType').html(option);              
                
            }
        },
        error:function(){
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();           
        }
    });
}

function showRows(myThis){
    var ledgerText = $(myThis).parent().siblings()[0];
    var ledgerClass = $(ledgerText).text().replace(/ /g,'');
    var parentTr = $(myThis).parent().parent()[0];
    var trClass = $(parentTr).parent().find("."+ledgerClass);
    $(parentTr).parent().find("."+ledgerClass).removeClass('hide');
    $(myThis).addClass('hide');
    $(myThis).siblings().removeClass('hide');
}
function hideRows(myThis){
    var ledgerText = $(myThis).parent().siblings()[0];
    var ledgerClass = $(ledgerText).text().replace(/ /g,'');
    var parentTr = $(myThis).parent().parent()[0];
    var trClass = $(parentTr).parent().find("."+ledgerClass);
    $(parentTr).parent().find("."+ledgerClass).addClass('hide');
    $(myThis).addClass('hide');
    $(myThis).siblings().removeClass('hide');
}


function ValidateEmail(email) {
        var expr = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;;
        return expr.test(email);
}

//This function is used for show the list of seller name
function tabReceiverList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#customerPopUPForm").hide();
    $(".blackborder").show();
    $("#tabCustomer").removeClass('tab-detail-add');
    $("#tabCustomer").addClass('tab-detail-remove');
    $("#tabCustomerList").removeClass('tab-list-remove');    
    $("#tabCustomerList").addClass('tab-list-add');
    $("#divProducts").addClass('list');

    $("#btnReset").show();
    $("#btnSave").show();
    $('#btnUpdate').hide();
     $(".showCustomerTabs").hide();
    $('#tabCustomer').html("+Add Customer");
    clearFormDetails(".clearForm");
}

function showAddTab(){
    $("#customerPopUPForm").show();
    $(".blackborder").hide();
    $("#mainScreen").removeClass("hide");
    $("#tabCustomer").addClass('tab-detail-add');
    $("#tabCustomer").removeClass('tab-detail-remove');
    $("#tabCustomerList").removeClass('tab-list-add');
    $("#tabCustomerList").addClass('tab-list-remove');
    $("#divProducts").addClass('list');

    $("#txtFirstName").focus();
}
        