var drugTable;
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   drugs.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages drug categories data
 *************************************************************************************************** */
debugger;
	loader();
	//defining global varibale for data table
	
	//Calling these functions for binding the data 
	bindDrugCategory();
	bindUnitMeasure();
	bindAlternateDrugName();
	bindDrugId();

	$("#form_dept").hide();
    $("#drugs_list").addClass('list');    
    $("#tab_drugs").addClass('tab-list-add');
	
	//This click function is used for change the tab
    $("#tab_add_drugs").click(function() {
        showAddTab();
		clear();
    });
	
	//This function is used for show the drug list 
    $("#tab_drugs").click(function() {
    	tabReceiverList();//Calling this function for show the list 
    });	
	
	//Click function is used for validate and save the data on save button
    $("#btnSave").click(function() {
		
		var flag = "false";
		$("#txtUnitprice").val($("#txtUnitprice").val().trim());
		$("#txtUnitCost").val($("#txtUnitCost").val().trim());
		$("#txtAlternateName").val($("#txtAlternateName").val().trim());
		$("#txtDrugName").val($("#txtDrugName").val().trim());
		
        if ($("#txtUnitprice").val() == '') {
			$('#txtUnitprice').focus();
            $("#txtUnitpriceError").text("Please enter unit price");
            $("#txtUnitprice").addClass("errorStyle");
            flag = "true";
        }
		if(!(/^\d*(\.\d{1})?\d{0,9}$/).test($('#txtUnitprice').val()))
		{			
			$('#txtUnitprice').focus();
            $("#txtUnitpriceError").text("Please enter number only");
			$("#txtUnitprice").addClass("errorStyle");    
            flag = "true";
        }
		if($("#txtUnitprice").val() != ''){
			var unitCost = parseInt($("#txtUnitCost").val());
			var unitPrice = parseInt($("#txtUnitprice").val());
			if (unitPrice < unitCost) {
				$('#txtUnitprice').focus();
				$("#txtUnitpriceError").text("Please enter price greater than cost");
				$("#txtUnitprice").addClass("errorStyle");       
				flag = "true";
			}			
		}
		if ($("#txtUnitCost").val() == '') {		
			$('#txtUnitCost').focus();
            $("#txtUnitCostError").text("Please enter unit cost");
            $("#txtUnitCost").addClass("errorStyle");
            flag = "true";
        }
		if(!(/^\d*(\.\d{1})?\d{0,9}$/).test($('#txtUnitCost').val()))
		{			
			$('#txtUnitCost').focus();
            $("#txtUnitCostError").text("Please enter number only");
			$("#txtUnitCost").addClass("errorStyle");     
            flag = "true";
        }
		if ($("#selUnitMeasure").val() == '-1') {		
			$('#selUnitMeasure').focus();
            $("#selUnitMeasureError").text("Please select unit measure");
            $("#selUnitMeasure").addClass("errorStyle");
            flag = "true";
        }
		if ($("#selDrugCategory").val() == '-1') {		
			$('#selDrugCategory').focus();
            $("#selDrugCategoryError").text("Please select drug category");
            $("#selDrugCategory").addClass("errorStyle");
            flag = "true";
        }

		if ($("#txtDrugName").val() == '') {		
			$('#txtDrugName').focus();
            $("#txtDrugNameError").text("Please enter drug name");
            $("#txtDrugName").addClass("errorStyle");
            flag = "true";
        }
		
		if(flag == "true"){			
			return false;
		}	
		
		//ajax call		
		var drugCode = $("#txtDrugCode").val();
		var alternateName = $("#txtAlternateName").val();
		var drugName = $("#txtDrugName").val();
		var drugCategory = $("#selDrugCategory").val();
		var unitMeasure = $("#selUnitMeasure").val();
		var unitCost = $("#txtUnitCost").val();
		var unitPrice = $("#txtUnitprice").val();
		var alternateDrugName = $("#selAlternateDrugName").val();
		var composition = $("#txtComposition").val();
		
		var postData = {
			"operation":"save",
			"drugCode":drugCode,
			"drugName":drugName,
			"alternateName":alternateName,
			"drugCategory":drugCategory,
			"unitMeasure":unitMeasure,
			"unitCost":unitCost,
			"unitPrice":unitPrice,
			"alternateDrugName":alternateDrugName,
			"composition" : composition
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/drugs.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Drug saved successfully!!!");
					$('#messagemyModal').modal();
					tabReceiverList();
				}
				if(data =="0"){
				}				
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
		
    });

    //keyup functionality
    $("#txtAlternateName").keyup(function() {
        if ($("#txtAlternateName").val() != '') {
            $("#txtAlternateNameError").text("");
            $("#txtAlternateName").removeClass("errorStyle");
        } 
    });
	
	$("#txtDrugName").keyup(function() {
        if ($("#txtDrugName").val() != '') {
            $("#txtDrugNameError").text("");
            $("#txtDrugName").removeClass("errorStyle");
        }
    });
	
	$("#selDrugCategory").change(function() {
        if ($("#selDrugCategory").val() != '') {
            $("#selDrugCategoryError").text("");
            $("#selDrugCategory").removeClass("errorStyle");
        } 
    });
	
	$("#selUnitMeasure").change(function() {
        if ($("#selUnitMeasure").val() != '') {
            $("#selUnitMeasureError").text("");
            $("#selUnitMeasure").removeClass("errorStyle");
        } 
    });
	
	$("#txtUnitCost").keyup(function() {
        if ($("#txtUnitCost").val() != '') {
            $("#txtUnitCostError").text("");
            $("#txtUnitCost").removeClass("errorStyle");
        } 
    });
	
	$("#txtUnitprice").keyup(function() {
        if ($("#txtUnitprice").val() != '') {
            $("#txtUnitpriceError").text("");
            $("#txtUnitprice").removeClass("errorStyle");
        } 
    });

    //Datatable code
    if($('.inactive-checkbox').not(':checked')){
    	loadDataTable();
    }
	
	 //checked or not functionality to show active or inactive data
    $('.inactive-checkbox').change(function() {
    	debugger;
    	if($('.inactive-checkbox').is(":checked")){
    		drugTable.fnClearTable();
	    	drugTable.fnDestroy();
	    	drugTable="";
	    	drugTable = $('#drugsTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					// Need to redo the counters if filtered or sorted /
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  drugTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);//Calling this function for restore the data 
						}						
					});
				},
				
				"sAjaxSource":"controllers/admin/drugs.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "checked" });
				},
				"aoColumns": [
					/* { "mData": "drug_id" }, */ 
					{  
						"mData": function (o) { 	
							var drugId = o["id"];
							var drugPrefix = o["drug_prefix"];
								
							var drugIdLength = drugId.length;
							for (var i=0;i<6-drugIdLength;i++) {
								drugId = "0"+drugId;
							}
							drugId = drugPrefix+drugId;
							return drugId; 
						}
					},
					{ "mData": "name" },
					{ "mData": "alternate_name" }, 
					{ "mData": "category" },
					{ "mData": "unit_abbr" },
					{ "mData": "cost" },
					{ "mData": "price" },
					{ "mData": "alternate_drug_name" },					
					{
						"mData": function (o) { 
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }		
					},	
				],
				aoColumnDefs: [
		           { aTargets: [ 8 ], bSortable: false },
	        	]
			});
    	}
    	else{
    		drugTable.fnClearTable();
	    	drugTable.fnDestroy();
	    	drugTable="";
	    	loadDataTable();
    	}
    });
	
	//Click function for reset button
	$("#btnReset").click(function(){
    	$("#txtDrugName").focus();
    	clear();
    });
});//Close the document.ready function 

//Calling this function for edit button
function editClick(id,drugId,drugName,composition,alternateName,drugCategory,unit,cost,price,alternateDrugName,drugCode){	
	showAddTab();
    $("#btnReset").hide();
    $("#btnSave").hide();
    $('#btnUpdate').show();
    $('#tab_add_drugs').html("+Update Drug");

	
	$('#txtDrugCode').val(drugId);
	$('#txtComposition').val(composition);
	$('#txtDrugName').val(drugName);
	$('#txtAlternateName').val(alternateName);
	$('#selDrugCategory').val(drugCategory);
	$('#selUnitMeasure').val(unit);
	$('#txtUnitCost').val(cost);
	$('#txtUnitprice').val(price);
	$('#selAlternateDrugName').val(alternateDrugName);
	$("#txtDrugName").focus();
	$('#selectedRow').val(id);
	
	
	//Click function for update button			
	$("#btnUpdate").click(function(){
		/*focus on first element*/
		$("#txtDrugName").focus();
		var flag = "false";
		$("#txtUnitprice").val($("#txtUnitprice").val().trim());
		$("#txtUnitCost").val($("#txtUnitCost").val().trim());
		$("#txtAlternateName").val($("#txtAlternateName").val().trim());
		$("#txtDrugName").val($("#txtDrugName").val().trim());
		
        if ($("#txtUnitprice").val() == '') {
			$('#txtUnitprice').focus();
            $("#txtUnitpriceError").text("Please enter unit price");
            $("#txtUnitprice").addClass("errorStyle");
            flag = "true";
        }
		if(!(/^\d*(\.\d{1})?\d{0,9}$/).test($('#txtUnitprice').val()))
		{			
			$('#txtUnitprice').focus();
            $("#txtUnitpriceError").text("Please enter number only");
			$("#txtUnitprice").addClass("errorStyle");    
            flag = "true";
        }
		if($("#txtUnitprice").val() == ''){
			var unitCost = parseInt($("#txtUnitCost").val());
			var unitPrice = parseInt($("#txtUnitprice").val());
			if (unitPrice < unitCost) {
				$('#txtUnitprice').focus();
	            $("#txtUnitpriceError").text("Please enter price greater than cost");
				$("#txtUnitprice").addClass("errorStyle");      
	            flag = "true";
	        }
	    }
		if ($("#txtUnitCost").val() == '') {		
			$('#txtUnitCost').focus();
            $("#txtUnitCostError").text("Please enter unit cost");
            $("#txtUnitCost").addClass("errorStyle");
            flag = "true";
        }
		if(!(/^\d*(\.\d{1})?\d{0,9}$/).test($('#txtUnitCost').val()))
		{			
			$('#txtUnitCost').focus();
            $("#txtUnitCostError").text("Please enter number only");
			$("#txtUnitCost").addClass("errorStyle");      
            flag = "true";
        }
		if ($("#selUnitMeasure").val() == '-1') {		
			$('#selUnitMeasure').focus();
            $("#selUnitMeasureError").text("Please select unit measure");
            $("#selUnitMeasure").addClass("errorStyle");
            flag = "true";
        }
		if ($("#selDrugCategory").val() == '-1') {		
			$('#selDrugCategory').focus();
            $("#selDrugCategoryError").text("Please select drug category");
            $("#selDrugCategory").addClass("errorStyle");
            flag = "true";
        }
		if ($("#txtDrugName").val() == '') {		
			$('#txtDrugName').focus();
            $("#txtDrugNameError").text("Please enter drug name");
            $("#txtDrugName").addClass("errorStyle");
            flag = "true";
        }
		
		if(flag == "true"){			
			return false;
		}
		
		var drugCode = $("#txtDrugCode").val();
		var alternateName = $("#txtAlternateName").val();
		var drugName = $("#txtDrugName").val();
		var drugCategory = $("#selDrugCategory").val();
		var unitMeasure = $("#selUnitMeasure").val();
		var unitCost = $("#txtUnitCost").val();
		var unitPrice = $("#txtUnitprice").val();
		var alternateDrugName = $("#selAlternateDrugName").val();
		var composition = $("#txtComposition").val();
		var id = $('#selectedRow').val();			
			
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to update this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){	
			$.ajax({
				type: "POST",
				cache: "false",
				url: "controllers/admin/drugs.php",
				data :{            
					"operation" : "update",
					"id" : id,
					"drugCode":drugCode,
					"drugName":drugName,
					"alternateName":alternateName,
					"drugCategory":drugCategory,
					"unitMeasure":unitMeasure,
					"unitCost":unitCost,
					"unitPrice":unitPrice,
					"composition":composition,
					"alternateDrugName":alternateDrugName				
				},
				success: function(data) {	
					if(data != "0" && data != ""){
						$('#myModal').modal('hide');
					    $('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Drug updated successfully!!!");
						tabReceiverList();
						$('#messagemyModal').modal();
						
					}
					else{
						
					}
				},
				error:function()
				{
					$('.close-confirm').click();
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();			  
				}
			});
		});
	});
}

//Click function for delete the drug
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete drug");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteDrugs("'+type+'");');
}

//Click function for restore the drug
function restoreClick(id){
	$('#selectedRow').val(id);
	$('.modal-body').text("");	
	$('#confirmMyModalLabel').text("Restore drug");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();
	
	var type="restore";
	$('#confirm').attr('onclick','deleteDrugs("'+type+'");');
}
//function for delete the drugList
function deleteDrugs(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
			
		//Ajax call for delete the drug 	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/drugs.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Drug deleted successfully!!!");
					drugTable.fnReloadAjax();
					$('#messagemyModal').modal();
				}
			},
			error:function()
			{
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();  
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
	
		//Ajax call for restore the drug 
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/drugs.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Drug restored successfully!!!");
					drugTable.fnReloadAjax();
					$('#messagemyModal').modal();
					 clear();
				}			
			},
			error:function()
			{
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();		  
			}
		});
	}
}

//This function is used for binding the drug 
function bindDrugCategory(){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/drugs.php",
		data: {
			"operation":"showDrugCategory"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>-- Select --</option>";
				for (var i=0;i<parseData.length;i++)
				{
					option+="<option value='"+parseData[i].id+"'>"+parseData[i].category+"</option>";
				}
				$('#selDrugCategory').html(option);				
				
			}
		},
		error:function(){
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();			
		}
	});
}

//This function is used for binding the unit measure 
function bindUnitMeasure(){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/drugs.php",
		data: {
			"operation":"showUnitMeasure"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>-- Select --</option>";
				for (var i=0;i<parseData.length;i++)
				{
					option+="<option value='"+parseData[i].id+"'>"+parseData[i].unit_abbr+"</option>";
				}
				$('#selUnitMeasure').html(option);				
				
			}
		},
		error:function(){
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();			
		}
	});
}

//This function is used for binding the altername drug name 
function bindAlternateDrugName(){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/drugs.php",
		data: {
			"operation":"showAlternateDrugName"
		},
		success: function(data) {	
			if(data != null && data != ""){
				var parseData= jQuery.parseJSON(data);
			
				var option ="<option value='-1'>-- Select --</option>";
				for (var i=0;i<parseData.length;i++)
				{
					option+="<option value='"+parseData[i].id+"'>"+parseData[i].name+"</option>";
				}
				$('#selAlternateDrugName').html(option);				
				
			}
		},
		error:function(){
			$('.modal-body').text("");
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();			
		}
	});
}

//This function is used for binding the drug id
function bindDrugId(){
	$.ajax({					
		type: "POST",
		cache: false,
		url: "controllers/admin/drugs.php",
		data: {
			"operation":"showDrugId"
		},
		success: function(data) {	
		
			if (data != "0" && data != "") {
				var parseData = jQuery.parseJSON(data);
				
                var DrugId = parseData[0][0].match(/-*[0-9]+/);
				if(DrugId != null){
					var prefix = parseData[0][1];
					DrugId = parseInt(DrugId) + 1;
					var IdLength = DrugId.toString().length;
					for (var i=0;i<6-IdLength;i++) {
						DrugId = "0"+DrugId;
					}
					DrugId = prefix+DrugId;
						
					$("#txtDrugCode").val(DrugId);
				}
				else{
					var prefix = parseData[0][0];
					DrugId = prefix+"000001";
					$("#txtDrugCode").val(DrugId);
				}				
            }
		},
		error:function(){
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();			
		}
	});
}

//This function is used for clear teh data 
function clear(){
	$('#txtAlternateName').val("");
	$('#txtAlternateNameError').text("");
	$('#txtDrugName').val("");
	$('#txtDrugNameError').text("");
	$('#txtDrugNameError').text("");
	$('#selDrugCategory').val("-1");
	$('#selDrugCategoryError').text("");
	$('#selUnitMeasure').val("-1");
	$('#selUnitMeasureError').text("");
	$('#txtUnitCost').val("");
	$('#txtUnitCostError').text("");
	$('#txtUnitprice').val("");
	$('#txtUnitpriceError').text("");
	$('#txtComposition').val("");
	$('#selAlternateDrugName').val("-1");

	$('#txtDrugName').removeClass("errorStyle");
	$('#txtAlternateName').removeClass("errorStyle");
	$('#selUnitMeasure').removeClass("errorStyle");
	$('#txtUnitprice').removeClass("errorStyle");
	$('#selDrugCategory').removeClass("errorStyle");
	$('#txtUnitCost').removeClass("errorStyle");
}
//click escape button to close popup
$(document).keyup(function(event) {
    if(event.keyCode === 27) {
        $('.close').click();
    }
});

function loadDataTable(){
	drugTable = $('#drugsTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {
			// Need to redo the counters if filtered or sorted /
			$('.update').unbind();
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = drugTable.fnGetData(data);
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					/* var drugId = mData["drug_id"]; */
					var drugId = mData["id"];
					var drugPrefix = mData["drug_prefix"];
						
					var drugIdLength = drugId.length;
					for (var i=0;i<6-drugIdLength;i++) {
						drugId = "0"+drugId;
					}
					drugId = drugPrefix+drugId;
					var drugName = mData["name"];
					var alternateName = mData["alternate_name"];
					var drugCategory = mData["drug_category_id"];
					var unit = mData["unit"];
					var cost = mData["cost"];
					var price = mData["price"];
					var alternateDrugName = mData["alternate_drug_id"];	
					var composition = mData["composition"];	
					//Calling this function for update the data 
					editClick(id,drugId,drugName,composition,alternateName,drugCategory,unit,cost,price,alternateDrugName);       
				}
			});	
			$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  drugTable.fnGetData(data);
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);//Calling this function for delete the data 
				}
			});			
		},
		
		"sAjaxSource":"controllers/admin/drugs.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			/* { "mData": "drug_id" },  */
			{  
				"mData": function (o) { 	
					var drugId = o["id"];
					var drugPrefix = o["drug_prefix"];
						
					var drugIdLength = drugId.length;
					for (var i=0;i<6-drugIdLength;i++) {
						drugId = "0"+drugId;
					}
					drugId = drugPrefix+drugId;
					return drugId; 
				}
			},
			{ "mData": "name" },
			{ "mData": "alternate_name" }, 
			{ "mData": "category" },
			{ "mData": "unit_abbr" },
			{ "mData": "cost" },
			{ "mData": "price" },
			{ "mData": "alternate_drug_name" },
			
			{
				"mData": function (o) { 
				return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
				   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
				   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
				   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
				   "  data-original-title='Delete'></i>"; 
				}		
			},	
		],
		aoColumnDefs: [
		   { aTargets: [ 8 ], bSortable: false },
		]
	});
}
function tabReceiverList(){
	$('#inactive-checkbox-tick').prop('checked', false).change();
	$("#form_dept").hide();
	$(".blackborder").show();
    $("#tab_add_drugs").removeClass('tab-detail-add');
    $("#tab_add_drugs").addClass('tab-detail-remove');
    $("#tab_drugs").removeClass('tab-list-remove');    
    $("#tab_drugs").addClass('tab-list-add');
    $("#drugs_list").addClass('list');

   
    $("#btnReset").show();
    $("#btnSave").show();
    $('#btnUpdate').hide();
    $('#tab_add_drug').html("+Add Drug");
	clear();
}

function showAddTab(){
    $("#form_dept").show();
    $(".blackborder").hide();

    $("#tab_add_drugs").addClass('tab-detail-add');
    $("#tab_add_drugs").removeClass('tab-detail-remove');
    $("#tab_drugs").removeClass('tab-list-add');
    $("#tab_drugs").addClass('tab-list-remove');
    $("#drugs_list").addClass('list');  

			
	$("#txtAlternateNameError").text("");
    $("#txtAlternateName").removeClass("errorStyle");
    $("#txtDrugName").removeClass("errorStyle");
    $("#selDrugCategory").removeClass("errorStyle");
    $("#selUnitMeasure").removeClass("errorStyle");
    $("#txtUnitCost").removeClass("errorStyle");
    $("#txtUnitprice").removeClass("errorStyle");
	$('#txtDrugName').focus();
}
//# sourceURL=filename.js