$(document).ready(function() {
	debugger;
	$(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});

	var startDate = new Date();
	$('#toDate,#fromDate').datepicker('setEndDate', startDate);
	allGlAccount();//call from common.js
	google.charts.setOnLoadCallback(drawChart);
});
function drawChart() {
	var bigArray = '';
	$('#selGlAcount').change(function(){
		var glAccountId = $(this).val();
		if (glAccountId !='') {			
			var toDate = $("#toDate").val().trim();
			var fromDate = $("#fromDate").val().trim();
			var postData = {
				"operation" : "showChartData",
				'toDate' : toDate,
				'fromDate' : fromDate,
				'glAccountId' : glAccountId

			}
			dataCall("./controllers/admin/cash_collection.php", postData, function (result) {
				if (result.trim() != "") {
					var parseData = JSON.parse(result);
					var labCash = parseInt(parseData[0]['Lab Cash']);
					var forensicCash  = parseInt(parseData[0]['Forensic Cash']);
					var procedureCash = parseInt(parseData[0]['Procedure Cash']);
					var radiologyCash = parseInt(parseData[0]['Radiology Cash']);
					var serviceCash = parseInt(parseData[0]['Service Cash']);

					if (labCash == null) {
						labCash = 0;
					}
					if (forensicCash == null) {
						forensicCash = 0;
					}
					if (procedureCash == null) {
						procedureCash = 0;
					}
					if (radiologyCash == null) {
						radiologyCash = 0;
					}
					if (serviceCash == null) {
						serviceCash = 0;
					}
					bigArray = [
						['Name','Cash Collection'],
						['Lab Cash Collection', labCash],
						['Forensic Cash Collection', forensicCash],
						['Procedure Cash Collection', procedureCash],
						['Radiology Cash Collection' ,radiologyCash],
						['Service Cash Collection', serviceCash]
		            ];					

					var dataCash = google.visualization.arrayToDataTable(bigArray);
					var optionsCash = {
				      title: 'Cash Collection','height':800
				    };
				    var chartCash = new google.visualization.ColumnChart(document.getElementById('chart_div'));
					chartCash.draw(dataCash, optionsCash);
				}
			});
		}
	});
}