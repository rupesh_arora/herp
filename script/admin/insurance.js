/*
 * File Name    :   Insurance
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   21st feb, 2016
 * Description  :   This page use for load,save,update,delete,resotre operation
 */
var insuranceTable; //defining a global varibale for data table
$(document).ready(function() {
    loader();
    debugger;
    //Calling function for bind the insurance company 
    bindCompany();

    //Calling function for bind the insurance type

    //Change event of company dropdown
    $("#selInsuranceCompany").change(function(){
        var htmlRoom ="<option value='-1'>Select</option>";
        
        if ($('#selInsuranceCompany :selected').val() != -1) {
            var value = $('#selInsuranceCompany :selected').val();
            //Calling function for bind the insurance plan
            bindInsurancePlan(value, null);
        }
        else {
            $('#selWard').html(htmlRoom);
        }
    });
    if($("#hdnInsurance").val() == "1"){
        $("#insurance_list").hide();
        $("#searchPatientIcon").hide();
        $("#form_dept").show();
        $(".blackborder").hide();
        $("#txtPatientId").attr('disabled','disabled');
        $("#txtPatientId").val($("#hdnPatientId").val());
    }

	// bind company schemeon change company
	$("#selInsurancePlan").change(function() {
		var option = "<option value=''>--Select--</option>";
		
		if ($("#selInsurancePlan :selected") != "") {
			var value = $("#selInsurancePlan :selected").val();
			
			//on change call this function for load state
			bindPlanName(value,''); 
		} else {
			$("#selPlanName").html(option);
		}	

		if ($("#selInsurancePlan").val() != "") {
			$("#selInsurancePlanError").text("");
			$("#selInsurancePlan").removeClass("errorStyle");
		}
	});
	
    //Datepicker function 
    $("#txtMemberSince").datepicker({ changeYear: true,changeMonth: true,autoclose: true,endDate: '+0d',}) .on('changeDate', function(selected){
        startDate = new Date(selected.date.valueOf());
    }); 
    $('#txtExpirationDate').datepicker({
        startDate: '+0d',
        changeYear: true,
        changeMonth: true,
        autoclose: true
    });

    $("#form_dept").hide();
    $("#insurance_list").addClass('list');    
    $("#tabInsurance").addClass('tab-list-add');
    

    //Hide table record on click tab add insurance 
    $("#tabAddInsurance").click(function() { // click on add department tab
        showAddTab();
        clear();        
        $('#txtPatientId').focus();
    });
    removeErrorMessage();
    //Show table record
    $("#tabInsurance").click(function() { // click on list department tab
        showTableList();
    });

    //Click event for search icon 
    $("#searchPatientIcon").click(function(){
        //Open modal and load view patient screen 
        $("#txtPatientId").removeClass("errorStyle");
        $("#txtPatientIdError").text("");
        $("#hdnInsurance").val("1");
        $('#InsuranceModalLabel').text("Search Patient");
        $('#InsuranceModalBody').load('views/admin/view_patients.html');
        $('#myInsuranceModal').modal();
    });

    /* click on button for save  inforamtion */
    $("#btnAddInsurance").click(function() {
        // validation
        var flag = "false";
        $("#txtPatientId").val($("#txtPatientId").val().trim());

        if ($("#txtExpirationDate").val() == '') {
            $("#txtExpirationDateError").text("Please select expiration date");
            $("#txtExpirationDate").addClass("errorStyle");
            flag = "true";
        }

        if ($("#txtMemberSince").val() == '') {
            $("#txtMemberSinceError").text("Please select since date");
            $("#txtMemberSince").addClass("errorStyle");
            flag = "true";
        }
		
		if ($("#selPlanName").val() == '') {
            $('#selPlanName').focus();
            $("#selPlanNameError").text("Please select insurance plan");
            $("#selPlanName").addClass("errorStyle");
            flag = "true";
        }
		
        if ($("#selInsurancePlan").val() == '-1') {
            $('#selInsurancePlan').focus();
            $("#selInsurancePlanError").text("Please select insurance scheme");
            $("#selInsurancePlan").addClass("errorStyle");
            flag = "true";
        }

        if ($("#selInsuranceCompany").val() == '-1') {
            $('#selInsuranceCompany').focus();
            $("#selInsuranceCompanyError").text("Please select insurance company");
            $("#selInsuranceCompany").addClass("errorStyle");
            flag = "true";
        }
		
		if ($("#txtInsuranceNumber").val() == ''  || $("#txtInsuranceNumber").val() < 0) {
            $('#txtInsuranceNumber').focus();
            $("#txtInsuranceNumberError").text("Please enter insurance number");
            $("#txtInsuranceNumber").addClass("errorStyle");
            flag = "true";
        }
		
        if ($("#txtPatientId").val() == '') {
            $('#txtPatientId').focus();
            $("#txtPatientIdError").text("Please enter patient id");
            $("#txtPatientId").addClass("errorStyle");
            flag = "true";
        }

        var patientId = $("#txtPatientId").val();
        if(patientId.length != 9){
            $("#txtPatientId").focus();
            $("#txtPatientIdError").text("Please enter valid patient id");
            $("#txtPatientId").addClass("errorStyle"); 
            flag = "true";
        }
        var insuranceNumber = $("#txtInsuranceNumber").val().trim();

        if (flag == "true") {
            return false;
        }

        //ajax call for save operation
        var expirationDate = $("#txtExpirationDate").val();
        var memberSince = $("#txtMemberSince").val();
        var insurancePlan = $("#selInsurancePlan").val();
        var insuranceCompany = $("#selInsuranceCompany").val();
        var extraInfo = $("#txtExtraInfo").val().trim();
        extraInfo = extraInfo.replace(/'/g, "&#39");
        var insuranceNumber = $("#txtInsuranceNumber").val().trim();
        var planName = $("#selPlanName").val();

        
        var patientPrefix = patientId.substring(0, 3);
        patientId = patientId.replace ( /[^\d.]/g, '' ); 
        patientId = parseInt(patientId);

        var postData = {
            "operation": "save",
            "patientId": patientId,
            "expirationDate": expirationDate,
            "memberSince": memberSince,
            "insurancePlan": insurancePlan,
            "insuranceCompany": insuranceCompany,
            "patientPrefix": patientPrefix,
            "insuranceNumber": insuranceNumber,
            "planName": planName,
            "extraInfo": extraInfo
        }

        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/insurance.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "" && data != "2" && data !='Limit Crossed' && data == "1") {
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Insurance saved successfully!!!");
                    $('#messagemyModal').modal();
                    showTableList();
                }
                else if(data == "2"){
                    $('#txtPatientId').focus();
                    $("#txtPatientIdError").text("Please enter valid patient id");
                    $("#txtPatientId").addClass("errorStyle");                    
                }
                else if(data == "Limit Crossed"){
                    callSuccessPopUp("Alert","A patient can't request for more than 3 insurance at a time.");
                }
                else if(data == "Patient not exist"){
                    $('#txtPatientId').focus();
                    $("#txtPatientIdError").text("Please enter valid patient id");
                    $("#txtPatientId").addClass("errorStyle"); 
                }
                else if(data == "Insurance details already exist"){
                    callSuccessPopUp("Alert","This patient already has same insurance details.");
                }                
                else if (data =='') {
                    callSuccessPopUp("Alert","Insurance Number already exist!!!");
                }
                else if(data.indexOf("start date") > 0){
                    callSuccessPopUp("Alert",data);
                }
                else if(data.indexOf("expire date") > 0){
                    callSuccessPopUp("Alert",data);
                }
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });

    });

    //keyup functionality for remove validation style
    $("#txtPatientId").keyup(function() {
        if ($("#txtPatientId").val() != '') {
            $("#txtPatientIdError").text("");
            $("#txtPatientId").removeClass("errorStyle");
        }
    });

    $("#txtPatientId").change(function() {
        if ($("#txtPatientId").val() != '') {
            $("#txtPatientIdError").text("");
            $("#txtPatientId").removeClass("errorStyle");
        }
    });  
	
	$("#selPlanName").change(function() {
        if ($("#selPlanName").val() != '') {
            $("#selPlanNameError").text("");
            $("#selPlanName").removeClass("errorStyle");
        }
    });
	
    $("#selInsuranceCompany").change(function() {
        if ($("#selInsuranceCompany").val() != '-1') {
            $("#selInsuranceCompanyError").text("");
            $("#selInsuranceCompany").removeClass("errorStyle");
        }
    });

    $("#selInsurancePlan").change(function() {
        if ($("#selInsurancePlan").val() != '-1') {
            $("#selInsurancePlanError").text("");
            $("#selInsurancePlan").removeClass("errorStyle");
        }
    });

    $("#txtExpirationDate").keyup(function() {
        if ($("#txtExpirationDate").val() != '') {
            $("#txtExpirationDateError").text("");
            $("#txtExpirationDate").removeClass("errorStyle");
        }
    });

    $("#txtMemberSince").keyup(function() {
        if ($("#txtMemberSince").val() != '') {
            $("#txtMemberSinceError").text("");
            $("#txtMemberSince").removeClass("errorStyle");
        }
    });
	$("#txtInsuranceNumber").keyup(function() {
        if ($("#txtInsuranceNumber").val() != '') {
            $("#txtInsuranceNumberError").text("");
            $("#txtInsuranceNumber").removeClass("errorStyle");
        }
    });
    //load Datatable data on page load
    if ($('.inactive-checkbox').not(':checked')) {
        insuranceTable = $('#tblInsurance').dataTable({
            "bFilter": true,
            "processing": true,
            "bAutoWidth":false,
            "sPaginationType": "full_numbers",
            "fnDrawCallback": function(oSettings) {
                // update click event   
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = insuranceTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var patientId = mData["patient_id"];
                        var patientPrefix = mData["patient_prefix"];
                        
                        var patientIdLength = patientId.length;
                        for (var i=0;i<6-patientIdLength;i++) {
                            patientId = "0"+patientId;
                        }
                        patientId = patientPrefix+patientId;
                        var companyId = mData["insurance_company_id"];
                        var planId = mData["insurance_plan_id"];
                        var memberSince = mData["member_since"];
                        var expirationDate = mData["expiration_date"];
                        var extraInfo = mData["extra_info"];
                        var insuranceNumber = mData["insurance_number"];
                        var insurancePlanName = mData["insurance_plan_name_id"];
                        editClick(id, patientId, companyId, planId, memberSince, expirationDate, extraInfo,insuranceNumber,insurancePlanName);
                    }
                });
                // delete click event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = insuranceTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },

            "sAjaxSource": "controllers/admin/insurance.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [{
            "mData": function (o) {    
                    var patientId = o["patient_id"];
                    var patientPrefix = o["patient_prefix"];
                        
                    var patientIdLength = patientId.length;
                    for (var i=0;i<6-patientIdLength;i++) {
                        patientId = "0"+patientId;
                    }
                    patientId = patientPrefix+patientId;
                    return patientId; 
                }
            }, {
				"mData": "insurance_number"
			},{
                "mData": "company_name"
            }, {
                "mData": "plan_name"
            }, {
				"mData": "scheme_plan_name"
			}, {
                "mData": "member_since"
            }, {
                "mData": "expiration_date"
            }, {
                "mData": function(o) {
                    return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                        "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                        " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                        "  style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                        "  data-original-title='Delete'></i>";
                }
            }, ],
            aoColumnDefs: [{
                aTargets: [7],
                bSortable: false
            }]
        });
    }

    //checked or not checked functionality to show active or inactive data
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) {
            insuranceTable.fnClearTable();
            insuranceTable.fnDestroy();
            insuranceTable = "";
            insuranceTable = $('#tblInsurance').dataTable({
                "bFilter": true,
                "processing": true,
                 "bAutoWidth":false,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // restor click event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = insuranceTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var companyId = mData["insurance_company_id"];
                            var planId = mData["insurance_plan_id"];
                            var patientId = mData.patient_id
                            restoreClick(id, companyId, planId,patientId);
                        }   
                    });
                },

                "sAjaxSource": "controllers/admin/insurance.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },            
                "aoColumns": [{
                    "mData": function (o) {    
                            var patientId = o["patient_id"];
                            var patientPrefix = o["patient_prefix"];
                                
                            var patientIdLength = patientId.length;
                            for (var i=0;i<6-patientIdLength;i++) {
                                patientId = "0"+patientId;
                            }
                            patientId = patientPrefix+patientId;
                            return patientId; 
                        }
                    },{
                        "mData": "insurance_number"
                    }, {
                        "mData": "company_name"
                    }, {
                        "mData": "plan_name"
                    },  {
                        "mData": "scheme_plan_name"
                    },{
                        "mData": "member_since"
                    }, {
                        "mData": "expiration_date"
                    }, {
                    "mData": function(o) {
                        return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                    }
                }, ],
                aoColumnDefs: [{
                    aTargets: [7],
                    bSortable: false
                }]
            });
        } else {
            insuranceTable.fnClearTable();
            insuranceTable.fnDestroy();
            insuranceTable = "";
            insuranceTable = $('#tblInsurance').dataTable({
                "bFilter": true,
                "processing": true,
                "bAutoWidth":false,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // for update click event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = insuranceTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            
                            var patientId = mData["patient_id"];
                            var patientPrefix = mData["patient_prefix"];
                            
                            var patientIdLength = patientId.length;
                            for (var i=0;i<6-patientIdLength;i++) {
                                patientId = "0"+patientId;
                            }
                            patientId = patientPrefix+patientId;
                            var companyId = mData["insurance_company_id"];
                            var planId = mData["insurance_plan_id"];
                            var memberSince = mData["member_since"];
                            var expirationDate = mData["expiration_date"];
                            var extraInfo = mData["extra_info"];
							var insuranceNumber = mData["insurance_number"];
							var insurancePlanName = mData["insurance_plan_name_id"];
                            editClick(id, patientId, companyId, planId, memberSince, expirationDate, extraInfo,insuranceNumber,insurancePlanName);
                        }
                    });
                    // for delete click event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = insuranceTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/insurance.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },           
                "aoColumns": [{
                    "mData": function (o) {    
                            var patientId = o["patient_id"];
                            var patientPrefix = o["patient_prefix"];
                                
                            var patientIdLength = patientId.length;
                            for (var i=0;i<6-patientIdLength;i++) {
                                patientId = "0"+patientId;
                            }
                            patientId = patientPrefix+patientId;
                            return patientId; 
                        }
                    },{
                        "mData": "insurance_number"
                    }, {
                        "mData": "company_name"
                    }, {
                        "mData": "plan_name"
                    },  {
                        "mData": "scheme_plan_name"
                    },{
                        "mData": "member_since"
                    }, {
                        "mData": "expiration_date"
                    }, {
                    "mData": function(o) {
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            "  style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            "  data-original-title='Delete'></i>";
                    }
                },],
                aoColumnDefs: [{
                    aTargets: [7],
                    bSortable: false
                }]
            });
        }
    });

    // for reset functionality
    $("#btnReset").click(function() {
        $("#txtPatientId").focus();
        $("#txtPatientId").removeClass("errorStyle");
        clear();
    });

}); //close document.ready

// use fucntion for edit department for update 
function editClick(id, patientId, companyId, planId, memberSince, expirationDate, extraInfo,insuranceNumber,insurancePlanName) {
     showAddTab();
    $('#tabAddInsurance').html("+Add Insurance Member");
    $("#btnReset").hide();
    $("#btnAddInsurance").hide();
    $("#btnUpdateInsurance").removeAttr("style");
    $("#txtPatientIdError").text("");
    $("#txtPatientId").removeClass("errorStyle");
    $('#txtPatientId').val(patientId);
    $('#txtPatientId').attr('disabled','disabled'); 
    $('#searchPatientIcon').hide();
    $("#txtExpirationDate").val(expirationDate);
    $("#txtMemberSince").val(memberSince);
	
	bindInsurancePlan(companyId, planId);
	bindPlanName(planId, insurancePlanName);
	
    //$("#myModal #selInsurancePlan").val(planId);
    $("#selInsuranceCompany").val(companyId);
    $("#txtExtraInfo").val(extraInfo); 
    $("#txtInsuranceNumber").val(insuranceNumber); 

	//Change event of company dropdown
    $("#selInsuranceCompany").change(function(){
        var htmlRoom ="<option value='-1'>Select</option>";
        
        if ($('#selInsuranceCompany :selected').val() != -1) {
            var value = $('#selInsuranceCompany :selected').val();
            //Calling function for bind the insurance plan
            bindInsurancePlan(value, null);
        }
        else {
            $('#selWard').html(htmlRoom);
        }
    });
	// bind company schemeon change company
	$("#selInsurancePlan").change(function() {
		var option = "<option value=''>--Select--</option>";
		
		if ($("#selInsurancePlan :selected") != "") {
			var value = $("#selInsurancePlan :selected").val();
			
			//on change call this function for load state
			bindPlanName(value,''); 
		} else {
			$("#selPlanName").html(option);
		}	

		if ($("#selInsurancePlan").val() != "") {
			$("#selInsurancePlanError").text("");
			$("#selInsurancePlan").removeClass("errorStyle");
		}
	});
	
    $("#txtMemberSince").datepicker({        
        changeYear: true,
        changeMonth: true,
        autoclose: true
    });

    $("#txtExpirationDate").datepicker({  
        startDate: '+0d',      
        changeYear: true,
        changeMonth: true,
        autoclose: true
    });

    $("#txtMemberSince").click(function() {
        $(".datepicker").css({
            "z-index": "99999999999"
        });
    });

    $("#txtExpirationDate").click(function() {
        $(".datepicker").css({
            "z-index": "99999999999"
        });
    });

    $('#selectedRow').val(id);

    $("#btnUpdateInsurance").click(function() {
        var flag = "false";

        $("#txtPatientId").val($("#txtPatientId").val().trim());

        if ($("#txtExpirationDate").val() == '') {
            $('#txtExpirationDate').focus();
            $("#txtExpirationDateError").text("Please select expiration date");
            $("#txtExpirationDate").addClass("errorStyle");
            flag = "true";
        }

        if ($("#txtMemberSince").val() == '') {
            $('#txtMemberSince').focus();
            $("#txtMemberSinceError").text("Please slect since date");
            $("#txtMemberSince").addClass("errorStyle");
            flag = "true";
        }
		if ($("#selPlanName").val() == '') {
            $('#selPlanName').focus();
            $("#selPlanNameError").text("Please select insurance plan");
            $("#selPlanName").addClass("errorStyle");
            flag = "true";
        }
		
        if ($("#selInsurancePlan").val() == '-1') {
            $('#selInsurancePlan').focus();
            $("#selInsurancePlanError").text("Please select insurance scheme");
            $("#selInsurancePlan").addClass("errorStyle");
            flag = "true";
        }
		
        if ($("#selInsuranceCompany").val() == '-1') {
            $('#selInsuranceCompany').focus();
            $("#selInsuranceCompanyError").text("Please select insurance company");
            $("#selInsuranceCompany").addClass("errorStyle");
            flag = "true";
        }

		if ($("#txtInsuranceNumber").val() == '' || $("#myModal #txtInsuranceNumber").val() < 0) {
            $('#txtInsuranceNumber').focus();
            $("#txtInsuranceNumberError").text("Please enter insurance number");
            $("#txtInsuranceNumber").addClass("errorStyle");
            flag = "true";
        }
        if (flag == "true") {
            return false;
        }

        //ajax call for save operation
        var expirationDate = $("#txtExpirationDate").val();
        var memberSince = $("#txtMemberSince").val();
        var insurancePlan = $("#selInsurancePlan").val();
        var insuranceCompany = $("#selInsuranceCompany").val();
        var extraInfo = $("#txtExtraInfo").val().trim();
        extraInfo = extraInfo.replace(/'/g, "&#39"); 
        var id = $('#selectedRow').val();       
		var insuranceNumber = $("#txtInsuranceNumber").val().trim();
		var planName = $("#selPlanName").val();
        var postData = {
            "operation": "update",
            "expirationDate": expirationDate,
            "memberSince": memberSince,
            "insurancePlan": insurancePlan,
            "insuranceCompany": insuranceCompany,
            "insuranceNumber": insuranceNumber,
            "id":id,
            "extraInfo": extraInfo,
            "planName": planName
        }

        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){

            $.ajax({
                type: "POST",
                cache: false,
                url: "controllers/admin/insurance.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "" && data != "2") {
                        $('#myModal').modal("hide");
                        $('.close-confirm').click();
                        $('.modal-body').text('');
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Insurance updated successfully!!!");
                        $('#messagemyModal').modal();
                        $('#txtPatientId').attr('disabled',false);
                        $('#searchPatientIcon').show();
                        showTableList();
                    }
                    else if(data == "2"){
                        $('#txtPatientId').focus();
                        $("#txtPatientIdError").text("Please enter valid patient id");
                        $("#txtPatientId").addClass("errorStyle");                    
                    }
                    else if(data == "Insurance details already exist"){
                        callSuccessPopUp("Alert","This patient already has same insurance details.");
                    }
                    else if(data == ""){
                        callSuccessPopUp("Alert","Insurance Number already exist!!!");
                    }
                    else if(data.indexOf("start date") > 0){
                        callSuccessPopUp("Alert",data);
                    }
                    else if(data.indexOf("Expiry date") > 0){
                        callSuccessPopUp("Alert",data);
                    }
                },
                error: function() {
                    $('.close-confirm').click();
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                    $('#messagemyModal').modal();
                }
            });
        });
    });
}
// click function to delete insurance
function deleteClick(id) {

    $('#selectedRow').val(id);
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Insurance");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();

    var type = "delete";
    $('#confirm').attr('onclick', 'deleteInsurance("' + type + '","");');
}

//Click function for restore the insurance
function restoreClick(id, companyId, planId,patientId) {
    $('#selectedRow').val(id);
    $("#selInsuranceCompany").val(companyId);
    bindInsurancePlan(companyId, planId);
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Restore Insurance");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();

    var type = "restore";
    $('#confirm').attr('onclick', 'deleteInsurance("' + type + '","'+patientId+'");');
}

//function for delete and restore the insurance list
function deleteInsurance(type,patientId) {
    if (type == "delete") {
        var id = $('#selectedRow').val();

        //Ajax call for delete the insurance  
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/insurance.php",
            data: {
                "operation": "delete",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Insurance deleted successfully!!!");
                    $('#messagemyModal').modal();
                    insuranceTable.fnReloadAjax();
                } else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This Insurance is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    } else {
        var id = $('#selectedRow').val();         
        if($("#selInsurancePlan").val() == null || $("#selInsuranceCompany").val() == null)
        {  
            $('.modal-body').text("");
            $('#messageMyModalLabel').text("Sorry");
            $('.modal-body').text("It can not restored successfully!!!");
            insuranceTable.fnReloadAjax();
            $('#messagemyModal').modal();
            return false;
        }

        //Ajax call for restore the insurance 
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/insurance.php",
            data: {
                "operation": "restore",
                "id": id,
                "patientId" : patientId
            },
            success: function(data) {
                if (data != "0" && data != "" && data !='Limit Crossed') {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Insurance restored successfully!!!");
                    insuranceTable.fnReloadAjax();
                    $('#messagemyModal').modal();
                }
                else if (data =='Limit Crossed') {
                    callSuccessPopUp("Alert","This patient already have 3 insurance. So you can't restore it.");
                }
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}

//definr function for bind company
function bindCompany() {
       $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/insurance_plan.php",
        data: {
            "operation": "showCompany"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selInsuranceCompany').html(option);
            }
        },
        error: function() {}
    });
}

//Define function for bind insurance plan
function bindInsurancePlan(value, planId) {
       $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/insurance.php",
        data: {
            "operation": "showPlan",
            "companyId":value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].plan_name + "</option>";
                }
                $('#selInsurancePlan').html(option);
                if(planId != null){
                    $("#selInsurancePlan").val(planId);
                }                
            }
        },
        error: function() {}
    });
}

// call function for bind scheme name
function bindPlanName(value,insurancePlanName) {
	 $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/scheme_exclusion.php",
        data: {
            "operation": "showPlanName",
            schemeId: value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].scheme_plan_name + "</option>";
                }
                $('#selPlanName').html(option);
				if(insurancePlanName !="") {
					$('#selPlanName').val(insurancePlanName);
				}
            }
        },
        error: function() {}
    });
}
// clear function
function clear() {
    $("#txtExpirationDate").val("");
    $("#txtMemberSince").val("");
    $("#selInsurancePlan").val("-1");
    $("#selPlanName").val("");
    $("#selInsuranceCompany").val("-1");
    $("#txtInsuranceNumber").val("");
    $("#txtExtraInfo").val("");
    $('#txtPatientId').val("");
    $('#txtPatientIdError').text("");
    $('#txtExpirationDateError').text("");
    $('#selInsurancePlanError').text("");
    $('#selInsuranceCompanyError').text("");
    $('#txtMemberSinceError').text("");
    $('#selPlanNameError').text("");
    $('#txtInsuranceNumberError').text("");
    $('#txtPatientId').removeClass("errorStyle");
    $('#selPlanName').removeClass("errorStyle");
    $('#txtInsuranceNumber').removeClass("errorStyle");
    $("#txtExpirationDate").removeClass("errorStyle");
    $("#txtMemberSince").removeClass("errorStyle");
    $("#selInsurancePlan").removeClass("errorStyle");
    $("#selInsuranceCompany").removeClass("errorStyle");
    $("#myModal #txtPatientId").removeClass("errorStyle");
    bindCompany();
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        $(".close").click();
    }
});

function showTableList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#form_dept").hide();
    $(".blackborder").show();

    $("#tabAddInsurance").removeClass('tab-detail-add');
    $("#tabAddInsurance").addClass('tab-detail-remove');
    $("#tabInsurance").removeClass('tab-list-remove');    
    $("#tabInsurance").addClass('tab-list-add');
    $("#insurance_list").addClass('list');    
    $("#btnReset").show();
    $("#btnAddInsurance").show();
    $('#btnUpdateInsurance').hide();
    $('#tabAddInsurance').html("+Add Insurance Member");
    clear();
}

function showAddTab(){
    $("#form_dept").show();
    $(".blackborder").hide();
   
    $("#tabAddInsurance").addClass('tab-detail-add');
    $("#tabAddInsurance").removeClass('tab-detail-remove');
    $("#tabInsurance").removeClass('tab-list-add');
    $("#tabInsurance").addClass('tab-list-remove');
    $("#insurance_list").addClass('list');
    removeErrorMessage();
}