/*
 * File Name    :   view_patients.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load and send data to back hand.
 */
  var otable; // define otable variable for datatable
  var userId;
  var imagefile;
  var patientHospitalizationId;
  var attendingConsultantId ;
  var operatingConsultantId ;
$(document).ready(function() {
loader(); 
debugger;
    $("#txtFirstName").focus();
    $("#btnReset").click(function() { // reset button functionality
        $("#txtFirstName").val("");
        $("#txtLastName").val("");
        $("#txtDOB").val("");
        $("#txtMobile").val("");
        $("#txtEmail").val("");
        otable.fnClearTable();
        $("#txtFirstName").focus();
    });
	$("#txtDOB,#myAccountNoModal #txtDOB").datepicker({ // date picker function
		autoclose: true
	});
	$("#myCashAccountModal #txtDOB").datepicker({ // date picker function
		autoclose: true
	});
	$("#mySearchModal #txtDOB").click(function() {
		$(".datepicker").css({
			"z-index": "99999"
		});
	});
	$("#mySelfRequestModal #txtDOB").click(function() {
		$(".datepicker").css({
			"z-index": "99999"
		});
	});
    $("#myInsuranceModal #txtDOB").click(function() {
        $(".datepicker").css({
            "z-index": "99999"
        });
    });

    $('#searchModalBody #btnBackToList').click(function(){
        $('#searchPatient').show();
        $('#searchPatientdetail').hide();
    });
	$("#myCashAccountModal #txtDOB,#myAccountNoModal #txtDOB").click(function() {
		$(".datepicker").css({"z-index": "99999"});
	});
    $("#myAccountNoModal #txtDOB").attr("readonly",true);

    otable = $('#viewPatientTable').dataTable({ // inilize datatable on load time.
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
		"bAutoWidth" : false,
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [5]
        }]
    });

    /*Disable space functionality on mobile number*/
    $("#txtMobile").on("keydown", function(e) {
        return e.which !== 32;
    });
    $("#myPatientUpdate #txtphone").on("keydown", function(e) {
        return e.which !== 32;
    });
    $("#myPatientUpdate #val_COPhoneNo").on("keydown", function(e) {
        return e.which !== 32;
    });
	
	if ($("#mySearchModal #hdnOpdReg").val() != "1" && $("#myCashAccountModal #hdnCash").val() != "1" && 
	$("#myViewPatientModal #hdnViewPatientHistory").val() != "1" && $("#myAccountNoModal #hdnAccountNo").val() != "1" 
	&& $("#mySelfRequestModal #hdnSelfRequest").val() != "1" && $('#myAppointmentPatientModal #hdnAppointment').val() != "1" 
    && $("#myInsuranceModal #hdnInsurance").val() != "1" && $("#myPatientHospitalizationModal #hdnPatientHospitalization").val() != "1" 
    && $("#myInvestigationModal #hdnInvestigation").val() != "1" && $('#myTransactionModal #hdnTransaction').val() != 1) {
		var postData = {
			"operation": "searchToday"
		}
		getPatients(postData);
	}
    if ($("#myCashAccountModal #hdnCash").val() == "1") {		// for cash account pop up
        loadPatients('myCashAccountModal', 'txtPatientID', 'btnSelect' , postData ={"operation": "search"});
    }	
    if ($("#myInvestigationModal #hdnInvestigation").val() == "1") {       // for cash account pop up
        $($("#viewPatientTable thead th")[0]).text("IPD Id");
        loadPatients('myInvestigationModal', 'txtIPDId', 'btnLoadPatientId' , postData ={"operation": "searchIpdPatient"});
    }
    if ($("#myViewPatientModal #hdnViewPatientHistory").val() == "1") {	// for patient history account pop up
        loadPatients('myViewPatientModal', 'txtPatientIdHistory', 'btnShow' , postData ={"operation": "search",showVisitPatients:'showVisitPatients'});
    }
    if ($("#mySearchModal #hdnOpdReg").val() == "1") {	// for opd booking pop up
        loadPatients('mySearchModal', 'txtHrn', 'btnSelect' , postData={"operation": "search"});
    }
    if ($("#mySelfRequestModal #hdnSelfRequest").val() == 1) {
        //this hidden field from patient ot registration 
        if ($("#patientOTReg").val() == 1) {
            loadPatients('mySelfRequestModal', 'txtPatientid', '' , postData={"operation": "search"});
        }
        else{
            loadPatients('mySelfRequestModal', 'txtPatientId', 'btnLoadPatientId' , postData={"operation": "search"});
        }
    }
    if ($("#myInsuranceModal #hdnInsurance").val() == 1) {
        loadPatients('myInsuranceModal', 'txtPatientId', '' , postData={"operation": "search"});
        
    }
	if ($("#myPatientHospitalizationModal #hdnPatientHospitalization").val() == 1) {
		$(".btnPatientHospitalization").show();
		$("#btnBackToList").hide();		
        loadPatients('myPatientHospitalizationModal', '', '' , postData={"operation": "searchIPD"});        
    }
    if($('#myTransactionModal #hdnTransaction').val() == 1){
        loadPatients('myTransactionModal', 'txtPatientId', '', postData={"operation": "search"});
    }

    /* search functioanlty for patient details on search button*/
    $("#viewPatientForm #btnSearch").click(function() {
		if ($("#mySearchModal #hdnOpdReg").val() != "1" && $("#myCashAccountModal #hdnCash").val() != "1" && 
		$("#myViewPatientModal #hdnViewPatientHistory").val() != "1" && $("#myAccountNoModal #hdnAccountNo").val() != "1" 
		&& $("#mySelfRequestModal #hdnSelfRequest").val() != "1" && $('#myAppointmentPatientModal #hdnAppointment').val() != "1" 
         && $("#myInsuranceModal #hdnInsurance").val() != "1" && $("#myPatientHospitalizationModal #hdnPatientHospitalization").val() != "1" 
          && $("#myInvestigationModal #hdnInvestigation").val() != "1" && $('#myTransactionModal #hdnTransaction').val() != 1) {
			var firstName = $("#txtFirstName").val().trim();
			var lastName = $("#txtLastName").val().trim();
			var dob = $("#txtDOB").val().trim();
			var mobile = $("#txtMobile").val().trim();
			var email = $("#txtEmail").val().trim();			
			var clickData = 1;

			var postData = {
				"operation": "search",
				"firstName": firstName,
				"lastName": lastName,
				"dob": dob,
				"mobile": mobile,
				"email": email,
				"clickData" : clickData
			}

			getPatients(postData);
		}
    });
    //$('#mySearchModal #viewPatientTable').hide();
    //Code of reset on search modal
    $("#mySearchModal #btnReset").click(function() {
        $("#mySearchModal #txtFirstName").val("");
        $("#mySearchModal #txtLastName").val("");
        $("#mySearchModal #txtDOB").val("");
        $("#mySearchModal #txtMobile").val("");
        $("#mySearchModal #txtEmail").val("");
        otable.fnClearTable();
        $("#mySearchModal #txtFirstName").focus();
    });

     $("#myInsuranceModal #btnReset").click(function() {
        $("#myInsuranceModal #txtFirstName").val("");
        $("#myInsuranceModal #txtLastName").val("");
        $("#myInsuranceModal #txtDOB").val("");
        $("#myInsuranceModal #txtMobile").val("");
        $("#myInsuranceModal #txtEmail").val("");
        otable.fnClearTable();
        $("#myInsuranceModal #txtFirstName").focus();
    });

    //search details on click in search modal for patient visit for view opd booking screen 
    $("#mySearchModal #btnSearch").click(function() {
		//$('#mySearchModal #viewPatientTable').show();
		if ($("#mySearchModal #hdnOpdReg").val() == "1"){
			var firstName = $("#mySearchModal #txtFirstName").val().trim();
			var lastName = $("#mySearchModal #txtLastName").val().trim();
			var dob = $("#mySearchModal #txtDOB").val().trim();
			var mobile = $("#mySearchModal #txtMobile").val().trim();
			var email = $("#mySearchModal #txtEmail").val().trim();
            var clickData = 1;

			var postData = {
				"operation": "search",
				"firstName": firstName,
				"lastName": lastName,
				"dob": dob,
				"mobile": mobile,
				"email": email,
                "clickData" : clickData
			}
			
			loadPatients('mySearchModal', 'txtHrn', 'btnSelect' , postData);		
		} 
	});
		
    $("#mySearchModal #btnBackToList").click(function() {
        $("#mySearchModal #searchPatient").show();
        $("#mySearchModal #searchPatientdetail").hide();
    });

    //search details on click in search modal for patient visit for view opd booking screen 
    $("#myInsuranceModal #btnSearch").click(function() {
        //$('#mySearchModal #viewPatientTable').show();
        if ($("#myInsuranceModal #hdnInsurance").val() == "1"){
            var firstName = $("#myInsuranceModal #txtFirstName").val().trim();
            var lastName = $("#myInsuranceModal #txtLastName").val().trim();
            var dob = $("#myInsuranceModal #txtDOB").val().trim();
            var mobile = $("#myInsuranceModal #txtMobile").val().trim();
            var email = $("#myInsuranceModal #txtEmail").val().trim();
            var clickData = 1;

            var postData = {
                "operation": "search",
                "firstName": firstName,
                "lastName": lastName,
                "dob": dob,
                "mobile": mobile,
                "email": email,
                "clickData" : clickData
            }
            
            loadPatients('myInsuranceModal', 'txtPatientId', '' , postData);        
        } 
    });

    $("#myTransactionModal #btnSearch").click(function() {
        //$('#mySearchModal #viewPatientTable').show();
        if ($("#myTransactionModal #hdnTransaction").val() == "1"){
            var firstName = $("#myTransactionModal #txtFirstName").val().trim();
            var lastName = $("#myTransactionModal #txtLastName").val().trim();
            var dob = $("#myTransactionModal #txtDOB").val().trim();
            var mobile = $("#myTransactionModal #txtMobile").val().trim();
            var email = $("#myTransactionModal #txtEmail").val().trim();
            var clickData = 1;

            var postData = {
                "operation": "search",
                "firstName": firstName,
                "lastName": lastName,
                "dob": dob,
                "mobile": mobile,
                "email": email,
                "clickData" : clickData
            }
            
            loadPatients('myTransactionModal', 'txtPatientId', '' , postData);        
        } 
    });
        
    $("#myInsuranceModal #btnBackToList").click(function() {
        $("#myInsuranceModal #searchPatient").show();
        $("#myInsuranceModal #searchPatientdetail").hide();
    });

     //search details on click in search modal for patient visit for view opd booking screen 
    $("#myAppointmentPatientModal #btnSearch").click(function() {
        //$('#mySearchModal #viewPatientTable').show();
        if ($("#myAppointmentPatientModal #hdnAppointment").val() == "1"){
            var firstName = $("#myAppointmentPatientModal #txtFirstName").val().trim();
            var lastName = $("#myAppointmentPatientModal #txtLastName").val().trim();
            var dob = $("#myAppointmentPatientModal #txtDOB").val().trim();
            var mobile = $("#myAppointmentPatientModal #txtMobile").val().trim();
            var email = $("#myAppointmentPatientModal #txtEmail").val().trim();
            var clickData = 1;

            var postData = {
                "operation": "search",
                "firstName": firstName,
                "lastName": lastName,
                "dob": dob,
                "mobile": mobile,
                "email": email,
                "clickData" : clickData
            }
            
            loadPatients('myAppointmentPatientModal', 'txtPatientId', '' , postData);        
        } 
    });
        
    $("#myAppointmentPatientModal #btnBackToList").click(function() {
        $("#myAppointmentPatientModal #searchPatient").show();
        $("#myAppointmentPatientModal #searchPatientdetail").hide();
    });
	

     //search details on click in search modal for patient visit for view opd booking screen 
    $("#myInvestigationModal #btnSearch").click(function() {
        $($("#viewPatientTable thead th")[0]).text("IPD Id");
        //$('#mySearchModal #viewPatientTable').show();
        if ($("#myInvestigationModal #hdnInvestigation").val() == "1"){
            var firstName = $("#myInvestigationModal #txtFirstName").val().trim();
            var lastName = $("#myInvestigationModal #txtLastName").val().trim();
            var dob = $("#myInvestigationModal #txtDOB").val().trim();
            var mobile = $("#myInvestigationModal #txtMobile").val().trim();
            var email = $("#myInvestigationModal #txtEmail").val().trim();
            var clickData = 1;

            var postData = {
                "operation": "searchIpdPatient",
                "firstName": firstName,
                "lastName": lastName,
                "dob": dob,
                "mobile": mobile,
                "email": email,
                "clickData" : clickData
            }
            
            loadPatients('myInvestigationModal', 'txtIPDId', 'btnLoadPatientId' , postData);        
        } 
    });
        
    $("#myInvestigationModal #btnBackToList").click(function() {
        $("#myInvestigationModal #searchPatient").show();
        $("#myInvestigationModal #searchPatientdetail").hide();
    });

    $("#myInvestigationModal #btnReset").click(function() {
        $("#myInvestigationModal #txtFirstName").val("");
        $("#myInvestigationModal #txtLastName").val("");
        $("#myInvestigationModal #txtDOB").val("");
        $("#myInvestigationModal #txtMobile").val("");
        $("#myInvestigationModal #txtEmail").val("");
        otable.fnClearTable();
        $("#myInvestigationModal #txtFirstName").focus();
    });

	//search details on click in search modal for patient visit for view opd booking screen 
    $("#mySelfRequestModal #btnSearch").click(function() {
		if ($("#mySelfRequestModal #hdnSelfRequest").val() == "1"){
			var firstName = $("#mySelfRequestModal #txtFirstName").val().trim();
			var lastName = $("#mySelfRequestModal #txtLastName").val().trim();
			var dob = $("#mySelfRequestModal #txtDOB").val().trim();
			var mobile = $("#mySelfRequestModal #txtMobile").val().trim();
			var email = $("#mySelfRequestModal #txtEmail").val().trim();
            var clickData = 1;

			var postData = {
				"operation": "search",
				"firstName": firstName,
				"lastName": lastName,
				"dob": dob,
				"mobile": mobile,
				"email": email,
                "clickData" : clickData
			}
			if ($("#patientOTReg").val() == 1) {
                loadPatients('mySelfRequestModal', 'txtPatientid', '' , postData);
            }
            else{
                loadPatients('mySelfRequestModal', 'txtPatientId', 'btnLoadPatientId' , postData);
            }		
		} 
	});

	$("#mySelfRequestModal #btnBackToList").click(function() {
        $("#mySelfRequestModal #searchPatient").show();
        $("#mySelfRequestModal #searchPatientdetail").hide();
    });
	
	//Code of reset on search modal
    $("#mySelfRequestModal #btnReset").click(function() {
        $("#mySelfRequestModal #txtFirstName").val("");
        $("#mySelfRequestModal #txtLastName").val("");
        $("#mySelfRequestModal #txtDOB").val("");
        $("#mySelfRequestModal #txtMobile").val("");
        $("#mySelfRequestModal #txtEmail").val("");
        otable.fnClearTable();
        $("#mySelfRequestModal #txtFirstName").focus();
    });
	
		//search details on click in search modal for patient visit for view patient hospitalization
    $("#myPatientHospitalizationModal #btnSearch").click(function() {
			$(".btnPatientHospitalization").show();
			$("#btnBackToList").hide();	
		if ($("#myPatientHospitalizationModal #hdnPatientHospitalization").val() == "1"){
			var firstName = $("#myPatientHospitalizationModal #txtFirstName").val().trim();
			var lastName = $("#myPatientHospitalizationModal #txtLastName").val().trim();
			var dob = $("#myPatientHospitalizationModal #txtDOB").val().trim();
			var mobile = $("#myPatientHospitalizationModal #txtMobile").val().trim();
			var email = $("#myPatientHospitalizationModal #txtEmail").val().trim();
            var clickData = 1;

			var postData = {
				"operation": "searchIPD",
				"firstName": firstName,
				"lastName": lastName,
				"dob": dob,
				"mobile": mobile,
				"email": email,
                "clickData" : clickData
			}
			
			loadPatients('myPatientHospitalizationModal', '', '' , postData);		
		} 
	});

	$("#myPatientHospitalizationModal #btnSelectAgain").click(function() {
        $("#myPatientHospitalizationModal #searchPatient").show();
        $("#myPatientHospitalizationModal #searchPatientdetail").hide();
    });
	
	$("#myPatientHospitalizationModal #btnCancel").click(function() {
			$("#pop_up_close").unbind();
			$("#pop_up_close").click();
    });
	
	$("#myPatientHospitalizationModal #btnConfirmButton").click(function() {
			$('#textPatientId').val(patientid);
            $("#selAttendingConsultant").val(attendingConsultantId);
            $("#selOperatingConsultant").val(operatingConsultantId);
			$("#pop_up_close").click();
    });
	
	//Code of reset on search modal
    $("#myPatientHospitalizationModal #btnReset").click(function() {
        $("#myPatientHospitalizationModal #txtFirstName").val("");
        $("#myPatientHospitalizationModal #txtLastName").val("");
        $("#myPatientHospitalizationModal #txtDOB").val("");
        $("#myPatientHospitalizationModal #txtMobile").val("");
        $("#myPatientHospitalizationModal #txtEmail").val("");
        otable.fnClearTable();
        $("#myPatientHospitalizationModal #txtFirstName").focus();
    });
	
	//search details on click in search modal for patient visit for view opd booking screen 
    $("#myAccountNoModal #btnSearch").click(function() {
		if ($("#myAccountNoModal #hdnAccountNo").val() == "1"){
			var firstName = $("#myAccountNoModal #txtFirstName").val().trim();
			var lastName = $("#myAccountNoModal #txtLastName").val().trim();
			var dob = $("#myAccountNoModal #txtDOB").val().trim();
			var mobile = $("#myAccountNoModal #txtMobile").val().trim();
			var email = $("#myAccountNoModal #txtEmail").val().trim();
			if(firstName != '' ||  lastName != '' || dob != '' || mobile != '' || email != ''){
				var clickData = 1;

				var postData = {
					"operation": "searchPatientAccount",
					"firstName": firstName,
					"lastName": lastName,
					"dob": dob,
					"mobile": mobile,
					"email": email,
					"clickData" : clickData
				}
				
				loadPatients('myAccountNoModal', 'txtFamilyAccount', 'NULL' , postData);
			}            		
		} 
	});
		
    $("#myAccountNoModal #btnBackToList").click(function() {
        $("#myAccountNoModal #searchPatient").show();
        $("#myAccountNoModal #searchPatientdetail").hide();
    });
	
	//Code of reset on search modal
    $("#myAccountNoModal #btnReset").click(function() {
        $("#myAccountNoModal #txtFirstName").val("");
        $("#myAccountNoModal #txtLastName").val("");
        $("#myAccountNoModal #txtDOB").val("");
        $("#myAccountNoModal #txtMobile").val("");
        $("#myAccountNoModal #txtEmail").val("");
        otable.fnClearTable();
        $("#myAccountNoModal #txtFirstName").focus();
    });



    // code of patientUpdateModal
    $(function() {
        $('#myPatientUpdate #txt_DOB').datepicker({
            endDate: '+0d',
            autoclose: true

        });
    });
    $("#myPatientUpdate #resetPatient").click(function() {
        clear1();
        clearUpdatePatientStyle();

    });
    // image upload js.
    $("#myPatientUpdate #image-box").click(function() {
        $("#myPatientUpdate #upload").click();
        $("#myPatientUpdate #upload").on("change", function() {
            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

            if (/^image/.test(files[0].type)) { // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file

                reader.onloadend = function() { // set image data as background of div
                    $("#myPatientUpdate #user_reg_image").hide();
                    $("#myPatientUpdate #imagetext").hide();
                    $("#myPatientUpdate #image-box").css("background-image", "url(" + this.result + ")");

                }
            }
        });
    });



    $("#myPatientUpdate #val_Salutation").focus();
    // Ajax call for show data on country 
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/view_patients.php",
        data: {
            "operation": "showcountry"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $('#myPatientUpdate #txtCountry').html(option);
            }

        },
        error: function() {
			
			$('.modal-body').text("");
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
    });
    // Ajax call for show data on state select country 
    $('#myPatientUpdate #txtCountry').change(function() {
        var countryID = $("#txtCountry").val();
        bindState(countryID, '');
        bindCountryCode(countryID);
    });

    // Ajax call for show data on country select state 
    $('#myPatientUpdate #txtState').change(function() {
        var stateID = $("#txtState").val();
        bindCity(stateID, '');

    });

    /*Increase the z-index of datepeicker*/
    $("#myPatientUpdate #txt_DOB").click(function() {
        $(".datepicker").css({
            "z-index": "9999"
        });
    });

    /* on submit button for save details with validation */
    $("#myPatientUpdate #updatePatient").on('click', function() {
        flag = false;

        if ($("#myPatientUpdate #txtzipcode").val().trim() == "") {
            $("#myPatientUpdate #errorcode").show();
            $("#myPatientUpdate #errorcode").text("Please enter zip code");
            $("#myPatientUpdate #txtzipcode").addClass("errorStyle");
            $("#myPatientUpdate #txtzipcode").focus();
            flag = true;
        }
        if ($("#myPatientUpdate #txtCity").val() == "") {
            $("#myPatientUpdate #errorcity").show();
            $("#myPatientUpdate #errorcity").text("Please select city");
            $("#myPatientUpdate #txtCity").addClass("errorStyle");
            $("#myPatientUpdate #txtCity").focus();
            flag = true;
        }
        if ($("#myPatientUpdate #txtState").val() == "") {
            $("#myPatientUpdate #errorstate").show();
            $("#myPatientUpdate #errorstate").text("Please select state");
            $("#myPatientUpdate #txtState").addClass("errorStyle");
            $("#myPatientUpdate #txtState").focus();
            flag = true;
        }
        if ($("#myPatientUpdate #txtCountry").val() == "") {
            $("#myPatientUpdate #errorcountry").show();
            $("#myPatientUpdate #errorcountry").text("Please select country");
            $("#myPatientUpdate #txtCountry").addClass("errorStyle");
            $("#myPatientUpdate #txtCountry").focus();
            flag = true;
        }
        if (!validnumber($("#myPatientUpdate #txtmob").val().trim())) {
            $("#myPatientUpdate #errormob").show();
            $("#myPatientUpdate #errormob").text("Please enter valid mobile number");
            $("#myPatientUpdate #txtmob").addClass("errorStyle");
            flag = true;
        }
        if ($("#myPatientUpdate #txtmob").val().trim() == "") {
            $("#myPatientUpdate #errormob").show();
            $("#myPatientUpdate #errormob").text("Please enter mobile number");
            $("#myPatientUpdate #txtmob").addClass("errorStyle");
            $("#myPatientUpdate #txtmob").focus();
            flag = true;
        }
        if ($("#myPatientUpdate #txt_DOB").val() == "") {
            $("#myPatientUpdate #errordob").show();
            $("#myPatientUpdate #errordob").text("Please select date");
            $("#myPatientUpdate #txt_DOB").addClass("errorStyle");

            flag = true;
        }
        if ($("#myPatientUpdate #txtGender").val() == "") {
            $("#myPatientUpdate #errorgender").show();
            $("#myPatientUpdate #errorgender").text("Please select gender");
            $("#myPatientUpdate #txtGender").addClass("errorStyle");
            $("#myPatientUpdate #txtGender").focus();
            flag = true;
        }
        if ($("#myPatientUpdate #txtFirstName").val().trim() == "") {
            $("#myPatientUpdate #errorfname").show();
            $("#myPatientUpdate #errorfname").text("Please enter first name");
            $("#myPatientUpdate #txtFirstName").addClass("errorStyle");
            $("#myPatientUpdate #txtFirstName").focus();
            flag = true;
        }
        if ($("#myPatientUpdate #txtLastName").val().trim() == "") {
            $("#myPatientUpdate #errorlname").show();
            $("#myPatientUpdate #errorlname").text("Please enter last name");
            $("#myPatientUpdate #txtLastName").addClass("errorStyle");
            $("#myPatientUpdate #txtLastName").focus();
            flag = true;
        }
        if ($("#myPatientUpdate #val_Salutation").val() == "") {
            $("#myPatientUpdate #errorsalutation").show();
            $("#myPatientUpdate #errorsalutation").text("Please select salutation");
            $("#myPatientUpdate #val_Salutation").focus();
            $("#myPatientUpdate #val_Salutation").addClass("errorStyle");
            flag = true;
        }

        if (flag == true) {
            return false;

        } else {
			// show confirm modal for update
			$('#confirmUpdateModalLabel').text();
            $('#updateBody').text("Are you sure that you want to update this?");
            $('#confirmUpdateModal').modal();
            $("#btnConfirm").unbind();
            $("#btnConfirm").click(function(){
				// define variable to get value
				var salutation = $("#myPatientUpdate #val_Salutation").val();
				var lname = $("#myPatientUpdate #txtLastName").val().trim();
				var fname = $("#myPatientUpdate #txtFirstName").val().trim();
				var middleName = $("#myPatientUpdate #txtMiddleName").val().trim();
				var dob = $("#myPatientUpdate #txt_DOB").val();
				var gender = $("#myPatientUpdate #txtGender").val();
				var address = $("#myPatientUpdate #txtAddress").val().trim();
				var country = $("#myPatientUpdate #txtCountry").val();
				var state = $("#myPatientUpdate #txtState").val();
				var city = $("#myPatientUpdate #txtCity").val();
				var zipcode = $("#myPatientUpdate #txtzipcode").val().trim();
				var email = $("#myPatientUpdate #txtEmail").val().trim();
                var mobile = $("#myPatientUpdate #txtmob").val().trim();
				var mobileCode = $("#myPatientUpdate #mobileCode").val().trim();
				var phone = $("#myPatientUpdate #txtphone").val().trim();
				var careOff = $("#myPatientUpdate #val_CO").val().trim();
				var careOffPhone = $("#myPatientUpdate #val_COPhoneNo").val().trim();

				var file_data = $('#myPatientUpdate #upload').prop('files')[0];
				var form_data = new FormData();
				form_data.append('file', file_data);
				var imageName;
				$.ajax({
					url: 'controllers/admin/view_patients.php', // point to server-side PHP script 
					dataType: 'text', // what to expect back from the PHP script, if anything
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'post',
					success: function(data) {
						if (data == "invalid file") {
							$('.modal-body').text("");
							$(".close-modal").click();
							$("#myPatientUpdate #errorimage").show();
							$("#myPatientUpdate #errorimage").text("Please upload image file only.");
						} else {
							imageName = data;
							// for ajax call for insert
							$.ajax({
								type: 'POST',
								cache: false,
								url: "controllers/admin/view_patients.php",
								data: {
									id: userId,
									salutation: salutation,
									Lname: lname,
									Fname: fname,
									middleName: middleName,
									Dob: dob,
									Gender: gender,
									Address: address,
									Country: country,
									State: state,
									City: city,
									Zipcode: zipcode,
									Email: email,
									Mobile: mobile,
                                    mobileCode:mobileCode,
									Phone: phone,
									CareOff: careOff,
									CareOffPhone: careOffPhone,
									imageName: imageName,
									imagesPath: imagefile,
									operation: "updateDetails"
								},
								success: function(data) {
									$('#myPatientUpdate').modal("hide");
									$(".close-modal").click();
									$('.modal-body').text("");
									$('#messageMyModalLabel').text("Success");
									$('.modal-body').text("Patient details updated successfully!!!");
									$('#messagemyModal').modal();
									var postData = {
										"operation": "searchToday"
									}
									getPatients(postData);
									//$('#btnSearch').click();

								},
								error: function() {
									$(".close-modal").click();
									$('.modal-body').text("");
									$('#messageMyModalLabel').text("Error");
									$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
									$('#messagemyModal').modal();
								},							
							});
						}
					}
				});
			});
        }
    });

    // for remove error style	

    $("#myPatientUpdate input[type=text]").keyup(function() {

        if ($(this).val() != "") {
            $(this).next("span").hide();
            $(this).removeClass("errorStyle");
            //	$("label[for='"+$(this).attr("id")+"']").removeAttr("style");
        }
    });
    $("#myPatientUpdate select").on('change', function() {

        if ($(this).val() != "") {
            $(this).next("span").hide();
            /*  $(this).removeAttr("style"); */

            $(this).addClass("errorStyle");
            //	$("label[for='"+$(this).attr("id")+"']").removeAttr("style");
        }
    });
    $("#myPatientUpdate #txtDOB").change(function() {
        $("#myPatientUpdate #errordob").hide();
        $("#myPatientUpdate #txtDOB").removeClass("errorStyle");
        $("#myPatientUpdate #txtDOB").datepicker("hide");
    });
    $("#myPatientUpdate #txtGender").change(function() {
        if ($("#myPatientUpdate #txtGender").val() != "") {
            $("#myPatientUpdate #txtGender").removeClass("errorStyle");
        } else {
            $("#myPatientUpdate #txtGender").addClass("errorStyle");
        }
    });
    $("#myPatientUpdate #val_Salutation").change(function() {
        if ($("#myPatientUpdate #val_Salutation").val() != "") {
            $("#myPatientUpdate #val_Salutation").removeClass("errorStyle");
        } else {
            $("#myPatientUpdate #val_Salutation").addClass("errorStyle");
        }
    });
    $("#myPatientUpdate #txtState").change(function() {
        if ($("#myPatientUpdate #txtState").val() != "") {
            $("#myPatientUpdate #txtState").removeClass("errorStyle");
        } else {
            $("#myPatientUpdate #txtState").addClass("errorStyle");
        }
    });
    $("#myPatientUpdate #txtCountry").change(function() {
        if ($("#myPatientUpdate #txtCountry").val() != "") {
            $("#myPatientUpdate #txtCountry").removeClass("errorStyle");
        } else {
            $("#myPatientUpdate #txtCountry").addClass("errorStyle");
        }
    });
    $("#myPatientUpdate #txtCity").change(function() {
        if ($("#myPatientUpdate #txtCity").val() != "") {
            $("#myPatientUpdate #txtCity").removeClass("errorStyle");
        } else {
            $("#myPatientUpdate #txtCity").addClass("errorStyle");
        }
    });
	
	 //search details on click in search modal for view patient history screen
    $("#myViewPatientModal #btnSearch").click(function() {
		//$('#myViewPatientModal #viewPatientTable').show();
		var firstName = $("#myViewPatientModal #txtFirstName").val().trim();
		var lastName = $("#myViewPatientModal #txtLastName").val().trim();
		var dob = $("#myViewPatientModal #txtDOB").val().trim();
		var mobile = $("#myViewPatientModal #txtMobile").val().trim();
		var email = $("#myViewPatientModal #txtEmail").val().trim();	
		var clickData = 1;
	
		var postData = {
			"operation": "search",
			"firstName": firstName,
			"lastName": lastName,
			"dob": dob,
			"mobile": mobile,
			"email": email,
			"clickData": clickData,
            "showVisitPatients" : "showVisitPatients"
		}
		
		loadPatients('myViewPatientModal', 'txtPatientIdHistory', 'btnShow' , postData);	
		
    });
    $("#myViewPatientModal #btnBackToList").click(function() {
        $("#myViewPatientModal #searchPatient").show();
        $("#myViewPatientModal #searchPatientdetail").hide();
    });
	
	
	 //Code of reset on search modal
    $("#myCashAccountModal #btnReset").click(function() {
        $("#myCashAccountModal #txtFirstName").val("");
        $("#myCashAccountModal #txtLastName").val("");
        $("#myCashAccountModal #txtDOB").val("");
        $("#myCashAccountModal #txtMobile").val("");
        $("#myCashAccountModal #txtEmail").val("");
        otable.fnClearTable();
        $("#myCashAccountModal #txtFirstName").focus();
    });

    /*Call function during pop up load so to get today data*/
    //loadPatients('myCashAccountModal', 'txtPatientID', 'btnSelect' , postData ={"operation": "search"});
    //search details on click in search modal for patient visit for view opd booking screen 
    $("#myCashAccountModal #btnSearch").click(function() {
		var firstName = $("#myCashAccountModal #txtFirstName").val().trim();
		var lastName = $("#myCashAccountModal #txtLastName").val().trim();
		var dob = $("#myCashAccountModal #txtDOB").val().trim();
		var mobile = $("#myCashAccountModal #txtMobile").val().trim();
		var email = $("#myCashAccountModal #txtEmail").val().trim();
        var clickData = 1;
		var postData = {
			"operation": "search",
			"firstName": firstName,
			"lastName": lastName,
			"dob": dob,
			"mobile": mobile,
			"email": email,
            "clickData" : clickData
		}
		
		loadPatients('myCashAccountModal', 'txtPatientID', 'btnSelect' , postData);		
		
    });
    $("#myCashAccountModal #btnBackToList").click(function() {
        $("#myCashAccountModal #searchPatient").show();
        $("#myCashAccountModal #searchPatientdetail").hide();
    });
	
});

//function to calculate age
function getAge(dateString) {
    var now = new Date();
    var today = new Date(now.getYear(), now.getMonth(), now.getDate());

    var yearNow = now.getYear();
    var monthNow = now.getMonth();
    var dateNow = now.getDate();

    var dob = new Date(dateString.substring(0, 4), dateString.substring(5, 7) - 1, dateString.substring(8, 10));

    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();
    var age = {};
    var ageString = "";
    var yearString = "";
    var monthString = "";
    var dayString = "";


    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    age = {
        years: yearAge,
        months: monthAge,
        days: dateAge
    };

    if (age.years > 1) yearString = " years";
    else yearString = " year";
    if (age.months > 1) monthString = " months";
    else monthString = " month";
    if (age.days > 1) dayString = " days";
    else dayString = " day";


    if ((age.years > 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else ageString = "Oops! Could not calculate age!";

    return ageString;
}

/* bind state */
function bindState(countryID, stateID) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/view_patients.php",
        data: {
            "operation": "showstate",
            country_ID: countryID
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $('#myPatientUpdate #txtState').html(option);
                if (stateID != "") {
                    $("#myPatientUpdate #txtState").val(stateID);
                }
            }
        },
        error: function() {
		
			$('.modal-body').text("");
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
    });
}

/* bind city */
function bindCity(stateID, cityID) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/view_patients.php",
        data: {
            "operation": "showcity",
            state_ID: stateID
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $('#myPatientUpdate #txtCity').html(option);
                if (cityID != "") {
                    $("#myPatientUpdate #txtCity").val(cityID);
                }
            }

        },
        error: function() {}
    });

}

/* check for mobile number validation */
function validnumber(number) {
    intRegex = /^\d{10,15}$/;
    return intRegex.test(number);
}

/* clear function */
function clear1() {
    $("#myPatientUpdate #val_Salutation").val("");
    $("#myPatientUpdate #txtLastName").val("");
    $("#myPatientUpdate #txtFirstName").val("");
    $("#myPatientUpdate #txtFatherName").val("");
    $("#myPatientUpdate #txt_DOB").val("");
    $("#myPatientUpdate #txtGender").val("");
    $("#myPatientUpdate #txtAddress").val("");
    $("#myPatientUpdate #txtCountry").val("");
    $("#myPatientUpdate #txtState").val("");
    $("#myPatientUpdate #txtCity").val("");
    $("#myPatientUpdate #txtmob").val("");
    $("#myPatientUpdate #txtphone").val("");
    $("#myPatientUpdate #txtEmail").val("");
    $("#myPatientUpdate #val_CO").val("");
    $("#myPatientUpdate #val_COPhoneNo").val("");
    $("#myPatientUpdate #txtzipcode").val("");
    $("#myPatientUpdate #txt_year").val("");
    $("#myPatientUpdate #txt_month").val("");
    $("#myPatientUpdate #txt_day").val("");
    $("#myPatientUpdate #upload").val("");
    $("#myPatientUpdate #errorsalutation").text("");
    $("#myPatientUpdate #errorlname").text("");
    $("#myPatientUpdate #errorfname").text("");
    $("#myPatientUpdate #errorfathername").text("");
    $("#myPatientUpdate #errorgender").text("");
    $("#myPatientUpdate #errordob").text("");
    $("#myPatientUpdate #erroremail").text("");
    $("#myPatientUpdate #errormob").text("");
    $("#myPatientUpdate #erroraddress").text("");
    $("#myPatientUpdate #errorcountry").text("");
    $("#myPatientUpdate #errorstate").text("");
    $("#myPatientUpdate #errorcity").text("");
    $("#myPatientUpdate #errorcode").text("");

    $("#myPatientUpdate #val_Salutation").addClass("errorStyle");
    $("#myPatientUpdate #txtLastName").addClass("errorStyle");
    $("#myPatientUpdate #txtFirstName").addClass("errorStyle");
    $("#myPatientUpdate #txtFatherName").addClass("errorStyle");
    $("#myPatientUpdate #txt_DOB").addClass("errorStyle");
    $("#myPatientUpdate #txtGender").addClass("errorStyle");
    $("#myPatientUpdate #txtAddress").addClass("errorStyle");
    $("#myPatientUpdate #txtCountry").addClass("errorStyle");
    $("#myPatientUpdate #txtState").addClass("errorStyle");
    $("#myPatientUpdate #txtCity").addClass("errorStyle");
    $("#myPatientUpdate #txtmob").addClass("errorStyle");
    $("#myPatientUpdate #txtEmail").addClass("errorStyle");
    $("#myPatientUpdate #txtzipcode").addClass("errorStyle");
    $("#myPatientUpdate #txt_year").addClass("errorStyle");
    $("#myPatientUpdate #txt_month").addClass("errorStyle");
    $("#myPatientUpdate #txt_day").addClass("errorStyle");
    $("#myPatientUpdate #user_reg_image").show();
    $("#myPatientUpdate #imagetext").show();
    $("#myPatientUpdate #image-box").addClass("errorStyle");
    $("#myPatientUpdate #errorimage").hide();
    $("#myPatientUpdate #errorimage").text("");

}

function clearUpdatePatientStyle() {
    $("#myPatientUpdate #val_Salutation").removeClass("errorStyle");
    $("#myPatientUpdate #txtLastName").removeClass("errorStyle");
    $("#myPatientUpdate #txtFatherName").removeClass("errorStyle");
    $("#myPatientUpdate #txtFirstName").removeClass("errorStyle");
    $("#myPatientUpdate #txtGender").removeClass("errorStyle");
    $("#myPatientUpdate #txt_DOB").removeClass("errorStyle");
    $("#myPatientUpdate #txtmob").removeClass("errorStyle");
    $("#myPatientUpdate #txtCountry").removeClass("errorStyle");
    $("#myPatientUpdate #txtState").removeClass("errorStyle");
    $("#myPatientUpdate #txtCity").removeClass("errorStyle");
    $("#myPatientUpdate #txtzipcode").removeClass("errorStyle");

    $("#myPatientUpdate #errorcode").text("");
    $("#myPatientUpdate #errorcity").text("");
    $("#myPatientUpdate #errorcountry").text("");
    $("#myPatientUpdate #errorstate").text("");
    $("#myPatientUpdate #errormob").text("");
    $("#myPatientUpdate #errordob").text("");
    $("#myPatientUpdate #errorfathername").text("");
    $("#myPatientUpdate #errorgender").text("");
    $("#myPatientUpdate #errorlname").text("");
    $("#myPatientUpdate #errorfname").text("");
    $("#myPatientUpdate #errorsalutation").text("");

}

function loadPatients(modalId, txtPatientID, btnId, postData){
	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/view_patients.php",
		datatype: "json",
		data: postData,

		success: function(dataSet) {
			dataSet = JSON.parse(dataSet);
			//var dataSet = jQuery.parseJSON(data);
			otable.fnClearTable();
			otable.fnDestroy();
			otable = $('#'+ modalId + ' #viewPatientTable').DataTable({
				"sPaginationType": "full_numbers",
				"bAutoWidth" : false,
				"fnDrawCallback": function(oSettings) {
					
					$('#'+ modalId + ' #viewPatientTable tbody tr').on('dblclick', function() {
						if($('#'+ modalId + ' #hdnPatientHospitalization').val() != '1') {
							if ($(this).hasClass('selected')) {
								$(this).removeClass('selected');
							} else {
								otable.$('tr.selected').removeClass('selected');
								$(this).addClass('selected');
							}

							var mData = otable.fnGetData(this); // get datarow
							if(btnId != "NULL"){
								if (null != mData) // null if we clicked on title row
								{
                                    if(modalId != "myInvestigationModal"){
                                        //now aData[0] - 1st column(count_id), aData[1] -2nd, etc.                              
                                        var patientId = parseInt(mData["id"]);
                                        var prefix = mData["prefix"];
                                        
                                        var patientIdLength = patientId.toString().length;
                                        for (var i=0;i<6-patientIdLength;i++) {
                                            patientId = "0"+patientId;
                                        }
                                        patientid = prefix+patientId;
                                        patientHospitalizationId = patientid;
                                        $('#' + txtPatientID).val(patientid);
                                    }
                                    else{  

                                        var ipdId = parseInt(mData["ipd_id"]);
                                        var prefix = mData["prefix"];
                                        
                                        var patientIdLength = ipdId.toString().length;
                                        for (var i=0;i<6-patientIdLength;i++) {
                                            ipdId = "0"+ipdId;
                                        }
                                        ipdId = prefix+ipdId;
                                        $('#' + txtPatientID).val(ipdId);
                                    }									
								}
                                //this hidden field for patient ot reg so to not to close all pop up
                                if ($("#patientOTReg").val() == 1) {
                                    $(".patientRegClose").click();
                                }
                                else{
                                    $("#" + btnId).click();
                                    $(".close").click();
                                }    								
							}
							else{
								if (null != mData) // null if we clicked on title row
								{
									//now aData[0] - 1st column(count_id), aData[1] -2nd, etc. 								
									var accountNo = mData["fan_id"];
									if(accountNo != null){
										var accountPrefix = mData["account_prefix"];
									
										var accountNoLength = accountNo.length;
										for (var i=0;i<6-accountNoLength;i++) {
											accountNo = "0"+accountNo;
										}
										accountNo = accountPrefix+accountNo;
										$('#' + txtPatientID).val(accountNo);
										$('#'+ modalId).modal('hide');
									}
									else{
										var accountNo = mData["id"]
										if(accountNo != null){
											var accountPrefix = mData["account_prefix"];
										
											var accountNoLength = accountNo.length;
											for (var i=0;i<6-accountNoLength;i++) {
												accountNo = "0"+accountNo;
											}
											accountNo = accountPrefix+accountNo;
											$('#' + txtPatientID).val(accountNo);
											$('#'+ modalId).modal('hide');
										}
										
									}								
								}
							}			
						}
						else{
                            if ($(this).hasClass('selected')) {
                                $(this).removeClass('selected');
                            } else {
                                otable.$('tr.selected').removeClass('selected');
                                $(this).addClass('selected');
                            }

                            var mData = otable.fnGetData(this); // get datarow
                            if(mData["operating_consultant_id"] != ""){
                                attendingConsultantId = mData["attending_consultant_id"];
                                operatingConsultantId = mData["operating_consultant_id"];
                            }
							
							$(this).find('.patientDetails').click();
						}
					});

					$('#'+ modalId + '  .patientDetails').on('click', function() {
					var id = $(this).attr('data-id');
					var postData = {
							"operation": "searchPatient",
							"id": id
						}
						//$("#mySearchModal").hide();
					$.ajax({
						type: "POST",
						cache: false,
						url: "controllers/admin/view_patients.php",
						datatype: "json",
						data: postData,

						success: function(data) {
							if (data != "0" && data != "") {
								var parseData = jQuery.parseJSON(data);

								for (var i = 0; i < parseData.length; i++) {
								   
									$('#'+ modalId + '  #lblPatientName').text(parseData[i].patient_name);
									if(parseData[i].gender == "M")
									{
										$('#'+ modalId + '  #lblGender').text("Male");
									}else{
										$('#'+ modalId + ' #lblGender').text("Female");
									}
								   
								
									$('#'+ modalId + ' #lblDOB').text(parseData[i].dob);
									$('#'+ modalId + ' #lblCountry').text(parseData[i].country);
									$('#'+ modalId + ' #lblState').text(parseData[i].state);
									$('#'+ modalId + ' #lblCity').text(parseData[i].city);
									$('#'+ modalId + ' #lblZipCode').text(parseData[i].zip);

                                    if(parseData[i].address == "") {
									   $('#'+ modalId + ' #lblAddress').text("N/A");
                                    }
                                    else{
                                        $('#'+ modalId + ' #lblAddress').text(parseData[i].address);
                                    }

                                    var mobile = parseData[i].country_code + parseData[i].mobile;
									$('#'+ modalId + ' #lblMobile').text(mobile);
									
									if(parseData[i].email == "") {
										$('#'+ modalId + ' #lblEmail').text("N/A");
									}else {
										$('#'+ modalId + ' #lblEmail').text(parseData[i].email);
									}
									
									if(parseData[i].phone == "") {
										$('#'+ modalId + ' #lblPhone').text("N/A");
									}else {
										$('#'+ modalId + ' #lblPhone').text(parseData[i].phone);
									}
									
									if(parseData[i].care_of == "") {
										$('#'+ modalId + ' #lblCo').text("N/A");
									}else {
										$('#'+ modalId + ' #lblCo').text(parseData[i].care_of);
									}
									
									if(parseData[i].care_contact == "") {
										$('#'+ modalId + ' #lblCoPhone').text("N/A");
									}else {
										$('#'+ modalId + ' #lblCoPhone').text(parseData[i].care_contact);
									}
									
									var getDOB = parseData[i].dob;
									var patientId = parseInt(parseData[i].id);
									var prefix = parseData[i].prefix;
									
									var patientIdLength = patientId.toString().length;
									for (var i=0;i<6-patientIdLength;i++) {
										patientId = "0"+patientId;
									}
									patientid = prefix+patientId;
									patientHospitalizationId = patientid;
									 $('#'+ modalId + ' #lblPatientId').text(patientid);

								}

								var new_age = getAge(getDOB);

								var split = new_age.split(' ');
								var age_years = split[0];
								var age_month = split[1];
								var age_day = split[2];

								$('#'+ modalId + ' #lblAge').html(age_years + " Yrs" + "  " + age_month + " Mon" + "  " + age_day + " Days");
								$('#'+ modalId + ' #searchPatient').hide();
								$('#'+ modalId + ' #searchPatientdetail').show();

							}
						},
						error: function() {
							alert('error');
						}
					});
				});
				},
				"aaData": dataSet,
				"aoColumns": [{
				"mData":  function (o) { 	
					if(o["ipd_id"] != null){
                            var patientId = o["ipd_id"];
                            var patientPrefix = o["prefix"];
                                
                            var patientIdLength = patientId.length;
                            for (var i=0;i<6-patientIdLength;i++) {
                                patientId = "0"+patientId;
                            }
                            patientid = patientPrefix+patientId;
                            return patientid;
                        }
                        else{
                            var patientId = o["id"];
                            var patientPrefix = o["prefix"];
                                
                            var patientIdLength = patientId.length;
                            for (var i=0;i<6-patientIdLength;i++) {
                                patientId = "0"+patientId;
                            }
                            patientid = patientPrefix+patientId;
                            return patientid;
                        }
                    }
				}, {
					"mData": "name"
				}, {
					"mData": "dob"
				}, {
					"mData": function(mobile) {
                        var data = mobile;
                        var mobileCode = data["country_code"];
                        var mobile = data["mobile"];
                        var mobile = mobileCode + mobile;
                        return mobile;
                    }
				}, {
					"mData": "email"
				}, {
					"mData": function(o) {
						var data = o;
						var id = data["id"];
						return "<i class='fa fa-eye patientDetails' id='btnView' data-id=" + id + " style='font-size: 17px; margin-left: 20px; cursor:pointer;' title='view' value='view'" +
							"></i>";
					}
				}, ],
				aoColumnDefs: [{
					'bSortable': false,
					'aTargets': [5]
				}]
			});

		},
		error: function() {
			
			$('.modal-body').text("");
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
	});
}

function getPatients(postData) {
	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/view_patients.php",
		datatype: "json",
		data: postData,

		success: function(data) {
			dataSet = JSON.parse(data); // convert data into json
			//var dataSet = jQuery.parseJSON(data);
			otable.fnClearTable(); // clear datatable
			otable.fnDestroy(); // remove datatable
			
			otable = $('#viewPatientTable').DataTable({
				"sPaginationType": "full_numbers",
				"bAutoWidth" : false,
				"fnDrawCallback": function(oSettings) {
					$(".editPatient").on('click', function() { // on edit click event to load data for update 
						clearUpdatePatientStyle();
						
						//focus on first element
						$("#myPatientUpdate #val_Salutation").focus();
						$('#patientUpdateLabel').text("Update Patient Details");
						$('.modal-body').text();
						$('#myPatientUpdate').modal();
						var id = $(this).attr('data-id');
						userId = id;
						var postData = {
							"operation": "patientShow",
							"id": id
						}
						$.ajax({
							type: "POST",
							cache: false,
							url: "controllers/admin/view_patients.php",
							datatype: "json",
							data: postData,

							success: function(data) {
								if (data != "0" && data != "") {

									var parseData = jQuery.parseJSON(data);

									for (var i = 0; i < parseData.length; i++) {
										$("#myPatientUpdate #val_Salutation").val(parseData[i].salutation);
										$("#myPatientUpdate #user_reg_image").hide();
										$("#myPatientUpdate #imagetext").hide();
										if (parseData[i].images_path != "null") {
											var imagePath = "./upload_images/patient_dp/" + parseData[i].images_path;
											$("#myPatientUpdate #image-box").css({
												"background-image": "url(" + imagePath + ")"
											});
										}
										if (parseData[i].images_path == "") {

											$("#myPatientUpdate #image-box").css({
												"background-image": "url(./images/default.jpg)"
											}); // for default image
											$("#myPatientUpdate #imagetext").show();
											$("#myPatientUpdate #imagetext").css({
												"text-align": "center",
												"position": "relative",
												"top": "61%",
												"display": "block"
											});
										}
										$("#myPatientUpdate #txtLastName").val(parseData[i].last_name);
										$("#myPatientUpdate #txtFirstName").val(parseData[i].first_name);
										$("#myPatientUpdate #txtMiddleName").val(parseData[i].middle_name);
										$("#myPatientUpdate #txtGender").val(parseData[i].gender);
										$("#myPatientUpdate #txtEmail").val(parseData[i].email);
										$("#myPatientUpdate #txt_DOB").val(parseData[i].dob);
										$("#myPatientUpdate #txtCountry").val(parseData[i].country_id);

										$("#myPatientUpdate #txtzipcode").val(parseData[i].zip);
										$("#myPatientUpdate #txtAddress").val(parseData[i].address);
                                        $("#myPatientUpdate #txtmob").val(parseData[i].mobile);
										$("#myPatientUpdate #mobileCode").val(parseData[i].country_code);
										$("#myPatientUpdate #txtphone").val(parseData[i].phone);
										$("#myPatientUpdate #val_CO").val(parseData[i].care_of);
										$("#myPatientUpdate #val_COPhoneNo").val(parseData[i].care_contact);
										
										bindState(parseData[i].country_id, parseData[i].state_id);
										bindCity(parseData[i].state_id, parseData[i].city_id);
										var patientPrefix = parseData[i].patient_prefix;
										var fanId = parseData[i].fan_id;
										var Id = parseData[i].id;
										if(fanId != "" && fanId != null){
											var fanIdLength = fanId.length;
											for (var j=0;j<6-fanIdLength;j++) {
												fanId = "0"+fanId;
											}
											fanId = patientPrefix+fanId;
											$("#txtFamilyHead").val(fanId);
										}
										else{
											$("#txtFamilyHead").val("");
										}
										var getDOB = parseData[i].dob;
										imagefile = imagePath;
									}

									var new_age = getAge(getDOB);

									var split = new_age.split(' ');
									var age_years = split[0];
									var age_month = split[1];
									var age_day = split[2];
									$("#myPatientUpdate #txt_year").val(age_years);
									$("#myPatientUpdate #txt_month").val(age_month);
									$("#myPatientUpdate #txt_day").val(age_day);

								}
							},
							error: function() {
								
								$('.modal-body').text("");
								$('#messageMyModalLabel').text("Error");
								$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
								$('#messagemyModal').modal();
							}
						});

					});
					$(".patientDetails").on('click', function() { // on view click event to load data for view details

						var id = $(this).attr('data-id');
						var postData = {
							"operation": "patientDetail",
							"id": id
						}
						$.ajax({
							type: "POST",
							cache: false,
							url: "controllers/admin/view_patients.php",
							datatype: "json",
							data: postData,

							success: function(data) {
								$('#patientModal').modal("hide");
								if (data != "0" && data != "") {
									$('#patientModalLabel').text("Patient Details");
									$('.modal-body').text();
									var parseData = jQuery.parseJSON(data);

									for (var i = 0; i < parseData.length; i++) {
									   
										$("#myPatientModal #user_reg_image").hide();
										$("#myPatientModal #imagetext").hide();
										var imagePath;
										if (parseData[i].images_path != "null") {
											imagePath = "./upload_images/patient_dp/" + parseData[i].images_path;
											$("#myPatientModal #image-box").css({
												"background-image": "url(" + imagePath + ")"
											});
										}
										if (parseData[i].images_path == "") {

											$("#myPatientModal #image-box").css({
												"background-image": "url(./images/default.jpg)"
											}); // for default image
										}
										$("#myPatientModal #lblPatientName").text(parseData[i].patient_name);
										if(parseData[i].gender == "M")
										{
											$("#myPatientModal #lblGender").text("Male");
										}else{
											$("#myPatientModal #lblGender").text("Female");
										}
										
                                        if (parseData[i].email !='') {
                                            $("#myPatientModal #lblEmail").text(parseData[i].email);
                                        }
                                        else{
                                            $("#myPatientModal #lblEmail").text("N/A");

                                        }
										$("#myPatientModal #lblDOB").text(parseData[i].dob);
										$("#myPatientModal #lblCountry").text(parseData[i].country);
										$("#myPatientModal #lblState").text(parseData[i].state);
										$("#myPatientModal #lblCity").text(parseData[i].city);
										$("#myPatientModal #lblZipCode").text(parseData[i].zip);

                                        if (parseData[i].address !='') {
                                            $("#myPatientModal #lblAddress").text(parseData[i].address);
                                        }
                                        else {
                                            $("#myPatientModal #lblAddress").text("N/A");
                                        }
                                        if (parseData[i].phone !='') {
                                            $("#myPatientModal #lblPhone").text(parseData[i].phone);
                                        }
                                        else {
                                            $("#myPatientModal #lblPhone").text("N/A");
                                        }
										
                                        if (parseData[i].care_of !='') {
                                            $("#myPatientModal #lblCo").text(parseData[i].care_of);
                                        }
                                        else {
                                            $("#myPatientModal #lblCo").text("N/A");
                                        }
                                        if (parseData[i].care_contact !='') {
                                            $("#myPatientModal #lblCoPhone").text(parseData[i].care_contact);
                                        }
                                        else {
                                            $("#myPatientModal #lblCoPhone").text("N/A");
                                        }
                                        var mobile = parseData[i].country_code + parseData[i].mobile;
										$("#myPatientModal #lblMobile").text(mobile);
										var getDOB = parseData[i].dob;
										var patientId = parseInt(parseData[i].id);

                                        if(parseData[i].fan_id !=null){
                                          var accountId = parseInt(parseData[i].fan_id);  
                                        }
                                        
										var patient_prefix = parseData[i].patient_prefix;

                                        var account_prefix = parseData[i].account_prefix;
										
										var patientIdLength = patientId.toString().length;
										for (var i=0;i<6-patientIdLength;i++) {
                                            if (accountId != undefined) {
                                                accountId = "0"+accountId;
                                            }                                            
											patientId = "0"+patientId;
										}
										patientid = patient_prefix+patientId;
										patientHospitalizationId = patientid;
                                        if (accountId != undefined) {
                                            accountId = account_prefix+accountId;
                                        }


										$("#myPatientModal #lblPatientId").text(patientid);

                                        if (accountId != undefined) {
                                            $("#myPatientModal #lblFan").text(accountId);
                                        }
                                        else{
                                            $("#myPatientModal #lblFan").text("N/A");
                                        }

									}

									var new_age = getAge(getDOB);

									var split = new_age.split(' ');
									var age_years = split[0];
									var age_month = split[1];
									var age_day = split[2];

									$("#myPatientModal #lblAge").html(age_years + " Yrs" + "   " + age_month + " Mon" + "   " + age_day + " Days");

									$('#myPatientModal').modal();

								}
							},
							error: function() {
							
								$('.modal-body').text("");
								$('#messageMyModalLabel').text("Error");
								$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
								$('#messagemyModal').modal();
							}
						});

					});
				},
				"aaData": dataSet,
				"aoColumns": [{
					"mData": function (o) { 	
						var patientId = o["id"];
						var patientPrefix = o["prefix"];
							
						var patientIdLength = patientId.length;
						for (var i=0;i<6-patientIdLength;i++) {
							patientId = "0"+patientId;
						}
						patientid = patientPrefix+patientId;
						return patientid;	}
				}, {
					"mData": "name"
				}, {
					"mData": "dob"
				}, {
                    "mData": function(mobile) {
                        var data = mobile;
                        var mobileCode = data["country_code"];
                        var mobile = data["mobile"];
                        var mobile = mobileCode + mobile;
                        return mobile;
                    }
                }, {
					"mData": "email"
				}, {
					"mData": function(o) {
						var data = o;
						var id = data["id"];
						return "<i class='fa fa-eye patientDetails' id='btnView' data-id=" + id + " style='font-size: 16px;     margin-left: 20px; cursor:pointer;' title='view' value='view'" +
							" style='font-size: 16px; cursor:pointer;'></i>" +
							"<i class='ui-tooltip fa fa-pencil editPatient' data-id=" + id + " title='Edit'" +
							"  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>";
					}
				}, ],
				aoColumnDefs: [{
					'bSortable': false,
					'aTargets': [5]
				}]
			});

		},
		error: function() {
		
			$('.modal-body').text("");
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
	});
}
function bindCountryCode(countryID) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/hospital_profile.php",
        data: {
            "operation": "showCountryCode",
            countryID: countryID
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                
                var countryCode = '+' + parseData.country_code;
                $("#myPatientUpdate #mobileCode").val(countryCode);
            }
        },
        error: function() {}
    });
}
function reset(modalId){

}
//# sourceURL = view_patients.js