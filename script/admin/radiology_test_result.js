$(document).ready(function() {
	loader();
	$('.modal-imagebox').hide();
	if ($('#thing').val() == 4) {
		var getVisitValue = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
		var redPatientId = parseInt($("#txtPatientID").val().replace ( /[^\d.]/g, '' ));
	}
	if ($('#thing').val() == 5) {
		var getVisitValue = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
		var redPatientId = parseInt($("#txtPatientIdHistory").val().replace ( /[^\d.]/g, '' ));
	}
	if ($('#thing').val() == 3) {
		var getVisitValue = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
		var redPatientId = parseInt($("#txtPatientID").val().replace ( /[^\d.]/g, '' ));
	}
	$("#PrintRadiologyTest").addClass("hide");
	var otable = $('#radiologyResultsTbl').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"sAjaxSource":"controllers/admin/radiology_test_result.php",
		"fnDrawCallback": function ( oSettings ) {
			$('.Print').click(function(){
				var data=$(this).parents('tr')[0];
				var mData = $('#radiologyResultsTbl').dataTable().fnGetData(data);
				if (null != mData)  // null if we clicked on title row
				{
					$('#lblPrintVisitId').text($("#textvisitId").val());								
					$('#lblPrintPatientId').text($("#txtPatientID").val());
					$('#lblPrintPatient').text(mData["patient_name"]);
					$('#printTestname').text(mData["name"]);
					$("#printRemark").text(mData["report"]);	 
					var visitDate = convertDate(mData["visit_date"]);
					$('#lblPrintVisitDate').text(visitDate);

					/*hospital information through global varibale in main.js*/
					$("#lblPrintEmail").text(hospitalEmail);
					$("#lblPrintPhone").text(hospitalPhoneNo);
					$("#lblPrintAddress").text(hospitalAddress);
					if (hospitalLogo != "null") {
						imagePath = "./images/" + hospitalLogo;
						$("#printLogo").attr("src",imagePath);
					}
					$("#printHospitalName").text(hospitalName);

					//$.print("#PrintRadiologyTest");
					window.print();
				}
			});
		
			$('.viewImage').click(function(){
				
				$('.modal-imagebox').hide();
				var imageName =$(this).attr('data-images');
				if(imageName == "" || imageName == "null" ){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("There are no image here..");
					$('#messagemyModal').modal();
				}else{
					$(".modalbody").text("");
					var imageList = imageName.split(';');
					$('#imageMyModalLabel').text("Images");
					$('#myimageModal').click();
					for (i = 0;i< imageList.length-1;i++){
						$(".modalbody").append("<img class='img-thumbnail pickImage' data-action='zoom' style='width:auto;height:25%;margin:1%;"+
													"cursor:pointer' src=./upload_images/radio_test_dp/"+imageList[i]+">");						
					}
					
					$(".pickImage").on('click', function(){	
						/* $('.img-header').hide();
						$('.imageContent').css({"background":"transparent"});	
						$('.modalbody').hide(); */
						$("#imageModal").hide();
						$('.modal-imagebox').show();
						var imagesrc = $(this).attr('src');
						$(".modal-imagebox").html("<input type = 'button' value='X' id = 'imgClose' class='imageClose' onclick=imageClose();"+
													"><img class='img-thumbnail imgBlock'"+
													"style='cursor:pointer' src="+imagesrc+">");
						if($(".imgBlock").height() < 600)
						{
							$('.modal-imagebox').css({"overflow-y": "hidden","height": "auto","left":"30%"});
						}else{
							$('.modal-imagebox').css({"overflow-y": "scroll", "height": "600px","left":"10%"});
						}
					});
				}
			});
		}, 
		"fnServerParams": function ( aoData ) {
		  aoData.push( {"name": "operation", "value": "bindTableData" },
		  {"name": "visitId", "value": getVisitValue },
		  {"name": "paitentId", "value": redPatientId });
		},
		"aoColumns": [
				{  "mData": "name" },
				{  "mData": function(o) {
                    var data = o;
					var imagesPath;
					if(data.images_path == ""){
						imagesPath="null";
					}else{
						imagesPath=data.images_path; 
					}
					
                    return "<span title='View Image'><a href='#imageModal' class = 'viewImage' data-images="+imagesPath+" style='text-decoration: none;color: #000;'>Click here for view Image</a></span>";
                   } 
				},
				{  "mData": "report" },
				{  "mData": function(o) {
                    var data = o;
					var id=data.id;
                    return "<i class='fa fa-print Print' title='Print'" +
                        " data-id="+id+" data-original-title='Print'/>";
                   }
				}
				],
				aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [2]
            },{
                'bSortable': false,
                'aTargets': [1]
            },{
                'bSortable': false,
                'aTargets': [3]
            }]
	});
});
function imageClose(){
	/* $('.imageContent').css({"background":"#fff"});
	$('.img-header').show();
	$('.modalbody').show(); */
	$("#imageModal").show();
	$('.modal-imagebox').hide();
}
//function to calculate date
function convertDate(o){
        var dbDateTimestamp = o;
        var dateObj = new Date(dbDateTimestamp * 1000);
        var yyyy = dateObj.getFullYear().toString();
        var mm = (dateObj.getMonth()+1).toString(); // getMonth() is zero-based
        var dd  = dateObj.getDate().toString();
        return yyyy +"-"+ (mm[1]?mm:"0"+mm[0]) +"-"+ (dd[1]?dd:"0"+dd[0]);
}