var roomTypeTable;//define global variable for datatable
$(document).ready(function(){
	debugger;
	loader();	
/* ****************************************************************************************************
 * File Name    :   room_type.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Arora
 * Created Date :   30th dec, 2015
 * Description  :   This page add and mange room types
 *************************************************************************************************** */	
	
	/*By default hide this add screen part*/
	$("#advanced-wizard").hide();
    $("#wardList").addClass('list');    
    $("#tabRoomTypeList").addClass('tab-list-add');
			
	//Click for go to add the room type screen part
    $("#tabAddRoomType").click(function() {
        clear();
        showAddTab();
    });
	
		
	/*Click function for show the room type lists*/
    $("#tabRoomTypeList").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();		
        tabRoomTypeList();// Call the function for show the room type lists
    });
	
	/*By default when radio button is not checked show all active data*/
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		loadDataTable();
    }
	// Ajax call for loading the data table for inactive room type
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	roomTypeTable.fnClearTable();
	    	roomTypeTable.fnDestroy();
	    	//Datatable code
			roomTypeTable = $('#tblVendorType').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					/*On click of restore icon call this function*/
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  roomTypeTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/vendor_type.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					/* {  "mData": "id" }, */
					{ "mData": "type" },
					{ "mData": "description" },
					{
						"mData": function (o) { 
						var data = o;
						var id = data["id"];
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},
				],
				aoColumnDefs: [
					{ 'bSortable': false, 'aTargets': [ 1 ] },
					{ 'bSortable': false, 'aTargets': [ 2 ] }
				]
			} );
		}
		else{
			roomTypeTable.fnClearTable();
	    	roomTypeTable.fnDestroy();
	    	//Datatable code
			loadDataTable();
		}
    });
	
	//Click function for save the room type 
	$("#btnSubmit").click(function(){
		/*perform validation*/
		var flag="false";
		$("#txtVendorType").val($("#txtVendorType").val().trim());
		
		if($("#txtVendorType").val()==""){
			$('#txtVendorType').focus();
			$("#txtVendorTypeError").text("Please enter vendor type");
			$("#txtVendorType").addClass("errorStyle");    
			flag="true";
		}
		
		if(flag=="true"){
		return false;
		}
		
		var roomType = $("#txtVendorType").val();
		var description = $("#txtDescription").val();
		description = description.replace(/'/g, "&#39");
		var postData = {
			"operation":"save",
			"roomType":roomType,
			"description":description
		}
		
		// Ajax call for save room type
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/vendor_type.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Vendor type saved successfully!!!");
					$('#messagemyModal').modal();
					$('#inactive-checkbox-tick').prop('checked', false).change();
					tabRoomTypeList();
				}
				else{
					$("#txtVendorTypeError").text("Vendor Type already exists");
					$("#txtVendorType").addClass("errorStyle");
					$('#txtVendorType').focus();
				}				
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	});
	
	//keyup functionality
	$("#txtVendorType").keyup(function(){
		if($("#txtVendorType").val()!=""){
			$("#txtVendorTypeError").text("");
			$("#txtVendorType").removeClass("errorStyle");	    	 
		}
	});		
});

/*define edit Click function for update*/
function editClick(id,vendor_type,description){	
	showAddTab();
	$("#btnReset").hide();
    $("#btnSubmit").hide();
    $('#btnUpdateRoomType').show();
    $('#tabAddRoomType').html("+Update Vendor Type");
    $("#txtVendorType").focus();
   
	$("#btnUpdateRoomType").removeAttr("style");
	$('#txtVendorType').val(vendor_type);
	$('#txtDescription').val(description.replace(/&#39/g, "'"));
	
	$('#selectedRow').val(id);
	
	//Click function for update room type 
	$("#btnUpdateRoomType").click(function(){
		var flag = "false";
		$("#txtVendorType").val($("#txtVendorType").val().trim());
		
		if ($("#txtVendorType").val() == '') {
			$("#txtVendorType").focus();
			$("#txtVendorTypeError").text("Please enter vendor type");
			$("#txtVendorType").addClass("errorStyle");
			flag = "true";
		}
		
		if ($("#txtVendorTypeError").text() != '') {
			flag = "true";
		}
		
		if(flag == "true"){			
			return "false";
		}
		else{
			var roomType = $("#txtVendorType").val();
			var description = $("#txtDescription").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();			
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				// Ajax call for update room type
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/vendor_type.php",
					data :{            
						"operation" : "update",
						"id" : id,
						"roomType":roomType,
						"description":description
					},
					success: function(data) {	
						if(data != "0" && data != ""){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Vendor type updated successfully!!!");
							roomTypeTable.fnReloadAjax();
							$('#messagemyModal').modal();
							tabRoomTypeList();
							clear();
						}
						if(data == "0"){
							$("#txtVendorTypeError").text("Vendor type already exists");
							$("#txtVendorType").addClass("errorStyle");
							$('#txtVendorType').focus();
							
						}
					},
					error:function() {
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messagemyModal').modal();
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					}
				});
			});
		}
	});
}
/*Define function for delete the room type*/ 
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete vendor type");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	var type="delete";
	$('#confirm').attr('onclick','deleteRoomType("'+type+'");');//pass attribute with sent opertion and function with it's type
	
}

/*Define function for restore the room type*/ 
function restoreClick(id){
	$('#selectedRow').val(id);	
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Restore vendor type");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();
	var type="restore";
	$('#confirm').attr('onclick','deleteRoomType("'+type+'");');
	
}

//function for show the list of room type
function tabRoomTypeList(){
	$("#advanced-wizard").hide();
	$(".blackborder").show();
	$('#inactive-checkbox-tick').prop('checked', false).change();
	
   
    $("#tabAddRoomType").removeClass('tab-detail-add');
    $("#tabAddRoomType").addClass('tab-detail-remove');
    $("#tabRoomTypeList").removeClass('tab-list-remove');    
    $("#tabRoomTypeList").addClass('tab-list-add');
    $("#wardList").addClass('list');
    $("#btnReset").show();
    $("#btnSubmit").show();
    $('#btnUpdateRoomType').hide();
    $('#tabAddRoomType').html("+Add Vendor Type");
	clear();
}

function showAddTab(){
    $("#advanced-wizard").show();
    $(".blackborder").hide();

    $("#tabAddRoomType").addClass('tab-detail-add');
    $("#tabAddRoomType").removeClass('tab-detail-remove');
    $("#tabRoomTypeList").removeClass('tab-list-add');
    $("#tabRoomTypeList").addClass('tab-list-remove');
    $("#wardList").addClass('list');
    $('#txtVendorType').focus();
}
function deleteRoomType(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
			
		//Ajax call for delete the room type 	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/vendor_type.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Vendor type deleted successfully!!!");
					roomTypeTable.fnReloadAjax();
					$('#messagemyModal').modal();
					 
				}
				else{
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("This vendor type is used , so you can not delete!!!");
					$('#messagemyModal').modal();
				}
			},
			error: function()
			{
				alert('error');
				  
			}
		});
	}
	else{

	var id = $('#selectedRow').val();
	
		//Ajax call for restore the room type 
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/vendor_type.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Vendor type restored successfully!!!");
					$('#messagemyModal').modal();
					roomTypeTable.fnReloadAjax();
				}			
			},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
		});
	}
}
/*key press event on ESC button*/
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		 window.location.href = "http://localhost/herp/";
    }
});
/*reset button functionality*/
$("#btnReset").click(function(){
	$("#txtVendorType").removeClass("errorStyle");
	clear();
	$('#txtVendorType').focus();
});

//function for clear the data
function clear(){
	$('#txtVendorType').val("");
	$('#txtVendorTypeError').text("");
	$('#txtDescription').val("");
}

function loadDataTable(){
roomTypeTable = $('#tblVendorType').dataTable( {
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {
			/*On click of update icon call this function*/
			$('.update').unbind();
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = roomTypeTable.fnGetData(data);//get datatable data
				if (null != mData)  // null if we clicked on title row
				{
					/*get the value of that row from database column*/
					var id = mData["id"];
					var type = mData["type"];
					var description = mData["description"];
					editClick(id,type,description);//call edit click function to update data       
				}
			});
			/*On click of delete icon call this function*/
			$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  roomTypeTable.fnGetData(data);
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);
				}
			});
		},
		
		"sAjaxSource":"controllers/admin/vendor_type.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{ "mData": "type" },
			{ "mData": "description" },
			{
				"mData": function (o) { 
					var data = o;
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
				   " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
				   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
				   " style='font-size: 22px; color:#a94442; cursor:pointer;' "+
				   " data-original-title='Delete'></i>"; 
				}
			},	
			],
			/*Disable sort for following columns*/
			aoColumnDefs: [
				{ 'bSortable': false, 'aTargets': [ 1 ] },
				{ 'bSortable': false, 'aTargets': [ 2 ] }
			]
	} );
}
//# sourceURL = room_type.js