$(document).ready(function() {
	debugger;
	var changedAgeParseData = '';
	$(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});	
	$("#toDate").attr("disabled",true);
	$("#fromDate").attr("disabled",true);
	google.charts.setOnLoadCallback(drawChart);

	//give top to chart
	
	$("#piechart_3d").find('div:eq(1)').css("top","200px");

	$('.changeRadio').change(function() {
		if ($('#radToFromDate').prop( "checked") == true) {
	        $("#toDate").attr("disabled",false);
			$("#fromDate").attr("disabled",false);
	    }
	    else{
	    	$("#toDate").attr("disabled",true);
			$("#fromDate").attr("disabled",true);
			$("#toDate").val("");
			$("#fromDate").val("");
	    }
	});	

	$("#fromDate").change(function() {
		if ($("#fromDate").val() !='') {
			$("#fromDate").removeClass("errorStyle");
			$("#txtFromDateError").text("");
		}
	});
	$("#toDate").change(function() {
		if ($("#toDate").val() !='') {
			$("#toDate").removeClass("errorStyle");
			$("#txtToDateError").text("");
		}
	});	

	$("#clear").click(function(){
		clear();
	});

});
function drawChart() {
	var chartWidth  = $("#page-content").width() -25;
	var dataLoad;
	var bigArrayDepartment = [['Task', 'Hours per Day']];
	var bigArrayGender = [];
	var age = [['Age-Group', 'Count']];

	$("#viewReport").on("click",function(){
		var fromDate ;
		var toDate ;
		if ($('#radToday').prop( "checked") == true) {
			dataLoad = "today";
		}
		else if($('#radLastSevenDays').prop( "checked") == true) {
			dataLoad = "last7days";
		}
		else if($('#radToFromDate').prop( "checked") == true) {
			dataLoad = "betweenDate";
			fromDate = $("#fromDate").val();
			toDate = $("#toDate").val();
			if (fromDate =='') {
				$("#txtFromDateError").text("Please choose from date");
				$("#fromDate").addClass("errorStyle");
				$("#fromDate").focus();
				return false;
			}
			if (toDate =='') {
				$("#txtToDateError").text("Please choose from date");
				$("#toDate").addClass("errorStyle");
				$("#toDate").focus();
				return false;
			}
		}
		else if($('#radLastThirtyDays').prop( "checked") == true) {
			dataLoad = "last30days";
		}
		else if($('#radAllTime').prop( "checked") == true) {
			dataLoad = "all";
		}

		if (fromDate == undefined) {
			fromDate ="";
		}
		if (toDate == undefined) {
			toDate = "";
		}

		if (dataLoad==undefined) {
			return false;
		}
		var postData = {
			"operation" : "showChartData",
			"dataLoad" : dataLoad,
			"fromDate" : fromDate,
			"toDate" : toDate
		}
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/department_wise_report.php",
			data: postData,
			success: function(data) { 
				if(data != null && data != ""){
					var parseData =  JSON.parse(data);
					if (parseData[0] =='' && parseData[1] =='' && parseData[2][0]['0-10 Yrs'] ==0 && parseData[2][0]['11-20 Yrs'] ==0
						&& parseData[2][0]['21-30 Yrs'] ==0 && parseData[2][0]['31-40 Yrs'] ==0 && parseData[2][0]['41-50 Yrs'] ==0
						&& parseData[2][0]['51-60 Yrs'] ==0 && parseData[2][0]['61-70 Yrs'] ==0 && parseData[2][0]['71-80 Yrs'] ==0) {

						$('#messagemyModal').modal();
						$('#messageMyModalLabel').text("Alert");
						$('.modal-body').text("No data exist!!!");
	            		$(".hideShowDept,#btnSaveExcel,#btnSavePdf,#chart_div2,#chart_div,#piechart_3d").hide();
						return false;					
					}
					$("#chart_div2").show();
            		$("#chart_div").show();
            		$("#piechart_3d").show();
					bigArrayDepartment = [['Task', 'Hours per Day']];
					bigArrayGender = [];
					bigArrayAge = [];
					age = [['Age-Group', 'Count']];
					$.each(parseData,function(index,v){
						for(var i=0;i<parseData[index].length;i++){
							if(index==0){
								smallArrayDepartment = [];
								smallArrayDepartment.push(parseData[index][i].Department);
								smallArrayDepartment.push(parseInt(parseData[index][i].Visits));
								bigArrayDepartment.push(smallArrayDepartment);
							} 
							else if (index == 1) {
								smallArrayGender = [];
								smallArrayGender.push(parseData[index][i].Department);
								smallArrayGender.push(parseInt(parseData[index][i].Male));
								smallArrayGender.push(parseInt(parseData[index][i].Female));
								bigArrayGender.push(smallArrayGender);
							}	

							else if (index == 2) {
								$.each(parseData[index][i],function(im,nn){
									age.push([im,parseInt(nn)]);	
								});																		
							}						
						}
					});

					/*google chart intialization for department visit*/					
					var dataDepartment = google.visualization.arrayToDataTable(bigArrayDepartment);

					/*google chart intialization  gender visit*/
					var dataGender = new google.visualization.DataTable();
		            dataGender.addColumn('string', 'Gender');
		            dataGender.addColumn('number', 'Male');
		            dataGender.addColumn('number', 'Female');
		            dataGender.addRows(bigArrayGender);

		            /*google chart intialization for age group visit*/
		            var dataAgeGroup = google.visualization.arrayToDataTable(age);

				    var options = {
				      title: 'Visitors Analysis','width':1200,'height':600,is3D:true 
				    };

				    var options2 = {
				    	'title':'Visitors Gender Analysis','width':1200,'height':1000,is3D:true,isStacked: true,bars: 'vertical' // Required for Material Bar Charts.
					};

					var options3 = {
				    	'title':'Visitors Age Analysis','width':1200,'height':1000,is3D:true
					};

				    var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
				    chart.draw(dataDepartment, options);

				    var chart2 = new google.visualization.BarChart(document.getElementById('chart_div'));
            		chart2.draw(dataGender, options2);

            		var chart3 = new google.visualization.ColumnChart(document.getElementById('chart_div2'));
            		chart3.draw(dataAgeGroup, options3);

            		$('svg').css("max-width",$("#page-content").width()-50);//max-width for graph
            		
            		bindDepartment();
            		$(".hideShowDept,#btnSaveExcel,#btnSavePdf").show();            		
				}

				/*Scroll down code*/
				$('html, body').animate({
			        scrollTop: $("#piechart_3d").offset().top
			    }, 1000);

				window.saveFile = function saveFile () {

					var dataExcelDept = parseData[0];
					var dataExcelGender = parseData[1];
					var dataExcelAge = parseData[2];

					if ($("#selDepartment").val() != '-1') {

						var opts = [{sheetid:'One',header:true},{sheetid:'Two',header:true},{sheetid:'Three',header:true}];
						var res = alasql('SELECT INTO XLSX("DepartmentWiseReport.xlsx",?) FROM ?',
	                 	[opts,[dataExcelDept,dataExcelGender,changedAgeParseData]]);
					}

					else {
						var opts = [{sheetid:'One',header:true},{sheetid:'Two',header:true},{sheetid:'Three',header:true}];
						var res = alasql('SELECT INTO XLSX("DepartmentWiseReport.xlsx",?) FROM ?',
	                 	[opts,[dataExcelDept,dataExcelGender,dataExcelAge]]);
					}
				}
			}			
		});
	});
	
	$("#btnSavePdf").click(function(){
		$.print("#printableArea_1,#printableArea_2");
	});
	
}
function bindDepartment() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/opd_registration.php",
        data: {
            "operation": "showDepartment"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);
                var option =  "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].department + "</option>";
                }
                $('#selDepartment').html(option);
                changeShowDepartment();
            }
        },
        error: function() {}
    });
}
function changeShowDepartment(){		
	$("#selDepartment").change(function(){
		var fromDate ;
		var toDate ;

		var departmentId = $("#selDepartment").val();
		if ($('#radToday').prop( "checked") == true) {
			dataLoad = "today";
		}
		else if($('#radLastSevenDays').prop( "checked") == true) {
			dataLoad = "last7days";
		}
		else if($('#radToFromDate').prop( "checked") == true) {
			dataLoad = "betweenDate";
			fromDate = $("#fromDate").val();
			toDate = $("#toDate").val();
		}
		else if($('#radLastThirtyDays').prop( "checked") == true) {
			dataLoad = "last30days";
		}
		else if($('#radAllTime').prop( "checked") == true) {
			dataLoad = "all";
		}

		if (fromDate == undefined) {
			fromDate ="";
		}
		if (toDate == undefined) {
			toDate = "";
		}

		var postData = {
			"operation" : "showChartData",
			"dataLoad" : dataLoad,
			"fromDate" : fromDate,
			"toDate" : toDate,
			"departmentId" : departmentId
		};
		$.ajax({     
			type: "POST",
			cache: false,
			datatype: "JSON",
			url: "controllers/admin/department_wise_report.php",
			data: postData,
			success: function(data) { 
				if (data !='' && data !=null) {
					age = [['Age-Group', 'Count']];
					var parseData =  JSON.parse(data);

					changedAgeParseData = parseData;

					if (parseData[0]['0-10 Yrs'] ==0 && parseData[0]['11-20 Yrs'] ==0	&& parseData[0]['21-30 Yrs'] ==0 && parseData[0]['31-40 Yrs'] ==0 && parseData[0]['41-50 Yrs'] ==0
						&& parseData[0]['51-60 Yrs'] ==0 && parseData[0]['61-70 Yrs'] ==0 && parseData[0]['71-80 Yrs'] ==0) {

						$('#messagemyModal').modal();
						$('#messageMyModalLabel').text("Alert");
						$('.modal-body').text("No data exist for choosen department!!!");
						$("#chart_div2").hide();
						return false;					
					}
					$("#chart_div2").show();
					$.each(parseData,function(i,v){
						$.each(parseData[i],function(im,nn){
							age.push([im,parseInt(nn)]);	
						});
					});

					var dataAgeGroup = google.visualization.arrayToDataTable(age);

					var options = {
				    	'title':'Visitors Age Analysis','width':1200,'height':1000
					};

            		var chart = new google.visualization.ColumnChart(document.getElementById('chart_div2'));
            		chart.draw(dataAgeGroup, options);
				}
			}			
		});
	});
}
function clear(){
	$("#chart_div,#chart_div2,#piechart_3d,.hideShowDept,#btnSaveExcel,#btnSavePdf").hide();
	$("#toDate,#fromDate").val("");
	$("#txtFromDateError,#txtToDateError").text("");
	$("#fromDate,#toDate").removeClass("errorStyle");
}