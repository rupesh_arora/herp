var surgeryTable;

$(document).ready(function(){
	debugger;
    $(".amountPrefix").text(amountPrefix);
	$("#formSurgery").hide();
    $("#divSurgery").addClass('list');    
    $("#tabSurgeryList").addClass('tab-list-add');
    
    $("#tabSurgery").click(function() { // show the add allergies categories tab
        showAddTab();
        clear();
    });

    $("#tabSurgeryList").click(function() { // show allergies list tab
        showTableList();		
    });

    removeErrorMessage();// remove error messaages

    $("#btnSave").click(function(){
        var flag = false;

        if(validTextField('#txtCost','#txtCostError','Please enter surgery cost') == true)
        {
            flag = true;
        }
        if(validTextField('#txtName','#txtNameError','Please enter surgery name') == true)
        {
            flag = true;
        }
        if(flag == true) {
            return false;
        }

        var name = $("#txtName").val().trim();
        var cost = $("#txtCost").val().trim();
        var desc = $("#txtDesc").val().trim();
        desc = desc.replace(/'/g, "&#39");

        var postData = {
            name : name,
            cost: cost,
            desc : desc,
            "operation" : "save"
        }

        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/surgery.php",
            datatype: "json",
            data: postData,
             success: function(data){
                if(data !='' && data!=null && data !="0"){
                    callSuccessPopUp("Success","Saved  successfully!!!");
                    showTableList();
                    $("#txtName").focus();
                }
                else if (data =="0") {
                    $("#txtName").focus();
                    $("#txtName").addClass("errorStyle");
                    $("#txtNameError").text("This surgery alreday exist.");
                }
            },
            error:function(){

            }
        });
    });

     if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code
        surgeryTable = $('#surgeryTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "bAutoWidth":false,
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = surgeryTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var name = mData["name"];
                        var cost = mData["cost"];
                        var desc = mData["description"];
                        editClick(id,name,cost,desc);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = surgeryTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/surgery.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "name"
                }, {
                    "mData": function(value) {
                            var cost = amountPrefix + value['cost'];
                            return cost;
                        }
                }, {
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [2]
            }, {
                'bSortable': false,
                'aTargets': [3]
            }]

        });
    }
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            surgeryTable.fnClearTable();
            surgeryTable.fnDestroy();
            surgeryTable = "";
            surgeryTable = $('#surgeryTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "bAutoWidth":false,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = surgeryTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var surgeryName = mData['name'];
                            restoreClick(id,surgeryName);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/surgery.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    {
                    "mData": "name"
                }, {
                    "mData": function(value) {
                            var cost = amountPrefix + value['cost'];
                            return cost;
                        }
                }, {
                    "mData": "description"
                }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [2]
                }, {
                    'bSortable': false,
                    'aTargets': [3]
                }]
            });
        } else { // show active data on unchecked   
            surgeryTable.fnClearTable();
            surgeryTable.fnDestroy();
            surgeryTable = "";
            surgeryTable = $('#surgeryTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "bAutoWidth":false,
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = surgeryTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                        var id = mData["id"];
                        var name = mData["name"];
                        var cost = mData["cost"];
                        var desc = mData["description"];
                        editClick(id,name,cost,desc);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = surgeryTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/surgery.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "name"
                    }, {
                        "mData": function(value) {
                            var cost = amountPrefix + value['cost'];
                            return cost;
                        }
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [2,3]
                }]
            });
        }
    });

    $("#btnReset").click(function() {
       $(".amountPrefix").text(amountPrefix);
       clear();
       removeErrorMessage();
       $("#txtName").focus();
    });

});
function clear() {
    $('#txtName').val('');
    $('#txtName').removeClass('errorStyle');
    $('#txtNameError').text('');
    $('#txtCost').val('');
    $('#txtCost').removeClass('errorStyle');
    $('#txtCostError').text('');
    $('#txtDesc').val('');
}
 function editClick(id,name,cost,desc) {
    showAddTab();
    $('#tabSurgery').html("+Update Surgery");
    $("#btnReset").hide();
    $("#btnSave").hide();    
    
    $(".amountPrefix").text(amountPrefix);
    $("#txtValueBox").val(amountPrefix);
    $("#txtValueBox").prop('disabled', true);
    $("#btnUpdate").removeAttr("style");
    
    $("#txtName").removeClass("errorStyle");
    $("#txtNameError").text("");
    $("#txtCost").removeClass("errorStyle");
    $("#txtCostError").text("");
    
    
    $('#txtName').val(name);
    $('#txtCost').val(cost);
    $('#txtDesc').val(desc.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
    //validation
   
    $("#btnUpdate").click(function() { // click update button
        var flag = "false";
        if ($("#txtCost").val().trim()== '') {
            $("#txtCostError").text("Please enter surgery cost");
            $("#txtCost").focus();
            $("#txtCost").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtName").val().trim()== '') {
            $("#txtNameError").text("Please enter surgery name");
            $("#txtName").focus();
            $("#txtName").addClass("errorStyle");
            flag = "true";
        }
        
        if(flag == "true") {
            return false;
        }
        
        
        var name = $("#txtName").val().trim();
        var cost = $("#txtCost").val().trim();
        var desc = $("#txtDesc").val().trim();
        desc = desc.replace(/'/g, "&#39");
        
        
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
        var postData = {
            "operation": "update",
            "name": name,
            "cost":cost,
            "desc": desc,
            "id": id
        }
        $.ajax( //ajax call for update data
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/surgery.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data == "1") {
                        callSuccessPopUp("Success","Surgery updated Successfully!!!");
                        $('#myModal .close').click();
                        showTableList();
                    }
                    else if (data =="0") {
                        $("#txtName").focus();
                        $("#txtName").addClass("errorStyle");
                        $("#txtNameError").text("This surgery alreday exist.");
                    }
                },
                error: function() {
                    $('.close-confirm').click();
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                    $('#messagemyModal').modal();
                }
            }); // end of ajax
        });
    });
} // end update button
function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete surgery");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    var surgeryName = '';
    $('#confirm').attr('onclick', 'deleteSurgery("'+type+'","'+surgeryName+'");');
} // end click fucntion

function restoreClick(id,surgeryName) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore surgery");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteSurgery("'+type+'","'+surgeryName+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteSurgery(type,surgeryName) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/surgery.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    callSuccessPopUp("Success","Surgery deleted Successfully!!!");
                    surgeryTable.fnReloadAjax();
                } else {
                    callSuccessPopUp("Sorry","This Surgery is used , so you can not delete!!!");
                }
            },
            error: function() {
                callSuccessPopUp("Error","Temporary Unavailable to Respond.Try again later!!!");
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/surgery.php",
            data: {
                "operation": "restore",
                "id": id,
                "surgeryName" : surgeryName
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    callSuccessPopUp("Success","Surgery restored Successfully!!!");
                    surgeryTable.fnReloadAjax();
                }
                else{
                    callSuccessPopUp('Alert','This surgery already exist. So you can\'t restore it.!!!');
                }
            },
            error: function() {    
                callSuccessPopUp("Error","Temporary Unavailable to Respond.Try again later!!!");    
            }
        });
    }
}

function showTableList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#formSurgery").hide();
    $(".blackborder").show();

    $("#tabSurgeryList").addClass('tab-detail-add');
    $("#tabSurgeryList").removeClass('tab-detail-remove');
    $("#tabSurgery").removeClass('tab-list-add');
    $("#tabSurgery").addClass('tab-list-remove');
    $("#divSurgery").addClass('list');    

    $("#btnReset").show();
    $("#btnSave").show();
    $('#btnUpdate').hide();
    $('#tabSurgery').html("+Add Surgery");
    clear();
}

function showAddTab(){
    $("#formSurgery").show();
    $(".blackborder").hide();
   
    $("#tabSurgeryList").removeClass('tab-detail-add');
    $("#tabSurgeryList").addClass('tab-detail-remove');
    $("#tabSurgery").removeClass('tab-list-remove');    
    $("#tabSurgery").addClass('tab-list-add');
    $("#divSurgery").addClass('list');
    
    $("#txtValueBox").prop('disabled', true);
    $("#txtValueBox").val(amountPrefix);
    $("#txtName").focus();    
    removeErrorMessage();
}  
       