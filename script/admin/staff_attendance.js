var attendanceTable;
var absentTable;
$(document).ready(function () {
    debugger;
    showAbsent();
    $("#attendanceList").hide();
    $("#absentList").show();
    var count = 0;
    attendanceTable = $('#tblSearchAttendanceList').dataTable({ // inilize datatable on load time.
        "bFilter": true,
        "processing": true,
        "aaSorting": [],
        "sPaginationType": "full_numbers",
        "bAutoWidth" : false,
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [4]
        }]
     });
    absentTable = $('#tblAbsentList').dataTable({ // inilize datatable on load time.
        "bFilter": true,
        "processing": true,
        "aaSorting": [],
        "sPaginationType": "full_numbers",
        "bAutoWidth" : false,
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [3]
        }]
     });
    // bind staff name from staff type
    $("#SelStaffType").change(function(){
        var staffId = $('#SelStaffType').val();
        if(staffId != ''){
            bindStaffId(staffId);
        }
        else{
            $("#SelStaffType").html("<option value=''>--Select--</option>");
        }
    });
   /*$("#txtFromDate").datepicker({autoclose:true}).on('changeDate', function(selected){
        startDate = new Date(selected.date.valueOf());
        $('#txtToDate').datepicker('setStartDate', startDate);
    });  
    $("#txtToDate").datepicker({autoclose:true,endDate:'+0d'}).on('changeDate', function(selected){
        startDate = new Date(selected.date.valueOf());
        $('#txtFromDate').datepicker('setEndDate', startDate);
    });*/
    $("#txtFromDate").datepicker().on('changeDate', function(selected){
        startDate = new Date(selected.date.valueOf());
        $('#txtToDate').datepicker('setStartDate', startDate);
        $(this).datepicker('hide');
    });  
    
    $("#txtToDate").datepicker().on('changeDate', function(selected){
        startDate = new Date(selected.date.valueOf());
        $('#txtFromDate').datepicker('setEndDate', startDate);
        $(this).datepicker('hide');
    });
    
    $("#txtFromDate").click(function() {
        $("#txtFromDate").datepicker("show");
    });
    
    $("#txtToDate").click(function() {
        $("#txtToDate").datepicker("show");
    });

    $("#txtFromDate").click(function() {
        $("#txtFromDateError").text("");
    });
    $("#txtToDate").click(function() {
        $("#txtToDateError").text("");
    });
    bindStaffType();
    removeErrorMessage();

    $("#btnSearch").click(function() {
       
        var flag="false";
       
        if(validTextField('#SelStaffType','#SelStaffTypeError','Please select staff type') == true)
        {
            flag = true;
        }
        if(flag == true) {
            return false;
        }

        $("#attendanceList").show();
        $("#absentList").hide();

        var staffId = $("#selStaffId").val();
        var fromDate = $('#txtFromDate').val().trim();
        var toDate = $('#txtToDate').val().trim();
        var staffTypeId = $("#SelStaffType").val();
        var attendanceType = $('#selAttendance').val();

        var postData = {
            staffId:staffId,
            fromDate:fromDate,
            toDate:toDate,
            attendanceType:attendanceType,
            "operation" : "showAttendance",
            "staffTypeId" : staffTypeId
        }

        dataCall("controllers/admin/staff_attendance.php", postData, function (result){
            if (result !='') {
                
                $("#todayAbsentStaff").hide();
                attendanceTable.fnClearTable();
                var parseData = jQuery.parseJSON(result);

                for(var i=0;i<parseData.length;i++) {

                    var staffId = parseData[i].name;
                    var date = parseData[i].date;                    
                    var remarks = parseData[i].remarks;
                    var punchIn = parseData[i].punch_in;
                    var punchOut = parseData[i].punch_out;
                    if(parseData[i].punch_out != "" && parseData[i].punch_out != null){

                        newPunchIn = new Date(punchIn);
                        newPunchOut = new Date(punchOut);

                        var diffTime = newPunchOut - newPunchIn;

                        var diffSeconds = diffTime/1000;
                        var HH = Math.floor(diffSeconds/3600);
                        var MM = Math.floor(diffSeconds%3600)/60;

                        var duration = ((HH < 10)?("0" + HH):HH) + ":" + ((MM < 10)?("0" + MM):MM);
                    }
                    else{
                        var duration = "N/A";
                    }


                    if ($("#selAttendance").val() == "absent") {

                        showAbsintes(staffId,parseData,i);
                                                  
                    }
                    else {                            
                        attendanceTable.fnAddData([staffId,date,punchIn,punchOut,duration,remarks]);
                    }                                
                }
            }
            else {
                if ($("#selStaffId").val() !='') {
                    var staffId = $("#selStaffId").val();
                    
                    showAbsintes(staffId,'','');
                }
            }          
        });
    });
    $("#btnReset").click(function() {
        clearFormDetails(".leaveApplyForm");
        attendanceTable.fnClearTable();
        $('#selAttendance').val("present");
    });
});
function bindStaffType() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/staffregistration.php",
        data: {
            "operation": "showstafftype"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
                }
                $('#SelStaffType').html(option);
            }
        },
        error: function() {}
    });
}
function bindStaffId(staffId){
    var staffType = $('#SelStaffType').val();
    var postData = {
        staffType:staffType,
        "operation":"showstaffid"
    };
    
    dataCall("controllers/admin/staff_attendance.php", postData, function (result){
        if(result.length > 2){
            var parseData = jQuery.parseJSON(result);
            var option = "<option value=''>--Select--</option>";
            $.each(parseData,function(i,v){
                option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
            });
            $('#selStaffId').html(option);
        }
    });
}
function showAbsent() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/staff_attendance.php",
        data: {
            "operation": "showAbsent"
        },
        success: function(data) {
            if (data != null && data != "") {
               absentTable.fnClearTable();
                var parseData = jQuery.parseJSON(data);

                for(var i=0;i<parseData.length;i++) {
                    var staffId = parseData[i].id;

                    /*Adding prefix to satff id*/
                    var staffIdLength = staffId.length;
                    for (var j=0;j<6-staffIdLength;j++) {
                        staffId = "0"+staffId;
                    }
                    staffId = staffPrefix+staffId;

                    var staffName = parseData[i].staff_name;
                    absentTable.fnAddData([staffId,staffName,todayDate(),"Absent"]);                
                }
            }
        },
        error: function() {}
    });
}
function showAbsintes(staffId,parseData,i) {
    if ($('#txtFromDate').val() =='' && $('#txtToDate').val() == '') {

        //calculate the last three month date
        var start = new Date();
        start.setFullYear(start.getFullYear(), start.getMonth()-3);

        var splitDate = (start.toLocaleDateString()).split('/');

        var lastThirdMonth = splitDate[0];
        if (lastThirdMonth.toString().length == 1) {
            var lastThirdMonth = "0"+splitDate[0]
        }

        var lastThirdDay = splitDate[1];
        if (lastThirdDay.toString().length == 1) {
            var lastThirdDay = "0"+splitDate[1];
        }

        var lastThirdMonthYear = splitDate[2];


        var day = 1000*60*60*24;

        date1 = new Date(lastThirdMonthYear+"-"+lastThirdMonth+"-"+lastThirdDay);

        if (parseData != '') {
            //if last third month record not available for a staff then show data from joining date
            if (date1 < new Date(parseData[i].created_on)) {
                date1 = new Date(parseData[i].created_on);
            }
        } 

        date2 = new Date(todayDate());
    }

    else if ($('#txtFromDate').val() !='' && $('#txtToDate').val() == '') {

        date1 = new Date($('#txtFromDate').val());

        if (parseData != '') {
            //if last third month record not available for a staff then show data from joining date
            if (date1 < new Date(parseData[i].created_on)) {
                date1 = new Date(parseData[i].created_on);
            }
        }
        date2 = new Date(todayDate());
    }

    else if ($('#txtFromDate').val() =='' && $('#txtToDate').val() != '') {

        //calculate the last three month date
        var start = new Date($('#txtToDate').val());
        start.setFullYear(start.getFullYear(), start.getMonth()-3);

        var splitDate = (start.toLocaleDateString()).split('/');

        var lastThirdMonth = splitDate[0];
        if (lastThirdMonth.toString().length == 1) {
            var lastThirdMonth = "0"+splitDate[0]
        }

        var lastThirdDay = splitDate[1];
        if (lastThirdDay.toString().length == 1) {
            var lastThirdDay = "0"+splitDate[1];
        }

        var lastThirdMonthYear = splitDate[2];                            

        date1 = new Date(lastThirdMonthYear+"-"+lastThirdMonth+"-"+lastThirdDay);

        if (parseData != '') {
            //if last third month record not available for a staff then show data from joining date
            if (date1 < new Date(parseData[i].created_on)) {
                date1 = new Date(parseData[i].created_on);
            }
        }

        date2 = new Date($('#txtToDate').val());
    }

    else{
        date1 = new Date($('#txtFromDate').val());
        if (parseData != '') {
            //if last third month record not available for a staff then show data from joining date
            if (date1 < new Date(parseData[i].created_on)) {
                date1 = new Date(parseData[i].created_on);
            }
        }
        date2 = new Date($('#txtToDate').val());
    }

    var day = 1000*60*60*24;
    var diff = (date2.getTime()- date1.getTime())/day;

    for(var j=0;j<=diff; j++) {                        
       var xx = date1.getTime()+day*j;
       var yy = new Date(xx);

       var matchedMonth = (yy.getMonth()+1);
       if (matchedMonth.toString().length == 1) {
            var matchedMonth = "0"+matchedMonth;
        }

        var matchedDate = yy.getDate();
        if (matchedDate.toString().length == 1) {
            var matchedDate = "0"+matchedDate;
        }

        var matchedYear = yy.getFullYear();

        var compareExactDate = matchedYear+"-"+matchedMonth+"-"+matchedDate;//final Dates between thoswe

        if (parseData != '') {
            if (compareExactDate !=  parseData[i].date) {                                   
                    
                attendanceTable.fnAddData([staffId,compareExactDate,"N/A","N/A","N/A",'']); 
            }      
        }
        else{
            attendanceTable.fnAddData([staffId,compareExactDate,"N/A","N/A","N/A",'']);
        }                      
    }
}