var viewTreatmentTable;
$("document").ready(function(){	
debugger;
	$("#txtTreatmentDate").datepicker();
	viewTreatmentTable = $("#tblViewTreatment").dataTable();
	$("#btnReset").on('click',function(){
		clear();
	});

	// keyup functionality
	$("#txtIPDId").keyup(function(){
		if($("#txtIPDId").val() != "") {
			$("#txtIPDIdError").text("");
			$("#txtIPDId").removeClass('errorStyle');
		}
	});
	
	// call event for search details
	$("#btnSearch").unbind();
	$("#btnSearch").on('click',function() {
		var flag = false;
		if($("#txtIPDId").val() == "") {
			$("#txtIPDIdError").text("Please enter IPD Id");
			$("#txtIPDId").addClass('errorStyle');
			$("#txtIPDId").focus();
			return true;
		}
		var IPDId = $("#txtIPDId").val();
		var ipdPrefix = IPDId.substring(0, 3);
		IPDId = IPDId.replace ( /[^\d.]/g, '' ); 
		IPDId = parseInt(IPDId);
		if(isNaN(IPDId)){
			IPDId="";
		}
		var treatmentDate = $("#txtTreatmentDate").val();		
		var postData = {
			"operation": "search",
			"IPDId": IPDId,
			"treatmentDate": treatmentDate
		}
		
		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/view_treatments.php",
			datatype: "json",
			data: postData,

			success: function(result) {
				
				//$('input[type=text]').val("");
				$('select[name=tblViewTreatment_length]').val("10");
	
				dataSet = JSON.parse(result); // convert data into json
				//var dataSet = jQuery.parseJSON(data);
				viewTreatmentTable.fnClearTable(); // clear datatable
				viewTreatmentTable.fnDestroy(); // remove datatable
			
				//Datatable code
				viewTreatmentTable = $("#tblViewTreatment").dataTable({
					"bFilter": true,
					"processing": true,
					"bAutoWidth":false,
					"sPaginationType":"full_numbers",
					"fnDrawCallback": function ( oSettings ) {						
					},
					"aaData": dataSet,
					"aoColumns": [
						{ "mData": function (o) { 	
							var ipdId = o["ipd_id"];
							var ipdPrefix = o["ipdPrefix"];
								
							var ipdIdLength = ipdId.length;
							for (var i=0;i<6-ipdIdLength;i++) {
								ipdId = "0"+ipdId;
							}
							ipdId = ipdPrefix+ipdId;
							return ipdId; 
							}
						},
						{ "mData": "treatment" },
						{ "mData": "price" },
						{ "mData": "treatments_date" }
					],
					/*Disable sort for following columns*/
					/* aoColumnDefs: [
					   { 'bSortable': false, 'aTargets': [ 6 ] },
					   { 'bSortable': false, 'aTargets': [ 7 ] }
				   ] */
				} );
			},
			error: function() {
									
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
		
	});
});
// clear details
function clear(){
	$('input[type=text]').next("span").text("");
	$('input[type=text]').val("");
	$('input[type=text]').removeClass("errorStyle");
	$('select').next("span").hide();
	$('#txtReasonAdmission').val("");
	$('#txtNotes').val("");
	viewTreatmentTable.fnClearTable();
	$('#checkICU').attr('checked',false);
	$("#textIPDId").removeAttr("disabled");
	$("#txtMobile").val("");
	$('select[name=tblViewTreatment_length]').val("10");
	loadDetails();
}