$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   triage.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages triage data
 *************************************************************************************************** */	
	loader();
	debugger;
	$(".tab-content .block-title").hide(); // hide title in consultant screen
	
    var otable;
	var imagePath;
    //making time a global varibale
    var time;
    var getBMI;

    //during opemning of page focus on this text box
    $("#textVisitId").focus();

	//validation
	$("#btnLoad").click(function() {
		$("#selPatientType").val("");
		$("#txtPatientId").val("");
		$("#txtPatientName").val("");
		$("#txtGender").val("");
		$("#txtVisitDate").val("");
		$("#txt_year").val("");
		$("#txt_month").val("");
		$("#txt_day").val("");
		var visitId = $("#textVisitId").val();
		
		var flag=false;
        if ($("#textVisitId").val() == "") {
			$("#textVisitId").focus();
            $("#textVisitIdError").text("Please enter visit id");
            $("#textVisitId").addClass("errorStyle");
			flag=true;
        }
        if(visitId.length != 9){
			clearTriage();
			$("#textVisitId").focus();
			$("#textVisitIdError").text("Please enter valid visit id");
			$("#textVisitId").addClass("errorStyle");     
			flag = "true";
		}
        if (flag == "true") {
            return false;
        }
       
		var visitPrefix = visitId.substring(0, 3);
		visitId = visitId.replace ( /[^\d.]/g, '' ); 
		visitId = parseInt(visitId);
        
	    //ajax call to show data

	    
        var postData = {
   			"operation":"showData",
   			"visitId" : visitId,
   			"visitPrefix" : visitPrefix
   		}
	    $.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/triage.php",
			data: postData,
			success: function(data) { 
				
				if(data != "0"){		 
					var parseData = jQuery.parseJSON(data);					
					
					var patientId = parseData[0].id;
					var patientPrefix = parseData[0].patient_prefix;
					
					var patientIdLength = patientId.length;
					for (var i=0;i<6-patientIdLength;i++) {
						patientId = "0"+patientId;
					}
					patientId = patientPrefix+patientId;
					$("#txtPatientId").val(patientId);
					
					$("#txtPatientName").val(parseData[0].first_name); 
   					
					if(parseData[0].gender == "M") {
						$("#txtGender").val("Male");
					}
					else {
						$("#txtGender").val("Female");
					}
					
					if(parseData[0].images_path != ""){
						 imagePath = "./upload_images/patient_dp/"+parseData[0].images_path;
						 $("#imgPatient").attr("src",imagePath);
					}
					else{
						$("#imgPatient").attr("src",'./img/default.jpg');
					}
                   
					
					//convert time string to date
					time = parseData[0].created_on;
					var visitDate=convertDate(time);
					$("#txtVisitDate").val(visitDate);
					//var dateTime = new Date(parseInt(time));
					    					
					var getDOB = parseData[0].dob; 
					$("#txtWeight").val(parseData[0]["weight"]);
					$("#selPatientType").val(parseData[0]["type"]);
					$("#txtTemprature").val(parseData[0]["temperature"]) ;
					$("#txtHeight").val(parseData[0]["height"]);
					$("#txtPulse").val(parseData[0]["pulse"]);
					
					if(parseData[0]["blood_pressure"] != null){
						var arr=parseData[0]["blood_pressure"].split('/');
						$("#txtSystolic").val(arr[0]);
						$("#txtDiastolic").val(arr[1]);
					}
					
					
					$("#txtHeadCircumference").val(parseData[0]["head_circum"]);
					$("#txtBodySurfaceArea").val(parseData[0]["body_surface_area"]);
					$("#txtRespirationRate").val(parseData[0]["respiration_rate"]);
					$("#txtOxygenSaturation").val(parseData[0]["oxygen_saturation"]);
					$("#txtHeartRate").val(parseData[0]["weight"]);
					$("#txtBMI").val(parseData[0]["bmi"]);
					$("#txtNotes").val(parseData[0]["notes"]);
					
					var new_age = getAge(getDOB);

					var split = new_age.split(' ');
					var age_years = split[0];
					var age_month = split[1];
					var age_day = split[2];

					$("#txt_year").val(age_years);
					$("#txt_month").val(age_month);
					$("#txt_day").val(age_day);
					
					$("#textVisitIdError").text("");
					$("#textVisitId").removeClass("errorStyle");
					$("#textVisitId").attr("disabled", "disabled");

					if($("#txtTemprature").val().trim() != '' && $("#txtTemprature").val().trim() < 35.0){
                    $("#txtTempratureError").text("Hypothemia");
                }
                else if($("#txtTemprature").val().trim() >= 35 && $("#txtTemprature").val().trim() < 37.5){
                    $("#txtTempratureError").text("");
                }
                else if($("#txtTemprature").val().trim() > 37.5){
                    $("#txtTempratureError").text("Fever");
                }
                else {
                    $("#txtTempratureError").text("");
                }
             
            
                if($("#txtBMI").val().trim() != '' && $("#txtBMI").val().trim() < 18.5){
                    $("#txtBMIError").text("Underweight");
                }
                else if($("#txtBMI").val().trim() >= 18.5 && $("#txtBMI").val().trim() < 25){
                    $("#txtBMIError").text("");
                }
                else if($("#txtBMI").val().trim() >= 25.0 && $("#txtBMI").val().trim() <= 29.9){
                    $("#txtBMIError").text("Overweight");
                }
                else if($("#txtBMI").val().trim() >= 30.0 && $("#txtBMI").val().trim() <= 34.9){
                    $("#txtBMIError").text("Obesity class I");
                }
                else if($("#txtBMI").val().trim() >= 35.0 && $("#txtBMI").val().trim() <= 39.9){
                    $("#txtBMIError").text("Obesity class II");
                }
                else if($("#txtBMI").val().trim() >= 40){
                    $("#txtBMIError").text("Obesity class III");
                }
                else {
                    $("#txtBMIError").text("");
                }
            

            
                if($("#txtBMI").val().trim() != '' && $("#txtBMI").val().trim() < 18.5){
                    $("#txtBMIError").text("Underweight");
                }
                else if($("#txtBMI").val().trim() >= 18.5 && $("#txtBMI").val().trim() < 25){
                    $("#txtBMIError").text("");
                }
                else if($("#txtBMI").val().trim() >= 25.0 && $("#txtBMI").val().trim() <= 29.9){
                    $("#txtBMIError").text("Overweight");
                }
                else if($("#txtBMI").val().trim() >= 30.0 && $("#txtBMI").val().trim() <= 34.9){
                    $("#txtBMIError").text("Obesity class I");
                }
                else if($("#txtBMI").val().trim() >= 35.0 && $("#txtBMI").val().trim() <= 39.9){
                    $("#txtBMIError").text("Obesity class II");
                }
                else if($("#txtBMI").val().trim() >= 40){
                    $("#txtBMIError").text("Obesity class III");
                }
                else {
                    $("#txtBMIError").text("");
                }
           
            
            
                if($("#txtPulse").val().trim() != '' && $("#txtPulse").val().trim() < 60){
                    $("#txtPulseError").text("Bradycardia");
                }
                else if($("#txtPulse").val().trim() >= 60 && $("#txtPulse").val().trim() <= 100){
                    $("#txtPulseError").text("");
                }
                else if($("#txtPulse").val().trim() > 100){
                    $("#txtPulseError").text("Tachycardia");
                }
                else {
                    $("#txtPulseError").text("");
                }
            
               if($("#txtSystolic").val().trim() >= 140 || $("#txtDiastolic").val().trim() >= 90 ){
					$("#txtBloodPressureError").text("High BP");
				}
				else if($("#txtSystolic").val().trim() != ''){
					if($("#txtSystolic").val().trim() <= 90 || $("#txtDiastolic").val().trim() <= 60){
						$("#txtBloodPressureError").text("Low BP");
					}
					else {
					$("#txtBloodPressureError").text("");
					}
				}
				else if($("#txtSystolic").val().trim() < 140 && $("#txtDiastolic").val().trim() < 90 
					&& $("#txtDiastolic").val().trim() > 90 && $("#txtDiastolic").val().trim() > 60){
					$("#txtBloodPressureError").text("");
				}
				else {
					$("#txtBloodPressureError").text("");
				}

           
                if($("#txtRespirationRate").val().trim() != '' &&  $("#txtRespirationRate").val().trim() < 12){
                    $("#txtRespirationRateError").text("Bradypnea");
                }
                else if($("#txtRespirationRate").val().trim() >= 12 && $("#txtRespirationRate").val().trim() <= 18){
                    $("#txtRespirationRateError").text("");
                }
                else if($("#txtRespirationRate").val().trim() > 18){
                    $("#txtRespirationRateError").text("Tachypnea");
                }
                else {
                    $("#txtRespirationRateError").text("");
                }
           
                if($("#txtHeartRate").val().trim() != '' &&  $("#txtHeartRate").val().trim() < 60){
                    $("#txtHeartRateError").text("Bradycardia");
                }
                else if($("#txtHeartRate").val().trim() >= 60 && $("#txtHeartRate").val().trim() <= 100 ){
                    $("#txtHeartRateError").text("");
                }
                else if($("#txtHeartRate").val().trim() > 100){
                    $("#txtHeartRateError").text("Tachycardia");
                }
                else {
                    $("#txtHeartRateError").text("");
                }
				} 
			    if(data == "0"){
					clearTriage();
					$("#textVisitId").focus();
					$("#textVisitIdError").text("Please enter valid visit id");
					$("#textVisitId").addClass("errorStyle");
				} 
			},
			error:function(){
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	});
	
	$("#txtTemprature").blur(function() {
		if($("#txtTemprature").val().trim() != '' && $("#txtTemprature").val().trim() < 35.0){
			$("#txtTempratureError").text("Hypothemia");
		}
		else if($("#txtTemprature").val().trim() >= 35 && $("#txtTemprature").val().trim() < 37.5){
			$("#txtTempratureError").text("");
		}
		else if($("#txtTemprature").val().trim() > 37.5){
			$("#txtTempratureError").text("Fever");
		}
		else {
			$("#txtTempratureError").text("");
		}
	}); 

	$("#txtHeight").blur(function() {
		if($("#txtBMI").val().trim() != '' && $("#txtBMI").val().trim() < 18.5){
			$("#txtBMIError").text("Underweight");
		}
		else if($("#txtBMI").val().trim() >= 18.5 && $("#txtBMI").val().trim() < 25){
			$("#txtBMIError").text("");
		}
		else if($("#txtBMI").val().trim() >= 25.0 && $("#txtBMI").val().trim() <= 29.9){
			$("#txtBMIError").text("Overweight");
		}
		else if($("#txtBMI").val().trim() >= 30.0 && $("#txtBMI").val().trim() <= 34.9){
			$("#txtBMIError").text("Obesity class I");
		}
		else if($("#txtBMI").val().trim() >= 35.0 && $("#txtBMI").val().trim() <= 39.9){
			$("#txtBMIError").text("Obesity class II");
		}
		else if($("#txtBMI").val().trim() >= 40){
			$("#txtBMIError").text("Obesity class III");
		}
		else {
			$("#txtBMIError").text("");
		}
	}); 

	$("#txtWeight").blur(function() {
		if($("#txtBMI").val().trim() != '' && $("#txtBMI").val().trim() < 18.5){
			$("#txtBMIError").text("Underweight");
		}
		else if($("#txtBMI").val().trim() >= 18.5 && $("#txtBMI").val().trim() < 25){
			$("#txtBMIError").text("");
		}
		else if($("#txtBMI").val().trim() >= 25.0 && $("#txtBMI").val().trim() <= 29.9){
			$("#txtBMIError").text("Overweight");
		}
		else if($("#txtBMI").val().trim() >= 30.0 && $("#txtBMI").val().trim() <= 34.9){
			$("#txtBMIError").text("Obesity class I");
		}
		else if($("#txtBMI").val().trim() >= 35.0 && $("#txtBMI").val().trim() <= 39.9){
			$("#txtBMIError").text("Obesity class II");
		}
		else if($("#txtBMI").val().trim() >= 40){
			$("#txtBMIError").text("Obesity class III");
		}
		else {
			$("#txtBMIError").text("");
		}
	});
	
	
	$("#txtPulse").blur(function() {
		if($("#txtPulse").val().trim() != '' && $("#txtPulse").val().trim() < 60){
			$("#txtPulseError").text("Bradycardia");
		}
		else if($("#txtPulse").val().trim() >= 60 && $("#txtPulse").val().trim() <= 100){
			$("#txtPulseError").text("");
		}
		else if($("#txtPulse").val().trim() > 100){
			$("#txtPulseError").text("Tachycardia");
		}
		else {
			$("#txtPulseError").text("");
		}
	}); 
	$("#txtDiastolic").blur(function() {
		if($("#txtSystolic").val().trim() >= 140 || $("#txtDiastolic").val().trim() >= 90 ){
			$("#txtBloodPressureError").text("High BP");
		}
		else if($("#txtSystolic").val().trim() != ''){
			if($("#txtSystolic").val().trim() <= 90 || $("#txtDiastolic").val().trim() <= 60){
				$("#txtBloodPressureError").text("Low BP");
			}
			else {
			$("#txtBloodPressureError").text("");
		}
		}
		else if($("#txtSystolic").val().trim() < 140 && $("#txtDiastolic").val().trim() < 90 
			&& $("#txtDiastolic").val().trim() > 90 && $("#txtDiastolic").val().trim() > 60){
			$("#txtBloodPressureError").text("");
		}
		else {
			$("#txtBloodPressureError").text("");
		}
	});
	$("#txtRespirationRate").blur(function() {
		if($("#txtRespirationRate").val().trim() != '' &&  $("#txtRespirationRate").val().trim() < 12){
			$("#txtRespirationRateError").text("Bradypnea");
		}
		else if($("#txtRespirationRate").val().trim() >= 12 && $("#txtRespirationRate").val().trim() <= 18){
			$("#txtRespirationRateError").text("");
		}
		else if($("#txtRespirationRate").val().trim() > 18){
			$("#txtRespirationRateError").text("Tachypnea");
		}
		else {
			$("#txtRespirationRateError").text("");
		}
	});
	$("#txtHeartRate").blur(function() {
		if($("#txtHeartRate").val().trim() != '' &&  $("#txtHeartRate").val().trim() < 60){
			$("#txtHeartRateError").text("Bradycardia");
		}
		else if($("#txtHeartRate").val().trim() >= 60 && $("#txtHeartRate").val().trim() <= 100 ){
			$("#txtHeartRateError").text("");
		}
		else if($("#txtHeartRate").val().trim() > 100){
			$("#txtHeartRateError").text("Tachycardia");
		}
		else {
			$("#txtHeartRateError").text("");
		}
	}); 
	
	/*$("#txtSystolic").keyup(function() {
		if(!validRangeNumber($(this).val())){
			$("#txtBloodPressureError").text("Please enter valid systolic value.");
		}
		else{
			$("#txtBloodPressureError").text("");
			$("#txtSystolic").removeClass("errorStyle");
		}
	});	
	

	$("#txtDiastolic").keyup(function() {
		if(!validRangeNumber($(this).val())){
			$("#txtBloodPressureError").text("Please enter valid diastolic value.");
		}
		else{
			$("#txtBloodPressureError").text("");
			$("#txtDiastolic").removeClass("errorStyle");
		}
	});	*/
	
	$("#txtHeight").keyup(function() {
        if ($("#txtHeight").val() != "") {
            $("#txtHeightError").text("");
            $("#txtHeight").removeClass("errorStyle");
        }
    });
	
	$("#txtWeight").keyup(function() {	
        if ($("#txtWeight").val() != "") {
            $("#txtWeightError").text("");
            $("#txtWeight").removeClass("errorStyle");
        }
    });
	
    $("#txtHeight").keyup(function() {
        $("#txtBMI").val(calculateBmi());
    });
	
	$("#txtWeight").keyup(function() {
        $("#txtBMI").val(calculateBmi());
    });
	
	//Save the data on save button click
    $("#btnSubmit").click(function() {
        //validation
        var flag=false;
		var flagCheck = false;
		
		if ($("#textVisitId").val() == "") {
			$("#textVisitId").focus();
            $("#textVisitIdError").text("Please enter visit id");
            $("#textVisitId").addClass("errorStyle");
			flag = true;
        }

        if (flag == true) {
            return false;
        }        

        var visitId = parseInt($("#textVisitId").val().replace ( /[^\d.]/g, '' ));
        var patientId = parseInt($("#txtPatientId").val().replace ( /[^\d.]/g, '' ));
        var weight = $("#txtWeight").val();
        var temprature = $("#txtTemprature").val() ;
        var height = $("#txtHeight").val();
        var pulse = $("#txtPulse").val();
        var bloodPressure = ($("#txtSystolic").val() + "/" + $("#txtDiastolic").val());
        var headCircumference = $("#txtHeadCircumference").val();
        var bodySurfaceArea = $("#txtBodySurfaceArea").val();
        var respirationRate = $("#txtRespirationRate").val();
        var oxygenSaturation = $("#txtOxygenSaturation").val();
        var heartRate = $("#txtHeartRate").val();
        var bmi = calculateBmi();
        var notes = $("#txtNotes").val();
        var postData = {
            "operation":"save",
            "visitId" :visitId,
            "patientId" : patientId,
            "weight":weight,
            "temprature":temprature,
            "height":height,
            "pulse":pulse,
            "bloodPressure":bloodPressure,
            "headCircumference":headCircumference,
            "bodySurfaceArea":bodySurfaceArea,
            "respirationRate" : respirationRate,
            "oxygenSaturation" : oxygenSaturation,
            "heartRate" : heartRate,
            "bmi" : bmi,
            "notes" : notes
        }
		
		// use confirm modal
		$('#confirmUpdateModalLabel').text();
		if(flagCheck == true) {
			$('#updateBody').text("Are you sure that you want to update with out of range value?");
		}
		else if(flagCheck == false) {
			$('#updateBody').text("Are you sure that you want to update this?");
		}
        
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function() { 
			$.ajax(
				{                   
				type: "POST",
				cache: false,
				url: "controllers/admin/triage.php",
				datatype:"json",
				data: postData,
				
				success: function(data) {
					if(data != "0" && data != ""){
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Triage record updated successfully!!!");
						$("#textVisitId").removeAttr("disabled", "disabled");
						clearTriage();
						$('#txtBMIError').text('');
						$('#messagemyModal').modal();
					}
				},
				error:function() {
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
			});
		}); 
    });


    // keyup functionality
    $("#textVisitId").keyup(function() {
        if ($("#textVisitId").val() != "") {
            $("#textVisitIdError").text("");
            $("#textVisitId").removeClass("errorStyle");
        }
    });

    //on reset button click
    $("#btnReset").click(function() {
        $("#textVisitId").focus();
		$("#textVisitId").removeAttr("disabled", "disabled");
        clearTriage();
    });
	
	// on search icon function
	$("#serachIcon").click(function(){
		$('#triageModalLabel').text("Search Visit");
		$('#triageModalBody').load('views/admin/view_visit.html',function(){
			$("#thing").val("0");
		});
		$('#myTriageModal').modal();
        $('#btnSubmit').css({"position":"static"});
        $('#btnReset').css({"position":"static"});
	
	});
	
	// that event use in consultant screen
	$("#myTriageModal #btnLoad").click(function(){
		var value = $("#thing").val();
		if(value == "4"){
			var flag=false;
        if ($("#myTriageModal #textVisitId").val() == "") {
			$("#myTriageModal #textVisitId").focus();
            $("#myTriageModal #textVisitIdError").text("Please enter visit id");
            $("#myTriageModal #textVisitId").addClass("errorStyle");
			flag=true;
        }
        if (flag == true) {
        	return false;
        }
        
	    //ajax call to show data

	    var visitId = $("#myTriageModal #textVisitId").val();
        var postData = {
   			"operation":"showData",
   			"visitId" : visitId
   		}
	    $.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/triage.php",
			data: postData,
			success: function(data) { 
				if(data != "0"){		 
					var parseData = jQuery.parseJSON(data);
				
						for (var i=0;i<parseData.length;i++) {
							$("#myTriageModal #txtPatientId").val(parseData[i].id);
							$("#myTriageModal #txtPatientName").val(parseData[i].first_name);    					
							$("#myTriageModal #txtGender").val(parseData[i].gender);
							
							//convert time string to date
							time = parseData[i].created_on;
							//var dateTime = new Date(parseInt(time));
							var date = new Date(time*1000);
							
							var getDOB = parseData[i].dob;
						}  
						var new_age = getAge(getDOB);

						var split = new_age.split(' ');
						var age_years = split[0];
						var age_month = split[1];
						var age_day = split[2];

						$("#myTriageModal #txt_year").val(age_years);
						$("#myTriageModal #txt_month").val(age_month);
						$("#myTriageModal #txt_day").val(age_day);
						
						var get_date = date.getDate();
						var year = date.getFullYear();
						var month = date.getMonth();
	 
						$("#myTriageModal #txtVisitDate").val(get_date +"/" + month + "/" + year);
						$("#myTriageModal #textVisitIdError").text("");
						$("#myTriageModal #textVisitId").removeClass("errorStyle");
						
					} 
					else{
						 $('#messageMyModalLabel').text("Sorry");
						 $('.modal-body').text("Data does not exist!!!");
						 $('#messagemyModal').modal();
						 clearTriage();
					} 		   
				},
				error:function(){	
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
				}
			});
		}
	});
	$("#txtBloodPressure").keypress(function(e) {
        if ((e.which < 47 || e.which > 57)) { // for disable alphabates keys
            return false;
        }
    });

    $('#textVisitId').keypress(function (e) {
        if (e.which == 13) {
            $("#btnLoad").click();
        }
    });
});

function convertDate(o){
        var dbDateTimestamp = o;
        var dateObj = new Date(dbDateTimestamp * 1000);
        var yyyy = dateObj.getFullYear().toString();
        var mm = (dateObj.getMonth()+1).toString(); // getMonth() is zero-based
        var dd  = dateObj.getDate().toString();
        return yyyy +"-"+ (mm[1]?mm:"0"+mm[0]) +"-"+ (dd[1]?dd:"0"+dd[0]);
}
		
//This function is used for calculate the BMI
function calculateBmi(){
	    var getBMI;
		if(($("#txtWeight").val() != "") && ($("#txtHeight").val() != "")){
			var getWeight = $("#txtWeight").val();
			var getpartialheight = $("#txtHeight").val();
			var getHeight =  Math.round(getpartialheight / 100);

			getBMI=  getWeight / (getHeight *  getHeight);
		}
		else{
			getBMI= "";
		}
		return getBMI;
}

/*Function to validate number*/
function validnumber(number) {
    floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
    return floatRegex.test(number);
}

// check number  only 1 to 3 digit vallue
function validRangeNumber(number) {
	intRegex = /^\d{1,3}$/;
	return intRegex.test(number);
};
//This function is used for calculate the age 
function getAge(dateString) {
    var now = new Date();
    var today = new Date(now.getYear(), now.getMonth(), now.getDate());

    var yearNow = now.getYear();
    var monthNow = now.getMonth();
    var dateNow = now.getDate();

    var dob = new Date(dateString.substring(0, 4), dateString.substring(5, 7) - 1, dateString.substring(8, 10));

    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();
    var age = {};
    var ageString = "";
    var yearString = "";
    var monthString = "";
    var dayString = "";


    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    age = {
        years: yearAge,
        months: monthAge,
        days: dateAge
    };

    if (age.years > 1) yearString = " years";
    else yearString = " year";
    if (age.months > 1) monthString = " months";
    else monthString = " month";
    if (age.days > 1) dayString = " days";
    else dayString = " day";


    if ((age.years > 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else ageString = "Oops! Could not calculate age!";

    return ageString;
}

//This function is used for clear the data 
function clearTriage(){
	$("#textVisitId").val("");
	$("#txtPatientId").val("");
	$("#txtPatientName").val("");
	$("#txtGender").val("");
	$("#txtVisitDate").val("");
	$("#txt_year").val("");
	$("#txt_month").val("");
	$("#txt_day").val("");
	$("#txtSystolic").val("");
	$("#txtDiastolic").val("");
	
		
    $("#txtWeight").val("");
    $("#txtTemprature").val("");
    $("#txtHeight").val("");
    $("#txtPulse").val("");
    $("#txtBloodPressure").val("");
    $("#txtHeadCircumference").val("");
    $("#txtBodySurfaceArea").val("");
    $("#txtRespirationRate").val("");
    $("#txtOxygenSaturation").val("");
    $("#txtHeartRate").val("");
    $("#txtBMI").val("");
    $("#txtNotes").val("");
	

    $("#textVisitId").removeClass("errorStyle");
    $("#txtWeight").removeClass("errorStyle");
    $("#txtTemprature").removeClass("errorStyle");
    $("#txtHeight").removeClass("errorStyle");
    $("#txtPulse").removeClass("errorStyle");
    $("#txtBloodPressure").removeClass("errorStyle");
    $("#txtHeadCircumference").removeClass("errorStyle");
    $("#txtBodySurfaceArea").removeClass("errorStyle");
    $("#txtRespirationRate").removeClass("errorStyle");
    $("#txtOxygenSaturation").removeClass("errorStyle");
    $("#txtHeartRate").removeClass("errorStyle");

    $("#textVisitIdError").text("");
    $("#txtWeightError").text("");
    $("#txtTempratureError").text("");
    $("#txtHeightError").text("");
    $("#txtPulseError").text("");
    $("#txtBloodPressureError").text("");
    $("#txtHeadCircumferenceError").text("");
    $("#txtBodySurfaceAreaError").text("");
    $("#txtRespirationRateError").text("");
    $("#txtOxygenSaturationError").text("");
    $("#txtHeartRateError").text("");
    $("#textVisitIdError").text("");
	$("#user_reg_image").hide();
	$("#imagetext").hide();
	$("#imgPatient").attr("src",'./img/default.jpg');
}
//debugger src triage.js// JavaScript Document