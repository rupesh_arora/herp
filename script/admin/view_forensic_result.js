var tblViewForensicResult;
$(document).ready(function() {
	loader();
	debugger;
	$("#PrintForensicTest").addClass("hide");
	tblViewForensicResult = $('#viewForensicResultsTbl').dataTable( {
			"bFilter": true,
			"processing": true,
			"sPaginationType":"full_numbers",
			"fnDrawCallback": function ( oSettings ) {
			$('.Print').unbind();
			$('.Print').click(function(){
				var data=$(this).parents('tr')[0];
				var mData = tblViewForensicResult.fnGetData(data);
				if (null != mData)  // null if we clicked on title row
				{
					/*hospital information through global varibale in main.js*/
					$("#lblPrintEmail").text(hospitalEmail);
					$("#lblPrintPhone").text(hospitalPhoneNo);
					$("#lblPrintAddress").text(hospitalAddress);
					if (hospitalLogo != "null") {
						imagePath = "./images/" + hospitalLogo;
						$("#printLogo").attr("src",imagePath);
					}
					$("#printHospitalName").text(hospitalName);
					$('#lblPrintVisitId').text(mData[6]);								
					$('#lblPrintPatientId').text(mData[0]);
					$('#lblPrintPatient').text(mData[7]);
					$('#lblPrintTestName').text(mData[1]);
					$('#lblPrintNormalRange').text(mData[2]);
					$('#lblPrintResult').text(mData[3]);
					$('#lblPrintStatus').text(mData[8]);
					$("#lblPrintRemark").text(mData[4]);	 
					var visitDate = convertDate(mData[9]);
					$('#lblPrintVisitDate').text(visitDate);
					//$.print("#PrintForensicTest");
					window.print();

				}
			});
		}, 
		aoColumnDefs: [{
			'bSortable': false,
			'aTargets': [5]
		},{
			'bSortable': false,
			'aTargets': [4]
		}]	
	});
		
	// click on search button
	$("#btnSearch").on('click',function() {
		tblViewForensicResult.fnClearTable();
		var flag = "false";	 
		var visitId = $("#txtVisitId").val();
		var patientId = $("#txtPatientId").val();
		var patinetName = $("#txtPatientName").val();
		
		if(visitId != "") {
			if(visitId.length != 9){					
				flag = "true";
			}
			else{
				var visitPrefix = visitId.substring(0, 3);
				visitId = visitId.replace ( /[^\d.]/g, '' ); 
				visitId = parseInt(visitId);
			}
		}
		else{
			visitPrefix = "";
		}		

		if(patientId != ""){
			if(patientId.length != 9){					
				flag = "true";
			}
			else{
				var patientId = $("#txtPatientId").val();
				var patientPrefix = patientId.substring(0, 3);
				patientId = patientId.replace ( /[^\d.]/g, '' ); 
				patientId = parseInt(patientId);
			}
		}
		else{
			patientPrefix = "";
		}
		if (flag == "true") {
			return false;
		}
		// call ajax for search details
		 $.ajax({ // ajax call for show content
			type: "POST",
			cache: "false",
			url: "controllers/admin/forensic_test_result.php",
			datatype: "json",
			data: {
				visitId:visitId,
				patientId: patientId,
				patinetName: patinetName,
				"operation": "searchFroensicResult"
			},
			 success: function(data) {
				 var dataSet = jQuery.parseJSON(data); 
				 for(var i = 0; i<dataSet.length;i++) {
					var patientId = dataSet[i].patient_id;
					var patientPrefix = dataSet[i].prefix;						
					var patientIdLength = patientId.length;
					for (var j=0;j<6-patientIdLength;j++) {
						patientId = "0"+patientId;
					}
					patientid = patientPrefix+patientId;
					
					var visitId = dataSet[i].visit_id;
					var visitPrefix = dataSet[i].visitPrefix
					var visitIdLength = visitId.length;
					for (var k=0;k<6-visitIdLength;k++) {
						visitId = "0"+visitId;
					}
					visitid = visitPrefix+visitId;
					
					tblViewForensicResult.fnAddData([patientid,dataSet[i].name,dataSet[i].normal_range,dataSet[i].result,dataSet[i].report,"<i class='fa fa-print Print' title='Print' data-id="+dataSet[i].id+" data-original-title='Print'/>",
					visitid,dataSet[i].patient_name,dataSet[i].result_flag,dataSet[i].visit_date]);
				 }
			 },
			  error: function() {
			
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
			
		 });
	});

	// reset data functionality
	$("#btnReset").on('click',function(){
		clearDetails();
	});		
});

function clearDetails(){
	tblViewForensicResult.fnClearTable();
	$("#txtVisitId").val("");
	$("#txtPatientId").val("");
	$("#txtPatientName").val("");
}

//function to calculate date
function convertDate(o){
        var dbDateTimestamp = o;
        var dateObj = new Date(dbDateTimestamp * 1000);
        var yyyy = dateObj.getFullYear().toString();
        var mm = (dateObj.getMonth()+1).toString(); // getMonth() is zero-based
        var dd  = dateObj.getDate().toString();
        return yyyy +"-"+ (mm[1]?mm:"0"+mm[0]) +"-"+ (dd[1]?dd:"0"+dd[0]);
}