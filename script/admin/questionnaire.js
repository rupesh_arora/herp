var otable = null;
$(document).ready(function() {
    loader();
	var flag = "false";
	
	$('#oldHistoryModalDetails input[type=text]').addClass("as_label");
    $('#oldHistoryModalDetails input[type=text]').attr('readonly', true);
    $('#oldHistoryModalDetails input[type="button"]').addClass("btnHide");
    $('#oldHistoryModalDetails #questionnaireTbl input[type="button"]').addClass("btnHide");
    $('#oldHistoryModalDetails #divDropDownAllergyRequest').hide();

    /**DataTable Initialization**/
    otable = $('#questionnaireTbl').DataTable({
        "bPaginate": false,
        aoColumnDefs: [{'bSortable': false,'aTargets': [1] }],
		"fnDrawCallback" : function() {
			//function for make editable column of quantity in data table
		},
		"bAutoWidth" : false,
		aoColumnDefs: [
		   { aTargets: [ 1 ], bSortable: false },
           { aTargets: [ 2 ], bSortable: false },
           { aTargets: [ 3 ], bSortable: false }
		]			
    });
	
	if ($('#thing').val() ==3) {
        var visitId = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
		var patientId = parseInt($('#txtPatientID').val().replace ( /[^\d.]/g, '' ));		
    }
    if ($('#thing').val() ==4) {
        var visitId = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
		var patientId = parseInt($('#txtPatientID').val().replace ( /[^\d.]/g, '' ));
    }
    if ($('#thing').val() ==5) {
        var visitId = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
		var patientId = $('#txtPatientID').val();
    }
	
	
    oldDataRequest(visitId);/*Fetch data from db and insert in to table during page load*/ 

    //Add Lab Test click event
    $('#addQuestion').click(function() {

        $('#addQuestion').css('border', ''); //remove the red error border from add test button
        $('#noDataError').html(""); //remove the error message after the add button

		flag = "false";
		var rowcount = otable.fnGetData().length;
         var currentData = otable.fnAddData([rowcount+1,"<input type='text' class='currentTextBox' placeholder='Please enter Question'>","<input type='text' class='currentTextBox' placeholder='Please enter Answer'>","<input type='button' class='btn-remove' onclick='rowDelete($(this));' value='Remove'>"]);
		 
		 var getCurrDataRow = otable.fnSettings().aoData[ currentData ].nTr;
		 $(getCurrDataRow).find('td').css({"background-color":"#fff","color":"#000"});
			/*$($(getCurrDataRow).find('td')[1]).html("Please click for add Question"); // show text in data column 1
			$($(getCurrDataRow).find('td')[2]).html("Please click for add Answer");*/ // show text in data column 2
    });

    //Save button functionality
    $('#btnSaveQuestionnaire').click(function() {
		$(".close-modal").click();
		$('.modal-body').text("");
        var tableData = otable.fnGetData(); //fetching the datatable data
		var questionnaire = new Array(); 
		var bigquestionnaire =new Array(); 
		var rowcollection =  otable.$(".btn-remove");        
		for (i=0;i<rowcollection.length;i++) {
			questionnaire = new Array();
			var question = $(otable.$(".btn-remove")).parent().prev().prev().children().val();
			var answer = $(otable.$(".btn-remove")).parent().prev().children().val();
			if((question == "" || answer == "") || (question == "Please click for add Question" || answer == "Please click for add Answer")){
				$(".close-modal").click();
                $('#messageMyModalLabel').text("Alert");
                $('#messagemyModal .modal-body').text("Please enter question/answer.");
                $('#messagemyModal').modal();
                return false;
			}	
			else{				
                questionnaire.push(question,answer);
                bigquestionnaire.push(questionnaire);
			}
		}
		if (flag == "true") {
			return false;
		}
        //Checking whether datatable is empty or not
        if (tableData.length == 0) {

            $('#messagemyModal').modal();
            $('#messageMyModalLabel').text("Alert");
            $('#messagemyModal .modal-body').text("Please add questionnaire!!!");           

            return false;

        } else { //Saving the data       
			$(".close-modal").click();
			$('.modal-body').text("");
            $('#confirmUpdateModalLabel').text();
            $('#updateBody').text("Are you sure that you want to save this?");
            $('#confirmUpdateModal').modal();
            $("#btnConfirm").unbind();
            $("#btnConfirm").click(function(){
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: "controllers/admin/questionnaire.php",
                    datatype: "json",
                    data: {
                        'operation': 'save',
                        'bigquestionnaire': JSON.stringify(bigquestionnaire),
                        'patientId': patientId,
                        'visitId': visitId,
                        'tblName': 'patients_questionnaire'
                    },

                    success: function(data) {
                        if (data != "0" && data != "") {
    						/* flag = "true"; */
                            $('#messageMyModalLabel').text("Success");
                            $('#messagemyModal .modal-body').text("Questionnaire saved successfully!!!");
                            $('#messagemyModal').modal();
    						$('#questionnaireTbl input[type="button"]').addClass("btnHide");
    						$('#questionnaireTbl input[type="button"]').removeClass("btn-remove");
    						oldDataRequest(visitId);
                        } else {
							$(".close-modal").click();
    						$('#messageMyModalLabel').text("Alert");
    						$('#messagemyModal .modal-body').text("Please enter question/answer.");
    						$('#messagemyModal').modal();
                        }
                    },
                    error: function() {
						$(".close-modal").click();
                        $('#messageMyModalLabel').text("Error");
                        $('#messagemyModal .modal-body').text("Something awful happened!! Please try to contact admin.");
                        $('#messagemyModal').modal();
                    }
                });
            });
        }

    });

    //Reset button functionality
    $('#btnResetQuestionnaire').click(function() {
        otable.fnClearTable();
    });

});

/**Row Delete functionality**/
function rowDelete(currInst) {
    var row = currInst.closest("tr").get(0);
  /*   var oTable = $('#questionnaireTbl').dataTable(); */
    otable.fnDeleteRow(row);
}
function oldDataRequest(visitId){
    /* var visitId = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' )); */
    var postData = {
        "operation": "oldQuestionnaireSearch",
        "visit_id": visitId
    }
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/old_test_requests.php",
        datatype: "json",
        data: postData,

        success: function(data) {
            dataSet = JSON.parse(data);
            otable.fnClearTable();
            
            for(var i=0; i< dataSet.length; i++){
                var id = dataSet[i].id;
                var question = dataSet[i].question;
                var answer = dataSet[i].answer;
                var rowCount = otable.fnGetData().length;
                /*Apply here span to recognise previous or current data*/
                otable.fnAddData([rowCount+1,"<span data-previous='true'>"+question+"</span>","<span data-previous='true'>"+answer+"</span>","<input type='button' class ='btnDisabled' value='Remove' disabled>",id]);
            }
            otable.find('tbody').find('tr').find('td').css({"background-color":"#ccc"});
        }
    });
}