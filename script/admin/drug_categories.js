var drugCategoryTable;
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   drug_category.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages drug categories data
 *************************************************************************************************** */
    loader();
    debugger;
    
	$("#form_dept").hide();
	$("#drug_list").css({
		"background-color": "#fff"
	});
	$("#tab_drug").css({
		"background-color": "#0c71c8",
		"color": "#fff"
	});
	
	//This click function is used for change the tab
    $("#tab_add_drug").click(function() {
        $("#form_dept").show();
        $(".blackborder").hide();
        //to focus on a text box when div shown
        $("#txtCategories").focus();
		clear();
        $("#tab_add_drug").css({
           "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tab_drug").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#drug_list").css({
            "background": "#fff"
        });
		$("#txtCategoriesError").text("");
        $("#txtCategories").removeClass("errorStyle");
    });
	
		// import excel on submit click
	$('#importDataFile').click(function(){
		var flag = false;
		if($('#file').prop('files')[0] == undefined || $('#file').val() == "") {
			$('#txtFileError').show();
			$('#txtFileError').text("Please choose file.");
			flag  = true;
		}
		if(flag == true){
			return false;
		}
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to upload this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			var file_data = $('#file').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: 'controllers/admin/drug_categories.php', // point to server-side PHP script 
				dataType: 'text', // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					if(data != "" && data != "invalid file" && data == "1"){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Your file has been successfully imported!!!");
						$('#messagemyModal').modal();
						$('#file').val("");
					}
					if(data == "invalid file"){
						$('#txtFileError').show();
						$('#txtFileError').text("Please import only CSV/XLS file.");
						$('#file').val("");
					}
					else if (data =="You must have two column in your file i.e category and description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="First column should be category" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Second column should be description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Data can't be null in category column" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Please insert data in first column and second column only i.e A & B") {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
					else if(data == ""){
						$('#txtFileError').show();
						$('#txtFileError').text("Data already exist.");
						$('#file').val("");
					}
				},
				error: function() {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Data Not Found!!!");
					$('#messagemyModal').modal();

				}
			});
		});
	});
	
	//This click function is used for show the drug category list
    $("#tab_drug").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
        tabDrugCategoryList();//Calling this function for show the list 
    });
	
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		loadDataTable();	
    }
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	drugCategoryTable.fnClearTable();
	    	drugCategoryTable.fnDestroy();
	    	//Datatable code
			drugCategoryTable = $('#drugCategoriesTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"bAutoWidth":false,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  drugCategoryTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);//Calling this function for restore the data
						}						
					});
				},
				
				"sAjaxSource":"controllers/admin/drug_categories.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					{ "mData": "category" },
					{ "mData": "description" }, 
					{
					  "mData": function (o) { 
					   		return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; 
					   }		
					},	
				],
				aoColumnDefs: [
			        { 'bSortable': false, 'aTargets': [ 1,2 ] }
			    ]
			} );
		}
		else{
			drugCategoryTable.fnClearTable();
	    	drugCategoryTable.fnDestroy();
	    	//Datatable code
			loadDataTable();
		}
    });
	
	//This click function is used for validate and save the data on save button
    $("#btnSave").click(function() {
		
		var flag = false;
		
		$("#txtCategories").val($("#txtCategories").val().trim());
		
        if ($("#txtCategories").val() == '') {
            $("#txtCategoriesError").text("Please enter categories");
            $("#txtCategories").addClass("errorStyle");
            $("#txtCategories").focus();
            flag = true;
        }
		if(flag == true){			
			return false;
		}
		
		//ajax call
		
		var categoriesName = $("#txtCategories").val();
		var description = $("#txtDescription").val();
		description = description.replace(/'/g, "&#39");
		var postData = {
			"operation":"save",
			"categoriesName":categoriesName,
			"description":description
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/drug_categories.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Drug category saved successfully!!!");
					$('#messagemyModal').modal();
					$('#inactive-checkbox-tick').prop('checked', false).change();
						tabDrugCategoryList();
				}				
				else{
					$("#txtCategoriesError").text("Drug category is already exists");
					$("#txtCategories").addClass("errorStyle");
					$("#txtCategories").focus();
				}				
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});		
    });//Close save button

    //Keyup functionality
    $("#txtCategories").keyup(function() {
        if ($("#txtCategories").val() != '') {
            $("#txtCategoriesError").text("");
            $("#txtCategories").removeClass("errorStyle");
        } 
    });
	
	//Click function is used for reset button
    $("#btnReset").click(function(){
    	$("#txtCategories").focus();
    	clear();
    });
});//Close document.ready function

//This function will call on edit button
function editClick(id,category,description){	
	$('#myModalLabel').text("Update Drug Category");
	$('.modal-body').html($("#popUPFormDrug").html()).show();
    $('#myModal').modal('show');
	$('#myModal').on('shown.bs.modal', function() {
        $("#myModal #txtCategories").focus();
    });
	$("#myModal #uploadFile").hide();
	
	$("#myModal #btnUpdate").removeAttr("style");
	$("#myModal #txtCategoriesError").text("");
    $("#myModal #txtCategories").removeClass("errorStyle");
	$('#myModal #txtCategories').val(category);
	$('#myModal #txtDescription').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id);
	
	//Click function for update button
	$("#myModal #btnUpdate").click(function(){
		var flag = "false";
		$("#myModal #txtCategories").val($("#myModal #txtCategories").val().trim());
		if ($("#myModal #txtCategories").val() == '') {
			$("#myModal #txtCategoriesError").text("Please enter categories name");
			$("#myModal #txtCategories").addClass("errorStyle");
			flag = "true";
		}
		if(flag == "true"){			
			return false;
		}
		else{
			var categoriesName = $("#myModal #txtCategories").val();
			var description = $(" #myModal #txtDescription").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();	
			
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/drug_categories.php",
					data :{            
						"operation" : "update",
						"id" : id,
						"categoriesName":categoriesName,
						"description":description
					},
					success: function(data) {	
						if(data != "0" && data != ""){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Drug category updated successfully!!!");
							drugCategoryTable.fnReloadAjax();
							$('#messagemyModal').modal();
							clear();
						}
						else{
							$("#myModal #txtCategoriesError").text("Drug category is already exists");
							$("#myModal #txtCategories").addClass("errorStyle");
							$("#myModal #txtCategories").focus();
						}
					},
					error:function()
					{
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();			  
					}
				});
			});
		}
	});
	
	$('#myModal #txtCategories').keyup(function() {
        if ($("#myModal #txtCategories").val() != '') {
            $("#myModal #txtCategoriesError").text("");
            $("#myModal #txtCategories").removeClass("errorStyle");
        } 
    });
}

//Click function for delete the drugcategory
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete drug category");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteDrugCategory("'+type+'");');
}

//Click function for restore the drugcategory
function restoreClick(id){
	$('#selectedRow').val(id);
	$('.modal-body').text("");	
	$('#confirmMyModalLabel').text("Restore drug category");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();
	
	var type="restore";
	$('#confirm').attr('onclick','deleteDrugCategory("'+type+'");');
}
//function for delete the drugcategoryList
function deleteDrugCategory(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
			
		//Ajax call for delete the drugcategory 	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/drug_categories.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Drug category deleted successfully!!!");
					drugCategoryTable.fnReloadAjax();
					$('#messagemyModal').modal();
					 
				}
			},
			error:function()
			{
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
	
		//Ajax call for restore the drugcategory 
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/drug_categories.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Drug category restored successfully!!!");
					drugCategoryTable.fnReloadAjax();
					$('#messagemyModal').modal();
					 clear();
				}			
			},
			error:function()
			{
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	}
}

//This function is used for show the list of drug category
function tabDrugCategoryList(){
	$("#form_dept").hide();
	$(".blackborder").show();
	$("#tab_add_drug").css({
		"background": "#fff",
		"color": "#000"
	});
	$("#tab_drug").css({
		"background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
		"color": "#fff"
	});
	$("#drug_list").css({
		"background": "#fff"
	});
	clear();
}

// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		 $(".close").click();
    }
});

//This function is used for clear the data
function clear(){
	$('#txtCategories').val("");
	$('#txtCategoriesError').text("");
	$('#txtFileError').text('')
	$('#txtDescription').val("");
	$('#txtCategories').removeClass("errorStyle");
	$("#txtCategories").removeClass("errorStyle");
	$("#myModal #txtCategories").removeClass("errorStyle");
}

function loadDataTable(){
	drugCategoryTable = $('#drugCategoriesTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"bAutoWidth":false,
		"sPaginationType":"full_numbers",
		"fnDrawCallback": function ( oSettings ) {	
			$('.update').unbind();				
			$('.update').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData = drugCategoryTable.fnGetData(data);
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					var category = mData["category"];
					var description = mData["description"];
					editClick(id,category,description); //Calling this function for update the data 
				}
			});
			$('.delete').unbind();
			$('.delete').on('click',function(){
				var data=$(this).parents('tr')[0];
				var mData =  drugCategoryTable.fnGetData(data);
				
				if (null != mData)  // null if we clicked on title row
				{
					var id = mData["id"];
					deleteClick(id);//Calling this function for delete the data 
				}
			});	
		},
		
		"sAjaxSource":"controllers/admin/drug_categories.php",
		"fnServerParams": function ( aoData ) {
		  aoData.push( { "name": "operation", "value": "show" });
		},
		"aoColumns": [
			{ "mData": "category" },
			{ "mData": "description" }, 
			{
				"mData": function (o) { 
				return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
				   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
				   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
				   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
				   "  data-original-title='Delete'></i>"; 
				}	
			},	
		],
		aoColumnDefs: [
			{ 'bSortable': false, 'aTargets': [ 1,2 ] }
		]
	} );
}
//# sourceURL=filename.js