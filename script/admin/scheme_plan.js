 var schemePlanTable;
$(document).ready(function() {
	debugger;
    bindInsuranceCompany(); //bind company
	bindCopay(); //bind copay
    $(".input-datepicker").datepicker({ // date picker function
        autoclose: true
    });
	$(".amountPrefix").text(amountPrefix);
    $("#formSchemePlan").hide();
    $("#divSchemePlan").addClass('list');    
    $("#tabSchemePlanList").addClass('tab-list-add');
   
    $("#tabSchemePlan").click(function() { // show the add allergies categories tab
    	showAddTab();
        clear();
    });

    $("#tabSchemePlanList").click(function() { // show allergies list tab
    	showTableList();		
    });

    // save details with validation
    $("#txtValueBox").prop('disabled', true);
    $("#txtValue").prop('disabled', false);
    $("#selCopay").change(function() {
        if($("#selCopay").val() != ''){
            $("#selCopayError").text("");
        }
    });

    removeErrorMessage();// remove error messaages


    var startDate = new Date();
    $("#txtStartDate").datepicker('setEndDate',startDate);
    $('#txtEndDate').datepicker('setStartDate', startDate);

    $("#txtStartDate").click(function() {
        $("#txtStartDateError").text("");
    });
    $("#txtEndDate").click(function() {
        $("#txtEndDateError").text("");
    });

    $('#selCopay').change(function() {
        if($('#selCopay').val() != ''){
            $('#txtValueError').text("");
            $('#txtValue').removeClass('errorStyle');
        }
    });
    // save scheme plan
    $("#btnAddSchemePlan").click(function() {

        var flag = false;

        if(validTextField('#txtValue','#txtValueError','Please enter value') == true)
        {
            flag = true;
        }
        if(validTextField('#selCopay','#selCopayError','Please select copay') == true)
        {
            flag = true;
        }
        if ($("#txtEndDate").val() == '') {
            $("#txtEndDateError").text("Please select end date");
            $("#txtEndDate").addClass("errorStyle");
            flag = true;
        }
        if ($("#txtStartDate").val() == '') {
            $("#txtStartDateError").text("Please select start date");
            $("#txtStartDate").addClass("errorStyle");
            flag = true;
        }
        
        if(validTextField('#txtPlanName','#txtPlanNameError','Please enter plan name') == true)
        {
            flag = true;
        }
        if(validTextField('#selSchemeName','#selSchemeNameError','Please select scheme name') == true)
        {
            flag = true;
        }
        if(validTextField('#selInsuaracnceCompany','#selInsuaracnceCompanyError','Please select company name') == true)
        {
            flag = true;
        }
        
		if(flag == true) {
			return false;
		}	
        if($("#selCopay option:selected").text() == "Percentage")
        {
            if($('#txtValue').val() > 100){
                $('#txtValue').val('');
                $('#txtValue').addClass("errorStyle");
                $('#txtValueError').text('Not a valid %');
                return false;
            }
        }
        
        var insuranceCompany = $("#selInsuaracnceCompany").val();
        var schemeName = $('#selSchemeName').val();
        var planName = $("#txtPlanName").val();
        var description = $("#txtDesc").val();
        var StartDate = $("#txtStartDate").val();
        var EndDate = $("#txtEndDate").val();
        var copay = $("#selCopay").val();
        var value = $("#txtValue").val();


        var postData = {
            insuranceCompany:insuranceCompany,
            schemeName:schemeName,
            planName:planName,
            description:description,
            StartDate:StartDate,
            EndDate:EndDate,
            copay:copay,
            value:value,
            "operation":"saveSchemePlan"
        }

        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/scheme_plan.php",
            datatype:"json",
            data: postData,
            success: function(data){
                if(data !='' && data!=null && data!="0"){
                    callSuccessPopUp("Success","Saved sucessfully!!!");
                    showTableList();
                    $("#txtStartDate").datepicker('setEndDate',startDate);
                    $('#txtEndDate').datepicker('setStartDate', startDate);
                }
                else{
                    $("#txtPlanNameError").text("Plan name is already exist");
                    $("#txtPlanName").focus();
                }
            },
            error:function(){

            }
        });

	});
// bind scheme type from insurance company type
    $("#selInsuaracnceCompany").change(function(){
        var insuranceCompany = $('#selInsuaracnceCompany').val();
        if(insuranceCompany != ''){
            bindSchemeName(insuranceCompany,'');
        }
        else{
            $("#selSchemeName").html("<option value=''>--Select--</option>");
        }
    });

    //add signs of Rs.,% for for value on click of copay type
    $('#selCopay').change(function(){
        var copayType = $("#selCopay option:selected").attr('data-value');

       var copay = $("#selCopay option:selected").text();
        if(copayType == "Percentage"){
            $('#txtValue').val('');
            $('#txtValue').val();
            $('#txtValueBox').val("%");
            $("#txtValue").prop('disabled', false);
        }
        else if(copayType == "Amount Prefix" && copay != "None"){
            $('#txtValue').val('');
            $('#txtValue').val();
            $('#txtValueBox').val(amountPrefix);
            $("#txtValue").prop('disabled', false);
        }
        else{
            $('#txtValue').val('');
            $('#txtValue').val('0');
            $('#txtValueBox').val(amountPrefix);
            $("#txtValue").prop('disabled', true);
        }
    });

    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code
        schemePlanTable = $('#schemePlanTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "autoWidth": false,
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = schemePlanTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var insuranceCompany = mData["company_name"];
                        var insuranceCompanyId = mData["insurance_company_id"];
                        var schemeName = mData["scheme_name"];
                        var planName = mData["plan_name"];
                        var insurancePlanId = mData["insurance_plan_id"];                        
                        var description = mData["description"];
                        var StartDate = mData["start_date"];
                        var EndDate = mData["end_date"];
                        var copay = mData["copay"];
                        var value = mData["value"];
                        editClick(id, insuranceCompany,schemeName,planName,description,insuranceCompanyId,insurancePlanId,StartDate,EndDate,copay,value);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = schemePlanTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/scheme_plan.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "company_name"
                }, {
                    "mData": "scheme_name"
                }, {
                    "mData": "plan_name"
                }, {
                    "mData": "start_date"
                }, {
                    "mData": "end_date"
                }, {
                    "mData": function(o) {
                        var value = o["value"];
                        var copayValue = o["copay_value"];
                        if(copayValue == "Amount Prefix") {
                          value = amountPrefix + value;
                          return value;  
                        }
                        else {
                          value = value + "%";
                          return value;  
                        } 
                        
                    }
                }, {
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [6]
            }, {
                'bSortable': false,
                'aTargets': [7]
            }]

        });
    }
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            schemePlanTable.fnClearTable();
            schemePlanTable.fnDestroy();
            schemePlanTable = "";
            schemePlanTable = $('#schemePlanTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "autoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = schemePlanTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var insuranceCompanyId = mData["company_id"];
                            var schemeName = mData["insurance_plan_id"];
                            var planName = mData["plan_name"];
                            restoreClick(id,insuranceCompanyId,schemeName,planName);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/scheme_plan.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    {
                    "mData": "company_name"
                }, {
                    "mData": "scheme_name"
                }, {
                    "mData": "plan_name"
                }, {
                    "mData": "start_date"
                }, {
                    "mData": "end_date"
                },  {
                    "mData": function(o) { //show Rs,% sign on datable
                        var value = o["value"];
                        var copayValue = o["copay_value"];
                        if(copayValue == "Amount Prefix") {
                          value = amountPrefix + value;
                          return value;  
                        }
                        else {
                          value = value + "%";
                          return value;  
                        } 
                        
                    }
                },  {
                    "mData": "description"
                }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [6]
                }, {
                    'bSortable': false,
                    'aTargets': [7]
                }]
            });
        } else { // show active data on unchecked   
            schemePlanTable.fnClearTable();
            schemePlanTable.fnDestroy();
            schemePlanTable = "";
            schemePlanTable = $('#schemePlanTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "autoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = schemePlanTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                        var insuranceCompany = mData["company_name"];
                        var insuranceCompanyId = mData["insurance_company_id"];
                        var schemeName = mData["scheme_name"];
                        var planName = mData["plan_name"];
                        var insurancePlanId = mData["insurance_plan_id"];                        
                        var description = mData["description"];
                        var StartDate = mData["start_date"];
                        var EndDate = mData["end_date"];
                        var copay = mData["copay"];
                        var value = mData["value"];
                        editClick(id, insuranceCompany,schemeName,planName,description,insuranceCompanyId,insurancePlanId,StartDate,EndDate,copay,value);

                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = schemePlanTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/scheme_plan.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "company_name"
                    }, {
                        "mData": "scheme_name"
                    }, {
                        "mData": "plan_name"
                    }, {
                        "mData": "start_date"
                    }, {
                        "mData": "end_date"
                    },{ "mData": function(o) { //shoe Rs,% sign on datable 
                            var value = o["value"];
                            var copayValue = o["copay_value"];
                            if(copayValue == "Amount Prefix") {
                              value = amountPrefix + value;
                              return value;  
                            }
                            else {
                              value = value + "%";
                              return value;  
                            } 
                        }
                    },  {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [6]
                }, {
                    'bSortable': false,
                    'aTargets': [7]
                }]
            });
        }
    });

    $("#btnReset").click(function() {
        $("#selInsuaracnceCompany").focus();
        clear();
    });
});

 function clear() {
    $('#selInsuaracnceCompany').val('');
    $('#selInsuaracnceCompany').removeClass('errorStyle');
    $('#selInsuaracnceCompanyError').text('');
    $('#selSchemeName').val('');
    $('#selSchemeName').removeClass('errorStyle');
    $('#selSchemeNameError').text('');
    $('#txtPlanName').val('');
    $('#txtPlanName').removeClass('errorStyle');
    $('#txtPlanNameError').text('');
    $('#txtStartDate').val('');
    $('#txtStartDate').removeClass('errorStyle');
    $('#txtStartDateError').text('');
    $('#txtEndDate').val('');
    $('#txtEndDate').removeClass('errorStyle');
    $('#txtEndDateError').text('');
    $('#selCopay').val('');
    $('#selCopay').removeClass('errorStyle');
    $('#selCopayError').text('');
    $('#txtValue').val('');
    $('#txtValue').removeClass('errorStyle');
    $('#txtValueError').text('');
    $('#txtDesc').val('');

 }
 function editClick(id, insuranceCompany,schemeName,planName,description,insuranceCompanyId,insurancePlanId,StartDate,EndDate,copay,value) {
     showAddTab();
    $('#tabSchemePlan').html("+Update Scheme Plan");
    $("#btnReset").hide();
    $("#btnAddSchemePlan").hide();

   
    $("#selInsuaracnceCompany").change(function(){
        var insuranceCompany = $('#selInsuaracnceCompany').val();
        if(insuranceCompany != ''){
            bindSchemeName(insuranceCompany,'');
        }
        else{
            $("#selSchemeName").html("<option value=''>--Select--</option>");
        }
    });
    /*$('#myModal').on('shown.bs.modal', function() {
        var copayValue = $("#myModal #selCopay option:selected").text();
        if(copayValue == 'None'){
            $("#myModal #txtValueBox").val(amountPrefix);
            $("#myModal #txtValue").prop('disabled', true);
        }
        else if(copayValue == 'Fixed'){
            $("#myModal #txtValueBox").val(amountPrefix);
            $("#myModal #txtValue").prop('disabled', false);
        }
        else if(copayValue == 'Percentage'){
            $("#myModal #txtValueBox").val('%');
            $("#myModal #txtValue").prop('disabled', false);
        }
    });*/


    $('#txtValueBox').val(amountPrefix)
    $('#selCopay').change(function(){
        var copayType = $("#selCopay option:selected").attr('data-value');

       var copay = $("#selCopay option:selected").text();
        if(copayType == "Percentage"){
            $('#txtValue').val('');
            $('#txtValue').val();
            $('#txtValueBox').val("%");
            $("#txtValue").prop('disabled', false);
        }
        else if(copayType == "Amount Prefix" && copay != "None"){
            $('#txtValue').val('');
            $('#txtValue').val();
            $('#txtValueBox').val(amountPrefix);
            $("#txtValue").prop('disabled', false);
        }
        else{
            $('#txtValue').val('');
            $('#txtValue').val('0');
            $('#txtValueBox').val(amountPrefix);
            $("#txtValue").prop('disabled', true);
        }
    });

    var startDate = new Date();
    $("#txtStartDate").datepicker('setEndDate',startDate);
    $('#txtEndDate').datepicker('setStartDate', startDate);
    $("#txtStartDate,#txtEndDate").click(function(){
        $(".datepicker").css({"z-index":"99999"});
    });

    $('#myModal').on('shown.bs.modal', function() {
        $("#myModal #selInsuaracnceCompany").focus();
    });
    $("#btnUpdate").removeAttr("style");
    
    $("#selInsuaracnceCompany").removeClass("errorStyle");
    $("#selInsuaracnceCompanyError").text("");
    $("#selSchemeName").removeClass("errorStyle");
    $("#selSchemeNameError").text("");
    $("#txtPlanName").removeClass("errorStyle");
    $("#txtPlanNameError").text("");
    $("#txtStartDate").removeClass("errorStyle");
    $("#txtStartDateError").text("");
    $("#txtEndDate").removeClass("errorStyle");
    $("#txtEndDateError").text("");
    $("#selCopay").removeClass("errorStyle");
    $("#selCopayError").text("");
    $("#txtValue").removeClass("errorStyle");
    $("#txtValueError").text("");
    
    $('#selInsuaracnceCompany').val(insuranceCompanyId);

    bindSchemeName(insuranceCompanyId,insurancePlanId);

    $('#txtPlanName').val(planName);
    $('#txtDesc').val(description.replace(/&#39/g, "'"));
    $('#txtStartDate').val(StartDate);
    $('#txtEndDate').val(EndDate);
    $('#selCopay').val(copay);
    $('#txtValue').val(value);
    $('#selectedRow').val(id);
    //validation
   
    
    $("#btnUpdate").click(function() { // click update button
        var flag = "false";
        if ($("#txtValue").val()== '') {
            $("#txtValueError").text("Please enter value");
            $("#txtValue").focus();
            $("#txtValue").addClass("errorStyle");
            flag = "true";
        }
        if ($("#selCopay").val() == '') {
            $("#selCopayError").text("Please select copay");
            $("#selCopay").focus();
            $("#selCopay").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtEndDate").val() == '') {
            $("#txtEndDateError").text("Please select end date");
            $("#txtEndDate").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtStartDate").val() == '') {
            $("#txtStartDate").text("Please select start date");
            $("#txtStartDate").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtPlanName").val() == '') {
            $("#txtPlanNameError").text("Please enter plan name");
            $("#txtPlanName").focus();
            $("#txtPlanName").addClass("errorStyle");
            flag = "true";
        }
        if ($("#selSchemeName").val() == '') {
            $("#selSchemeNameError").text("Please select scheme name");
            $("#selSchemeName").focus();
            $("#selSchemeName").addClass("errorStyle");
            flag = "true";
        }
        if ($("#selInsuaracnceCompany").val() == '') {
            $("#selInsuaracnceCompanyError").text("Please select company name");
            $("#selInsuaracnceCompany").focus();
            $("#selInsuaracnceCompany").addClass("errorStyle");
            flag = "true";
        }  
        if(flag == "true") {
            return false;
        }
        
        if($("#selCopay option:selected").text() == "Percentage")
        {
            if($('#txtValue').val() > 100){
                $('#txtValue').val('');
                $('#txtValue').addClass("errorStyle");
                $('#txtValueError').text('Not a valid %');
                return false;
            }
        }

        var insuranceCompany = $("#selInsuaracnceCompany").val();
        var schemeName = $("#selSchemeName").val();
        var planName = $("#txtPlanName").val();
        var description = $("#txtDesc").val();
        description = description.replace(/'/g, "&#39");
        var StartDate = $("#txtStartDate").val();
        var EndDate = $("#txtEndDate").val();
        var copay = $("#selCopay").val();
        var value = $("#txtValue").val();
        var id = $('#selectedRow').val();
        
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
        var postData = {
            "operation": "update",
            "insuranceCompany": insuranceCompany,
            "schemeName": schemeName,
            "planName": planName,
            "description": description,
            "StartDate":StartDate,
            "EndDate":EndDate,
            "copay":copay,
            "value":value,
            "id": id
        }
        $.ajax( //ajax call for update data
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/scheme_plan.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('#myModal').modal('hide');
                        $('.close-confirm').click();
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Scheme plan updated successfully!!!");
                        $('#messagemyModal').modal();
                        showTableList();
                    }
                    else {                
                        $("#txtPlanNameError").text("Plan name is already exist");
                        $("#txtPlanName").focus();               
                        $("#txtPlanName").addClass('errorStyle');
                    }
                },
                error: function() {
                    $('.close-confirm').click();
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                    $('#messagemyModal').modal();
                }
            }); // end of ajax
        });
    });
} // end update button

function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Scheme Plan");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteSchemePlan("' + type + '","","","");');
} // end click fucntion

function restoreClick(id,insuranceCompanyId,schemeName,planName) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Scheme Plan");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteSchemePlan("' + type + '","'+insuranceCompanyId+'","'+schemeName+'","'+planName+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteSchemePlan(type,insuranceCompanyId,schemeName,planName) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/scheme_plan.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Scheme plan deleted successfully!!!");
                    $('#messagemyModal').modal();
                    schemePlanTable.fnReloadAjax();
                } 
                else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This scheme plan is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/scheme_plan.php",
            data: {
                "operation": "restore",
                "insuranceCompanyId":insuranceCompanyId,
                "schemeName":schemeName,
                "planName":planName,
                "id": id
            },
            success: function(data) {
                if (data == "1" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Scheme plan restored successfully!!!");
                    $('#messagemyModal').modal();
                    schemePlanTable.fnReloadAjax();
                }
                else {
                    callSuccessPopUp('Alert','This scheme plan already exist.So you can\'t restore it.!!!');
                }
            },
            error: function() {             
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary unavailable to respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}
function bindCopay() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/scheme_plan.php",
        data: {
            "operation": "showCopay"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option data-value='" + parseData[i].value + "' value='" + parseData[i].id + "'>" + parseData[i].copay_type + "</option>";
                }
                $('#selCopay').html(option);
            }
        },
        error: function() {}
    });
}

function showTableList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#formSchemePlan").hide();
    $(".blackborder").show();

    $("#tabSchemePlan").removeClass('tab-detail-add');
    $("#tabSchemePlan").addClass('tab-detail-remove');
    $("#tabSchemePlanList").removeClass('tab-list-remove');    
    $("#tabSchemePlanList").addClass('tab-list-add');
    $("#divSchemePlan").addClass('list');  

    $("#btnReset").show();
    $("#btnAddSchemePlan").show();
    $('#btnUpdate').hide();
    $('#tabSchemePlan').html("+Add Scheme Plan");
    clear();
}

function showAddTab(){
    $("#formSchemePlan").show();
    $(".blackborder").hide();
   
    $("#tabSchemePlan").addClass('tab-detail-add');
    $("#tabSchemePlan").removeClass('tab-detail-remove');
    $("#tabSchemePlanList").removeClass('tab-list-add');
    $("#tabSchemePlanList").addClass('tab-list-remove');
    $("#divSchemePlan").addClass('list');    
    $("#selInsuaracnceCompany").focus();
	$("#txtValueBox").val(amountPrefix);    
    removeErrorMessage();
}
