var leavePeriodTable; //defing a global variable for datatable
$(document).ready(function() {
	loader();    
    debugger;
    $("#txtStartDate").datepicker({autoclose: true}) .on('changeDate', function(selected){
        var d = selected.date;
        d.setFullYear(d.getFullYear() + 1);
        d.setDate(d.getDate()-1);
        $('#txtEndDate').datepicker('setDate', d);
        startDate = new Date(d);
         $('#txtEndDate').datepicker('setStartDate', startDate);
    }); 
    $("#txtEndDate").datepicker();

    removeErrorMessage();

    $("#form_dept").hide();
    $("#designation_list").addClass('list');    
    $("#tab_dept").addClass('tab-list-add');

    $("#tab_add_dept").click(function() { // click on add designation tab
         showAddTab();
        clearFormDetails(".clearForm"); 
    });
    $("#tab_dept").click(function() { // click on list designation tab
		showTableList();
    });

    $("#btnSaveLeavePeriod").click(function() {
    	var flag = false;
        if($('#txtEndDate').val() == '') 
        {
            $('#txtEndDateError').text('Please enter end date');
            $('#txtEndDate').addClass('errorStyle');
            flag = true;
        }
        if($('#txtStartDate').val() == '') 
        {
            $('#txtStartDateError').text('Please enter start date');
            $('#txtStartDate').addClass('errorStyle');
            flag = true;
        }
         /*Check radio button checked or not*/
        if ($('#chkActive').is(":checked")==false) {
            $('#chkActive').val(0);
        }
        else{
            $('#chkActive').val(1);
        }
		if (flag == true) {
			return false;
		}
		var startDate = $("#txtStartDate").val();
        var endDate = $("#txtEndDate").val();
        var availability = $('#chkActive').val();
        var description = $("#txtDescription").val().trim();
        description = description.replace(/'/g, "&#39");
        var postData = {
            "operation": "saveLeavePeriod",
            "startDate": startDate,
            "endDate": endDate,
            "availability": availability,
            "description" : description
        }        
        dataCall("controllers/admin/holidays.php", postData, function (result){
        	if (result.trim() == 1) {
        		callSuccessPopUp('Success','Saved sucessfully!!!');
        		showTableList();
        	}
        	else{
        		callSuccessPopUp('Error','Try to refresh page once');
        		clearFormDetails(".clearForm");
        	}
        });
    });

    //load Datatable data on page load
    if ($('.inactive-checkbox').not(':checked')) {
        leavePeriodTable = $('#leavePeriodTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "fnDrawCallback": function(oSettings) {

				$('.update').unbind();
                $('.update').on('click', function() { // update click event 
                    var data = $(this).parents('tr')[0];
                    var mData = leavePeriodTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var fromDate = mData["from_date"];
                        var toDate = mData["to_date"];
                        var period_status = mData["period_status"];
                        var description = mData["description"];
                        editClick(id, fromDate, toDate, description, period_status);

                    }
                });
				$('.delete').unbind();
                $('.delete').on('click', function() { // delete click event
                    var data = $(this).parents('tr')[0];
                    var mData = leavePeriodTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/holidays.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "showLeavePeriod"
                });
            },
            "aoColumns": [{
                "mData": function (o) { 
                    var period = o["from_date"] +"-"+o["to_date"];
                    return period;
                }
            }, {
                "mData": "description"
            }, {
                "mData": function(o) {
                    var data = o;
                    return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                        " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                        " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                        " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                        "  data-original-title='Delete'></i>";
                }
            }, ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [1,2]
            }]
        });
    }
    //checked or not checked functionality to show active or inactive data
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) {
            leavePeriodTable.fnClearTable();
            leavePeriodTable.fnDestroy();
            leavePeriodTable = "";
            leavePeriodTable = $('#leavePeriodTable').dataTable({
                "bFilter": true,
                "processing": true,
				
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
					$('.restore').unbind();
                    $('.restore').on('click', function() { // restor click event
                        var data = $(this).parents('tr')[0];
                        var mData = leavePeriodTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            restoreClick(id);
                        }
                    });
                },
                "sAjaxSource": "controllers/admin/holidays.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checkedLeavePeriod"
                    });
                },
                "aoColumns": [{
                    "mData": function (o) { 
                        var period = o["from_date"] +"-"+o["to_date"];
                        return period;
                    }
                },{
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                    }
                }, ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1,2]
                }]
            });
        } else {
            leavePeriodTable.fnClearTable();
            leavePeriodTable.fnDestroy();
            leavePeriodTable = "";
            leavePeriodTable = $('#leavePeriodTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
					$('.update').unbind();
                    $('.update').on('click', function() { // for update click event
                        var data = $(this).parents('tr')[0];
                        var mData = leavePeriodTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var fromDate = mData["from_date"];
                            var toDate = mData["to_date"];
                            var period_status = mData["period_status"];
                            var description = mData["description"];
                            editClick(id, fromDate, toDate, description, period_status);
                        }
                    });
					$('.delete').unbind();
                    $('.delete').on('click', function() { // for delete click event
                        var data = $(this).parents('tr')[0];
                        var mData = leavePeriodTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },
                "sAjaxSource": "controllers/admin/holidays.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "showLeavePeriod"
                    });
                },
                "aoColumns": [{
                   "mData": function (o) { 
                        var period = o["from_date"] +"-"+o["to_date"];
                        return period;
                    }
                },{
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;

                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            "  data-original-title='Delete'></i>";
                    }
                }, ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1,2]
                }]
            });
        }
    });


    $("#btnReset").click(function(){
	    clearFormDetails(".clearForm");
    });
});

// use fucntion for edit designation for update 
function editClick(id, fromTime, toTime, description, period_status) {	
   showAddTab();
    $('#tab_add_dept').html("+Update Fiscal Year");
    $("#uploadFile").hide();
    $("#btnReset").hide();
    $("#btnAddDesignation").hide();
    $('#btnUpdateLeavePeriod').removeAttr("style");
    $("#txtEndDateError").text("");
    $("#txtStartDate").removeClass("errorStyle");
    $('#txtStartDate').val(fromTime);
    $("#txtEndDate").removeClass("errorStyle");
    $("#txtStartDateError").text("");
    $('#txtEndDate').val(toTime);
    $('#txtDescription').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
    $(".btnContainer").hide();

    $("#lblActive").click(function(){
        if ($('#chkActive').is(":checked") == true) {
            $("#chkActive").prop('checked', false);
        }
        else{
            $("#chkActive").prop('checked', true);
        }
        
    });

    if (period_status == 0) {
        $('#chkActive').prop('checked', false);
    }
    else{
        $('#chkActive').prop('checked', true);
    }

    //keyup
    removeErrorMessage();
     $("#txtStartDate").datepicker({autoclose: true}) .on('changeDate', function(selected){
        var d = selected.date;
        d.setFullYear(d.getFullYear() + 1);
        d.setDate(d.getDate()-1);
        $('#txtEndDate').datepicker('setDate', d);
        startDate = new Date(d);
        $('#txtEndDate').datepicker('setStartDate', startDate);
    }); 
    $("#txtEndDate").datepicker();
    $("#txtStartDate,#txtEndDate").click(function(){
		$(".datepicker").css({"z-index":"99999"});
	});
    $("#btnUpdateLeavePeriod").click(function() {
        var flag = "false";
        if(validTextField('#txtStartDate','#txtStartDateError','Please enter start date') == true)
		{
			flag = true;
		}
		if(validTextField('#txtStartDate','#txtStartDateError','Please enter start date') == true)
		{
			flag = true;
		}
        /*Check radio button checked or not*/
        if ($('#chkActive').is(":checked")==false) {
            $('#chkActive').val(0);
        }
        else{
            $('#chkActive').val(1);
        }
        if (flag == "true") {
            return false;
        }
		else{
			var startDate = $("#txtStartDate").val();
	        var endDate = $("#txtEndDate").val();
	        //var completeDate = startDate+' - '+endDate;
            var availability = $('#chkActive').val();
			var description = $("#txtDescription").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();
			var postData = {
	            "operation": "updateLeavePeriod",
                "startDate": startDate,
                "endDate": endDate,
	            "availability": availability,
	            "description" : description,
	            "id" : id
	        }  
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				dataCall("controllers/admin/holidays.php", postData, function (result){
					if (result != null && result != "") {
						$("#myModal .close-modal").click();
						callSuccessPopUp('Success','Updated sucessfully!!!');
        				clearFormDetails(".clearForm");
        				showTableList();
					}
				});
			});
		}
    });
}
// click function to delete designation
function deleteClick(id) {
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Delete holiday");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    var type = "delete";
    $('#confirm').attr('onclick', 'deletePeriod("' + type + '");');
}

//Click function for restore the departement
function restoreClick(id) {
    $('#selectedRow').val(id);
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Restore holiday");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deletePeriod("' + type + '");');
}
//function for delete and restore the designationtList
function deletePeriod(type) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "deleteLeavePeriod",
            "id" : id
        }
        dataCall("controllers/admin/holidays.php", postData, function (result){
			if (result != null && result != "") {
				callSuccessPopUp('Success','Leave period deleted sucessfully!!!');
				leavePeriodTable.fnReloadAjax();
			}
			else {
                $('.modal-body').text("");
                callSuccessPopUp('Sorry','This leave type is in used , so you can not delete it!!!');
            }
		});         
    } 
    else {
        var id = $('#selectedRow').val();
        $('.modal-body').text("");
        var postData = {
            "operation": "restoreLeavePeriod",
            "id" : id
        }
        dataCall("controllers/admin/holidays.php", postData, function (result){
			if (result != null && result != "") {
				callSuccessPopUp('Success','Leave period restored sucessfully!!!');
				leavePeriodTable.fnReloadAjax();
			}
		});        
    }
}
function showTableList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#form_dept").hide();
    $(".blackborder").show();

    $("#tab_add_dept").removeClass('tab-detail-add');
    $("#tab_add_dept").addClass('tab-detail-remove');
    $("#tab_dept").removeClass('tab-list-remove');    
    $("#tab_dept").addClass('tab-list-add');
    $("#department_list").addClass('list');    
    $("#btnReset").show();
    $("#btnAddDesignation").show();
    $('#btnUpdateLeavePeriod').hide();
    $('#tab_add_dept').html("+Add Fiscal Year");
    clear();
}

function showAddTab(){
    $("#form_dept").show();
    $(".blackborder").hide();
   
    $("#tab_add_dept").addClass('tab-detail-add');
    $("#tab_add_dept").removeClass('tab-detail-remove');
    $("#tab_dept").removeClass('tab-list-add');
    $("#tab_dept").addClass('tab-list-remove');
    $("#designation_list").addClass('list');
    removeErrorMessage();
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});