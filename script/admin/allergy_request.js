var otable = null;
$(document).ready(function() {
    debugger;
/* ****************************************************************************************************
* File Name    :   allergy_request.js
* Company Name :   Qexon Infotech
* Created By   :   Rupesh Arora
* Created Date :   31th dec, 2015
* Description  :   This page  manages alergy test in consultant module
*************************************************************************************************** */

    loader();
	var check = "";
	$('#oldHistoryModalDetails input[type=text]').addClass("as_label");
    $('#oldHistoryModalDetails input[type=text]').attr('readonly', true);
    $('#oldHistoryModalDetails input[type="button"]').addClass("btnHide");
    $('#oldHistoryModalDetails #allergyRequestsTbl input[type="button"]').addClass("btnHide");
    $('#oldHistoryModalDetails #divDropDownAllergyRequest').hide();

    bindAllergyCategory(); //Loading the Lab type dropdown at the start

    /**DataTable Initialization**/
    otable = $('#allergyRequestsTbl').DataTable({
        "bPaginate": false,
        aoColumnDefs: [{'bSortable': false,'aTargets': [2] }]
    });
	
    if ($('#thing').val() ==3) {
        var visitId = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
    }
    if ($('#thing').val() ==4) {
        var visitId = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
    }
    if ($('#thing').val() ==5) {
        var visitId = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
    }

    allergy(visitId);
	/*Fetch data from db and insert in to table during page load*/
    function allergy(visitId){
		var postData = {
        "operation": "oldAllergySearch",
        "visit_id": visitId
		}
		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/old_test_requests.php",
			datatype: "json",
			data: postData,

			success: function(data) {
				dataSet = JSON.parse(data);
				otable.fnClearTable();
				
				for(var i=0; i< dataSet.length; i++){
					var id = dataSet[i].id;
					var code = dataSet[i].code;
					var name = dataSet[i].name;

					/*Apply here span to recognise previous or current data*/
					otable.fnAddData(["<span data-previous='true'>"+name+"</span>",code, "<input type='button' class ='btnDisabled' value='Remove' disabled>",id]);
				}
				otable.find('tbody').find('tr').find('td').css({"background-color":"#ccc"});				
			}
		});
	}	

    /*On selecting the lab type drop down*/
    $("#selAllergyCategory").change(function() {
        var option = "<option value='-1'>Select</option>";
        if ($("#selAllergyCategory :selected") != -1) {
            var value = $("#selAllergyCategory :selected").val();
            $('#selAllergyCategoryError').text("");
            $('#selAllergyCategory').removeClass("errorStyle");
        }
        else {
            $("#selAllergy").html(option);
        }
        bindAllergy(value); //on change of allergy type call this function
    });

    //On selecting the allergy test drop down
    $("#selAllergy").change(function() {
        var option = "<option value='-1'>Select</option>";
        if ($("#selAllergy :selected") != -1) {
            var value = $("#selAllergy :selected").val();
            $('#selAllergyError').text("");
            $('#selAllergy').removeClass("errorStyle");
        }
    });

    //Binding the Allergy Type dropdown
    function bindAllergyCategory() {
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/allergies.php",
            data: {
                "operation": "showcategory"
            },
            success: function(data) {
                if (data != null && data != "") {
                    var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                    var option = "<option value='-1'>--Select--</option>";
                    for (var i = 0; i < parseData.length; i++) {
                        option += "<option value='" + parseData[i].id + "'>" + parseData[i].category + "</option>";
                    }
                    $('#selAllergyCategory').html(option);
                }
            },
            error: function() {}
        });
    }

    //Binding Allergy Test drop down
    function bindAllergy(value) {
        var visitId = parseInt($('#textvisitId').val().replace ( /[^\d.]/g, '' ));//get the visit id so to show limited dropdown
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/allergy_general.php",
            data: {
                "operation": "showAllergy",
                "visitId" :visitId,
                "value": value
            },
            success: function(data) {
                if (data != null && data != "") {

                    var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                    var option = "<option value='-1'>--Select--</option>";
                    for (var i = 0; i < parseData.length; i++) {
                        option += "<option value='" + parseData[i].id + "' data-code='" + parseData[i].code + "' data-name='" + parseData[i].name + "'>" + parseData[i].name + "</option>";
                    }
                    $('#selAllergy').html(option);
                }
            },
            error: function() {}
        });
    }


    /*if($("#selAllergyCategoryError").show().delay(3000).fadeOut() == '3000'){
        $('#selAllergyCategory').removeClass("errorStyle");
    }*/

    /*Add allergy Test click event*/
    $('#addAllergyRequest').click(function() {
		var flag = false;
        $('#addAllergyRequest').removeClass("errorStyle"); //remove the red error border from add test button
        $('#noDataError').html(""); //remove the error message after the add button

        if ($('#selAllergyCategory').val() == "-1" || $('#selAllergyCategory').val() == null) {
            $('#selAllergyCategoryError').text("Please select allergy category");
            $('#selAllergyCategory').addClass('errorStyle');
                setTimeout(function() {
                    $('#selAllergyCategory').removeClass('errorStyle');
                }, 3000);
            $("#selAllergyCategoryError").show().delay(3000).fadeOut();
            flag = true;
        }
        if ($('#selAllergy').val() == "-1" || $('#selAllergy').val() == null) {
            $('#selAllergyError').text("Please select allergy");
            $('#selAllergy').addClass("errorStyle");
                setTimeout(function() {
                    $('#selAllergy').removeClass('errorStyle');
                }, 3000);
            $("#selAllergyError").show().delay(3000).fadeOut();
            flag = true;
        }

        if(flag == true){
           return false; 
        }

        var id = $('#selAllergy').val();
        var name = $('#selAllergy  option:selected').attr('data-name');
        var code = $('#selAllergy  option:selected').attr('data-code');

        /*Check that test already exist*/
        for (var i = 0; i < otable.fnGetNodes().length; i++) {
            var iRow = otable.fnGetNodes()[i];   // assign current row to iRow variable
            var aData = otable.fnGetData(iRow); // Pull the row

            testId = aData[3];//getting test id
            var flag = false;

            if (testId == id) {
                $("#addedStatus").text('Allergy already exist!!!');
                $("#addedStatus").show().delay(3000).fadeOut();
                return false;
            }
            if (flag == true) {
                return false;
            }
        }

        /*Add data in table*/
        var currData = otable.fnAddData(["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", code, "<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>", id]);

        $("#addStatus").text('Allergy added successfully!!!');
        $("#addStatus").show().delay(3000).fadeOut();
        /*Get current row to add color in that*/
        var getCurrDataRow = otable.fnSettings().aoData[ currData ].nTr;
        $(getCurrDataRow).find('td').css({"background-color":"#fff","color":"#000"});
        $(getCurrDataRow).find('span').css("color","#000 !important;");
    });

    /*Save button functionality*/
    $('#btnSaveAllergyTestRequest').click(function() {

        var tableData = otable.fnGetData(); //fetching the datatable data

        /*Code to get only current data on click means not to get previous data coming from database*/
        var arr= [];
        jQuery.grep(tableData, function( n, i ){
            if($(n[0]).attr("data-previous") == "false"){
                arr.push(n);
            }
        });
		if(check == "true"){
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Sorry");
			$('.modal-body').text("Your request already in pending..");
			return false;
		}

        //Checking whether datatable is empty or not
        if (tableData.length == 0) {

            $('#addAllergyRequest').addClass("errorStyle"); //Put the red border around the add lab button
            $('#noDataError').html("&nbsp;&nbsp;Please add allergy"); //Put the error message after the add lab test button
            return false;

        } else { //Saving the data

            var patientId = parseInt($('#txtPatientID').val().replace ( /[^\d.]/g, '' ));
            var visitId = parseInt($('#textvisitId').val().replace ( /[^\d.]/g, '' ));
            /*If no new request*/
            if(arr.length == 0){
                $('#messagemyModal').modal();
                $('#messageMyModalLabel').text("Sorry");
                $('.modal-body').text("Your request already in pending!!!");
            }
            else{
                $('#confirmUpdateModalLabel').text();
                $('#updateBody').text("Are you sure that you want to save this?");
                $('#confirmUpdateModal').modal();
                $("#btnConfirm").unbind();
                $("#btnConfirm").click(function(){
                    $.ajax({
                        type: "POST",
                        cache: false,
                        url: "controllers/admin/allergy_general.php",
                        datatype: "json",
                        data: {
                            'operation': 'save',
                            'data': JSON.stringify(arr),//send only curent data only 
                            'patientId': patientId,
                            'visitId': visitId,
                            'tblName': 'allergy_request',
                            'fkColName': 'created_by'
                        },

                        success: function(data) {
                            if (data != "0" && data != "") {
                                $('#messageMyModalLabel').text("Success");
                                $('#messagemyModal .modal-body').text("Allergy request saved successfully!!!");
                                $('#messagemyModal').modal();
                                $('#allergyRequestsTbl input[type="button"]').addClass("btnHide");
                                $('#allergyRequestsTbl input[type="button"]').closest('tr').find('td').removeAttr("style");
                                check = "true";
                                allergy(visitId);
                                clear();
                            } 
                            else {
                                $('#messageMyModalLabel').text("Error");
                                $('#messagemyModal .modal-body').text("Something awful happened!! Please try to contact admin.");
                                $('#messagemyModal').modal();
                            }
                        },
                        error: function() {                   
                            $('#messageMyModalLabel').text("Error");
                            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                            $('#messagemyModal').modal();
                        }
                    });
                });
            }
        }
    });

    //Reset button functionality
    $('#btnResetLabTestRequest').click(function() {
        clear();
    });
});

/**Row Delete functionality**/
function rowDelete(currInst) {
    var row = currInst.closest("tr").get(0);
    var otable = $('#allergyRequestsTbl').dataTable();
    $('#totalBill').text(parseInt($('#totalBill').text()) - parseInt($(row).find('td:eq(3)').text()));
    otable.fnDeleteRow(row);
}

function clear() {
    $("#selAllergyCategory").val("-1");
    $("#selAllergyCategoryError").text("");
    $("#selAllergy").val("-1");
    $("#selAllergyCategory").removeClass("errorStyle");
    $("#selAllergy").removeClass("errorStyle");
    $("#selAllergyCategory").focus();
    $("#selAllergyError").text("");
    /*Reset recent row*/
    var lengthDeleteRow = $(".btnEnabled").closest("tr").length;
    for(var i=0;i<lengthDeleteRow;i++){
        var myDeleteRow = $(".btnEnabled").closest("tr")[0]; 
        otable.fnDeleteRow(myDeleteRow);
    }
}