$(document).ready(function() {
	debugger;	
	bindOTRooms();
	$("#btnBookOT").mouseover(function () {
    	debugger;
		$(this).val();    	
    });
});
function bindOTRooms(){
	var postData = {
		operation : "showOTRooms"
	};
	var parseData = '';
	dataCall("controllers/admin/ot_rooms_availablity.php", postData, function (result){
		parseData = JSON.parse(result)
		if (parseData[0] !='' || parseData[1] !='') {			
			var  OTRooms = parseData[0];
			var bigArray = [];			
			
			var title = '';
			var start = '';
			var end = '';
			var resourceId = '';
			if (parseData[1] =='') {
				callSuccessPopUp("Alert","No patient booking for OT!!!")
			}
			$.each(parseData[1],function(i,v){

				var patientId = parseData[1][i].patient_id;
                                    
                var patientIdLength = patientId.length;
                for (var j=0;j<6-patientIdLength;j++) {
                    patientId = "0"+patientId;
                }
                patientId = patientPrefix+patientId;

				var splitDate = parseData[1][i]['operation_date'].split("-");	
				var surgeryYear = splitDate[0];
				var surgeryMonth = splitDate[1]-1;
				var surgeryDate = splitDate[2];

				var splitSurgeryStartTime = parseData[1][i]['from_time'].split(":");
				var surgeryStartTimeHour = splitSurgeryStartTime[0];
				var surgeryStartTimeMin = splitSurgeryStartTime[1];

				var splitSurgeryEndTime = parseData[1][i]['to_time'].split(":");
				var surgeryEndTimeHour = splitSurgeryEndTime[0];
				var surgeryEndTimeMin = splitSurgeryEndTime[1];
				var obj = {};
				obj.title = parseData[1][i].patient_name+" ("+patientId+")"+"\n"+"Surgeon : "+parseData[1][i].surgeon_name+"\n"+"Anesthesian : "+parseData[1][i].anethaesthiest_name+"\n"+"Nurse : "+parseData[1][i].nurse_name;
				obj.start = new Date(surgeryYear, surgeryMonth, surgeryDate, surgeryStartTimeHour, surgeryStartTimeMin);
				obj.end = new Date(surgeryYear, surgeryMonth, surgeryDate, surgeryEndTimeHour, surgeryEndTimeMin);
				obj.resourceId = parseData[1][i].roomId;
				obj.allDay = false;				
				bigArray.push(obj);
				$($('.fc-event-time')[i]).text(patientId);
			});
		}
		else{
			callSuccessPopUp("Alert","No data Exist!!!");
		}
		var calendar = $('#calendar').fullCalendar({
			renderEvent: 'newEvent', 
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'resourceDay,month,agendaWeek,agendaDay'
			},
			titleFormat: 'ddd, MMM dd, yyyy',
			defaultView: 'resourceDay',
			selectable: false,//on click open that pop up
			selectHelper: true,

			select: function(start, end, allDay, event, resourceId) {
				var title = prompt('Event Title:');
				if (title) {
					console.log("@@ adding event " + title + ", start " + start + ", end " + end + ", allDay " + allDay + ", resource " + resourceId);
					calendar.fullCalendar('renderEvent',
					{
						title: title,
						start: start,
						end: end,
						allDay: allDay,
						resourceId: resourceId
					},
					true // make the event "stick"
				);
				}
				calendar.fullCalendar('unselect');
			},
			eventResize: function(event, dayDelta, minuteDelta) {
				console.log("@@ resize event " + event.title + ", start " + event.start + ", end " + event.end + ", resource " + event.resourceId);
			},
			eventDrop: function( event, dayDelta, minuteDelta, allDay) {
				console.log("@@ drag/drop event " + event.title + ", start " + event.start + ", end " + event.end + ", resource " + event.resourceId);
			},
			editable: false,				
			resources: OTRooms,//Putting here the dynamic data
			events: bigArray,//Putting here the dynamic data,
			eventRender:function(event,element){
				$(element).tooltip({title:event.title});
			}
		});

		$(".fc-header-right").find('span:eq(1)').children('span:eq(0)').text("OT Rooms");

		/*if ($("#btnBookOT").length ==0) {*/
			$('.fc-header tbody tr').append('<td><input type ="button" id= "btnBookOT"class ="btn_save" onclick="loadPage()" onhover = "showValue()" value ="Book OT"></td>');
		/*}*/

		if ($('.fc-header').length >1) {
			$($('.fc-header')[1]).html('');
		}
		if ($('.fc-content').length >1) {
			$($('.fc-content')[1]).html('');
		}
		
	});
}

function loadPage(){
    $('#modalRegOT').load('views/admin/patient_ot_reg.html',function(){
    	$("#btnView").click(function(){
    		$("#selfRequestModalBody #myPatientModal").find('button').removeClass('close');
    		$("#selfRequestModalBody #myPatientModal").find('button').removeAttr('data-dismiss');
    		$("#selfRequestModalBody #myPatientModal").find('button').addClass('subPatientModal');
    	});
    }); 
    $('#patientOTReg').modal();
}
function showValue(){
	$(this);
}

/* var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();

	var calendar = $('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'resourceDay,month,agendaWeek,agendaDay'
		},
		titleFormat: 'ddd, MMM dd, yyyy',
		defaultView: 'resourceDay',
		selectable: true,
		selectHelper: true,
		select: function(start, end, allDay, event, roomId) {
			var title = prompt('Event Title:');
			if (title) {
				console.log("@@ adding event " + title + ", start " + start + ", end " + end + ", allDay " + allDay + ", resource " + roomId);
				calendar.fullCalendar('renderEvent',
				{
					title: title,
					start: start,
					end: end,
					allDay: allDay,
					roomId: roomId
				},
				true // make the event "stick"
			);
			}
			calendar.fullCalendar('unselect');
		},
		/*eventResize: function(event, dayDelta, minuteDelta) {
			console.log("@@ resize event " + event.title + ", start " + event.start + ", end " + event.end + ", resource " + event.roomId);
		},
		eventDrop: function( event, dayDelta, minuteDelta, allDay) {
			console.log("@@ drag/drop event " + event.title + ", start " + event.start + ", end " + event.end + ", resource " + event.roomId);
		},
		editable: true,
		resources: OTRooms,//define the Ot rooms here
		events: [                
			{
				title: 'Short Event 1',
				start: new Date(y, m, d, 11, 30),
				end: new Date(y, m, d, 13, 00),
				allDay: false,
				roomId: 'room2'
			},
			{
				title: 'Short Event 2',
				start: new Date(y, m, d + 1, 14, 00),
				end: new Date(y, m, d + 1, 15, 00),
				allDay: false,
				roomId: 'room3'
			},
			{
				title: 'All Day Event 2',
				start: new Date(y, m, d - 2),
				end: new Date(y, m, d - 1),
				roomId: 'room4'
			},
			{
				title: 'Lunch',
				start: new Date(y, m, d, 12, 0),
				end: new Date(y, m, d, 14, 0),
				allDay: false,
				roomId: 'room5'
			},
			{
				title: 'All Day Event 3',
				start: new Date(y, m, d),
				roomId: 'room1'
			},
			{
				title: 'Click for Google',
				start: new Date(y, m, d, 16, 0),
				end: new Date(y, m, d, 16, 30),
				allDay: false,
				url: 'http://google.com/',
				roomId: 'room1'
			}
		]
	});
}); */