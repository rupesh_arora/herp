var taxTypeTable;
$(document).ready(function(){
	debugger;
	$("#formTaxType").hide();
    $("#divTaxType").addClass('list');    
    $("#tabTaxTypeList").addClass('tab-list-add');
    
    $("#tabTaxType").click(function() { // show the add allergies categories tab
         showAddTab();
         clearFormDetails(".clearForm");
         
    });

    $("#tabTaxTypeList").click(function() { // show allergies list tab
        showTabList();
        clearFormDetails(".clearForm");
    });

    removeErrorMessage();// remove error messaages

    $("#btnSave").click(function(){
    	var flag = false;

        
        if(validTextField('#txtTaxRate','#txtTaxRateError','Please enter tax rate') == true)
        {
            flag = true;
        }
        if(validTextField('#txtTaxName','#txtTaxNameError','Please enter tax name') == true)
        {
            flag = true;
        }
        if(flag == true) {
			return false;
		}

		var taxName = $("#txtTaxName").val().trim();
		var taxRate = $("#txtTaxRate").val().trim();

		var postData = {
			taxName : taxName,
			taxRate : taxRate,
			"operation" : "save"
		}

		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/tax_type.php",
			datatype: "json",
			data: postData,
			 success: function(data){
                if(data !='' && data!=null && data !='0'){
                    callSuccessPopUp("Success","Saved  successfully!!!");
                    clearFormDetails(".clearForm");
                    showTabList();
                    $("#txtTaxName").focus();
                }
                else if (data == "0") {
                    $("#txtTaxName").focus();
                    $("#txtTaxName").addClass("errorStyle");
                    $("#txtTaxNameError").text("This tax name alreday exist.");                    
                }
            },
            error:function(){

            }
        });

    });

    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code
        taxTypeTable = $('#taxTypeTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = taxTypeTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var taxName = mData["tax_name"];
                        var taxRate = mData["tax_rate"];
                        editClick(id,taxName,taxRate);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = taxTypeTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/tax_type.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "tax_name"
                }, {
                    "mData": "tax_rate"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [2]
            }]
        });
    }
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            taxTypeTable.fnClearTable();
            taxTypeTable.fnDestroy();
            taxTypeTable = "";
            taxTypeTable = $('#taxTypeTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = taxTypeTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var tax_name = mData['tax_name'];
                            restoreClick(id,tax_name);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/tax_type.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    {
                    "mData": "tax_name"
                }, {
                    "mData": "tax_rate"
                }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [2]
                }]
            });
        } else { // show active data on unchecked   
            taxTypeTable.fnClearTable();
            taxTypeTable.fnDestroy();
            taxTypeTable = "";
            taxTypeTable = $('#taxTypeTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = taxTypeTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                        var id = mData["id"];
                        var taxName = mData["tax_name"];
                        var taxRate = mData["tax_rate"];
                        editClick(id, taxName,taxRate);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = taxTypeTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/tax_type.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "tax_name"
                    }, {
                        "mData": "tax_rate"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [2]
                }]
            });
        }
    });

    $("#btnReset").click(function() {
       clearFormDetails(".clearForm");
       removeErrorMessage();
       $("#txtTaxName").focus();
    });

});
function editClick(id,taxName,taxRate) {
    showAddTab();
    $("#uploadFile").hide();
    $("#btnReset").hide();
    $("#btnSave").hide();
    $('#btnUpdate').show();
    $('#tabTaxType').html("+Update Tax Type");
    
    $("#txtTaxName").focus();
   
    $("#myModal #btnUpdate").removeAttr("style");
    
    $("#txtTaxName").removeClass("errorStyle");
    $("#txtTaxNameError").text("");
    
    
    $('#txtTaxName').val(taxName);
    $('#txtTaxRate').val(taxRate);
    $('#selectedRow').val(id);
    //validation
    //remove validation style
    // edit data function for update 
   removeErrorMessage();
    
    
    $("#btnUpdate").click(function() { // click update button
        var flag = "false";
        if ($("#txtTaxRate").val().trim()== '') {
            $("#txtTaxRateError").text("Please enter tax rate");
            $("#txtTaxRate").focus();
            $("#txtTaxRate").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtTaxName").val().trim()== '') {
            $("#txtTaxNameError").text("Please enter tax name");
            $("#txtTaxName").focus();
            $("#txtTaxName").addClass("errorStyle");
            flag = "true";
        }
        
        if(flag == "true") {
            return false;
        }
        
        
        var taxName = $("#txtTaxName").val().trim();
        var taxRate = $("#txtTaxRate").val().trim();        
        
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();

        $("#btnConfirm").click(function(){
            var postData = {
                "operation": "update",
                "taxName": taxName,
                "taxRate": taxRate,
                "id": id
            }
            $.ajax({ //ajax call for update data
                
                type: "POST",
                cache: false,
                url: "controllers/admin/tax_type.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data == "1") {
                        callSuccessPopUp("Success","Tax name updated Successfully!!!");
                        $('#myModal .close').click();
                        taxTypeTable.fnReloadAjax();
                        clearFormDetails(".clearForm");
                        showTabList();
                    }
                    else{
                        $("#txtTaxName").focus();
                        $("#txtTaxName").addClass('errorStyle');
                        $("#txtTaxNameError").text("This name alreday exist.");
                    }
                },
                error: function() {
                    callSuccessPopUp("Error","Temporary Unavailable to Respond.Try again later!!!");
                }
            }); // end of ajax
        });
    });
} // end update button
function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete tax name");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    var taxName = '';
    $('#confirm').attr('onclick', 'deleteTaxName("'+type+'","'+taxName+'");');
} // end click fucntion

function restoreClick(id,tax_name) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore tax name");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    var taxName = tax_name;
    $('#confirm').attr('onclick', 'deleteTaxName("'+type+'","'+taxName+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteTaxName(type,taxName) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/tax_type.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    callSuccessPopUp("Success","Tax name deleted Successfully!!!");
                    taxTypeTable.fnReloadAjax();
                } else {
                    callSuccessPopUp("Sorry","This tax name is used , so you can not delete!!!");
                }
            },
            error: function() {
                callSuccessPopUp("Error","Temporary Unavailable to Respond.Try again later!!!");
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/tax_type.php",
            data: {
                "operation": "restore",
                "id": id,
                "taxName" : taxName
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    callSuccessPopUp("Success","Tax name restored Successfully!!!");
                    taxTypeTable.fnReloadAjax();
                }
                else{
                    callSuccessPopUp('Alert','This tax name already exist. So you can\'t restore it.!!!');
                }
            },
            error: function() {
                callSuccessPopUp("Error","Temporary Unavailable to Respond.Try again later!!!"); 
            }
        });
    }
}

function showAddTab(){
    $("#formTaxType").show();
    $(".blackborder").hide();

    $("#tabTaxType").addClass('tab-detail-add');
    $("#tabTaxType").removeClass('tab-detail-remove');
    $("#tabTaxTypeList").removeClass('tab-list-add');
    $("#tabTaxTypeList").addClass('tab-list-remove');
    $("#divTaxType").addClass('list');
    $("#txtTaxName").focus();
}

function showTabList(){

    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#formTaxType").hide();
    $(".blackborder").show();

    $("#tabTaxType").removeClass('tab-detail-add');
    $("#tabTaxType").addClass('tab-detail-remove');
    $("#tabTaxTypeList").removeClass('tab-list-remove');    
    $("#tabTaxTypeList").addClass('tab-list-add');
    $("#divTaxType").addClass('list');


    $("#uploadFile").show();
    $("#btnReset").show();
    $("#btnSave").show();
    $('#btnUpdate').hide();
    $('#tabTaxType').html("+Add Tax Type");
}