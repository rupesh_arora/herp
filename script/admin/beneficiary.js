var BeneficiaryTable;
$(document).ready(function(){
    loader();
    debugger;
	
	/*Hide add ward by default functionality*/
	$("#advanced-wizard").hide();
    $("#beneficiaryList").addClass('list');    
    $("#tabBeneficiaryList").addClass('tab-list-add');
		
	/*Click for add the beneficiary*/
    $("#tabAddBeneficiary").click(function() {
        clear();
        showAddTab();
    });
	
	//Click function for show the beneficiary lists
    $("#tabBeneficiaryList").click(function() {
        clear();
        tabBeneficiaryList();
    });

    removeErrorMessage();

    $('#btnSubmit').click(function() {
        var flag = false;

        if($('#txtCompanyName').val() == '') {
            $('#txtCompanyNameError').text('Please enter company name');
            $('#txtCompanyName').focus();
            $('#txtCompanyName').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtBeneficiaryName').val() == '') {
            $('#txtBeneficiaryNameError').text('Please enter beneficiary name');
            $('#txtBeneficiaryName').focus();
            $('#txtBeneficiaryName').addClass("errorStyle"); 
            flag = true;
        }
        if(flag == true) {
            return false;
        }

        var beneficiaryName = $('#txtBeneficiaryName').val().trim();
        var companyName = $('#txtCompanyName').val().trim();
        var description = $('#txtDescription').val().trim();
        description = description.replace(/'/g, "&#39");

        var postData = {
            beneficiaryName : beneficiaryName,
            companyName : companyName,
            description : description,
            operation : "saveBeneficiary"
        }


        $.ajax(
            {                   
            type: "POST",
            cache: false,
            url: "controllers/admin/beneficiary.php",
            datatype:"json",
            data: postData,
            
            success: function(data) {
                if(data != "0" && data != ""){
                    callSuccessPopUp('Success','Saved successfully!!!');
                    tabBeneficiaryList();
                    clear();
                    $('#txtBeneficiaryName').focus();
                }
                else{
                    $('#txtBeneficiaryNameError').text('beneficiary name already exist');
                    $('#txtBeneficiaryName').focus();
                    $('#txtBeneficiaryName').addClass("errorStyle"); 
                }
            },
            error: function(){

            }
        });
    });

    $('#btnReset').click(function(){
        clear();
    });


    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
    //Datatable code 
        BeneficiaryTable = $('#BeneficiaryTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "bAutoWidth": false,
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = BeneficiaryTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var beneficiary_name = mData["beneficiary_name"]; 
                        var company_name = mData["company_name"];                      
                        var description = mData["description"];
                        editClick(id,beneficiary_name,company_name,description);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = BeneficiaryTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/beneficiary.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "beneficiary_name"
                }, {
                    "mData": "company_name"
                }, {
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [2,3]
            }]

        });
    }

    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            BeneficiaryTable.fnClearTable();
            BeneficiaryTable.fnDestroy();
            BeneficiaryTable = "";
            BeneficiaryTable = $('#BeneficiaryTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = BeneficiaryTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var beneficiaryName = mData["beneficiary_name"];                      
                            var companyName = mData["company_name"];
                            restoreClick(id,beneficiaryName,companyName);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/beneficiary.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "beneficiary_name"
                    }, {
                        "mData": "company_name"
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [2,3]
                }]
            });
        } else { // show active data on unchecked   
            BeneficiaryTable.fnClearTable();
            BeneficiaryTable.fnDestroy();
            BeneficiaryTable = "";
            BeneficiaryTable = $('#BeneficiaryTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = BeneficiaryTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var beneficiary_name = mData["beneficiary_name"]; 
                            var company_name = mData["company_name"];                      
                            var description = mData["description"];
                            editClick(id,beneficiary_name,company_name,description);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = BeneficiaryTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/beneficiary.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "beneficiary_name"
                    }, {
                        "mData": "company_name"
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [2,3]
                }]
            });
        }
    });
});

function editClick(id,beneficiary_name,company_name,description) {
    
    showAddTab();
    $("#btnReset").hide();
    $("#btnSubmit").hide();
    $('#btnUpdate').show();
    $('#tabAddBeneficiary').html("+Update Beneficiary");
    $('#txtBeneficiaryName').focus();
    
    $("#txtBeneficiaryName").removeClass("errorStyle");
    $("#txtBeneficiaryNameError").text("");
    $("#txtCompanyName").removeClass("errorStyle");
    $("#txtCompanyNameError").text("");

   
    $('#txtBeneficiaryName').val(beneficiary_name);
    $('#txtCompanyName').val(company_name);
    $('#txtDescription').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
   
 
    removeErrorMessage();
    
     //validation
    $("#btnUpdate").click(function() { // click update button
        var flag = false;
        if ($("#txtCompanyName").val()== '') {
            $("#txtCompanyNameError").text("Please enter company name");
            $("#txtCompanyName").focus();
            $("#txtCompanyName").addClass("errorStyle");
            flag = true;
        }
        if ($("#txtBeneficiaryName").val()== '') {
            $("#txtBeneficiaryNameError").text("Please enter beneficiary name");
            $("#txtBeneficiaryName").focus();
            $("#txtBeneficiaryName").addClass("errorStyle");
            flag = true;
        }
        
        if(flag == true) {
            return false;
        }

        var beneficiaryName = $("#txtBeneficiaryName").val().trim();
        var companyName = $("#txtCompanyName").val().trim();
        var description = $("#txtDescription").val().trim();
        description = description.replace(/'/g, "&#39");
        var id = $('#selectedRow').val();
        
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
        var postData = {
            "operation": "update",
            "beneficiaryName": beneficiaryName,
            "companyName": companyName,
            "description": description,
            "id": id
        }
        $.ajax( //ajax call for update data
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/beneficiary.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('#myModal').modal('hide');
                        $('.close-confirm').click();
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Beneficiary updated successfully!!!");
                        $('#messagemyModal').modal();
                        BeneficiaryTable.fnReloadAjax();
                        tabBeneficiaryList();
                        clear();
                    }
                    else{
                        $("#txtBeneficiaryNameError").text("Beneficiary name already exist");
                        $("#txtBeneficiaryName").focus();
                        $("#txtBeneficiaryName").addClass("errorStyle");
                    }
                },
                error: function() {
                    $('.close-confirm').click();
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                    $('#messagemyModal').modal();
                }
            }); // end of ajax
        });
    });
} // end update button

function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Beneficiary");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteBeneficiary("' + type + '","'+id+'","","");');
} // end click fucntion

function restoreClick(id,beneficiaryName,companyName) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Beneficiary");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteBeneficiary("' + type + '","'+id+'","'+beneficiaryName+'","'+companyName+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteBeneficiary(type,id,beneficiaryName,companyName) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/beneficiary.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Beneficiary deleted successfully!!!");
                    $('#messagemyModal').modal();
                    BeneficiaryTable.fnReloadAjax();
                } 
                else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This Beneficiary is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/beneficiary.php",
            data: {
                "operation": "restore",
                "beneficiaryName":beneficiaryName,
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Beneficiary restored successfully!!!");
                    $('#messagemyModal').modal();
                    BeneficiaryTable.fnReloadAjax();
                }
                else{
                    callSuccessPopUp("Sorry","This beneficiary is already exist so you can't restore it!!!")
                }
            },
            error: function() {             
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary unavailable to respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}


function tabBeneficiaryList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
   
    $("#tabAddBeneficiary").removeClass('tab-detail-add');
    $("#tabAddBeneficiary").addClass('tab-detail-remove');
    $("#tabBeneficiaryList").removeClass('tab-list-remove');    
    $("#tabBeneficiaryList").addClass('tab-list-add');
    $("#beneficiaryList").addClass('list');
   
    $("#btnReset").show();
    $("#btnSubmit").show();
    $('#btnUpdate').hide();
    $('#tabAddBeneficiary').html("+Add Beneficiary");
    $('#inactive-checkbox-tick').prop('checked', false).change();
    clear();
}
function showAddTab(){
    $("#advanced-wizard").show();
    $(".blackborder").hide();

    $("#tabAddBeneficiary").addClass('tab-detail-add');
    $("#tabAddBeneficiary").removeClass('tab-detail-remove');
    $("#tabBeneficiaryList").removeClass('tab-list-add');
    $("#tabBeneficiaryList").addClass('tab-list-remove');
    $("#beneficiaryList").addClass('list');
    $('#txtName').focus();
}
function clear() {
    removeErrorMessage();
    clearFormDetails(".clearForm"); 
    $('#txtBeneficiaryName').focus();
}