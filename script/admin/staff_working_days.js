var flag = "false";
var selectedDate; // define variable for store the selected date 

var dayEnum = {
	0: 'Monday',
	1: 'Tuesday',
	2: 'Wednesday',
	3: 'Thursday',
	4: 'Friday',
	5: 'Saturday',
	6: 'Sunday'
}

$(document).ready(function() {
	debugger;
	loader();
	//Calling function for binding the staff type
	bindStaffType();	
	$("#divSchedular").hide(); //show div for repetitive schedule
	$("#divRepetitiveSchedule").show(); //show div for repetitive schedule	
	$(".chkAvailable").prop("checked",false);	//unchecked all checked checkbox
	$(".scheduler").hide();	//remove attr disabled for add slots button

	//Change event for checkbox
	$(".chkAvailable").change(function(){
		var radioValue = $("input[name='Schedule']:checked").val();
		if(radioValue == "Repetitive"){
			if(!$(this).is(':checked')){
				//Remove all dynamic text box of this particular div
				$(this).parent().parent().find('.div').remove();
				//make add slots button disabled of this particular div
				$(this).parent().parent().parent().find('.scheduler').hide();
			}
			else{
				//remove disabled from button of this div
				$(this).parent().parent().parent().find('.scheduler').show();
			}
		}
		else{
			if(!$(this).is(':checked')){
				//Remove all dynamic text box of this particular div
				$(this).parent().parent().find(".div").remove();	
				//make add slots button disabled of this particular div
				$(this).parent().parent().parent().find('.scheduler').hide();
			}
			else{
				//remove disabled from button of this div
				$(this).parent().parent().parent().find('.scheduler').show();
			}
		}
	});
	
	// remove error on change
	//Change event of staff type
	$('#selTimeDuration').change(function() {	
		if($('#selTimeDuration') != -1) {
			$("#selTimeDurationError").text("");
			$("#selTimeDuration").removeClass("errorStyle");
		}
	});
	//Change event of staff type
	$('#selStaffType').change(function() {				
        var htmlRoom = "<option value='-1'>-- Select --</option>";
		$("#selStaffTypeError").text("");
		$("#selStaffType").removeClass("errorStyle");

        if ($('#selStaffType :selected').val() != -1) {
			$("#divSchedular").show(); //show div for repetitive schedule
            var value = $('#selStaffType :selected').val();
			
			//Calling function for binding the staff name
            bindStaffName(value);                     
			//$("#divSchedular").hide(); //show div for repetitive schedule
			$('#lblDesignation').text("N/A");
			$('#lblEmailId').text("N/A");
			$('#lblPhoneNo').text("N/A");
			$('#lblAddress').text("N/A");
			
        } else {
            $('#selStaffName').html(htmlRoom);
            $("#divSchedular").hide(); //show div for repetitive schedule
			$('#lblDesignation').text("N/A");
			$('#lblEmailId').text("N/A");
			$('#lblPhoneNo').text("N/A");
			$('#lblAddress').text("N/A");
        }
    });
	
	//Change event of staff name
	$('#selStaffName').change(function() {
		$("#divSchedular").show(); //show div for repetitive schedule
		$(".day-container").html("");						//Remove the dynamic text box
		$(".chkAvailable").prop("checked",false);		//unchecked all checked checkbox
		$(".scheduler").hide()				//remove attr disabled for add slots button
		$("#selStaffNameError").text("");
		$("#selStaffName").removeClass("errorStyle");
		
        if ($('#selStaffName :selected').val() != -1) {
            var value = $('#selStaffName :selected').val();
			if(value != "-1"){
				bindStaffInformation(value);                     //Calling function for binding the staff information
				bindStaffSchedule();
			}
           else{
           		$("#divSchedular").hide(); //show div for repetitive schedule
			    $('#lblDesignation').text("N/A");
				$('#lblEmailId').text("N/A");
				$('#lblPhoneNo').text("N/A");
				$('#lblAddress').text("N/A");
		   }				
        }
		else{
			$("#divSchedular").hide(); //show div for repetitive schedule
			$('#lblDesignation').text("N/A");
			$('#lblEmailId').text("N/A");
			$('#lblPhoneNo').text("N/A");
			$('#lblAddress').text("N/A");
		}
    });

	// call event for add slots		
	$(".scheduler").bind("click", function () {
		var div = $("<div />");
		div.html(GetDynamicText(""));
		
		$(this).parent().parent().find('#divContainer').append(div);
		//Add time picker 
		$('.time').timepicker({
			timeFormat: 'H:i:s'
		}); 
		
		//change event for remove the error message
		$(".fromTime").change(function(){
			if($(this).val() != ""){
				$(this).parent().find('.from-time-error').text('');
			}
		});
		
		//change event for remove the error message
		$(".toTime").change(function(){
			if($(this).val() != ""){
				$(this).parent().find('.to-time-error').text('');
			}
		});
	
	});// end of add slots
	
	// remove div slots
	$("body").on("click", ".remove", function () {
		$(this).closest("div").remove();
	});
	
	//click event for save the data
	$("#btnSaveRepetitive").click(function(){
		flag = "false";
		
		if($("#selStaffType").val() == "-1"){
			$("#selStaffType").focus();
			$("#selStaffTypeError").text("Please select staff type");
			$("#selStaffType").addClass("errorStyle");
			flag = "true";
		}
		/* if($("#selStaffName").val() == "-1"){
			$("#selStaffName").focus();
			$("#selStaffNameError").text("Please select staff name");
			$("#selStaffName").addClass("errorStyle");
			flag = "true";
		} */

		if(flag == "true"){
			return false;
		}

		//Check validation for Repetitive weekly schedule
		if($(".divMonday").find(".chkAvailable").is(":checked")){
		checkValidation(".divMonday");
		}
		if($(".divTuesday").find(".chkAvailable").is(":checked")){
			checkValidation(".divTuesday");
		}
		if($(".divWednesday").find(".chkAvailable").is(":checked")){
			checkValidation(".divWednesday");
		}
		if($(".divThursday").find(".chkAvailable").is(":checked")){
			checkValidation(".divThursday");
		}
		if($(".divFriday").find(".chkAvailable").is(":checked")){
			checkValidation(".divFriday");
		}
		if($(".divSaturday").find(".chkAvailable").is(":checked")){
			checkValidation(".divSaturday");
		}
		if($(".divSunday").find(".chkAvailable").is(":checked")){
			checkValidation(".divSunday");
		}				
		if($('.chkAvailable').is(":checked") == false) {
			$('.modal-body').text("");
			$('#messageMyModalLabel').text("Alert");
			$('.modal-body').text("Please select any weekly schedule !!!");
			$('#messagemyModal').modal();
			flag = "true";
		}
		if(flag == "true"){
			return false;
		}
		
		var staffTypeId = $("#selStaffType").val();
		var staffId = $("#selStaffName").val();

		var repetativeSchedule =  new Array(); //Array for save the text box data of repetitive weekly schedule
		
		$(".main-div").each(function () {
			var tempArray =  new Array();   //Array for temperory store the data 
			
			$(this).find('.div').find('.fromtime').each(function(){
				var Obj = {};					
				Obj.fromTime = $(this).val();
				Obj.toValue = $(this).parent().parent().parent().find('.toTime').val();
				Obj.notesValue = $(this).parent().parent().parent().find('#txtNotes').val();
				tempArray.push(Obj);	
			});
			repetativeSchedule.push(tempArray);	
		});
		
		var postData = {
			"operation": "saveRepetitive",
			"staffTypeId": staffTypeId,
			"staffId": staffId,
			"repetativeSchedule":JSON.stringify(repetativeSchedule)
		}

		//Confirmation pop up
		$('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to save this?");
	    $('#confirmUpdateModal').modal();
	    $("#btnConfirm").unbind();
	    $("#btnConfirm").click(function(){			
			$.ajax({
				type: "POST",
				cache: false,
				url: "controllers/admin/staff_working_days.php",
				datatype:"json",
				data: postData,
				success : function(data){
					if(data !=""){
						$('#myModal').modal('hide');
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("saved successfully!!!");
						$('#messagemyModal').modal();
						$(".day-container").html("");						//Remove the dynamic text box
						$(".chkAvailable").prop("checked",false);			//unchecked all checked checkbox
						$(".scheduler").hide();								//remove attr disabled for add slots button
						clear();
					}
				},
				error : function(){
					$('.close-confirm').click();
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
			});
		});		
	});	
});

// Function for create dynamic text box
function GetDynamicText(value) {	
    return '<div class="day-container div row">'+	
			'<div class="form-group col-md-3 from-text">'+
				'<label class="control-label" for="txtFromTime">From Time<span class="text-danger">*</span></label>'+                                
				'<div class="">'+
					'<input type="text" class="time fromTime" id="txtFromTime" >'+
					'<span id="txtFromTimeError" class="error from-time-error"></span>'+
				'</div>'+
			'</div>'+
			'<div class="form-group col-md-3 to-text">'+
				'<label class=" control-label" for="txtToTime">To Time<span class="text-danger">*</span></label>'+                                
				'<div class="">'+
					'<input type="text" class="time toTime" id="txtToTime" >'+
					'<span id="txtToTimeError" class="error to-time-error"></span>'+
				'</div>'+
			'</div>'+
			'<div class="form-group col-md-3">'+
				'<label class="control-label" for="txtNotes">Notes</label> '+                               
				'<div class="">'+
					'<input type="text" id="txtNotes" class="notes">'+
				'</div>'+
			'</div>'+
				 '<input type="button" value="Remove" class="remove btn btn-primary btn_reset" />'+
		'</div>';
}

//Function for binding the staff type
function bindStaffType(){
	var postData = {
		"operation": "showStaffType"
	}

	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/staff_working_days.php",
		datatype: "json",
		data: postData,
		success: function(data) {		
			if(data != ""){
				var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
				var option = "<option value='-1'>--Select--</option>";
				
				for (var i = 0; i < parseData.length; i++) {
					option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
				}
				
				$('#selStaffType').html(option);
			}
		},		
		error:function() {
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
	});
}

//Function for binding the staff name 
function bindStaffName(value){
	var postData = {
		"operation": "showStaffName",
		"staffTypeId": value
	}

	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/staff_working_days.php",
		datatype: "json",
		data: postData,
		success: function(data) {		
			if(data != ""){
				var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
				var option = "<option value='-1'>--Select--</option>";
				
				for (var i = 0; i < parseData.length; i++) {
					option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
				}
				
				$('#selStaffName').html(option);
			}
		},		
		error:function() {
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
	});
}

//Function for binding the staff information 
function bindStaffInformation(value){
	var postData = {
		"operation": "showStaffinformation",
		"userId": value
	}

	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/staff_working_days.php",
		datatype: "json",
		data: postData,
		success: function(data) {		
			if(data != ""){
				var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery				
				$('#lblDesignation').text(parseData[0].designation);
				$('#lblEmailId').text(parseData[0].email_id);
				$('#lblPhoneNo').text(parseData[0].mobile);
				$('#lblAddress').text(parseData[0].address);
			}
		},		
		error:function() {
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
	});
}

//Function for check validation
function checkValidation(divId){
	var startTime = "";
	var endTime = "";
	var tempStartTime = "";
	var tempEndTime = "";
	var regExp = /(\d{1,2})\:(\d{1,2})\:(\d{1,2})/;
	if($(divId).find(".div").length == ""){
		$('#messageMyModalLabel').text("Error");
		$('.modal-body').text("Please select at least one slot.");
		$('#messagemyModal').modal();
		flag ="true";
		return false;
	}
	else{
		debugger;
		var timeLength = $(divId).find(".div").find(".fromTime").length;
		for(i=0;i<timeLength;i++){
			if($($(divId).find(".div").find(".fromTime")[i]).val() == ""){
				$($(divId).find(".div").find('.from-text').find(".from-time-error")[i]).text("Select from time");
				flag ="true";
			}
			else{
				startTime = $($(divId).find(".div").find(".fromTime")[i]).val();
				tempEndTime = endTime;
			} 
			if(i != 0){
				if(startTime != "" && endTime != ""){
					if((parseInt(startTime.replace(regExp, "$1$2$3")) > parseInt( tempStartTime.replace(regExp, "$1$2$3"))) && (parseInt(startTime .replace(regExp, "$1$2$3")) < parseInt(endTime .replace(regExp, "$1$2$3")))){
						$($(divId).find(".div").find('.from-text').find(".from-time-error")[i]).text("Select correct time");	
						flag ="true";
					}
				}				
			} 
			if($($(divId).find(".div").find(".toTime")[i]).val() == ""){
				$($(divId).find(".div").find('.to-text').find(".to-time-error")[i]).text("Select to time");	
				flag ="true";
			}
			else{
				endTime = $($(divId).find(".div").find(".toTime")[i]).val();				
			}
			if(startTime != "" && endTime != ""){
				if(parseInt(endTime .replace(regExp, "$1$2$3")) <= parseInt(startTime .replace(regExp, "$1$2$3"))){
					$($(divId).find(".div").find('.to-text').find(".to-time-error")[i]).text("Select correct time");	
					flag ="true";
				}
			}
			if(i != 0){
				if(startTime != "" && endTime != ""){
					if((parseInt(endTime.replace(regExp, "$1$2$3")) > parseInt( tempStartTime.replace(regExp, "$1$2$3"))) && (parseInt(endTime .replace(regExp, "$1$2$3")) < parseInt(tempEndTime .replace(regExp, "$1$2$3")))){
						$($(divId).find(".div").find('.to-text').find(".to-time-error")[i]).text("Select correct time");	
						flag ="true";
					}
				}				
			} 
			tempStartTime = startTime;
		}
	}
}

//This function is used for show the date in own format
function dateFormat(fullDate){
	
	//convert month to 2 digits
	var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '' + (fullDate.getMonth()+1);
	var todayDay = fullDate.getDate();
	
	if(twoDigitMonth.length == 1){
		twoDigitMonth = "0"+twoDigitMonth;
	}
	 
	if(todayDay < "10"){
		todayDay = "0"+todayDay;
	}
	
	selectedDate = fullDate.getFullYear() + "-" + twoDigitMonth+ "-" + todayDay ;	
}

//function for show the previous schedule
function editData(i, obj, divClass){	
	if(obj[i].is_available == "available"){
		var div = $("<div />");
		div.html(GetDynamicText(""));								
		$(divClass).find('#divContainer').append(div);
		$(divClass).find('.fromTime').last().val(obj[i].from_time);
		$(divClass).find('.toTime').last().val(obj[i].to_time);
		$(divClass).find('.notes').last().val(obj[i].notes);
		$(divClass).find(".chkAvailable").prop("checked",true);
		$(divClass).find(".scheduler").show();
	}
	else{
		$(divClass).find(".chkAvailable").prop("checked",false);
		$(divClass).find(".scheduler").hide();
	}
}

function particularDateData(postData){
	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/staff_working_days.php",
		datatype:"json",
		data: postData,
		success : function(data){
			var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
			if(parseData !=""){							
				for (var i = 0; i < parseData.length; i++) {
					editData(i, parseData, '.divDaily');
					var timeDuration = parseData[i].time_duration;
				}
				$("#selTimeDuration").val(timeDuration);
				$(".chkAvailable").prop("checked",true);		//unchecked all checked checkbox
				$(".scheduler").show();				//remove attr disabled for add slots button
				$('.time').timepicker({
					timeFormat: 'H:i:s'
				});
			}
		},
		error : function(){
			$('.close-confirm').click();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
	});
}

function bindStaffSchedule(){
	var flag = "false";
		
	if($("#selStaffType").val() == "-1"){
		$("#selStaffType").focus();
		$("#selStaffTypeError").text("Please select staff type");
		$("#selStaffType").addClass("errorStyle");
		flag = "true";
	}
	if($("#selStaffName").val() == "-1"){
		$("#selStaffName").focus();
		$("#selStaffNameError").text("Please select staff name");
		$("#selStaffName").addClass("errorStyle");
		flag = "true";
	}
	if(flag == "true"){
		return false;
	}
	
	var staffTypeId = $("#selStaffType").val();
	var staffId = $("#selStaffName").val();		

	var postData = {
		"operation": "showRepetitive",
		"staffTypeId": staffTypeId,
		"staffId": staffId
	}

	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/staff_working_days.php",
		datatype:"json",
		data: postData,
		success : function(data){
			if(data !=""){
				var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery	
				for (var i = 0; i < parseData.length; i++) {
					editData(i, parseData, '.div' + dayEnum[parseData[i].day]);
					var timeDuration = parseData[i].time_duration;
				}
				$("#selTimeDuration").val(timeDuration);
				$('.time').timepicker({
					timeFormat: 'H:i:s'
				});
			}
		},
		error : function(){
			$('.close-confirm').click();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
	});
}

//function for clear the data
function clear(){
	$("#selStaffName").val("-1");
	$("#selTimeDuration").val("-1");
	$("#selStaffNameError").text("");
	$("#selStaffType").val("-1");
	$("#selStaffTypeError").text("");
	$("#selTimeDurationError").text("");
	$('#lblDesignation').text("N/A");
	$('#lblEmailId').text("N/A");
	$('#lblPhoneNo').text("N/A");
	$('#lblAddress').text("N/A");
}