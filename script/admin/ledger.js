/*
 * File Name    :   diseases.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre opeartion	
 */
var ledgerTable; //define variable for ledgerTable 
$(document).ready(function() {
    debugger;
	loader();
    parentGroup();
    $("#form_diseases").hide();
    $("#diseases_list").css({
        "background": "#fff"
    });
    $("#tab_diseases").css({
        "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
        "color": "#fff"
    });
    $("#tab_add_diseases").click(function() { // show the add diseases tab
        $("#form_diseases").show();
        $(".blackborder").hide();
        $("#tab_add_diseases").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tab_diseases").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#diseases_list").css({
            "background-color": "#fff"
        });clear();
        $("#txtCode").focus();
    });
    $("#tab_diseases").click(function() { // show the diseases list tab
		$('#inactive-checkbox-tick').prop('checked', false).change();
        $("#form_diseases").hide();
        $(".blackborder").show();
        $("#tab_add_diseases").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#tab_diseases").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#diseases_list").css({
            "background": "#fff"
        });
    });
	
		
	
    $("#btnAddDiseases").click(function() { // save details with validation
        var flag = "false";
        if ($("#txtBalance").val() == '') {
            $("#txtBalanceError").text("Please enter balance");
            $("#txtBalance").focus();
            $("#txtBalance").addClass("errorStyle");
            flag = "true";
        } 
        if ($("#selType").val().trim() == '-1') {
            $("#selTypeError").text("Please select type");
            $("#selType").focus();
            $("#selType").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtName").val().trim() == '') {
            $("#txtNameError").text("Please enter name");
            $("#txtName").focus();
            $("#txtName").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtCode").val().trim() == '') {
            $("#txtCodeError").text("Please enter code");
            $("#txtCode").focus();
            $("#txtCode").addClass("errorStyle");
            flag = "true";
        }

        if (flag == "true") {
            return false;
        } 
        else {
            var code = $("#txtCode").val().trim();
            var name = $("#txtName").val().trim();
            var parentId = $("#selParentGroup").val();
            var balance = $("#txtBalance").val().trim();
            var description = $("#txtDescription").val();
            var unit = $("#selUnit").val();
            var type = $("#selType").val();
			description = description.replace(/'/g, "&#39");

           
            //ajax call for insert data into data base
            var postData = {
               "operation": "save",
                Code: code,
                Name: name,
                parentId: parentId,
                balance: balance,
                unit: unit,
                type: type,
                description: description
            }
            $.ajax({
                type: "POST",
                cache: false,
                url: "controllers/admin/ledger.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Ledger saved successfully!!!");
						//$('#inactive-checkbox-tick').prop('checked', false).change();
                        $('#messagemyModal').modal();

                        clear();
                        //after sucessful data sent to database redirect to datatable list
                        $("#form_diseases").hide();
                        $(".blackborder").show();
                        $("#tab_add_diseases").css({
                            "background": "#fff",
                            "color": "#000"
                        });
                        $("#tab_diseases").css({
                            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
                            "color": "#fff"
                        });
                    }
                },
                error: function() {
                    alert('error');
                }
            }); //  end ajax call
        }
    }); //end button click function

    // key up and change functionality for remove style 
    $("#txtCode").keyup(function() {
        if ($("#txtCode").val() != '') {
            $("#txtCodeError").text("");
            $("#txtCode").removeClass("errorStyle");
        }
    });
    $("#txtName").keyup(function() {
        if ($("#txtName").val() != '') {
            $("#txtNameError").text("");
            $("#txtName").removeClass("errorStyle");
        }
    });
    $("#selType").keyup(function() {
        if ($("#selType").val() != '-1') {
            $("#selTypeError").text("");
            $("#selType").removeClass("errorStyle");
        }
    });
    $("#txtBalance").keyup(function() {
        if ($("#txtBalance").val() != '') {
            $("#txtBalanceError").text("");
            $("#txtBalance").removeClass("errorStyle");
        }
    });

    //if ($('.inactive-checkbox').not(':checked')) { // show active details on load 
        //Datatable code
        ledgerTable = $('#tblLedger').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "fnDrawCallback": function(oSettings) {
                //perform update event
				$('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = ledgerTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var code = mData["code"];
                        var name = mData["name"];
                        var cr_balance = mData["cr_balance"];
                        var db_balance = mData["db_balance"];
                        if(cr_balance == "0"){
                            var unit = "1";
                            var balance = db_balance;
                        }
                        else{
                            var unit = "2";
                            var balance = cr_balance;
                        }
                        var parentId = mData["parent_id"];
                        var type = mData["type"];
                        var description = mData["description"];
                        editClick(id, code, name, description, unit, balance, type, parentId);

                    }
                });
                // perform delete event
				$('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = ledgerTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/ledger.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [{
                "mData": "code"
            }, {
                "mData": "name"
            }, {
                "mData": "parent_id"
            }, {
                "mData": function(o) {
                    var type = o['type'];
                    if(type == "1"){
                        type = "Ledger";
                    }
                    else{
                        type = "Group";
                    }
                    return type;
                }
            }, {
                "mData": "cr_balance"
            },  {
                "mData": "db_balance"
            }, {
                "mData": "description"
            }, {
                "mData": function(o) {
                    var data = o;
                    return '<i class="ui-tooltip fa fa-pencil update" style="font-size: 22px;cursor:pointer;" data-original-title="Edit"></i> <i class="ui-tooltip fa fa-trash-o delete" style="font-size: 22px; color:#a94442; cursor:pointer;" data-original-title="Delete"></i>';
                }
            }, ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [7]
            }]

        });
    //}
    /*$('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show inactive details on checked event
            ledgerTable.fnClearTable();
            ledgerTable.fnDestroy();
            ledgerTable = "";
            ledgerTable = $('#tblLedger').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
					$('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = ledgerTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            restoreClick(id);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/ledger.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    //{  "mData": "id" }, 
                    {
                        "mData": "code"
                    }, {
                        "mData": "name"
                    }, {
                        "mData": "parent_id"
                    },{
                        "mData": "cr_balance"
                    },  {
                        "mData": "db_balance"
                    },{
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [3]
                }]
            });
        } else { // show active details unchecked event
            ledgerTable.fnClearTable();
            ledgerTable.fnDestroy();
            ledgerTable = "";
            ledgerTable = $('#tblLedger').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform update event
					$('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = ledgerTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var code = mData["code"];
                            var name = mData["name"];
                            var description = mData["description"];
							editClick(id, code, name, description);

                        }
                    });
                    // perform delere event
					$('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = ledgerTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/ledger.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    //				{  "mData": "id" }, 
                    {
                        "mData": "code"
                    }, {
                        "mData": "name"
                    }, {
                        "mData": "parent_id"
                    },{
                        "mData": "cr_balance"
                    },  {
                        "mData": "db_balance"
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil update" style="font-size: 22px;cursor:pointer;" data-original-title="Edit"></i> <i class="ui-tooltip fa fa-trash-o delete" style="font-size: 22px; color:#a94442; cursor:pointer;" data-original-title="Delete"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [3]
                }]
            });
        }
    });*/
    // perform reset functionality on reset click event	
    $("#btnReset").click(function() {
        $("#txtCategory").removeClass("errorStyle");
        $("#txtCodeError").text("");
        $("#txtCode").removeClass("errorStyle");
        $("#txtNameError").text("");
        $("#txtName").removeClass("errorStyle");
        $("#txtCode").focus();
        clear();
    });

});
// edit data dunction for update 
function editClick(id, code, name, description, unit, balance, type, parentId) {
	
    $('.modal-body').text("");
    $('#myModalLabel').text("Update Ledger");
    $('.modal-body').html($("#popUPForm").html()).show();
	$("#myModal #uploadFile").hide();
    $('#myModal').modal('show');
	$('#myModal').on('shown.bs.modal', function() {
        $("#myModal #txtCode").focus();
		
    });
    $("#myModal #btnUpdate").removeAttr("style");
    $("#myModal #txtCode").removeClass("errorStyle");
    $("#myModal #txtCodeError").text("");
    $("#myModal #txtName").removeClass("errorStyle");
    $("#myModal #txtNameError").text("");

    $('#myModal #txtCode').val(code);
    $('#myModal #txtName').val(name);
    $('#myModal #selUnit').val(unit);
    $('#myModal #selUnit').attr("disabled","disabled");
    $('#myModal #txtBalance').val(balance);
    $('#myModal #selType').val(type);
    if(parentId==null){
        $('#myModal #selParentGroup').val("-1");
    }
    else{
        $('#myModal #selParentGroup').val(parentId);
    }
	$('#myModal #txtDescription').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);

    // key up validation
    $("#myModal #txtCode").keyup(function() {
        if ($("#myModal #txtCode").val() != '') {
            $("#myModal #txtCodeError").text("");
            $("#myModal #txtCode").removeClass("errorStyle");
        }
    });
    $("#myModal #txtName").keyup(function() {
        if ($("#myModal #txtName").val() != '') {
            $("#myModal #txtNameError").text("");
            $("#myModal#txtName").removeClass("errorStyle");
        }
    });
    //validation
    $("#myModal #btnUpdate").click(function() { // click update button
        var flag = "false";
        if ($("#myModal #txtBalance").val() == '') {
            $("#myModal #txtBalanceError").text("Please enter balance");
            $("#myModal #txtBalance").focus();
            $("#myModal #txtBalance").addClass("errorStyle");
            flag = "true";
        } 
        if ($("#myModal #selType").val().trim() == '-1') {
            $("#myModal #selTypeError").text("Please select type");
            $("#myModal #selType").focus();
            $("#myModal #selType").addClass("errorStyle");
            flag = "true";
        }
        if ($("#myModal #txtName").val().trim() == '') {
            $("#myModal #txtNameError").text("Please enter name");
            $("#myModal #txtName").focus();
            $("#myModal #txtName").addClass("errorStyle");
            flag = "true";
        }
        if ($("#myModal #txtCode").val().trim() == '') {
            $("#myModal #txtCodeError").text("Please enter code");
            $("#myModal #txtCode").focus();
            $("#myModal #txtCode").addClass("errorStyle");
            flag = "true";
        }


        if (flag == "true") {
            return false;
        }
		else {
            var code = $("#myModal #txtCode").val().trim();
            var name = $("#myModal #txtName").val().trim();
            var parentId = $("#myModal #selParentGroup").val();
            var balance = $("#myModal #txtBalance").val().trim();
            var description = $("#myModal #txtDescription").val();
            var unit = $("#myModal #selUnit").val();
            var type = $("#myModal #selType").val();
            description = description.replace(/'/g, "&#39");

           
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				
				var postData = {
					"operation": "update",
                    Code: code,
                    Name: name,
                    parentId: parentId,
                    balance: balance,
                    unit: unit,
                    type: type,
                    description: description,
					Id: id
				}
				$.ajax( //ajax call for update data
				{
					type: "POST",
					cache: false,
					url: "controllers/admin/ledger.php",
					datatype: "json",
					data: postData,

					success: function(data) {
						if (data != "0" && data != "") {
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Ledger updated successfully!!!");
							$('#messagemyModal').modal();
							ledgerTable.fnReloadAjax();
							clear();

						}
					},
					error: function() {
						$('.close-confirm').click();
						//$('#myModal').modal('hide');
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
				}); // end of ajax
			});
        }
    });
} // end update button

function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Ledger");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);

    var type = "delete";
    $('#confirm').attr('onclick', 'deleteDiseases("' + type + '");');
} // end click fucntion

/*function restoreClick(id) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Ledger");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteDiseases("' + type + '");');
}*/
// define function for restore and delete when this is call 
function deleteDiseases(type) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax( // ajax call for delete
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/ledger.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Ledger deleted successfully!!!");
                        $('#messagemyModal').modal();
                        ledgerTable.fnReloadAjax();
                    } 
                    else {
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Sorry");
                        $('.modal-body').text("This diseases is used , so you can not delete!!!");
                        $('#messagemyModal').modal();
                    }
                },
                error: function() {
					
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
            }); // end ajax 
    } 
   /* else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/ledger.php",
            data: {
                "operation": "restore",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Ledger restored successfully!!!");
                    $('#messagemyModal').modal();
                    ledgerTable.fnReloadAjax();
                }
                if (data == "0") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("It can not be restore!!!");
                    ledgerTable.fnReloadAjax();
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    }*/
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});

// Ajax call for show data on Diseases Category
function parentGroup(){
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/ledger.php",
        data: {
            "operation": "showParentGroup"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selParentGroup').html(option);
            }
        },
        error: function() {}
    }); 
}
    

// define clear function
function clear() {
    $('#txtCode').val("");
    $('#txtCodeError').text("");
    $('#txtCode').removeClass("errorStyle");
    $('#selType').val("-1");
    $('#selTypeError').text("");
    $('#selType').removeClass("errorStyle");
    $('#txtBalance').val("");
    $('#txtBalanceError').text("");
    $('#txtBalance').removeClass("errorStyle");
    $('#txtName').val("");
    $('#txtNameError').text("");
    $('#txtName').removeClass("errorStyle");
    $('#txtDescription').val("");
}