var sellerTable;
var pendingInvoiceTable;
var paymentHistoryTable;
var orderHistoryTable;
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   seller.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   24th feb, 2016
 * Description  :   This page  manages seller name data
 *************************************************************************************************** */
    loader();
    debugger;
    removeErrorMessage();
	loadVendorType();

    pendingInvoiceTable =  $('#pendingInvoiceTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth":false
    });

    paymentHistoryTable =  $('#paymentHistoryTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth":false
    });

    orderHistoryTable =  $('#orderHistoryTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth":false
    });

	$("#form_dept").hide();
    $("#drug_list").addClass('list');    
    $("#tab_drug").addClass('tab-list-add');
	
	
	//This click function is used for change the tab
    $("#tab_add_drug").click(function() {
        clearFormDetails(".ClearForm");
  		showAddTab();
		loadVendorType();
    });

	//This click function is used for show the seller name list
    $("#tab_drug").click(function() {		
        tabSellerList();//Calling this function for show the list 
    });

	// import excel on submit click
	$('#importDataFile').click(function(){
		var flag = false;
		if($('#file').prop('files')[0] == undefined || $('#file').val() == "") {
			$('#txtFileError').show();
			$('#txtFileError').text("Please choose file.");
			flag  = true;
		}
		if(flag == true){
			return false;
		}
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to upload this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
			var file_data = $('#file').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: 'controllers/admin/seller.php', // point to server-side PHP script 
				dataType: 'text', // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					if(data != "" && data != "invalid file" && data == "1"){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Your file has been successfully imported!!!");
						$('#messagemyModal').modal();
						$('#file').val("");
					}
					else if (data =="You must have four column in your file i.e vendor name, firm name, vendor email and description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="First column should be supplier name" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Second column should be firm name" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Third column should be supplier email" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Fourth column should be description" ) {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data =="Please insert data in first , second , third and fourth column only i.e A , B , C and D") {
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if(data == "Data can't be null in vendor name ,firm name and vendor email"){
                        $('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
                    else if (data == "Email Id is not valid") {
                    	$('#txtFileError').show();
                        $('#txtFileError').text(data);
                        $('#file').val("");
                    }
					else if(data == "invalid file"){
						$('#txtFileError').show();
						$('#txtFileError').text("Please import only CSV/XLS file.");
						$('#file').val("");
					}
					else if(data == ""){
						$('#txtFileError').show();
						$('#txtFileError').text("Data already exist.");
						$('#file').val("");
					}
				},
				error: function() {
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Data Not Found!!!");
					$('#messagemyModal').modal();
				}
			});
		});
	});	
	
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
			sellerTable = $('#tblSeller').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {	
					$('.update').unbind();				
					$('.update').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData = sellerTable.fnGetData(data);
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var name = mData["name"];
							var email = mData["email"];
							var firmName = mData["firm_name"];
							var phone = mData["phone"];
							var code = mData["ifsc_code"];
							var bankName = mData["bank_name"];
							var accountNo = mData["account_no"];
							var address = mData["address"];
							var vendorType = mData["vendor_type_id"];
							var description = mData["description"];
							editClick(id,name,email,firmName,phone,code,bankName,accountNo,address,vendorType,description); //Calling this function for update the data 
						}
					});
					$('.delete').unbind();
					$('.delete').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  sellerTable.fnGetData(data);
						
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							deleteClick(id);//Calling this function for delete the data 
						}
					});	
				},
				
				"sAjaxSource":"controllers/admin/seller.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "show" });
				},
				"aoColumns": [
					{ "mData": "name" },
					{ "mData": "email" },
					{ "mData": "firm_name" },
					{ "mData": "description" }, 
					{
						"mData": function (o) { 
						return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
						   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
						   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
						   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
						   "  data-original-title='Delete'></i>"; 
						}	
					},	
				],
				aoColumnDefs: [
			        { 'bSortable': false, 'aTargets': [ 3 ] },
			        { 'bSortable': false, 'aTargets': [ 4 ] }
			    ]
			} );
    }
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	sellerTable.fnClearTable();
	    	sellerTable.fnDestroy();
	    	//Datatable code
			sellerTable = $('#tblSeller').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.restore').unbind();
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  sellerTable.fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);//Calling this function for restore the data
						}						
					});
				},
				
				"sAjaxSource":"controllers/admin/seller.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					{ "mData": "name" },
					{ "mData": "email" },
					{ "mData": "firm_name" },
					{ "mData": "description" }, 
					{
					  "mData": function (o) { 
					   		return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; 
					   }		
					},	
				],
				aoColumnDefs: [
			        { 'bSortable': false, 'aTargets': [ 3 ] },
			        { 'bSortable': false, 'aTargets': [ 4 ] }
			    ]
			} );
		}
		else{
			sellerTable.fnClearTable();
	    	sellerTable.fnDestroy();
	    	//Datatable code
			sellerTable = $('#tblSeller').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					$('.update').unbind();
					$('.update').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData = sellerTable.fnGetData(data);
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var name = mData["name"];
							var email = mData["email"];
							var firmName = mData["firm_name"];
							var phone = mData["phone"];
							var code = mData["ifsc_code"];
							var bankName = mData["bank_name"];
							var accountNo = mData["account_no"];
							var address = mData["address"];
							var vendorType = mData["vendor_type_id"];
							var description = mData["description"];
							editClick(id,name,email,firmName,phone,code,bankName,accountNo,address,vendorType,description); //Calling this function for update the data 
						}
					});
					$('.delete').unbind();
					$('.delete').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  sellerTable.fnGetData(data);
						
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							deleteClick(id);//Calling this function for delete the data 
						}
					});
				},
				
				"sAjaxSource":"controllers/admin/seller.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "show" });
				},
				"aoColumns": [
					{ "mData": "name" },
					{ "mData": "email" },
					{ "mData": "firm_name" },
					{ "mData": "description" }, 
					{
						"mData": function (o) { 
						return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
						   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
						   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
						   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
						   "  data-original-title='Delete'></i>"; 
						}
					},	
				],
				aoColumnDefs: [
			        { 'bSortable': false, 'aTargets': [ 3 ] },
			        { 'bSortable': false, 'aTargets': [ 4 ] }
			    ]
			} );
		}
    });
	
	//This click function is used for validate and save the data on save button
    $("#btnSave").click(function() {
		
		var flag = "false";
		
		$("#txtVendorName").val($("#txtVendorName").val().trim());
		
		/*if ($("#txtAccountNo").val() == '') {
        	$("#txtAccountNo").focus();
            $("#txtAccountNoError").text("Please enter account no");
            $("#txtAccountNo").addClass("errorStyle");
            flag = "true";
        }

        if ($("#txtBankName").val() == '') {
        	$("#txtBankName").focus();
            $("#txtBankNameError").text("Please enter bank name");
            $("#txtBankName").addClass("errorStyle");
            flag = "true";
        }
        
        if ($("#txtCode").val() == '') {
        	$("#txtCode").focus();
            $("#txtCodeError").text("Please enter code");
            $("#txtCode").addClass("errorStyle");
            flag = "true";
        }*/

        if ($("#txtAddress").val() == '') {
        	$("#txtAddress").focus();
            $("#txtAddressError").text("Please enter address");
            $("#txtAddress").addClass("errorStyle");
            flag = "true";
        }

        if ($("#txtPhone").val() == '') {
        	$("#txtPhone").focus();
            $("#txtPhoneError").text("Please enter phone number");
            $("#txtPhone").addClass("errorStyle");
            flag = "true";
        }

        if (validateEmail($("#txtSellerEmail").val(),"txtSellerEmail","txtSellerEmailError","Please enter valid email") == true) {
        	flag = "true";
        }
        if ($("#txtSellerEmail").val() == '') {
        	$("#txtSellerEmail").focus();
            $("#txtSellerEmailError").text("Please enter email");
            $("#txtSellerEmail").addClass("errorStyle");
            flag = "true";
        }

        if ($("#txtFirmName").val() == '') {
        	$("#txtFirmName").focus();
            $("#txtFirmNameError").text("Please enter firm name");
            $("#txtFirmName").addClass("errorStyle");
            flag = "true";
        }

        if ($("#selVendorType").val() == '') {
        	$("#selVendorType").focus();
            $("#selVendorTypeError").text("Please select vendor type");
            $("#selVendorType").addClass("errorStyle");
            flag = "true";
        }

        if ($("#txtVendorName").val() == '') {
        	$("#txtVendorName").focus();
            $("#txtVendorNameError").text("Please enter vendor name");
            $("#txtVendorName").addClass("errorStyle");
            flag = "true";
        }
		if(flag == "true"){			
			return false;
		}
		
		//ajax call
		
		var sellerName = $("#txtVendorName").val().trim();
		var description = $("#txtDescription").val();
		var firmName = $("#txtFirmName").val().trim();
		var sellerEmail = $("#txtSellerEmail").val().trim(); 
		var phone = $("#txtPhone").val();
		var address = $("#txtAddress").val();
		var vendorType = $("#selVendorType").val();
		var code = $("#txtCode").val();
		var bankName = $("#txtBankName").val();
		var accountNo = $("#txtAccountNo").val();
		description = description.replace(/'/g, "&#39");
		var postData = {
			"operation":"save",
			"sellerName":sellerName,
			"firmName" : firmName,
			"sellerEmail" : sellerEmail,
			"phone" : phone,
			"address" : address,
			"vendorType" : vendorType,
			"code" : code,
			"bankName" : bankName,
			"accountNo" : accountNo,
			"description":description
		}
		
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/seller.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Vendor name saved successfully!!!");
					$('#messagemyModal').modal();
					$('#inactive-checkbox-tick').prop('checked', false).change();
					tabSellerList();
					loadVendorType();
				}				
				else{
					$("#txtVendorNameError").text("Vendor name is already exists");
					$("#txtVendorName").addClass("errorStyle");
					$("#txtVendorName").focus();
				}				
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});		
    });//Close save button
	
	//Click function is used for reset button
    $("#btnReset").click(function(){
    	$("#txtSellerName").focus();
    	$("#txtFileError").text('');
    	clearFormDetails(".ClearForm");
    });
});//Close document.ready function

//This function will call on edit button
function editClick(id,name,email,firmName,phone,code,bankName,accountNo,address,vendorType,description){
 	showAddTab()
    $("#btnReset").hide();
    $("#btnSave").hide();
    $('#btnUpdate').show();
    $('#tab_add_drug').html("+Update Vendor");	
    $("#txtVendoName").focus();
    $(".showCustomerTabs").show();
   
	$("#uploadFile").hide();
	$('#txtVendorName').val(name);
	$('#txtFirmName').val(firmName);
	$('#txtSellerEmail').val(email);
	$('#txtPhone').val(phone);
	$('#txtAddress').val(address);
	$('#txtCode').val(code);
	$('#txtBankName').val(bankName);
	$('#txtAccountNo').val(accountNo);
	$('#selVendorType').val(vendorType);
	$('#txtDescription').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id);
	
	//Click function for update button
	$("#btnUpdate").click(function(){
		var flag = "false";
		$("#txtVendorName").val($("#txtVendorName").val().trim());

		/*if ($("#myModal #txtAccountNo").val() == '') {
        	$("#myModal #txtAccountNo").focus();
            $("#myModal #txtAccountNoError").text("Please enter account no");
            $("#myModal #txtAccountNo").addClass("errorStyle");
            flag = "true";
        }

        if ($("#myModal #txtBankName").val() == '') {
        	$("#myModal #txtBankName").focus();
            $("#myModal #txtBankNameError").text("Please enter bank name");
            $("#myModal #txtBankName").addClass("errorStyle");
            flag = "true";
        }
        
        if ($("#myModal #txtCode").val() == '') {
        	$("#myModal #txtCode").focus();
            $("#myModal #txtCodeError").text("Please enter code");
            $("#myModal #txtCode").addClass("errorStyle");
            flag = "true";
        }*/

        if ($("#txtAddress").val() == '') {
        	$("#txtAddress").focus();
            $("#txtAddressError").text("Please enter address");
            $("#txtAddress").addClass("errorStyle");
            flag = "true";
        }

        if ($("#txtPhone").val() == '') {
        	$("#txtPhone").focus();
            $("#txtPhoneError").text("Please enter phone number");
            $("#txtPhone").addClass("errorStyle");
            flag = "true";
        }

		if (validateEmail($("#txtSellerEmail").val(),"txtSellerEmail","txtSellerEmailError","Please enter valid email") == true) {
        	flag = "true";
        }
        if ($("#txtSellerEmail").val() == '') {
        	$("#txtSellerEmail").focus();
            $("#txtSellerEmailError").text("Please enter email");
            $("#txtSellerEmail").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtFirmName").val() == '') {
        	$("#txtFirmName").focus();
            $("#txtFirmNameError").text("Please enter firm name");
            $("#txtFirmName").addClass("errorStyle");
            flag = "true";
        }
		if ($("#txtVendorName").val() == '') {
			$("#txtVendorName").focus();
			$("#txtVendorNameError").text("Please enter vendor name");
			$("#txtVendorName").addClass("errorStyle");
			flag = "true";
		}
		if(flag == "true"){			
			return false;
		}
		else{


			var sellerName = $("#txtVendorName").val().trim();
			var description = $("#txtDescription").val();
			var firmName = $("#txtFirmName").val().trim();
			var sellerEmail = $("#txtSellerEmail").val().trim(); 
			var phone = $("#txtPhone").val();
			var address = $("#txtAddress").val();
			var vendorType = $("#selVendorType").val();
			var code = $("#txtCode").val();
			var bankName = $("#txtBankName").val();
			var accountNo = $("#txtAccountNo").val();
			description = description.replace(/'/g, "&#39");
			var id = $('#selectedRow').val();
			var postData = {
				"operation":"update",
				"id" : id,
				"sellerName":sellerName,
				"firmName" : firmName,
				"sellerEmail" : sellerEmail,
				"phone" : phone,
				"address" : address,
				"vendorType" : vendorType,
				"code" : code,
				"bankName" : bankName,
				"accountNo" : accountNo,
				"description":description
			}
			var sellerName = $("#txtVendoName").val();
			var description = $("#txtDescription").val();
			description = description.replace(/'/g, "&#39");
				
			var firmName = $("#txtFirmName").val().trim();
			var sellerEmail = $("#txtSellerEmail").val().trim();
			$('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
				$.ajax({
					type: "POST",
					cache: "false",
					url: "controllers/admin/seller.php",
					datatype:"json",
					data :postData,

					success: function(data) {	
						if(data != "0" && data != ""){
							$('#myModal').modal('hide');
							$('.close-confirm').click();
							$('.modal-body').text("");
							$('#messageMyModalLabel').text("Success");
							$('.modal-body').text("Vendor name updated successfully!!!");
							sellerTable.fnReloadAjax();
							$('#messagemyModal').modal();
							clearFormDetails(".ClearForm");
							tabSellerList();
						}
						else{
							callSuccessPopUp("Alert","Either name or email id already exist");
						}
					},
					error:function()
					{
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();			  
					}
				});
			});
		}
	});
	
	$(".customerBtn").click(function(){
        $(".customerBtn").removeClass('activeSelf');
        $(this).addClass('activeSelf');
        $(".tabScreen").addClass('hide');
        if ($(this).attr("attr") == "mainScreen") {
            $("#mainScreen").removeClass('hide');
        }
        else if($(this).attr("attr") == "pendingInvoiceScreen"){
            $("#pendingInvoiceScreen").removeClass('hide');
        }
        else if($(this).attr("attr") == "paymentHistoryScreen"){
            $("#paymentHistoryScreen").removeClass('hide');
        }
        else{
        	 $("#orderHistoryScreen").removeClass('hide');
        }
    });

    $($(".customerBtn")[0]).addClass("activeSelf");


    /*var postData = {
		"operation" : "showCustomerDetails",
		customerId : $('#selectedRow').val()
	}

	dataCall("controllers/admin/receipt_number.php", postData, function (result){
		var parseData = JSON.parse(result);
		if (parseData != '') {		
			receiptNumberTable.fnClearTable();
			var id = '';
			var receiptNumber;
			totalamount = 0;
			openAmount = 0;
			tableOpenAmount = [];
			$.each(parseData,function(i,v){
				var obj = {};
				receiptNumber = v.id;
				var prefix = v.invoice_prefix;
				var idLength = 6-receiptNumber.length;
				for(j=0; j<idLength; j++){
					receiptNumber="0"+receiptNumber;
				}
				receiptNumber=prefix+receiptNumber;
	            if (v.id !=id) {
	            	
	                receiptNumberTable.fnAddData(["<input type='checkbox'  class='chkInvoice chkInvoice"+v.id+"' data-id="+v.id+" style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;'"+					 
								   " style='font-size: 22px; cursor:pointer;'>"+"<span class='details-control' onclick='showRows($(this));'></span>"+"<span class='hide-details-control hide' onclick='hideRows($(this));'></span>",receiptNumber,v.description,v.total_bill,v.total_bill,v.billing_date]);

	                var currData = receiptNumberTable.fnAddData(["<input type='checkbox' tr-class="+receiptNumber+" data-class='chkInvoice"+v.id+"'  class='chkProduct' data-id="+v.id+"  productId="+v.customer_product_id+"  style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative; margin-left: 42%;'"+					 
								   " style='font-size: 22px; cursor:pointer;'>",v.name,"",v.total,v.total,v.billing_date]);

	                var getCurrDataRow = receiptNumberTable.fnSettings().aoData[ currData ].nTr;
	                $(getCurrDataRow).addClass('hide '+(receiptNumber).replace(/ /g,''));
	                id = v.id;
	                grandTotal += parseFloat(v.total);
	                if(i != 0){
	                	obj.openAmount = openAmount;
	                	tableOpenAmount.push(obj);
	                	openAmount =0;
	                }
	                openAmount += parseFloat(v.total);
	            }

	            else {		            	

	                var currData = receiptNumberTable.fnAddData(["<input type='checkbox' tr-class="+receiptNumber+" data-class='chkInvoice"+v.id+"' class='chkProduct' data-id="+v.id+" productId="+v.customer_product_id+"  style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;  margin-left: 42%;'"+					 
								   " style='font-size: 22px; cursor:pointer;'>",v.name,"",v.total,v.total,v.billing_date]);

	                var getCurrDataRow = receiptNumberTable.fnSettings().aoData[ currData ].nTr;
	                $(getCurrDataRow).addClass('hide '+(receiptNumber).replace(/ /g,''));
	                grandTotal += parseFloat(v.total);
	                 openAmount += parseFloat(v.total);
	                id = v.id;
	            }
	             if(i == parseData.length-1){
	             	obj = {};
                	obj.openAmount = openAmount;
                	tableOpenAmount.push(obj);
                	openAmount =0;
					k=1;
                }
	        });	
	        if(k == 1){
        		var airows = receiptNumberTable.$(".chkInvoice:unchecked", {"page": "all"});	//Select all unchecked row only
	        	for(i=0; i<airows.length; i++){
	        		$($(airows[i]).parent().siblings()[3]).text(tableOpenAmount[i].openAmount);
	        	}
        	}				
			$("#txtBalance").val(grandTotal);
		}
		else{
			callSuccessPopUp("Alert","No data exist for choosen id.");
			$("#lblFirstName").text("");
			$("#lblLastName").text("");
			$("#lblEmail").text("");
			$("#lblPhone").text('');
		}
	});*/
	
	removeErrorMessage();
}

//Click function for delete the seller
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete vendor name");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteSeller("'+type+'");');
}

//Click function for restore the seller
function restoreClick(id){
	$('#selectedRow').val(id);
	$('.modal-body').text("");	
	$('#confirmMyModalLabel').text("Restore vendor name");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();
	
	var type="restore";
	$('#confirm').attr('onclick','deleteSeller("'+type+'");');
}
//function for delete the seller
function deleteSeller(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
			
		//Ajax call for delete the seller 	
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/seller.php",
			data :{            
				"operation" : "delete",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Vendor name deleted successfully!!!");
					sellerTable.fnReloadAjax();
					$('#messagemyModal').modal();
					 
				}
			},
			error:function()
			{
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	}
	else{
		var id = $('#selectedRow').val();
	
		//Ajax call for restore the seller 
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/seller.php",
			data :{            
				"operation" : "restore",
				"id" : id
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Vendor name restored successfully!!!");
					sellerTable.fnReloadAjax();
					$('#messagemyModal').modal();
					clearFormDetails(".ClearForm");
				}			
			},
			error:function()
			{
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	}
}

//This function is used for show the list of seller name
function tabSellerList(){
	$("#form_dept").hide();
	$(".blackborder").show();
	$('#inactive-checkbox-tick').prop('checked', false).change();
	clearFormDetails(".ClearForm");
   
    $("#tab_add_drug").removeClass('tab-detail-add');
    $("#tab_add_drug").addClass('tab-detail-remove');
    $("#tab_drug").removeClass('tab-list-remove');    
    $("#tab_drug").addClass('tab-list-add');
    $("#drug_list").addClass('list');
  	$("#uploadFile").show();
    $("#btnReset").show();
    $("#btnSave").show();
    $('#btnUpdate').hide();
    $(".showCustomerTabs").hide();
    $('#tab_add_drug').html("+Add Vendor");
	
}
function showAddTab(){
    $("#form_dept").show();
    $(".blackborder").hide();

    $("#tab_add_drug").addClass('tab-detail-add');
    $("#tab_add_drug").removeClass('tab-detail-remove');
    $("#tab_drug").removeClass('tab-list-add');
    $("#tab_drug").addClass('tab-list-remove');
    $("#drug_list").addClass('list');
	$("#txtVendorNameError").text("");
    $("#txtVendorName").removeClass("errorStyle");
    $('#txtVendorName').focus();
}

function loadVendorType(){
	var postData = {
		operation : "showVendorType"
	}

	dataCall("controllers/admin/seller.php", postData, function (result){
		var parseData = JSON.parse(result);
		var option = "<option value=''>--Select--</option>";
		if (parseData != '') {		
			for (var i=0;i<parseData.length;i++){				

				option += "<option value='" + parseData[i].id + "'>"  + parseData[i].type + "</option>";
			}
			$("#selVendorType").html(option);
		}
		else{
			$("#selVendorType").html(option);
		}
	});
}

function showRows(myThis){
    var ledgerText = $(myThis).parent().siblings()[0];
    var ledgerClass = $(ledgerText).text().replace(/ /g,'');
    var parentTr = $(myThis).parent().parent()[0];
    var trClass = $(parentTr).parent().find("."+ledgerClass);
    $(parentTr).parent().find("."+ledgerClass).removeClass('hide');
    $(myThis).addClass('hide');
    $(myThis).siblings().removeClass('hide');
}
function hideRows(myThis){
    var ledgerText = $(myThis).parent().siblings()[0];
    var ledgerClass = $(ledgerText).text().replace(/ /g,'');
    var parentTr = $(myThis).parent().parent()[0];
    var trClass = $(parentTr).parent().find("."+ledgerClass);
    $(parentTr).parent().find("."+ledgerClass).addClass('hide');
    $(myThis).addClass('hide');
    $(myThis).siblings().removeClass('hide');
}

// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		$(".close").click();
    }
});