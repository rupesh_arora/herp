/* var printData = new Array();
var bigPrintData =new Array(); */
var printData = [];
var reciptNumber;
var dataSet;
var globalVisitId ;
var globalPatientid;
var globalPatientName;
var calRowLenght;
var totalBillTable;
var table;
var printDiv;
var visitPaymentMode;
var cashPaymentoTable; //Initialize this variable for data table 
var finalBalance;
$(document).ready(function(){
/* ****************************************************************************************************
 * File Name    :   cash_payment.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages cash payment
 *************************************************************************************************** */	
 debugger;
 	getAmountPrefix();
	loader();
	
	// bind nsurance company
    bindInsuranceCompany();
	$(".insurance").hide();
    // bind company schemeon change company
    $("#selInsuranceCompany").change(function() {
        var option = "<option value='-1'>--Select--</option>";
        
        if ($("#selInsuranceCompany :selected") != "") {
            var value = $("#selInsuranceCompany :selected").val();
            
            //on change call this function for load state
            bindSchemeName(value, null); 
        } else {
            $("#selInsurancePlan").html(option);
        }   

        if ($("#selInsuranceCompany").val() != "") {
            $("#selInsuranceCompanyError").text("");
            $("#selInsuranceCompany").removeClass("errorStyle");
        }
    }); 
    
    // bind company schemeon change company
    $("#selInsurancePlan").change(function() {
        var option = "<option value='-1'>--Select--</option>";
        
        if ($("#selInsurancePlan :selected") != "") {
            var value = $("#selInsurancePlan :selected").val();
            
            //on change call this function for load state
            bindPlanName(value, null); 
        } else {
            $("#selInsuranceScheme").html(option);
        }   

        if ($("#selInsurancePlan").val() != "") {
            $("#selInsurancePlanError").text("");
            $("#selInsurancePlan").removeClass("errorStyle");
        }
    });
	$("#txtVisit").focus();
	$("#divOutstandingBalance").hide();//Hide outstanding div when payment mode = cash account
	currentDate();	//This is used for date picker 
	$('#txtPayDate').datepicker({
		dateFormat: 'yy-mm-dd',
		yearRange: "1901:" + new Date().getFullYear(),
		changeYear: true,
		changeMonth: true,
		autoclose: true
	});
	
	//Initialize the data table on document.ready 
	cashPaymentoTable = $('#tblCashPayment').dataTable( {
		"bFilter": true,
		"processing": true,
		 "bPaginate": false,
		"bAutoWidth" : false,
		aoColumnDefs: [{'bSortable': false,'aTargets': [5]}],
	});
	
	//This change function is used for select the payment mode 
	$("#selModeOfPayment").change(function(){
		if($("#selModeOfPayment").val() == "2"){			
			$("#divOutstandingBalance").show();
			$("#divAmountTendered").hide();
		}
		else{
			$("#divOutstandingBalance").hide();
			$("#divAmountTendered").show();
		}
	});
	//paymentModeChange("#selModeOfPayment");
	
	//Click function for select button
	$("#btnSelect").click(function(){
		var flag = "false";
		var visitId = $("#txtVisit").val();
		if($("#txtVisit").val()==""){
			$("#txtVisit").focus();
			$("#txtVisitError").text("Please enter visit id");
			$("#txtVisit").addClass("errorStyle");       
			flag = "true";
		}
		
		if(visitId.length != 9){
			$("#txtVisit").focus();
			$("#txtVisitError").text("Please enter valid visit id");
			$("#txtVisit").addClass("errorStyle"); 
			flag = "true";
		}
        if (flag == "true") {
            return false;
        }
        var visitId = $("#txtVisit").val();
		var visitPrefix = visitId.substring(0, 3);
		visitId = visitId.replace ( /[^\d.]/g, '' ); 
		visitId = parseInt(visitId);
		
		
		var postData = {
			"operation":"showData",
			"visitId":visitId,
			"visitPrefix":visitPrefix
		}
		
		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/cash_payment.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != "" && data != "2"){
					$.session.set('accountBill', 0);
					$.session.set('bill', 0);
					var parseData = jQuery.parseJSON(data);
					var length = parseData.length - 1;				
					for (var i=0;i<parseData.length-1;i++) {
						var patientId = parseData[i].patient_id;
						visitPaymentMode = parseData[i].payment_mode;
						var patientPrefix = parseData[i].patient_prefix;						
						$("#txtName").val(parseData[i].name);
						$("#hdnFamilyMemberId").val(parseData[i].fan_id);
						time = parseData[i].created_on;
						var date = new Date(time*1000);
					} 
					var patientIdLength = patientId.length;
					for (var i=0;i<6-patientIdLength;i++) {
						patientId = "0"+patientId;
					}
					patientId = patientPrefix+patientId;
					$("#txtPatientId").val(patientId);
					var accountBalance = parseData[length].amount;
					if(accountBalance != null){
						$.session.set('accountBill', accountBalance);
						$("#txtOutstandingBalance").val(accountBalance);
					}
					else{
						$.session.set('accountBill', 0);
						$("#txtOutstandingBalance").val(0);
					}
					var get_date = date.getDate();
					var year = date.getFullYear();
					var month = date.getMonth() + 1;
					if(month.toString().length == 1){
						month = "0"+month;
					}
					if(get_date.toString().length == 1){
						get_date = "0"+get_date;
					}
 
					$("#txtVisitDate").val( year+"-" + month + "-" + get_date);
					$("#txtVisitError").text("");
					$("#txtVisit").removeClass("errorStyle");
					$("#txtVisit").attr("disabled", "disabled");					
					if(visitPaymentMode == 'insurance') {
						$("#selModeOfPayment").val('1');
						getInsuranceValue('Insurance');
					}else if(visitPaymentMode == 'cash') {
						$("#selModeOfPayment").val('1');
					}
					else{
						$("#selModeOfPayment").val('-1');
					}	
				}
				if(data == "0" || data == "" || data == "2"){
					$("#txtVisit").focus();
					$("#txtVisitError").text("Please enter valid visit id");
					$("#txtVisit").addClass("errorStyle");
				}
				currentDate();
				
				var postData = {
					"operation":"showTableData",
					"visitId":visitId,
					"visitPrefix":visitPrefix
				}
				
				$.ajax({
					type: "POST",
					cache: false,
					url: "controllers/admin/cash_payment.php",
					datatype:"json",
					data: postData,
					
					success: function(data) {
						dataSet = JSON.parse(data);
						cashPaymentoTable.fnClearTable();
						cashPaymentoTable.fnDestroy();
						finalBalance = 0;
						cashPaymentoTable = $('#tblCashPayment').DataTable( { //Reinitialize the data table 
							"bSort": false,
							"bPaginate": false,
							"bAutoWidth" : false,
							"fnDrawCallback": function ( oSettings ) {
								var cells = 0;
								var totalCells = 0;
								var cashTotal = 0;
								var insuranceTotal = 0;
								var copay = 0;
								 
								var copayType = $("#selInsuranceScheme").attr("data-copay-name");
								var copayValue = parseFloat($("#selInsuranceScheme").attr("data-copay-value"));
								
								calRowLenght = cashPaymentoTable.fnGetNodes();
								for(var i=2;i<=calRowLenght.length + 1;i++)
								{	
									
									var tempCell = $($(".tdTotal")[i]).text();
									cells += parseFloat(tempCell);
									var paymentType = $($(".chkbox")[i-2]).attr('data-paymentmode');
									if(paymentType == 'insurance') {
										insuranceTotal += parseFloat(tempCell);
									}
									else{
										cashTotal += parseFloat(tempCell);
									}									
								} 
								if(copayType == 'Percentage'){
									copay =  parseFloat(insuranceTotal * copayValue / 100);
									insuranceTotal = parseFloat(insuranceTotal) - copay;	
								}
								else if(copayType == 'None'){
									insuranceTotal = 0;		
								}
								else if(copayType == 'Fixed'){
									if(copayValue >= insuranceTotal) {
										insuranceTotal = 0;
									}
									else{
										insuranceTotal = insuranceTotal - copayValue;
									}
								}
								$("#amountPrefix").text(' '+amountPrefix);
								$("#amountPrefixOutStanding").text(amountPrefix);
								$("#amountPrefixCopay").text(amountPrefix);
								$("#amountPrefixTotalBill").text(amountPrefix);

								totalCells = totalCells + cashTotal + copay;
								$("#txtTotalBill").val(totalCells);
								$("#hdnTotalBill").val(totalCells);
								finalBalance = totalCells;
								$.session.set('bill', totalCells);
								$("#txtAmountTendered").val(totalCells);
								$("#totalBill").text(cells);								
								$("#txtCopay").val('-'+insuranceTotal);								
								table = totalCells;
								//change function for checkbox in data table
								$(".chkbox").on('change',function(){
									var newTotal = 0;
									var amount = parseFloat($(this).attr('data-amount'));
									var newCopay =  parseFloat(amount *copayValue / 100)
									var insuranceAmount = amount - newCopay ;
									var checkBox = $(this);
									var checkBoxTd = $(this).parent();
									var paymentMode = $(this).attr('data-paymentMode');
									if($(this).is(":checked")){
										$(this).closest("tr").css('background-color','#ff6666');
										cells = cells - amount;
										newTotal = parseFloat($("#txtAmountTendered").val()) - amount;
										if(paymentMode == 'insurance') {
											if(copayType == 'Percentage'){
												insuranceTotal = insuranceTotal - insuranceAmount;
												copay = copay- newCopay	;								
												$("#txtCopay").val('-'+insuranceTotal);
											}
											totalCells = parseFloat($("#txtAmountTendered").val())-newCopay;
											$("#txtTotalBill").val(totalCells);
											$("#hdnTotalBill").val(totalCells);
											$.session.set('bill', totalCells);
											$("#txtAmountTendered").val(totalCells);
											table = totalCells;
										}
										else{
											$("#txtTotalBill").val(newTotal);
											$("#hdnTotalBill").val(newTotal);
											$.session.set('bill', newTotal);
											$("#txtAmountTendered").val(newTotal);
											table = newTotal;
										}										
										$("#totalBill").text(cells);
									}
									else{
										$(this).closest("tr").css('background-color','#E7E3E4');
										cells = cells + amount;

										newTotal = parseFloat($("#txtAmountTendered").val())  + amount;

										if(paymentMode == 'insurance') {
											if(copayType == 'Percentage'){
												insuranceTotal = insuranceTotal + insuranceAmount;
												copay = copay+ newCopay	;								
												$("#txtCopay").val('-'+insuranceTotal);
											}
											totalCells =  parseFloat($("#txtAmountTendered").val())+newCopay;
											$("#txtTotalBill").val(totalCells);
											$("#hdnTotalBill").val(totalCells);
											$.session.set('bill', totalCells);
											$("#txtAmountTendered").val(totalCells);
											table = totalCells;
										}
										else{
											$("#txtTotalBill").val(newTotal);
											$("#hdnTotalBill").val(newTotal);
											$.session.set('bill', newTotal);
											$("#txtAmountTendered").val(newTotal);
											table = newTotal;
										}
										/*$("#txtTotalBill").val(cells);
										$("#txtAmountTendered").val(cells);*/
										$("#totalBill").text(cells);
									}									
								});									
							},
							"aaData": dataSet,						
							"aoColumns": [ // set the data into data table
								{ "mData": "name" },
								{ "mData": "cost"},
								{ "mData": "quantity" },						
								{ "mData": "description" },
								{
								  "mData": function (o) { 	//function for checkbox			
									var data = o;
									var billId = data["bill_id"];	
									var id = data["id"];	
									var amount = data["amount"];	
									var tblName = data["tblname"];
									if(tblName == 'Lab'){
										var labTestId = data["lab_test_id"];
										return "<span  class='visitDetails' lab="+labTestId+" data-id="+id+" data-billId="+billId+" data-tbl-name="+tblName+" style='color: black !important;'>"+amount+"</span>";
									}	
									else{
										return "<span  class='visitDetails' data-id="+id+" data-billId="+billId+" data-tbl-name="+tblName+" style='color: black !important;'>"+amount+"</span>"; 
									}
									},
									"sClass": "tdTotal"
								},
								{
								  "mData": function (o) { 	//function for checkbox			
									var data = o;	
									var amount = data["amount"];
									var paymentMode = data["payment_mode"];
									return "<input type='checkbox'  class='chkbox' data-amount="+amount+" data-paymentMode = "+paymentMode+" style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;'"+					 
									   " style='font-size: 22px; cursor:pointer;'>"; 
									},
									"sClass": "tdCheckbox" 
								},{
									"mData":"payment_mode","bVisible": false 
								}
							],						
						});
					},
					error : function(){						
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
				});
			},
			error : function(){				
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	});
	
	/*Print functionality*/
	$("#msgmyModal #printCashDetailss").click(function(){		
		/*Call print function*/
		//$.print("#PrintDiv");
		window.print();
		printData = [];
		$(".close").unbind();
		$(".close").click();
	});


	
	//Click function for validation and save the data 
	$("#btnSave").click(function(){	
		
		globalVisitId = $("#txtVisit").val();
		globalPatientid = $("#txtPatientId").val();
		globalPatientName = $("#txtName").val();
		totalBillTable = $("#totalBill").text();		
		
		$("#txtAmountTendered").val($("#txtAmountTendered").val().trim());
		$("#txtPayDate").val($("#txtPayDate").val().trim());
		$("#txtVisit").val($("#txtVisit").val().trim());
		
		var flag = "false";
		
		if($("#txtTotalBill").val() == "0"){
				
			if($("#txtTotalBill").val() == $("#hdnTotalBill").val()){
				$('#messageMyModalLabel').text("Sorry");
				$('.modal-body').text("Your total bill is zero!!!");
				flag = "true";	
				$('#messagemyModal').modal();
			}
			else if($("#hdnTotalBill").val() != $.session.get('bill')){
				$('#messageMyModalLabel').text("Sorry");
				$('.modal-body').text("Don't show your smartness here!!!");
				flag = "true";	
				$('#messagemyModal').modal();
			}
			else{
				$('#messageMyModalLabel').text("Sorry");
				$('.modal-body').text("Your total bill has changed!!!");
				flag = "true";	
				$('#messagemyModal').modal();
			}

		}
		else{
			if($("#txtTotalBill").val() != $("#hdnTotalBill").val()){
				$('#messageMyModalLabel').text("Sorry");
				$('.modal-body').text("Your total bill has changed!!!");
				flag = "true";	
				$('#messagemyModal').modal();
			}
			else{
				 if($("#hdnTotalBill").val() != $.session.get('bill')){
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("Don't show your smartness here!!!");
					flag = "true";	
					$('#messagemyModal').modal();
				}
			}
		}	
		if($("#txtOutstandingBalance").val() != $.session.get('accountBill')){
			$('#messageMyModalLabel').text("Sorry");
			$('.modal-body').text("Don't show your smartness here!!!");
			flag = "true";	
			$('#messagemyModal').modal();
		}
		if($("#txtName").val()== ""){
			$("#txtVisit").focus();
			$("#txtVisitError").text("Please select data");
			$("#txtVisit").addClass("errorStyle");      
			flag = "true";			
		}
		
		if($("#selModeOfPayment").val()== "1"){
			var outStandingBalance = parseInt($("#txtOutstandingBalance").val());
			var netBill = parseInt($("#hdnTotalBill").val());
			var tendredAmount = parseInt($("#txtAmountTendered").val());		 
			
			if(tendredAmount < netBill){
				$("#txtAmountTendered").focus();
				$("#txtAmountTenderedError").text("Tendered amount is equal to total bill");
				$("#txtAmountTendered").addClass("errorStyle");      
				flag = "true";
			}	
			if(tendredAmount > netBill){
				var extraBalance = tendredAmount - netBill;
			}
			
			if($("#txtAmountTendered").val()== ""){
				$("#txtAmountTendered").focus();
				$("#txtAmountTenderedError").text("Please enter amount tendered");
				$("#txtAmountTendered").addClass("errorStyle");      
				flag = "true";			
			}
			else if(!validnumber($("#txtAmountTendered").val())){
				$("#txtAmountTendered").focus();
				$("#txtAmountTenderedError").text("Please enter number only");
				$("#txtAmountTendered").addClass("errorStyle");    
				flag = "true";			
			}
		}
		else if($("#selModeOfPayment").val()== "2"){
			if($("#txtOutstandingBalance").val() == ""){
				$("#txtOutstandingBalance").focus();
				$("#txtOutstandingBalanceError").text("Insufficient fund");
				$("#txtOutstandingBalance").addClass("errorStyle");      
				flag = "true";
			}
			else{
				var outStandingBalance = parseInt($("#txtOutstandingBalance").val());
				var netBill = parseInt($("#hdnTotalBill").val());
				if(outStandingBalance < netBill){
					$("#txtOutstandingBalance").focus();
					$("#txtOutstandingBalanceError").text("Insufficient fund");
					$("#txtOutstandingBalance").addClass("errorStyle");       
					flag = "true";
				}
				var familyMemberId =$("#hdnFamilyMemberId").val();
				if(familyMemberId == "" || familyMemberId == undefined){
					familyMemberId = parseInt($("#txtPatientId").val().replace ( /[^\d.]/g, '' ));
				}
			}				
		}
		if($("#selModeOfPayment").val()== "-1"){
			$("#selModeOfPayment").focus();
			$("#selModeOfPaymentError").text("Please select mode of payment");
			$("#selModeOfPayment").addClass("errorStyle");      
			flag = "true";			
		}
		if ($("#txtPayDate").val() == "") {
			$("#txtPayDateError").text("Please select date");
			$("#txtPayDate").addClass("errorStyle");
			$('#txtPayDate').datepicker()
			   .off('focus')
			   .click(function () {
				   $(this).datepicker('show');
			   });
			flag = "true";
		}
		
		if($("#txtVisit").val()== ""){
			$("#txtVisit").focus();
			$("#txtVisitError").text("Please enter visit id");
			$("#txtVisit").addClass("errorStyle");      
			flag = "true";			
		}
		
		if(flag == "true"){
			return false;
		}
		var serviceBill = [];    			//Array for stored the serviceId and serviceBillId
		var forensicBill = [];   			//Array for stored the forensicId and forensicBillId
		var labBill = [];					//Array for stored the labId and labBillId
		var procedureBill = [];				//Array for stored the procedureId and procedureBillId
		var radiologyBill = [];				//Array for stored the radiologyId and radiologyBillId
		var medicineBillId = new Array();
		
		
		var visitId = parseInt($("#txtVisit").val().replace ( /[^\d.]/g, '' ));
		var payDate = $("#txtPayDate").val();
		var paymentMode = $("#selModeOfPayment").val();
		var totalBill = $("#hdnTotalBill").val();
		var amountTendered = $("#txtAmountTendered").val();
		var patientID = parseInt($("#txtPatientId").val().replace ( /[^\d.]/g, '' )); 
		var balance = finalBalance - parseInt($("#hdnTotalBill").val());
		
		var aiRows =  cashPaymentoTable.$(".chkbox:unchecked", {"page": "all"});//select unchecked rows 
		//var rowcollection =  cashPaymentoTable.$(".visitDetails");	// This is used for get the ammount of td 
		
		for (i=0; i<aiRows.length; i++) {
			var TblName = $(aiRows[i]).parent().siblings('.tdTotal').find('.visitDetails').attr('data-tbl-name');
			var Id = $(aiRows[i]).parent().siblings('.tdTotal').find('.visitDetails').attr('data-id');
			var billId = $(aiRows[i]).parent().siblings('.tdTotal').find('.visitDetails').attr('data-billId');
			var amount = $(aiRows[i]).parent().siblings('.tdTotal').find('.visitDetails').text();
			//var trLength = $(rowcollection[i]).parent().siblings().length;
			/* var printData = new Array(); */
			/* for(j=0;j<trLength;j++){ */
				var ObjPrintData = {};
				ObjPrintData.Name = $($(aiRows[i]).parent().siblings()[0]).text();
				ObjPrintData.Cost = $($(aiRows[i]).parent().siblings()[1]).text();
				ObjPrintData.Quantity = $($(aiRows[i]).parent().siblings()[2]).text();
				ObjPrintData.Description = $($(aiRows[i]).parent().siblings()[3]).text();
				ObjPrintData.Amount = $($(aiRows[i]).parent().siblings()[4]).text();
				printData.push(ObjPrintData);
				/* bigPrintData.push(printData); */
			/* } */
			
			if(TblName == "Service"){
				var ObjServices = {};
				ObjServices.serviceBillId = billId;
				ObjServices.serviceId = Id;
				ObjServices.amount = amount;
				serviceBill.push(ObjServices);
			}
			if(TblName == "Forensic"){
				var ObjForensic = {};
				ObjForensic.forensicBillId = billId;
				ObjForensic.forensicId = Id;
				ObjForensic.amount = amount;
				forensicBill.push(ObjForensic);
			}
			if(TblName == "Lab"){
				var ObjLab = {};
				ObjLab.labBillId = billId;
				ObjLab.labId = Id;
				ObjLab.amount = amount;
				ObjLab.labTestId = $(aiRows[i]).parent().siblings('.tdTotal').find('.visitDetails').attr('labTestId');
				labBill.push(ObjLab)
			}
			if(TblName == "Procedure"){
				var ObjProcedure = {};
				ObjProcedure.procedureBillId = billId;
				ObjProcedure.procedureId = Id;
				ObjProcedure.amount = amount;
				procedureBill.push(ObjProcedure)
			}
			if(TblName == "Radiology"){
				var ObjRadiology = {};
				ObjRadiology.radiologyBillId = billId;
				ObjRadiology.radiologyId = Id;
				ObjRadiology.amount = amount;
				radiologyBill.push(ObjRadiology)
			}
			if(TblName == "Medicine"){
				var ObjMedicine = {};
				ObjMedicine.billId = billId;
				ObjMedicine.amount = amount;
				medicineBillId.push(ObjMedicine);
				//medicineBillId.amount = amount;
			}			
		}
		var cashData = {
			"operation":"save",
			"visitId":visitId,
			"payDate":payDate,
			"paymentMode":paymentMode,
			"amountTendered":amountTendered,				
			"familyMemberId":familyMemberId,				
			"medicineBillId":JSON.stringify(medicineBillId),
			"radiologyBill":JSON.stringify(radiologyBill),
			"procedureBill":JSON.stringify(procedureBill),
			"labBill":JSON.stringify(labBill),
			"forensicBill":JSON.stringify(forensicBill),
			"serviceBill":JSON.stringify(serviceBill),
			"totalBill":totalBill,
			"balance":balance,
			"table":table,
			"patientID":patientID
		}
		generateReceiptNo(cashData);		
		
	});
	
	//Keyup functionality 
	$("#txtVisit").keyup(function(){
        if ($("#txtVisit").val() != "") {
            $("#txtVisitError").text("");
            $("#txtVisit").removeClass("errorStyle");
        }
	});
	$("#txtPayDate").change(function(){
        if ($("#txtPayDate").val() != "") {
            $("#txtPayDateError").text("");
            $("#txtPayDate").removeClass("errorStyle");
        }
	});
	$("#selModeOfPayment").change(function(){
        if ($("#selModeOfPayment").val() != "-1") {
            $("#selModeOfPaymentError").text("");
            $("#selModeOfPayment").removeClass("errorStyle");
            $("#txtOutstandingBalanceError").text("");
            $("#txtOutstandingBalance").removeClass("errorStyle");
        }
	});
	$("#txtAmountTendered").keyup(function(){
        if ($("#txtAmountTendered").val() != "") {
            $("#txtAmountTenderedError").text("");
            $("#txtAmountTendered").removeClass("errorStyle");
        }
	});
	
	//Click function for reset button 
	$("#btnReset").click(function(){
		$("#txtAmountTendered").removeClass("errorStyle");
		$("#selModeOfPayment").removeClass("errorStyle");
		$("#txtPayDate").removeClass("errorStyle");
		$("#txtVisit").removeClass("errorStyle");
		$("#txtVisit").focus();
		$("#divOutstandingBalance").hide();
		$("#divAmountTendered").show();
		cashPaymentoTable.fnClearTable();
		clearCash();
		currentDate();
	});
	
	//click on search icon load view visit screen in modal 
	$("#iconSearch").click(function(){
		$.session.set('bill', 0);		
		$("#txtOutstandingBalanceError").text("");
		$("#txtOutstandingBalance").removeClass("errorStyle");		
		$('#cashModalLabel').text("Search Visit");
        $('#cashModalBody').load('views/admin/search_visit_cash_payment.html',function(){
			$("#myCashModal #txtFirstName").focus();
			$("#thing").val("2");
		});
		$('#myCashModal').modal();	
		$("#divOutstandingBalance").hide();
		$("#divAmountTendered").show();	
		$(".insurance").hide();
	});

	$('#txtVisit').keypress(function (e) {
        if (e.which == 13) {
            $("#btnSelect").click();
        }
    });

    $("#txtTotalBill").keydown(function(e){
    	if (e.keyCode < 500) {
    		return false;
    	}
    });
});

// getAmountPrefix function
function getAmountPrefix(){
    var postData = {
        "operation": "show"
    }
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/configuration.php",
        datatype: "json",
        data: postData,

        success: function(data) {
            if(data != "0" && data != ""){
				var parseData = jQuery.parseJSON(data);				
				amountPrefix = parseData[5].value;
				$("#amountPrefixCopayValue").text(amountPrefix);
				$(".amountPrefix").text(' '+amountPrefix);
				$("#amountPrefixCopay").text(amountPrefix);
				$("#amountPrefixTotalBill").text(amountPrefix);
			}
        },
        error: function() {             
            $('.modal-body').text("");
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();
        }            
    });
}

//This function is used for clear the data 
function clearCash(){
	$("#txtVisit").val("");
	$("#txtVisitError").text("");
	$("#txtAmountTendered").val("");
	$("#txtAmountTenderedError").text("");
	$("#selModeOfPayment").val("-1");
	$("#selModeOfPaymentError").text("");
	$("#txtPayDate").val("");
	$("#txtPayDateError").text("");
	$("#txtName").val("");
	$("#txtPatientId").val("");
	$("#txtOPDRegNo").val("");
	$("#txtOutstandingBalance").val(0);
	$("#txtTotalBill").val("");
	$("#txtVisitDate").val("");
	$("#txtVisit").removeAttr("disabled", "disabled");
	$(".insurance").hide();
	$.session.set('bill', 0);
}

// function for check validation for numeric and float value
 function validnumber(number) {
	floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
	return floatRegex.test(number);
};

//click escape button to close popup
$(document).keyup(function(event) {
    if(event.keyCode === 27) {
        $('.close').click();
    }
});

//This function is used for wshow the current date 
function currentDate(){
	var fullDate = new Date();
	
	//convert month to 2 digits
	var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '' + (fullDate.getMonth()+1);
	var todayDay = fullDate.getDate();
	 if(twoDigitMonth < "10"){
		 twoDigitMonth = "0"+twoDigitMonth;
	 }
	 if(todayDay < "10"){
		 todayDay = "0"+todayDay;
	 }
	var currentDate = fullDate.getFullYear() + "-" + twoDigitMonth+ "-" + todayDay ;	
	
	$('#txtPayDate').val(currentDate);
}
function generateReceiptNo(objCashData){
	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/cash_payment.php",
		datatype:"json",
		data: {
			"operation":"receipt"
		},					
		success: function(data) {
			if(data != null && data != ""){
				 var parseData = jQuery.parseJSON(data);
				var receiptNo = parseInt((parseData[0].id).match(/-*[0-9]+/));
				var prefix = parseData[0].prefix;
				receiptNo = receiptNo + 1;
				objCashData.receiptNo = receiptNo;
				var receiptNoLength = receiptNo.toString().length;
				for(i=0;i<6-receiptNoLength;i++){
					receiptNo = "0"+receiptNo;
				}
				reciptNumber = prefix+receiptNo;//to fetch data during print time
				$("#reciptNoMsg").text(reciptNumber);
				var htmlPrintData = printDetails();	

				objCashData.printDiv = htmlPrintData;
				


				confirmSaveDetails(objCashData);			
			}			
			else{
				$("#reciptNoMsg").text("000001" );
			}			
		},
		error : function(){			
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
	});
}


function printDetails(){
		/*firstly empty page after that load data*/
		$("#codexplBody").find("tr:gt(0)").html("");
		$("#printTotalBill").text("");
		/*Load data to print page*/
		$("#lblPrintReceipt").text($("#reciptNoMsg").text());
		$("#lblPrintVisitId").text(globalVisitId);
		$("#lblPrintPatientId").text(globalPatientid);
		$("#lblPrintPatient").text(globalPatientName);
		$("#lblPrintDate").text(todayDate());
		/*hospital information through global varibale in main.js*/
		$("#lblPrintEmail").text(hospitalEmail);
		if($("#txtCopay").val() != ''){
			$(".insurance-print").show();
			$("#lblPrintCopay").text($("#txtCopay").val());
			$("#lblPrintTotalBill").text($("#txtTotalBill").val());
		}
		else{
			$(".insurance-print").hide();
		}		
		$("#lblPrintPhone").text(hospitalPhoneNo);
		$("#lblPrintAddress").text(hospitalAddress);
		if (hospitalLogo != "null") {
			imagePath = "./images/" + hospitalLogo;
			$("#printLogo").attr("src",imagePath);
		}
		$("#printHospitalName").text(hospitalName);
		$("#printTotalBill").text(totalBillTable);
		//$("#").text();
		for(var i=0;i<printData.length;i++){
			$("#codexplBody").append('<tr><td>'+ (i+1) +'</td><td>'+printData[i].Name+'</td><td>'
			+printData[i].Quantity+'</td><td>'+printData[i].Amount+'</td>');
		}

		return $("#PrintDiv").html();
	}

	function confirmSaveDetails(objCashData){
		$(".close-modal").click();
		$('#confirmMyModalLabel').text("Submit Payment");
		$('.modal-body').text("Are you sure that you want to submit this?");
		$('#confirmmyModal').modal();
		$("#confirm").unbind();
		$("#confirm").on('click',function(){		
			$.ajax({
				type: "POST",
				cache: false,
				url: "controllers/admin/cash_payment.php",
				datatype:"json",
				data: objCashData,
				
				success: function(data) {
					if(data != "0" && data != "" && data != "Don't show your smartness"){
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#msgMyModalLabel').text("Success");
						$('#paymentMsg').text("Payment submitted successfully!!!");
						$('#msgmyModal').modal();
						$('.modal-footer').show();
						cashPaymentoTable.fnClearTable();
						clearCash();
						$("#divOutstandingBalance").hide();
						$("#divAmountTendered").show();
					}
					if(data == "Don't show your smartness"){
						$('#messageMyModalLabel').text("Sorry");
						$('.modal-body').text("Don't show your smartness here!!!");
						$('#messagemyModal').modal();
					}
					currentDate();
				},
				error : function(){
					$('.close-confirm').click();
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
			});
		});
	}
	
// call function for bind data
function bindInsuranceCompany() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/insurance_plan.php",
        data: {
            "operation": "showCompany"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selInsuranceCompany').html(option);
    			$("#amountPrefix").text(' '+ amountPrefix);
    			$("#amountPrefixOutStanding").text(amountPrefix);
            }
        },
        error: function() {}
    });
}

// call function for bind scheme name
function bindSchemeName(value,scheme) {
     $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/scheme_exclusion.php",
        data: {
            "operation": "showSchemeName",
            companyId: value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].plan_name + "</option>";
                }
                $('#selInsurancePlan').html(option);
				if(scheme != null){
                    $('#selInsurancePlan').val(scheme);
                }
            }
        },
        error: function() {}
    });
}

// call function for bind scheme name
function bindPlanName(value,plan) {
     $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/scheme_exclusion.php",
        data: {
            "operation": "showPlanName",
            schemeId: value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].scheme_plan_name + "</option>";
                }
                $('#selInsuranceScheme').html(option);
				if(plan != null){
                    $('#selInsuranceScheme').val(plan);
                }
            }
        },
        error: function() {}
    });
}

function paymentModeChange(selModeOfPayment){
	$(selModeOfPayment).change(function() {
		
        if ($(selModeOfPayment).val() != '-1') {
            $("#selPaymentModeError").text("");
            $(selModeOfPayment).removeClass("errorStyle");			
							
            if($(selModeOfPayment).val() == 'Insurance'){
				if ($("#txtPatientId").val().trim() == "") {
					$("#txtPatientId").focus();
					$("#txtPatientIdError").text("Please enter patient Id");
					$("#txtPatientId").addClass("errorStyle");
					return;
				}
		
				paymentModeValue = $(selModeOfPayment).val();
					$(".insurance").show();
                var patientId = $("#txtPatientId").val();
                var hrn = parseInt($("#txtPatientId").val().replace ( /[^\d.]/g, '' ));

                var postData = {
                    "operation": "insuranceDetails",
                    "hrn": hrn
                }

                $.ajax({
                    type: "POST",
                    cache: false,
                    url: "controllers/admin/opd_registration.php",
                    datatype: "json",
                    data: postData,

                    success: function(data) {
                        var parseData = jQuery.parseJSON(data);
                         if (parseData != "0" && parseData != "") {     
							insuranceNumberValue = parseData[0].insurance_number;
                            $('#selInsuranceCompany').val(parseData[0].insurance_company_id);
                            $('#txtInsuranceNo').val(parseData[0].insurance_number);
                            bindSchemeName(parseData[0].insurance_company_id, parseData[0].insurance_plan_id);
                            bindPlanName(parseData[0].insurance_plan_id, parseData[0].insurance_plan_name_id);  
							//getCopayValue(parseData[0].insurance_plan_name_id);										
                        }
                        else{                        
                            $('#confirmInsuranceModalLabel').text("Alert");
                            $('#confirmInsuranceBody').text("Insurance detail is not present.Do you want to continue?");
                            $('#confirmInsuranceModal').modal();

                            $("#btnOk").click(function(){
                                $('#insuranceModalLabel').text("Insurance");
                                $("#hdnInsurance").val("1");
                                $("#hdnPatientId").val($("#txtPatientId").val());
                                $('#insuranceBody').load('views/admin/modal_insurance.html');
                                $('#insuranceModal').modal();
                            });
                        }
                    },
                    error: function(){

                    }
                });
            } 
        }

    });
}

function getInsuranceValue(value) {
				
	$(".insurance").show();
	var visitId = parseInt($("#txtVisit").val().replace ( /[^\d.]/g, '' ));
	var patientId = $("#txtPatientId").val();
	var hrn = parseInt($("#txtPatientId").val().replace ( /[^\d.]/g, '' ));

	var postData = {
		"operation": "insuranceDetails",
		"visitId" : visitId,
		"patientID": hrn
	}

	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/cash_payment.php",
		datatype: "json",
		data: postData,

		success: function(data) {
			var parseData = jQuery.parseJSON(data);
			 if (parseData != "0" && parseData != "") {     
				insuranceNumberValue = parseData[0].insurance_number;
				$('#selInsuranceCompany').val(parseData[0].insurance_company_id);
				$('#txtInsuranceNo').val(parseData[0].insurance_number);
				bindSchemeName(parseData[0].insurance_company_id, parseData[0].insurance_plan_id);
				bindPlanName(parseData[0].insurance_plan_id, parseData[0].insurance_plan_name_id);
				$("#selInsuranceScheme").attr('data-copay-name',parseData[0].copay_type);
				$("#selInsuranceScheme").attr('data-copay-value',parseData[0].value);   
				//getCopayValue(parseData[0].insurance_plan_name_id);
				//getData(postData);
			}
			else{                        
				$('#confirmInsuranceModalLabel').text("Alert");
				$('#confirmInsuranceBody').text("Insurance detail is not present.Do you want to continue?");
				$('#confirmInsuranceModal').modal();

				$("#btnOk").click(function(){
					$('#insuranceModalLabel').text("Insurance");
					$("#hdnInsurance").val("1");
					$("#hdnPatientId").val($("#txtPatientId").val());
					$('#insuranceBody').load('views/admin/modal_insurance.html');
					$('#insuranceModal').modal();
				});
			}
		},
		error: function(){
		}

	});
}

//# sourceURL = cash payment.js