var recurrencePeriodTable;
$(document).ready(function(){
    loader();
    debugger;
	
	/*Hide add ward by default functionality*/
	$("#advanced-wizard").hide();
	$("#subTypeList").css({
        "background-color": "#fff"
    });
	$("#tabsubTypeList").css({
        "background-color": "#0c71c8",
        "color": "#fff"
    });
		
	/*Click for add the sub Type*/
    $("#tabAddsubType").click(function() {
        $("#advanced-wizard").show();
        $(".blackborder").hide();
		clear();
        $("#tabAddsubType").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tabsubTypeList").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#subTypeList").css({
            "background": "#fff"
        });
    });

    $('#txtRecurrencePeriod').keyup(function() {
         if($('#txtRecurrencePeriod').val() != '') {
            $('#txtRecurrencePeriodError').text('');
            $('#txtRecurrencePeriod').removeClass("errorStyle"); 
        }
    });

     $('#btnSubmit').click(function() {
        var flag = false;

        if($('#txtRecurrencePeriod').val() == '') {
            $('#txtRecurrencePeriodError').text('Please enter recurrence period');
            $('#txtRecurrencePeriod').focus();
            $('#txtRecurrencePeriod').addClass("errorStyle"); 
            flag = true;
        }
        if(flag == true) {
            return false;
        }

        var recurrencePeriod = $('#txtRecurrencePeriod').val().trim();
        var postData = {
            recurrencePeriod : recurrencePeriod,
            operation : "saveRecurrencePeriod"
        }


        $.ajax(
            {                   
            type: "POST",
            cache: false,
            url: "controllers/admin/recurrence_period.php",
            datatype:"json",
            data: postData,
            
            success: function(data) {
                if(data != "0" && data != ""){
                    callSuccessPopUp('Success','Saved successfully!!!');
                    tabsubTypeList();
                    clear();
                }
                else {
                    $('#txtRecurrencePeriodError').text('Recurrence period is already exist');
                    $('#txtRecurrencePeriod').focus();
                    $('#txtRecurrencePeriod').addClass('errorStyle');
                }
            },
            error: function(){

            }
        });
    });
     if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code 
        recurrencePeriodTable = $('#recurrencePeriodTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "autoWidth": false,
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = recurrencePeriodTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var recurrencePeriod = mData["recurrence_period"];
                        editClick(id,recurrencePeriod);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = recurrencePeriodTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/recurrence_period.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "recurrence_period"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [1]
            }]

        });
    }

    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            recurrencePeriodTable.fnClearTable();
            recurrencePeriodTable.fnDestroy();
            recurrencePeriodTable = "";
            recurrencePeriodTable = $('#recurrencePeriodTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "autoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = recurrencePeriodTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var recurrencePeriod = mData["recurrence_period"];
                            restoreClick(id,recurrencePeriod);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/recurrence_period.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "recurrence_period"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1]
                }]
            });
        } else { // show active data on unchecked   
            recurrencePeriodTable.fnClearTable();
            recurrencePeriodTable.fnDestroy();
            recurrencePeriodTable = "";
            recurrencePeriodTable = $('#recurrencePeriodTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "autoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = recurrencePeriodTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var recurrencePeriod = mData["recurrence_period"];
                            editClick(id,recurrencePeriod);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = recurrencePeriodTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/recurrence_period.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "recurrence_period"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1]
                }]
            });
        }
    });
	
     $('#btnReset').click(function() {
        clear();''
     });

	//Click function for show the sub_type lists
    $("#tabsubTypeList").click(function() {
        clear();
        tabsubTypeList();
    });
});

function editClick(id,recurrencePeriod) {
    
    $('.modal-body').text("");
    $('#myModalLabel').text("Update recurrence period");
    $('.modal-body').html($(".step").html()).show();
    $('#myModal').modal('show');

    $('#myModal').on('shown.bs.modal', function() {
       $('#myModal #txtRecurrencePeriod').focus();
    });
    $('#myModal #btnSubmit').hide();
    $('#myModal #btnReset').hide()
    $("#myModal #btnUpdate").removeAttr("style");
    
    $("#myModal #txtRecurrencePeriod").removeClass("errorStyle");
    $("#myModal #txtRecurrencePeriodError").text("");

    
    $('#myModal #txtRecurrencePeriod').val(recurrencePeriod);
    $('#selectedRow').val(id);
    //validation
    //remove validation style
    // edit data function for update 
    $("#myModal #txtRecurrencePeriod").keyup(function() {
        if ($("#myModal #txtRecurrencePeriod").val() != '') {
            $("#myModal #txtRecurrencePeriodError").text("");
            $("#myModal #txtRecurrencePeriod").removeClass("errorStyle");
        }
    });
    
    
    $("#myModal #btnUpdate").click(function() { // click update button
        var flag = false;
        if ($("#myModal #txtRecurrencePeriod").val()== '') {
            $("#myModal #txtRecurrencePeriodError").text("Please enter recurrence period");
            $("#myModal #txtRecurrencePeriod").focus();
            $("#myModal #txtRecurrencePeriod").addClass("errorStyle");
            flag = true;
        }
        
        if(flag == true) {
            return false;
        }

        var recurrencePeriod = $("#myModal #txtRecurrencePeriod").val().trim();
        var id = $('#selectedRow').val();
        
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
        var postData = {
            "operation": "update",
            "recurrencePeriod": recurrencePeriod,
            "id": id
        }
        $.ajax( //ajax call for update data
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/recurrence_period.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('#myModal').modal('hide');
                        $('.close-confirm').click();
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Recurrence period updated successfully!!!");
                        $('#messagemyModal').modal();
                        recurrencePeriodTable.fnReloadAjax();
                        clear();
                    }
                    else {                
                        $("#myModal #txtRecurrencePeriodError").text("Recurrence period is already exist");
                        $("#myModal #txtRecurrencePeriod").focus();               
                        $("#myModal #txtRecurrencePeriod").addClass('errorStyle');
                    }
                },
                error: function() {
                    $('.close-confirm').click();
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                    $('#messagemyModal').modal();
                }
            }); // end of ajax
        });
    });
} // end update button
function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete recurrence period");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteRecurrencePeriod("' + type + '","'+id+'","");');
} // end click fucntion

function restoreClick(id,recurrencePeriod) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore sub type");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteRecurrencePeriod("' + type + '","'+id+'","'+recurrencePeriod+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteRecurrencePeriod(type,id,recurrencePeriod) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/recurrence_period.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Recurrence period deleted successfully!!!");
                    $('#messagemyModal').modal();
                    recurrencePeriodTable.fnReloadAjax();
                } 
                else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This recurrence period is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/recurrence_period.php",
            data: {
                "operation": "restore",
                "recurrencePeriod":recurrencePeriod,
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Recurrence period restored successfully!!!");
                    $('#messagemyModal').modal();
                    recurrencePeriodTable.fnReloadAjax();
                }
                else {
                    callSuccessPopUp('Sorry','Recurrence period already exist you can not restore !!!');
                }
            },
            error: function() {             
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary unavailable to respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}


function tabsubTypeList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
    $("#tabAddsubType").css({
        "background": "#fff",
        "color": "#000"
    });
    $("#tabsubTypeList").css({
        "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
        "color": "#fff"
    });
    $("#subTypeList").css({
        "background-color": "#fff"
    });
    $('#inactive-checkbox-tick').prop('checked', false).change();
    clear();
}

function clear() {
    $('#txtRecurrencePeriod').val('');
    $('#txtRecurrencePeriodError').text('');
    $('#txtRecurrencePeriod').removeClass('errorStyle');
    $('#txtRecurrencePeriod').focus();
}