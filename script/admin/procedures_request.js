var oProcedureTable = null;
var paymentMode = '';
var insuranceCompanyId = "";
var insurancePlanId = "";
var insurancePlanNameid = "";
var revisedCost = '';
var paymentMethod = '';
var copayType = '';
var copayValue = '';
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   procedure_requets.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Arora
 * Created Date :   31th dec, 2015
 * Description  :   This page add procedure tests in consultant module
 *************************************************************************************************** */
    debugger;
    loader();
	var check = "";
	$(".amountPrefix").text(amountPrefix);
	$('#oldHistoryModalDetails input[type=text]').addClass("as_label");
    $('#oldHistoryModalDetails input[type=text]').attr('readonly', true);
    $('#oldHistoryModalDetails input[type="button"]').addClass("btnHide");
    $('#oldHistoryModalDetails #divDropDownProcedureRequest').hide();

    /*Focus on first element*/
    $("#selProcedureType").focus();

    bindProcedure();//Loading the procedure dropdown at the start

    /**DataTable Initialization**/
    oProcedureTable = $('#procedureTbl').DataTable({
        "bPaginate": false,
        aoColumnDefs: [{ aTargets: [ 4 ], bSortable: false }]
    });
	
	if ($('#thing').val() ==3) {
        var visitId = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
		getPaymentMode(visitId);
    }
    if ($('#thing').val() ==4) {
        var visitId = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
		getPaymentMode(visitId);
    }
    if ($('#thing').val() ==5) {
        var visitId = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
		getPaymentMode(visitId);
    }

    /*Add Lab Test click event*/
    $('#addProcedureRequest').click(function() {
		check = "false";
        $('#addProcedureRequest').removeClass("errorStyle"); //remove the red error border from add test button
        $('#noProcedureDataError').html(""); //remove the error message after the add button

        if ($('#selProcedureType').val() == "-1" || $('#selProcedureType').val() == null) {
            $('#selProcedureTypeError').text("Please select service type");
            $('#selProcedureType').addClass("errorStyle");
            return false;
        }
        if($("#selRequestType").val() == -1){
            $("#selRequestType").focus();
            $("#selRequestType").addClass("errorStyle");
            $("#selRequestTypeError").text("Please select request type");
            return false;
        }

        var id = $('#selProcedureType').val();//getting value of selected element
        var name = $('#selProcedureType  option:selected').attr('data-name');//getting attribute name of selected element
        var cost = $('#selProcedureType  option:selected').attr('data-cost');
        var requestType = $('#selRequestType').val();
        var newRevisedCost = '';
        if (revisedCost == "") {
            newRevisedCost = cost;
            paymentMethod = "cash";
        }
        else {
            newRevisedCost = revisedCost;
            paymentMethod = "insurance";
        }

        /*Check that test already exist*/
        for (var i = 0; i < oProcedureTable.fnGetNodes().length; i++) {
            var iRow = oProcedureTable.fnGetNodes()[i];   // assign current row to iRow variable
            var aData = oProcedureTable.fnGetData(iRow); // Pull the row

            testId = aData[6];//getting procedure id
            var flag = false;
           
			var tstName = $(aData[0]).text();
			if (id == testId && aData[2]!="done") {
				$('#selProcedureType').focus();
                $('#selProcedureTypeError').text("Procedure already exist");
                $('#selProcedureType').addClass("errorStyle");
                return true;
			}
            if (flag == true) {
                return false;
            }
        }

        if (checkTestExcluded =="yes") {

            $('#confirmUpdateModalLabel').text();
            $('#updateBody').text("Are you sure you want to add this still it is not included in your insurance?");
            $('#confirmUpdateModal').modal();
            $("#btnConfirm").unbind();
            $("#btnConfirm").click(function(){

                newRevisedCost = cost;
                var newPaymentMethod = "cash";//cauz if service is excluded then always payment method always cash

                if (requestType == "internal") {
                    /*Add data in table*/
                    var currData = oProcedureTable.fnAddData(["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", "Unpaid", "Pending", cost,newRevisedCost,"<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>", id,requestType,newPaymentMethod]);
                }
                else {
                    var currData = oProcedureTable.fnAddData(["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", "N/A", "N/A", 0,0,"<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>", id,requestType,newPaymentMethod]);
                }

                /*Get current row to add color in that*/
                var getCurrDataRow = oProcedureTable.fnSettings().aoData[ currData ].nTr;
                $(getCurrDataRow).find('td').css({"background-color":"#fff","color":"#000"});

                $('#totalProcedureBill').text(parseInt($('#totalProcedureBill').text()) + parseInt(newRevisedCost));//add total bill
            });
        }
        else{
            /*Add data in table*/
            if (requestType == "internal") {
                var currData = oProcedureTable.fnAddData(["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", "Unpaid", "Pending", cost,newRevisedCost,"<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>", id,requestType,paymentMethod]);
            }
            else {
                var currData = oProcedureTable.fnAddData(["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", "N/A", "N/A", 0,0,"<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>", id,requestType,paymentMethod]);

            }

            /*Get current row to add color in that*/
            var getCurrDataRow = oProcedureTable.fnSettings().aoData[ currData ].nTr;
            $(getCurrDataRow).find('td').css({"background-color":"#fff","color":"#000"});

            if (requestType == "internal") {
                $('#totalProcedureBill').text(parseInt($('#totalProcedureBill').text()) + parseInt(newRevisedCost));//add total bill
            }
        }
    });

    /*Save button functionality*/
    $('#btnSaveProcedureTestRequest').click(function() {

        var tableData = oProcedureTable.fnGetData(); //fetching the datatable data

        /*Code to compare previpous and current data based on attribute so on click not to get previous data coming from database*/
        var arr= [];
        jQuery.grep(tableData, function( n, i ){
            if($(n[0]).attr("data-previous") == "false"){
                arr.push(n);
            }
        });
		
		if(check == "true"){
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Sorry");
			$('.modal-body').text("Your request already in pending..");
			return false;
		}

        //Checking whether datatable is empty or not
        if (tableData.length == 0) {

            $('#addProcedureRequest').addClass("errorStyle"); //Put the red border around the add lab button
            $('#noProcedureDataError').html("&nbsp;&nbsp;Please add test(s)."); //Put the error message after the add lab test button
            return false;
        } else { //Saving the data

            var patientId = parseInt($('#txtPatientID').val().replace ( /[^\d.]/g, '' ));
            var visitId = parseInt($('#textvisitId').val().replace ( /[^\d.]/g, '' ));

            /*Checked the already presented data */
			if(arr.length == 0){
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Sorry");
				$('.modal-body').text("Your request already in pending..");
			}

			else{
                $('#confirmUpdateModalLabel').text();
                $('#updateBody').text("Are you sure that you want to save this?");
                $('#confirmUpdateModal').modal();
                $("#btnConfirm").unbind();
                $("#btnConfirm").click(function(){

                    var totalCurrentReqBill = 0;
                    //get the total cost of all currently added request
                    $.each(arr ,function(i,v){  
                        if (arr[i][7] == 'insurance') {
                            totalCurrentReqBill= totalCurrentReqBill + parseInt(arr[i][4]);
                        }  
                    });

    				$.ajax({
    					type: "POST",
    					cache: false,
    					url: "controllers/admin/procedure_request.php",
    					datatype: "json",
    					data: {
    						'operation': 'save',
    						'data': JSON.stringify(arr),
    						'patientId': patientId,
    						'visitId': visitId,
    						'tblName': 'procedures_requests',
    						'fkColName': 'procedure_id',
                            'copayType' : copayType,
                            'copayValue' : copayValue,
                            'totalCurrentReqBill' : totalCurrentReqBill
    					},

    					success: function(data) {
    						if (data != "0" && data == "1") {
    							$('#messageMyModalLabel').text("Success");
    							$('#messagemyModal .modal-body').text("Procedure request saved successfully!!!");
    							$('#messagemyModal').modal();
    							bindProcedure();
    							check = "true";
    							Procedures(visitId,paymentMode);
    						} 
    						else if(data =="0"){
    							$('#messagemyModal').modal();
    							$('#messageMyModalLabel').text("Sorry");
    							$('.modal-body').text("Your test result is still not sent");
    						}
    						else{}//nothing
    					},
    					error: function() {
    						$('#messageMyModalLabel').text("Error");
    						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
    						$('#messagemyModal').modal();
    					}
    				});
                });
			}
            
        }
    });
 
    /*On click of ok button clear whole data*/
    $('#ok').click(function() {
        $("#selProcedureType").focus();
    });

    //Reset button functionality
    $('#btnResetProcedureTestRequest').click(function() {
        clear();
    });   

    /*Remove style on change*/
    $('#selProcedureType').change(function(){
        var procedureId = $('#selProcedureType').val();
        if (procedureId != "-1" ) {
            $('#selProcedureTypeError').text("");
            $('#selProcedureType').removeClass("errorStyle");
            if (paymentMode.toLowerCase() == "insurance") {
                getRevisedCost(procedureId);
                checkExclusion(procedureId,insuranceCompanyId,insurancePlanId,insurancePlanNameid,"procedure");
            }
            else{
                checkTestExcluded = '';
            }
        }
    }); 
});

/**Row Delete functionality**/
function rowDelete(currInst) {
    var row = currInst.closest("tr").get(0);//getting closest tr
    var oProcedureTable = $('#procedureTbl').dataTable();
    $('#totalProcedureBill').text(parseInt($('#totalProcedureBill').text()) - parseInt($(row).find('td:eq(4)').text()));//subtract amount of that row from total amount on remove of a row
    oProcedureTable.fnDeleteRow(row);
}

/*Binding Procedure*/
function bindProcedure() {
    var visitIdProcedure = $('#textvisitId').val();
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/procedure_request.php",
        data: {
            "operation": "showProcedure",
            "visitIdProcedure" : visitIdProcedure
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "' data-cost='" + parseData[i].price + "' data-name='" + parseData[i].name + "'>" + parseData[i].name + "</option>";
                }
                $('#selProcedureType').html(option);
            }
        },
        error: function() {
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();
        }
    });
}

function clear() {
    $("#selProcedureType").val("-1");
    $("#selProcedureTypeError").text("");
    $("#addProcedureRequest").removeClass("errorStyle");
    $("#noProcedureDataError").text("");
    var lengthDeleteRow = $(".btnEnabled").closest("tr").length;
    for(var i=0;i<lengthDeleteRow;i++){
        var myDeleteRow = $(".btnEnabled").closest("tr")[0];            
        $('#totalProcedureBill').text(parseInt($('#totalProcedureBill').text()) - parseInt($(myDeleteRow).find('td:eq(4)').text()));
        oProcedureTable.fnDeleteRow(myDeleteRow);
    }
    $("#selProcedureType").focus();
}

function Procedures(visitId,paymentMode){
    /*Fetch data from db and insert in to table during page load*/
    var postData = {
        "operation": "oldProcedureSearch",
        "visit_id": visitId,
        "paymentMode" : paymentMode,
        "activeTab" : "procedure"
    }
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/old_test_requests.php",
        datatype: "json",
        data: postData,

        success: function(data) {
            dataSet = JSON.parse(data);  
            oProcedureTable.fnClearTable();
            $('#totalProcedureBill').text("0");
            
            for(var i=0; i< dataSet.length; i++){
                var id = dataSet[i].id;
                var pay_status = dataSet[i].pay_status;
                var name = dataSet[i].name;
                var test_status = dataSet[i].test_status;
                var cost = dataSet[i].price;
                var oldRevisedCost =''; 
                if (paymentMode != null && paymentMode.toLowerCase() == "insurance") {
                    oldRevisedCost = dataSet[i].revised_cost;
                }
                else {
                    oldRevisedCost = cost;
                }

                //check wheteher in name external is coming or not
                if (name.indexOf("(external)") > 1) {
                    pay_status = "N/A";
                    test_status = "N/A";
                    cost = 0;
                    oldRevisedCost = 0;
                }

                /*Apply here span to recognise previous or current data*/
                oProcedureTable.fnAddData(["<span data-previous='true'>"+name+"</span>",pay_status, test_status,cost,oldRevisedCost,"<input type='button' class ='btnDisabled' value='Remove' disabled>",id]);//send one column null so user can't remove previous data
                $('#totalProcedureBill').text(parseInt($('#totalProcedureBill').text()) + parseInt(oldRevisedCost));//add total bill                
            }
            oProcedureTable.find('tbody').find('tr').find('td').css({"background-color":"#ccc"});
        }
    });
}
//get payment mode insurance or other
function getPaymentMode(visitId){
    var postData = {
        "operation": "payment_mode",
        "visitId": visitId
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (result.trim() != '') {
            paymentMode = parseData[0].payment_mode;

            if (paymentMode.toLowerCase() == "insurance") {
                insuranceDetails(visitId);
            }            
        } 
        Procedures(visitId,paymentMode);
    });
}
function insuranceDetails(visitId){
    var postData = {
        "operation": "insuranceDetail",
        "visitId": visitId
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (result.trim() != '') {
            insuranceCompanyId = parseData[0].insurance_company_id;
            insurancePlanId = parseData[0].insurance_plan_id;
            insurancePlanNameid = parseData[0].insurance_plan_name_id;

            getCopayValue();//get the copay details
        }
    });
}
function getRevisedCost(procedureId){
    var postData = {
        "operation": "revisedCostDetail",
        "insuranceCompanyId": insuranceCompanyId,
        "insurancePlanId" : insurancePlanId,
        "insurancePlanNameid" : insurancePlanNameid,
        "componentId" : procedureId,
        "activeTab" : "procedure"
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (result.length > 2) {
            revisedCost = parseData[0].revised_cost;
        }
        else{
            revisedCost ='';
        }
    });
}
function getCopayValue(){
    var postData = {
        "operation": "getCopayValue",
        "insuranceCompanyId": insuranceCompanyId,
        "insurancePlanId" : insurancePlanId,
        "insurancePlanNameid" : insurancePlanNameid
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (parseData != '' && parseData != null) {
            copayType = parseData[0].copay_type;
            copayValue = parseData[0].value;
        }
    });
}