var transactionTable;
$(document).ready(function(){
	debugger;
// inilize data table
    /**DataTable Initialization**/
    transactionTable = $('#viewTransactionTable').DataTable({
		"bFilter": true,
		"processing": true,
		"sPaginationType":"full_numbers",
		"fnDrawCallback" : function() {
			$('.transactionDetails').unbind();
			$('.transactionDetails').on('click',function() {
				var data = $(this).closest('tr')[0];
				var mData = transactionTable.fnGetData(data);
				if(mData != null) {
					//$.print("#PrintDiv");
					$('#myTransactionDetails').modal();
					$('#transactionDetailsLabel').text('Transaction Details');
					$(".modal-body #PrintDiv").html(mData[5]);
				}				 
			});
		},
		"bAutoWidth" : false, 
		aoColumnDefs: [
           { aTargets: [ 4 ], bSortable: false }
		]			
    });	
	var startDate = new Date();
    $("#txtFromDate").datepicker('setEndDate',startDate);
    $('#txtToDate').datepicker('setStartDate', startDate);

	//focus on first field
	$("#iconSearch").click(function() {			// show popup for search patient
        $('#searchModalLabel').text("Search Patient");
        $('#searchModalBody').load('views/admin/view_patients.html');
        $('#myTransactionModal').modal();
        $('#txtPatientId').removeClass('errorStyle');
        $('#errorPatientId').text('');
        transactionTable.fnClearTable();
    });

	$('#txtPatientId').focus();
	// show date picker 
	$('#txtFromDate').datepicker();
	$('#txtToDate').datepicker();
	
	// change functionality on date picker
	$("#txtFromDate").change(function() {
	
		if ($("#txtFromDate").val() != "") {
			$("#errordob").hide();
			$("#txtFromDate").removeClass("errorStyle");
			$("#txtFromDate").datepicker("hide");
		}			
	});
	
	$("#txtToDate").change(function() {
	
		if ($("#txtToDate").val() != "") {
			$("#txtToDate").removeClass("errorStyle");
			$("#txtToDate").datepicker("hide");
		}			
	});
	
	$("#txtPatientId").keyup(function() {
	
		if ($("#txtPatientId").val() != "") {
			$("#errorPatientId").hide();
			$("#txtPatientId").removeClass("errorStyle");			
		}
	});
	
	// reset functioanlity
	$('#btnReset').on('click',function() {
		// clear details
		clearDetails();
	});
	
	// print functioanlity
	$('#printTransactionDetails').on('click',function() {
		$.print("#PrintDiv");
	});
	// print statement on click
	$('#btnShow').on('click',function() {
		transactionTable.fnClearTable();
		/*firstly empty page after that load data*/
		$("#codexplBody").find("tr:gt(0)").html("");
		var flag = false;
		if($("#txtPatientId").val().length != 9) {
			$("#errorPatientId").show();
			$("#errorPatientId").text("Please enter valid patient Id");
			$("#txtPatientId").addClass("errorStyle");
			$("#txtPatientId").focus();
			flag = true;
		}
		if($("#txtPatientId").val() == '') {
			$("#errorPatientId").show();
			$("#errorPatientId").text("Please enter patient Id");
			$("#txtPatientId").addClass("errorStyle");
			$("#txtPatientId").focus();
			flag = true;
		}
		if(flag == true)
		{
			return false;
		}

		var patientId = $("#txtPatientId").val();
		var patientPrefix = patientId.substring(0, 3);
		patientId = patientId.replace ( /[^\d.]/g, '' ); 
		patientId = parseInt(patientId);
		
		var fromDate = $('#txtFromDate').val();
		var toDate = $('#txtToDate').val();
		debugger;
		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/transaction_statement.php",
			data: {
				"operation": "showTransactionStatement",
				"patientId" :patientId,
				"fromDate" :fromDate,
				"toDate" :toDate,
				"patientPrefix" : patientPrefix
			},
			success: function(data) {
				if (data.length > 2) {
					var parseData = jQuery.parseJSON(data);
					for(var i=0; i< parseData.length; i++){
						var patientId = parseData[i].patient_id;
						var prefix = parseData[i].patient_prefix;
						
						var patientIdLength = patientId.toString().length;
						for (var j=0;j<6-patientIdLength;j++) {
							patientId = "0"+patientId;
						}
						var receiptNo = parseData[i].invoice_id
						var receiptNoLength = receiptNo.toString().length;
						for(k=0;k<6-receiptNoLength;k++){
							receiptNo = "0"+receiptNo;
						}
						patientId = prefix+patientId;
						var transactionsDate = parseData[i].transactionsDate;
						var type_description = parseData[i].type_description;
						var invoice_details = parseData[i].invoice_details;

						/*Apply here span to recognise previous or current data*/
						transactionTable.fnAddData([patientId,transactionsDate,type_description,receiptNo,'<i class="fa fa-eye transactionDetails" id="btnView" data-id="1" style="font-size: 16px;     margin-left: 20px; cursor:pointer;" title="view" value="view"></i>',invoice_details]);
					}
					/* $("#PrintDiv").html(parseData[0].details);
					$.print("#PrintDiv"); */
				}
				else{
					callSuccessPopUp("Alert","No records found !!!");
				}
				if(data == ""){
					$("#errorPatientId").show();
					$("#errorPatientId").text("Please enter valid patient id");
					$("#txtPatientId").addClass("errorStyle");
					$("#txtPatientId").focus();					
				}
			},
			error: function() {
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	});	
});
$('#searchModalBody #btnBackToList').click(function(){
	$('#searchPatient').show();
	$('#searchPatientdetail').hide();

});
$('#txtPatientId').keypress(function (e) {
	    if (e.which == 13) {
		    $("#btnShow").click();
    	}
 });
// clear details function
function clearDetails() {
	$("#errorPatientId").hide();
	$("#txtPatientId").removeClass("errorStyle");
	$("#txtToDate").val("");
	$("#txtFromDate").val("");
	$("#txtPatientId").val("");
	$("#txtPatientId").focus();
	transactionTable.fnClearTable();
}