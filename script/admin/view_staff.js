/*
 * File Name    :   view_staff.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load and save inforamtion
 */
var otable; // define global variable for datatable
$(document).ready(function() {
loader();
debugger;
    //call function to bind stafftype and designation
    bindDesignation();
    bindStaffType();

    //for review staff screen hide the title of screen
    if($("#reviewStaffHiddenId").val() == "1"){
        $("#myViewStaffModal .block-title").hide();
    }
    var imagePath;
    $("#btnReset").click(function() { // reset functionality
        $(".chk-active_inactive").hide();
		
		$(".inactive-checkbox").prop('checked', false);
        $("#txtStaffId").val("");
        $("#txtFirstName").val("");
        $("#txtLastName").val("");
        $("#selDesignation").val("-1");
        $("#selStaffType").val("-1");
        $("#txtMobile").val("");
        otable.fnClearTable();
    });
	
	// load staff details 
	checkActiveStaffList();
		
    otable = $('#viewStaffTable').dataTable({ // inilize datatable
        "bFilter": true,
        "bProcessing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false
    });
	$("#btnSearch").unbind();
    $("#btnSearch").click(function() { // perform search functioanlity
        if ($('.inactive-checkbox').is(":checked")) {

			var staffId = $("#txtStaffId").val().trim();            
            if (staffId !='') {
                if (staffId.length != 9) {
                    otable.fnClearTable();
                    return false;
                }
                var getStaffPrefix = staffId.substring(0, 3);
                if (getStaffPrefix.toLowerCase() != staffPrefix.toLowerCase()) {
                    return false;
                }
                staffId = staffId.replace ( /[^\d.]/g, '' ); 
                staffId = parseInt(staffId);
            }            
            var firstName = $("#txtFirstName").val().trim();
            var lastName = $("#txtLastName").val().trim();
            var designation = $("#selDesignation").val().trim();
            var stafftype = $("#selStaffType").val().trim();
            var mobile = $("#txtMobile").val().trim();

            var postData = {
                "operation": "searchInactiveData",
                "staffId": staffId,
                "firstName": firstName,
                "lastName": lastName,
                "designation": designation,
                "stafftype": stafftype,
                "mobile": mobile
            }

            $.ajax({
                type: "POST",
                cache: false,
                url: "controllers/admin/view_staff.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    dataSet = JSON.parse(data);
                    //var dataSet = jQuery.parseJSON(data);
                    otable.fnClearTable();
                    otable.fnDestroy();
                    otable = $('#viewStaffTable').DataTable({
                        "sPaginationType": "full_numbers",
						"bAutoWidth" : false,
                        "fnDrawCallback": function(oSettings) {
							$(".restore").on('click', function() {
								$('.modal-body').text("");
								var id = $(this).attr('data-id');
								restoreClick(id);
							}); 

                            //review staff seach
                            $('#myViewStaffModal #viewStaffTable tbody tr').on('dblclick', function() {

                                if ($(this).hasClass('selected')) {
                                    $(this).removeClass('selected');
                                } else {
                                    otable.$('tr.selected').removeClass('selected');
                                    $(this).addClass('selected');
                                }

                                var mData = otable.fnGetData(this); // get datarow
                                if (null != mData) // null if we clicked on title row
                                {
                                    var compelteStaffId = $(this).find('td:eq(0)').text();
                                    $('#txtStaffId').val(compelteStaffId);
                                    $('#txtStaffName').val(mData["name"]);
                                }

                                $("#btnStaffSelect").click();
                                $(".close").click();
                            });
                          
                        },
						
                        "aaData": dataSet,
                        "aoColumns": [{
							"mData": function (o) {  
                                var staffId = o["id"];
                                    
                                var staffIdLength = staffId.length;
                                for (var i=0;i<6-staffIdLength;i++) {
                                    staffId = "0"+staffId;
                                }
                                staffId = staffPrefix+staffId;
                                return staffId; 
                            }
						}, {
							"mData": "type"
						}, {
							"mData": "designation"
						}, {
							"mData": "name"
						}, {
							"mData": function(mobile){
                                var data = mobile;
                                var mobileCode = data['country_code'];
                                var mobile = data['mobile'];
                                var mobile = mobileCode + mobile;
                                return mobile;
                            }
						}, {
							"mData": function(o) {
								var data = o;
								var id = data["id"];
								return '<i class="ui-tooltip fa fa-pencil-square-o restore" data-id=' + id + ' style="font-size: 15px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
							}
						}, ],
						aoColumnDefs: [{
							'bSortable': false,
							'aTargets': [5]
						}]

					});
				},
                error: function() {
					
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
            });
        } else {
            checkActiveStaffList();	// call search on checkbox is checked
        }
    });

    
	$("#mySearchModal #btnSearch").unbind();
     // load on search modal in access management screen
    $("#mySearchModal #btnSearch").click(function() {

        var staffId = $("#mySearchModal #txtStaffId").val().trim();
        if (staffId !='') {
            if (staffId.length != 9) {
                otable.fnClearTable();
                return false;
            }
            var getStaffPrefix = staffId.substring(0, 3);
            if (getStaffPrefix.toLowerCase() != staffPrefix.toLowerCase()) {
                return false;
            }
            staffId = staffId.replace ( /[^\d.]/g, '' ); 
            staffId = parseInt(staffId);
        }
        var firstName = $("#mySearchModal #txtFirstName").val().trim();
        var lastName = $("#mySearchModal #txtLastName").val().trim();
        var designation = $("#mySearchModal #selDesignation").val().trim();
        var stafftype = $("#mySearchModal #selStaffType").val().trim();
        var mobile = $("#mySearchModal #txtMobile").val().trim();

        var postData = {
            "operation": "search",
            "staffId": staffId,
            "firstName": firstName,
            "lastName": lastName,
            "designation": designation,
            "stafftype": stafftype,
            "mobile": mobile
        }

        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/view_staff.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                dataSet = JSON.parse(data);
                loader();
                otable.fnClearTable();
                otable.fnDestroy();
                otable = $('#mySearchModal #viewStaffTable').DataTable({
                    "sPaginationType": "full_numbers",
					"bAutoWidth" : false,
                    "fnDrawCallback": function(oSettings) {

                        $('#mySearchModal #viewStaffTable tbody tr').on('dblclick', function() {

                            if ($(this).hasClass('selected')) {
                                $(this).removeClass('selected');
                            } else {
                                otable.$('tr.selected').removeClass('selected');
                                $(this).addClass('selected');
                            }

                            var mData = otable.fnGetData(this); // get datarow
                            if (null != mData){
                                var compelteStaffId = $(this).find('td:eq(0)').text();
                                $('#txtStaffId').val(compelteStaffId);
                            }

                            $("#btnStaffSelect").click();
                            $(".close").click();
                        });

                        //this db click for review staff screen
                        $('#myViewStaffModal #viewStaffTable tbody tr').on('dblclick', function() {

                            if ($(this).hasClass('selected')) {
                                $(this).removeClass('selected');
                            } else {
                                otable.$('tr.selected').removeClass('selected');
                                $(this).addClass('selected');
                            }

                            var mData = otable.fnGetData(this); // get datarow
                            if (null != mData) // null if we clicked on title row
                            {
                                var compelteStaffId = $(this).find('td:eq(0)').text();
                                $('#txtStaffId').val(compelteStaffId);
                                $('#txtStaffName').val(mData["id"]);
                            }

                            $("#btnStaffSelect").click();
                            $(".close").click();
                        });
                    },
                    "aaData": dataSet,
                    "aoColumns": [{
                        "mData": function (o) {  
                            var staffId = o["id"];
                                
                            var staffIdLength = staffId.length;
                            for (var i=0;i<6-staffIdLength;i++) {
                                staffId = "0"+staffId;
                            }
                            staffId = staffPrefix+staffId;
                            return staffId; 
                        }
                    }, {
                        "mData": "type"
                    }, {
                        "mData": "designation"
                    }, {
                        "mData": "name"
                    }, {
                        "mData": function(mobile){
                            var data = mobile;
                            var mobileCode = data['country_code'];
                            var mobile = data['mobile'];
                            var mobile = mobileCode + mobile;
                            return mobile;
                        }
                    }],
                });

            },
            error: function() {
                alert('error');
            }
        });
        $("#mySearchModal #btnBackToList").click(function() {
            $("#mySearchModal #searchPatient").show();
            $("#mySearchModal #searchPatientdetail").hide();
        });
    }); 

    $('.inactive-checkbox').change(function() { // show data on check event
        if ($('.inactive-checkbox').is(":checked")) {
           checkedDatatable();
        } else {
            uncheckedDatatable(); // calling uncheckedDatatable function
        }
    });
	if ($("#thing").val() == undefined) {
        $("#mySearchModal #btnSearch").click();
    }
});
// bind designation
function bindDesignation() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/view_staff.php",
        data: {
            "operation": "showDesignation"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].designation + "</option>";
                }
                $('#selDesignation').html(option);

            }
        },
        error: function() {

        }
    });
}
// bind bind staff type
function bindStaffType() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/view_staff.php",
        data: {
            "operation": "showStaffType"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
                }
                $('#selStaffType').html(option);

            }
        },
        error: function() {

        }
    });
}

// perform delete click function
function deleteClick(id) {
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Delete Staff");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteStaff("' + type + '");');
}
// perform restore click function
function restoreClick(id) {
    $('#selectedRow').val(id);
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Restore Staff");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteStaff("' + type + '");');
}

// call for delete and restore value in that function
function deleteStaff(type) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "deleteStaffDetail",
            "id": id
        }
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/view_staff.php",
            data: postData,
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Staff deleted sccessfully!!!");
                    $('#messagemyModal').modal();
                    uncheckedDatatable(); // calling uncheckedDatatable function

                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    } else {
        var id = $('#selectedRow').val();
        $('.modal-body').text("");
        var postData = {
            "operation": "restoreStaffDetail",
            "id": id
        }
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/view_staff.php",
            data: postData,
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messagemyModal #messageMyModalLabel').text("Success");
                    $('#messagemyModal .modal-body').text("Staff restored successfully!!!");
                    $('#messagemyModal').modal();
					checkedDatatable();	// calling checkedDatatable function
                  
                }
            },
            error: function() {
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });

    }
}

// for unchecked function
function uncheckedDatatable() {
    otable.fnClearTable();
    otable.fnDestroy();
    otable = "";
    otable = $('#viewStaffTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
		"bAutoWidth" : false,
        "sAjaxSource": "controllers/admin/view_staff.php",
        "fnServerParams": function(aoData) {
            aoData.push({
                "name": "operation",
                "value": "unChecked"
            });
        },
        "fnDrawCallback": function(oSettings) {
			
            //on update click click load staffregistration form in modal body 
            $(".update").on('click', function() {
                var id = $(this).attr('data-id');
                $("#dataTableHiddenId").val(id);
                //update hidden field value eqauls to one
                $("#getValueToChangeJs").val('1');


                $("#updateStaffModelLabel").text("Update Staff Information");

                //loading the staff registration form
                $(".edit-modal-body").load('views/admin/staff_registration_form.html');
                $("#myUpdateStaffModel").modal();
            });


            $(".staffDetails").on('click', function() {

                $('#myStaffModel').modal("show");
                $('#staffModelLabel').text("Staff Details");

                var id = $(this).attr('data-id');
                var postData = {
                    "operation": "staffDetail",
                    "id": id
                }
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: "controllers/admin/view_staff.php",
                    datatype: "json",
                    data: postData,

                    success: function(data) {
                        $('#myStaffModel').modal("show");
                        $('#staffModelLabel').text("Staff Details");
                        if (data != "0" && data != "") {
                            var parseData = jQuery.parseJSON(data);

                            for (var i = 0; i < parseData.length; i++) {
                                $("#lblSalutation").text(parseData[i].salutation);
                                $("#userreimage").hide();
                                $("#imagetext").hide();
								if (parseData[i].images_path != null) {
									var imagePath = "./upload_images/staff_dp/" + parseData[i].images_path;
									$("#imagebox").css({
										"background-image": "url(" + imagePath + ")"
									});
								}
								if (parseData[i].images_path == null || parseData[i].images_path == "") {
									$("#imagebox").css({
										"background-image": "url(./images/default.jpg)"
									}); // for default image
								}
                                var staffId = parseData[i].id;
                    
                                var staffIdLength = staffId.length;
                                for (var j=0;j<6-staffIdLength;j++) {
                                    staffId = "0"+staffId;
                                }
                                staffId = staffPrefix+staffId;

                                $("#lblStaffId").text(staffId);
								$("#lblName").text(parseData[i].staff_name);
                                $("#lblEmail").text(parseData[i].email_id);
                                $("#lblDesignation").text(parseData[i].designation);
                                $("#lblStaffType").text(parseData[i].type);
                                var mobile = parseData[i].country_code + parseData[i].mobile;
                                $("#lblMobile").text(mobile);
                               
                                $("#lblDOB").text(parseData[i].dob);
                             
								 if (parseData[i].martial_status == "M") {
									$("#lblMartialStatus").text("Married");
								}
								if (parseData[i].martial_status == "U") {
									$("#lblMartialStatus").text("Unmarried");
								}
								if (parseData[i].gender == "M") {
									$("#lblGender").text("Male");
								}
								if (parseData[i].gender == "F") {
									$("#lblGender").text("Female");
								}
                                $("#lblAddress").text(parseData[i].address);
                            }


                        }
                    },
                    error: function() {
						
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
                });
            });

            // delete event perform
            $(".deleteStaffDetails").on('click', function() {
                $('.modal-body').text("");
                var id = $(this).attr('data-id');
                deleteClick(id);

            });
        },

        "aoColumns": [{
            "mData": function (o) {  
                var staffId = o["id"];
                    
                var staffIdLength = staffId.length;
                for (var i=0;i<6-staffIdLength;i++) {
                    staffId = "0"+staffId;
                }
                staffId = staffPrefix+staffId;
                return staffId; 
            }
        }, {
            "mData": "type"
        }, {
            "mData": "designation"
        }, {
            "mData": "name"
        }, {
            "mData": function(mobile){
                var data = mobile;
                var mobileCode = data['country_code'];
                var mobile = data['mobile'];
                var mobile = mobileCode + mobile;
                return mobile;
            }
        }, {
            "mData": function(o) {
                var data = o;
                var id = data["id"];
                return "<i class='staffDetails fa fa-eye' id='btnView' title='View' data-id=" + id + " " + "'></i>" +
                    "<i class='ui-tooltip  fa fa-pencil update' data-id=" + id + " id='iconEditStaff' title='Edit'" +
                    "data-original-title='Edit'></i>" +
                    "<i class='fa fa-trash-o deleteStaffDetails' data-id=" + id + " title='Delete'" +
                    "data-original-title='Delete'></i>";
            }
        }, ],
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [5]
        }]
    });
}
//on click of button show data in datatable that searched
function checkActiveStaffList() {
    $(".chk-active_inactive").removeAttr("style"); //if we click on search button then it will show    
    var staffId = $("#txtStaffId").val().trim();
    if (staffId !='') {
        if (staffId.length != 9) {
            otable.fnClearTable();
            return false;
        }
        var getStaffPrefix = staffId.substring(0, 3);
        if (getStaffPrefix.toLowerCase() != staffPrefix.toLowerCase()) {
            return false;
        }
        staffId = staffId.replace ( /[^\d.]/g, '' ); 
        staffId = parseInt(staffId);
    }    
    var firstName = $("#txtFirstName").val().trim();
    var lastName = $("#txtLastName").val().trim();
    var designation = $("#selDesignation").val().trim();
    var stafftype = $("#selStaffType").val().trim();
    var mobile = $("#txtMobile").val().trim();

    var postData = {
        "operation": "search",
        "staffId": staffId,
        "firstName": firstName,
        "lastName": lastName,
        "designation": designation,
        "stafftype": stafftype,
        "mobile": mobile
    }

    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/view_staff.php",
        datatype: "json",
        data: postData,

        success: function(data) {
            dataSet = JSON.parse(data);
            //var dataSet = jQuery.parseJSON(data);
            otable.fnClearTable();
            otable.fnDestroy();
            otable = $('#viewStaffTable').DataTable({
                "sPaginationType": "full_numbers",
				"bAutoWidth" : false,
                "fnDrawCallback": function(oSettings) {

                    //on update click click load staffregistration form in modal body 
                    $(".update").on('click', function() {
                        var id = $(this).attr('data-id');
                        $("#dataTableHiddenId").val(id);
                        //update hidden field value eqauls to one
                        $("#getValueToChangeJs").val('1');
                        $("#updateStaffModelLabel").text("Update Staff Information");

                        //loading the staff registration form
                        $(".edit-modal-body").load('views/admin/staff_registration_form.html');
                        $("#myUpdateStaffModel").modal();
                    });

                    $('#myViewStaffModal #viewStaffTable tbody tr').on('dblclick', function() {

                        if ($(this).hasClass('selected')) {
                            $(this).removeClass('selected');
                        } else {
                            otable.$('tr.selected').removeClass('selected');
                            $(this).addClass('selected');
                        }

                        var mData = otable.fnGetData(this); // get datarow
                        if (null != mData) // null if we clicked on title row
                        {   
                            var compelteStaffId = $(this).find('td:eq(0)').text();
                            $('#txtStaffId').val(compelteStaffId);
                            $('#txtStaffName').val(mData["name"]);
                        }

                        $("#btnStaffSelect").click();
                        $(".close").click();
                    });

                    $(".staffDetails").on('click', function() {

                        $('#myStaffModel').modal("show");
                        $('#staffModelLabel').text("Staff Details");

                        var id = $(this).attr('data-id');
                        var postData = {
                            "operation": "staffDetail",
                            "id": id
                        }
                        $.ajax({
                            type: "POST",
                            cache: false,
                            url: "controllers/admin/view_staff.php",
                            datatype: "json",
                            data: postData,

                            success: function(data) {
                                $('#myStaffModel').modal("show");
                                $('#staffModelLabel').text("Staff Details");
                                if (data != "0" && data != "") {
                                    var parseData = jQuery.parseJSON(data);

                                    for (var i = 0; i < parseData.length; i++) {
                                        $("#lblSalutation").text(parseData[i].salutation);
                                        $("#userreimage").hide();
                                        $("#imagetext").hide();
                                        if (parseData[i].images_path != null) {
                                            imagePath = "./upload_images/staff_dp/" + parseData[i].images_path;
                                            $("#imagebox").css({
                                                "background-image": "url(" + imagePath + ")"
                                            });
                                        }
                                        if (parseData[i].images_path == null || parseData[i].images_path == "") {
                                            $("#imagebox").css({
                                                "background-image": "url(./img/default.jpg)"
                                            });
                                        }
                                        var staffId = parseData[i].id;
                    
                                        var staffIdLength = staffId.length;
                                        for (var j=0;j<6-staffIdLength;j++) {
                                            staffId = "0"+staffId;
                                        }
                                        staffId = staffPrefix+staffId;
                                        $("#lblStaffId").text(staffId);
										$("#lblName").text(parseData[i].staff_name);
                                        $("#lblEmail").text(parseData[i].email_id);
                                        $("#lblDesignation").text(parseData[i].designation);
                                        $("#lblStaffType").text(parseData[i].type);
                                        var mobile = parseData[i].country_code + parseData[i].mobile;
                                        $("#lblMobile").text(mobile);
                                        if (parseData[i].martial_status == "M") {
                                            $("#lblMartialStatus").text("Married");
                                        }
                                        if (parseData[i].martial_status == "U") {
                                            $("#lblMartialStatus").text("Unmarried");
                                        }
										if (parseData[i].gender == "M") {
                                            $("#lblGender").text("Male");
                                        }
                                        if (parseData[i].gender == "F") {
                                            $("#lblGender").text("Female");
                                        }
                                        $("#lblDOB").text(parseData[i].dob);
                                        
                                        $("#lblAddress").text(parseData[i].address);

                                        if (parseData[i].joining_date !='') {
                                            $("#lblJod").text(parseData[i].joining_date);
                                        }
                                        else {
                                            $("#lblJod").text("N/A");
                                        }

                                        if (parseData[i].department !='') {
                                            $("#lblDept").text(parseData[i].department);
                                        }
                                        else {
                                            $("#lblDept").text("N/A");
                                        }

                                        if (parseData[i].remarks !='') {
                                            $("#lblRemarks").text(parseData[i].remarks);
                                        }
                                        else {
                                            $("#lblRemarks").text("N/A");
                                        }

                                        if (parseData[i].emergency_contact !='') {
                                            $("#lblEmergencyNo").text(parseData[i].emergency_contact);
                                        }
                                        else {
                                            $("#lblEmergencyNo").text("N/A");
                                        }

                                        if (parseData[i].emergency_name !='') {
                                            $("#lblEmergencyPerson").text(parseData[i].emergency_name);
                                        }
                                        else {
                                            $("#lblEmergencyPerson").text("N/A");
                                        }
                                       
                                        if (parseData[i].degree_held !='') {
                                            $("#lblDegree").text(parseData[i].degree_held);
                                        }
                                        else {
                                            $("#lblDegree").text("N/A");
                                        }                                        
                                        if (parseData[i].referred_by !='') {
                                            $("#lblRefferedBy").text(parseData[i].referred_by);
                                        }
                                        else{
                                            $("#lblRefferedBy").text("N/A");
                                        }
                                        if (parseData[i].phone !='') {
                                            $("#lblPhone").text(parseData[i].phone);
                                        }
                                        else{
                                            $("#lblPhone").text("N/A");
                                        }
                                        
                                        $("#lblZipcode").text(parseData[i].zip);
                                        $("#lblCity").text("");
                                        $("#lblState").text("");
                                        $("#lblCountry").text(parseData[i].country);
                                        $("#lblState").text(parseData[i].state);
                                        $("#lblCity").text(parseData[i].city);

                                    }
                                }
                            },
                            error: function() {
								
								$('.modal-body').text("");
								$('#messageMyModalLabel').text("Error");
								$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
								$('#messagemyModal').modal();
							}
                        });
                    });
					// perform delete event
                    $(".deleteStaffDetails").on('click', function() {
                        $('.modal-body').text("");
                        var id = $(this).attr('data-id');
                        deleteClick(id);
                    });
                },
                "aaData": dataSet,
                "aoColumns": [{
                    "mData": function (o) {  
                        var staffId = o["id"];
                            
                        var staffIdLength = staffId.length;
                        for (var i=0;i<6-staffIdLength;i++) {
                            staffId = "0"+staffId;
                        }
                        staffId = staffPrefix+staffId;
                        return staffId; 
                    }
                }, {
                    "mData": "type"
                }, {
                    "mData": "designation"
                }, {
                    "mData": "name"
                }, {
                    "mData": function(mobile){
                        var data = mobile;
                        var mobileCode = data['country_code'];
                        var mobile = data['mobile'];
                        var mobile = mobileCode + mobile;
                        return mobile;
                    }
                }, {
                    "mData": function(o) {
                        var data = o;
                        var id = data["id"];
                        return "<i class='staffDetails fa fa-eye' id='btnView' title='View' data-id=" + id + " " + "'></i>" +
                            "<i class='ui-tooltip  fa fa-pencil update' data-id=" + id + " id='iconEditStaff' title='Edit'" +
                            "data-original-title='Edit'></i>" +
                            "<i class='fa fa-trash-o deleteStaffDetails' data-id=" + id + " title='Delete'" +
                            "data-original-title='Delete'></i>";
                    }
                }, ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [5]
                }]
            });

        },
        error: function() {
			
			$('.modal-body').text("");
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
    });
}
// for checked function
function checkedDatatable(){
	otable.fnClearTable();
	otable.fnDestroy();
	otable = "";
	otable = $('#viewStaffTable').dataTable({
		"bFilter": true,
		"processing": true,
		"bAutoWidth" : false,
		"sPaginationType": "full_numbers",
		"fnDrawCallback": function(oSettings) {
			$(".restore").on('click', function() {
				$('.modal-body').text("");
				var id = $(this).attr('data-id');
				restoreClick(id);
			});
		},
		"sAjaxSource": "controllers/admin/view_staff.php",
		"fnServerParams": function(aoData) {
			aoData.push({
				"name": "operation",
				"value": "checked"
			});
		},

		"aoColumns": [{
			"mData": function (o) {  
                var staffId = o["id"];
                    
                var staffIdLength = staffId.length;
                for (var i=0;i<6-staffIdLength;i++) {
                    staffId = "0"+staffId;
                }
                staffId = staffPrefix+staffId;
                return staffId; 
            }
		}, {
			"mData": "type"
		}, {
			"mData": "designation"
		}, {
			"mData": "name"
		}, {
			"mData": function(mobile){
                var data = mobile;
                var mobileCode = data['country_code'];
                var mobile = data['mobile'];
                var mobile = mobileCode + mobile;
                return mobile;
            }
		}, {
			"mData": function(o) {
				var data = o;
				var id = data["id"];
				return '<i class="ui-tooltip fa fa-pencil-square-o restore" data-id=' + id + ' style="font-size: 15px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
			}
		}, ],
		aoColumnDefs: [{
			'bSortable': false,
			'aTargets': [5]
		}]

	});
}
//click escape button to close popup
$(document).keyup(function(event) {
    if (event.keyCode === 27) {
        $('.close').click();
    }
});