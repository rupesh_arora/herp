var admittedPatientTbl = '';
$(document).ready(function() {

	admittedPatientTbl = $('#admittedPatientTbl').DataTable({});

	debugger;
	$(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});

	$("#toDate").attr("disabled",true);
	$("#fromDate").attr("disabled",true);	

	$('.changeRadio').change(function() {
		if ($('#radToFromDate').prop( "checked") == true) {
	        $("#toDate").attr("disabled",false);
			$("#fromDate").attr("disabled",false);
	    }
	    else{
	    	$("#toDate").attr("disabled",true);
			$("#fromDate").attr("disabled",true);
			$("#toDate").val("");
			$("#fromDate").val("");
	    }
	});	
	$("#fromDate").change(function() {
		if ($("#fromDate").val() !='') {
			$("#fromDate").removeClass("errorStyle");
			$("#txtFromDateError").text("");
		}
	});
	$("#toDate").change(function() {
		if ($("#toDate").val() !='') {
			$("#toDate").removeClass("errorStyle");
			$("#txtToDateError").text("");
		}
	});	

	$("#viewReport").on("click",function(){
		var fromDate ;
		var toDate ;
		var arr = new Array();
		var opts = new Array();
		if ($('#radToday').prop( "checked") == true) {
			dataLoad = "today";
		}
		else if($('#radLastSevenDays').prop( "checked") == true) {
			dataLoad = "last7days";
		}
		else if($('#radToFromDate').prop( "checked") == true) {
			dataLoad = "betweenDate";
			fromDate = $("#fromDate").val();
			toDate = $("#toDate").val();
			if (fromDate =='') {
				$("#txtFromDateError").text("Please choose from date");
				$("#fromDate").addClass("errorStyle");
				$("#fromDate").focus();
				return false;
			}
			if (toDate =='') {
				$("#txtToDateError").text("Please choose from date");
				$("#toDate").addClass("errorStyle");
				$("#toDate").focus();
				return false;
			}
		}
		else if($('#radLastThirtyDays').prop( "checked") == true) {
			dataLoad = "last30days";
		}
		else if($('#radAllTime').prop( "checked") == true) {
			dataLoad = "all";
		}

		if (fromDate == undefined) {
			fromDate ="";
		}
		if (toDate == undefined) {
			toDate = "";
		}
		if (dataLoad==undefined) {
			return false;
		}
		var postData = {
			"operation" : "showChartData",
			"dataLoad" : dataLoad,
			"fromDate" : fromDate,
			"toDate" : toDate
		}
	});

	$("#clear").click(function(){
		clear();
	});

	$("#btnSavePdf").click(function(){
		$.print("#printableArea");
	});
	
});
function clear(){
	$(".chartDiv,#btnSaveExcel,#btnSavePdf").hide();
	$("#toDate,#fromDate").val("");
	$("#txtFromDateError,#txtToDateError").text("");
	$("#fromDate,#toDate").removeClass("errorStyle");
}