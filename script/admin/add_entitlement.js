var entitlmentTable; //defing a global variable for datatable
$(document).ready(function() {
    loader();    
    debugger;    
    bindLeavePeriod();
    bindLeaveType();
    bindStaffType();
    removeErrorMessage();

    $("#advanced-wizard").hide();
    $("#wardList").addClass('list');    
    $("#tabWardList").addClass('tab-list-add');
   
    /*Click for add the ward*/
    $("#tabAddWard").click(function() {
        showAddTab();
        clear();
    });

    //Click function for show the ward lists
    $("#tabWardList").click(function() {
        // Call the function for show the ward lists
        tabWardList();
    });

    entitlmentTable = $('#tblAddEntitlements').dataTable( {
        "bFilter": true,
        "processing": true,
        "sPaginationType":"full_numbers",
        "fnDrawCallback": function ( oSettings ) {
        },
        
        "sAjaxSource":"controllers/admin/add_entitlement.php",
        "fnServerParams": function ( aoData ) {
          aoData.push( { "name": "operation", "value": "show" });
        },

        /*defining datatables column*/
        "aoColumns": [
            { "mData": "staff_type" },
            { "mData": "leave_name" },
            { "mData": "leave_period" },
            { "mData": "entitlements" }, 
        ],
        /*False sort for following columns*/
        aoColumnDefs: [
           { 'bSortable': false, 'aTargets': [ 2 ] },
           { 'bSortable': false, 'aTargets': [ 3 ] }
        ]
    } );

    $("#selStaffType").focus();
    $("#ok").click(function(){
        $("#selStaffType").focus();
    });
    $("#btnSaveEntitlement").click(function() {
        var flag = 'false';
        if(validTextField('#selStaffType','#selStaffTypeError','Please select staff type') == true)
        {
            flag = true;
        }
        if(validTextField('#selLeaveType','#selLeaveTypeError','Please select leave type') == true)
        {
            flag = true;
        }
        if(validTextField('#selLeavePeriod','#selLeavePeriodError','Please select leave period') == true)
        {
            flag = true;
        }
        if(validTextField('#txtEntitlement','#txtEntitlementError','Please enter leave entitlements') == true)
        {
            flag = true;
        }
        if ($("#txtEntitlement").val() >365) {
            $("#txtEntitlementError").text("Can't provide more than 365 holidays.");
            $("#txtEntitlement").focus();
            $("#txtEntitlement").addClass("errorStyle");
            flag = true;
        }
        if ($("#txtEntitlement").val() < 0) {
            $("#txtEntitlementError").text("Holiday can't be negative.")
            $("#txtEntitlement").focus();
            $("#txtEntitlement").addClass("errorStyle");
            flag = true;
        }
        if (flag == true) {
            return false;
        }
        
        var staffType = $("#selStaffType").val();
        var leaveType = $("#selLeaveType").val();
        var leavePeriod = $("#selLeavePeriod").val();
        var entitlement = $("#txtEntitlement").val().trim();
        var postData = {
            "operation": "save",
            "staffType": staffType,
            "leaveType" : leaveType,
            "leavePeriod" : leavePeriod,
            "entitlement" : entitlement,
        }
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){        
            dataCall("controllers/admin/add_entitlement.php", postData, function (result){
                if (result.trim() == 1) {
                    callSuccessPopUp('Success','Saved successfully!!!');
                    clearFormDetails(".clearForm"); 
                    entitlmentTable.fnReloadAjax(); 
                    tabWardList();//call function to update list                  
                }
                else if (result.trim() == "0") {
                    callSuccessPopUp('Alert','This leave type for the same staff type and leave period is already exist!!!');
                }
                else{
                    callSuccessPopUp('Error','Try to refresh page once');
                    clearFormDetails(".clearForm");
                }
            });
        });
    });

    $("#btnReset").click(function(){
        clearFormDetails(".clearForm");
        $("#selStaffType").focus();
    });
});
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});

function bindStaffType() {
    var postData ={
        operation : "showStaffType"
    }
    dataCall("controllers/admin/add_entitlement.php", postData, function (result){
        if (result != null && result != "") {
            var parseData = jQuery.parseJSON(result);

            var option = "<option value=''>-- Select --</option>";
            for (var i = 0; i < parseData.length; i++) {
                option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
            }
            $('#selStaffType').html(option);
        } 
    });
}
function bindLeavePeriod() {
    var postData ={
        operation : "showLeavePeriod"
    }
    dataCall("controllers/admin/add_entitlement.php", postData, function (result){
        if (result != null && result != "") {
            var parseData = jQuery.parseJSON(result);

            var option = "<option value=''>-- Select --</option>";
            for (var i = 0; i < parseData.length; i++) {
                option += "<option value='" + parseData[i].id + "'>" + parseData[i].period + "</option>";
            }
            $('#selLeavePeriod').html(option);
        } 
    });
}
function bindLeaveType() {
    var postData ={
        operation : "showLeaveType"
    }
    dataCall("controllers/admin/add_entitlement.php", postData, function (result){
        if (result != null && result != "") {
            var parseData = jQuery.parseJSON(result);

            var option = "<option value=''>-- Select --</option>";
            for (var i = 0; i < parseData.length; i++) {
                option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
            }
            $('#selLeaveType').html(option);
        } 
    });
}

//function for show the list of wards
function tabWardList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
    $('#inactive-checkbox-tick').prop('checked', false).change();

    $("#tabAddWard").removeClass('tab-detail-add');
    $("#tabAddWard").addClass('tab-detail-remove');
    $("#tabWardList").removeClass('tab-list-remove');    
    $("#tabWardList").addClass('tab-list-add');
    $("#department_list").addClass('list'); 
    $('#tabAddWard').html("+Add Entitlements");
    clear();
}
function showAddTab(){
    $("#advanced-wizard").show();
    $(".blackborder").hide();
   
    $("#tabAddWard").addClass('tab-detail-add');
    $("#tabAddWard").removeClass('tab-detail-remove');
    $("#tabWardList").removeClass('tab-list-add');
    $("#tabWardList").addClass('tab-list-remove');
    $("#designation_list").addClass('list');
    $('#selStaffType').focus();
    removeErrorMessage();
}