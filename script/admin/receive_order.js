/*
 * File Name    :   receive_order.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Gupta
 * Created Date :   16th Feb, 2016
 * Description  :   This page is use for load and view data of orders
*/
var orgItemQty = '';
var viewOrderDetailsTable;
$(document).ready(function() {
	var printData = '';
	var printOrderId = '';
	var printInvoiceNumber ='';
	var printTotalBill = '';
	$(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});
	debugger;
	removeErrorMessage();
	//call function from common.js to load char of account
	bindGLAccount("#selGLAccount",'');
	bindGLAccount("#selAccountPayable","accPayable");
	bindLedger("#selLedger");//defined in common.js

	$(".amountPrefix").text(amountPrefix);
	orderDetailTable = $('#viewOrderDetailsTable').dataTable({ // inilize datatable
        "bFilter": true,
        "bProcessing": true,
        "bAutoWidth":false,
        "sPaginationType": "full_numbers",
        aoColumnDefs: [{'bSortable': false,'aTargets': [8]}],
        aLengthMenu: [["All"],["All"]]//declare so that show all rows
    });

	$("#txtReceiveOrderId").focus();

	var startDate = new Date();
	$("#txtMfgDate").datepicker('setEndDate',startDate);
	$('#txtExpDate').datepicker('setStartDate', startDate);

	$("#searchorderIcon").unbind();
	$("#searchorderIcon").on('click',function(){		
		$('#txtReceiveOrderIdError').text("");
		$('#txtReceiveOrderId').removeClass("errorStyle");
        $($('.myModaldata')[0]).load('views/admin/view_orders.html',function(){
        	if ($('#txtReceiveOrderId').val() !='') {
        		$("#receiveOrderDetails #txtOrderId").val($("#txtReceiveOrderId").val());
        		$("#receiveOrderDetails #btnSearch").click();
	        	$("#receiveOrderDetails #viewOrdersForm").addClass('hideForm');
	        }
        }); 
        $('#receiveOrderDetails').modal();
        $("#txtReceiveOrderId").attr('readonly',true);                
	});

	$("#btnShowOrderDetails").on("click",function(){
		
		var orderId = $("#txtReceiveOrderId").val().trim();

		if (orderId =='') {
			$("#txtReceiveOrderIdError").text("Please select order id");
			$("#txtReceiveOrderId").addClass('errorStyle');
			$("#txtReceiveOrderId").focus();
			return false;
		}
		var postData = {
			"operation" : "serachOrderId",
			"orderId" : orderId
		}
		dataCall("controllers/admin/receive_order.php", postData, function (result){
			if (result.length > 2) {
				$(".showOnClick").show();
				bindItem(orderId);
				$("#txtReceiveOrderId").attr('readonly',true);
			}
			else{
				$("#txtReceiveOrderId").addClass('errorStyle');
				$("#txtReceiveOrderId").focus();
				$("#txtReceiveOrderIdError").text("Please select valid order id");
				$(".showOnClick").hide();
			}
		});	
	});

	$("#btnAdd").click(function(){
		if ($(".showOnClick").attr('style') == undefined) {
			return false;
		}
		var flag = false;
		if(validTextField('#txtInvoiceNo','#txtInvoiceNoError','Please enter invoice number') == true)
		{
			flag = true;
		}
		if(validTextField('#txtBatchNo','#txtBatchNoError','Please enter batch number') == true)
		{
			flag = true;
		}
		if(validTextField('#txtUnitCost','#txtUnitCostError','Please enter unit cost') == true)
		{
			flag = true;
		}	
		if(validTextField('#txtQuantity','#txtQuantityError','Please enter quantity') == true)
		{
			flag = true;
		}	
		if(validTextField('#selItemName','#selItemNameError','Please enter item name') == true)
		{
			flag = true;
		}
		if(validTextField('#selGLAccount','#selGLAccountError','Please select GL Account.') == true)
		{
			flag = true;
		}
		if (flag == true) {
			return false
		}

		if (orgItemQty < parseInt($("#txtQuantity").val().trim())) {
			$("#txtQuantity").focus();
			$("#txtQuantity").addClass("errorStyle");
			$("#txtQuantityError").text("Can't add more than ordered quantity");
			return false;
		}
		 
		var itemName = $("#selItemName :selected").text();		
		var itemId = $("#selItemName").val();
		var glAccountName = $("#selGLAccount :selected").text();
		var glAccountId = $("#selGLAccount :selected").val();
		var quantity = parseInt($("#txtQuantity").val().trim());			
		var batchNo = $("#txtBatchNo").val().trim();
		var invoiceNo = $("#txtInvoiceNo").val().trim();
		var mfgDate = $("#txtMfgDate").val().trim();
		var expDate = $("#txtExpDate").val().trim();
		var orgUnitCost = parseInt($("#txtUnitCost").val().trim());

		var discount = '0';
		if ($("#txtDiscount").val().trim() !='') {
			var discount = parseInt($("#txtDiscount").val().trim());			
			var discountedUnitCost = (orgUnitCost*discount)/100;
			var discountedUnitCost = orgUnitCost-discountedUnitCost;
			var totalCost = discountedUnitCost*quantity;
		}
		else{
			var totalCost = orgUnitCost*quantity;
		}
				

		if (discountedUnitCost < 0 && discountedUnitCost !=undefined) {
			$("#txtDiscount").focus();
			$("#txtDiscount").addClass("errorStyle");
			$("#txtDiscountError").text("Please enter valid discount");			
		}

		else{
			//remove value from dropdown
			$("#selItemName option[value ="+ itemId +"]").remove();
			orderDetailTable.fnAddData([itemName,quantity,orgUnitCost,batchNo,mfgDate,expDate,
				discount,totalCost,glAccountName,"<input type='button' id ='newButtonColor' class='btnEnabled' onclick='rowDelete($(this));' value='Remove'>",
				itemId,glAccountId
			]);
	        $('#totalBill').text(parseInt($('#totalBill').text()) + parseInt(totalCost));//add total bill
	        $("#txtInvoiceNo").attr('readonly',true);
	        clearFormDetails(".showOnClick");
	        $("#txtInvoiceNo").val(invoiceNo);
	        curDate();
		}
	});
	


	$("#btnReceived").on("click",function(){
		var flag = false;
		
		
		//Checking whether datatable is empty or not
		
		if(validTextField('#selAccountPayable','#selAccountPayableError','Please select account payable.') == true)
		{
			flag = true;
		}
		if(validTextField('#selLedger','#selLedgerError','Please select ledger') == true)
		{
			flag = true;
		}
		if ($("#txtReceiveOrderId").val() =='') {
			$("#txtReceiveOrderIdError").text("Please load order id");
			$("#txtReceiveOrderId").addClass('errorStyle');
			$("#txtReceiveOrderId").focus();
			flag = true;
		}
		if (flag == true) {
			return false;
		}
        if (orderDetailTable.fnGetNodes() == '') {
        	callSuccessPopUp("Alert","Add data in table") ;
        	return false;
        }
        else{
        	var ledgerId = $("#selLedger").val();
        	var accountPayable = $("#selAccountPayable").val();
        	var data = orderDetailTable.fnGetData();
			printData = data;
        	var invoiceNumber = $("#txtInvoiceNo").val().trim();
	        var orderId = $("#txtReceiveOrderId").val().trim();
	        printOrderId = orderId;
	        printInvoiceNumber = invoiceNumber;
	        printTotalBill = $("#totalBill").text();


	        var postData = {
	        	"operation" : "saveReceivedDetails",
	        	"data" : JSON.stringify(data),
	        	"orderId" : orderId,
	        	"invoiceNumber" : invoiceNumber,
	        	ledgerId : ledgerId,
	        	accountPayable : accountPayable
	        }

	        /*Check wheteher array shouldn't be null*/
	        if (data.length > 0) {
	        	$.ajax({
					type: "POST",
					cache: false,
					url: "controllers/admin/receive_order.php",
					datatype:"json",
					data: postData,
					success : function(data){
						if(data !="" && data != "0" && data !="2" && data !="Already exist"){
							$('#myReceiveOrderModal').modal();
							clearReceiveOrderDetails();
							$("#txtInvoiceNo").val('');
							$("#txtReceiveOrderId").focus();
							$("#txtInvoiceNo").attr('readonly',false);
						}
						else if(data =="0"){
							$('#messageMyModalLabel').text("Error");
							$('.modal-body').text("Something awful happened!! Can't fetch old requests. Please try to contact admin.");
							$('#messagemyModal').modal();
							$("#txtInvoiceNo").attr('readonly',false);
						}
						else if(data =="Already exist"){
							callSuccessPopUp("Alert","Item already exist") ;
							$("#txtInvoiceNo").attr('readonly',false);
						}
						else{
							$('#messageMyModalLabel').text("Error");
							$('.modal-body').text("Not able to update orders");
							$('#messagemyModal').modal();
							$("#txtInvoiceNo").attr('readonly',false);
						}
					},
					error:function() {
						$('#messagemyModal').modal();
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					}
				});
	        }
	        else{
	        	$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Alert");
				$('.modal-body').text("Please choose any of the data");
	        }		        
	    }
	});

	$("#myReceiveOrderModal #print").click(function(){
		$("#codexplBody").find("tr:gt(0)").html("");

		/*hospital information through global varibale in main.js*/
		$("#lblPrintEmail").text(hospitalEmail);
		$("#lblPrintPhone").text(hospitalPhoneNo);
		$("#lblPrintAddress").text(hospitalAddress);
		if (hospitalLogo != "null") {
			imagePath = "./images/" + hospitalLogo;
			$("#printLogo").attr("src",imagePath);
		}
		$("#printHospitalName").text(hospitalName);
		$("#lblPrintOrderId").text(printOrderId);
		$("#lblInvoiceNumber").text(printInvoiceNumber);
		$("#lblPrintDate").text(todayDate());
		$("#printTotalBill").text(printTotalBill);

		for(var i=0;i<printData.length;i++){
			$("#codexplBody").append('<tr><td>'+ (i+1) +'</td><td>'+printData[i][0]+'</td><td>'
			+printData[i][1]+'</td><td>'+printData[i][2]+'</td><td>'+printData[i][3]+'</td><td>'
			+printData[i][4]+'</td><td>'+printData[i][5]+'</td><td>'+printData[i][8]+'</td><td>'
			+printData[i][6]+'</td><td>'+printData[i][7]);
		}

		/*Call print function*/
		//$.print("#PrintDiv");
		window.print();
	});

	$("#btnReset").on("click",function(){
		clearReceiveOrderDetails();
		clearFormDetails("#viewOrdersForm");
		$("#txtInvoiceNo").attr('readonly',false);
		$("#txtReceiveOrderId").attr('readonly',false);
		$('.showOnClick').hide();
	});

	$("#selItemName").change(function(){
		var itemId = $("#selItemName").val();
		if (itemId !='') {
			var orderId = $("#txtReceiveOrderId").val().trim();
			itemQty(itemId,orderId);
		}		
	});
	$("#txtReceiveOrderId").keyup(function(){
		if ($("#txtReceiveOrderId").val() !='') {
			$("#txtReceiveOrderId").removeClass("errorStyle");
			$("#txtReceiveOrderIdError").text("");
		}
	});	
    
    $('#txtReceiveOrderId').keypress(function (e) {
       
        if (e.which == 13) {
        	e.preventDefault();
            $("#btnShowOrderDetails").click();
        }
    });

   /* $("#txtMfgDate").datepicker({autoclose:true,endDate: '+0d'});
	$("#txtExpDate").datepicker({autoclose:true,startDate: '+0d'});*/
});
/*function showDetails(orderId){	
	var postData = {
		"operation": "showOrderDetails",
		"orderId": orderId
	}
	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/receive_order.php",
		datatype: "json",
		data: postData,

		success: function(data) {
			if (data !="0" && data !='') {
				dataSet = JSON.parse(data);
				orderDetailTable.fnClearTable();
				$('#totalBill').text("0");

				var counter = 1;
				var receiveOrderCounter =0 ;
				for(var i=0; i< dataSet.length; i++){

					var item = dataSet[i].item_name;
					var itemId = dataSet[i].item_id;
					var quantity = dataSet[i].quantity;
					var cost = dataSet[i].cost;
					var totalCost = cost * quantity;
					var checkStatus = dataSet[i].status;

					if (checkStatus =="received") {
						var receivedTr = orderDetailTable.fnAddData([counter,item, quantity,cost,totalCost,"<input type='checkbox' id='disabledCheckBox' class = 'order-received' disabled>",itemId]);//send one column null so user can't remove previous data
						var getCurrDataRow = orderDetailTable.fnSettings().aoData[ receivedTr ].nTr;	
						$(getCurrDataRow).find('td').css({"background-color":"#ccc"});					
					}
					else{
						var nonReceivedTr = orderDetailTable.fnAddData([counter,item, quantity,cost,totalCost,"<input type='checkbox' id='removeRowCheck' onclick='changePrice($(this));'>",itemId]);//send one column null so user can't remove previous data
						var getCurrDataRow = orderDetailTable.fnSettings().aoData[ nonReceivedTr ].nTr;	
						$(getCurrDataRow).find('td').css({"background-color":"#fff"});
						receiveOrderCounter++;
					}
	                $('#totalBill').text(parseInt($('#totalBill').text()) + parseInt(totalCost));//add total bill

	                counter++;
				}

				if (receiveOrderCounter < 1) {
					$('#messagemyModal').modal();
					$('#messageMyModalLabel').text("Alert");
					$('.modal-body').text("Order already received");
				}
			}
			else{
				$("#txtReceiveOrderId").focus();
				$("#txtReceiveOrderIdError").text("Please enter valid id");
				orderDetailTable.fnClearTable();
			}							
		}
	});
}
Row Delete functionality
function changePrice(currInst) {
	if ($(currInst).prop( "checked") == true) {
		var row = currInst.closest("tr");
		$(row).addClass('selected');				
		$('#totalBill').text(parseInt($('#totalBill').text()) - parseInt($(row).find('td:eq(4)').text()));
	}else{
		var row = currInst.closest("tr");
		$(row).removeClass('selected');
		$('#totalBill').text(parseInt($('#totalBill').text()) + parseInt($(row).find('td:eq(4)').text()));
		if ($('#viewOrderDetailsTable input:checked').length == 0) {
			$("#totalBill").text($(intialReceiveOrderBill).text());
			var intialBillValue = 0;
			var intialBill = 0			
			$.each(orderDetailTable.fnGetData(),function(index,value){
				intialBillValue = value[4];
				intialBill = intialBill + intialBillValue;
			});
			$('#totalBill').text(intialBill);
		}
	}
}*/
function clearReceiveOrderDetails(){
	$('#totalBill').text("0");
	orderDetailTable.fnClearTable();
	$("#txtReceiveOrderId ,#selLedger, #selAccountPayable").val("");
	$("#txtReceiveOrderIdError,#selLedgerError,#selAccountPayableError").text("");
	$("#txtReceiveOrderId").focus();
	$("#selLedger, #selAccountPayable").removeClass('errorStyle');
}

/**Row Delete functionality**/
function rowDelete(currInst) {
    var row = currInst.closest("tr").get(0);
    var retainitemId = $(orderDetailTable.fnGetData(row))[10];
    var retainItemName = $(orderDetailTable.fnGetData(row))[0];
	$("#selItemName").append("<option value='" + retainitemId + "'>" + retainItemName + "</option>"); 
    $('#totalBill').text(parseInt($('#totalBill').text()) - parseInt($(row).find('td:eq(7)').text()));
    orderDetailTable.fnDeleteRow(row);
    if(orderDetailTable.fnGetData().length == 0){
    	$("#txtInvoiceNo").attr('readonly',false);
    	$("#txtInvoiceNo").val('');
    }
}

function bindItem(orderId){
	var postData = {
        "operation":"searchItem",
        "orderId" : orderId
    }
    dataCall("controllers/admin/receive_order.php", postData, function (result){
    	if (result.trim().length > 2) {
    		var parseData = JSON.parse(result);
    		var option = "<option value = ''>--Select--</option>";
    		$.each(parseData,function(i,v){
    			option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
    		});
    		$("#selItemName").html(option);
    	}
    });
}
function itemQty(itemId,orderId){
	var postData ={
		operation : "itemQuantity",
		itemId :  itemId,
		orderId : orderId
	}
	dataCall("controllers/admin/receive_order.php", postData, function (result){
		if (result.trim().length > 2) {
			var parseData = JSON.parse(result);
			orgItemQty = parseData[0].ordered_quantity;
			$("#txtQuantity").val(parseData[0].ordered_quantity);
		}
	});
}

function curDate() {
	var startDate = new Date();
	$("#txtMfgDate").datepicker('setEndDate',startDate);
	$('#txtExpDate').datepicker('setStartDate', startDate);

}