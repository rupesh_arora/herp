$(document).ready(function() {
var otable; //defile variable for otable 
	
loader();
	
// show the add diseases
    $("#form_diseases").hide();
	$("#diagnosisList").css({
            "background": "#fff"
        });
		$("#tabDiagnosis").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
    $("#tabAddDiagnosis").click(function() {
        $("#form_diseases").show();
        $(".blackborder").hide();
        $("#tabAddDiagnosis").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tabDiagnosis").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#diagnosisList").css({
            "background-color": "#fff"
        });
		$("#txtCategoryError").text("");
        $("#txtCategory").removeClass("errorStyle");
		$("#txtCodeError").text("");
        $("#txtCode").removeClass("errorStyle");
		$("#txtNameError").text("");
        $("#txtName").removeClass("errorStyle");
		$("#txtCode").focus(); 
    });
// show the add diseases list
    $("#tabDiagnosis").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
        $("#form_diseases").hide();
        $(".blackborder").show();
        $("#tabAddDiagnosis").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#tabDiagnosis").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#diagnosisList").css({
            "background": "#fff"
        });
		$("#diagnosisDataTable").dataTable().fnReloadAjax(); 
	});
	
// Ajax call for show data on Diseases Category
	$.ajax({     
		type: "POST",
		cache: false,
		url: "controllers/admin/diagnosis.php",
		data: {
		"operation":"showcategory"
		},
		success: function(data) { 
		if(data != null && data != ""){
			var parseData= jQuery.parseJSON(data); // parse the value in Array string  jquery

			var option ="<option value=''>--Select--</option>";
			for (var i=0;i<parseData.length;i++)
			{
				option+="<option value='"+parseData[i].id+"'>"+parseData[i].category+"</option>";
			}
			$('#txtCategory').html(option);          
			}
		},
		error:function(){
		}
	});
	

    $("#btnDiagnosis").click(function() {
		// check the validation
		var flag = "false";
		if ($("#txtCategory").val() == '') {
            $("#txtCategoryError").text("Please select category");
			$("#txtCategory").focus(); 
            $("#txtCategory").addClass("errorStyle");
            flag = "true";
        }
		if ($("#txtName").val().trim() == '') {
            $("#txtNameError").text("Please enter diagnosis name");
			$("#txtName").focus(); 
            $("#txtName").addClass("errorStyle");
            flag = "true";
        }
        if ($("#txtCode").val().trim() == '') {
            $("#txtCodeError").text("Please enter code");
			$("#txtCode").focus(); 
            $("#txtCode").addClass("errorStyle");
            flag = "true";
        }
		
		
		if (flag == "true")
		{
			return false;
		}
		else
		{
			var code = $("#txtCode").val().trim();
			var name = $("#txtName").val().trim();
			var category_id = $("#txtCategory").val();
			$.ajax({	// ajax call for diagnosis duplicacy validation
				type: "POST",
				cache: false,
				url: "controllers/admin/diagnosis.php",
				datatype:"json",  
				data:{
					Code:code,
					Name:name,
					category_Id:category_id,
					Id:"",
					operation:"checkdiseases"
				},
				success:function(data) {
					if(data == "1")
					{
						  $("#txtCodeError").text("Code is already exists");
						  $("#txtCode").addClass("errorStyle");
						  $("#txtCode").focus();
					}
					else{
						//ajax call for insert data into data base
						var postData = {
							"operation":"save",
							Code:code,
							Name:name,
							category_Id:category_id,
						}
						$.ajax(
							{					
							type: "POST",
							cache: false,
							url: "controllers/admin/diagnosis.php",
							datatype:"json",
							data: postData,

							success: function(data) {
								if(data != "0" && data != ""){
									$('.modal-body').text("");
									$('#messageMyModalLabel').text("Success");
									$('.modal-body').text("Diagnosis saved successfully!!!");
									$('#messagemyModal').modal(); 
									$('#inactive-checkbox-tick').prop('checked', false).change();
									clear();
									//after sucessful data sent to database redirect to datatable list
									$("#form_diseases").hide();
        							$(".blackborder").show();
				       				$("#tabAddDiagnosis").css({
							            "background": "#fff",
							            "color": "#000"
							        });
							        $("#tabDiagnosis").css({
							            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
							            "color": "#fff"
							        });
				        			$("#diagnosisDataTable").dataTable().fnReloadAjax();
								}
								
							},
							error:function() {
								alert('error');
							}
						});//  end ajax call
					}
				},
				error:function(){
					alert ("data not found");
				}
			});
		}
   }); //end button click function
   
// key up validation
    $("#txtCode").keyup(function() {
        if ($("#txtCode").val() != '') {
            $("#txtCodeError").text("");
            $("#txtCode").removeClass("errorStyle");
        } 
    }); 
	 $("#txtName").keyup(function() {
        if ($("#txtName").val() != '') {
            $("#txtNameError").text("");
            $("#txtName").removeClass("errorStyle");
        } 
    }); 
	$("select").on('change', function() {

        if ($("#txtCategory").val() != "") {
            $("#txtCategoryError").text("");
            $("#txtCategory").removeClass("errorStyle");
            
        }
    });
	
   if($('.inactive-checkbox').not(':checked')){
    //Datatable code
		 otable = $('#diagnosisDataTable').dataTable( {
			"bFilter": true,
			"processing": true,
			"sPaginationType":"full_numbers",
			"fnDrawCallback": function ( oSettings ) {
				
				$('.update').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData = $('#diagnosisDataTable').dataTable().fnGetData(data);
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						var code = mData["code"];
						var name = mData["name"];
						var category = mData["diagnosis_cat_id"];
						editClick(id,code,name,category);       
					}
				});
				$('.delete').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData =  $('#diagnosisDataTable').dataTable().fnGetData(data);
					
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						deleteClick(id);
					}
				});
			},
			"sAjaxSource":"controllers/admin/diagnosis.php",
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "operation", "value": "show" });
			},
			"aoColumns": [
				/* {  "mData": "id" }, */
				{  "mData": "code" },
				{  "mData": "name" },
				{  "mData": "category" },	
				{  "mData": function (o) { 
					var data = o;
					return '<i class="ui-tooltip fa fa-pencil update" style="font-size: 22px;cursor:pointer;" data-original-title="Edit"></i> <i class="ui-tooltip fa fa-trash-o delete" style="font-size: 22px; color:#a94442; cursor:pointer;" data-original-title="Delete"></i>'; }
				},	
				],
			aoColumnDefs: [
				{ 'bSortable': false, 'aTargets': [ 3 ] }
			]

		});
   }
   $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	otable.fnClearTable();
	    	otable.fnDestroy();
	    	otable="";
	    	otable = $('#diagnosisDataTable').dataTable( {
				"bFilter": true,
				"processing": true,
				 "deferLoading": 57,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					// Need to redo the counters if filtered or sorted /
					/* if ( oSettings.bSorted || oSettings.bFiltered )
					{
						for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
						{
							$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
						}
					} */
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  $('#diagnosisDataTable').dataTable().fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var category = mData["diagnosis_cat_id"];
							restoreClick(id,category);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/diagnosis.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "checked" });
				},
				"aoColumns": [
					/* {  "mData": "id" }, */
					{  "mData": "code" },
					{  "mData": "name" },
					{  "mData": "category" },	
					{  
						"mData": function (o) { 
						var data = o;
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},
				],
				aoColumnDefs: [
				{ 'bSortable': false, 'aTargets': [ 3 ] }
				]
			});
		}
		else{
			otable.fnClearTable();
	    	otable.fnDestroy();
	    	otable="";
			otable = $('#diagnosisDataTable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					// Need to redo the counters if filtered or sorted /
					/* if ( !oSettings.bSorted || !oSettings.bFiltered )
					{
						for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
						{
							$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
						}
					} */
					$('.update').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData = $('#diagnosisDataTable').dataTable().fnGetData(data);
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							var code = mData["code"];
							var name = mData["name"];
							var category = mData["diagnosis_cat_id"];
							editClick(id,code,name,category);
		   
						}
					});
					$('.delete').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  $('#diagnosisDataTable').dataTable().fnGetData(data);
						
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							deleteClick(id);
						}
					});
				},
				
				"sAjaxSource":"controllers/admin/diagnosis.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "show" });
				},
				"aoColumns": [
/* 					{  "mData": "id" }, */
					{  "mData": "code" },
					{  "mData": "name" },
					{  "mData": "category" },	
					{  
						"mData": function (o) { 
						var data = o;
						return '<i class="ui-tooltip fa fa-pencil update" style="font-size: 22px;cursor:pointer;" data-original-title="Edit"></i> <i class="ui-tooltip fa fa-trash-o delete" style="font-size: 22px; color:#a94442; cursor:pointer;" data-original-title="Delete"></i>'; }
					},	
					],
				aoColumnDefs: [
				{ 'bSortable': false, 'aTargets': [ 3 ] }
				]
			});
		}
    });
	
	$("#btnReset").click(function(){
        $("#txtCategory").removeClass("errorStyle");
		$("#txtCodeError").text("");
        $("#txtCode").removeClass("errorStyle");
		$("#txtNameError").text("");
        $("#txtName").removeClass("errorStyle");
		$("#txtCode").focus(); 
		clear();
	});
	
});
// edit data dunction for update 
function editClick(id,code,name,category){	
	$('.modal-body').text("");
	$('#myModalLabel').text("Update Diagnosis");
	$('.modal-body').html($("#diagnosispopUPForm").html()).show();
    $('#myModal').modal('show');
	$('#myModal').on('shown.bs.modal', function() {
        $("#myModal #txtCode").focus();
    });
	$("#myModal #btnUpdate").removeAttr("style");
	$("#myModal #txtCode").removeClass("errorStyle");
	$("#myModal #txtCodeError").text("");
	$("#myModal #txtName").removeClass("errorStyle");
	$("#myModal #txtNameError").text("");
	$("#myModal #txtCategory").removeClass("errorStyle");
	$("#myModal #txtCategoryError").text("");
	
	$('#myModal #txtCode').val(code);
	$('#myModal #txtName').val(name);
	$('#myModal #txtCategory').val(category);
	$('#selectedRow').val(id); 
	    
  // key up validation
    $("#myModal #txtCode").keyup(function() {
        if ($("#myModal #txtCode").val() != '') {
            $("#myModal #txtCodeError").text("");
            $("#myModal #txtCode").removeClass("errorStyle");
        } 
    }); 
	 $("#myModal #txtName").keyup(function() {
        if ($("#myModal #txtName").val() != '') {
            $("#myModal #txtNameError").text("");
            $("#myModal #txtName").removeClass("errorStyle");
        } 
    }); 
	$("#myModal select").on('change', function() {

        if ($("#myModal #txtCategory").val() != "") {
            $("#myModal #txtCategoryError").text("");
            $("#myModal #txtCategory").removeClass("errorStyle");
            
        }
    });
	//validation
	$("#myModal #btnUpdate").click(function(){ // click update button
		var flag = "false";
		if ($("#myModal #txtCategory").val() == '') {
            $("#myModal #txtCategoryError").text("Please select category");
			$("#myModal #txtCategory").focus();
            $("#myModal #txtCategory").addClass("errorStyle");
            flag = "true";
        }
		if ($("#myModal #txtName").val().trim() == '') {
            $("#myModal #txtNameError").text("Please enter diagnosis name");
			$("#myModal #txtName").focus();
            $("#myModal #txtName").addClass("errorStyle");
            flag = "true";
        }
        if ($("#myModal #txtCode").val().trim() == '') {
            $("#myModal #txtCodeError").text("Please enter code");
			$("#myModal #txtCode").focus();
            $("#myModal #txtCode").addClass("errorStyle");
            flag = "true";
        }		
		
		if (flag == "true")
		{
			return false;
		}
		else
		{
			var id = $('#selectedRow').val();	
			var code = $("#myModal #txtCode").val().trim();
			var name = $("#myModal #txtName").val().trim();
			var category_id = $("#myModal #txtCategory").val();
			$.ajax({									// ajax call for diseases duplicacy validation
				type: "POST",
				cache: false,
				url: "controllers/admin/diagnosis.php",
				datatype:"json",  
				data:{
					Code:code,
					Name:name,
					category_Id:category_id,
					Id:id,	
					operation:"checkdiseases"
				},
				success:function(data) {
					if(data == "1")
					{
						  $("#myModal #txtCodeError").text("Code is already exists");
						  $("#myModal #txtCode").addClass("errorStyle");
						  $("#myModal #txtCode").focus();
						  
					}
					else{
						var postData = {
							"operation":"update",
							Code:code,
							Name:name,
							category_Id:category_id,
							Id:id
						}
						$.ajax( //ajax call for update data
						{					
							type: "POST",
							cache: false,
							url: "controllers/admin/diagnosis.php",
							datatype:"json",
							data: postData,

							success: function(data) {
								if(data != "0" && data != ""){
								$('#myModal').modal('toggle');
									$('.modal-body').text("");
									$('#messageMyModalLabel').text("Success");
									$('.modal-body').text("Diagnosis updated successfully!!!");
									$('#messagemyModal').modal();
									$("#diagnosisDataTable").dataTable().fnReloadAjax();
									clear();
								
								}
							},
							error:function() {
								alert('error');
							}
						});// end of ajax
					}
				},
				error:function(){
					alert ("data not found");
				}
			});
		}	
	});
}// end update button
function deleteClick(id){ // delete click function
	$('.modal-body').text("");
	$('#confirmMyModalLabel').text("Delete Diagnosis");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	$('#selectedRow').val(id); 
	
	var type="delete";
	$('#confirm').attr('onclick','diagnosisDelete("'+type+'");');
}// end click fucntion
function restoreClick(id,category){
	$('.modal-body').text("");
	$('#selectedRow').val(id);
	$('#confirmMyModalLabel').text("Restore Diagnosis");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal(); 
	var type="restore";
	$('#confirm').attr('onclick','diagnosisDelete("'+type+'",'+category+');');
}
// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		 /* window.location.href = "http://localhost/herp/"; */
		 $('.close').click();
    }
});


function clear(){
	$('#txtCode').val("");
	$('#txtCodeError').text("");
	$('#txtCode').removeClass("errorStyle");
	$('#txtName').val("");
	$('#txtNameError').text("");
	$('#txtName').removeClass("errorStyle");
	$('#txtCategory').val("");
	$('#txtCategoryError').text("");
	$('#txtCategory').removeClass("errorStyle");
}
function diagnosisDelete(type,category){
		if(type=="delete"){
			var id = $('#selectedRow').val();
			var postData = {
				"operation":"delete",
				"id":id
			}
			$.ajax( // ajax call for delete
				{					
				type: "POST",
				cache: false,
				url: "controllers/admin/diagnosis.php",
				datatype:"json",
				data: postData,

				success: function(data) {
					if(data != "0" && data != ""){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Diagnosis deleted successfully!!!");
						$('#messagemyModal').modal();
						$("#diagnosisDataTable").dataTable().fnReloadAjax(); 
					}
					else{
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Sorry");
						$('.modal-body').text("This diseases is used , so you can not delete!!!");
						$('#messagemyModal').modal();
					}
				},
				error:function() {
					alert('error');
				}
			}); // end ajax 
		}
		else{
			var id = $('#selectedRow').val();
			$.ajax({
				type: "POST",
				cache: "false",
				url: "controllers/admin/diagnosis.php",
				data :{            
					"operation" : "restore",
					"id" : id,
					"category":category
				},
				success: function(data) {	
					if(data != "0" && data != ""){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Diagnosis restored successfully!!!");
						$('#messagemyModal').modal();
						$("#diagnosisDataTable").dataTable().fnReloadAjax(); 
					
					}	
					if(data=="0"){
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Sorry");
						$('.modal-body').text("It can not be restore!!!");
						$("#diagnosisDataTable").dataTable().fnReloadAjax(); 
						$('#messagemyModal').modal();
						
					}
				},
				error:function()
				{
					alert('error');
					  
				}
			});
		}
	}


//# sourceURL=diseasescategory.js