/*
 * File Name    :   hospital_profile.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   29th dec, 2015
 * Description  :   This page is use for load data of hospital profile
*/
$(document).ready(function() {
	loader();
    debugger;
    var hID;
    countryID = -1; // defile countryID variable
    country(countryID); // function call for show data on country
    show(); // function use for show profile on load time
	
    $("#txtHospitalName").focus();
    /* use reset functionality */
    $("#reset").click(function() {
        clear();  // call clear function
        $('#txtHospitalName').focus();
    });

    // image upload js.
    $("#imgHospitalLogo").click(function() {
        $("#upload").click();
        $("#upload").on("change", function() {
            $("#errorimage").hide();
            $("#errorimage").hide();
            $("#errorimage").text("");
            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

            if (/^image/.test(files[0].type)) { // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file

                reader.onloadend = function() { // set image data as background of div
                    $("#imgHospital").hide();
                    $("#imagetext").hide();
                    $("#imgHospitalLogo").css("background-image", "url(" + this.result + ")");
                    $("#imgHospitalLogo").text("");
                }
            }
        });
    });

    /*Disable space functionality*/
    $("#txtPhone").on("keydown", function(e) {
        return e.which !== 32;
    });

    /*validation on chane and keyup functionality */
    $("#selCity").change(function() {
        if ($("#selCity").val() != "") {
            $("#selCityError").text("");
            $("#selCity").removeClass("errorStyle");
        }
    });
    $("#selState").change(function() {
        if ($("#selState").val() != "") {
            $("#selStateError").text("");
            $("#selState").removeClass("errorStyle");
        }
    });
    $("#selCountry").change(function() {
        if ($("#selCountry").val() != "-1") {
            $("#selCountryError").text("");
            $("#selCountry").removeClass("errorStyle");
        }
    });
    $("#txtHospitalName").keyup(function() {
        if ($("#txtHospitalName").val().trim() != "") {
            $("#txtHospitalNameError").text("");
            $("#txtHospitalName").removeClass("errorStyle");
        }
    });
    $("#txtAddress").keyup(function() {
        if ($("#txtAddress").val().trim() != "") {
            $("#txtAddressError").text("");
            $("#txtAddress").removeClass("errorStyle");
        }
    });
    $("#txtZipcode").keyup(function() {
        if ($("#txtZipcode").val().trim() != "") {
            $("#txtZipcodeError").text("");
            $("#txtZipcode").removeClass("errorStyle");
        }
    });
    $("#txtPhone").keyup(function() {
        if ($("#txtPhone").val().trim() != "") {
            $("#txtPhoneError").text("");
            $("#txtPhone").removeClass("errorStyle");
        }
        $("#txtEmail").keyup(function() {
            if ($("#txtEmail").val().trim() != "") {
                $("#txtEmailError").text("");
                $("#txtEmail").removeClass("errorStyle");
            }
        });

        /*Disable space functionality*/
        $("#txtmob").on("keydown", function(e) {
            return e.which !== 32;
        });
    }); //end key up validation


    // Ajax call for show data on state select country 
    $('#selCountry').change(function() {
        var countryID = $("#selCountry").val();
        bindState(countryID, '');
        bindCountryCode(countryID);
    });

    // Ajax call for show data on country select state 
    $('#selState').change(function() {
        var stateID = $("#selState").val();
        bindCity(stateID, '');
    });

    /* on save click button for save data */
    $("#btnSave").click(function() {

        //validation
        var flag = false;
        if ($("#txtEmail").val($("#txtEmail").val().trim()) == "" || !ValidateEmail($("#txtEmail").val())) {
            $("#txtEmailError").text("Please enter valid email");
            $("#txtEmail").addClass("errorStyle");
            $("#txtEmail").focus();
            flag = true;
        }
        if (!validnumber($("#txtPhone").val())) {
            $("#txtPhoneError").text("Please enter valid phone number");
            $("#txtPhone").addClass("errorStyle");
            $("#txtPhone").focus();
            flag = true;
        }
        if ($("#txtPhone").val().trim() == "") {
            $("#txtPhoneError").text("Please enter phone number");
            $("#txtPhone").addClass("errorStyle");
            $("#txtPhone").focus();
            flag = true;
        }
        if ($("#selCity").val() == "") {
            $("#selCityError").text("Please select city");
            $("#selCity").addClass("errorStyle");
            $("#selCity").focus();
            flag = true;
        }
        if ($("#selState").val() == "") {
            $("#selStateError").text("Please select state");
            $("#selState").addClass("errorStyle");
            $("#selState").focus();
            flag = true;
        }
        if ($("#selCountry").val() == "-1") {
            $("#selCountryError").text("Please select country");
            $("#selCountry").addClass("errorStyle");
            $("#selCountry").focus();
            flag = true;
        }
        if ($("#txtAddress").val().trim() == "") {
            $("#txtAddressError").text("Please enter address");
            $("#txtAddress").addClass("errorStyle");
            $("#txtAddress").focus();
            flag = true;
        }
        if ($("#txtHospitalName").val().trim() == "") {
            $("#txtHospitalNameError").text("Please enter hospital name");
            $("#txtHospitalName").addClass("errorStyle");
            $("#txtHospitalName").focus();
            flag = true;
        }
        if ($("#txtZipcode").val().trim() == "") {
            $("#txtZipcodeError").text("Please enter zipcode");
            $("#txtZipcode").addClass("errorStyle");
            $("#txtZipcode").focus();
            flag = true;
        }
        if (flag == true) {
            return false;
        }

        $("#upload").change(function() {
            if ($("#imgHospitalLogo").css("background-image") != 'url("http://localhost/herp/images/")') {
                $("#imgHospitalLogo").text("");
            }
        });

        //If button text equals to save
        if ($("#btnSave").val() == "Save") {
            var hospitalName = $("#txtHospitalName").val().trim();
            var address = $("#txtAddress").val().trim();
            var phone = $("#txtPhone").val().trim();
            var email = $("#txtEmail").val().trim();
            var country = $("#selCountry").val().trim();
            var state = $("#selState").val().trim();
            var city = $("#selCity").val().trim();
            var zipCode = $("#txtZipcode").val().trim();

            var file_data = $('#upload').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            var imageName;
            $('#confirmMyModalLabel').text("Save Hospital Information");
            $('.modal-body').text("Are you sure that you want to save this?");
            $('#confirmmyModal').modal();
            $('#confirm').unbind();
            $('#confirm').on('click',function(){
                $.ajax({
                    url: 'controllers/admin/hospital_profile.php', // point to server-side PHP script 
                    dataType: 'text', // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(data) {
                        if (data == "invalid file") {
                            $("#errorimage").show();
                            $("#errorimage").text("Please upload logo.");
                        } else {
                            imageName = data;
                            // for ajax call for insert
                            var postData = {
                                "operation": "save",
                                "hospitalName": hospitalName,
                                "address": address,
                                "phone": phone,
                                "email": email,
                                "country": country,
                                "state": state,
                                "city": city,
                                imageName: imageName,
                                "zipCode" : zipCode
                            }
                            $.ajax({
                                type: "POST",
                                cache: false,
                                url: "controllers/admin/hospital_profile.php",
                                datatype: "json",
                                data: postData,

                                success: function(data) {
                                    if (data != "") {
                                        $('#messageMyModalLabel').text("Success");
                                        $('.modal-body').text("Hospital information updated!!!");
                                        $('#messagemyModal').modal();
                                        show();
    									if(imageName != "")
    									{
    										$('.sidebar-user-avatar a img').attr("src","images/"+imageName+"");
    									}
                                        /*On sucessful completion of data change save button text to update and hide reset button*/
                                        $("#btnSave").val("Update");
                                    }
                                },
                                error: function() {
    								
    								$('.modal-body').text("");
    								$('#messageMyModalLabel').text("Error");
    								$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
    								$('#messagemyModal').modal();
    							}
                            });
                        }

                    }
                });
            });
        } //end if to check


        //If button text equals to Update
        if ($("#btnSave").val() == "Update") {
            var hospitalName = $("#txtHospitalName").val().trim();
            var address = $("#txtAddress").val().trim();
            var phone = $("#txtPhone").val().trim();
            var email = $("#txtEmail").val().trim();
            var country = $("#selCountry").val().trim();
            var state = $("#selState").val().trim();
            var city = $("#selCity").val().trim();
            var zipCode = $("#txtZipcode").val().trim();

            var file_data = $('#upload').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            var imageName;
            $('#confirmUpdateModalLabel').text();
			$('#updateBody').text("Are you sure that you want to update this?");
			$('#confirmUpdateModal').modal();
			$("#btnConfirm").unbind();
			$("#btnConfirm").click(function(){
                $.ajax({ // ajax call for image upload data
                    url: 'controllers/admin/hospital_profile.php', // point to server-side PHP script 
                    dataType: 'text', // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(data) {
                        if (data == "invalid file") {
                             
                        } else {
                            imageName = data;
                            // for ajax call for insert
                            var postData = {
                                "operation": "update",
                                "hospitalName": hospitalName,
                                "address": address,
                                "phone": phone,
                                "email": email,
                                "country": country,
                                "state": state,
                                "city": city,
                                imagename: imageName,
                                "zipCode" : zipCode
                                /* imagesPath: imagefile */
                            }

                            $.ajax({ //Ajax call to save data
                                type: "POST",
                                cache: false,
                                url: "controllers/admin/hospital_profile.php",
                                datatype: "json",
                                data: postData,

                                success: function(data) {
                                    if (data != "") {
										$('#myModal').modal('hide');
										$('.close-confirm').click();
										$('.modal-body').text("");
                                        $('#messageMyModalLabel').text("Updated");
                                        $('.modal-body').text("Hospital information updated!!!");
                                        $('#messagemyModal').modal();
                                        $('#txtHospitalName').focus();
                                        show();
    									if(imageName !="")
    									{
    										$('.sidebar-user-avatar a img').attr("src","images/"+imageName+"");
    									}
                                    }
                                },
                                error: function() {
    								$('.modal-body').text("");
    								$('#messageMyModalLabel').text("Error");
    								$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
    								$('#messagemyModal').modal();
    							}
                            });
                        }

                    }
                });
            });
        }

    }); //end click function
}); //End document ready


// function for country
function country(countryID) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/hospital_profile.php",
        data: {
            "operation": "showcountry"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $('#selCountry').html(option);
            }
            if (countryID != "") {
                $("#selCountry").val(countryID);
                bindCountryCode(countryID);
            }

        },
        error: function() {}
    });
}

// function for state 
function bindState(countryID, stateID) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/hospital_profile.php",
        data: {
            "operation": "showstate",
            country_ID: countryID
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $('#selState').html(option);
                if (stateID != "") {
                    $("#selState").val(stateID);
                }
            }
        },
        error: function() {}
    });
}

// function for city
function bindCity(stateID, cityID) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/hospital_profile.php",
        data: {
            "operation": "showcity",
            state_ID: stateID
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].location_id + "'>" + parseData[i].name + "</option>";
                }
                $('#selCity').html(option);
                if (cityID != "") {
                    $("#selCity").val(cityID);
                }
            }
        },
        error: function() {}
    });
}
/* function use for show data on load time */
function show() {
        var postData = {
            "operation": "show",
        }
        $.ajax({ // call ajax for update on load time
            type: "POST",
            cache: false,
            url: "controllers/admin/hospital_profile.php",
            datatype: "json",
            data: postData,

            success: function(data) {

                if (data != "") {
                    var parseData = jQuery.parseJSON(data);
                    for (var i = 0; i < parseData.length; i++) {
                        $("#imgHospital").hide();
                        $("#imagetext").hide();


                        var imagePath = "./images/" + parseData[i].images_path;
                        $("#imgHospitalLogo").css({
                            "background-image": "url(" + imagePath + ")"
                        });


                        $("#txtHospitalName").val(parseData[i].hospital_name);
                        $("#txtAddress").val(parseData[i].address);
                        /* $("#selCountry").val(parseData[i].country_id); */
                        country(parseData[i].country_id);
                        $("#txtPhone").val(parseData[i].phone_number);
                        $("#txtEmail").val(parseData[i].email);
                        $("#txtZipcode").val(parseData[i].zipcode);

                        bindState(parseData[i].country_id, parseData[i].state_id);
                        bindCity(parseData[i].state_id, parseData[i].city_id);

                        /*  imagefile = imagePath; */

                        hID = parseData[i].id;
                        /*update(hID, imagefile);*/
                        /*To check whether image is saved in database*/
                        var logo = parseData[i].images_path;
                        if (logo == "") {
                            $("#imgHospitalLogo").text("Click to Upload logo").css({
                                "padding": "2.5% 0 0 0",
                                "font-size": "15px"
                            });
                        }
                        $("#btnSave").val("Update");
                    }

                } else {
                    $("#btnSave").val("Save");
                    $("#reset").css({
                        "display": " -webkit-inline-box;"
                    });
                }
            },
            error: function() {
                alert('error');
            }
        });
    }
    //validate email
function ValidateEmail(email) {
    var expr = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;;
    return expr.test(email);
};

//validate phone number
function validnumber(number) {
    intRegex = /^\d{10,15}$/;
    return intRegex.test(number);
};

// function for clear
function clear() {
    $('#selState').val("-1");
    $('#selStateError').text("");
    $('#selCountry').val("");
    $('#txtZipcode').val("");
    $('#selCountryError').text("");
    $('#txtZipcodeError').text("");
    $('#selCity').val("-1");
    $('#selCityError').text("");
    $('#txtEmail').val("");
    $('#txtEmailError').text("");
    $('#txtPhone').val("");
    $('#txtPhoneError').text("");
    $('#txtHospitalName').val("");
    $('#txtHospitalNameError').text("");
    $('#txtAddress').val("");
    $('#txtAddressError').text("");
    $("#imgHospital").show();
    $("#imagetext").show();
    $("#imgHospitalLogo").css('background-image', 'none');
    $("#upload").val();
    $("#selState").removeClass("errorStyle");
    $("#selCountry").removeClass("errorStyle");
    $("#selCity").removeClass("errorStyle");
    $("#txtEmail").removeClass("errorStyle");
    $("#txtEmail").removeClass("errorStyle");
    $("#txtPhone").removeClass("errorStyle");
    $("#txtHospitalName").removeClass("errorStyle");
    $("#txtAddress").removeClass("errorStyle");
    $("#txtZipcode").removeClass("errorStyle");
    $("#errorimage").hide();
    $("#errorimage").text("");
}
function bindCountryCode(countryID) {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/hospital_profile.php",
        data: {
            "operation": "showCountryCode",
            countryID: countryID
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                
                var countryCode = '+' + parseData.country_code;
                $("#txtPhoneCode").val(countryCode);
                
               
            }
        },
        error: function() {}
    });
}