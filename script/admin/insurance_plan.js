/*
 * File Name    :   insurance_plan.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar Gupta
 * Created Date :   30th dec, 2015
 * Description  :   This page use for load,save,update,delete,resotre operation
 */
var insurancePlanTbl; //defile variable for insurancePlanTbl 
$(document).ready(function() {
	loader();
    $('#btnReset').click(function() {
		clear();
    });
	
	// bind company
	bindCompany();
	
    $("#formInsurancePlan").hide();
    $("#divInsurancePlan").addClass('list');    
    $("#tabInsurancePlanList").addClass('tab-list-add');
    
    $("#tabInsurancePlan").click(function() { // show the add allergies categories tab
        showAddTab();
        clear();
    });

    $("#tabInsurancePlanList").click(function() { // show allergies list tab
        showTableList();
    });

    // save details with validation
    $("#btnAddInsurancePlan").click(function() {

        var flag = "false";
        if ($("#txtPlanName").val().trim() == '') {
            $("#txtPlanNameError").text("Please enter scheme name");
            $("#txtPlanName").focus();
            $("#txtPlanName").addClass("errorStyle");
            flag = "true";
        }
		if ($("#selCompanyName").val() == '-1') {
            $("#selCompanyNameError").text("Please select company name");
            $("#selCompanyName").focus();
            $("#selCompanyName").addClass("errorStyle");
            flag = "true";
        } 	
		if(flag == "true") {
			return false;
		}		
		
            var companyName = $("#selCompanyName").val().trim();
            var planName = $("#txtPlanName").val().trim();
            var description = $("#txtDesc").val();
            description = description.replace(/'/g, "&#39");
			//ajax call for insert data into data base
			var postData = {
				"operation": "save",
				"companyName": companyName,
				"planName": planName,
				"description": description
			}
			$.ajax({
				type: "POST",
				cache: false,
				url: "controllers/admin/insurance_plan.php",
				datatype: "json",
				data: postData,

				success: function(data) {
					if (data == "1") {
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Insurance scheme saved successfully!!!");
						$('#messagemyModal').modal();
						showTableList();
					}
					if (data == "0") {
						$("#txtPlanName").addClass('errorStyle');
						$("#txtPlanName").focus();
						$('#txtPlanNameError').text('Scheme name already exists.');
					}

				},
				error: function() {
					alert('error');
				}
			}); //  end ajax call
		}); //end button click function

    //remove validation style
    $("#txtPlanName").keyup(function() {
        if ($("#txtPlanName").val() != '') {
            $("#txtPlanNameError").text("");
            $("#txtPlanName").removeClass("errorStyle");
        }
    });
	$("#selCompanyName").change(function(){
		if($("#selCompanyName").val()!="-1"){
			$("#selCompanyNameError").text("");
			$("#selCompanyName").removeClass("errorStyle"); 
		}
	});	
	
    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code
        insurancePlanTbl = $('#insurancePlanTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "fnDrawCallback": function(oSettings) {
                // perform update event
				$('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = insurancePlanTbl.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
						var companyName = mData["company_name_id"];
                        var planName = mData["plan_name"];
                        var description = mData["description"];
                        editClick(id, companyName,planName,description);

                    }
                });
                //perform delete event
				$('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = insurancePlanTbl.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/insurance_plan.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "CompanyName"
                }, {
                    "mData": "plan_name"
                }, {
                    "mData": "description"
                },{
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [3]
            }, {
                'bSortable': false,
                'aTargets': [2]
            }]

        });
    }
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            insurancePlanTbl.fnClearTable();
            insurancePlanTbl.fnDestroy();
            insurancePlanTbl = "";
            insurancePlanTbl = $('#insurancePlanTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
					$('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = insurancePlanTbl.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            restoreClick(id);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/insurance_plan.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    {
                    "mData": "CompanyName"
                }, {
                    "mData": "plan_name"
                }, {
                    "mData": "description"
                }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [3]
                }, {
                    'bSortable': false,
                    'aTargets': [2]
                }]
            });
        } else { // show active data on unchecked	
            insurancePlanTbl.fnClearTable();
            insurancePlanTbl.fnDestroy();
            insurancePlanTbl = "";
            insurancePlanTbl = $('#insurancePlanTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform update event
					$('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = insurancePlanTbl.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
							var companyName = mData["company_name_id"];
							var planName = mData["plan_name"];
							var description = mData["description"];
							editClick(id, companyName,planName,description);

                        }
                    });
                    // perform delete event
					$('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = insurancePlanTbl.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/insurance_plan.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
						"mData": "CompanyName"
					}, {
						"mData": "plan_name"
					}, {
						"mData": "description"
					}, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [3]
                }, {
                    'bSortable': false,
                    'aTargets': [2]
                }]
            });
        }
    });

});
// edit data dunction for update 
function editClick(id,companyName,planName,description) {
    showAddTab();
    $('#tabInsurancePlan').html("+Update Insurance Scheme");
    $("#btnReset").hide();
    $("#btnAddInsurancePlan").hide();
    
    $("#btnUpdate").removeAttr("style");
	
    $("#selCompanyName").removeClass("errorStyle");
    $("#selCompanyNameError").text("");
	$("#txtPlanName").removeClass("errorStyle");
    $("#txtPlanNameError").text("");
	
    $('#selCompanyName').val(companyName);
    $('#txtPlanName').val(planName);
    $('#txtDesc').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
    //validation
    //remove validation style
   	
    $("#btnUpdate").click(function() { // click update button
		var flag = "false";
        if ($("#txtPlanName").val().trim() == '') {
            $("#txtPlanNameError").text("Please enter scheme name");
            $("#txtPlanName").focus();
            $("#txtPlanName").addClass("errorStyle");
            flag = "true";
        }
		if ($("#selCompanyName").val() == '-1') {
            $("#selCompanyNameError").text("Please select company name");
            $("#selCompanyName").focus();
            $("#selCompanyName").addClass("errorStyle");
            flag = "true";
        } 	
		if(flag == "true") {
			return false;
		}
		var companyName = $("#selCompanyName").val().trim();
		var planName = $("#txtPlanName").val().trim();
		var description = $("#txtDesc").val();
		description = description.replace(/'/g, "&#39");
		var id = $('#selectedRow').val();
		
		$('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to update this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
		var postData = {
			"operation": "update",
			"companyName": companyName,
			"planName": planName,
			"description": description,
			"id": id
		}
		$.ajax( //ajax call for update data
			{
				type: "POST",
				cache: false,
				url: "controllers/admin/insurance_plan.php",
				datatype: "json",
				data: postData,

				success: function(data) {
					if (data == "1") {
						$('#myModal').modal('hide');
						$('.close-confirm').click();
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Insurance scheme updated successfully!!!");
						$('#messagemyModal').modal();
						showTableList();
					}
					if (data == "0") {
						$("#txtPlanName").addClass('errorStyle');
						$("#txtPlanName").focus();
						$('#txtPlanNameError').text('Scheme name already exists.');
					}
				},
				error: function() {
					$('.close-confirm').click();
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
			}); // end of ajax
		});
    });
} // end update button

function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Insurance Scheme");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteInsurancePlan("' + type + '");');
} // end click fucntion

function restoreClick(id) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Insurance Scheme");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteInsurancePlan("' + type + '");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
// clear function
function clear() {
	$("#txtPlanNameError").text("");
	$("#txtPlanName").removeClass("errorStyle");
	$("#txtPlanName").val("");
	$("#selCompanyNameError").text("");
	$("#selCompanyName").removeClass("errorStyle");
	$("#selCompanyName").val("-1");
	$("#txtDesc").val("");
	$("#selCompanyName").focus();
}
// define that function perform for delete and restore event
function deleteInsurancePlan(type) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete		
            type: "POST",
            cache: false,
            url: "controllers/admin/insurance_plan.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Insurance scheme deleted successfully!!!");
                    $('#messagemyModal').modal();
                    insurancePlanTbl.fnReloadAjax();
                } else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This Insurance scheme is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/insurance_plan.php",
            data: {
                "operation": "restore",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Insurance scheme restored successfully!!!");
                    $('#messagemyModal').modal();
                    insurancePlanTbl.fnReloadAjax();

                }
            },
            error: function() {				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });
    }
}

function bindCompany() {
	   $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/insurance_plan.php",
        data: {
            "operation": "showCompany"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selCompanyName').html(option);
            }
        },
        error: function() {}
    });
}
function showTableList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#formInsurancePlan").hide();
    $(".blackborder").show();

    $("#tabInsurancePlan").removeClass('tab-detail-add');
    $("#tabInsurancePlan").addClass('tab-detail-remove');
    $("#tabInsurancePlanList").removeClass('tab-list-remove');    
    $("#tabInsurancePlanList").addClass('tab-list-add');
    $("#divInsurancePlan").addClass('list');    
    $("#btnReset").show();
    $("#btnAddInsurancePlan").show();
    $('#btnUpdate').hide();
    $('#tabInsurancePlan').html("+Add Insurance Scheme");
    clear();
}

function showAddTab(){
    $("#formInsurancePlan").show();
    $(".blackborder").hide();
   
    $("#tabInsurancePlan").addClass('tab-detail-add');
    $("#tabInsurancePlan").removeClass('tab-detail-remove');
    $("#tabInsurancePlanList").removeClass('tab-list-add');
    $("#tabInsurancePlanList").addClass('tab-list-remove');
    $("#divInsurancePlan").addClass('list');
    $("#selCompanyNameError").text("");
    $("#selCompanyName").removeClass("errorStyle");
    $("#selCompanyName").focus();    
    removeErrorMessage();
}