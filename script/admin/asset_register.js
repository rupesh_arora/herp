var assetRegisterTable;
var checkInCheckOutTable;
var files = '';
var imagefile;
 var imagePath;
$(document).ready(function(){
    loader();
    debugger;
    $("#btnPrint").hide();
	
	/*Hide add ward by default functionality*/
	$("#advanced-wizard").hide();
	$("#ledgerList").addClass('list');
	$("#tabledgerList").addClass('tab-list-add');
    bindAssetClass();
    bindAssetLocation();
    bindRoom();
	

	/*Click for add the ledger*/
    $("#tabAddledger").click(function() {
        showAddTab();
        bindAssetClass();
        bindAssetLocation();
        bindRoom();
        clear();
    });

    checkInCheckOutTable = $('#tblCheckInCheckOut').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "autoWidth": false,
        "fnDrawCallback": function(oSettings) {
           
        },
    });
     var startDate = new Date();
    $("#txtPurchaseDate").datepicker('setEndDate',startDate);
     $('#txtPurchaseDate').on('change', function(){
        $('.datepicker').hide();
    });

    $("#selAssetClass").change(function(){
        var htmlRoom ="<option value='-1'>Select</option>";
        
        if ($('#selAssetClass :selected').val() != -1) {
            var value = $('#selAssetClass :selected').val();
            bindAssetSubClass(value,null);
        }
        else {
            $('#selAssetSubClass').html(htmlRoom);
        }
    });
	
	//Click function for show the ledger lists
    $("#tabledgerList").click(function() {
        clear();
        tabledgerList();
    });

   

    $(".customerBtn").click(function(){
       tabButton(this);
    });

     // image upload js.
    $("#image-box").click(function() {
        
        // click use for upload image
        $("#upload").click();
        
        // change image on upload click
        $("#upload").on("change", function() {
            $("#myUpdateStaffModel #errorimage").text("");
            $("#errorimage").text("");
            files = !!this.files ? this.files : [];
            
            // no file selected, or no FileReader support
            if (!files.length || !window.FileReader) return; 
            
            // only image file
            if (/^image/.test(files[0].type)) { 
            
                // instance of the FileReader
                var reader = new FileReader(); 
                
                // read the local file
                reader.readAsDataURL(files[0]); 
                
                // set image data as background of div
                reader.onloadend = function() { 
                    $("#user_reg_image").hide();
                    $("#imagetext").hide();
                    $("#image-box").css("background-image", "url(" + this.result + ")");
                }
            }
        });
    });

    removeErrorMessage();

    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code
       loadDataTable(); 
    }
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            assetRegisterTable.fnClearTable();
            assetRegisterTable.fnDestroy();
            assetRegisterTable = "";
            assetRegisterTable = $('#assetRegisterTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = assetRegisterTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            restoreClick(id);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/asset_register.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "showInActive"
                    });
                },
                "aoColumns":
                [   
                    {
                        "mData": "asset_no"
                    }, {
                        "mData": "asset_class_name"
                    },  {
                        "mData": "sub_class_name"
                    }, {
                        "mData": "location"
                    }, {
                        "mData": "manufacturer_name"
                    }, {
                        "mData": "asset_description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1]
                }, {
                    'bSortable': false,
                    'aTargets': [2]
                }]
            });
        } else { // show active data on unchecked   
            assetRegisterTable.fnClearTable();
            assetRegisterTable.fnDestroy();
            assetRegisterTable = "";
            loadDataTable();
        }
    });

    $("#btnSave").click(function(){
        var flag = validation();

        if(flag == true){
            return false;
        }
        else{
            debugger;
            var manufacturerId = $("#txtManufacturer").attr('data-id');
            if(manufacturerId == undefined){
                var manufacturerName = $("#txtManufacturer").val();
            }
            var brandNameId = $("#txtBrandName").attr('data-id');
            if(brandNameId == undefined){
                var brandName = $("#txtBrandName").val();
            }
            var modalId = $("#txtModal").attr('data-id');
            if(modalId == undefined){
                var modalName = $("#txtModal").val();
            }

            var assetNo = $("#txtAssetNo").val();
            var barcode = $("#txtAssetBarcode").val();
            var description = $("#txtAssetDescription").val();
            var advanceDescription = $("#txtAssetAdvanceDescription").val();
            var assetClass = $("#selAssetClass").val();
            var assetSubClass = $("#selAssetSubClass").val();
            var assetLocation = $("#selAssetLocation").val();
            var serialNo = $("#txtSerialNo").val();
            var licenceNo = $("#txtLicenceNo").val();
            var assignTo = $("#txtAssignTo").val();
            var room = $("#selRoom").val();
            var purchaseDate = $("#txtPurchaseDate").val();
            var purchaseValue = $("#txtPurchaseValue").val();
            
            var postData = {
                "operation" : "save",
                "manufacturerId" : manufacturerId,
                "manufacturerName" : manufacturerName,
                "brandNameId" : brandNameId,
                "brandName" : brandName,
                "modalId" : modalId,
                "modalName" : modalName,
                "assetNo" : assetNo,
                "barcode" : barcode,
                "description" : description,
                "advanceDescription" : advanceDescription,
                "assetClass" : assetClass,
                "assetSubClass" : assetSubClass,
                "assetLocation" : assetLocation,
                "serialNo" : serialNo,
                "licenceNo" : licenceNo,
                "assignTo" : assignTo,
                "room" : room,
                "purchaseDate" : purchaseDate,
                "purchaseValue" : purchaseValue
            }

            $.ajax({
                type: "POST",
                cache: false,
                url: "controllers/admin/asset_register.php",
                datatype:"json",
                data: postData,
                
                success: function(data) {
                    if(data != "0" && data != "" && data != "Barcode" && data != "Asset"){
                        var file_data = $('#upload').prop('files')[0];
                        var form_data = new FormData();
                        form_data.append('file', file_data);
                        var other_data = $('form').serializeArray();
                        form_data.append( 'id', data);
                        var result =  uploadImage(form_data);                        
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Saved successfully!!!");
                        $('#messagemyModal').modal();
                        $('#inactive-checkbox-tick').prop('checked', false).change();
                        clear();
                        
                    }
                    else if( data == "Barcode"){
                        $("#txtAssetBarcode").focus();
                        $("#txtAssetBarcodeError").text("Barcode is already exist");
                        $("#txtAssetBarcode").addClass("errorStyle"); 
                        $("#moreDetails").hide();
                        $("#purchaseInfo").hide();
                        $("#checkOutHistory").hide();
                        $("#attachment").hide();
                        $("#depreciation").hide();
                        $("#mainScreen").show();  
                        $(".customerBtn").removeClass('activeSelf');
                        $($(".customerBtn")[0]).addClass('activeSelf');    
                    }
                    else if( data == "Asset"){
                        $("#txtAssetNo").focus();
                        $("#txtAssetNoError").text("Asset is already exist");
                        $("#txtAssetNo").addClass("errorStyle");
                        $("#moreDetails").hide();
                        $("#purchaseInfo").hide();
                        $("#checkOutHistory").hide();
                        $("#attachment").hide();
                        $("#depreciation").hide();
                        $("#mainScreen").show();
                        $(".customerBtn").removeClass('activeSelf');
                        $($(".customerBtn")[0]).addClass('activeSelf');
                    }
                },
                error: function(){

                }
            });
        }
                    
                
    });

    $("#txtManufacturer").on('keyup', function(){   
        
        var postData = {
            "operation":"searchManufacturer",
            "name": $("#txtManufacturer").val()
        } 
        $("#txtBrandName").val('');
        $("#txtModal").val('');
        autoComplete("#txtManufacturer",postData);
    });

    $("#txtBrandName").on('keyup', function(){   
        if($("#txtManufacturer").attr('data-id') != ""){
            var postData = {
                "operation":"searchBrandName",
                "name": $("#txtBrandName").val(),
                "parentId" : $("#txtManufacturer").attr('data-id')
            } 
             $("#txtModal").val('');
            autoComplete("#txtBrandName",postData);
        }
        
    });

    $("#txtModal").on('keyup', function(){ 
        if($("#txtBrandName").attr('data-id') != ""){
            var postData = {
                "operation":"searchModal",
                "name":  $("#txtModal").val(),
                "parentId" : $("#txtBrandName").attr('data-id')
            }  
            
            autoComplete("#txtModal",postData);
        }
        
    });

    $("#btnReset").click(function(){
        clear();
    });
});
function tabledgerList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
    $("#tabAddledger").html('+Add Asset Count Period');
    $("#tabAddledger").removeClass('tab-detail-add');
    $("#tabAddledger").addClass('tab-detail-remove');
    $("#tabledgerList").addClass('tab-list-add');
    $("#tabledgerList").removeClass('tab-list-remove');
    $("#ledgerList").addClass('list');

    $("#btnReset").show();
    $("#btnSave").show();
    $('#btnUpdate').hide();
    $('#tabAddledger').html("+Add Asset Register");
    clear();
}

function clear(){
    $('input[type=text]').val("");
    $('input[type=text]').next().text(""); 
    $('input[type=text]').removeClass("errorStyle");
    $('select').val("-1");
    $('select').next().text("");
    $('select').removeClass("errorStyle");
    $('textarea').val("");
    $('textarea').next().text(""); 
    $('textarea').removeClass("errorStyle");
    bindAssetClass();
    bindAssetLocation();
    bindRoom();
}


function autoComplete(id,postData){
    //Autocomplete functionality 
   /* $(id).val("");*/
    var currentObj = $(id);
    jQuery(id).autocomplete({
        source: function(request, response) {      
            
            $.ajax(
                {                   
                type: "POST",
                cache: false,
                url: "controllers/admin/asset_register.php",
                datatype:"json",
                data: postData,
                
                success: function(dataSet) {
                    loader();
                    response($.map(JSON.parse(dataSet), function (item) {
                        return {
                            id: item.id,
                            value: item.name
                        }
                    }));
                },
                error: function(){
                    
                }
            });
        },
        select: function (e, i) {
            var id = i.item.id;
            currentObj.attr("data-id", id);                
            
        },
        minLength: 2
    });
}

function bindRoom(){
    $.ajax({                    
        type: "POST",
        cache: false,
        url: "controllers/admin/asset_register.php",
        data: {
            "operation":"showRoom"
        },
        success: function(data) {   
            if(data != null && data != ""){
                var parseData= jQuery.parseJSON(data);
            
                var htmlRoom ="<option value='-1'>Select</option>";
                for (var i=0;i<parseData.length;i++)
                {
                    htmlRoom+="<option value='"+parseData[i].id+"'>"+parseData[i].number+"</option>";
                }
                $('#selRoom').html(htmlRoom);
            }
        },
        error:function() {

        }
    }); 
}

function loadDataTable(){
    assetRegisterTable = $('#assetRegisterTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "fnDrawCallback": function(oSettings) {
            // perform update event
            $('.update').unbind();
            $('.update').on('click', function() {
                var data = $(this).parents('tr')[0];
                var mData = assetRegisterTable.fnGetData(data);
                if (null != mData) // null if we clicked on title row
                {   
                    var obj = {};
                    obj.id = mData["id"];
                    obj.asset_no = mData["asset_no"];
                    obj.asset_barcode = mData["asset_barcode"];
                    obj.asset_description = mData["asset_description"];
                    obj.asset_advance_description = mData["asset_advance_description"];
                    obj.asset_class_id = mData["asset_class_id"];
                    obj.asset_sub_class_id = mData["asset_sub_class_id"];
                    obj.asset_location_id = mData["asset_location_id"];
                    obj.manufacturer_name = mData["manufacturer_name"];
                    obj.manufacturer_id = mData["manufacturer_id"];
                    obj.brand_name = mData["brand_name"];
                    obj.brand_name_id = mData["brand_name_id"];
                    obj.modal_id = mData["modal_id"];
                    obj.modal = mData["modal"];
                    obj.serial_no = mData["serial_no"];
                    obj.licence_no = mData["licence_no"];
                    obj.assign_to = mData["assign_to"];
                    obj.room_id = mData["room_id"];
                    obj.image_path = mData["image_path"];
                    obj.purchase_date = mData["purchase_date"];
                    obj.purchase_value = mData["purchase_value"];
                    editClick(obj);

                }
            });
            //perform delete event
            $('.delete').unbind();
            $('.delete').on('click', function() {
                var data = $(this).parents('tr')[0];
                var mData = assetRegisterTable.fnGetData(data);

                if (null != mData) // null if we clicked on title row
                {
                    var id = mData["id"];
                    deleteClick(id);
                }
            });
        },
        "sAjaxSource": "controllers/admin/asset_register.php",
        "fnServerParams": function(aoData) {
            aoData.push({
                "name": "operation",
                "value": "show"
            });
        },
        "aoColumns":
        [   
            {
                "mData": "asset_no"
            }, {
                "mData": "asset_class_name"
            },  {
                "mData": "sub_class_name"
            }, {
                "mData": "location"
            }, {
                "mData": "manufacturer_name"
            }, {
                "mData": "asset_description"
            },{                
                "mData": function(o) {
                    var data = o;
                    return "<i class='fa fa-pencil update' title='Edit'" +
                        " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                        " <i class='fa fa-trash-o delete' title='Delete' " +
                        " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                        " data-original-title='Delete'></i>";
                }
            },
        ],
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [1]
        }, {
            'bSortable': false,
            'aTargets': [2]
        }]

    });
}

// edit data dunction for update 
function editClick(obj) {
    debugger;

     showAddTab();
    $('#chkUpdate').show();
    $("#txtManufacturer").attr('data-id',obj.manufacturer_id);
    $("#txtManufacturer").val(obj.manufacturer_name);
    $("#txtBrandName").attr('data-id',obj.brand_name_id);
    $("#txtBrandName").val(obj.brand_name);
    $("#txtModal").attr('data-id',obj.modal_id);
    $("#txtModal").val(obj.modal);

    $("#txtAssetNo").val(obj.asset_no);
    $("#txtAssetNo").attr('disabled','disabled');
    $("#txtAssetBarcode").val(obj.asset_barcode);
    $("#txtAssetBarcode").attr('disabled','disabled');
    $("#txtAssetDescription").val(obj.asset_description);
    $("#txtAssetAdvanceDescription").val(obj.asset_advance_description);
    $("#selAssetClass").val(obj.asset_class_id);
    bindAssetSubClass(obj.asset_class_id,obj.asset_sub_class_id);
    
    $("#selAssetLocation").val(obj.asset_location_id);
    $("#txtSerialNo").val(obj.serial_no);
    $("#txtLicenceNo").val(obj.licence_no);
    $("#txtAssignTo").val(obj.assign_to);
    $("#selRoom").val(obj.room_id);
    $("#txtPurchaseDate").val(obj.purchase_date);
    $("#txtPurchaseValue").val(obj.purchase_value);

    $("#btnReset").hide();
    $("#btnSave").hide();
    $("#btnPrint").hide();
    $('#tabAddledger').html("+Update Asset Register");

    $("#btnUpdate").show();
    $('#selectedRow').val(obj.id);
    
    //validation
   
    $("#btnUpdate").click(function() { // click update button
        var flag = validation();

        var manufacturerId = $("#txtManufacturer").attr('data-id');
        if(manufacturerId == undefined){
            var manufacturerName = $("#txtManufacturer").val();
        }
        var brandNameId = $("#txtBrandName").attr('data-id');
        if(brandNameId == undefined){
            var brandName = $("#txtBrandName").val();
        }
        var modalId = $("#txtModal").attr('data-id');
        if(modalId == undefined){
            var modalName = $("#txtModal").val();
        }

        var assetNo = $("#txtAssetNo").val();
        var barcode = $("#txtAssetBarcode").val();
        var description = $("#txtAssetDescription").val();
        var advanceDescription = $("#txtAssetAdvanceDescription").val();
        var assetClass = $("#selAssetClass").val();
        var assetSubClass = $("#selAssetSubClass").val();
        var assetLocation = $("#selAssetLocation").val();
        var serialNo = $("#txtSerialNo").val();
        var licenceNo = $("#txtLicenceNo").val();
        var assignTo = $("#txtAssignTo").val();
        var room = $("#selRoom").val();
        var purchaseDate = $("#txtPurchaseDate").val();
        var purchaseValue = $("#txtPurchaseValue").val();
        var id = $('#selectedRow').val();

        var postData = {
            "operation" : "update",
            "manufacturerId" : manufacturerId,
            "manufacturerName" : manufacturerName,
            "brandNameId" : brandNameId,
            "brandName" : brandName,
            "modalId" : modalId,
            "modalName" : modalName,
            "assetNo" : assetNo,
            "barcode" : barcode,
            "description" : description,
            "advanceDescription" : advanceDescription,
            "assetClass" : assetClass,
            "assetSubClass" : assetSubClass,
            "assetLocation" : assetLocation,
            "serialNo" : serialNo,
            "licenceNo" : licenceNo,
            "assignTo" : assignTo,
            "room" : room,
            "purchaseDate" : purchaseDate,
            "purchaseValue" : purchaseValue,
            "id": id
        }

        $.ajax( //ajax call for update data
            {
            type: "POST",
            cache: false,
            url: "controllers/admin/asset_register.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('#myModal').modal('hide');
                    $('.close-confirm').click();
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Updated successfully!!!");
                    $('#messagemyModal').modal();
                    var file_data = $('#upload').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('file', file_data);
                    form_data.append( 'id', $('#selectedRow').val());
                    var result =  uploadImage(form_data);
                    clear();
                    tabledgerList();
                   
                }
            },
            error: function() {
                $('.close-confirm').click();
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end of ajax
    });
} // end update button

function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Asset");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteDisease("' + type + '");');
} // end click fucntion

function restoreClick(id) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Asset");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteDisease("' + type + '");');
}

// define that function perform for delete and restore event
function deleteDisease(type) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/asset_register.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Asset deleted successfully!!!");
                    $('#messagemyModal').modal();
                    assetRegisterTable.fnReloadAjax();
                } else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This asset is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/asset_register.php",
            data: {
                "operation": "restore",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Asset restored successfully!!!");
                    $('#messagemyModal').modal();
                    assetRegisterTable.fnReloadAjax();

                }
            },
            error: function() {             
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}

function tabButton(current){
     $(".customerBtn").removeClass('activeSelf');
        $(current).addClass('activeSelf');
        if($(current).val() == 'More Details'){
            $("#moreDetails").show();
            $("#purchaseInfo").hide();
            $("#checkOutHistory").hide();
            $("#attachment").hide();
            $("#depreciation").hide();
            $("#mainScreen").hide();
            $(".showBtn").show();
        }
        else if($(current).val() == 'Purchase & Insurance Info'){
            $("#moreDetails").hide();
            $("#purchaseInfo").show();
            $("#checkOutHistory").hide();
            $("#attachment").hide();
            $("#depreciation").hide();
            $("#mainScreen").hide();
            $(".showBtn").show();
        }
        else if($(current).val() == 'Check In/Out History'){
            $("#moreDetails").hide();
            $("#purchaseInfo").hide();
            $("#checkOutHistory").show();
            $("#attachment").hide();
            $("#depreciation").hide();
            $("#mainScreen").hide();
            $(".showBtn").hide();
        }
        else if($(current).val() == 'Attachments'){
            $("#moreDetails").hide();
            $("#purchaseInfo").hide();
            $("#checkOutHistory").hide();
            $("#attachment").show();
            $("#depreciation").hide();
            $("#mainScreen").hide();
            $(".showBtn").hide();
        }
        else if($(current).val() == 'Depreciation Schedule'){
            $("#moreDetails").hide();
            $("#purchaseInfo").hide();
            $("#checkOutHistory").hide();
            $("#attachment").hide();
            $("#depreciation").show();
            $("#mainScreen").hide();
            $(".showBtn").hide();
        }
        else{
            $("#moreDetails").hide();
            $("#purchaseInfo").hide();
            $("#checkOutHistory").hide();
            $("#attachment").hide();
            $("#depreciation").hide();
            $("#mainScreen").show();
            $(".showBtn").show();
        }
}

function validation(){
    var flag = false;

    if($("#txtModal").val() == ""){
        $("#txtModal").focus();
        $("#txtModalError").text("Please enter modal");
        $("#txtModal").addClass("errorStyle");
        flag = true;
    }

    if($("#txtBrandName").val() == ""){
        $("#txtBrandName").focus();
        $("#txtBrandNameError").text("Please enter brand name");
        $("#txtBrandName").addClass("errorStyle");
        flag = true;
    }

    if($("#txtManufacturer").val() == ""){
        $("#txtManufacturer").focus();
        $("#txtManufacturerError").text("Please enter manufacturer");
        $("#txtManufacturer").addClass("errorStyle");
        flag = true;
    }

    if($("#selAssetLocation").val() == "-1"){
        $("#selAssetLocation").focus();
        $("#selAssetLocationError").text("Please select asset location");
        $("#selAssetLocation").addClass("errorStyle");
        flag = true;
    }

    if($("#selAssetSubClass").val() == "-1"){
        $("#selAssetSubClass").focus();
        $("#selAssetSubClassError").text("Please select asset sub class");
        $("#selAssetSubClass").addClass("errorStyle");
        flag = true;
    }

    if($("#selAssetClass").val() == "-1"){
        $("#selAssetClass").focus();
        $("#selAssetClassError").text("Please select asset class");
        $("#selAssetClass").addClass("errorStyle");
        flag = true;
    }

    if($("#txtAssetDescription").val() == ""){
        $("#txtAssetDescription").focus();
        $("#txtAssetDescriptionError").text("Please enter asset description");
        $("#txtAssetDescription").addClass("errorStyle");
        flag = true;
    }

    if($("#txtAssetBarcode").val() == ""){
        $("#txtAssetBarcode").focus();
        $("#txtAssetBarcodeError").text("Please enter barcode");
        $("#txtAssetBarcode").addClass("errorStyle");
        flag = true;
    }

    if($("#txtAssetNo").val() == ""){
        $("#txtAssetNo").focus();
        $("#txtAssetNoError").text("Please enter asset no");
        $("#txtAssetNo").addClass("errorStyle");
        flag = true;
    }

    if (flag == true) {
        $(".customerBtn").removeClass('activeSelf');
        $("#moreDetails").hide();
        $("#purchaseInfo").hide();
        $("#checkOutHistory").hide();
        $("#attachment").hide();
        $("#depreciation").hide();
        $("#mainScreen").show();
        $($(".customerBtn")[0]).addClass("activeSelf");
        return flag;
    }
    else{
        $(".customerBtn").removeClass('activeSelf');
        $("#moreDetails").show();
        $("#purchaseInfo").hide();
        $("#checkOutHistory").hide();
        $("#attachment").hide();
        $("#depreciation").hide();
        $("#mainScreen").hide();
        $($(".customerBtn")[1]).addClass("activeSelf");

        if($("#selRoom").val() == "-1"){
            $("#selRoom").focus();
            $("#selRoomError").text("Please select room");
            $("#selRoom").addClass("errorStyle");
            flag = true;
        }

    }
    if(flag == true){
        $(".customerBtn").removeClass('activeSelf');
        $("#moreDetails").show();
        $("#purchaseInfo").hide();
        $("#checkOutHistory").hide();
        $("#attachment").hide();
        $("#depreciation").hide();
        $("#mainScreen").hide();
        $($(".customerBtn")[1]).addClass("activeSelf");
        return flag;
    }
    else{
        $(".customerBtn").removeClass('activeSelf');
        $("#moreDetails").hide();
        $("#purchaseInfo").show();
        $("#checkOutHistory").hide();
        $("#attachment").hide();
        $("#depreciation").hide();
        $("#mainScreen").hide();
        $($(".customerBtn")[2]).addClass("activeSelf");

        if($("#txtPurchaseDate").val() == ""){
            $("#txtPurchaseDate").focus();
            $("#txtPurchaseDateError").text("Please enter purchase date");
            $("#txtPurchaseDate").addClass("errorStyle");
            flag = true;
        }

        if($("#txtPurchaseValue").val() == ""){
            $("#txtPurchaseValue").focus();
            $("#txtPurchaseValueError").text("Please enter purchase value");
            $("#txtPurchaseValue").addClass("errorStyle");
            flag = true;
        }
    }

    return flag;
}

function bindAssetClass() {     // function for loading account number of asset type
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/asset_register.php",
        data: {
            "operation": "showAssetClass"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value='-1'>--Select--</option>";
                $.each(parseData,function(i,v){
                    option += "<option value='" +v.id+ "'>" + v.asset_class_name + "</option>";
                });
                
                $('#selAssetClass').html(option);
                $('#selAssetSubClass').html("<option value='-1'>--Select--</option>");
            }
        },
        error: function() {}
    });
}

function bindAssetSubClass(value,subClassId){
    $.ajax({                    
        type: "POST",
        cache: false,
        url: "controllers/admin/asset_register.php",
        data: {
            "operation":"showAssetsubClass",
            "subClass" : value
        },
        success: function(data) {   
            if(data != null && data != ""){
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value='-1'>--Select--</option>";
                $.each(parseData,function(i,v){
                    option += "<option value='" +v.id+ "'>" + v.sub_class_name + "</option>";
                });
                
                $('#selAssetSubClass').html(option); 
                if(subClassId != null){
                     $("#selAssetSubClass").val(subClassId);
                }

                             
            }
        },
        error:function() {
            $('#messagemyModal').modal();
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
        }
    }); 
}

function bindAssetLocation() {     // function for loading account number of asset type
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/asset_register.php",
        data: {
            "operation": "showAssetLocation"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value='-1'>--Select--</option>";
                $.each(parseData,function(i,v){
                    option += "<option value='" +v.id+ "'>" + v.location + "</option>";
                });
                
                $('#selAssetLocation').html(option);
            }
        },
        error: function() {}
    });
}

function showAddTab(){
    $("#advanced-wizard").show();
    $("#txtAssetNo").attr('disabled',false);
    $("#txtAssetBarcode").attr('disabled',false);
    $(".blackborder").hide();
    $("#tabAddledger").addClass('tab-detail-add');
    $("#tabAddledger").removeClass('tab-detail-remove');
    $("#tabledgerList").removeClass('tab-list-add');
    $("#tabledgerList").addClass('tab-list-remove');
    $("#ledgerList").addClass('list');
    
    $('#chkUpdate').hide();
    $('#btnSubmit').show();
    $('#btnReset').show();
    $('#btnUpdate').hide();
    $("#moreDetails").hide();
    $("#purchaseInfo").hide();
    $("#checkOutHistory").hide();
    $("#attachment").hide();
    $("#depreciation").hide();
    $("#mainScreen").show();
    $($(".customerBtn")[0]).addClass("activeSelf"); 
}

function uploadImage(form_data){   
    
    var imageName;
    $.ajax({
        url: 'controllers/admin/asset_register.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'POST',
        success: function(data) {
            if (data == "invalid file") {
                $("#errorimage").show();
                $("#errorimage").text("Please upload image of less than 2mb size.");
                $("#advanced-first").show();
                $("#advanced-Second").hide();
            } 
            else {
                return data;
            }
        },
        error : function(){

        }
    });
}