$(document).ready(function() {
	debugger;	
	google.charts.setOnLoadCallback(drawChart);
});

function drawChart() {	
	var arr = new Array();
	var opts = new Array();
	var chartWidth  = $("#page-content").width()/2 -25;
	var bigArrayLabBill = [['Paid','Unpaid']];
	var bigArrayRadiologyBill = [['Paid','Unpaid']];
	var bigArrayServiceBill = [['Paid','Unpaid']];
	var bigArrayProcedureBill = [['Paid','Unpaid']];
	var bigArrayForensicBill = [['Paid','Unpaid']];

	var bigArrayGpLabBill = [['Test Name','Paid','Unpaid']];
	var bigArrayGpRadiologyBill = [['Test Name','Paid','Unpaid']];
	var bigArrayGpServiceBill = [['Test Name','Paid','Unpaid']];
	var bigArrayGpProcedureBill = [['Test Name','Paid','Unpaid']];
	var bigArrayGpForensicBill = [['Test Name','Paid','Unpaid']];

	var bigArrayDepartment = [['Department','Count']];

	/*Ajax call for chart*/
	var postData = {
		"operation" : "showChartData"
	}
	$.ajax({     
		type: "POST",
		cache: false,
		url: "controllers/admin/daily_sales_report.php",
		data: postData,
		success: function(data) {
			if(data != null && data != ""){
				var parseData =  JSON.parse(data);
				bigArrayLabBill = [['paid-patients','Unpaid-patients']];
				bigArrayRadiologyBill = [['paid-patients','Unpaid-patients']];
				bigArrayServiceBill = [['paid-patients','Unpaid-patients']];
				bigArrayProcedureBill = [['paid-patients','Unpaid-patients']];
				bigArrayForensicBill = [['paid-patients','Unpaid-patients']];

				bigArrayGpLabBill = [['Test Name','Paid','Unpaid']];
				bigArrayGpRadiologyBill = [['Test Name','Paid','Unpaid']];
				bigArrayGpServiceBill = [['Test Name','Paid','Unpaid']];
				bigArrayGpProcedureBill = [['Test Name','Paid','Unpaid']];
				bigArrayGpForensicBill = [['Test Name','Paid','Unpaid']];

				bigArrayDepartment = [['Department','Count']];
				$.each(parseData,function(index,v){
					//code for excel file
					if (parseData[index].length > 0) {

						arr.push(parseData[index]);

						if (index == 0) {
							opts.push({sheetid:'Lab Bill Patients',header:true});
						}
						else if (index == 1) {
							opts.push({sheetid:'Lab Bill',header:true});
						}
						else if (index == 2) {
							opts.push({sheetid:'Radioliogy Bill Patients',header:true});
						}
						else if (index == 3) {
							opts.push({sheetid:'Radioliogy Bill',header:true});
						}
						else if (index == 4) {
							opts.push({sheetid:'Service Bill Patients',header:true});
						}
						else if (index == 5) {
							opts.push({sheetid:'Service Bill',header:true});
						}
						else if (index == 6) {
							opts.push({sheetid:'Procedure Bill Patients',header:true});
						}
						else if (index == 7) {
							opts.push({sheetid:'Procedure Bill',header:true});
						}
						else if (index == 8) {
							opts.push({sheetid:'Forensic Bill Patients',header:true});
						}
						else {
							opts.push({sheetid:'Forensic Bill',header:true});
						}
					}//code for excel file end

					for(var i=0;i<parseData[index].length;i++){

						if(index==0){
							smallArrayGpLabBill = [];
							smallArrayGpLabBill.push(parseData[index][i].name);
							smallArrayGpLabBill.push(parseInt(parseData[index][i]['paid-patients']));
							smallArrayGpLabBill.push(parseInt(parseData[index][i]['unpaid-patients']));
							bigArrayGpLabBill.push(smallArrayGpLabBill);
						}
						else if (index == 1) {

							$.each(parseData[index][i],function(im,nn){
								bigArrayLabBill.push([im,parseInt(nn)]);
							});
						}

						else if (index == 2) {

							smallArrayGpRadiologyBill = [];
							smallArrayGpRadiologyBill.push(parseData[index][i].name);
							smallArrayGpRadiologyBill.push(parseInt(parseData[index][i]['paid-patients']));
							smallArrayGpRadiologyBill.push(parseInt(parseData[index][i]['unpaid-patients']));
							bigArrayGpRadiologyBill.push(smallArrayGpRadiologyBill);
						}

						else if (index == 3) {

							$.each(parseData[index][i],function(im,nn){
								bigArrayRadiologyBill.push([im,parseInt(nn)]);
							});
						}

						else if (index == 4) {

							smallArrayGpServiceBill = [];
							smallArrayGpServiceBill.push(parseData[index][i].name);
							smallArrayGpServiceBill.push(parseInt(parseData[index][i]['paid-patients']));
							smallArrayGpServiceBill.push(parseInt(parseData[index][i]['unpaid-patients']));
							bigArrayGpServiceBill.push(smallArrayGpServiceBill);
						}

						else if (index == 5) {

							$.each(parseData[index][i],function(im,nn){
								bigArrayServiceBill.push([im,parseInt(nn)]);
							});
						}	

						else if (index == 6) {

							smallArrayGpProcedureBill = [];
							smallArrayGpProcedureBill.push(parseData[index][i].name);
							smallArrayGpProcedureBill.push(parseInt(parseData[index][i]['paid-patients']));
							smallArrayGpProcedureBill.push(parseInt(parseData[index][i]['unpaid-patients']));
							bigArrayGpProcedureBill.push(smallArrayGpProcedureBill);
						}

						else if (index == 7) {
							
							$.each(parseData[index][i],function(im,nn){
								bigArrayProcedureBill.push([im,parseInt(nn)]);
							});
						}

						else if (index == 8) {

							smallArrayGpForensicBill = [];
							smallArrayGpForensicBill.push(parseData[index][i].name);
							smallArrayGpForensicBill.push(parseInt(parseData[index][i]['paid-patients']));
							smallArrayGpForensicBill.push(parseInt(parseData[index][i]['unpaid-patients']));
							bigArrayGpForensicBill.push(smallArrayGpForensicBill);
						}

						else if (index == 9) {
							$.each(parseData[index][i],function(im,nn){
								bigArrayForensicBill.push([im,parseInt(nn)]);
							});								
						}
						else if (index ==10) {
							smallArrayDepartment = [];
							smallArrayDepartment.push(parseData[index][i].department);
							smallArrayDepartment.push(parseInt(parseData[index][i].count));
							bigArrayDepartment.push(smallArrayDepartment);
						}
					}
				});

				var dataLabBill = google.visualization.arrayToDataTable(bigArrayLabBill);

				if (bigArrayGpLabBill.length > 1) {
					var dataGpLabBill = google.visualization.arrayToDataTable(bigArrayGpLabBill);
				}
				
				var dataRadiologyBill = google.visualization.arrayToDataTable(bigArrayRadiologyBill);

				if (bigArrayGpRadiologyBill.length > 1){ 
					var dataGpRadiologyBill = google.visualization.arrayToDataTable(bigArrayGpRadiologyBill);
				}
				var dataServiceBill = google.visualization.arrayToDataTable(bigArrayServiceBill);

				if (bigArrayGpServiceBill.length > 1){
					var dataGpServiceBill = google.visualization.arrayToDataTable(bigArrayGpServiceBill);
				}

				var dataProcedureBill = google.visualization.arrayToDataTable(bigArrayProcedureBill);

				if (bigArrayGpProcedureBill.length > 1){
					var dataGpProcedureBill = google.visualization.arrayToDataTable(bigArrayGpProcedureBill);
				}

				var dataForensicBill = google.visualization.arrayToDataTable(bigArrayForensicBill);

				if (bigArrayGpForensicBill.length > 1){
					var dataGpForensicBill = google.visualization.arrayToDataTable(bigArrayGpForensicBill);
				}

				if (bigArrayDepartment.length >1) {
					var dataDepartment = google.visualization.arrayToDataTable(bigArrayDepartment);
				}					


				var optionsLabBill = {
			      title: 'Lab Bill Analysis','width':1200,'height':600,is3D:true
			    };
			    var optionsGpLabBill = {
			      title: 'Lab Bill Analysis','width':1200,'height':1000,is3D:true
			    };
			    var optionsRadiologyBill = {
			      title: 'RadiologyBill Analysis','width':1200,'height':600,is3D:true
			    };
			    var optionsGpRadiologyBill = {
			      title: 'RadiologyBill Analysis','width':1200,'height':1000,is3D:true
			    };
			    var optionsServiceBill = {
			      title: 'ServiceBill Analysis','width':1200,'height':600,is3D:true
			    };
			    var optionsGpServiceBill = {
			      title: 'ServiceBill Analysis','width':1200,'height':1000,is3D:true
			    };
			    var optionsProcedureBill = {
			      title: 'ProcedureBill Analysis','width':1200,'height':600,is3D:true
			    };
			    var optionsGpProcedureBill = {
			      title: 'ProcedureBill Analysis','width':1200,'height':1000,is3D:true
			    };
			    var optionsForensicBill = {
			      title: 'ForensicBill Analysis','width':1200,'height':600,is3D:true
			    };
			    var optionsGpForensicBill = {
			      title: 'ForensicBill Analysis','width':1200,'height':1000,is3D:true
			    };

			    var optionsVisitDept = {
			      title: 'Visitors Analysis','width':chartWidth,'height':500
			    };

			    if (dataDepartment !=undefined) {
			    	var chartVisitDept = new google.visualization.PieChart(document.getElementById('chart_div1'));
			    	chartVisitDept.draw(dataDepartment, optionsVisitDept);
			    }			    

			    var chartLabBill = new google.visualization.PieChart(document.getElementById('chart_div'));
			    chartLabBill.draw(dataLabBill, optionsLabBill);

			    var chartRadiologyBill = new google.visualization.PieChart(document.getElementById('chart_div2'));
			    chartRadiologyBill.draw(dataRadiologyBill, optionsRadiologyBill);

			    var chartServiceBill = new google.visualization.PieChart(document.getElementById('chart_div3'));
			    chartServiceBill.draw(dataServiceBill, optionsServiceBill);

			    var chartProcedureBill = new google.visualization.PieChart(document.getElementById('chart_div4'));
			    chartProcedureBill.draw(dataProcedureBill, optionsProcedureBill);

			    var chartForensicBill = new google.visualization.PieChart(document.getElementById('chart_div5'));
			    chartForensicBill.draw(dataForensicBill, optionsForensicBill);


			    /*GroupChart BarChart*/

			    if (dataGpLabBill!= undefined){ 
				    var chartGpLabBill = new google.visualization.BarChart(document.getElementById('chart_div6'));
				    chartGpLabBill.draw(dataGpLabBill, optionsGpLabBill);
				}

				if (dataGpRadiologyBill!= undefined){ 
				    var chartGpRadiologyBill = new google.visualization.BarChart(document.getElementById('chart_div7'));
				    chartGpRadiologyBill.draw(dataGpRadiologyBill, optionsGpRadiologyBill);
				}

				if (dataGpServiceBill!= undefined){ 
				    var chartGpServiceBill = new google.visualization.BarChart(document.getElementById('chart_div8'));
				    chartGpServiceBill.draw(dataGpServiceBill, optionsGpServiceBill);
				}

				if (dataGpProcedureBill!= undefined){ 
				    var chartGpProcedureBill = new google.visualization.BarChart(document.getElementById('chart_div9'));
				    chartGpProcedureBill.draw(dataGpProcedureBill, optionsGpProcedureBill);
				}

				if (dataGpForensicBill!= undefined){ 
				    var chartGpForensicBill = new google.visualization.BarChart(document.getElementById('chart_div10'));
				    chartGpForensicBill.draw(dataGpForensicBill, optionsGpForensicBill);
				}

				$('svg').css("max-width",$("#page-content").width()-50);//max-width for graph
				
				if ($('#chart_div1').text().length == 0){
					$('#chart_div1').hide();
				}
				
				$("#btnSaveExcel,#btnSavePdf").show();

				/*function to save in to excel file*/
				window.saveFile = function saveFile () {

					var res = alasql('SELECT INTO XLSX("DailySalesReport.xlsx",?) FROM ?',
	             		[opts,arr]
	             	);
				}
			}
		}
	});

	$("#btnSavePdf").click(function(){
		$.print("#printableArea");
	});
}