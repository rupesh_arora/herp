$(document).ready(function() {
	debugger;
	$(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});

	$("#toDate").attr("disabled",true);
	$("#fromDate").attr("disabled",true);	

	$('.changeRadio').change(function() {
		if ($('#radToFromDate').prop( "checked") == true) {
	        $("#toDate").attr("disabled",false);
			$("#fromDate").attr("disabled",false);
	    }
	    else{
	    	$("#toDate").attr("disabled",true);
			$("#fromDate").attr("disabled",true);
			$("#toDate").val("");
			$("#fromDate").val("");
	    }
	});	
	$("#fromDate").change(function() {
		if ($("#fromDate").val() !='') {
			$("#fromDate").removeClass("errorStyle");
			$("#txtFromDateError").text("");
		}
	});
	$("#toDate").change(function() {
		if ($("#toDate").val() !='') {
			$("#toDate").removeClass("errorStyle");
			$("#txtToDateError").text("");
		}
	});	

	$("#clear").click(function(){
		clear();
	});
	google.charts.setOnLoadCallback(drawChart);
});

function drawChart() {
	var dataLoad;
	var chartWidth  = $("#page-content").width() -25;
	var bigArrayLabBill = [['Paid','Unpaid']];
	var bigArrayRadiologyBill = [['Paid','Unpaid']];
	var bigArrayServiceBill = [['Paid','Unpaid']];
	var bigArrayProcedureBill = [['Paid','Unpaid']];
	var bigArrayForensicBill = [['Paid','Unpaid']];

	var bigArrayGpLabBill = [['Test Name','Paid','Unpaid']];
	var bigArrayGpRadiologyBill = [['Test Name','Paid','Unpaid']];
	var bigArrayGpServiceBill = [['Test Name','Paid','Unpaid']];
	var bigArrayGpProcedureBill = [['Test Name','Paid','Unpaid']];
	var bigArrayGpForensicBill = [['Test Name','Paid','Unpaid']];

	$("#viewReport").on("click",function(){
		var fromDate ;
		var toDate ;
		var arr = new Array();
		var opts = new Array();
		if ($('#radToday').prop( "checked") == true) {
			dataLoad = "today";
		}
		else if($('#radLastSevenDays').prop( "checked") == true) {
			dataLoad = "last7days";
		}
		else if($('#radToFromDate').prop( "checked") == true) {
			dataLoad = "betweenDate";
			fromDate = $("#fromDate").val();
			toDate = $("#toDate").val();
			if (fromDate =='') {
				$("#txtFromDateError").text("Please choose from date");
				$("#fromDate").addClass("errorStyle");
				$("#fromDate").focus();
				return false;
			}
			if (toDate =='') {
				$("#txtToDateError").text("Please choose from date");
				$("#toDate").addClass("errorStyle");
				$("#toDate").focus();
				return false;
			}
		}
		else if($('#radLastThirtyDays').prop( "checked") == true) {
			dataLoad = "last30days";
		}
		else if($('#radAllTime').prop( "checked") == true) {
			dataLoad = "all";
		}

		if (fromDate == undefined) {
			fromDate ="";
		}
		if (toDate == undefined) {
			toDate = "";
		}
		if (dataLoad==undefined) {
			return false;
		}
		var postData = {
			"operation" : "showChartData",
			"dataLoad" : dataLoad,
			"fromDate" : fromDate,
			"toDate" : toDate
		}
		$.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/sales_report.php",
			data: postData,
			success: function(data) {
				if(data != null && data != ""){
					var parseData =  JSON.parse(data);
					if (parseData[1] =='' &&parseData[3] =='' && parseData[5] =='' && parseData[5] =='' &&
						parseData[7] =='' && parseData[0][0]['Paid Amount'] =='' && parseData[2][0]['Unpaid Amount'] ==''
						&& parseData[2][0]['Paid Amount'] =='' && parseData[2][0]['Unpaid Amount'] ==''
						&& parseData[4][0]['Paid Amount'] =='' && parseData[4][0]['Unpaid Amount'] ==''
						&& parseData[6][0]['Paid Amount'] =='' && parseData[6][0]['Unpaid Amount'] ==''
						&& parseData[8][0]['Paid Amount'] =='' && parseData[8][0]['Unpaid Amount'] =='') {
						clear();
						$('#messagemyModal').modal();
						$('#messageMyModalLabel').text("Alert");
						$('.modal-body').text("No data exist!!!");
						$("#btnSaveExcel,#btnSavePdf").hide();
						return false;
					}
					$(".chartDiv,#btnSaveExcel,#btnSavePdf").show();
					bigArrayGpLabBill = [['Paid','Unpaid']];
					bigArrayGpRadiologyBill = [['Paid','Unpaid']];
					bigArrayGpServiceBill = [['Paid','Unpaid']];
					bigArrayGpProcedureBill = [['Paid','Unpaid']];
					bigArrayGpForensicBill = [['Paid','Unpaid']];

					bigArrayLabBill = [['Test Name','Paid','Unpaid']];
					bigArrayRadiologyBill = [['Test Name','Paid','Unpaid']];
					bigArrayServiceBill = [['Test Name','Paid','Unpaid']];
					bigArrayProcedureBill = [['Test Name','Paid','Unpaid']];
					bigArrayForensicBill = [['Test Name','Paid','Unpaid']];

					$.each(parseData,function(index,v){
						//code for excel file
						if (parseData[index] !='') {
							if (parseData[index][0]['Paid Amount'] !='' || parseData[index][0]['Unpaid Amount'] !='' ) {
								arr.push(parseData[index]);

								if (index == 0) {
									opts.push({sheetid:'Over all Lab Bill',header:true});
								}
								else if (index == 1) {
									opts.push({sheetid:'Department wise Lab Bill',header:true});
								}
								else if (index == 2) {
									opts.push({sheetid:'Over all Radioliogy Bill Patients',header:true});
								}
								else if (index == 3) {
									opts.push({sheetid:'Department wise Radioliogy Bill',header:true});
								}
								else if (index == 4) {
									opts.push({sheetid:'Over all Service Bill Patients',header:true});
								}
								else if (index == 5) {
									opts.push({sheetid:'Department wise Service Bill',header:true});
								}
								else if (index == 6) {
									opts.push({sheetid:'Over all Procedure Bill Patients',header:true});
								}
								else if (index == 7) {
									opts.push({sheetid:'Department wise Procedure Bill',header:true});
								}
								else if (index == 8) {
									opts.push({sheetid:'Over all Forensic Bill Patients',header:true});
								}
								else {
									opts.push({sheetid:'Department wise Forensic Bill',header:true});
								}
							}//code for excel file end
						}
							

						for(var i=0;i<parseData[index].length;i++){
							if(index==0){

								$.each(parseData[index][i],function(i,v){
									if (v !='') {
										bigArrayGpLabBill.push([i,parseInt(v)]);
									}										
								});
							}
							else if (index == 1) {

								smallArrayLabBill = [];
								smallArrayLabBill.push(parseData[index][i]['Lab Test']);
								smallArrayLabBill.push(parseInt(parseData[index][i]['Paid Amount']));
								smallArrayLabBill.push(parseInt(parseData[index][i]['Unpaid Amount']));
								bigArrayLabBill.push(smallArrayLabBill);
							}

							else if (index == 2) {

								$.each(parseData[index][i],function(i,v){
									if (v !='') {
										bigArrayGpRadiologyBill.push([i,parseInt(v)]);
									}
								});
							}

							else if (index == 3) {

								smallArrayRadiologyBill = [];
								smallArrayRadiologyBill.push(parseData[index][i]['Radiology Test']);
								smallArrayRadiologyBill.push(parseInt(parseData[index][i]['Paid Amount']));
								smallArrayRadiologyBill.push(parseInt(parseData[index][i]['Unpaid Amount']));
								bigArrayRadiologyBill.push(smallArrayRadiologyBill);								
							}

							else if (index == 4) {

								$.each(parseData[index][i],function(i,v){
									if (v !='') {
										bigArrayGpServiceBill.push([i,parseInt(v)]);
									}
								});
							}

							else if (index == 5) {

								smallArrayServiceBill = [];
								smallArrayServiceBill.push(parseData[index][i]['Service Name']);
								smallArrayServiceBill.push(parseInt(parseData[index][i]['Paid Amount']));
								smallArrayServiceBill.push(parseInt(parseData[index][i]['Unpaid Amount']));
								bigArrayServiceBill.push(smallArrayServiceBill);
							}	

							else if (index == 6) {

								$.each(parseData[index][i],function(i,v){
									if (v !='') {
										bigArrayGpProcedureBill.push([i,parseInt(v)]);
									}
								});
							}

							else if (index == 7) {
								
								smallArrayProcedureBill = [];
								smallArrayProcedureBill.push(parseData[index][i]['Procedue Name']);
								smallArrayProcedureBill.push(parseInt(parseData[index][i]['Paid Amount']));
								smallArrayProcedureBill.push(parseInt(parseData[index][i]['Unpaid Amount']));
								bigArrayProcedureBill.push(smallArrayProcedureBill);
							}

							else if (index == 8) {

								$.each(parseData[index][i],function(i,v){
									if (v !='') {
										bigArrayGpForensicBill.push([i,parseInt(v)]);
									}
								});
							}

							else if (index == 9) {

								smallArrayForensicBill = [];
								smallArrayForensicBill.push(parseData[index][i]['Forensic Test']);
								smallArrayForensicBill.push(parseInt(parseData[index][i]['Paid Amount']));
								smallArrayForensicBill.push(parseInt(parseData[index][i]['Unpaid Amount']));
								bigArrayForensicBill.push(smallArrayForensicBill);
							}
						}
					});
					
					if (bigArrayGpLabBill.length > 1) {
						var dataLabBill = google.visualization.arrayToDataTable([['','']]);
						var dataLabBill = google.visualization.arrayToDataTable(bigArrayGpLabBill);
						var optionsLabBill = {
					      title: 'Over all Lab Bill Analysis','width':1200,'height':600,is3D:true
					    };
					    var chartLabBill = new google.visualization.PieChart(document.getElementById('chart_div'));
					    chartLabBill.draw(dataLabBill, optionsLabBill);
					}
					else{
						$('#chart_div').hide();
					}
					if (bigArrayGpRadiologyBill.length > 1) {
					    var dataRadiologyBill = google.visualization.arrayToDataTable([['','']]);
					    var dataRadiologyBill = google.visualization.arrayToDataTable(bigArrayGpRadiologyBill);
					    var optionsRadiologyBill = {
					      title: 'Over all RadiologyBill Analysis','width':1200,'height':600,is3D:true
					    };
					    var chartRadiologyBill = new google.visualization.PieChart(document.getElementById('chart_div2'));
					    chartRadiologyBill.draw(dataRadiologyBill, optionsRadiologyBill);
					}
					else{
						$('#chart_div2').hide();
					}    
					if (bigArrayGpServiceBill.length > 1) {

					    var dataServiceBill = google.visualization.arrayToDataTable([['','']]);
					    var dataServiceBill = google.visualization.arrayToDataTable(bigArrayGpServiceBill);
					    var optionsServiceBill = {
					      title: 'Over all ServiceBill Analysis','width':1200,'height':600,is3D:true
					    };
					    var chartServiceBill = new google.visualization.PieChart(document.getElementById('chart_div3'));
					    chartServiceBill.draw(dataServiceBill, optionsServiceBill);
				    }
					else{
						$('#chart_div3').hide();
					}
					if (bigArrayGpProcedureBill.length > 1) {
					    var dataProcedureBill = google.visualization.arrayToDataTable([['','']]);
					    var dataProcedureBill = google.visualization.arrayToDataTable(bigArrayGpProcedureBill);
					    var optionsProcedureBill = {
					      title: 'Over all ProcedureBill Analysis','width':1200,'height':600,is3D:true
					    };
					    var chartProcedureBill = new google.visualization.PieChart(document.getElementById('chart_div4'));
					    chartProcedureBill.draw(dataProcedureBill, optionsProcedureBill);
					}
				    else{
						$('#chart_div4').hide();
					}

					if (bigArrayGpForensicBill.length > 1) {
					    var dataForensicBill = google.visualization.arrayToDataTable([['','']]);
					    var dataForensicBill = google.visualization.arrayToDataTable(bigArrayGpForensicBill);
					    var optionsForensicBill = {
					      title: 'Over all ForensicBill Analysis','width':1200,'height':600,is3D:true
					    };
					    var chartForensicBill = new google.visualization.PieChart(document.getElementById('chart_div5'));
					    chartForensicBill.draw(dataForensicBill, optionsForensicBill);
					}

					else{
						$('#chart_div5').hide();
					}


				    /*Chart for group*/
					if (bigArrayLabBill.length > 1) {
						var dataGpLabBill = google.visualization.arrayToDataTable(bigArrayLabBill);
						var optionsGpLabBill = {
					      title: 'Department wise Lab Bill Analysis','width':1200,'height':1000,isStacked: true
					    };
					    var chartGpLabBill = new google.visualization.BarChart(document.getElementById('chart_div6'));
					    chartGpLabBill.draw(dataGpLabBill, optionsGpLabBill);
					    $('#chart_div6').show();
					}
					else{
						$('#chart_div6').hide();
					}
					
					if (bigArrayRadiologyBill.length > 1){
						var dataGpRadiologyBill = google.visualization.arrayToDataTable(bigArrayRadiologyBill);
						var optionsGpRadiologyBill = {
					      title: 'Department wise RadiologyBill Analysis','width':1200,'height':1000,isStacked: true
					    };
					    var chartGpRadiologyBill = new google.visualization.BarChart(document.getElementById('chart_div7'));
					    chartGpRadiologyBill.draw(dataGpRadiologyBill, optionsGpRadiologyBill);
					    $('#chart_div7').show();
					}
					else{						 
						$('#chart_div7').hide();
					}
					

					if (bigArrayServiceBill.length > 1){
						var dataGpServiceBill = google.visualization.arrayToDataTable(bigArrayServiceBill);
						var optionsGpServiceBill = {
					      title: 'Department wise ServiceBill Analysis','width':1200,'height':1000,isStacked: true
					    };
					    var chartGpServiceBill = new google.visualization.BarChart(document.getElementById('chart_div8'));
					    chartGpServiceBill.draw(dataGpServiceBill, optionsGpServiceBill);
					    $('#chart_div8').show();
					}
					else{
						$('#chart_div8').hide();					
					}
					

					if (bigArrayProcedureBill.length > 1){
						var dataGpProcedureBill = google.visualization.arrayToDataTable(bigArrayProcedureBill);
						var optionsGpProcedureBill = {
					      title: 'Department wise ProcedureBill Analysis','width':1200,'height':1000,isStacked: true
					    };
					    var chartGpProcedureBill = new google.visualization.BarChart(document.getElementById('chart_div9'));
					    chartGpProcedureBill.draw(dataGpProcedureBill, optionsGpProcedureBill);
					    $('#chart_div9').show();
					}
					else{
						$('#chart_div9').hide();
					}
					

					if (bigArrayForensicBill.length > 1){
						var dataGpForensicBill = google.visualization.arrayToDataTable(bigArrayForensicBill);
						var optionsGpForensicBill = {
					      title: 'Department wise ForensicBill Analysis','width':1200,'height':1000,isStacked: true
					    };
						var chartGpForensicBill = new google.visualization.BarChart(document.getElementById('chart_div10'));
					    chartGpForensicBill.draw(dataGpForensicBill, optionsGpForensicBill);
					    $('#chart_div10').show();
					}
					else{											
						$('#chart_div10').hide();
					}
				}

				$('svg').css("max-width",$("#page-content").width()-50);//max-width for graph
				
				/*Scroll down code*/
				$('html, body').animate({
			        scrollTop: $("#chart_div").offset().top
			    }, 1000);

				$("#btnSaveExcel").show();

				/*function to save in to excel file*/
				window.saveFile = function saveFile () {

					var res = alasql('SELECT INTO XLSX("Sales-Report.xlsx",?) FROM ?',[opts,arr]);
				}
			}
		});
	});

	$("#btnSavePdf").click(function(){
		$.print("#printableArea");
	});
}
function clear(){
	$(".chartDiv,#btnSaveExcel,#btnSavePdf").hide();
	$("#toDate,#fromDate").val("");
	$("#txtFromDateError,#txtToDateError").text("");
	$("#fromDate,#toDate").removeClass("errorStyle");
}