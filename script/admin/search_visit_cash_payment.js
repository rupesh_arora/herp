$(document).ready(function(){
/* ****************************************************************************************************
 * File Name    :   serach_visit_cash_payment.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages visits data
 *************************************************************************************************** */
	loader(); 
	var otable;
	debugger;
	//varibale for time stammps
	var getTimeStamp;
	var getFromDateTimeStamp;
	var getToDateTimeStamp;
	
	otable = $('#tblCashPaymentVisit').dataTable( {
		"bFilter": true,
		"processing": true,
		"bAutoWidth" : false,		
		"sPaginationType":"full_numbers",
		aoColumnDefs: [
			{ 'bSortable': false, 'aTargets': [ 5 ] }
		]		
	});
	
	//This code is used for show the date picker in view visit and related with this screen 
	$('#txtFromDate').datepicker();
	$('#txtToDate').datepicker();
	
	$("#myCashModal #txtFromDate").click(function(){
		$(".datepicker").css({"z-index":"99999"});
	});
	
	$("#myCashModal #txtToDate").click(function(){
		$(".datepicker").css({"z-index":"99999"});
	});
	
	$("#myCashModal #txtToDate").change(function(){
		$("#txtToDate").datepicker("hide");
	});	
	$("#myCashModal #txtFromDate").change(function(){
		$("#txtFromDate").datepicker("hide");
	});
	
	//This code is used for cash payment 
	//Code for click on search icon of cash payment screen
	$("#myCashModal #btnReset").click(function(){
		$("#myCashModal #txtFromDate").val("");
		$("#myCashModal #txtToDate").val("");	
		$("#myCashModal #txtPatientId").val("");
		otable.fnClearTable();
		clear();
	});
	
	$("#myCashModal #btnBackToList").click(function(){
		$("#myCashModal #searchVisit").show();
		$("#myCashModal #viewDetails").hide();
	});

	$("#myCashModal #btnSearch").click(function(){
		var flag = "false";
		var firstName = $ ("#myCashModal #txtFirstName").val();
		var lastName = $ ("#myCashModal #txtLastName").val();
		var getFromDateTimeStamp = $ ("#myCashModal #txtFromDate").val();
		var getToDateTimeStamp = $ ("#myCashModal #txtToDate").val();
		var mobile = $ ("#myCashModal #txtMobile").val();
		var patientId = $ ("#myCashModal #txtPatientId").val();
		
		if($("#myCashModal #txtPatientId").val() != ""){
			if(patientId.length != 9){					
				flag = "true";
			}
			else{
				var patientId = $("#myCashModal #txtPatientId").val();
				var patientPrefix = patientId.substring(0, 3);
				patientId = patientId.replace ( /[^\d.]/g, '' ); 
				patientId = parseInt(patientId);
			}
		}
		else{
			patientPrefix = "";
		}
		if (flag == "true") {
			return false;
		}
		
		/*This hidden value and id coming from insurance payment*/
		if ($("#inurancePayment").val() =="insurancePayment") {
			var postData = {
				"operation":"searchCashModal",
				"getFromDateTimeStamp" : getFromDateTimeStamp,
				"getToDateTimeStamp" : getToDateTimeStamp,
				"firstName" : firstName,
				"lastName" : lastName,
				"mobile" : mobile,
				"patientPrefix" : patientPrefix,
				"patientId" : patientId,
				"inurancePayment" : "inurancePayment"
			};
		}
		/*This post data for cash payment */
		else{
			var postData = {
				"operation":"searchCashModal",
				"getFromDateTimeStamp" : getFromDateTimeStamp,
				"getToDateTimeStamp" : getToDateTimeStamp,
				"firstName" : firstName,
				"lastName" : lastName,
				"mobile" : mobile,
				"patientPrefix" : patientPrefix,
				"patientId" : patientId
			};
		}		
		$.ajax({					
			type: "POST",
			cache: false,
			url: "controllers/admin/view_visit.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				dataSet = JSON.parse(data);
				//var dataSet = jQuery.parseJSON(data);
				otable.fnClearTable();
				otable.fnDestroy();
				otable = $('#myCashModal #tblCashPaymentVisit').DataTable( {
					"sPaginationType":"full_numbers",
					"aaSorting": [],
					"bAutoWidth" : false,
					"fnDrawCallback": function ( oSettings ) {

						var aiRows = otable.fnGetNodes(); //Get all rows of data table

					 	if(aiRows.length == 0){
					 		return false;
					 	}
					 	for (var j=0,c=aiRows.length; j<c; j++) {
					 		var visitType = $(aiRows[j]).find('td:eq(1)').html();
					 		if(visitType == "Emergency Case"){
					 			$(aiRows[j]).css('background-color','#ff6666');
					 		}
					 	}
						
						$('#myCashModal #tblCashPaymentVisit tbody tr').on( 'dblclick', function () {

							if ( $(this).hasClass('selected') ) {
							  $(this).removeClass('selected');
							}
							else {
							  otable.$('tr.selected').removeClass('selected');
							   $(this).addClass('selected');
							}
							
							var mData = otable.fnGetData(this); // get datarow
							if (null != mData)  // null if we clicked on title row
							{
								//now aData[0] - 1st column(count_id), aData[1] -2nd, etc. 								
								var visitId = mData["id"];
								var patientPrefix = mData["visit_prefix"];
									
								var visitIdLength = visitId.length;
								for (var i=0;i<6-visitIdLength;i++) {
									visitId = "0"+visitId;
								}
								visitId = patientPrefix+visitId;
								$('#txtVisit').val(visitId);
								$("#txtName").val(mData.name);					
							}							
							
							$("#btnSelect").click();
							$(".close").click();
										
						} );
						
						$("#myCashModal .visitDetails").on('click',function(){
							var id = $(this).attr('data-id');
							var postData = {
								"operation":"visitDetailModal",
								"id":id
							}
							
							$.ajax({					
								type: "POST",
								cache: false,
								url: "controllers/admin/view_visit.php",
								datatype:"json",
								data: postData,
								
								success: function(data) {
									if(data != "0" && data != ""){
										var parseData = jQuery.parseJSON(data);
														
										for (var i=0;i<parseData.length;i++) {

											$("#myCashModal #lblDepartementType").text(parseData[i].type);
											$("#myCashModal #lblName").text(parseData[i].name);
											$("#myCashModal #lblServiceName").text(parseData[i].service_name);
											$("#myCashModal #lblVisitDate").text(parseData[i].visit_date);
											if( parseData[i].triage_status== "UnTaken"){
												$("#myCashModal #lblTriageStatus").text("Not Taken");
											}
											else{
												$("#myCashModal #lblTriageStatus").text("Taken");
											}
											if( parseData[i].service_status == null){
												$("#myCashModal #lblServiceStatus").text("N/A");
											}
											else{
												$("#myCashModal #lblServiceStatus").text(parseData[i].service_status);
											}
											if( parseData[i].forensic_status == null){
												$("#myCashModal #lblForensicStatus").text("N/A");
											}
											else{
												$("#myCashModal #lblForensicStatus").text(parseData[i].forensic_status);
											}
											if( parseData[i].lab_status == null){
												$("#myCashModal #lblLabStatus").text("N/A");
											}
											else{
												$("#myCashModal #lblLabStatus").text(parseData[i].lab_status);
											}
											if( parseData[i].pharmacy_status == null){
												$("#myCashModal #lblPharmacyStatus").text("N/A");
											}
											else{
												$("#myCashModal #lblPharmacyStatus").text(parseData[i].pharmacy_status);
											}
											if( parseData[i].procedure_status == null){
												$("#myCashModal #lblProcedureStatus").text("N/A");
											}
											else{
												$("#myCashModal #lblProcedureStatus").text(parseData[i].procedure_status);
											}
											if( parseData[i].radiology_status == null){
												$("#myCashModal #lblRadiologyStatus").text("N/A");
											}
											else{
												$("#myCashModal #lblRadiologyStatus").text(parseData[i].radiology_status);
											}
																					
											var visitId = parseInt(parseData[i].id);
											var patientPrefix = parseData[i].prefix;
												
											var visitIdLength = visitId.toString().length;
											for (var j=0;j<6-visitIdLength;j++) {
												visitId = "0"+visitId;
											}
											visitid = patientPrefix+visitId;
											$("#myCashModal #lblVisitId").text(visitid);

										} 
										$("#myCashModal #searchVisit").hide();
										$("#myCashModal #viewDetails").show();
									}
								},
								error:function() {
									$('#messageMyModalLabel').text("Error");
									$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
									$('#messagemyModal').modal();
								}
							});
						});
					},
					"aaData": dataSet,
					"aoColumns": [
						{  
							"mData": function (o) { 	
							var visitId = o["id"];
							var patientPrefix = o["visit_prefix"];
								
							var visitIdLength = visitId.length;
							for (var i=0;i<6-visitIdLength;i++) {
								visitId = "0"+visitId;
							}
							visitId = patientPrefix+visitId;
							return visitId; 
							}
						},
						/* { "mData": "id"}, */
						{ "mData": "type" },
						{ "mData": "name" },
						{ "mData": "visit_date" },
						{
						  "mData": function (o) { 				
								var data = o;
								var id = data["id"];
								var visitType = o["type"];
								if(visitType == "Emergency Case"){
									return "<i class='visitDetails fa fa-eye' id='btnView' visitType="+visitType+" title='View' data-id="+id+" "+ "></i>"; 
								}
								else{	
									return "<i class='visitDetails fa fa-eye' id='btnView' title='View' data-id="+id+" "+ "></i>"; 
								}
							}
						},
					],
					aoColumnDefs: [
						{ 'bSortable': false, 'aTargets': [ 3 ] },
						{ 'bSortable': false, 'aTargets': [ 4 ] }
					]
				} );				
				
			},
			error:function() {
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});
	});

	/*Click function to get today data*/
	$ ("#myCashModal #txtFromDate").val(todayDate());
	$ ("#myCashModal #txtToDate").val(todayDate());
	$ ("#myCashModal #btnSearch").click();
	$ ("#myCashModal #txtFromDate").val("");
	$ ("#myCashModal #txtToDate").val("");

	$("#myCashModal #btnBackToList").click(function(){
		$("#myCashModal #searchVisit").show();
		$("#myCashModal #viewDetails").hide();
	});
});