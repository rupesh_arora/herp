var subTypeTable;
$(document).ready(function(){
    loader();
    debugger;
	
	/*Hide add ward by default functionality*/
	$("#advanced-wizard").hide();
    $("#subTypeList").addClass('list');    
    $("#tabsubTypeList").addClass('tab-list-add');
	

    /*$("#subTypeTable").dataTable();*/		
	/*Click for add the sub Type*/
    $("#tabAddsubType").click(function() {
        showAddTab();        
        clear();        
    });

    $('#txtName').keyup(function() {
         if($('#txtName').val() != '') {
            $('#txtNameError').text('');
            $('#txtName').removeClass("errorStyle"); 
        }
    });
	
	//Click function for show the sub_type lists
    $("#tabsubTypeList").click(function() {
        clear();
        tabsubTypeList();
    });

    $('#btnSubmit').click(function() {
        var flag = false;

        if($('#txtName').val() == '') {
            $('#txtNameError').text('Please enter name');
            $('#txtName').focus();
            $('#txtName').addClass("errorStyle"); 
            flag = true;
        }
        if(flag == true) {
            return false;
        }

        var name = $('#txtName').val().trim();
        var description = $('#txtDescription').val().trim();

        var postData = {
            name : name,
            description : description,
            operation : "saveSubType"
        }


        $.ajax(
            {                   
            type: "POST",
            cache: false,
            url: "controllers/admin/sub_type.php",
            datatype:"json",
            data: postData,
            
            success: function(data) {
                if(data != "0" && data != ""){
                    callSuccessPopUp('Success','Saved successfully!!!');
                     tabsubTypeList();
                    clear();
                }
                else {
                    $('#txtNameError').text('Sub type is already exist');
                    $('#txtName').focus();
                    $('#txtName').addClass('errorStyle');
                }
            },
            error: function(){

            }
        });
    });

    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
        //Datatable code 
        subTypeTable = $('#subTypeTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "autoWidth": false,
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = subTypeTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var name = mData["name"];                      
                        var description = mData["description"];
                        editClick(id,name,description);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = subTypeTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/sub_type.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "name"
                }, {
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [1,2]
            }]

        });
    }

    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            subTypeTable.fnClearTable();
            subTypeTable.fnDestroy();
            subTypeTable = "";
            subTypeTable = $('#subTypeTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "autoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = subTypeTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var name = mData["name"];                      
                            var description = mData["description"];
                            restoreClick(id,name,description);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/sub_type.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "name"
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1,2]
                }]
            });
        } else { // show active data on unchecked   
            subTypeTable.fnClearTable();
            subTypeTable.fnDestroy();
            subTypeTable = "";
            subTypeTable = $('#subTypeTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "autoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = subTypeTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var name = mData["name"];                      
                            var description = mData["description"];
                            editClick(id,name,description);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = subTypeTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/sub_type.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "name"
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [1,2]
                }]
            });
        }
    });


    $('#btnReset').click(function(){
        clear();
    });
});

function editClick(id,name,description) {
    
    showAddTab();
    $("#uploadFile").hide();
    $("#btnReset").hide();
    $("#btnSubmit").hide();
    $('#btnUpdate').show();
    $('#tabAddsubType').html("+Update sub Type");
    $('#txtName').focus();
   
    
    $("#btnUpdate").removeAttr("style");
    
    $("#txtName").removeClass("errorStyle");
    $("#txtNameError").text("");

    
    $('#txtName').val(name);
    $('#txtDescription').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
    //validation
   
    
    $(" #btnUpdate").click(function() { // click update button
        var flag = false;
        if ($("#txtName").val()== '') {
            $(" #txtNameError").text("Please enter name");
            $("#txtName").focus();
            $("#txtName").addClass("errorStyle");
            flag = true;
        }
        
        if(flag == true) {
            return false;
        }

        var name = $("#txtName").val().trim();
        var description = $("#txtDescription").val().trim();
        description = description.replace(/'/g, "&#39");
        var id = $('#selectedRow').val();
        
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
        var postData = {
            "operation": "update",
            "name": name,
            "description": description,
            "id": id
        }
        $.ajax( //ajax call for update data
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/sub_type.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('#myModal').modal('hide');
                        $('.close-confirm').click();
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Sub type updated successfully!!!");
                        $('#messagemyModal').modal();
                        subTypeTable.fnReloadAjax();
                        tabsubTypeList();
                        clear();
                    }
                    else {                
                        $("#txtNameError").text("Sub type is already exist");
                        $("#txtName").focus();               
                        $("#txtName").addClass('errorStyle');
                    }
                },
                error: function() {
                    $('.close-confirm').click();
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                    $('#messagemyModal').modal();
                }
            }); // end of ajax
        });
    });
} // end update button
function deleteClick(id) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete sub type");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteSubType("' + type + '","'+id+'","","");');
} // end click fucntion

function restoreClick(id,name,description) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore sub type");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteSubType("' + type + '","'+id+'","'+name+'","'+description+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteSubType(type,id,name,description) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/sub_type.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Sub type deleted successfully!!!");
                    $('#messagemyModal').modal();
                    subTypeTable.fnReloadAjax();
                } 
                else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This sub type is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/sub_type.php",
            data: {
                "operation": "restore",
                "name":name,
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Sub type restored successfully!!!");
                    $('#messagemyModal').modal();
                    subTypeTable.fnReloadAjax();
                }
                else {
                    callSuccessPopUp('Sorry','Sub type already exist you can not restore !!!');
                }
            },
            error: function() {             
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary unavailable to respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}

function tabsubTypeList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
    $('#inactive-checkbox-tick').prop('checked', false).change();
    clear();

    $("#tabAddsubType").removeClass('tab-detail-add');
    $("#tabAddsubType").addClass('tab-detail-remove');
    $("#tabsubTypeList").removeClass('tab-list-remove');    
    $("#tabsubTypeList").addClass('tab-list-add');
    $("#subTypeList").addClass('list');


    $("#uploadFile").show();
    $("#btnReset").show();
    $("#btnSubmit").show();
    $('#btnUpdate').hide();
    $('#tabAddsubType').html("+Add Sub Type");
}

function showAddTab(){
    $("#advanced-wizard").show();
    $(".blackborder").hide();

    $("#tabAddsubType").addClass('tab-detail-add');
    $("#tabAddsubType").removeClass('tab-detail-remove');
    $("#tabsubTypeList").removeClass('tab-list-add');
    $("#tabsubTypeList").addClass('tab-list-remove');
    $("#subTypeList").addClass('list');
    $("#txtName").focus();

}

function clear() {
    $('#txtName').val('');
    $('#txtNameError').text('');
    $('#txtDescription').val('')
    $('#txtName').removeClass("errorStyle");
    $('#txtName').focus();
}