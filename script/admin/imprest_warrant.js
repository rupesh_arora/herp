var imprestWarrantTable;
var lineItemsTable;
var totalamount =0;
var mainAmount = 0;
$(document).ready(function(){
    loader();
    totalamount =0;
    mainAmount = 0;
    debugger;
    bindLedger();
	
    imprestWarrantTable = $('#imprestWarrantTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "bAutoWidth":false,
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = imprestWarrantTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var imprestNo = mData["imprest_request_id"];
                        var staffId = mData["staff_id"];
                        var ledger = mData["ledger_id"];
                        var name = mData["name"];
                        var amount = mData["amount"];
                        var natureOfDuty = mData["nature_of_duty"];
                        var EstDays = mData['est_day'];
                        var startDate = mData['assignment_start'];
                        var surrendorDate = mData['assignment_end'];
                        editClick(id,imprestNo,staffId,ledger,name,amount,natureOfDuty,EstDays,startDate,surrendorDate);
                    }
                });
                //perform view event
                $('.imprestWarrantview').unbind();
                $('.imprestWarrantview').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = imprestWarrantTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var imprestNo = mData["imprest_request_id"];
                        var staffId = mData["staff_id"];
                        var ledger = mData["ledger_id"];
                        var name = mData["name"];
                        var amount = mData["amount"];
                        var natureOfDuty = mData["nature_of_duty"];
                        var EstDays = mData['est_day'];
                        var startDate = mData['assignment_start'];
                        var surrendorDate = mData['assignment_end'];
                        var ledgerName = mData['ledger_name']; 
                        viewClick(id,imprestNo,staffId,ledger,name,amount,natureOfDuty,EstDays,startDate,surrendorDate,ledgerName);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/imprest_warrant.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "name"
                },
                {
                    "mData": "nature_of_duty"
                },
                {
                    "mData": "assignment_start"
                },
                {
                    "mData": "assignment_end"
                },        
                {
                
                    "mData": function(o) {
                        var data = o;
                        var status = o['surrender_status'];
                        if(status == 'no'){
                            return "<i class=' fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            "<i class='fa fa-eye imprestWarrantview' style='font-size: 17px; cursor:pointer;'' title='view' value='view'></i>";
                        }
                        else{
                            return "<i class='fa fa-eye imprestWarrantview' style='font-size: 17px; cursor:pointer;'' title='view' value='view'></i>";
                        }
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [4]
            }]

        });
    lineItemsTable = $('#lineItemsTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth":false,
        "fnDrawCallback": function(oSettings) {
        },
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [3]
        }]
    });

});
function editClick(id,imprestNo,staffId,ledger,name,amount,natureOfDuty,EstDays,startDate,surrendorDate) {
    $('.updateMain').removeAttr('style');
    $('#imprestWarrantButtons').removeAttr("style");
    $(".blackborder").hide();
    $('.tblLineItems').hide();
    $('#btnBackToList').removeAttr("style");
     $('#mainPopUp').addClass("activeSelf");
        $('#lineItemsPopUp').removeClass("activeSelf");
        $("#mainPopUp").css("background-color", "rgb(115, 115, 115)");
        $(".updateMain").show();
        $('.tblLineItems').hide();
        removeErrorMessage();
        
        $('#txtImprestNo').val(imprestNo);
        $('#txtStaffId').val(staffId);
        $('#selLedger').val(ledger);
        $('#txtName').val(name);
        $('#txtAmount').val(amount);
        mainAmount = amount
        $('#txtNatureOfDuty').val(natureOfDuty);
        $('#txtEstDays').val(EstDays);
        $('#txtStartDate').val(startDate);
        $('#txtSurrenderDate').val(surrendorDate);
        $('#selectedRow').val(id);
    

    $('#BackToList').click(function() {
        $(".blackborder").show();
        $(".updateMain").hide();
        $('.tblLineItems').hide();
        $('#imprestWarrantButtons').hide();
        $('#btnBackToList').hide();
    });

    
    $('#mainPopUp').click(function() {
        $('#mainPopUp').addClass("activeSelf");
        $('#lineItemsPopUp').removeClass("activeSelf");
        $("#mainPopUp").css("background-color", "rgb(115, 115, 115)");
        $(".updateMain").show();
        $('.tblLineItems').hide();
        removeErrorMessage();
        
        $('#txtImprestNo').val(imprestNo);
        $('#txtStaffId').val(staffId);
        $('#selLedger').val(ledger);
        $('#txtName').val(name);
        $('#txtAmount').val(amount);
        mainAmount = amount
        $('#txtNatureOfDuty').val(natureOfDuty);
        $('#txtEstDays').val(EstDays);
        $('#txtStartDate').val(startDate);
        $('#txtSurrenderDate').val(surrendorDate);
        $('#selectedRow').val(id);
        //validation
        //remove validation style
        // edit data function for update 
        
        
        
        
    });

    $('#lineItemsPopUp').click(function() {
        $('#lineItemsPopUp').addClass("activeSelf");
        $('#mainPopUp').removeClass("activeSelf");
        $('.tblLineItems').removeAttr('style');
        $('.tblLineItems').show();
        $(".updateMain").hide();

        loadLineItemTable();
        
        $('#btnAdd').unbind();
        $('#btnAdd').click(function() {
            var currentData = lineItemsTable.fnAddData(['<input type="text"  id="txtdescription" placeholder="Enter description">',
                '<input type="number"  id="txtLineAmount" placeholder = "Enter amount">',
                '<select id ="selChartOfAccount"><option value="">--Select--</option></select>',
                '<input type="button" id ="newButtonColor" class="btnEnabled" onclick="rowDelete($(this));" value="Remove">'
            ]);
            var getCurrDataRow = lineItemsTable.fnSettings().aoData[ currentData ].nTr;
            bindGLAccount($(getCurrDataRow).find('td:eq(2)').children()[0],'');
        });

        $('#btnSave').unbind();
        $('#btnSave').click(function() {
        	var checkTotal = totalamount;
            var updateLineItems = [];
            var flag = false;
            var items = $("#lineItemsTable tbody tr");
            var rows = $(lineItemsTable).find('tbody').find('.btnEnabled');

            if($('#lineItemsTable tr').find('td').text() == "No data available in table"){
                callSuccessPopUp("Sorry","Please insert row on datatable");
                return false;
            }
            
            $.each(rows,function(i,v){
                var obj = { };
                obj.description = $($(rows[i]).parent().parent().find('input[type="text"]')).val();
                var amount = $($(rows[i]).parent().parent().find('input[type="number"]')).val();
                obj.amount = amount;
                checkTotal =checkTotal +parseFloat(amount);
                if(parseFloat(checkTotal) > parseFloat(mainAmount)){
                    callSuccessPopUp("Sorry","Line item amount should be less than main amount");
                    flag = true;
                    return false;
                }
                if(obj.description == ''){
                    callSuccessPopUp("Alert","Please enter line item description");
                    flag = true;
                    return false;
                }
                if(obj.amount == ''){
                    callSuccessPopUp("Alert","Please enter line item amount");
                    flag = true;
                    return false;
                }                
                obj.glLedger = $($(rows[i]).parent().parent().find('select')).val();
                if(obj.glLedger == ''){
                    callSuccessPopUp("Alert","Please enter line item GL account");
                    flag = true;
                    return false;
                }
                updateLineItems.push(obj);
            });
            

            if($('#txtdescription').val() == '') {
                flag = true;
            }
            if($('#txtLineAmount').val() == '') {
                flag = true;
            }
            if($('#selChartOfAccount').val() == '') {
                flag = true;
            }
            if($('#selLedger').val() == '') {
                callSuccessPopUp("Alert","Please choose ledger");
                flag = true;
            }
            if(flag == true){
                return false;
            }
            var ledger = $('#selLedger').val();
            var imprestRequestId = $('#txtImprestNo').val();
            var imprestWarrantId = $('#selectedRow').val();
            var glAccount = $('#selChartOfAccount').val();
            var description = $('#txtdescription').val();
            var lineAmount = $('#txtLineAmount').val();

            var addData = {
                "updateLineItems" : JSON.stringify(updateLineItems),
                "ledger" : ledger,
                "imprestRequestId" : imprestRequestId,
                "imprestWarrantId" : imprestWarrantId,
                "glAccount" : glAccount,
                "description" : description,
                "lineAmount" : lineAmount,
                "operation" : "saveLineItems"
            }

            $.ajax(
            {                   
                type: "POST",
                cache: false,
                url: "controllers/admin/imprest_warrant.php",
                datatype:"json",
                data: addData,
                
                success: function(data) {
                    if(data != "0" && data != ""){
                        callSuccessPopUp('Success','Saved successfully!!!');
                        loadLineItemTable();
                    }
                },
                error: function(){

                }
            });
        });


    });
    $('#mainPopUp').addClass("activeSelf");
} // end update button

function loadLineItemTable(){
	var addData = {
        "warrantId" : $('#selectedRow').val(),
        "operation" : "showWarrentDetail"
    }

    $.ajax(
    {                   
        type: "POST",
        cache: false,
        url: "controllers/admin/imprest_warrant.php",
        datatype:"json",
        data: addData,
        
        success: function(data) {
            if(data != "0" && data != ""){
            	 var parseData = jQuery.parseJSON(data);
            	 lineItemsTable.fnClearTable();
            	 totalamount =0;
            	  $.each(parseData,function(i,v){
                 	var currentData = lineItemsTable.fnAddData([v.description,v.initial_amount,v.account_name,'']);
                 	totalamount =totalamount +parseFloat(v.initial_amount);
                 });
            }
        },
        error: function(){

        }
    });
}
function viewClick(id,imprestNo,staffId,ledger,name,amount,natureOfDuty,EstDays,startDate,surrendorDate,ledgerName) {
    $('#myImprestWarrantModel').modal("show");
    $('#imprestWarrantModelLabel').text("Imprest Warrant Details");

    var staffIdLength = staffId.length;
    for (var j=0;j<6-staffIdLength;j++) {
        staffId = "0"+staffId;
    }
    staffId = staffPrefix+staffId;

    $('#lblStaffId').text(staffId);
    $('#lblName').text(name);
    $('#lblLedger').text(ledgerName);
    $('#lblImprestRequestId').text(imprestNo);
    $('#lblNatureOfDuty').text(natureOfDuty);
    $('#lblEstDay').text(EstDays);
    $('#lblAmount').text(amount);
    $('#lblStartDate').text(startDate);
    $('#lblEndDate').text(surrendorDate);

} 
function bindLedger() {     //  function for loading ledger
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/payment_mode.php",
        data: {
            "operation": "showLedger"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option data-value='" + parseData[i].value + "' value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selLedger').html(option);
            }
        },
        error: function() {}
    });
}

function rowDelete(currInst) {
    var row = currInst.closest("tr").get(0);

    lineItemsTable.fnDeleteRow(row);
    /*row.remove();*/
}
function clear() {
    $('#selChartOfAccount').val('');
    $('#txtdescription').val('');
    $('#txtLineAmount').val('');
}