$(document).ready(function() {
loader();

	$("#form_dept").hide();
	$("#visit_type_list").css({
            "background-color": "#fff"
        });
		$("#tab_visit_type").css({
            "background-color": "#0c71c8",
            "color": "#fff"
        });
		
	//Click function open tab for add visit type
    $("#tab_add_lab_visit").click(function() {
        $("#form_dept").show();
        $(".blackborder").hide();
		clear();
        $("#tab_add_lab_visit").css({
            "background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
            "color": "#fff"
        });
        $("#tab_visit_type").css({
            "background": "#fff",
            "color": "#000"
        });
        $("#visit_type_list").css({
            "background": "#fff"
        });
		$("#txtVisitTypeError").text("");
        $("#txtVisitType").removeClass("errorStyle");
		$('#txtVisitType').focus();
    });
	
	//Click function for open tab for show the list of visit type
    $("#tab_visit_type").click(function() {
		$('#inactive-checkbox-tick').prop('checked', false).change();
		//calling function for show the list
		tabLabType();        
    });
	
	
	//Code for show the data table 
	if($('.inactive-checkbox').not(':checked')){
    	//Datatable code
		otable = $('#visitTypedatatable').dataTable( {
			"bFilter": true,
			"processing": true,
			"sPaginationType":"full_numbers",
			"fnDrawCallback": function ( oSettings ) {
				
				$('.update').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData = $('#visitTypedatatable').dataTable().fnGetData(data);
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						var type = mData["type"];
						var description = mData["description"];
						editClick(id,type,description);
       
					}
				});
				$('.delete').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData =  $('#visitTypedatatable').dataTable().fnGetData(data);
					
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						deleteClick(id);
					}
				});
			},
			
			"sAjaxSource":"controllers/admin/visit_type.php",
			"fnServerParams": function ( aoData ) {
			  aoData.push( { "name": "operation", "value": "show" });
			},
			"aoColumns": [
				{ "mData": "type" },
				{ "mData": "description" },	
				{  
					"mData": function (o) { 
					
					return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
					   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
					   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
					   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
					   "  data-original-title='Delete'></i>"; 
					}
				},
			],
			aoColumnDefs: [
			   { 'bSortable': false, 'aTargets': [ 1 ] },
			   { 'bSortable': false, 'aTargets': [ 2 ] }
			]
		} );
    }
    $('.inactive-checkbox').change(function() {
    	if($('.inactive-checkbox').is(":checked")){
	    	otable.fnClearTable();
	    	otable.fnDestroy();
	    	//Datatable code
			otable = $('#visitTypedatatable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					
					$('.restore').on('click',function(){
						var data=$(this).parents('tr')[0];
						var mData =  $('#visitTypedatatable').dataTable().fnGetData(data);
					
						if (null != mData)  // null if we clicked on title row
						{
							var id = mData["id"];
							restoreClick(id);
						}
						
					});
				},
				
				"sAjaxSource":"controllers/admin/visit_type.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "showInActive" });
				},
				"aoColumns": [
					{ "mData": "type" },
					{ "mData": "description" },
					{  
						"mData": function (o) { 
						
						return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>'; }
					},	
				],
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 1 ] },
				   { 'bSortable': false, 'aTargets': [ 2 ] }
			    ]
			} );
		}
		else if($('.inactive-checkbox').not(':checked')){
			otable.fnClearTable();
	    	otable.fnDestroy();
	    	//Datatable code
			otable = $('#visitTypedatatable').dataTable( {
				"bFilter": true,
				"processing": true,
				"sPaginationType":"full_numbers",
				"fnDrawCallback": function ( oSettings ) {
					
				$('.update').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData = $('#visitTypedatatable').dataTable().fnGetData(data);
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						var type = mData["type"];
						var description = mData["description"];
						editClick(id,type,description);
       
					}
				});
				$('.delete').on('click',function(){
					var data=$(this).parents('tr')[0];
					var mData =  $('#visitTypedatatable').dataTable().fnGetData(data);
					
					if (null != mData)  // null if we clicked on title row
					{
						var id = mData["id"];
						deleteClick(id);
					}
				});
				},
				
				"sAjaxSource":"controllers/admin/visit_type.php",
				"fnServerParams": function ( aoData ) {
				  aoData.push( { "name": "operation", "value": "show" });
				},
				"aoColumns": [
					{ "mData": "type" },
					{ "mData": "description" },	
					{  
						"mData": function (o) { 
						
						return "<i class='ui-tooltip fa fa-pencil update' title='Edit'"+
						   "  style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>"+
						   " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' "+
						   "  style='font-size: 22px; color:#a94442; cursor:pointer;' "+
						   "  data-original-title='Delete'></i>"; 
						}
					},
				],
				aoColumnDefs: [
				   { 'bSortable': false, 'aTargets': [ 1 ] },
				   { 'bSortable': false, 'aTargets': [ 2 ] }
			    ]
			} );
		}
    });
	
	//Function for add the visit type
    $("#btnSave").click(function() {
		
		var flag = "false";
		
		$("#txtVisitType").val($("#txtVisitType").val().trim());
		
        if ($("#txtVisitType").val() == '') {
			$('#txtVisitType').focus();
            $("#txtVisitTypeError").text("Please enter visit type");
            $("#txtVisitType").addClass("errorStyle");
            flag = "true";
        }
		
		if(flag == "true"){			
        return false;
		}
		
		//ajax call
		
		var visitType = $("#txtVisitType").val().toLowerCase();
		var description = $("#txtDescription").val();
		description = description.replace(/'/g, "&#39");
		var postData = {
			"operation":"save",
			"visitType":visitType,
			"description":description
		}
		
		//Ajax call
		$.ajax(
			{					
			type: "POST",
			cache: false,
			url: "controllers/admin/visit_type.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Visit type saved successfully!!!");
					$('#messagemyModal').modal();
					$('#inactive-checkbox-tick').prop('checked', false).change();
						tabLabType();
				}
				else{
					$("#txtVisitTypeError").text("Visit is already exists");
					$('#txtVisitType').focus();
					$("#txtVisitType").addClass("errorStyle");
				}
				
			},
			error:function() {
				alert('error');
			}
		});
		
    });

    //keyup functionality
    $("#txtVisitType").keyup(function() {
        if ($("#txtVisitType").val() != '') {
            $("#txtVisitTypeError").text("");
            $("#txtVisitType").removeClass("errorStyle");
        } 
    });   
	
});

//Edit function for update the data
function editClick(id,type,description){	
	$('#myModalLabel').text("Update Visit Type");
	$('.modal-body').html($("#popUPFormVisitType").html()).show();
	$("#myModal #btnUpdate").removeAttr("style");
	$('#myModal #txtVisitType').focus();
	$("#myModal #txtVisitTypeError").text("");
    $("#myModal #txtVisitType").removeClass("errorStyle");
	$('#myModal #txtVisitType').val(type);
	$('#myModal #txtDescription').val(description.replace(/&#39/g, "'"));
	$('#selectedRow').val(id);
	$('#myModal').modal();
	
	$("#myModal #btnUpdate").click(function(){
		var flag = "false";
		
		$("#myModal #txtVisitType").val($("#myModal #txtVisitType").val().trim());
		
		if ($("#myModal #txtVisitType").val() == '') {
			$('#myModal #txtVisitType').focus();
			$("#myModal #txtVisitTypeError").text("Please enter visit type");
			$("#myModal #txtVisitType").addClass("errorStyle");
			flag = "true";
		}
		if(flag == "true"){			
		return false;
		}
		
		var visitType = $("#myModal #txtVisitType").val().toLowerCase();
		var description = $(" #myModal #txtDescription").val();
		description = description.replace(/'/g, "&#39");
		var id = $('#selectedRow').val();
		
		
		$.ajax({
			type: "POST",
			cache: "false",
			url: "controllers/admin/visit_type.php",
			data :{            
				"operation" : "update",
				"id" : id,
				"visitType":visitType,
				"description":description
			},
			success: function(data) {	
				if(data != "0" && data != ""){
					$('#myModal').modal('toggle');
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').text("Visit type updated successfully!!!");
					$("#visitTypedatatable").dataTable().fnReloadAjax();
					$('#messagemyModal').modal();
					clear();
				}
				else{
					$("#myModal #txtVisitTypeError").text("Lab is already exists");
					$('#myModal #txtVisitType').focus();
					$("#myModal #txtVisitType").addClass("errorStyle");
					
				}
			},
			error:function()
			{
				alert('error');
				  
			}
		});
	});
	
	$('#myModal #txtVisitType').keyup(function() {
        if ($("#myModal #txtVisitType").val() != '') {
            $("#myModal #txtVisitTypeError").text("");
            $("#myModal #txtVisitType").removeClass("errorStyle");
        }
    });
}

//Delete function for delete the data
function deleteClick(id){
	
	$('#selectedRow').val(id);
	$('#confirmMyModalLabel').text("Delete Visit Type");
	$('.modal-body').text("Are you sure that you want to delete this?");
	$('#confirmmyModal').modal(); 
	
	var type="delete";
	$('#confirm').attr('onclick','deleteVisitType("'+type+'");');
	
}

//Function for restore the data
function restoreClick(id){	
	$('#selectedRow').val(id);
	
	$('#confirmMyModalLabel').text("Restore Visit Type");
	$('.modal-body').text("Are you sure that you want to restore this?");
	$('#confirmmyModal').modal();
	
	var type="restore";
	$('#confirm').attr('onclick','deleteVisitType("'+type+'");');

}

function deleteVisitType(type){
	if(type=="delete"){
		var id = $('#selectedRow').val();
				
			//Ajax call	
			$.ajax({
				type: "POST",
				cache: "false",
				url: "controllers/admin/visit_type.php",
				data :{            
					"operation" : "delete",
					"id" : id
				},
				success: function(data) {	
					if(data != "0" && data != ""){
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Visit type deleted successfully!!!");
						$("#visitTypedatatable").dataTable().fnReloadAjax();	
						$('#messagemyModal').modal();				 
					}
					else{
						$('#messageMyModalLabel').text("Sorry");
						$('.modal-body').text("This lab is used , so you can not delete!!!");
						$('#messagemyModal').modal();
					}
				},
				error:function()
				{
					alert('error');
					  
				}
			});
	}
	else{
		var id = $('#selectedRow').val();
			
			$.ajax({
				type: "POST",
				cache: "false",
				url: "controllers/admin/visit_type.php",
				data :{            
					"operation" : "restore",
					"id" : id
				},
				success: function(data) {	
					if(data != "0" && data != ""){
						$('.modal-body').text('');
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Visit type restored successfully!!!");
						$("#visitTypedatatable").dataTable().fnReloadAjax();
						$('#messagemyModal').modal();
					}			
				},
				error:function()
				{
					alert('error');
					  
				}
			});
	}
}

//function for  show the lab type
function tabLabType(){
	$("#form_dept").hide();
	$(".blackborder").show();
	$("#visitTypedatatable").dataTable().fnReloadAjax();
	$("#tab_add_lab_visit").css({
		"background": "#fff",
		"color": "#000"
	});
	$("#tab_visit_type").css({
		"background": "linear-gradient(rgb(30, 106, 217), rgb(146, 219, 246)) rgb(12, 113, 200)",
		"color": "#fff"
	});
	$("#visit_type_list").css({
		"background": "#fff"
	});
	clear();
}

// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) {  
		 $(".close").click();
    }
});

//Click function for reset the button 
$("#btnReset").click(function(){
	$("#txtVisitType").removeClass("errorStyle");
	clear();
	$("#txtVisitType").focus();
});

//function for clear 
function clear(){
	$('#txtVisitType').val("");
	$('#txtVisitTypeError').text("");
	$('#txtDescription').val("");
}
//# sourceURL=filename.js