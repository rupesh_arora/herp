var imprestTable = '';
$(document).ready(function() {
	debugger;
	$(".input-datepicker").datepicker({ // date picker function
		autoclose: true
	});
	var arr=[];
	removeErrorMessage();//call from common.js
	imprestTable = $('#imprestTable').dataTable({
        "bFilter": true,
        "processing": true,
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "aaSorting": []
    });

    $("#viewReport").on('click',function(){
    	var flag = false;
    	if(validTextField("#selImprestType","#selImprestTypeError","Please select imprest type") == true) {
			flag = true;
		}
		if(flag == true) {
			return false;
		}
		var toDate = $("#toDate").val().trim();
		var fromDate = $("#fromDate").val().trim();
		var imprestType = $("#selImprestType").val();

    	var postData = {
			"operation" : "showData",
			'toDate' : toDate,
			'fromDate' : fromDate
		}
		imprestTable.fnClearTable();
	    imprestTable.fnDestroy();

	    imprestTable = $('#imprestTable').dataTable({
	        "bFilter": true,
	        "processing": true,
	        "sPaginationType": "full_numbers",
	        "bAutoWidth": false,
	        "aaSorting": [],
	        "sAjaxSource": "controllers/admin/imprest_warrant.php",
	        "fnServerParams": function(aoData) {
	        	aoData.push({
	                "name": "fromDate",
	                "value": fromDate
	            });
	            aoData.push({
	                "name": "toDate",
	                "value": toDate
	            });
	        	if (imprestType == 1) {
	        		aoData.push({
		                "name": "operation",
		                "value": "show"
		            });
	        	}
	        	else if (imprestType == 2) {
	        		aoData.push({
		                "name": "operation",
		                "value": "showNotDisbursed"
		            });
	        	}
	        	else if (imprestType == 3) {
	        		aoData.push({
		                "name": "operation",
		                "value": "showSurrender"
		            });
	        	}
	        },
	        "aoColumns": [
	        	{
	                "mData": "imprest_date"
	            },
	            {
                    "mData": function(o){
                        var imprestId = o["imprest_request_id"];
                            
                        var visitIdLength = imprestId.length;
                        for (var i=0;i<6-visitIdLength;i++) {
                            imprestId = "0"+imprestId;
                        }
                        imprestId = imprestPrefix+imprestId;
                        return imprestId; 
                    } 
                },
	            {
	                "mData": "name"
	            },
	            {
	                "mData": "nature_of_duty"
	            },
	            {
	                "mData": "assignment_end"
	            },        
	            {
	                "mData": "amount"
	            }, 
	        ],
	        aoColumnDefs: [{
	            'bSortable': false,
	            'aTargets': [3]
	        }]
	    });		
	});
});