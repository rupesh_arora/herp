var oRadTable = null;
var paymentMode = '';
var insuranceCompanyId = "";
var insurancePlanId = "";
var insurancePlanNameid = "";
var revisedCost = '';
var paymentMethod ='';
var copayType = '';
var copayValue = '';
$(document).ready(function() {
/* ****************************************************************************************************
 * File Name    :   radiology_test_request.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Arora
 * Created Date :   30th dec, 2015
 * Description  :   This page  manages lab test in consultant module
 *************************************************************************************************** */
    loader();
    debugger;
	$(".amountPrefix").text(amountPrefix);
	$('#oldHistoryModalDetails input[type=text]').addClass("as_label");
    $('#oldHistoryModalDetails input[type=text]').attr('readonly', true);
    $('#oldHistoryModalDetails input[type="button"]').addClass("btnHide");
    $('#oldHistoryModalDetails #divDropDownRadiologyRequest').hide();

    /*Focus on first element*/
    $("#selRadioTest").focus();

    /**DataTable Initialization**/
    oRadTable = $('#radRequestsTbl').DataTable({
        "bPaginate": false,
        aoColumnDefs: [{'bSortable': false,'aTargets': [4] }]        
    });
    
	
	if ($('#thing').val() ==3) {
        var visitId = parseInt($("#textvisitId").val().replace ( /[^\d.]/g, '' ));
        getPaymentMode(visitId);		
    }
    if ($('#thing').val() ==4) {
        var visitId = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
        getPaymentMode(visitId);
    }
    if ($('#thing').val() ==5) {
        var visitId = parseInt($("#oldHistoryVisitId").text().replace ( /[^\d.]/g, '' ));
        getPaymentMode(visitId);
    }

	/*Call function to bind test*/
    bindRadiologyTest(visitId);

    /*Binding Radiology Test*/
    function bindRadiologyTest(visitId) {
        /* var visitIdTest = parseInt($('#textvisitId').val().replace( /[^\d.]/g, '' )); */
        /*AJAX call to show radiology test*/
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/radiology_test_request.php",
            data: {
                "operation": "showRadiologyTest",
                "visitIdTest" : visitId
            },
            success: function(data) {
                if (data != null && data != "") {
                    var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                    var option = "<option value='-1'>--Select--</option>";
                    for (var i = 0; i < parseData.length; i++) {
                        option += "<option value='" + parseData[i].id + "' data-cost='" + parseData[i].fee + "' data-name='" + parseData[i].name + "'>" + parseData[i].name + "</option>";
                    }
                    $('#selRadioTest').html(option);
                }
            },
            error: function() {
                $('#messagemyModal').modal();
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            }
        });
    }

    /*Add Lab Test click event*/
    $('#addRadLabRequest').click(function() {

        $('#addRadLabRequest').css('border', ''); //remove the red error border from add test button
        $('#noRadDataError').html(""); //remove the error message after the add button

        if ($('#selRadioTest').val() == "-1" || $('#selRadioTest').val() == null) {
            $('#selRadioTestError').text("Please select radiology test");
            $('#selRadioTest').addClass("errorStyle");
            $('#selRadioTest').focus();
            return false;
        }
        if($("#selRequestType").val() == -1){
            $("#selRequestType").focus();
            $("#selRequestType").addClass("errorStyle");
            $("#selRequestTypeError").text("Please select request type");
            return false;
        }
        var newRevisedCost = '';
        var id = $('#selRadioTest').val();//test id from button click
        var name = $('#selRadioTest  option:selected').attr('data-name');//getting attribute name of selected element
        var cost = $('#selRadioTest  option:selected').attr('data-cost');
        var requestType = $('#selRequestType').val();
        if (revisedCost == "") {
            newRevisedCost = cost;
            paymentMethod = "cash";
        }
        else {
            newRevisedCost = revisedCost;
            paymentMethod = "insurance";
        }
        if (checkTestExcluded =="yes") {
            $('#confirmUpdateModalLabel').text();
            $('#updateBody').text("Are you sure you want to add this still it is not included in your insurance?");
            $('#confirmUpdateModal').modal();
            $("#btnConfirm").unbind();
            $("#btnConfirm").click(function(){
                var newPaymentMethod = "cash"; //if it is excluded from insurance the paymentmethod is always cash 
                newRevisedCost = cost;//if it is excluded then revised cost is equal to orignal cost

                if (requestType == "internal") {
                    var addData = ["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", "Unpaid", "Pending", cost,newRevisedCost, "<input type='button' id ='newButtonColor' style='color: initial;' class='btnEnabled' onclick='rowRadDelete($(this));' value='Remove'>", id,requestType,newPaymentMethod,"notrestrictBill"];
                }
                else {
                    var addData = ["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", "N/A", "N/A", 0,0, "<input type='button' id ='newButtonColor' style='color: initial;' class='btnEnabled' onclick='rowRadDelete($(this));' value='Remove'>", id,requestType,newPaymentMethod,"restrictBill"];
                }
                
                /*Check that test already exist*/
                for (var i = 0; i < oRadTable.fnGetNodes().length; i++) {
                    var iRow = oRadTable.fnGetNodes()[i];   // assign current row to iRow variable
                    var aData = oRadTable.fnGetData(iRow); // Pull the row

                    testId = aData[6];//getting test id from table
                    var flag = false;
                    if (id == testId && aData[2]!="resultsent") {                
                        $('#selRadioTest').focus();
                        $('#selRadioTestError').text("Test already exist");
                        $('#selRadioTest').addClass("errorStyle");
                        return true;
                    }
                    if (flag == true) {
                        return false;
                    }
                    if (id == testId && aData[2]=="resultsent") {
                        if (requestType == "internal") {
                            addData = ["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", "Unpaid", "Pending", cost, newRevisedCost,"<input type='button' class='btnEnabled' id ='newButtonColor' style='color: initial;' onclick='rowRadDelete($(this));' value='Remove'>", id,requestType,newPaymentMethod,"restrictBill"];
                        }
                        else {
                            addData = ["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", "N/A", "N/A", 0, 0,"<input type='button' class='btnEnabled' id ='newButtonColor' style='color: initial;' onclick='rowRadDelete($(this));' value='Remove'>", id,requestType,newPaymentMethod,"restrictBill"];
                        }
                    }
                }
                
                /*Add data in table*/
                var currData = oRadTable.fnAddData(addData);

                /*Get current row to add color in that*/
                var getCurrDataRow = oRadTable.fnSettings().aoData[ currData ].nTr;
                $(getCurrDataRow).find('td').css({"background-color":"#fff","color":"#000"});
                $(getCurrDataRow).find('span').css("color","#000 !important;");

                $('#totalRadBill').text(parseInt($('#totalRadBill').text()) + parseInt(newRevisedCost));//add total bill
            });
        }

        else{
            if (requestType == "internal") {
                var addData = ["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", "Unpaid", "Pending", cost,newRevisedCost, "<input type='button' id ='newButtonColor' style='color: initial;' class='btnEnabled' onclick='rowRadDelete($(this));' value='Remove'>", id,requestType,paymentMethod,"notrestrictBill"];
            }
            else {
                var addData = ["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", "N/A", "N/A", 0,0, "<input type='button' id ='newButtonColor' style='color: initial;' class='btnEnabled' onclick='rowRadDelete($(this));' value='Remove'>", id,requestType,paymentMethod,"restrictBill"];
            }
            
            /*Check that test already exist*/
            for (var i = 0; i < oRadTable.fnGetNodes().length; i++) {
                var iRow = oRadTable.fnGetNodes()[i];   // assign current row to iRow variable
                var aData = oRadTable.fnGetData(iRow); // Pull the row

                testId = aData[6];//getting test id from table
                var flag = false;
    			if (id == testId && aData[2]!="resultsent") {                
    				$('#selRadioTest').focus();
                    $('#selRadioTestError').text("Test already exist");
                    $('#selRadioTest').addClass("errorStyle");
                    return true;
    			}
                if (flag == true) {
                    return false;
                }
                if (id == testId && aData[2]=="resultsent") {
                    if (requestType == "internal") {
                        addData = ["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", "Unpaid", "Pending", cost, newRevisedCost,"<input type='button' class='btnEnabled' id ='newButtonColor' style='color: initial;' onclick='rowRadDelete($(this));' value='Remove'>", id,requestType,paymentMethod,"restrictBill"];
                    }
                    else {
                        addData = ["<span id ='newDataOfTable' data-previous='false'>"+name+"</span>", "N/A", "N/A", 0, 0,"<input type='button' class='btnEnabled' id ='newButtonColor' style='color: initial;' onclick='rowRadDelete($(this));' value='Remove'>", id,requestType,paymentMethod,"restrictBill"];
                    }
                }
            }
            
            /*Add data in table*/
            var currData = oRadTable.fnAddData(addData);

            /*Get current row to add color in that*/
            var getCurrDataRow = oRadTable.fnSettings().aoData[ currData ].nTr;
            $(getCurrDataRow).find('td').css({"background-color":"#fff","color":"#000"});
            $(getCurrDataRow).find('span').css("color","#000 !important;");

            if (requestType == "internal") {
                $('#totalRadBill').text(parseInt($('#totalRadBill').text()) + parseInt(newRevisedCost));//add total bill 
            }
        }
    });
    
    //Save button functionality
    $('#btnSaveRadTestRequest').click(function() {

        var tableData = oRadTable.fnGetData(); //fetching the datatable data       

        /*Code to compare previpous and current data based on attribute so on click not to get previous data coming from database*/
        var arr= [];
        jQuery.grep(tableData, function( n, i ){
            if($(n[0]).attr("data-previous") == "false"){
                arr.push(n);
            }
        });

        /*Checking whether datatable is empty or not*/
        if (tableData.length == 0) {
            $('#addRadLabRequest').addClass("errorStyle"); //Put the red border around the add lab button
            $('#noRadDataError').html("&nbsp;&nbsp;Please add test(s)."); //Put the error message after the add lab test button
            return false;
        } 
        else { //Saving the data

            var patientId = parseInt($('#txtPatientID').val().replace ( /[^\d.]/g, '' ));
            var visitId = parseInt($('#textvisitId').val().replace ( /[^\d.]/g, '' ));
			if(arr.length == 0){
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Sorry");
				$('.modal-body').text("Your request already in pending!!!");
			}
			else{
				/*AJAX call to save data*/
                $('#confirmUpdateModalLabel').text();
                $('#updateBody').text("Are you sure that you want to save this?");
                $('#confirmUpdateModal').modal();
                $("#btnConfirm").unbind();
                $("#btnConfirm").click(function(){

                    var totalCurrentReqBill = 0;
                    //get the total cost of all currently added request
                    $.each(arr ,function(i,v){  
                        if (arr[i][8] == 'insurance') {
                            totalCurrentReqBill= totalCurrentReqBill + parseInt(arr[i][4]);
                        }  
                    });

    				$.ajax({
    					type: "POST",
    					cache: false,
    					url: "controllers/admin/radiology_test_request.php",
    					datatype: "json",
    					data: {
    						'operation': 'save',
    						'data': JSON.stringify(arr),
    						'patientId': patientId,
    						'visitId': visitId,
    						'tblName': 'radiology_test_request',
    						'fkColName': 'radiology_test_id',
                            'copayType' : copayType,
                            'copayValue' : copayValue,
                            'totalCurrentReqBill' : totalCurrentReqBill
    					},

    					success: function(data) {
    						if (data != "0" && data == "1") {
    							$('#messagemyModal').modal();
    							$('#messageMyModalLabel').text("Success");
    							$('.modal-body').text("Radiology test requested successfully!!!");
    							bindRadiologyTest(visitId);
    							RadiologyTestTable(visitId,paymentMode);
    							$("#selRadioTest").focus();
    						} 
    						if (data =='0'){
    							$('#messagemyModal').modal();
    							$('#messageMyModalLabel').text("Sorry");
    							$('.modal-body').text("Your test result is still pending out");
    							bindRadiologyTest(visitId);
    							$('#radRequestsTbl input[type="button"]').addClass("btnHide");
    							$('#radRequestsTbl input[type="button"]').closest('tr').find('td').removeAttr("style");
    						}
    					},
    					error: function() {
    						$('#messagemyModal').modal();
    						$('#messageMyModalLabel').text("Error");
    						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
    					}
    				});
                });
			}
        }
    });
    
    /*Change functionality*/
    $('#selRadioTest').change(function() {
        var radiologyTestId = $('#selRadioTest').val();
        if (radiologyTestId != "") {
            $('#selRadioTest').removeClass("errorStyle");
            if (paymentMode.toLowerCase() == "insurance") {
                getRevisedCost(radiologyTestId);
                checkExclusion(radiologyTestId,insuranceCompanyId,insurancePlanId,insurancePlanNameid,"radiology");
            }
            else{
                checkTestExcluded = '';
            }
        }
    });

    $('#selRadioTest').change(function() {
        if ($('#selRadioTest').val() != -1) {
            $('#selRadioTest').removeClass("errorStyle");
            $('#selRadioTestError').text("");
        }
    });

    $("#selRequestType").change(function() {
        if ($("#selRequestType :selected") != -1) {
            $('#selRequestTypeError').text("");
            $('#selRequestType').removeClass("errorStyle");
        }
    });
    //Reset button functionality
    $('#btnResetRadTestRequest').click(function() {
        clear();
        $("#selRadioTest").focus();
    });
});

/**Row Delete functionality**/
function rowRadDelete(currInst) {
    var row = currInst.closest("tr").get(0);
    var oRadTbl = $('#radRequestsTbl').dataTable();
    $('#totalRadBill').text(parseInt($('#totalRadBill').text()) - parseInt($(row).find('td:eq(4)').text()));
    oRadTbl.fnDeleteRow(row);

}

function clear() {
    $('#selRadioTest').val("-1");
    $('#selRadioTest').removeClass("errorStyle");
    $('#addRadLabRequest').removeClass("errorStyle");
    $('#noRadDataError').text("");
    var lengthDeleteRow = $(".btnEnabled").closest("tr").length;
    for(var i=0;i<lengthDeleteRow;i++){
        var myDeleteRow = $(".btnEnabled").closest("tr")[0];            
        $('#totalRadBill').text(parseInt($('#totalRadBill').text()) - parseInt($(myDeleteRow).find('td:eq(4)').text()));
        oRadTable.fnDeleteRow(myDeleteRow);
    }
}

function RadiologyTestTable(visitId,paymentMode){
    /*Fetch data from db and insert in to table during page load*/
    var postData = {
        "operation": "rad_search",
        "visit_id":  visitId,
        "paymentMode" : paymentMode,
        "activeTab" : "radiology"
    }
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/old_test_requests.php",
        datatype: "json",
        data: postData,

        success: function(data) {
            dataSet = JSON.parse(data);
            oRadTable.fnClearTable();
            $('#totalRadBill').text("0");
            for(var i=0; i< dataSet.length; i++){
                var id = dataSet[i].id;
                var pay_status = dataSet[i].pay_status;
                var name = dataSet[i].name;
                var test_status = dataSet[i].test_status;
                var cost = dataSet[i].fee;
                var oldRevisedCost =''; 
                if (paymentMode != null && paymentMode.toLowerCase() == "insurance") {
                    oldRevisedCost = dataSet[i].revised_cost;
                }
                else {
                    oldRevisedCost = cost;
                }

                //check wheteher in name external is coming or not
                if (name.indexOf("(external)") > 1) {
                    pay_status = "N/A";
                    test_status = "N/A";
                    cost = 0;
                    oldRevisedCost = 0;
                }

                /*Apply here span to recognise previous or current data*/
                oRadTable.fnAddData(["<span data-previous='true'>"+name+"</span>",pay_status, test_status,cost,oldRevisedCost,"<input type='button' class ='btnDisabled' value='Remove' disabled>",id]);//send one column null so user can't remove previous data
                $('#totalRadBill').text(parseInt($('#totalRadBill').text()) + parseInt(oldRevisedCost));//add total bill                
            }
            /*Given css*/
            oRadTable.find('tbody').find('tr').find('td').css({"background-color":"#ccc"});
        }
    });
}

function getPaymentMode(visitId){
    var postData = {
        "operation": "payment_mode",
        "visitId": visitId
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (result.trim() != '') {
            paymentMode = parseData[0].payment_mode;

            if (paymentMode.toLowerCase() == "insurance") {
                insuranceDetails(visitId);
            }            
        } 
        RadiologyTestTable(visitId,paymentMode);
    });
}
function insuranceDetails(visitId){
    var postData = {
        "operation": "insuranceDetail",
        "visitId": visitId
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (parseData != '') {
            insuranceCompanyId = parseData[0].insurance_company_id;
            insurancePlanId = parseData[0].insurance_plan_id;
            insurancePlanNameid = parseData[0].insurance_plan_name_id;

            getCopayValue();//get the copay details
        }
    });
}
function getRevisedCost(radiologyTestId){
    var postData = {
        "operation": "revisedCostDetail",
        "insuranceCompanyId": insuranceCompanyId,
        "insurancePlanId" : insurancePlanId,
        "insurancePlanNameid" : insurancePlanNameid,
        "componentId" : radiologyTestId,
        "activeTab" : "Radiology"
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (result.length > 2) {
            revisedCost = parseData[0].revised_cost;
        }
        else{
            revisedCost ='';
        }
    });
}
function getCopayValue(){
    var postData = {
        "operation": "getCopayValue",
        "insuranceCompanyId": insuranceCompanyId,
        "insurancePlanId" : insurancePlanId,
        "insurancePlanNameid" : insurancePlanNameid
    }
    dataCall("controllers/admin/lab_test_request.php", postData, function (result){
        var parseData = JSON.parse(result);
        if (parseData != '' && parseData != null) {
            copayType = parseData[0].copay_type;
            copayValue = parseData[0].value;
        }
    });
}