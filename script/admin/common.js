/*
 * File Name    :   common.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar
 * Created Date :   15/jan/2016
 * Description  :   Define common  method for use in project
 */
 // call for ajax function
var checkTestExcluded = '';
// American Numbering System
var th = ['', 'thousand', 'million', 'billion', 'trillion'];

var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];

var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];

var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

function dataCall(requestUrl, objData, callback) {
    $.ajax({
        type: "POST",
		cache: false,
        url: requestUrl,
        data: objData,
        success: function (result) {
            return callback(result);
        },
        error: function () {
        }
    });
}

// call for check email validation
function validateEmail(email,textId,textErrorId,message) {
	var expr = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;;
	var value = expr.test(email);
	if(value == false){
		$('#'+textErrorId).text(message);
		$('#'+textId).focus();
		return true;
	}
	else{
		return false;
	}
};

// function call for disable alpha and special characterSet
function disableAlpha(textId){
	
	$("#"+textId).keypress(function(e) {
  
		// for disable alphabates and special character keys
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { 
            return false;
        }
   });
}

// call for check phone number validation range between 10 to 15
function validNumber(number,textId,textErrorId,message) {
	intRegex = /^\d{10,15}$/;
	var value = intRegex.test(number);
	if(value == false){
		$('#'+textErrorId).text(message);
		$('#'+textId).focus();
		return true;
	}
	else{
		return false;
	}
};

// call for check integer value only
function validNumericNumber(number,textId,textErrorId,message) {
    floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
    var value = floatRegex.test(number);
	if(value == false){
		$('#'+textErrorId).text(message);
		$('#'+textId).focus();
		$('#'+textId).addClass('errorStyle');
		return true;
	}
	else{
		return false;
	}
};

// call function for check url or website
function validURL(number,textId,textErrorId,message) {
	intRegex =  /(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/;
	var value = intRegex.test(number);
	if(value == false){
		$('#'+textErrorId).text(message);
		$('#'+textId).focus();
		$('#'+textId).addClass('errorStyle');
		return true;
	}
	else{
		return false;
	}
};

// call function for check validation of text field
function validTextField(textId,textErrorId,errorMessage) {
	var text = $(textId).val().trim();
	if(text == "" || text == null) {
		$(textErrorId).text(errorMessage);
		$(textId).focus();
		$(textId).addClass('errorStyle');
		return true;
	}
	else{
		return false;
	}
};

// date format converstion
function dateFormat(date) {
	var convertDate = date.split('-');
	return convertDate[2].split(" ")[0]+"/"+convertDate[1]+"/"+convertDate[0];
}

// call function for check validation of text field
function callSuccessPopUp(lblMessage,successMessage) {
		$("#messageMyModalLabel").text(lblMessage);
		$("#messageBody").text(successMessage);
		$("#messagemyModal").modal();
		return true;
}

// for remove error color on keyup.
function removeErrorMessage() {
	
	$("input[type=text]").keyup(function() {

		if ($(this).val() != "") {
			$(this).next("span").text("");
			$(this).removeClass('errorStyle');
		}
		
	});

	$("input[type=number]").keyup(function() {

		if ($(this).val() != "") {
			$(this).next("span").text("");
			$(this).removeClass('errorStyle');
		}
		
	});

	$("textarea.form-control").keyup(function() {
		if ($(this).val() != "") {
			$(this).next("span").text("");
			$(this).removeClass('errorStyle');
		}
	});
	
	$("input[type=Password]").keyup(function() {

		if ($(this).val() != "") {
			$(this).next("span").text("");
			$(this).removeClass('errorStyle');
		}
		
	});

	$("input[type=text]").change(function() {

		if ($(this).val() != "") {
			$(this).next("span").text("");
			$(this).removeClass("errorStyle");
		}
		
	});

	$("input[type=number]").change(function() {

		if ($(this).val() != "") {
			$(this).next("span").text("");
			$(this).removeClass("errorStyle");
		}
		
	});

	$("select").on('change', function() {

		if ($(this).val() != "") {
			$(this).next("span").text("");   
			$(this).removeClass('errorStyle');   
		}
		
	});
}

// split date function
function spilitdate(myVar){
	var arr = myVar.split("/");
	var spilitedDate = (arr[2]+'-'+arr[1]+'-'+arr[0]);
	return spilitedDate;
}
//function to bind inventory type
function bindInventoryType(selId){
	var postData = {
		operation : "showInventoryType"
	}
	dataCall("controllers/admin/item_category.php", postData, function (result){	
		if(result != null && result != ""){
			var parseData= jQuery.parseJSON(result);
		
			var option ="<option value=''>--Select--</option>";
			for (var i=0;i<parseData.length;i++) {

				option+="<option value='"+parseData[i].id+"'>"+parseData[i].type+"</option>";
			}
			$(selId).html(option);
		}		
	});	
}
// clear field
function clearFormDetails(formClass) {
	$(formClass+" input[type=text]").val("");
	$(formClass+" input[type=text]").next("span").text("");
	$(formClass+" input[type=text]").parent().next("span").text("");  
	$(formClass+" input[type=text]").removeClass('errorStyle'); 
	$(formClass+" input[type=number]").val("");
	$(formClass+" input[type=number]").next("span").text("");  
	$(formClass+" input[type=number]").parent().next("span").text("");
	$(formClass+" input[type=number]").removeClass('errorStyle');  
	$(formClass+" textarea").val("");
	$(formClass+" textarea").next("span").text("");  
	$(formClass+" textarea").removeClass('errorStyle');  
	$(formClass+" select").val("");
	$(formClass+" select").next("span").text(""); 
	$(formClass+" select").parent().next("span").text(""); 
	$(formClass+" .dependentSelect").html("<option value=''>-- Select --</option>");
	$(formClass+" select").next("span").text("");  
	$(formClass+" select").removeClass('errorStyle');  
}
//function to compare two date second date shuld be greater than end date
function compareTwoDate(txtFirstDate,txtSecondDate){
	$(txtFirstDate).datepicker({endDate: '+0d'}) .on('changeDate', function(selected){
		startDate = new Date(selected.date.valueOf());
		$(txtSecondDate).datepicker('setStartDate', startDate);
	}); 
	
	$(txtSecondDate).datepicker() .on('changeDate', function(selected){
		startDate = new Date(selected.date.valueOf());
		$(txtFirstDate).datepicker('setEndDate', startDate);
	});
}
function DateTime() {
 	var d = new Date();
 	var seconds = d.getSeconds();
	var minutes = d.getMinutes();
 	var hours = d.getHours();
 	if (seconds.toString().length < 2) {
 		seconds = "0"+seconds;
 	}
 	if (minutes.toString().length < 2) {
 		minutes = "0"+minutes;
 	}
 	if (hours.toString().length < 2) {
 		hours = "0"+hours;
 	}
 	var t = todayDate() +" "+hours + ":" + minutes + ":" + seconds;
 	return t;
}
function checkExclusion(item_id,insurance_company_id,scheme_plan_id,scheme_name_id,service_type){
	var postData = {
        "operation": "checkExclusion",
        "item_id": item_id,
        "insurance_company_id": insurance_company_id,
        "scheme_plan_id": scheme_plan_id,
        "scheme_name_id" : scheme_name_id,
        "service_type" : service_type
    }
    dataCall("controllers/admin/old_test_requests.php", postData, function (result){
    	if (result.trim() == "excluded") {
    		checkTestExcluded = 'yes';
    	}
    	else{
    		checkTestExcluded = 'no';
    	}
    });
}

//bind gl account for parent child
function bindGLAccount(textField,accPayable){//pass null if you want to show all list
	var postData = {
        operation : "showCOA",
        accPayable : accPayable
    }
    dataCall("controllers/admin/payment_voucher.php", postData, function (result){
        debugger;
        var parseData = JSON.parse(result);
        if (postData !='') {
            var option = "<option value=''>--Select--</option>";
            var parentId = '';
            $.each(parseData,function(i,v){
            	if (v.is_parent == "yes") {
            		if (parentId !='' && parentId !=v.parent_account_no) {
            			option +="</optgroup>";
            		}
                	option += "<optgroup value='' label="+v.account_name+">";
                	parentId = v.parent_account_no;
            	}
            	else{
            		option += "<option value='" + v.id + "'>" +v.account_name+"</option>";
            	}
            	if (Object.keys(parseData).length == i+1 && parentId == v.parent_account_no) {
            		option +="</optgroup>";
            	}
            	
            });
            $(textField).html(option);
        }
    });
}
function bindAccountReceivable(textField){
    var postData = {
        operation : "showAccReceivable"
    }
    dataCall("controllers/admin/payment_voucher.php", postData, function (result){
        debugger;
        var parseData = JSON.parse(result);
        if (postData !='') {
            var option = "<option value=''>--Select--</option>";
            var parentId = '';
            $.each(parseData,function(i,v){               
                option += "<option value='" + v.id + "'>" +v.account_name+"</option>";
            });
            $(textField).html(option);
        }
    });
}
function bindLedger(selField) {     //  function for loading ledger
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/payment_mode.php",
        data: {
            "operation": "showLedger"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option data-value='" + parseData[i].value + "' value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $(selField).html(option);
            }
        },
        error: function() {}
    });
}
function allGlAccount(){
    var postData = {
        "operation" : "showGLAccount"
    }
    dataCall("./controllers/admin/cash_collection.php", postData, function (result) {
        if (result.trim() != "") {
            var parseData = JSON.parse(result);
            var option = '';
            var option =  "<option value='-1'>--Select--</option>";
            $.each(parseData,function(i,v){
                option += "<option value='" + v.id + "'>" + v.account_name + "</option>";
            }); 
            $('#selGlAcount').html(option);
        }
    });
}
function chequeDetails(chequeDate,chequePayeeName,chequeAmount){
	debugger;
    var postData = {
        "operation": "getChequeDetails"
    }
   	dataCall("controllers/admin/configuration.php", postData, function (result){     	
        if (result.length > 2) {
            var v = JSON.parse(result);
            
            var styleCheque = document.createElement('style');
            styleCheque.type = 'text/css';
            styleCheque.innerHTML+='#chequeDiv { width:'+v[0].paper_width+'px !important; height:'+v[0].paper_height+'px !important;}';
            styleCheque.innerHTML+= '#chequeDate { top:'+v[0].date_top+'px !important; left:'+v[0].date_left+'px !important; }';
            styleCheque.innerHTML+= '#chequePayeeName { top:'+v[0].payee_top+'px !important; left:'+v[0].payee_left+'px !important; width:'+v[0].paper_width+'px !important;}';
            styleCheque.innerHTML+= '#chequeAmountWords { top:'+v[0].amount_word_top+'px !important; left:'+v[0].amount_word_left+'px !important; width:'+v[0].amount_word_width+'px !important; height:'+v.amount_word_height+'px !important;}';
            styleCheque.innerHTML+= '#chequeAmountFigures { top:'+v[0].amount_figure_top+'px !important; left:'+v[0].amount_figure_left+'px !important; width:'+v[0].amount_figure_width+'px !important;}';  
            document.getElementsByTagName('head')[0].appendChild(styleCheque);
           
            
            $("#chequeDate").text(chequeDate);
            
            $("#cheque-payee-name").text(chequePayeeName);
            //$("#chequePayeePrefix").text(v[0].payee_prefix);
            //$("#chequePayeeSuffix").text(v[0].payee_suffix);
        
            $("#chequeAmountFigurePrefix").text(v[0].amount_figure_prefix);
            $("#chequeAmountFigureSuffix").text(v[0].amount_figure_suffix);
            $("#chequeAmount").text(chequeAmount);
            $("#cheque-amount-word").text(toWords(chequeAmount));//amount to word conversion
            if (toWords(chequeAmount) == '') {
            	$("#nullAmountWordPrefix").text(v[0].prefix_for_null_value);
            }
            window.print();
            //$.print("#chequeDiv");
        }
    });
}
//amount to word conversion
function toWords(s) {
    s = s.toString();
    s = s.replace(/[\, ]/g, '');
    if (s != parseFloat(s)) return 'not a number';
    var x = s.indexOf('.');
    if (x == -1) x = s.length;
    if (x > 15) return 'too big';
    var n = s.split('');
    var str = '';
    var sk = 0;
    for (var i = 0; i < x; i++) {
        if ((x - i) % 3 == 2) {
            if (n[i] == '1') {
                str += tn[Number(n[i + 1])] + ' ';
                i++;
                sk = 1;
            } else if (n[i] != 0) {
                str += tw[n[i] - 2] + ' ';
                sk = 1;
            }
        } else if (n[i] != 0) {
            str += dg[n[i]] + ' ';
            if ((x - i) % 3 == 0) str += 'hundred ';
            sk = 1;
        }
        if ((x - i) % 3 == 1) {
            if (sk) str += th[(x - i - 1) / 3] + ' ';
            sk = 0;
        }
    }
    if (x != s.length) {
        var y = s.length;
        str += 'point ';
        for (var i = x + 1; i < y; i++) str += dg[n[i]] + ' ';
    }
    return str.replace(/\s+/g, ' ');
}

function convertUTCDateToLocalDate(date) {
    var newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);

    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();

    newDate.setHours(hours - offset);

    return newDate;
}
function bindInsuranceCompany() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/scheme_plan.php",
        data: {
            "operation": "ShowInsuranceCompany"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selInsuaracnceCompany').html(option);
            }
        },
        error: function() {}
    });
}

function bindSchemeName(insuranceCompany,insurancePlanId){
    var postData = {
        insuranceCompany:insuranceCompany,
        "operation":"ShowSchemeName"
     }
        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/scheme_plan.php",
            datatype:"json",
            data: postData,
            success: function(data) {
                if (data != null && data != "") {
                    var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                    var option = "<option value=''>--Select--</option>";
                    for (var i = 0; i < parseData.length; i++) {
                        option += "<option value='" + parseData[i].id + "'>" + parseData[i].plan_name + "</option>";
                    }
                    $('#selSchemeName').html(option);
                    if(insurancePlanId !="") {                        
                     $('#selSchemeName').val(insurancePlanId);
                    }
                }
            },
            error: function() {}
        });  
}