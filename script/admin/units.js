/*
 * File Name    :   units.js
 * Company Name :   Qexon Infotech
 * Created By   :   Tushar gupta
 * Created Date :   31th dec, 2015
 * Description  :   This page manage update,save,delete,restore operation
 */
$(document).ready(function() {

    var otable; //defined the global variable for datatable
    $("#form_dept").hide();
    $("#Units_list").addClass('list');    
    $("#tab_Units").addClass('tab-list-add');
    
    $("#tab_add_Units").click(function() { // show tab for add units
        showAddTab();
        clear();       
    });
    $("#tab_Units").click(function() { // show tab for add units list
        showTabList();		
    });

    $("#btnSave").click(function() { // click on save details with check validation

        var flag = "false";
        $("#txtUnitName").val($("#txtUnitName").val().trim());
        $("#txtUnitAbbreviation").val($("#txtUnitAbbreviation").val().trim());

        if ($("#txtUnitAbbreviation").val() == '') {
            $('#txtUnitAbbreviation').focus();
            $("#txtUnitAbbreviationError").text("Please enter unit abbreviation");
            $("#txtUnitAbbreviation").addClass("errorStyle");
            flag = "true";
        }

        if ($("#txtUnitName").val() == '') {
            $('#txtUnitName').focus();
            $("#txtUnitNameError").text("Please enter unit name");
            $("#txtUnitName").addClass("errorStyle");
            flag = "true";
        }
        if (flag == "true") {
            return false;
        }

        var unitName = $("#txtUnitName").val();
        var unitAbbreviation = $("#txtUnitAbbreviation").val();
        var description = $("#txtDescription").val();
        description = description.replace(/'/g, "&#39");
        var postData = {
            "operation": "save",
            "unitName": unitName,
            "unitAbbreviation": unitAbbreviation,
            "description": description
        }

        $.ajax( //ajax call cperform save information
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/units.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Unit saved successfully!!!");
                        $('#messagemyModal').modal();
                        showTabList();
                    } else {
                        $("#txtUnitAbbreviationError").text("Unit abbreviation is already exists");
                        $("#txtUnitAbbreviation").addClass("errorStyle");
                    }

                },
                error: function() {
					
					$('.modal-body').text("");
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
            });

    });

    $("#txtUnitName").keyup(function() { //validation style remove on keyup 
        if ($("#txtUnitName").val() != '') {
            $("#txtUnitNameError").text("");
            $("#txtUnitName").removeClass("errorStyle");
        }
    });

    $("#txtUnitAbbreviation").keyup(function() {
        if ($("#txtUnitAbbreviation").val() != '') {
            $("#txtUnitAbbreviationError").text("");
            $("#txtUnitAbbreviation").removeClass("errorStyle");
        }
    });


    if ($('.inactive-checkbox').not(':checked')) { // show details on load 
        otable = $('#unitsTable').dataTable({ //Datatable code
            "bFilter": true,
            "processing": true,
            "bAutoWidth": false,
            "sPaginationType": "full_numbers",
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = $('#unitsTable').dataTable().fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var unit = mData["unit"];
                        var unitAbbr = mData["unit_abbr"];
                        var description = mData["description"];
                        editClick(id, unit, unitAbbr, description);

                    }
                });
                // perform delete event
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = $('#unitsTable').dataTable().fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        deleteClick(id);
                    }
                });
            },

            "sAjaxSource": "controllers/admin/units.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                /* {  "mData": "id" }, */
                {
                    "mData": "unit"
                }, {
                    "mData": "unit_abbr"
                }, {
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;

                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [3]
            }, {
                'bSortable': false,
                'aTargets': [2]
            }]
        });
    }
    //checked or not functionality to show active or inactive data
    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { //then show inactive data
            otable.fnClearTable();
            otable.fnDestroy();
            otable = "";
            otable = $('#unitsTable').dataTable({
                "bFilter": true,
                "processing": true,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = $('#unitsTable').dataTable().fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            restoreClick(id);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/units.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    /* {  "mData": "id" }, */
                    {
                        "mData": "unit"
                    }, {
                        "mData": "unit_abbr"
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;

                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [3]
                }, {
                    'bSortable': false,
                    'aTargets': [2]
                }]
            });
        } else {
            otable.fnClearTable();
            otable.fnDestroy();
            otable = "";
            otable = $('#unitsTable').dataTable({
                "bFilter": true,
                "processing": true,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = $('#unitsTable').dataTable().fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var unit = mData["unit"];
                            var unitAbbr = mData["unit_abbr"];
                            var description = mData["description"];
                            editClick(id, unit, unitAbbr, description);
                        }
                    });
                    // perform delete event
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = $('#unitsTable').dataTable().fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            deleteClick(id);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/units.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    /* {  "mData": "id" }, */
                    {
                        "mData": "unit"
                    }, {
                        "mData": "unit_abbr"
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;

                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [3]
                }, {
                    'bSortable': false,
                    'aTargets': [2]
                }]
            });
        }
    });

    $("#btnReset").click(function() {
        $("#txtUnitName").focus();
        clear();
    });
});
/* editClick event perform for update unit details with check validation */
function editClick(id, unit, unitAbbr, description) {
    showAddTab();
    $("#btnReset").hide();
    $("#btnSave").hide();
    $('#btnUpdate').show();
    $('#tab_add_Units').html("+Update Unit");
    
    $('#txtUnitName').val(unit);
    $('#txtUnitAbbreviation').val(unitAbbr);
    $('#txtDescription').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);

    $("#btnUpdate").click(function() {
        var flag = "false";

        $("#txtUnitName").val($("#txtUnitName").val().trim());
        $("#txtUnitAbbreviation").val($("#txtUnitAbbreviation").val().trim());

        if ($("#txtUnitAbbreviation").val() == '') {
            $("#txtUnitAbbreviation").focus();
            $("#txtUnitAbbreviationError").text("Please enter unit abbreviation");
            $("#txtUnitAbbreviation").addClass("errorStyle");
            flag = "true";
        }

        if ($("#txtUnitName").val() == '') {
            $("#txtUnitName").focus();
            $("#txtUnitNameError").text("Please enter unit name");
            $("#txtUnitName").addClass("errorStyle");
            flag = "true";
        }

        if (flag == "true") {
            return false;
        }

        var unitName = $("#txtUnitName").val();
        var unitAbbreviation = $("#txtUnitAbbreviation").val();
        var description = $(" #txtDescription").val();
        description = description.replace(/'/g, "&#39");
        var id = $('#selectedRow').val();
		
        $('#confirmUpdateModalLabel').text();
		$('#updateBody').text("Are you sure that you want to update this?");
		$('#confirmUpdateModal').modal();
		$("#btnConfirm").unbind();
		$("#btnConfirm").click(function(){
            $.ajax({
                type: "POST",
                cache: "false",
                url: "controllers/admin/units.php",
                data: {
                    "operation": "update",
                    "id": id,
                    "unitName": unitName,
                    "unitAbbreviation": unitAbbreviation,
                    "description": description
                },
                success: function(data) {
                    if (data != "") {
                        $('#myModal').modal('hide');
						$('.close-confirm').click();
						$('.modal-body').text("");
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Unit updated successfully!!!");                        
                        $('#messagemyModal').modal();
                        showTabList();
                        clear();
                    }
                    if (data == "") {
                        $("#txtUnitAbbreviationError").text("Unit abbreviation is already exists");
                        $("#txtUnitAbbreviation").addClass("errorStyle");
                        $("#txtUnitAbbreviation").focus();
                    }
                },
                error: function() {
    				$('#myModal').modal('hide');
    				$('.modal-body').text("");
    				$('#messageMyModalLabel').text("Error");
    				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
    				$('#messagemyModal').modal();
    			}
            });
        });
    });
}

/* call to delete functionality */
function deleteClick(id) {
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Delete Unit Record");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteUnit("' + type + '");');
}

/* call to restore functionality */
function restoreClick(id) {
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Unit Record");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteUnit("' + type + '");');
}

// perform that function when this is call for perform delete or restore opearion 
function deleteUnit(type) {
    if (type == "delete") {

        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/units.php",
            data: {
                "operation": "delete",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Unit deleted successfully!!!");
                    $("#unitsTable").dataTable().fnReloadAjax();
                    $('#messagemyModal').modal();
                } else {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Sorry");
                    $('.modal-body').text("This unit is used , so you can not delete!!!");
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });

    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/units.php",
            data: {
                "operation": "restore",
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Unit restored successfully!!!");
                    $("#unitsTable").dataTable().fnReloadAjax();
                    $('#messagemyModal').modal();
                }
            },
            error: function() {
				
				$('.modal-body').text("");
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
        });

    }
}

function showTabList(){
    $('#inactive-checkbox-tick').prop('checked', false).change();
    $("#form_dept").hide();
    $(".blackborder").show();

    $("#tab_add_Units").removeClass('tab-detail-add');
    $("#tab_add_Units").addClass('tab-detail-remove');
    $("#tab_Units").removeClass('tab-list-remove');    
    $("#tab_Units").addClass('tab-list-add');
    $("#Units_list").addClass('list');    
    $("#btnReset").show();
    $("#btnSave").show();
    $('#btnUpdate').hide();
    $('#tab_add_Units').html("+Add Unit");
    clear();
}

function showAddTab(){
    $("#form_dept").show();
    $(".blackborder").hide();
   
    $("#tab_add_Units").addClass('tab-detail-add');
    $("#tab_add_Units").removeClass('tab-detail-remove');
    $("#tab_Units").removeClass('tab-list-add');
    $("#tab_Units").addClass('tab-list-remove');
    $("#Units_list").addClass('list');
    removeErrorMessage();
    $("#txtUnitNameError").text("");
    $("#txtUnitName").removeClass("errorStyle");
    $('#txtUnitName').focus();
}

// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});

// clear function
function clear() {
    $('#txtUnitName').val("");
    $('#txtUnitAbbreviation').val("");
    $('#txtUnitNameError').text("");
    $('#txtUnitAbbreviationError').text("");
    $('#txtDescription').val("");
    $('#txtUnitName').removeClass("errorStyle");
    $('#txtUnitAbbreviation').removeClass("errorStyle");
}