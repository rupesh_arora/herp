/*
 * File Name    :   configure_order.js
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh Gupta
 * Created Date :   17th Feb, 2016
 * Description  :   This page is use for load and view data of item and their price
*/
var itemPriceTable;
$(document).ready(function() {
	loader();
	debugger;
	itemPriceTable = $('#viewItemPriceTable').dataTable({ // inilize datatable
        "bFilter": true,
        "bProcessing": true,
        "sPaginationType": "full_numbers",
        aoColumnDefs: [{'bSortable': false,'aTargets': [2]}]
    });

    $("#txtConfItem").focus();

    getData();

    $("#btnConfsave").on('click',function(){
    	var flag = false;
    	var itemName = $("#txtConfItem").val().trim();
    	var price = $("#txtConfItemPrice").val().trim();
    	
    	if (!validnumber($("#txtConfItemPrice").val())) {
			$('#txtConfItemPrice').focus();
            $("#txtConfItemPriceError").text("Please enter only number");
            $('#txtConfItemPrice').addClass("errorStyle");
          flag=true;  
		}
		if (price =='') {
			$('#txtConfItemPrice').focus();
            $("#txtConfItemPriceError").text("Please enter price");
            $('#txtConfItemPrice').addClass("errorStyle");
          flag=true;  
		}
		if (itemName =='') {
    		$("#txtConfItem").focus();
    		$("#txtConfItem").addClass('errorStyle');
    		$("#txtConfItemError").text("Please enter item");
    		flag = true;
    	}
    	if (flag == true) {
    		return false;
    	}

    	var postData = {
    		"itemName" : itemName,
    		"price" : price,
    		"operation" : "save"
    	}
    	$.ajax({
			type: "POST",
	        cache: false,
	        url: "controllers/admin/configure_item_price.php",
	        datatype: "json",
	        data:postData,

	        success : function(data){
	        	if (data !='' && data !='0') {
	        		$('#messagemyModal').modal();
					$('#messageMyModalLabel').text("Sucess");
					$('.modal-body').text("Data saved sucessfully!!!");
					clear();
					getData();
					$("#txtConfItem").focus();
	        	}
	        	else{
	        		$('#messagemyModal').modal();
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Item name doesn't exist");
	        	}
        	},
			error:function() {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			}
        });
    });

    $("#btnReset").click(function(){
    	clear();
    });

    $("#txtConfItem").keyup(function(){
    	if ($("#txtConfItem").val() !='') {
    		$("#txtConfItem").removeClass('errorStyle');
    		$("#txtConfItemError").text("");
    	}
    });
    $("#txtConfItemPrice").keyup(function(){
    	if ($("#txtConfItemPrice").val() !='') {
    		$("#txtConfItemPrice").removeClass('errorStyle');
    		$("#txtConfItemPriceError").text("");
    	}
    });

    $("#btnSearchItem").unbind();
    $("#btnSearchItem").on("click",function(){

    	var itemName = $("#txtConfItem").val().trim();
    	if (itemName =='') {
    		$("#txtConfItemError").text("Please enter correct name");
    		return false;
    	}

    	var postData = {
    		"itemName" : itemName,
    		"operation" : "serachItemDetails"
    	};

    	$.ajax({
			type: "POST",
	        cache: false,
	        url: "controllers/admin/configure_item_price.php",
	        datatype: "json",
	        data:postData,
	        success : function(data){
	        	if (data !='' && data !='0') {
					parseData = JSON.parse(data);

					$("#txtConfItem").val(parseData[0].name);
					$("#txtConfItemDesc").val(parseData[0].description);
					$("#txtOldConfItemPrice").val(parseData[0].unit_cost);
				}
				else{
					clear();
					$("#txtConfItemError").text("Item doesn't exist");
				}
	        }
	    });
    });
    $("#searchItemIcon").on("click",function(){
    	$("#txtConfItem").removeClass('errorStyle');
		$("#txtConfItemError").text("");
		$("#hdnConfigureItem").val("1");
		$("#configureItemPriceModal").modal();
		$("#configureItemPriceBody").load('views/admin/view_item.html');
    });

    $('#txtConfItem').keypress(function (e) {
        if (e.which == 13) {
            $("#btnSearchItem").click();
        }
    });
});

function getData(){
	$.ajax({
		type: "POST",
        cache: false,
        url: "controllers/admin/configure_item_price.php",
        datatype: "json",
        data:{"operation" : "showData"},

		success : function(data){
			if (data !='' && data !='0') {
				parseData = JSON.parse(data);
				itemPriceTable.fnClearTable();
				for(var i= 0; i<parseData.length; i++){

					var itemName = parseData[i].name;
					var description = parseData[i].description;
					var price = parseData[i].price;

					itemPriceTable.fnAddData([itemName,price,description]);
				}
			}
			else {
				$('#messagemyModal').modal();
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("No data exist");
			}
		},
		error:function() {
			$('#messagemyModal').modal();
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
		}
	});
}
function validnumber(number) {
    floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
    return floatRegex.test(number);
}
function clear(){
	$('input[type="text"]').val('');
	$("#txtConfItemDesc").val('');
	$("#txtConfItem").removeClass('errorStyle');
	$("#txtConfItemPrice").removeClass('errorStyle');
	$("#txtConfItemPriceError").text("");
	$("#txtConfItemError").text("");
	$("#txtConfItem").focus();
}