var attendanceTable;
$(document).ready(function () {
	debugger;
	attendanceTable = $('#tblSearchAttendanceList').dataTable({ // inilize datatable on load time.
        "bFilter": true,
        "processing": true,
		"aaSorting": [],
        "sPaginationType": "full_numbers",
		"bAutoWidth" : false,
		aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [1,2,3,4]
        }]
    });


	$("#txtFromDate").datepicker({autoclose:true}).on('changeDate', function(selected){
		startDate = new Date(selected.date.valueOf());
		$('#txtToDate').datepicker('setStartDate', startDate);
	});  
	$("#txtToDate").datepicker({autoclose:true}).on('changeDate', function(selected){
		startDate = new Date(selected.date.valueOf());
		$('#txtFromDate').datepicker('setEndDate', startDate);
	});

	$("#btnSearch").click(function() {

		var fromDate = $('#txtFromDate').val();
		var toDate = $('#txtToDate').val();

		var postData = {
			fromDate:fromDate,
			toDate:toDate,
			"operation":"showAttendance"
		}

		$.ajax({
			type:"POST",
            cache:false,
            url: "controllers/admin/view_attendance.php",
            datatype:"json",
            data:postData,

            success:function(data) {
            	if (data !='') {
            		attendanceTable.fnClearTable();
            		var parseData = jQuery.parseJSON(data);

		        	for(var i=0;i<parseData.length;i++) {
		        		var date = parseData[i].date;
		        		var punchIn = parseData[i].punch_in;
		        		var punchOut = parseData[i].punch_out;
        				var remarks = parseData[i].remarks;
        				var duration = parseData[i].duration;
						attendanceTable.fnAddData([date,punchIn,punchOut,remarks,duration]);        		
					}
				}

            },
            error:function() {

            }
		});

	});
	$("#btnReset").click(function() {
		clear();
	});
});
function clear() {
	$("#txtFromDate").val("");
	$("#txtToDate").val("");
}