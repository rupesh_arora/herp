/*
 * File Name    :   Modal Insurance
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   21st feb, 2016
 * Description  :   This page use for load,save,update,delete,resotre operation
 */
var insuranceTable; //defining a global varibale for data table
$(document).ready(function() {
    loader();
    debugger;
    //Calling function for bind the insurance company 
    bindCompany();
    $(".input-datepicker").datepicker({ // date picker function
        autoclose: true
    });
    //Change event of company dropdown
    $("#insuranceModal #selInsuranceCompany").change(function(){
        var htmlRoom ="<option value='-1'>Select</option>";
        
        if ($('#insuranceModal #selInsuranceCompany :selected').val() != -1) {
            var value = $('#insuranceModal #selInsuranceCompany :selected').val();
            //Calling function for bind the insurance plan
            bindInsurancePlan(value, null);
        }
    });
    $("#searchModalPatientIcon").hide();
    $("#txtModalPatientId").attr('disabled','disabled');
    $("#txtModalPatientId").val($("#hdnPatientId").val());

	// bind company schemeon change company
	$("#insuranceModal #selInsurancePlan").change(function() {
		var option = "<option value=''>--Select--</option>";
		
		if ($("#insuranceModal #selInsurancePlan :selected") != "") {
			var value = $("#insuranceModal #selInsurancePlan :selected").val();
			
			//on change call this function for load state
			bindSchemeName(value, null); 
		} else {
			$("#insuranceModal #selPlanName").html(option);
		}	

		if ($("#insuranceModal #selInsurancePlan").val() != "") {
			$("#insuranceModal #selInsurancePlanError").text("");
			$("#insuranceModal #selInsurancePlan").removeClass("errorStyle");
		}
	});
	
    /*//Datepicker function 
    $("#insuranceModal #txtMemberSince").datepicker({ changeYear: true,changeMonth: true,autoclose: true}) .on('changeDate', function(selected){
        startDate = new Date(selected.date.valueOf());
        $('#insuranceModal #txtExpirationDate').datepicker('setStartDate', startDate);
    }); 
    $('#insuranceModal #txtExpirationDate').datepicker({
        changeYear: true,
        changeMonth: true,
        autoclose: true
    });*/

    var startDate = new Date();
    $("#insuranceModal #txtMemberSince").datepicker('setEndDate',startDate);
    $('#insuranceModal #txtExpirationDate').datepicker('setStartDate', startDate);

    $("#insuranceModal #txtMemberSince,#insuranceModal #txtExpirationDate").click(function() {
        $(".datepicker").css({
            "z-index": "99999999999"
        });
    });

    /* click on button for save  inforamtion */
    $("#insuranceModal #btnAddInsurance").click(function() {
        // validation
        var flag = "false";
        $("#insuranceModal #txtModalPatientId").val().trim();

        if ($("#insuranceModal #txtExpirationDate").val() == '') {
            $("#insuranceModal #txtExpirationDateError").text("Please select expiration date");
            $("#insuranceModal #txtExpirationDate").addClass("errorStyle");
            flag = "true";
        }

        if ($("#insuranceModal #txtMemberSince").val() == '') {
            $("#insuranceModal #txtMemberSinceError").text("Please slect since date");
            $("#insuranceModal #txtMemberSince").addClass("errorStyle");
            flag = "true";
        }
		
		if ($("#insuranceModal #selPlanName").val() == '') {
            $('#insuranceModal #selPlanName').focus();
            $("#insuranceModal #selPlanNameError").text("Please select insurance plan");
            $("#insuranceModal #selPlanName").addClass("errorStyle");
            flag = "true";
        }
		
        if ($("#insuranceModal #selInsurancePlan").val() == '-1') {
            $('#insuranceModal #selInsurancePlan').focus();
            $("#insuranceModal #selInsurancePlanError").text("Please select insurance scheme");
            $("#insuranceModal #selInsurancePlan").addClass("errorStyle");
            flag = "true";
        }

        if ($("#insuranceModal #selInsuranceCompany").val() == '-1') {
            $('#insuranceModal #selInsuranceCompany').focus();
            $("#insuranceModal #selInsuranceCompanyError").text("Please select insurance company");
            $("#insuranceModal #selInsuranceCompany").addClass("errorStyle");
            flag = "true";
        }
		
		if ($("#insuranceModal #txtInsuranceNumber").val() == ''  || $("#txtInsuranceNumber").val() < 0) {
            $('#insuranceModal #txtInsuranceNumber').focus();
            $("#insuranceModal #txtInsuranceNumberError").text("Please enter insurance number");
            $("#insuranceModal #txtInsuranceNumber").addClass("errorStyle");
            flag = "true";
        }
		
        if ($("#insuranceModal #txtModalPatientId").val() == '') {
            $('#insuranceModal #txtModalPatientId').focus();
            $("#insuranceModal #txtModalPatientIdError").text("Please enter patient id");
            $("#insuranceModal #txtModalPatientId").addClass("errorStyle");
            flag = "true";
        }

        var patientId = $("#insuranceModal #txtModalPatientId").val();
        if(patientId.length != 9){
            $("#insuranceModal #txtModalPatientId").focus();
            $("#insuranceModal #txtModalPatientIdError").text("Please enter valid patient id");
            $("#insuranceModal #txtModalPatientId").addClass("errorStyle"); 
            flag = "true";
        }

        if (flag == "true") {
            return false;
        }

        //ajax call for save operation
        var expirationDate = $("#insuranceModal #txtExpirationDate").val();
        var memberSince = $("#insuranceModal #txtMemberSince").val();
        var insurancePlan = $("#insuranceModal #selInsurancePlan").val();
        var insuranceCompany = $("#insuranceModal #selInsuranceCompany").val();
        var extraInfo = $("#insuranceModal #txtExtraInfo").val();
        var insuranceNumber = $("#insuranceModal #txtInsuranceNumber").val();
        var planName = $("#insuranceModal #selPlanName").val();

        
        var patientPrefix = patientId.substring(0, 3);
        patientId = patientId.replace ( /[^\d.]/g, '' ); 
        patientId = parseInt(patientId);

        var postData = {
            "operation": "save",
            "patientId": patientId,
            "expirationDate": expirationDate,
            "memberSince": memberSince,
            "insurancePlan": insurancePlan,
            "insuranceCompany": insuranceCompany,
            "patientPrefix": patientPrefix,
            "insuranceNumber": insuranceNumber,
            "planName": planName,
            "extraInfo": extraInfo
        }

        $.ajax({
            type: "POST",
            cache: false,
            url: "controllers/admin/insurance.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "" && data != "2" && data == "1") {
                    var company = $("#insuranceModal #selInsuranceCompany").val();
                    $('#selInsuranceCompany').val(company);
                    $('#txtInsuranceNo').val($("#insuranceModal #txtInsuranceNumber").val());
                    var plan = $("#insuranceModal #selInsurancePlan").val();
                    bindInsuranceCompany(patientId);
                    var scheme = $("#insuranceModal #selPlanName").val();
                    $('.close').unbind();
                    $('.close').click();
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Insurance saved successfully!!!");
                    $('#messagemyModal').modal();
                    clearModal();
                }
                else if(data == "Limit Crossed"){
                    callSuccessPopUp("Alert","A patient can't request for more than 3 insurance at a time.");
                }
                else if(data == "2"){
                    $('#txtPatientId').focus();
                    $("#txtPatientIdError").text("Please enter valid patient id");
                    $("#txtPatientId").addClass("errorStyle");                    
                }
                else if(data == "Patient not exist"){
                    $('#txtPatientId').focus();
                    $("#txtPatientIdError").text("Please enter valid patient id");
                    $("#txtPatientId").addClass("errorStyle"); 
                }
                else if(data == "Insurance details already exist"){
                    callSuccessPopUp("Alert","This patient already has same insurance details.");
                }                
                else if (data =='') {
                    callSuccessPopUp("Alert","Insurance Number already exist!!!");
                }
                else if(data.indexOf("start date") > 0){
                    callSuccessPopUp("Alert",data);
                }
                else if(data.indexOf("expire date") > 0){
                    callSuccessPopUp("Alert",data);
                }
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });

    });

    //keyup functionality for remove validation style
    $("#insuranceModal #txtModalPatientId").keyup(function() {
        if ($("#insuranceModal #txtModalPatientId").val() != '') {
            $("#insuranceModal #txtModalPatientIdError").text("");
            $("#insuranceModal #txtModalPatientId").removeClass("errorStyle");
        }
    });

    $("#insuranceModal #txtModalPatientId").change(function() {
        if ($("#insuranceModal #txtModalPatientId").val() != '') {
            $("#insuranceModal #txtModalPatientIdError").text("");
            $("#insuranceModal #txtModalPatientId").removeClass("errorStyle");
        }
    });

    $("#insuranceModal #selInsuranceType").change(function() {
        if ($("#insuranceModal #selInsuranceType").val() != '-1') {
            $("#insuranceModal #selInsuranceTypeError").text("");
            $("#insuranceModal #selInsuranceType").removeClass("errorStyle");
        }
    });
	
	$("#insuranceModal #selPlanName").change(function() {
        if ($("#insuranceModal #selPlanName").val() != '') {
            $("#insuranceModal #selPlanNameError").text("");
            $("#insuranceModal #selPlanName").removeClass("errorStyle");
        }
    });
	
    $("#insuranceModal #selInsuranceCompany").change(function() {
        if ($("#insuranceModal #selInsuranceCompany").val() != '-1') {
            $("#insuranceModal #selInsuranceCompanyError").text("");
            $("#insuranceModal #selInsuranceCompany").removeClass("errorStyle");
        }
    });

    $("#insuranceModal #selInsurancePlan").change(function() {
        if ($("#insuranceModal #selInsurancePlan").val() != '-1') {
            $("#insuranceModal #selInsurancePlanError").text("");
            $("#insuranceModal #selInsurancePlan").removeClass("errorStyle");
        }
    });

    $("#insuranceModal #txtExpirationDate").keyup(function() {
        if ($("#insuranceModal #txtExpirationDate").val() != '') {
            $("#insuranceModal #txtExpirationDateError").text("");
            $("#insuranceModal #txtExpirationDate").removeClass("errorStyle");
        }
    });

    $("#insuranceModal #txtMemberSince").keyup(function() {
        if ($("#insuranceModal txtMemberSince").val() != '') {
            $("#insuranceModal txtMemberSinceError").text("");
            $("#insuranceModal txtMemberSince").removeClass("errorStyle");
        }
    });
	$("#insuranceModal txtInsuranceNumber").keyup(function() {
        if ($("#insuranceModal txtInsuranceNumber").val() != '') {
            $("#insuranceModal #txtInsuranceNumberError").text("");
            $("#insuranceModal #txtInsuranceNumber").removeClass("errorStyle");
        }
    });

    // for reset functionality
    $("#insuranceModal #btnReset").click(function() {
        $("#insuranceModal #txtModalPatientId").focus();
        $("#insuranceModal #txtModalPatientId").removeClass("errorStyle");
        clearModal();
    });

    $("#insuranceModal .input-datepicker").keydown(function(e){
        if (e.keyCode < 500) {
            return false;
        }
    });

}); //close document.ready


//definr function for bind company
function bindCompany() {
       $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/insurance_plan.php",
        data: {
            "operation": "showCompany"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#insuranceModal #selInsuranceCompany').html(option);
            }
        },
        error: function() {}
    });
}

//Define function for bind insurance plan
function bindInsurancePlan(value,plan) {
       $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/insurance.php",
        data: {
            "operation": "showPlan",
            "companyId":value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].plan_name + "</option>";
                }
                $('#insuranceModal #selInsurancePlan').html(option);
                if(plan != null){ 
                    $('#selInsurancePlan').html(option);                   
                    $('#selInsurancePlan').val(plan);
                }
            }
        },
        error: function() {}
    });
}

// call function for bind scheme name
function bindSchemeName(value, scheme) {
	 $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/scheme_exclusion.php",
        data: {
            "operation": "showPlanName",
            schemeId: value
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value=''>--Select--</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].scheme_plan_name + "</option>";
                }
                $('#insuranceModal #selPlanName').html(option);
                if(scheme != null){
                    $('#selInsuranceScheme').html(option);
                    $('#selInsuranceScheme').val(scheme);
                }
            }
        },
        error: function() {}
    });
}
// clearModal function
function clearModal() {
    $("#insuranceModal #txtExpirationDate").val("");
    $("#insuranceModal #txtMemberSince").val("");
    $("#insuranceModal #selInsurancePlan").val("-1");
    $("#insuranceModal #selPlanName").val("");
    $("#insuranceModal #selInsuranceCompany").val("-1");
    $("#insuranceModal #selInsuranceType").val("-1");
    $("#insuranceModal #txtInsuranceNumber").val("");
    $("#insuranceModal #txtExtraInfo").val("");
    /*$('#insuranceModal #txtModalPatientId').val("");*/
    $('#insuranceModal #txtModalPatientIdError').text("");
    $('#insuranceModal #txtExpirationDateError').text("");
    $('#insuranceModal #selInsurancePlanError').text("");
    $('#insuranceModal #selInsuranceCompanyError').text("");
    $('#insuranceModal #selInsuranceTypeError').text("");
    $('#insuranceModal #txtMemberSinceError').text("");
    $('#insuranceModal #selPlanNameError').text("");
    $('#insuranceModal #txtInsuranceNumberError').text("");
    $('#insuranceModal #txtModalPatientId').removeClass("errorStyle");
    $('#insuranceModal #selPlanName').removeClass("errorStyle");
    $('#insuranceModal #txtInsuranceNumber').removeClass("errorStyle");
    $("#insuranceModal #txtExpirationDate").removeClass("errorStyle");
    $("#insuranceModal #txtMemberSince").removeClass("errorStyle");
    $("#insuranceModal #selInsurancePlan").removeClass("errorStyle");
    $("#insuranceModal #selInsuranceCompany").removeClass("errorStyle");
    $("#insuranceModal #selInsuranceType").removeClass("errorStyle");
    bindCompany();
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        $(".close").click();
    }
});