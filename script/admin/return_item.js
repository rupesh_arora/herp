$(document).ready(function () {
	debugger;
	$(".input-datepicker").datepicker({ // date picker function
     	autoclose: true
    });
	$("#txtMfgDate").datepicker({ changeYear: true,changeMonth: true,autoclose: true}) .on('changeDate', function(selected){
		startDate = new Date(selected.date.valueOf());
		$('#txtExpDate').datepicker('setStartDate', startDate);
	}); 
	$('#txtExpDate').datepicker({
		changeYear: true,
		changeMonth: true,
		autoclose: true
	});
    removeErrorMessage(); //remove error is defined in common.js

    keyUpAutoComplete(); // call auto complete item name function 

	$("#btnSearch").click(function() {
		var flag = "false";
		if(validTextField('#txtItemName','#txtItemNameError','Please enter Item Name') == true)
        {
            flag = true;
        }
        if(validTextField('#txtBatchNo','#txtBatchNoError','Please enter batch no') == true)
        {
            flag = true;
        }
        if(validTextField('#txtSellerName','#txtSellerNameError','Please enter seller name') == true)
        {
            flag = true;
        }
        if(validTextField('#txtQuantity','#txtQuantityError','Please enter item quantity') == true)
        {
            flag = true;
        }
        if(validTextField('#txtMfgDate','#txtMfgDateError','Please select mfg date') == true)
        {
            flag = true;
        }
        if(validTextField('#txtExpDate','#txtExpDateError','Please select exp date') == true)
        {
            flag = true;
        }
        if(validTextField('#txtRecievedBy','#txtRecievedByError','Please enter recieved by') == true)
        {
            flag = true;
        }
        if(validTextField('#txtDate','#txtDateError','Please select date') == true)
        {
            flag = true;
        }
        if ($("#txtItem").attr('item-id') == undefined) {
            $('#txtItem').focus();
            $("#txtItemError").text("Please select valid item that exist.");
            $('#txtItem').addClass("errorStyle");
            flag = true;
        }
        if(flag == true) {
            return false;
        }
        var itemName = $("#txtItemName").attr('item-id');
        var batch = $("#txtBatchNo").val();
        var sellerName = $("#txtSellerName").attr('seller-id');
        var quantity = $("#txtQuantity").val();
        var mfg = $("#txtMfgDate").val();
        var exp = $("#txtExpDate").val();
        var recieved = $("#txtRecievedBy").val();
        var date = $("#txtDate").val();

        var postData= {
        	itemName:itemName,
        	batch:batch,
        	sellerName:sellerName,
        	quantity:quantity,
        	mfg:mfg,
        	exp:exp,
        	receivedBy:recieved,
        	date:date,
        	"operation":"save"
        }

        $.ajax({
        	type: "POST",
            cache: false,
            url: "controllers/admin/return_item.php",
            datatype:"json",
            data: postData,

            success:function(data) {
            	if(data!='' && data != null){
            		clearFormDetails(".returnItem");
            	}
            },
        	error:function(){

        	}
        });
	});

	$("#txtBatchNo").blur(function(){
		
    	var batch = $("#txtBatchNo").val();
    	var itemName = $("#txtItemName").attr("item-id");
        if (itemName == undefined || itemName == '') {
            $("#txtItemName").focus();
            $("#txtItemName").addClass("errorStyle");
            $("#txtItemNameError").text("No such item exist.");
            return false;
        }
        if (batch == '') {
            return false;
        }

    	var postData = {
    	 	batch:batch,
    	 	itemName:itemName,
    	 	"operation":"bindBatch"
    	 }

    	 	$.ajax({
    	 		type: "POST",
                cache: false,
                url: "controllers/admin/return_item.php",
                datatype:"json",
                data: postData,

			success:function(data) {
                var parseData = jQuery.parseJSON(data);
				if (parseData !='' && parseData!=null) {					
					$("#txtSellerName").val(parseData[0].name);
					$("#txtSellerName").attr('seller-id',parseData[0].id);
				}
                else{
                    $("#txtSellerName").val('');
                    $("#txtBatchNo").focus();
                    $("#txtBatchNo").addClass("errorStyle");
                    $("#txtBatchNoError").text("No such batch number exist.");
                }
			},
			error:function(){

			}
		});
    });



	$("#btnReset").click(function() {
		clearFormDetails(".returnItem"); // calling function for clear fileds i.e. defined in common.js
	});
});
function keyUpAutoComplete(){	
    $("#txtItemName").keyup(function(){     //Autocomplete functionality 
    	if ($(this).val() != '') {
    		$("#txtItemName").removeAttr("item-id");
    	}
        var currentObj = $(this);
        jQuery("#txtItemName").autocomplete({
            source: function(request, response) {
                var postData = {
                    "operation":"searchItem",
                    "itemName": currentObj.val()
                }
                
                $.ajax(
                    {                   
                    type: "POST",
                    cache: false,
                    url: "controllers/admin/return_item.php",
                    datatype:"json",
                    data: postData,
                    
                    success: function(dataSet) {
                    	if (dataSet.length > 2) {
                    		loader();
	                        response($.map(JSON.parse(dataSet), function (item) {
	                            
	                            return {
	                                item_id: item.item_id,
	                                value: item.name
	                            }
	                        }));
                    	}
                    	else{
                    		$("#txtItemName").focus();
                    		$("#txtItemName").addClass("errorStyle");
                    		$("#txtItemNameError").text("No such item exist.");
                    	}	                        
                    },
                    error: function(){
                        
                    }
                });
            },
            select: function (e, i) {
                var itemId = i.item.item_id;
                currentObj.attr("item-id", itemId);
				$("#txtItemName").val(i.item.value);
            },
            minLength: 3
        });     
    });
}
    

