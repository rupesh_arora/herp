var reciptNumber;
var printData = [];
var globalVisitId ;
var globalPatientid;
var globalPatientName;
var totalBillTable;
var printDiv;
var otable ;
var status;
var paymentStatus;
$(document).ready(function(){
/* ****************************************************************************************************
 * File Name    :   pharmacy_dispense.js
 * Company Name :   Qexon Infotech
 * Created By   :   Kamesh Pathak
 * Created Date :   29th dec, 2015
 * Description  :   This page  manages pharmacy dipense data
 *************************************************************************************************** */
	loader();
	$("#pop_up_close").click(function(){
		//Array for store the print data 
		printData = [];
	});
	debugger;
	$(".amountPrefix").text(amountPrefix);

	//Initiliaze data table 
	otable = $('#pharmacydispenseTable').dataTable( {
		"bFilter": true,
		"processing": true,
		"bSort": false,
		"sPaginationType":"full_numbers",
	});	
	$("#divOutstandingBalance").hide();//Hide outstanding div when payment mode = cash account
	
	//This change function is used for select the payment mode 
/*	$("#selModeOfPayment").change(function(){
		if($("#selModeOfPayment").val() == "2"){			
			$("#divOutstandingBalance").show();
			$("#divTotalAmount").hide();
		}
		else{
			$("#divOutstandingBalance").hide();
			$("#divTotalAmount").show();
		}
	});
	*/
	
	// on search icon function
	$("#iconSearch").click(function(){
		$('#pharmacyModalLabel').text("Search Visit");
		$('#pharmacyModalBody').load('views/admin/search_visit_pharmacy.html',function(){
			 $("#thing").val("3");
		});
		$('#myPharmacyModal').modal();
	
	});
	
	/*Print functionality*/
	$("#msgmyModal #printCashDetailss").click(function(){
		/*Call print function*/
		//$.print("#PrintDiv");
		window.print();
		printData = [];
		$(".close").unbind();
		$(".close").click();
	});
	
	//Click function for save the data 
	$("#btnSubmit").click(function(){

		//Start validation 
		var flag=false; 
		
		/*if($("#selModeOfPayment").val()== "2"){
			if($("#txtOutstandingBalance").val() == ""){
				$("#txtOutstandingBalance").focus();
				$("#txtOutstandingBalanceError").text("Insufficient fund");
				$("#txtOutstandingBalance").addClass("errorStyle");      
				flag = true;
			}
			else{
				var outStandingBalance = parseInt($("#txtOutstandingBalance").val());
				var netBill = parseFloat($("#txtNetAmount").val());
				if(outStandingBalance < netBill){
					$("#txtOutstandingBalance").focus();
					$("#txtOutstandingBalanceError").text("Insufficient fund");
					$("#txtOutstandingBalance").addClass("errorStyle");       
					flag = true;
				}
			}				
		}
		if($("#selModeOfPayment").val()== "-1"){
			$("#selModeOfPayment").focus();
			$("#selModeOfPaymentError").text("Select payment mode");
			$("#selModeOfPayment").addClass("errorStyle");      
			flag = true;			
		}*/
		
        if ($("#txtName").val() == "") {
			$("#txtVisitId").focus();
            $("#txtVisitIdError").text("Please select data");
            $("#txtVisitId").addClass("errorStyle");
			flag=true;
        }
		/*if ($("#txtVisitId").val() != "") {
			if ($("#txtName").val() != "") {
				if($("#totalBill").text() <= "0"){
					$('#messageMyModalLabel').text("Sorry");
					$('.modal-body').text("There is no amount!!!");
					$('#messagemyModal').modal();
					flag=true;
				}
			}
		}*/
		if ($("#txtVisitId").val() == "") {
			$("#txtVisitId").focus();
            $("#txtVisitIdError").text("Please enter visit id");
            $("#txtVisitId").addClass("errorStyle");
			flag=true;
        }

        if (flag == true) {
        	return false;
        }

        //End Validation 
		
		/*Defining array outside loop so to hold multiple values*/
		var prescription_id = new Array(); //small array contain prescription  id
		var bigprescription_id =new Array(); // big 2d array conatin small array
		var selfRequestId = new Array(); //small array contain prescription  id
		var bigSelfRequestId =new Array(); // big 2d array conatin small array
		var finalStock = new Array(); //small array contain stock
		var bigFinalStock =new Array(); // big 2d array conatin small array
		var extraMedicine = [];
		
		var aiRows = otable.fnGetNodes(); //Get all rows of data table 		
		var rowcollection =  otable.$(".visitDetails:unchecked", {"page": "all"});	//Select all unchecked row only

		//Getting data from all unchecked rows 
		for (i=0,c=rowcollection.length; i<c; i++) {	

			if (todayDate() > otable.fnGetData($(rowcollection[i]).parent().parent()[0])[7]) {
				var expireDrugName = otable.fnGetData($(rowcollection[i]).parent().parent()[0])[1];
				callSuccessPopUp("Alert","This " +expireDrugName+ " is already expired. So you can't dispense it.");
				return false;
			}		
			var printObj = {};
			prescription_id = new Array();
			selfRequestId = new Array();
			finalStock = new Array();
			var stockDrugId = 0;
			//data-extra attribute is used for check the extra medicine 
			var extra = $(rowcollection[i]).attr('data-extra');
			// this attribute is used for check the visit type 
			var selfRequest = $(rowcollection[i]).attr('visit-type');
			
			//Getting data from all unchecked rows if visit type is SELF REQUEST 
			if(selfRequest == 'selfRequest'){
				selfRequestId.push($(rowcollection[i]).attr('data-id'));
				stockDrugId=($(rowcollection[i]).attr('data-drug-id'));			
				
				//This is used for extra medicine 
				if(extra == "extra"){ 
					stockDrugId = $(rowcollection[i]).attr('data-id');
					var ObjExtra = {};
					
					ObjExtra.drugId = $(rowcollection[i]).attr('data-id');  //Drug id
					ObjExtra.drugName = $(rowcollection[i]).parent().parent().find('td:eq(1)').text(); //drugname		
					ObjExtra.price = $(rowcollection[i]).parent().parent().find('td:eq(4)').text(); //price					
					ObjExtra.quantity = $(rowcollection[i]).parent().parent().find("td.tdQuantity").text(); //Quantity
					ObjExtra.patientId = $("#txtPatientId").val(); //Patient id
					ObjExtra.visitId = $("#txtVisitId").val(); // Visit id
					extraMedicine.push(ObjExtra);
				}
				else{
					prescription_id.push($(rowcollection[i]).attr('data-id'));
					stockDrugId=($(rowcollection[i]).attr('data-drug-id'));
				}
			}
			else{
				//This is used for extra medicine
				if(extra == "extra"){  
					stockDrugId = $(rowcollection[i]).attr('data-id');
					var ObjExtra = {};
					
					ObjExtra.drugId = $(rowcollection[i]).attr('data-id');  //Drug id
					ObjExtra.drugName = $(rowcollection[i]).parent().parent().find('td:eq(1)').text(); //drugname
					ObjExtra.price = $(rowcollection[i]).parent().parent().find('td:eq(4)').text(); //price
					ObjExtra.quantity = $(rowcollection[i]).parent().parent().find("td.tdQuantity").text(); //Quantity
					ObjExtra.patientId = $("#txtPatientId").val(); //Patient id
					ObjExtra.visitId = $("#txtVisitId").val(); // Visit id
					extraMedicine.push(ObjExtra);
				}
				else{
					var currentQuantity = $(rowcollection[i]).parent().parent().find("td.tdQuantity").text();
					prescription_id.push($(rowcollection[i]).attr('data-id'),currentQuantity);
					stockDrugId=($(rowcollection[i]).attr('data-drug-id'));
				}
			}
			
			//Calculate the remaining stock for inventory 
			var currentStock = $(rowcollection[i]).parent().parent().find("td.tdStock").text();
			//Getting the current quantity of medicine 
			var currentQuantity = $(rowcollection[i]).parent().parent().find("td.tdQuantity").text();
			//Getting remaining stock 
			var remStock = parseInt(currentStock) - parseInt(currentQuantity);
			//Push data in final stock array for maintain the inventory 
			finalStock.push(stockDrugId,remStock);
			//this data is comes from consultant screen 
			bigprescription_id.push(prescription_id);
			//this data is comes from self request screen
			bigSelfRequestId.push(selfRequestId);
			bigFinalStock.push(finalStock);
			//Getting data for print 
			printObj.drugName = $($(rowcollection[i]).parent().parent().find('td')[1]).text();
			printObj.drugQuantity = $($(rowcollection[i]).parent().parent().find('td')[3]).text();
			printObj.drugUnitCost = $($(rowcollection[i]).parent().parent().find('td')[4]).text();
			printObj.drugTotal= $($(rowcollection[i]).parent().parent().find('td')[5]).text();			
			
			printData.push(printObj);
		}
		
		if($("#selModeOfPayment").val()== "1"){
			var totalAmount = $("#txtTotalAmount").val();
		}
		else{
			//Getting family Account Number 
			var familyMemberId =$("#hdnFamilyMemberId").val();
			if(familyMemberId == "" || familyMemberId == undefined){
				familyMemberId = parseInt($("#txtPatientId").val().replace ( /[^\d.]/g, '' ));
			}
		}

		var outStandingBalance = $("#txtOutstandingBalance").val();
		var paymentMode = $("#selModeOfPayment").val();
		var totalAmount = $("#txtTotalAmount").val();
		var netAmount = $("#txtNetAmount").val();
		var discount = $("#txtDiscount").val();
		var PatientId = parseInt($("#txtPatientId").val().replace ( /[^\d.]/g, '' ));
		var VisitId = parseInt($("#txtVisitId").val().replace ( /[^\d.]/g, '' ));
			
		globalPatientid = $("#txtPatientId").val();
		globalVisitId = $("#txtVisitId").val();
		globalPatientName = $("#txtName").val();
		totalBillTable = $("#totalBill").text();
		var postData ={
			"operation":"saveDataTableData",				
			"bigprescription_id":JSON.stringify(bigprescription_id),
			"bigSelfRequestId":JSON.stringify(bigSelfRequestId),
			"bigFinalStock":JSON.stringify(bigFinalStock),
			"extraMedicine":JSON.stringify(extraMedicine),
			"paymentMode":paymentMode,
			"totalAmount":totalAmount,
			"familyMemberId":familyMemberId,
			"outStandingBalance":outStandingBalance,
			"netAmount":netAmount,
			"discount":discount,
			"PatientId":PatientId,
			"status":paymentStatus,
			"VisitId":VisitId
		}
		//receiptNo(postData);
		$('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to dispense this?");
	    $('#confirmUpdateModal').modal();
	    $("#btnConfirm").unbind();
	    $("#btnConfirm").click(function(){			
			$.ajax({
				type: "POST",
				cache: false,
				url: "controllers/admin/pharmacy_dispense.php",
				datatype:"json",
				data: postData,
				success : function(data){
					if(data !="" && data != 'Update successfully' && data != '0'){
						$('#myModal').modal('hide');
						$('.close-confirm').click();						
						$('.modal-body').text("");
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Pharmacy dispensed successfully !!!");
						$('#messagemyModal').modal();						
						$("#divOutstandingBalance").hide();
						$("#divTotalAmount").show();
						otable.fnClearTable();
						clearDispense();
					}
					else if(data == 'Update successfully'){
						$('.close-confirm').click();
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Quantity updated successfully!!!");
						$('#messagemyModal').modal();
						otable.fnClearTable();
						clearDispense();
					}
					else{
						$('.close-confirm').click();
						$('#messageMyModalLabel').text("Sorry");
						$('.modal-body').text("Please go to cash counter!!!");
						$('#messagemyModal').modal();
						otable.fnClearTable();
						clearDispense();
					}
				},
				error : function(){
					$('.close-confirm').click();
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
			});
		});
		
	});
	
	// that event use in consultant screen
	$("#btnSelect").click(function(){

		var checkStock = new Array();
		var checkQuantity = new Array();
		var checkPrescriptionId = new Array();
		var flag=false;
		var visitId = $("#txtVisitId").val();

        if ($("#txtVisitId").val() == "") {
			$("#txtVisitId").focus();
            $("#txtVisitIdError").text("Please enter visit id");
            $("#txtVisitId").addClass("errorStyle");
			flag=true;
        }

        if(visitId.length != 9){
			$('#messageMyModalLabel').text("Sorry");
			$('.modal-body').text("Visit id does not exist!!!");					
			$('#messagemyModal').modal();
			$("#ok").click(function(){
				$("#txtVisitId").focus();
			});      
			flag = "true";
		}

        if (flag == "true") {
            return false;
        }
       
		var visitPrefix = visitId.substring(0, 3);
		visitId = visitId.replace ( /[^\d.]/g, '' ); 
		visitId = parseInt(visitId);
		otable.fnClearTable();
        
	    //ajax call to show data

	   
        var postData = {
   			"operation":"showData",
   			"visitId" : visitId,
   			"visitPrefix" : visitPrefix
   		}
	    $.ajax({     
			type: "POST",
			cache: false,
			url: "controllers/admin/pharmacy_dispense.php",
			data: postData,
			success: function(data) { 
				if(data != "0" && data != "2"){
					paymentStatus = '';
					var checkId;					
					var parseData = jQuery.parseJSON(data);
					var totalBill = 0;
					var dataLength = parseData.length-1;
					dataSet = JSON.parse(data);
						for (var i=0;i<dataLength;i++) {
							var patientId = parseData[i].id;
							var patientPrefix = parseData[i].patient_prefix;
							$("#txtName").val(parseData[i].name);
							$("#hdnFamilyMemberId").val(parseData[i].fan_id);
                            var gender = parseData[i].gender;
							if(gender == "F"){
								$("#txtGender").val("Female");
							}
							else{
								$("#txtGender").val("Male");
							}

							$("#txtPaymentStatus").val(parseData[i].insurance_payment);
							
							//convert time string to date
							time = parseData[i].created_on;
							//var dateTime = new Date(parseInt(time));
							var date = new Date(time*1000);
							
							var getDOB = parseData[i].dob;
							checkId = parseData[i].prescription_id;
							status = parseData[i].insurance_payment;
							if(status == 'paid'){
								paymentStatus = 'paid';
							}
							 
							//checkId variable is used for data table content
							if(checkId != null && checkId != ""){ 
								checkStock.push(parseData[i].stock);
								checkQuantity.push(parseData[i].quantity);
								checkPrescriptionId.push(parseData[i].prescription_id);
								
								
								var name = parseData[i].drug_name;
								var stock = parseData[i].stock;
								var quantity = parseData[i].quantity;
								var price = parseData[i].price;
								var total = parseData[i].total;	
								var drugId = parseData[i].drug_id;
								var id = parseData[i].prescription_id;
								var visitType = parseData[i].visit_type;
								var expireDate = parseData[i].expiry_date;
								//Add row if stock is greater than quantity
								if(parseInt(stock) > parseInt(quantity) && parseData[i].insurance_payment == 'paid'){ 
									/*if(visitType == 'self_request'){
										otable.fnAddData([i+1,name, stock,quantity,price,total,
										"<input type='checkbox'  class='visitDetails' visit-type='selfRequest' data-id="+id+" data-price="+total+" data-drug-id="+drugId+" style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;'"+					 
										   " style='font-size: 22px; cursor:pointer;'>",expireDate]);
										otable.find('tr:last').css('background-color','#E7E3E4');
										totalBill += parseInt(total);
									}
									else{*/
										otable.fnAddData([i+1,name, stock,quantity,price,total,
										"<input type='checkbox'  class='visitDetails' data-id="+id+" data-price="+total+" data-drug-id="+drugId+" style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;'"+					 
										   " style='font-size: 22px; cursor:pointer;'>",expireDate]);
										otable.find('tr:last').css('background-color','#E7E3E4');
										totalBill += parseInt(total);
									/*}*/				
								}
								else{ //Add row if stock is less than quantity 
									/*if(visitType == 'self_request'){
										otable.fnAddData([i+1,name, stock,quantity,price,total,
										"<input type='checkbox' checked='checked' disabled='disabled' visit-type='selfRequest'   class='visitDetails' data-id="+id+" data-price="+total+" data-drug-id="+drugId+" style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;'"+					 
										   " style='font-size: 22px; cursor:pointer;'>",expireDate]);
										otable.find('tr:last').css('background-color','#ff6666');
									}
									else{*/
										otable.fnAddData([i+1,name, stock,quantity,price,total,
										"<input type='checkbox' checked='checked' disabled='disabled'  class='visitDetails' data-id="+id+" data-price="+total+" data-drug-id="+drugId+" style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;'"+					 
										   " style='font-size: 22px; cursor:pointer;'>",expireDate]);
										otable.find('tr:last').css('background-color','#ff6666');
									/*}*/			
								}
							}
						}
						if(paymentStatus == 'paid'){ 
							$("#btnAddExtra").hide();
							$("#txtPaymentStatus").val('paid');
						}
						var patientIdLength = patientId.toString().length;
						for (var i=0;i<6-patientIdLength;i++) {
							patientId = "0"+patientId;
						}
						patientId = patientPrefix+patientId;
						$("#txtPatientId").val(patientId);
						//Set the class for all columns 
						$('#pharmacydispenseTable td:nth-child(1)').addClass("tdPrescriptionId");
						$('#pharmacydispenseTable td:nth-child(3)').addClass("tdStock");
						$('#pharmacydispenseTable td:nth-child(4)').addClass("tdQuantity");
						$('#pharmacydispenseTable td:nth-child(5)').addClass("tdPrice");
						$('#pharmacydispenseTable td:nth-child(6)').addClass("tdTotal");
						$('#pharmacydispenseTable td:nth-child(7)').addClass("tdCheckbox");
						var new_age = getAge(getDOB);

						var split = new_age.split(' ');
						var age_years = split[0];
						var age_month = split[1];
						var age_day = split[2];

						$("#txt_year").val(age_years);
						$("#txt_month").val(age_month);
						$("#txt_day").val(age_day);
						
						var get_date = date.getDate();
						var year = date.getFullYear();
						var month = date.getMonth();
						month = month +1;
						if(month < "10"){
							month = "0"+month;
						}
						 
						if(get_date < "10"){
							get_date = "0"+get_date;
						}
						 var VisitDate = year + "-" + month + "-" + get_date;
	 
						$("#txtVisitDate").val(VisitDate);
						$("#txtOutstandingBalance").val(parseData[dataLength].amount);
						$("#txtVisitIdError").text("");
						$("#txtVisitId").removeClass("errorStyle");
						$("#txtVisitId").attr("disabled", "disabled");				
						$("#totalBill").text(totalBill);
						$("#txtTotalAmount").val(totalBill);
						var discountValue = $("#txtDiscount").val();
						if(discountValue == ""){
							discountValue = 0;
						}
						var discount = parseFloat($("#txtDiscount").val());
						var netAmount = getDiscountedPrice(totalBill, discount);
						$("#txtNetAmount").val(Math.round(netAmount));
				}
				else{
					 $('#messageMyModalLabel').text("Sorry");
					 $('.modal-body').text("Visit id does not exist!!!");
					 $('#messagemyModal').modal();
					 clearDispense();
				} 				
					
				//change function for checkbox in data table
				$("#pharmacydispenseTable .visitDetails").on('change', function(){
					if(status != 'paid'){

						//Getting infromation from datatable and stored in variable 
						var id = $(this).attr('data-id');
						var checkBox = $(this);
						var checkBoxTd = $(this).parent();
						var price = parseInt(checkBoxTd.siblings(".tdTotal").text());
						var stock = checkBoxTd.siblings(".tdStock").text();
						var quantity = checkBoxTd.siblings(".tdQuantity").text();
						var totalBill = 0;

						//Check stock with quantity 
						if(parseInt(stock) > parseInt(quantity)){
							if($(this).is(":checked")){
								$(this).closest("tr").css('background-color','#ff6666');
								var totalBill = parseInt($("#totalBill").text()) - price;
								$("#totalBill").text(totalBill);
								$("#txtTotalAmount").val(totalBill);
								var discount = parseFloat($("#txtDiscount").val());
								var netAmount = getDiscountedPrice(totalBill, discount);
								$("#txtNetAmount").val(Math.round(netAmount));
							}							
							else{
								$(this).closest("tr").css('background-color','#E7E3E4');
								var totalBill = parseInt($("#totalBill").text()) + price;
								$("#totalBill").text(totalBill);
								$("#txtTotalAmount").val(totalBill);
								var discount = parseFloat($("#txtDiscount").val());
								var netAmount = getDiscountedPrice(totalBill, discount);
								$("#txtNetAmount").val(Math.round(netAmount));
							}							
						}
					}
				});	
				
				//function for make editable column of quantity in data table
				$("#pharmacydispenseTable").unbind();
				$(function () {					
					$("#pharmacydispenseTable").on('dblclick','.tdQuantity', function () {
						if(status != 'paid'){
							var OriginalContent = $(this).text();
							var parentObj = $(this);
							$(this).addClass("cellEditing"); 
							$(this).html("<input type='text' value='" + OriginalContent + "' />");

							//Enter button function of keyboard 
							$(this).children().first().keypress(function (e) { 
								if (e.which == 13) {
									if ($(this).val() =='') {
										callSuccessPopUp('Alert',"Can't enter a null value");
										return false;
									}
									var newContent = $(this).val();
									//check for number only
									var floatRegex =   /^\d*(\.\d{1})?\d{0,9}$/;
									var value = floatRegex.test(newContent);
									if(value == false){
										callSuccessPopUp('Alert',"Enter number only");
										return false;
									}
									var newQuantity = $(this).parent().text(newContent); 
									var stock = $(this).parent().text(newContent); 
									var price = parentObj.siblings(".tdPrice").text();
									var newTotal = parseInt(price) * parseInt(newQuantity.text()) ;		
									var oldTotal = parentObj.siblings(".tdTotal").text();											
									var stock = parentObj.siblings(".tdStock").text();
									var totalBill = parseInt($("#totalBill").text());
									parentObj.siblings(".tdTotal").text(newTotal);
									if($(parentObj.siblings(".tdCheckbox").find('input')).is(":checked")){//if checkbox is checked
										if(stock >= parseInt(newContent)){ //if stock is greater than quantity
											totalBill = totalBill + newTotal; //total bill (net amount)
											$("#totalBill").text(totalBill);
											$("#txtTotalAmount").val(totalBill);
											var discount = parseInt($("#txtDiscount").val());
											var netAmount = getDiscountedPrice(totalBill, discount);
											$("#txtNetAmount").val(Math.round(netAmount));
											$(this).parent().removeClass("cellEditing");
										}
										else{ //if stock is less than quantity
											$("#totalBill").text(totalBill);
											$("#txtTotalAmount").val(totalBill);											
											var discount = parseFloat($("#txtDiscount").val());
											var netAmount = getDiscountedPrice(totalBill, discount);
											$("#txtNetAmount").val(Math.round(netAmount));
											$(this).parent().removeClass("cellEditing");												
										}
									}
									else{ // if checkbox is not checked
										if(stock >= parseInt(newContent)){ // if stock is greater than quantity
											totalBill = Math.abs(totalBill - oldTotal);
											totalBill = totalBill + newTotal;
											$("#totalBill").text(totalBill);
											$("#txtTotalAmount").val(totalBill);
											var discount = parseFloat($("#txtDiscount").val());
											var netAmount = getDiscountedPrice(totalBill, discount);
											$("#txtNetAmount").val(Math.round(netAmount));
											$(this).parent().removeClass("cellEditing");
										}
										else{ // if stock is less than quantity
											totalBill = Math.abs(totalBill - oldTotal); // change negative value into positive
											$("#totalBill").text(totalBill);
											$("#txtTotalAmount").val(totalBill);
											var discount = parseFloat($("#txtDiscount").val());
											var netAmount = getDiscountedPrice(totalBill, discount);
											$("#txtNetAmount").val(Math.round(netAmount));
											$(this).parent().removeClass("cellEditing");													
										}
									}
									if(stock >= parseInt(newContent)){ //unckecked the checkbox and change the background color
										$(parentObj.siblings(".tdCheckbox").find('input')).prop('checked', false);
										$(parentObj.siblings(".tdCheckbox").find('input')).attr('disabled', false);
										$(parentObj.siblings(".tdCheckbox").find('input')).closest("tr").css('background-color','#E7E3E4');
									}
									else{ //ckecked the checkbox and change the background color
										$(parentObj.siblings(".tdCheckbox").find('input')).prop('checked', true);
										$(parentObj.siblings(".tdCheckbox").find('input')).attr('disabled', true);
										$(parentObj.siblings(".tdCheckbox").find('input')).closest("tr").css('background-color','#ff6666');
									}
								} 
							});
							$(this).children().first().blur(function(){ // blur function for quantity column in datatable
								if ($(OriginalContent).val() =='') {
									callSuccessPopUp('Alert',"Can't enter a null value");
									return false;
								}
								$(this).parent().text(OriginalContent);
								$(this).parent().removeClass("cellEditing");
							});

							/*Code to focus on last charcter*/
							var SearchInput = $(this).children();
							SearchInput.val(SearchInput.val());
							var strLength= SearchInput.val().length;
							SearchInput.focus();
						}
					}); 
				});				
			},
			error:function(){
				$('#messageMyModalLabel').text("Error");
				$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
				$('#messagemyModal').modal();
			}
		});		
	});
	
	//Keyup functionality
	$("#txtVisitId").keyup(function() {
        if ($("#xtVisitId").val() != "") {
            $("#txtVisitIdError").text("");
            $("#txtVisitId").removeClass("errorStyle");
        }
    }); 
	$("#selModeOfPayment").change(function(){
        if ($("#selModeOfPayment").val() != "-1") {
            $("#selModeOfPaymentError").text("");
            $("#selModeOfPayment").removeClass("errorStyle");
            $("#txtOutstandingBalanceError").text("");
            $("#txtOutstandingBalance").removeClass("errorStyle");
        }
	});
	$("#txtDiscount").keyup(function(e) {
        if ($("#txtDiscount").val() != "") {
	
			var totalAmount = parseInt($("#txtTotalAmount").val());
			var discount = parseFloat($("#txtDiscount").val());
			var netAmount = getDiscountedPrice(totalAmount, discount);
			$("#txtNetAmount").val(Math.round(netAmount));		
        }
		if(e.keyCode == 8){ // this is used for back space button on discount textbox
			if ($("#txtDiscount").val() != "") {
				var discount = parseFloat($("#txtDiscount").val());
				var totalAmount = parseInt($("#txtTotalAmount").val());
				var netAmount = getDiscountedPrice(totalAmount, discount);
				$("#txtNetAmount").val(Math.round(netAmount));	
			}
			if ($("#txtDiscount").val() == "") {
				$("#txtDiscount").val("0");
				var discount = parseInt($("#txtDiscount").val());
				var totalAmount = parseInt($("#txtTotalAmount").val());
				var percentageAmount = (totalAmount*discount)/100;
				var netAmount = totalAmount - percentageAmount;
				$("#txtNetAmount").val(Math.round(netAmount));
			}
		}
    });
	$("#txtVisitId").change(function() {
        if ($("#txtVisitId").val() != "") {
            $("#txtVisitIdError").text("");
            $("#txtVisitId").removeClass("errorStyle");
        }
    }); 
	
	// function for reset button
	$("#btnReset").click(function(){
		otable.fnClearTable();
	$("#selModeOfPayment").val("-1");
		clearDispense();
	});
	
	//Click function for extra medicine 
	$("#btnAddExtra").click(function(){
		if($("#btnAddExtra").val() == "Add Extra"){
			$("#divtbl").hide();
			$("#divAddExtra").show();
			$("#btnAddExtra").val("Back To List");
		}
		else{
			$("#divtbl").show();
			$("#divAddExtra").hide();
			$("#btnAddExtra").val("Add Extra");
			clearExtraMedicine();//Calling function for clear the extra medicine 
		}		
	});
	
	//Add Lab Test click event
    $('#btnAdd').click(function() {

        $('#btnAdd').css('border', ''); //remove the red error border from add test button
        $('#noDataError').html(""); //remove the error message after the add button

        
        if ($('#txtDrugId').val() == "") {
            $('#txtDrugIdError').text("Please enter drug id");
            $('#txtDrugId').addClass("errorStyle");
            return false;
        }
		
		if($('#txtAvailableStocks').val() < "1"){
			 $('#messageMyModalLabel').text("Sorry");
			 $('.modal-body').text("Stock is not available!!!");
			 $('#messagemyModal').modal();
			 return false;
		}		
		
		$("#divtbl").show();
		$("#divAddExtra").hide();
		$("#btnAddExtra").val("Add Extra");
		
		var srNo = $("#pharmacydispenseTable").dataTable().fnGetNodes().length + 1;
        var drugId = $('#txtDrugId').val();
        var name = $('#txtDrugName').val();
        var stock = $('#txtAvailableStocks').val();
        var unitCost = $('#txtUnitPrice').val();
        var total = $('#txtUnitPrice').val();        
		
		//Add new row in data table 
		otable.fnAddData([srNo,name, stock,"1",unitCost,total,
		"<input type='checkbox'  class='visitDetails' data-id="+drugId+" data-extra='extra' style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;'"+					 
		   " style='font-size: 22px; cursor:pointer;'>"]);
		
		//Set the class for all columns
		$('#pharmacydispenseTable tr:last td:eq(2)').addClass("tdStock");
		$('#pharmacydispenseTable tr:last td:eq(3)').addClass("tdQuantity");
		$('#pharmacydispenseTable tr:last td:eq(4)').addClass("tdPrice");
		$('#pharmacydispenseTable tr:last td:eq(5)').addClass("tdTotal");
		$('#pharmacydispenseTable tr:last td:eq(6)').addClass("tdCheckbox");  
		
		 clearExtraMedicine();//Calling function for clear the extra medicine 
		
		//Calculate the total amount
		var totalBill = parseInt($("#totalBill").text()) + parseInt(total);
		$("#totalBill").text(totalBill);
		$("#txtTotalAmount").val(totalBill);
		var discount = parseFloat($("#txtDiscount").val());
		var netAmount = getDiscountedPrice(totalBill, discount);
		$("#txtNetAmount").val(Math.round(netAmount));
    });
	
	//Autocomplete functionality for drug name 
	$("#txtDrugName").on('keyup', function(){		
		var currentObj = $(this);
		jQuery("#txtDrugName").autocomplete({
			source: function(request, response) {
				var postData = {
					"operation":"search",
					"drugId": '',
					"drugName": currentObj.val(),
					"drugCode": ''
				}
				
				$.ajax(
					{					
					type: "POST",
					cache: false,
					url: "controllers/admin/search_drug.php",
					datatype:"json",
					data: postData,
					
					success: function(dataSet) {
						loader();
						var data = JSON.parse(dataSet);
						response($.map(data, function (item) {
							return {
								id: item.id,
								value: item.name,
								stock: item.quantity,
								unit: item.unit_measure,
								price: item.price
							}
						}));
					},
					error: function(){
						$('#messageMyModalLabel').text("Error");
						$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
						$('#messagemyModal').modal();
					}
				});
			},
            select: function (e, i) {
				$('#txtDrugId').val(i.item.id);
				$('#txtAvailableStocks').val(i.item.stock);
				$('#txtUnitPrice').val(i.item.price);
				$('#txtUnitMeasure').val(i.item.unit);
				
            },
			minLength: 3
		});		
	});
	
	//Click function for search icon for extra medicine 
	$("#drugIconSearch").click(function(){
		$('#drugModalLabel').text("Search Drug");
		$('#drugModalBody').load('views/admin/search_drug.html');
		$('#myDrugModal').modal();
	});

	$('#txtVisitId').keypress(function (e) {
        if (e.which == 13) {
            $("#btnSelect").click();
        }
    });

    $("#txtTotalAmount,#txtNetAmount,#txtOutstandingBalance").keydown(function(e){
        if (e.keyCode < 500) {
            return false;
        }
    });
});

//Function calculate the net amount after the discount
function getDiscountedPrice(totalBill, discount) {
	if(discount == null || discount == "" || discount == undefined || isNaN(discount)){
		discount = 0;
	}
	var percentageAmount = (totalBill*discount)/100;
	var netAmount = totalBill - percentageAmount;
	return netAmount;
}

// function for calculate the age 
function getAge(dateString) {
    var now = new Date();
    var today = new Date(now.getYear(), now.getMonth(), now.getDate());

    var yearNow = now.getYear();
    var monthNow = now.getMonth();
    var dateNow = now.getDate();

    var dob = new Date(dateString.substring(0, 4), dateString.substring(5, 7) - 1, dateString.substring(8, 10));

    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();
    var age = {};
    var ageString = "";
    var yearString = "";
    var monthString = "";
    var dayString = "";


    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    age = {
        years: yearAge,
        months: monthAge,
        days: dateAge
    };

    if (age.years > 1) yearString = " years";
    else yearString = " year";
    if (age.months > 1) monthString = " months";
    else monthString = " month";
    if (age.days > 1) dayString = " days";
    else dayString = " day";


    if ((age.years > 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
        ageString = age.years + " " + age.months + " " + age.days + "";

    else ageString = "Oops! Could not calculate age!";

    return ageString;
}
//Clear function for extra medicine 
function clearExtraMedicine(){
	$('#txtDrugId').val("");
	$('#txtDrugId').removeClass("errorStyle");
	$('#txtDrugIdError').text("");
	$('#txtDrugName').val("");
	$('#txtAvailableStocks').val("");
	$('#txtUnitPrice').val("");
	$('#txtUnitMeasure').val("");
}

//function for clear the data 
function clearDispense(){
	$("#txtVisitId").val("");
	$("#txtVisitId").removeClass("errorStyle");
	$("#txtVisitIdError").text("");
	$("#txtPatientId").val("");
	$("#txtName").val("");
	$("#divOutstandingBalance").hide();
	$("#divTotalAmount").show();
	$("#selModeOfPayment").val("-1");
	$("#txtGender").val("");
	$("#txtVisitDate").val("");
	$("#txt_year").val("");
	$("#txt_month").val("");
	$("#txt_day").val("");  
	$("#totalBill").text(0);
	$("#txtTotalAmount").val(0);
	$("#txtNetAmount").val(0);
	$("#txtDiscount").val("");
	$("#btnAddExtra").show();
	$("#txtVisitId").removeAttr("disabled", "disabled");
}
//debugger src triage.js
function receiptNo(objCashData){
	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/pharmacy_dispense.php",
		datatype:"json",
		data: {
			"operation":"receipt"
		},					
		success: function(data) {
			if(data != null && data != ""){
				var receiptNo = parseInt(data.match(/-*[0-9]+/));
				receiptNo = receiptNo + 1;
				var receiptNoLength = receiptNo.toString().length;
				for(i=0;i<6-receiptNoLength;i++){
					receiptNo = "0"+receiptNo;
				}
				reciptNumber = receiptNo;//to fetch data during print time
				$("#reciptNoMsg").text(receiptNo);
				var htmlPrintData = printDetails();	

				objCashData.printDiv = htmlPrintData;	
				confirmSaveDetails(objCashData);			
			}
			else{
				$("#reciptNoMsg").val("000001");
			}			
		},
		error : function(){			
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
	});
}

// function call of print details
function printDetails(){
	/*firstly empty page after that load data*/
	$("#codexplBody").find("tr:gt(0)").html("");
	$("#printTotalBill").text("");
	/*Load data to print page*/
	$("#lblPrintReceipt").text($("#reciptNoMsg").text());
	$("#lblPrintVisitId").text(globalVisitId);
	$("#lblPrintPatientId").text(globalPatientid);
	$("#lblPrintPatient").text(globalPatientName);
	$("#lblPrintDate").text(todayDate());
	/*hospital information through global varibale in main.js*/
	$("#lblPrintEmail").text(hospitalEmail);
	$("#lblPrintPhone").text(hospitalPhoneNo);
	$("#lblPrintAddress").text(hospitalAddress);
	if (hospitalLogo != "null") {
		imagePath = "./images/" + hospitalLogo;
		$("#printLogo").attr("src",imagePath);
	}
	$("#printHospitalName").text(hospitalName);
	$("#printTotalBill").text(totalBillTable);
	//$("#").text();
	for(var i=0;i<printData.length;i++){
		$("#codexplBody").append('<tr><td>'+ (i+1) +'</td><td>'+printData[i].drugName+'</td><td>'
		+printData[i].drugQuantity +'</td><td>'+printData[i].drugUnitCost+'</td><td>'+printData[i].drugTotal+'</td>');
	}
	return $("#PrintDiv").html();
}

function confirmSaveDetails(objCashData){
		
	}

