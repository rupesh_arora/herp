var assetClassTable;
$(document).ready(function(){
    loader();
    debugger;

    bindGLAccount($('#selGlAccount'),'');
    bindGLAccount($('#selAccumulatedGlAccount'),'');
    bindPpeClass();

	$('#txtValueBox').val('%');
	/*Hide add ward by default functionality*/
	$("#advanced-wizard").hide();
    $("#assetClassList").addClass('list');    
    $("#tabAssetClassList").addClass('tab-list-add');
		
	/*Click for add the asset class*/
    $("#tabAddassetClass").click(function() {
        clear();
       showAddTab();
    });
	
	//Click function for show the beneficiary lists
    $("#tabAssetClassList").click(function() {
        clear();
        tabAssetClassList();
    });

    removeErrorMessage();

    $('#btnSubmit').click(function() {
        var flag = false;

        if($('#selPpeClass').val() == '') {
            $('#selPpeClassError').text('Please select ppe class');
            $('#selPpeClass').focus();
            $('#selPpeClass').addClass("errorStyle"); 
            flag = true;
        }
        if($('#selAccumulatedGlAccount').val() == '') {
            $('#selAccumulatedGlAccountError').text('Please select accumulated GL account');
            $('#selAccumulatedGlAccount').focus();
            $('#selAccumulatedGlAccount').addClass("errorStyle"); 
            flag = true;
        }
        if($('#selGlAccount').val() == '') {
            $('#selGlAccountError').text('Please select GL account');
            $('#selGlAccount').focus();
            $('#selGlAccount').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtDepriciationRate').val() == '') {
            $('#txtDepriciationRateError').text('Please enter depriciation name');
            $('#txtDepriciationRate').focus();
            $('#txtDepriciationRate').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtAssetClass').val() == '') {
            $('#txtAssetClassError').text('Please enter asset class');
            $('#txtAssetClass').focus();
            $('#txtAssetClass').addClass("errorStyle"); 
            flag = true;
        }
        if(flag == true) {
            return false;
        }

        var assetClass = $('#txtAssetClass').val().trim();
        assetClass =  assetClass.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });
        var depriciationRate = $('#txtDepriciationRate').val().trim();
        var glAccount = $('#selGlAccount').val();
        var accumulatedGlAccount = $('#selAccumulatedGlAccount').val();
        var ppeClass = $('#selPpeClass').val();
        var description = $('#txtDescription').val().trim();

        var postData = {
            assetClass : assetClass,
            depriciationRate : depriciationRate,
            glAccount : glAccount,
            accumulatedGlAccount : accumulatedGlAccount,
            ppeClass : ppeClass,
            description : description,
            operation : "saveAssetClass"
        }


        $.ajax(
            {                   
            type: "POST",
            cache: false,
            url: "controllers/admin/asset_class.php",
            datatype:"json",
            data: postData,
            
            success: function(data) {
                if(data != "0" && data != ""){
                    callSuccessPopUp('Success','Saved successfully!!!');
                    tabAssetClassList();
                    clear();
                    $('#txtBeneficiaryName').focus();
                }
                else{
                    $('#txtAssetClassError').text('Asset class name is already exist');
                    $('#txtAssetClass').addClass('errorStyle');
                    $('#txtAssetClass').focus();
                }
            },
            error: function(){

            }
        });
    });

    $('#btnReset').click(function(){
        clear();
    });

    if ($('.inactive-checkbox').not(':checked')) { // show details in table on load
    //Datatable code 
        assetClassTable = $('#assetClassTable').dataTable({
            "bFilter": true,
            "processing": true,
            "sPaginationType": "full_numbers",
            "bAutoWidth": false,
            "fnDrawCallback": function(oSettings) {
                // perform update event
                $('.update').unbind();
                $('.update').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = assetClassTable.fnGetData(data);
                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var assetClassName = mData["asset_class_name"];
                        var depriciationRate = mData["depriciation_rate"];
                        var glAccountID = mData["gl_account_id"];
                        var accumulatedGlAccountId = mData["accumluated_gl_account_id"];
                        var ppeClassId = mData["ppe_class_id"];                      
                        var description = mData["description"];
                        editClick(id,assetClassName,depriciationRate,glAccountID,accumulatedGlAccountId,ppeClassId,description);

                    }
                });
                //perform delete event
                $('.delete').unbind();
                $('.delete').on('click', function() {
                    var data = $(this).parents('tr')[0];
                    var mData = assetClassTable.fnGetData(data);

                    if (null != mData) // null if we clicked on title row
                    {
                        var id = mData["id"];
                        var name = mData['asset_class_name'];
                        deleteClick(id,name);
                    }
                });
            },
            "sAjaxSource": "controllers/admin/asset_class.php",
            "fnServerParams": function(aoData) {
                aoData.push({
                    "name": "operation",
                    "value": "show"
                });
            },
            "aoColumns": [
                {
                    "mData": "asset_class_name"
                }, {
                    "mData": "depriciation_rate"
                }, {
                    "mData": "gl_account_name"
                }, {
                    "mData": "acc_gl_account_name"
                }, {
                    "mData": "ppe_class_name"
                }, {
                    "mData": "description"
                }, {
                    "mData": function(o) {
                        var data = o;
                        return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                            " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                            " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                            " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                            " data-original-title='Delete'></i>";
                    }
                },
            ],
            aoColumnDefs: [{
                'bSortable': false,
                'aTargets': [5,6]
            }]

        });
    }

    $('.inactive-checkbox').change(function() {
        if ($('.inactive-checkbox').is(":checked")) { // show incative data on checked
            assetClassTable.fnClearTable();
            assetClassTable.fnDestroy();
            assetClassTable = "";
            assetClassTable = $('#assetClassTable').dataTable({
                "bFilter": true,
                "processing": true,
                "deferLoading": 57,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform restore event
                    $('.restore').unbind();
                    $('.restore').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = assetClassTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var name = mData["asset_class_name"];
                            restoreClick(id,name);
                        }

                    });
                },

                "sAjaxSource": "controllers/admin/asset_class.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "checked"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "asset_class_name"
                    }, {
                        "mData": "depriciation_rate"
                    }, {
                        "mData": "gl_account_name"
                    }, {
                        "mData": "acc_gl_account_name"
                    }, {
                        "mData": "ppe_class_name"
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return '<i class="ui-tooltip fa fa-pencil-square-o restore" style="font-size: 22px; text-align:center;width:100%;cursor:pointer;" title="Restore"></i>';
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [5,6]
                }]
            });
        } else { // show active data on unchecked   
            assetClassTable.fnClearTable();
            assetClassTable.fnDestroy();
            assetClassTable = "";
            assetClassTable = $('#assetClassTable').dataTable({
                "bFilter": true,
                "processing": true,
                "sPaginationType": "full_numbers",
                "bAutoWidth": false,
                "fnDrawCallback": function(oSettings) {
                    // perform update event
                    $('.update').unbind();
                    $('.update').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = assetClassTable.fnGetData(data);
                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var assetClassName = mData["asset_class_name"];
                            var depriciationRate = mData["depriciation_rate"];
                            var glAccountID = mData["gl_account_id"];
                            var accumulatedGlAccountId = mData["accumluated_gl_account_id"];
                            var ppeClassId = mData["ppe_class_id"];                      
                            var description = mData["description"];
                            editClick(id,assetClassName,depriciationRate,glAccountID,accumulatedGlAccountId,ppeClassId,description);
                        }
                    });
                    // perform delete event
                    $('.delete').unbind();
                    $('.delete').on('click', function() {
                        var data = $(this).parents('tr')[0];
                        var mData = assetClassTable.fnGetData(data);

                        if (null != mData) // null if we clicked on title row
                        {
                            var id = mData["id"];
                            var name = mData['asset_class_name'];
                            deleteClick(id,name);
                        }
                    });
                },

                "sAjaxSource": "controllers/admin/asset_class.php",
                "fnServerParams": function(aoData) {
                    aoData.push({
                        "name": "operation",
                        "value": "show"
                    });
                },
                "aoColumns": [
                    {
                        "mData": "asset_class_name"
                    }, {
                        "mData": "depriciation_rate"
                    }, {
                        "mData": "gl_account_name"
                    }, {
                        "mData": "acc_gl_account_name"
                    }, {
                        "mData": "ppe_class_name"
                    }, {
                        "mData": "description"
                    }, {
                        "mData": function(o) {
                            var data = o;
                            return "<i class='ui-tooltip fa fa-pencil update' title='Edit'" +
                                " style='font-size: 22px; cursor:pointer;' data-original-title='Edit'></i>" +
                                " <i class='ui-tooltip fa fa-trash-o delete' title='Delete' " +
                                " style='font-size: 22px; color:#a94442; cursor:pointer;' " +
                                " data-original-title='Delete'></i>";
                        }
                    },
                ],
                aoColumnDefs: [{
                    'bSortable': false,
                    'aTargets': [5,6]
                }]
            });
        }
    });


});

function editClick(id,assetClassName,depriciationRate,glAccountID,accumulatedGlAccountId,ppeClassId,description) {
    showAddTab()  
    $("#btnReset").hide();
    $("#btnSubmit").hide();
    $('#btnUpdate').show();
    $('#tabAddassetClass').html("+Update Asset Class");
   
    $('#txtValueBox').val('%');
    $('#txtAssetClass').focus();
    
    $("#txtAssetClass").removeClass("errorStyle");
    $("#txtAssetClassError").text("");
    $("#txtDepriciationRate").removeClass("errorStyle");
    $("#ttxtDepriciationRateError").text("");
    $("#selGlAccount").removeClass("errorStyle");
    $("#selGlAccountError").text("");
    $("#selAccumulatedGlAccount").removeClass("errorStyle");
    $("#selAccumulatedGlAccountError").text("");
    $("#selPpeClass").removeClass("errorStyle");
    $("#selPpeClassError").text("");
   
    $('#txtAssetClass').val(assetClassName);
    $('#txtDepriciationRate').val(depriciationRate);
    $('#selGlAccount').val(glAccountID);
    $('#selAccumulatedGlAccount').val(accumulatedGlAccountId);
    $('#selPpeClass').val(ppeClassId);
    $('#txtDescription').val(description.replace(/&#39/g, "'"));
    $('#selectedRow').val(id);
   
 
    removeErrorMessage();
    
     //validation
    $("#btnUpdate").click(function() { // click update button
        var flag = false;
        if($('#selPpeClass').val() == '') {
            $('#selPpeClassError').text('Please select ppe class');
            $('#selPpeClass').focus();
            $('#selPpeClass').addClass("errorStyle"); 
            flag = true;
        }
        if($('#selAccumulatedGlAccount').val() == '') {
            $('#selAccumulatedGlAccountError').text('Please select accumulated gl account');
            $('#selAccumulatedGlAccount').focus();
            $('#selAccumulatedGlAccount').addClass("errorStyle"); 
            flag = true;
        }
        if($('#selGlAccount').val() == '') {
            $('#selGlAccountError').text('Please select gl account');
            $('#selGlAccount').focus();
            $('#selGlAccount').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtDepriciationRate').val() == '') {
            $('#txtDepriciationRateError').text('Please enter beneficiary name');
            $('#txtDepriciationRate').focus();
            $('#txtDepriciationRate').addClass("errorStyle"); 
            flag = true;
        }
        if($('#txtAssetClass').val() == '') {
            $('#txtAssetClassError').text('Please enter asset class');
            $('#txtAssetClass').focus();
            $('#txtAssetClass').addClass("errorStyle"); 
            flag = true;
        }
        
        if(flag == true) {
            return false;
        }

        var assetClass = $('#txtAssetClass').val().trim();
        assetClass =  assetClass.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });
        var depriciationRate = $('#txtDepriciationRate').val().trim();
        var glAccount = $('#selGlAccount').val();
        var accumulatedGlAccount = $('#selAccumulatedGlAccount').val();
        var ppeClass = $('#selPpeClass').val();
        var description = $("#txtDescription").val().trim();
        description = description.replace(/'/g, "&#39");
        var id = $('#selectedRow').val();
        
        $('#confirmUpdateModalLabel').text();
        $('#updateBody').text("Are you sure that you want to update this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){
        var postData = {
            "operation": "update",
            "assetClass": assetClass,
            "depriciationRate": depriciationRate,
            "glAccount": glAccount,
            "accumulatedGlAccount": accumulatedGlAccount,
            "ppeClass": ppeClass,
            "description": description,
            "id": id
        }
        $.ajax( //ajax call for update data
            {
                type: "POST",
                cache: false,
                url: "controllers/admin/asset_class.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "0" && data != "") {
                        $('#myModal').modal('hide');
                        $('.close-confirm').click();
                        $('.modal-body').text("");
                        $('#messageMyModalLabel').text("Success");
                        $('.modal-body').text("Asset class updated successfully!!!");
                        $('#messagemyModal').modal();
                        assetClassTable.fnReloadAjax();
                        tabAssetClassList()
                        clear();
                    }
                    else{
                        $('#txtAssetClassError').text('Asset class already exist');
                        $('#txtAssetClass').focus();
                        $('#txtAssetClass').addClass("errorStyle"); 
                    }
                },
                error: function() {
                    $('.close-confirm').click();
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Error");
                    $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                    $('#messagemyModal').modal();
                }
            }); // end of ajax
        });
    });
} // end update button

function deleteClick(id,name) { // delete click function
    $('.modal-body').text("");
    $('#confirmMyModalLabel').text("Delete Asset Class");
    $('.modal-body').text("Are you sure that you want to delete this?");
    $('#confirmmyModal').modal();
    $('#selectedRow').val(id);
    var type = "delete";
    $('#confirm').attr('onclick', 'deleteAssetClass("' + type + '","'+id+'","'+name+'");');
} // end click fucntion

function restoreClick(id,name) { // restore click function
    $('.modal-body').text("");
    $('#selectedRow').val(id);
    $('#confirmMyModalLabel').text("Restore Asset Class");
    $('.modal-body').text("Are you sure that you want to restore this?");
    $('#confirmmyModal').modal();
    var type = "restore";
    $('#confirm').attr('onclick', 'deleteAssetClass("' + type + '","'+id+'","'+name+'");');
}
// key press event on ESC button
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        /* window.location.href = "http://localhost/herp/"; */
        $('.close').click();
    }
});
function deleteAssetClass(type,id,name) {
    if (type == "delete") {
        var id = $('#selectedRow').val();
        var postData = {
            "operation": "delete",
            "name" : name,
            "id": id
        }
        $.ajax({ // ajax call for delete        
            type: "POST",
            cache: false,
            url: "controllers/admin/asset_class.php",
            datatype: "json",
            data: postData,

            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Asset class deleted successfully!!!");
                    $('#messagemyModal').modal();
                    assetClassTable.fnReloadAjax();
                } 
                else{
                    callSuccessPopUp("Sorry","This Class is used So you can not delete it !!!.")
                }
            },
            error: function() {
                
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        }); // end ajax 
    } else {
        var id = $('#selectedRow').val();
        $.ajax({
            type: "POST",
            cache: "false",
            url: "controllers/admin/asset_class.php",
            data: {
                "operation": "restore",
                "name" : name,
                "id": id
            },
            success: function(data) {
                if (data != "0" && data != "") {
                    $('.modal-body').text("");
                    $('#messageMyModalLabel').text("Success");
                    $('.modal-body').text("Asset class restored successfully!!!");
                    $('#messagemyModal').modal();
                    assetClassTable.fnReloadAjax();
                }
                else{
                    callSuccessPopUp("Sorry","Asset class already exist so you can't restore it!!!");
                }
            },
            error: function() {             
                $('.modal-body').text("");
                $('#messageMyModalLabel').text("Error");
                $('.modal-body').text("Temporary unavailable to respond.Try again later!!!");
                $('#messagemyModal').modal();
            }
        });
    }
}

function bindPpeClass() {     // function for loading account number of asset type
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/asset_class.php",
        data: {
            "operation": "showPpeClass"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string  jquery

                var option = "<option value=''>--Select--</option>";
                $.each(parseData,function(i,v){
                    option += "<option value='" +v.id+ "'>" + v.ppe_class_name + "</option>";
                });
                
                $('#selPpeClass').html(option);
            }
        },
        error: function() {}
    });
} 

function tabAssetClassList(){
    $("#advanced-wizard").hide();
    $(".blackborder").show();
    $("#tabAddassetClass").removeClass('tab-detail-add');
    $("#tabAddassetClass").addClass('tab-detail-remove');
    $("#tabAssetClassList").removeClass('tab-list-remove');    
    $("#tabAssetClassList").addClass('tab-list-add');
    $("#assetClassList").addClass('list');
   
    $("#btnReset").show();
    $("#btnSubmit").show();
    $('#btnUpdate').hide();
    $('#tabAddassetClass').html("+Add Asset Class");
    $('#inactive-checkbox-tick').prop('checked', false).change();
}
function showAddTab(){
    $("#advanced-wizard").show();
    $(".blackborder").hide();

    $("#tabAddassetClass").addClass('tab-detail-add');
    $("#tabAddassetClass").removeClass('tab-detail-remove');
    $("#tabAssetClassList").removeClass('tab-list-add');
    $("#tabAssetClassList").addClass('tab-list-remove');
    $("#assetClassList").addClass('list');
    $("#txtName").focus();
}



function clear() {
    $('#txtAssetClass').val('');
    $('#txtAssetClassError').text('');
    $('#txtAssetClass').removeClass("errorStyle");

    $('#txtDepriciationRate').val('');
    $('#txtDepriciationRateError').text('');
    $('#txtDepriciationRate').removeClass("errorStyle");

    $('#selGlAccount').val('');
    $('#selGlAccountError').text('');
    $('#selGlAccount').removeClass("errorStyle");

    $('#selAccumulatedGlAccount').val('');
    $('#selAccumulatedGlAccountError').text('');
    $('#selAccumulatedGlAccount').removeClass("errorStyle");

    $('#selPpeClass').val('');
    $('#selPpeClassError').text('');
    $('#selPpeClass').removeClass("errorStyle");

    $('#txtDescription').val('')
    
    $('#txtAssetClass').focus();
}