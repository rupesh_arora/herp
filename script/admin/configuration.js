$(document).ready(function(){
	loader();
	showValue();
	debugger;

	$('#txtErrorColor,#txtBgColor').click(function(){
		$('#txtErrorColor,#txtBgColor').ColorPicker({
			onSubmit: function(hsb, hex, rgb, el) {
				$(el).val(hex);
				$(el).ColorPickerHide();
			},
			onBeforeShow: function () {
				$(this).ColorPickerSetColor(this.value);
			}
		})
		.bind('keyup', function(){
			$(this).ColorPickerSetColor(this.value);
		});
	});

	$("#btnUpdate").click(function(){		
		var errorColor = $("#txtErrorColor").val().trim();
		var patientId = $("#txtPatientId").val().trim();
		var VisitId = $("#txtVisitId").val().trim();
		var drugCode = $("#txtDrugCode").val().trim();	
		var ipdPrefix = $("#txtIPDPrefix").val().trim();	
		var familyAccountPrefix = $("#txtFamilyAccountPrefix").val().trim();	
		var amountPrefix = $("#txtAmountPrefix").val().trim();
		var themeBgColor = $("#txtBgColor").val().trim();
		var staffPrefix = $("#txtStaffPrefix").val().trim();
		var invoicePrefix = $("#txtInvoicePrefix").val().trim();
		var productPrefix = $("#txtProductPrefix").val().trim();
		var customerPrefix = $("#txtCustomerPrefix").val().trim();
		var receiptPrefix = $("#txtReceiptPrefix").val().trim();
		var voucherPrefix = $("#txtVoucherPrefix").val().trim();
		var imprestPrefix = $("#txtImprestPrefix").val().trim();
		var pettyPrefix = $("#txtPettyPrefix").val().trim();

		if (errorColor =='' && patientId =='' && VisitId =='' && drugCode =='' && ipdPrefix =='' && 
			familyAccountPrefix =='' && amountPrefix =='' && themeBgColor =='' && staffPrefix =='' &&
			invoicePrefix == ''  && imprestPrefix == '') {
			callSuccessPopUp('Alert','Can\'t fill the empaty form');
			return false;
		}

		//check two text or more text box don't have same values
		var allTextBoxes = [];

		$('#form_submit_services input[type=text]').each(function () {
	        allTextBoxes.push($(this).val())
	    });

	    var sortedOrder = allTextBoxes.sort();

	    for (var i = 0; i < allTextBoxes.length - 1; i++) {
	        if (sortedOrder[i + 1] == sortedOrder[i]) {
	            callSuccessPopUp('Alert','Please enter different value in each textBox.');
	            return false;
	        }
	    }

		var postData = {
			"operation":"update",
			"errorColor":errorColor,
			"patientId":patientId,
			"VisitId":VisitId,
			"familyAccountPrefix":familyAccountPrefix,
			"drugCode":drugCode,
			"ipdPrefix":ipdPrefix,
			"amountPrefix":amountPrefix,
			"themeBgColor" : themeBgColor,
			"staffPrefix" : staffPrefix,
			"invoicePrefix" : invoicePrefix,
			"productPrefix" : productPrefix,
			"customerPrefix" : customerPrefix,
			"receiptPrefix" : receiptPrefix,
			"voucherPrefix" : voucherPrefix,
			"imprestPrefix" : imprestPrefix,
			"pettyPrefix" : pettyPrefix
		}
		$('#confirmmyModal').modal();
        $('#confirmMyModalLabel').text("Update Configuration");
        $('.modal-body').text("Are you sure that you want to update this?"); 
        $('#confirm').unbind();       
        $('#confirm').on('click',function(){
			$.ajax({
				type: "POST",
				cache: false,
				url: "controllers/admin/configuration.php",
				datatype:"json",
				data: postData,
				
				success: function(data) {
					if(data != "0" && data != ""){
						$('#messageMyModalLabel').text("Success");
						$('.modal-body').text("Updated successfully!!!");
						showValue();
						$('#messagemyModal').modal();
						configurationDeatils();
					}
					else{
						$('#messageMyModalLabel').text("Sorry");
						$('.modal-body').text("Data doesn't exist. ");
						$('#messagemyModal').modal();
					}
				},
				error : function(){
					$('#messageMyModalLabel').text("Error");
					$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
					$('#messagemyModal').modal();
				}
			});
		});
	});
});

function showValue(){
	var postData = {
		"operation":"show"
	}
		
	$.ajax({
		type: "POST",
		cache: false,
		url: "controllers/admin/configuration.php",
		datatype:"json",
		data: postData,
		
		success: function(data) {
			if(data != "0" && data != ""){
				var parseData = jQuery.parseJSON(data);
				  					
				$("#txtErrorColor").val(parseData[0].value);
				var color = parseData[0].value;								
				$("#txtPatientId").val(parseData[1].value);								
				$("#txtVisitId").val(parseData[2].value);								
				$("#txtDrugCode").val(parseData[3].value);				
				$("#txtFamilyAccountPrefix").val(parseData[4].value);				
				$("#txtAmountPrefix").val(parseData[5].value);				
				$("#txtIPDPrefix").val(parseData[6].value);
				$("#txtBgColor").val(parseData[7].value);			
				$("#txtStaffPrefix").val(parseData[8].value);
				$("#txtInvoicePrefix").val(parseData[9].value);
				$("#txtProductPrefix").val(parseData[10].value);
				$("#txtReceiptPrefix").val(parseData[11].value);
				$("#txtCustomerPrefix").val(parseData[12].value);
				$("#txtVoucherPrefix").val(parseData[14].value);
				$("#txtImprestPrefix").val(parseData[13].value);
				$("#txtPettyPrefix").val(parseData[15].value);
			}
			else{
				$('#messageMyModalLabel').text("Sorry");
				$('.modal-body').text("Data doesn't exist. ");
				$('#messagemyModal').modal();
			}
			
		},
		error : function(){
			$('#messageMyModalLabel').text("Error");
			$('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
			$('#messagemyModal').modal();
		}
	});
}
//# sourceURL = configuration.js