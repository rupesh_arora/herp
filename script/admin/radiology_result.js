$(document).ready(function(){
	loader(); 
	debugger;
	var files;
	var otable;
	$("#txtSpecimenId").focus();
	$("#btnSpecimenLoad").click(function(){
		$("#txtSpecimenIdError").text("");
		$("#txtSpecimenId").removeClass("errorStyle");
		loadSpecimenResults();
	});
	
	// upload multiple image
	$("#upload").change(function(){
		$('.imageLabel').remove();
		files = !!this.files ? this.files : [];
		if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
		$('#fileLabel').text(files.length +" files");
		for(i = 0 ;i<files.length;i++){
			if (/^image/.test(files[i].type)) { // only image file
				var reader = new FileReader();
				$("#imageNumber").prepend('<label class="control-label imageLabel" style ="width: 100%;" id ="imageList'+i+'" for="imageList">'+files[i].name+'<i class="fa fa-times deleteImage" title="Delete" data-imageName='+files[i].name+' style="margin-left: 20px;font-size: 15px;cursor:pointer;font-weight: normal;"></i></label>');			// instance of the FileReader
				
			}
			$('.deleteImage').on('click', function(event) {   				
				$(this).parent().remove();    
				var fileName = $(this).attr('data-imageName');	                     
			
				files = jQuery.grep(files, function(value) {
					return value.name != fileName;
				});			
				$('#fileLabel').text(files.length +" files");
				if(files.length == 0){
					$('#fileLabel').text("No file choosen.");
				}
			});
		}	
	});
	
	
	// for remove error style	
    $("#txtSpecimenId").keyup(function() {

        if ($("#txtSpecimenId").val().trim() != "") {
            $("#txtSpecimenIdError").hide();
			$("#txtSpecimenIdError").text("");
			$("#txtSpecimenId").removeClass("errorStyle");
        }
		else{
			$("#txtSpecimenIdError").show();
			$("#txtSpecimenIdError").text("Please enter specimen ID");
			$("#txtSpecimenId").addClass("errorStyle");
			$('#txtSpecimenId').focus();
			radilologyclear();
		}
    });
	$("#txtSpecimenId").change(function() {

        if ($("#txtSpecimenId").val().trim() != "") {
            $("#txtSpecimenIdError").hide();
			$("#txtSpecimenIdError").text("");
			$("#txtSpecimenId").removeClass("errorStyle");
        }
	});
	$('#searchSpecimen').click(function(){
		$('#specimenModalLabel').text("Search Request Id");
		$('#specimenModalBody').load('views/admin/view_specimen.html',function(){
		});
		$('#mySpecimenModal').modal();
	});
	$("#txtSpecimenId").focusin(function(){
		radilologyclear();
	
	});
	$('#btnSubmit').click(function(){
		saveResult(files);
	});
	$('#btnReset').click(function(){
		$('#txtSpecimenDescription').val("");
		$('#txtDrawnDate').val("");
		$('#txtLabTest').val("");
		/* $('#txtNormalRange').val("");
		$('#txtResult').val(""); */
		$('#txtReport').val("");
		$("#txtSpecimenId").removeClass("errorStyle");
		$("#txtSpecimenIdError").text("");
		radilologyclear();
		$('#txtSpecimenId').focus();
	});
	
	$('#txtSpecimenId').keypress(function (e) {
        if (e.which == 13) {
            $("#btnSpecimenLoad").click();
        }
    });
	
});
function saveResult(files){
	var flag = false;
	if($("#txtSpecimenId").val().trim()== "")
	{
		$("#txtSpecimenIdError").show();
		$("#txtSpecimenIdError").text("Please enter request ID");
		$("#txtSpecimenId").addClass("errorStyle");
		$('#txtSpecimenId').focus();
		clear();
		flag = true;
	}
	if($("#txtDrawnDate").val().trim()== "")
	{
		$("#txtSpecimenIdError").show();
		$("#txtSpecimenIdError").text("Please select load button");
		
		flag = true;
	}
	if(flag == true){
		return false;
	}
	else{
		if(files == undefined){
			var formData = $('#radiologyResultForm').serializeArray();
			$.post("controllers/admin/radiology_result.php", formData).done(function (data) {
				if(data){
					$('#messageMyModalLabel').text("Success");
					$('.modal-body').html('Radiology result saved successfully!!!');
					$('#messagemyModal').modal();
					$('#txtSpecimenId').focus();
					radilologyclear();
				}
			});
		}
		if(files != undefined){
			var form_data = new FormData();
			for (var i = 0; i < files.length; i++) {
				 fileup = files[i];
				 form_data.append('filename[]',fileup,fileup.name);
			}
		    var imageName = "";
			$.ajax({
				url: 'controllers/admin/radiology_result.php', // point to server-side PHP script 
				dataType: 'text', // what to expect back from the PHP script, if anything
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(data) {
					if (data == "invalid file") {
					   /*  $("#errorimage").show();
						$("#errorimage").text("Please upload the image of less than 2mb size.");
						$("#advanced-first").show();
						$("#advanced-Second").hide(); */
					} else {
						var parseData = jQuery.parseJSON(data);
						for (var i = 0; i < parseData.length; i++) {
							img = parseData[i];
							imageName = imageName+img+";";
						}
						$('#operation').val("saveResult");
						var imageArray = new Object();
						imageArray.name = "radioImage";
						imageArray.value = imageName;
						
						var formData = $('#radiologyResultForm').serializeArray();
						formData.push(imageArray);
						
						$.post("controllers/admin/radiology_result.php", formData).done(function (data) {
							if(data){
								$('#messageMyModalLabel').text("Success");
								$('.modal-body').html('Radiology result saved successfully!!!');
								$('#messagemyModal').modal();
								$('#txtSpecimenId').focus();
								radilologyclear();
							}
						});
					}
				},
			});
		}
	}
}
//function to load Specimen Results
function loadSpecimenResults(){
	var flag = false;
		if($("#txtSpecimenId").val().trim()== "")
		{
			$("#txtSpecimenIdError").show();
			$("#txtSpecimenIdError").text("Please enter request ID");
			$("#txtSpecimenId").addClass("errorStyle");
			$('#txtSpecimenId').focus();
			radilologyclear();
			flag = true;
		}
		if(flag == true){
			return false;
		}
		else{
			var specimenId =$("#txtSpecimenId").val();
			var postData = {
				"operation":"loadSpecimen",
				"specimenId":specimenId
			}
			
			$.ajax({					
				type: "POST",
				cache: false,
				url: "controllers/admin/radiology_result.php",
				datatype:"json",
				data: postData,
				
				success: function(data) {
					if(data != "0" && data != ""){
						var parseData = jQuery.parseJSON(data);
						if(parseData == ""){
							$("#txtSpecimenIdError").show();
							$("#txtSpecimenIdError").text("Please enter a valid specimen ID");
							$("#txtSpecimenId").addClass("errorStyle");
							$('#txtSpecimenId').focus();
							return false;
						}
						
						
						$('#txtDrawnDate').val(convertTimestamp(parseData[0]['request_date']));
						$('#txtLabTest').val(parseData[0]['name']);
						/* if(parseData[0]['rangeNormalModified'] != "" && parseData[0]['rangeNormalModified'] != null){
							$('#txtNormalRange').val(parseData[0]['rangeNormalModified']);
						}
						else{
							$('#txtNormalRange').val(parseData[0]['rangeNormal']);
						} */
						
						/* $('#txtResult').val(parseData[0]['result']); */
						
						if(parseData[0]['report'] != "" && parseData[0]['report'] != null)
						{
							$('#txtReport').val(parseData[0]['report']);
						}
						else{
							$('#txtReport').val("");
						}	
					}
					if(data == ""){
							$("#txtSpecimenIdError").show();
							$("#txtSpecimenIdError").text("Please enter a valid request ID");
							$("#txtSpecimenId").addClass("errorStyle");
							$('#txtSpecimenId').focus();
					}
				},
				error:function() {
					alert('error');
				}
			});
		}
}
// key press event on ESC button
$(document).keyup(function(e) {
     if (e.keyCode == 27) { 
		 $(".close").click();
    }
});
function convertTimestamp(timestamp) {
  var d = new Date(timestamp * 1000),	// Convert the passed timestamp to milliseconds
		yyyy = d.getFullYear(),
		mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
		dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
		hh = d.getHours(),
		h = hh,
		min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
		ampm = 'AM',
		time;
			
	if (hh > 12) {
		h = hh - 12;
		ampm = 'PM';
	} else if (hh === 12) {
		h = 12;
		ampm = 'PM';
	} else if (hh == 0) {
		h = 12;
	}
	
	// ie: 2013-02-18, 8:35 AM	
	time = yyyy + '-' + mm + '-' + dd + ' ' + h + ':' + min + ' ' + ampm;
		
	return time;
}
function radilologyclear(){
	$('#radiologyResultForm')[0].reset();
	$('#ddlResultFlag option:contains("N/A")').attr('selected', 'selected');
	$('#txtSpecimenDescription').val("");
	$('#txtDrawnDate').val("");
	$('#txtLabTest').val("");
	/* $('#txtNormalRange').val("");
	$('#txtResult').val(""); */
	$('#txtReport').val("");
	$('#upload').val("");
	 $('.imageLabel').remove();
	$('#fileLabel').text("No file choosen.");
}