/**********************************************************************************************/
/**********************************************************************************************/
/**********************************************************************************************/
/* Hidden field for this screen is inventoryPaymentHdnId used to do some operation***************/
/***********************i.e. being used in view_order screen**************************/
/**********************************************************************************************/
/**********************************************************************************************/

var inventoryPaymentTable;
var getSellerId = '';
$(document).ready(function(){
	debugger;
	bindSeller();
	$(".amountPrefix").text(amountPrefix);
	$("#searchorderIcon").unbind();
	$("#searchorderIcon").on('click',function(){
		$('#txtReceiveOrderIdError').text("");
		$('#txtReceiveOrderId').removeClass("errorStyle");
        $($('.myModaldata')[0]).load('views/admin/view_orders.html',function(){        	
			$("#receiveOrderDetails #txtSellerId").val(getSellerId);
	        $("#receiveOrderDetails #btnSearch").click();
        }); 
        $('#receiveOrderDetails').modal();    

	});
	inventoryPaymentTable = $('#tblInventoryPayment').dataTable( {
		"bFilter": true,
		"processing": true,
		"bPaginate": false,
		"bAutoWidth" : false,
	});

	removeErrorMessage();// remove error messaages
	$('#txtReceiveOrderId').keyup(function() {
		if($('#txtReceiveOrderId').val != ''){
			$('#txtReceiveOrderIdError').text('');
		}
	});

	$("#selSellerId").change(function(){
		if($(this).val() !=-1 && $(this).val() !=""){
			getSellerId = $(this).val();
		}
	});

	 removeErrorMessage();// remove error messaages

	$('#btnShow').click(function(){
		var flag = false;

        if(validTextField('#txtReceiveOrderId','#txtReceiveOrderIdError','Please enter order id value') == true)
        {
            flag = true;
        }
        if(validTextField('#selSellerId','#selSellerIdError','Please select seller Id') == true)
        {
            flag = true;
        }
        if(flag == true) {
			return false;
		}

		var orderId = $('#txtReceiveOrderId').val();

		var postData = {
		"operation": "showOrderDetails",
		"orderId": orderId
		}
		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/inventory_payment.php",
			datatype: "json",
			data: postData,

			success:function(data) {
				if (data !='') {
		        	dataSet = JSON.parse(data);
					inventoryPaymentTable.fnClearTable();
					inventoryPaymentTable.fnDestroy();
					inventoryPaymentTable = $('#tblInventoryPayment').DataTable( { //Reinitialize the data table 
						"bSort": false,
						"bPaginate": false,
						"bAutoWidth" : false,
						"fnDrawCallback": function ( oSettings ) {
							var cells = 0;
							calRowLenght = inventoryPaymentTable.fnGetNodes();
							for(var i=2;i<=calRowLenght.length + 1;i++)
							{	
								var tempCell = $($(".tdTotal")[i]).text();
								cells += parseInt(tempCell);									
							} 
							$("#totalBill").text(cells);
															
							
							//change function for checkbox in data table
							$(".chkbox").on('change',function(){
								var amount = parseInt($(this).attr('data-amount'));
								var checkBox = $(this);
								var checkBoxTd = $(this).parent();
								if($(this).is(":checked")){
									$(this).closest("tr").css('background-color','#ff6666');
									cells = cells - amount;
									$("#totalBill").text(cells);
								}
								else{
									$(this).closest("tr").css('background-color','#E7E3E4');
									cells = cells + amount;
									$("#totalBill").text(cells);
								}									
							});									
						},
						"aaData": dataSet,						
						"aoColumns": [ // set the data into data table
							{ "mData": "item_name" },
							{ "mData": "unit_cost"},
							{ "mData": "quantity" },
							{
							  "mData": function (o) { 	//function for checkbox			
								var data = o;
								var unitCost = data["unit_cost"];	
								var qty = data["quantity"];	
								var id = data["id"];
								var amount = unitCost*qty;								
								return "<span  class='visitDetails' data-id="+id+" style='color: black !important;'>"+amount+"</span>"; 
								},
								"sClass": "tdTotal"
							},
							{
							  "mData": function (o) { 	//function for checkbox			
								var data = o;
								var unitCost = data["unit_cost"];	
								var qty = data["quantity"];	
								var id = data["id"];	 
								var amount = unitCost*qty;
								var ledger = data['ledger'];
								var accPayable = data['account_payable'];
								var glAccount = data['gl_account'];
								return "<input type='checkbox'  class='chkbox' data-id="+id+" data-accPayable="+glAccount+" data-amount="+amount+" data-ledger="+ledger+" data-glAcc="+glAccount+" style='background: #0C71C8; color: #fff; padding: 3px 12px; border: none; border-radius: 3px; margin:0 auto; position:relative;'"+					 
								   " style='font-size: 22px; cursor:pointer;'>"; 
								},
								"sClass": "tdCheckbox" 
							},
						],						
					});
				}

			},
			error:function(){

			}

		});		
	});

	$("#btnSave").click(function(){

		var flag = false;
		
		if(validTextField('#SelPaymentMode','#SelPaymentModeError','Please select payment mode') == true)
        {
            flag = true;
        }
        if(validTextField('#txtAmount','#txtAmountError','Please enter amount') == true)
        {
            flag = true;
        }
        
		if($('.chkbox:checked').length == inventoryPaymentTable.fnGetData().length){
			callSuccessPopUp("Success","Please include any item name");
			return false;
		}
		if(flag == true){
			return false;
		}
		if(parseInt($('#txtAmount').val()) < parseInt($('#totalBill').text())) {
            $('#txtAmountError').text("please enter valid amount");
            return false;
        }

        var aiRows =  inventoryPaymentTable.$(".chkbox:unchecked", {"page": "all"});
        var arr = [];
        $.each(aiRows,function(i,v){
        	var obj = {};
        	obj.id = $(v).attr('data-id');
        	obj.ledger = $(v).attr('data-ledger');
        	obj.glAccount = $(v).attr('data-glAcc');
        	obj.accPayable = $(v).attr('data-accPayable');
        	obj.itemAmount = $(v).parent().siblings().last().text();
        	arr.push(obj);
        });


		var sellerId = $("#selSellerId").val();
		var orderId = $("#txtReceiveOrderId").val();
		var paymentMode = $("#SelPaymentMode").val();
		var amount = $("#txtAmount").val();
		 
		var postData = {
			"operation":"save",
			"sellerId":sellerId,
			"orderId":orderId,
			"paymentMode":paymentMode,
			"arr": arr,
			"amount":amount
		}
		$.ajax({
			type: "POST",
			cache: false,
			url: "controllers/admin/inventory_payment.php",
			datatype:"json",
			data: postData,
			
			success: function(data) {
				if(data != "0" && data != ""){
					callSuccessPopUp("Success","saved  sucessfully");
					inventoryPaymentTable.fnClearTable();
					 clearFormDetails("#inventoryPayment");
				}
			},
			error : function(){
				
			}
		});
		
	});
	$("#btnReset").click(function() {
       clearFormDetails("#inventoryPayment");
       $('#txtReceiveOrderIdError').text('');
       removeErrorMessage();
    });
});

// define function for bind department
function bindSeller() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/inventory_payment.php",
        data: {
            "operation": "showseller"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value=''>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                $('#selSellerId').html(option);
            }
        },
        error: function() {}
    });
}