<?php
/*
 * File Name    :   login.php
 * Company Name :   Qexon Infotech
 * Created By   :   Rupesh
 * Created Date :   31th dec, 2015
 * Description  :   This page use check login for user
 */
session_start(); 							// start session
if(isset($_COOKIE['u_id']) || isset($_COOKIE['u_pass']) || isset($_SESSION['globaluser'])){		// check user id from session	
	header('Location: index.php');
}
?>
<html>
<head>
    <link rel="stylesheet" href="./css/bootstrap.min.css" />
    <link rel="stylesheet" href="./css/plugins.css" />
    <link rel="stylesheet" href="./css/main.css" />
    <link rel="stylesheet" href="./css/themes.css" />
    <link rel="stylesheet" href="css/new_log_in.css" type="text/css">
    <style type="text/css">body{overflow: hidden;}</style>
</head>
        
<body>
<div id="page-content" style="padding:0px;margin:0px;">
   <div class="block">
    	<div class="row" style="margin:10%">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <div class="col-md-4">
                	<div id="logo">
                    	<p>Logo hospital</p>
                    </div>
                </div>
                <div class="col-md-10">
					<form id="new_login_form" method="post" action="checklogin.php">
					   <div id="top">Login to Health ERP</div>
							<div id="form-group-container">
								<div class="form-group">
									<div class="input-group">
										<div class="icon_container"><img src="./img/email_icon.png"></div>
										<input type="email" id="" name="depemail" class="form-control" 
										placeholder="Enter Email.." style="width:94% !important;">
									</div>                       
								</div> 
								<div class="form-group">
									<div class="input-group">
										<div class="icon_container"><img src="./img/password_icon.png"></div>
									   <input type="password" id="deppassword" name="deppwd" class="form-control"
										placeholder="Enter Password.." style="width:94% !important;">
									</div>                       
								</div>
								<div class="form-group">
									<div class="input-group">
									<span style="text-align:center;color:#FF0000;"><?php if(isset($_SESSION['error'])){ if($_SESSION['error'] == "1"){echo "Email/Password are incorrect.";$_SESSION['error']=""; } } ?></span>
									</div>
								</div>
								
								<div class="remember-checked">
								
								<label for="checked-tick">Keep me logged in</label>
									<input type="checkbox" id="checked-tick"  name="remember" value="on" class="checkbox">
									<a id="forgetPWD" href="forgetpwd.php">Forgotten your password?</a>
								</div>
								<div class="clear">
								</div>
								<input type="submit" class="send_button" value="Log In" name="send">
							</div>  
					</form>      
                </div>
            </div>
        </div>    
    </div> 	
</div>
</body>
<!-- END Page Content -->
<?php include 'footer.php'; ?>