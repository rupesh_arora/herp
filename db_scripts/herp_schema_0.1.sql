-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.0.17-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for herp
CREATE DATABASE IF NOT EXISTS `herp` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `herp`;


-- Dumping structure for table herp.allergies
CREATE TABLE IF NOT EXISTS `allergies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `allergy_cat_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `reaction_element` varchar(255) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_allergy_cat_id` (`allergy_cat_id`),
  CONSTRAINT `fk_allergy_cat_id` FOREIGN KEY (`allergy_cat_id`) REFERENCES `allergies_categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.allergies_categories
CREATE TABLE IF NOT EXISTS `allergies_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `description` text,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.allergy_request
CREATE TABLE IF NOT EXISTS `allergy_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL DEFAULT '0',
  `visit_id` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `allergy_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK__patients_allergy` (`patient_id`),
  KEY `FK__visits_allergy` (`visit_id`),
  KEY `FK__users_allergy` (`created_by`),
  KEY `FK__allergies_allergy` (`allergy_id`),
  CONSTRAINT `FK__allergies_allergy` FOREIGN KEY (`allergy_id`) REFERENCES `allergies` (`id`),
  CONSTRAINT `FK__patients_allergy` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `FK__users_allergy` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK__visits_allergy` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.beds
CREATE TABLE IF NOT EXISTS `beds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ward_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `cost` double NOT NULL,
  `price` double NOT NULL,
  `number` varchar(30) NOT NULL,
  `description` text,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `ward_id` (`ward_id`),
  KEY `room_id` (`room_id`),
  CONSTRAINT `FK_beds_rooms` FOREIGN KEY (`room_id`) REFERENCES `ward_room` (`id`),
  CONSTRAINT `beds_ibfk_1` FOREIGN KEY (`ward_id`) REFERENCES `wards` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.cash_account
CREATE TABLE IF NOT EXISTS `cash_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `credit` double DEFAULT NULL,
  `debit` double DEFAULT NULL,
  `created_on` int(20) NOT NULL,
  `created_by` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_patient_id` (`patient_id`),
  KEY `cash_amount_1` (`created_by`),
  CONSTRAINT `cash_amount_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_patient_id` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.clinical_findings
CREATE TABLE IF NOT EXISTS `clinical_findings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visit_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `presenting_complaint` varchar(255) NOT NULL,
  `general_appearance` varchar(255) DEFAULT NULL,
  `pv_pr` varchar(255) NOT NULL,
  `respiratory` varchar(255) NOT NULL,
  `psychological_status` varchar(255) DEFAULT NULL,
  `cvs` varchar(255) NOT NULL,
  `ent` varchar(255) NOT NULL,
  `abdomen` varchar(255) NOT NULL,
  `pmh_psh` varchar(255) NOT NULL,
  `cns` varchar(255) NOT NULL,
  `poh` varchar(255) NOT NULL,
  `eye` varchar(255) NOT NULL,
  `pgh` varchar(255) NOT NULL,
  `muscular_skeletal` varchar(255) NOT NULL,
  `fsh` varchar(255) NOT NULL,
  `skin` varchar(255) NOT NULL,
  `clinical_notes` varchar(255) NOT NULL,
  `provisional_diagnosis` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cf_visit_id` (`visit_id`),
  KEY `fk_cf_patient_id` (`patient_id`),
  CONSTRAINT `fk_cf_patient_id` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `fk_cf_visit_id` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`),
  CONSTRAINT `fk_visit_id` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.configurations
CREATE TABLE IF NOT EXISTS `configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `value` text NOT NULL,
  `remarks` text,
  `status` enum('A','I') DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.consulation_type
CREATE TABLE IF NOT EXISTS `consulation_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `cost` double NOT NULL,
  `tax` double NOT NULL,
  `other_charges` double NOT NULL,
  `other_charges_detail` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`),
  KEY `consulation_type_ibfk_1` (`created_by`),
  KEY `consulation_type_ibfk_2` (`updated_by`),
  CONSTRAINT `consulation_type_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `consulation_type_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.department
CREATE TABLE IF NOT EXISTS `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(50) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `department` (`department`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.designation
CREATE TABLE IF NOT EXISTS `designation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(50) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `designation` (`designation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.diagnosis
CREATE TABLE IF NOT EXISTS `diagnosis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diagnosis_cat_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_diagnosis_cat_id` (`diagnosis_cat_id`),
  CONSTRAINT `fk_diagnosis_cat_id` FOREIGN KEY (`diagnosis_cat_id`) REFERENCES `diagnosis_categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.diagnosis_categories
CREATE TABLE IF NOT EXISTS `diagnosis_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.diseases
CREATE TABLE IF NOT EXISTS `diseases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `code` (`code`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `diseases_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `diseases_categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.diseases_categories
CREATE TABLE IF NOT EXISTS `diseases_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `description` text,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `category` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.drugs
CREATE TABLE IF NOT EXISTS `drugs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `drug_id` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `alternate_name` varchar(50) DEFAULT NULL,
  `drug_code` varchar(50) NOT NULL,
  `drug_category_id` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `cost` double NOT NULL,
  `price` double NOT NULL,
  `alternate_drug_id` int(11) DEFAULT NULL,
  `composition` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `drug_code` (`drug_code`),
  KEY `drug_category_id` (`drug_category_id`),
  KEY `unit` (`unit`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `drugs_ibfk_1` FOREIGN KEY (`drug_category_id`) REFERENCES `drug_categories` (`id`),
  CONSTRAINT `drugs_ibfk_2` FOREIGN KEY (`unit`) REFERENCES `units` (`id`),
  CONSTRAINT `drugs_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `drugs_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.drug_categories
CREATE TABLE IF NOT EXISTS `drug_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `description` text,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `category` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.forensic_tests
CREATE TABLE IF NOT EXISTS `forensic_tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `specimen_id` int(11) NOT NULL,
  `test_code` varchar(255) DEFAULT 'Test Code',
  `range` varchar(50) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `fee` double NOT NULL,
  `description` text NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) DEFAULT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `FK_forensic_tests_specimens_types` (`specimen_id`),
  KEY `FK_forensic_tests_units` (`unit_id`),
  CONSTRAINT `FK_forensic_tests_specimens_types` FOREIGN KEY (`specimen_id`) REFERENCES `specimens_types` (`id`),
  CONSTRAINT `FK_forensic_tests_units` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.forensic_test_request
CREATE TABLE IF NOT EXISTS `forensic_test_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `forensic_test_id` int(11) NOT NULL,
  `request_date` int(20) NOT NULL,
  `pay_status` enum('paid','unpaid','settled') NOT NULL DEFAULT 'unpaid',
  `test_status` enum('pending','done','resultsent') NOT NULL DEFAULT 'pending',
  `cost` double NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK__patients_fk_3` (`patient_id`),
  KEY `FK__visits_fk_3` (`visit_id`),
  KEY `FK__forensic_tests_fk_3` (`forensic_test_id`),
  CONSTRAINT `FK__forensic_tests_fk_3` FOREIGN KEY (`forensic_test_id`) REFERENCES `forensic_tests` (`id`),
  CONSTRAINT `FK__patients_fk_3` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `FK__visits_fk_3` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.forensic_test_result
CREATE TABLE IF NOT EXISTS `forensic_test_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `specimen_id` int(11) DEFAULT NULL,
  `result_flag` enum('Low','Normal''High','N/A') DEFAULT NULL,
  `report` text NOT NULL,
  `created_on` int(11) DEFAULT NULL,
  `updated_on` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__patient_specimen1` (`specimen_id`),
  KEY `FK__users_11` (`created_by`),
  KEY `FK__users_31` (`updated_by`),
  CONSTRAINT `FK__patient_specimen1` FOREIGN KEY (`specimen_id`) REFERENCES `patient_specimen` (`id`),
  CONSTRAINT `FK__users_11` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK__users_31` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.hospital_profile
CREATE TABLE IF NOT EXISTS `hospital_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(100) NOT NULL,
  `hospital_name` text NOT NULL,
  `address` text NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `images_path` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `FK__locations` (`country_id`),
  KEY `FK__locations_2` (`state_id`),
  KEY `FK__locations_3` (`city_id`),
  CONSTRAINT `FK__locations` FOREIGN KEY (`country_id`) REFERENCES `locations` (`location_id`),
  CONSTRAINT `FK__locations_2` FOREIGN KEY (`state_id`) REFERENCES `locations` (`location_id`),
  CONSTRAINT `FK__locations_3` FOREIGN KEY (`city_id`) REFERENCES `locations` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.ipd_cases
CREATE TABLE IF NOT EXISTS `ipd_cases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__patients_ipd` (`patient_id`),
  KEY `FK__department_ipd` (`department_id`),
  KEY `FK__visits_ipd` (`visit_id`),
  CONSTRAINT `FK__department_ipd` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  CONSTRAINT `FK__patients_ipd` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `FK__visits_ipd` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.lab_tests
CREATE TABLE IF NOT EXISTS `lab_tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `lab_type_id` int(11) NOT NULL,
  `specimen_id` int(11) NOT NULL,
  `test_code` varchar(255) DEFAULT 'Test Code',
  `range` varchar(50) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `fee` double NOT NULL,
  `description` tinytext,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `lab_type_id` (`lab_type_id`),
  KEY `specimen_id` (`specimen_id`),
  KEY `FK_lab_tests_units` (`unit_id`),
  CONSTRAINT `FK_lab_tests_units` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`),
  CONSTRAINT `lab_tests_ibfk_1` FOREIGN KEY (`lab_type_id`) REFERENCES `lab_types` (`id`),
  CONSTRAINT `lab_tests_ibfk_2` FOREIGN KEY (`specimen_id`) REFERENCES `specimens_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.lab_test_requests
CREATE TABLE IF NOT EXISTS `lab_test_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `lab_test_id` int(11) NOT NULL,
  `request_date` int(20) NOT NULL,
  `pay_status` enum('paid','unpaid','settled') NOT NULL DEFAULT 'unpaid',
  `test_status` enum('pending','done','resultsent') NOT NULL DEFAULT 'pending',
  `cost` double NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK__patient_fk_1` (`patient_id`),
  KEY `FK__visit_fk_1` (`visit_id`),
  KEY `FK__lab_fk_1` (`lab_test_id`),
  CONSTRAINT `FK__lab_fk_1` FOREIGN KEY (`lab_test_id`) REFERENCES `lab_tests` (`id`),
  CONSTRAINT `FK__patient_fk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `FK__visit_fk_1` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.lab_test_result
CREATE TABLE IF NOT EXISTS `lab_test_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `specimen_id` int(11) DEFAULT NULL,
  `result_flag` enum('Low','Normal','N/A','High') DEFAULT NULL,
  `report` text NOT NULL,
  `created_on` int(11) DEFAULT NULL,
  `updated_on` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__patient_specimen` (`specimen_id`),
  KEY `FK__users_1` (`created_by`),
  KEY `FK__users_3` (`updated_by`),
  CONSTRAINT `FK__patient_specimen` FOREIGN KEY (`specimen_id`) REFERENCES `patient_specimen` (`id`),
  CONSTRAINT `FK__users_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK__users_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.lab_types
CREATE TABLE IF NOT EXISTS `lab_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.locations
CREATE TABLE IF NOT EXISTS `locations` (
  `location_id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `location_type` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `is_visible` int(11) NOT NULL,
  `country_code` int(11) DEFAULT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.opd_consultants
CREATE TABLE IF NOT EXISTS `opd_consultants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opd_room_id` int(11) NOT NULL,
  `consultant_id` int(11) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK_opd_consultants_opd_rooms` (`opd_room_id`),
  KEY `FK_opd_consultants_users` (`consultant_id`),
  CONSTRAINT `FK_opd_consultants_opd_rooms` FOREIGN KEY (`opd_room_id`) REFERENCES `opd_rooms` (`id`),
  CONSTRAINT `FK_opd_consultants_users` FOREIGN KEY (`consultant_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `screen_name` varchar(30) NOT NULL,
  `screen_load_url` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.pages_options
CREATE TABLE IF NOT EXISTS `pages_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `screen_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `edit` tinyint(1) NOT NULL DEFAULT '0',
  `update` tinyint(1) NOT NULL DEFAULT '0',
  `delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_screen_id` (`screen_id`),
  KEY `FK_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.patients
CREATE TABLE IF NOT EXISTS `patients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salutation` varchar(10) DEFAULT '0',
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) NOT NULL,
  `dob` date DEFAULT NULL,
  `father_name` varchar(50) NOT NULL,
  `gender` enum('M','F','NM') NOT NULL DEFAULT 'NM',
  `care_of` varchar(50) DEFAULT NULL,
  `care_contact` varchar(20) DEFAULT NULL,
  `address` text,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `family_details` tinytext,
  `email` varchar(50) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `ref_source` varchar(50) DEFAULT NULL,
  `panel_details` varchar(50) DEFAULT NULL,
  `id_proof` varchar(50) DEFAULT NULL,
  `id_proof_file_name` varchar(50) DEFAULT NULL,
  `biometric_data` text,
  `remarks` text,
  `password` varchar(285) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  `images_path` varchar(300) DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_proof_file_name` (`id_proof_file_name`),
  KEY `patients_ibfk_1` (`created_by`),
  KEY `patients_ibfk_2` (`updated_by`),
  KEY `FK_countryid` (`country_id`),
  KEY `FK_stateid` (`state_id`),
  KEY `FK_cityid` (`city_id`),
  CONSTRAINT `FK_cityid` FOREIGN KEY (`city_id`) REFERENCES `locations` (`location_id`),
  CONSTRAINT `FK_countryid` FOREIGN KEY (`country_id`) REFERENCES `locations` (`location_id`),
  CONSTRAINT `FK_stateid` FOREIGN KEY (`state_id`) REFERENCES `locations` (`location_id`),
  CONSTRAINT `patients_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `patients_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.patients_prescription_request
CREATE TABLE IF NOT EXISTS `patients_prescription_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visit_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `drug_id` int(11) NOT NULL,
  `quantity` int(20) NOT NULL,
  `issued_on` date NOT NULL,
  `dosage` varchar(50) NOT NULL,
  `duration` varchar(50) NOT NULL,
  `notes` varchar(250) NOT NULL,
  `dispense_status` enum('Dispensed','Pending') NOT NULL DEFAULT 'Pending',
  PRIMARY KEY (`id`),
  KEY `FK__visits12` (`visit_id`),
  KEY `FK__patients12` (`patient_id`),
  KEY `FK__drugs12` (`drug_id`),
  CONSTRAINT `FK__drugs12` FOREIGN KEY (`drug_id`) REFERENCES `drugs` (`id`),
  CONSTRAINT `FK__patients12` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `FK__visits12` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.patient_bill_payment
CREATE TABLE IF NOT EXISTS `patient_bill_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount_tendered` int(11) NOT NULL,
  `receipt_no` int(11) NOT NULL,
  `payment_mode` varchar(50) NOT NULL,
  `visit_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `receipt_no` (`receipt_no`),
  KEY `FK__visits_id` (`visit_id`),
  CONSTRAINT `FK__visits_id` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.patient_diagnosis_request
CREATE TABLE IF NOT EXISTS `patient_diagnosis_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL DEFAULT '0',
  `visit_id` int(11) NOT NULL DEFAULT '0',
  `diagnosis_id` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK__patients_diagnosis` (`patient_id`),
  KEY `FK__visits_diagnosis` (`visit_id`),
  KEY `FK3_created_by_diagnosis` (`created_by`),
  CONSTRAINT `FK3_created_by_diagnosis` FOREIGN KEY (`created_by`) REFERENCES `session` (`id`),
  CONSTRAINT `FK__patients_diagnosis` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `FK__visits_diagnosis` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.patient_forensic_bill
CREATE TABLE IF NOT EXISTS `patient_forensic_bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `forensic_test_id` int(11) NOT NULL,
  `charge` int(20) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('paid','settled','unpaid') NOT NULL DEFAULT 'unpaid',
  PRIMARY KEY (`id`),
  KEY `FK__patient` (`patient_id`),
  KEY `FK__forensic_test_id` (`forensic_test_id`),
  KEY `FK__visits_1` (`visit_id`),
  KEY `forensic_bill_ibfk_1` (`created_by`),
  KEY `forensic_billnts_ibfk_2` (`updated_by`),
  CONSTRAINT `FK__forensic_test_id` FOREIGN KEY (`forensic_test_id`) REFERENCES `forensic_tests` (`id`),
  CONSTRAINT `FK__patient` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `FK__visits_1` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`),
  CONSTRAINT `forensic_bill_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `forensic_billnts_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.patient_lab_bill
CREATE TABLE IF NOT EXISTS `patient_lab_bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `lab_test_id` int(11) NOT NULL,
  `charge` int(20) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('paid','settled','unpaid') NOT NULL DEFAULT 'unpaid',
  PRIMARY KEY (`id`),
  KEY `FK__patients_id1` (`patient_id`),
  KEY `FK__lab_tests_1` (`lab_test_id`),
  KEY `FK__visits_id1` (`visit_id`),
  KEY `lab_bill_ibfk_1` (`created_by`),
  KEY `lab_bill_ibfk_2` (`updated_by`),
  CONSTRAINT `FK__lab_tests_1` FOREIGN KEY (`lab_test_id`) REFERENCES `lab_tests` (`id`),
  CONSTRAINT `FK__patients_id1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `FK__visits_id1` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`),
  CONSTRAINT `lab_bill_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `lab_bill_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.patient_pharmacy_bill
CREATE TABLE IF NOT EXISTS `patient_pharmacy_bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `amount` int(20) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('paid','settled','unpaid') NOT NULL DEFAULT 'unpaid',
  PRIMARY KEY (`id`),
  KEY `FK__patient6` (`patient_id`),
  KEY `FK__visits_6` (`visit_id`),
  KEY `pharmacy_bill_ibfk_1` (`created_by`),
  KEY `pharmacy_bill_ibfk_2` (`updated_by`),
  CONSTRAINT `FK__patient6` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `FK__visits_6` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`),
  CONSTRAINT `pharmacy_bill_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `pharmacy_bill_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.patient_procedure_bill
CREATE TABLE IF NOT EXISTS `patient_procedure_bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL DEFAULT '0',
  `procedure_id` int(11) NOT NULL,
  `charge` int(20) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('paid','settled','unpaid') NOT NULL DEFAULT 'unpaid',
  PRIMARY KEY (`id`),
  KEY `FK_patient_procedure_bill_patients` (`patient_id`),
  KEY `FK_patient_procedure_bill_procedures` (`procedure_id`),
  KEY `FK_patient_procedure_bill_visits` (`visit_id`),
  KEY `procedure_bill_ibfk_1` (`created_by`),
  KEY `procedure_bill_ibfk_2` (`updated_by`),
  CONSTRAINT `FK_patient_procedure_bill_patients` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `FK_patient_procedure_bill_procedures` FOREIGN KEY (`procedure_id`) REFERENCES `procedures` (`id`),
  CONSTRAINT `FK_patient_procedure_bill_visits` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`),
  CONSTRAINT `procedure_bill_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `procedure_bill_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.patient_radiology_bill
CREATE TABLE IF NOT EXISTS `patient_radiology_bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `radiology_test_id` int(11) NOT NULL,
  `charge` int(20) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('paid','settled','unpaid') NOT NULL DEFAULT 'unpaid',
  PRIMARY KEY (`id`),
  KEY `FK__patients1` (`patient_id`),
  KEY `FK__radiology_test_id` (`radiology_test_id`),
  KEY `FK__visits_id3` (`visit_id`),
  KEY `radiology_bill_ibfk_1` (`created_by`),
  KEY `radiology_bill_ibfk_2` (`updated_by`),
  CONSTRAINT `FK__patients1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `FK__radiology_test_id` FOREIGN KEY (`radiology_test_id`) REFERENCES `radiology_tests` (`id`),
  CONSTRAINT `FK__visits_id3` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`),
  CONSTRAINT `radiology_bill_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `radiology_bill_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.patient_service_bill
CREATE TABLE IF NOT EXISTS `patient_service_bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `charge` int(20) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('paid','settled','unpaid') NOT NULL DEFAULT 'unpaid',
  PRIMARY KEY (`id`),
  KEY `FK__patients` (`patient_id`),
  KEY `FK__services` (`service_id`),
  KEY `FK__visits_id2` (`visit_id`),
  KEY `service_bill_ibfk_1` (`created_by`),
  KEY `service_bill_ibfk_2` (`updated_by`),
  CONSTRAINT `FK__patients` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `FK__services` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`),
  CONSTRAINT `FK__visits_id2` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`),
  CONSTRAINT `service_bill_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `service_bill_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.patient_specimen
CREATE TABLE IF NOT EXISTS `patient_specimen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lab_test_id` int(11) DEFAULT NULL,
  `description` text NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  `created_on` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__lab_test_requests` (`lab_test_id`),
  KEY `FK__users` (`created_by`),
  CONSTRAINT `FK__lab_test_requests` FOREIGN KEY (`lab_test_id`) REFERENCES `lab_test_requests` (`id`),
  CONSTRAINT `FK__users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.pharmacy_inventory
CREATE TABLE IF NOT EXISTS `pharmacy_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `drug_name_id` int(11) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `manufacture_name` varchar(50) NOT NULL,
  `unit_vol` varchar(50) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `manufacture_date` date NOT NULL,
  `expiry_date` date NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK__drugs` (`drug_name_id`),
  KEY `FK_pharmacy_inventory_units` (`unit_id`),
  CONSTRAINT `FK__drugs` FOREIGN KEY (`drug_name_id`) REFERENCES `drugs` (`id`),
  CONSTRAINT `FK_pharmacy_inventory_units` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.procedures
CREATE TABLE IF NOT EXISTS `procedures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_name` varchar(50) DEFAULT NULL,
  `price` double NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.procedures_requests
CREATE TABLE IF NOT EXISTS `procedures_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `procedure_id` int(11) NOT NULL,
  `request_date` int(20) NOT NULL,
  `pay_status` enum('paid','unpaid','settled') NOT NULL DEFAULT 'unpaid',
  `test_status` enum('pending','done','resultsent') NOT NULL DEFAULT 'pending',
  `cost` double NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK__patients_fk_4` (`patient_id`),
  KEY `FK__visits_fk_4` (`visit_id`),
  KEY `FK__procedures_fk_4` (`procedure_id`),
  CONSTRAINT `FK__patients_fk_4` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `FK__procedures_fk_4` FOREIGN KEY (`procedure_id`) REFERENCES `procedures` (`id`),
  CONSTRAINT `FK__visits_fk_4` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.radiology_tests
CREATE TABLE IF NOT EXISTS `radiology_tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `specimen_id` int(11) NOT NULL,
  `test_code` varchar(255) DEFAULT 'Test Code',
  `range` varchar(50) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `fee` double NOT NULL,
  `description` text NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) DEFAULT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK_radiology_tests_specimens_types` (`specimen_id`),
  KEY `FK_radiology_tests_units` (`unit_id`),
  CONSTRAINT `FK_radiology_tests_specimens_types` FOREIGN KEY (`specimen_id`) REFERENCES `specimens_types` (`id`),
  CONSTRAINT `FK_radiology_tests_units` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.radiology_test_request
CREATE TABLE IF NOT EXISTS `radiology_test_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `radiology_test_id` int(11) NOT NULL,
  `request_date` int(20) NOT NULL,
  `pay_status` enum('paid','unpaid','settled') NOT NULL DEFAULT 'unpaid',
  `test_status` enum('pending','done','resultsent') NOT NULL DEFAULT 'pending',
  `cost` double NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK__patients_fk_2` (`patient_id`),
  KEY `FK__visits_fk_2` (`visit_id`),
  KEY `FK__radiology_tests_fk_2` (`radiology_test_id`),
  CONSTRAINT `FK__patients_fk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `FK__radiology_tests_fk_2` FOREIGN KEY (`radiology_test_id`) REFERENCES `radiology_tests` (`id`),
  CONSTRAINT `FK__visits_fk_2` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.radiology_test_result
CREATE TABLE IF NOT EXISTS `radiology_test_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `specimen_id` int(11) DEFAULT NULL,
  `result_flag` enum('Low','Normal''High','N/A') DEFAULT NULL,
  `report` text NOT NULL,
  `created_on` int(11) DEFAULT NULL,
  `updated_on` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__patient_specimen2` (`specimen_id`),
  KEY `FK__users_12` (`created_by`),
  KEY `FK__users_32` (`updated_by`),
  CONSTRAINT `FK__patient_specimen2` FOREIGN KEY (`specimen_id`) REFERENCES `patient_specimen` (`id`),
  CONSTRAINT `FK__users_12` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK__users_32` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.room
CREATE TABLE IF NOT EXISTS `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_type_id` int(11) NOT NULL,
  `number` varchar(30) NOT NULL,
  `description` text,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK_rooms_room_type` (`room_type_id`),
  CONSTRAINT `FK_room_type` FOREIGN KEY (`room_type_id`) REFERENCES `ward_room_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.room_type
CREATE TABLE IF NOT EXISTS `room_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `description` text,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.services
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `cost` double NOT NULL,
  `fee` double NOT NULL,
  `description` tinytext,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `FK_service_type_id` (`service_type_id`),
  CONSTRAINT `FK_service_type_id` FOREIGN KEY (`service_type_id`) REFERENCES `service_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.services_request
CREATE TABLE IF NOT EXISTS `services_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL DEFAULT '0',
  `visit_id` int(11) NOT NULL DEFAULT '0',
  `service_id` int(11) NOT NULL DEFAULT '0',
  `request_date` int(20) NOT NULL DEFAULT '0',
  `pay_status` enum('paid','unpaid','settled') NOT NULL DEFAULT 'unpaid',
  `test_status` enum('pending','done','resultsent') NOT NULL DEFAULT 'pending',
  `cost` double NOT NULL DEFAULT '0',
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK__pat` (`patient_id`),
  KEY `FK__vis` (`visit_id`),
  KEY `FK__ser` (`service_id`),
  CONSTRAINT `FK__pat` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `FK__ser` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`),
  CONSTRAINT `FK__vis` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.service_type
CREATE TABLE IF NOT EXISTS `service_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.session
CREATE TABLE IF NOT EXISTS `session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mac_address` varchar(32) NOT NULL,
  `ip_address` varchar(32) NOT NULL,
  `login_timestamp` int(20) NOT NULL,
  `status` varchar(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `session_ibfk_1` (`user_id`),
  CONSTRAINT `session_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.specimens_types
CREATE TABLE IF NOT EXISTS `specimens_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.staff_type
CREATE TABLE IF NOT EXISTS `staff_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  `user_code` int(11) NOT NULL DEFAULT '12',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.triage
CREATE TABLE IF NOT EXISTS `triage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visit_id` int(11) NOT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `weight` varchar(10) DEFAULT NULL,
  `temperature` varchar(10) DEFAULT NULL,
  `height` varchar(10) DEFAULT NULL,
  `pulse` varchar(10) DEFAULT NULL,
  `blood_pressure` varchar(10) DEFAULT NULL,
  `head_circum` varchar(10) DEFAULT NULL,
  `body_surface_area` varchar(10) DEFAULT NULL,
  `respiration_rate` varchar(10) DEFAULT NULL,
  `oxygen_saturation` varchar(10) DEFAULT NULL,
  `heart_rate` varchar(10) DEFAULT NULL,
  `bmi` varchar(10) DEFAULT NULL,
  `notes` varchar(10) DEFAULT NULL,
  `created_on` int(20) NOT NULL,
  `created_by` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_triage_visit_type` (`visit_id`),
  KEY `FK_triage_patients` (`patient_id`),
  KEY `FK_triage_users` (`created_on`),
  KEY `FK_triage_users_2` (`created_by`),
  CONSTRAINT `FK_triage_patients` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `FK_triage_users_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_triage_visit_type` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.units
CREATE TABLE IF NOT EXISTS `units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit` varchar(50) NOT NULL,
  `unit_abbr` varchar(20) NOT NULL,
  `description` text,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unit_abbr` (`unit_abbr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `gender` enum('M','F','NM') NOT NULL,
  `martial_status` enum('M','U','D','NM') DEFAULT NULL,
  `dob` date NOT NULL,
  `address` text NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `zip` varchar(15) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email_id` varchar(50) NOT NULL,
  `joining_date` date NOT NULL,
  `emergency_name` varchar(50) DEFAULT NULL,
  `emergency_contact` varchar(15) DEFAULT NULL,
  `referred_by` varchar(50) DEFAULT NULL,
  `staff_type_id` int(11) NOT NULL,
  `created_on` int(20) DEFAULT NULL,
  `updated_on` int(20) DEFAULT NULL,
  `id_proof_type` varchar(50) DEFAULT NULL,
  `id_proof_num` varchar(250) DEFAULT NULL,
  `department_id` int(11) NULL,
  `degree_held` text,
  `designation` int(11) NULL,
  `remarks` text,
  `images_path` varchar(300) DEFAULT NULL,
  `password` varchar(285) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `salutation` varchar(10) DEFAULT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_id` (`email_id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `FK_city_id` (`city_id`),
  KEY `FK_country_id` (`country_id`),
  KEY `FK_state_id` (`state_id`),
  KEY `FK_designation` (`department_id`),
  KEY `FK_staff_type` (`staff_type_id`),
  KEY `FK_designation_id` (`designation`),
  CONSTRAINT `FK_city_id` FOREIGN KEY (`city_id`) REFERENCES `locations` (`location_id`),
  CONSTRAINT `FK_country_id` FOREIGN KEY (`country_id`) REFERENCES `locations` (`location_id`),
  CONSTRAINT `FK_department_id` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  CONSTRAINT `FK_designation_id` FOREIGN KEY (`designation`) REFERENCES `designation` (`id`),
  CONSTRAINT `FK_staff_type` FOREIGN KEY (`staff_type_id`) REFERENCES `staff_type` (`id`),
  CONSTRAINT `FK_state_id` FOREIGN KEY (`state_id`) REFERENCES `locations` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.vision_assissment
CREATE TABLE IF NOT EXISTS `vision_assissment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visit_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `eye_status` varchar(30) DEFAULT NULL,
  `visualacuityright` varchar(30) DEFAULT NULL,
  `visualacuityleft` varchar(30) DEFAULT NULL,
  `visualacuityextright` varchar(30) DEFAULT NULL,
  `visualacuityextleft` varchar(30) DEFAULT NULL,
  `handmovementright` varchar(30) DEFAULT NULL,
  `handmovementleft` varchar(30) DEFAULT NULL,
  `preceptionright` varchar(30) DEFAULT NULL,
  `preceptionleft` varchar(30) DEFAULT NULL,
  `Comment` varchar(30) DEFAULT NULL,
  `notes` varchar(30) DEFAULT NULL,
  `created_on` int(20) NOT NULL,
  `created_by` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_vision_visit_type` (`visit_id`),
  KEY `FK_vision_patients` (`patient_id`),
  KEY `FK_vision_users` (`created_on`),
  KEY `FK_vision_users_2` (`created_by`),
  CONSTRAINT `FK_vision_patients` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `FK_vision_users_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_vision_visit_type` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.visits
CREATE TABLE IF NOT EXISTS `visits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visit_type` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `remark` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  `triage_status` enum('A','I') DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `visits_ibfk_2` (`patient_id`),
  KEY `visits_ibfk_3` (`created_by`),
  KEY `visits_ibfk_4` (`updated_by`),
  KEY `fk_visit_type` (`visit_type`),
  CONSTRAINT `FK_visits_visit_type` FOREIGN KEY (`visit_type`) REFERENCES `visit_type` (`id`),
  CONSTRAINT `fk_visit_type` FOREIGN KEY (`visit_type`) REFERENCES `visit_type` (`id`),
  CONSTRAINT `visits_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  CONSTRAINT `visits_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `visits_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.visit_type
CREATE TABLE IF NOT EXISTS `visit_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`),
  KEY `visit_type_ibfk_1` (`created_by`),
  KEY `visit_type_ibfk_2` (`updated_by`),
  CONSTRAINT `visit_type_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `visit_type_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.wards
CREATE TABLE IF NOT EXISTS `wards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `ward_type` enum('Male','Female') NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.ward_room
CREATE TABLE IF NOT EXISTS `ward_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ward_id` int(11) NOT NULL,
  `room_type_id` int(11) NOT NULL,
  `number` varchar(30) NOT NULL,
  `description` text,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `ward_id` (`ward_id`),
  KEY `FK_rooms_room_type` (`room_type_id`),
  CONSTRAINT `FK_rooms_room_type` FOREIGN KEY (`room_type_id`) REFERENCES `ward_room_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table herp.ward_room_type
CREATE TABLE IF NOT EXISTS `ward_room_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `description` text,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


-- Added by Shashank Gupta 30 Dec 2015 --
ALTER TABLE `pages_options` CHANGE `edit` `view` TINYINT(1) NOT NULL DEFAULT '0';


-- Added by Kamesh 31 Dec 2015 --
CREATE TABLE `patient_pharmacy_extra` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `patient_id` INT(11) NOT NULL,
 `visit_id` INT(11) NOT NULL,
 `drug_id` INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__patients_extra` (`patient_id`),
 INDEX `FK__visits_extra` (`visit_id`),
 INDEX `FK__drugs_extra` (`drug_id`),
 CONSTRAINT `FK__patients_extra` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
 CONSTRAINT `FK__visits_extra` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`),
 CONSTRAINT `FK__drugs_extra` FOREIGN KEY (`drug_id`) REFERENCES `drugs` (`id`)
)
ENGINE=InnoDB
;

-- Added by Rupesh 31 Dec 2015 --
SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `locations`
 CHANGE COLUMN `location_id` `location_id` INT(11) NOT NULL AUTO_INCREMENT FIRST;
SET FOREIGN_KEY_CHECKS=1;

-- Added by Kamesh 31 Dec 2015 --
ALTER TABLE `patient_pharmacy_extra` ADD COLUMN `quantity` VARCHAR(50) NOT NULL AFTER `drug_id`;

-- Added by Subodh Kant 31 Dec 2015 --

ALTER TABLE `hospital_profile` CHANGE `images_path` `images_path` VARCHAR(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'logo.gif';

-- Added by Tushar 04 Jan 2016 --
ALTER TABLE `hospital_profile`
 DROP COLUMN `logo`;
 
 -- Added by Rupesh 04 Jan 2016 --
 ALTER TABLE `patient_diagnosis_request`
 DROP FOREIGN KEY `FK3_created_by_diagnosis`;
ALTER TABLE `patient_diagnosis_request`
 ADD CONSTRAINT `FK3_created_by_diagnosis` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 ADD CONSTRAINT `FK_patient_diagnosis_request_diagnosis` FOREIGN KEY (`diagnosis_id`) REFERENCES `diagnosis` (`id`);
 
  -- Added by Rupesh 04 Jan 2016 --
 CREATE TABLE `opd_rooms` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `room_id` INT(11) NOT NULL,
 `consultant_id` INT(11) NOT NULL,
 `department_id` INT(11) NOT NULL,
 `created_on` INT(20) NOT NULL,
 `updated_on` INT(20) NOT NULL,
 `status` ENUM('A','I') NOT NULL DEFAULT 'A',
 PRIMARY KEY (`id`),
 INDEX `FK_opd_rooms_room` (`room_id`),
 INDEX `FK_opd_rooms_users` (`consultant_id`),
 INDEX `FK_opd_rooms_department` (`department_id`),
 CONSTRAINT `FK_opd_rooms_department` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
 CONSTRAINT `FK_opd_rooms_room` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
 CONSTRAINT `FK_opd_rooms_users` FOREIGN KEY (`consultant_id`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

-- Added by Kamesh Pathak 5 Jan 2016 --

ALTER TABLE `visits`
 ADD COLUMN `case_id` VARCHAR(50) NOT NULL AFTER `triage_status`;
 
 
 -- Added by Tushar 6 Jan 2016 --
 CREATE TABLE `patient_forensic_specimen` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `forensic_test_id` INT(11) NULL DEFAULT NULL,
 `description` TEXT NOT NULL,
 `status` ENUM('A','I') NOT NULL DEFAULT 'A',
 `created_on` INT(11) NULL DEFAULT NULL,
 `created_by` INT(11) NULL DEFAULT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__forensic_test_request` (`forensic_test_id`),
 INDEX `FK__users` (`created_by`),
 CONSTRAINT `FK__forensic_test_request` FOREIGN KEY (`forensic_test_id`) REFERENCES `forensic_test_request` (`id`),
 CONSTRAINT `FK_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

 -- Added by Tushar 6 Jan 2016 --
ALTER TABLE `forensic_test_result`
 DROP FOREIGN KEY `FK__patient_specimen1`;
ALTER TABLE `forensic_test_result`
 ADD CONSTRAINT `FK__patient_specimen1` FOREIGN KEY (`specimen_id`) REFERENCES `patient_forensic_specimen` (`id`);
 
 -- Added by Kamesh 6 Jan 2016 --
 ALTER TABLE `visits`
 CHANGE COLUMN `triage_status` `triage_status` ENUM('Taken','UnTaken') NULL DEFAULT 'UnTaken' AFTER `status`;
 
 -- Added by Tushar 6 Jan 2016 --
 
 ALTER TABLE `radiology_test_result`
 DROP COLUMN `result_flag`;
 
ALTER TABLE `radiology_test_result`
 DROP FOREIGN KEY `FK__patient_specimen2`;
 
ALTER TABLE `radiology_test_result`
 CHANGE COLUMN `specimen_id` `request_id` INT(11) NULL DEFAULT NULL AFTER `id`;

ALTER TABLE `radiology_test_result`
 ADD CONSTRAINT `FK_request_id1` FOREIGN KEY (`request_id`) REFERENCES `radiology_test_request` (`id`);
 
ALTER TABLE `radiology_test_result`
 ADD COLUMN `images_path` VARCHAR(300) NULL AFTER `report`;
 
 
 -- Added by Rupesh 6 Jan 2016 --
 SET foreign_key_checks = 0;
ALTER TABLE `visits`
 ADD COLUMN `opd_room_id` INT(11)  NULL AFTER `updated_on`,
 ADD COLUMN `consultant_id` INT(11)  NULL AFTER `opd_room_id`,
 ADD CONSTRAINT `FK_visits_users` FOREIGN KEY (`consultant_id`) REFERENCES `users` (`id`),
 ADD CONSTRAINT `FK_visits_opd_rooms` FOREIGN KEY (`opd_room_id`) REFERENCES `opd_rooms` (`id`);
SET foreign_key_checks = 1;

-- Added by Rupesh 6 Jan 2016 --
SET foreign_key_checks = 0;
ALTER TABLE `room`
 DROP FOREIGN KEY `FK_room_type`;
ALTER TABLE `room`
 ADD CONSTRAINT `FK_room_type` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`);
 SET foreign_key_checks = 1;
 
 
 -- Added by Kamesh 9 Jan 2016 --
 
 SET foreign_key_checks = 0;
ALTER TABLE `visits`
 DROP FOREIGN KEY `FK_visits_opd_rooms`;
ALTER TABLE `visits`
 ADD CONSTRAINT `FK_visits_opd_rooms` FOREIGN KEY (`opd_room_id`) REFERENCES `room` (`id`);
 SET foreign_key_checks = 1;
 
 -- Added by Tushar 13 Jan 2016 --
 ALTER TABLE `forensic_test_result`
 ADD COLUMN `normal_range` VARCHAR(50) NOT NULL AFTER `result_flag`;
 
ALTER TABLE `lab_test_result`
 ADD COLUMN `normal_range` VARCHAR(50) NOT NULL AFTER `result_flag`;
 
 -- Added by Tushar 14 Jan 2016 --
 ALTER TABLE `forensic_test_result`
 ADD COLUMN `result` VARCHAR(50) NOT NULL AFTER `result_flag`;
 
ALTER TABLE `lab_test_result`
 ADD COLUMN `result` VARCHAR(50) NOT NULL AFTER `result_flag`;
 
-- Added by Kamesh 14 Jan 2016 --
ALTER TABLE `patient_diagnosis_request`
 ALTER `patient_id` DROP DEFAULT,
 ALTER `visit_id` DROP DEFAULT,
 ALTER `diagnosis_id` DROP DEFAULT,
 ALTER `created_by` DROP DEFAULT;
ALTER TABLE `patient_diagnosis_request`
 CHANGE COLUMN `patient_id` `patient_id` INT(11) NOT NULL AFTER `id`,
 CHANGE COLUMN `visit_id` `visit_id` INT(11) NOT NULL AFTER `patient_id`,
 CHANGE COLUMN `diagnosis_id` `diagnosis_name` VARCHAR(50) NOT NULL AFTER `visit_id`,
 ADD COLUMN `diagnosis_code` VARCHAR(50) NOT NULL AFTER `diagnosis_name`,
 CHANGE COLUMN `created_by` `created_by` INT(11) NOT NULL AFTER `diagnosis_code`,
 DROP INDEX `FK_patient_diagnosis_request_diagnosis`,
 DROP FOREIGN KEY `FK_patient_diagnosis_request_diagnosis`;
 
 -- Added by Tushar 16 Jan 2016 --
 ALTER TABLE `visits`
 ADD COLUMN `department` TEXT NULL AFTER `patient_id`;
 
 ALTER TABLE `radiology_tests`
 DROP FOREIGN KEY `FK_radiology_tests_specimens_types`,
 DROP FOREIGN KEY `FK_radiology_tests_units`;
 
 ALTER TABLE `radiology_tests`
 DROP COLUMN `specimen_id`,
 DROP COLUMN `range`,
 DROP COLUMN `unit_id`;
 
 -- Added by Kamesh 18 Jan 2016 --
 ALTER TABLE `visits`
 ADD COLUMN `prescription_status` ENUM('Yes','No') NULL DEFAULT 'NO' AFTER `triage_status`;
 
 -- Added by Kamesh 18 Jan 2016 --
 
CREATE TABLE `configuration` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `name` VARCHAR(50) NOT NULL,
 `value` VARCHAR(50) NOT NULL,
 PRIMARY KEY (`id`)
)
ENGINE=InnoDB
;



   
   
   -- Added by Tushar 27 Jan 2016 --
   ALTER TABLE `diseases`
 DROP FOREIGN KEY `diseases_ibfk_1`;
 
ALTER TABLE `diseases`
 DROP COLUMN `category_id`;
 
ALTER TABLE `diseases`
 ADD COLUMN `description` TEXT NULL DEFAULT '' AFTER `name`;
 
 
 -- Added by Rupesh 27 Jan 2016 --
 ALTER TABLE `patient_pharmacy_extra`
 ADD COLUMN `request_date` TIMESTAMP NULL AFTER `quantity`,
 ADD COLUMN `pay_status` ENUM('paid', 'unpaid', 'settled') NULL DEFAULT 'unpaid' AFTER `request_date`,
 ADD COLUMN `cost` DOUBLE NULL AFTER `pay_status`;
 
 -- Added by Kamesh 27 Jan 2016 --
 ALTER TABLE `patient_pharmacy_bill`
 ADD COLUMN `net_amount` INT(20) NOT NULL AFTER `amount`,
 ADD COLUMN `discount` INT(20) NOT NULL AFTER `net_amount`;
 
 
 -- Added by Rupesh 28 Jan 2016 --
 CREATE TABLE `pharmacy_request` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `patient_id` INT(11) NOT NULL,
 `visit_id` INT(11) NOT NULL,
 `drug_id` INT(11) NOT NULL,
 `quantity` VARCHAR(50) NOT NULL,
 `request_date` INT(11) NOT NULL,
 `pay_status` ENUM('paid','unpaid','settled') NOT NULL DEFAULT 'unpaid',
 `cost` DOUBLE NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__patients_pharma_request` (`patient_id`),
 INDEX `FK__visits_pharma_request` (`visit_id`),
 INDEX `FK__drugs_pharma_request` (`drug_id`),
 CONSTRAINT `FK__drugs_pharma_request` FOREIGN KEY (`drug_id`) REFERENCES `drugs` (`id`),
 CONSTRAINT `FK__patients_pharma_request` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
 CONSTRAINT `FK__visits_pharma_request` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
)
ENGINE=InnoDB
;

-- Added by Rupesh 28 Jan 2016 --
ALTER TABLE `room`
 ADD COLUMN `availability` ENUM('0','1') NULL DEFAULT '0' AFTER `description`;
 
 -- Added by Tushar 28 Jan 2016 --
 ALTER TABLE `patient_pharmacy_bill`
 CHANGE COLUMN `discount` `discount` FLOAT NOT NULL AFTER `net_amount`;
 
 -- Added by Kamesh 28 Jan 2016 --
 CREATE TABLE `lab_test_component` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `lab_test_id` INT(11) NOT NULL,
 `unit_id` INT(11) NOT NULL,
 `name` VARCHAR(50) NOT NULL,
 `normal_range` VARCHAR(50) NOT NULL,
 PRIMARY KEY (`id`),
 CONSTRAINT `FK__lab_tests` FOREIGN KEY (`lab_test_id`) REFERENCES `lab_tests` (`id`),
 CONSTRAINT `FK__units` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

-- Added by Rupesh 29 Jan 2016 --
ALTER TABLE `patient_pharmacy_extra`
 DROP COLUMN `request_date`,
 DROP COLUMN `pay_status`,
 DROP COLUMN `cost`;
 
 ALTER TABLE `lab_test_requests`
 ADD COLUMN `request_type` ENUM('external','internal') NOT NULL DEFAULT 'internal' AFTER `status`;
 
 -- Added by Tushar 29 Jan 2016 --
 CREATE TABLE `patients_questionnaire` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `patient_id` INT(11) NOT NULL,
 `visit_id` INT(11) NOT NULL,
 `question`TEXT,
 `answer` TEXT,
 `created_by` INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__patients_question` (`patient_id`),
 INDEX `FK__visits_question` (`visit_id`),
 INDEX `FK3_created_by_question` (`created_by`),
 CONSTRAINT `FK3_created_by_question` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__patients_question` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
 CONSTRAINT `FK__visits_question` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

-- Added by Kamesh Pathak 30 Jan 2016 --
ALTER TABLE `lab_tests`
 DROP COLUMN `range`;
 
 -- Added by Kamesh Pathak 03 Feb 2016 --
 ALTER TABLE `patients`
 ADD COLUMN `fan_id` INT(11) NULL AFTER `father_name`,
 ADD CONSTRAINT `FK_patients_patients` FOREIGN KEY (`fan_id`) REFERENCES `patients` (`id`);
 ALTER TABLE `cash_account`
 ADD COLUMN `fan_patient_id` INT(11) NULL AFTER `patient_id`,
 ADD CONSTRAINT `FK_cash_account_patients` FOREIGN KEY (`fan_patient_id`) REFERENCES `patients` (`id`);
ALTER TABLE `cash_account`
	DROP FOREIGN KEY `FK_cash_account_patients`;
ALTER TABLE `cash_account`
	CHANGE COLUMN `fan_patient_id` `depositor_id` INT(11) NULL DEFAULT NULL AFTER `patient_id`,
	ADD CONSTRAINT `FK_cash_account_patients` FOREIGN KEY (`depositor_id`) REFERENCES `patients` (`id`);

 
 
 -- Added by Rupesh 5 Feb 2016 --
 ALTER TABLE `patients`
 ALTER `father_name` DROP DEFAULT;
ALTER TABLE `patients`
 CHANGE COLUMN `father_name` `middle_name` VARCHAR(50) NULL AFTER `dob`,
 DROP COLUMN `middle_name`;
 
 
 -- Added by Kamesh 6 Feb 2016 --
 ALTER TABLE `patient_diagnosis_request`
 ADD COLUMN `disease_id` INT(11) NOT NULL AFTER `visit_id`,
 DROP COLUMN `diagnosis_code`,
 ADD CONSTRAINT `FK_patient_diagnosis_request_diseases` FOREIGN KEY (`disease_id`) REFERENCES `diseases` (`id`);
 
 
 -- Added by Kamesh Pathak 09 Feb 2016 --
 ALTER TABLE `lab_test_result`
 ADD COLUMN `lab_test_component_id` INT(11) NULL DEFAULT NULL AFTER `specimen_id`,
 ADD CONSTRAINT `FK_lab_test_result_lab_test_component` FOREIGN KEY (`lab_test_component_id`) REFERENCES `lab_test_component` (`id`);
 
 -- Added by Sumit 09 Feb 2016 --
 -- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE `pages`;


CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL,
  `screen_name` varchar(30) NOT NULL,
  `screen_load_url` text NOT NULL,
  `parent_menu_id` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);
  
  --
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



--
-- Table structure for table `pages_options`
--

DROP TABLE `pages_options`;

CREATE TABLE IF NOT EXISTS `pages_options` (
  `id` int(11) NOT NULL,
  `screen_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '0',
  `update` tinyint(1) NOT NULL DEFAULT '0',
  `delete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=1431 DEFAULT CHARSET=latin1;

--
-- Indexes for table `pages_options`
--
ALTER TABLE `pages_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_screen_id` (`screen_id`),
  ADD KEY `FK_user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pages_options`
--
ALTER TABLE `pages_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1431;
  
  --
-- Table structure for table `user_type_access`
--

CREATE TABLE IF NOT EXISTS `user_type_access` (
  `id` int(11) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `screen_id` int(11) NOT NULL,
  `view` tinyint(4) DEFAULT '0',
  `update` tinyint(4) DEFAULT '0',
  `delete` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=841 DEFAULT CHARSET=latin1;


--
-- Indexes for table `user_type_access`
--
ALTER TABLE `user_type_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK__staff_type` (`user_type_id`),
  ADD KEY `FK__pages` (`screen_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_type_access`
--
ALTER TABLE `user_type_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=841;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_type_access`
--
ALTER TABLE `user_type_access`
  ADD CONSTRAINT `FK__pages` FOREIGN KEY (`screen_id`) REFERENCES `pages` (`id`),
  ADD CONSTRAINT `FK__staff_type` FOREIGN KEY (`user_type_id`) REFERENCES `staff_type` (`id`);
  
 -- Added by Kamesh 10 Feb 2016 --
CREATE TABLE `staff_weekly_schedule` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `staff_type_id` INT(11) NOT NULL,
 `staff_id` INT(11) NOT NULL,
 `day` ENUM('0','1','2','3','4','5','6') NOT NULL,
 `is_available` ENUM('available','notAvailable') NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__staff_type_id` (`staff_type_id`),
 INDEX `FK__users_id` (`staff_id`),
 CONSTRAINT `FK__staff_type_id` FOREIGN KEY (`staff_type_id`) REFERENCES `staff_type` (`id`),
 CONSTRAINT `FK__users_id` FOREIGN KEY (`staff_id`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;


-- Added by Kamesh 10 Feb 2016 -- 
CREATE TABLE `weekly_schedule_slots` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `schedule_id` INT(11) NOT NULL,
 `from_time` VARCHAR(20) NOT NULL,
 `to_time` VARCHAR(20) NOT NULL,
 `notes` VARCHAR(250) NULL DEFAULT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__staff_weekly_schedule` (`schedule_id`),
 CONSTRAINT `FK__staff_weekly_schedule` FOREIGN KEY (`schedule_id`) REFERENCES `staff_weekly_schedule` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

-- Added by Kamesh 11 Feb 2016 --
CREATE TABLE `staff_daily_schedule` (
 `id` INT NOT NULL AUTO_INCREMENT,
 `staff_type_id` INT NOT NULL,
 `staff_id` INT NOT NULL,
 `date` DATE NOT NULL,
 `is_available` ENUM('available','notAvailable') NOT NULL,
 PRIMARY KEY (`id`),
 CONSTRAINT `FK__staff_type_daily` FOREIGN KEY (`staff_type_id`) REFERENCES `staff_type` (`id`),
 CONSTRAINT `FK__users_daily` FOREIGN KEY (`staff_id`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

RENAME TABLE `staff_daily_schedule` TO `staff_date_schedule`;

CREATE TABLE `date_schedule_slots` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `schedule_id` INT(11) NOT NULL,
 `from_time` VARCHAR(20) NOT NULL,
 `to_time` VARCHAR(20) NOT NULL,
 `notes` VARCHAR(250) NULL DEFAULT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__staff_date_schedule` (`schedule_id`),
 CONSTRAINT `FK__staff_date_schedule` FOREIGN KEY (`schedule_id`) REFERENCES `staff_date_schedule` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

-- Added by Rupesh Arora 11 Feb 2016 --
ALTER TABLE `hospital_profile`
 ADD COLUMN `zipcode` VARCHAR(20) NOT NULL AFTER `city_id`;
 
 -- Added by Rupesh Arora 12 Feb 2016 --
 ALTER TABLE `drugs`
 DROP COLUMN `drug_code`;
 
ALTER TABLE `drugs`
 ALTER `drug_id` DROP DEFAULT;
ALTER TABLE `drugs`
 CHANGE COLUMN `drug_id` `drug_code` VARCHAR(20) NOT NULL AFTER `id`;
 
 -- Added by Kamesh 12 Feb 2016 --
 CREATE TABLE `appointments` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `consultant_id` INT(11) NOT NULL,
 `visit_id` INT(11) NOT NULL,
 `notes` VARCHAR(50) NULL DEFAULT NULL,
 `start_date_time` DATETIME NOT NULL,
 `end_date_time` DATETIME NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__users_appointment` (`consultant_id`),
 INDEX `FK__visits_appointment` (`visit_id`),
 CONSTRAINT `FK__users_appointment` FOREIGN KEY (`consultant_id`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__visits_appointment` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`)
)
ENGINE=InnoDB
AUTO_INCREMENT=1
;


-- Addded by Kamesh 15 Feb 2016 --
CREATE TABLE `item_categories` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `category` VARCHAR(255) NOT NULL,
 `description` TEXT NULL,
 `status` ENUM('A','I') NOT NULL DEFAULT 'A',
 PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

-- Added by Kamesh 16 Feb 2016 --
CREATE TABLE `items` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `item_category_id` INT(11) NOT NULL,
 `code` VARCHAR(10) NOT NULL,
 `name` VARCHAR(255) NOT NULL,
 `status` ENUM('A','I') NOT NULL DEFAULT 'A',
 PRIMARY KEY (`id`),
 INDEX `fk_item_category_id` (`item_category_id`),
 CONSTRAINT `fk_item_category_id` FOREIGN KEY (`item_category_id`) REFERENCES `item_categories` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `seller` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `name` VARCHAR(255) NOT NULL,
 `description` TEXT NULL,
 `status` ENUM('A','I') NOT NULL DEFAULT 'A',
 PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

ALTER TABLE `items`
 ADD COLUMN `description` TEXT NULL AFTER `name`;
 
 -- Added by Rupesh 16 Feb 2016 --
 CREATE TABLE `orders` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `seller_id` INT(11) NOT NULL,
 `date` INT(11) NOT NULL,
 `notes` TEXT NOT NULL,
 `ordered_by` INT(11) NOT NULL,
 `status` ENUM('ordered','received') NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__seller_orders` (`seller_id`),
 INDEX `FK__users_orders` (`ordered_by`),
 CONSTRAINT `FK__seller_orders` FOREIGN KEY (`seller_id`) REFERENCES `seller` (`id`),
 CONSTRAINT `FK__users_orders` FOREIGN KEY (`ordered_by`) REFERENCES `users` (`id`)
)
ENGINE=InnoDB
;

CREATE TABLE `order-item` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `order_id` INT(11) NOT NULL,
 `item_id` INT(11) NOT NULL,
 `quantity` VARCHAR(250) NOT NULL,
 `cost` VARCHAR(250) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__orders_order-item` (`order_id`),
 INDEX `FK__items_order-item` (`item_id`),
 CONSTRAINT `FK__items_order-item` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
 CONSTRAINT `FK__orders_order-item` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
)
ENGINE=InnoDB
;


ALTER TABLE `orders`
 CHANGE COLUMN `status` `status` ENUM('pending','received','not delivered') NOT NULL AFTER `ordered_by`;
 
 
  ALTER TABLE `orders`
 CHANGE COLUMN `status` `status` ENUM('pending','received','not delivered') NOT NULL DEFAULT 'pending' AFTER `ordered_by`;
 
 -- Added by Tushar 16 Feb 2016 --
 
 CREATE TABLE `receiver` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `name` VARCHAR(255) NOT NULL,
 `description` TEXT NULL,
 `status` ENUM('A','I') NOT NULL DEFAULT 'A',
 PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `stocking` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `Item` VARCHAR(50) NOT NULL,
 `Quantity` INT(10) NOT NULL,
 `expire_date` DATE NULL DEFAULT NULL,
 `notes` text NULL,
 `shelf_no` VARCHAR(100) NULL DEFAULT NULL,
 `stocking_date` DATE NULL DEFAULT NULL,

 PRIMARY KEY (`id`) 
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

-- Added by Rupesh 16 Feb 2016 --
CREATE TABLE `received_orders` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `order_id` INT(11) NOT NULL,
 `item_id` INT(11) NOT NULL,
 `quantity` VARCHAR(250) NOT NULL,
 `unit_cost` VARCHAR(250) NOT NULL,
 `date_time` INT(11) NOT NULL,
 `received_by` INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__received__orders_orders` (`order_id`),
 INDEX `FK__received-orders__items` (`item_id`),
 INDEX `FK__received-orders__users` (`received_by`),
 CONSTRAINT `FK__received-orders__items` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
 CONSTRAINT `FK__received-orders__users` FOREIGN KEY (`received_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__received__orders_orders` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
)
ENGINE=InnoDB
;

-- Added by Rupesh 17 Feb 2016 --
CREATE TABLE `issuance` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `item_id` INT(11) NOT NULL,
 `category_id` INT(11) NOT NULL,
 `quantity` VARCHAR(50) NOT NULL,
 `cost` INT(11) NOT NULL,
 `issued_date` date NOT NULL,
 `expiry_date` date NULL,
 `received_by`INT(11) NOT NULL,
 `issued_by`  INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__issuance_category` (`category_id`),
 INDEX `FK__issuance_items` (`item_id`),
 INDEX `FK__issuance_users` (`issued_by`),
 CONSTRAINT `FK__issuance_items` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
 CONSTRAINT `FK__issuance_users` FOREIGN KEY (`issued_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__issuance_category` FOREIGN KEY (`category_id`) REFERENCES `item_categories` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

-- Added by Kamesh 17 Feb 2016 --
ALTER TABLE `item_categories`
 ADD UNIQUE INDEX `category` (`category`);
 
 ALTER TABLE `items`
 ADD UNIQUE INDEX `code` (`code`);
 
 ALTER TABLE `seller`
 ADD UNIQUE INDEX `name` (`name`);

 
 -- Added by Tushar 18 Feb 2016 --
 ALTER TABLE `stocking`
 CHANGE COLUMN `Item` `Item` INT(11) NOT NULL AFTER `id`;
  
  -- Added by Tushar 19 Feb 2016 --
  ALTER TABLE `stocking`
 ADD CONSTRAINT `FK_stocking_items` FOREIGN KEY (`Item`) REFERENCES `items` (`id`);
 
 -- Added by Rupesh 19 Feb 2016 --
 ALTER TABLE `order-item`
 ADD COLUMN `status` ENUM('pending','received') NOT NULL DEFAULT 'pending' AFTER `cost`;
 
 -- Added by Tushar 19 Feb 2016 --
 ALTER TABLE `issuance`
 DROP COLUMN `category_id`,
 DROP FOREIGN KEY `FK__issuance_category`;
ALTER TABLE `issuance`
 ADD COLUMN `item_price` INT(11) NULL AFTER `cost`;
 
 -- Added by Rupesh 19 Feb 2016 --
 SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `lab_tests`
DROP FOREIGN KEY `FK_lab_tests_units`;
ALTER TABLE `lab_tests` DROP COLUMN `unit_id`;
SET FOREIGN_KEY_CHECKS=1;

-- Added by Tushar 22 Feb 2016 --
CREATE TABLE `clinical_notes_templates` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `name` VARCHAR(255) NOT NULL,
 `value` TEXT NOT NULL,
 PRIMARY KEY (`id`))
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

-- Added by Tushar 23 Feb 2016 --
ALTER TABLE `vision_assissment`
 CHANGE COLUMN `notes` `notes` TEXT NULL DEFAULT NULL AFTER `Comment`;
 
 ALTER TABLE `patients_prescription_request`
 CHANGE COLUMN `notes` `notes` TEXT NOT NULL AFTER `duration`;
 
 ALTER TABLE `pharmacy_inventory`
 CHANGE COLUMN `quantity` `quantity` INT(11) NULL AFTER `drug_name_id`;
 
 ALTER TABLE `pharmacy_request`
 CHANGE COLUMN `quantity` `quantity` INT(11) NULL AFTER `drug_id`;
 
 ALTER TABLE `received_orders`
 CHANGE COLUMN `quantity` `quantity` INT(11) NULL AFTER `item_id`,
 CHANGE COLUMN `unit_cost` `unit_cost` VARCHAR(250) NOT NULL AFTER `quantity`;
 
 ALTER TABLE `weekly_schedule_slots`
 CHANGE COLUMN `notes` `notes` TEXT NULL DEFAULT NULL AFTER `to_time`;
 
 
 -- Added by Tushar 25 Feb 2016 --
 SET foreign_key_checks = 0;
ALTER TABLE `wards`
 ADD COLUMN `department` INT(11) NOT NULL AFTER `ward_type`,
 ADD CONSTRAINT `FK_wards_department` FOREIGN KEY (`department`) REFERENCES `department` (`id`);
 SET foreign_key_checks = 1;
 
 -- Added by Tushar 25 Feb 2016 --
 ALTER TABLE `forensic_tests`
 CHANGE COLUMN `unit_id` `unit_id` INT(11) NULL AFTER `range`;
 
 SET foreign_key_checks = 0;
ALTER TABLE `visits`
 CHANGE COLUMN `department` `department` INT(11) NULL AFTER `patient_id`,
 ADD CONSTRAINT `FK_visits_department` FOREIGN KEY (`department`) REFERENCES `department` (`id`);
SET foreign_key_checks = 1;

CREATE TABLE `insurance_companies` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `name` VARCHAR(100) NOT NULL,
 `address` TEXT NULL,
 `country_id` INT(11) NOT NULL,
 `state_id` INT(11) NOT NULL,
 `city_id` INT(11) NOT NULL,
 `zip` VARCHAR(15) NOT NULL,
 `phone` VARCHAR(15) NOT NULL,
 `email_id` VARCHAR(50) NULL,
 `description` TEXT NULL,
 `created_on` INT(20) NULL DEFAULT NULL,
 `updated_on` INT(20) NULL DEFAULT NULL,
 `status` ENUM('A','I') NOT NULL DEFAULT 'A',
 PRIMARY KEY (`id`),
 UNIQUE INDEX `name` (`name`),
 INDEX `FK_insurance_city_id` (`city_id`),
 INDEX `FK_insurance_country_id` (`country_id`),
 INDEX `FK_insurance_state_id` (`state_id`),
 CONSTRAINT `FK_insurance_city_id` FOREIGN KEY (`city_id`) REFERENCES `locations` (`location_id`),
 CONSTRAINT `FK_insurance_country_id` FOREIGN KEY (`country_id`) REFERENCES `locations` (`location_id`),
 CONSTRAINT `FK_insurance_state_id` FOREIGN KEY (`state_id`) REFERENCES `locations` (`location_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

-- Added by Kamesh 25 Feb 2016 --
CREATE TABLE `insurance_type` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `name` VARCHAR(50) NOT NULL,
 `description` TEXT NULL,
 `created_on` INT(20) NOT NULL,
 `updated_on` INT(20) NOT NULL,
 `status` ENUM('A','I') NOT NULL DEFAULT 'A',
 PRIMARY KEY (`id`),
 UNIQUE INDEX `name` (`name`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

CREATE TABLE `insurance_plan` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `plan_name` VARCHAR(50) NOT NULL,
 `company_name_id` INT(11) NOT NULL,
 `description` TEXT NULL,
 `created_on` INT(20) NOT NULL,
 `updated_on` INT(20) NOT NULL,
 `created_by` INT(11) NOT NULL,
 `updated_by` INT(11) NOT NULL,
 `status` ENUM('A','I') NOT NULL DEFAULT 'A',
 PRIMARY KEY (`id`),
 UNIQUE INDEX `plan_name` (`plan_name`),
 CONSTRAINT `FK__insurance_companies` FOREIGN KEY (`company_name_id`) REFERENCES `insurance_companies` (`id`),
 CONSTRAINT `FK__users_insurance` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `insurance` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `patient_id` INT(11) NOT NULL,
 `insurance_type_id` INT(11) NOT NULL,
 `insurance_company_id` INT(11) NOT NULL,
 `insurance_plan_id` INT(11) NOT NULL,
 `member_since` INT(20) NOT NULL,
 `expiration_date` INT(20) NOT NULL,
 `extra_info` TEXT NULL,
 `created_on` INT(20) NOT NULL,
 `updated_on` INT(20) NOT NULL,
 `created_by` INT(11) NOT NULL,
 `updated_by` INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 CONSTRAINT `FK__patients_insurance` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
 CONSTRAINT `FK__insurance_type` FOREIGN KEY (`insurance_type_id`) REFERENCES `insurance_type` (`id`),
 CONSTRAINT `FK__insurance_companies_1` FOREIGN KEY (`insurance_company_id`) REFERENCES `insurance_companies` (`id`),
 CONSTRAINT `FK__insurance_plan` FOREIGN KEY (`insurance_plan_id`) REFERENCES `insurance_plan` (`id`),
 CONSTRAINT `FK__users_insurance_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__users_insurance_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

ALTER TABLE `insurance`
 ADD COLUMN `status` ENUM('A','I') NOT NULL DEFAULT 'A' AFTER `updated_by`;
 
 -- Added by Kamesh 26 Feb 2016 --
 ALTER TABLE `insurance`
 CHANGE COLUMN `member_since` `member_since` DATE NOT NULL AFTER `insurance_plan_id`,
 CHANGE COLUMN `expiration_date` `expiration_date` DATE NOT NULL AFTER `member_since`;
 
 CREATE TABLE `ipd_registration` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `patient_id` INT(11) NOT NULL,
 `admission_date` DATE NOT NULL,
 `discharge_date` DATE NULL,
 `attending_consultant_id` INT(11) NULL,
 `operating_consultant_id` INT(11) NULL,
 `admission_type` VARCHAR(50) NOT NULL,
 `admission_reason` TEXT NOT NULL,
 `notes` TEXT NULL,
 `created_on` TIMESTAMP NOT NULL,
 `updated_on` TIMESTAMP NOT NULL,
 `created_by` INT(11) NOT NULL,
 `updated_by` INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 CONSTRAINT `FK__patients_icd` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
 CONSTRAINT `FK__users_icd_1` FOREIGN KEY (`attending_consultant_id`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__users_icd_2` FOREIGN KEY (`operating_consultant_id`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__users_icd_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__users_icd_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `beds_ipd_history` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `ipd_id` INT(11) NOT NULL,
 `bed_id` INT(11) NOT NULL,
 `old_bed_id` INT(11) NULL,
 `is_transfer` ENUM('1','0') NOT NULL DEFAULT '0',
 `ic_icu` ENUM('1','0') NOT NULL DEFAULT '0',
 `transfer_reason` TEXT NULL,
 `created_on` INT(20) NOT NULL,
 `updated_on` INT(20) NOT NULL,
 `created_by` INT(11) NOT NULL,
 `updated_by` INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 CONSTRAINT `FK__ipd_registration_history` FOREIGN KEY (`ipd_id`) REFERENCES `ipd_registration` (`id`),
 CONSTRAINT `FK__beds_history` FOREIGN KEY (`bed_id`) REFERENCES `beds` (`id`),
 CONSTRAINT `FK__beds_history_2` FOREIGN KEY (`old_bed_id`) REFERENCES `beds` (`id`),
 CONSTRAINT `FK__users_history` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__users_history_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

SET foreign_key_checks = 0;
ALTER TABLE `beds`
 ADD COLUMN `department_id` INT(11) NOT NULL AFTER `room_id`,
 ADD CONSTRAINT `FK_beds_department` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`);
 SET foreign_key_checks = 1;
 
 ALTER TABLE `ipd_registration`
 CHANGE COLUMN `created_on` `created_on` INT NOT NULL AFTER `notes`,
 CHANGE COLUMN `updated_on` `updated_on` INT NOT NULL AFTER `created_on`;
 
 -- Added by Tushar 27 Feb 2016 --
 ALTER TABLE `beds`
 ADD COLUMN `bed_availability` ENUM('1','0') NOT NULL DEFAULT '1' AFTER `status`;
 
 -- Added by Tushar 29 Feb 2016 --
 ALTER TABLE `insurance_plan`
 DROP INDEX `plan_name`;
 
 -- Added by Kamesh 29 Feb 2016 --
 CREATE TABLE `treatments` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `price` INT(20) NOT NULL,
 `treatment` TEXT NOT NULL,
 `created_by` INT(11) NOT NULL,
 `created_on` INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK_treatments_users` (`created_by`),
 CONSTRAINT `FK_treatments_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
)
ENGINE=InnoDB
;

-- Added by Kamesh 1 Mar 2016 --
CREATE TABLE `ipd_treatment` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `ipd_id` INT(11) NOT NULL,
 `treatments_date` DATE NOT NULL,
 `shift` ENUM('0','1') NOT NULL,
 `updated_on` INT(20) NOT NULL,
 `updated_by` INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__users_treatment` (`updated_by`),
 INDEX `FK__ipd_registration` (`ipd_id`),
 CONSTRAINT `FK__users_treatment` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__ipd_registration` FOREIGN KEY (`ipd_id`) REFERENCES `ipd_registration` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `ipd_treatment_details` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `ipd_treatment_id` INT(11) NOT NULL,
 `treatment_id` INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__ipd_treatment` (`ipd_treatment_id`),
 INDEX `FK__treatments` (`treatment_id`),
 CONSTRAINT `FK__ipd_treatment` FOREIGN KEY (`ipd_treatment_id`) REFERENCES `ipd_treatment` (`id`),
 CONSTRAINT `FK__treatments` FOREIGN KEY (`treatment_id`) REFERENCES `treatments` (`id`)
)
ENGINE=InnoDB
;

-- Added by Tushar 1 Mar 2016 --
ALTER TABLE `ipd_registration`
 ADD COLUMN `discharge_advice` TEXT NULL AFTER `notes`;
 
 -- Added by Kamesh 2 Mar 2016 --
 ALTER TABLE `lab_tests`
 DROP COLUMN `fee`;
 
 ALTER TABLE `lab_test_component`
 ADD COLUMN `fee` DOUBLE NOT NULL AFTER `name`;
 
 -- Added by Tushar 2 Mar 2016 --
 ALTER TABLE `lab_test_requests`
 ADD COLUMN `lab_component_id` TEXT NOT NULL AFTER `lab_test_id`;
 
 -- Added by Tushar 3 Mar 2016 --
 CREATE TABLE `lab_test_request_component` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `lab_test_request_id` INT(11) NOT NULL,
 `lab_component_id` INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__lab_test_request_id` (`lab_test_request_id`),
 INDEX `FK__lab_componet_id` (`lab_component_id`),
 CONSTRAINT `FK__lab_test_request_id` FOREIGN KEY (`lab_test_request_id`) REFERENCES `lab_test_requests` (`id`),
 CONSTRAINT `FK__lab_componet_id` FOREIGN KEY (`lab_component_id`) REFERENCES `lab_test_component` (`id`)
 
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
ALTER TABLE `lab_test_requests`
 DROP COLUMN `lab_component_id`;
 
 ALTER TABLE `patient_specimen`
 ADD COLUMN `lab_test_requested_id` INT(11) NOT NULL AFTER `lab_test_id`;
 
 SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `patient_specimen`
 ADD CONSTRAINT `FK_patient_specimen_lab_test_requests` FOREIGN KEY (`lab_test_requested_id`) REFERENCES `lab_test_requests` (`id`);
 SET FOREIGN_KEY_CHECKS=1;
 
 -- Added by Kamesh 03 Mar 2016 --
 
 ALTER TABLE `appointments`
 ALTER `notes` DROP DEFAULT;
ALTER TABLE `appointments`
 ADD COLUMN `patient_id` INT(11) NULL AFTER `consultant_id`,
 ADD COLUMN `visit_type_id` INT(11) NULL AFTER `patient_id`,
 CHANGE COLUMN `notes` `notes` VARCHAR(50) NULL AFTER `visit_type_id`,
 DROP COLUMN `visit_id`,
 DROP FOREIGN KEY `FK__visits_appointment`,
 ADD CONSTRAINT `FK_appointments_patients` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
 ADD CONSTRAINT `FK_appointments_visit_type` FOREIGN KEY (`visit_type_id`) REFERENCES `visit_type` (`id`);
 
 -- Added by Tushar 3 Mar 2016 --
 CREATE TABLE `transaction` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `type_description` VARCHAR(200) NOT NULL,
 PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

 CREATE TABLE `transactions` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `patient_id` INT(11) NOT NULL,
 `transaction_type_id` INT(11) NOT NULL,
 `credit` DOUBLE NULL DEFAULT NULL,
 `debit` DOUBLE NULL DEFAULT NULL,
 `processed_on` INT(20) NOT NULL,
 `processed_by` INT(20) NOT NULL,
 `details` TEXT NULL,
 PRIMARY KEY (`id`),
 INDEX `fk_transactions_patient_id` (`patient_id`),
 INDEX `fk_transactions_transaction_type_id` (`transaction_type_id`),
 INDEX `fk_transactions_processed_by` (`processed_by`),
 CONSTRAINT `fk_transactions_processed_by` FOREIGN KEY (`processed_by`) REFERENCES `users` (`id`),
 CONSTRAINT `fk_transactions_transaction_type_id` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction` (`id`),
 CONSTRAINT `fk_transactions_patient_id` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `invoice_details` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `invoice details` text NULL,
 PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

 ALTER TABLE `transactions`
 ADD COLUMN `invoice_id` INT(11) NOT NULL AFTER `transaction_type_id`,
 DROP COLUMN `details`,
 ADD CONSTRAINT `FK_transactions_invoice_details` FOREIGN KEY (`invoice_id`) REFERENCES `invoice_details` (`id`);
 
 
ALTER TABLE `invoice_details`
 CHANGE COLUMN `invoice details` `invoice_details` TEXT NULL AFTER `id`;
 
 
 -- Added by Kamesh 3 Mar 2016 --
 ALTER TABLE `appointments`
 ADD COLUMN `status` ENUM('Booked','Activated') NOT NULL DEFAULT 'Booked' AFTER `end_date_time`;
 
 -- Added by Kamesh 4 Mar 2016 --
 
CREATE TABLE `ipd_lab_test_requests` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `patient_id` INT(11) NOT NULL,
 `ipd_id` INT(11) NOT NULL,
 `lab_test_id` INT(11) NOT NULL,
 `request_date` INT(20) NOT NULL,
 `pay_status` ENUM('paid','unpaid','settled') NOT NULL DEFAULT 'unpaid',
 `test_status` ENUM('pending','done','resultsent') NOT NULL DEFAULT 'pending',
 `cost` DOUBLE NOT NULL,
 `status` ENUM('A','I') NOT NULL DEFAULT 'A',
 `requested_by` INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__patient_fk_12` (`patient_id`),
 INDEX `FK__ipd_id_1` (`ipd_id`),
 INDEX `FK__requested_by` (`requested_by`),
 INDEX `FK__lab_fk_12` (`lab_test_id`),
 CONSTRAINT `FK__lab_fk_12` FOREIGN KEY (`lab_test_id`) REFERENCES `lab_tests` (`id`),
 CONSTRAINT `FK__requested_by` FOREIGN KEY (`requested_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__patient_fk_12` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
 CONSTRAINT `FK__ipd_id_1` FOREIGN KEY (`ipd_id`) REFERENCES `ipd_registration` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

CREATE TABLE `ipd_lab_test_request_component` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `ipd_lab_test_request_id` INT(11) NOT NULL,
 `lab_component_id` INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__ipd_lab_test_request_id` (`ipd_lab_test_request_id`),
 INDEX `FK__lab_componet_id_1` (`lab_component_id`),
 CONSTRAINT `FK__lab_componet_id_1` FOREIGN KEY (`lab_component_id`) REFERENCES `lab_test_component` (`id`),
 CONSTRAINT `FK__ipd_lab_test_request_id` FOREIGN KEY (`ipd_lab_test_request_id`) REFERENCES `ipd_lab_test_requests` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

CREATE TABLE `ipd_patient_specimen` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `lab_test_id` INT(11) NULL DEFAULT NULL,
 `ipd_lab_test_requested_id` INT(11) NOT NULL,
 `description` TEXT NOT NULL,
 `status` ENUM('A','I') NOT NULL DEFAULT 'A',
 `created_on` INT(11) NULL DEFAULT NULL,
 `created_by` INT(11) NULL DEFAULT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__lab_test_requests1` (`lab_test_id`),
 INDEX `FK__users1` (`created_by`),
 INDEX `FK_patient_specimen_lab_test_requests1` (`ipd_lab_test_requested_id`),
 CONSTRAINT `FK_patient_specimen_lab_test_requests1` FOREIGN KEY (`ipd_lab_test_requested_id`) REFERENCES `ipd_lab_test_requests` (`id`),
 CONSTRAINT `FK__lab_test_requests1` FOREIGN KEY (`lab_test_id`) REFERENCES `lab_test_requests` (`id`),
 CONSTRAINT `FK__users1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

CREATE TABLE `ipd_lab_test_result` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `ipd_specimen_id` INT(11) NULL DEFAULT NULL,
 `lab_test_component_id` INT(11) NULL DEFAULT NULL,
 `result_flag` ENUM('Low','Normal','N/A','High') NULL DEFAULT NULL,
 `result` VARCHAR(50) NOT NULL,
 `normal_range` VARCHAR(50) NOT NULL,
 `report` TEXT NOT NULL,
 `created_on` INT(11) NULL DEFAULT NULL,
 `updated_on` INT(11) NULL DEFAULT NULL,
 `created_by` INT(11) NULL DEFAULT NULL,
 `updated_by` INT(11) NULL DEFAULT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__patient_specimen_ipd1` (`ipd_specimen_id`),
 INDEX `FK__users__ipd11` (`created_by`),
 INDEX `FK__users__ipd1` (`updated_by`),
 INDEX `FK_lab_test_result_lab_test_component_ipd11` (`lab_test_component_id`),
 CONSTRAINT `FK_lab_test_result_lab_test_component_ipd11` FOREIGN KEY (`lab_test_component_id`) REFERENCES `lab_test_component` (`id`),
 CONSTRAINT `FK__patient_specimen_ipd1` FOREIGN KEY (`ipd_specimen_id`) REFERENCES `ipd_patient_specimen` (`id`),
 CONSTRAINT `FK__users__ipd11` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__users__ipd1` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;
 
  -- Added by Rupesh 07 March 2016 --
 ALTER TABLE `forensic_tests`
 DROP INDEX `name`;
 
   -- Added by kamesh pathak 07 March 2016 --
 CREATE TABLE `patient_account` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `name` VARCHAR(50) NOT NULL,
 `code` VARCHAR(50) NOT NULL,
 `cr_balance` INT(20) NOT NULL,
 `db_balance` INT(20) NOT NULL,
 `parent_id` INT(20) NULL,
 `type` VARCHAR(50) NOT NULL,
 PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

 ALTER TABLE `patient_account`
 ADD CONSTRAINT `FK_patient_account_patient_account` FOREIGN KEY (`parent_id`) REFERENCES `patient_account` (`id`);
 
 ALTER TABLE `patient_account`
 ADD COLUMN `description` TEXT NULL AFTER `type`;
 
  -- Added by  Rupesh Arora 07 March 2016 --
 ALTER TABLE `lab_tests`
 ALTER `test_code` DROP DEFAULT;
ALTER TABLE `lab_tests`
 CHANGE COLUMN `test_code` `test_code` VARCHAR(255) NOT NULL AFTER `specimen_id`;
 
  -- Added by Tusharg 07 March 2016 --
 ALTER TABLE `staff_weekly_schedule`
 ADD COLUMN `time_duration` INT(11) NOT NULL AFTER `staff_id`;
 
 ALTER TABLE `staff_date_schedule`
 ADD COLUMN `time_duration` INT(11) NOT NULL AFTER `staff_id`;
 
 SET foreign_key_checks = 0;
ALTER TABLE `ward_room`
 ADD COLUMN `department_id` INT(11) NOT NULL AFTER `ward_id`,
 ADD CONSTRAINT `FK_ward_room_department` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`);
SET foreign_key_checks = 1;

ALTER TABLE `treatments`
 ADD COLUMN `status` ENUM('A','I') NOT NULL AFTER `treatment`;
 
  -- Added by  Rupesh Arora 15 March 2016 --
 CREATE TABLE `holidays` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `name` VARCHAR(50) NOT NULL,
 `date` INT(20) NOT NULL,
 `description` TEXT NULL,
 `created_on` INT(11) NOT NULL,
 `updated_on` INT(11) NOT NULL,
 `created_by` INT(11) NOT NULL,
 `updated_by` INT(11) NOT NULL,
 `status` ENUM('A','I') NOT NULL DEFAULT 'A',
 PRIMARY KEY (`id`),
 INDEX `FK__created_on_holidays` (`created_by`),
 INDEX `FK__updated_on_holidays` (`updated_by`),
 CONSTRAINT `FK__created_on_holidays` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__updated_on_holidays` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
)
ENGINE=InnoDB
;

CREATE TABLE `staff_attendance` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `staff_id` INT(11) NOT NULL,
 `punch_in` INT(20) NOT NULL,
 `punch_out` INT(20) NOT NULL,
 `remarks` TEXT NULL,
 `created_on` INT(20) NOT NULL,
 `updated_on` INT(20) NOT NULL,
 `created_by` INT(11) NOT NULL,
 `updated_by` INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__users_staff_attendance` (`staff_id`),
 INDEX `FK__users_2_staff_attendance` (`created_by`),
 INDEX `FK__users_3_staff_attendance` (`updated_by`),
 CONSTRAINT `FK__users_2_staff_attendance` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__users_3_staff_attendance` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__users_staff_attendance` FOREIGN KEY (`staff_id`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `leave_type` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `leave_name` VARCHAR(50) NOT NULL,
 `created_on` INT(20) NOT NULL,
 `updated_on` INT(20) NOT NULL,
 `created_ by` INT(20) NOT NULL,
 `updated_by` INT(20) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK_leave_type_users` (`created_by`),
 INDEX `FK_leave_type_users_2` (`updated_by`),
 CONSTRAINT `FK_leave_type_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK_leave_type_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `pay_grade` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `pay_grade_name` VARCHAR(50) NOT NULL,
 `min_salary` FLOAT NOT NULL,
 `max_salary` FLOAT NOT NULL,
 `remarks` TEXT NULL,
 `created_on` INT(11) NOT NULL,
 `updated_on` INT(11) NOT NULL,
 `created_by` INT(11) NOT NULL,
 `updated_by` INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK_pay_grade_users` (`created_by`),
 INDEX `FK_pay_grade_users_2` (`updated_by`),
 CONSTRAINT `FK_pay_grade_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK_pay_grade_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `leave_period` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `period` VARCHAR(50) NOT NULL,
 `status` ENUM('A','I') NOT NULL,
 `description` TEXT NULL,
 `created_on` INT(20) NOT NULL,
 `updated_on` INT(20) NOT NULL,
 `created_by` INT(20) NOT NULL,
 `updated_by` INT(20) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK_leave_period_users` (`created_by`),
 INDEX `FK_leave_period_users_2` (`updated_by`),
 CONSTRAINT `FK_leave_period_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK_leave_period_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
CREATE TABLE `default_leaves` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `leave_type_id` INT(11) NOT NULL,
 `date` INT(20) NOT NULL,
 `remarks` TEXT NULL,
 `created_on` INT(20) NOT NULL,
 `updated_on` INT(20) NOT NULL,
 `created_by` INT(20) NOT NULL,
 `updated_by` INT(20) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__leave_type` (`leave_type_id`),
 INDEX `FK_default_leaves_users` (`created_by`),
 INDEX `FK_default_leaves_users_2` (`updated_by`),
 CONSTRAINT `FK__leave_type` FOREIGN KEY (`leave_type_id`) REFERENCES `leave_type` (`id`),
 CONSTRAINT `FK_default_leaves_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK_default_leaves_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
CREATE TABLE `staff_leaves` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `staff_id` INT(11) NOT NULL,
 `leave_type_id` INT(11) NOT NULL,
 `leave_reason` VARCHAR(250) NOT NULL,
 `leave_date` INT(20) NOT NULL,
 `leave_status` ENUM('taken','not_taken','schedule') NOT NULL DEFAULT 'not_taken',
 `remarks` TEXT NOT NULL,
 `created_on` INT(11) NOT NULL,
 `updated_on` INT(11) NOT NULL,
 `created_by` INT(11) NOT NULL,
 `updated_by` INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__users_staff_leaves` (`staff_id`),
 INDEX `FK__leave_type_staff_leaves` (`leave_type_id`),
 INDEX `FK_staff_leaves_users` (`created_by`),
 INDEX `FK_staff_leaves_users_2` (`updated_by`),
 CONSTRAINT `FK__leave_type_staff_leaves` FOREIGN KEY (`leave_type_id`) REFERENCES `leave_type` (`id`),
 CONSTRAINT `FK__users_staff_leaves` FOREIGN KEY (`staff_id`) REFERENCES `users` (`id`),
 CONSTRAINT `FK_staff_leaves_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK_staff_leaves_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
CREATE TABLE `staff_performance` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `staff_id` INT(11) NOT NULL,
 `rating` INT(11) NOT NULL,
 `remarks` TEXT NOT NULL,
 `created_on` INT(20) NOT NULL,
 `updated_on` INT(20) NOT NULL,
 `created_by` INT(20) NOT NULL,
 `updated_by` INT(20) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK_users_staff_performance` (`staff_id`),
 INDEX `FK_staff_performance_users` (`created_by`),
 INDEX `FK_staff_performance_users_2` (`updated_by`),
 CONSTRAINT `FK_staff_performance_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK_staff_performance_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK_users_staff_performance` FOREIGN KEY (`staff_id`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
CREATE TABLE `staff_salary` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `staff_id` INT(11) NOT NULL,
 `pay_grade_id` INT(11) NOT NULL,
 `mode_of_transaction` ENUM('cash','check','online_transaction') NOT NULL DEFAULT 'cash',
 `transaction_id` VARCHAR(50) NOT NULL DEFAULT 'cash',
 `created_on` INT(20) NOT NULL,
 `updated_on` INT(20) NOT NULL,
 `created_by` INT(20) NOT NULL,
 `updated_by` INT(20) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK_users_staff_salary` (`staff_id`),
 INDEX `FK_pay_grade_staff_salary` (`pay_grade_id`),
 INDEX `FK_staff_salary_users` (`created_by`),
 INDEX `FK_staff_salary_users_2` (`updated_by`),
 CONSTRAINT `FK_pay_grade_staff_salary` FOREIGN KEY (`pay_grade_id`) REFERENCES `pay_grade` (`id`),
 CONSTRAINT `FK_staff_salary_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK_staff_salary_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK_users_staff_salary` FOREIGN KEY (`staff_id`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `entitlements` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `staff_id` INT(11) NOT NULL,
 `leave_type_id` INT(11) NOT NULL,
 `leave_period_id` INT(11) NOT NULL,
 `entitlements` INT(11) NOT NULL,
 `created_on` INT(11) NOT NULL,
 `updated_on` INT(11) NOT NULL,
 `created_by` INT(11) NOT NULL,
 `updated_by` INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__users_entitlements` (`staff_id`),
 INDEX `FK__leave_type_entitlements` (`leave_type_id`),
 INDEX `FK__leave_period_entitlements` (`leave_period_id`),
 INDEX `FK_entitlements_users` (`created_by`),
 INDEX `FK_entitlements_users_2` (`updated_by`),
 CONSTRAINT `FK__leave_period_entitlements` FOREIGN KEY (`leave_period_id`) REFERENCES `leave_period` (`id`),
 CONSTRAINT `FK__leave_type_entitlements` FOREIGN KEY (`leave_type_id`) REFERENCES `leave_type` (`id`),
 CONSTRAINT `FK__users_entitlements` FOREIGN KEY (`staff_id`) REFERENCES `users` (`id`),
 CONSTRAINT `FK_entitlements_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK_entitlements_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
)
ENGINE=InnoDB
;

 -- Added by Rupesh 07 March 2016 --

 ALTER TABLE `leave_type`
 CHANGE COLUMN `status` `status` ENUM('A','I') NOT NULL DEFAULT 'A' AFTER `leave_name`;
 ALTER TABLE `leave_type`
 CHANGE COLUMN `leave_name` `name` VARCHAR(50) NOT NULL AFTER `id`;
 ALTER TABLE `leave_type`
 ADD COLUMN `description` TEXT NOT NULL AFTER `name`;
 ALTER TABLE `leave_type`
 CHANGE COLUMN `created_ by` `created_by` INT(20) NOT NULL AFTER `updated_on`;
 
 
 ALTER TABLE `entitlements`
 ADD COLUMN `status` ENUM('A','I') NOT NULL DEFAULT 'A' AFTER `updated_by`;
 
 ALTER TABLE `entitlements`
 DROP FOREIGN KEY `FK__users_entitlements`;
ALTER TABLE `entitlements`
 ALTER `staff_id` DROP DEFAULT;
ALTER TABLE `entitlements`
 CHANGE COLUMN `staff_id` `staff_type_id` INT(11) NOT NULL AFTER `id`,
 ADD CONSTRAINT `FK__staff_type_entitlements` FOREIGN KEY (`staff_type_id`) REFERENCES `staff_type` (`id`);
 
 -- Added by Tushar 07 March 2016 --
 
 ALTER TABLE `staff_leaves`
 CHANGE COLUMN `leave_status` `leave_status` ENUM('taken','not_taken','schedule','cancel','approval') NOT NULL DEFAULT 'schedule' AFTER `leave_date`;
 
 
 -- Added by Rupesh 07 March 2016 --
 
 CREATE TABLE `review_staff` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `staff_id` INT(11) NOT NULL,
 `rating` INT(11) NOT NULL,
 `comments` TEXT NULL,
 `review_by` INT(11) NOT NULL,
 `review_date` INT(20) NOT NULL,
 `created_by` INT(20) NOT NULL,
 `created_on` INT(20) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__users_review_by` (`staff_id`),
 INDEX `FK__users_2_review_by` (`review_by`),
 CONSTRAINT `FK__users_2_review_by` FOREIGN KEY (`review_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__users_review_by` FOREIGN KEY (`staff_id`) REFERENCES `users` (`id`)
)
ENGINE=InnoDB
;

 ALTER TABLE `review_staff`
 ADD CONSTRAINT `FK_review_staff_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);
 
 
 