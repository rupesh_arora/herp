-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 22, 2016 at 11:26 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `herp`
--

-- --------------------------------------------------------

--
-- Table structure for table `allergies`
--

CREATE TABLE IF NOT EXISTS `allergies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `allergy_cat_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `reaction_element` varchar(255) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_allergy_cat_id` (`allergy_cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `allergies_categories`
--

CREATE TABLE IF NOT EXISTS `allergies_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `description` text,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `allergy_request`
--

CREATE TABLE IF NOT EXISTS `allergy_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL DEFAULT '0',
  `visit_id` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `allergy_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK__patients_allergy` (`patient_id`),
  KEY `FK__visits_allergy` (`visit_id`),
  KEY `FK__users_allergy` (`created_by`),
  KEY `FK__allergies_allergy` (`allergy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE IF NOT EXISTS `appointments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consultant_id` int(11) NOT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `visit_type_id` int(11) DEFAULT NULL,
  `notes` varchar(50) DEFAULT NULL,
  `start_date_time` datetime NOT NULL,
  `end_date_time` datetime NOT NULL,
  `status` enum('Booked','Activated','Hold On') NOT NULL DEFAULT 'Booked',
  PRIMARY KEY (`id`),
  KEY `FK__users_appointment` (`consultant_id`),
  KEY `FK_appointments_patients` (`patient_id`),
  KEY `FK_appointments_visit_type` (`visit_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `beds`
--

CREATE TABLE IF NOT EXISTS `beds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ward_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `cost` double NOT NULL,
  `price` double NOT NULL,
  `number` varchar(30) NOT NULL,
  `description` text,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  `bed_availability` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ward_id` (`ward_id`),
  KEY `room_id` (`room_id`),
  KEY `FK_beds_department` (`department_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Table structure for table `beds_ipd_history`
--

CREATE TABLE IF NOT EXISTS `beds_ipd_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ipd_id` int(11) NOT NULL,
  `bed_id` int(11) NOT NULL,
  `old_bed_id` int(11) DEFAULT NULL,
  `is_transfer` enum('1','0') NOT NULL DEFAULT '0',
  `ic_icu` enum('1','0') NOT NULL DEFAULT '0',
  `transfer_reason` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__ipd_registration_history` (`ipd_id`),
  KEY `FK__beds_history` (`bed_id`),
  KEY `FK__beds_history_2` (`old_bed_id`),
  KEY `FK__users_history` (`created_by`),
  KEY `FK__users_history_2` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cash_account`
--

CREATE TABLE IF NOT EXISTS `cash_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `depositor_id` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `credit` double DEFAULT NULL,
  `debit` double DEFAULT NULL,
  `created_on` int(20) NOT NULL,
  `created_by` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_patient_id` (`patient_id`),
  KEY `cash_amount_1` (`created_by`),
  KEY `FK_cash_account_patients` (`depositor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `clinical_findings`
--

CREATE TABLE IF NOT EXISTS `clinical_findings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visit_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `presenting_complaint` varchar(255) NOT NULL,
  `general_appearance` varchar(255) DEFAULT NULL,
  `pv_pr` varchar(255) NOT NULL,
  `respiratory` varchar(255) NOT NULL,
  `psychological_status` varchar(255) DEFAULT NULL,
  `cvs` varchar(255) NOT NULL,
  `ent` varchar(255) NOT NULL,
  `abdomen` varchar(255) NOT NULL,
  `pmh_psh` varchar(255) NOT NULL,
  `cns` varchar(255) NOT NULL,
  `poh` varchar(255) NOT NULL,
  `eye` varchar(255) NOT NULL,
  `pgh` varchar(255) NOT NULL,
  `muscular_skeletal` varchar(255) NOT NULL,
  `fsh` varchar(255) NOT NULL,
  `skin` varchar(255) NOT NULL,
  `clinical_notes` varchar(255) NOT NULL,
  `provisional_diagnosis` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cf_visit_id` (`visit_id`),
  KEY `fk_cf_patient_id` (`patient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `clinical_notes_templates`
--

CREATE TABLE IF NOT EXISTS `clinical_notes_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE IF NOT EXISTS `configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `configurations`
--

CREATE TABLE IF NOT EXISTS `configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `value` text NOT NULL,
  `remarks` text,
  `status` enum('A','I') DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `consulation_type`
--

CREATE TABLE IF NOT EXISTS `consulation_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `cost` double NOT NULL,
  `tax` double NOT NULL,
  `other_charges` double NOT NULL,
  `other_charges_detail` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`),
  KEY `consulation_type_ibfk_1` (`created_by`),
  KEY `consulation_type_ibfk_2` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `date_schedule_slots`
--

CREATE TABLE IF NOT EXISTS `date_schedule_slots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_id` int(11) NOT NULL,
  `from_time` varchar(20) NOT NULL,
  `to_time` varchar(20) NOT NULL,
  `notes` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__staff_date_schedule` (`schedule_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `default_leaves`
--

CREATE TABLE IF NOT EXISTS `default_leaves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `leave_type_id` int(11) NOT NULL,
  `date` int(20) NOT NULL,
  `remarks` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(20) NOT NULL,
  `updated_by` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__leave_type` (`leave_type_id`),
  KEY `FK_default_leaves_users` (`created_by`),
  KEY `FK_default_leaves_users_2` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(50) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `department` (`department`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE IF NOT EXISTS `designation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(50) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `designation` (`designation`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis`
--

CREATE TABLE IF NOT EXISTS `diagnosis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `diagnosis_cat_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `fk_diagnosis_cat_id` (`diagnosis_cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis_categories`
--

CREATE TABLE IF NOT EXISTS `diagnosis_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `diseases`
--

CREATE TABLE IF NOT EXISTS `diseases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69825 ;

-- --------------------------------------------------------

--
-- Table structure for table `diseases_categories`
--

CREATE TABLE IF NOT EXISTS `diseases_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `description` text,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `category` (`category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Table structure for table `drugs`
--

CREATE TABLE IF NOT EXISTS `drugs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `drug_code` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `alternate_name` varchar(50) DEFAULT NULL,
  `drug_category_id` int(11) NOT NULL,
  `unit` int(11) NOT NULL,
  `cost` double NOT NULL,
  `price` double NOT NULL,
  `alternate_drug_id` int(11) DEFAULT NULL,
  `composition` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `drug_category_id` (`drug_category_id`),
  KEY `unit` (`unit`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `drug_categories`
--

CREATE TABLE IF NOT EXISTS `drug_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `description` text,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `category` (`category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `entitlements`
--

CREATE TABLE IF NOT EXISTS `entitlements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_type_id` int(11) NOT NULL,
  `leave_type_id` int(11) NOT NULL,
  `leave_period_id` int(11) NOT NULL,
  `entitlements` int(11) NOT NULL,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK__users_entitlements` (`staff_type_id`),
  KEY `FK__leave_type_entitlements` (`leave_type_id`),
  KEY `FK__leave_period_entitlements` (`leave_period_id`),
  KEY `FK_entitlements_users` (`created_by`),
  KEY `FK_entitlements_users_2` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `forensic_tests`
--

CREATE TABLE IF NOT EXISTS `forensic_tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `specimen_id` int(11) NOT NULL,
  `test_code` varchar(255) DEFAULT 'Test Code',
  `range` varchar(50) NOT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `fee` double NOT NULL,
  `description` text NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) DEFAULT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `FK_forensic_tests_specimens_types` (`specimen_id`),
  KEY `FK_forensic_tests_units` (`unit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `forensic_test_request`
--

CREATE TABLE IF NOT EXISTS `forensic_test_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `forensic_test_id` int(11) NOT NULL,
  `request_date` int(20) NOT NULL,
  `pay_status` enum('paid','unpaid','settled') NOT NULL DEFAULT 'unpaid',
  `test_status` enum('pending','done','resultsent') NOT NULL DEFAULT 'pending',
  `cost` double NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK__patients_fk_3` (`patient_id`),
  KEY `FK__visits_fk_3` (`visit_id`),
  KEY `FK__forensic_tests_fk_3` (`forensic_test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `forensic_test_result`
--

CREATE TABLE IF NOT EXISTS `forensic_test_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `specimen_id` int(11) DEFAULT NULL,
  `result_flag` enum('Low','Normal''High','N/A') DEFAULT NULL,
  `result` varchar(50) NOT NULL,
  `normal_range` varchar(50) NOT NULL,
  `report` text NOT NULL,
  `created_on` int(11) DEFAULT NULL,
  `updated_on` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__patient_specimen1` (`specimen_id`),
  KEY `FK__users_11` (`created_by`),
  KEY `FK__users_31` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE IF NOT EXISTS `holidays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `date` int(20) NOT NULL,
  `description` text,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK__created_on_holidays` (`created_by`),
  KEY `FK__updated_on_holidays` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hospital_profile`
--

CREATE TABLE IF NOT EXISTS `hospital_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hospital_name` text NOT NULL,
  `address` text NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `zipcode` varchar(20) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `images_path` varchar(300) DEFAULT 'logo.gif',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `FK__locations` (`country_id`),
  KEY `FK__locations_2` (`state_id`),
  KEY `FK__locations_3` (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `insurance`
--

CREATE TABLE IF NOT EXISTS `insurance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `insurance_type_id` int(11) NOT NULL,
  `insurance_company_id` int(11) NOT NULL,
  `insurance_plan_id` int(11) NOT NULL,
  `member_since` date NOT NULL,
  `expiration_date` date NOT NULL,
  `extra_info` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK__patients_insurance` (`patient_id`),
  KEY `FK__insurance_type` (`insurance_type_id`),
  KEY `FK__insurance_companies_1` (`insurance_company_id`),
  KEY `FK__insurance_plan` (`insurance_plan_id`),
  KEY `FK__users_insurance_1` (`created_by`),
  KEY `FK__users_insurance_2` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `insurance_companies`
--

CREATE TABLE IF NOT EXISTS `insurance_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` text,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `zip` varchar(15) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email_id` varchar(50) DEFAULT NULL,
  `description` text,
  `created_on` int(20) DEFAULT NULL,
  `updated_on` int(20) DEFAULT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `FK_insurance_city_id` (`city_id`),
  KEY `FK_insurance_country_id` (`country_id`),
  KEY `FK_insurance_state_id` (`state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `insurance_plan`
--

CREATE TABLE IF NOT EXISTS `insurance_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_name` varchar(50) NOT NULL,
  `company_name_id` int(11) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK__insurance_companies` (`company_name_id`),
  KEY `FK__users_insurance` (`created_by`),
  KEY `FK__users_2` (`updated_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Table structure for table `insurance_type`
--

CREATE TABLE IF NOT EXISTS `insurance_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_type`
--

CREATE TABLE IF NOT EXISTS `inventory_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(250) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_details`
--

CREATE TABLE IF NOT EXISTS `invoice_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_details` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `ipd_cases`
--

CREATE TABLE IF NOT EXISTS `ipd_cases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__patients_ipd` (`patient_id`),
  KEY `FK__department_ipd` (`department_id`),
  KEY `FK__visits_ipd` (`visit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ipd_lab_test_requests`
--

CREATE TABLE IF NOT EXISTS `ipd_lab_test_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `ipd_id` int(11) NOT NULL,
  `lab_test_id` int(11) NOT NULL,
  `request_date` int(20) NOT NULL,
  `pay_status` enum('paid','unpaid','settled') NOT NULL DEFAULT 'unpaid',
  `test_status` enum('pending','done','resultsent') NOT NULL DEFAULT 'pending',
  `cost` double NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  `requested_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__patient_fk_12` (`patient_id`),
  KEY `FK__ipd_id_1` (`ipd_id`),
  KEY `FK__requested_by` (`requested_by`),
  KEY `FK__lab_fk_12` (`lab_test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ipd_lab_test_request_component`
--

CREATE TABLE IF NOT EXISTS `ipd_lab_test_request_component` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ipd_lab_test_request_id` int(11) NOT NULL,
  `lab_component_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__ipd_lab_test_request_id` (`ipd_lab_test_request_id`),
  KEY `FK__lab_componet_id_1` (`lab_component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ipd_lab_test_result`
--

CREATE TABLE IF NOT EXISTS `ipd_lab_test_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ipd_specimen_id` int(11) DEFAULT NULL,
  `lab_test_component_id` int(11) DEFAULT NULL,
  `result_flag` enum('Low','Normal','N/A','High') DEFAULT NULL,
  `result` varchar(50) NOT NULL,
  `normal_range` varchar(50) NOT NULL,
  `report` text NOT NULL,
  `created_on` int(11) DEFAULT NULL,
  `updated_on` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__patient_specimen_ipd1` (`ipd_specimen_id`),
  KEY `FK__users__ipd11` (`created_by`),
  KEY `FK__users__ipd1` (`updated_by`),
  KEY `FK_lab_test_result_lab_test_component_ipd11` (`lab_test_component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ipd_patient_request`
--

CREATE TABLE IF NOT EXISTS `ipd_patient_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `operating_consultant_id` int(11) NOT NULL,
  `attending_consultant_id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `reason` text NOT NULL,
  `created_on` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__users_ipd_patient_request` (`patient_id`),
  KEY `FK__users_2_ipd_patient_request` (`operating_consultant_id`),
  KEY `FK__users_3_ipd_patient_request` (`attending_consultant_id`),
  KEY `FK__visits_ipd_patient_request` (`visit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `ipd_patient_specimen`
--

CREATE TABLE IF NOT EXISTS `ipd_patient_specimen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lab_test_id` int(11) DEFAULT NULL,
  `ipd_lab_test_requested_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  `created_on` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__lab_test_requests1` (`lab_test_id`),
  KEY `FK__users1` (`created_by`),
  KEY `FK_patient_specimen_lab_test_requests1` (`ipd_lab_test_requested_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ipd_registration`
--

CREATE TABLE IF NOT EXISTS `ipd_registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `admission_date` date NOT NULL,
  `discharge_date` date DEFAULT NULL,
  `attending_consultant_id` int(11) DEFAULT NULL,
  `operating_consultant_id` int(11) DEFAULT NULL,
  `admission_type` varchar(50) NOT NULL,
  `admission_reason` text NOT NULL,
  `notes` text,
  `discharge_advice` text,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__patients_icd` (`patient_id`),
  KEY `FK__users_icd_1` (`attending_consultant_id`),
  KEY `FK__users_icd_2` (`operating_consultant_id`),
  KEY `FK__users_icd_3` (`created_by`),
  KEY `FK__users_icd_4` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ipd_treatment`
--

CREATE TABLE IF NOT EXISTS `ipd_treatment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ipd_id` int(11) NOT NULL,
  `treatments_date` date NOT NULL,
  `shift` enum('0','1') NOT NULL,
  `updated_on` int(20) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__users_treatment` (`updated_by`),
  KEY `FK__ipd_registration` (`ipd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ipd_treatment_details`
--

CREATE TABLE IF NOT EXISTS `ipd_treatment_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ipd_treatment_id` int(11) NOT NULL,
  `treatment_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__ipd_treatment` (`ipd_treatment_id`),
  KEY `FK__treatments` (`treatment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `issuance`
--

CREATE TABLE IF NOT EXISTS `issuance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `quantity` varchar(50) NOT NULL,
  `cost` int(11) NOT NULL,
  `item_price` int(11) DEFAULT NULL,
  `issued_date` date NOT NULL,
  `expiry_date` date DEFAULT NULL,
  `received_by` int(11) NOT NULL,
  `issued_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__issuance_items` (`item_id`),
  KEY `FK__issuance_users` (`issued_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_category_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `fk_item_category_id` (`item_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

-- --------------------------------------------------------

--
-- Table structure for table `item_categories`
--

CREATE TABLE IF NOT EXISTS `item_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `inventory_type_id` int(11) DEFAULT NULL,
  `description` text,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `category` (`category`),
  KEY `FK_item_categories_inventory_type` (`inventory_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

-- --------------------------------------------------------

--
-- Table structure for table `item_price`
--

CREATE TABLE IF NOT EXISTS `item_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `price` varchar(50) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_item_price_items` (`item_id`),
  KEY `FK_item_price_users` (`updated_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `item_request`
--

CREATE TABLE IF NOT EXISTS `item_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_type_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `ordered_quantity` int(20) NOT NULL,
  `received_quantity` int(20) NOT NULL DEFAULT '0',
  `ordered_by` int(11) NOT NULL,
  `ordered_date` date NOT NULL,
  `status` enum('Pending','Received') NOT NULL DEFAULT 'Pending',
  PRIMARY KEY (`id`),
  KEY `FK__users_item_request` (`ordered_by`),
  KEY `FK__items_item_request` (`item_id`),
  KEY `FK__inventory_type_item_request` (`inventory_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `item_response`
--

CREATE TABLE IF NOT EXISTS `item_response` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_type_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(20) NOT NULL,
  `dispatched_by` int(11) NOT NULL,
  `dispatched_date` date NOT NULL,
  `invoice_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__inventory_type_item_response` (`inventory_type_id`),
  KEY `FK__items_item_response` (`item_id`),
  KEY `FK__users_item_response` (`dispatched_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `item_stock`
--

CREATE TABLE IF NOT EXISTS `item_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `mfg_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_cost` int(20) NOT NULL,
  `created_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_on` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__items_item_stock` (`item_id`),
  KEY `FK__users_item_stock` (`created_by`),
  KEY `FK_item_stock_users` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lab_tests`
--

CREATE TABLE IF NOT EXISTS `lab_tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `lab_type_id` int(11) NOT NULL,
  `specimen_id` int(11) NOT NULL,
  `test_code` varchar(255) NOT NULL,
  `description` tinytext,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `lab_type_id` (`lab_type_id`),
  KEY `specimen_id` (`specimen_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Table structure for table `lab_test_component`
--

CREATE TABLE IF NOT EXISTS `lab_test_component` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lab_test_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `fee` double NOT NULL,
  `normal_range` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__lab_tests` (`lab_test_id`),
  KEY `FK__units` (`unit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

-- --------------------------------------------------------

--
-- Table structure for table `lab_test_requests`
--

CREATE TABLE IF NOT EXISTS `lab_test_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `lab_test_id` int(11) NOT NULL,
  `request_date` int(20) NOT NULL,
  `pay_status` enum('paid','unpaid','settled') NOT NULL DEFAULT 'unpaid',
  `test_status` enum('pending','done','resultsent') NOT NULL DEFAULT 'pending',
  `cost` double NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  `request_type` enum('external','internal') NOT NULL DEFAULT 'internal',
  PRIMARY KEY (`id`),
  KEY `FK__patient_fk_1` (`patient_id`),
  KEY `FK__visit_fk_1` (`visit_id`),
  KEY `FK__lab_fk_1` (`lab_test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lab_test_request_component`
--

CREATE TABLE IF NOT EXISTS `lab_test_request_component` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lab_test_request_id` int(11) NOT NULL,
  `lab_component_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__lab_test_request_id` (`lab_test_request_id`),
  KEY `FK__lab_componet_id` (`lab_component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lab_test_result`
--

CREATE TABLE IF NOT EXISTS `lab_test_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `specimen_id` int(11) DEFAULT NULL,
  `lab_test_component_id` int(11) DEFAULT NULL,
  `result_flag` enum('Low','Normal','N/A','High') DEFAULT NULL,
  `result` varchar(50) NOT NULL,
  `normal_range` varchar(50) NOT NULL,
  `report` text NOT NULL,
  `created_on` int(11) DEFAULT NULL,
  `updated_on` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__patient_specimen` (`specimen_id`),
  KEY `FK__users_1` (`created_by`),
  KEY `FK__users_3` (`updated_by`),
  KEY `FK_lab_test_result_lab_test_component` (`lab_test_component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lab_types`
--

CREATE TABLE IF NOT EXISTS `lab_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `leave_period`
--

CREATE TABLE IF NOT EXISTS `leave_period` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `status` enum('A','I') NOT NULL,
  `period_status` enum('1','0') NOT NULL DEFAULT '0',
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(20) NOT NULL,
  `updated_by` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_leave_period_users` (`created_by`),
  KEY `FK_leave_period_users_2` (`updated_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `leave_type`
--

CREATE TABLE IF NOT EXISTS `leave_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(20) NOT NULL,
  `updated_by` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_leave_type_users` (`created_by`),
  KEY `FK_leave_type_users_2` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `location_type` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_visible` int(11) NOT NULL,
  `country_code` int(11) DEFAULT NULL,
  PRIMARY KEY (`location_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52827 ;

-- --------------------------------------------------------

--
-- Table structure for table `opd_consultants`
--

CREATE TABLE IF NOT EXISTS `opd_consultants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opd_room_id` int(11) NOT NULL,
  `consultant_id` int(11) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK_opd_consultants_opd_rooms` (`opd_room_id`),
  KEY `FK_opd_consultants_users` (`consultant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `opd_rooms`
--

CREATE TABLE IF NOT EXISTS `opd_rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `consultant_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK_opd_rooms_room` (`room_id`),
  KEY `FK_opd_rooms_users` (`consultant_id`),
  KEY `FK_opd_rooms_department` (`department_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Table structure for table `order-item`
--

CREATE TABLE IF NOT EXISTS `order-item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `ordered_quantity` varchar(250) NOT NULL,
  `received_quantity` varchar(250) NOT NULL,
  `cost` varchar(250) NOT NULL,
  `status` enum('pending','received') NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`id`),
  KEY `FK__orders_order-item` (`order_id`),
  KEY `FK__items_order-item` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `notes` text NOT NULL,
  `ordered_by` int(11) NOT NULL,
  `status` enum('pending','received','not delivered') NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`id`),
  KEY `FK__seller_orders` (`seller_id`),
  KEY `FK__users_orders` (`ordered_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `screen_name` varchar(50) DEFAULT NULL,
  `screen_load_url` varchar(50) DEFAULT NULL,
  `parent_menu_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=143 ;

-- --------------------------------------------------------

--
-- Table structure for table `pages_options`
--

CREATE TABLE IF NOT EXISTS `pages_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `screen_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '0',
  `update` tinyint(1) NOT NULL DEFAULT '0',
  `delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_screen_id` (`screen_id`),
  KEY `FK_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5197 ;

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE IF NOT EXISTS `patients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salutation` varchar(10) DEFAULT '0',
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `dob` date DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `fan_id` int(11) DEFAULT NULL,
  `gender` enum('M','F','NM') NOT NULL DEFAULT 'NM',
  `care_of` varchar(50) DEFAULT NULL,
  `care_contact` varchar(20) DEFAULT NULL,
  `address` text,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `family_details` tinytext,
  `email` varchar(50) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `ref_source` varchar(50) DEFAULT NULL,
  `panel_details` varchar(50) DEFAULT NULL,
  `id_proof` varchar(50) DEFAULT NULL,
  `id_proof_file_name` varchar(50) DEFAULT NULL,
  `biometric_data` text,
  `remarks` text,
  `password` varchar(285) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  `images_path` varchar(300) DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_proof_file_name` (`id_proof_file_name`),
  KEY `patients_ibfk_1` (`created_by`),
  KEY `patients_ibfk_2` (`updated_by`),
  KEY `FK_countryid` (`country_id`),
  KEY `FK_stateid` (`state_id`),
  KEY `FK_cityid` (`city_id`),
  KEY `FK_patients_patients` (`fan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `patients_prescription_request`
--

CREATE TABLE IF NOT EXISTS `patients_prescription_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visit_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `drug_id` int(11) NOT NULL,
  `quantity` int(20) NOT NULL,
  `issued_on` date NOT NULL,
  `dosage` varchar(50) NOT NULL,
  `duration` varchar(50) NOT NULL,
  `notes` text NOT NULL,
  `dispense_status` enum('Dispensed','Pending') NOT NULL DEFAULT 'Pending',
  PRIMARY KEY (`id`),
  KEY `FK__visits12` (`visit_id`),
  KEY `FK__patients12` (`patient_id`),
  KEY `FK__drugs12` (`drug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `patients_questionnaire`
--

CREATE TABLE IF NOT EXISTS `patients_questionnaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `question` text,
  `answer` text,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__patients_question` (`patient_id`),
  KEY `FK__visits_question` (`visit_id`),
  KEY `FK3_created_by_question` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `patient_account`
--

CREATE TABLE IF NOT EXISTS `patient_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `cr_balance` int(20) NOT NULL,
  `db_balance` int(20) NOT NULL,
  `parent_id` int(20) DEFAULT NULL,
  `type` varchar(50) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `FK_patient_account_patient_account` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `patient_bill_payment`
--

CREATE TABLE IF NOT EXISTS `patient_bill_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount_tendered` int(11) NOT NULL,
  `receipt_no` int(11) NOT NULL,
  `payment_mode` varchar(50) NOT NULL,
  `visit_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `receipt_no` (`receipt_no`),
  KEY `FK__visits_id` (`visit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `patient_diagnosis_request`
--

CREATE TABLE IF NOT EXISTS `patient_diagnosis_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `disease_id` int(11) NOT NULL,
  `diagnosis_name` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__patients_diagnosis` (`patient_id`),
  KEY `FK__visits_diagnosis` (`visit_id`),
  KEY `FK3_created_by_diagnosis` (`created_by`),
  KEY `FK_patient_diagnosis_request_diseases` (`disease_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `patient_forensic_bill`
--

CREATE TABLE IF NOT EXISTS `patient_forensic_bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `forensic_test_id` int(11) NOT NULL,
  `charge` int(20) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('paid','settled','unpaid') NOT NULL DEFAULT 'unpaid',
  PRIMARY KEY (`id`),
  KEY `FK__patient` (`patient_id`),
  KEY `FK__forensic_test_id` (`forensic_test_id`),
  KEY `FK__visits_1` (`visit_id`),
  KEY `forensic_bill_ibfk_1` (`created_by`),
  KEY `forensic_billnts_ibfk_2` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `patient_forensic_specimen`
--

CREATE TABLE IF NOT EXISTS `patient_forensic_specimen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forensic_test_id` int(11) DEFAULT NULL,
  `description` text NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  `created_on` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__forensic_test_request` (`forensic_test_id`),
  KEY `FK__users` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `patient_lab_bill`
--

CREATE TABLE IF NOT EXISTS `patient_lab_bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `lab_test_id` int(11) NOT NULL,
  `charge` int(20) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('paid','settled','unpaid') NOT NULL DEFAULT 'unpaid',
  PRIMARY KEY (`id`),
  KEY `FK__patients_id1` (`patient_id`),
  KEY `FK__lab_tests_1` (`lab_test_id`),
  KEY `FK__visits_id1` (`visit_id`),
  KEY `lab_bill_ibfk_1` (`created_by`),
  KEY `lab_bill_ibfk_2` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `patient_pharmacy_bill`
--

CREATE TABLE IF NOT EXISTS `patient_pharmacy_bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `amount` int(20) NOT NULL,
  `net_amount` int(20) NOT NULL,
  `discount` float NOT NULL,
  `visit_id` int(11) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('paid','settled','unpaid') NOT NULL DEFAULT 'unpaid',
  PRIMARY KEY (`id`),
  KEY `FK__patient6` (`patient_id`),
  KEY `FK__visits_6` (`visit_id`),
  KEY `pharmacy_bill_ibfk_1` (`created_by`),
  KEY `pharmacy_bill_ibfk_2` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `patient_pharmacy_extra`
--

CREATE TABLE IF NOT EXISTS `patient_pharmacy_extra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `drug_id` int(11) NOT NULL,
  `quantity` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__patients_extra` (`patient_id`),
  KEY `FK__visits_extra` (`visit_id`),
  KEY `FK__drugs_extra` (`drug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `patient_procedure_bill`
--

CREATE TABLE IF NOT EXISTS `patient_procedure_bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL DEFAULT '0',
  `procedure_id` int(11) NOT NULL,
  `charge` int(20) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('paid','settled','unpaid') NOT NULL DEFAULT 'unpaid',
  PRIMARY KEY (`id`),
  KEY `FK_patient_procedure_bill_patients` (`patient_id`),
  KEY `FK_patient_procedure_bill_procedures` (`procedure_id`),
  KEY `FK_patient_procedure_bill_visits` (`visit_id`),
  KEY `procedure_bill_ibfk_1` (`created_by`),
  KEY `procedure_bill_ibfk_2` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `patient_radiology_bill`
--

CREATE TABLE IF NOT EXISTS `patient_radiology_bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `radiology_test_id` int(11) NOT NULL,
  `charge` int(20) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('paid','settled','unpaid') NOT NULL DEFAULT 'unpaid',
  PRIMARY KEY (`id`),
  KEY `FK__patients1` (`patient_id`),
  KEY `FK__radiology_test_id` (`radiology_test_id`),
  KEY `FK__visits_id3` (`visit_id`),
  KEY `radiology_bill_ibfk_1` (`created_by`),
  KEY `radiology_bill_ibfk_2` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `patient_service_bill`
--

CREATE TABLE IF NOT EXISTS `patient_service_bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `charge` int(20) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('paid','settled','unpaid') NOT NULL DEFAULT 'unpaid',
  PRIMARY KEY (`id`),
  KEY `FK__patients` (`patient_id`),
  KEY `FK__services` (`service_id`),
  KEY `FK__visits_id2` (`visit_id`),
  KEY `service_bill_ibfk_1` (`created_by`),
  KEY `service_bill_ibfk_2` (`updated_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `patient_specimen`
--

CREATE TABLE IF NOT EXISTS `patient_specimen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lab_test_id` int(11) DEFAULT NULL,
  `lab_test_requested_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  `created_on` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__lab_test_requests` (`lab_test_id`),
  KEY `FK__users` (`created_by`),
  KEY `FK_patient_specimen_lab_test_requests` (`lab_test_requested_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pay_grade`
--

CREATE TABLE IF NOT EXISTS `pay_grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_grade_name` varchar(50) NOT NULL,
  `min_salary` float NOT NULL,
  `max_salary` float NOT NULL,
  `remarks` text,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pay_grade_users` (`created_by`),
  KEY `FK_pay_grade_users_2` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pharmacy_inventory`
--

CREATE TABLE IF NOT EXISTS `pharmacy_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `drug_name_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `manufacture_name` varchar(50) NOT NULL,
  `unit_vol` varchar(50) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `manufacture_date` date NOT NULL,
  `expiry_date` date NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK__drugs` (`drug_name_id`),
  KEY `FK_pharmacy_inventory_units` (`unit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `pharmacy_request`
--

CREATE TABLE IF NOT EXISTS `pharmacy_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `drug_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `request_date` int(11) NOT NULL,
  `pay_status` enum('paid','unpaid','settled') NOT NULL DEFAULT 'unpaid',
  `cost` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__patients_pharma_request` (`patient_id`),
  KEY `FK__visits_pharma_request` (`visit_id`),
  KEY `FK__drugs_pharma_request` (`drug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `procedures`
--

CREATE TABLE IF NOT EXISTS `procedures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_name` varchar(50) DEFAULT NULL,
  `price` double NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `procedures_requests`
--

CREATE TABLE IF NOT EXISTS `procedures_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `procedure_id` int(11) NOT NULL,
  `request_date` int(20) NOT NULL,
  `pay_status` enum('paid','unpaid','settled') NOT NULL DEFAULT 'unpaid',
  `test_status` enum('pending','done','resultsent') NOT NULL DEFAULT 'pending',
  `cost` double NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK__patients_fk_4` (`patient_id`),
  KEY `FK__visits_fk_4` (`visit_id`),
  KEY `FK__procedures_fk_4` (`procedure_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `radiology_tests`
--

CREATE TABLE IF NOT EXISTS `radiology_tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `test_code` varchar(255) DEFAULT 'Test Code',
  `fee` double NOT NULL,
  `description` text NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) DEFAULT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `radiology_test_request`
--

CREATE TABLE IF NOT EXISTS `radiology_test_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `visit_id` int(11) NOT NULL,
  `radiology_test_id` int(11) NOT NULL,
  `request_date` int(20) NOT NULL,
  `pay_status` enum('paid','unpaid','settled') NOT NULL DEFAULT 'unpaid',
  `test_status` enum('pending','done','resultsent') NOT NULL DEFAULT 'pending',
  `cost` double NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK__patients_fk_2` (`patient_id`),
  KEY `FK__visits_fk_2` (`visit_id`),
  KEY `FK__radiology_tests_fk_2` (`radiology_test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `radiology_test_result`
--

CREATE TABLE IF NOT EXISTS `radiology_test_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) DEFAULT NULL,
  `report` text NOT NULL,
  `images_path` varchar(300) DEFAULT NULL,
  `created_on` int(11) DEFAULT NULL,
  `updated_on` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__patient_specimen2` (`request_id`),
  KEY `FK__users_12` (`created_by`),
  KEY `FK__users_32` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `received_orders`
--

CREATE TABLE IF NOT EXISTS `received_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `unit_cost` varchar(250) NOT NULL,
  `received_by` int(11) NOT NULL,
  `received_date` int(11) NOT NULL,
  `batch_no` varchar(250) NOT NULL,
  `invoice_no` varchar(250) NOT NULL,
  `mfg_date` date DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `discount` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__received__orders_orders` (`order_id`),
  KEY `FK__received-orders__items` (`item_id`),
  KEY `FK__received-orders__users` (`received_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=80 ;

-- --------------------------------------------------------

--
-- Table structure for table `receiver`
--

CREATE TABLE IF NOT EXISTS `receiver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `return_to_seller`
--

CREATE TABLE IF NOT EXISTS `return_to_seller` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `batch_no` int(11) NOT NULL,
  `mfg_date` date NOT NULL,
  `exp_date` date NOT NULL,
  `quantity` int(20) NOT NULL,
  `return_date` date NOT NULL,
  `received_by` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__items_return` (`item_id`),
  KEY `FK__seller_return` (`seller_id`),
  KEY `FK__users_return` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `review_staff`
--

CREATE TABLE IF NOT EXISTS `review_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `dependability_rating` int(11) NOT NULL,
  `cooperativeness_rating` int(11) NOT NULL,
  `adaptability_rating` int(11) NOT NULL,
  `sercice_rating` int(11) NOT NULL,
  `attedance_rating` int(11) NOT NULL,
  `communication_rating` int(11) NOT NULL,
  `comments` text,
  `review_by` int(11) NOT NULL,
  `review_date` int(20) NOT NULL,
  `created_by` int(20) NOT NULL,
  `created_on` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__users_review_by` (`staff_id`),
  KEY `FK__users_2_review_by` (`review_by`),
  KEY `FK_review_staff_users` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roaster_management`
--

CREATE TABLE IF NOT EXISTS `roaster_management` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roaster_staff_id` int(20) NOT NULL,
  `assigned_staff_id` int(20) DEFAULT NULL,
  `date` int(20) NOT NULL,
  `created_on` int(20) NOT NULL,
  `created_by` int(20) NOT NULL,
  `available` enum('A','P') NOT NULL DEFAULT 'P',
  PRIMARY KEY (`id`),
  KEY `FK__users_roaster` (`roaster_staff_id`),
  KEY `FK__users_2_roaster` (`assigned_staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE IF NOT EXISTS `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_type_id` int(11) NOT NULL,
  `number` varchar(30) NOT NULL,
  `description` text,
  `availability` enum('0','1') DEFAULT '0',
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK_rooms_room_type` (`room_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

-- --------------------------------------------------------

--
-- Table structure for table `room_type`
--

CREATE TABLE IF NOT EXISTS `room_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `description` text,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `seller`
--

CREATE TABLE IF NOT EXISTS `seller` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `firm_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `description` text,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `cost` double NOT NULL,
  `fee` double NOT NULL,
  `description` tinytext,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `FK_service_type_id` (`service_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `services_request`
--

CREATE TABLE IF NOT EXISTS `services_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL DEFAULT '0',
  `visit_id` int(11) NOT NULL DEFAULT '0',
  `service_id` int(11) NOT NULL DEFAULT '0',
  `request_date` int(20) NOT NULL DEFAULT '0',
  `pay_status` enum('paid','unpaid','settled') NOT NULL DEFAULT 'unpaid',
  `test_status` enum('pending','done','resultsent') NOT NULL DEFAULT 'pending',
  `cost` double NOT NULL DEFAULT '0',
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK__pat` (`patient_id`),
  KEY `FK__vis` (`visit_id`),
  KEY `FK__ser` (`service_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `service_type`
--

CREATE TABLE IF NOT EXISTS `service_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mac_address` varchar(32) NOT NULL,
  `ip_address` varchar(32) NOT NULL,
  `login_timestamp` int(20) NOT NULL,
  `status` varchar(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `session_ibfk_1` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=135 ;

-- --------------------------------------------------------

--
-- Table structure for table `specimens_types`
--

CREATE TABLE IF NOT EXISTS `specimens_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `staff_attendance`
--

CREATE TABLE IF NOT EXISTS `staff_attendance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `punch_in` int(20) NOT NULL,
  `punch_out` int(20) NOT NULL,
  `remarks` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__users_staff_attendance` (`staff_id`),
  KEY `FK__users_2_staff_attendance` (`created_by`),
  KEY `FK__users_3_staff_attendance` (`updated_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `staff_date_schedule`
--

CREATE TABLE IF NOT EXISTS `staff_date_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_type_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `time_duration` int(11) NOT NULL,
  `date` date NOT NULL,
  `is_available` enum('available','notAvailable') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__staff_type_daily` (`staff_type_id`),
  KEY `FK__users_daily` (`staff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `staff_leaves`
--

CREATE TABLE IF NOT EXISTS `staff_leaves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `leave_type_id` int(11) NOT NULL,
  `leave_reason` varchar(250) NOT NULL,
  `leave_date` int(20) NOT NULL,
  `leave_status` enum('taken','not_taken','schedule','cancel','approval') NOT NULL DEFAULT 'schedule',
  `remarks` text NOT NULL,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__users_staff_leaves` (`staff_id`),
  KEY `FK__leave_type_staff_leaves` (`leave_type_id`),
  KEY `FK_staff_leaves_users` (`created_by`),
  KEY `FK_staff_leaves_users_2` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `staff_performance`
--

CREATE TABLE IF NOT EXISTS `staff_performance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `remarks` text NOT NULL,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(20) NOT NULL,
  `updated_by` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_users_staff_performance` (`staff_id`),
  KEY `FK_staff_performance_users` (`created_by`),
  KEY `FK_staff_performance_users_2` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `staff_salary`
--

CREATE TABLE IF NOT EXISTS `staff_salary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `pay_grade_id` int(11) NOT NULL,
  `mode_of_transaction` enum('cash','check','online_transaction') NOT NULL DEFAULT 'cash',
  `transaction_id` varchar(50) NOT NULL DEFAULT 'cash',
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(20) NOT NULL,
  `updated_by` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_users_staff_salary` (`staff_id`),
  KEY `FK_pay_grade_staff_salary` (`pay_grade_id`),
  KEY `FK_staff_salary_users` (`created_by`),
  KEY `FK_staff_salary_users_2` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `staff_type`
--

CREATE TABLE IF NOT EXISTS `staff_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  `user_code` int(11) NOT NULL DEFAULT '12',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `staff_weekly_schedule`
--

CREATE TABLE IF NOT EXISTS `staff_weekly_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_type_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `time_duration` int(11) NOT NULL,
  `day` enum('0','1','2','3','4','5','6') NOT NULL,
  `is_available` enum('available','notAvailable') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__staff_type_id` (`staff_type_id`),
  KEY `FK__users_id` (`staff_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=127 ;

-- --------------------------------------------------------

--
-- Table structure for table `stocking`
--

CREATE TABLE IF NOT EXISTS `stocking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Item` int(11) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `expire_date` date DEFAULT NULL,
  `notes` text,
  `shelf_no` varchar(100) DEFAULT NULL,
  `stocking_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_stocking_items` (`Item`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `transaction_type_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `credit` double DEFAULT NULL,
  `debit` double DEFAULT NULL,
  `processed_on` int(20) NOT NULL,
  `processed_by` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_transactions_patient_id` (`patient_id`),
  KEY `fk_transactions_transaction_type_id` (`transaction_type_id`),
  KEY `fk_transactions_processed_by` (`processed_by`),
  KEY `FK_transactions_invoice_details` (`invoice_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `treatments`
--

CREATE TABLE IF NOT EXISTS `treatments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` int(20) NOT NULL,
  `treatment` text NOT NULL,
  `status` enum('A','I') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_treatments_users` (`created_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `triage`
--

CREATE TABLE IF NOT EXISTS `triage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visit_id` int(11) NOT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `weight` varchar(10) DEFAULT NULL,
  `temperature` varchar(10) DEFAULT NULL,
  `height` varchar(10) DEFAULT NULL,
  `pulse` varchar(10) DEFAULT NULL,
  `blood_pressure` varchar(10) DEFAULT NULL,
  `head_circum` varchar(10) DEFAULT NULL,
  `body_surface_area` varchar(10) DEFAULT NULL,
  `respiration_rate` varchar(10) DEFAULT NULL,
  `oxygen_saturation` varchar(10) DEFAULT NULL,
  `heart_rate` varchar(10) DEFAULT NULL,
  `bmi` varchar(10) DEFAULT NULL,
  `notes` text,
  `created_on` int(20) NOT NULL,
  `created_by` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_triage_visit_type` (`visit_id`),
  KEY `FK_triage_patients` (`patient_id`),
  KEY `FK_triage_users` (`created_on`),
  KEY `FK_triage_users_2` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE IF NOT EXISTS `units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit` varchar(50) NOT NULL,
  `unit_abbr` varchar(20) NOT NULL,
  `description` text,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unit_abbr` (`unit_abbr`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `gender` enum('M','F','NM') NOT NULL,
  `martial_status` enum('M','U','D','NM') DEFAULT NULL,
  `dob` date NOT NULL,
  `address` text NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `zip` varchar(15) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email_id` varchar(50) NOT NULL,
  `joining_date` date NOT NULL,
  `emergency_name` varchar(50) DEFAULT NULL,
  `emergency_contact` varchar(15) DEFAULT NULL,
  `referred_by` varchar(50) DEFAULT NULL,
  `staff_type_id` int(11) NOT NULL,
  `created_on` int(20) DEFAULT NULL,
  `updated_on` int(20) DEFAULT NULL,
  `id_proof_type` varchar(50) DEFAULT NULL,
  `id_proof_num` varchar(250) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `degree_held` text,
  `designation` int(11) DEFAULT NULL,
  `remarks` text,
  `images_path` varchar(300) DEFAULT NULL,
  `password` varchar(285) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `salutation` varchar(10) DEFAULT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_id` (`email_id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `FK_city_id` (`city_id`),
  KEY `FK_country_id` (`country_id`),
  KEY `FK_state_id` (`state_id`),
  KEY `FK_designation` (`department_id`),
  KEY `FK_staff_type` (`staff_type_id`),
  KEY `FK_designation_id` (`designation`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_type_access`
--

CREATE TABLE IF NOT EXISTS `user_type_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_id` int(11) NOT NULL,
  `screen_id` int(11) NOT NULL,
  `view` tinyint(4) DEFAULT '0',
  `update` tinyint(4) DEFAULT '0',
  `delete` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK__staff_type` (`user_type_id`),
  KEY `FK__pages` (`screen_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=841 ;

-- --------------------------------------------------------

--
-- Table structure for table `vision_assissment`
--

CREATE TABLE IF NOT EXISTS `vision_assissment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visit_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `eye_status` varchar(30) DEFAULT NULL,
  `visualacuityright` varchar(30) DEFAULT NULL,
  `visualacuityleft` varchar(30) DEFAULT NULL,
  `visualacuityextright` varchar(30) DEFAULT NULL,
  `visualacuityextleft` varchar(30) DEFAULT NULL,
  `handmovementright` varchar(30) DEFAULT NULL,
  `handmovementleft` varchar(30) DEFAULT NULL,
  `preceptionright` varchar(30) DEFAULT NULL,
  `preceptionleft` varchar(30) DEFAULT NULL,
  `Comment` varchar(30) DEFAULT NULL,
  `notes` text,
  `created_on` int(20) NOT NULL,
  `created_by` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_vision_visit_type` (`visit_id`),
  KEY `FK_vision_patients` (`patient_id`),
  KEY `FK_vision_users` (`created_on`),
  KEY `FK_vision_users_2` (`created_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `visits`
--

CREATE TABLE IF NOT EXISTS `visits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visit_type` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `department` int(11) DEFAULT NULL,
  `remark` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `opd_room_id` int(11) DEFAULT NULL,
  `consultant_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  `triage_status` enum('Taken','UnTaken') DEFAULT 'UnTaken',
  `prescription_status` enum('Yes','No') DEFAULT 'No',
  `case_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `visits_ibfk_2` (`patient_id`),
  KEY `visits_ibfk_3` (`created_by`),
  KEY `visits_ibfk_4` (`updated_by`),
  KEY `fk_visit_type` (`visit_type`),
  KEY `FK_visits_users` (`consultant_id`),
  KEY `FK_visits_opd_rooms` (`opd_room_id`),
  KEY `FK_visits_department` (`department`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `visit_type`
--

CREATE TABLE IF NOT EXISTS `visit_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `updated_on` int(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`),
  KEY `visit_type_ibfk_1` (`created_by`),
  KEY `visit_type_ibfk_2` (`updated_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `wards`
--

CREATE TABLE IF NOT EXISTS `wards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `ward_type` enum('Male','Female') NOT NULL,
  `department` int(11) NOT NULL,
  `description` text,
  `created_on` int(20) NOT NULL,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK_wards_department_2` (`department`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `ward_room`
--

CREATE TABLE IF NOT EXISTS `ward_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ward_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `room_type_id` int(11) NOT NULL,
  `number` varchar(30) NOT NULL,
  `description` text,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `ward_id` (`ward_id`),
  KEY `FK_rooms_room_type` (`room_type_id`),
  KEY `FK_ward_room_department` (`department_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

-- --------------------------------------------------------

--
-- Table structure for table `ward_room_type`
--

CREATE TABLE IF NOT EXISTS `ward_room_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `description` text,
  `status` enum('A','I') NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `weekly_schedule_slots`
--

CREATE TABLE IF NOT EXISTS `weekly_schedule_slots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_id` int(11) NOT NULL,
  `from_time` varchar(20) NOT NULL,
  `to_time` varchar(20) NOT NULL,
  `notes` text,
  PRIMARY KEY (`id`),
  KEY `FK__staff_weekly_schedule` (`schedule_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `allergies`
--
ALTER TABLE `allergies`
  ADD CONSTRAINT `fk_allergy_cat_id` FOREIGN KEY (`allergy_cat_id`) REFERENCES `allergies_categories` (`id`);

--
-- Constraints for table `allergy_request`
--
ALTER TABLE `allergy_request`
  ADD CONSTRAINT `FK__allergies_allergy` FOREIGN KEY (`allergy_id`) REFERENCES `allergies` (`id`),
  ADD CONSTRAINT `FK__patients_allergy` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__users_allergy` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__visits_allergy` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

--
-- Constraints for table `appointments`
--
ALTER TABLE `appointments`
  ADD CONSTRAINT `FK_appointments_patients` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK_appointments_visit_type` FOREIGN KEY (`visit_type_id`) REFERENCES `visit_type` (`id`),
  ADD CONSTRAINT `FK__users_appointment` FOREIGN KEY (`consultant_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `beds`
--
ALTER TABLE `beds`
  ADD CONSTRAINT `beds_ibfk_1` FOREIGN KEY (`ward_id`) REFERENCES `wards` (`id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `FK_beds_department` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `FK_beds_rooms` FOREIGN KEY (`room_id`) REFERENCES `ward_room` (`id`) ON DELETE NO ACTION;

--
-- Constraints for table `beds_ipd_history`
--
ALTER TABLE `beds_ipd_history`
  ADD CONSTRAINT `FK__beds_history` FOREIGN KEY (`bed_id`) REFERENCES `beds` (`id`),
  ADD CONSTRAINT `FK__beds_history_2` FOREIGN KEY (`old_bed_id`) REFERENCES `beds` (`id`),
  ADD CONSTRAINT `FK__ipd_registration_history` FOREIGN KEY (`ipd_id`) REFERENCES `ipd_registration` (`id`),
  ADD CONSTRAINT `FK__users_history` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__users_history_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `cash_account`
--
ALTER TABLE `cash_account`
  ADD CONSTRAINT `cash_amount_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_cash_account_patients` FOREIGN KEY (`depositor_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `fk_patient_id` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`);

--
-- Constraints for table `clinical_findings`
--
ALTER TABLE `clinical_findings`
  ADD CONSTRAINT `fk_cf_patient_id` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `fk_cf_visit_id` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`),
  ADD CONSTRAINT `fk_visit_id` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

--
-- Constraints for table `consulation_type`
--
ALTER TABLE `consulation_type`
  ADD CONSTRAINT `consulation_type_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `consulation_type_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `date_schedule_slots`
--
ALTER TABLE `date_schedule_slots`
  ADD CONSTRAINT `FK__staff_date_schedule` FOREIGN KEY (`schedule_id`) REFERENCES `staff_date_schedule` (`id`);

--
-- Constraints for table `default_leaves`
--
ALTER TABLE `default_leaves`
  ADD CONSTRAINT `FK_default_leaves_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_default_leaves_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__leave_type` FOREIGN KEY (`leave_type_id`) REFERENCES `leave_type` (`id`);

--
-- Constraints for table `diagnosis`
--
ALTER TABLE `diagnosis`
  ADD CONSTRAINT `fk_diagnosis_cat_id` FOREIGN KEY (`diagnosis_cat_id`) REFERENCES `diagnosis_categories` (`id`);

--
-- Constraints for table `drugs`
--
ALTER TABLE `drugs`
  ADD CONSTRAINT `drugs_ibfk_1` FOREIGN KEY (`drug_category_id`) REFERENCES `drug_categories` (`id`),
  ADD CONSTRAINT `drugs_ibfk_2` FOREIGN KEY (`unit`) REFERENCES `units` (`id`),
  ADD CONSTRAINT `drugs_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `drugs_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `entitlements`
--
ALTER TABLE `entitlements`
  ADD CONSTRAINT `FK_entitlements_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_entitlements_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__leave_period_entitlements` FOREIGN KEY (`leave_period_id`) REFERENCES `leave_period` (`id`),
  ADD CONSTRAINT `FK__leave_type_entitlements` FOREIGN KEY (`leave_type_id`) REFERENCES `leave_type` (`id`),
  ADD CONSTRAINT `FK__staff_type_entitlements` FOREIGN KEY (`staff_type_id`) REFERENCES `staff_type` (`id`);

--
-- Constraints for table `forensic_tests`
--
ALTER TABLE `forensic_tests`
  ADD CONSTRAINT `FK_forensic_tests_specimens_types` FOREIGN KEY (`specimen_id`) REFERENCES `specimens_types` (`id`),
  ADD CONSTRAINT `FK_forensic_tests_units` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`);

--
-- Constraints for table `forensic_test_request`
--
ALTER TABLE `forensic_test_request`
  ADD CONSTRAINT `FK__forensic_tests_fk_3` FOREIGN KEY (`forensic_test_id`) REFERENCES `forensic_tests` (`id`),
  ADD CONSTRAINT `FK__patients_fk_3` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__visits_fk_3` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

--
-- Constraints for table `forensic_test_result`
--
ALTER TABLE `forensic_test_result`
  ADD CONSTRAINT `FK__patient_specimen1` FOREIGN KEY (`specimen_id`) REFERENCES `patient_forensic_specimen` (`id`),
  ADD CONSTRAINT `FK__users_11` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__users_31` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `holidays`
--
ALTER TABLE `holidays`
  ADD CONSTRAINT `FK__created_on_holidays` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__updated_on_holidays` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `hospital_profile`
--
ALTER TABLE `hospital_profile`
  ADD CONSTRAINT `FK__locations` FOREIGN KEY (`country_id`) REFERENCES `locations` (`location_id`),
  ADD CONSTRAINT `FK__locations_2` FOREIGN KEY (`state_id`) REFERENCES `locations` (`location_id`),
  ADD CONSTRAINT `FK__locations_3` FOREIGN KEY (`city_id`) REFERENCES `locations` (`location_id`);

--
-- Constraints for table `insurance`
--
ALTER TABLE `insurance`
  ADD CONSTRAINT `FK__insurance_companies_1` FOREIGN KEY (`insurance_company_id`) REFERENCES `insurance_companies` (`id`),
  ADD CONSTRAINT `FK__insurance_plan` FOREIGN KEY (`insurance_plan_id`) REFERENCES `insurance_plan` (`id`),
  ADD CONSTRAINT `FK__insurance_type` FOREIGN KEY (`insurance_type_id`) REFERENCES `insurance_type` (`id`),
  ADD CONSTRAINT `FK__patients_insurance` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__users_insurance_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__users_insurance_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `insurance_companies`
--
ALTER TABLE `insurance_companies`
  ADD CONSTRAINT `FK_insurance_city_id` FOREIGN KEY (`city_id`) REFERENCES `locations` (`location_id`),
  ADD CONSTRAINT `FK_insurance_country_id` FOREIGN KEY (`country_id`) REFERENCES `locations` (`location_id`),
  ADD CONSTRAINT `FK_insurance_state_id` FOREIGN KEY (`state_id`) REFERENCES `locations` (`location_id`);

--
-- Constraints for table `insurance_plan`
--
ALTER TABLE `insurance_plan`
  ADD CONSTRAINT `FK__insurance_companies` FOREIGN KEY (`company_name_id`) REFERENCES `insurance_companies` (`id`),
  ADD CONSTRAINT `FK__users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__users_insurance` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `ipd_cases`
--
ALTER TABLE `ipd_cases`
  ADD CONSTRAINT `FK__department_ipd` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  ADD CONSTRAINT `FK__patients_ipd` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__visits_ipd` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

--
-- Constraints for table `ipd_lab_test_requests`
--
ALTER TABLE `ipd_lab_test_requests`
  ADD CONSTRAINT `FK__ipd_id_1` FOREIGN KEY (`ipd_id`) REFERENCES `ipd_registration` (`id`),
  ADD CONSTRAINT `FK__lab_fk_12` FOREIGN KEY (`lab_test_id`) REFERENCES `lab_tests` (`id`),
  ADD CONSTRAINT `FK__patient_fk_12` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__requested_by` FOREIGN KEY (`requested_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `ipd_lab_test_request_component`
--
ALTER TABLE `ipd_lab_test_request_component`
  ADD CONSTRAINT `FK__ipd_lab_test_request_id` FOREIGN KEY (`ipd_lab_test_request_id`) REFERENCES `ipd_lab_test_requests` (`id`),
  ADD CONSTRAINT `FK__lab_componet_id_1` FOREIGN KEY (`lab_component_id`) REFERENCES `lab_test_component` (`id`);

--
-- Constraints for table `ipd_lab_test_result`
--
ALTER TABLE `ipd_lab_test_result`
  ADD CONSTRAINT `FK_lab_test_result_lab_test_component_ipd11` FOREIGN KEY (`lab_test_component_id`) REFERENCES `lab_test_component` (`id`),
  ADD CONSTRAINT `FK__patient_specimen_ipd1` FOREIGN KEY (`ipd_specimen_id`) REFERENCES `ipd_patient_specimen` (`id`),
  ADD CONSTRAINT `FK__users__ipd1` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__users__ipd11` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `ipd_patient_request`
--
ALTER TABLE `ipd_patient_request`
  ADD CONSTRAINT `FK__users_2_ipd_patient_request` FOREIGN KEY (`operating_consultant_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__users_3_ipd_patient_request` FOREIGN KEY (`attending_consultant_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__users_ipd_patient_request` FOREIGN KEY (`patient_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__visits_ipd_patient_request` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

--
-- Constraints for table `ipd_patient_specimen`
--
ALTER TABLE `ipd_patient_specimen`
  ADD CONSTRAINT `FK_patient_specimen_lab_test_requests1` FOREIGN KEY (`ipd_lab_test_requested_id`) REFERENCES `ipd_lab_test_requests` (`id`),
  ADD CONSTRAINT `FK__lab_test_requests1` FOREIGN KEY (`lab_test_id`) REFERENCES `lab_test_requests` (`id`),
  ADD CONSTRAINT `FK__users1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `ipd_registration`
--
ALTER TABLE `ipd_registration`
  ADD CONSTRAINT `FK__patients_icd` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__users_icd_1` FOREIGN KEY (`attending_consultant_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__users_icd_2` FOREIGN KEY (`operating_consultant_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__users_icd_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__users_icd_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `ipd_treatment`
--
ALTER TABLE `ipd_treatment`
  ADD CONSTRAINT `FK__ipd_registration` FOREIGN KEY (`ipd_id`) REFERENCES `ipd_registration` (`id`),
  ADD CONSTRAINT `FK__users_treatment` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `ipd_treatment_details`
--
ALTER TABLE `ipd_treatment_details`
  ADD CONSTRAINT `FK__ipd_treatment` FOREIGN KEY (`ipd_treatment_id`) REFERENCES `ipd_treatment` (`id`),
  ADD CONSTRAINT `FK__treatments` FOREIGN KEY (`treatment_id`) REFERENCES `treatments` (`id`);

--
-- Constraints for table `issuance`
--
ALTER TABLE `issuance`
  ADD CONSTRAINT `FK__issuance_items` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `FK__issuance_users` FOREIGN KEY (`issued_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `fk_item_category_id` FOREIGN KEY (`item_category_id`) REFERENCES `item_categories` (`id`);

--
-- Constraints for table `item_categories`
--
ALTER TABLE `item_categories`
  ADD CONSTRAINT `FK_item_categories_inventory_type` FOREIGN KEY (`inventory_type_id`) REFERENCES `inventory_type` (`id`);

--
-- Constraints for table `item_price`
--
ALTER TABLE `item_price`
  ADD CONSTRAINT `FK_item_price_items` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `FK_item_price_users` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `item_request`
--
ALTER TABLE `item_request`
  ADD CONSTRAINT `FK__inventory_type_item_request` FOREIGN KEY (`inventory_type_id`) REFERENCES `inventory_type` (`id`),
  ADD CONSTRAINT `FK__items_item_request` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `FK__users_item_request` FOREIGN KEY (`ordered_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `item_response`
--
ALTER TABLE `item_response`
  ADD CONSTRAINT `FK__inventory_type_item_response` FOREIGN KEY (`inventory_type_id`) REFERENCES `inventory_type` (`id`),
  ADD CONSTRAINT `FK__items_item_response` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `FK__users_item_response` FOREIGN KEY (`dispatched_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `item_stock`
--
ALTER TABLE `item_stock`
  ADD CONSTRAINT `FK_item_stock_users` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__items_item_stock` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `FK__users_item_stock` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `lab_tests`
--
ALTER TABLE `lab_tests`
  ADD CONSTRAINT `lab_tests_ibfk_1` FOREIGN KEY (`lab_type_id`) REFERENCES `lab_types` (`id`),
  ADD CONSTRAINT `lab_tests_ibfk_2` FOREIGN KEY (`specimen_id`) REFERENCES `specimens_types` (`id`);

--
-- Constraints for table `lab_test_component`
--
ALTER TABLE `lab_test_component`
  ADD CONSTRAINT `FK__lab_tests` FOREIGN KEY (`lab_test_id`) REFERENCES `lab_tests` (`id`),
  ADD CONSTRAINT `FK__units` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`);

--
-- Constraints for table `lab_test_requests`
--
ALTER TABLE `lab_test_requests`
  ADD CONSTRAINT `FK__lab_fk_1` FOREIGN KEY (`lab_test_id`) REFERENCES `lab_tests` (`id`),
  ADD CONSTRAINT `FK__patient_fk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__visit_fk_1` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

--
-- Constraints for table `lab_test_request_component`
--
ALTER TABLE `lab_test_request_component`
  ADD CONSTRAINT `FK__lab_componet_id` FOREIGN KEY (`lab_component_id`) REFERENCES `lab_test_component` (`id`),
  ADD CONSTRAINT `FK__lab_test_request_id` FOREIGN KEY (`lab_test_request_id`) REFERENCES `lab_test_requests` (`id`);

--
-- Constraints for table `lab_test_result`
--
ALTER TABLE `lab_test_result`
  ADD CONSTRAINT `FK_lab_test_result_lab_test_component` FOREIGN KEY (`lab_test_component_id`) REFERENCES `lab_test_component` (`id`),
  ADD CONSTRAINT `FK__patient_specimen` FOREIGN KEY (`specimen_id`) REFERENCES `patient_specimen` (`id`),
  ADD CONSTRAINT `FK__users_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__users_3` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `leave_period`
--
ALTER TABLE `leave_period`
  ADD CONSTRAINT `FK_leave_period_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_leave_period_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `leave_type`
--
ALTER TABLE `leave_type`
  ADD CONSTRAINT `FK_leave_type_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_leave_type_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `locations`
--
ALTER TABLE `locations`
  ADD CONSTRAINT `locations_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `locations` (`location_id`) ON DELETE CASCADE;

--
-- Constraints for table `opd_consultants`
--
ALTER TABLE `opd_consultants`
  ADD CONSTRAINT `FK_opd_consultants_opd_rooms` FOREIGN KEY (`opd_room_id`) REFERENCES `opd_rooms` (`id`),
  ADD CONSTRAINT `FK_opd_consultants_users` FOREIGN KEY (`consultant_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `opd_rooms`
--
ALTER TABLE `opd_rooms`
  ADD CONSTRAINT `FK_opd_rooms_department` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  ADD CONSTRAINT `FK_opd_rooms_room` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  ADD CONSTRAINT `FK_opd_rooms_users` FOREIGN KEY (`consultant_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `order-item`
--
ALTER TABLE `order-item`
  ADD CONSTRAINT `FK__items_order-item` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `FK__orders_order-item` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FK__seller_orders` FOREIGN KEY (`seller_id`) REFERENCES `seller` (`id`),
  ADD CONSTRAINT `FK__users_orders` FOREIGN KEY (`ordered_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `patients`
--
ALTER TABLE `patients`
  ADD CONSTRAINT `FK_cityid` FOREIGN KEY (`city_id`) REFERENCES `locations` (`location_id`),
  ADD CONSTRAINT `FK_countryid` FOREIGN KEY (`country_id`) REFERENCES `locations` (`location_id`),
  ADD CONSTRAINT `FK_patients_patients` FOREIGN KEY (`fan_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK_stateid` FOREIGN KEY (`state_id`) REFERENCES `locations` (`location_id`),
  ADD CONSTRAINT `patients_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `patients_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `patients_prescription_request`
--
ALTER TABLE `patients_prescription_request`
  ADD CONSTRAINT `FK__drugs12` FOREIGN KEY (`drug_id`) REFERENCES `drugs` (`id`),
  ADD CONSTRAINT `FK__patients12` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__visits12` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

--
-- Constraints for table `patients_questionnaire`
--
ALTER TABLE `patients_questionnaire`
  ADD CONSTRAINT `FK3_created_by_question` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__patients_question` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__visits_question` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

--
-- Constraints for table `patient_account`
--
ALTER TABLE `patient_account`
  ADD CONSTRAINT `FK_patient_account_patient_account` FOREIGN KEY (`parent_id`) REFERENCES `patient_account` (`id`);

--
-- Constraints for table `patient_bill_payment`
--
ALTER TABLE `patient_bill_payment`
  ADD CONSTRAINT `FK__visits_id` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

--
-- Constraints for table `patient_diagnosis_request`
--
ALTER TABLE `patient_diagnosis_request`
  ADD CONSTRAINT `FK3_created_by_diagnosis` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_patient_diagnosis_request_diseases` FOREIGN KEY (`disease_id`) REFERENCES `diseases` (`id`),
  ADD CONSTRAINT `FK__patients_diagnosis` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__visits_diagnosis` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

--
-- Constraints for table `patient_forensic_bill`
--
ALTER TABLE `patient_forensic_bill`
  ADD CONSTRAINT `FK__forensic_test_id` FOREIGN KEY (`forensic_test_id`) REFERENCES `forensic_tests` (`id`),
  ADD CONSTRAINT `FK__patient` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__visits_1` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`),
  ADD CONSTRAINT `forensic_billnts_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `forensic_bill_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `patient_forensic_specimen`
--
ALTER TABLE `patient_forensic_specimen`
  ADD CONSTRAINT `FK_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__forensic_test_request` FOREIGN KEY (`forensic_test_id`) REFERENCES `forensic_test_request` (`id`);

--
-- Constraints for table `patient_lab_bill`
--
ALTER TABLE `patient_lab_bill`
  ADD CONSTRAINT `FK__lab_tests_1` FOREIGN KEY (`lab_test_id`) REFERENCES `lab_tests` (`id`),
  ADD CONSTRAINT `FK__patients_id1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__visits_id1` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`),
  ADD CONSTRAINT `lab_bill_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `lab_bill_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `patient_pharmacy_bill`
--
ALTER TABLE `patient_pharmacy_bill`
  ADD CONSTRAINT `FK__patient6` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__visits_6` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`),
  ADD CONSTRAINT `pharmacy_bill_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `pharmacy_bill_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `patient_pharmacy_extra`
--
ALTER TABLE `patient_pharmacy_extra`
  ADD CONSTRAINT `FK__drugs_extra` FOREIGN KEY (`drug_id`) REFERENCES `drugs` (`id`),
  ADD CONSTRAINT `FK__patients_extra` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__visits_extra` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

--
-- Constraints for table `patient_procedure_bill`
--
ALTER TABLE `patient_procedure_bill`
  ADD CONSTRAINT `FK_patient_procedure_bill_patients` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK_patient_procedure_bill_procedures` FOREIGN KEY (`procedure_id`) REFERENCES `procedures` (`id`),
  ADD CONSTRAINT `FK_patient_procedure_bill_visits` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`),
  ADD CONSTRAINT `procedure_bill_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `procedure_bill_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `patient_radiology_bill`
--
ALTER TABLE `patient_radiology_bill`
  ADD CONSTRAINT `FK__patients1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__radiology_test_id` FOREIGN KEY (`radiology_test_id`) REFERENCES `radiology_tests` (`id`),
  ADD CONSTRAINT `FK__visits_id3` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`),
  ADD CONSTRAINT `radiology_bill_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `radiology_bill_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `patient_service_bill`
--
ALTER TABLE `patient_service_bill`
  ADD CONSTRAINT `FK__patients` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__services` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`),
  ADD CONSTRAINT `FK__visits_id2` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`),
  ADD CONSTRAINT `service_bill_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `service_bill_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `patient_specimen`
--
ALTER TABLE `patient_specimen`
  ADD CONSTRAINT `FK_patient_specimen_lab_test_requests` FOREIGN KEY (`lab_test_requested_id`) REFERENCES `lab_test_requests` (`id`),
  ADD CONSTRAINT `FK__lab_test_requests` FOREIGN KEY (`lab_test_id`) REFERENCES `lab_test_requests` (`id`),
  ADD CONSTRAINT `FK__users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `pay_grade`
--
ALTER TABLE `pay_grade`
  ADD CONSTRAINT `FK_pay_grade_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_pay_grade_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `pharmacy_inventory`
--
ALTER TABLE `pharmacy_inventory`
  ADD CONSTRAINT `FK_pharmacy_inventory_units` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`),
  ADD CONSTRAINT `FK__drugs` FOREIGN KEY (`drug_name_id`) REFERENCES `drugs` (`id`);

--
-- Constraints for table `pharmacy_request`
--
ALTER TABLE `pharmacy_request`
  ADD CONSTRAINT `FK__drugs_pharma_request` FOREIGN KEY (`drug_id`) REFERENCES `drugs` (`id`),
  ADD CONSTRAINT `FK__patients_pharma_request` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__visits_pharma_request` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

--
-- Constraints for table `procedures_requests`
--
ALTER TABLE `procedures_requests`
  ADD CONSTRAINT `FK__patients_fk_4` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__procedures_fk_4` FOREIGN KEY (`procedure_id`) REFERENCES `procedures` (`id`),
  ADD CONSTRAINT `FK__visits_fk_4` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

--
-- Constraints for table `radiology_test_request`
--
ALTER TABLE `radiology_test_request`
  ADD CONSTRAINT `FK__patients_fk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__radiology_tests_fk_2` FOREIGN KEY (`radiology_test_id`) REFERENCES `radiology_tests` (`id`),
  ADD CONSTRAINT `FK__visits_fk_2` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

--
-- Constraints for table `radiology_test_result`
--
ALTER TABLE `radiology_test_result`
  ADD CONSTRAINT `FK_request_id1` FOREIGN KEY (`request_id`) REFERENCES `radiology_test_request` (`id`),
  ADD CONSTRAINT `FK__users_12` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__users_32` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `received_orders`
--
ALTER TABLE `received_orders`
  ADD CONSTRAINT `FK__received-orders__items` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `FK__received-orders__users` FOREIGN KEY (`received_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__received__orders_orders` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `return_to_seller`
--
ALTER TABLE `return_to_seller`
  ADD CONSTRAINT `FK__items_return` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `FK__seller_return` FOREIGN KEY (`seller_id`) REFERENCES `seller` (`id`),
  ADD CONSTRAINT `FK__users_return` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `review_staff`
--
ALTER TABLE `review_staff`
  ADD CONSTRAINT `FK_review_staff_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__users_2_review_by` FOREIGN KEY (`review_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__users_review_by` FOREIGN KEY (`staff_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `roaster_management`
--
ALTER TABLE `roaster_management`
  ADD CONSTRAINT `FK__users_2_roaster` FOREIGN KEY (`assigned_staff_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__users_roaster` FOREIGN KEY (`roaster_staff_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `FK_room_type` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`);

--
-- Constraints for table `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `FK_service_type_id` FOREIGN KEY (`service_type_id`) REFERENCES `service_type` (`id`);

--
-- Constraints for table `services_request`
--
ALTER TABLE `services_request`
  ADD CONSTRAINT `FK__pat` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK__ser` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`),
  ADD CONSTRAINT `FK__vis` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

--
-- Constraints for table `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `session_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `staff_attendance`
--
ALTER TABLE `staff_attendance`
  ADD CONSTRAINT `FK__users_2_staff_attendance` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__users_3_staff_attendance` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__users_staff_attendance` FOREIGN KEY (`staff_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `staff_date_schedule`
--
ALTER TABLE `staff_date_schedule`
  ADD CONSTRAINT `FK__staff_type_daily` FOREIGN KEY (`staff_type_id`) REFERENCES `staff_type` (`id`),
  ADD CONSTRAINT `FK__users_daily` FOREIGN KEY (`staff_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `staff_leaves`
--
ALTER TABLE `staff_leaves`
  ADD CONSTRAINT `FK_staff_leaves_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_staff_leaves_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK__leave_type_staff_leaves` FOREIGN KEY (`leave_type_id`) REFERENCES `leave_type` (`id`),
  ADD CONSTRAINT `FK__users_staff_leaves` FOREIGN KEY (`staff_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `staff_performance`
--
ALTER TABLE `staff_performance`
  ADD CONSTRAINT `FK_staff_performance_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_staff_performance_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_users_staff_performance` FOREIGN KEY (`staff_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `staff_salary`
--
ALTER TABLE `staff_salary`
  ADD CONSTRAINT `FK_pay_grade_staff_salary` FOREIGN KEY (`pay_grade_id`) REFERENCES `pay_grade` (`id`),
  ADD CONSTRAINT `FK_staff_salary_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_staff_salary_users_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_users_staff_salary` FOREIGN KEY (`staff_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `staff_weekly_schedule`
--
ALTER TABLE `staff_weekly_schedule`
  ADD CONSTRAINT `FK__staff_type_id` FOREIGN KEY (`staff_type_id`) REFERENCES `staff_type` (`id`),
  ADD CONSTRAINT `FK__users_id` FOREIGN KEY (`staff_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `stocking`
--
ALTER TABLE `stocking`
  ADD CONSTRAINT `FK_stocking_items` FOREIGN KEY (`Item`) REFERENCES `items` (`id`);

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `FK_transactions_invoice_details` FOREIGN KEY (`invoice_id`) REFERENCES `invoice_details` (`id`),
  ADD CONSTRAINT `fk_transactions_patient_id` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `fk_transactions_processed_by` FOREIGN KEY (`processed_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_transactions_transaction_type_id` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction` (`id`);

--
-- Constraints for table `treatments`
--
ALTER TABLE `treatments`
  ADD CONSTRAINT `FK_treatments_users` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `triage`
--
ALTER TABLE `triage`
  ADD CONSTRAINT `FK_triage_patients` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK_triage_users_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_triage_visit_type` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_city_id` FOREIGN KEY (`city_id`) REFERENCES `locations` (`location_id`),
  ADD CONSTRAINT `FK_country_id` FOREIGN KEY (`country_id`) REFERENCES `locations` (`location_id`),
  ADD CONSTRAINT `FK_department_id` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  ADD CONSTRAINT `FK_designation_id` FOREIGN KEY (`designation`) REFERENCES `designation` (`id`),
  ADD CONSTRAINT `FK_staff_type` FOREIGN KEY (`staff_type_id`) REFERENCES `staff_type` (`id`),
  ADD CONSTRAINT `FK_state_id` FOREIGN KEY (`state_id`) REFERENCES `locations` (`location_id`);

--
-- Constraints for table `user_type_access`
--
ALTER TABLE `user_type_access`
  ADD CONSTRAINT `FK__pages` FOREIGN KEY (`screen_id`) REFERENCES `pages` (`Id`),
  ADD CONSTRAINT `FK__staff_type` FOREIGN KEY (`user_type_id`) REFERENCES `staff_type` (`id`);

--
-- Constraints for table `vision_assissment`
--
ALTER TABLE `vision_assissment`
  ADD CONSTRAINT `FK_vision_patients` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `FK_vision_users_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK_vision_visit_type` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

--
-- Constraints for table `visits`
--
ALTER TABLE `visits`
  ADD CONSTRAINT `FK_visits_department` FOREIGN KEY (`department`) REFERENCES `department` (`id`),
  ADD CONSTRAINT `FK_visits_opd_rooms` FOREIGN KEY (`opd_room_id`) REFERENCES `room` (`id`),
  ADD CONSTRAINT `FK_visits_users` FOREIGN KEY (`consultant_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_visit_type` FOREIGN KEY (`visit_type`) REFERENCES `visit_type` (`id`),
  ADD CONSTRAINT `visits_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `visits_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `visits_ibfk_4` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `visit_type`
--
ALTER TABLE `visit_type`
  ADD CONSTRAINT `visit_type_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `visit_type_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `wards`
--
ALTER TABLE `wards`
  ADD CONSTRAINT `FK_wards_department_2` FOREIGN KEY (`department`) REFERENCES `department` (`id`);

--
-- Constraints for table `ward_room`
--
ALTER TABLE `ward_room`
  ADD CONSTRAINT `FK_rooms_room_type` FOREIGN KEY (`room_type_id`) REFERENCES `ward_room_type` (`id`),
  ADD CONSTRAINT `FK_ward_room_department` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`);

--
-- Constraints for table `weekly_schedule_slots`
--
ALTER TABLE `weekly_schedule_slots`
  ADD CONSTRAINT `FK__staff_weekly_schedule` FOREIGN KEY (`schedule_id`) REFERENCES `staff_weekly_schedule` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


--
-- Add all query after 23 march 
--


 CREATE TABLE `scheme_exclusion` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `insurance_company_id` INT(11) NOT NULL,
 `scheme_plan_id` INT(11) NOT NULL,
 `scheme_name_id` INT(11) NULL DEFAULT NULL,
 `item_name` TEXT NULL,
 `item_id` TEXT NULL,
 `service_type` TEXT NULL,
 `created_on` INT(20) NOT NULL,
 `created_by` INT(11) NOT NULL,
 `status` ENUM('A','I') NOT NULL DEFAULT 'A',
 PRIMARY KEY (`id`),
 INDEX `scheme_exclusionfk_1` (`created_by`),
 INDEX `scheme_exclusionfk_2` (`insurance_company_id`),
 INDEX `scheme_exclusionfk_3` (`scheme_plan_id`),
 INDEX `scheme_exclusionfk_4` (`scheme_name_id`),

 CONSTRAINT `scheme_exclusionfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `scheme_exclusionfk_2` FOREIGN KEY (`insurance_company_id`) REFERENCES `insurance_companies` (`id`),
 CONSTRAINT `scheme_exclusionfk_3` FOREIGN KEY (`scheme_plan_id`) REFERENCES `insurance_plan` (`id`),
 CONSTRAINT `scheme_exclusionfk_4` FOREIGN KEY (`scheme_name_id`) REFERENCES `scheme_plans` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;


CREATE TABLE `insurance_revised_cost` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `insurance_company_id` INT(11) NOT NULL,
 `scheme_plan_id` INT(11) NOT NULL,
 `scheme_name_id` INT(11) NULL DEFAULT NULL,
 `item_name` varchar(100) NULL,
 `item_id` INT(20) NOT NULL,
 `actual_cost` FLOAT default 0,
 `revised_cost` FLOAT default 0,
 `created_on` INT(20) NOT NULL,
 `created_by` INT(11) NOT NULL,
 `status` ENUM('A','I') NOT NULL DEFAULT 'A',
 PRIMARY KEY (`id`),
 INDEX `insurance_revised_costfk_1` (`created_by`),
 INDEX `insurance_revised_costfk_2` (`insurance_company_id`),
 INDEX `insurance_revised_costfk_3` (`scheme_plan_id`),
 INDEX `insurance_revised_costfk_4` (`scheme_name_id`),

 CONSTRAINT `insurance_revised_costfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `insurance_revised_costfk_2` FOREIGN KEY (`insurance_company_id`) REFERENCES `insurance_companies` (`id`),
 CONSTRAINT `insurance_revised_costfk_3` FOREIGN KEY (`scheme_plan_id`) REFERENCES `insurance_plan` (`id`),
 CONSTRAINT `insurance_revised_costfk_4` FOREIGN KEY (`scheme_name_id`) REFERENCES `scheme_plans` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

 ALTER TABLE `scheme_exclusion`
 CHANGE COLUMN `item_name` `item_name` VARCHAR(100) NULL AFTER `scheme_name_id`,
 CHANGE COLUMN `item_id` `item_id` INT(20) NULL AFTER `item_name`,
 CHANGE COLUMN `service_type` `service_type` VARCHAR(100) NULL AFTER `item_id`;

 
ALTER TABLE `insurance_revised_cost`
 ADD COLUMN `service_type` VARCHAR(100) NULL DEFAULT NULL AFTER `item_name`;

 ALTER TABLE `visits`
 ADD COLUMN `insurance_no` VARCHAR(50) NULL DEFAULT NULL AFTER `department`,
 ADD COLUMN `payment_mode` ENUM('Cash','Insurance') NULL DEFAULT NULL AFTER `insurance_no`;

 ALTER TABLE `insurance`
 ADD COLUMN `insurance_number` INT(11) NOT NULL AFTER `patient_id`;

 ALTER TABLE `insurance`
 ADD UNIQUE INDEX `insurance_number` (`insurance_number`);

 SET foreign_key_checks = 0;
ALTER TABLE `insurance`
 ADD COLUMN `insurance_plan_name_id` INT(11) NOT NULL AFTER `insurance_plan_id`,
 ADD CONSTRAINT `FK_insurance_scheme_plans` FOREIGN KEY (`insurance_plan_name_id`) REFERENCES `scheme_plans` (`id`);
 SET foreign_key_checks = 1;

 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP TABLE `scheme_plans`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;

 CREATE TABLE `scheme_plans` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `scheme_plan_name` VARCHAR(50) NOT NULL,
 `insurance_company_id` INT(11) NOT NULL,
 `insurance_plan_id` INT(11) NOT NULL,
 `description` TEXT NOT NULL,
 `start_date` DATE NOT NULL,
 `end_date` DATE NOT NULL,
 `copay` INT(11) NOT NULL,
 `value` FLOAT NOT NULL,
 `created_on` INT(20) NOT NULL,
 `updated_on` INT(20) NOT NULL,
 `created_by` INT(11) NOT NULL,
 `updated_by` INT(11) NOT NULL,
 `status` ENUM('A','I') NOT NULL DEFAULT 'A',
 PRIMARY KEY (`id`),
 INDEX `FK__insurance_companies_scheme_plans` (`insurance_company_id`),
 INDEX `FK__insurance_plan_scheme_plans` (`insurance_plan_id`),
 INDEX `FK__users_scheme_plans` (`created_by`),
 INDEX `FK__users_scheme_plans_2` (`updated_by`),
 CONSTRAINT `FK__insurance_companies_scheme_plans` FOREIGN KEY (`insurance_company_id`) REFERENCES `insurance_companies` (`id`),
 CONSTRAINT `FK__insurance_plan_scheme_plans` FOREIGN KEY (`insurance_plan_id`) REFERENCES `insurance_plan` (`id`),
 CONSTRAINT `FK__users_scheme_plans` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__users_scheme_plans_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;


SET foreign_key_checks = 0;
ALTER TABLE `scheme_plans` ADD CONSTRAINT `FK_scheme_plans_copay` FOREIGN KEY (`copay`) REFERENCES `copay` (`id`);
SET foreign_key_checks = 1;


 ALTER TABLE `patient_forensic_bill`
 ADD COLUMN `payment_mode` ENUM('cash','insurance') NOT NULL DEFAULT 'cash' AFTER `status`;

 ALTER TABLE `patient_lab_bill`
 ADD COLUMN `payment_mode` ENUM('cash','insurance') NOT NULL DEFAULT 'cash' AFTER `status`;
 
 ALTER TABLE `patient_pharmacy_bill`
 ADD COLUMN `payment_mode` ENUM('cash','insurance') NOT NULL DEFAULT 'cash' AFTER `status`;
 
 ALTER TABLE `patient_procedure_bill`
 ADD COLUMN `payment_mode` ENUM('cash','insurance') NOT NULL DEFAULT 'cash' AFTER `status`;
 
 ALTER TABLE `patient_radiology_bill`
 ADD COLUMN `payment_mode` ENUM('cash','insurance') NOT NULL DEFAULT 'cash' AFTER `status`;
 
 ALTER TABLE `patient_service_bill`
 ADD COLUMN `payment_mode` ENUM('cash','insurance') NOT NULL DEFAULT 'cash' AFTER `status`;

 ALTER TABLE `visits`
 CHANGE COLUMN `payment_mode` `payment_mode` ENUM('cash','insurance') NULL DEFAULT NULL AFTER `insurance_no`;

 ALTER TABLE `holidays`
 CHANGE COLUMN `date` `date` DATE NOT NULL AFTER `name`;

 SET GLOBAL event_scheduler = ON; 
SELECT @@event_scheduler; 
CREATE EVENT update_leave_event  
    ON SCHEDULE
      EVERY 1 DAY 
    DO
      UPDATE staff_leaves set leave_status ='taken' WHERE DATE_FORMAT(FROM_UNIXTIME(leave_date), '%Y-%m-%d') = CURDATE() AND ( leave_status = 'approval' OR leave_status = 'schedule')

	  ALTER EVENT update_leave_event
    ON SCHEDULE
    EVERY 1 DAY
   STARTS (TIMESTAMP(CURRENT_DATE) + INTERVAL 1 DAY)

   ALTER TABLE `lab_test_request_component`
 ADD COLUMN `cost` INT(50) NOT NULL AFTER `lab_component_id`;

 ALTER TABLE `lab_test_request_component`
 ADD COLUMN `payment_mode` ENUM('cash','insurance') NOT NULL DEFAULT 'cash';

 
 ALTER TABLE `insurance`
 ALTER `insurance_number` DROP DEFAULT;
ALTER TABLE `insurance`
 CHANGE COLUMN `insurance_number` `insurance_number` VARCHAR(250) NOT NULL AFTER `patient_id`;


 SET foreign_key_checks = 0;
DROP TABLE `insurance_type`;

ALTER TABLE `insurance`
 DROP FOREIGN KEY `FK__insurance_type`;
ALTER TABLE `insurance`
 DROP COLUMN `insurance_type_id`;
 SET foreign_key_checks = 1;

 ALTER TABLE `order-item`
 DROP COLUMN `cost`;

 ALTER TABLE `holidays`
 ALTER `updated_on` DROP DEFAULT;
ALTER TABLE `holidays`
 CHANGE COLUMN `updated_on` `updated_on` INT(11) NOT NULL AFTER `description`,
 ADD COLUMN `repeat_annually` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `created_on`;



ALTER TABLE `visits`
 CHANGE COLUMN `payment_mode` `payment_mode` ENUM('cash','insurance') NULL DEFAULT NULL AFTER `insurance_no`;
INSERT INTO `herp`.`pages` (`screen_name`, `screen_load_url`, `parent_menu_id`) VALUES ('Inventory Payment', 'inventory_payment.html', 74);
CREATE TABLE `insurance_payment` (
 `id` INT(11) NOT NULL,
 `insurance_no` VARCHAR(250) NOT NULL,
 `insurance_company` INT(11) NOT NULL,
 `insurance_plan` INT(11) NOT NULL,
 `insurance_scheme_plan` INT(11) NOT NULL,
 `total_bill` FLOAT NOT NULL,
 `created_by` INT(20) NOT NULL,
 `date` DATE NOT NULL,
 `status` ENUM('A','I') NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__insurance_companies_insurance_payment` (`insurance_company`),
 INDEX `FK__insurance_plan_insurance_payment` (`insurance_plan`),
 INDEX `FK__scheme_plans_insurance_payment` (`insurance_scheme_plan`),
 CONSTRAINT `FK__insurance_companies_insurance_payment` FOREIGN KEY (`insurance_company`) REFERENCES `insurance_companies` (`id`),
 CONSTRAINT `FK__insurance_plan_insurance_payment` FOREIGN KEY (`insurance_plan`) REFERENCES `insurance_plan` (`id`),
 CONSTRAINT `FK__scheme_plans_insurance_payment` FOREIGN KEY (`insurance_scheme_plan`) REFERENCES `scheme_plans` (`id`)
)
ENGINE=InnoDB
;

 CREATE TABLE `staff_working_days_schedule` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `staff_type_id` INT(11) NOT NULL,
 `staff_id` INT(11) NOT NULL,
 `day` ENUM('0','1','2','3','4','5','6') NOT NULL,
 `is_available` ENUM('available','notAvailable') NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__staff_working_days_schedule_staff_type_id` (`staff_type_id`),
 INDEX `FK__staff_working_days_schedule_users_id` (`staff_id`),
 CONSTRAINT `FK__staff_working_days_schedule_staff_type_id` FOREIGN KEY (`staff_type_id`) REFERENCES `staff_type` (`id`),
 CONSTRAINT `FK__staff_working_days_schedule_users_id` FOREIGN KEY (`staff_id`) REFERENCES `users` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1;

CREATE TABLE `staff_days_schedule_slots` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `schedule_id` INT(11) NOT NULL,
 `from_time` VARCHAR(20) NOT NULL,
 `to_time` VARCHAR(20) NOT NULL,
 `notes` TEXT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__staff_days_schedule_slots` (`schedule_id`),
 CONSTRAINT `FK__staff_days_schedule_slots` FOREIGN KEY (`schedule_id`) REFERENCES `staff_working_days_schedule` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

 CREATE TABLE `inventory_payment` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `seller_id` INT(11) NOT NULL,
 `order_id` INT(11) NOT NULL,
 `total_amount` INT(20) NOT NULL,
 `date` DATE NOT NULL,
 `created_on` INT(20) NOT NULL,
 `created_by` INT(20) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__seller_inventory_payment` (`seller_id`),
 INDEX `FK__orders_inventory_payment` (`order_id`),
 INDEX `FK__users_inventory_payment` (`created_by`),
 CONSTRAINT `FK__orders_inventory_payment` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
 CONSTRAINT `FK__seller_inventory_payment` FOREIGN KEY (`seller_id`) REFERENCES `seller` (`id`),
 CONSTRAINT `FK__users_inventory_payment` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
)
ENGINE=InnoDB
;

ALTER TABLE `insurance`
 ALTER `insurance_number` DROP DEFAULT;
ALTER TABLE `insurance`
 CHANGE COLUMN `insurance_number` `insurance_number` VARCHAR(250) NOT NULL AFTER `patient_id`;

 ALTER TABLE `insurance_payment`
 ADD COLUMN `patient_id` INT(11) NOT NULL AFTER `insurance_no`,
 ADD COLUMN `visit_id` INT(11) NOT NULL AFTER `patient_id`,
 CHANGE COLUMN `status` `status` ENUM('A','I') NOT NULL DEFAULT 'A' AFTER `date`,
 ADD CONSTRAINT `FK_insurance_payment_users_insurance_payment` FOREIGN KEY (`patient_id`) REFERENCES `users` (`id`),
 ADD CONSTRAINT `FK_insurance_payment_visits_insurance_payment` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`id`);

 ALTER TABLE `insurance_payment`
 CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT FIRST;

 CREATE TABLE `notification` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `notification_staff_id` INT(11) NOT NULL,
 `message` TEXT NOT NULL,
 `created_on` INT(20) NOT NULL,
 `created_by` INT(20) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__users_notification` (`notification_staff_id`),
 CONSTRAINT `FK__users_notification` FOREIGN KEY (`notification_staff_id`) REFERENCES `users` (`id`)
)
ENGINE=InnoDB
;

ALTER TABLE `insurance_payment`
 DROP FOREIGN KEY `FK_insurance_payment_users_insurance_payment`;
ALTER TABLE `insurance_payment`
 ADD CONSTRAINT `FK_insurance_payment_users_insurance_payment` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`);
ALTER TABLE `inventory_payment`
 ADD COLUMN `payment_mode` ENUM('cash','cheque') NOT NULL AFTER `created_by`,
 ADD COLUMN `status` ENUM('A','I') NOT NULL DEFAULT 'A' AFTER `payment_mode`;


 CREATE TABLE `ot_rooms` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `room_no` VARCHAR(50) NOT NULL,
 `description` TEXT NULL,
 `created_on` INT(11) NOT NULL,
 `updated_on` INT(11) NOT NULL,
 `status` ENUM('A','I') NOT NULL DEFAULT 'A',
 PRIMARY KEY (`id`)
)
ENGINE=InnoDB
;

 CREATE TABLE `surgery` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `name` VARCHAR(50) NOT NULL,
 `description` TEXT NULL,
 `created_on` INT(11) NOT NULL,
 `updated_on` INT(11) NOT NULL,
 `status` ENUM('A','I') NOT NULL DEFAULT 'A',
 PRIMARY KEY (`id`)
)
ENGINE=InnoDB
;

CREATE TABLE `ot_room_availability` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `ot_room_id` INT(11) NOT NULL,
 `patient_operation_id` INT(11) NOT NULL,
 `from_time` VARCHAR(50) NOT NULL,
 `to_time` VARCHAR(50) NOT NULL,
 `created_on` INT(20) NOT NULL,
 `created_by` INT(20) NOT NULL,
 `updated_on` INT(20) NOT NULL,
 `updated_by` INT(20) NOT NULL,
 `status` ENUM('A','I') NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__ot_rooms_ot_room_availability` (`ot_room_id`),
 INDEX `FK__patients_ot_room_availability` (`patient_operation_id`),
 INDEX `FK__users_ot_room_availability` (`created_by`),
 INDEX `FK__users_2_ot_room_availability` (`updated_by`),
 CONSTRAINT `FK__ot_rooms_ot_room_availability` FOREIGN KEY (`ot_room_id`) REFERENCES `ot_rooms` (`id`),
 CONSTRAINT `FK__patients_ot_room_availability` FOREIGN KEY (`patient_operation_id`) REFERENCES `patients` (`id`),
 CONSTRAINT `FK__users_2_ot_room_availability` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__users_ot_room_availability` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
)
ENGINE=InnoDB
;

ALTER TABLE `ot_room_availability`
 CHANGE COLUMN `status` `status` ENUM('A','I') NOT NULL DEFAULT 'A' AFTER `updated_by`;

 
 CREATE TABLE `patient_ot_registration` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `patient_id` INT(11) NOT NULL,
 `operation_date` DATE NOT NULL,
 `surgeon_id` INT(11) NOT NULL,
 `surgery_id` INT(11) NOT NULL,
 `anethaesthiest_id` INT(11) NOT NULL,
 `ot_type` ENUM('major','minor') NOT NULL,
 `duration` VARCHAR(50) NOT NULL,
 `created_on` INT(20) NOT NULL,
 `updated_on` INT(20) NOT NULL,
 `created_by` INT(11) NOT NULL,
 `updated_by` INT(11) NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `FK__patients_patient_ot_registration` (`patient_id`),
 INDEX `FK__users_patient_ot_registration` (`surgeon_id`),
 INDEX `FK__surgery_patient_ot_registration` (`surgery_id`),
 INDEX `FK__users_2_patient_ot_registration` (`anethaesthiest_id`),
 INDEX `FK__users_3_patient_ot_registration` (`created_by`),
 INDEX `FK__users_4_patient_ot_registration` (`updated_by`),
 CONSTRAINT `FK__patients_patient_ot_registration` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
 CONSTRAINT `FK__surgery_patient_ot_registration` FOREIGN KEY (`surgery_id`) REFERENCES `surgery` (`id`),
 CONSTRAINT `FK__users_2_patient_ot_registration` FOREIGN KEY (`anethaesthiest_id`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__users_3_patient_ot_registration` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__users_4_patient_ot_registration` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
 CONSTRAINT `FK__users_patient_ot_registration` FOREIGN KEY (`surgeon_id`) REFERENCES `users` (`id`)
)
ENGINE=InnoDB
;

ALTER TABLE `patient_ot_registration`
 ALTER `ot_type` DROP DEFAULT;

 ALTER TABLE `ot_room_availability`
 DROP FOREIGN KEY `FK__patients_ot_room_availability`;
ALTER TABLE `ot_room_availability`
 ALTER `patient_operation_id` DROP DEFAULT;
ALTER TABLE `ot_room_availability`
 CHANGE COLUMN `patient_operation_id` `patient_ot_reg_id` INT(11) NOT NULL AFTER `ot_room_id`,
 ADD CONSTRAINT `FK__patients_ot_room_availability` FOREIGN KEY (`patient_ot_reg_id`) REFERENCES `patient_ot_registration` (`id`);

 ALTER TABLE `copay`
 ADD COLUMN `value` ENUM('Percentage','Amount Prefix') NULL AFTER `copay_type`;


 ALTER TABLE `patient_ot_registration`
 ADD COLUMN `surgery_type` INT(11) NOT NULL AFTER `surgery_id`;
