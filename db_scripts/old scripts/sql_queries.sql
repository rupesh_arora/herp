ALTER TABLE `users`
	ALTER `id_proof_type` DROP DEFAULT;
ALTER TABLE `users`
	CHANGE COLUMN `id_proof_type` `id_proof_type` VARCHAR(50) NULL;
ALTER TABLE `users`
	ALTER `gender` DROP DEFAULT,
	ALTER `id_proof_num` DROP DEFAULT,
	ALTER `salt` DROP DEFAULT;
ALTER TABLE `users`
	CHANGE COLUMN `gender` `gender` ENUM('M','F','NM') NOT NULL ,
	CHANGE COLUMN `id_proof_num` `id_proof_num` VARCHAR(250) NULL ,
	CHANGE COLUMN `designation_held` `degree_held` TEXT NULL ,
	CHANGE COLUMN `remarks` `remarks` TEXT NULL ,
	CHANGE COLUMN `salt` `salt` VARCHAR(255) NULL ,
	ADD COLUMN `salutation` VARCHAR(10) NULL ;
SELECT `DEFAULT_COLLATION_NAME` FROM `information_schema`.`SCHEMATA` WHERE `SCHEMA_NAME`='herp';	

  --4/11/2015 4:58
ALTER TABLE `designation`
	CHANGE COLUMN `Id` `id` INT(11) NOT NULL AUTO_INCREMENT FIRST;
	ALTER TABLE `users`
	ADD CONSTRAINT `FK_designation_id` FOREIGN KEY (`designation`) REFERENCES `designation` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;

--Create table wards and beds (date-04-11-2015/ time-05:20)
create table beds
(
    id int not null auto_increment, 
    ward_id int(11) not null, 
    number varchar(30) not null, 
    description text null,
    foreign key (ward_id) references wards(id),
    primary key (id)
);

create table wards
(
    id int not null auto_increment, 
    name varchar(50) not null, 
    description text null,
    primary key (id)
);

-- change user table firstle delete table and execute that query
CREATE TABLE /*!32312 IF NOT EXISTS*/ `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `gender` enum('M','F','NM') NOT NULL,
  `martial_status` enum('M','U','D','NM') DEFAULT NULL,
  `dob` date NOT NULL,
  `address` text NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `zip` varchar(15) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email_id` varchar(50) NOT NULL,
  `joining_date` date NOT NULL,
  `emergency_name` varchar(50) DEFAULT NULL,
  `emergency_contact` varchar(15) DEFAULT NULL,
  `referred_by` varchar(50) DEFAULT NULL,
  `staff_type_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id_proof_type` varchar(50) DEFAULT NULL,
  `id_proof_num` varchar(250) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `degree_held` text,
  `designation` int(11) NOT NULL,
  `remarks` text,
  `password` varchar(285) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `salutation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_id` (`email_id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `FK_city_id` (`city_id`),
  KEY `FK_country_id` (`country_id`),
  KEY `FK_state_id` (`state_id`),
  KEY `FK_designation` (`department_id`),
  KEY `FK_staff_type` (`staff_type_id`),
  KEY `FK_designation_id` (`designation`),
  CONSTRAINT `FK_designation_id` FOREIGN KEY (`designation`) REFERENCES `designation` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_city_id` FOREIGN KEY (`city_id`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_country_id` FOREIGN KEY (`country_id`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_department_id` FOREIGN KEY (`department_id`) REFERENCES `department` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  
  CONSTRAINT `FK_staff_type` FOREIGN KEY (`staff_type_id`) REFERENCES `staff_typetable` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_state_id` FOREIGN KEY (`state_id`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;
/* Add Session Table Inside HERP(Must For Login Functionality) */
CREATE TABLE IF NOT EXISTS `session` (
  `id` int(20) unsigned NOT NULL,
  `session_id` varchar(50) NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `mac_address` varchar(32) NOT NULL,
  `ip_address` varchar(32) NOT NULL,
  `login_timestamp` int(20) unsigned NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;