-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2015 at 02:45 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `herp`
--
CREATE DATABASE IF NOT EXISTS `herp` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `herp`;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `gender` enum('M','F','NM') NOT NULL,
  `martial_status` enum('M','U','D','NM') DEFAULT NULL,
  `dob` date NOT NULL,
  `address` text NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `zip` varchar(15) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email_id` varchar(50) NOT NULL,
  `joining_date` date NOT NULL,
  `emergency_name` varchar(50) DEFAULT NULL,
  `emergency_contact` varchar(15) DEFAULT NULL,
  `referred_by` varchar(50) DEFAULT NULL,
  `staff_type_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id_proof_type` varchar(50) DEFAULT NULL,
  `id_proof_num` varchar(250) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `degree_held` text,
  `designation` int(11) NOT NULL,
  `remarks` text,
  `password` varchar(285) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `salutation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_id` (`email_id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `FK_city_id` (`city_id`),
  KEY `FK_country_id` (`country_id`),
  KEY `FK_state_id` (`state_id`),
  KEY `FK_designation` (`department_id`),
  KEY `FK_staff_type` (`staff_type_id`),
  KEY `FK_designation_id` (`designation`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=70 ;

--
-- RELATIONS FOR TABLE `users`:
--   `designation`
--       `designation` -> `Id`
--   `city_id`
--       `location` -> `id`
--   `country_id`
--       `location` -> `id`
--   `department_id`
--       `department` -> `Id`
--   `staff_type_id`
--       `staff_typetable` -> `id`
--   `state_id`
--       `location` -> `id`
--

-- --------------------------------------------------------
--
-- Table structure for table `beds`
--

CREATE TABLE IF NOT EXISTS `beds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ward_id` int(11) NOT NULL,
  `number` varchar(30) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `ward_id` (`ward_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- RELATIONS FOR TABLE `beds`:
--   `ward_id`
--       `wards` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Description` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE IF NOT EXISTS `designation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL DEFAULT '0',
  `Description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `staff_typetable`
--

CREATE TABLE IF NOT EXISTS `staff_typetable` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------



--
-- Table structure for table `wards`
--

CREATE TABLE IF NOT EXISTS `wards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `beds`
--
ALTER TABLE `beds`
  ADD CONSTRAINT `beds_ibfk_1` FOREIGN KEY (`ward_id`) REFERENCES `wards` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_designation_id` FOREIGN KEY (`designation`) REFERENCES `designation` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_city_id` FOREIGN KEY (`city_id`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_country_id` FOREIGN KEY (`country_id`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_department_id` FOREIGN KEY (`department_id`) REFERENCES `department` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_staff_type` FOREIGN KEY (`staff_type_id`) REFERENCES `staff_typetable` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_state_id` FOREIGN KEY (`state_id`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- 04-11-2015 07:48
-- Table structure for table `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ward_id` int(11) NOT NULL,
  `number` varchar(30) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `ward_id` (`ward_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- RELATIONS FOR TABLE `rooms`:
--   `ward_id`
--       `wards` -> `id`
--

-- --------------------------------------------------------

-- 05-11-2015 10:44
-- First Drop table `beds`
--

DROP TABLE beds;

-- 05-11-2015 10:44
-- Again Create  table `beds`
--
CREATE TABLE `beds` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`ward_id` INT(11) NOT NULL,
	`room_id` INT(11) NOT NULL,
	`cost` DOUBLE NOT NULL,
	`price` DOUBLE NOT NULL,
	`number` VARCHAR(30) NOT NULL,
	`description` TEXT NULL,
	PRIMARY KEY (`id`),
	INDEX `ward_id` (`ward_id`),
	INDEX `room_id` (`room_id`),
	CONSTRAINT `FK_beds_rooms` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`),
	CONSTRAINT `beds_ibfk_1` FOREIGN KEY (`ward_id`) REFERENCES `wards` (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=6
;
-- --------------------------------------------------------

ALTER TABLE designation CHANGE Name name varchar(50);
ALTER TABLE designation CHANGE Description description text;
ALTER TABLE department CHANGE Description description text;
ALTER TABLE department CHANGE Name name varchar(50);

-- change in staff table date 5/11/2015
ALTER TABLE staff_typetable add name varchar(50);
ALTER TABLE staff_typetable add description text;
ALTER TABLE users DROP FOREIGN KEY FK_staff_type;
ALTER TABLE staff_typetable DROP PRIMARY KEY;
ALTER TABLE `staff_typetable` modify `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY;
 DROP TABLE users;
CREATE TABLE /*!32312 IF NOT EXISTS*/ `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `gender` enum('M','F','NM') NOT NULL,
  `martial_status` enum('M','U','D','NM') DEFAULT NULL,
  `dob` date NOT NULL,
  `address` text NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `zip` varchar(15) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email_id` varchar(50) NOT NULL,
  `joining_date` date NOT NULL,
  `emergency_name` varchar(50) DEFAULT NULL,
  `emergency_contact` varchar(15) DEFAULT NULL,
  `referred_by` varchar(50) DEFAULT NULL,
  `staff_type_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id_proof_type` varchar(50) DEFAULT NULL,
  `id_proof_num` varchar(250) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `degree_held` text,
  `designation` int(11) NOT NULL,
  `remarks` text,
  `password` varchar(285) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `salutation` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_id` (`email_id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `FK_city_id` (`city_id`),
  KEY `FK_country_id` (`country_id`),
  KEY `FK_state_id` (`state_id`),
  KEY `FK_designation` (`department_id`),
  KEY `FK_staff_type` (`staff_type_id`),
  KEY `FK_designation_id` (`designation`),
  CONSTRAINT `FK_designation_id` FOREIGN KEY (`designation`) REFERENCES `designation` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_city_id` FOREIGN KEY (`city_id`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_country_id` FOREIGN KEY (`country_id`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_department_id` FOREIGN KEY (`department_id`) REFERENCES `department` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  
  CONSTRAINT `FK_staff_type` FOREIGN KEY (`staff_type_id`) REFERENCES `staff_typetable` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_state_id` FOREIGN KEY (`state_id`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;