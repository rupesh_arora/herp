<?php
    session_start();
?>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<link rel="stylesheet" href="./css/bootstrap.min.css" />
<link href="./css/jquery.dataTables.css" rel="stylesheet">
<link href="./css/dataTables.jqueryui.css" rel="stylesheet">
<link href="scheduler/css/dailog.css" rel="stylesheet" type="text/css" />
<link href="scheduler/css/calendar.css" rel="stylesheet" type="text/css" />
<link href="scheduler/css/dp.css" rel="stylesheet" type="text/css" />
<link href="scheduler/css/alert.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="./css/plugins.css" />
<link rel="stylesheet" href="./css/main.css" />
<link rel="stylesheet" href="./css/themes.css" />
<link href="scheduler/css/style.css" rel="stylesheet" type="text/css" />
<script src="./script/vendor/jquery-1.11.3.min.js"></script>

<script src="./script/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
<script src="./script/modernizer-loader.js"></script>
<script src="./script/vendor/bootstrap.min.js"></script>
<script src="./script/jquerysession.js"></script>

<script src="scheduler/src/Plugins/Common.js" type="text/javascript"></script>
<script src="scheduler/src/Plugins/datepicker_lang_US.js" type="text/javascript"></script>
<script src="scheduler/src/Plugins/jquery.datepicker.js" type="text/javascript"></script>


<script src="scheduler/src/Plugins/jquery.alert.js" type="text/javascript"></script>
<script src="scheduler/src/Plugins/jquery.ifrmdailog.js" defer="defer" type="text/javascript"></script>
<script src="scheduler/src/Plugins/wdCalendar_lang_US.js" type="text/javascript"></script>
<script src="scheduler/src/Plugins/jquery.calendar.js" type="text/javascript"></script>
<script src="./script/plugins.js"></script>
<script src="./script/app.js"></script>
<script src="./script/pages/index.js"></script>
<script src="./script/main.js"></script>

<script type="text/javascript">

$(document).ready(function() {
    bindVisitType();
    $("#mainHtml").hide();
    var timeSlots = "";
    var date = new Date();
    $("#txtdatetimeshow").text( date.toString());
    debugger;  
     //Calling function for binding the staff type
     bindStaffType();
     if($("#hdnFunction").length != 0){
         fillCalender("-2");
         $("#mainHtml").fadeIn('slow');
     }
    
    //Change event of staff type
    $('#selStaffType').change(function() {
        var htmlRoom = "<option value='-1'>-- Select --</option>";
        $("#selStaffTypeError").text("");
        $("#selStaffType").removeClass("errorStyle");

        if ($('#selStaffType :selected').val() != -1) {
            var value = $('#selStaffType :selected').val();
            $('#mainHtml').fadeOut('slow');
            //Calling function for binding the staff name
            bindStaffName(value, null);
        } else {
            $('#selStaffName').html(htmlRoom);
            
        }
    });

    //Change event of staff name
    $('#selStaffName').change(function() {
        $("#selStaffNameError").text("");
        $("#selStaffName").removeClass("errorStyle");
        var staffId = $('#selStaffName :selected').val();
        //$('#gridcontainer').html('');
        /*$("#mainHtml").html(mainHtml);*/
        if($("#BBIT_DP_CONTAINER").length > 0){
            $("#BBIT_DP_CONTAINER").remove();
            $("#hdtxtshow").unbind();
            $("#hdtxtshow").datepicker1({
                picker: "#txtdatetimeshow",
                showtarget: $("#txtdatetimeshow"),
                onReturn: function(r) {
                    var p = $("#gridcontainer").gotoDate(r).BcalGetOp();
                    if (p && p.datestrshow) {
                        $("#txtdatetimeshow").text(p.datestrshow);
                    }
                }
            });
        }
        $('#mainHtml').fadeIn('slow');
        fillCalender(staffId);


    });

    function fillCalender(staffId) {

        var view = "week";
        var DATA_FEED_URL = "scheduler/php/datafeed.php";
        var op = {
            view: view,
            staffId: staffId,
            theme: 3,
            showday: new Date(),
            EditCmdhandler: Edit,
            DeleteCmdhandler: Delete,
            ViewCmdhandler: View,
            onWeekOrMonthToDay: wtd,
            onBeforeRequestData: cal_beforerequest,
            onAfterRequestData: cal_afterrequest,
            onRequestDataError: cal_onerror,
            autoload: true,
            url: DATA_FEED_URL + "?method=list",
            quickAddUrl: DATA_FEED_URL + "?method=add",
            quickUpdateUrl: DATA_FEED_URL + "?method=update",
            quickDeleteUrl: DATA_FEED_URL + "?method=remove"
        };
        var $dv = $("#calhead");
        var _MH = document.documentElement.clientHeight;
        var dvH = $dv.height() + 2;
        op.height = _MH - dvH;
        op.eventItems = [];

        var p = $("#gridcontainer").bcalendar(op).BcalGetOp();
        if (p && p.datestrshow) {
            $("#txtdatetimeshow").text(p.datestrshow);
        }
        $("#caltoolbar").noSelect();

        $("#hdtxtshow").datepicker1({
            picker: "#txtdatetimeshow",
            showtarget: $("#txtdatetimeshow"),
            onReturn: function(r) {
                var p = $("#gridcontainer").gotoDate(r).BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
            }
        });

        function cal_beforerequest(type) {
            var t = "Loading data...";
            switch (type) {
                case 1:
                    t = "Loading data...";
                    break;
                case 2:
                case 3:
                case 4:
                    t = "The request is being processed ...";
                    break;
            }
            $("#errorpannel").hide();
            $("#loadingpannel").html(t).show();
        }

        function cal_afterrequest(type) {
            switch (type) {
                case 1:
                    $("#loadingpannel").hide();
                    break;
                case 2:
                case 3:
                case 4:
                    $("#loadingpannel").html("Success!");
                    window.setTimeout(function() {
                        $("#loadingpannel").hide();
                    }, 2000);
                    break;
            }

        }

        function cal_onerror(type, data) {
            $("#errorpannel").show();
        }
    }

    function Edit(data) {
		debugger;
		/* bindStaffType();*/
	
		//Change event of staff type
		$('#selStaffTypeModal').change(function() {
			var htmlRoom = "<option value='-1'>-- Select --</option>";
			$("#selStaffTypeModalError").text("");
			$("#selStaffTypeModal").removeClass("errorStyle");

			if ($('#selStaffTypeModal :selected').val() != -1) {
				var value = $('#selStaffTypeModal :selected').val();
				//Calling function for binding the staff name
				bindStaffName(value, 'check');
			} else {
				$('#selStaffNameModal').html(htmlRoom);				
			}
		});
		
        $("#divBookSlots").html('');
		$(".staffDropDown").show();
		
        var id = data[0];
        var postData = {
            "operation": "selectData",
            "id": id
        }

        $.ajax({
            type: "POST",
            cache: false,
            url: "./controllers/admin/appointment.php",
            datatype: "json",
            data: postData,
            success: function(result) {
                if (result != "0") {
                    var parseData = jQuery.parseJSON(result); // parse the value in Array string jquery                
                    var patientId = parseData[0].patient_id;
                    var patientPrefix = parseData[0].prefix;
                    var date = parseData[0].start_date_time;
                    var newDate = date.substring(0, 10);
                    $('#txtDate').val(newDate);
                    var fromTime = date.substring(11, 16);
                    var toTime = parseData[0].end_date_time;
                    toTime = toTime.substring(11, 16);
                    timeSlots = fromTime+"-"+toTime;
                    var div = "<div class='slots'><label style='cursor: pointer; font-size: 18px;' class='lblSlots'>"+timeSlots+"</label></div>";                    
                           
                    $("#divBookSlots").append(div);
                    $('#txtNote').val(parseData[0].notes);
                    $('#selConsultant').val(parseData[0].visit_type_id);
					$("#hdnId").val(parseData[0].id);
                    bindStaffName(parseData[0].staff_type_id, parseData[0].consultant_id);
                    $("#selStaffTypeModal").val(parseData[0].staff_type_id);
                    idLength = 6 - patientId.length;
                    for (i = 0; i < idLength; i++) {
                        patientId = "0"+patientId;
                    }
                    patientId = patientPrefix + patientId;
                    $("#txtPatientId").val(patientId);
                    //$("#hdnVisitId").val(parseData[0].visit_id);
                }

            },
            error: function() {

            }
        });
        $('#myAppointmentModalLabel').text("Update Appointment");
        $('#AppointmentModalBody').text();
        $('#myAppointmentModal').modal();
        $("#btnBookAppointment").val("Update");
        $("#txtPatientId").attr("disabled","disabled");
        $("#selConsultant").attr("disabled","disabled");
        $("#btnRegister").attr("disabled","disabled");
        $("#iconPatientSearch").hide();
    }

    function View(data) {
        var str = "";
        $.each(data, function(i, item) {
            str += "[" + i + "]: " + item + "\n";
        });
        alert(str);
    }

    function Delete(data, callback) {
        var id = data[0];
        var postData = {
            "operation": "delete",
            "id": id
        }

        $('#confirmUpdateModalLabel').text("Confirmation");
        $('#updateBody').text("Are you sure that you want to delete this?");
        $('#confirmUpdateModal').modal();
        $("#btnConfirm").unbind();
        $("#btnConfirm").click(function(){

            $.ajax({
                type: "POST",
                cache: false,
                url: "./controllers/admin/appointment.php",
                datatype: "json",
                data: postData,
                success: function(data) {
                    if (data != "0") {
                        $('#messageBody').html("");
                        $('#messageMyModalLabel').text("Success");
                        $('#messageBody').html("Appointment deleted successfully!!!");
                        $('#messagemyModal').modal();
                        $(".showdayflash").unbind();
                        $(".showdayflash").click();
                    }

                },
                error: function() {

                }
            });
        });
    }

    function wtd(p) {
        if (p && p.datestrshow) {
            $("#txtdatetimeshow").text(p.datestrshow);
        }
        $("#caltoolbar div.fcurrent").each(function() {
            $(this).removeClass("fcurrent");
        })
        $("#showdaybtn").addClass("fcurrent");
    }
    //to show day view
    $("#showdaybtn").click(function(e) {
        //document.location.href="#day";
        $("#caltoolbar div.fcurrent").each(function() {
            $(this).removeClass("fcurrent");
        })
        $(this).addClass("fcurrent");
        var p = $("#gridcontainer").swtichView("day").BcalGetOp();
        if (p && p.datestrshow) {
            $("#txtdatetimeshow").text(p.datestrshow);
        }
    });
    //to show week view
    $("#showweekbtn").click(function(e) {
        //document.location.href="#week";
        $("#caltoolbar div.fcurrent").each(function() {
            $(this).removeClass("fcurrent");
        })
        $(this).addClass("fcurrent");
        var p = $("#gridcontainer").swtichView("week").BcalGetOp();
        if (p && p.datestrshow) {
            $("#txtdatetimeshow").text(p.datestrshow);
        }

    });
    //to show month view
    $("#showmonthbtn").click(function(e) {
        //document.location.href="#month";
        $("#caltoolbar div.fcurrent").each(function() {
            $(this).removeClass("fcurrent");
        })
        $(this).addClass("fcurrent");
        var p = $("#gridcontainer").swtichView("month").BcalGetOp();
        if (p && p.datestrshow) {
            $("#txtdatetimeshow").text(p.datestrshow);
        }
    });

    $("#showreflashbtn").click(function(e) {
        $("#gridcontainer").reload();
    });

    //Add a new event
    $("#faddbtn").click(function(e) {
		$(".staffDropDown").hide();
		timeSlots = "";
		$('#selStaffTypeModal').val(-1);
		$('#selStaffNameModal').val(-1);
        clear();		
        $('#myAppointmentModalLabel').text("Create Appointment");
        $('#AppointmentModalBody').text();
        $('#myAppointmentModal').modal();
        $("#btnBookAppointment").val("Book Appointment");
    });
    //go to today
    $("#showtodaybtn").click(function(e) {
        var p = $("#gridcontainer").gotoDate().BcalGetOp();
        if (p && p.datestrshow) {
            $("#txtdatetimeshow").text(p.datestrshow);
        }
    });
    //previous date range
    $("#sfprevbtn").click(function(e) {
        var p = $("#gridcontainer").previousRange().BcalGetOp();
        if (p && p.datestrshow) {
            $("#txtdatetimeshow").text(p.datestrshow);
        }

    });
    //next date range
    $("#sfnextbtn").click(function(e) {
        var p = $("#gridcontainer").nextRange().BcalGetOp();
        if (p && p.datestrshow) {
            $("#txtdatetimeshow").text(p.datestrshow);
        }
    });

    $("#iconPatientSearch").click(function() {
        $('#hdnAppointment').val("1");
        $('#myAppointmentPatientModalLabel').text("Search Patient");
        $('#AppointmentPatientModalBody').load('./views/admin/view_patients.html');
        $('#myAppointmentPatientModal').modal();
    })

    $("#btnRegister").click(function() {
        $('#hdnRegister').val("1");
        $('#myRegisterPatientModalLabel').text("Register Patient");
        $('#RegisterPatientModalBody').load('./views/admin/patient_registration.html');
        $('#myRegisterPatientModal').modal();
    })

    $("#myAppointmentModal #txtDate").click(function() {
        $(".datepicker").css({
            "z-index": "99999"
        });
    });
     
    /*$("#txtdatetimeshow").datepicker();*/

  
    var DATA_FEED_URL = "scheduler/php/datafeed.php";
    var arrT = [];
    var tt = "{0}:{1}";
    for (var i = 0; i < 24; i++) {
        arrT.push({
            text: StrFormat(tt, [i >= 10 ? i : "0" + i, "00"])
        }, {
            text: StrFormat(tt, [i >= 10 ? i : "0" + i, "30"])
        });
    }
    $.each(arrT, function(index, value) {
        $("#myAppointmentModal #txtToTime").append("<option value=" + value.text + ">" + value.text + "</option>");
        $("#myAppointmentModal #txtFromTime").append("<option value=" + value.text + ">" + value.text + "</option>");
    });

    $("#btnBookAppointment").click(function() {
        var flag = "false";

        if ($("#txtDate").val() == "") {
            $("#txtDateError").text("Please select date");
            flag = "true";
        }        

        if ($("#selConsultant").val() == "-1") {
            $("#selConsultant").focus();
            $("#selConsultantError").text("Please select visit type");
            flag = "true";
        }
        if ($("#txtPatientId").val() == "") {
            $("#txtPatientId").focus();
            $("#txtPatientIdError").text("Please enter patient id");
            flag = "true";
        }
        if ($("#txtPatientId").val().length != 9) {
            $("#txtPatientId").focus();
            $("#txtPatientIdError").text("Please enter valid patient id");
            flag = "true";
        }


        if ($("#selStaffName").val() == "-1") {
            $("#selStaffName").focus();
            $("#selStaffNameError").text("Please select staff name");
            flag = "true";
        }
		if ( $(".activeSelf").text() == "") {            
            flag = "true";
        }

        if (flag == "true") {
            return false;
        }

        var patientId = $("#txtPatientId").val();
		var patient_id = patientId;
        var patientPrefix = patientId.substring(0, 3);
        patientId = patientId.replace(/[^\d.]/g, '');
        patientId = parseInt(patientId);
        var visitType = $("#selConsultant").val();
        var notes = $("#txtNote").val();
        var date = $("#txtDate").val();
        var fromTime =  $(".activeSelf").text().split('-')[0];
        var toTime =  $(".activeSelf").text().split('-')[1];
        var btnName = $("#btnBookAppointment").val();
        if (btnName == "Book Appointment") {
			if($("#selStaffName").length != 0){
				var consultantId = $("#selStaffName").val();
			}
			else{
				var consultantId ="-2";
			}
		}
		else{			
			if($("#selStaffNameModal").val() != "-1"){
				var consultantId = $("#selStaffNameModal").val();
			}
		}


        var btnName = $("#btnBookAppointment").val();
        if (btnName == "Book Appointment") {
            var postData = {
                "operation": "save",
                "patientId": patientId,
                "patient_id": patient_id,
                "consultantId": consultantId,
                "patientPrefix": patientPrefix,
                "visitType": visitType,
                "notes": notes,
                "date": date,
                "fromTime": fromTime,
                "toTime": toTime,
                "hospitalName": hospitalName,                 
                "hospitalPhoneNo": hospitalPhoneNo
            }

            $.ajax({
                type: "POST",
                cache: false,
                url: "./controllers/admin/appointment.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "" && data != "booked"  && data != "available" && data != "Already booked") {
                        $(".modal_close").unbind();
                        $(".modal_close").click();
                        $('#messageMyModalLabel').text("Success");
                        $('#messageBody').html("Appointment booked successfully!!!" + "<br>" + "Appointment Id = " + data);
                        $('#messagemyModal').modal();
                        $(".showdayflash").unbind();
                        $(".showdayflash").click();
                        clear();
                    }
                    if (data == "booked") {
                        $("#viewSlotError").text("This slots is already book");
                    }
                    if(data == "available"){
                        $("#viewSlotError").text("Doctor is not available");
                    }
					if(data == ""){
                        $("#viewSlotError").text("Doctor is not available");
                    }
					if (data == "Already booked") {
                        $("#viewSlotError").text("This slots is already book");
                    }
                },
                error: function() {

                }
            });
        } 
        else {
            var id = $("#hdnId").val();
            var postData = {
                "operation": "update",
                "patientId": patientId,
                "patient_id": patient_id,
                "consultantId": consultantId,
                "patientPrefix": patientPrefix,
                "visitType": visitType,
                "notes": notes,
                "date": date,
                "fromTime": fromTime,
                "toTime": toTime,
                "id": id,
				"hospitalName": hospitalName,                 
                "hospitalPhoneNo": hospitalPhoneNo
            }
			debugger;
            $.ajax({
                type: "POST",
                cache: false,
                url: "./controllers/admin/appointment.php",
                datatype: "json",
                data: postData,

                success: function(data) {
                    if (data != "" && data != "0" && data != "booked" && data != "available" && data != "Already booked") {
                        $(".modal_close").unbind();
                        $(".modal_close").click();
                        $('#messageMyModalLabel').text("Success");
                        $('#messageBody').html("Appointment updated successfully!!!");
                        $('#messagemyModal').modal();
                        $(".showdayflash").unbind();
                        $(".showdayflash").click(); //Click on refresh button
                        clear();
                    }
                    if (data == "booked") {
                        $("#viewSlotError").text("This slots is already book");
                    }
                    if(data == "available"){
                        $("#viewSlotError").text("Doctor is not available");
                    }
					if(data == ""){
                        $("#viewSlotError").text("Doctor is not available");
                    }
					if (data == "Already booked") {
                        $("#viewSlotError").text("This slots is already book");
                    }

                },
                error: function() {

                }
            });
        }

    });

    $("#txtPatientId").keyup(function() {
        if ($("#txtPatientId").val() != "") {
            $("#txtPatientIdError").text("");
        }
    });

     $("#txtPatientId").change(function() {
        if ($("#txtPatientId").val() != "") {
            $("#txtPatientIdError").text("");
        }
    });

    $("#selConsultant").change(function() {
        if ($("#selConsultant").val() != "-1") {
            $("#selConsultantError").text("");		
        }
    });

    //$("#myAppointmentModal #txtDate").change(function() {
    $("#myAppointmentModal #txtDate").datepicker({autoclose: true, startDate: '+0d'}) .on('changeDate', function(selected){
        if ($("#myAppointmentModal #txtDate").val() != "") {
            $("#myAppointmentModal #txtDateError").text("");
            $("#divBookSlots").html("");
            $("#divAppointmentSlot").html("");
            var btnName = $("#btnBookAppointment").val();
            if (btnName == "Book Appointment") {
                if($("#selStaffName").length != 0){
                    var consultantId = $("#selStaffName").val();
                }
                else{
                    var consultantId ="-2";
                }
            }
            else{           
                if($("#selStaffNameModal").val() != "-1"){
                    var consultantId = $("#selStaffNameModal").val();
                }
            }
            var scheduleDate = $("#txtDate").val();
            var postData = {
                "operation": "showSlots",
                "consultantId": consultantId,
                "scheduleDate": scheduleDate
            }
            debugger;
            $.ajax({
                type: "POST",
                cache: false,
                url: "./controllers/admin/appointment.php",
                datatype: "json",
                data: postData,

                success: function(dataSet) {                
                    var newTime =[];
                    var bookedAppointment = [];
                    var duration = 0;
                    var preId = "0";
                    var k =0;
                    var parseData = jQuery.parseJSON(dataSet);
                    if (parseData.length > 0) {
                        $(parseData).each(function(index){
                            var obj = {};
                            var objAppointment = {};
                            var id = this.id;
                            if(id != preId){
                                duration = this.time_duration;
                                obj.time = ((parseInt(this.to_time.split(':')[0]) * 60) + (parseInt(this.to_time.split(':')[1])) - ((parseInt(this.from_time.split(':')[0]) * 60) + (parseInt(this.from_time.split(':')[1]))));
                                obj.fromTime = this.from_time.substring(0,5);
                                obj.toTime = this.to_time.substring(0,5);
                                newTime.push(obj);
                                preId = this.id;
                            }

                            if(this.start_date_time != null){
                                var startTime = this.start_date_time.split(" ")[1].substring(0,5);
                                var endTime = this.end_date_time.split(" ")[1].substring(0,5);
                                objAppointment.bookedSlots = startTime+"-"+endTime;
                                
                                objAppointment.startDate = this.start_date_time.split(" ")[0];
                                objAppointment.endDate = this.end_date_time.split(" ")[0];
                                
                                bookedAppointment.push(objAppointment); 
                            }   
                        });
                        var L = bookedAppointment.length;
                        for(var i = 0; i<newTime.length;i++) {
                            var timeSlot =[];
                            var fromTime = newTime[i].fromTime;
                            var toTime = fromTime;

                            var slots = ((newTime[i].time))/parseInt(duration);
                            var nextTime = '';
                            var nextFromTime = '';

                            for(var j=0;j<slots;j++) {
                                var obj = {};
                                var hrsToTime = toTime.split(':')[0];
                                var minsToTime = toTime.split(':')[1];

                                tempToTime = parseInt(minsToTime)+parseInt(duration);

                                if(tempToTime == 60) {
                                    tempToTime = "00";
                                    hrsToTime = parseInt(hrsToTime) + 1;
                                    hrsToTime = hrsToTime.toString().length == 1 ? "0" + hrsToTime : hrsToTime;
                                    toTime = hrsToTime + ":" + tempToTime;
                                } else if(tempToTime > 60){
									tempToTime = parseInt(tempToTime) - 60;
                                    hrsToTime = parseInt(hrsToTime) + 1;
                                    hrsToTime = hrsToTime.toString().length == 1 ? "0" + hrsToTime : hrsToTime;
                                    tempToTime = tempToTime.toString().length == 1 ? "0" + tempToTime : tempToTime;
                                    toTime = hrsToTime + ":" + tempToTime;
								}else {
                                    hrsToTime = hrsToTime.toString().length == 1 ? "0" + hrsToTime : hrsToTime;
									tempToTime = tempToTime.toString().length == 1 ? "0" + tempToTime : tempToTime;
                                    toTime = hrsToTime + ":" + tempToTime;
                                }

                                obj.time = fromTime + "-" + toTime;
                                fromTime = toTime;
                                timeSlot.push(obj);
                                
                                if(k < L){
                                    if((bookedAppointment[k].bookedSlots == timeSlot[j].time) && (bookedAppointment[k].startDate == scheduleDate)){
                                        var div = "<div class='bookedSlots'><label class='lblBookedSlots'  style='font-size: 18px;'>"+timeSlot[j].time+"</label></div>";                                    
                                        k++
                                    }
                                    else{               
                                        var div = "<div class='slots'><label style='cursor: pointer; font-size: 18px;' class='lblSlots'>"+timeSlot[j].time+"</label></div>";
                                    }                               
                                }
                                else{               
                                    var div = "<div class='slots'><label style='cursor: pointer; font-size: 18px;' class='lblSlots'>"+timeSlot[j].time+"</label></div>";
                                }
                                $("#divAppointmentSlot").append(div);
                                $(".slots").on('click', function(){
                                    $(".slots").removeClass("activeSelf");
                                    $(this) .addClass("activeSelf");
                                });
                                
                            }                   
                        }
                        $("#lblMessage").text("Available Slots.");
                        $("#lblMessage").css('color','green');
                    }
                    else{                        
                        $("#lblMessage").text("No schedule exists for this date.");
                        $("#lblMessage").css('color','red');
                    }
                },
                error: function() {

                }
            });
        }
    });
});

//Function for binding the staff type
function bindStaffType() {
    var postData = {
        "operation": "showStaffType"
    }

    $.ajax({
        type: "POST",
        cache: false,
        url: "./controllers/admin/schedule_staff.php",
        datatype: "json",
        data: postData,
        success: function(data) {
            if (data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";

                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
                }

                $('#selStaffType').html(option);
                $('#selStaffTypeModal').html(option);
            }
        },
        error: function() {
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();
        }
    });
}

//Function for binding the staff name 
function bindStaffName(value, check) {
    var postData = {
        "operation": "showStaffName",
        "staffTypeId": value
    }

    $.ajax({
        type: "POST",
        cache: false,
        url: "./controllers/admin/schedule_staff.php",
        datatype: "json",
        data: postData,
        success: function(data) {
            if (data != "") {
                var parseData = jQuery.parseJSON(data); // parse the value in Array string jquery
                var option = "<option value='-1'>--Select--</option>";

                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].name + "</option>";
                }
                if(check == null){
                    $('#selStaffName').html(option);
                }
                else{
                    $('#selStaffNameModal').html(option);
                    $("#selStaffNameModal").val(check);
                }
            }
        },
        error: function() {
            $('#messageMyModalLabel').text("Error");
            $('.modal-body').text("Temporary Unavailable to Respond.Try again later!!!");
            $('#messagemyModal').modal();
        }
    });
}

// define function for bind service type
function bindVisitType() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "controllers/admin/opd_registration.php",
        data: {
            "operation": "showVisitType"
        },
        success: function(data) {
            if (data != null && data != "") {
                var parseData = jQuery.parseJSON(data);

                var option = "<option value='-1'>-- Select --</option>";
                for (var i = 0; i < parseData.length; i++) {
                    option += "<option value='" + parseData[i].id + "'>" + parseData[i].type + "</option>";
                }
                $('#selConsultant').html(option);
            }
        },
        error: function() {}
    });
}

function clear() {
    $("#txtPatientId").val("");
    $("#selConsultant").val("-1");
    $("#txtNote").val("");
    $("#txtDate").val("");
    $("#txtFromTime").val("00:00");
    $("#txtToTime").val("00:00");
    $("#hdnVisitId").val("");
    $("#hdnId").val("");
    $("#txtPatientIdError").text("");
    $("#selConsultantError").text("");
    $("#txtDateError").text("");
    $("#txtFromTimeError").text("");
    $("#txtToTimeError").text("");
    $("#txtPatientId").removeAttr("disabled");
    $("#selConsultant").removeAttr("disabled");
    $("#btnRegister").removeAttr("disabled");
    $("#iconPatientSearch").show();
	$("#divBookSlots").html("");
	$("#divAppointmentSlot").html("");
	/*$("#divBookContent").show();
	$("#appointmentSlotDiv").hide();*/
    $("#viewSlotError").text("");
	$("#lblMessage").text("");
}
</script>
<input type="hidden" id="hdnId" value="">
<input type="hidden" id="hdnVisitId" value="">
<?php
    
    $userRole = $_SESSION['role'];
    if($userRole == 1 || $userRole == 3){
    echo '<div class="row">
            <!-- Ist row -->
            <div class="half_aligned">
                <div class="form-group">
                    <label class="forty_aligned control-label" for="selStaffType">Staff Type<span class="text-danger">*</span></label>
                    <div class="sixty_aligned">
                        <div class="">
                            <select id="selStaffType" class="form-control val_wrad" size="1">
                                <option value="-1">--Select--</option>
                            </select>
                            <span id="selStaffTypeError" class="error text-danger"></span>
                        </div>
                    </div>
                </div>
                <!--  form group-->
            </div>
            <div class="half_aligned">
                <div class="form-group">
                    <label class="forty_aligned control-label" for="selStaffName">Staff Name<span class="text-danger">*</span></label>
                    <div class="sixty_aligned">
                        <div class="">
                            <select id="selStaffName" class="form-control val_wrad" size="1">
                                <option value="-1">--Select--</option>
                            </select>
                            <span id="selStaffNameError" class="error text-danger"></span>
                        </div>
                    </div>
                </div>
                <!--  form group-->
            </div>
        </div>';
    }
    else{
        echo '<input type="hidden" id="hdnFunction" />';
    }
?>
<div id="mainHtml">
    <div id="calhead" style="padding-left:1px;padding-right:1px;">


        <div id="caltoolbar" class="ctoolbar">
            <div id="faddbtn" class="fbutton">
                <div>
                    <span title='Click to Create New Event' class="addcal">
                        New Appointment                
                    </span>
                </div>
            </div>
            <div class="btnseparator"></div>
            <div id="showtodaybtn" class="fbutton">
                <div>
                    <span title='Click to back to today ' class="showtoday">Today </span>
                </div>
            </div>
            <div class="btnseparator">

            </div>

            <div id="showdaybtn" class="fbutton">
                <div>
                    <span title='Day' class="showdayview">Day</span>
                </div>
            </div>
            <div id="showweekbtn" class="fbutton fcurrent">
                <div>
                    <span title='Week' class="showweekview">Week</span>
                </div>
            </div>
            <div id="showmonthbtn" class="fbutton">
                <div>
                    <span title='Month' class="showmonthview">Month</span>
                </div>
            </div>
            <div class="btnseparator"></div>
            <div id="showreflashbtn" class="fbutton">
                <div>
                    <span title='Refresh view' class="showdayflash">Refresh</span>
                </div>
            </div>
            <div class="btnseparator"></div>
            <div id="sfprevbtn" title="Prev" class="fbutton">
                <span class="fprev"></span>
            </div>
            <div id="sfnextbtn" title="Next" class="fbutton">
                <span class="fnext"></span>
            </div>
            <div class="fshowdatep fbutton">
                <div>
                    <input type="hidden" name="txtshow" id="hdtxtshow" />
                    <span id="txtdatetimeshow">Loading</span>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div style="padding:1px;">
        <div class="t1 chromeColor">&nbsp;</div>
        <div class="t2 chromeColor">&nbsp;</div>
        <div id="dvCalMain" class="calmain printborder">
            <div id="gridcontainer" style="overflow-y: visible;">
            </div>
        </div>
        <div class="t2 chromeColor">&nbsp;</div>
        <div class="t1 chromeColor">&nbsp;</div>
    </div>
</div>
<!--for update content Modal -->
<div class="modal fade" id="myAppointmentModal" role="dialog">
    <a href="#myAppointmentModal" id="Appointmentmodal" role="button" class="hidden" data-toggle="modal"></a>
    <div class="modal-dialog" style="width:750px;">
        <!-- Modal content-->
        <div class="modal-content" style="width:100% !important;">
            <div class="modal-header" style="background-color: #6fcceb;">
                <button type="button" class="modal_close" data-dismiss="modal"><div id="pop_up_close" class="close-modal">X</div></button>
                <h4 id="myAppointmentModalLabel"></h4>
            </div>
            <div class="modal-body" id="AppointmentModalBody">
				<div id="divBookContent">
					<div class="row">
						<!-- Ist row -->
						<input type="button" class="opd_btn" id="btnRegister" value="New Patient">
					</div>
					<div class="row">
						<!-- Ist row -->
						<div class="half_aligned">
							<div class="form-group">
								<label class="forty_aligned control-label" for="txtPatientId">Patient Id<span class="text-danger">*</span></label>
								<div class="sixty_aligned">
									<div class="">
										<input type="text" id="txtPatientId" name="txtPatientId" class="form-control"><i id="iconPatientSearch" class="fa fa-search" style="position: relative;"></i>
										<span id="txtPatientIdError" class="error text-danger">
									</div>
								</div>
							</div><!--  form group-->
						</div>
						<div class="half_aligned">
							<div class="form-group">
								<label class="forty_aligned control-label" for="selConsultant">Visit Type<span class="text-danger">*</span></label>
										<div class="sixty_aligned">
											<div class="">
												<select id="selConsultant" class="form-control">
											<option value="-1">-- Select --</option>
											<option value="1">New</option>
											<option value="2">Follow Up</option>                                 
										</select>
										<span id="selConsultantError" class="error text-danger">
									</div>
								</div>
							</div><!--  form group-->
						</div>
					</div><!-- end row --> 
					<div class="row"><!-- Ist row -->
						<div class="half_aligned">
							<div class="form-group">
								<label class="forty_aligned control-label" for="txtNote">Note</label>
								<div class="sixty_aligned">
									<div class="">
										<input type="text" id="txtNote" name="txtNote" class="form-control" >
										<span id="txtNoteError" class="error text-danger">
									</div>
								</div>
							</div><!--  form group-->
						</div>
						<div class="half_aligned">
							<div class="form-group">
								<label class="forty_aligned control-label" for="txtDate">Date<span class="text-danger">*</span></label>
								<div class="sixty_aligned">
									<div class="">
										<input type="text" id="txtDate" name="txtDate" data-date-format="yyyy-mm-dd" class="form-control ">
										<span id="txtDateError" class="error text-danger">
									</div>
								</div>
							</div>
						</div>
					</div> 

					<div class="row staffDropDown" style="display: none;margin-bottom: 13px;">
						<!-- Ist row -->
						<div class="half_aligned">
							<div class="form-group">
								<label class="forty_aligned control-label" for="selStaffTypeModal">Staff Type</label>
								<div class="sixty_aligned">
									<div class="">
										<select id="selStaffTypeModal" class="form-control val_wrad" size="1">
											<option value="-1">--Select--</option>
										</select>
										<span id="selStaffTypeModalError" class="error text-danger"></span>
									</div>
								</div>
							</div>
							<!--  form group-->
						</div>
						<div class="half_aligned">
							<div class="form-group">
								<label class="forty_aligned control-label" for="selStaffNameModal">Staff Name</label>
								<div class="sixty_aligned">
									<div class="">
										<select id="selStaffNameModal" class="form-control val_wrad" size="1">
											<option value="-1">--Select--</option>
										</select>
										<span id="selStaffNameModalError" class="error text-danger"></span>
									</div>
								</div>
							</div>
							<!--  form group-->
						</div>
					</div><!-- end row --> 
                    <div class="row">
                         <div class="half_aligned">                    
                            <div class="form-group">
                                <label  class="control-label" id="lblMessage"></label>
                            </div>
                        </div>                       
                    </div>	
                    <div class="row">
                        <div id= "appointmentSlotDiv" >
                            <div id="divAppointmentSlot" >
                            </div>                      
                        </div>
                    </div>
					
					<div class="row"><!-- Ist row -->					
					   <div class="half_aligned">					   
							<div class="form-group">
								<label class="forty_aligned control-label"></label>
								<div class="sixty_aligned">
									<div class="">
									</div>
								</div>
							</div>
						</div>
						<div class="half_aligned">						
							<div class="form-group">
								<label class="forty_aligned control-label"></label>
								<div class="sixty_aligned">
									<div class="">
										<div class="form-group" id="divBookSlots">
										</div>
										<span id="viewSlotError" class="error text-danger">
									</div>
								</div>
							</div>
						</div>  
					</div> 
					<div class="row"><!-- Ist row -->
						<input type="button" class="book" id="btnBookAppointment" value="Book Appointment">
					</div> 
				</div>	
            </div>
            <div class="modal-footer" style="background-color: #6fcceb;">
               
            </div> 
      </div>              
    </div>
</div>
<!--for update content Modal -->
<div class="modal fade" id="myAppointmentPatientModal" role="dialog">
    <input type="hidden" id="hdnAppointment" value="">
    <a href="#myAppointmentPatientModal" id="AppointmentPatientmodal" role="button" class="hidden" data-toggle="modal"></a>
    <div class="modal-dialog" style="width:750px;">         
      <!-- Modal content-->
        <div class="modal-content" style="width:100% !important;">
            <div class="modal-header" style="background-color: #6fcceb;">
                <button type="button" class="close" data-dismiss="modal"><div id="pop_up_close" class="close-modal">X</div></button>
                <h4 id="myAppointmentPatientModalLabel"></h4>
            </div>
            <div class="modal-body" id="AppointmentPatientModalBody"> 
                                          
            </div>
            <div class="modal-footer" style="background-color: #6fcceb;">               
            </div> 
      </div>              
    </div>
</div>

<!--for update content Modal -->
<div class="modal fade" id="myRegisterPatientModal" role="dialog">
    <input type="hidden" id="hdnRegister" value="">
    <a href="#myRegisterPatientModal" id="RegisterPatientmodal" role="button" class="hidden" data-toggle="modal"></a>
    <div class="modal-dialog" style="width: 1000px;">         
      <!-- Modal content-->
        <div class="modal-content" style="width:100% !important;">
            <div class="modal-header" style="background-color: #6fcceb;">
                <button type="button" class="close_patient" data-dismiss="modal"><div id="close" class="close-modal">X</div></button>
                <h4 id="myRegisterPatientModalLabel"></h4>
            </div>
            <div class="modal-body" id="RegisterPatientModalBody"> 
                                          
            </div>
            <div class="modal-footer" style="background-color: #6fcceb;">               
            </div> 
      </div>              
    </div>
</div>

<!-- message pop up -->
<div class="modal fade" id="messagemyModal" role="dialog">
    <div id="message_popup">
        <a href="#messagemyModal" id="mymessageModal" role="button" class="hidden" data-toggle="modal"></a>
        <div class="modal-dialog modal-style">          
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="pop_up_close" data-dismiss="modal">×</button>
                    <h4 id="messageMyModalLabel"></h4>
                </div>
                <div class="modal-body" id="messageBody">                   
                </div>
                <div class="modal-footer" style="">
                    <input type="button" class="btn" data-dismiss="modal" aria-hidden="true" value="OK" id="ok"  />
                </div>
          </div>              
        </div>
    </div>    
</div>

<!--Confirm  Modal -->
<div class="modal fade" id="confirmUpdateModal" role="dialog">
    <div id="confirm_popup">
        <a href="#confirmUpdateModal" id="updateConfirmModal" role="button" class="hidden" data-toggle="modal"></a>
        <div class="modal-dialog modal-style">          
          <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close-confirm" id="pop_up_close" data-dismiss="modal">×</button>
                    <h4 id="confirmUpdateModalLabel">Confirmation</h4>
                </div>
                <div class="" id="updateBody">                  
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn" data-dismiss="modal" aria-hidden="true" value="Confirm" id="btnConfirm" />
                    <input type="button" class="btn" style="margin-bottom: 2px !important;" data-dismiss="modal" aria-hidden="true" value="Cancel" id="cancel" />
                </div>
          </div>              
        </div>
    </div>    
</div>
<style type="text/css">
#confirmUpdateModal,#messagemyModal{
    width: 100%;
    margin-left: 0px !important;
}
.modal-style{
    width: 30%;
}
</style>
