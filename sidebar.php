<div id="sidebar">
	
	<!-- div to make it scrollable -->
	<div class="sidebar-scroll pre-scrollable">
		
		<!-- Starting Sidebar Content -->
		<div class="sidebar-content">
			<a href="./index.php" class="sidebar-brand">
				<i class="gi gi-hospital"></i><strong><?php echo $software_name;?></strong>
			</a>
			<div class="sidebar-section sidebar-user clearfix">
				<div class="sidebar-user-avatar">
				<a href="index.php">
					<img src="images/<?php echo $_SESSION['hpImage']; ?>" alt="avatar" />
				</a>
				</div>
				<div class="sidebar-user-name"><?php echo $_SESSION['globalName']; ?></div>
				<div class="sidebar-user-links">
					<a href="" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user" id="profile"></i></a>
					<!-- <a href="" data-toggle="tooltip" data-placement="bottom" title="Messages"><i class="gi gi-envelope"></i></a>
					<a href="" data-toggle="modal" class="enable-tooltip" data-placement="bottom" title="Settings"><i class="gi gi-cogwheel"></i></a> -->
					<a href="logout.php" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
				</div>
			</div>
			
			<!-- Side bar pages -->
			<?php
				include 'controllers/admin/config.php';
				$query = "SELECT pages.id as id,pages.screen_name as title,pages.parent_menu_id as parentid,pages.screen_load_url as link FROM `pages_options` LEFT JOIN pages ON pages.id=pages_options.screen_id WHERE user_id = ".$_SESSION['globaluser']." AND view = 1  ORDER BY pages.sort_order";
				
				$resultSet = mysqli_query($conn,$query);
				$resultRows = array();
				$menu = array(
					'menus' => array(),
					'parent_menus' => array()
				);
				while ($row = mysqli_fetch_assoc($resultSet)) {
					//creates entry into menus array with current menu id ie. $menus['menus'][1]
					$menu['menus'][$row['id']] = $row;
					//creates entry into parent_menus array. parent_menus array contains a list of all menus with children
					$menu['parent_menus'][$row['parentid']][] = $row['id'];
				}
				function buildMenu($parent,$menu,$counter) {
					$html = "";
					
					if (isset($menu['parent_menus'][$parent])) {
						if($counter == 0){
							$html .= "<ul class='sidebar-nav'>";
							$counter++;
						}
						else{
							$html .= "<ul>";
						}
						foreach ($menu['parent_menus'][$parent] as $menu_id) {
							if (!isset($menu['parent_menus'][$menu_id])) {
								$html .= "<li><a href='' data-id=" . $menu['menus'][$menu_id]['id'] . "   data-parentId=" .$menu['menus'][$menu_id]['parentid']."  data-url='" . $menu['menus'][$menu_id]['link'] . "' class='sidebar-nav-menu' >" . $menu['menus'][$menu_id]['title'] . "</a></li>";
							}
							if (isset($menu['parent_menus'][$menu_id])) {
								$html .= "<li><a href='' data-id=" . $menu['menus'][$menu_id]['id'] . "  data-parentId=" .$menu['menus'][$menu_id]['parentid']."  data-url='" . $menu['menus'][$menu_id]['link'] . "' class='sidebar-nav-submenu' ><i class='fa fa-angle-left sidebar-nav-indicator'></i>" . $menu['menus'][$menu_id]['title'] . "</a>";
								$html .= buildMenu($menu_id, $menu,1);
								$html .= "</li>";
							}
						}
						$html .= "</ul>";
					}
					return $html;
				}
				echo buildMenu(0, $menu,0);;
			?>
		</div><!--end sidebar-content class -->
	</div><!--end sidebar-scroll class -->
</div><!--end sidebar -->