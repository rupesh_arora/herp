<header class="navbar navbar-default" id="header">
   <ul class="nav navbar-nav-custom slidebar_ul width-10">
        <!-- Main Sidebar Toggle Button -->
        <li>
            <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');" id="slide_sidebar">
                <i class="fa fa-bars fa-fw"></i>
            </a>
        </li>
    </ul>    
    <div class="hearderInfo"><h1 class="hospitalName"></h1><p id="currentTime"></p></div>
    <ul class="nav navbar-nav-custom pull-right width-10">
        <li class="dropdown">
            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
            <img src="./upload_images/staff_dp/<?php echo $_SESSION['image'] ?>" alt="Profile Pic" /> <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                <li class="dropdown-header text-center">Account</li>
               <!--  <li>
                    <a href="./page_ready_timeline.php.html">
                    <i class="fa fa-clock-o fa-fw pull-right"></i>
                    <span class="badge pull-right">10</span>
                    Updates
                    </a>
                    <a href="./page_ready_inbox.php.html">
                    <i class="fa fa-envelope-o fa-fw pull-right"></i>
                    <span class="badge pull-right">5</span>
                    Messages
                    </a>
                    <a href="./page_ready_pricing_tables.php.html"><i class="fa fa-magnet fa-fw pull-right"></i>
                    <span class="badge pull-right">3</span>
                    Subscriptions
                    </a>
                    <a href="./page_ready_faq.php.html"><i class="fa fa-question fa-fw pull-right"></i>
                    <span class="badge pull-right">11</span>
                    FAQ
                    </a>
                </li> -->
                <li class="divider"></li>
                <li>
                    <a href="#modal-user-profile" data-toggle="modal">
                    <i class="fa fa-user fa-fw pull-right"></i>
                    My Profile
                    </a>
                    <a href="#modal-user-settings" data-toggle="modal">
                    <i class="fa fa-cog fa-fw pull-right"></i>
                    Change Password
                    </a>
					<a href="help/index.php" data-toggle="modal">
                    <i class="fa fa-question fa-fw pull-right"></i>
                    Help
                    </a>
                    <a href="" data-toggle="modal" id="notficatonClick">
                    <i class="fa fa-bell fa-fw pull-right"></i>
                    Notification
                    </a>
                </li>
                <li class="divider"></li>
                <li id='btnLogOut'>
                    <a href="logout.php"><i class="fa fa-ban fa-fw pull-right"></i>Logout</a>
                </li>
            </ul>
        </li>
    </ul>
</header>